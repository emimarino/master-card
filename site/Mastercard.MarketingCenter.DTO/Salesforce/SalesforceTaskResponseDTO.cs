﻿using Newtonsoft.Json;

namespace Mastercard.MarketingCenter.DTO.Salesforce
{
    public class SalesforceTaskResponseDTO
    {
        [JsonProperty("id")]
        public string TaskId { get; set; }
        [JsonProperty("success")]
        public bool Success { get; set; }
        [JsonProperty("errors")]
        public string[] Errors { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("errorCode")]
        public string ErrorCode { get; set; }
        [JsonProperty("fields")]
        public string[] Fields { get; set; }
    }
}