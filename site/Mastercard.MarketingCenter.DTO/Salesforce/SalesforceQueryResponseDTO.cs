﻿using Newtonsoft.Json;

namespace Mastercard.MarketingCenter.DTO.Salesforce
{
    public class SalesforceQueryResponseDTO
    {
        [JsonProperty("totalSize")]
        public int TotalSize { get; set; }
        [JsonProperty("done")]
        public bool Done { get; set; }
        [JsonProperty("nextRecordsUrl")]
        public string NextRecordsUrl { get; set; }
        [JsonProperty("records")]
        public object[] Records { get; set; }
    }
}