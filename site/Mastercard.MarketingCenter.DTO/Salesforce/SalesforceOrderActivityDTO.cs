﻿namespace Mastercard.MarketingCenter.DTO.Salesforce
{
    public class SalesforceOrderActivityDTO : SalesforceActivityDTO
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
        public string Url { get; set; }
    }
}