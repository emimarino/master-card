﻿using Newtonsoft.Json;

namespace Mastercard.MarketingCenter.DTO.Salesforce
{
    public class SalesforceAccessTokenResponseDTO
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("scope")]
        public string Scope { get; set; }
        [JsonProperty("instance_url")]
        public string InstanceUrl { get; set; }
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("token_type")]
        public string TokenType { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("errorCode")]
        public string ErrorCode { get; set; }
    }
}