﻿using System;

namespace Mastercard.MarketingCenter.DTO.Salesforce
{
    public abstract class SalesforceActivityDTO
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Region { get; set; }
        public DateTime Date { get; set; }
    }
}