﻿namespace Mastercard.MarketingCenter.DTO.Salesforce
{
    public class SalesforceDownloadActivityDTO : SalesforceActivityDTO
    {
        public string Name { get; set; }
        public string File { get; set; }
        public string Url { get; set; }
    }
}