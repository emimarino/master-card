﻿using Newtonsoft.Json;

namespace Mastercard.MarketingCenter.DTO.Salesforce
{
    public class SalesforceContactQueryResponseDTO
    {
        [JsonProperty("attributes")]
        public SalesforceContactAttributesResponseDTO Attributes { get; set; }
        public string Id { get; set; }
        public SalesforceContactAccountResponseDTO Account { get; set; }
        public string Email { get; set; }
    }

    public class SalesforceContactAttributesResponseDTO
    {
        [JsonProperty("type")]
        public string Type { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
    }

    public class SalesforceContactAccountResponseDTO
    {
        [JsonProperty("attributes")]
        public SalesforceContactAttributesResponseDTO Attributes { get; set; }
        public string OwnerId { get; set; }
        public string Id { get; set; }
    }
}