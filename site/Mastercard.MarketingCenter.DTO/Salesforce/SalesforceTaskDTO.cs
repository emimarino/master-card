﻿using Newtonsoft.Json;

namespace Mastercard.MarketingCenter.DTO.Salesforce
{
    public class SalesforceTaskDTO
    {
        public string Status { get; set; }
        public string WhatId { get; set; }
        [JsonProperty("Activity_Group__c")]
        public string TaskActivityType { get; set; }
        public string ActivityDate { get; set; }
        public string WhoId { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
    }
}