﻿using Newtonsoft.Json;

namespace Mastercard.MarketingCenter.DTO.GDP
{
    public class UserAddressDTO
    {
        [JsonProperty("addressLine1")]
        public string Address1 { get; set; }

        [JsonProperty("addressLine2")]
        public string Address2 { get; set; }

        [JsonProperty("addressLine3")]
        public string Address3 { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("postalCode")]
        public string PostalCode { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("province")]
        public string Province { get; set; }
    }
}