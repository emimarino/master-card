﻿using Newtonsoft.Json;

namespace Mastercard.MarketingCenter.DTO.GDP
{
    public class RequestSearchKeyDTO
    {
        [JsonProperty("email")]
        public string Email { get; set; }
    }
}