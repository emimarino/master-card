﻿namespace Mastercard.MarketingCenter.DTO.GDP
{
    public class ExpiredUserDTO
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string Region { get; set; }
        public string Language { get; set; }
    }
}