﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.DTO.GDP
{
    public class GdpResponseViewDTO : GdpResponseDTO
    {
        private IList<UserDTO> _userDTOs;
        [JsonIgnore]
        public IList<UserDTO> UserDTOs { get { return _userDTOs; } set { _userDTOs = value; } }

        public GdpResponseViewDTO()
        {
            UserDTOs = new List<UserDTO>();
        }

        public bool ShouldSerializeresponseList()
        {
            return (UserDTOs.Count > 0);
        }

        [JsonProperty("responseData")]
        public override string ResponseData
        {
            get
            {
                return JsonConvert.SerializeObject(UserDTOs);
            }
        }
    }
}