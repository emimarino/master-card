﻿namespace Mastercard.MarketingCenter.DTO.GDP
{
    public class UseCategoryDTO
    {
        public string UseCategoryCode { get; set; }
        public string UseCategoryValue { get; set; }
        public string UseCategoryDescription { get; set; }
    }
}