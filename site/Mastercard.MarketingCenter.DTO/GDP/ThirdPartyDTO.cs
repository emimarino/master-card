﻿namespace Mastercard.MarketingCenter.DTO.GDP
{
    public class ThirdPartyDTO
    {
        public string CompanyName { get; set; }
    }
}