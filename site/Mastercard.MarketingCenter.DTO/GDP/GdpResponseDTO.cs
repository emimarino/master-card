﻿using Newtonsoft.Json;

namespace Mastercard.MarketingCenter.DTO.GDP
{
    public class GdpResponseDTO
    {
        private string _serviceFunctionCode;
        [JsonProperty("serviceFunctionCode")]
        public string ServiceFunctionCode { get { return _serviceFunctionCode; } set { _serviceFunctionCode = value; } }

        private int _requestIdentifier;
        [JsonProperty("requestIdentifier")]
        public int RequestId { get { return _requestIdentifier; } set { _requestIdentifier = value; } }

        private string _responseCode;
        [JsonProperty("responseCode")]
        public string ResponseCode { get { return _responseCode; } set { _responseCode = value; } }

        [JsonProperty("responseData")]
        public virtual string ResponseData { get; set; }
    }
}