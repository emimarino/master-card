﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.DTO.GDP
{
    public class GdpResponseConsentDTO : GdpResponseDTO
    {
        private IList<UserConsentDTO> _userConsentDTOs;
        [JsonIgnore]
        public IList<UserConsentDTO> UserConsentDTOs { get { return _userConsentDTOs; } set { _userConsentDTOs = value; } }
        
        public GdpResponseConsentDTO()
        {
            _userConsentDTOs = new List<UserConsentDTO>();
        }

        public bool ShouldSerializeresponseList()
        {
            return (_userConsentDTOs.Count > 0);
        }

        public override string ResponseData
        {
            get
            {
                return JsonConvert.SerializeObject(_userConsentDTOs);
            }
        }
    }
}