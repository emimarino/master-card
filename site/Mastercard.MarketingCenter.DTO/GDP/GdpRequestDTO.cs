﻿using Newtonsoft.Json;

namespace Mastercard.MarketingCenter.DTO.GDP
{
    public class GdpRequestDTO
    {
        public GdpRequestDTO()
        {
            SearchKey = new RequestSearchKeyDTO();
        }

        [JsonProperty("requestId")]
        public int RequestId { get; set; }

        [JsonProperty("requestType")]
        public string RequestType { get; set; }

        [JsonProperty("requestContext")]
        public string RequestContext { get; set; }

        [JsonProperty("searchKey")]
        public RequestSearchKeyDTO SearchKey { get; set; }
    }
}