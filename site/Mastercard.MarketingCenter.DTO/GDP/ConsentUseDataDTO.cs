﻿namespace Mastercard.MarketingCenter.DTO.GDP
{
    public class ConsentUseDataDTO
    {
        public UseCategoryDTO UseCategory { get; set; }
        public ConsentDataDTO ConsentData { get; set; }
    }
}