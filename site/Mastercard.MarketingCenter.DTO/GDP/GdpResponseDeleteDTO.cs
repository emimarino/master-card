﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.DTO.GDP
{
    public class GdpResponseDeleteDTO : GdpResponseDTO
    {
        private IList<NonDeletedDataDTO> _responseList;
        [JsonProperty("responseList")]
        public IList<NonDeletedDataDTO> NonDeletedDataDTOs { get { return _responseList; } set { _responseList = value; } }

        public GdpResponseDeleteDTO()
        {
            NonDeletedDataDTOs = new List<NonDeletedDataDTO>();
        }

        public bool ShouldSerializeresponseList()
        {
            return (NonDeletedDataDTOs.Count > 0);
        }
    }
}