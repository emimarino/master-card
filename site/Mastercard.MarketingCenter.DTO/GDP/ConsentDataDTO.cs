﻿namespace Mastercard.MarketingCenter.DTO.GDP
{
    public class ConsentDataDTO
    {
        public string Data { get; set; }
        public string DocumentType { get; set; }
    }
}