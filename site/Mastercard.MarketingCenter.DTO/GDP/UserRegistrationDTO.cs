﻿using System;

namespace Mastercard.MarketingCenter.DTO.GDP
{
    public class UserRegistrationDTO
    {
        public int UserRegistrationId { get; set; }
        public string IndividualFirstName { get; set; }

        public string IndividualLastName { get; set; }

        public string IndividualEmail { get; set; }

        public string LocalIssuerName { get; set; }

        public string NoteComment { get; set; }

        public string CID { get; set; }

        public string ImportedIssuerName { get; set; }

        public string RegionId { get; set; }

        public DateTime LastLogin { get; set; }

        public DateTime RegisteredDate { get; set; }
    }
}