﻿using Newtonsoft.Json;

namespace Mastercard.MarketingCenter.DTO.Priceless
{
    public abstract class PricelessApiDTO
    {
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("msg")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public dynamic Data { get; set; }
    }
}