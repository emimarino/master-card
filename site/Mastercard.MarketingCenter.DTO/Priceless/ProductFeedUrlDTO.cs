﻿namespace Mastercard.MarketingCenter.DTO.Priceless
{
    public class ProductFeedUrlDTO : PricelessApiDTO
    {
        public string Url
        {
            get
            {
                return Data.url;
            }
        }
    }
}