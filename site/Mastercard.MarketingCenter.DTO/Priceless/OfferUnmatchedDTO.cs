﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace Mastercard.MarketingCenter.DTO.Priceless
{
    [XmlRoot("OffersUnmatched")]
    public class OfferUnmatchedDTO
    {
        [XmlElement("PricelessOfferUnmatched")]
        public List<PricelessOfferUnmatched> PricelessOfferUnmatched { get; set; }

        public OfferUnmatchedDTO()
        {
            PricelessOfferUnmatched = new List<PricelessOfferUnmatched>();
        }

        public OfferUnmatchedDTO(string pricelessOfferUnmatchedData)
        {
            PricelessOfferUnmatched = ((OfferUnmatchedDTO)new XmlSerializer(typeof(OfferUnmatchedDTO))
                                                                        .Deserialize(new StringReader(pricelessOfferUnmatchedData)))
                                                                        .PricelessOfferUnmatched;
        }

        public override string ToString()
        {
            string result = string.Empty;
            foreach (var offer in PricelessOfferUnmatched.OrderBy(pou => pou.PricelessOfferId))
            {
                result += $"<p>Priceless Offer Id: {offer.PricelessOfferId}, Priceless Field: {offer.PricelessField}, Pricesless Value: {offer.PricelessValue}</p>";
            }

            return result;
        }
    }

    public class PricelessOfferUnmatched
    {
        [XmlElement("PricelessOfferId")]
        public string PricelessOfferId { get; set; }
        [XmlElement("PricelessField")]
        public string PricelessField { get; set; }
        [XmlElement("PricelessValue")]
        public string PricelessValue { get; set; }
    }
}