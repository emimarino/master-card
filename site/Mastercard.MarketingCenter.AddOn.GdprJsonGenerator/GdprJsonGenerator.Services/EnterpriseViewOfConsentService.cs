﻿using GdprJsonGenerator.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using GdprJsonGenerator.Repositories;

namespace GdprJsonGenerator.Services
{
    public class EnterpriseViewOfConsentService : IEnterpriseViewOfConsentService
    {
        private IEnterpriseViewOfConsentRepository _enterpriesViewOfConsentRepository;

        public EnterpriseViewOfConsentService(IEnterpriseViewOfConsentRepository enterpriesViewOfConsentRepository)
        {
            _enterpriesViewOfConsentRepository = enterpriesViewOfConsentRepository;
        }

        public string GetEnterpriseViewOfConsent(string emails, string serviceFunctionCode, string requestId)
        {
            var result = new EnterpriseViewOfConsentDTO { serviceFunctionCode = serviceFunctionCode, requestId = requestId };

            result.responseCode = Status.NotFound;

            foreach (var email in emails.Split(','))
            {
                var userConsentObject = _enterpriesViewOfConsentRepository.GetEnterpiseViewOfConsent(email);
                if (userConsentObject != null)
                {
                    result.responseData.responseList.Add(userConsentObject);
                    result.responseCode = Status.Success;
                }
            }

            return JsonConvert.SerializeObject(result, Formatting.Indented);
        }
    }
}
