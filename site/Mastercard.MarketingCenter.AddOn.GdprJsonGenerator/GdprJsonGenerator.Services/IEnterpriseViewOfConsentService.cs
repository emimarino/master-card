﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GdprJsonGenerator.Services
{
    public interface IEnterpriseViewOfConsentService
    {
        string GetEnterpriseViewOfConsent(string emails, string serviceFunctionCode, string requestId);
    }
}
