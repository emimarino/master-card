﻿using GdprJsonGenerator.DTO;
using GdprJsonGenerator.Repositories;
using Newtonsoft.Json;
using System;

namespace GdprJsonGenerator.Services
{
    public class ViewRequestService : IViewRequestService
    {
        IViewRequestRepository _viewRequestRepository;
        public ViewRequestService(IViewRequestRepository viewRequestRepository)
        {
            _viewRequestRepository = viewRequestRepository;
        }
        public string GetViewRequest(string emails, string serviceFunctionCode, string requestId)
        {
            ViewRequestDTO result = new ViewRequestDTO { serviceFunctionCode = serviceFunctionCode, requestId = requestId };

            foreach (var email in emails.Split(','))
            {
                var dataObject = _viewRequestRepository.GetViewRequest(email);
                result.responseData.responseList.Add(dataObject);
            }

            result.responseCode = Status.Success;

            return JsonConvert.SerializeObject(result, Formatting.Indented);
        }
    }
}
