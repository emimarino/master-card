﻿namespace GDPR_JSON_Generator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGenerateEnterpriseViewOfConsent = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEmails = new System.Windows.Forms.TextBox();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtServiceFunctionCode = new System.Windows.Forms.TextBox();
            this.txtRequestId = new System.Windows.Forms.TextBox();
            this.cmdGenerateViewData = new System.Windows.Forms.Button();
            this.cmdDeleteData = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnGenerateEnterpriseViewOfConsent
            // 
            this.btnGenerateEnterpriseViewOfConsent.Location = new System.Drawing.Point(538, 21);
            this.btnGenerateEnterpriseViewOfConsent.Name = "btnGenerateEnterpriseViewOfConsent";
            this.btnGenerateEnterpriseViewOfConsent.Size = new System.Drawing.Size(219, 54);
            this.btnGenerateEnterpriseViewOfConsent.TabIndex = 0;
            this.btnGenerateEnterpriseViewOfConsent.Text = "Generate Enterprise View Of Consent";
            this.btnGenerateEnterpriseViewOfConsent.UseVisualStyleBackColor = true;
            this.btnGenerateEnterpriseViewOfConsent.Click += new System.EventHandler(this.btnGenerateEnterpriseViewOfConsent_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Email/s (comma separated)";
            // 
            // txtEmails
            // 
            this.txtEmails.Location = new System.Drawing.Point(223, 89);
            this.txtEmails.Name = "txtEmails";
            this.txtEmails.Size = new System.Drawing.Size(280, 22);
            this.txtEmails.TabIndex = 2;
            // 
            // txtResult
            // 
            this.txtResult.Location = new System.Drawing.Point(26, 131);
            this.txtResult.Multiline = true;
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(477, 307);
            this.txtResult.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "serviceFunctionCode";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "requestId";
            // 
            // txtServiceFunctionCode
            // 
            this.txtServiceFunctionCode.Location = new System.Drawing.Point(223, 21);
            this.txtServiceFunctionCode.Name = "txtServiceFunctionCode";
            this.txtServiceFunctionCode.Size = new System.Drawing.Size(280, 22);
            this.txtServiceFunctionCode.TabIndex = 7;
            this.txtServiceFunctionCode.Text = "CDMMCMW-b";
            // 
            // txtRequestId
            // 
            this.txtRequestId.Location = new System.Drawing.Point(223, 55);
            this.txtRequestId.Name = "txtRequestId";
            this.txtRequestId.Size = new System.Drawing.Size(280, 22);
            this.txtRequestId.TabIndex = 8;
            // 
            // cmdGenerateViewData
            // 
            this.cmdGenerateViewData.Location = new System.Drawing.Point(538, 89);
            this.cmdGenerateViewData.Name = "cmdGenerateViewData";
            this.cmdGenerateViewData.Size = new System.Drawing.Size(219, 56);
            this.cmdGenerateViewData.TabIndex = 10;
            this.cmdGenerateViewData.Text = "Generate View Data";
            this.cmdGenerateViewData.UseVisualStyleBackColor = true;
            this.cmdGenerateViewData.Click += new System.EventHandler(this.cmdGenerateViewData_Click);
            // 
            // cmdDeleteData
            // 
            this.cmdDeleteData.Location = new System.Drawing.Point(538, 165);
            this.cmdDeleteData.Name = "cmdDeleteData";
            this.cmdDeleteData.Size = new System.Drawing.Size(219, 56);
            this.cmdDeleteData.TabIndex = 11;
            this.cmdDeleteData.Text = "Delete Data";
            this.cmdDeleteData.UseVisualStyleBackColor = true;
            this.cmdDeleteData.Click += new System.EventHandler(this.cmdDeleteData_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cmdDeleteData);
            this.Controls.Add(this.cmdGenerateViewData);
            this.Controls.Add(this.txtRequestId);
            this.Controls.Add(this.txtServiceFunctionCode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtResult);
            this.Controls.Add(this.txtEmails);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGenerateEnterpriseViewOfConsent);
            this.Name = "Form1";
            this.Text = "GDPR Data Access Request Manager";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGenerateEnterpriseViewOfConsent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEmails;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtServiceFunctionCode;
        private System.Windows.Forms.TextBox txtRequestId;
        private System.Windows.Forms.Button cmdGenerateViewData;
        private System.Windows.Forms.Button cmdDeleteData;
    }
}

