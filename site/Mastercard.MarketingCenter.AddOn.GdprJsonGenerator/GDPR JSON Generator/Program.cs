﻿using GdprJsonGenerator.Repositories;
using GdprJsonGenerator.Services;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using SimpleInjector;
using System;
using System.Configuration;
using System.Windows.Forms;

namespace GDPR_JSON_Generator
{
    static class Program
    {
        public static Container container;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Bootstrap();
            Application.Run(new Form1());
        }

        private static void Bootstrap()
        {
            container = new Container();

            //container.Options.DefaultScopedLifestyle = new SimpleInjector.Lifestyles.ThreadScopedLifestyle();

            // 2. Configure the container (register)

            var dbConnectionFactory = new OrmLiteConnectionFactory(ConfigurationManager.ConnectionStrings["MasterCardMarketingCenter"].ConnectionString, SqlServerDialect.Provider);
            container.Register<IDbConnectionFactory>(() => dbConnectionFactory);
            container.Register<IEnterpriseViewOfConsentService, EnterpriseViewOfConsentService>();
            container.Register<IEnterpriseViewOfConsentRepository, EnterpriseViewOfConsentRepository>();
            container.Register<IViewRequestService, ViewRequestService>();
            container.Register<IViewRequestRepository, ViewRequestRepository>();
            container.Register<IUserPrivacyDataService, UserPrivacyDataService>();
            //container.Register<Form1>();

            // 3. Verify your configuration
            container.Verify();
        }
    }
}