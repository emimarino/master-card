﻿using GdprJsonGenerator.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Windows.Forms;

namespace GDPR_JSON_Generator
{
    public partial class Form1 : Form
    {
        private IEnterpriseViewOfConsentService _enterpriseViewOfConsentService;
        private IViewRequestService _viewRequestService;
        private IUserPrivacyDataService _deleteDataService;

        public Form1()
        {
            InitializeComponent();
            _enterpriseViewOfConsentService = Program.container.GetInstance<IEnterpriseViewOfConsentService>();
            _viewRequestService = Program.container.GetInstance<IViewRequestService>();
            _deleteDataService = Program.container.GetInstance<IUserPrivacyDataService>();
        }

        private void btnGenerateEnterpriseViewOfConsent_Click(object sender, EventArgs e)
        {
            if (ValidateEmails())
            {
                txtResult.Text = _enterpriseViewOfConsentService.GetEnterpriseViewOfConsent(
                    txtEmails.Text,
                    txtServiceFunctionCode.Text,
                    txtRequestId.Text
                    );
            }
        }

        private void cmdGenerateViewData_Click(object sender, EventArgs e)
        {
            if (ValidateEmails())
            {
                txtResult.Text = _viewRequestService.GetViewRequest(
                    txtEmails.Text,
                    txtServiceFunctionCode.Text,
                    txtRequestId.Text
                    );
            }
        }

        private void cmdDeleteData_Click(object sender, EventArgs e)
        {
            if (ValidateEmail())
            {
                txtResult.Text = _deleteDataService.MaskUserData(txtEmails.Text, out _);
            }
        }

        private bool ValidateEmails()
        {
            return true;
        }

        private bool ValidateEmail()
        {
            return true;
        }
    }
}