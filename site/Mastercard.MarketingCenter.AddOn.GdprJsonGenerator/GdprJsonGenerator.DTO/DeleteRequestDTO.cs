﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GdprJsonGenerator.DTO
{
    public class DeleteRequestDTO
    {
        public string serviceFunctionCode { get; set; }
        public string requestId { get; set; }
        public string responseCode { get; set; }
        public DeleteRequestDataDTO responseData { get; set; }

        public DeleteRequestDTO()
        {
            responseData = new DeleteRequestDataDTO();
        }
    }

    public class DeleteRequestDataDTO
    {
        public string userId { get; set; }
    }
}
