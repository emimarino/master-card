﻿namespace GdprJsonGenerator.DTO
{
    public static class Status
    {
        public const string Success = "SUCCESS";
        public const string NotFound = "NO_DATA_FOUND";
    }
}