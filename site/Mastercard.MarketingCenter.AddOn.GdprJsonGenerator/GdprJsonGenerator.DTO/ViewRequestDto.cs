﻿using System;
using System.Collections.Generic;

namespace GdprJsonGenerator.DTO
{
    public class ViewRequestDTO
    {
        public string serviceFunctionCode { get; set; }
        public string requestId { get; set; }
        public string responseCode { get; set; }
        public ViewRequestDataDTO responseData { get; set; }

        public ViewRequestDTO()
        {
            responseData = new ViewRequestDataDTO();
        }
    }

    public class ViewRequestDataDTO
    {
        public IList<ViewRequestDataItemDTO> responseList { get; set; }

        public ViewRequestDataDTO()
        {
            responseList = new List<ViewRequestDataItemDTO>();
        }
    }
    public class ViewRequestDataItemDTO
    {
        private string _fullName;
        private string _firstName;
        private string _lastName;

        public string email { get; set; }
        public string fullName { get { return string.IsNullOrEmpty(_fullName) ? string.Empty : _fullName; } set { _fullName = value; } }
        public string firstName { get { return string.IsNullOrEmpty(_firstName) ? string.Empty : _firstName; } set { _firstName = value; } }
        public string lastName { get { return string.IsNullOrEmpty(_lastName) ? string.Empty : _lastName; } set { _lastName = value; } }

        public ViewRequestDataItemAddressDTO addressess { get; set; }
        public IList<ViewRequestDataItemPhoneNumberDTO> phoneNumbers { get; set; }

        public ViewRequestDataItemDTO()
        {
            addressess = new ViewRequestDataItemAddressDTO();
            phoneNumbers = new List<ViewRequestDataItemPhoneNumberDTO>();
        }
    }

    public class ViewRequestDataItemAddressDTO
    {
        private string _stateProvince;
        private string _addressLine1;
        private string _addressLine2;
        private string _addressLine3;
        private string _city;
        private string _country;
        private string _postalCode;

        public string addressLine1 { get { return string.IsNullOrEmpty(_addressLine1) ? string.Empty : _addressLine1; } set { _addressLine1 = value; } }
        public string addressLine2 { get { return string.IsNullOrEmpty(_addressLine2) ? string.Empty : _addressLine2; } set { _addressLine2 = value; } }
        public string addressLine3 { get { return string.IsNullOrEmpty(_addressLine3) ? string.Empty : _addressLine3; } set { _addressLine3 = value; } }
        public string city { get { return string.IsNullOrEmpty(_city) ? string.Empty : _city; } set { _city = value; } }
        public string country { get { return string.IsNullOrEmpty(_country) ? string.Empty : _country; } set { _country = value; } }
        public string postalCode { get { return string.IsNullOrEmpty(_postalCode) ? string.Empty : _postalCode; } set { _postalCode = value; } }
        public string state
        {
            get
            {
                if (isUS())
                    return string.IsNullOrEmpty(_stateProvince) ? string.Empty : _stateProvince;
                else
                    return string.Empty;
            }
            set
            {
                _stateProvince = value;
            }
        }
        public string province
        {
            get
            {
                if (isUS())
                    return string.Empty;
                else
                    return string.IsNullOrEmpty(_stateProvince) ? string.Empty : _stateProvince;
            }
            set
            {
                _stateProvince = value;
            }
        }

        private bool isUS()
        {
            return country.ToLower() == "us";
        }

    }

    public class ViewRequestDataItemPhoneNumberDTO
    {
        public string phoneNumber { get; set; }
    }
}
