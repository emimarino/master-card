﻿using System;
using System.Collections.Generic;

namespace GdprJsonGenerator.DTO
{
    public class EnterpriseViewOfConsentDTO
    {
        public string serviceFunctionCode { get; set; }
        public string requestId { get; set; }
        public string responseCode { get; set; }
        public EnterpriseViewOfConsentDataDTO responseData { get; set; }

        public EnterpriseViewOfConsentDTO()
        {
            responseData = new EnterpriseViewOfConsentDataDTO();
        }
    }

    public class EnterpriseViewOfConsentDataDTO
    {
        public IList<EnterpriseViewOfConsentDataItemDTO> responseList { get; set; }

        public EnterpriseViewOfConsentDataDTO()
        {
            responseList = new List<EnterpriseViewOfConsentDataItemDTO>();
        }
    }
    public class EnterpriseViewOfConsentDataItemDTO
    {
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public IList<EnterpriseViewOfConsentDataItemConsent> consents { get; set; }

        public EnterpriseViewOfConsentDataItemDTO()
        {
            consents = new List<EnterpriseViewOfConsentDataItemConsent>();
        }
    }

    public class EnterpriseViewOfConsentDataItemConsent
    {
        private DateTime _consentDate { get; set; }
        private DateTime _publishedDate { get; set; }

        public string serviceFunctionCode { get; set; }
        public string useCategoryCode { get; set; }
        public string documentType { get; set; }
        public string consentText { get; set; }
        public string currentVersion { get; set; }
        public string status { get; set; }
        public string locale { get; set; }
        public string reacceptedVersion { get; set; }
        public string notificationVersion { get; set; }
        public string reacceptanceRequired { get; set; }
        public string emailNotificationRequired { get; set; }
        public string consentDate
        {
            get
            {
                return _consentDate.ToString("s");
            }
            set
            {
                _consentDate = DateTime.Parse(value);
            }
        }
        public string publishedDate
        {
            get
            {
                return _consentDate.ToString("s");
            }
            set
            {
                _consentDate = DateTime.Parse(value);
            }
        }
    }
}
