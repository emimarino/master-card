﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GdprJsonGenerator.DTO;
using ServiceStack.Data;
using ServiceStack.OrmLite;

namespace GdprJsonGenerator.Repositories
{
    public class ViewRequestRepository : IViewRequestRepository
    {
        IDbConnectionFactory _dbFactory;
        public ViewRequestRepository(IDbConnectionFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }
        public ViewRequestDataItemDTO GetViewRequest(string email)
        {
            ViewRequestDataItemDTO result;
            using (var db = _dbFactory.OpenDbConnection())
            {
                result = db.Single<ViewRequestDataItemDTO>($@"
                    select 
	                    u.Email as 'email',
	                    dbo.fn_GetProfileElement('FirstName',p.PropertyNames,p.PropertyValuesString) as 'firstName',
	                    dbo.fn_GetProfileElement('LastName',p.PropertyNames,p.PropertyValuesString) as 'lastName',
						u.Name as 'fullName'
                    from [User] u
	                    inner join aspnet_Users au on u.UserName = 'mastercardmembers:' + au.UserName
	                    inner join aspnet_Profile p on au.UserId = p.UserId
                    where u.email = '{email}'
                ");

                if (result != null)
                {
                    result.addressess = db.Single<ViewRequestDataItemAddressDTO>($@"
                    select 
	                    dbo.fn_GetProfileElement('Address1',p.PropertyNames,p.PropertyValuesString) as 'addressLine1',
	                    dbo.fn_GetProfileElement('Address2',p.PropertyNames,p.PropertyValuesString) as 'addressLine2',
	                    dbo.fn_GetProfileElement('Address3',p.PropertyNames,p.PropertyValuesString) as 'addressLine3',
	                    dbo.fn_GetProfileElement('City',p.PropertyNames,p.PropertyValuesString) as 'city',
	                    dbo.fn_GetProfileElement('Country',p.PropertyNames,p.PropertyValuesString) as 'country',
	                    dbo.fn_GetProfileElement('Zip',p.PropertyNames,p.PropertyValuesString) as 'postalCode',
	                    dbo.fn_GetProfileElement('State',p.PropertyNames,p.PropertyValuesString) as 'state'
                    from [User] u
	                    inner join aspnet_Users au on u.UserName = 'mastercardmembers:' + au.UserName
	                    inner join aspnet_Profile p on au.UserId = p.UserId
                    where u.email = '{email}'
                ");

                    result.phoneNumbers.Add(db.Single<ViewRequestDataItemPhoneNumberDTO>($@"
                    select 
	                    dbo.fn_GetProfileElement('Phone',p.PropertyNames,p.PropertyValuesString) as 'phoneNumber'
                    from [User] u
	                    inner join aspnet_Users au on u.UserName = 'mastercardmembers:' + au.UserName
	                    inner join aspnet_Profile p on au.UserId = p.UserId
                    where u.email = '{email}'
                "));
                }
            }

            if (result == null)
                result = new ViewRequestDataItemDTO { email = email };
            return result;
        }
    }
}
