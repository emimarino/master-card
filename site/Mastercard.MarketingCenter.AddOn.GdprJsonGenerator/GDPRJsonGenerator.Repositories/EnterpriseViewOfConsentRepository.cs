﻿using GdprJsonGenerator.DTO;
using ServiceStack.Data;
using ServiceStack.OrmLite;

namespace GdprJsonGenerator.Repositories
{
    public class EnterpriseViewOfConsentRepository : IEnterpriseViewOfConsentRepository
    {
        IDbConnectionFactory _dbFactory;
        public EnterpriseViewOfConsentRepository(IDbConnectionFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }
        public EnterpriseViewOfConsentDataItemDTO GetEnterpiseViewOfConsent(string email)
        {
            EnterpriseViewOfConsentDataItemDTO result;
            using (var db = _dbFactory.OpenDbConnection())
            {
                result = db.Single<EnterpriseViewOfConsentDataItemDTO>($@"
                    select 
	                    u.Email,
	                    dbo.fn_GetProfileElement('FirstName',p.PropertyNames,p.PropertyValuesString) as 'FirstName',
	                    dbo.fn_GetProfileElement('LastName',p.PropertyNames,p.PropertyValuesString) as 'LastName'
                    from [User] u
	                    inner join aspnet_Users au on u.UserName = 'mastercardmembers:' + au.UserName
	                    inner join aspnet_Profile p on au.UserId = p.UserId
                    where u.email = '{email}'
                ");
                if (result == null)
                    return null;
                result.consents = db.Select<EnterpriseViewOfConsentDataItemConsent>($@"
                    select 
	                    cfd.ServiceFunctionCode,
	                    cfd.UseCategoryCode,
	                    cfd.DocumentType,
	                    cfd.ConsentData as ConsentText,
	                    cfd.CurrentVersion,
	                    cfd.Status,
	                    cfd.Locale,
	                    cfd.ReacceptedVersion,
	                    cfd.NotificationVersion,
	                    cfd.ReacceptanceRequired,
	                    cfd.EmailNotificationRequired,
	                    cfd.PublishedDate,
	                    uc.ConsentDate
                    from [User] u
	                    inner join aspnet_Users au on u.UserName = 'mastercardmembers:' + au.UserName
	                    inner join aspnet_Profile p on au.UserId = p.UserId
	                    left join UserConsent uc on uc.UserId = u.UserID
	                    inner join ConsentFileData cfd on uc.ConsentFileDataId = cfd.ConsentFileDataId
                    where u.email = '{email}'
                ");
            }
            return result;
        }
    }
}
