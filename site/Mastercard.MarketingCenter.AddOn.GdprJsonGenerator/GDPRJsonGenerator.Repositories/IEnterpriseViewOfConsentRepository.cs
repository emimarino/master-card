﻿using GdprJsonGenerator.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GdprJsonGenerator.Repositories
{
    public interface IEnterpriseViewOfConsentRepository
    {
        EnterpriseViewOfConsentDataItemDTO GetEnterpiseViewOfConsent(string email);
    }
}
