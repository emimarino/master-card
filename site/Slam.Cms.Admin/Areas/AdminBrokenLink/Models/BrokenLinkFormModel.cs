﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Slam.Cms.Admin.Areas.AdminBrokenLink.Models
{
    public class BrokenLinkFormModel
    {
        public int BrokenLinkId { get; set; }
        public string Url { get; set; }
        public List<string> Referrers { get; set; }
		public List<SelectListItem> Applications { get; set; }
        public string CustomErrorMessage { get; set; }
		public string RedirectApplication { get; set; }
        public string RedirectUrl { get; set; }
		public int Action { get; set; }		
    }
}