﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Slam.Cms.Admin.Data;
using Telerik.Web.Mvc;
using Slam.Cms.Admin.Areas.AdminBrokenLink.Models;
using Slam.Cms.Configuration;
using Slam.Cms.Admin.Core;

namespace Slam.Cms.Admin.Areas.AdminBrokenLink.Controllers
{
	//[AuthorizeAttribute]
	public class BrokenLinkController : Controller
	{
		private readonly BrokenLinkRepository brokenLinkRepository;

		public BrokenLinkController(BrokenLinkRepository brokenLinkRepository)
		{
			this.brokenLinkRepository = brokenLinkRepository;
		}

		public ActionResult Index()
		{
			return View();
		}

		[GridAction]
		public ActionResult _Index()
		{
			return View(new GridModel<BrokenLinkView> { Data = brokenLinkRepository.GetAllUnmanagedBrokenLinks().OrderByDescending(v => v.Count) });
		}

		public ActionResult Managed()
		{
			return View();
		}

		[GridAction]
		public ActionResult _Managed()
		{
			return View(new GridModel<BrokenLinkView> { Data = brokenLinkRepository.GetAllManagedBrokenLinks().OrderByDescending(l => l.ModifiedDate) });
		}

		public ActionResult Manage(int id)
		{
			var brokenLink = brokenLinkRepository.GetById(id);
			var brokenLinkFormModel = new BrokenLinkFormModel();
			brokenLinkFormModel.BrokenLinkId = brokenLink.BrokenLinkId;
			brokenLinkFormModel.Url = brokenLink.Url;
			brokenLinkFormModel.Referrers = string.IsNullOrEmpty(brokenLink.Referrer) ? new List<string>() : brokenLink.Referrer.Split(',').Take(25).ToList();
			brokenLinkFormModel.RedirectApplication = brokenLink.RedirectApplication ?? Constants.SelectApplicationValue;

			var applications = new List<SelectListItem>();
			applications.Add(new SelectListItem() { Text = "Select", Value = Constants.SelectApplicationValue });
			applications = applications.Concat(ConfigurationManager.Solution.Applications.ToList().ConvertAll<SelectListItem>(a => new SelectListItem() { Text = a.Value.Name, Value = a.Value.Name })).ToList();
			applications.Add(new SelectListItem() { Text = "Fully Qualified URL", Value = Constants.FullyQualifiedUrlValue });
			brokenLinkFormModel.Applications = applications;

			if (!string.IsNullOrEmpty(brokenLink.Message) && brokenLink.Message != Constants.IgnoreOneTimeMessageValue)
			{
				brokenLinkFormModel.Action = 2;
				brokenLinkFormModel.CustomErrorMessage = brokenLink.Message;
			}
			else if (!string.IsNullOrEmpty(brokenLink.RedirectUrl))
			{
				brokenLinkFormModel.Action = 3;
				brokenLinkFormModel.RedirectUrl = brokenLink.RedirectUrl;
			}
			else
				brokenLinkFormModel.Action = (brokenLink.Message == Constants.IgnoreOneTimeMessageValue || !brokenLink.IsHandled) ? 0 : 1;

			return View(brokenLinkFormModel);
		}

		[HttpPost, ValidateInput(false)]
		public ActionResult Manage(BrokenLinkFormModel brokenLinkFormModel)
		{
			var brokenLink = brokenLinkRepository.GetById(brokenLinkFormModel.BrokenLinkId);

			bool isValid = true;

			if (brokenLinkFormModel.Action == 2 && string.IsNullOrWhiteSpace(brokenLinkFormModel.CustomErrorMessage))
			{
				isValid = false;
				ModelState.AddModelError("CustomErrorMessage", "Required");
			}
			else if (brokenLinkFormModel.Action == 3)
			{
				if (string.IsNullOrWhiteSpace(brokenLinkFormModel.RedirectUrl))
				{
					isValid = false;
					ModelState.AddModelError("RedirectUrl", "Required");
				}

				if (brokenLinkFormModel.RedirectApplication == Constants.SelectApplicationValue)
				{
					isValid = false;
					ModelState.AddModelError("RedirectApplication", "Required");
				}
			}

			if (isValid)
			{
				if (brokenLinkFormModel.Action == 2)
				{
					brokenLink.Message = brokenLinkFormModel.CustomErrorMessage.Trim();
					brokenLink.RedirectUrl = null;
					brokenLink.RedirectApplication = null;
				}
				else if (brokenLinkFormModel.Action == 3)
				{
					brokenLink.Message = null;
					brokenLink.RedirectApplication = brokenLinkFormModel.RedirectApplication == Constants.FullyQualifiedUrlValue ? null : brokenLinkFormModel.RedirectApplication;
					brokenLink.RedirectUrl = brokenLinkFormModel.RedirectUrl.Trim();
				}
				else if (brokenLinkFormModel.Action == 0)
				{
					brokenLink.Message = Constants.IgnoreOneTimeMessageValue;
					brokenLink.RedirectUrl = null;
					brokenLink.RedirectApplication = null;
				}
				else
				{
					brokenLink.Message = null;
					brokenLink.RedirectUrl = null;
					brokenLink.RedirectApplication = null;
				}

				brokenLink.ModifiedDate = DateTime.Now;
				brokenLink.IsHandled = true;
				brokenLinkRepository.UpdateBrokenLink(brokenLink);

				return RedirectToAction("Index");
			}
			else
			{
				brokenLinkFormModel.Url = brokenLink.Url;
				brokenLinkFormModel.Referrers = string.IsNullOrEmpty(brokenLink.Referrer) ? new List<string>() : brokenLink.Referrer.Split(',').Take(25).ToList();
				brokenLinkFormModel.RedirectApplication = brokenLink.RedirectApplication ?? Constants.SelectApplicationValue;

				var applications = new List<SelectListItem>();
				applications.Add(new SelectListItem() { Text = "Select", Value = Constants.SelectApplicationValue });
				applications = applications.Concat(ConfigurationManager.Solution.Applications.ToList().ConvertAll<SelectListItem>(a => new SelectListItem() { Text = a.Value.Name, Value = a.Value.Name })).ToList();
				applications.Add(new SelectListItem() { Text = "Fully Qualified URL", Value = Constants.FullyQualifiedUrlValue });
				brokenLinkFormModel.Applications = applications;

				return View(brokenLinkFormModel);
			}
		}

		[HttpPost]
		public ActionResult ClearAllUnmanagedBrokenLinks()
		{
			brokenLinkRepository.ClearAllUnmanagedBrokenLinks();

			return Json(true);
		}
	}
}
