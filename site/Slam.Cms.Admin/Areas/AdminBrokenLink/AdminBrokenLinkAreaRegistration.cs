﻿using System.Web.Mvc;

namespace Slam.Cms.Admin.Areas.AdminBrokenLink
{
	public class AdminBrokenLinkAreaRegistration : AreaRegistration
	{
		public override string AreaName
		{
			get
			{
				return "AdminBrokenLink";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapRoute("Admin_BrokenLinks",	"admin/broken-links",		new { controller = "BrokenLink", action = "Index" });
			context.MapRoute("Admin_ManagedLinks",	"admin/managed-links",		new { controller = "BrokenLink", action = "Managed" });
			context.MapRoute("Admin_ManageLink",	"admin/manage-link/{id}",	new { controller = "BrokenLink", action = "Manage" });
			
			context.MapRoute("Admin_BrokenLinkDefault", "AdminBrokenLink/{controller}/{action}");
		}
	}
}
