﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Slam.Cms.Admin.Core
{
	public static class Constants
	{
		public const string SelectApplicationValue = "[Select]";
		public const string FullyQualifiedUrlValue = "[Fully Qualified URL]";
		public const string IgnoreOneTimeMessageValue = "[IgnoreOneTime]";
	}
}
