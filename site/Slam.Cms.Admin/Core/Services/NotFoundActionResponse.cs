﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Slam.Cms.Admin.Core.Services
{
	public class NotFoundActionResponse
	{
		public bool ShowCustomErrorMessage { get; set; }
		public bool Redirect { get; set; }
		public string CustomErrorMessage { get; set; }
		public string RedirectUrl { get; set; }
	}
}