﻿using Slam.Cms.Admin.Data;
using Slam.Cms.Configuration;
using System.IO;
using System.Web;

namespace Slam.Cms.Admin.Core.Services
{
    public static class BrokenLinkService
    {
        public static NotFoundActionResponse GetNotFoundAction(HttpRequestBase request)
        {
            var notFoundActionResponse = new NotFoundActionResponse();
            var url = request.RawUrl.TrimEnd('/');
            var referrer = request.UrlReferrer != null ? request.UrlReferrer.AbsoluteUri : null;
            var application = GetCurrentApplication(request);

            if (url == "/filenotfound" || application == null)
            {
                return notFoundActionResponse;
            }

            var brokenLinkRepository = new BrokenLinkRepository(new SlamCmsAdminDbContext());

            if (brokenLinkRepository.IsBrokenLinkManaged(url, application.Name))
            {
                var brokenLink = brokenLinkRepository.GetByUrlAndApplication(url, application.Name);

                if (!string.IsNullOrEmpty(brokenLink.RedirectUrl))
                {
                    notFoundActionResponse.Redirect = true;

                    if (!brokenLink.RedirectUrl.StartsWith("http:") && brokenLink.RedirectApplication != null)
                        notFoundActionResponse.RedirectUrl = Path.Combine(application.Url, brokenLink.RedirectUrl.TrimStart('/'));
                    else
                        notFoundActionResponse.RedirectUrl = brokenLink.RedirectUrl;
                }
                else if (!string.IsNullOrEmpty(brokenLink.Message) && brokenLink.Message != Constants.IgnoreOneTimeMessageValue)
                {
                    notFoundActionResponse.ShowCustomErrorMessage = true;
                    notFoundActionResponse.CustomErrorMessage = brokenLink.Message;
                }
            }
            else
            {
                brokenLinkRepository.TrackBrokenLink(url, application.Name, referrer, HttpContext.Current.User.Identity.Name);
            }

            return notFoundActionResponse;
        }

        private static ConfigurationApplication GetCurrentApplication(HttpRequestBase request)
        {
            foreach (var application in ConfigurationManager.Solution.Applications)
            {
                if (request.Url.AbsoluteUri.ToLower().Contains(application.Value.Url.ToLower()))
                {
                    return application.Value;
                }
            }

            return null;
        }
    }
}