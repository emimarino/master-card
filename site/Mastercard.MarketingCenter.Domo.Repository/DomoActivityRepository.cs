﻿using Mastercard.MarketingCenter.Domo.Domain;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Domo.Repository
{
    internal class DomoActivityRepository : IDomoActivityRepository
    {
        private readonly IDbConnectionFactory _dbFactory;
        public DomoActivityRepository(IDbConnectionFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }

        public IList<ViewDomoActivityApiByContext> GetAllActivity()
        {
            IList<ViewDomoActivityApiByContext> result;
            using (var db = _dbFactory.Open())
            {
                var q = db.From<ViewDomoActivityApiByContext>();
                result = db.Select(q);
            }

            return result;
        }

        public IList<ViewDomoActivityApiByContext> GetActivity(DateTime from, DateTime to, string contentRegion = null, string userRegion = null, string userMarket = null, string userEmail = null, bool includeExcludedResults = false)
        {
            to = to.Date.AddDays(1);
            IList<ViewDomoActivityApiByContext> result;
            using (var db = _dbFactory.Open())
            {
                var q = db.From<ViewDomoActivityApiByContext>("vDomoActivityApiByContext")
                    .Where(i => i.When >= from)
                    .And(i => i.When < to);

                if (!string.IsNullOrWhiteSpace(contentRegion))
                {
                    q = q.And(i => i.ContentRegionName.Trim().ToLower() == contentRegion.Trim().ToLower());
                }

                if (!string.IsNullOrWhiteSpace(userRegion))
                {
                    q = q.And(i => i.UserRegion.Trim().ToLower() == userRegion.Trim().ToLower());
                }

                if (!string.IsNullOrWhiteSpace(userMarket))
                {
                    q = q.And(i => i.UserCountry.Trim().ToLower() == userMarket.Trim().ToLower());
                }

                if (!string.IsNullOrWhiteSpace(userEmail))
                {
                    q = q.And(i => i.UserEmail.Trim().ToLower() == userEmail.Trim().ToLower());
                }
                if (!includeExcludedResults)
                {
                    q.And(i => !i.HiddenResult);
                }

                db.ExecuteSql($"EXEC UpdateDomoActivityApiContext @StartDate = '{from}'");

                result = db.Select(q);
            }

            return result;
        }

        public void UpdateActivityUserData(int userId, string userEmail, string userFirstName, string userLastName)
        {
            using (var db = _dbFactory.Open())
            {
                // hardcoded timeout here because it doesn't make sense to make a config more complex to store this value
                db.SetCommandTimeout(180);

                db.UpdateOnly(() => new Domain.Archive.DomoActivityApi
                {
                    UserEmail = userEmail,
                    UserFirstName = userFirstName,
                    UserLastName = userLastName
                },
                a => a.UserId == userId);

                db.UpdateOnly(() => new DomoActivityApi
                {
                    UserEmail = userEmail,
                    UserFirstName = userFirstName,
                    UserLastName = userLastName
                },
                a => a.UserId == userId);
            }
        }

        public void ExpireActivityOrphanUserData(DateTime expirationDate)
        {
            using (var db = _dbFactory.Open())
            {
                // hardcoded timeout here because it doesn't make sense to make a config more complex to store this value
                db.SetCommandTimeout(180);

                db.UpdateOnly(() => new Domain.Archive.DomoActivityApi
                {
                    UserEmail = Mask(),
                    UserFirstName = Mask(),
                    UserLastName = Mask()
                },
                a => a.UserId == 0 && a.When < expirationDate && a.UserEmail.Contains("@"));

                db.UpdateOnly(() => new DomoActivityApi
                {
                    UserEmail = Mask(),
                    UserFirstName = Mask(),
                    UserLastName = Mask()
                },
                a => a.UserId == 0 && a.When < expirationDate && a.UserEmail.Contains("@"));
            }
        }

        private string Mask()
        {
            return Guid.NewGuid().ToString();
        }
    }
}