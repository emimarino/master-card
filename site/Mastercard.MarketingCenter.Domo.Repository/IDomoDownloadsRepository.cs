﻿using Mastercard.MarketingCenter.Domo.Domain;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Domo.Repository
{
    public interface IDomoDownloadsRepository
    {
        /// <summary>
        /// Creates and populates the table that will hold the SiteTrackingDownloads
        /// </summary>
        void CreateAuxTable();

        /// <summary>
        /// Decodes and updates the file names that are URL decoded.
        /// </summary>
        void DecodeFileNames();

        /// <summary>
        /// Processes Aggregated data to be used by API. 
        /// </summary>
        /// <returns>Returns the number of inserted Registers</returns>
        int UpdateData();

        IList<DomoDownloadsApi> GetDownloads(DateTime from, DateTime to, string contentRegion = null, string ContentTypeId = null, string userRegion = null, string userFinancialInstitution = null, string userEmail = null, bool includeExcludedResults = false);
        void UpdateDownloadUserData(int userId, string userEmail, string userFirstName, string userLastName);
        void ExpireDownloadOrphanUserData(DateTime expirationDate);
    }
}