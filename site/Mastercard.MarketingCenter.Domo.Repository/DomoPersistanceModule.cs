﻿using Autofac;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System.Configuration;

namespace Mastercard.MarketingCenter.Domo.Repository
{
    public class DomoPersistanceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["MasterCardMarketingCenter"].ConnectionString;
            var dbConnectionFactory = new OrmLiteConnectionFactory(connectionString, SqlServerDialect.Provider);

            builder.Register(c => dbConnectionFactory).As<IDbConnectionFactory>();
            builder.RegisterType<DomoDownloadsRepository>().As<IDomoDownloadsRepository>();
            builder.RegisterType<DomoActivityRepository>().As<IDomoActivityRepository>();
        }
    }
}