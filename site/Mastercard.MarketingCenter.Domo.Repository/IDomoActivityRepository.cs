﻿using Mastercard.MarketingCenter.Domo.Domain;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Domo.Repository
{
    public interface IDomoActivityRepository
    {
        IList<ViewDomoActivityApiByContext> GetAllActivity();
        IList<ViewDomoActivityApiByContext> GetActivity(DateTime from, DateTime to, string contentRegion = null, string userRegion = null, string userMarket = null, string userEmail = null, bool includeExcludedResults = false);
        void UpdateActivityUserData(int userId, string userEmail, string userFirstName, string userLastName);
        void ExpireActivityOrphanUserData(DateTime expirationDate);
    }
}