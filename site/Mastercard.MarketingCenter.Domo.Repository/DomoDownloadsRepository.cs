﻿using Mastercard.MarketingCenter.Domo.Domain;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Mastercard.MarketingCenter.Domo.Repository
{
    internal class DomoDownloadsRepository : IDomoDownloadsRepository
    {
        private readonly IDbConnectionFactory _dbFactory;
        public DomoDownloadsRepository(IDbConnectionFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }

        public void CreateAuxTable()
        {
            using (var db = _dbFactory.Open())
            {
                //Timeout incremented because could be a long running stored procedure
                db.SetCommandTimeout(60 * 20);
                db.ExecuteSql("EXEC UpdateDownloadsAuxTable");
            }
        }

        public void DecodeFileNames()
        {
            using (var db = _dbFactory.Open())
            {
                db.SetCommandTimeout(60 * 20);
                using (var dbTrans = db.OpenTransaction())
                {
                    var decodedDataecode = db.Select<DataToBeDecodedAux>();
                    decodedDataecode.ForEach(a => a.What = WebUtility.UrlDecode(a.What));

                    db.UpdateAll(decodedDataecode);
                    dbTrans.Commit();
                }
            }
        }

        public int UpdateData()
        {
            var updatedItems = 0;

            using (var db = _dbFactory.Open())
            {
                //Timeout incremented because could be a long running stored procedure
                db.SetCommandTimeout(60 * 20);
                updatedItems = db.ExecuteSql("EXEC ProcessDomoDownloadsApi");
            }

            return updatedItems;
        }

        public IList<DomoDownloadsApi> GetDownloads(DateTime from, DateTime to, string contentRegion = null, string ContentTypeId = null, string userRegion = null, string userFinancialInstitution = null, string userEmail = null, bool includeExcludedResults = false)
        {
            to = to.Date.AddDays(1);
            IList<DomoDownloadsApi> result;
            using (var db = _dbFactory.Open())
            {
                var q = db.From<DomoDownloadsApi>()
                    .Where(i => i.DownloadDate >= from)
                    .And(i => i.DownloadDate < to);

                if (!string.IsNullOrWhiteSpace(contentRegion))
                {
                    q = q.And(i => i.ContentItemRegion.Trim().ToLower() == contentRegion.Trim().ToLower());
                }
                if (!string.IsNullOrWhiteSpace(ContentTypeId))
                {
                    q = q.And(i => i.ContentTypeId.Trim().ToLower() == ContentTypeId.Trim().ToLower());
                }
                if (!string.IsNullOrWhiteSpace(userRegion))
                {
                    q = q.And(i => i.UserRegion.Trim().ToLower() == userRegion.Trim().ToLower());
                }
                if (!string.IsNullOrWhiteSpace(userFinancialInstitution))
                {
                    q = q.And(i => i.UserFinancialInstitutionCID.Trim().ToLower() == userFinancialInstitution.Trim().ToLower());
                }
                if (!string.IsNullOrWhiteSpace(userEmail))
                {
                    q = q.And(i => i.UserEmail.Trim().ToLower() == userEmail.Trim().ToLower());
                }
                if (!includeExcludedResults)
                {
                    q.And(i => !i.HiddenResult);
                }

                result = db.Select(q);
            }

            return result;
        }

        public void UpdateDownloadUserData(int userId, string userEmail, string userFirstName, string userLastName)
        {
            using (var db = _dbFactory.Open())
            {
                // hardcoded timeout here because it doesn't make sense to make a config more complex to store this value
                db.SetCommandTimeout(180);

                db.UpdateOnly(() => new DomoDownloadsApi
                {
                    UserEmail = userEmail,
                    UserFirstName = userFirstName,
                    UserLastName = userLastName
                },
                a => a.UserId == userId);
            }
        }

        public void ExpireDownloadOrphanUserData(DateTime expirationDate)
        {
            using (var db = _dbFactory.Open())
            {
                // hardcoded timeout here because it doesn't make sense to make a config more complex to store this value
                db.SetCommandTimeout(180);

                db.UpdateOnly(() => new DomoDownloadsApi
                {
                    UserEmail = Mask(),
                    UserFirstName = Mask(),
                    UserLastName = Mask()
                },
                a => a.UserId == 0 && a.DownloadDate < expirationDate && a.UserEmail.Contains("@"));
            }
        }

        private string Mask()
        {
            return Guid.NewGuid().ToString();
        }
    }
}