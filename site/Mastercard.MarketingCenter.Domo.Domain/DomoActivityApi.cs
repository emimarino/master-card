﻿using ServiceStack.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Domo.Domain
{
    [Schema("dbo")]
    [Table("DomoActivityApi")]
    public class DomoActivityApi
    {
        [PrimaryKey]
        public int Id { get; set; }
        public int SiteTrackingId { get; set; }
        public int VisitId { get; set; }
        [Alias("User_UserId")]
        [Column("User_UserId")]
        public int UserId { get; set; }
        [Alias("User_LastName")]
        [Column("User_LastName")]
        public string UserLastName { get; set; }
        [Alias("User_FirstName")]
        [Column("User_FirstName")]
        public string UserFirstName { get; set; }
        [Alias("User_Email")]
        [Column("User_Email")]
        public string UserEmail { get; set; }
        [Alias("User_Country")]
        [Column("User_Country")]
        public string UserCountry { get; set; }
        [Alias("User_IssuerName")]
        [Column("User_IssuerName")]
        public string UserIssuerName { get; set; }
        [Alias("User_IssuerImportedName")]
        [Column("User_IssuerImportedName")]
        public string UserIssuerImportedName { get; set; }
        [Alias("User_IssuerCID")]
        [Column("User_IssuerCID")]
        public string UserIssuerCID { get; set; }
        [Alias("User_ProcessorName")]
        [Column("User_ProcessorName")]
        public string UserProcessorName { get; set; }
        [Alias("User_Region")]
        [Column("User_Region")]
        public string UserRegion { get; set; }
        public DateTime When { get; set; }
        public string From { get; set; }
        public string How { get; set; }
        [Alias("Content_Type")]
        [Column("Content_Type")]
        public string ContentType { get; set; }
        [Alias("Content_Id")]
        [Column("Content_Id")]
        public string ContentId { get; set; }
        [Alias("Content_Title")]
        [Column("Content_Title")]
        public string ContentTitle { get; set; }
        [Alias("Content_RegionName")]
        [Column("Content_RegionName")]
        public string ContentRegionName { get; set; }
        [Alias("Hidden_Result")]
        [Column("Hidden_Result")]
        public bool HiddenResult { get; set; }
    }
}