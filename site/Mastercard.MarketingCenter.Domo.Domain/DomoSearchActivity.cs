﻿using System;

namespace Mastercard.MarketingCenter.Domo.Domain
{
    public class DomoSearchActivity
    {
        public int SearchActivityID { get; set; }
        public int UserId { get; set; }
        public string UserFullName { get; set; }
        public string UserEmail { get; set; }
        public string UserTitle { get; set; }
        public string UserRegion { get; set; }
        public string UserCountry { get; set; }
        public DateTime SearchDateTime { get; set; }
        public string SearchTerm { get; set; }
        public string UrlVisited { get; set; }
        public string RegionalSite { get; set; }
        public string UserFinancialInstitution { get; set; }
        public bool HiddenResult { get; set; }
    }
}