﻿using System;

namespace Mastercard.MarketingCenter.Domo.Domain
{
    public class DataToBeDecodedAux
    {
        public int Id { get; set; }
        public string What { get; set; }
        public string ContentItemID { get; set; }
        public string From { get; set; }
        public string How { get; set; }
        public string RegionId { get; set; }
        public int SiteTrackingId { get; set; }
        public int VisitID { get; set; }
        public DateTime When { get; set; }
        public string Who { get; set; }
    }
}