﻿using ServiceStack.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Domo.Domain
{
    public class DomoDownloadsApi
    {
        [PrimaryKey]
        public int Id { get; set; }
        public string ContentItemId { get; set; }
        public string ContentItemTitle { get; set; }
        public string ContentItemRegion { get; set; }
        public string ContentTypeId { get; set; }
        public string ContentTypeTitle { get; set; }
        public string UserRegion { get; set; }
        public DateTime DownloadDate { get; set; }
        public string FileName { get; set; }
        public int SiteTrackingId { get; set; }
        public int VisitId { get; set; }
        [Alias("Country")]
        [Column("Country")]
        public string UserCountry { get; set; }
        public string UserEmail { get; set; }
        public string UserFinancialInstitution { get; set; }
        public string UserFinancialInstitutionImportedName { get; set; }
        public string UserFinancialInstitutionID { get; set; }
        public string UserFinancialInstitutionCID { get; set; }
        public int UserId { get; set; }
        public bool HiddenResult { get; set; }
        public bool? BelongsToED { set; get; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
    }
}