select 
       u.Name
       ,u.Email
       ,am.CreateDate as CreatedDate
       ,i.IssuerId
       ,I.Title as [IssuerName]
	   ,dbo.fn_GetProfileElement('Address1',ap.PropertyNames,ap.PropertyValuesString) as Address1
	   ,dbo.fn_GetProfileElement('Address2',ap.PropertyNames,ap.PropertyValuesString) as Address2
	   ,dbo.fn_GetProfileElement('Address3',ap.PropertyNames,ap.PropertyValuesString) as Address3
       ,dbo.fn_GetProfileElement('City',ap.PropertyNames,ap.PropertyValuesString) as City
		,iif(try_parse(dbo.fn_GetProfileElement('State',ap.PropertyNames,ap.PropertyValuesString) as int) is not null,s.StateName, dbo.fn_GetProfileElement('State',ap.PropertyNames,ap.PropertyValuesString)) as State
       ,dbo.fn_GetProfileElement('zip',ap.PropertyNames,ap.PropertyValuesString) as ZipCode
from [User] u
       INNER JOIN aspnet_Users au ON u.UserName = 'mastercardmembers:' + au.UserName
       INNER JOIN aspnet_membership am on au.userid = am.userid
       INNER JOIN Issuer i on u.issuerId = i.IssuerId
       INNER JOIN aspnet_Profile ap on ap.userid = au.userid
       LEFT JOIN state s ON cast(s.StateId as varchar(100)) = dbo.fn_GetProfileElement('State',ap.PropertyNames,ap.PropertyValuesString)
where 
       am.CreateDate >= '2018-03-12'
       AND u.Region = 'us'
       AND i.IssuerId = 'ff8'
