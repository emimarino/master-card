﻿This project is a helper for updating User Profile information handled by ASP Membership. The initial problem is that
the information is stored y Fields / Values format in ASPNET_Profile table.

The application expects a parameter with a path to the json file. An example json file can be found in SampleJson folder.

Also, an excel file can be found in Util folder. That file is a helper to build the json file