﻿///this function is use to restrict user to type only numeric value
const enabledButtonClassName = "submit_bg_btn";
const enabledButtonParentNodeClassName = "bg_btn_left mr11 fr";
const disabledButtonClassName = "grey_bg_btn";
const disabledButtonParentNodeClassName = "bg_grey_left mr11 fr";
function getTarget(e) {
    if (window.event) {
        return window.event.srcElement;
    }
    return e ? e.target : null;
}

function ForceNumericInput(e, AllowDot, AllowMinus) {
    var target = getTarget(e);
    if (arguments.length === 1 && target) {
        var s = target.value;
        // if "-" exists then it better be the 1st character
        var i = s.lastIndexOf("-");
        if (i > 0) {
            target.value = s.substring(0, i) + s.substring(i + 1);
        }
    }

    var code;
    if (window.event) // IE
    {
        code = e.keyCode;
    }
    else if (e.which) // Netscape/Firefox/Opera
    {
        code = e.which;
    }

    switch (code) {
        case 9:
        case 8:     // backspace
        case 37:    // left arrow
        case 39:    // right arrow
        case 46:    // delete
        case 110:    // delete
            e.returnValue = true;
            return true;
        case 109:
        case 189:
            if (AllowMinus === false) {
                e.returnValue = false;
                return false;
            }
            else {
                e.returnValue = true;
                return true;
            }
    }
    if (AllowDot && code === 190) {
        if (target && target.value.indexOf(".") >= 0) {
            // don't allow more than one dot
            e.returnValue = false;
            return false;
        }
        e.returnValue = true;
        return true;
    }
    // allow character of between 0 and 9
    if ((code >= 48 && code <= 57) || (code >= 96 && code <= 106)) {
        event.returnValue = true;
        return true;
    }
    e.returnValue = false;
    return false;
}

function OpenInNewWindow(url) {
    window.open(url, 'Image');
}

function checkTextAreaMaxLength(textBox, e, length) {
    var mLen = textBox["MaxLength"];
    if (null == mLen) {
        mLen = length;
    }

    var maxLength = parseInt(mLen);
    if (!checkSpecialKeys(e)) {
        if (textBox.value.length > maxLength - 1) {
            if (window.event)//IE
            {
                e.returnValue = false;
            } else//Firefox
            {
                e.preventDefault();
            }
        }
    }
}
function checkSpecialKeys(e) {
    return e.keyCode === 8 || e.keyCode === 46 || e.keyCode === 37 || e.keyCode === 38 || e.keyCode === 39 || e.keyCode === 40;
}

function validateFiles(divPass, btnComplete) {
    var _btnComplete = document.getElementById(btnComplete);
    var items = divPass.getElementsByTagName('input');
    for (var i = 0; i < items.length; i++) {
        if (items[i].type === 'file' && items[i].value === '') {
            _btnComplete.parentNode.className = disabledButtonParentNodeClassName;
            _btnComplete.className = disabledButtonClassName;
            _btnComplete.disabled = true;

            return false;
        }
        else if (items[i].type == 'file') {
            var ext = items[i].value;
            ext = ext.substring(ext.length - 3, ext.length);
            ext = ext.toLowerCase();
            if (ext != 'pdf') {
                alert('You selected a .' + ext + ' file; please select a .pdf file instead!');
                _btnComplete.parentNode.className = disabledButtonParentNodeClassName;
                _btnComplete.className = disabledButtonClassName;
                _btnComplete.disabled = true;

                return false;
            }

        }

    }

    _btnComplete.disabled = false;
    _btnComplete.className = enabledButtonClassName;
    _btnComplete.parentNode.className = enabledButtonParentNodeClassName;

    return true;
}

function validateCost(txtTrack, txtShipping, btnComplete, txtprice, txtdesc) {
    var _txtTrack = document.getElementById(txtTrack);
    var _txtShipping = document.getElementById(txtShipping);
    var _txtprice = document.getElementById(txtprice);
    var _txtdesc = document.getElementById(txtdesc);

    if (_txtShipping.value === '') {
        alert('Please enter Shipping Cost first');
        return false;
    }
    else if (_txtTrack.value === '') {
        alert('Please enter Tracking No first');
        return false;
    }
    else if (_txtprice.value !== '' && _txtdesc.value === '') {
        alert('Please enter Description first');
        return false;
    }

    return true;
}

function validateCorrection(optfeedback, optapprove, txtFeedback, btnsubmit) {
    var _optfeedback = document.getElementById(optfeedback);
    var _optapprove = document.getElementById(optapprove);
    var _btnsubmit = document.getElementById(btnsubmit);

    if (_optapprove.checked || _optfeedback.checked) {
        _btnsubmit.disabled = false;
        _btnsubmit.className = enabledButtonClassName;
        _btnsubmit.parentNode.className = enabledButtonParentNodeClassName;
    }
    else {
        _btnsubmit.disabled = true;
        _btnsubmit.className = disabledButtonClassName;
        _btnsubmit.parentNode.className = disabledButtonParentNodeClassName;
    }

    return true;
}
