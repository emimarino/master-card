function initializeOfferList(offerListUrl) {
    var offers = new Vue({
        el: '#offer-search',
        data: {
            isLoading: false,
            bottom: false,
            items: [],
            selectedEventLocations: [],
            currentIndex: 0,
            noMoreData: false,
            offersQuantity: 0,
            filters: {
                text: '',
                length: 12,
                start: 0,
                categoryIds: [],
                cardExclusivityIds: [],
                marketApplicabilityIds: [],
                eventLocationIds: [],
                startDate: null,
                endDate: null,
                sortBy: 'CreationDate',
                sortDirection: 'Desc',
                offerQuantityOnly: true
            }
        },
        methods: {
            fetchData: function () {
                var self = this;
                if (!this.isLoading) {
                    self.isLoading = true;
                    var params = JSON.stringify(self.filters);
                    var xhr = new XMLHttpRequest();
                    xhr.open('POST', offerListUrl);
                    xhr.setRequestHeader('Content-type', 'application/json')
                    xhr.send(params);
                    xhr.onload = function () {
                        var response = JSON.parse(xhr.responseText);
                        response.Offers.forEach(function (x) {
                            self.items.push(x);
                        });
                        self.isLoading = false;
                        self.currentIndex = self.currentIndex + 1
                        self.filters.start = self.filters.length * self.currentIndex;
                        if (response.Offers.length < self.filters.length) {
                            self.noMoreData = true;
                        }
                    }
                }
            },
            getOffersQuantity: function () {
                var self = this;
                var params = JSON.stringify(self.filters);
                var xhr = new XMLHttpRequest();
                xhr.open('POST', offerListUrl);
                xhr.setRequestHeader('Content-type', 'application/json')
                xhr.send(params);
                xhr.onload = function () {
                    self.offersQuantity = JSON.parse(xhr.responseText);
                }
                this.filters.offerQuantityOnly = false;
            },
            updateFilters: function (newFilters) {
                this.filters.categoryIds = newFilters.categories || [];
                this.filters.cardExclusivityIds = newFilters.cardExclusivities || [];
                this.filters.marketApplicabilityIds = newFilters.marketApplicabilities || [];
                this.filters.startDate = newFilters.dateBegin ? newFilters.dateBegin.toISOString() : null;
                this.filters.endDate = newFilters.dateEnd ? newFilters.dateEnd.toISOString() : null;
                this.filters.eventLocationIds = this.selectedEventLocations || [];
                this.reset();

                this.getOffersQuantity();
                this.fetchData();
            },
            updateSortBy: function (id, dir) {
                this.filters.sortBy = id;
                this.filters.sortDirection = dir;
                this.reset();
                this.getOffersQuantity();
                this.fetchData();
            },
            reset: function () {
                this.filters.start = this.currentIndex = 0;
                this.noMoreData = false;
                this.items = [];
                this.filters.offerQuantityOnly = true;
            },
            refresh: function () {
            },
            doSearch: function () {
                this.reset();
                this.getOffersQuantity();
                this.fetchData();
            },
            htmlEllipsisFromLetters: function (value, letters) {
                var span = document.createElement('span');
                span.innerHTML = value;
                var text = span.innerText;
                return (text.length <= letters ? text : text.substring(0, letters) + "...");
            },
            showLocations: function (locations) {
                if (locations.length) {
                    return locations.length > 1 ? 'Multiple Locations' : locations[0].Title;
                }

                return '';
            },
            bottomVisible: function () {
                const scrollY = window.pageYOffset
                const visible = document.documentElement.clientHeight
                const pageHeight = document.documentElement.scrollHeight - document.getElementsByClassName('page-footer')[0].clientHeight
                const bottomOfPage = visible + scrollY >= pageHeight
                return bottomOfPage || pageHeight < visible
            },
            showEventLocationFilter: function () {
                if (this.$refs.locationFilter) {
                    return this.$refs.locationFilter.localOptions.length > 0;
                }
                return false;
            }
        },
        watch: {
            bottom: function (bottom) {
                if (bottom && !this.noMoreData) {
                    this.fetchData()
                }
            },
            selectedEventLocations: function () {
                this.updateFilters(this.filters);
            }
        },
        created: function () {
            var self = this;
            window.addEventListener('scroll', function () {
                self.bottom = self.bottomVisible()
            });
            this.getOffersQuantity();
            this.fetchData();
        },
        filters: {
            ellipsisFromLetters: function (value, letters) {
                return (value.length <= letters ? value : value.substring(0, letters) + "...");
            },
            stripHtml: function (value) {
                return value == "" ? new Option(value).innerHTML : new Option(value.replace(/<[^>]*>?/gm, "").replace(/&nbsp;/, " ")).innerHTML;
            }
        }
    });
}
