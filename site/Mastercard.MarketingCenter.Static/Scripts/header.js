﻿function initializeSearchBox(autoCompleteUrl, segmentationIds, specialAudience, region, lang, searchUrl) {
    new Vue({
        el: '#search-box',
        data: {
            isLoading: false,
            resultList: [],
            fullSearchLink: '',
            encodedSearch: '',
            timeDelay: false,
            filters: {
                searchTerm: '',
                currentSegmentationIds: segmentationIds,
                currentSpecialAudience: specialAudience,
                regionId: region,
                language: lang
            }
        },
        methods: {
            fetchData: function () {
                var self = this;
                if (!self.isLoading && self.filters.searchTerm) {
                    self.isLoading = true;
                    var xhr = new XMLHttpRequest();
                    xhr.open('POST', autoCompleteUrl);
                    xhr.setRequestHeader('Content-type', 'application/json')
                    xhr.send(JSON.stringify(self.filters));
                    xhr.onload = function () {
                        self.resultList = JSON.parse(xhr.responseText);
                        self.formatResultHtml();
                        self.isLoading = false;
                    }
                }
            },
            redirectToSearch: function () {
                if (this.encodedSearch !== "") {
                    document.location.href = this.fullSearchLink;
                }
            },
            formatResultHtml: function () {
                var self = this;
                self.resultList.forEach(function (result) {
                    var indices = [];
                    var i = -1;
                    while (self.filters.searchTerm && result.value && (i = result.value.toLowerCase().indexOf(self.filters.searchTerm.toLowerCase(), i + 1)) >= 0) {
                        indices.push(i);
                    }
                    indices.reverse().forEach(function (ind) {
                        if (!result.valueHtml) {
                            result.valueHtml = result.value.slice(0, ind) + "<b>" + result.value.slice(ind, ind + self.filters.searchTerm.length) + "</b>" + result.value.slice(ind + self.filters.searchTerm.length);
                        }
                        else {
                            result.valueHtml = result.valueHtml.slice(0, ind) + "<b>" + result.valueHtml.slice(ind, ind + self.filters.searchTerm.length) + "</b>" + result.valueHtml.slice(ind + self.filters.searchTerm.length);
                        }
                    });
                });
            },
            curateSearchTextAndRedirect: function (e, searchTerm) {
                e.preventDefault();
                var self = this;
                var chosenSearchTerm = searchTerm !== undefined ? searchTerm : self.encodedSearch
                var xhr = new XMLHttpRequest();
                xhr.open('POST', baseSiteUrl + '/header/curatesearchtext');
                xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                xhr.send('searchText=' + chosenSearchTerm);
                xhr.onload = function () {
                    var data = JSON.parse(xhr.responseText);
                    if (data !== undefined) {
                        self.fullSearchLink = searchUrl + data.curatedText;
                        self.redirectToSearch();
                    }
                }
                xhr.onerror = function (e) {
                    throw e;
                }
            }
        },
        watch: {
            'filters.searchTerm': function (newValue) {
                this.encodedSearch = encodeURIComponent(newValue.replace("%", "").trim());
                this.fullSearchLink = searchUrl + this.encodedSearch;
                self = this;
                if (self.timeDelay) {
                    clearInterval(self.timeDelay);
                    self.timeDelay = false;
                }
                self.timeDelay = setInterval(function () {
                    if (newValue.length > 0) {
                        self.fetchData();
                    }
                    else {
                        self.resultList = [];
                    }
                    clearInterval(self.timeDelay);
                    self.timeDelay = false;
                }, 1000)
            }
        },
    });
}
