Vue.component('mc-calendar-selector', {
    template: '#calendar-template',
    data: function () {
        return {
            today: moment().minutes(0).seconds(0).milliseconds(0),
            dateContext: moment().minutes(0).seconds(0).milliseconds(0),
            days: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
            rangeBegin: null,
            rangeEnd: null,
            showDropdownMonth: false,
            showDropdownYear: false,
        };
    },
    computed: {
        year: function () {
            var self = this;
            return self.dateContext.format('Y');
        },
        month: function () {
            var self = this;
            return self.dateContext.format('M');
        },
        monthName: function () {
            var self = this;
            return self.dateContext.format('MMMM');
        },
        daysInMonth: function () {
            var self = this;
            return self.dateContext.daysInMonth();
        },
        currentDate: function () {
            var self = this;
            return self.dateContext.get('date');
        },
        firstDayOfMonth: function () {
            var self = this;
            var firstDay = moment(self.dateContext).subtract((self.currentDate - 1), 'days');
            return firstDay.weekday();
        },
        initialDate: function () {
            var self = this;
            return self.today.get('date');
        },
        initialMonth: function () {
            var self = this;
            return self.today.format('M');
        },
        initialYear: function () {
            var self = this;
            return self.today.format('Y');
        },
    },
    watch: {
        rangeBegin: function () {
            this.$emit('update', this.getSelectedObject());
        },
        rangeEnd: function () {
            this.$emit('update', this.getSelectedObject());
        }
    },
    methods: {
        getSelectedObject: function () {
            return {
                dateBegin: this.rangeBegin,
                dateEnd: this.rangeEnd
            };
        },
        isValidDate: function (date) {
            var self = this;
            var _date = new Date(self.year, self.month - 1, date, 0, 0, 0, 0);
            var _initial = new Date(self.initialYear, self.initialMonth - 1, self.initialDate, 0, 0, 0, 0);

            return moment(_date).isSameOrAfter(_initial);
        },
        selectDate: function (date) {
            var self = this;
            var _date = new Date(self.year, self.month - 1, date, 0, 0, 0, 0);
            if (self.rangeBegin == null || self.rangeEnd != null) {
                self.rangeEnd = null;
                self.rangeBegin = moment(_date);
            } else {
                var _dateEnd = moment(_date);
                if (self.rangeBegin > _date) {
                    _dateEnd = self.rangeBegin;
                    self.rangeBegin = moment(_date);
                }
                self.rangeEnd = _dateEnd;
            }
        },
        inRange: function (date) {
            var self = this;
            if (self.rangeBegin !== null && self.rangeEnd !== null) {
                var _date = new Date(self.year, self.month - 1, date, 0, 0, 0, 0);
                return self.rangeEnd.isSameOrAfter(_date) && self.rangeBegin.isSameOrBefore(_date);
            }
            return false;
        },
        isRangeStart: function (date) {
            var self = this;
            if (self.rangeBegin != null) {
                var _date = new Date(self.year, self.month - 1, date, 0, 0, 0, 0);
                return self.rangeBegin.isSame(_date);
            }
            return false;
        },
        isRangeEnd: function (date) {
            var self = this;
            if (self.rangeEnd != null) {
                var _date = new Date(self.year, self.month - 1, date, 0, 0, 0, 0);
                return self.rangeEnd.isSame(_date);
            }
            return false;
        },
        changeYear: function (year) {
            var self = this;
            self.dateContext = moment(self.dateContext).year(year);
            self.showDropdownYear = false;
        },
        changeMonth: function (month) {
            var self = this;
            self.dateContext = moment(self.dateContext).month(month);
            self.showDropdownMonth = false;
        }
    }
});
