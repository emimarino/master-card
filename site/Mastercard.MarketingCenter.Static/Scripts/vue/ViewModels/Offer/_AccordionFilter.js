Vue.component('accordion-filter', {
    template: '#accordion-template',
    props: {
        url: {
            type: String,
            required: true
        }
    },
    data: function () {
        return {
            showSelectCategory: false,
            showSelectCardExclusivity: false,
            showSelectMarketApplicability: false,
            showFilterMarketApplicability: false,
            showDateRange: false,

            categories: [],
            cardexclusivities: [],
            marketApplicabilities: [],

            selectedCategories: [],
            selectedCardExclusivities: [],
            selectedMarketApplicabilities: [],
            selectedDateBegin: null,
            selectedDateEnd: null
        };
    },
    created: function () {
        this.fetchData();
    },
    watch: {
        selectedCategories: function () {
            this.$emit('update', this.getSelectedObject());
        },
        selectedCardExclusivities: function () {
            this.$emit('update', this.getSelectedObject());
        },
        selectedMarketApplicabilities: function () {
            this.$emit('update', this.getSelectedObject());
        }
    },
    computed: {
        areFiltersBeingUsed: function () {
            var self = this;
            return self.selectedCategories.length > 0 ||
                self.selectedCardExclusivities.length > 0 ||
                self.selectedMarketApplicabilities.length > 0 ||
                self.selectedDateBegin !== null ||
                self.selectedDateEnd !== null;
        }
    },
    methods: {
        clearFilters: function () {
            var self = this;
            self.selectedCategories = [];
            self.selectedCardExclusivities = [];
            self.selectedMarketApplicabilities = [];
            self.selectedDateBegin = null;
            self.selectedDateEnd = null;
        },
        getSelectedObject: function () {
            return {
                categories: this.selectedCategories,
                cardExclusivities: this.selectedCardExclusivities,
                marketApplicabilities: this.selectedMarketApplicabilities,
                dateBegin: this.selectedDateBegin,
                dateEnd: this.selectedDateEnd
            };
        },
        updateDates: function (newDates) {
            this.selectedDateBegin = newDates.dateBegin;
            this.selectedDateEnd = newDates.dateEnd;

            this.$emit('update', this.getSelectedObject());
        },
        fetchData: function () {
            var xhr = new XMLHttpRequest();
            var self = this;
            xhr.open('GET', this.url);
            xhr.onload = function () {
                var data = JSON.parse(xhr.responseText);
                self.categories = data.Categories;
                self.cardexclusivities = data.CardExclusivities;
                self.marketApplicabilities = data.MarketApplicabilities;
                self.showFilterMarketApplicability = data.ShowMarketApplicabilityFilter;
                self.isLoaded = true;
            };
            xhr.send();
        },
        toggleSelectCategory: function () {
            var self = this;
            self.showSelectCategory = !self.showSelectCategory;
        },
        toggleSelectCardExclusivity: function () {
            var self = this;
            self.showSelectCardExclusivity = !self.showSelectCardExclusivity;
        },
        toggleSelectMarketApplicability: function () {
            var self = this;
            self.showSelectMarketApplicability = !self.showSelectMarketApplicability;
        },
        toggleDateRange: function () {
            var self = this;
            self.showDateRange = !self.showDateRange;
        },
        isCategorySelected: function (id) {
            return this.selectedCategories.indexOf(id) !== -1;
        },
        isCardExclusivitySelected: function (id) {
            return this.selectedCardExclusivities.indexOf(id) !== -1;
        },
        isMarketApplicabilitySelected: function (id) {
            return this.selectedMarketApplicabilities.indexOf(id) !== -1;
        }
    }
});
