Vue.component('location-filter', {
    template: '#location-template',
    props: {
        url: {
            type: String,
            required: true
        },
        value: {
            type: [String, Number, Array, Object],
            default: null
        },
        options: {
            type: [Array, Object],
            default: function () { return [] }
        },
        label: {
            type: String,
            default: 'label'
        },
        trackBy: {
            type: String,
            default: 'id'
        },
        multiple: {
            type: Boolean,
            default: false
        },
        maxselectedoptions: {
            type: Number,
            default: 0
        },
        placeholder: {
            type: String,
            default: 'Select'
        },
        searchable: {
            type: Boolean,
            default: true
        },
        searchableMin: {
            type: Number,
            default: 1
        },
        allowEmpty: {
            type: Boolean,
            default: true
        },
        allowAll: {
            type: Boolean,
            default: false
        },
        disabled: {
            type: Boolean,
            default: false
        },
        onValueReturn: {
            type: Object,
            default: function () { return ({}) }
        }
    },
    created: function () {
        this.fetchData();
    },
    methods: {
        fetchData: function () {
            if (!Array.isArray(this.options) || !this.options.length) {
                var xhr = new XMLHttpRequest();
                var self = this;
                xhr.open('GET', this.url);
                xhr.onload = function () {
                    var data = JSON.parse(xhr.responseText);
                    self.options = data.EventLocations;
                    self.isLoaded = true;
                };
                xhr.send();
            }
        }
    },
    computed: {
        localOptions: function () {
            const vm = this,
                options = [];
            if (this.allowAll) {
                const defaultOption = {};
                defaultOption[this.trackBy] = -1;
                defaultOption[this.label] = 'All';
                options.push(defaultOption);
            }
            if (Array.isArray(this.options)) {
                return options.concat(this.options);
            }

            const keys = [];
            for (var i in this.options) {
                if (this.options.hasOwnProperty(i)) {
                    keys.push(i);
                }
            }
            keys.forEach(function (key) {
                const option = {};
                option[vm.trackBy] = key;
                option[vm.label] = vm.options[key];
                options.push(option);
            })

            const emptyArray = [];
            const emptyOption = {};
            emptyOption[this.trackBy] = null;
            emptyOption[this.label] = '';
            emptyArray.push(emptyOption);

            return this.allowEmpty ? emptyArray.concat(options) : options
        },
        localValue: function () {
            const value = this.allowAll && this.value === null ? -1 : this.value
            this.$nextTick(function () {
                $(this.$el).val(value).trigger("chosen:updated")
            })
            return value
        }
    },
    watch: {
        localValue: function () {
        },
        localOptions: function () {
            this.$nextTick(function () {
                const value = this.allowAll && this.value === null ? '-1' : this.value
                $(this.$el).val(value).trigger("chosen:updated")
            })
        }
    },
    mounted: function () {
        const component = this
        $(this.$el).chosen({
            width: "100%",
            max_selected_options: this.maxselectedoptions > 0 ? this.maxselectedoptions : 100000,
            disable_search_threshold: this.searchable ? this.searchableMin : 100000
        }).change(function ($event) {
            const value = $($event.target).val()
            if (component.onValueReturn && typeof component.onValueReturn[value] !== 'undefined') {
                return component.$emit('input', component.onValueReturn[value])
            }
            if (component.allowAll && ($($event.target).val() === '-1' || $($event.target).val() === -1)) {
                return component.$emit('input', null)
            }
            component.$emit('input', $($event.target).val())
        })
    }
});
