﻿$('.preview-locations').each(function () {
    new Vue({
        el: this,
        components: {
            'preview-popup': {
                name: 'preview-popup',
                template: '<template><div class="preview-location-wrapper"><slot></slot></div></template>'
            }
        },
        data: {
            showPreview: false,
            isMultiple: ($(this).data('is-multiple') === 'True')
        },
        methods: {
            EventLocationListApplyEllipsis: function () {
                $clamp(document.getElementById('location-list'), {
                    clamp: 2,
                    useNativeClamp: false
                });
            }
        },
        mounted: function () {
            this.EventLocationListApplyEllipsis();
        }
    });
});
