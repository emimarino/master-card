Vue.component('mmc-dropdown', {
    template: '#mmc-dropdown-template',
    props: ['items', 'value', 'label', 'identifier', 'title', 'multiple', 'selectedlabel', 'noneselectedlabel'],
    data: function () {
        return {
            hide: false,
            selectedItem: [],
            dropdownIsClicked: false
        };
    },
    created: function () {
        if (this.value && this.items) {
            var self = this;
            this.selectedItems = this.items.filter(function (i) {
                return (Array.isArray(self.value) && self.value.indexOf(i[self.identifier]) !== -1) || i[self.identifier] == self.value;
            });
        }
    },
    methods: {
        getSelectedText: function () {
            if (!this.selectedItems) {
                this.selectedItems = [];
            }
            var count = this.selectedItems.length;
            if (count > 0) {
                if (this.multiple && count == this.items.length) {
                    return this.label;
                }
                if (count > 1) {
                    return this.selectedlabel + ' (' + count + ')';
                }
                return this.selectedItems[0][this.title];
            } else if (this.multiple) {
                return this.noneselectedlabel;
            }
            return null;
        },
        toggleSelected: function (item) {
            if (!this.selectedItems || (!this.multiple && !item)) {
                this.selectedItems = [];
            }
            if (item) {
                if (this.multiple) {
                    if ((Array.isArray(this.selectedItems) && this.selectedItems.indexOf(item) !== -1) || this.selectedItems == item) {
                        if (this.selectedItems.length > 1) {
                            this.selectedItems = this.selectedItems.filter(function (a) {
                                return a !== item;
                            });
                        } else {
                            this.selectedItems = [];
                        }
                    } else {
                        this.selectedItems.push(item);
                    }
                } else {
                    this.selectedItems = [item];
                }
            } else if (this.multiple) {
                if (this.selectedItems.length == this.items.length) {
                    this.selectedItems = [];
                } else {
                    this.selectedItems = this.items;
                }
            }

            var self = this;
            var valueIds = [];
            if (this.selectedItems.length > 0) {
                valueIds = this.selectedItems.map(function (value) {
                    return value[self.identifier];
                });
            }

            this.$forceUpdate();
            if (!this.multiple) {
                this.dropdownIsClicked = !this.dropdownIsClicked;
            }

            this.$emit('update', valueIds);
        },
        isItemActive: function (item) {
            if (!item && ((!this.multiple && this.selectedItems.length === 0) || this.items.length == this.selectedItems.length)) {
                return true;
            }

            return (Array.isArray(this.selectedItems) && this.selectedItems.indexOf(item) !== -1) || this.selectedItems == item;
        },
        clickDropdown: function () {
            this.dropdownIsClicked = !this.dropdownIsClicked;
        }
    }
});
