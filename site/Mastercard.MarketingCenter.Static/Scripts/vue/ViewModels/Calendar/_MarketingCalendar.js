//MMC-Calendar requires array.prototype.find, check and add polyfill if not available
// https://tc39.github.io/ecma262/#sec-array.prototype.find
if (!Array.prototype.find) {
    Object.defineProperty(Array.prototype, 'find', {
        value: function (predicate) {
            // 1. Let O be ? ToObject(this value).
            if (this == null) {
                throw new TypeError('"this" is null or not defined');
            }

            var o = Object(this);

            // 2. Let len be ? ToLength(? Get(O, "length")).
            var len = o.length >>> 0;

            // 3. If IsCallable(predicate) is false, throw a TypeError exception.
            if (typeof predicate !== 'function') {
                throw new TypeError('predicate must be a function');
            }

            // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
            var thisArg = arguments[1];

            // 5. Let k be 0.
            var k = 0;

            // 6. Repeat, while k < len
            while (k < len) {
                // a. Let Pk be ! ToString(k).
                // b. Let kValue be ? Get(O, Pk).
                // c. Let testResult be ToBoolean(? Call(predicate, T, � kValue, k, O �)).
                // d. If testResult is true, return kValue.
                var kValue = o[k];
                if (predicate.call(thisArg, kValue, k, o)) {
                    return kValue;
                }
                // e. Increase k by 1.
                k++;
            }

            // 7. Return undefined.
            return undefined;
        },
        configurable: true,
        writable: true
    });
}

function initializeCalendar(contentItemCampaignEventsUrl, monthsArray, isDownload, downloadCalendarBaseUrl, showEndYear) {
    var newCalendar = new Vue({
        el: '#mmc-calendar',
        data: {
            downloadCalendar: false,
            isFixed: false,
            contentItems: [],
            filters: {
                year: [],
                campaignCategory: [],
                markets: []
            },
            dropdowns: {
                campaignCategory: {
                    defaultText: 'All categories',
                    key: 'ItemId',
                    title: 'Title'
                },
                markets: {
                    defaultText: 'All Markets',
                    key: 'Identifier',
                    title: 'Title'
                },
                years: {
                    defaultText: '',
                    key: 'Id',
                    title: 'Id'
                }
            },
            yearList: [],
            campaignCategoryList: [],
            marketList: [],
            regionIdentifier: null,
            initialized: false
        },
        created: function () {
            var self = this;
            self.downloadCalendar = isDownload;
            self.fetchData();
            window.onscroll = function () {
                var headerTop = self.initialized && document.getElementById('calendar-body') != undefined ? document.getElementById('calendar-body').offsetTop : 0;
                var scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
                self.isFixed = !self.downloadCalendar && (scrollTop > headerTop + 48);
            };
        },
        updated: function () {
            if (this.initialized && this.downloadCalendar) {
                this.initializeDownload();
            }
        },
        computed: {
            campaignCategoryDropdownList: function () {
                var self = this;
                return self.campaignCategoryList.filter(function (i) {
                    return self.getItemsByCampaignCategory(i.ItemId).length > 0;
                });
            },
            campaignCategoryListFiltered: function () {
                var self = this;
                var filtered = self.campaignCategoryDropdownList;
                if (self.filters.campaignCategory.length > 0) {
                    filtered = filtered.filter(function (i) {
                        return self.filters.campaignCategory.indexOf(i.ItemId) !== -1;
                    });
                }

                return filtered;
            },
            marketListFiltered: function () {
                var self = this;
                var filtered = self.marketList;
                if (self.filters.markets) {
                    filtered = filtered.filter(function (i) {
                        return self.filters.markets.indexOf(i.Identifier) !== -1;
                    });
                }

                return filtered;
            }
        },
        methods: {
            fetchData: function () {
                var self = this;
                var xhr = new XMLHttpRequest();
                xhr.open('GET', contentItemCampaignEventsUrl);
                xhr.onload = function () {
                    var data = JSON.parse(xhr.responseText);
                    self.contentItems = data.ContentItems;
                    self.campaignCategoryList = data.CampaignCategories;
                    self.marketList = data.Markets;
                    self.filters.markets = self.marketList.map(function (m) {
                        return m.Identifier
                    });
                    self.regionIdentifier = data.RegionIdentifier;
                    var startYear = self.getDate(data.CalendarStartDate).getFullYear();
                    var endYear = self.getDate(data.CalendarEndDate).getFullYear();
                    var years = [{ Id: startYear }];
                    for (var i = startYear + 1; i <= endYear; i++) {
                        years.push({ Id: i });
                    }
                    self.yearList = years.filter(function (y) {
                        return self.contentItems.filter(function (i) {
                            return i.CampaignEvents.some(function (e) {
                                return self.getDate(e.StartDate).getFullYear() <= y.Id
                                    && self.getDate(e.EndDate).getFullYear() >= y.Id;
                            });
                        }).length > 0;
                    });
                    if (Array.isArray(self.yearList) && self.yearList.length > 0) {
                        if (showEndYear) {
                            self.updateYearSelected([self.yearList[self.yearList.length - 1].Id]);
                        }
                        else {
                            self.updateYearSelected([self.yearList[0].Id]);
                        }
                    }
                    self.initialized = true;
                };
                xhr.send();
            },
            getDate: function (dateText) {
                var date = new Date(dateText.match(/\d+/)[0] * 1);
                return new Date(date.toUTCString().substr(0, 25));
            },
            getDateFormatString: function (dateText) {
                var date = this.getDate(dateText);
                return date.getDate() + ' ' + monthsArray[date.getMonth()] + ', ' + date.getFullYear();
            },
            getMonthNumber: function (date) {
                return parseInt(date.getMonth());
            },
            getEventClasses: function (event) {
                var _start = this.getDate(event.StartDate);
                var _end = this.getDate(event.EndDate);
                var offset = 0;
                var span = 12;

                var currentYear = parseInt(this.filters.year);

                if (_start.getFullYear() === currentYear) {
                    offset = this.getMonthNumber(_start);
                }
                if (_end.getFullYear() === currentYear) {
                    span = this.getMonthNumber(_end) - offset + 1;
                }
                if (_start.getFullYear() > currentYear || _end.getFullYear() < currentYear) {
                    return ['hide'];
                }

                return ['o' + offset, 'w' + span];
            },
            getItemsByCampaignCategory: function (campaignCategoryId) {
                var self = this;
                var filtered = self.contentItems.filter(function (i) {
                    return i.CampaignCategoryId === campaignCategoryId
                        && i.CampaignEvents.some(function (e) {
                            var currentYear = parseInt(self.filters.year);
                            return self.getDate(e.StartDate).getFullYear() <= currentYear
                                && self.getDate(e.EndDate).getFullYear() >= currentYear;
                        });
                });
                if (self.filters.markets) {
                    filtered = filtered.filter(function (i) {
                        return i.MarketIdentifiers.indexOf(self.regionIdentifier) !== -1
                            || i.MarketIdentifiers.filter(function (m) { return self.filters.markets.indexOf(m) !== -1; }).length > 0;
                    });
                }

                return filtered;
            },
            updateCampaignCategorySelected: function (campaignCategoryIds) {
                this.filters.campaignCategory = campaignCategoryIds;
            },
            updateMarketSelected: function (marketIds) {
                this.filters.markets = marketIds;
            },
            updateYearSelected: function (yearIds) {
                this.filters.year = yearIds;
                this.filters.campaignCategory = [];
                if (this.$refs.campaignCategoryFilter) {
                    this.$refs.campaignCategoryFilter.selectedItems = [];
                }
                $("#breadcrumbCalendarYear").text(yearIds);
                $("#titleCalendarYear").text(yearIds);
            },
            downloadCalendarUrl: function () {
                return downloadCalendarBaseUrl + '?downloadYear=' + this.filters.year + '&campaignCategoryIds=' + this.campaignCategoryListFiltered.map(function (ct) { return ct.ItemId }) + '&marketIdentifiers=' + this.marketListFiltered.map(function (m) { return m.Identifier });
            },
            campaignCategoryShowEventsToggle: function (e) {
                var $this = $(e.target);
                if (!$this.is('header')) {
                    $this = $this.closest("header");
                }
                var $article = $this.next('.mmc-calendarV3--section-article');
                if ($article.hasClass('opened')) {
                    $this.removeClass('opened');
                    $article.removeClass('opened');
                    $article.fadeOut(0);
                }
                else {
                    $this.addClass('opened');
                    $article.addClass('opened');
                    $article.fadeIn();
                }
            },
            initializeDownload: function () {
                var self = this;
                var element;
                var elementHeight = 0;
                var calendarHeight = 600;
                var header = $('.mmc-calendarV3--header');
                var initialHeight = header.height();
                var currentHeight = initialHeight + $('.mmc-calendarV3--filters-item.inline').height();
                $.each(self.campaignCategoryListFiltered, function (index, campaignCategory) {
                    element = $('#' + campaignCategory.ItemId);
                    elementHeight = element.height();
                    if (currentHeight + elementHeight < calendarHeight) {
                        currentHeight += elementHeight;
                    }
                    else {
                        element = element.find("header");
                        elementHeight = element.height();
                        if (currentHeight + elementHeight < calendarHeight) {
                            currentHeight += elementHeight;
                        }
                        else {
                            header.clone().attr("style", "page-break-before: always;").insertBefore(element);
                            currentHeight = initialHeight + elementHeight;
                        }
                        $.each(self.getItemsByCampaignCategory(campaignCategory.ItemId), function (index, contentItem) {
                            element = $('#' + campaignCategory.ItemId + '-' + contentItem.ContentItemId);
                            elementHeight = element.height();
                            if (currentHeight + elementHeight < calendarHeight) {
                                currentHeight += elementHeight;
                            }
                            else {
                                header.clone().attr("style", "page-break-before: always;").insertBefore(element);
                                currentHeight = initialHeight;
                            }
                        });
                    }
                });
            }
        }
    });
}
