import Vue from 'vue'
import PreviewFile from './PreviewFile.vue'

Vue.config.productionTip = false

$('.preview-file').each(function() {
    new Vue({
        el: this,
        components: {
            'preview-file': PreviewFile
        },
        data: {
            showPreviewFile: false
        }
    });
});
