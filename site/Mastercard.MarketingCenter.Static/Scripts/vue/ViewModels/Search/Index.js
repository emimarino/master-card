﻿function initializeSearchResults(searchUrl, resultsCount) {
    new Vue({
        el: '#search-results',
        data: {
            bottom: false,
            isLoading: false,
            currentIndex: 1,
            noMoreData: false,
            model_result: [],
            model_featured_result: [],
            searchCount: 0,
            filters: {
                length: 10,
                start: 0,
                sortBy: 'Relevance',
                sortDirection: 'Asc',
                searchQuery: ""
            }
        },

        methods: {
            postback: function (event, targetUrlLink, resultsQty) {
                event.preventDefault()
                var form = document.createElement("form")
                form.setAttribute('method', "post")
                var urlVisitedInput = document.createElement("input")
                urlVisitedInput.setAttribute('type', "hidden")
                urlVisitedInput.setAttribute('name', "UrlVisited")
                urlVisitedInput.setAttribute('value', targetUrlLink)
                var btn = document.createElement("input")
                btn.setAttribute('type', "submit")
                form.appendChild(urlVisitedInput)
                document.body.appendChild(form)
                form.submit()

            },
            bottomVisible: function () {
                const scrollY = window.pageYOffset
                const visible = document.documentElement.clientHeight
                const pageHeight = document.documentElement.scrollHeight - document.getElementsByClassName('page-footer')[0].clientHeight
                const bottomOfPage = visible + scrollY >= pageHeight
                return bottomOfPage || pageHeight < visible
            },
            fetchData: function () {
                self = this;
                if (!self.isLoading) {
                    self.isLoading = true;
                    var xhr = new XMLHttpRequest();
                    xhr.open('POST', searchUrl);
                    xhr.setRequestHeader('Content-type', 'application/json')
                    xhr.send(JSON.stringify(self.filters));
                    xhr.onload = function () {
                        var response = JSON.parse(xhr.responseText);

                        if (self.filters.start == 0 && response.FeaturedSearchResults) {
                            response.FeaturedSearchResults.forEach(function (x) {
                                self.model_featured_result.push(x);
                            })
                        }

                        if (response.SearchResults) {
                            response.SearchResults.forEach(function (x) {
                                self.model_result.push(x);
                            })
                            if (response.SearchResults.length < self.filters.length) {
                                self.noMoreData = true;
                            }
                            self.filters.start = self.currentIndex * self.filters.length;
                            self.currentIndex = self.currentIndex + 1;
                        }
                        else {
                            self.noMoreData = true;
                        }
                        self.isLoading = false;
                    }
                }
            },
            addTooltip: function () {
                $(".pagerfield").each(function () {
                    if ($(this).attr("disabled")) {
                        $(this).attr("src", $(this).attr("src").replace("_active", ""));
                    }
                });
                $.widget.bridge("ui.tooltip", $.fn.tooltip);
                $(".status.new,.status.updated").tooltip();
            },
            updateSortBy: function (id, dir) {
                this.filters.sortBy = id;
                this.filters.sortDirection = dir;
                this.reset();

                this.fetchData();
            },
            reset: function () {
                this.filters.start = 0;
                this.currentIndex = 1;
                this.noMoreData = false;
                this.model_result = [];
                this.model_featured_result = [];
            },
            formatString: function (sentence, word) {
                var isPlural = word < 2 ? "" : "s"
                var words = [word, isPlural];
                return sentence.replace(/{(\d+)}/g, function (match, number) {
                    return typeof words[number] != 'undefined'
                        ? words[number]
                        : match
                        ;
                });
            }
        },
        watch: {
            bottom: function (bottom) {
                if (bottom && !this.noMoreData) {
                    this.fetchData()
                }
            }
        },
        created: function () {
            this.searchCount = resultsCount;
            this.filters.searchQuery = window.location.pathname.split("/").pop();
            var self = this;
            window.addEventListener('scroll', function () {
                self.bottom = self.bottomVisible()
            })
            this.reset();
            this.fetchData();
        },
        updated: function () {
            this.addTooltip();
        }
    })
}
