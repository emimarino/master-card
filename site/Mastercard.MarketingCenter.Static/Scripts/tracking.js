﻿var addParams = function (url, params) {
    url += (url.indexOf('?') >= 0 ? '&' : '?') + params;
    return url;
}

var urlParam = function (url, param) {
    var results = new RegExp('[\?&]' + param + '=([^&#]*)').exec(url);
    if (results == null) {
        return null;
    }
    else {
        return results[1] || 0;
    }
}

var trackLink = function (url, how) {
    if (url.indexOf('javascript:void(0)') >= 0) {
        return;
    }
    $.ajax({
        type: 'POST',
        async: true,
        url: '/portal/tracking/tracklink',
        data: { href: url, how: how }
    });
}

function updateTrackLink(self) {
    var href = $(self).attr('href');
    if (!href || href.indexOf('javascript:void(0)') >= 0) {
        return;
    }
    var type = $(self).attr('tracking-type');
    if (type !== undefined && urlParam($(self).attr('href'), 'trackingType') == null) {
        $(self).attr('href', addParams($(self).attr('href'), 'trackingType=' + type));
    }
}

$(document).ready(function () {
    $('a').click(function () {
        updateTrackLink(this);
    });
    $('a').mousedown(function () {
        updateTrackLink(this);
    });
});
