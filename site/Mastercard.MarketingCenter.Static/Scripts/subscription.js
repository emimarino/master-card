﻿var subscription = {
    contentItemId: null,
    isSubscribed: null,
    textAdd: null,
    textRemove: null,
    showFirstTimeSubscribeModalDialog: null,
    showFirstTimeSubscribeModalClose: null,
    showNeverFrecuencyMessageModalDialog: null,
    showNeverFrecuencyMessageModalNotNow: null,
    htmlElements: {
        spanContainer: null,
        spanText: null,
        i: null,
        loading: null
    },
    constants: {
        gray: "btn-gray-light",
        orange: "btn-orange-light",
        check: "fa-check",
        heart: "fa-heart",
        enabled: "btn-enabled",
        disabled: "btn-disabled",
        loadingShow: "loading-show",
        loadingHide: "loading-hide",
        iconShow: "icon-show",
        iconHide: "icon-hide"
    },
    init: function () {
        //General
        this.htmlElements.spanContainer = document.getElementById("cmdSubscriptionAction[" + this.contentItemId + "]");
        this.htmlElements.spanText = this.htmlElements.spanContainer.querySelector("#textValue");
        this.htmlElements.i = this.htmlElements.spanContainer.querySelector("#icon");
        this.htmlElements.loading = this.htmlElements.spanContainer.querySelector("#loading");

        this.htmlElements.spanContainer.addEventListener("click", this.executeToggle.bind(this), false);
        this.renderButton(this.isSubscribed);

        //showFirstTimeSubscribeModal
        if (document.getElementById("showFirstTimeSubscribeModal") != undefined) {
            this.showFirstTimeSubscribeModalDialog = $("#showFirstTimeSubscribeModal");
            this.showFirstTimeSubscribeModalClose = document.getElementById("showFirstTimeSubscribeModalClose");
            this.setUpShowFirstTimeSubscribePopUp();
            this.showFirstTimeSubscribeModalClose.addEventListener("click", this.closeShowFirstTimeSubscribePopUp.bind(this), false);
        }

        //showNeverFrecuencyMessageModal
        if (document.getElementById("showNeverFrecuencyMessageModal") != undefined) {
            this.showNeverFrecuencyMessageModalDialog = $("#showNeverFrecuencyMessageModal");
            this.showNeverFrecuencyMessageModalNotNow = document.getElementById("showNeverFrecuencyMessageModalNotNow");

            this.setUpNeverFrecuencyMessagePopUp();
            this.showNeverFrecuencyMessageModalNotNow.addEventListener("click", this.closeNeverFrecuencyMessagePopUp.bind(this), false);
        }
    },
    executeToggle: function (event) {
        this.disableButton();
        $.ajax({
            url: '/portal/Profile/ToggleSubscription?contentItemId=' + this.contentItemId,
            context: this
        }).done(function (data) {
            if (data.error) {
                console.dir("Error Toggling Subscription");
            }
            else {
                this.renderButton(data.subscribed);
                this.checkPopups(data);
            }
        }).fail(function (error) {
            console.dir(error);
            this.enableButton();
        }).always(function () {
            this.enableButton();
        });
    },
    showGray: function (contentItemId) {
        this.htmlElements.spanContainer.classList.add(this.constants.gray);
        this.htmlElements.spanContainer.classList.remove(this.constants.orange);
        this.htmlElements.spanText.innerHTML = this.textRemove;
        this.htmlElements.i.classList.add(this.constants.check);
        this.htmlElements.i.classList.remove(this.constants.heart);
    },
    showOrange: function (contentItemId) {
        this.htmlElements.spanContainer.classList.add(this.constants.orange);
        this.htmlElements.spanContainer.classList.remove(this.constants.gray);
        this.htmlElements.spanText.innerHTML = this.textAdd;
        this.htmlElements.i.classList.add(this.constants.heart);
        this.htmlElements.i.classList.remove(this.constants.check);
    },
    renderButton: function (isSubscribed) {
        if (isSubscribed) {
            this.showGray(this.contentItemId);
        }
        else {
            this.showOrange(this.contentItemId);
        }
    },
    checkPopups: function (data) {
        if (data.showFirstTimeSubscribe) {
            this.openShowFirstTimeSubscribePopUp();
        }
        else if (data.showNeverFrecuencyMessage) {
            this.openNeverFrecuencyMessagePopUp();
        }
    },
    setUpShowFirstTimeSubscribePopUp: function () {
        this.showFirstTimeSubscribeModalDialog.dialog({
            modal: true,
            autoOpen: false,
            draggable: true,
            height: 'auto',
            width: 1000,
            resizable: false,
            dialogClass: 'customize-start-modal'
        });
    },
    openShowFirstTimeSubscribePopUp: function () {
        this.showFirstTimeSubscribeModalDialog.dialog("open");
    },
    closeShowFirstTimeSubscribePopUp: function () {
        this.showFirstTimeSubscribeModalDialog.dialog("close");
    },
    setUpNeverFrecuencyMessagePopUp: function () {
        this.showNeverFrecuencyMessageModalDialog.dialog({
            modal: true,
            autoOpen: false,
            draggable: true,
            height: 'auto',
            width: 1000,
            resizable: false,
            dialogClass: 'customize-start-modal'
        });
    },
    openNeverFrecuencyMessagePopUp: function () {
        this.showNeverFrecuencyMessageModalDialog.dialog("open");
    },
    closeNeverFrecuencyMessagePopUp: function () {
        this.showNeverFrecuencyMessageModalDialog.dialog("close");
    },
    disableButton: function () {
        this.htmlElements.spanContainer.classList.add(this.constants.disabled);
        this.htmlElements.spanContainer.classList.remove(this.constants.enabled);
        this.htmlElements.loading.classList.add(this.constants.loadingShow);
        this.htmlElements.loading.classList.remove(this.constants.loadingHide);
        this.htmlElements.i.classList.add(this.constants.iconHide);
        this.htmlElements.i.classList.remove(this.constants.iconShow);
    },
    enableButton: function () {
        this.htmlElements.spanContainer.classList.add(this.constants.enabled);
        this.htmlElements.spanContainer.classList.remove(this.constants.disabled);
        this.htmlElements.loading.classList.add(this.constants.loadingHide);
        this.htmlElements.loading.classList.remove(this.constants.loadingShow);
        this.htmlElements.i.classList.add(this.constants.iconShow);
        this.htmlElements.i.classList.remove(this.constants.iconHide);
    }
};
