﻿$(document).ready(function () {
    $('#feedback-ready').click(function () {
        $.ajax({
            type: 'POST',
            async: true,
            url: '/portal/header/feedback/true',
            success: function () {
                window.location = '/portal/contact'
            }
        });
    });

    $('#feedback-not-now').click(function () {
        $.ajax({
            type: 'POST',
            async: true,
            url: '/portal/header/feedback/false',
            success: function () {
                window.location.reload();
            }
        });
    });
});
