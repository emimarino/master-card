﻿function ShowMoreTags_Click(t, CommandArgument) {
    if ($(t).text() == "Show more ") {
        CommandArgument.split("|").forEach(function (item) {
            var listArguments = item.split(",");
            if (eval(listArguments[3].toLowerCase())) {
                $('<li><a class="item on" href="' + listArguments[0] + '">' + listArguments[1] + '(' + listArguments[2] + ')<i class="x"></i></a></li>').insertBefore($(t));
            }
            else {
                $('<li><a class="item " href="' + listArguments[0] + '">' + listArguments[1] + '(' + listArguments[2] + ')</a></li>').insertBefore($(t));
            }
        });
        $(t).html("Show less <i class='fa fa-angle-up style='color: #008e96'>");
        $("#categories-id").resize();
    }
    else {
        for (var i = 0; i < CommandArgument.split("|").length; i++) {
            $(t).prev().remove();
        }
        $(t).html("Show more <i class='fa fa-angle-down style='color: #008e96'>");
        $("#categories-id").resize();
    }
}
