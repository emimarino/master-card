﻿const { VueLoaderPlugin } = require('vue-loader');
const path = require('path');
const webpack = require('webpack');
const fs = require('fs');
const glob = require('glob');

const appBasePath = './Scripts/vue/';

module.exports = {
    target: 'web',
    entry: {
        // entry name is the [name] property used on the output filename.
        //calendar: glob.sync(path.resolve(appBasePath, 'ViewModels/Calendar/*.js*')),
        shared: glob.sync(path.resolve(appBasePath, 'ViewModels/Shared/**/*.js*'))
    },
    output: {
        path: path.resolve(__dirname, './Scripts/vue/bundle/'),
        publicPath: '/Scripts/vue/bundle/',
        filename: '[name].bundle.js'
    },
    resolve: {
        extensions: ['.js', '.vue', '.json'],
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
            'root': path.join(__dirname, appBasePath),
            '@': path.join(__dirname, appBasePath),
        }
    },
    module: {
        rules: [
            { test: /\.vue$/, use: 'vue-loader' },
            { test: /\.js$/, use: 'babel-loader', exclude: /node_modules/ },
            { test: /\.scss$/, use: 'style-loader!css-loader!sass-loader' },
            { test: /\.css$/, use: 'style-loader!css-loader' },
            { test: /\.(eot|svg|ttf|woff|woff2)(\?\S*)?$/, use: 'file-loader' },
            { test: /\.(png|jpe?g|gif|svg)(\?\S*)?$/, use: 'file-loader' }
        ]
    },
    plugins: [
        new VueLoaderPlugin()
    ]
};
