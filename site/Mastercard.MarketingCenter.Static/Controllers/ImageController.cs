﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Static.Controllers
{
    public class ImageController : Controller
    {
        private readonly ISettingsService _settingsService;
        private readonly IImageService _imageService;

        public ImageController(ISettingsService settingsService, IImageService imageService)
        {
            _settingsService = settingsService;
            _imageService = imageService;
        }

        public async Task<ActionResult> GetImage(string url, int? width = null, int? height = null, bool crop = false, int jpegQuality = 95, bool rootPath = false)
        {
            if (!string.IsNullOrWhiteSpace(url))
            {
                var image = await Task.Run(() => _imageService.GetImage(url, width.GetValueOrDefault(), height.GetValueOrDefault(), crop, jpegQuality, rootPath));
                if (image != null)
                {
                    return await Task.Run(() => File(image, url.GetMimeType(), Path.GetFileName(url)));
                }
            }

            return await Task.Run(() => RedirectToActionPermanent("GetBrokenImage", new { width }));
        }

        public async Task<ActionResult> GetBrokenImage(int width)
        {
            var brokenImageName = _settingsService.GetBrokenImageName(width);
            return await Task.Run(() => File(_imageService.GetMappedImage($"~/Images/{brokenImageName}"), brokenImageName.GetMimeType()));
        }
    }
}