﻿using Autofac;
using Autofac.Integration.Mvc;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Reflection;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Static.Core.Bootstrappers
{
    public class AutofacBootstrapper : IBootstrapper
    {
        public void Execute()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly()).PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);
            builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinderProvider();
            builder.RegisterFilterProvider();
            builder.RegisterType<SettingsService>().As<ISettingsService>();
            builder.RegisterType<ImageService>().As<IImageService>();

            var container = builder.Build();
            var provider = new StandaloneLifetimeScopeProvider(container);
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container, provider));
        }
    }
}