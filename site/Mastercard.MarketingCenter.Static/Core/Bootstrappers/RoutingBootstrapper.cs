﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using System.Web.Mvc;
using System.Web.Routing;

namespace Mastercard.MarketingCenter.Static.Core.Bootstrappers
{
    public class RoutingBootstrapper : IBootstrapper
    {
        public void Execute()
        {
            var routes = RouteTable.Routes;

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });

            routes.MapRoute("GetImage", "GetImage", new { controller = "Image", action = "GetImage" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}