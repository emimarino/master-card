﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using System.Web.Optimization;

namespace Mastercard.MarketingCenter.Static.Core.Bootstrappers
{
    public class BundleBootstrapper : IBootstrapper
    {
        public void Execute()
        {
            RegisterBundles(BundleTable.Bundles);
            BundleTable.EnableOptimizations = true;
            BundleTable.Bundles.UseCdn = true;
        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            RegisterStylesBundle(bundles);
            RegisterScriptsBundle(bundles);
        }

        private static void RegisterStylesBundle(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/content/css")
                            .Include("~/Styles/slick.css")
                            .Include("~/Styles/font-awesome.min.css")
                            .Include("~/Styles/bootstrap-grid.min.css")
                            .Include("~/Styles/bootstrap-modal.min.css")
                            .Include("~/Styles/fonts.css")
                            .Include("~/Styles/main.css")
                            .Include("~/Styles/lazyYT.css")
                            .Include("~/Styles/responsive.css")
                            .Include("~/Styles/header.css")
                       );

            bundles.Add(new StyleBundle("~/triggermarketing/css").Include("~/Styles/trigger-marketing.css"));

            bundles.Add(new StyleBundle("~/header/css").Include("~/Styles/header.css"));

            bundles.Add(new StyleBundle("~/favorites/css").Include("~/Styles/component-favorites.css"));

            bundles.Add(new StyleBundle("~/homebanner/css").Include("~/Styles/home-banner.css"));

            bundles.Add(new StyleBundle("~/calendar/css").Include("~/Styles/mmc-calendar.css"));

            bundles.Add(new StyleBundle("~/downloadcalendar/css")
                            .Include("~/Styles/fonts.css")
                            .Include("~/Styles/main.css")
                            .Include("~/Styles/mmc-calendar.css")
                        );

            bundles.Add(new StyleBundle("~/offer/css")
                            .Include("~/Styles/offer.css")
                            .Include("~/Styles/chosen.min.css")
                        );

            bundles.Add(new StyleBundle("~/search/css")
                            .Include("~/Styles/search.css")
                            .Include("~/Styles/bootstrap-tooltip.css")
                        );

            bundles.Add(new StyleBundle("~/shoppingcart/css")
                            .Include("~/Styles/main.css")
                            .Include("~/Styles/shoppingcart.css")
                            .Include("~/Styles/responsive.css")
                            .Include("~/Styles/header.css")
                            .Include("~/Styles/fonts.css")
                            .Include("~/Styles/font-awesome.min.css")
                        );

            bundles.Add(new StyleBundle("~/onetrust/css").Include("~/Styles/onetrust.css"));
        }

        private static void RegisterScriptsBundle(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/content/js")
                            .Include("~/Scripts/slick.min.js")
                            .Include("~/Scripts/bootstrap.modal.min.js")
                            .Include("~/Scripts/bootstrap.dropdown.js")
                            .Include("~/Scripts/masonry.pkgd.min.js")
                            .Include("~/Scripts/lazyYT.js")
                            .Include("~/Scripts/jwplayer.js")
                            .Include("~/Scripts/jwplayer.key.js")
                            .Include("~/Scripts/main.js")
                            .Include("~/Scripts/video-modal.js")
                            .Include("~/Scripts/tracking.js")
                            .Include("~/Scripts/jquery.selectric.js")
                            .Include("~/Scripts/subscription.js")
                            .Include("~/Scripts/vue.min.js")
                            .Include("~/Scripts/clipboard.min.js")
                            .Include("~/Scripts/footer.js")
                            .Include("~/Scripts/header.js")
                        );

            bundles.Add(new ScriptBundle("~/triggermarketing/js")
                            .Include("~/Scripts/vue.min.js")
                            .Include("~/Scripts/trigger-marketing.js")
                        );

            bundles.Add(new ScriptBundle("~/header/js")
                            .Include("~/Scripts/jquery-1.11.3.min.js")
                            .Include("~/Scripts/jquery-ui-1.11.4.min.js")
                            .Include("~/Scripts/vue.min.js")
                            .Include("~/Scripts/jquery.selectric.js")
                            .Include("~/Scripts/masonry.pkgd.min.js")
                            .Include("~/Scripts/header.js")
                        );

            bundles.Add(new ScriptBundle("~/favorites/js")
                            .Include("~/Scripts/tracking.js")
                            .Include("~/Scripts/clamp.js")
                        );

            bundles.Add(new ScriptBundle("~/calendar/js")
                            .Include("~/Scripts/vue/ViewModels/Calendar/_MarketingCalendar.js")
                            .Include("~/Scripts/vue/ViewModels/Calendar/_MarketingCalendarFilter.js")
                       );

            bundles.Add(new ScriptBundle("~/tag-browser/js").Include("~/Scripts/TagBrowser/tag-browser.js"));

            bundles.Add(new ScriptBundle("~/downloadcalendar/js")
                            .Include("~/Scripts/vue.min.js")
                            .Include("~/Scripts/vue/ViewModels/Calendar/_MarketingCalendar.js")
                            .Include("~/Scripts/vue/ViewModels/Calendar/_MarketingCalendarFilter.js")
                       );

            bundles.Add(new ScriptBundle("~/vue/bundle/js").Include("~/Scripts/vue/bundle/shared.bundle.js"));

            bundles.Add(new ScriptBundle("~/jquery/js", "https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js")
                            .Include("~/Scripts/jquery-1.11.3.min.js")
                       );

            bundles.Add(new ScriptBundle("~/jqueryui/js", "https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js")
                            .Include("~/Scripts/jquery-ui-1.11.4.min.js")
                       );

            bundles.Add(new ScriptBundle("~/offer/js")
                            .Include("~/Scripts/moment.min.js")
                            .Include("~/Scripts/chosen.jquery.min.js")
                            .Include("~/Scripts/vue/ViewModels/Offer/_AccordionFilter.js")
                            .Include("~/Scripts/vue/ViewModels/Offer/_CalendarFilter.js")
                            .Include("~/Scripts/vue/ViewModels/Offer/_LocationFilter.js")
                            .Include("~/Scripts/offer.js")
                       );

            bundles.Add(new ScriptBundle("~/offerdetails/js")
                            .Include("~/Scripts/clamp.js")
                            .Include("~/Scripts/vue/ViewModels/Offer/_PreviewLocationsPopup.js")
                       );

            bundles.Add(new ScriptBundle("~/footer/js").Include("~/Scripts/footer.js"));

            bundles.Add(new ScriptBundle("~/search/js")
                            .Include("~/Scripts/vue/ViewModels/Search/Index.js")
                            .Include("~/Scripts/bootstrap.tooltip.js")
                        );

            bundles.Add(new ScriptBundle("~/captchavalidator/js").Include("~/Scripts/captcha-validator.js"));

            bundles.Add(new ScriptBundle("~/shoppingcart/js")
                            .Include("~/Scripts/main.js")
                            .Include("~/Scripts/jquery-ui-1.8.11.min")
                            .Include("~/Scripts/jquery.jq.modal.js")
                            .Include("~/Scripts/common.function.js")
                            .Include("~/Scripts/autocomplete.jquery.js")
                            .Include("~/Scripts/jquery.selectric.js")
                            .Include("~/Scripts/tracking.js")
                            .Include("~/Scripts/shared.js")
                            .Include("~/Scripts/header.js")
                        );

            bundles.Add(new ScriptBundle("~/commonfunction/js").Include("~/Scripts/common.function.js"));
        }
    }
}