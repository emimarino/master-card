﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastercard.MarketingCenter.Domo.ReportGenerator
{
    public interface IToolModules
    {
        IDictionary<string, string> GetParameters();
        void Execute(string[] args);
    }
}
