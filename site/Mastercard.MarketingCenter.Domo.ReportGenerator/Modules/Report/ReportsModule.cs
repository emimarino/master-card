﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastercard.MarketingCenter.Domo.ReportGenerator.Report
{
    public class ReportsModule:IToolModules
    {

        private readonly IEnumerable<IReportGenerator> _reportGenerators;
        public ReportsModule(IEnumerable<IReportGenerator> reportGenerators) {
            _reportGenerators = reportGenerators;
        }
        public IDictionary<string, string> GetParameters()
        {
            var dict = new Dictionary<string, string>();
            foreach (var generator in _reportGenerators)
            {
                dict.Add(generator.GetType().Name.Replace("Module",""),$"\t{generator.GetParameters().Aggregate(new StringBuilder(), (previous, kv) => previous.Append($"\n\t\t{kv.Key} : {kv.Value}"))} --\n");
            }
            return dict;

        }

        public void Execute(string[] args)
        {

            foreach (var generator in _reportGenerators)
            {
                generator.Execute(args);
                
            }
        }


    }
}
