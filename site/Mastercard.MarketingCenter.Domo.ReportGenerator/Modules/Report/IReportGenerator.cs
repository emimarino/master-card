﻿using System;

namespace Mastercard.MarketingCenter.Domo.ReportGenerator.Report
{
    public interface IReportGenerator:IToolModules
    {
        TimeSpan GetElpasedTime();
    }
}
