﻿using Autofac;
using Mastercard.MarketingCenter.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Mastercard.MarketingCenter.Domo.ReportGenerator.Report
{
    public class UserReportGenerator : AbstractModules, IReportGenerator
    {
        private readonly ReportRepository _reportRepository;
        private TimeSpan elapsedTime;
        private const string ModuleName = "User";
        private static IDictionary<string, string> Parameters = new Dictionary<string, string> { { "From", "From" }, { "To", "To" }, { "UserRegion", "UserRegion" }, { "Verbose", "-Verbose" } };
        public UserReportGenerator(ReportRepository reportRepository) : base(ModuleName, Parameters)
        {

            _reportRepository = reportRepository;
        }
        public void Execute(string[] args)
        {
            var clock = Stopwatch.StartNew();


            var userParam = args.Where(a => a.IndexOf("User.") >= 0).Select(a => a.Replace("User.", "").Replace(" ", "")).ToDictionary(a => a.Split(':', '=')[0], a => a.Split(':', '=')[1]);
            if (userParam.Keys.Contains("To") && userParam.Keys.Contains("From"))
            {
                var vals2 = new
                {
                    from = DateTime.Parse(userParam.ContainsKey("From") ? userParam["From"] : ""),
                    to = DateTime.Parse(userParam.ContainsKey("To") ? userParam["To"] : ""),
                    userRegion = userParam.ContainsKey("UserRegion") ? userParam["UserRegion"] : "",
                };

                var results2 = _reportRepository.GetUserRegistrationReport(vals2.from, vals2.to, vals2.userRegion, true);
                var stringBuilder = new StringBuilder();
                stringBuilder.AppendLine(@"<html><header><style>table, th, td {border: 1px solid black;}</style></header><body><table ><tr><th>AudienceSegmentDisplay</th><th>AudienceSegments</th><th>Country</th><th>Editable</th><th>Email</th><th>EmailConsent</th><th>FI</th><th>FirstName</th><th>Frequency</th><th>FullName</th><th>UserRegion</th><th>LastLoginDate</th><th>LastName</th><th>Processor</th><th>RegistrationDate</th><th>UserIsDisabled</th><th>UserName</th></tr><tbody>");
                foreach (var result in results2)
                {
                    stringBuilder.AppendLine($@"<tr><td>{result.AudienceSegmentDisplay}</td><td>{result.AudienceSegments}</td><td>{result.Country}</td><td>{!result.IsDisabled}</td><td>{ result.Email}</td><td>{result.EmailConsent.ToString()}</td><td>{result.FI}</td><td>{result.FirstName.ToString()}</td><td>{result.Frequency}</td><td>{result.FullName}</td><td>{result.LastLoginDate}</td><td>{result.LastName}</td><td>{result.Processor}</td><td>{result.RegistrationDate}</td><td>{result.IsDisabled}</td><td>{result.UserName}</td></tr>");

                }
                stringBuilder.AppendLine(@"<tbody></table></body></html>");
                var body = stringBuilder.ToString();
                File.WriteAllText($@"{AppDomain.CurrentDomain.BaseDirectory}/Users.html", body, Encoding.UTF8);
                if (args.Any(a => a.Equals("-Verbose", StringComparison.InvariantCultureIgnoreCase)))
                { Console.WriteLine(body + "/n"); }


            }
            else { if (args.Any(p => Parameters.Any(kv => kv.Value.Equals(p)))) { Console.WriteLine("No Users"); } }
            clock.Stop();
            elapsedTime = TimeSpan.FromMilliseconds(clock.ElapsedMilliseconds);
        }

        public TimeSpan GetElpasedTime()
        {
            if (elapsedTime == null)
            {
                return new TimeSpan();
            }
            else { return elapsedTime; }
        }
    }
}
