﻿using Autofac;
using Mastercard.MarketingCenter.Domo.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Mastercard.MarketingCenter.Domo.ReportGenerator.Report
{
    public class DownloadsReportGenerator :  AbstractModules, IReportGenerator
    {
        private readonly IDownloadsService _downloadsService;
        private readonly DownloadsExtensionRepository _downloadsExtensionRepository;
        private TimeSpan elapsedTime;
        private const string ModuleName = "Download";
       private static IDictionary<string, string> Parameters = new Dictionary<string, string>{ { "UpdateDownloadsRecords", "-Update" },{"From","From" },{ "To", "To"},{ "ContentRegion","ContentRegion" }
            ,  {"ContentTypeId" ,"ContentTypeId" }, {"UserRegion" ,"UserRegion" } , {"UserFinancialInstitution","UserFinancialInstitution" },{"UserEmail" ,"UserEmail" }
                ,{"IncludeExcludedResults","IncludeExcludedResults" } ,{"Verbose","-Verbose"} };
        public DownloadsReportGenerator(IDownloadsService downloadsService, DownloadsExtensionRepository downloadsExtensionRepository) : base(ModuleName, Parameters)
        {
          
            _downloadsService = downloadsService;
            _downloadsExtensionRepository = downloadsExtensionRepository;
        }
        public void Execute(string[] args)
        {
            var clock = Stopwatch.StartNew();

            var maxLoops = int.Parse(ConfigurationManager.AppSettings["downloads.max.loop"] ?? "100");
            var totalLoops = maxLoops;
            if (args.Contains("-Update"))
            {
                do
                {
                    _downloadsService.ProcessData();
                    maxLoops = maxLoops - 1;
                    Console.WriteLine($"Loops gone by {totalLoops - maxLoops}");

                } while (_downloadsExtensionRepository.AvailableToDecode() > 0 && maxLoops > 1);

            }


            if (args == null || args.Count() == 0) { Console.WriteLine("Missing Input Params"); return; }
            var param = args.Where(a => a.IndexOf("Download.") >= 0).Select(a => a.Replace("Download.", "").Replace(" ", "")).ToDictionary(a => a.Split(':', '=')[0], a => a.Split(':', '=')[1]);
            if (param.Keys.Contains("To") && param.Keys.Contains("From"))
            {
                var vals = new
                {
                    from = DateTime.Parse(param.ContainsKey("From") ? param["From"] : ""),
                    to = DateTime.Parse(param.ContainsKey("To") ? param["To"] : ""),
                    contentRegion = param.ContainsKey("ContentRegion") ? param["ContentRegion"] : "",
                    contentTypeId = param.ContainsKey("ContentTypeId") ? param["ContentTypeId"] : "",
                    userRegion = param.ContainsKey("UserRegion") ? param["UserRegion"] : "",
                    userFinancialInstitution = param.ContainsKey("UserFinancialInstitution") ? param["UserFinancialInstitution"] : "",
                    userEmail = param.ContainsKey("UserEmail") ? param["UserEmail"] : "",
                    includeExcludedResults = param.ContainsKey("IncludeExcludedResults") ? bool.Parse(param["IncludeExcludedResults"]) : false
                };
                var results = _downloadsService.GetDownloads(vals.from, vals.to, vals.contentRegion, vals.contentTypeId,
                 vals.userRegion, vals.userFinancialInstitution, vals.userEmail, vals.includeExcludedResults);
                var stringBuilder = new StringBuilder();
                stringBuilder.AppendLine(@"<html><header><style>table, th, td {border: 1px solid black;}</style></header><body><table ><tr><th>contentItemId</th><th>ContentItemRegion</th><th>ContentItemTitle</th><th>ContentType</th><th>Country</th><th>DownloadDate</th><th>FileName</th><th>SitetrackingId</th><th>UserEmail</th><th>UserFinancialInstitution</th><th>UserRegion</th><th>VisitId</th></tr><tbody>");
                foreach (var result in results)
                {
                    stringBuilder.AppendLine($@"<tr><td>{result.ContentItemId}</td><td>{result.ContentItemRegion}</td><td>{result.ContentItemTitle}</td><td>{result.ContentType}</td><td>{ result.Country}</td><td>{result.DownloadDate.ToString()}</td><td>{result.FileName}</td><td>{result.SiteTrackingId.ToString()}</td><td>{result.UserEmail}</td><td>{result.UserFinancialInstitution}</td><td>{result.UserRegion}</td><td>{result.VisitId}</td></tr>");

                }
                stringBuilder.AppendLine(@"<tbody></table></body></html>");
                var body = stringBuilder.ToString();
                File.WriteAllText($@"{AppDomain.CurrentDomain.BaseDirectory}/Downloads.html", body, Encoding.UTF8);
                if (args.Any(a => a.Equals("-Verbose", StringComparison.InvariantCultureIgnoreCase)))
                { Console.WriteLine(body + "/n"); }
            }
            else { if (args.Any(p=>Parameters.Any(kv=>kv.Value.Equals(p)))) { Console.WriteLine("No Downloads"); } }
            clock.Stop();
            elapsedTime = TimeSpan.FromMilliseconds(clock.ElapsedMilliseconds);
        }
        public TimeSpan GetElpasedTime()
        {
            if (elapsedTime == null)
            {
                return new TimeSpan();
            }
            else { return elapsedTime; }
        }

       
    }
}
