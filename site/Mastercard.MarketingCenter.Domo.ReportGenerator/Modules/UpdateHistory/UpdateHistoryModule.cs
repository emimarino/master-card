﻿using Mastercard.MarketingCenter.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Domo.ReportGenerator.Modules
{
    public class UpdateHistoryModule : AbstractModules, IToolModules
    {
        private readonly ContentItemVersionDaoExtension _contentItemVersionDaoExtension;
        private readonly IFieldDataNormalizationContext _fieldDataNormalizationContext;
        private const string _moduleName = "UpdateContentHistory";
        private static IDictionary<string, string> _parameters = new Dictionary<string, string> { { "DryRun", "-Dry-Run" }, { "MinUpdateDate", "From" }, { "MaxUpdateDate", "To" } };
        // private readonly ContentItemDao _contentItemDao;
        public UpdateHistoryModule(ContentItemVersionDaoExtension contentItemVersionDaoExtension, IFieldDataNormalizationContext fieldDataNormalizationContext
            //, ContentItemDao contentItemDao
            ) : base(_moduleName, _parameters)
        {

            _contentItemVersionDaoExtension = contentItemVersionDaoExtension;
            _fieldDataNormalizationContext = fieldDataNormalizationContext;
            // _contentItemDao = contentItemDao;
        }

        public void Execute(string[] args)
        {
            var to = $"{_moduleName}.{_parameters["MaxUpdateDate"]}";
            var from = $"{_moduleName}.{_parameters["MinUpdateDate"]}";
            var isDryRun = args.Contains(_parameters["DryRun"]);
            if (args.Any(a => a.Contains(from)) && args.Any(a => a.Contains(to)))
            {
                var maxDate = DateTime.Parse(args.FirstOrDefault(a => a.StartsWith(to)).Split(':', '=')[1]);
                var minDate = DateTime.Parse(args.FirstOrDefault(a => a.StartsWith(from)).Split(':', '=')[1]);
                var versions = _contentItemVersionDaoExtension.GetAll().Where(civ => civ.DateSaved <= maxDate && civ.DateSaved >= minDate);
                foreach (var version in versions)
                {
                    var fieldDefinitions = _contentItemVersionDaoExtension.GetContentTypeFields(version.ContentItemId);
                    var oldData = version.SerializationData;
                    var newData = JsonConvert.SerializeObject(_fieldDataNormalizationContext.Apply(JsonConvert.DeserializeObject<Dictionary<string, object>>(version.SerializationData), fieldDefinitions));
                    version.SerializationData = newData;
                    if (isDryRun)
                    {
                        if (oldData != newData)
                        {
                            Console.WriteLine(JsonConvert.SerializeObject(version));
                        }
                    }
                    else
                    {
                        _contentItemVersionDaoExtension.Update(version);
                    }
                }
                if (!isDryRun)
                {
                    _contentItemVersionDaoExtension.Commit();
                }
            }
        }
    }
}