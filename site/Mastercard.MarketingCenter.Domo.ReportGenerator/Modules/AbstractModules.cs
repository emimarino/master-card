﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastercard.MarketingCenter.Domo.ReportGenerator
{
   public abstract class AbstractModules
    {
        private  IDictionary<string, string> _parameters = new Dictionary<string, string>();
        private  string _moduleName = string.Empty;
        public AbstractModules(string moduleName, IDictionary<string, string> parameters) {
            _moduleName = moduleName;
            _parameters = parameters;
        }
        public virtual IDictionary<string, string> GetParameters() {
            return _parameters.ToDictionary(kv => kv.Key, kv => (kv.Value.StartsWith("-") ? string.Empty : $"{_moduleName}.") + kv.Value);
        }
        
    }
}
