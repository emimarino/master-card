﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using ServiceStack.OrmLite.Dapper;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Domo.ReportGenerator
{
    public class ContentItemVersionDaoExtension : ContentItemVersionDao
    {
        private readonly MarketingCenterDbContext _ctx;
        public ContentItemVersionDaoExtension(MarketingCenterDbContext context) : base(context)
        {
            _ctx = context;
        }

        public ContentItemVersion Update(ContentItemVersion itemToUpdate)
        {
            DbSet<ContentItemVersion> set = _ctx.Set<ContentItemVersion>();
            set.Attach(itemToUpdate);
            _ctx.Entry(itemToUpdate).State = EntityState.Modified;
            return itemToUpdate;
        }

        public void Commit()
        {
            _ctx.SaveChanges();
        }

        public IEnumerable<FieldDefinition> GetContentTypeFields(string contentItemId)
        {
            return _ctx.ContentItems.FirstOrDefault(ci => ci.ContentItemId.Equals(contentItemId)).ContentType.ContentTypeFieldDefinitions.Select(ctf => ctf.FieldDefinition);
        }

        public string GetContentTypeId(string contentItemId)
        {
            return _ctx.Database.Connection.ExecuteScalar<string>("SELECT ContentTypeId FROM ContentItem WHERE ContentItemId = @contentItemId", new { contentItemId });
        }
    }
}