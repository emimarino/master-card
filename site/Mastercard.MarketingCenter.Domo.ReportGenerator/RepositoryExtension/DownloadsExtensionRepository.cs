﻿using Mastercard.MarketingCenter.Domo.Domain;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using System.Linq;

namespace Mastercard.MarketingCenter.Domo.ReportGenerator
{
    public class DownloadsExtensionRepository
    {
        private readonly IDbConnectionFactory _dbFactory;
        public DownloadsExtensionRepository(IDbConnectionFactory dbFactory)
        {
            _dbFactory = dbFactory;
        }

        public long AvailableToDecode()
        {
            using (var db = _dbFactory.OpenDbConnection())
            {
                return db.Count<DataToBeDecodedAux>();
            }
        }
    }
}