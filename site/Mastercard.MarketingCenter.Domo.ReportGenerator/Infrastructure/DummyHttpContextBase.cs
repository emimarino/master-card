﻿using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace Mastercard.MarketingCenter.Domo.ReportGenerator { 
   public class DummyHttpContextBase: HttpContextBase
    {
        private readonly IPrincipal _principal;

        public DummyHttpContextBase()
        {
            this._principal = new RolePrincipal(new GenericIdentity("Generic identity"));
        }

        public override IPrincipal User
        {
            get
            {
                return this._principal;
            }
        }
    }
}