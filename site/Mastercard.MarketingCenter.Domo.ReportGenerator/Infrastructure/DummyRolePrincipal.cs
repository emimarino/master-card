﻿using System.Security.Principal;


namespace Mastercard.MarketingCenter.Domo.ReportGenerator
{
    public class DummyRolePrincipal: IPrincipal
    {
        public DummyRolePrincipal()
        {

        }

        public IIdentity Identity
        {
            get
            {
                return new GenericIdentity("username");
            }
        }

        public bool IsInRole(string role)
        {
            return true;
        }
    }
}