﻿using Autofac;
using Mastercard.MarketingCenter.Cms.Services.Modules;
using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Domo.ReportGenerator.Report;
using Mastercard.MarketingCenter.Domo.Repository;
using Mastercard.MarketingCenter.Domo.Services;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Modules;
using ServiceStack.OrmLite;
using Slam.Cms.Common;
using Slam.Cms.Data;
using Slam.Cms.Common.Interfaces;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Security.Principal;
using System.Web;

namespace Mastercard.MarketingCenter.Domo.ReportGenerator
{
    public static class AutofacConfig
    {
        private static bool allowInProd = false;

        public static void AllowInProd() { allowInProd = true; }

        public static IContainer Register(string[] args)
        {
            var builder = new ContainerBuilder();

            var connectionString = ConfigurationManager.ConnectionStrings["MasterCardMarketingCenter"].ConnectionString;
            var productionSeverName = ConfigurationManager.AppSettings["ServerName.Production"];
            if (!allowInProd && connectionString.Contains(productionSeverName))
            {
                throw new Exception("You cannot run this on Production it's dangerous.");
            }

            builder.Register(c =>
            {
                var connection = new SqlConnection(connectionString);
                connection.Open();
                return connection;
            }).As<IDbConnection>().InstancePerLifetimeScope();

            builder.RegisterType<HttpContextCachingService>().As<ICachingService>().InstancePerLifetimeScope();

            builder.Register(c =>
            {
                return new SlamContext(c.Resolve<IPreviewMode>())
                    .SetKnownContentTypes(typeof(AssetDTO).GetAssembly())
                    .SetCachingService(c.Resolve<ICachingService>())
                    .SetConnectionFactory(c.Resolve<Func<IDbConnection>>());
            }).InstancePerLifetimeScope();
            builder.RegisterModule(new ContextModule(false));

            builder.RegisterModule<CmsModule>();
            builder.RegisterType<DummyHttpContextBase>().As<HttpContextBase>();
            builder.RegisterType<StandaloneCookieService>().As<ICookieService>();
            builder.RegisterType<StandaloneSessionService>().As<ISessionService>();
            builder.RegisterType<StandaloneMembershipProvider>().As<IMembershipProvider>();
            builder.Register<IPrincipal>(c =>
            {
                return new DummyRolePrincipal();
            }).InstancePerLifetimeScope();

            builder.RegisterModule(new ServicesModule(false, false));
            builder.RegisterType<ContentItemVersionDaoExtension>();
            builder.RegisterType<MarketingCenterDbContext>().InstancePerLifetimeScope();
            builder.RegisterType<ReportRepository>();
            builder.RegisterType<UserSubscriptionRepository>();

            builder.RegisterType<DownloadsExtensionRepository>();
            builder.RegisterType<ContentItemVersionDaoExtension>();

            builder.RegisterType<DownloadsService>().As<IDownloadsService>();

            builder.RegisterModule<DomoPersistanceModule>();

            builder.RegisterType<ActivityService>().As<IActivityService>();
            builder.RegisterType<ReportRepository>();
            var dataAccess = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(dataAccess)
                   .Where(t => typeof(IReportGenerator).IsAssignableFrom(t))
                   .As<IReportGenerator>();

            builder.RegisterAssemblyTypes(dataAccess)
                   .Where(t => typeof(IToolModules).IsAssignableFrom(t) && t.Name.EndsWith("Module"))
                   .As<IToolModules>();

            builder.RegisterAssemblyTypes(typeof(CmsModule).Assembly)
                   .Where(t => typeof(IFieldDataRetrieval).IsAssignableFrom(t))
                   .As<IFieldDataRetrieval>();

            builder.RegisterType<FieldDataNormalizationContext>().As<IFieldDataNormalizationContext>();

            return builder.Build();
        }
    }
}