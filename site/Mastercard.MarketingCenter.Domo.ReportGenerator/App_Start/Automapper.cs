﻿using AutoMapper;
using Mastercard.MarketingCenter.Data.Entities.Reporting;
using Mastercard.MarketingCenter.Domo.Domain;
using Mastercard.MarketingCenter.Domo.DTO;

namespace Mastercard.MarketingCenter.Domo.ReportGenerator
{
    public static class Automapper
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<DomoActivityApi, DomoActivityItemDto>()
                .ForMember(x => x.UserRegion, opt => opt.MapFrom(source => source.UserRegion.Trim().ToLower()))
                .ForMember(x => x.ContentRegionName, opt => opt.MapFrom(source => source.ContentRegionName.Trim().ToLower()));

                cfg.CreateMap<UserRegistrationReportResultItem, UserRegistrationResultItemDto>()
                .ForMember(x => x.AudienceSegments, opt => opt.MapFrom(source => source.AudienceSegments))
                .ForMember(x => x.Country, opt => opt.MapFrom(source => source.Country))
                .ForMember(x => x.Email, opt => opt.MapFrom(source => source.RealEmail))
                .ForMember(x => x.EmailConsents, opt => opt.MapFrom(source => source.EmailConsent))
                .ForMember(x => x.FI, opt => opt.MapFrom(source => source.FI))
                .ForMember(x => x.FirstName, opt => opt.MapFrom(source => source.RealFirstName))
                .ForMember(x => x.LastLoginDate, opt => opt.MapFrom(source => source.LastLoginDate))
                .ForMember(x => x.LastName, opt => opt.MapFrom(source => source.RealLastName))
                .ForMember(x => x.Processor, opt => opt.MapFrom(source => source.Processor))
                .ForMember(x => x.RegistrationDate, opt => opt.MapFrom(source => source.RegistrationDate));

                cfg.CreateMap<DomoDownloadsApi, DomoDownloadItemDto>()
                .ForMember(x => x.Country, opt => opt.MapFrom(source => source.UserCountry))
                .ForMember(x => x.ContentType, opt => opt.MapFrom(source => source.ContentTypeTitle))
                ;
            });
        }
    }
}