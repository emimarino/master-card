﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Mastercard.MarketingCenter.Domo.ReportGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var clock = Stopwatch.StartNew();
            if (args.Any(a => a.Equals("-AllowInProd", StringComparison.CurrentCultureIgnoreCase)))
            {
                AutofacConfig.AllowInProd();
            }
            var container = AutofacConfig.Register(args);
            Automapper.Configure();
            Console.WriteLine("Begin");
            using (var scope = container.BeginLifetimeScope())
            {
                var modules = scope.Resolve<IEnumerable<IToolModules>>();
                foreach (var module in modules)
                {
                    if (args.Contains("-Help"))
                    {
                        Console.Write($"{module.GetType().Name.Replace("Module", "")} \n{module.GetParameters().Aggregate(new StringBuilder(), (previous, kv) => previous.Append($"\n\t{kv.Key} : {kv.Value}"))} --\n");
                    }
                    module.Execute(args);
                }
            }

            clock.Stop();
            Console.WriteLine($"\nEnd Total time Elapsed: {TimeSpan.FromMilliseconds(clock.ElapsedMilliseconds)} \n");
        }
    }
}