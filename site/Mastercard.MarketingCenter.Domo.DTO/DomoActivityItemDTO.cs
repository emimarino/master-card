﻿using System;

namespace Mastercard.MarketingCenter.Domo.DTO
{
    public class DomoActivityItemDto
    {
        public int Id { get; set; }
        public int SiteTrackingId { get; set; }
        public int VisitId { get; set; }
        public int UserId { get; set; }
        public string UserLastName { get; set; }
        public string UserFirstName { get; set; }
        public string UserEmail { get; set; }
        public string UserCountry { get; set; }
        public string UserIssuerName { get; set; }
        public string UserProcessorName { get; set; }
        public string UserRegion { get; set; }
        public DateTime When { get; set; }
        public string From { get; set; }
        public string How { get; set; }
        public string ContentType { get; set; }
        public string ContentId { get; set; }
        public string ContentTitle { get; set; }
        public string ContentRegionName { get; set; }
        public string ValueExcluded { get; set; }
    }
}