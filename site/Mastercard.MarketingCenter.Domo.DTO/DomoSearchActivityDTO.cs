﻿using System;

namespace Mastercard.MarketingCenter.Domo.DTO
{
    public class DomoSearchActivityDto
    {
        public int SearchActivityID { get; set; }
        public int UserId { get; set; }
        public string UserFullName { get; set; }
        public string UserEmail { get; set; }
        public string UserTitle { get; set; }
        public string UserRegion { get; set; }
        public string UserCountry { get; set; }
        public DateTime SearchDateTime { get; set; }
        public string SearchTerm { get; set; }
        public string UrlVisited { get; set; }
        public string RegionalSite { get; set; }
        public string UserFinancialInstitution { get; set; }
        public string ValueExcluded { get; set; }
    }
}