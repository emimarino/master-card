﻿using System;

namespace Mastercard.MarketingCenter.Domo.DTO
{
    public class UserRegistrationResultItemDto
    {
        public int UserId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime LastLoginDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string FI { get; set; }
        public string AudienceSegments { get; set; }
        public string Processor { get; set; }
        public string EmailConsents { get; set; }
        public string RegionId { get; set; }
        public string ValueExcluded { get; set; }
    }
}