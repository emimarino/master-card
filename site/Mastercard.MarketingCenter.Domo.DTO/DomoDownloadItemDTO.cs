﻿using System;

namespace Mastercard.MarketingCenter.Domo.DTO
{
    public class DomoDownloadItemDto
    {
        public string ContentItemId { get; set; }
        public string ContentItemTitle { get; set; }
        public string ContentType { get; set; }
        public string ContentItemRegion { get; set; }
        public DateTime DownloadDate { get; set; }
        public string FileName { get; set; }
        public string SiteTrackingId { get; set; }
        public string VisitId { get; set; }
        public int UserId { get; set; }
        public string UserEmail { get; set; }
        public string UserFinancialInstitution { get; set; }
        public string Country { get; set; }
        public string UserRegion { get; set; }
        public string ValueExcluded { get; set; }
    }
}