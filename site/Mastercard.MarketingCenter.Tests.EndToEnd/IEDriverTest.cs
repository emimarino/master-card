using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.IE;
using System.Configuration;

namespace Mastercard.MarketingCenter.Tests.EndToEnd
{
    [TestClass]
    public class IEDriverTest
    {
        private InternetExplorerDriver _driver;

        [TestInitialize]
        public void EdgeDriverInitialize()
        {
            var options = new InternetExplorerOptions
            {
                PageLoadStrategy = PageLoadStrategy.Normal,
                IgnoreZoomLevel = true
            };

            // Please ensure that IE Driver file is available to be reached in the %PATH%
            // https://www.seleniumhq.org/download/ at "Internet Explorer Driver Server"
            _driver = new InternetExplorerDriver(options);
        }

        [TestMethod]
        public void Verify_Page_Title()
        {
            _driver.Url = ConfigurationManager.AppSettings.Get("Host");
            Assert.AreEqual("Mastercard Marketing Center", _driver.Title);
        }

        [TestCleanup]
        public void EdgeDriverCleanup()
        {
            _driver.Quit();
        }
    }
}
