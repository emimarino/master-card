﻿using Autofac;
using Autofac.Core;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Jobs;
using NUnit.Framework;
using Slam.Cms.Admin.Data;
using System;
using System.Collections;
using System.Data.Entity;
using System.Data.Linq;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Mastercard.MarketingCenter.Tests.Integration
{
    [TestFixture]
    public class BasicFunctionalityTests
    {
        private const int DbContextEntitiesToReadCount = 5;

        [Test]
        public void Can_Resolve_Most_of_Autofac_Registrations()
        {
            using (var scope = TestContext.Container.BeginLifetimeScope())
            {
                var registrations = scope.ComponentRegistry.Registrations;
                foreach (var reg in registrations)
                {
                    foreach (var service in reg.Services)
                    {
                        if (!(service is TypedService))
                        {
                            throw new Exception("not a typed service");
                        }

                        var s = service as TypedService;

                        var instance = scope.Resolve(s.ServiceType);
                        Assert.IsNotNull(instance, "An instance for {0} is null.", s.ServiceType);
                    }
                }
            }
        }

        [Test]
        public void Can_Connect_To_DB()
        {
            using (var scope = TestContext.Container.BeginLifetimeScope())
            {
                var regionService = scope.Resolve<IRegionService>();
                var regions = regionService.GetAll();
                Assert.GreaterOrEqual(regions.Count(), 0);
            }
        }

        [Test]
        public async Task Can_Read_All_Entities_From_DB_via_MarketingCenterDbContext()
        {
            await this.Can_Read_All_Entities_From_DbContext<MarketingCenterDbContext>();
        }

        [Test]
        public async Task Can_Read_All_Entities_From_DB_via_MastercardCmsDbContext()
        {
            await this.Can_Read_All_Entities_From_DbContext<MarketingCenterDbContext>();
        }

        [Test]
        public async Task Can_Read_All_Entities_From_DB_via_SlamCmsAdminDbContext()
        {
            await this.Can_Read_All_Entities_From_DbContext<SlamCmsAdminDbContext>();
        }

        [Test]
        public async Task Can_Read_All_Entities_From_DB_via_MasterCardPortalDataContext()
        {
            await this.Can_Read_All_Entities_From_DataContext<MasterCardPortalDataContext>();
        }

        [Test]
        public async Task Can_Run_Job_MailDispatcher()
        {
            await this.Can_Run_Job<MailDispatcherJob>(j =>
            {
                j.Execute();

                return Task.FromResult(0);
            });
        }

        [Test]
        public async Task Can_Run_Job_ExpireContentJob()
        {
            await this.Can_Run_Job<ExpireContentJob>(j =>
            {
                j.Execute();

                return Task.FromResult(0);
            });
        }

        [Test]
        public async Task Can_Run_Job_GdprUserDataExpirationJobb()
        {
            await this.Can_Run_Job<GdprUserDataExpirationJob>(j =>
            {
                j.Execute(null);

                return Task.FromResult(0);
            });
        }

        [Test]
        public async Task Can_Run_Job_DeletePublishedTriggerMarketingsJob()
        {
            await this.Can_Run_Job<DeletePublishedTriggerMarketingsJob>(j =>
            {
                j.Execute();

                return Task.FromResult(0);
            });
        }

        [Test]
        public async Task Can_Run_Job_ContentLinkValidatorJob()
        {
            await this.Can_Run_Job<ContentLinkValidatorJob>(j =>
            {
                j.Execute();

                return Task.FromResult(0);
            });
        }

        [Test]
        public async Task Can_Run_Job_MostPopularJob()
        {
            await this.Can_Run_Job<MostPopularJob>(j =>
            {
                j.RefreshMostPopularData();

                return Task.FromResult(0);
            });
        }

        [Test]
        public async Task Can_Run_Job_ExpiredContentNotificationJob()
        {
            await this.Can_Run_Job<ExpiredContentNotificationJob>(j =>
            {
                j.Execute();

                return Task.FromResult(0);
            });
        }

        private async Task Can_Run_Job<TJob>(Func<TJob, Task> execution)
        {
            using (var scope = TestContext.Container.BeginLifetimeScope())
            {
                var job = scope.Resolve<TJob>();
                await execution(job);
            }
        }

        private async Task Can_Read_All_Entities_From_DbContext<TDbContext>() where TDbContext : DbContext
        {
            var queryableType = typeof(QueryableExtensions);
            var takeMethodInfo = queryableType.GetMethod("Take");
            Expression<Func<int>> takeCount = () => DbContextEntitiesToReadCount;

            using (var scope = TestContext.Container.BeginLifetimeScope())
            {
                var dbContext = scope.Resolve<TDbContext>();
                foreach (var dbSetPropertyInfo in dbContext.GetDbSetProperties())
                {
                    try
                    {
                        // reading all entities from a DB Set to check if it fails
                        var dbSet = dbSetPropertyInfo.GetValue(dbContext);
                        if (dbSet != null)
                        {
                            var entityType = dbSetPropertyInfo.PropertyType.GenericTypeArguments.FirstOrDefault();
                            var genericTakeMethodInfo = takeMethodInfo.MakeGenericMethod(entityType);

                            var query = genericTakeMethodInfo.Invoke(dbSet, new object[] { dbSet, takeCount }) as IQueryable;
                            var entities = await query.ToListAsync();
                        }
                        else
                        {
                            Assert.Fail("DbSet {0} is null and will not be able to be read.", dbSet);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("DbSet {0} could not be read.", dbSetPropertyInfo), ex);
                    }
                }
            }
        }

        private async Task Can_Read_All_Entities_From_DataContext<TDataContext>() where TDataContext : DataContext
        {
            var queryableType = typeof(Queryable);
            var enumerableType = typeof(Enumerable);
            var takeMethodInfo = queryableType.GetMethod("Take");
            var toListMethodInfo = enumerableType.GetMethod("ToList");

            using (var scope = TestContext.Container.BeginLifetimeScope())
            {
                var dataContext = scope.Resolve<TDataContext>();

                foreach (var metaTable in dataContext.Mapping.GetTables())
                {
                    try
                    {
                        // reading all entities from a DB Set to check if it fails
                        var entityType = metaTable.RowType.Type;
                        var tableProperty = dataContext.GetTable(entityType);
                        if (tableProperty != null)
                        {
                            var genericTakeMethodInfo = takeMethodInfo.MakeGenericMethod(entityType);
                            var query = genericTakeMethodInfo.Invoke(tableProperty, new object[] { tableProperty, DbContextEntitiesToReadCount }) as IEnumerable;

                            var genericToListMethodInfo = toListMethodInfo.MakeGenericMethod(entityType);
                            var entities = await Task.FromResult(genericToListMethodInfo.Invoke(query, new object[] { query }) as IEnumerable);
                        }
                        else
                        {
                            Assert.Fail("ITable {0} is null and will not be able to be read.", metaTable.TableName);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(string.Format("ITable {0} could not be read.", metaTable.TableName), ex);
                    }
                }
            }
        }
    }
}