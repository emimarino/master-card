﻿using Mastercard.MarketingCenter.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Mastercard.MarketingCenter.Tests.Integration
{
    public class DummyRolePrincipal: IPrincipal
    {
        public DummyRolePrincipal()
        {

        }

        public IIdentity Identity
        {
            get
            {
                return new GenericIdentity("username");
            }
        }

        public bool IsInRole(string role)
        {
            return true;
        }
    }
}