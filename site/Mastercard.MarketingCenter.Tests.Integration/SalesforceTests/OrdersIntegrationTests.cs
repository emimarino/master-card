﻿using Autofac;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Tests.Integration.SalesforceTests;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Tests.Integration
{
    [TestFixture]
    public class OrdersIntegrationTests
    {


        private readonly MarketingCenterDbContext _context;
        private readonly IOrderRepository _userReposotory;
        private IQueryable<OrderDownload> _dataSet;
        private ParameterTestRunner<IOrderRepository> _testRunner;
        private readonly ILifetimeScope _lifetimeScope;

        public OrdersIntegrationTests()
        {
            _lifetimeScope = TestContext.Container.BeginLifetimeScope();

            _context = _lifetimeScope.Resolve<MarketingCenterDbContext>();
            _userReposotory = _lifetimeScope.Resolve<IOrderRepository>();
            _dataSet = _userReposotory.GetOrderDownloads().Take(10);
            _testRunner = new ParameterTestRunner<IOrderRepository>(_userReposotory, typeof(IOrderRepository).GetMethod("GetOrderDownloads"));

        }


        [Test]
        public void Can_Filter_Proper_Data()
        {

            foreach (var data in _dataSet.ToList().Select(ds =>
                    new List<KeyValuePair<string, int>>{
                        new KeyValuePair<string, int> ( "itemQuantityMin", ds.ItemQuantity-1 ),
                        new KeyValuePair<string, int> ( "itemQuantityMax", ds.ItemQuantity+1) }))
            {
                _testRunner.RunTestOn(data.ToDictionary(k => k.Key, v => v.Value), (dict, res) =>
                {
                    var start = dict["itemQuantityMin"];
                    var end = dict["itemQuantityMax"];

                    return ((IQueryable<OrderDownload>)res).All(r => start < r.ItemQuantity && end > r.ItemQuantity);

                });

            }


            foreach (var data in _dataSet.ToList().Select(ds =>
            new List<KeyValuePair<string, int>>{
                        new KeyValuePair<string, int> ( "estimatedDistributionMin", ds.EstimatedDistribution-1 ),
                        new KeyValuePair<string, int> ( "estimatedDistributionMax", ds.EstimatedDistribution+1) }))
            {
                _testRunner.RunTestOn(data.ToDictionary(k => k.Key, v => v.Value), (dict, res) =>
                {
                    var start = dict["estimatedDistributionMin"];
                    var end = dict["estimatedDistributionMax"];

                    return ((IQueryable<OrderDownload>)res).All(r => start < r.ItemQuantity && end > r.ItemQuantity);

                });

            }


            foreach (var stringParam in typeof(IOrderRepository).GetMethod("GetOrderDownloads").GetParameters().Where(t => t.ParameterType == typeof(string)))
            {

                foreach (var data in _dataSet.ToList().Where(ds => !string.IsNullOrEmpty((string)typeof(OrderDownload).GetProperties().FirstOrDefault(p => p.Name == stringParam.Name)?.GetValue(ds))).Select(ds =>
                       new List<KeyValuePair<string, string>> { new KeyValuePair<string, string>(stringParam.Name, (string)typeof(OrderDownload).GetProperties().FirstOrDefault(p => p.Name == stringParam.Name)?.GetValue(ds)) }))
                {
                    _testRunner.RunTestOn(data.ToDictionary(k => k.Key, v => v.Value), (dict, res) =>
                    {
                        var str = dict[stringParam.Name];
                        if (str != null)
                        {
                            return ((IEnumerable<OrderDownload>)res).All(r => str == (string)typeof(OrderDownload).GetProperties().FirstOrDefault(p => p.Name == stringParam.Name).GetValue(r));
                        }
                        return true;
                    });

                }

            }

            var exceptions = _testRunner.GetExceptions();
            if (exceptions.Count() > 0)
            { throw new AggregateException(); }

        }
    }
}
