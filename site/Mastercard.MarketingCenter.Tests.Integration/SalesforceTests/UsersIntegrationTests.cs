﻿using Autofac;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Tests.Integration.SalesforceTests;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Tests.Integration
{
    [TestFixture]
    public class UsersIntegrationTests
    {
        private readonly MarketingCenterDbContext _context;
        private readonly IUserRepository _userReposotory;
        private IQueryable<UserRegistration> _dataSet;
        private ParameterTestRunner<IUserRepository> _testRunner;
        private readonly ILifetimeScope _lifetimeScope;

        public UsersIntegrationTests()
        {
            _lifetimeScope = TestContext.Container.BeginLifetimeScope();

            _context = _lifetimeScope.Resolve<MarketingCenterDbContext>();
            _userReposotory = _lifetimeScope.Resolve<IUserRepository>();
            _dataSet = _userReposotory.GetUserRegistrations().Take(10);
            _testRunner = new ParameterTestRunner<IUserRepository>(_userReposotory, typeof(IUserRepository).GetMethod("GetUserRegistrations"));
        }


        [Test]
        public void Can_Filter_Proper_Data()
        {

            foreach (var data in _dataSet.ToList().Select(ds =>
                    new List<KeyValuePair<string, DateTime>>{
                        new KeyValuePair<string, DateTime> ( "lastLoginStart",( ds.LastLogin.AddDays(-1.0)) ),
                        new KeyValuePair<string, DateTime> ( "lastLoginEnd", (ds.LastLogin.AddDays(1.0)) ) }))
            {
                _testRunner.RunTestOn(data.ToDictionary(k => k.Key, v => v.Value), (dict, res) =>
                {
                    var start = dict["lastLoginStart"];
                    var end = dict["lastLoginEnd"];

                    if (start != null && end != null)
                    {
                        var val = ((IQueryable<UserRegistration>)res).All(r => start <= r.LastLogin && end >= r.LastLogin);
                        return val;
                    }
                    return true;
                });

            }



            foreach (var data in _dataSet.ToList().Select(ds =>
                    new List<KeyValuePair<string, DateTime>>{
                        new KeyValuePair<string, DateTime> ( "registeredDateStart", (ds.RegisteredDate.AddDays(-1.0)) ),
                        new KeyValuePair<string, DateTime> ( "registeredDateEnd",( ds.RegisteredDate.AddDays(1.0)) ) }))
            {
                _testRunner.RunTestOn(data.ToDictionary(k => k.Key, v => v.Value), (dict, res) =>
                {
                    var start = dict["registeredDateStart"];
                    var end = dict["registeredDateEnd"];

                    if (start != null && end != null)
                    {
                        return ((IQueryable<UserRegistration>)res).All(r => start <= r.RegisteredDate && end >= r.RegisteredDate);
                    }
                    return true;
                });

            }

            foreach (var stringParam in typeof(IUserRepository).GetMethod("GetUserRegistrations").GetParameters().Where(t => t.ParameterType == typeof(string)))
            {

                foreach (var data in _dataSet.ToList().Where(ds => !string.IsNullOrEmpty((string)typeof(UserRegistration).GetProperties().FirstOrDefault(p => p.Name == stringParam.Name)?.GetValue(ds))).Select(ds =>
                       new List<KeyValuePair<string, string>> { new KeyValuePair<string, string>(stringParam.Name, (string)typeof(UserRegistration).GetProperties().FirstOrDefault(p => p.Name == stringParam.Name)?.GetValue(ds)) }))
                {
                    _testRunner.RunTestOn(data.ToDictionary(k => k.Key, v => v.Value), (dict, res) =>
                    {
                        var str = dict[stringParam.Name];
                        if (str != null)
                        {
                            return ((IEnumerable<UserRegistration>)res).All(r => str == (string)typeof(UserRegistration).GetProperties().FirstOrDefault(p => p.Name == stringParam.Name).GetValue(r));
                        }
                        return true;
                    });

                }

            }
            var exceptions = _testRunner.GetExceptions();
            if (exceptions.Count() > 0)
            { throw new AggregateException(); }


        }

    }
}
