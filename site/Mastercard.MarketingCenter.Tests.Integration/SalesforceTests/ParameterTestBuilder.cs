﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NUnit.Framework;

namespace Mastercard.MarketingCenter.Tests.Integration.SalesforceTests
{
    /// <summary>
    /// Runs Tests on specific method that is instance of the K method
    /// </summary>
    /// <typeparam name="K">The instance where the method resides</typeparam>
    public  class ParameterTestRunner<K>
    {

        private readonly K _instance;
        private readonly MethodBase _method;
        private List<Exception> exceptions = new List<Exception>();
        public ParameterTestRunner(K instance, MethodBase method ) {
            _instance = instance;
            _method = method;
           
        }

        public IEnumerable<Exception> GetExceptions() {
            return exceptions; }
        /// <summary>
        /// Runs the specified test
        /// </summary>
        /// <typeparam name="T">Type of the parametrs to pass</typeparam>
        /// <param name="values">the value and name of the paramters</param>
        /// <param name="retrieval">The Method used to test weather the test failed if the resilt is false it will return false, the object is the expceted result, the IDictionary is the other parameter in this method</param>
        public void RunTestOn<T>(IDictionary<string, T> values, Func<IDictionary<string, T>,object , bool> retrieval) {

            var result = _method.Invoke(_instance, GetParametersWithValues(values));

                if (!retrieval(values,result ) )
            {
                exceptions.Add(new AssertionException($"Exception involving {string.Join(";", values.Select(kv=>kv.Key+","+kv.Value))}"));
            }
        }

        private object[] GetParametersWithValues<T>(IDictionary<string, T> values)
        {
            var parameterList = _method.GetParameters().ToArray();
            var parameters = new object[parameterList.Length];
            for (var ind= 0;ind < parameterList.Length;ind++) {
                var currKey = parameterList[ind].Name;
                if (values.Any(kv => kv.Key.Equals(currKey, StringComparison.InvariantCultureIgnoreCase)))
                {
                    parameters[ind] = values[currKey];
                }
                else
                {
                    parameters[ind] = Type.Missing;
                }
            }
            return parameters;

        }

    }
}
