﻿using Autofac;

namespace Mastercard.MarketingCenter.Tests.Integration
{
    public class TestContext
    {
        private static IContainer _container=null;

        public static IContainer Container
        {
            get
            {
                return _container;
            }
            set
            {
                _container = value;
            }
        }
    }
}