﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastercard.MarketingCenter.Tests.Integration
{
    [TestFixture]
    public class CalendarTests : RolledBackDataContextTests<MarketingCenterDbContext>
    {


        private bool JointQueryContainsTable<TableType>()
        {
            var sqlQuery = Context.AbstractContentItemForCalendars.Sql;
            return sqlQuery.Contains($"[{GetTableName<TableType>()}]");
        }

        [Test]
        public void Can_Include_Asset()
        {
            Assert.IsTrue(JointQueryContainsTable<Asset>());
        }
        [Test]
        public void Can_Include_Downloadable_Asset()
        {
            Assert.IsTrue(JointQueryContainsTable<DownloadableAsset>());
        }
        [Test]
        public void Can_Include_Programs()
        {
            Assert.IsTrue(JointQueryContainsTable<Program>());
        }


        public void CanIncludeCampaignEventToCotent<TContentType>() where TContentType : ContentItemForCalendar
        {
            var testEvent = Context.ContentItemCampaignEvents.First();
            var testContent = Context.Set<TContentType>().First();
            if (testEvent != null && testContent != null)
            {

                testEvent.ContentItemForCalendar = testContent;

            }
        }


        [Test]
        public void Can_Include_CampaignEvent_To_Asset()
        {
            Assert.DoesNotThrow(
                () => { CanIncludeCampaignEventToCotent<Asset>(); });

        }
        [Test]
        public void Can_Include_CampaignEvent_To_Downloadable_Asset()
        {
            Assert.DoesNotThrow(
                   () => { CanIncludeCampaignEventToCotent<DownloadableAsset>(); });
        }
        [Test]
        public void Can_Include_CampaignEvent_To_Programs()
        {
            Assert.DoesNotThrow(
                   () => { CanIncludeCampaignEventToCotent<Program>(); });
        }
        [Test]
        public void Can_Delete_Campaign_Events_In_All_Content_Types()
        {
            var calendarEvent = Context.ContentItemCampaignEvents.First(c => c.ContentItemForCalendar != null);
            if (calendarEvent != null)
            {
                Assert.DoesNotThrow(
                    () => { calendarEvent.ContentItemForCalendar = null; });
            }
        }

    }
}
