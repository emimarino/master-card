using NUnit.Framework;
using System;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Transactions;

namespace Mastercard.MarketingCenter.Tests.Integration
{
    /// <summary>
    /// The base class will allow LINQ to SQL integration testing with automatic rollbacks after each test
    /// </summary>
    /// <typeparam name="TContext">The type of data context to use</typeparam>
    public abstract class RolledBackDataContextTests<TContext> where TContext : DbContext
    {
        private TContext _context;
        private IDisposable _transaction;

        [SetUp]
        public void InitDataContext()
        {
            _transaction = new TransactionScope();
            _context = Activator.CreateInstance<TContext>();
        }
        /// <summary>
        /// Get the Table Name from Metadata
        /// </summary>
        /// <typeparam name="TableType">The POCO object Representing the table</typeparam>
        /// <returns></returns>
        protected string GetTableName<TableType>()
        {
            var typeName = typeof(TableType).Name;
            ObjectContext objectContext = ((IObjectContextAdapter)Context).ObjectContext;
            var eData = objectContext.MetadataWorkspace
                  .GetItemCollection(DataSpace.SSpace)
                  .GetItems<EntityContainer>()
                  .SelectMany(c => c.BaseEntitySets
                                  .Where(e =>
                                  e.Name == typeName
                                    || e.Name == typeName
                                  ))
                  .FirstOrDefault();
            return eData.Table;
        }

        /// <summary>
        /// Provides access to the internal data context. Any changes made will be automatically rolled back
        /// </summary>
        protected TContext Context
        {
            get
            {
                if (_context == null)
                {
                    InitDataContext();
                }

                return _context;
            }
        }

        [TearDown]
        public void TestCleanup()
        {
            _context.Dispose();
            _transaction.Dispose();
            _context = null;
        }
    }
}