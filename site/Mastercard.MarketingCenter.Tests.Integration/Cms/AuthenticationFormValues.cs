﻿namespace Mastercard.MarketingCenter.Tests.Integration.Cms
{
    internal class AuthenticationFormValues
    {
        public string EventValidation { get; set; }
        public string ViewState { get; set; }
        public string ViewStateGenerator { get; set; }
    }
}