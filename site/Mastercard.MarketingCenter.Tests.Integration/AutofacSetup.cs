﻿using Autofac;
using Mastercard.MarketingCenter.Cms.Services.Modules;
using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Modules;
using Mastercard.MarketingCenter.Domo.Repository;
using Mastercard.MarketingCenter.Domo.Services;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Modules;
using Mastercard.MarketingCenter.Web.Core.Modules;
using NUnit.Framework;
using System.Security.Principal;
using System.Web;

namespace Mastercard.MarketingCenter.Tests.Integration
{
    [SetUpFixture]
    public class AutofacSetup
    {
        private IContainer _container;

        [OneTimeSetUp]
        public void GlobalSetup()
        {
            // Do login here.
            var builder = new ContainerBuilder();

            builder.RegisterModule(new ServicesModule(false, false));
            builder.RegisterModule(new ContextModule(false));

            builder.RegisterModule<DomoPersistanceModule>();
            builder.RegisterModule<DomoServiceModule>();

            builder.RegisterModule<JobsModule>();
            builder.RegisterModule<WebCoreModule>();
            builder.RegisterModule<CmsModule>();

            builder.RegisterType<MarketingCenterDbContext>().InstancePerLifetimeScope();

            // This is a Dummy HttpContext to emulate real one. 
            // Ideally Mastercard.MarketingCenter.Services should have an implementation for "Request", "Response" and "Session" for standalone application instead of using HttpContext
            builder.RegisterType<DummyHttpContextBase>().As<HttpContextBase>();
            builder.RegisterType<StandaloneCookieService>().As<ICookieService>();
            builder.RegisterType<StandaloneSessionService>().As<ISessionService>();
            builder.RegisterType<StandaloneMembershipProvider>().As<IMembershipProvider>();
            builder.Register<IPrincipal>(c =>
            {
                return new DummyRolePrincipal();
            }).InstancePerLifetimeScope();

            _container = builder.Build();

            TestContext.Container = _container;
        }

        [OneTimeTearDown]
        public void GlobalTeardown()
        {
            // Do logout here
            _container.Dispose();
        }
    }
}