﻿CREATE TABLE [Archive].[ImportedOffer] (
	[ImportedOfferId] INT NOT NULL
	,[ProductId] NVARCHAR(MAX) NULL
	,[DisplayName] NVARCHAR(MAX) NULL
	,[ProductName] NVARCHAR(MAX) NULL
	,[LongDescription] NVARCHAR(MAX) NULL
	,[Description] NVARCHAR(MAX) NULL
	,[SpecialOffer] NVARCHAR(MAX) NULL
	,[CelebId] NVARCHAR(MAX) NULL
	,[LocaleId] NVARCHAR(MAX) NULL
	,[CelebName] NVARCHAR(MAX) NULL
	,[CelebUrl] NVARCHAR(MAX) NULL
	,[CelebImageUrl] NVARCHAR(MAX) NULL
	,[CharityId] NVARCHAR(MAX) NULL
	,[CharityName] NVARCHAR(MAX) NULL
	,[CharityUrl] NVARCHAR(MAX) NULL
	,[FinePrint] NVARCHAR(MAX) NULL
	,[Details] NVARCHAR(MAX) NULL
	,[ProductTerms] NVARCHAR(MAX) NULL
	,[IsEvent] NVARCHAR(MAX) NULL
	,[MaxQuantityPerOrder] NVARCHAR(MAX) NULL
	,[Experience] NVARCHAR(MAX) NULL
	,[EventTime] NVARCHAR(MAX) NULL
	,[EndDate] NVARCHAR(MAX) NULL
	,[EventCity] NVARCHAR(MAX) NULL
	,[EventState] NVARCHAR(MAX) NULL
	,[Latitude] NVARCHAR(MAX) NULL
	,[Longitude] NVARCHAR(MAX) NULL
	,[GeographicId] NVARCHAR(MAX) NULL
	,[SharedPeoplePerItem] NVARCHAR(MAX) NULL
	,[EventVenue] NVARCHAR(MAX) NULL
	,[CatId] NVARCHAR(MAX) NULL
	,[CatName] NVARCHAR(MAX) NULL
	,[CatUrl] NVARCHAR(MAX) NULL
	,[AllCategories] NVARCHAR(MAX) NULL
	,[EventDuration] NVARCHAR(MAX) NULL
	,[ImageUrl] NVARCHAR(MAX) NULL
	,[ProductUrl] NVARCHAR(MAX) NULL
	,[AltImageUrl] NVARCHAR(MAX) NULL
	,[SourcedSiteName] NVARCHAR(MAX) NULL
	,[InitiativeName] NVARCHAR(MAX) NULL
	,[VoiceSkillDescription] NVARCHAR(MAX) NULL
	,[Variants] NVARCHAR(MAX) NULL
	,[ProgramName] NVARCHAR(MAX) NULL
	,[CardExclusivity] NVARCHAR(MAX) NULL
	,[IssuerExclusivity] NVARCHAR(MAX) NULL
	,[PunchoutInfo] NVARCHAR(MAX) NULL
	,[GeographicInclusions] NVARCHAR(MAX) NULL
	,[GeographicExclusions] NVARCHAR(MAX) NULL
	,[MastercardGlobal] NVARCHAR(MAX) NULL
	,[Price] NVARCHAR(MAX) NULL
	,[CurrencyCode] NVARCHAR(MAX) NULL
	,[CurrencyCharacter] NVARCHAR(MAX) NULL
	,[InventoryCount] NVARCHAR(MAX) NULL
	,[MasterProductIds] NVARCHAR(MAX) NULL
	,[LuminaryHtml] NVARCHAR(MAX) NULL
	,[IsShippable] NVARCHAR(MAX) NULL
	,[ProcessingDays] NVARCHAR(MAX) NULL
	,[ProcessingMsg] NVARCHAR(MAX) NULL
	,[Restriction] NVARCHAR(MAX) NULL
	,[ImportedDate] DATETIME NOT NULL
	,[IsLastImportedVersion] BIT NOT NULL
	,[IsProcessed] BIT NOT NULL
	,[IsArchived] BIT DEFAULT ((0)) NOT NULL
    ,CONSTRAINT [PK_ImportedOffer] PRIMARY KEY CLUSTERED ([ImportedOfferId] ASC)
	)