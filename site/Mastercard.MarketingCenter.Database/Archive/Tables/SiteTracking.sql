﻿CREATE TABLE [Archive].[SiteTracking] (
    [SiteTrackingID] BIGINT          NOT NULL,
    [VisitID]        BIGINT          NOT NULL,
    [What]           NVARCHAR (1000) NOT NULL,
    [Who]            NVARCHAR (1000) NULL,
    [When]           DATETIME        NOT NULL,
    [From]           NVARCHAR (MAX)  NULL,
    [How]            NVARCHAR (1000) NULL,
    [ContentItemID]  VARCHAR (25)    NULL,
    [PageGroup]      VARCHAR (50)    NULL,
    [Region]         NVARCHAR (100)  NOT NULL,
    [IsArchived]     BIT             NOT NULL,
    CONSTRAINT [PK_SiteTracking] PRIMARY KEY CLUSTERED ([SiteTrackingID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ix_ArchiveSiteTracking_ContentItemID]
    ON [Archive].[SiteTracking]([ContentItemID] ASC);

GO
CREATE NONCLUSTERED INDEX [ix_ArchiveSiteTracking_When]
    ON [Archive].[SiteTracking]([When] ASC)
    INCLUDE([From], [SiteTrackingID], [Who]) WITH (FILLFACTOR = 90);

GO
CREATE NONCLUSTERED INDEX [ix_ArchiveSiteTracking_ContentItemIDRegionWhen]
    ON [Archive].[SiteTracking]([ContentItemID] ASC, [Region] ASC, [When] ASC)
    INCLUDE([Who], [How]);

GO
CREATE NONCLUSTERED INDEX [ix_ArchiveSiteTracking_WhenSiteTrackingID]
    ON [Archive].[SiteTracking]([When] ASC, [SiteTrackingID] ASC);

GO
CREATE NONCLUSTERED INDEX [ix_ArchiveSiteTracking_Who]
    ON [Archive].[SiteTracking]([Who] ASC);