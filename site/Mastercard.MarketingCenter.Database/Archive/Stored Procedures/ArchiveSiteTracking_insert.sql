﻿CREATE PROCEDURE [Archive].ArchiveSiteTracking_insert
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @batchSize INT = 5000 -- maximim batch size to avoid blocking
		,@startID BIGINT = 0
		,@maxID BIGINT = 0
		,@rowsProcessed INT = - 1
		,@IsSchemaValid BIT = 0
		,@msg NVARCHAR(500) = ''
		,@dt DATETIME = sysdatetime();
	DECLARE @id TABLE (SiteTrackingID BIGINT NOT NULL);

	BEGIN TRY
		/***************************************/
		EXEC Archive.ArchiveSiteTracking_schemaCheck @IsSchemaValid OUTPUT;

		IF @IsSchemaValid = 0 throw 50001
			,'ERROR: Archive.SiteTracking table schema does not match dbo.SiteTracking one.'
			,1;
			SET @startID = isnull((
						SELECT max([SiteTrackingID]) + 1
						FROM Archive.[SiteTracking]
						), 0);
		SET @maxID = isnull((
					SELECT max([SiteTrackingID]) + 1
					FROM dbo.[SiteTracking]
					), 0);

		-- archieving fresh records
		WHILE @startID <= @maxID + @batchSize
		BEGIN
			DELETE
			FROM @id;

			BEGIN TRANSACTION

			INSERT INTO Archive.SiteTracking (
				SiteTrackingID
				,VisitID
				,What
				,Who
				,[When]
				,[From]
				,How
				,ContentItemID
				,PageGroup
				,Region
				,IsArchived
				)
			OUTPUT inserted.SiteTrackingID
			INTO @id(SiteTrackingID)
			SELECT SiteTrackingID
				,VisitID
				,What
				,Who
				,[When]
				,[From]
				,How
				,ContentItemID
				,PageGroup
				,Region
				,IsArchived
			FROM dbo.SiteTracking
			WHERE SiteTrackingID BETWEEN @startID
					AND @startID + @batchSize;

			SET @rowsProcessed = @@ROWCOUNT;

			UPDATE st
			SET st.IsArchived = 1
			FROM dbo.SiteTracking AS st
			INNER JOIN @id AS id ON st.SiteTrackingID = id.SiteTrackingID;

			COMMIT;

			SET @startID += @batchSize + 1;
			SET @msg = '-->>-- [Archive].ArchiveSiteTracking_insert: ' + ltrim(@rowsProcessed) + ' row(s) archived';

			RAISERROR (
					@msg
					,0
					,1
					)
			WITH NOWAIT;
				--print '-->>-- [Archive].ArchiveSiteTracking_insert: ' + ltrim(@rowsProcessed) + ' row(s) archived'
		END;
	END TRY

	BEGIN CATCH
		IF XACT_STATE() <> 0
			ROLLBACK;

		PRINT 'ERROR in ArchiveSiteTracking_insert';

		throw;
	END CATCH

	PRINT '-->>-- [Archive].ArchiveSiteTracking_insert was runned successfully in ' + ltrim(datediff(ms, @dt, sysdatetime()) / 1000.0) + ' sec.';
END;