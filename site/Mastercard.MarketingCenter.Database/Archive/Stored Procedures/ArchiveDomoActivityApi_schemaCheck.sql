﻿CREATE PROCEDURE [Archive].[ArchiveDomoActivityApi_schemaCheck] @isValid BIT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	SET @isValid = 0;

	IF EXISTS (
			SELECT c.[name]
			FROM sys.columns AS c
			WHERE c.object_id = object_id('DomoActivityApi', 'U')
			
			EXCEPT
			
			SELECT c.[name]
			FROM sys.columns AS c
			WHERE c.object_id = object_id('Archive.DomoActivityApi', 'U')
			) throw 50001
		,'ERROR: Archive.DomoActivityApi table schema does not match dbo.DomoActivityApi one.'
		,1;ELSE
		SET @isValid = 1;
END;