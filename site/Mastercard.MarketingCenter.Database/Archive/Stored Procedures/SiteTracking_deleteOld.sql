﻿CREATE PROCEDURE [Archive].SiteTracking_deleteOld
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @yearsToKeep INT = 2 -- let's keep in the operational store the last 2*365 days
		,@thresholdDate DATE = '1990-01-01'
		,@batchSize INT = 5000 -- maximim batch size to avoid blocking
		,@startID BIGINT = 0
		,@maxID BIGINT = 0
		,@dt DATETIME = sysdatetime()
		,@msg NVARCHAR(500) = ''
		,@rowsProcessed INT = - 1;

	BEGIN TRY
		SET @thresholdDate = cast(dateadd(year, - @yearsToKeep, SYSDATETIME()) AS DATE);

		SELECT @maxID = max(st.[SiteTrackingID])
			,@startID = min(st.[SiteTrackingID])
		FROM dbo.[SiteTracking] AS st
		WHERE st.[When] <= @thresholdDate
			AND st.IsArchived = 1;

		-- deleting old rows
		WHILE @startID <= @maxID + @batchSize
		BEGIN
			DELETE
			FROM dbo.SiteTracking
			WHERE SiteTrackingID BETWEEN @startID
					AND @startID + @batchSize
				AND IsArchived = 1;

			SET @rowsProcessed = @@ROWCOUNT;
			SET @startID += @batchSize + 1;
			SET @msg = '-->>-- [Archive].SiteTracking_deleteOld: ' + ltrim(@rowsProcessed) + ' row(s) deleted';

			RAISERROR (
					@msg
					,0
					,1
					)
			WITH NOWAIT;
		END;
	END TRY

	BEGIN CATCH
		PRINT 'ERROR in SiteTracking_deleteOld';

		throw;
	END CATCH

	PRINT '-->>-- [Archive].SiteTracking_deleteOld was runned successfully in ' + ltrim(datediff(ms, @dt, sysdatetime()) / 1000.0) + ' sec.';
END;