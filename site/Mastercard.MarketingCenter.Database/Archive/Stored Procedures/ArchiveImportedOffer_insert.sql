﻿CREATE PROCEDURE [Archive].[ArchiveImportedOffer_insert]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @batchSize INT = 5000 -- maximim batch size to avoid blocking
		,@startID INT = 0
		,@maxID INT = 0
		,@rowsProcessed INT = - 1
		,@IsSchemaValid BIT = 0
		,@msg NVARCHAR(500) = ''
		,@dt DATETIME = sysdatetime();
	DECLARE @id TABLE (ImportedOfferId INT NOT NULL);

	BEGIN TRY
		/***************************************/
		EXEC Archive.ArchiveImportedOffer_schemaCheck @IsSchemaValid OUTPUT;

		IF @IsSchemaValid = 0 throw 50001
			,'ERROR: Archive.ImportedOffer table schema does not match dbo.ImportedOffer one.'
			,1;
			SET @startID = isnull((
						SELECT max([ImportedOfferId]) + 1
						FROM Archive.[ImportedOffer]
						), 0);
		SET @maxID = isnull((
					SELECT max([ImportedOfferId]) + 1
					FROM dbo.[ImportedOffer]
					), 0);

		-- archieving fresh records
		WHILE @startID <= @maxID + @batchSize
		BEGIN
			DELETE
			FROM @id;

			BEGIN TRANSACTION

			INSERT INTO Archive.ImportedOffer (
				[ImportedOfferId]
				,[ProductId]
				,[DisplayName]
				,[ProductName]
				,[LongDescription]
				,[Description]
				,[SpecialOffer]
				,[CelebId]
				,[LocaleId]
				,[CelebName]
				,[CelebUrl]
				,[CelebImageUrl]
				,[CharityId]
				,[CharityName]
				,[CharityUrl]
				,[FinePrint]
				,[Details]
				,[ProductTerms]
				,[IsEvent]
				,[MaxQuantityPerOrder]
				,[Experience]
				,[EventTime]
				,[EventCity]
				,[EventState]
				,[Latitude]
				,[Longitude]
				,[GeographicId]
				,[SharedPeoplePerItem]
				,[EventVenue]
				,[CatId]
				,[CatName]
				,[CatUrl]
				,[AllCategories]
				,[EventDuration]
				,[ImageUrl]
				,[ProductUrl]
				,[AltImageUrl]
				,[SourcedSiteName]
				,[InitiativeName]
				,[VoiceSkillDescription]
				,[Variants]
				,[CardExclusivity]
				,[IssuerExclusivity]
				,[PunchoutInfo]
				,[GeographicInclusions]
				,[GeographicExclusions]
				,[MastercardGlobal]
				,[Price]
				,[CurrencyCode]
				,[CurrencyCharacter]
				,[InventoryCount]
				,[MasterProductIds]
				,[LuminaryHtml]
				,[IsShippable]
				,[ProcessingDays]
				,[ProcessingMsg]
				,[Restriction]
				,[ImportedDate]
				,[IsLastImportedVersion]
				,[IsProcessed]
				,[IsArchived]
				)
			OUTPUT inserted.ImportedOfferId
			INTO @id(ImportedOfferId)
			SELECT [ImportedOfferId]
				,[ProductId]
				,[DisplayName]
				,[ProductName]
				,[LongDescription]
				,[Description]
				,[SpecialOffer]
				,[CelebId]
				,[LocaleId]
				,[CelebName]
				,[CelebUrl]
				,[CelebImageUrl]
				,[CharityId]
				,[CharityName]
				,[CharityUrl]
				,[FinePrint]
				,[Details]
				,[ProductTerms]
				,[IsEvent]
				,[MaxQuantityPerOrder]
				,[Experience]
				,[EventTime]
				,[EventCity]
				,[EventState]
				,[Latitude]
				,[Longitude]
				,[GeographicId]
				,[SharedPeoplePerItem]
				,[EventVenue]
				,[CatId]
				,[CatName]
				,[CatUrl]
				,[AllCategories]
				,[EventDuration]
				,[ImageUrl]
				,[ProductUrl]
				,[AltImageUrl]
				,[SourcedSiteName]
				,[InitiativeName]
				,[VoiceSkillDescription]
				,[Variants]
				,[CardExclusivity]
				,[IssuerExclusivity]
				,[PunchoutInfo]
				,[GeographicInclusions]
				,[GeographicExclusions]
				,[MastercardGlobal]
				,[Price]
				,[CurrencyCode]
				,[CurrencyCharacter]
				,[InventoryCount]
				,[MasterProductIds]
				,[LuminaryHtml]
				,[IsShippable]
				,[ProcessingDays]
				,[ProcessingMsg]
				,[Restriction]
				,[ImportedDate]
				,[IsLastImportedVersion]
				,[IsProcessed]
				,[IsArchived]
			FROM dbo.ImportedOffer
			WHERE ImportedOfferId BETWEEN @startID
					AND @startID + @batchSize;

			SET @rowsProcessed = @@ROWCOUNT;

			UPDATE o
			SET o.IsArchived = 1
			FROM dbo.ImportedOffer AS o
			INNER JOIN @id AS id ON o.ImportedOfferId = id.ImportedOfferId;

			COMMIT;

			SET @startID += @batchSize + 1;
			SET @msg = '-->>-- [Archive].ArchiveImportedOffer_insert: ' + ltrim(@rowsProcessed) + ' row(s) archived';

			RAISERROR (
					@msg
					,0
					,1
					)
			WITH NOWAIT;
				--print '-->>-- [Archive].ArchiveImportedOffer_insert: ' + ltrim(@rowsProcessed) + ' row(s) archived'
		END;
	END TRY

	BEGIN CATCH
		IF XACT_STATE() <> 0
			ROLLBACK;

		PRINT 'ERROR in ArchiveImportedOffer_insert';

		throw;
	END CATCH

	PRINT '-->>-- [Archive].ArchiveImportedOffer_insert was runned successfully in ' + ltrim(datediff(ms, @dt, sysdatetime()) / 1000.0) + ' sec.';
END;