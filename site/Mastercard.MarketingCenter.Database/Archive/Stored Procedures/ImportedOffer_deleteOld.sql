﻿CREATE PROCEDURE [Archive].ImportedOffer_deleteOld
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @batchSize INT = 5000 -- maximim batch size to avoid blocking
		,@thresholdDate DATETIME
		,@startID BIGINT = 0
		,@maxID BIGINT = 0
		,@dt DATETIME = sysdatetime()
		,@msg NVARCHAR(500) = ''
		,@rowsProcessed INT = - 1;

	BEGIN TRY
		SELECT @thresholdDate = MAX([ImportedDate])
		FROM dbo.[ImportedOffer]
		WHERE [IsLastImportedVersion] = 0;

		SELECT @maxID = max(o.[ImportedOfferId])
			,@startID = min(o.[ImportedOfferId])
		FROM dbo.[ImportedOffer] AS o
		WHERE o.[ImportedDate] < @thresholdDate
			AND o.[IsLastImportedVersion] = 0
			AND o.[IsArchived] = 1;

		-- deleting old rows
		WHILE @startID <= @maxID + @batchSize
		BEGIN
			DELETE
			FROM dbo.ImportedOffer
			WHERE [ImportedOfferId] BETWEEN @startID
					AND CASE 
							WHEN @startID + @batchSize > @maxID
								THEN @maxID
							ELSE @startID + @batchSize
							END
				AND IsArchived = 1;

			SET @rowsProcessed = @@ROWCOUNT;
			SET @startID += @batchSize + 1;
			SET @msg = '-->>-- [Archive].ImportedOffer_deleteOld: ' + ltrim(@rowsProcessed) + ' row(s) deleted';

			RAISERROR (
					@msg
					,0
					,1
					)
			WITH NOWAIT;
		END;
	END TRY

	BEGIN CATCH
		PRINT 'ERROR in ImportedOffer_deleteOld';

		throw;
	END CATCH

	PRINT '-->>-- [Archive].ImportedOffer_deleteOld was runned successfully in ' + ltrim(datediff(ms, @dt, sysdatetime()) / 1000.0) + ' sec.';
END;