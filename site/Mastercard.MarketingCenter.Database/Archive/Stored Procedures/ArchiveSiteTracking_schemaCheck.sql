﻿CREATE PROCEDURE Archive.ArchiveSiteTracking_schemaCheck @isValid BIT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	SET @isValid = 0;

	IF EXISTS (
			SELECT c.[name]
			FROM sys.columns AS c
			WHERE c.object_id = object_id('SiteTracking', 'U')
			
			EXCEPT
			
			SELECT c.[name]
			FROM sys.columns AS c
			WHERE c.object_id = object_id('Archive.SiteTracking', 'U')
			) throw 50001
		,'ERROR: Archive.SiteTracking table schema does not match dbo.SiteTracking one.'
		,1;ELSE
		SET @isValid = 1;
END;