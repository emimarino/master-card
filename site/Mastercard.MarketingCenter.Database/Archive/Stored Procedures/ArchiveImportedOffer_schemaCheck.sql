﻿CREATE PROCEDURE [Archive].[ArchiveImportedOffer_schemaCheck] @isValid BIT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	SET @isValid = 0;

	IF EXISTS (
			SELECT c.[name]
			FROM sys.columns AS c
			WHERE c.object_id = object_id('ImportedOffer', 'U')
			
			EXCEPT
			
			SELECT c.[name]
			FROM sys.columns AS c
			WHERE c.object_id = object_id('Archive.ImportedOffer', 'U')
			) throw 50001
		,'ERROR: Archive.ImportedOffer table schema does not match dbo.ImportedOffer one.'
		,1;ELSE
		SET @isValid = 1;
END;