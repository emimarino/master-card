﻿CREATE PROCEDURE [Archive].[ArchiveDomoActivityApi_insert]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @batchSize INT = 5000 -- maximim batch size to avoid blocking
		,@startID BIGINT = 0
		,@maxID BIGINT = 0
		,@rowsProcessed INT = - 1
		,@IsSchemaValid BIT = 0
		,@msg NVARCHAR(500) = ''
		,@dt DATETIME = sysdatetime();
	DECLARE @id TABLE (Id BIGINT NOT NULL);

	BEGIN TRY
		/***************************************/
		EXEC [Archive].[ArchiveDomoActivityApi_schemaCheck] @IsSchemaValid OUTPUT;

		IF @IsSchemaValid = 0 throw 50001
			,'ERROR: [Archive].[DomoActivityApi] table schema does not match [dbo].[DomoActivityApi] one.'
			,1;
			SET @startID = isnull((
						SELECT max([Id]) + 1
						FROM [Archive].[DomoActivityApi]
						), 0);
		SET @maxID = isnull((
					SELECT max([Id]) + 1
					FROM [dbo].[DomoActivityApi]
					), 0);

		-- archieving fresh records
		WHILE @startID <= @maxID + @batchSize
		BEGIN
			DELETE
			FROM @id;

			BEGIN TRANSACTION

			INSERT INTO [Archive].[DomoActivityApi] (
				[Id]
				,[SiteTrackingId]
				,[VisitId]
				,[User_UserId]
				,[User_LastName]
				,[User_FirstName]
				,[User_Email]
				,[User_Region]
				,[User_Country]
				,[User_IssuerName]
				,[User_ProcessorName]
				,[When]
				,[From]
				,[How]
				,[Content_Type]
				,[Content_Id]
				,[Content_Title]
				,[Content_RegionName]
				,[Hidden_Result]
				,[User_IssuerImportedName]
				,[User_IssuerCID]
				,[IsArchived]
				)
			OUTPUT inserted.[Id]
			INTO @id([Id])
			SELECT [Id]
				,[SiteTrackingId]
				,[VisitId]
				,[User_UserId]
				,[User_LastName]
				,[User_FirstName]
				,[User_Email]
				,[User_Region]
				,[User_Country]
				,[User_IssuerName]
				,[User_ProcessorName]
				,[When]
				,[From]
				,[How]
				,[Content_Type]
				,[Content_Id]
				,[Content_Title]
				,[Content_RegionName]
				,[Hidden_Result]
				,[User_IssuerImportedName]
				,[User_IssuerCID]
				,[IsArchived]
			FROM [dbo].[DomoActivityApi]
			WHERE [Id] BETWEEN @startID
					AND @startID + @batchSize;

			SET @rowsProcessed = @@ROWCOUNT;

			UPDATE daa
			SET daa.[IsArchived] = 1
			FROM [dbo].[DomoActivityApi] AS daa
			INNER JOIN @id AS id ON daa.[Id] = id.[Id];

			COMMIT;

			SET @startID += @batchSize + 1;
			SET @msg = '-->>-- [Archive].[ArchiveDomoActivityApi_insert]: ' + ltrim(@rowsProcessed) + ' row(s) archived';

			RAISERROR (
					@msg
					,0
					,1
					)
			WITH NOWAIT;
				--print '-->>-- [Archive].[ArchiveDomoActivityApi_insert]: ' + ltrim(@rowsProcessed) + ' row(s) archived'
		END;
	END TRY

	BEGIN CATCH
		IF XACT_STATE() <> 0
			ROLLBACK;

		PRINT 'ERROR in [ArchiveDomoActivityApi_insert]';

		throw;
	END CATCH

	PRINT '-->>-- [Archive].[ArchiveDomoActivityApi_insert] was runned successfully in ' + ltrim(datediff(ms, @dt, sysdatetime()) / 1000.0) + ' sec.';
END;