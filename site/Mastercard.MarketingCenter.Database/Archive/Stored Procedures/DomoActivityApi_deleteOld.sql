﻿CREATE PROCEDURE [Archive].[DomoActivityApi_deleteOld]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @monthsToKeep TINYINT = 3
		,@batchSize INT = 5000 -- maximim batch size to avoid blocking
		,@startID BIGINT = 0
		,@maxID BIGINT = 0
		,@dt DATETIME = sysdatetime()
		,@msg NVARCHAR(500) = ''
		,@rowsProcessed INT = - 1;

	BEGIN TRY
		SELECT @maxID = max(daa.[Id])
			,@startID = min(daa.[Id])
		FROM [dbo].[DomoActivityApi] AS daa
		WHERE daa.[When] <= cast(dateadd(month, - @monthsToKeep, sysdatetime()) AS DATE)
			AND daa.[IsArchived] = 1;

		-- deleting old rows
		WHILE @startID <= @maxID + @batchSize
		BEGIN
			DELETE
			FROM [dbo].[DomoActivityApi]
			WHERE [Id] BETWEEN @startID
					AND @startID + @batchSize
				AND [Id] <= @maxID
				AND [IsArchived] = 1;

			SET @rowsProcessed = @@ROWCOUNT;
			SET @startID += @batchSize + 1;
			SET @msg = '-->>-- [Archive].[DomoActivityApi_deleteOld]: ' + ltrim(@rowsProcessed) + ' row(s) deleted';

			RAISERROR (
					@msg
					,0
					,1
					)
			WITH NOWAIT;
		END;
	END TRY

	BEGIN CATCH
		PRINT 'ERROR in [DomoActivityApi_deleteOld]';

		throw;
	END CATCH

	PRINT '-->>-- [Archive].[DomoActivityApi_deleteOld] was runned successfully in ' + ltrim(datediff(ms, @dt, sysdatetime()) / 1000.0) + ' sec.';
END;