﻿CREATE TABLE [Admin].[BrokenLink] (
    [BrokenLinkId]        INT           IDENTITY (1, 1) NOT NULL,
    [Url]                 VARCHAR (MAX) NOT NULL,
    [Application]         VARCHAR (200) NOT NULL,
    [ModifiedDate]        DATETIME      NULL,
    [IsHandled]           BIT           CONSTRAINT [DF_BrokenLink_IsHandled] DEFAULT ((0)) NOT NULL,
    [Message]             VARCHAR (MAX) NULL,
    [RedirectUrl]         VARCHAR (MAX) NULL,
    [RedirectApplication] VARCHAR (200) NULL,
    [Referrer]            VARCHAR (MAX) NULL,
    CONSTRAINT [PK_BrokenLink] PRIMARY KEY CLUSTERED ([BrokenLinkId] ASC) WITH (FILLFACTOR = 90)
);

