﻿CREATE TABLE [Admin].[BrokenLinkTrack] (
    [BrokenLinkTrackId] INT           IDENTITY (1, 1) NOT NULL,
    [BrokenLinkId]      INT           NOT NULL,
    [Date]              DATETIME      NOT NULL,
    [UserName]          VARCHAR (255) NULL,
    CONSTRAINT [PK_BrokenLinkTrack] PRIMARY KEY CLUSTERED ([BrokenLinkTrackId] ASC),
    CONSTRAINT [FK_BrokenLinkTrack_BrokenLink] FOREIGN KEY ([BrokenLinkId]) REFERENCES [Admin].[BrokenLink] ([BrokenLinkId])
);

