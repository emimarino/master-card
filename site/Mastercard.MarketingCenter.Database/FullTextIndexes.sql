﻿CREATE FULLTEXT INDEX ON [dbo].[Page]
    ([Title] LANGUAGE 0, [Uri] LANGUAGE 0, [IntroCopy] LANGUAGE 0, [Terms] LANGUAGE 0, [ShortDescription] LANGUAGE 0, [AsideContent] LANGUAGE 0)
    KEY INDEX [PK_Page]
    ON ([MasterCardMarketingCenter], FILEGROUP [ftfg_MasterCardMarketingCenter]);




GO
CREATE FULLTEXT INDEX ON [dbo].[TagTranslatedContent]
    ([DisplayName] LANGUAGE 1033, [Description] LANGUAGE 1033)
    KEY INDEX [UX_TagTranslatedContent]
    ON ([MasterCardMarketingCenter], FILEGROUP [ftfg_MasterCardMarketingCenter]);


GO
CREATE FULLTEXT INDEX ON [dbo].[Processor]
    ([Title] LANGUAGE 0)
    KEY INDEX [PK_Processor]
    ON [MasterCardMarketingCenter];


GO
CREATE FULLTEXT INDEX ON [dbo].[PricingSchedule]
    ([Title] LANGUAGE 0, [Details] LANGUAGE 0)
    KEY INDEX [PK_PricingSchedule]
    ON [MasterCardMarketingCenter];


GO
CREATE FULLTEXT INDEX ON [dbo].[Icon]
    ([Name] LANGUAGE 0, [Extension] LANGUAGE 0, [Icon] LANGUAGE 0)
    KEY INDEX [PK_Icon]
    ON [MasterCardMarketingCenter];


GO
CREATE FULLTEXT INDEX ON [dbo].[FeatureLocation]
    ([Title] LANGUAGE 0)
    KEY INDEX [PK_FeatureLocation]
    ON [MasterCardMarketingCenter];


GO
CREATE FULLTEXT INDEX ON [dbo].[Asset]
    ([Title] LANGUAGE 0, [ShortDescription] LANGUAGE 0, [LongDescription] LANGUAGE 0, [CallToAction] LANGUAGE 0, [BrowseAllMaterialsDisplayText] LANGUAGE 0, [BrowseAllMaterialsTags] LANGUAGE 0)
    KEY INDEX [PK_Asset]
    ON ([MasterCardMarketingCenter], FILEGROUP [ftfg_MasterCardMarketingCenter]);




GO
CREATE FULLTEXT INDEX ON [dbo].[Issuer]
    ([Title] LANGUAGE 0, [DomainName] LANGUAGE 0)
    KEY INDEX [PK_Issuer]
    ON [MasterCardMarketingCenter];


GO
CREATE FULLTEXT INDEX ON [dbo].[AssetFullWidth]
    ([Title] LANGUAGE 0, [ShortDescription] LANGUAGE 0, [LongDescription] LANGUAGE 0)
    KEY INDEX [PK_AssetFullWidth]
    ON ([MasterCardMarketingCenter], FILEGROUP [ftfg_MasterCardMarketingCenter]);


GO
CREATE FULLTEXT INDEX ON [dbo].[DownloadableAsset]
    ([Title] LANGUAGE 0, [ShortDescription] LANGUAGE 0, [LongDescription] LANGUAGE 0, [CallToAction] LANGUAGE 0)
    KEY INDEX [PK_DownloadableAsset]
    ON ([MasterCardMarketingCenter], FILEGROUP [ftfg_MasterCardMarketingCenter]);




GO
CREATE FULLTEXT INDEX ON [dbo].[OrderableAsset]
    ([Title] LANGUAGE 0, [ShortDescription] LANGUAGE 0, [LongDescription] LANGUAGE 0, [CallToAction] LANGUAGE 0, [Sku] LANGUAGE 0)
    KEY INDEX [PK_OrderableAsset]
    ON ([MasterCardMarketingCenter], FILEGROUP [ftfg_MasterCardMarketingCenter]);




GO
CREATE FULLTEXT INDEX ON [dbo].[PromoCode]
    ([PromotionCode] LANGUAGE 0, [Description] LANGUAGE 0)
    KEY INDEX [PK_PromoCode]
    ON [MasterCardMarketingCenter];


GO
CREATE FULLTEXT INDEX ON [dbo].[Program]
    ([Title] LANGUAGE 0, [ShortDescription] LANGUAGE 0, [LongDescription] LANGUAGE 0, [CallToAction] LANGUAGE 0)
    KEY INDEX [PK_Program]
    ON ([MasterCardMarketingCenter], FILEGROUP [ftfg_MasterCardMarketingCenter]);




GO
CREATE FULLTEXT INDEX ON [dbo].[SearchTerm]
    ([Title] LANGUAGE 0, [SearchTerms] LANGUAGE 0, [Synonyms] LANGUAGE 0, [FeaturedSearchResultHtml] LANGUAGE 0)
    KEY INDEX [PK_SearchTerm]
    ON ([MasterCardMarketingCenter], FILEGROUP [ftfg_MasterCardMarketingCenter]);


GO
CREATE FULLTEXT INDEX ON [dbo].[Tag]
    ([Identifier] LANGUAGE 0, [DisplayName] LANGUAGE 0, [Description] LANGUAGE 0)
    KEY INDEX [PK_Tag]
    ON ([MasterCardMarketingCenter], FILEGROUP [ftfg_MasterCardMarketingCenter]);


GO
CREATE FULLTEXT INDEX ON [dbo].[Webinar]
    ([Title] LANGUAGE 0, [ShortDescription] LANGUAGE 0, [LongDescription] LANGUAGE 0, [Link] LANGUAGE 0)
    KEY INDEX [PK_Webinar]
    ON ([MasterCardMarketingCenter], FILEGROUP [ftfg_MasterCardMarketingCenter]);


GO
CREATE FULLTEXT INDEX ON [dbo].[HtmlDownload]
    ([Title] LANGUAGE 0, [Url] LANGUAGE 0, [Filename] LANGUAGE 0)
    KEY INDEX [PK_HtmlDownload]
    ON [MasterCardMarketingCenter];


GO
CREATE FULLTEXT INDEX ON [dbo].[QuickLink]
    ([Title] LANGUAGE 0, [Paragraph] LANGUAGE 0, [Url] LANGUAGE 0)
    KEY INDEX [PK_QuickLink]
    ON [MasterCardMarketingCenter];


GO
CREATE FULLTEXT INDEX ON [dbo].[VanityUrl]
    ([Url] LANGUAGE 0, [VanityUrl] LANGUAGE 0)
    KEY INDEX [PK_VanityUrl]
    ON [MasterCardMarketingCenter];


GO
CREATE FULLTEXT INDEX ON [dbo].[YoutubeVideo]
    ([Title] LANGUAGE 0, [Url] LANGUAGE 0)
    KEY INDEX [PK_YoutubeVideo]
    ON [MasterCardMarketingCenter];


GO
CREATE FULLTEXT INDEX ON [dbo].[DomainBlacklist]
    ([Domain] LANGUAGE 0)
    KEY INDEX [PK_DomainBlacklist]
    ON [MasterCardMarketingCenter];


GO
CREATE FULLTEXT INDEX ON [dbo].[CustomizationOption]
    ([FriendlyName] LANGUAGE 0, [Instructions] LANGUAGE 0)
    KEY INDEX [PK_CustomizationOption]
    ON [MasterCardMarketingCenter];




GO
CREATE FULLTEXT INDEX ON [dbo].[ImportedIca]
    ([CID] LANGUAGE 1033, [LegalName] LANGUAGE 1033, [HQCityName] LANGUAGE 1033, [HQProvinceName] LANGUAGE 1033)
    KEY INDEX [PK_ImportedICAs]
    ON [MasterCardMarketingCenter];


GO
CREATE FULLTEXT INDEX ON [dbo].[Snippet]
    ([Title] LANGUAGE 0, [Html] LANGUAGE 0)
    KEY INDEX [PK_Snippet]
    ON [MasterCardMarketingCenter];


GO
CREATE FULLTEXT INDEX ON [dbo].[MarqueeSlide]
    ([Title] LANGUAGE 0, [LongDescription] LANGUAGE 0)
    KEY INDEX [PK_MarqueeSlide]
    ON [MasterCardMarketingCenter];


GO
CREATE FULLTEXT INDEX ON [dbo].[SiteTracking]
    ([What] LANGUAGE 1033, [From] LANGUAGE 1033)
    KEY INDEX [PK_SiteTracking]
    ON [MasterCardMarketingCenter];

