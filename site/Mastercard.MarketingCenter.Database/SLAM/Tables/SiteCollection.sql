﻿CREATE TABLE [SLAM].[SiteCollection] (
    [SiteCollectionID]  UNIQUEIDENTIFIER NOT NULL,
    [ServerRelativeUrl] VARCHAR (260)    NOT NULL,
    CONSTRAINT [PK_SLAM_SiteCollection] PRIMARY KEY CLUSTERED ([SiteCollectionID] ASC)
);

