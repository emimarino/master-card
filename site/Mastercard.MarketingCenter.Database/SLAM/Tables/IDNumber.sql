﻿CREATE TABLE [SLAM].[IDNumber] (
    [ID] INT IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [PK_SLAM_IdNumber] PRIMARY KEY CLUSTERED ([ID] ASC)
);

