﻿CREATE TABLE [SLAM].[EventLog] (
    [EventLogID] INT            IDENTITY (1, 1) NOT NULL,
    [EventDate]  DATETIME       NULL,
    [EventName]  VARCHAR (1000) NULL,
    [TypeName]   VARCHAR (200)  NULL,
    [Message]    VARCHAR (MAX)  NULL,
    CONSTRAINT [PK_SLAM_EventLog] PRIMARY KEY CLUSTERED ([EventLogID] ASC)
);

