﻿CREATE TABLE [SLAM].[Site] (
    [SiteID]           UNIQUEIDENTIFIER NOT NULL,
    [Name]             VARCHAR (260)    NOT NULL,
    [Title]            VARCHAR (260)    NOT NULL,
    [Url]              VARCHAR (260)    NOT NULL,
    [SiteCollectionID] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_SLAM_Site] PRIMARY KEY CLUSTERED ([SiteID] ASC),
    CONSTRAINT [FK_Site_SiteCollection] FOREIGN KEY ([SiteCollectionID]) REFERENCES [SLAM].[SiteCollection] ([SiteCollectionID]) ON DELETE CASCADE
);

