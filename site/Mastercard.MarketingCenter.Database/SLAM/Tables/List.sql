﻿CREATE TABLE [SLAM].[List] (
    [ListID]         UNIQUEIDENTIFIER NOT NULL,
    [DefaultViewUrl] VARCHAR (260)    NOT NULL,
    [NewFormUrl]     VARCHAR (260)    NOT NULL,
    [EditFormUrl]    VARCHAR (260)    NOT NULL,
    [DisplayFormUrl] VARCHAR (260)    NOT NULL,
    [SiteID]         UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_SLAM_List] PRIMARY KEY CLUSTERED ([ListID] ASC),
    CONSTRAINT [FK_List_Site] FOREIGN KEY ([SiteID]) REFERENCES [SLAM].[Site] ([SiteID]) ON DELETE CASCADE
);