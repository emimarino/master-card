﻿CREATE TABLE [SLAM].[MessageLog] (
    [MessageLogID] INT            IDENTITY (1, 1) NOT NULL,
    [MessageDate]  DATETIME       NULL,
    [MessageType]  VARCHAR (1000) NULL,
    [Message]      VARCHAR (MAX)  NULL,
    CONSTRAINT [PK_SLAM_MessageLog] PRIMARY KEY CLUSTERED ([MessageLogID] ASC)
);

