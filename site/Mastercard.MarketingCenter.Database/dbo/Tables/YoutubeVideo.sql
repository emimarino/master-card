﻿CREATE TABLE [dbo].[YoutubeVideo] (
    [ContentItemID] VARCHAR (25)   NOT NULL,
    [Title]         NVARCHAR (255) NULL,
    [Url]           NVARCHAR (255) NULL,
    [OrderNumber]   INT            NULL,
	[StaticVideoImage]          VARCHAR (255)  NULL
    CONSTRAINT [PK_YoutubeVideo] PRIMARY KEY CLUSTERED ([ContentItemID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_YoutubeVideo_ContentItem] FOREIGN KEY ([ContentItemID]) REFERENCES [dbo].[ContentItem] ([ContentItemID])
);

