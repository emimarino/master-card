﻿CREATE TABLE [dbo].[DownloadableAssetRelatedProgram] (
    [DownloadableAssetContentItemId] VARCHAR (25) NOT NULL,
    [ProgramContentItemId]           VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_DownloadableAssetRelatedProgram] PRIMARY KEY CLUSTERED ([DownloadableAssetContentItemId] ASC, [ProgramContentItemId] ASC),
    CONSTRAINT [FK_DownloadableAssetRelatedProgram_DownloadableAsset] FOREIGN KEY ([DownloadableAssetContentItemId]) REFERENCES [dbo].[DownloadableAsset] ([ContentItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_DownloadableAssetRelatedProgram_Program] FOREIGN KEY ([ProgramContentItemId]) REFERENCES [dbo].[Program] ([ContentItemId]) ON DELETE CASCADE
);

