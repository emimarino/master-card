﻿CREATE TABLE [dbo].[UserKeyStatus] (
    [UserKeyStatusID] INT           NOT NULL,
    [Name]            VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_UserKeyStatus] PRIMARY KEY CLUSTERED ([UserKeyStatusID] ASC) WITH (FILLFACTOR = 90)
);

