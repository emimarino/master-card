﻿CREATE TABLE [dbo].[AssetFullWidthRelatedProgram] (
    [AssetFullWidthContentItemId] VARCHAR (25) NOT NULL,
    [ProgramContentItemId]        VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_AssetFullWidthRelatedProgram] PRIMARY KEY CLUSTERED ([AssetFullWidthContentItemId] ASC, [ProgramContentItemId] ASC),
    CONSTRAINT [FK_AssetFullWidthRelatedProgram_AssetFullWidth] FOREIGN KEY ([AssetFullWidthContentItemId]) REFERENCES [dbo].[AssetFullWidth] ([ContentItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_AssetFullWidthRelatedProgram_Program] FOREIGN KEY ([ProgramContentItemId]) REFERENCES [dbo].[Program] ([ContentItemId]) ON DELETE CASCADE
);

