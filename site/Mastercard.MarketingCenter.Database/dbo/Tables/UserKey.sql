﻿CREATE TABLE [dbo].[UserKey] (
    [UserKeyID]        INT           IDENTITY (1, 1) NOT NULL,
    [UserId]           INT           NOT NULL,
    [UserKeyStatusID]  INT           NOT NULL,
    [Key]              VARCHAR (250) NOT NULL,
    [DateKeyGenerated] DATETIME      NOT NULL,
    PRIMARY KEY CLUSTERED ([UserKeyID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_UserKey_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId]) ON DELETE CASCADE,
    CONSTRAINT [FK_UserKey_UserKeyStatus] FOREIGN KEY ([UserKeyStatusID]) REFERENCES [dbo].[UserKeyStatus] ([UserKeyStatusID]) ON DELETE CASCADE
);

