﻿CREATE TABLE [dbo].[DownloadableAssetAttachment] (
    [AttachmentID]  INT             IDENTITY (1, 1) NOT NULL,
    [ContentItemId] VARCHAR (25)    NOT NULL,
    [FileUrl]       NVARCHAR (2000) NULL,
    CONSTRAINT [PK_DownloadableAssetAttachment] PRIMARY KEY CLUSTERED ([AttachmentID] ASC),
    CONSTRAINT [FK_DownloadableAssetAttachment_DownloadableAsset] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[DownloadableAsset] ([ContentItemId]) ON DELETE CASCADE
);



