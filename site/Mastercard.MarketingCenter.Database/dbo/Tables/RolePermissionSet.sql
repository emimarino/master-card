﻿CREATE TABLE [dbo].[RolePermissionSet] (
    [Id]               INT              IDENTITY (1, 1) NOT NULL,
    [RolePermissionId] INT              NOT NULL,
    [RoleId]           UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_RolePermissionSet] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_RolePermissionSet_aspnet_Roles] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[aspnet_Roles] ([RoleId]),
    CONSTRAINT [FK_RolePermissionSet_RolePermission] FOREIGN KEY ([RolePermissionId]) REFERENCES [dbo].[RolePermission] ([Id])
);

