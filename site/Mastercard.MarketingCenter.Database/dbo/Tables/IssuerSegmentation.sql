﻿CREATE TABLE [dbo].[IssuerSegmentation] (
    [IssuerID]       VARCHAR (25) NOT NULL,
    [SegmentationId] VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_IssuerSegmentation] PRIMARY KEY CLUSTERED ([IssuerID] ASC, [SegmentationId] ASC),
    CONSTRAINT [FK_IssuerSegmentation_Issuer] FOREIGN KEY ([IssuerID]) REFERENCES [dbo].[Issuer] ([IssuerID]) ON DELETE CASCADE,
    CONSTRAINT [FK_IssuerSegmentation_Segmentation] FOREIGN KEY ([SegmentationId]) REFERENCES [dbo].[Segmentation] ([SegmentationId])
);



