﻿CREATE TABLE [dbo].[EventLocation] (
	[EventLocationId] INT NOT NULL PRIMARY KEY IDENTITY
	,[Title] VARCHAR(MAX) NOT NULL
	,[PricelessEventCity] NVARCHAR(MAX) NULL
	,[CreatedDate] DATETIME NOT NULL
	,[ModifiedDate] DATETIME NULL
	);