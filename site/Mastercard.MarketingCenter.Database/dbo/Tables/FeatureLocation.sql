﻿CREATE TABLE [dbo].[FeatureLocation] (
    [FeatureLocationID] VARCHAR (25)     NOT NULL,
    [Title]             NVARCHAR (255)   NULL,
    CONSTRAINT [PK_FeatureLocation] PRIMARY KEY CLUSTERED ([FeatureLocationID] ASC),
);