﻿CREATE TABLE [dbo].[UserGroup] (
    [UserID]  INT NOT NULL,
    [GroupID] INT NOT NULL,
    CONSTRAINT [PK_UserGroup] PRIMARY KEY CLUSTERED ([UserID] ASC, [GroupID] ASC),
    CONSTRAINT [FK_UserGroup_Group] FOREIGN KEY ([GroupID]) REFERENCES [dbo].[Group] ([GroupID]) ON DELETE CASCADE,
    CONSTRAINT [FK_UserGroup_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserId]) ON DELETE CASCADE
);

