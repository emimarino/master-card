﻿CREATE TABLE [dbo].[PricingSchedule] (
    [PricingScheduleID]     VARCHAR (25)     NOT NULL,
    [Title]                 NVARCHAR (255)   NULL,
    [PricingTable]          VARCHAR (MAX)    NULL,
    [Details]               VARCHAR (MAX)    NULL,
    [MinimumPrice]          DECIMAL (18, 2)  NULL,
    [DownloadPrice]         DECIMAL (18, 2)  NULL,
    [ContactPrinterForMore] BIT              NULL,
    [TableHeader]           VARCHAR (255)    NULL,
    [UnitSize]              VARCHAR (255)    NULL,
    [UnitPrice]             DECIMAL (18, 2)  NULL,
    CONSTRAINT [PK_PricingSchedule] PRIMARY KEY CLUSTERED ([PricingScheduleID] ASC),
);