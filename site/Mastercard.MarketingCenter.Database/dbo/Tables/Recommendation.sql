﻿CREATE TABLE [dbo].[Recommendation] (
    [ContentItemId]    VARCHAR (25)  NOT NULL,
    [DisplayName]      VARCHAR (255) NOT NULL,
    [ShortDescription] VARCHAR (MAX) NULL,
    CONSTRAINT [PK_Recommendation] PRIMARY KEY CLUSTERED ([ContentItemId] ASC)
);

