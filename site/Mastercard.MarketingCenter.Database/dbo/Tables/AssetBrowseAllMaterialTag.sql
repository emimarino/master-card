﻿CREATE TABLE [dbo].[AssetBrowseAllMaterialTag] (
    [ContentItemId] VARCHAR (25) NOT NULL,
    [TagID]         VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_AssetBrowseAllMaterialTag] PRIMARY KEY CLUSTERED ([ContentItemId] ASC, [TagID] ASC),
    CONSTRAINT [FK_AssetBrowseAllMaterialTag_Asset] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[Asset] ([ContentItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_AssetBrowseAllMaterialTag_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([TagID]) ON DELETE CASCADE
);

