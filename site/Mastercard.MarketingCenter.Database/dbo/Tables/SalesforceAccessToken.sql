﻿CREATE TABLE [dbo].[SalesforceAccessToken]
(
	[SalesforceTokenId] INT NOT NULL PRIMARY KEY IDENTITY,
    [AccessToken] VARCHAR(255) NOT NULL,
    [Scope] VARCHAR(50) NOT NULL,
    [InstanceUrl] VARCHAR(255) NOT NULL,
    [TokenId] VARCHAR(255) NOT NULL,
    [TokenType] VARCHAR(255) NOT NULL,
    [CreatedDate] DATETIME NOT NULL,
    [ModifiedDate] DATETIME NOT NULL
)