﻿CREATE TABLE [dbo].[SearchActivity] (
    [SearchActivityID] INT            IDENTITY (1, 1) NOT NULL,
    [UserName]         NVARCHAR (255) NULL,
    [SearchDateTime]   DATETIME       NOT NULL,
    [SearchBoxEntry]   NVARCHAR (255) NULL,
    [UrlVisited]       VARCHAR (255)  NULL,
    [Region]           VARCHAR (255)  DEFAULT ('us') NOT NULL,
    [SearchResultsQty] INT NULL DEFAULT NULL, 
    CONSTRAINT [PK_SearchActivity] PRIMARY KEY CLUSTERED ([SearchActivityID] ASC)
);

