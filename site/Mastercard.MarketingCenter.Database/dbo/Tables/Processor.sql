﻿CREATE TABLE [dbo].[Processor] (
    [ProcessorID]    VARCHAR (25)     NOT NULL,
    [Title]          NVARCHAR (255)   NULL,
    [CobrandLogo]    VARCHAR (MAX)    NULL,
    [Modified]       DATETIME         NULL,
    [CalendarTabs]   VARCHAR (1000)   NULL,
    [Special]        BIT              NULL,
    [UserRole]       VARCHAR (255)    NULL,
    CONSTRAINT [PK_Processor] PRIMARY KEY CLUSTERED ([ProcessorID] ASC),
);