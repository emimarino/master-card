﻿CREATE TABLE [dbo].[AnonymousTokenData]
(
	[AnonymousTokenId] BIGINT NOT NULL PRIMARY KEY IDENTITY(1,1), 
    [AnonymousToken] UNIQUEIDENTIFIER NOT NULL, 
    [UserId] INT NOT NULL, 
    [ClaimsTo] NVARCHAR(50) NULL, 
    [OptionalParameter] NCHAR(10) NULL, 
    [RegionId] NCHAR(6) NULL
)
