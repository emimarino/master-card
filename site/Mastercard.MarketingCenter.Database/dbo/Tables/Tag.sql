﻿CREATE TABLE [dbo].[Tag] (
	[TagID] VARCHAR(25) NOT NULL
	,[Identifier] VARCHAR(255) NOT NULL
	,[DisplayName] NVARCHAR(255) NULL
	,[RestrictAccess] BIT NULL
	,[Featured] BIT NULL
	,[Description] NVARCHAR(MAX) NULL
	,[PageContent] VARCHAR(MAX) NULL
	,[CreatedDate] DATETIME NOT NULL
	,[ModifiedDate] DATETIME NOT NULL
	,[HideFromSearch] BIT NULL
	,[SpecialAudienceAccess] VARCHAR(255) NULL
	,[RestrictToSpecialAudience] VARCHAR(255) NULL
	,CONSTRAINT [PK_Tag] PRIMARY KEY CLUSTERED ([TagID] ASC) WITH (FILLFACTOR = 90)
	,CONSTRAINT [UN_IDENTIFIER] UNIQUE NONCLUSTERED ([Identifier] ASC)
	);
GO

CREATE TRIGGER TagCacheInvalidator ON [dbo].[Tag]
AFTER INSERT
	,UPDATE
AS
IF EXISTS (
		SELECT 1
		FROM Cache
		WHERE Id = 'Tag'
		)
	UPDATE Cache
	SET LastInvalidationCalled = GETDATE()
		,MustInvalidate = 1
	WHERE Id = 'Tag'
ELSE
	INSERT INTO Cache (
		Id
		,LastInvalidationCalled
		,MustInvalidate
		)
	VALUES (
		'Tag'
		,GETDATE()
		,1
		)
GO

CREATE NONCLUSTERED INDEX [IX_Tag_Ids] ON [dbo].[Tag] (
	[TagID] ASC
	,[Identifier] ASC
	)
	WITH (
			PAD_INDEX = OFF
			,STATISTICS_NORECOMPUTE = OFF
			,SORT_IN_TEMPDB = OFF
			,DROP_EXISTING = OFF
			,ONLINE = OFF
			,ALLOW_ROW_LOCKS = OFF
			,ALLOW_PAGE_LOCKS = OFF
			)
GO