﻿CREATE TABLE [dbo].[UpcomingEvent] (
    [ContentItemId] VARCHAR (25)   NOT NULL,
    [Title]         NVARCHAR (255) NULL,
    [Location]      NVARCHAR (255) NULL,
    [Date]          DATETIME       NULL,
    [Link]          NVARCHAR (500) NULL,
    [Ordering]      INT            NOT NULL,
    [Image]         NVARCHAR(500)  NOT NULL
);



