﻿CREATE TABLE [dbo].[HtmlDownload] (
    [ContentItemID] VARCHAR (25)   NOT NULL,
    [Title]         NVARCHAR (255) NULL,
    [Url]           NVARCHAR (255) NOT NULL,
    [Filename]      VARCHAR (255)  NOT NULL,
    CONSTRAINT [PK_HtmlDownload] PRIMARY KEY CLUSTERED ([ContentItemID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_HtmlDownload_ContentItem] FOREIGN KEY ([ContentItemID]) REFERENCES [dbo].[ContentItem] ([ContentItemID])
);

