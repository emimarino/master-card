﻿CREATE TABLE [dbo].[DownloadableAssetEstimatedDistribution] (
    [Id]                    INT              IDENTITY (1, 1) NOT NULL,
    [ContentItemId]         VARCHAR (25)     NOT NULL,
    [ElectronicDeliveryId]  UNIQUEIDENTIFIER NULL,
    [EstimatedDistribution] INT              NULL,
    [UserId]                INT              NULL,
    [DownloadDate]          DATETIME         CONSTRAINT [DF_DownloadableAssetEstimatedDistribution_CreatedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_DownloadableAssetEstimatedDistribution] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_DownloadableAssetEstimatedDistribution_DownloadableAsset] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[DownloadableAsset] ([ContentItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_DownloadableAssetEstimatedDistribution_ElectronicDelivery] FOREIGN KEY ([ElectronicDeliveryId]) REFERENCES [dbo].[ElectronicDelivery] ([ElectronicDeliveryID]),
    CONSTRAINT [FK_DownloadableAssetEstimatedDistribution_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
);


GO
ALTER TABLE [dbo].[DownloadableAssetEstimatedDistribution] NOCHECK CONSTRAINT [FK_DownloadableAssetEstimatedDistribution_ElectronicDelivery];

