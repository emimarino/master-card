﻿CREATE TABLE [dbo].[UserRoleRegion] (
	[UserId]			INT NOT NULL,
    [RoleId]			INT NOT NULL,
	[RegionId]			NCHAR (6) NOT NULL,
    PRIMARY KEY CLUSTERED ([UserId] ASC, [RoleId] ASC, [RegionId] ASC),
	FOREIGN KEY ([UserId])			REFERENCES [dbo].[User] ([UserId]),
	FOREIGN KEY ([RoleId])			REFERENCES [dbo].[Role] ([RoleId]),
    FOREIGN KEY ([RegionId])		REFERENCES [dbo].[Region] ([Id])
);