﻿CREATE TABLE [dbo].[ContentItemVersion] (
    [ContentItemVersionId] BIGINT         IDENTITY (1, 1) NOT NULL,
    [ContentItemId]        VARCHAR (25)   NOT NULL,
    [SerializationData]    NVARCHAR (MAX) NOT NULL,
    [DateSaved]            DATETIME       CONSTRAINT [DF_ContentItemVersion_VersionSaved] DEFAULT (GETDATE()) NOT NULL,
    [LastModificationByUserId] INT NULL, 
    [LastModifyDate] DATETIME NULL, 
    CONSTRAINT [PK_ContentItemVersion] PRIMARY KEY CLUSTERED ([ContentItemVersionId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ContentItemVersion_ContentItem] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[ContentItem] ([ContentItemID])
);