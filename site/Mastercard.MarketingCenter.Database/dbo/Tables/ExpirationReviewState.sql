﻿CREATE TABLE [dbo].[ExpirationReviewState]
(
	[ReviewStateId] INT NOT NULL PRIMARY KEY  IDENTITY(1,1), 
    [Name] VARCHAR(255) NOT NULL,
	[Color] VARCHAR(255)  NULL
)