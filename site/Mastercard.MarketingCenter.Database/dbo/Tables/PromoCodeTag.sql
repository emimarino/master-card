﻿CREATE TABLE [dbo].[PromoCodeTag] (
    [PromoCodeID] VARCHAR (25) NOT NULL,
    [TagID]       VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_PromoCodeTag] PRIMARY KEY CLUSTERED ([PromoCodeID] ASC, [TagID] ASC),
    CONSTRAINT [FK_PromoCodeTag_PromoCode] FOREIGN KEY ([PromoCodeID]) REFERENCES [dbo].[PromoCode] ([PromoCodeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_PromoCodeTag_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([TagID]) ON DELETE CASCADE
);

