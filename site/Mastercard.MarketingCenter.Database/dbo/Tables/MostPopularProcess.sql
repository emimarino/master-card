﻿CREATE TABLE [dbo].[MostPopularProcess] (
    [Id]          BIGINT   IDENTITY (1, 1) NOT NULL,
    [ProcessDate] DATETIME NOT NULL,
    CONSTRAINT [PK_MostPopularProcess] PRIMARY KEY CLUSTERED ([Id] ASC)
);

