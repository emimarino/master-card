﻿CREATE TABLE [dbo].[EventLocationPositionHierarchy]
(
	[PositionId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [ParentPositionId] INT NULL, 
    [EventLocationId] INT NOT NULL, 
	CONSTRAINT [FK_EventLocationPositionHierarchy_EventLocation] FOREIGN KEY ([EventLocationId]) REFERENCES [dbo].[EventLocation] ([EventLocationId]) ON DELETE CASCADE,
)
