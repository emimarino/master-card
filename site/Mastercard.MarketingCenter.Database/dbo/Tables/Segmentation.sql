﻿CREATE TABLE [dbo].[Segmentation] (
    [SegmentationId]   VARCHAR (25)     NOT NULL,
    [SegmentationName] NVARCHAR (255)   NOT NULL,
    [RegionId]         NCHAR (6)        CONSTRAINT [DF_Segmentation_RegionId] DEFAULT (N'us') NOT NULL,
    CONSTRAINT [PK_Segmentation] PRIMARY KEY CLUSTERED ([SegmentationId] ASC),
    CONSTRAINT [FK_Segmentation_Region] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Region] ([Id])
);
GO
CREATE NONCLUSTERED INDEX IX_Segmentation ON [Segmentation] ( [RegionId], [SegmentationName])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO