﻿CREATE TABLE [dbo].[PromoCode] (
    [PromoCodeID]               VARCHAR (25)     NOT NULL,
    [PromotionCode]             VARCHAR (255)    NOT NULL,
    [StartDate]                 DATETIME         NOT NULL,
    [EndDate]                   DATETIME         NOT NULL,
    [Description]               NVARCHAR (MAX)   NULL,
    [DiscountAmount]            DECIMAL (18, 2)  NOT NULL,
    [DiscountType]              VARCHAR (255)    NOT NULL,
    [OrderMinimum]              DECIMAL (18, 2)  NOT NULL,
    [SingleUseOnlyPerIssuer]    BIT              NULL,
    [TotalFundingLimit]         DECIMAL (18, 2)  NULL,
    [TotalFundingCurrentAmount] DECIMAL (18, 2)  CONSTRAINT [DF_PromoCode_TotalFundingCurrentAmount] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_PromoCode] PRIMARY KEY CLUSTERED ([PromoCodeID] ASC),
);