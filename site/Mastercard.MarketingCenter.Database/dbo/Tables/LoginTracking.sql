﻿CREATE TABLE [dbo].[LoginTracking] (
    [LoginTrackingID] INT           IDENTITY (1, 1) NOT NULL,
    [UserName]        VARCHAR (255) NOT NULL,
    [Date]            DATETIME      NOT NULL,
    CONSTRAINT [PK_LoginTracking] PRIMARY KEY CLUSTERED ([LoginTrackingID] ASC)
);

