﻿CREATE TABLE [dbo].[OrderableAssetBrowseAllMaterialTag] (
    [ContentItemId] VARCHAR (25) NOT NULL,
    [TagID]         VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_OrderableAssetBrowseAllMaterialTag] PRIMARY KEY CLUSTERED ([ContentItemId] ASC, [TagID] ASC),
    CONSTRAINT [FK_OrderableAssetBrowseAllMaterialTag_OrderableAsset] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[OrderableAsset] ([ContentItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_OrderableAssetBrowseAllMaterialTag_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([TagID]) ON DELETE CASCADE
);

