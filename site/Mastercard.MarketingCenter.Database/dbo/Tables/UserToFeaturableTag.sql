﻿CREATE TABLE [dbo].[UserToFeaturableTag] (
    [UserID] INT          NOT NULL,
    [TagID]  VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_UserToFeaturableTag] PRIMARY KEY CLUSTERED ([UserID] ASC, [TagID] ASC),
    CONSTRAINT [FK_UserToFeaturableTag_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([TagID]) ON DELETE CASCADE,
    CONSTRAINT [FK_UserToFeaturableTag_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserId]) ON DELETE CASCADE
);

