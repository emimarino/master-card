﻿CREATE TABLE [dbo].[PricelessOffersProcess] (
    [Id]          BIGINT   IDENTITY (1, 1) NOT NULL,
    [ProcessStartDate] DATETIME NOT NULL,
    [ProcessEndDate] DATETIME NOT NULL,
    [OffersCreated] INT NOT NULL,
    [OffersUpdated] INT NOT NULL,
    [OffersDeleted] INT NOT NULL,
    [OffersUnmatched] INT NOT NULL,
    [OffersCreatedData] VARCHAR(MAX) NOT NULL,
    [OffersUpdatedData] VARCHAR(MAX) NOT NULL,
    [OffersDeletedData] VARCHAR(MAX) NOT NULL,
    [OffersUnmatchedData] VARCHAR(MAX) NOT NULL,
    CONSTRAINT [PK_PricelessOffersProcess] PRIMARY KEY CLUSTERED ([Id] ASC)
);