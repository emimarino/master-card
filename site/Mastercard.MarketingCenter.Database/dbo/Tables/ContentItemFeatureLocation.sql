﻿CREATE TABLE [dbo].[ContentItemFeatureLocation] (
    [ContentItemID]     VARCHAR (25) NOT NULL,
    [FeatureLocationID] VARCHAR (25) NOT NULL,
    [FeatureLevel]      INT          NOT NULL,
    CONSTRAINT [PK_ContentItemFeatureLocation] PRIMARY KEY CLUSTERED ([ContentItemID] ASC, [FeatureLocationID] ASC),
    CONSTRAINT [FK_ContentItemFeatureLocation_ContentItem] FOREIGN KEY ([ContentItemID]) REFERENCES [dbo].[ContentItem] ([ContentItemID]) ON DELETE CASCADE,
    CONSTRAINT [FK_ContentItemFeatureLocation_FeatureLocation] FOREIGN KEY ([FeatureLocationID]) REFERENCES [dbo].[FeatureLocation] ([FeatureLocationID]) ON DELETE CASCADE
);

