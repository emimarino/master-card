﻿CREATE TABLE [dbo].[AssessmentCriteria] (
    [ContentItemID]        VARCHAR (25)  NOT NULL,
    [Title]                VARCHAR (255) NOT NULL,
    [ShortDescription]     VARCHAR (MAX) NOT NULL,
    [AssessmentColumnName] VARCHAR (250) NOT NULL,
    CONSTRAINT [PK_AssessmentCriteria] PRIMARY KEY CLUSTERED ([ContentItemID] ASC),
    CONSTRAINT [FK_AssessmentCriteria_ContentItem] FOREIGN KEY ([ContentItemID]) REFERENCES [dbo].[ContentItem] ([ContentItemID])
);

