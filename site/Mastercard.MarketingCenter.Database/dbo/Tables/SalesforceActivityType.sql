﻿CREATE TABLE [dbo].[SalesforceActivityType] (
    [SalesforceActivityTypeId] INT IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (255) NOT NULL,
    [FieldName] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_SalesforceActivityType] PRIMARY KEY CLUSTERED ([SalesforceActivityTypeId] ASC) WITH (FILLFACTOR = 90)
);