﻿CREATE TABLE [dbo].[OrderItem] (
    [OrderItemID]           INT             IDENTITY (1, 1) NOT NULL,
    [OrderID]               INT             NOT NULL,
    [ShippingInformationID] INT             NULL,
    [ItemID]                NVARCHAR (50)   NULL,
    [ItemName]              NVARCHAR (1000) NULL,
    [ItemPrice]             DECIMAL (18, 2) NOT NULL,
    [ItemQuantity]          INT             NULL,
    [ProofPDF]              NVARCHAR (1000) NULL,
    [SKU]                   VARCHAR (255)   NULL,
    [Customizations]        BIT             NULL,
    [SubOrderID]            INT             NULL,
    [ElectronicDelivery]    BIT             CONSTRAINT [DF_OrderItems_ElectronicDelivery] DEFAULT ((0)) NOT NULL,
    [VDP]                   BIT             CONSTRAINT [DF_OrderItems_VDP] DEFAULT ((0)) NOT NULL,
    [ApproverEmailAddress]  NVARCHAR (255)  NULL,
    [Modified]              DATETIME        NULL,
    [EstimatedDistribution] INT             NULL,
    [HasAdvisorsTag]        BIT             DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_OrderItems] PRIMARY KEY CLUSTERED ([OrderItemID] ASC),
    CONSTRAINT [FK_OrderItems_SubOrders] FOREIGN KEY ([SubOrderID]) REFERENCES [dbo].[SubOrder] ([SubOrderID])
);


GO
CREATE NONCLUSTERED INDEX [ix_OrderItem_OrderId]
    ON [dbo].[OrderItem]([OrderID] ASC)
    INCLUDE([ItemID], [ItemName], [ItemPrice], [ItemQuantity], [ElectronicDelivery], [EstimatedDistribution], [HasAdvisorsTag]) WITH (FILLFACTOR = 90);

