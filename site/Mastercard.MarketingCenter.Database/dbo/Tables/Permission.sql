﻿CREATE TABLE [dbo].[Permission] (
    [PermissionId]		INT				IDENTITY (1, 1) NOT NULL,
	[Group]				NVARCHAR (50)	NOT NULL,
    [Name]				NVARCHAR (50)	NOT NULL,
	[Description]		NVARCHAR (MAX)	NOT NULL,
    CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED ([PermissionId] ASC)
);