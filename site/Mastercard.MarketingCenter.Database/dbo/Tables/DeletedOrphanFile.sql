﻿CREATE TABLE [dbo].[DeletedOrphanFile] (
    [DeletedOrphanFileId] INT           IDENTITY (1, 1) NOT NULL,
    [FolderKey]           VARCHAR (50)  NOT NULL,
    [FileFullName]        VARCHAR (MAX) NOT NULL,
    [FileDeletedDate]     DATETIME      NOT NULL,
    CONSTRAINT [PK_DeletedOrphanFile] PRIMARY KEY CLUSTERED ([DeletedOrphanFileId] ASC)
);

