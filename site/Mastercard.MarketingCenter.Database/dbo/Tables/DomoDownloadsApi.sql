﻿CREATE TABLE [dbo].[DomoDownloadsApi] (
	[Id] INT NOT NULL PRIMARY KEY Identity(1, 1)
	,[ContentItemId] VARCHAR(25)
	,[ContentItemTitle] NVARCHAR(255)
	,[ContentItemRegion] NCHAR(6)
	,[ContentTypeId] VARCHAR(25)
	,[ContentTypeTitle] VARCHAR(255)
	,[UserRegion] NCHAR(6)
	,[DownloadDate] DATETIME
	,[FileName] NVARCHAR(3000)
	,[SiteTrackingId] INT
	,[VisitId] INT
	,[Country] NVARCHAR(255)
	,[UserEmail] NVARCHAR(255)
	,[UserFinancialInstitution] NVARCHAR(255)
	,[UserFinancialInstitutionID] VARCHAR(25)
	,[UserFinancialInstitutionCID] VARCHAR(50)
	,[UserId] INT DEFAULT 0
	,[HiddenResult] BIT
	,[BelongsToED] NCHAR(10) NULL
	,[UserFinancialInstitutionImportedName] NCHAR(255) NULL
	,[UserFirstName] NCHAR(255) NULL
	,[UserLastName] NCHAR(255) NULL
	)