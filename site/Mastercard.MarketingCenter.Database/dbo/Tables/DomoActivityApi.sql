﻿CREATE TABLE [dbo].[DomoActivityApi] (
	[Id] BIGINT IDENTITY(1, 1) NOT NULL
	,[SiteTrackingId] BIGINT NULL
	,[VisitId] BIGINT NULL
	,[User_UserId] INT DEFAULT 0
	,[User_LastName] NVARCHAR(255) NULL
	,[User_FirstName] NVARCHAR(255) NULL
	,[User_Email] NVARCHAR(256) NULL
	,[User_Region] NCHAR(6) NULL
	,[User_Country] NVARCHAR(255) NULL
	,[User_IssuerName] NVARCHAR(255) NULL
	,[User_ProcessorName] NVARCHAR(255) NULL
	,[When] DATETIME NULL
	,[From] NVARCHAR(MAX) NULL
	,[How] VARCHAR(1000) NULL
	,[Content_Type] VARCHAR(25) NULL
	,[Content_Id] VARCHAR(25) NULL
	,[Content_Title] NVARCHAR(255) NULL
	,[Content_RegionName] NCHAR(6) NULL
	,[Hidden_Result] BIT NOT NULL DEFAULT 0
	,[User_IssuerImportedName] NVARCHAR(255) NULL
	,[User_IssuerCID] VARCHAR(50) NULL
	,[IsArchived] BIT DEFAULT((0)) NOT NULL
	,PRIMARY KEY CLUSTERED ([Id] ASC)
	);
GO

CREATE INDEX [IX_DOMOACTIVITY_CONTENT] ON [DomoActivityApi] ([Content_Type]);