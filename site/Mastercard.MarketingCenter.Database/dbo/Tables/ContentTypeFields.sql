﻿CREATE TABLE [dbo].[ContentTypeFields] (
    [ContentTypeId]					VARCHAR (25)	NOT NULL,
    [FieldDefinitionId]				INT				NOT NULL,
    [Order]							INT				CONSTRAINT [DF_ContentTypeFields_Order] DEFAULT ((0)) NOT NULL,
    [VisibleInList]					BIT				CONSTRAINT [DF_ContentTypeFields_VisibleInList] DEFAULT ((0)) NOT NULL,
    [HiddenInNew]					BIT				CONSTRAINT [DF_ContentTypeFields_HiddenInNew] DEFAULT ((0)) NOT NULL,
    [Searcheable]					BIT				CONSTRAINT [DF_ContentTypeFields_Searcheable] DEFAULT ((0)) NOT NULL,
    [DefaultOrder]					VARCHAR (10)	NULL,
    [Permission]					VARCHAR (MAX)	NULL,
    [HiddenInHistory]				BIT				DEFAULT ((0)) NOT NULL,
    [InterRegionCloning]			BIT				CONSTRAINT [ContentTypeFields_InterRegionCloning_Default] DEFAULT ((1)) NOT NULL,
    [BusinessOwnerOrder]			INT				NULL,
    [EnabledBySetting]				VARCHAR (MAX)	NULL,
    [Clonable]						BIT				NULL,
	[ReadOnlyInCrossBorderRegion]	BIT				DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ContentTypeFields] PRIMARY KEY CLUSTERED ([ContentTypeId] ASC, [FieldDefinitionId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ContentTypeFields_ContentType] FOREIGN KEY ([ContentTypeId]) REFERENCES [dbo].[ContentType] ([ContentTypeID]),
    CONSTRAINT [FK_ContentTypeFields_FieldDefinition] FOREIGN KEY ([FieldDefinitionId]) REFERENCES [dbo].[FieldDefinition] ([FieldDefinitionId])
);