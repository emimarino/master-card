﻿CREATE TABLE [dbo].[AssetFullWidthBrowseAllMaterialTag] (
    [ContentItemId] VARCHAR (25) NOT NULL,
    [TagID]         VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_AssetFullWidthBrowseAllMaterialTag] PRIMARY KEY CLUSTERED ([ContentItemId] ASC, [TagID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_AssetFullWidthBrowseAllMaterialTag_AssetFullWidth] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[AssetFullWidth] ([ContentItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_AssetFullWidthBrowseAllMaterialTag_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([TagID]) ON DELETE CASCADE
);

