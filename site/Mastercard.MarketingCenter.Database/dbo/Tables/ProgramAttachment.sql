﻿CREATE TABLE [dbo].[ProgramAttachment] (
    [AttachmentID]  INT             IDENTITY (1, 1) NOT NULL,
    [ContentItemId] VARCHAR (25)    NOT NULL,
    [FileUrl]       NVARCHAR (2000) NULL,
    CONSTRAINT [PK_ProgramAttachment] PRIMARY KEY CLUSTERED ([AttachmentID] ASC),
    CONSTRAINT [FK_ProgramAttachment_Program] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[Program] ([ContentItemId]) ON DELETE CASCADE
);



