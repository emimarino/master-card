﻿CREATE TABLE [dbo].[Offer] (
    [ContentItemId]                 VARCHAR (25)   NOT NULL,
    [Title]                         NVARCHAR (255) NOT NULL,
	[BusinessOwnerID]               INT            NOT NULL,
	[Evergreen]						BIT            NOT NULL,
	[ValidityPeriodFromDate]		DATETIME	   NULL,
	[ValidityPeriodToDate]			DATETIME	   NULL,
	[EventDate]						DATETIME	   NULL,
	[BookByDate]					DATETIME	   NULL,
	[MainImage]                     NVARCHAR (255) NOT NULL,
	[ShortDescription]              NVARCHAR (MAX) NOT NULL,
	[LongDescription]               NVARCHAR (MAX) NOT NULL,
	[TermsAndConditions]			NVARCHAR (MAX) NULL,
	[PreviewFiles]					BIT			   NOT NULL,
	[DownloadZipFile]		        NVARCHAR (255) NULL,
	[PricelessOfferId]				NVARCHAR(MAX)  NULL,
	[LogoImage]                     NVARCHAR (255) NULL,
	[OfferTypeId]					NVARCHAR (25)  NULL,
	[ProductUrl]					NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Offer] PRIMARY KEY CLUSTERED ([ContentItemId] ASC) WITH (FILLFACTOR = 90),
	CONSTRAINT [FK_Offer_BusinessOwner] FOREIGN KEY (BusinessOwnerID) REFERENCES [dbo].[User] ([UserId]),
	CONSTRAINT [FK_Offer_OfferType] FOREIGN KEY (OfferTypeId) REFERENCES [dbo].[OfferType] ([OfferTypeId])
);