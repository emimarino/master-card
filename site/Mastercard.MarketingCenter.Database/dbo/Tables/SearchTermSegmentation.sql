﻿CREATE TABLE [dbo].[SearchTermSegmentation] (
    [SearchTermID]   VARCHAR (25) NOT NULL,
    [SegmentationId] VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_SearchTermSegmentation] PRIMARY KEY CLUSTERED ([SearchTermID] ASC, [SegmentationId] ASC),
    CONSTRAINT [FK_SearchTermSegmentation_SearchTerm] FOREIGN KEY ([SearchTermID]) REFERENCES [dbo].[SearchTerm] ([SearchTermID]),
    CONSTRAINT [FK_SearchTermSegmentation_Segmentation] FOREIGN KEY ([SegmentationId]) REFERENCES [dbo].[Segmentation] ([SegmentationId])
);

