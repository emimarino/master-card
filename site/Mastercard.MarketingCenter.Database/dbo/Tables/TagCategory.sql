﻿CREATE TABLE [dbo].[TagCategory] (
    [TagCategoryID]  VARCHAR (25)     NOT NULL,
    [Title]          VARCHAR (255)    NOT NULL,
    [ListLocation]   VARCHAR (255)    NULL,
    [TagBrowser]     VARCHAR (255)    NOT NULL,
    [DisplayOrder]   INT              NOT NULL,
    [Featurable]     VARCHAR (255)    NULL,
    [IsRelatable]    BIT              NOT NULL,
    CONSTRAINT [PK_TagCategory] PRIMARY KEY CLUSTERED ([TagCategoryID] ASC),
);

GO

CREATE TRIGGER TagCategoryCacheInvalidator ON [dbo].[TagCategory] AFTER INSERT, UPDATE
AS
	IF EXISTS (SELECT 1 FROM Cache WHERE Id = 'Tag')
		UPDATE Cache SET LastInvalidationCalled = GETDATE(), MustInvalidate = 1 WHERE Id = 'Tag'
	ELSE	
		INSERT INTO Cache (Id, LastInvalidationCalled, MustInvalidate) VALUES ('Tag', GETDATE(), 1)