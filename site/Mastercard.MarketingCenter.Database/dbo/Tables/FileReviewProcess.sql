﻿CREATE TABLE [dbo].[FileReviewProcess] (
	[Id] BIGINT IDENTITY(1, 1) NOT NULL
	,[Folder] VARCHAR(MAX) NOT NULL
	,[ProcessDate] DATETIME NOT NULL
	,[FilesReviewed] INT NOT NULL
	,[FilesMatched] INT NOT NULL
	,[FilesUnmatched] INT NOT NULL
	,[FilesReferenced] INT NOT NULL
	,CONSTRAINT [PK_FileReviewProcess] PRIMARY KEY CLUSTERED ([Id] ASC)
);