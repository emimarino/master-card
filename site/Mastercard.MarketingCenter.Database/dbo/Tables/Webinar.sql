﻿CREATE TABLE [dbo].[Webinar] (
    [ContentItemId]             VARCHAR (25)   NOT NULL,
    [Title]                     NVARCHAR (255) NULL,
    [ShortDescription]          NVARCHAR (MAX) NULL,
    [ThumbnailImage]            NVARCHAR(255)  NOT NULL,
    [LongDescription]           NVARCHAR (MAX) NULL,
    [ShowInCrossSellBox]        VARCHAR (255)  NULL,
    [PriorityLevel]             VARCHAR (255)  NULL,
    [ShowInTagBrowser]          BIT            NULL,
    [Date]                      DATETIME       NULL,
    [Link]                      NVARCHAR (255) NULL,
    [LinkText]                  NVARCHAR (255) NULL,
    [RelatedItems]              VARCHAR (255)  NULL,
    [IconID]                    VARCHAR (25)   NOT NULL,
    [AssetType]                 VARCHAR (255)  DEFAULT ('') NOT NULL,
    [SpecialAudienceAccess]     VARCHAR (255)  NULL,
    [RestrictToSpecialAudience] VARCHAR (255)  NULL,
    [VideoFile]                 VARCHAR (255)  NULL,
    [StaticVideoImage]          VARCHAR (255)  NULL,
    [ShowImageInsteadVideo]     BIT            NULL,
    [EnableFavoriting]          BIT            CONSTRAINT [DF_Webinar_EnableFavoriting] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Webinar] PRIMARY KEY CLUSTERED ([ContentItemId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Webinar_Icon0] FOREIGN KEY ([IconID]) REFERENCES [dbo].[Icon] ([IconID]) ON DELETE CASCADE
);



