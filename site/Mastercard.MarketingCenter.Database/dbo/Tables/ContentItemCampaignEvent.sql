﻿CREATE TABLE [dbo].[ContentItemCampaignEvent] (
    [ContentItemId] VARCHAR (25) NOT NULL,
    [CampaignEventId]         NVARCHAR (25) NOT NULL,
    CONSTRAINT [PK_ContentItemCampaignEvent] PRIMARY KEY CLUSTERED ([ContentItemId] ASC, [CampaignEventId] ASC),
    CONSTRAINT [FK_ContentItemCampaignEvent_ContentItem] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[ContentItem] ([ContentItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_ContentItemCampaignEvent_CampaignEvent] FOREIGN KEY ([CampaignEventId]) REFERENCES [dbo].[CampaignEvent] ([CampaignEventId]) ON DELETE CASCADE
);

