﻿CREATE TABLE [dbo].[OrderableAssetAttachment] (
    [AttachmentID]  INT             IDENTITY (1, 1) NOT NULL,
    [ContentItemId] VARCHAR (25)    NOT NULL,
    [FileUrl]       NVARCHAR (2000) NULL,
    CONSTRAINT [PK_OrderableAssetAttachment] PRIMARY KEY CLUSTERED ([AttachmentID] ASC),
    CONSTRAINT [FK_OrderableAssetAttachment_OrderableAsset] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[OrderableAsset] ([ContentItemId]) ON DELETE CASCADE
);



