﻿CREATE TABLE [dbo].[DownloadableAssetBrowseAllMaterialTag] (
    [ContentItemId] VARCHAR (25) NOT NULL,
    [TagID]         VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_DownloadableAssetBrowseAllMaterialTag] PRIMARY KEY CLUSTERED ([ContentItemId] ASC, [TagID] ASC),
    CONSTRAINT [FK_DownloadableAssetBrowseAllMaterialTag_DownloadableAsset] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[DownloadableAsset] ([ContentItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_DownloadableAssetBrowseAllMaterialTag_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([TagID]) ON DELETE CASCADE
);

