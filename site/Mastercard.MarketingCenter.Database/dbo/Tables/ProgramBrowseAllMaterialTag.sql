﻿CREATE TABLE [dbo].[ProgramBrowseAllMaterialTag] (
    [ContentItemId] VARCHAR (25) NOT NULL,
    [TagID]         VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_ProgramBrowseAllMaterialTag] PRIMARY KEY CLUSTERED ([ContentItemId] ASC, [TagID] ASC),
    CONSTRAINT [FK_ProgramBrowseAllMaterialTag_Program] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[Program] ([ContentItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_ProgramBrowseAllMaterialTag_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([TagID]) ON DELETE CASCADE
);

