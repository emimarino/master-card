﻿CREATE TABLE [dbo].[ContentItemFeatureOnTag] (
    [ContentItemID] VARCHAR (25) NOT NULL,
    [TagID]         VARCHAR (25) NOT NULL,
    [FeatureLevel]  INT          NOT NULL,
    CONSTRAINT [PK_ContentItemFeatureOnTag] PRIMARY KEY CLUSTERED ([ContentItemID] ASC, [TagID] ASC),
    CONSTRAINT [FK_ContentItemFeatureOnTag_ContentItem] FOREIGN KEY ([ContentItemID]) REFERENCES [dbo].[ContentItem] ([ContentItemID]) ON DELETE CASCADE,
    CONSTRAINT [FK_ContentItemFeatureOnTag_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([TagID]) ON DELETE CASCADE
);

