﻿CREATE TABLE [dbo].[DomainBlacklist] (
    [DomainBlacklistID] VARCHAR (25)     NOT NULL,
    [Domain]            VARCHAR (255)    NOT NULL,
    CONSTRAINT [PK_DomainBlacklist] PRIMARY KEY CLUSTERED ([DomainBlacklistID] ASC),
);