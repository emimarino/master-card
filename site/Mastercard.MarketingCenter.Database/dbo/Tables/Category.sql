﻿CREATE TABLE [dbo].[Category] (
	[CategoryId] NVARCHAR(25) NOT NULL
	,[Title] NVARCHAR(255) NOT NULL
	,[Description] NVARCHAR(MAX) NULL
	,[Image] NVARCHAR(255) NULL
	,[PricelessCategoryId] INT NULL
	,CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED ([CategoryId] ASC)
	);