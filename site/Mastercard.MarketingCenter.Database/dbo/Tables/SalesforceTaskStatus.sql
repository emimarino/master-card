﻿CREATE TABLE [dbo].[SalesforceTaskStatus] (
    [SalesforceTaskStatusId] INT IDENTITY (1, 1) NOT NULL,
    [Name]                   VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_SalesforceTaskStatus] PRIMARY KEY CLUSTERED ([SalesforceTaskStatusId] ASC) WITH (FILLFACTOR = 90)
);