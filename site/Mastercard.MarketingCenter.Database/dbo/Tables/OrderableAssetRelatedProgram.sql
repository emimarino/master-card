﻿CREATE TABLE [dbo].[OrderableAssetRelatedProgram] (
    [OrderableAssetContentItemId] VARCHAR (25) NOT NULL,
    [ProgramContentItemId]        VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_OrderableAssetRelatedProgram] PRIMARY KEY CLUSTERED ([OrderableAssetContentItemId] ASC, [ProgramContentItemId] ASC),
    CONSTRAINT [FK_OrderableAssetRelatedProgram_OrderableAsset] FOREIGN KEY ([OrderableAssetContentItemId]) REFERENCES [dbo].[OrderableAsset] ([ContentItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_OrderableAssetRelatedProgram_Program] FOREIGN KEY ([ProgramContentItemId]) REFERENCES [dbo].[Program] ([ContentItemId]) ON DELETE CASCADE
);

