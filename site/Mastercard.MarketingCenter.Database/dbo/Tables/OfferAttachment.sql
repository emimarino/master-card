﻿CREATE TABLE [dbo].[OfferAttachment] (
    [AttachmentId]  INT             IDENTITY (1, 1) NOT NULL,
    [ContentItemId] VARCHAR (25)    NOT NULL,
    [FileUrl]       NVARCHAR (2000) NULL,
    CONSTRAINT [PK_OfferAttachment] PRIMARY KEY CLUSTERED ([AttachmentID] ASC),
    CONSTRAINT [FK_OfferAttachment_Offer] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[Offer] ([ContentItemId]) ON DELETE CASCADE
);