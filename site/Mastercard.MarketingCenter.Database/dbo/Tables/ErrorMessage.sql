﻿CREATE TABLE ErrorMessage (
	[ListId] INT
	,[EventName] NVARCHAR(25)
	,[Message] NVARCHAR(255)
	,CONSTRAINT [PK_Error_List] PRIMARY KEY (
		[ListId]
		,[EventName]
		)
	)