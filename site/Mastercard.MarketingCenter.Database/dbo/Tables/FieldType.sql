﻿CREATE TABLE [dbo].[FieldType] (
    [FieldTypeCode]     VARCHAR (20) NOT NULL,
    [Name]              VARCHAR (50) NOT NULL,
    [Description]       NCHAR (10)   NULL,
    [ShouldContainData] BIT          NULL,
    CONSTRAINT [PK_FieldType] PRIMARY KEY CLUSTERED ([FieldTypeCode] ASC) WITH (FILLFACTOR = 90)
);