﻿CREATE TABLE [dbo].[CustomizationDetail] (
    [CustomizationDetailID] INT            IDENTITY (1, 1) NOT NULL,
    [OrderID]               INT            NULL,
    [ShoppingCartID]        INT            NULL,
    [CustomizationOptionID] VARCHAR (25)   NOT NULL,
    [CustomizationData]     NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_CustomizationDetail] PRIMARY KEY CLUSTERED ([CustomizationDetailID] ASC),
    CONSTRAINT [FK_CustomizationDetail_CustomizationOption] FOREIGN KEY ([CustomizationOptionID]) REFERENCES [dbo].[CustomizationOption] ([CustomizationOptionID])
);

