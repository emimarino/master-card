﻿CREATE TABLE [dbo].[UserFeatureLocation] (
    [UserID]            INT          NOT NULL,
    [FeatureLocationID] VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_UserFeatureLocation] PRIMARY KEY CLUSTERED ([UserID] ASC, [FeatureLocationID] ASC),
    CONSTRAINT [FK_UserFeatureLocation_FeatureLocation] FOREIGN KEY ([FeatureLocationID]) REFERENCES [dbo].[FeatureLocation] ([FeatureLocationID]) ON DELETE CASCADE,
    CONSTRAINT [FK_UserFeatureLocation_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserId]) ON DELETE CASCADE
);

