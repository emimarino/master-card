﻿CREATE TABLE [dbo].[ContentItemTag] (
	[ContentItemID] VARCHAR(25) NOT NULL
	,[TagID] VARCHAR(25) NOT NULL
	,CONSTRAINT [PK_ContentItemTag] PRIMARY KEY CLUSTERED (
		[ContentItemID] ASC
		,[TagID] ASC
		)
	,CONSTRAINT [FK_ContentItemTag_ContentItem] FOREIGN KEY ([ContentItemID]) REFERENCES [dbo].[ContentItem]([ContentItemID]) ON DELETE CASCADE
	,CONSTRAINT [FK_ContentItemTag_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag]([TagID]) ON DELETE CASCADE
	);
GO