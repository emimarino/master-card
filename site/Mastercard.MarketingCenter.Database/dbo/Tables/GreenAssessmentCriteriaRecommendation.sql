﻿CREATE TABLE [dbo].[GreenAssessmentCriteriaRecommendation] (
    [ContentItemID]    VARCHAR (25) NOT NULL,
    [RecommendationID] VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_GreenAssessmentCriteriaRecommendation] PRIMARY KEY CLUSTERED ([ContentItemID] ASC, [RecommendationID] ASC),
    CONSTRAINT [FK_GreenAssessmentCriteriaRecommendation_AssessmentCriteria] FOREIGN KEY ([ContentItemID]) REFERENCES [dbo].[AssessmentCriteria] ([ContentItemID]),
    CONSTRAINT [FK_GreenAssessmentCriteriaRecommendation_Recommendation] FOREIGN KEY ([RecommendationID]) REFERENCES [dbo].[Recommendation] ([ContentItemId])
);

