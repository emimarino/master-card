﻿CREATE TABLE [dbo].[AssessmentReport] (
    [CID]                VARCHAR (25) NOT NULL,
    [Activation]         CHAR (1)     NOT NULL,
    [FraudPrevention]    CHAR (1)     NOT NULL,
    [Authorization]      CHAR (1)     NOT NULL,
    [ImproveAcquisition] CHAR (1)     CONSTRAINT [DF_AssessmentReport_ImproveAcquisition] DEFAULT ('R') NOT NULL,
    [ProductGraduation]  CHAR (1)     CONSTRAINT [DF_AssessmentReport_ProductGraduation] DEFAULT ('R') NOT NULL,
    CONSTRAINT [PK_AssessmentReport] PRIMARY KEY CLUSTERED ([CID] ASC) WITH (FILLFACTOR = 90)
);

