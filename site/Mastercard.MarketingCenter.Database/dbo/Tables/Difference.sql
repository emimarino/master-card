﻿CREATE TABLE [dbo].[Difference] (
    [ContentItemID]               VARCHAR (25) NOT NULL,
    [MSdifftool_ErrorCode]        SMALLINT     NULL,
    [MSdifftool_ErrorDescription] VARCHAR (50) NULL,
    [MSdifftool_FixSQL]           NTEXT        NULL,
    [MSdifftool_OffendingColumns] NTEXT        NULL
);

