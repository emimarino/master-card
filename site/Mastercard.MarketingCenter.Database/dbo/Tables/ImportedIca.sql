﻿CREATE TABLE [dbo].[ImportedIca] (
    [ImportedIcaId]  INT            IDENTITY (1, 1) NOT NULL,
    [StatusCode]     NVARCHAR (255) NULL,
    [CID]            VARCHAR (50)   NULL,
    [LegalName]      NVARCHAR (255) NULL,
    [HQAddress]      NVARCHAR (255) NULL,
    [HQCityName]     NVARCHAR (255) NULL,
    [HQPostalCode]   NVARCHAR (255) NULL,
    [HQProvinceName] NVARCHAR (255) NULL,
    [HQCountryCode]  NVARCHAR (255) NULL,
    [HQCountryName]  NVARCHAR (255) NULL,
    [AffiliateBIN]   VARCHAR (50)   NULL,
    CONSTRAINT [PK_ImportedICAs] PRIMARY KEY CLUSTERED ([ImportedIcaId] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20160421-102336]
    ON [dbo].[ImportedIca]([CID] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20160421-102458]
    ON [dbo].[ImportedIca]([LegalName] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20160421-102541]
    ON [dbo].[ImportedIca]([HQCityName] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20160421-102605]
    ON [dbo].[ImportedIca]([HQProvinceName] ASC) WITH (FILLFACTOR = 90);

