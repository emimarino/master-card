﻿CREATE TABLE [dbo].[TagHierarchyPosition] (
    [PositionID]       INT          IDENTITY (1, 1) NOT NULL,
    [ParentPositionID] INT          NULL,
    [TagID]            VARCHAR (25) NOT NULL,
    [TagCategoryID]    VARCHAR (25) NOT NULL,
    [SortOrder]        INT          NOT NULL,
    CONSTRAINT [PK_TagHierarchyPosition] PRIMARY KEY CLUSTERED ([PositionID] ASC),
    CONSTRAINT [FK_TagHierarchyPosition_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([TagID]) ON DELETE CASCADE,
    CONSTRAINT [FK_TagHierarchyPosition_TagCategory] FOREIGN KEY ([TagCategoryID]) REFERENCES [dbo].[TagCategory] ([TagCategoryID]) ON DELETE CASCADE,
    CONSTRAINT [FK_TagHierarchyPosition_self] FOREIGN KEY  (ParentPositionID) REFERENCES TagHierarchyPosition(PositionID)
);




GO

CREATE TRIGGER TagHierarchyPositionCacheInvalidator ON [dbo].[TagHierarchyPosition] AFTER INSERT, UPDATE
AS
	
	IF EXISTS (SELECT 1 FROM Cache WHERE Id = 'Tag')
		UPDATE Cache SET LastInvalidationCalled = GETDATE(), MustInvalidate = 1 WHERE Id = 'Tag'
	ELSE	
		INSERT INTO Cache (Id, LastInvalidationCalled, MustInvalidate) VALUES ('Tag', GETDATE(), 1)

