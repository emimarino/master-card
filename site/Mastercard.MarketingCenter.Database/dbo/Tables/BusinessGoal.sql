﻿CREATE TABLE [dbo].[BusinessGoal] (
    [ContentItemId]        VARCHAR (25)  NOT NULL,
    [DisplayName]          VARCHAR (255) NOT NULL,
    [AssessmentColumnName] VARCHAR (250) NOT NULL,
    CONSTRAINT [PK_BusinessGoal] PRIMARY KEY CLUSTERED ([ContentItemId] ASC) WITH (FILLFACTOR = 90)
);

