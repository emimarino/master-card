﻿CREATE TABLE [dbo].[ContentItemRegionVisibility] (
	[ContentItemId]	VARCHAR (25)	NOT NULL,
    [RegionId]		NCHAR (6)		NOT NULL,
	[Value]			INT				NOT NULL,
	CONSTRAINT [PK_ContentItemRegionVisibility] PRIMARY KEY (RegionId, ContentItemId),
    CONSTRAINT [FK_ContentItemRegionVisibility_Region] FOREIGN KEY ([RegionId]) REFERENCES [Region](Id),
	CONSTRAINT [FK_ContentItemRegionVisibility_ContentItem] FOREIGN KEY ([ContentItemId]) REFERENCES [ContentItem](ContentItemID)
)