﻿CREATE TABLE [dbo].[Icon] (
    [IconID]    VARCHAR (25)     NOT NULL,
    [Name]      VARCHAR (255)    NOT NULL,
    [Extension] VARCHAR (255)    NOT NULL,
    [Icon]      VARCHAR (255)    NOT NULL,
    CONSTRAINT [PK_Icon] PRIMARY KEY CLUSTERED ([IconID] ASC),
);