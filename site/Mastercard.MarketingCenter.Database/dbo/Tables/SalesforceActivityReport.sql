﻿CREATE TABLE [dbo].[SalesforceActivityReport]
(
	[SalesforceActivityReportId] INT NOT NULL PRIMARY KEY IDENTITY,
    [StartDate] DATETIME NOT NULL,
    [EndDate] DATETIME NULL,
    [Sent] INT NOT NULL DEFAULT 0,
    [Failed] INT NOT NULL DEFAULT 0,
    [IsSuccessed] BIT NOT NULL DEFAULT 0,
    [SalesforceTokenId] VARCHAR(MAX) NULL,
    [LoggingEventId] VARCHAR(38) NULL
)