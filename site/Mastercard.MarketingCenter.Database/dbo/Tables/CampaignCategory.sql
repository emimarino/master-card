﻿CREATE TABLE [dbo].[CampaignCategory] (
    [CampaignCategoryId]    NVARCHAR (25)    NOT NULL,
    [Title]             NVARCHAR (255)   NULL,
    [OrderNumber]       INT              NULL,
    [RegionId]          NCHAR (6)        NOT NULL,
    [CreatedDate]       DATETIME         NOT NULL,
    [CreatedByUserID]   INT              NOT NULL,
    [ModifiedDate]      DATETIME         NOT NULL,
    [ModifiedByUserID]  INT              NOT NULL,
    [MarketingCalendarUrl]  VARCHAR (255)    NULL,
    CONSTRAINT [PK_CampaignCategory] PRIMARY KEY CLUSTERED ([CampaignCategoryId] ASC),
    CONSTRAINT [FK_CampaignCategory_Region] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Region] ([Id]),
    CONSTRAINT [FK_CampaignCategory_User1] FOREIGN KEY ([CreatedByUserID]) REFERENCES [dbo].[User] ([UserId]),
    CONSTRAINT [FK_CampaignCategory_User2] FOREIGN KEY ([ModifiedByUserID]) REFERENCES [dbo].[User] ([UserId])
);