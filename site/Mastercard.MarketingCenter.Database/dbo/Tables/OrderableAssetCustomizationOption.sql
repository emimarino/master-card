﻿CREATE TABLE [dbo].[OrderableAssetCustomizationOption] (
    [ContentItemID]         VARCHAR (25) NOT NULL,
    [CustomizationOptionID] VARCHAR (25) NOT NULL,
    [Required]              BIT          NOT NULL,
    CONSTRAINT [PK_OrderableAssetCustomizationOption] PRIMARY KEY CLUSTERED ([ContentItemID] ASC, [CustomizationOptionID] ASC),
    CONSTRAINT [FK_OrderableAssetCustomization_CustomizationOption] FOREIGN KEY ([CustomizationOptionID]) REFERENCES [dbo].[CustomizationOption] ([CustomizationOptionID]) NOT FOR REPLICATION,
    CONSTRAINT [FK_OrderableAssetCustomization_OrderableAsset] FOREIGN KEY ([ContentItemID]) REFERENCES [dbo].[OrderableAsset] ([ContentItemId]) NOT FOR REPLICATION
);


GO
ALTER TABLE [dbo].[OrderableAssetCustomizationOption] NOCHECK CONSTRAINT [FK_OrderableAssetCustomization_CustomizationOption];


GO
ALTER TABLE [dbo].[OrderableAssetCustomizationOption] NOCHECK CONSTRAINT [FK_OrderableAssetCustomization_OrderableAsset];

