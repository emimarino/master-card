﻿CREATE TABLE [dbo].[PendingRegistrations] (
    [PendingRegistrationID]      INT              IDENTITY (1, 1) NOT NULL,
    [FirstName]                  NVARCHAR (255)   NULL,
    [LastName]                   NVARCHAR (255)   NULL,
    [FinancialInstitution]       NVARCHAR (255)   NULL,
    [Phone]                      NVARCHAR (255)   NULL,
    [Fax]                        NVARCHAR (255)   NULL,
    [Email]                      NVARCHAR (255)   NULL,
    [Address1]                   NVARCHAR (255)   NULL,
    [Address2]                   NVARCHAR (255)   NULL,
    [Address3]                   NVARCHAR (255)   NULL,
    [City]                       NVARCHAR (255)   NULL,
    [StateID]                    INT              NOT NULL,
    [Zip]                        NVARCHAR (255)   NULL,
    [DateAdded]                  DATETIME         NOT NULL,
    [GUID]                       UNIQUEIDENTIFIER NOT NULL,
    [Status]                     VARCHAR (255)    NOT NULL,
    [DateStatusChanged]          DATETIME         NULL,
    [RejectionReason]            NVARCHAR (MAX)   NULL,
    [StatusChangedBy]            VARCHAR (255)    NULL,
    [Title]                      NVARCHAR (255)   NULL,
    [Processor]                  NVARCHAR (255)   NULL,
    [RegionId]                   NCHAR (6)        CONSTRAINT [DF_PendingRegistrations_RegionId] DEFAULT (N'us') NULL,
    [Language]                   NVARCHAR (255)   NULL,
    [Country]                    NVARCHAR (255)   NULL,
    [Mobile]                     NVARCHAR (255)   NULL,
    [Province]                   NVARCHAR (255)   NULL,
    [AcceptedConsents]           NCHAR (255)      NULL,
    [AcceptedEmailSubscriptions] BIT              CONSTRAINT [DF_PendingRegistrations_AcceptedEmailSubscriptions] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_PendingRegistrations] PRIMARY KEY CLUSTERED ([PendingRegistrationID] ASC),
    CONSTRAINT [FK_PendingRegistrations_Region] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Region] ([Id]),
    CONSTRAINT [FK_PendingRegistrations1] FOREIGN KEY ([StateID]) REFERENCES [dbo].[State] ([StateID])
);




GO
EXECUTE sp_addextendedproperty @name = N'NonUsStateOrProvince', @value = N'the State or Province Name is for Non Us Mainly', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'PendingRegistrations', @level2type = N'COLUMN', @level2name = N'Province';

