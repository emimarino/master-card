﻿CREATE TABLE [dbo].[TagHit] (
    [TagHitID] INT           IDENTITY (1, 1) NOT NULL,
    [TagID]    VARCHAR (25)  NOT NULL,
    [UserName] VARCHAR (255) NOT NULL,
    [Date]     DATETIME      NOT NULL,
    [VisitID]  BIGINT        NOT NULL,
    CONSTRAINT [PK_TagHit] PRIMARY KEY CLUSTERED ([TagHitID] ASC)
);

