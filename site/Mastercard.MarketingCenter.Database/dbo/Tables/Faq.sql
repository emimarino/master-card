﻿CREATE TABLE [dbo].[Faq] (
    [ContentItemId]             VARCHAR (25)  NOT NULL,
    [Question]                  VARCHAR (MAX) NOT NULL,
    [Answer]                    VARCHAR (MAX) NOT NULL,
    [ShortDescription]          VARCHAR (MAX) NULL,
    [ThumbnailImage]            VARCHAR (255) NULL,
    [LongDescription]           VARCHAR (MAX) NULL,
    [ShowInCrossSellBox]        VARCHAR (255) NULL,
    [PriorityLevel]             VARCHAR (255) NULL,
    [ShowInTagBrowser]          BIT           NULL,
    [RelatedItems]              VARCHAR (255) NULL,
    [IconID]                    VARCHAR (25)  NOT NULL,
    [AssetType]                 VARCHAR (255) DEFAULT ('') NULL,
    [SpecialAudienceAccess]     VARCHAR (255) NULL,
    [RestrictToSpecialAudience] VARCHAR (255) NULL,
    CONSTRAINT [PK_Faq] PRIMARY KEY CLUSTERED ([ContentItemId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_Faq_Icon0] FOREIGN KEY ([IconID]) REFERENCES [dbo].[Icon] ([IconID]) ON DELETE CASCADE
);



