﻿CREATE TABLE [dbo].[ContentItemVisits]
(
	[ContentItemId] varchar(25) NOT NULL PRIMARY KEY,
	[Visits] int not null
)