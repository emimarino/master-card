﻿CREATE TABLE [dbo].[ContentItemCategory] (
    [ContentItemId]	VARCHAR (25) NOT NULL,
    [CategoryId]    NVARCHAR (25) NOT NULL,
    CONSTRAINT [PK_ContentItemCategory] PRIMARY KEY CLUSTERED ([ContentItemId] ASC, [CategoryId] ASC),
    CONSTRAINT [FK_ContentItemCategory_ContentItem] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[ContentItem] ([ContentItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_ContentItemCategory_Category] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Category] ([CategoryId])
);