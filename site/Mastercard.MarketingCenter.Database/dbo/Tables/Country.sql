﻿CREATE TABLE [dbo].[Country] (
    [CountryId]        BIGINT  IDENTITY (1, 1) NOT NULL,
    [Code]				VARCHAR (25)	NOT NULL,
    [Name]				VARCHAR (255)   NOT NULL,
    [TagIdentifier]     VARCHAR (255)   NULL,
    CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED ([CountryId] ASC)
);