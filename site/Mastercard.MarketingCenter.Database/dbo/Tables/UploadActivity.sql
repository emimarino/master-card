﻿CREATE TABLE [dbo].[UploadActivity]
(
	[UploadActivityId] INT NOT NULL PRIMARY KEY IDENTITY,
    [ContentItemId] VARCHAR(25) NOT NULL,
    [Filename] VARCHAR(MAX) NOT NULL,
    [FileReference] VARCHAR(MAX) NOT NULL,
    [Size] DECIMAL(18, 2) NOT NULL,
    [FieldDefinitionName] VARCHAR(MAX) NOT NULL,
    [ActivityDate] DATETIME NOT NULL,
    [ActivityUserId] INT NOT NULL,
    [UploadActivityActionId] INT NOT NULL,
    [UploadActivityStatusId] INT NOT NULL,
    [DateSaved] DATETIME NULL,
    CONSTRAINT [FK_UploadActivity_User] FOREIGN KEY ([ActivityUserId]) REFERENCES [dbo].[User] ([UserId]),
    CONSTRAINT [FK_UploadActivity_UploadActivityAction] FOREIGN KEY ([UploadActivityActionId]) REFERENCES [dbo].[UploadActivityAction] ([UploadActivityActionId]) ON DELETE CASCADE,
    CONSTRAINT [FK_UploadActivity_UploadActivityStatus] FOREIGN KEY ([UploadActivityStatusId]) REFERENCES [dbo].[UploadActivityStatus] ([UploadActivityStatusId]) ON DELETE CASCADE
)