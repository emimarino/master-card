﻿CREATE TABLE [dbo].[OrderableAsset] (
    [ContentItemId]                 VARCHAR (25)   NOT NULL,
    [Title]                         NVARCHAR (255) NULL,
    [ShortDescription]              NVARCHAR (MAX) NULL,
    [ThumbnailImage]                NVARCHAR (255) NULL,
    [LongDescription]               NVARCHAR (MAX) NULL,
    [ShowInCrossSellBox]            VARCHAR (255)  NULL,
    [PriorityLevel]                 VARCHAR (255)  NULL,
    [ShowInTagBrowser]              BIT            NULL,
    [MainImage]                     NVARCHAR (255) NOT NULL,
    [CallToAction]                  NVARCHAR (MAX) NULL,
    [BrowseAllMaterialsDisplayText] VARCHAR (255)  NULL,
    [BrowseAllMaterialsTags]        VARCHAR (255)  NULL,
    [RelatedItems]                  VARCHAR (255)  NULL,
    [Sku]                           VARCHAR (255)  NOT NULL,
    [Vdp]                           BIT            NULL,
    [Customizable]                  BIT            NULL,
    [ElectronicDeliveryFile]        NVARCHAR (255) NULL,
    [ApproverEmailAddress]          VARCHAR (255)  NULL,
    [IconID]                        VARCHAR (25)   NULL,
    [PricingScheduleID]             VARCHAR (25)   CONSTRAINT [DF_OrderableAsset_PricingScheduleID] DEFAULT ('dc') NOT NULL,
    [AssetType]                     VARCHAR (255)  DEFAULT ('') NULL,
    [PDFPreview]                    VARCHAR (255)  CONSTRAINT [DF_OrderableAsset_PDFPreview] DEFAULT ('') NOT NULL,
    [SpecialAudienceAccess]         VARCHAR (255)  NULL,
    [RestrictToSpecialAudience]     VARCHAR (255)  NULL,
    [EnableFavoriting]              BIT            CONSTRAINT [DF_OrderableAsset_EnableFavoriting] DEFAULT ((1)) NOT NULL,
    [OriginalContentItemId]         VARCHAR (25)   NULL,
    [BusinessOwnerID]               INT            NULL,
    CONSTRAINT [PK_OrderableAsset] PRIMARY KEY CLUSTERED ([ContentItemId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_OrderableAsset_Icon0] FOREIGN KEY ([IconID]) REFERENCES [dbo].[Icon] ([IconID]) ON DELETE CASCADE,
    CONSTRAINT [FK_OrderableAsset_PricingSchedule] FOREIGN KEY ([PricingScheduleID]) REFERENCES [dbo].[PricingSchedule] ([PricingScheduleID]),
	CONSTRAINT [FK_OrderableAsset_BusinessOwner] FOREIGN KEY (BusinessOwnerID) REFERENCES [dbo].[User] ([UserId])
);





