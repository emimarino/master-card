﻿CREATE TABLE [dbo].[LoggingEvents] (
    [Id]			BIGINT          IDENTITY (1, 1) NOT NULL,
    [EventId]		VARCHAR (38)    NOT NULL,
    [LogKey]		VARCHAR (100)   NOT NULL,
    [ApiKey]		VARCHAR (100)   NULL,
	[CorrelationId]	VARCHAR (100)   NULL,
    [Date]			DATETIME        NOT NULL,
    [Level]			INT             NULL,
    [Value]			DECIMAL (15, 4) NULL,
    [Text]			NVARCHAR (MAX)	NULL,
    [Tags]			VARCHAR (1000)  NULL,
    [Data]			TEXT            NULL,
    [User]			VARCHAR (500)   NULL,
    [Psid]			VARCHAR (50)    NULL,
    [Ppid]			VARCHAR (50)    NULL,
    [MachineName]	NVARCHAR (100)  NULL,
    [Host]			VARCHAR (255)   NULL,
    [Url]			VARCHAR (2000)  NULL,
    [HttpMethod]	VARCHAR (10)    NULL,
    [IpAddress]		VARCHAR (40)    NULL,
    [Source]		VARCHAR (500)   NULL,
    [StatusCode]	INT             NULL,
    [Hash]			INT             NULL,
    [Count]			INT             DEFAULT ((1)) NOT NULL
);

GO
CREATE NONCLUSTERED INDEX [LoggingEvents_Index]
    ON [dbo].[LoggingEvents]([EventId] ASC);