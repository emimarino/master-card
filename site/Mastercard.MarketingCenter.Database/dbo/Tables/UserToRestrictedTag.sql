﻿CREATE TABLE [dbo].[UserToRestrictedTag] (
    [UserID] INT          NOT NULL,
    [TagID]  VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_UserToRestrictedTag] PRIMARY KEY CLUSTERED ([UserID] ASC, [TagID] ASC),
    CONSTRAINT [FK_UserToRestrictedTag_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([TagID]) ON DELETE CASCADE,
    CONSTRAINT [FK_UserToRestrictedTag_User] FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([UserId]) ON DELETE CASCADE
);

