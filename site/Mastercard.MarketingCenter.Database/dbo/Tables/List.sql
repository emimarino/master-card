﻿CREATE TABLE [dbo].[List] (
    [ListId]           INT           IDENTITY (1, 1) NOT NULL,
    [Name]             VARCHAR (100) NOT NULL,
    [TableName]        VARCHAR (100) NOT NULL,
    [ShowInNavigation] BIT           CONSTRAINT [DF_List_ShowInNavigation] DEFAULT ((0)) NOT NULL,
    [WhereClause]      VARCHAR (MAX) NULL,
	[CanExpire]		   BIT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_List] PRIMARY KEY CLUSTERED ([ListId] ASC) WITH (FILLFACTOR = 90)
);