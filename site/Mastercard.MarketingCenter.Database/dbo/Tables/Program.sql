﻿CREATE TABLE [dbo].[Program] (
	[ContentItemId] VARCHAR(25) NOT NULL
	,[Title] NVARCHAR(255) NULL
	,[ShortDescription] NVARCHAR(MAX) NULL
	,[ThumbnailImage] NVARCHAR(255) NOT NULL
	,[LongDescription] NVARCHAR(MAX) NULL
	,[ShowInCrossSellBox] VARCHAR(255) NULL
	,[PriorityLevel] VARCHAR(255) NULL
	,[ShowInTagBrowser] BIT NULL
	,[MainImage] NVARCHAR(255) NOT NULL
	,[CallToAction] NVARCHAR(MAX) NULL
	,[BrowseAllMaterialsDisplayText] NVARCHAR(255) NULL
	,[BrowseAllMaterialsTags] VARCHAR(255) NULL
	,[PrintedOrderPeriodStartDate] DATETIME NULL
	,[PrintedOrderPeriodEndDate] DATETIME NULL
	,[PrintedOrderPeriodAlternateDisplayText] NVARCHAR(255) NULL
	,[OnlineOrderPeriodStartDate] DATETIME NULL
	,[OnlineOrderPeriodEndDate] DATETIME NULL
	,[OnlineOrderPeriodAlternateDisplayText] NVARCHAR(255) NULL
	,[InMarketStartDate] DATETIME NULL
	,[InMarketEndDate] DATETIME NULL
	,[InMarketAlternateDisplayText] NVARCHAR(255) NULL
	,[PrintedOrderPeriodAlternateDateText] NVARCHAR(255) NULL
	,[OnlineOrderPeriodAlternateDateText] NVARCHAR(255) NULL
	,[InMarketAlternateDateText] NVARCHAR(255) NULL
	,[RelatedItems] VARCHAR(255) NULL
	,[ShowOrderPeriods] BIT NULL
	,[IconID] VARCHAR(25) CONSTRAINT [DF_Program_IconID] DEFAULT('2f') NOT NULL
	,[AssetType] VARCHAR(255) CONSTRAINT [DF__Program__AssetTy__6ECB5E34] DEFAULT('') NOT NULL
	,[RedirectURL] VARCHAR(255) NULL
	,[SpecialAudienceAccess] VARCHAR(255) NULL
	,[RestrictToSpecialAudience] VARCHAR(255) NULL
	,[EnableFavoriting] BIT DEFAULT((1)) NOT NULL
	,[ShowInMarketingCalendar] BIT CONSTRAINT [DF_Program_ShowInMarketingCalendar] DEFAULT((0)) NOT NULL
	,[PreviewFiles] BIT DEFAULT((0)) NOT NULL
	,[MarketingCalendarPriority] INT DEFAULT 5 NOT NULL
	,[BusinessOwnerID] INT NULL
	,CONSTRAINT [PK_Program] PRIMARY KEY CLUSTERED ([ContentItemId] ASC) WITH (FILLFACTOR = 90)
	,CONSTRAINT [FK_Program_Icon] FOREIGN KEY ([IconID]) REFERENCES [dbo].[Icon]([IconID])
	,CONSTRAINT [FK_Program_BusinessOwner] FOREIGN KEY (BusinessOwnerID) REFERENCES [dbo].[User]([UserId])
	);
GO

CREATE NONCLUSTERED INDEX [IX_Program_Date] ON [dbo].[Program] (
	[PrintedOrderPeriodStartDate] ASC
	,[PrintedOrderPeriodEndDate] ASC
	,[OnlineOrderPeriodStartDate] ASC
	,[OnlineOrderPeriodEndDate] ASC
	,[InMarketStartDate] ASC
	,[InMarketEndDate] ASC
	)
	WITH (
			PAD_INDEX = OFF
			,STATISTICS_NORECOMPUTE = OFF
			,SORT_IN_TEMPDB = OFF
			,DROP_EXISTING = OFF
			,ONLINE = OFF
			,ALLOW_ROW_LOCKS = ON
			,ALLOW_PAGE_LOCKS = ON
			)
GO