﻿CREATE TABLE [dbo].[SiteVisits] (
    [VisitID]     BIGINT         IDENTITY (1, 1) NOT NULL,
    [UrlReferrer] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_SiteVisits] PRIMARY KEY CLUSTERED ([VisitID] ASC)
);

