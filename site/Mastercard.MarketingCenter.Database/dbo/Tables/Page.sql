﻿CREATE TABLE [dbo].[Page] (
    [ContentItemId]             VARCHAR (25)   NOT NULL,
    [Title]                     NVARCHAR (255) NULL,
    [Uri]                       NVARCHAR (255) NULL,
    [IntroCopy]                 NVARCHAR (MAX) NULL,
    [Terms]                     NVARCHAR (MAX) NULL,
    [HideFromSearch]            BIT            NULL,
    [ShortDescription]          NVARCHAR (MAX) NULL,
    [Rank]                      INT            NULL,
    [OrderNumber]               INT            NULL,
    [RestrictToSpecialAudience] VARCHAR (255)  NULL,
    [BottomTitle]               VARCHAR (255)  NULL,
    [HeaderTitle]               NVARCHAR (255) NULL,
    [BottomParagraph]           NVARCHAR (MAX) NULL,
    [HeaderParagraph]           NVARCHAR (MAX) NULL,
    [BottomBackgroundImage]     VARCHAR (255)  NULL,
    [HeaderBackgroundImage]     VARCHAR (255)  NULL,
    [BottomAsideImage]          VARCHAR (255)  NULL,
    [HeaderAsideImage]          VARCHAR (255)  NULL,
    [BottomUrl]                 NVARCHAR (255) NULL,
    [HeaderUrl]                 NVARCHAR (255) NULL,
    [AsideContent]              NVARCHAR (MAX) NULL,
    [EnableFavoriting]          BIT            DEFAULT ((1)) NOT NULL,
	[InMarketStartDate]				DATETIME	   NULL,
	[InMarketEndDate]				DATETIME	   NULL,
	[ShowInMarketingCalendar]       BIT            DEFAULT ((0)) NOT NULL,
    [MarketingCalendarPriority]		INT			   DEFAULT 5 NOT NULL, 
    CONSTRAINT [PK_Page] PRIMARY KEY CLUSTERED ([ContentItemId] ASC)
);







