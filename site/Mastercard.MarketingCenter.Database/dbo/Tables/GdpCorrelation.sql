﻿CREATE TABLE [dbo].[GdpCorrelation] (
    [GdpCorrelationId] INT      IDENTITY (1, 1) NOT NULL,
    [HttpStatusCode]   INT      NULL,
    [XTotalCount]      INT      NULL,
    [CreatedDateTime]  DATETIME NOT NULL,
    [UpdatedDateTime]  DATETIME NULL,
    CONSTRAINT [PK_GdpCorrelation] PRIMARY KEY CLUSTERED ([GdpCorrelationId] ASC)
);



