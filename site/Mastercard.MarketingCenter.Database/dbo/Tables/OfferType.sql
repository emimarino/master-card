﻿CREATE TABLE [dbo].[OfferType] (
	[OfferTypeId] NVARCHAR(25) NOT NULL
	,[Title] NVARCHAR(255) NOT NULL
	,[PricelessProgramName] NVARCHAR(MAX) NULL
	,[ShowProductUrl] BIT DEFAULT ((0)) NOT NULL
	,CONSTRAINT [PK_OfferType] PRIMARY KEY CLUSTERED ([OfferTypeId] ASC)
	);