﻿CREATE TABLE [dbo].[Snippet] (
    [ContentItemId]             VARCHAR (25)   NOT NULL,
    [Title]                     NVARCHAR (255) NULL,
    [Html]                      NVARCHAR (MAX) NULL,
    [RestrictToSpecialAudience] VARCHAR (255)  NULL,
    CONSTRAINT [PK_Snippet] PRIMARY KEY CLUSTERED ([ContentItemId] ASC)
);



