﻿CREATE TABLE [dbo].[AssetAttachment] (
    [AttachmentID]  INT             IDENTITY (1, 1) NOT NULL,
    [ContentItemId] VARCHAR (25)    NOT NULL,
    [FileUrl]       NVARCHAR (2000) NULL,
    CONSTRAINT [PK_AssetAttachment] PRIMARY KEY CLUSTERED ([AttachmentID] ASC),
    CONSTRAINT [FK_AssetAttachment_Asset] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[Asset] ([ContentItemId]) ON DELETE CASCADE
);