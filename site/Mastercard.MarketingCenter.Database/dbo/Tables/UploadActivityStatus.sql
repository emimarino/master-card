﻿CREATE TABLE [dbo].[UploadActivityStatus] (
    [UploadActivityStatusId] INT IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_UploadActivityStatus] PRIMARY KEY CLUSTERED ([UploadActivityStatusId] ASC) WITH (FILLFACTOR = 90)
);