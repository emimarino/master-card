﻿CREATE TABLE [dbo].[UserSubscriptionFrequency] (
    [UserSubscriptionFrequencyId] INT           IDENTITY (1, 1) NOT NULL,
    [Frequency]                   VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_UserSubscriptionFrequency] PRIMARY KEY CLUSTERED ([UserSubscriptionFrequencyId] ASC) WITH (FILLFACTOR = 90)
);

