﻿CREATE TABLE [dbo].[ConsentFileData] (
    [ConsentFileDataId]         INT              IDENTITY (1, 1) NOT NULL,
    [Uuid]                      UNIQUEIDENTIFIER NULL,
    [ServiceFunctionCode]       VARCHAR (10)     NULL,
    [UseCategoryCode]           VARCHAR (10)     NULL,
    [DocumentType]              VARCHAR (50)     NULL,
    [ConsentData]               VARCHAR (MAX)    NULL,
    [CurrentVersion]            VARCHAR (50)     NULL,
    [Status]                    VARCHAR (50)     CONSTRAINT [DF__ConsentFi__Statu__686E569E] DEFAULT ((0)) NOT NULL,
    [Locale]                    VARCHAR (10)     NULL,
    [ReacceptedVersion]         VARCHAR (50)     NULL,
    [NotificationVersion]       VARCHAR (50)     NULL,
    [ReacceptanceRequired]      BIT              CONSTRAINT [DF__ConsentFi__Reacc__69627AD7] DEFAULT ((0)) NOT NULL,
    [EmailNotificationRequired] BIT              CONSTRAINT [DF__ConsentFi__Email__6A569F10] DEFAULT ((0)) NOT NULL,
    [PublishedDate]             DATETIME         NULL,
    [UseCategoryValue]          VARCHAR (100)    NULL,
    [UseCategoryDescription]    VARCHAR (100)    NULL,
    CONSTRAINT [PK__ConsentF__A05749491DC86511] PRIMARY KEY CLUSTERED ([ConsentFileDataId] ASC)
);



