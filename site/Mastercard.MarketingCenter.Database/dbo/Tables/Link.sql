﻿CREATE TABLE [dbo].[Link] (
    [ContentItemId]      VARCHAR (25)  NOT NULL,
    [Title]              VARCHAR (255) NOT NULL,
    [ShortDescription]   VARCHAR (MAX) NULL,
    [ThumbnailImage]     VARCHAR (255) NULL,
    [ShowInCrossSellBox] VARCHAR (255) NULL,
    [PriorityLevel]      VARCHAR (255) NULL,
    [ShowInTagBrowser]   BIT           NULL,
    [AssetType]          VARCHAR (255) DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_Link] PRIMARY KEY CLUSTERED ([ContentItemId] ASC)
);



