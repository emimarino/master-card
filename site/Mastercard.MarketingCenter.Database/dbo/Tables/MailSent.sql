﻿CREATE TABLE [dbo].[MailSent] (
    [MailSentId] INT            IDENTITY (1, 1) NOT NULL,
    [MailData]   NVARCHAR (MAX) NULL,
    [Date]       DATETIME       NOT NULL,
    [UserId]     INT            NOT NULL,
    CONSTRAINT [PK_MailSent] PRIMARY KEY CLUSTERED ([MailSentId] ASC)
);

