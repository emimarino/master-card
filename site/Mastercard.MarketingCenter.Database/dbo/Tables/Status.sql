﻿CREATE TABLE [dbo].[Status] (
    [StatusID] INT           NOT NULL,
    [Name]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED ([StatusID] ASC)
);

