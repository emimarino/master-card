﻿CREATE TABLE ContentAboutToExpire
(
	[ContentItemId]			VARCHAR(25) Primary Key NOT NULL,
	[ContentTypeId]			VARCHAR(25) NOT NULL,
    [RegionId]				NCHAR(6) NOT NULL,
	[Title]					VARCHAR(MAX) NOT NULL,
    [ExpirationDate]		DATETIME NOT NULL,
    [BusinessOwnerId]		INT NULL,
    CONSTRAINT [FK_ContentAboutToExpire_ContentItem] FOREIGN KEY (ContentItemId) REFERENCES [ContentItem](ContentItemID), 
	CONSTRAINT [FK_ContentAboutToExpire_ContentType] FOREIGN KEY (ContentTypeId) REFERENCES [ContentType](ContentTypeID), 
    CONSTRAINT [FK_ContentAboutToExpire_Region] FOREIGN KEY ([RegionId]) REFERENCES [Region](Id), 
    CONSTRAINT [FK_ContentAboutToExpire_BusinessOwner] FOREIGN KEY (BusinessOwnerId) REFERENCES [User]([UserId])
)
GO