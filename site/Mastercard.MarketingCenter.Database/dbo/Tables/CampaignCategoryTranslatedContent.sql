﻿CREATE TABLE [dbo].[CampaignCategoryTranslatedContent] (
    [Id]             INT            IDENTITY (1, 1) NOT NULL,
    [CampaignCategoryId] NVARCHAR (25)  NOT NULL,
    [Title]          NVARCHAR (255) NULL,
    [LanguageCode]   NCHAR (3)      NOT NULL,
    CONSTRAINT [PK_CampaignCategoryTranslatedContent] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CampaignCategoryTranslatedContent_CampaignCategory] FOREIGN KEY ([CampaignCategoryId]) REFERENCES [dbo].[CampaignCategory] ([CampaignCategoryId]) ON UPDATE NO ACTION ON DELETE CASCADE
);