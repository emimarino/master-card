﻿CREATE TABLE [dbo].[SiteTracking] (
    [SiteTrackingID] BIGINT          IDENTITY (1, 1) NOT NULL,
    [VisitID]        BIGINT          NOT NULL,
    [What]           NVARCHAR (1000) NOT NULL,
    [Who]            NVARCHAR (1000) NULL,
    [When]           DATETIME        NOT NULL,
    [From]           NVARCHAR (MAX)  NULL,
    [How]            NVARCHAR (1000) NULL,
    [ContentItemID]  VARCHAR (25)    NULL,
    [PageGroup]      VARCHAR (50)    NULL,
    [Region]         NVARCHAR (100)  DEFAULT ('us') NOT NULL,
    [IsArchived]     BIT             DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_SiteTracking] PRIMARY KEY CLUSTERED ([SiteTrackingID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [ix_SiteTracking_ContentItemID]
    ON [dbo].[SiteTracking]([ContentItemID] ASC);

GO
CREATE NONCLUSTERED INDEX [ix_SiteTracking_When]
    ON [dbo].[SiteTracking]([When] ASC)
    INCLUDE([From], [SiteTrackingID], [Who]) WITH (FILLFACTOR = 90);

GO
CREATE NONCLUSTERED INDEX [ix_SiteTracking_ContentItemIDRegionWhen]
    ON [dbo].[SiteTracking]([ContentItemID] ASC, [Region] ASC, [When] ASC)
    INCLUDE([Who], [How]);

GO
CREATE NONCLUSTERED INDEX [ix_SiteTracking_WhenSiteTrackingID]
    ON [dbo].[SiteTracking]([When] ASC, [SiteTrackingID] ASC);

GO
CREATE NONCLUSTERED INDEX [ix_SiteTracking_Who]
    ON [dbo].[SiteTracking]([Who] ASC);
GO