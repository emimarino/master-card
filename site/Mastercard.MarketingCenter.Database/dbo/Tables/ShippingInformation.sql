﻿CREATE TABLE [dbo].[ShippingInformation] (
    [ShippingInformationID] INT             IDENTITY (1, 1) NOT NULL,
    [ShoppingCartID]        INT             NOT NULL,
    [ContactName]           NVARCHAR (1000) NULL,
    [Phone]                 NVARCHAR (100)  NULL,
    [Address]               NVARCHAR (MAX)  NULL,
    [OrderTrackingNumber]   NVARCHAR (1000) NULL,
    CONSTRAINT [PK_ShippingInformation] PRIMARY KEY CLUSTERED ([ShippingInformationID] ASC)
);

