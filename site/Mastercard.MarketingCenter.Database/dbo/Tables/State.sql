﻿CREATE TABLE [dbo].[State] (
    [StateID]   INT            IDENTITY (1, 1) NOT NULL,
    [StateName] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_MMC.State] PRIMARY KEY CLUSTERED ([StateID] ASC)
);

