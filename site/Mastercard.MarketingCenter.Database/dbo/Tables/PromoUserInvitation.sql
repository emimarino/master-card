﻿CREATE TABLE [dbo].[PromoUserInvitation] (
    [PromoUserInvitationID] INT              IDENTITY (1, 1) NOT NULL,
    [FirstName]             NVARCHAR (255)   NULL,
    [LastName]              NVARCHAR (255)   NULL,
    [Email]                 NVARCHAR (255)   NULL,
    [IssuerID]              VARCHAR (40)     NOT NULL,
    [GUID]                  UNIQUEIDENTIFIER NOT NULL,
    [ActivatedDate]         DATETIME         NULL,
    [ExpiryDate]            DATETIME         NOT NULL,
    [SendResult]            BIT              NULL,
    [LastViewDate]          DATETIME         NULL,
    [LastClickDate]         DATETIME         NULL,
    [IsTest]                BIT              NULL,
    [BouncedBack]           BIT              NULL,
    CONSTRAINT [PK_PromoUserInvitation] PRIMARY KEY CLUSTERED ([PromoUserInvitationID] ASC)
);

