﻿CREATE TABLE [dbo].[GdpRequest] (
    [GdpRequestId]     INT           IDENTITY (1, 1) NOT NULL,
    [GdpCorrelationId] INT           NOT NULL,
    [RequestId]        INT           NOT NULL,
    [RequestType]      CHAR (1)      NOT NULL,
    [RequestContext]   CHAR (3)      NOT NULL,
    [SearchKey]        VARCHAR (MAX) NOT NULL,
    [CreatedDateTime]  DATETIME      NOT NULL,
    CONSTRAINT [PK__GdpReque__C1514FC29DA173FF] PRIMARY KEY CLUSTERED ([GdpRequestId] ASC),
    CONSTRAINT [FK_GdpRequest_GdpCorrelation] FOREIGN KEY ([GdpCorrelationId]) REFERENCES [dbo].[GdpCorrelation] ([GdpCorrelationId]),
    CONSTRAINT [FK_GdpRequest_GdpRequest] FOREIGN KEY ([GdpRequestId]) REFERENCES [dbo].[GdpRequest] ([GdpRequestId])
);

