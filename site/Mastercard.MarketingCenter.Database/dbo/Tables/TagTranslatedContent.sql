﻿CREATE TABLE [dbo].[TagTranslatedContent] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [TagId]        VARCHAR (25)   NOT NULL,
    [DisplayName]  NVARCHAR (255) NULL,
    [Description]  NVARCHAR (MAX) NULL,
    [PageContent]  NVARCHAR (MAX) NULL,
    [LanguageCode] NCHAR (3)      NOT NULL,
    CONSTRAINT [FK_TagTranslatedContent_Tag] FOREIGN KEY ([TagId]) REFERENCES [dbo].[Tag] ([TagID]) ON DELETE CASCADE
);
GO
CREATE NONCLUSTERED INDEX IX_Tag_Translated ON [TagTranslatedContent] ( [LanguageCode], [TagId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO



GO
CREATE UNIQUE CLUSTERED INDEX [UX_TagTranslatedContent]
    ON [dbo].[TagTranslatedContent]([Id] ASC);

