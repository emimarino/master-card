﻿CREATE TABLE [dbo].[InsertsBundle] (
    [InsertsBundlesID] INT            IDENTITY (1, 1) NOT NULL,
    [Name]             VARCHAR (512)  NOT NULL,
    [UnitSize]         INT            NOT NULL,
    [UnitCost]         MONEY          NOT NULL,
    [IsActive]         BIT            CONSTRAINT [DF_InsertsBundles_IsActive] DEFAULT ((1)) NOT NULL,
    [Q1PromotionName]  VARCHAR (1024) NOT NULL,
    [Q2PromotionName]  VARCHAR (1024) NOT NULL,
    [Q3PromotionName]  VARCHAR (1024) NOT NULL,
    [Q4PromotionName]  VARCHAR (1024) NOT NULL,
    [Q1PromotionSKU]   VARCHAR (1024) NOT NULL,
    [Q2PromotionSKU]   VARCHAR (1024) NOT NULL,
    [Q3PromotionSKU]   VARCHAR (1024) NOT NULL,
    [Q4PromotionSKU]   VARCHAR (1024) NOT NULL,
    [Modified]         DATETIME       CONSTRAINT [DF_InsertsBundles_Modified] DEFAULT (getdate()) NOT NULL,
    [Created]          DATETIME       CONSTRAINT [DF_InsertsBundles_Created] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_InsertsBundles] PRIMARY KEY CLUSTERED ([InsertsBundlesID] ASC)
);

