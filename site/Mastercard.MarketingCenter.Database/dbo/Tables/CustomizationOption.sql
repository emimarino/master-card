﻿CREATE TABLE [dbo].[CustomizationOption] (
    [CustomizationOptionID] VARCHAR (25)     NOT NULL,
    [FieldType]             VARCHAR (MAX)    NULL,
    [FriendlyName]          VARCHAR (MAX)    NOT NULL,
    [Instructions]          VARCHAR (MAX)    NULL,
    [InternalName]          VARCHAR (MAX)    NULL,
    [MaxLength]             INT              NULL,
    [Modified]              DATETIME         CONSTRAINT [DF_CustomizationOption_Modified] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CustomizationOption] PRIMARY KEY CLUSTERED ([CustomizationOptionID] ASC),
);