﻿CREATE TABLE [dbo].[TagScope] (
    [TagID]         VARCHAR (25) NOT NULL,
    [ContentTypeID] VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_TagScope] PRIMARY KEY CLUSTERED ([TagID] ASC, [ContentTypeID] ASC),
    CONSTRAINT [FK_TagScope_ContentType] FOREIGN KEY ([ContentTypeID]) REFERENCES [dbo].[ContentType] ([ContentTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_TagScope_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([TagID]) ON DELETE CASCADE
);

