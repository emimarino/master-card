﻿CREATE TABLE [dbo].[GdprFollowUpStatus] (
    [GdprFollowUpStatusId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]                 VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_GdprFollowUpStatus] PRIMARY KEY CLUSTERED ([GdprFollowUpStatusId] ASC) WITH (FILLFACTOR = 90)
);

