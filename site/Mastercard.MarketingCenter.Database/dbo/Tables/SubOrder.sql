﻿CREATE TABLE [dbo].[SubOrder] (
    [SubOrderID]            INT             IDENTITY (1, 1) NOT NULL,
    [OrderID]               INT             NOT NULL,
    [ShippingInformationID] INT             NOT NULL,
    [PriceAdjustment]       DECIMAL (18, 2) NULL,
    [ShippingCost]          DECIMAL (18, 2) NULL,
    [PostageCost]           DECIMAL (18, 2) NULL,
    [Description]           NVARCHAR (1000) NULL,
    [TrackingNumber]        NVARCHAR (1000) NULL,
    [Completed]             BIT             NOT NULL,
    [DateCompleted]         DATETIME        NULL,
    CONSTRAINT [PK_SubOrders] PRIMARY KEY CLUSTERED ([SubOrderID] ASC),
    CONSTRAINT [FK_SubOrders_Orders] FOREIGN KEY ([OrderID]) REFERENCES [dbo].[Order] ([OrderID]),
    CONSTRAINT [FK_SubOrders_ShippingInformation] FOREIGN KEY ([ShippingInformationID]) REFERENCES [dbo].[ShippingInformation] ([ShippingInformationID]) NOT FOR REPLICATION
);


GO
ALTER TABLE [dbo].[SubOrder] NOCHECK CONSTRAINT [FK_SubOrders_ShippingInformation];

