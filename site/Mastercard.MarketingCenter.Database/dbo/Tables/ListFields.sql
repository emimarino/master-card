﻿CREATE TABLE [dbo].[ListFields] (
    [ListId]            INT           NOT NULL,
    [FieldDefinitionId] INT           NOT NULL,
    [Order]             INT           CONSTRAINT [DF_ListFields_Order] DEFAULT ((0)) NOT NULL,
    [VisibleInList]     BIT           CONSTRAINT [DF_ListFields_VisibleInList] DEFAULT ((0)) NOT NULL,
    [HiddenInNew]       BIT           CONSTRAINT [DF_ListFields_HiddenInNew] DEFAULT ((0)) NOT NULL,
    [Searcheable]       BIT           CONSTRAINT [DF_ListFields_Searcheable] DEFAULT ((0)) NOT NULL,
    [DefaultOrder]      VARCHAR (10)  NULL,
    [Permission]        VARCHAR (MAX) NULL,
    [RequiredInRegions] VARCHAR (255) NULL,
    [EnabledBySetting] VARCHAR(MAX) NULL, 
    CONSTRAINT [PK_ListFields] PRIMARY KEY CLUSTERED ([ListId] ASC, [FieldDefinitionId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ListFields_FieldDefinition] FOREIGN KEY ([FieldDefinitionId]) REFERENCES [dbo].[FieldDefinition] ([FieldDefinitionId]),
    CONSTRAINT [FK_ListFields_List] FOREIGN KEY ([ListId]) REFERENCES [dbo].[List] ([ListId])
);