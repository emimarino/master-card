﻿CREATE TABLE [dbo].[UserSubscription] (
    [UserSubscriptionId]          INT      IDENTITY (1, 1) NOT NULL,
    [UserId]                      INT      NOT NULL,
    [UserSubscriptionFrequencyId] INT      NOT NULL,
    [SavedDate]                   DATETIME NOT NULL,
    CONSTRAINT [PK_UserSubscription] PRIMARY KEY CLUSTERED ([UserSubscriptionId] ASC),
    CONSTRAINT [FK_UserSubscription_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId]) ON DELETE CASCADE,
    CONSTRAINT [FK_UserSubscription_UserSubscriptionFrequency] FOREIGN KEY ([UserSubscriptionFrequencyId]) REFERENCES [dbo].[UserSubscriptionFrequency] ([UserSubscriptionFrequencyId]) ON DELETE CASCADE
);

