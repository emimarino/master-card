﻿CREATE TABLE [dbo].[MailDispatcherStatus] (
    [MailDispatcherStatusId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]                   VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_MailDispatcherStatus] PRIMARY KEY CLUSTERED ([MailDispatcherStatusId] ASC) WITH (FILLFACTOR = 90)
);