﻿CREATE TABLE [dbo].[CardExclusivity] (
	[CardExclusivityId] NVARCHAR(25) NOT NULL
	,[Title] NVARCHAR(255) NOT NULL
	,[Description] NVARCHAR(MAX) NULL
	,[Image] NVARCHAR(255) NULL
	,[PricelessCardExclusivity] NVARCHAR(MAX) NULL
	,CONSTRAINT [PK_CardExclusivity] PRIMARY KEY CLUSTERED ([CardExclusivityId] ASC)
	);