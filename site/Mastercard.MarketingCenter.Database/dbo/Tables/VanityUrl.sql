﻿CREATE TABLE [dbo].[VanityUrl] (
    [VanityUrlId] VARCHAR (25)     NOT NULL,
    [Url]         VARCHAR (255)    NOT NULL,
    [VanityUrl]   VARCHAR (255)    NOT NULL,
    [RegionId]    NCHAR (6)        DEFAULT ('us') NOT NULL,
    CONSTRAINT [PK_VanityUrl] PRIMARY KEY CLUSTERED ([VanityUrlId] ASC) WITH (FILLFACTOR = 90),
);