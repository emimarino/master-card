﻿CREATE TABLE [dbo].[ShoppingCart] (
    [CartID]               INT             IDENTITY (1, 1) NOT NULL,
    [UserID]               NVARCHAR (255)  NOT NULL,
    [DateCreated]          DATETIME        NOT NULL,
    [LastUpdated]          DATETIME        NOT NULL,
    [DatedCheckedOut]      DATETIME        NULL,
    [OrderID]              INT             NULL,
    [ShippingInstructions] NVARCHAR (4000) NULL,
    [RushOrder]            BIT             NULL,
    [PromotionID]          VARCHAR (40)    NULL,
    [BillingID]            INT             NULL,
    CONSTRAINT [PK_ShoppingCart] PRIMARY KEY CLUSTERED ([CartID] ASC)
);

