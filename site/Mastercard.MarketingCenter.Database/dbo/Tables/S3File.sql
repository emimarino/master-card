﻿CREATE TABLE [dbo].[S3File] (
    [S3FileId]      INT           IDENTITY (1, 1) NOT NULL,
    [Deleted]       BIT           NULL,
    [VideoFile]     VARCHAR (255) NULL,
    [CreationDate]  DATETIME      NULL,
    [ContentItemID] VARCHAR (25)  NULL,
    PRIMARY KEY CLUSTERED ([S3FileId] ASC)
);

