﻿CREATE TABLE [dbo].[RoleRegion] (
    [RoleId]			INT NOT NULL,
    [RegionId]			NCHAR (6) NOT NULL,
	[PermissionName]	NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([RoleId] ASC, [RegionId] ASC),
    FOREIGN KEY ([RoleId])			REFERENCES [dbo].[Role] ([RoleId]),
    FOREIGN KEY ([RegionId])		REFERENCES [dbo].[Region] ([Id])
);