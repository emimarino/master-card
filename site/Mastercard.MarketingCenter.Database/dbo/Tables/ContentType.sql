﻿CREATE TABLE [dbo].[ContentType] (
    [ContentTypeID]				VARCHAR (25) NOT NULL,
    [Title]						VARCHAR (255) NOT NULL,
    [DefaultIcon]				VARCHAR (255) NOT NULL,
    [MatchExtension]			VARCHAR (255) NULL,
    [TableName]					VARCHAR (50) NULL,
    [FrontEndUrl]				VARCHAR (150) NULL,
    [Clonable]					BIT DEFAULT ((0)) NOT NULL,
    [CanPreview]				BIT DEFAULT ((0)) NOT NULL,
    [MustExpire]				BIT DEFAULT ((1)) NOT NULL,
    [CanExpire]					BIT DEFAULT ((1)) NOT NULL,
    [CanCopy]					BIT DEFAULT ((0)) NOT NULL,
    [CanComment]				BIT DEFAULT ((0)) NOT NULL,
	[CanBePending] BIT NOT NULL DEFAULT ((0)),
    [BusinessOwnerCanRequest]	BIT NOT NULL DEFAULT ((0)), 
	[HasDownloads]				BIT NOT NULL DEFAULT ((0)),
	[HasCrossBorderRegions]		BIT NOT NULL DEFAULT ((0))
    CONSTRAINT [PK_ContentType] PRIMARY KEY CLUSTERED ([ContentTypeID] ASC),
);
GO
Create INDEX IX_CONTENT_TYPE ON ContentType(TableName,HasDownloads);