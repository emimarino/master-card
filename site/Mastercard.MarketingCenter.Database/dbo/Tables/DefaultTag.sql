﻿CREATE TABLE [dbo].[DefaultTag] (
    [SiteOrListDefaultID] VARCHAR (25) NOT NULL,
    [TagID]               VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_DefaultTag] PRIMARY KEY CLUSTERED ([SiteOrListDefaultID] ASC, [TagID] ASC),
    CONSTRAINT [FK_DefaultTag_SiteOrListDefault] FOREIGN KEY ([SiteOrListDefaultID]) REFERENCES [dbo].[SiteOrListDefault] ([SiteOrListDefaultID]) ON DELETE CASCADE,
    CONSTRAINT [FK_DefaultTag_Tag] FOREIGN KEY ([TagID]) REFERENCES [dbo].[Tag] ([TagID]) ON DELETE CASCADE
);

