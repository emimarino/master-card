﻿CREATE TABLE [dbo].[WebRoleAccess] (
    [WebRoleAccessId]            BIGINT        IDENTITY (1, 1) NOT NULL,
    [RoleType]                   VARCHAR (50)  NULL,
    [Area]                       VARCHAR (100) NULL,
    [Controller]                 VARCHAR (100) NOT NULL,
    [Action]                     VARCHAR (100) NULL,
    [Id]                         VARCHAR (100) NULL,
    [QueryString]                VARCHAR (250) NULL,
    [ContentAction]              VARCHAR (100) NULL,
    [RequiredRolePermissionName] VARCHAR (50)  NULL,
    CONSTRAINT [PK_WebRoleAccess] PRIMARY KEY CLUSTERED ([WebRoleAccessId] ASC) WITH (FILLFACTOR = 90)
);

