﻿CREATE TABLE [dbo].[DownloadCalendarKey] (
    [DownloadCalendarKeyId]       INT            IDENTITY (1, 1) NOT NULL,
    [Key]                         NVARCHAR (250) NOT NULL,
    [DownloadCalendarKeyStatusId] INT            NOT NULL,
    [UserId]                      INT            NOT NULL,
    [RegionId]                    NCHAR (6)      NOT NULL,
    [Language]                    NVARCHAR (6)   NULL,
    [SpecialAudience]             NVARCHAR (255) NOT NULL,
    [SegmentationIds]             NVARCHAR (MAX) NOT NULL,
	[MarketIdentifiers]			  NVARCHAR (MAX) NULL,
    [CampaignCategoryIds]             NVARCHAR (MAX) NOT NULL,
    [DateKeyGenerated]            DATETIME       NOT NULL,
    [DownloadYear]				  INT			 NOT NULL DEFAULT (DATEPART(YEAR,GETDATE())), 
    PRIMARY KEY CLUSTERED ([DownloadCalendarKeyId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_DownloadCalendarKey_DownloadCalendarKeyStatus] FOREIGN KEY ([DownloadCalendarKeyStatusId]) REFERENCES [dbo].[UserKeyStatus] ([UserKeyStatusID]),
    CONSTRAINT [FK_DownloadCalendarKey_Region] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Region] ([Id]),
    CONSTRAINT [FK_DownloadCalendarKey_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
);