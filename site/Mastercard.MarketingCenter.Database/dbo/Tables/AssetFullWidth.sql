﻿CREATE TABLE [dbo].[AssetFullWidth] (
    [ContentItemId]                 VARCHAR (25)   NOT NULL,
    [Title]                         NVARCHAR (255) NULL,
    [ShortDescription]              NVARCHAR (MAX) NULL,
    [ThumbnailImage]                NVARCHAR (255) NOT NULL,
    [LongDescription]               NVARCHAR (MAX) NULL,
    [ShowInCrossSellBox]            VARCHAR (255)  NULL,
    [PriorityLevel]                 VARCHAR (255)  NULL,
    [ShowInTagBrowser]              BIT            NULL,
    [RelatedItems]                  VARCHAR (255)  NULL,
    [IconID]                        VARCHAR (25)   NULL,
    [AssetType]                     VARCHAR (255)  DEFAULT ('') NOT NULL,
    [BottomAreaDescription]         NVARCHAR (MAX) NULL,
    [BrowseAllMaterialsDisplayText] VARCHAR (255)  NULL,
    [SpecialAudienceAccess]         VARCHAR (255)  NULL,
    [RestrictToSpecialAudience]     VARCHAR (255)  NULL,
    [EnableFavoriting]              BIT            CONSTRAINT [DF_AssetFullWidth_EnableFavoriting] DEFAULT ((1)) NOT NULL,
    [OriginalContentItemId]         VARCHAR (25)   NULL,
    [BusinessOwnerID]               INT            NULL,
    [AllowCloning]                  BIT            DEFAULT ((0)) NOT NULL,
    [NotifyWhenSourceChanges]       BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_AssetFullWidth] PRIMARY KEY CLUSTERED ([ContentItemId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_AssetFullWidth_Icon0] FOREIGN KEY ([IconID]) REFERENCES [dbo].[Icon] ([IconID]) ON DELETE CASCADE,
	CONSTRAINT [FK_AssetFullWidth_BusinessOwner] FOREIGN KEY (BusinessOwnerID) REFERENCES [dbo].[User] ([UserId])
);





