﻿CREATE TABLE [dbo].[ContentItemCrossBorderRegion] (
	[ContentItemId]	VARCHAR (25)	NOT NULL,
    [RegionId]		NCHAR (6)		NOT NULL, 
	CONSTRAINT [PK_ContentItemCrossBorderRegion] PRIMARY KEY (RegionId, ContentItemId),
    CONSTRAINT [FK_ContentItemCrossBorderRegion_Region] FOREIGN KEY ([RegionId]) REFERENCES [Region](Id),
	CONSTRAINT [FK_ContentItemCrossBorderRegion_ContentItem] FOREIGN KEY ([ContentItemId]) REFERENCES [ContentItem](ContentItemID)
)