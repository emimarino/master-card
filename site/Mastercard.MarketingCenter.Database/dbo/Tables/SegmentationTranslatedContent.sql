﻿CREATE TABLE [dbo].[SegmentationTranslatedContent] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [SegmentationId]   VARCHAR (25)   NOT NULL,
    [SegmentationName] NVARCHAR (255) NULL,
    [LanguageCode]     NCHAR (3)      NOT NULL,
    CONSTRAINT [PK_SegmentationTranslatedContent] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_SegmentationTranslatedContent_Segmentation] FOREIGN KEY ([SegmentationId]) REFERENCES [dbo].[Segmentation] ([SegmentationId]) ON DELETE CASCADE
);
GO
CREATE NONCLUSTERED INDEX IX_Segmentation_Translated ON [SegmentationTranslatedContent] ( [LanguageCode], [SegmentationName],[SegmentationId])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO