﻿CREATE TABLE [dbo].[ElectronicDelivery] (
    [ElectronicDeliveryID] UNIQUEIDENTIFIER NOT NULL,
    [OrderItemID]          INT              NULL,
    [FileLocation]         NVARCHAR(500)    NOT NULL,
    [DownloadCount]        INT              CONSTRAINT [DF_ElectronicDeliveries_DownloadCount] DEFAULT ((0)) NOT NULL,
    [ExpirationDate]       DATETIME         NOT NULL,
    CONSTRAINT [PK_ElectronicDeliveries] PRIMARY KEY CLUSTERED ([ElectronicDeliveryID] ASC)
);