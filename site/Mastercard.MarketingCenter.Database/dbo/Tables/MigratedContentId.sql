﻿CREATE TABLE [dbo].[MigratedContentId] (
    [Id]         VARCHAR (50) NOT NULL,
    [MigratedId] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_MigratedContentId] PRIMARY KEY CLUSTERED ([Id] ASC, [MigratedId] ASC)
);

