﻿CREATE TABLE [dbo].[CloneSourceNotification]
(
	[CloneSourceNotificationId] INT NOT NULL PRIMARY KEY Identity(1,1)	, 
    [CloneContentItemId] VARCHAR(25) NOT NULL, 
    [CloneRegionId] NCHAR(6) NOT NULL, 
    [SourceStatusName] varchar(255) NOT NULL, 
    [CreationDate] DATETIME NOT NULL, 
    [CloneTitle] NVARCHAR(255) NOT NULL, 
	[HasBeenSent] bit not null default 0,
    [SourceModificationDate] DATETIME NOT NULL, 
    CONSTRAINT [FK_CloneContentItemId_CloneSourceNotification_ContentItem] FOREIGN KEY (CloneContentItemId) REFERENCES [ContentItem](ContentItemID),
    CONSTRAINT [FK_CloneSourceNotification_Region] FOREIGN KEY (CloneRegionId) REFERENCES [Region](Id)

)
