﻿CREATE TABLE [dbo].[SiteOrListDefault] (
    [SiteOrListDefaultID] VARCHAR (25)     NOT NULL,
    [Title]               VARCHAR (255)    NOT NULL,
    [SiteListUrl]         VARCHAR (255)    NOT NULL,
    CONSTRAINT [PK_SiteOrListDefault] PRIMARY KEY CLUSTERED ([SiteOrListDefaultID] ASC),
);