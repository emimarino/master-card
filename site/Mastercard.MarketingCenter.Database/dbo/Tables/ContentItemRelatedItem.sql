﻿CREATE TABLE [dbo].[ContentItemRelatedItem] (
    [ContentItemId]   VARCHAR (25) NOT NULL,
    [RelatedContentItemId] VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_ContentItemRelatedItem] PRIMARY KEY CLUSTERED ([ContentItemId] ASC, [RelatedContentItemId] ASC),
    CONSTRAINT [FK_ContentItemRelatedItem_ContentItem] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[ContentItem] ([ContentItemID]) ON DELETE NO ACTION,
    CONSTRAINT [FK_ContentItemRelatedItem_RelatedContentItem] FOREIGN KEY ([RelatedContentItemId]) REFERENCES [dbo].[ContentItem] ([ContentItemID]) ON DELETE NO ACTION
);