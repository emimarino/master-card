﻿CREATE TABLE [dbo].[Cache] (
    [Id]                     VARCHAR (255) NOT NULL,
    [LastInvalidationCalled] DATETIME      NOT NULL,
    [MustInvalidate]         BIT           NOT NULL,
    CONSTRAINT [PK_Cache] PRIMARY KEY CLUSTERED ([Id] ASC)
);

