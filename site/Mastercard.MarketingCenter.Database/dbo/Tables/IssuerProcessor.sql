﻿CREATE TABLE [dbo].[IssuerProcessor] (
    [IssuerID]    VARCHAR (25) NOT NULL,
    [ProcessorID] VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_IssuerProcessor] PRIMARY KEY CLUSTERED ([IssuerID] ASC, [ProcessorID] ASC),
    CONSTRAINT [FK_IssuerProcessor_Issuer] FOREIGN KEY ([IssuerID]) REFERENCES [dbo].[Issuer] ([IssuerID]) ON DELETE CASCADE,
    CONSTRAINT [FK_IssuerProcessor_Processor] FOREIGN KEY ([ProcessorID]) REFERENCES [dbo].[Processor] ([ProcessorID]) ON DELETE CASCADE
);

