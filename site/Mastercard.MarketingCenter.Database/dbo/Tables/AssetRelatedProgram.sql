﻿CREATE TABLE [dbo].[AssetRelatedProgram] (
    [AssetContentItemId]   VARCHAR (25) NOT NULL,
    [ProgramContentItemId] VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_AssetRelatedProgram] PRIMARY KEY CLUSTERED ([AssetContentItemId] ASC, [ProgramContentItemId] ASC),
    CONSTRAINT [FK_AssetRelatedProgram_Asset] FOREIGN KEY ([AssetContentItemId]) REFERENCES [dbo].[Asset] ([ContentItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_AssetRelatedProgram_Program] FOREIGN KEY ([ProgramContentItemId]) REFERENCES [dbo].[Program] ([ContentItemId]) ON DELETE CASCADE
);

