﻿CREATE TABLE [dbo].[CampaignEvent] (
    [CampaignEventId]              NVARCHAR (25)    NOT NULL,
    [Title]                        NVARCHAR (255)   NULL,
    [LanguageCode]                 NVARCHAR (6)     NULL,
    [StartDate]                    DATETIME         NULL,
    [EndDate]                      DATETIME         NULL,
    [RegionId]                     NCHAR (6)        NOT NULL,
    [CreatedDate]                  DATETIME         NOT NULL,
    [CreatedByUserID]              INT              NOT NULL,
    [ModifiedDate]                 DATETIME         NOT NULL,
    [ModifiedByUserID]             INT              NOT NULL,
    [AlwaysOn]                     BIT              CONSTRAINT [DF_CampaignEvent_AlwaysOn] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_CampaignEvent] PRIMARY KEY CLUSTERED ([CampaignEventId] ASC),
    CONSTRAINT [FK_CampaignEvent_Region] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Region] ([Id]),
    CONSTRAINT [FK_CampaignEvent_User1] FOREIGN KEY ([CreatedByUserID]) REFERENCES [dbo].[User] ([UserId]),
    CONSTRAINT [FK_CampaignEvent_User2] FOREIGN KEY ([ModifiedByUserID]) REFERENCES [dbo].[User] ([UserId])
);