﻿CREATE TABLE [dbo].[Region] (
    [Id]    NCHAR (6)     NOT NULL,
    [Name]  NVARCHAR (50) NOT NULL,
    [TagId] VARCHAR (25)  NOT NULL,
    CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Region_Tag] FOREIGN KEY ([TagId]) REFERENCES [dbo].[Tag] ([TagID])
);