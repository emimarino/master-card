﻿CREATE TABLE [dbo].[GdpResponse] (
    [GdpResponseId]       INT           IDENTITY (1, 1) NOT NULL,
    [GdpCorrelationId]    INT           NOT NULL,
    [GdpRequestId]        INT           NOT NULL,
    [ServiceFunctionCode] VARCHAR (20)  NOT NULL,
    [ResponseCode]        VARCHAR (20)  NOT NULL,
    [ResponseData]        VARCHAR (MAX) NULL,
    CONSTRAINT [PK_GdpResponse] PRIMARY KEY CLUSTERED ([GdpResponseId] ASC),
    CONSTRAINT [FK_GdpCorrelation_GdpResponse] FOREIGN KEY ([GdpCorrelationId]) REFERENCES [dbo].[GdpCorrelation] ([GdpCorrelationId]),
    CONSTRAINT [FK_GdpResponse_GdpRequest] FOREIGN KEY ([GdpRequestId]) REFERENCES [dbo].[GdpRequest] ([GdpRequestId])
);



