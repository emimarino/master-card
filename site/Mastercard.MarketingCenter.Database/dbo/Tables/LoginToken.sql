﻿CREATE TABLE [dbo].[LoginToken] (
    [LoginTokenID] UNIQUEIDENTIFIER CONSTRAINT [DF_LoginToken_LoginTokenID] DEFAULT (newid()) NOT NULL,
    [Username]     NVARCHAR (255)   NULL,
    [Password]     VARCHAR (255)    NOT NULL,
    [CreatedDate]  DATETIME         CONSTRAINT [DF_LoginToken_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ModifiedDate] DATETIME         CONSTRAINT [DF_LoginToken_ModifiedDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_LoginToken] PRIMARY KEY CLUSTERED ([LoginTokenID] ASC)
);

