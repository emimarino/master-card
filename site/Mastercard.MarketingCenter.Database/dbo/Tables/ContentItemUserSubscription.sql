﻿CREATE TABLE [dbo].[ContentItemUserSubscription] (
    [ContentItemId] VARCHAR (25) NOT NULL,
    [UserId]        INT          NOT NULL,
    [Subscribed]    BIT          NOT NULL,
    [SavedDate]     DATETIME     NOT NULL,
    CONSTRAINT [PK_ContentItemUserSubscription] PRIMARY KEY CLUSTERED ([ContentItemId] ASC, [UserId] ASC),
    CONSTRAINT [FK_ContentItemUserSubscription_ContentItem] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[ContentItem] ([ContentItemID]) ON DELETE CASCADE,
    CONSTRAINT [FK_ContentItemUserSubscription_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId]) ON DELETE CASCADE
);

