﻿CREATE TABLE [dbo].[QuickLink] (
    [ContentItemId] VARCHAR (25)   NOT NULL,
    [Title]         NVARCHAR (255) NULL,
    [Paragraph]     NVARCHAR (MAX) NULL,
    [Url]           NVARCHAR (255) NULL,
    [Image]         NVARCHAR(255)  NOT NULL,
    [OrderNumber]   INT            NOT NULL,
    CONSTRAINT [PK_QuickLink] PRIMARY KEY CLUSTERED ([ContentItemId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_QuickLink_ContentItem] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[ContentItem] ([ContentItemID])
);

