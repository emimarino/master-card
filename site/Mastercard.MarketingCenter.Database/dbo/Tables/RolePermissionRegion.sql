﻿CREATE TABLE [dbo].[RolePermissionRegion] (
    [RoleId]			INT NOT NULL,
    [PermissionId]		INT NOT NULL,
	[RegionId]			NCHAR (6) NOT NULL,
    PRIMARY KEY CLUSTERED ([RoleId] ASC, [PermissionId] ASC, [RegionId] ASC),
    FOREIGN KEY ([RoleId])			REFERENCES [dbo].[Role] ([RoleId]),
    FOREIGN KEY ([PermissionId])	REFERENCES [dbo].[Permission] ([PermissionId]),
    FOREIGN KEY ([RegionId])		REFERENCES [dbo].[Region] ([Id])
);