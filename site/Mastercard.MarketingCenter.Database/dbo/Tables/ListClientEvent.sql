﻿CREATE TABLE [dbo].[ListClientEvent] (
    [ListClientEventId] INT           IDENTITY (1, 1) NOT NULL,
    [ListId]            INT           NOT NULL,
    [Name]              VARCHAR (50)  NOT NULL,
    [FiredOn]           VARCHAR (50)  NOT NULL,
    [ClientEventCode]   VARCHAR (MAX) NOT NULL,
    [ContentAction]     VARCHAR (MAX) NULL,
    CONSTRAINT [PK_ListClientEvent] PRIMARY KEY CLUSTERED ([ListClientEventId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ListClientEvent_List] FOREIGN KEY ([ListId]) REFERENCES [dbo].[List] ([ListId])
);