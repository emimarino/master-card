﻿CREATE TABLE [dbo].[ContentItemCloneToRegion]
(
	[ContentItemID] [varchar](25) NOT NULL,
    [RegionId] NCHAR(6) NOT NULL, 
    CONSTRAINT [FK_ContentItemCloneToRegion_Region] FOREIGN KEY ([RegionId]) REFERENCES [Region](Id),
	CONSTRAINT [FK_ContentItemCloneToRegion_ContentItem] FOREIGN KEY ([ContentItemID]) REFERENCES [ContentItem](ContentItemID), 
    CONSTRAINT [PK_ContentItemCloneToRegion] PRIMARY KEY (RegionId,ContentItemID)
)