﻿CREATE TABLE [dbo].[UploadActivityAction] (
    [UploadActivityActionId] INT IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_UploadActivityAction] PRIMARY KEY CLUSTERED ([UploadActivityActionId] ASC) WITH (FILLFACTOR = 90)
);