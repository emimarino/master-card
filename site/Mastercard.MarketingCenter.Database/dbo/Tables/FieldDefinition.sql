﻿CREATE TABLE [dbo].[FieldDefinition] (
    [FieldDefinitionId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]              VARCHAR (50)  NOT NULL,
    [Description]       VARCHAR (100) NULL,
    [FieldTypeCode]     VARCHAR (20)  NOT NULL,
    [Length]            INT           NOT NULL,
    [Required]          BIT           NOT NULL,
    [Data]              VARCHAR (500) NULL,
    [DefaultValue]      VARCHAR (500) NULL,
    [FieldGroupId]      INT           NOT NULL,
    [CustomSettings]    VARCHAR (MAX) NULL,
    CONSTRAINT [PK_FieldDefinition] PRIMARY KEY CLUSTERED ([FieldDefinitionId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_FieldDefinition_FieldGroup] FOREIGN KEY ([FieldGroupId]) REFERENCES [dbo].[FieldGroup] ([FieldGroupId]),
    CONSTRAINT [FK_FieldDefinition_FieldType] FOREIGN KEY ([FieldTypeCode]) REFERENCES [dbo].[FieldType] ([FieldTypeCode])
);

