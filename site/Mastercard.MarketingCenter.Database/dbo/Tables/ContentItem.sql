﻿CREATE TABLE [dbo].[ContentItem] (
    [ContentItemID]        VARCHAR (25)     NOT NULL,
    [PrimaryContentItemID] VARCHAR (25)     NOT NULL,
    [ContentTypeID]        VARCHAR (25)     NOT NULL,
    [FrontEndUrl]          VARCHAR (255)    NOT NULL,
    [StatusID]             INT              NOT NULL,
    [CreatedByUserID]      INT              NOT NULL,
    [ModifiedByUserID]     INT              NOT NULL,
    [CreatedDate]          DATETIME         NOT NULL,
    [ModifiedDate]         DATETIME         NOT NULL,
    [RegionId]             NCHAR (6)        CONSTRAINT [DF_ContentItem_RegionId] DEFAULT (N'us') NOT NULL,
    [ExpirationDate]       DATETIME         NULL,
    [ReviewStateId] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [PK_ContentItem] PRIMARY KEY CLUSTERED ([ContentItemID] ASC),
    CONSTRAINT [FK_ContentItem_ContentType0] FOREIGN KEY ([ContentTypeID]) REFERENCES [dbo].[ContentType] ([ContentTypeID]),
    CONSTRAINT [FK_ContentItem_Region] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Region] ([Id]),
    CONSTRAINT [FK_ContentItem_Status1] FOREIGN KEY ([StatusID]) REFERENCES [dbo].[Status] ([StatusID]),
    CONSTRAINT [FK_ContentItem_User2] FOREIGN KEY ([CreatedByUserID]) REFERENCES [dbo].[User] ([UserId]),
    CONSTRAINT [FK_ContentItem_User3] FOREIGN KEY ([ModifiedByUserID]) REFERENCES [dbo].[User] ([UserId]), 
    CONSTRAINT [FK_ContentItem_ExpirationReviewState] FOREIGN KEY ([ReviewStateId]) REFERENCES [ExpirationReviewState]([ReviewStateId])
);

GO
CREATE NONCLUSTERED INDEX [ContentItemRegion_IX]
    ON [dbo].[ContentItem]([PrimaryContentItemID] ASC, [RegionId] ASC)
    INCLUDE([ContentItemID], [FrontEndUrl], [CreatedDate], [ModifiedDate]);