﻿CREATE TABLE [dbo].[User] (
    [UserId]                                 INT           IDENTITY (1, 1) NOT NULL,
    [UserName]                               VARCHAR (255) NOT NULL,
    [FullName]                               VARCHAR (255) DEFAULT ('') NOT NULL,
    [Email]                                  VARCHAR (255) NULL,
	[Region]                                 NCHAR   (6)   DEFAULT ('us') NOT NULL,
	[Country]								 VARCHAR (255) DEFAULT ('us') NOT NULL,
	[Language]								 VARCHAR (255) DEFAULT ('en') NOT NULL,
	[IssuerId]                               VARCHAR (25)  NULL,
    [IsDisabled]							 BIT           DEFAULT ((0)) NOT NULL,
	[IsBlocked]								 BIT           DEFAULT ((0)) NOT NULL,
    [ExcludedFromDomoApi]                    BIT           DEFAULT ((0)) NOT NULL,
    [ModifiedDate]							 DATETIME	   DEFAULT GETDATE() NOT NULL,
	CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([UserId] ASC),
    FOREIGN KEY ([Region]) REFERENCES [dbo].[Region] ([Id])
);

GO
CREATE NONCLUSTERED INDEX [ix_User_IssuerId]
    ON [dbo].[User]([IssuerId] ASC)
    INCLUDE([UserName], [FullName]) WITH (FILLFACTOR = 90);

GO
CREATE TRIGGER UserCacheInvalidator ON [dbo].[User] AFTER INSERT, UPDATE
AS
	IF EXISTS (SELECT 1 FROM Cache WHERE Id = 'User')
		UPDATE Cache SET LastInvalidationCalled = GETDATE(), MustInvalidate = 1 WHERE Id = 'User'
	ELSE	
		INSERT INTO Cache (Id, LastInvalidationCalled, MustInvalidate) VALUES ('User', GETDATE(), 1)