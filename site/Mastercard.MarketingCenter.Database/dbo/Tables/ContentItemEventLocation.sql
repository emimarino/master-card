﻿CREATE TABLE [dbo].[ContentItemEventLocation]
(
    [ContentItemId]	VARCHAR (25) NOT NULL,
    [EventLocationId] INT NOT NULL,
    CONSTRAINT [PK_ContentItemEventLocation] PRIMARY KEY CLUSTERED ([ContentItemId] ASC, [EventLocationId] ASC),
    CONSTRAINT [FK_ContentItemEventLocation_ContentItem] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[Offer] ([ContentItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_ContentItemEventLocation_EventLocation] FOREIGN KEY ([EventLocationId]) REFERENCES [dbo].[EventLocation] ([EventLocationId])
)
