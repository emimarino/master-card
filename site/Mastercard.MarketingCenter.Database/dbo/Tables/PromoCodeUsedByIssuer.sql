﻿CREATE TABLE [dbo].[PromoCodeUsedByIssuer] (
    [PromoCodeUsedByIssuerID] INT          IDENTITY (1, 1) NOT NULL,
    [PromoCodeID]             VARCHAR (25) NOT NULL,
    [IssuerID]                VARCHAR (25) NOT NULL,
    [NumPromotionsUsed]       INT          CONSTRAINT [DF_PromoCodeUsedByIssuer_NumPromotionsUsed] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_PromoCodeUsedByIssuer] PRIMARY KEY CLUSTERED ([PromoCodeUsedByIssuerID] ASC),
    CONSTRAINT [FK_PromoCodeUsedByIssuer_Issuer] FOREIGN KEY ([IssuerID]) REFERENCES [dbo].[Issuer] ([IssuerID]),
    CONSTRAINT [FK_PromoCodeUsedByIssuer_PromoCode] FOREIGN KEY ([PromoCodeID]) REFERENCES [dbo].[PromoCode] ([PromoCodeID])
);

