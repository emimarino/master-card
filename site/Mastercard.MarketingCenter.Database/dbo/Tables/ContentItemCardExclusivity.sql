﻿CREATE TABLE [dbo].[ContentItemCardExclusivity] (
    [ContentItemId]		VARCHAR (25) NOT NULL,
    [CardExclusivityId] NVARCHAR (25) NOT NULL,
    CONSTRAINT [PK_ContentItemCardExclusivity] PRIMARY KEY CLUSTERED ([ContentItemId] ASC, [CardExclusivityId] ASC),
    CONSTRAINT [FK_ContentItemCardExclusivity_ContentItem] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[ContentItem] ([ContentItemId]) ON DELETE CASCADE,
    CONSTRAINT [FK_ContentItemCardExclusivity_CardExclusivity] FOREIGN KEY ([CardExclusivityId]) REFERENCES [dbo].[CardExclusivity] ([CardExclusivityId]) ON DELETE CASCADE
);