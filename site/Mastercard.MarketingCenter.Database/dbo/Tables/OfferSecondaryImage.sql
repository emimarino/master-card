﻿CREATE TABLE [dbo].[OfferSecondaryImage]
(
	[SecondaryImageId]  INT             IDENTITY (1, 1) NOT NULL,
    [ContentItemId] VARCHAR (25)    NOT NULL,
    [ImageUrl]       NVARCHAR (2000) NULL,
    CONSTRAINT [PK_OfferSecondaryImageId] PRIMARY KEY CLUSTERED ([SecondaryImageID] ASC),
    CONSTRAINT [FK_OfferSecondaryImage_Offer] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[Offer] ([ContentItemId]) ON DELETE CASCADE
);
