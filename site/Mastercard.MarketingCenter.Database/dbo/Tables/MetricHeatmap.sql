﻿CREATE TABLE [dbo].[MetricHeatmap] (
    [CID]              VARCHAR (25) NOT NULL,
    [SpendPerActive]   CHAR (1)     NOT NULL,
    [PerXbSpend]       CHAR (1)     NOT NULL,
    [XbActive]         CHAR (1)     NOT NULL,
    [TrxPerActive]     CHAR (1)     CONSTRAINT [DF_MetricHeatmap_ProductGraduation] DEFAULT ('R') NOT NULL,
    [HighMcc]          CHAR (1)     CONSTRAINT [DF_MetricHeatmap_IncreaseEngagement] DEFAULT ('R') NOT NULL,
    [EcActive]         CHAR (1)     CONSTRAINT [DF_MetricHeatmap_Acquisition] DEFAULT ('R') NOT NULL,
    [DeclineRate]      CHAR (1)     CONSTRAINT [DF_MetricHeatmap_MobileEngagement] DEFAULT ('R') NOT NULL,
    [DivCategorySpend] CHAR (1)     CONSTRAINT [DF_MetricHeatmap_DivCategorySpend] DEFAULT ('G') NOT NULL,
    CONSTRAINT [PK_MetricHeatmap] PRIMARY KEY CLUSTERED ([CID] ASC)
);

