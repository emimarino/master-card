﻿CREATE TABLE [dbo].[MostPopularActivity] (
    [Id]                   BIGINT       IDENTITY (1, 1) NOT NULL,
    [ContentTypeId]        VARCHAR (25) NOT NULL,
    [ContentItemId]        VARCHAR (25) NOT NULL,
    [RegionId]             NCHAR (6)    NOT NULL,
    [LastVisitDate]        DATETIME     NOT NULL,
    [VisitCount]           INT          NOT NULL,
    [MostPopularProcessId] BIGINT       NOT NULL,
    CONSTRAINT [PK_MostPopularActivity] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_MostPopularActivity_ContentItem] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[ContentItem] ([ContentItemID]),
    CONSTRAINT [FK_MostPopularActivity_ContentType] FOREIGN KEY ([ContentTypeId]) REFERENCES [dbo].[ContentType] ([ContentTypeID]),
    CONSTRAINT [FK_MostPopularActivity_MostPopularProcess] FOREIGN KEY ([MostPopularProcessId]) REFERENCES [dbo].[MostPopularProcess] ([Id]),
    CONSTRAINT [FK_MostPopularActivity_Region] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Region] ([Id])
);

