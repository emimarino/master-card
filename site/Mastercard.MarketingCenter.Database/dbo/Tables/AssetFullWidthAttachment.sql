﻿CREATE TABLE [dbo].[AssetFullWidthAttachment] (
    [AttachmentID]  INT             IDENTITY (1, 1) NOT NULL,
    [ContentItemId] VARCHAR (25)    NOT NULL,
    [FileUrl]       NVARCHAR (2000) NULL,
    CONSTRAINT [PK_AssetFullWidthAttachment] PRIMARY KEY CLUSTERED ([AttachmentID] ASC),
    CONSTRAINT [FK_AssetFullWidthAttachment_AssetFullWidth] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[AssetFullWidth] ([ContentItemId]) ON DELETE CASCADE
);



