﻿CREATE TABLE ContentComment(
ContentCommentId int Primary Key IDENTITY(1,1), 
SenderUserId int NOT NULL ,  --this is manly for  display and historic data since, the filter will be done by conetnitemid
CreationDate datetime NOT NULL ,
CommentBody nvarchar(MAX) NOT NULL,
ContentItemId varchar(25) NOT NULL, 
CONSTRAINT [FK_ContentComment_Sender_User] FOREIGN KEY (SenderUserId) REFERENCES [User]([UserId]), 
CONSTRAINT [FK_ContentComment_ContentItem] FOREIGN KEY ([ContentItemId]) REFERENCES [ContentItem]([ContentItemID])
)

go
 create nonclustered index IX_ContentItemId_ContentComment on ContentComment(ContentItemId)  