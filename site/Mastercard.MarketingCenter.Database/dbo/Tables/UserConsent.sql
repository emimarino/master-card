﻿CREATE TABLE [dbo].[UserConsent]
(
	UserConsentId INT NOT NULL PRIMARY KEY Identity(1,1),
	 UserId int,
      ConsentDate DateTime ,
        ConsentFileDataId int NOT NULL  ,
    CONSTRAINT [FK_UserConsent_ConsentFileDataId] FOREIGN KEY (ConsentFileDataId) REFERENCES ConsentFileData(ConsentFileDataId), 
    CONSTRAINT [FK_UserConsent_Users] FOREIGN KEY (UserId) REFERENCES [User]([UserId])
        
)