﻿CREATE TABLE [dbo].[VdpOrderInformation] (
    [VdpOrderInformationID] INT             IDENTITY (1, 1) NOT NULL,
    [OrderID]               INT             NOT NULL,
    [Description]           VARCHAR (MAX)   NULL,
    [PriceAdjustment]       DECIMAL (18, 2) NULL,
    [PostageCost]           DECIMAL (18, 2) NULL,
    [Completed]             BIT             NOT NULL,
    CONSTRAINT [PK_VdpOrderInformation] PRIMARY KEY CLUSTERED ([VdpOrderInformationID] ASC)
);

