﻿CREATE TABLE [dbo].[Language] (
    [LanguageId]        BIGINT  IDENTITY (1, 1) NOT NULL,
    [Code]				VARCHAR (25)	NOT NULL,
    [Name]				VARCHAR (255)   NOT NULL,
    [TagIdentifier]     VARCHAR (255)   NULL,
    CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED ([LanguageId] ASC)
);