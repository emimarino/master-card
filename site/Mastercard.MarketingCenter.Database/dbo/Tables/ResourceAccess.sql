﻿CREATE TABLE [dbo].[ResourceAccess] (
    [ResourceAccessId] BIGINT        IDENTITY (1, 1) NOT NULL,
    [IdentityId]       INT           NOT NULL,
    [IdentityType]     INT           NOT NULL,
    [Area]             VARCHAR (100) NULL,
    [Controller]       VARCHAR (100) NOT NULL,
    [Action]           VARCHAR (100) NULL,
    [Id]               VARCHAR (100) NULL,
    [QueryString]      VARCHAR (250) NULL,
    CONSTRAINT [PK_ResourceAccess] PRIMARY KEY CLUSTERED ([ResourceAccessId] ASC) WITH (FILLFACTOR = 90)
);

