﻿CREATE TABLE [dbo].[CartItem] (
    [CartItemID]            INT          IDENTITY (1, 1) NOT NULL,
    [CartID]                INT          NOT NULL,
    [ContentItemID]         VARCHAR (25) NULL,
    [ShippingInformationID] INT          NULL,
    [Quantity]              INT          NULL,
    [ElectronicDelivery]    BIT          CONSTRAINT [DF_CartItems_ElectronicDelivery] DEFAULT ((0)) NOT NULL,
    [EstimatedDistribution] INT          NULL,
    CONSTRAINT [PK_CartItems] PRIMARY KEY CLUSTERED ([CartItemID] ASC)
);

