﻿CREATE TABLE [dbo].[YRAssessmentCriteriaRecommendation] (
    [ContentItemID]    VARCHAR (25) NOT NULL,
    [RecommendationID] VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_AssessmentCriteriaRecommendation] PRIMARY KEY CLUSTERED ([ContentItemID] ASC, [RecommendationID] ASC),
    CONSTRAINT [FK_AssessmentCriteriaRecommendation_AssessmentCriteria] FOREIGN KEY ([ContentItemID]) REFERENCES [dbo].[AssessmentCriteria] ([ContentItemID]),
    CONSTRAINT [FK_AssessmentCriteriaRecommendation_Recommendation] FOREIGN KEY ([RecommendationID]) REFERENCES [dbo].[Recommendation] ([ContentItemId])
);

