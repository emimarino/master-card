﻿CREATE TABLE dbo.BrokenContentLink
	(
	BrokenContentLinkId int NOT NULL IDENTITY (1, 1),
	Title nvarchar(255) NULL,
	
	ReferrerUrl nvarchar(1000) NULL,
	SourceItemId varchar(25) NULL,
	ContentType nvarchar(25) NOT NULL,
	LastRequested datetime NOT NULL,
	RegionId nchar(6) NOT NULL,
	BrokenLinkReference nvarchar(1000) NULL,
     BrokenLinkText nvarchar(1000) NULL,
      FrontEndUrl nvarchar(1000) NULL,
	  BackEndUrl nvarchar(1000) NULL,
	  ContentTypeId varchar(max)
	CONSTRAINT [PK_BrokenContentLink] PRIMARY KEY CLUSTERED (BrokenContentLinkId ASC), 
    [BrokenContentStatus] VARCHAR(255) NULL
	);