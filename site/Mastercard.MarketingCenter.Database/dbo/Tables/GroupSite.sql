﻿CREATE TABLE [dbo].[GroupSite] (
    [GroupID] INT              NOT NULL,
    [SiteID]  UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_GroupSite] PRIMARY KEY CLUSTERED ([GroupID] ASC, [SiteID] ASC),
    CONSTRAINT [FK_GroupSite_Group] FOREIGN KEY ([GroupID]) REFERENCES [dbo].[Group] ([GroupID]) ON DELETE CASCADE,
    CONSTRAINT [FK_GroupSite_Site] FOREIGN KEY ([SiteID]) REFERENCES [SLAM].[Site] ([SiteID]) ON DELETE CASCADE
);

