﻿CREATE TABLE [dbo].[FtpAccount] (
    [FtpAccountID] INT           IDENTITY (1, 1) NOT NULL,
    [OrderID]      INT           NOT NULL,
    [Server]       VARCHAR (200) NOT NULL,
    [UserName]     VARCHAR (200) NOT NULL,
    [Password]     VARCHAR (200) NOT NULL,
    [FileAccepted] BIT           NOT NULL,
    [DateCreated]  DATETIME      CONSTRAINT [DF_FtpAccounts_DateCreated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_FtpAccounts] PRIMARY KEY CLUSTERED ([FtpAccountID] ASC)
);

