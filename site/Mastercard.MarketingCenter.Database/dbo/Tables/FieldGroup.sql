﻿CREATE TABLE [dbo].[FieldGroup] (
    [FieldGroupId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]         VARCHAR (250) NOT NULL,
    [Description]  VARCHAR (500) NULL,
    CONSTRAINT [PK_FieldGroup] PRIMARY KEY CLUSTERED ([FieldGroupId] ASC) WITH (FILLFACTOR = 90)
);

