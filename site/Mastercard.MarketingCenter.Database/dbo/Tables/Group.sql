﻿CREATE TABLE [dbo].[Group] (
    [GroupID] INT           NOT NULL,
    [Name]    VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED ([GroupID] ASC)
);

