﻿CREATE TABLE [dbo].[ContentTypeClientEvent] (
    [ContentTypeClientEventId] INT           IDENTITY (1, 1) NOT NULL,
    [ContentTypeID]            VARCHAR (25)  NOT NULL,
    [Name]                     VARCHAR (50)  NOT NULL,
    [FiredOn]                  VARCHAR (50)  NOT NULL,
    [ClientEventCode]          VARCHAR (MAX) NOT NULL,
    [ContentAction]            VARCHAR (MAX) NULL,
    CONSTRAINT [PK_ContentTypeClientEvent] PRIMARY KEY CLUSTERED ([ContentTypeClientEventId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ContentTypeClientEvent_ContentType] FOREIGN KEY ([ContentTypeID]) REFERENCES [dbo].[ContentType] ([ContentTypeID])
);



