﻿CREATE TABLE [dbo].[ContentItemSegmentation] (
    [ContentItemId]  VARCHAR (25) NOT NULL,
    [SegmentationId] VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_ContentItemSegmentation] PRIMARY KEY CLUSTERED ([ContentItemId] ASC, [SegmentationId] ASC),
    CONSTRAINT [FK_ContentItemSegmentation_ContentItem] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[ContentItem] ([ContentItemID]),
    CONSTRAINT [FK_ContentItemSegmentation_Segmentation] FOREIGN KEY ([SegmentationId]) REFERENCES [dbo].[Segmentation] ([SegmentationId])
);

