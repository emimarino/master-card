﻿CREATE TABLE [dbo].[MarqueeSlide] (
    [ContentItemId]             VARCHAR (25)   NOT NULL,
    [Title]                     NVARCHAR (255) NULL,
    [Image]                     NVARCHAR(255)  NOT NULL,
    [LongDescription]           NVARCHAR (MAX) NULL,
    [MainUrl]                   NVARCHAR (255) NULL,
    [OrderNumber]               INT            CONSTRAINT [DF_MarqueeSlide_OrderNumber] DEFAULT ((1)) NOT NULL,
    [SpecialAudienceAccess]     VARCHAR (255)  NULL,
    [RestrictToSpecialAudience] VARCHAR (255)  NULL,
    [ButtonText]                NVARCHAR (25)  NULL,
    [InnerDescription]          NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_MarqueeSlide] PRIMARY KEY CLUSTERED ([ContentItemId] ASC) WITH (FILLFACTOR = 90)
);



