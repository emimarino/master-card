﻿CREATE TABLE [dbo].[IssuerUnmatched] (
    [IssuerUnmatchedId] INT           IDENTITY (1, 1) NOT NULL,
    [Error]             VARCHAR (50)  NOT NULL,
    [Message]           VARCHAR (500) NOT NULL,
    [IssuerID]          VARCHAR (25)  NOT NULL,
    [ImportedIcaId]     INT           NULL,
    [UserId]            INT           NOT NULL,
    [Date]              DATETIME      NOT NULL,
    CONSTRAINT [PK_IssuerUnmatched] PRIMARY KEY CLUSTERED ([IssuerUnmatchedId] ASC)
);

