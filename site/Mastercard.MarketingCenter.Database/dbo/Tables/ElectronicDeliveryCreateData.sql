﻿CREATE TABLE [dbo].[ElectronicDeliveryCreateData] (
	[ElectronicDeliveryID] UNIQUEIDENTIFIER PRIMARY KEY
	,[ContentItemId] VARCHAR(25) NULL
	,[UserName] NVARCHAR(256) NULL
	,[Region] NCHAR(6) NULL
	,FOREIGN KEY ([Region]) REFERENCES [dbo].[Region]([Id])
	)