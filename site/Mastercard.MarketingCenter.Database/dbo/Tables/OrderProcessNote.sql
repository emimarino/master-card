﻿CREATE TABLE [dbo].[OrderProcessNote] (
    [ID]       INT             IDENTITY (1, 1) NOT NULL,
    [OrderID]  INT             NULL,
    [Date]     DATETIME        NOT NULL,
    [Note]     NVARCHAR (1000) NULL,
    [Approval] NVARCHAR (255)  NOT NULL,
    [IP]       NVARCHAR (50)   NULL,
    CONSTRAINT [PK_OrderProcessNotes] PRIMARY KEY CLUSTERED ([ID] ASC)
);

