﻿CREATE TABLE [dbo].[InsertsBundleEvent] (
    [InsertsBundleEventsID] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                  VARCHAR (1024) NOT NULL,
    [Created]               DATETIME       CONSTRAINT [DF_InsertsBundleEvents_Created] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_InsertsBundleEvents] PRIMARY KEY CLUSTERED ([InsertsBundleEventsID] ASC)
);

