﻿CREATE TABLE [dbo].[FeatureLocationScope] (
    [FeatureLocationID] VARCHAR (25) NOT NULL,
    [ContentTypeID]     VARCHAR (25) NOT NULL,
    CONSTRAINT [PK_FeatureLocationScope] PRIMARY KEY CLUSTERED ([FeatureLocationID] ASC, [ContentTypeID] ASC),
    CONSTRAINT [FK_FeatureLocationScope_ContentType] FOREIGN KEY ([ContentTypeID]) REFERENCES [dbo].[ContentType] ([ContentTypeID]) ON DELETE CASCADE,
    CONSTRAINT [FK_FeatureLocationScope_FeatureLocation] FOREIGN KEY ([FeatureLocationID]) REFERENCES [dbo].[FeatureLocation] ([FeatureLocationID]) ON DELETE CASCADE
);

