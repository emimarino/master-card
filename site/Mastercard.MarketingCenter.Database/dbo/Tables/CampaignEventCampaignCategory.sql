﻿CREATE TABLE [dbo].[CampaignEventCampaignCategory] (
    [CampaignEventId]   NVARCHAR (25) NOT NULL,
    [CampaignCategoryId]	NVARCHAR (25) NOT NULL,
    CONSTRAINT [PK_CampaignEventCampaignCategory] PRIMARY KEY CLUSTERED ([CampaignEventId] ASC, [CampaignCategoryId] ASC),
    CONSTRAINT [FK_CampaignEventCampaignCategory_CampaignEvent] FOREIGN KEY ([CampaignEventId]) REFERENCES [dbo].[CampaignEvent] ([CampaignEventId]) ON DELETE CASCADE,
    CONSTRAINT [FK_CampaignEventCampaignCategory_CampaignCategory] FOREIGN KEY ([CampaignCategoryId]) REFERENCES [dbo].[CampaignCategory] ([CampaignCategoryId])
);