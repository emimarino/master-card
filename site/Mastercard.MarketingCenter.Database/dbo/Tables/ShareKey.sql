﻿CREATE TABLE [dbo].[ShareKey] (
    [ShareKeyID]       INT           IDENTITY (1, 1) NOT NULL,
    [UserId]           INT           NOT NULL,
    [ShareKeyStatusID] INT           NOT NULL,
    [Key]              VARCHAR (250) NOT NULL,
    [CID]              VARCHAR (50)  NOT NULL,
    [EmailTo]          VARCHAR (255) NOT NULL,
    [DateKeyGenerated] DATETIME      NOT NULL,
    PRIMARY KEY CLUSTERED ([ShareKeyID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ShareKey_ShareKeyStatus] FOREIGN KEY ([ShareKeyStatusID]) REFERENCES [dbo].[UserKeyStatus] ([UserKeyStatusID]) ON DELETE CASCADE,
    CONSTRAINT [FK_ShareKey_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId]) ON DELETE CASCADE
);

