﻿CREATE TABLE [dbo].[ContentItemTriggerMarketing] (
    [ContentItemTriggerMarketingId] BIGINT        IDENTITY (1, 1) NOT NULL,
    [ContentItemId]                 VARCHAR (25)  NOT NULL,
    [Notify]                        BIT           NOT NULL,
    [Featured]                      BIT           NOT NULL,
    [Reasons]                       VARCHAR (MAX) NULL,
    [StatusId]                      INT           NOT NULL,
    [SavedDate]                     DATETIME      NOT NULL,
    [SentDailyDate]                 DATETIME      NULL,
    [SentWeeklyDate]                DATETIME      NULL,
    [IsNew]                         BIT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ContentItemTriggerMarketing] PRIMARY KEY CLUSTERED ([ContentItemTriggerMarketingId] ASC),
    CONSTRAINT [FK_ContentItemTriggerMarketing_ContentItem] FOREIGN KEY ([ContentItemId]) REFERENCES [dbo].[ContentItem] ([ContentItemID]),
    CONSTRAINT [FK_ContentItemTriggerMarketing_Status] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[Status] ([StatusID])
);

