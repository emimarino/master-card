﻿CREATE AGGREGATE [dbo].[Concat](@Value NVARCHAR (4000))
    RETURNS NVARCHAR (4000)
    EXTERNAL NAME [AWS.DatabaseFunctions].[Concat];

