﻿CREATE FUNCTION [dbo].[Split]
(@InputString NVARCHAR (4000), @Delimiter NVARCHAR (4000))
RETURNS 
     TABLE (
        [Token] NVARCHAR (4000) NULL)
AS
 EXTERNAL NAME [AWS.DatabaseFunctions].[UserDefinedFunctions].[Split]

