﻿
CREATE FUNCTION [dbo].[GetOrderItems]
(
	@Orderid AS int
	
)
RETURNS VARCHAR(MAX) 

AS
BEGIN

DECLARE @oResult VARCHAR(MAX)

SELECT	@oResult= COALESCE(@oResult + ', ','') + ItemName
FROM	(select distinct(ItemName) ItemName, OrderID
		 from	OrderItem WITH (NOLOCK)
		 WHERE	OrderID = @Orderid) items 
ORDER BY ItemName

RETURN @oResult


END
