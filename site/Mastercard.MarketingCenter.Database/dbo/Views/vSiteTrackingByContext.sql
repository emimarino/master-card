﻿CREATE VIEW vSiteTrackingByContext
AS
SELECT *
FROM dbo.SiteTracking
WHERE isnull(context_info(), 0x) = 0x2128506

UNION

SELECT *
FROM [Archive].SiteTracking
WHERE isnull(context_info(), 0x) = 0x;