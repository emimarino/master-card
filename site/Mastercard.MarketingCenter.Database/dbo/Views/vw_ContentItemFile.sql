﻿CREATE VIEW [dbo].[vw_ContentItemFile]
AS
SELECT DISTINCT CAST([Id] AS NVARCHAR(max)) [Id]
	,CAST([Filename] AS NVARCHAR(max)) AS [Filename]
	,CAST([Column] AS NVARCHAR(max)) AS [Column]
	,CAST([Table] AS NVARCHAR(max)) AS [Table]
	,COALESCE(s.[Name], files.[Status], '') 'Status'
	,COALESCE(ci.[ModifiedDate], files.[Date], '') 'Date'
FROM (
	SELECT [ContentItemId] AS [Id]
		,'AssetAttachment' AS 'Table'
		,[FileUrl] AS [Filename]
		,'FileUrl' AS 'Column'
		,'' AS 'Date'
		,'' AS 'Status'
	FROM [AssetAttachment]
	
	UNION
	
	SELECT [ContentItemId] AS [Id]
		,'AssetFullWidthAttachment' AS 'Table'
		,[FileUrl] AS [Filename]
		,'FileUrl' AS 'Column'
		,'' AS 'Date'
		,'' AS 'Status'
	FROM [AssetFullWidthAttachment]
	
	UNION
	
	SELECT [ContentItemId] AS [Id]
		,'DownloadableAsset' AS 'Table'
		,[Filename]
		,'Filename' AS 'Column'
		,'' AS 'Date'
		,'' AS 'Status'
	FROM [DownloadableAsset]
	
	UNION
	
	SELECT [ContentItemId] AS [Id]
		,'DownloadableAssetAttachment' AS 'Table'
		,[FileUrl] AS [Filename]
		,'Filename' AS 'Column'
		,'' AS 'Date'
		,'' AS 'Status'
	FROM [DownloadableAssetAttachment]
	
	UNION
	
	SELECT CAST([ElectronicDeliveryID] AS NVARCHAR(max)) AS [Id]
		,'ElectronicDelivery' AS 'Table'
		,[FileLocation] AS [Filename]
		,'FileLocation' AS 'Column'
		,[ExpirationDate] AS 'Date'
		,'' AS 'Status'
	FROM [ElectronicDelivery]
	
	UNION
	
	SELECT [ContentItemId] AS [Id]
		,'HtmlDownload' AS 'Table'
		,[Filename]
		,'Filename' AS 'Column'
		,'' AS 'Date'
		,'' AS 'Status'
	FROM [HtmlDownload]
	
	UNION
	
	SELECT [ContentItemId] AS [Id]
		,'OrderableAsset' AS 'Table'
		,[PDFPreview] AS [Filename]
		,'PDFPreview' AS 'Column'
		,'' AS 'Date'
		,'' AS 'Status'
	FROM [OrderableAsset]
	
	UNION
	
	SELECT [ContentItemId] AS [Id]
		,'OrderableAsset' AS 'Table'
		,[ElectronicDeliveryFile] AS [Filename]
		,'ElectronicDeliveryFile' AS 'Column'
		,'' AS 'Date'
		,'' AS 'Status'
	FROM [OrderableAsset]
	
	UNION
	
	SELECT [ContentItemId] AS [Id]
		,'Offer' AS 'Table'
		,[DownloadZipFile] AS [Filename]
		,'DownloadZipFile' AS 'Column'
		,'' AS 'Date'
		,'' AS 'Status'
	FROM [Offer]
	
	UNION
	
	SELECT [ContentItemId] AS [Id]
		,'OfferAttachment' AS 'Table'
		,[FileUrl] AS [Filename]
		,'FileUrl' AS 'Column'
		,'' AS 'Date'
		,'' AS 'Status'
	FROM [OfferAttachment]
	
	UNION
	
	SELECT [ContentItemId] AS [Id]
		,'OrderableAssetAttachment' AS 'Table'
		,[FileUrl] AS [Filename]
		,'FileUrl' AS 'Column'
		,'' AS 'Date'
		,'' AS 'Status'
	FROM [OrderableAssetAttachment]
	
	UNION
	
	SELECT CAST([OrderItemID] AS NVARCHAR(max)) AS [Id]
		,'OrderItem' AS 'Table'
		,[ProofPDF] AS [Filename]
		,'ProofPDF' AS 'Column'
		,[OrderDate] AS 'Date'
		,[OrderStatus] AS 'Status'
	FROM [OrderItem] oi
	JOIN [Order] o ON o.[OrderID] = oi.[OrderID]
	
	UNION
	
	SELECT [ContentItemId] AS [Id]
		,'ProgramAttachment' AS 'Table'
		,[FileUrl] AS [Filename]
		,'FileUrl' AS 'Column'
		,'' AS 'Date'
		,'' AS 'Status'
	FROM [ProgramAttachment]
	
	UNION
	
	SELECT [ContentItemId] AS [Id]
		,'Webinar' AS 'Table'
		,[VideoFile] AS [Filename]
		,'VideoFile' AS 'Column'
		,'' AS 'Date'
		,'' AS 'Status'
	FROM [Webinar]
	) files
LEFT JOIN [ContentItem] ci ON ci.[ContentItemID] = files.[Id]
LEFT JOIN [Status] s ON ci.[StatusID] = s.[StatusID]
WHERE LEN([Filename]) > 0;