﻿CREATE VIEW vDomoActivityApiByContext
AS
SELECT *
FROM dbo.DomoActivityApi
WHERE isnull(context_info(), 0x) = 0x2128506

UNION

SELECT *
FROM [Archive].DomoActivityApi
WHERE isnull(context_info(), 0x) = 0x;