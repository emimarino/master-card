﻿CREATE VIEW [dbo].[vw_AllFiles]
AS
SELECT CAST([TableId] AS NVARCHAR(255)) AS [TableId]
	,CAST([Table] AS NVARCHAR(255)) AS [Table]
	,CAST([Column] AS NVARCHAR(255)) AS [Column]
	,CAST([Filename] AS NVARCHAR(max)) AS [Filename]
FROM (
	SELECT [ContentItemId] 'TableId'
		,'Asset' 'Table'
		,[Column]
		,[Filename]
	FROM [Asset]
	UNPIVOT([Filename] FOR [Column] IN (
				MainImage
				,ThumbnailImage
				)) AS [AssetFiles]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'AssetAttachment' 'Table'
		,'FileUrl' AS [Column]
		,[FileUrl] AS [Filename]
	FROM [AssetAttachment]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'AssetFullWidth' 'Table'
		,'ThumbnailImage' AS [Column]
		,[ThumbnailImage] AS [Filename]
	FROM [AssetFullWidth]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'AssetFullWidthAttachment' 'Table'
		,'FileUrl' AS [Column]
		,[FileUrl] AS [Filename]
	FROM [AssetFullWidthAttachment]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'DownloadableAssetFiles' 'Table'
		,[Column]
		,[DownloadableAssetFiles].[Filename]
	FROM (
		SELECT [ContentItemId]
			,[MainImage]
			,[ThumbnailImage]
			,[Filename] AS [FileUrl]
		FROM [DownloadableAsset]
		) ReduxDownloadableAsset
	UNPIVOT([Filename] FOR [Column] IN (
				[MainImage]
				,[ThumbnailImage]
				,[FileUrl]
				)) AS [DownloadableAssetFiles]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'DownloadableAssetAttachment' 'Table'
		,'FileUrl' AS [Column]
		,[FileUrl] AS [Filename]
	FROM [DownloadableAssetAttachment]
	
	UNION
	
	SELECT CAST([ElectronicDeliveryID] AS NVARCHAR(255)) 'TableId'
		,'ElectronicDelivery' 'Table'
		,'FileLocation' AS [Column]
		,[FileLocation] AS [Filename]
	FROM [ElectronicDelivery]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'Faq' 'Table'
		,'ThumbnailImage' AS [Column]
		,[ThumbnailImage] AS [Filename]
	FROM [Faq]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'HtmlDownload' 'Table'
		,'Filename' AS [Column]
		,[Filename]
	FROM [HtmlDownload]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'MarqueeSlide' 'Table'
		,'Image' AS [Column]
		,[Image] AS [Filename]
	FROM [MarqueeSlide]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'OrderableAsset' 'Table'
		,[Column]
		,[Filename]
	FROM (
		SELECT ContentItemId
			,CAST(MainImage AS NVARCHAR(max)) AS MainImage
			,CAST(ThumbnailImage AS NVARCHAR(max)) AS ThumbnailImage
			,CAST(PDFPreview AS NVARCHAR(max)) AS PDFPreview
			,CAST(ElectronicDeliveryFile AS NVARCHAR(max)) AS ElectronicDeliveryFile
		FROM [OrderableAsset]
		) oa
	UNPIVOT([Filename] FOR [Column] IN (
				MainImage
				,ThumbnailImage
				,PDFPreview
				,ElectronicDeliveryFile
				)) AS [OrderableAssetFiles]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'OrderableAssetAttachment' 'Table'
		,'FileUrl' AS [Column]
		,[FileUrl] AS [Filename]
	FROM [OrderableAssetAttachment]
	
	UNION
	
	SELECT CAST([OrderItemID] AS NVARCHAR(255)) 'TableId'
		,'OrderItem' 'Table'
		,'ProofPDF' AS [Column]
		,[ProofPDF] AS [Filename]
	FROM [OrderItem]
	
	UNION
	
	SELECT DISTINCT [ContentItemId] 'TableId'
		,'Page' 'Table'
		,[Column]
		,[Filename]
	FROM [Page]
	UNPIVOT([Filename] FOR [Column] IN (
				BottomBackgroundImage
				,HeaderBackgroundImage
				,BottomAsideImage
				,HeaderAsideImage
				)) AS [PageFiles]
	
	UNION
	
	SELECT [ProcessorID] 'TableId'
		,'CobrandLogo' 'Table'
		,'CobrandLogo' AS [Column]
		,[CobrandLogo]
	FROM [Processor]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'Program' 'Table'
		,[Column]
		,[Filename]
	FROM [Program]
	UNPIVOT([Filename] FOR [Column] IN (
				MainImage
				,ThumbnailImage
				)) AS [ProgramFiles]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'ProgramAttachment' 'Table'
		,'FileUrl' AS [Column]
		,[FileUrl] AS [Filename]
	FROM [ProgramAttachment]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'QuickLink' 'Table'
		,'Image' AS [Column]
		,[Image] AS [Filename]
	FROM [QuickLink]
	
	UNION
	
	SELECT [SearchTermID] 'TableId'
		,'SearchTerm' 'Table'
		,'Image' AS [Column]
		,[Image]
	FROM [SearchTerm]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'Link' 'Table'
		,'ThumbnailImage' AS [Column]
		,[ThumbnailImage] AS [Filename]
	FROM [Link]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'Webinar' 'Table'
		,'S3File' AS [Column]
		,[VideoFile] AS [Filename]
	FROM [Webinar]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'UpcomingEvent' 'Table'
		,'Image' AS [Column]
		,[Image] AS [Filename]
	FROM [UpcomingEvent]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'Webinar' 'Table'
		,[Column]
		,[Filename]
	FROM (
		SELECT ContentItemId
			,CAST(VideoFile AS NVARCHAR(max)) AS VideoFile
			,CAST(ThumbnailImage AS NVARCHAR(max)) AS ThumbnailImage
			,CAST(StaticVideoImage AS NVARCHAR(max)) AS StaticVideoImage
		FROM [Webinar]
		) w
	UNPIVOT([Filename] FOR [Column] IN (
				VideoFile
				,ThumbnailImage
				,StaticVideoImage
				)) AS [WebinarFiles]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'Offer' 'Table'
		,[Column]
		,[Filename]
	FROM (
		SELECT ContentItemId
			,CAST(MainImage AS NVARCHAR(max)) AS MainImage
			,CAST(DownloadZipFile AS NVARCHAR(max)) AS DownloadZipFile
		FROM [Offer]
		) o
	UNPIVOT([Filename] FOR [Column] IN (
				MainImage
				,DownloadZipFile
				)) AS [WebinarFiles]
	
	UNION
	
	SELECT [ContentItemId] 'TableId'
		,'YoutubeVideo' 'Table'
		,'StaticVideoImage' AS [Column]
		,[StaticVideoImage] AS [Filename]
	FROM [YoutubeVideo]
	
	UNION
	
	SELECT [TableId]
		,[Table]
		,'VideoHtmlContent' AS [Column]
		,HtmlText AS [Filename]
	FROM (
		SELECT [ContentItemId] 'TableId'
			,'Faq' 'Table'
			,[Answer] AS HtmlText
		FROM [Faq]
		WHERE [Answer] LIKE '%data-video%'
			OR [Answer] LIKE '%video-link%'
			OR [Answer] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Page' 'Table'
			,[AsideContent] AS HtmlText
		FROM [Page]
		WHERE [AsideContent] LIKE '%data-video%'
			OR [AsideContent] LIKE '%video-link%'
			OR [AsideContent] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'AssetFullWidth' 'Table'
			,[BottomAreaDescription] AS HtmlText
		FROM [AssetFullWidth]
		WHERE [BottomAreaDescription] LIKE '%data-video%'
			OR [BottomAreaDescription] LIKE '%video-link%'
			OR [BottomAreaDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Page' 'Table'
			,[BottomParagraph] AS HtmlText
		FROM [Page]
		WHERE [BottomParagraph] LIKE '%data-video%'
			OR [BottomParagraph] LIKE '%video-link%'
			OR [BottomParagraph] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Asset' 'Table'
			,[CallToAction] AS HtmlText
		FROM [Asset]
		WHERE [CallToAction] LIKE '%data-video%'
			OR [CallToAction] LIKE '%video-link%'
			OR [CallToAction] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'DownloadableAsset' 'Table'
			,[CallToAction] AS HtmlText
		FROM [DownloadableAsset]
		WHERE [CallToAction] LIKE '%data-video%'
			OR [CallToAction] LIKE '%video-link%'
			OR [CallToAction] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'OrderableAsset' 'Table'
			,[CallToAction] AS HtmlText
		FROM [OrderableAsset]
		WHERE [CallToAction] LIKE '%data-video%'
			OR [CallToAction] LIKE '%video-link%'
			OR [CallToAction] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Program' 'Table'
			,[CallToAction] AS HtmlText
		FROM [Program]
		WHERE [CallToAction] LIKE '%data-video%'
			OR [CallToAction] LIKE '%video-link%'
			OR [CallToAction] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [PromoCodeID] 'TableId'
			,'PromoCode' 'Table'
			,[Description] AS HtmlText
		FROM [PromoCode]
		WHERE [Description] LIKE '%data-video%'
			OR [Description] LIKE '%video-link%'
			OR [Description] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [PricingScheduleID] 'TableId'
			,'PricingSchedule' 'Table'
			,[Details] AS HtmlText
		FROM [PricingSchedule]
		WHERE [Details] LIKE '%data-video%'
			OR [Details] LIKE '%video-link%'
			OR [Details] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Page' 'Table'
			,[HeaderParagraph] AS HtmlText
		FROM [Page]
		WHERE [HeaderParagraph] LIKE '%data-video%'
			OR [HeaderParagraph] LIKE '%video-link%'
			OR [HeaderParagraph] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Snippet' 'Table'
			,[Html] AS HtmlText
		FROM [Snippet]
		WHERE [Html] LIKE '%data-video%'
			OR [Html] LIKE '%video-link%'
			OR [Html] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [CustomizationOptionID] 'TableId'
			,'CustomizationOption' 'Table'
			,[Instructions] AS HtmlText
		FROM [CustomizationOption]
		WHERE [Instructions] LIKE '%data-video%'
			OR [Instructions] LIKE '%video-link%'
			OR [Instructions] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Page' 'Table'
			,[IntroCopy] AS HtmlText
		FROM [Page]
		WHERE [IntroCopy] LIKE '%data-video%'
			OR [IntroCopy] LIKE '%video-link%'
			OR [IntroCopy] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Asset' 'Table'
			,[LongDescription] AS HtmlText
		FROM [Asset]
		WHERE [LongDescription] LIKE '%data-video%'
			OR [LongDescription] LIKE '%video-link%'
			OR [LongDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'AssetFullWidth' 'Table'
			,[LongDescription] AS HtmlText
		FROM [AssetFullWidth]
		WHERE [LongDescription] LIKE '%data-video%'
			OR [LongDescription] LIKE '%video-link%'
			OR [LongDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'DownloadableAsset' 'Table'
			,[LongDescription] AS HtmlText
		FROM [DownloadableAsset]
		WHERE [LongDescription] LIKE '%data-video%'
			OR [LongDescription] LIKE '%video-link%'
			OR [LongDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'OrderableAsset' 'Table'
			,[LongDescription] AS HtmlText
		FROM [OrderableAsset]
		WHERE [LongDescription] LIKE '%data-video%'
			OR [LongDescription] LIKE '%video-link%'
			OR [LongDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Program' 'Table'
			,[LongDescription] AS HtmlText
		FROM [Program]
		WHERE [LongDescription] LIKE '%data-video%'
			OR [LongDescription] LIKE '%video-link%'
			OR [LongDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Webinar' 'Table'
			,[LongDescription] AS HtmlText
		FROM [Webinar]
		WHERE [LongDescription] LIKE '%data-video%'
			OR [LongDescription] LIKE '%video-link%'
			OR [LongDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [PricingScheduleID] 'TableId'
			,'PricingSchedule' 'Table'
			,[PricingTable] AS HtmlText
		FROM [PricingSchedule]
		WHERE [PricingTable] LIKE '%data-video%'
			OR [PricingTable] LIKE '%video-link%'
			OR [PricingTable] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Faq' 'Table'
			,[Question] AS HtmlText
		FROM [Faq]
		WHERE [Question] LIKE '%data-video%'
			OR [Question] LIKE '%video-link%'
			OR [Question] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'AssessmentCriteria' 'Table'
			,[ShortDescription] AS HtmlText
		FROM [AssessmentCriteria]
		WHERE [ShortDescription] LIKE '%data-video%'
			OR [ShortDescription] LIKE '%video-link%'
			OR [ShortDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Asset' 'Table'
			,[ShortDescription] AS HtmlText
		FROM [Asset]
		WHERE [ShortDescription] LIKE '%data-video%'
			OR [ShortDescription] LIKE '%video-link%'
			OR [ShortDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'AssetFullWidth' 'Table'
			,[ShortDescription] AS HtmlText
		FROM [AssetFullWidth]
		WHERE [ShortDescription] LIKE '%data-video%'
			OR [ShortDescription] LIKE '%video-link%'
			OR [ShortDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'DownloadableAsset' 'Table'
			,[ShortDescription] AS HtmlText
		FROM [DownloadableAsset]
		WHERE [ShortDescription] LIKE '%data-video%'
			OR [ShortDescription] LIKE '%video-link%'
			OR [ShortDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Faq' 'Table'
			,[ShortDescription] AS HtmlText
		FROM [Faq]
		WHERE [ShortDescription] LIKE '%data-video%'
			OR [ShortDescription] LIKE '%video-link%'
			OR [ShortDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Link' 'Table'
			,[ShortDescription] AS HtmlText
		FROM [Link]
		WHERE [ShortDescription] LIKE '%data-video%'
			OR [ShortDescription] LIKE '%video-link%'
			OR [ShortDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'OrderableAsset' 'Table'
			,[ShortDescription] AS HtmlText
		FROM [OrderableAsset]
		WHERE [ShortDescription] LIKE '%data-video%'
			OR [ShortDescription] LIKE '%video-link%'
			OR [ShortDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Page' 'Table'
			,[ShortDescription] AS HtmlText
		FROM [Page]
		WHERE [ShortDescription] LIKE '%data-video%'
			OR [ShortDescription] LIKE '%video-link%'
			OR [ShortDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Program' 'Table'
			,[ShortDescription] AS HtmlText
		FROM [Program]
		WHERE [ShortDescription] LIKE '%data-video%'
			OR [ShortDescription] LIKE '%video-link%'
			OR [ShortDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Recommendation' 'Table'
			,[ShortDescription] AS HtmlText
		FROM [Recommendation]
		WHERE [ShortDescription] LIKE '%data-video%'
			OR [ShortDescription] LIKE '%video-link%'
			OR [ShortDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Webinar' 'Table'
			,[ShortDescription] AS HtmlText
		FROM [Webinar]
		WHERE [ShortDescription] LIKE '%data-video%'
			OR [ShortDescription] LIKE '%video-link%'
			OR [ShortDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Page' 'Table'
			,[Terms] AS HtmlText
		FROM [Page]
		WHERE [Terms] LIKE '%data-video%'
			OR [Terms] LIKE '%video-link%'
			OR [Terms] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Offer' 'Table'
			,[ShortDescription] AS HtmlText
		FROM [Offer]
		WHERE [ShortDescription] LIKE '%data-video%'
			OR [ShortDescription] LIKE '%video-link%'
			OR [ShortDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Offer' 'Table'
			,[LongDescription] AS HtmlText
		FROM [Offer]
		WHERE [LongDescription] LIKE '%data-video%'
			OR [LongDescription] LIKE '%video-link%'
			OR [LongDescription] LIKE '%video-player-embed%'
		
		UNION
		
		SELECT [ContentItemId] 'TableId'
			,'Offer' 'Table'
			,[TermsAndConditions] AS HtmlText
		FROM [Offer]
		WHERE [TermsAndConditions] LIKE '%data-video%'
			OR [TermsAndConditions] LIKE '%video-link%'
			OR [TermsAndConditions] LIKE '%video-player-embed%'
		) HtmlContent
	) AllFilesRaw
WHERE LEN([Filename]) > 0;