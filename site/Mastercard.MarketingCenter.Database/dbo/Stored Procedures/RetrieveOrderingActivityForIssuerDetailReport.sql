﻿CREATE PROCEDURE [dbo].[RetrieveOrderingActivityForIssuerDetailReport] @IssuerID VARCHAR(25)
	,@StartDate DATETIME
	,@EndDate DATETIME
AS
BEGIN
	SET NOCOUNT ON;
	--
	DECLARE @Local_StartDate DATETIME = @StartDate;
	DECLARE @Local_EndDate DATETIME = @EndDate;
	DECLARE @Local_IssuerId VARCHAR(25) = @IssuerID;
	--
	EXEC [dbo].[UpdateSiteTrackingContext] @Local_StartDate;
	--
	WITH Downloads
	AS (
		SELECT [When]
			,Who
			,ContentItemID
		FROM (
			SELECT *
			FROM vSiteTrackingByContext
			WHERE [From] LIKE '/portal/asset/%'
				AND What LIKE '/portal/file/librarydownload%'
			) tempdownloads
		)
	--
	SELECT '' AS [OrderNumber]
		,[When] AS [Date]
		,U.[FullName] AS [User]
		,U.[Email]
		,P.Title AS Processor
		,I.Title AS 'FinancialInstitution'
		,U.UserName
		,s.SegmentationName AS AudienceSegments
		,DA.title AS 'ItemName'
		,'Download' AS 'ItemType'
		,NULL AS SKU
		,NULL AS Fulfillment
		,0 AS 'ItemPrice'
		,0 AS 'ItemQuantity'
		,NULL AS 'OrderStatus'
		,pr.contentitemid AS RelatedProgramID
		,PR.Title AS RelatedProgram
		,0 AS ED
		,0 AS POD
		,1 AS Download
		,0 AS EstDistribution
		,CONVERT(BIT, 0) AS AdvisorsTag
		,CONVERT(BIT, 0) AS ConsentGiven
	FROM Downloads AS d
	INNER JOIN [DownloadableAsset] DA ON DA.[ContentItemId] = d.ContentItemID
	INNER JOIN [DownloadableAssetRelatedProgram] DARP ON DA.[ContentItemId] = DARP.[DownloadableAssetContentItemId]
	INNER JOIN [Program] PR ON DARP.[ProgramContentItemId] = PR.[ContentItemId]
	INNER JOIN [aspnet_Users] AU ON 'mastercardmembers:' + AU.[UserName] = d.[Who]
	INNER JOIN [User] U ON U.[UserName] = 'mastercardmembers:' + AU.[UserName]
	INNER JOIN [Issuer] I ON I.[IssuerID] = U.[IssuerId]
	LEFT JOIN IssuerSegmentation iss ON i.IssuerId = iss.IssuerId
	LEFT JOIN Segmentation s ON s.SegmentationId = iss.SegmentationId
	INNER JOIN [IssuerProcessor] IPP ON IPP.[IssuerID] = I.[IssuerId]
	INNER JOIN [Processor] P ON P.[ProcessorID] = IPP.[ProcessorID]
	WHERE d.[When] >= @Local_StartDate
		AND d.[When] < @Local_EndDate
		AND P.Title NOT IN ('A Plus')
		AND I.IsMastercardIssuer = 0
		AND I.[IssuerID] <> 'a05'
		AND I.RegionId = 'us'
		AND (
			I.IssuerID = @Local_IssuerId
			OR @Local_IssuerId = 'all'
			)
		AND PR.ContentItemId NOT LIKE '%-p'
	
	UNION
	
	SELECT O.[OrderNumber]
		,O.[OrderDate] AS [Date]
		,U.[FullName] AS [User]
		,U.[Email]
		,P.Title AS Processor
		,I.Title AS 'FinancialInstitution'
		,U.UserName
		,s.SegmentationName AS AudienceSegments
		,OI.ItemName AS 'ItemName'
		,'Orderable' AS 'ItemType'
		,OA.SKU
		,CASE 
			WHEN OI.ElectronicDelivery = 1
				THEN 'Electronic Delivery'
			WHEN OA.Vdp = 1
				THEN 'VDP'
			ELSE 'Print On Demand'
			END AS 'Fulfillment'
		,OI.ItemPrice AS 'ItemPrice'
		,OI.ItemQuantity AS 'ItemQuantity'
		,O.OrderStatus AS 'OrderStatus'
		,PR.ContentItemId AS RelatedProgramID
		,PR.Title AS RelatedProgram
		,CASE 
			WHEN OI.ElectronicDelivery = 1
				THEN 1
			ELSE 0
			END AS 'ED'
		,CASE 
			WHEN OA.Vdp <> 1
				AND OI.ElectronicDelivery = 0
				THEN 1
			ELSE 0
			END AS 'POD'
		,0 AS Download
		,COALESCE(OI.EstimatedDistribution, 0) AS EstDistribution
		,COALESCE(OI.HasAdvisorsTag, CONVERT(BIT, 0)) AS AdvisorsTag
		,COALESCE(O.ConsentGiven, CONVERT(BIT, 0)) AS ConsentGiven
	FROM [Order] O
	INNER JOIN [OrderItem] OI ON OI.[OrderID] = O.[OrderID]
	INNER JOIN [OrderableAsset] OA ON OA.[ContentItemId] = OI.[ItemID]
	LEFT JOIN [OrderableAssetRelatedProgram] OARP ON OA.[ContentItemId] = OARP.[OrderableAssetContentItemId]
	LEFT JOIN [Program] PR ON OARP.[ProgramContentItemId] = PR.[ContentItemId]
	INNER JOIN [User] U ON U.[UserName] = O.[UserID]
	INNER JOIN [Issuer] I ON I.[IssuerID] = U.[IssuerId]
	LEFT JOIN IssuerSegmentation iss ON i.IssuerId = iss.IssuerId
	LEFT JOIN Segmentation s ON s.SegmentationId = iss.SegmentationId
	INNER JOIN [IssuerProcessor] IPP ON IPP.[IssuerID] = I.[IssuerId]
	INNER JOIN [Processor] P ON P.[ProcessorID] = IPP.[ProcessorID]
	WHERE O.[OrderDate] >= @Local_StartDate
		AND O.[OrderDate] < @Local_EndDate
		AND P.Title NOT IN ('A Plus')
		AND I.IsMastercardIssuer = 0
		AND I.[IssuerID] <> 'a05'
		AND I.RegionId = 'us'
		AND (
			I.IssuerID = @Local_IssuerId
			OR @Local_IssuerId = 'all'
			)
		AND (
			PR.ContentItemId NOT LIKE '%-p'
			OR PR.ContentItemId IS NULL
			)
		AND O.OrderStatus <> 'Cancelled'

	SET context_info 0x;
END;