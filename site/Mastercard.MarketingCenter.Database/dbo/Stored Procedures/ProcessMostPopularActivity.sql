﻿CREATE PROCEDURE [dbo].[ProcessMostPopularActivity] @StartDate DATETIME
	,@EndDate DATETIME
	,@ContentTypeIds VARCHAR(MAX)
	,@MostPopularProcessId BIGINT
AS
BEGIN
	--
	DECLARE @vContentTypeIds AS TABLE (ContentTypeId VARCHAR(25));

	--
	INSERT INTO @vContentTypeIds
	SELECT [Token]
	FROM Split(@ContentTypeIds, ';');

	--
	INSERT INTO [MostPopularActivity]
	SELECT [ContentTypeID] AS [ContentTypeId]
		,[ContentItemID] AS [ContentItemId]
		,[RegionId] AS [RegionId]
		,MAX([When]) AS [LastVisitDate]
		,COUNT([Id]) AS [VisitCount]
		,@MostPopularProcessId AS [MostPopularProcessId]
	FROM [dbo].[DomoActivityApi] daa
	INNER JOIN [dbo].[ContentItem] ci ON daa.Content_Id = ci.ContentItemID
	WHERE [ContentTypeID] IN (
			SELECT ContentTypeId
			FROM @vContentTypeIds
			)
		AND [When] >= @StartDate
		AND [When] < @EndDate
	GROUP BY [ContentTypeID]
		,[ContentItemID]
		,[RegionId]
END