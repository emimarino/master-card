﻿CREATE PROCEDURE [dbo].[UpdateDomoActivityApiContext] @StartDate DATETIME
	,@ResetContext BIT = 0
AS
BEGIN
	SET NOCOUNT ON;

	IF (@ResetContext = 0)
	BEGIN
		/**************************************************************************/
		/*       Choosing what table to query:                                    */
		/*             [dbo].[DomoActivityApi] or [Archive].[DomoActivityApi]     */
		/*       0x2128506 - a "magic value" from vDomoActivityApiByContext       */
		/**************************************************************************/
		DECLARE @Local_StartDate DATETIME = @StartDate;
		DECLARE @minDT DATETIME;

		SELECT @minDT = min(daa.[When])
		FROM dbo.DomoActivityApi AS daa;

		IF (@Local_StartDate >= @minDT)
		BEGIN
			-- query [dbo].[DomoActivityApi]
			SET context_info 0x2128506;
		END
		ELSE
		BEGIN
			-- query [Archive].[DomoActivityApi]
			SET context_info 0x;
		END;
	END;
	ELSE
	BEGIN
		SET context_info 0x;
	END;
END;