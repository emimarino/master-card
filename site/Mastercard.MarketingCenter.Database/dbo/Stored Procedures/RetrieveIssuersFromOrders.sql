﻿CREATE PROCEDURE [dbo].[RetrieveIssuersFromOrders]
(
	@FilterText varchar(255),
	@ProcessorID varchar(25)
)
as
	if @ProcessorID <> ''
		SELECT	distinct(i.Title) IssuerName
		from    [Issuer] i,
				[IssuerProcessor] ip,
				[User] u,
				[Order] o
		where	i.IssuerID = ip.IssuerID and
				ip.IssuerID = u.IssuerId and 
				o.UserID = u.UserName and 
				ip.ProcessorID = @ProcessorID and
				i.Title like @FilterText + '%' and
				u.IsDisabled = 0
		order by i.Title
	else
		SELECT	distinct(i.Title) IssuerName
		from    [Issuer] i,
				[User] u,
				[Order] o
		where	i.IssuerID = u.IssuerId and 
				o.UserID = u.UserName and 
				i.Title like @FilterText + '%' and
				u.IsDisabled = 0
		order by i.Title
