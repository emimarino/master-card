﻿
CREATE PROCEDURE  [dbo].[RetrieveShippingInformation]
(
@ShoppingCartID int = null,
@OrderID int = null
)
as
if (@OrderID is null and @ShoppingCartID is not null)
begin
	SELECT	SI.ShippingInformationID,
			SI.ContactName,
			SI.Phone, 
			SI.[Address],
			SI.OrderTrackingNumber
	FROM    [ShippingInformation] SI
	where	SI.ShoppingCartID = @ShoppingCartID
	order by SI.ShippingInformationID
end
else if (@ShoppingCartID is null and  @OrderID is not null)
begin
	SELECT	distinct(SI.ShippingInformationID),
			SI.ContactName,
			SI.Phone, 
			SI.[Address],
			SI.OrderTrackingNumber
	FROM    [ShippingInformation] SI,
			[OrderItem] oi
	where	SI.ShippingInformationID = oi.ShippingInformationID and
			oi.OrderID = @OrderID
	order by SI.ShippingInformationID
end
else
begin
	SELECT	distinct(SI.ShippingInformationID),
			SI.ContactName,
			SI.Phone, 
			SI.[Address],
			SI.OrderTrackingNumber
	FROM    [ShippingInformation] SI,
			[OrderItem] oi
	where	SI.ShippingInformationID = oi.ShippingInformationID and
			oi.OrderID = @OrderID and 
			SI.ShoppingCartID = @ShoppingCartID
	order by SI.ShippingInformationID
end
