﻿CREATE PROCEDURE [dbo].[RetrieveFilterOrders]
(
    @OrderNumber as varchar(255),
    @Name as nvarchar(255),
    @Email as nvarchar(255),
    @Issuer as nvarchar(255),
    @StartDate as Datetime,
    @EndDate as Datetime,
    @OrderStatus as nvarchar(255),
    @ProcessorID as varchar(25)
)
as
begin

    SELECT	u.FullName,
            p.Title ProcessorName, 
            u.Email,
            o.OrderNumber, 
            o.OrderDate, 
            o.OrderStatus, 
            SUM(oi.ItemPrice) + isnull(o.PriceAdjustment,0) + isnull(o.ShippingCost,0) + isnull(o.PostageCost,0) AS OrderPrice,
            SUM(oi.ItemQuantity) AS OrderQuantity, 
            i.Title IssuerName, 
            u.UserName,
            oi.OrderID
    FROM	[Order] o,
            [User] u,
            [Issuer] i,
            [IssuerProcessor] ip, 
            [Processor] p,
            [OrderItem] oi
    where	o.UserID = u.UserName and 
            i.IssuerID = u.IssuerId and 
            ip.IssuerID = u.IssuerId and 
            ip.ProcessorID = p.ProcessorID and
            o.OrderID = oi.OrderID and
            p.ProcessorID = coalesce(@ProcessorID, p.ProcessorID) and
            o.OrderStatus = coalesce(@OrderStatus, o.OrderStatus) and
            u.Email = coalesce(@Email, u.Email) and
            o.OrderNumber = coalesce(@OrderNumber, o.OrderNumber) and
            u.FullName like coalesce(@Name, u.FullName) + '%' and
            i.Title like coalesce(@Issuer, i.Title) + '%' and
            o.OrderDate between isnull(@StartDate, cast('1/1/2000' as datetime)) and
                dateadd(day, 1, isnull(@EndDate, cast('1/1/2100' as datetime))) and
            u.IsDisabled = 0
    GROUP BY	u.FullName, p.Title, u.Email, o.OrderNumber,
                o.OrderDate, o.OrderStatus, i.Title, u.UserName,
                oi.OrderID,o.PriceAdjustment,o.ShippingCost,o.PostageCost
    order by o.OrderDate desc
end