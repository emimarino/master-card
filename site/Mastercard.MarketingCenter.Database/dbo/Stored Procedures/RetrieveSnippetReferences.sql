﻿CREATE PROCEDURE [dbo].[RetrieveSnippetReferences]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SearchTerm NVARCHAR(255) = '%[[][[]%]]%'
		,@iteration TINYINT = 1;

	IF object_id('tempdb..#t', 'U') IS NOT NULL
		DROP TABLE #t;

	IF object_id('tempdb..#iter', 'U') IS NOT NULL
		DROP TABLE #iter;

	CREATE TABLE #t (
		id INT identity(1, 1) NOT NULL
		,ContentItemId VARCHAR(25) NOT NULL
		,text1 NTEXT NULL
		,text2 NTEXT NULL
		,text3 NTEXT NULL
		);

	CREATE TABLE #iter (
		id INT identity(1, 1) NOT NULL
		,ContentItemId VARCHAR(25) NOT NULL
		,iteration TINYINT NOT NULL
		,string1 NVARCHAR(4000) NULL
		,startPos1 INT
		,string2 NVARCHAR(4000) NULL
		,startPos2 INT
		,string3 NVARCHAR(4000) NULL
		,startPos3 INT
		);

	/***********************************************************/
	/*             Getting the data to process                 */
	/***********************************************************/
	INSERT INTO #t (
		ContentItemId
		,text1
		,text2
		,text3
		)
	SELECT a.ContentItemId
		,a.ShortDescription AS text1
		,a.LongDescription AS text2
		,a.CallToAction AS text3
	FROM dbo.Program AS a
	WHERE a.ShortDescription LIKE @SearchTerm
		OR a.LongDescription LIKE @SearchTerm
		OR a.CallToAction LIKE @SearchTerm
	
	UNION
	
	SELECT a.ContentItemId
		,a.ShortDescription AS text1
		,a.IntroCopy AS text2
		,a.AsideContent AS text3
	FROM dbo.[Page] AS a
	WHERE a.ShortDescription LIKE @SearchTerm
		OR a.IntroCopy LIKE @SearchTerm
		OR a.AsideContent LIKE @SearchTerm
	
	UNION
	
	SELECT a.ContentItemId
		,a.ShortDescription AS text1
		,a.LongDescription AS text2
		,a.CallToAction AS text3
	FROM dbo.Asset AS a
	WHERE a.ShortDescription LIKE @SearchTerm
		OR a.LongDescription LIKE @SearchTerm
		OR a.CallToAction LIKE @SearchTerm
	
	UNION
	
	SELECT a.ContentItemId
		,a.ShortDescription AS text1
		,a.LongDescription AS text2
		,NULL AS text3
	FROM dbo.AssetFullWidth AS a
	WHERE a.ShortDescription LIKE @SearchTerm
		OR a.LongDescription LIKE @SearchTerm
	
	UNION
	
	SELECT a.ContentItemId
		,a.ShortDescription AS text1
		,a.LongDescription AS text2
		,a.CallToAction AS text3
	FROM dbo.DownloadableAsset AS a
	WHERE a.ShortDescription LIKE @SearchTerm
		OR a.LongDescription LIKE @SearchTerm
		OR a.CallToAction LIKE @SearchTerm
	
	UNION
	
	SELECT a.ContentItemId
		,a.ShortDescription AS text1
		,a.LongDescription AS text2
		,a.CallToAction AS text3
	FROM dbo.OrderableAsset AS a
	WHERE a.ShortDescription LIKE @SearchTerm
		OR a.LongDescription LIKE @SearchTerm
		OR a.CallToAction LIKE @SearchTerm
	
	UNION
	
	SELECT a.ContentItemId
		,a.ShortDescription AS text1
		,a.LongDescription AS text2
		,NULL AS text3
	FROM dbo.Webinar AS a
	WHERE a.ShortDescription LIKE @SearchTerm
		OR a.LongDescription LIKE @SearchTerm
	
	UNION
	
	SELECT a.ContentItemId
		,NULL AS text1
		,a.LongDescription AS text2
		,NULL AS text3
	FROM dbo.MarqueeSlide AS a
	WHERE a.LongDescription LIKE @SearchTerm
	
	UNION
	
	SELECT a.ContentItemId
		,a.Html AS text1
		,NULL AS text2
		,NULL AS text3
	FROM dbo.Snippet AS a
	WHERE a.Html LIKE @SearchTerm

	---------------
	/***********************************************************/
	/*                  Processing the data                    */
	/* use CHARINDEX()                                         */
	/* because it works faster than LIKE on ntext              */
	/***********************************************************/
	SET @iteration = 0;;

	WITH cte_step
	AS (
		SELECT t.ContentItemId
			,@iteration AS iteration
			,CASE 
				WHEN (charindex('[[', t.text1, 0) > 0)
					AND (charindex(']]', t.text1, 0)) > 0
					THEN substring(t.text1, charindex('[[', t.text1, charindex('[[', t.text1, 0)), charindex(']]', t.text1, charindex('[[', t.text1, 0)) - charindex('[[', t.text1, charindex('[[', t.text1, 0)) + 2)
				ELSE NULL
				END AS string1
			,CASE charindex('[[', t.text1, 0)
				WHEN 0
					THEN DataLength(t.text1) / 2
				ELSE charindex(']]', t.text1, charindex('[[', t.text1, 0))
				END AS startPos1
			,
			--t.text1,
			CASE 
				WHEN (charindex('[[', t.text2, 0) > 0)
					AND (charindex(']]', t.text2, 0)) > 0
					THEN substring(t.text2, charindex('[[', t.text2, charindex('[[', t.text2, 0)), charindex(']]', t.text2, charindex('[[', t.text2, 0)) - charindex('[[', t.text2, charindex('[[', t.text2, 0)) + 2)
				ELSE NULL
				END AS string2
			,CASE charindex('[[', t.text2, 0)
				WHEN 0
					THEN DataLength(t.text2) / 2
				ELSE charindex('[[', t.text2, 0) + 2
				END AS startPos2
			,
			--t.text2,
			CASE 
				WHEN (charindex('[[', t.text3, 0) > 0)
					AND (charindex(']]', t.text3, 0)) > 0
					THEN substring(t.text3, charindex('[[', t.text3, charindex('[[', t.text3, 0)), charindex(']]', t.text3, charindex('[[', t.text3, 0)) - charindex('[[', t.text3, charindex('[[', t.text3, 0)) + 2)
				ELSE NULL
				END AS string3
			,CASE charindex('[[', t.text3, 0)
				WHEN 0
					THEN DataLength(t.text3) / 2
				ELSE charindex(']]', t.text3, charindex('[[', t.text3, 0))
				END AS startPos3
		--, t.text3
		FROM #t AS t
		)
	INSERT INTO #iter (
		ContentItemId
		,iteration
		,string1
		,startPos1
		,string2
		,startPos2
		,string3
		,startPos3
		)
	SELECT ContentItemId
		,iteration
		,string1
		,startPos1
		,string2
		,startPos2
		,string3
		,startPos3
	FROM cte_step
	WHERE coalesce(string1, string2, string3, N'-+no__data+-') <> N'-+no__data+-';

	WHILE (
			@@rowcount > 0
			AND @iteration < 100
			)
	BEGIN
		SET @iteration += 1;;

		WITH cte_step
		AS (
			SELECT t.ContentItemId
				,@iteration AS iteration
				,CASE 
					WHEN (charindex('[[', t.text1, i.startPos1) > 0)
						AND (charindex(']]', t.text1, i.startPos1) > 0)
						THEN substring(t.text1, charindex('[[', t.text1, charindex('[[', t.text1, i.startPos1)), charindex(']]', t.text1, charindex('[[', t.text1, i.startPos1)) - charindex('[[', t.text1, charindex('[[', t.text1, i.startPos1)) + 2)
					ELSE NULL
					END AS string1
				,CASE charindex('[[', t.text1, i.startPos1)
					WHEN 0
						THEN DataLength(t.text1) / 2
					ELSE charindex(']]', t.text1, charindex('[[', t.text1, i.startPos1))
					END AS startPos1
				,
				--t.text1,
				CASE 
					WHEN (charindex('[[', t.text2, i.startPos2) > 0)
						AND (charindex(']]', t.text2, i.startPos2) > 0)
						THEN substring(t.text2, charindex('[[', t.text2, charindex('[[', t.text2, i.startPos2)), charindex(']]', t.text2, charindex('[[', t.text2, i.startPos2)) - charindex('[[', t.text2, charindex('[[', t.text2, i.startPos2)) + 2)
					ELSE NULL
					END AS string2
				,CASE charindex('[[', t.text2, i.startPos2)
					WHEN 0
						THEN DataLength(t.text2) / 2
					ELSE charindex('[[', t.text2, i.startPos2) + 2
					END AS startPos2
				,
				--t.text2,
				CASE 
					WHEN (charindex('[[', t.text3, i.startPos3) > 0)
						AND (charindex(']]', t.text3, i.startPos3) > 0)
						THEN substring(t.text3, charindex('[[', t.text3, charindex('[[', t.text3, i.startPos3)), charindex(']]', t.text3, charindex('[[', t.text3, i.startPos3)) - charindex('[[', t.text3, charindex('[[', t.text3, i.startPos3) + 2))
					ELSE NULL
					END AS string3
				,CASE charindex('[[', t.text3, i.startPos3)
					WHEN 0
						THEN DataLength(t.text3) / 2
					ELSE charindex(']]', t.text3, charindex('[[', t.text3, i.startPos3))
					END AS startPos3
			--, t.text3
			FROM #t AS t
			INNER JOIN #iter AS i ON t.ContentItemId = i.ContentItemId
			WHERE i.iteration = @iteration - 1
			)
		INSERT INTO #iter (
			ContentItemId
			,iteration
			,string1
			,startPos1
			,string2
			,startPos2
			,string3
			,startPos3
			)
		SELECT ContentItemId
			,iteration
			,string1
			,startPos1
			,string2
			,startPos2
			,string3
			,startPos3
		FROM cte_step
		WHERE coalesce(string1, string2, string3, N'-+no__data+-') <> N'-+no__data+-';
	END;

	SELECT DISTINCT iter.ContentItemID AS ReferenceId
		,ContentItem.StatusID
		,ContentItem.ContentTypeID
		,ContentItem.FrontEndUrl
		,ContentItem.regionId
		,s.ContentItemId SnippetId
		,s.Title SnippetTitle
		,(
			SELECT FrontEndUrl
			FROM ContentItem
			WHERE ContentItemId = s.ContentItemId
			) AS SnippetFrontEndUrl
	FROM #iter AS iter
	INNER JOIN ContentItem ON ContentItem.ContentItemID = iter.ContentItemID
	INNER JOIN Snippet AS s ON iter.string1 LIKE '%[[][[]' + s.Title + ']]%'
		OR iter.string2 LIKE '%[[][[]' + s.Title + ']]%'
		OR iter.string3 LIKE '%[[][[]' + s.Title + ']]%'
	WHERE ContentItem.RegionId != 'demo';

	/**************************************************************/
	/*                         Cleanup                            */
	/**************************************************************/
	IF object_id('tempdb..#t', 'U') IS NOT NULL
		DROP TABLE #t;

	IF object_id('tempdb..#iter', 'U') IS NOT NULL
		DROP TABLE #iter;
END