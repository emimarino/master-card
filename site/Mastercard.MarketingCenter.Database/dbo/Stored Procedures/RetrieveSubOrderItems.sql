﻿
CREATE PROCEDURE [dbo].[RetrieveSubOrderItems]
(
	@SubOrderID int
)
as
	SELECT	oi.OrderItemID,
			oi.ItemName, 
            oi.SKU,
            oi.ItemQuantity,
            oi.ItemPrice
	from	[OrderItem] oi
	where	oi.SubOrderID = @SubOrderID
	order by oi.SKU
