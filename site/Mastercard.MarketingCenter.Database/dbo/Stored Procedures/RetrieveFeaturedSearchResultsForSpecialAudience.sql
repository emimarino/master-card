﻿CREATE PROCEDURE [dbo].[RetrieveFeaturedSearchResultsForSpecialAudience]
(
	@SearchQuery			NVARCHAR(200),
	@CurrentSegmentation	VARCHAR(MAX),
	@CurrentSpecialAudience	VARCHAR(255),
	@RegionId				NCHAR(6)
)
AS
--
DECLARE @vSegmentationIds AS TABLE (SegmentationId VARCHAR(25));
--
INSERT INTO @vSegmentationIds SELECT Token FROM Split(@CurrentSegmentation, ',');
--
WITH	SearchTermForSegmentations AS (
			SELECT DISTINCT	st.SearchTermID
			FROM			SearchTerm st WITH(NOLOCK)
			LEFT JOIN		SearchTermSegmentation sts WITH(NOLOCK) ON st.SearchTermID = sts.SearchTermID
			WHERE			sts.SearchTermID IS NULL OR sts.SegmentationId IN (SELECT SegmentationId FROM @vSegmentationIds)
		)
SELECT	[Title],
		[FeaturedSearchResultHtml],
		[Copy],
		[Url],
		[Image],
		[RegionId],
		[SearchLanguage]
FROM	SearchTerm st
		INNER JOIN SearchTermForSegmentations stfs WITH (NOLOCK) ON st.SearchTermID = stfs.SearchTermId
WHERE	(Title = @SearchQuery 
		OR REPLACE(Title, ' ', '-') = @SearchQuery 
		OR @SearchQuery IN (SELECT Token FROM dbo.Split(st.[Synonyms], '
		'))
		OR @SearchQuery IN (SELECT Token FROM dbo.Split(st.[Synonyms], ','))
		OR @SearchQuery = REPLACE(REPLACE(REPLACE(st.[Synonyms], ' ', '-'), CHAR(13), '-'), CHAR(10), '')
		OR REPLACE(REPLACE(REPLACE(@SearchQuery, ' ', '-'), CHAR(13), '-'), CHAR(10), '') LIKE CONCAT('%(', REPLACE(REPLACE(REPLACE(st.[Synonyms], ' ', '-'), CHAR(13), '-'), CHAR(10), ''), ')%'))
		AND (st.RestrictToSpecialAudience IS NULL OR
			st.RestrictToSpecialAudience = '' OR
			(st.RestrictToSpecialAudience LIKE '%' + @CurrentSpecialAudience + '%') OR
			(@CurrentSpecialAudience LIKE '%' + ST.RestrictToSpecialAudience + '%'))
		AND (st.ExpirationDate IS NULL OR st.ExpirationDate >= GETDATE())
		AND [RegionId] = @RegionId