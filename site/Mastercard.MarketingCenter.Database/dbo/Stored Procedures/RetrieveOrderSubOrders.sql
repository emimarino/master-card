﻿
CREATE PROCEDURE [dbo].[RetrieveOrderSubOrders]
(
	@OrderID int,
	@ShippingInformationID int
)
as
	select	so.SubOrderID,
			so.PriceAdjustment,
			so.ShippingCost,
			so.PostageCost,
			so.[Description],
			so.TrackingNumber,
			so.Completed
	from	SubOrder so
	where	so.OrderID = @OrderID and
			so.ShippingInformationID = @ShippingInformationID
	order by so.SubOrderID
