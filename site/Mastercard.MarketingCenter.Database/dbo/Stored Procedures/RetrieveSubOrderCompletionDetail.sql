﻿
CREATE PROCEDURE [dbo].[RetrieveSubOrderCompletionDetail]
(
	@SubOrderID int
)
as
begin
	SELECT	(select count(*)
			 from	[Order] o2,
					SubOrder so2
			 where	o2.OrderID = so2.OrderID and
					o.OrderID = o2.OrderID and
					so2.SubOrderID != @SubOrderID) SiblingSubOrders,
			(select count(*)
			 from	[Order] o3,
					SubOrder so3
			 where	o3.OrderID = so3.OrderID and
					o.OrderID = o3.OrderID and
					so3.SubOrderID != @SubOrderID and
					so3.Completed = 1) CompletedSiblingSubOrders
	from	[Order] o,
			SubOrder so
	where	o.OrderID = so.OrderID and
			so.SubOrderID = @SubOrderID
end
