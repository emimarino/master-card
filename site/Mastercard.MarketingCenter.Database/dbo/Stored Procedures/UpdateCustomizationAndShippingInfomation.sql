﻿
CREATE PROCEDURE [dbo].[UpdateCustomizationAndShippingInfomation]
(
	@ShoppingCartID int,
	@OrderID int
)
as
update CustomizationDetail
set OrderID=@OrderID
where ShoppingCartID=@ShoppingCartID

update [ShoppingCart]
set DatedCheckedOut=getdate(),OrderID=@OrderID
where  CartID=@ShoppingCartID
