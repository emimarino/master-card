﻿
CREATE PROCEDURE [dbo].[RetrieveElectronicDeliveryOrderItemsByOrderId]
(
	@OrderId int
)
as
	SELECT	oi.ItemName, 
            oi.SKU,
			oi.ItemQuantity,
			oi.ItemPrice,
			ed.ElectronicDeliveryID,
			ed.DownloadCount,
			ed.ExpirationDate,
			case 
				when oi.ApproverEmailAddress is not null and oi.ApproverEmailAddress != '' then cast(1 as bit) else cast(0 as bit)
			end NeedsApproval
	FROM	[OrderItem] oi
			join [ElectronicDelivery] ed
				on ed.OrderItemID = oi.OrderItemID
	where	oi.OrderID = @OrderId and
			oi.ElectronicDelivery = 1
