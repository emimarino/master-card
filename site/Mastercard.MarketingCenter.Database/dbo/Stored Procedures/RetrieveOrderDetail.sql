﻿
CREATE PROCEDURE [dbo].[RetrieveOrderDetail]
(
@OrderID varchar(255)
)
as
select OI.OrderID
	  ,OI.OrderNumber
	  ,OI.UserID
	  ,OI.OrderDate
	  ,OI.OrderStatus,
	   OI.PriceAdjustment,
	   OI.ShippingCost,
	   OI.PostageCost,
	   OI.Description,
	   OI.SpecialInstructions,
	   OI.RushOrder
 from [Order] OI 
where OI.OrderID=@OrderID
