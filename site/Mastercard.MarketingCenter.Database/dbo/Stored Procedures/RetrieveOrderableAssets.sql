﻿CREATE PROCEDURE [dbo].[RetrieveOrderableAssets] 
(
@GlobalID varchar(255)
)
as
	select	oa.Title,
			--ci.Tags,
			oa.ShortDescription,
			oa.LongDescription,
			ci.ModifiedDate,
			oa.Sku,
			oa.ThumbnailImage,
			--oa.[Unit size] Unitsize,
			--oa.[PDF preview] PDFpreview,
			oa.Vdp,
			ps.Title PricingSchedule,
			ps.PricingTable as PricingTable,
			ps.Details as Details,
			isnull(ps.MinimumPrice, 0) MinimumPrice,
			isnull(oa.Customizable, 0) Customizable,
			oa.ElectronicDeliveryFile,
			ps.DownloadPrice
	from	OrderableAsset oa 
			inner join PricingSchedule ps on ps.PricingScheduleID = oa.PricingScheduleID
			inner join ContentItem ci on oa.ContentItemId = ci.ContentItemID
	where	oa.ContentItemId = @GlobalID