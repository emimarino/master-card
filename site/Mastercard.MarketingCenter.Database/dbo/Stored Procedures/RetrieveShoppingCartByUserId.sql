﻿CREATE PROCEDURE [dbo].[RetrieveShoppingCartByUserId]
(
	@UserId as nvarchar(255)
)
as
	SELECT	oa.Title, 
            oa.ShortDescription, 
            oa.ThumbnailImage, 
            oa.Sku,
            sc.UserID, 
            sc.CartID, 
            sc.DateCreated, 
            sc.LastUpdated, 
            sc.DatedCheckedOut,
            oa.ContentItemId, 
            case ci.ElectronicDelivery when 0 then oa.Vdp else cast(0 as bit) end VDP, 
            oa.Customizable Customizable,
            sum(ci.Quantity) Quantity,
			oa.ElectronicDeliveryFile,
			ci.ElectronicDelivery,
			IsNull(substring
			(
				(select	(',' + tg.TagID)
				from	[ContentItemTag] cit
						join Tag tg
							on tg.TagID = cit.TagID
				where	cit.ContentItemID = oa.ContentItemId
				for xml path (''))
			, 2, 10000), '') Tags,
			p.PromotionCode,
			oa.PDFPreview
	FROM	[CartItem] ci join
			[ContentItem] coi on
				ci.ContentItemID = coi.ContentItemID join 
			[OrderableAsset] oa on 
				coi.ContentItemID = oa.ContentItemId	join 
			[ShoppingCart] sc on 
				ci.CartID = sc.CartID left join 
			PromoCode p on 
				p.PromoCodeID = sc.PromotionID
	where	sc.UserID = @UserId and 
			sc.DatedCheckedOut is null
	group by oa.Title, 
            oa.ShortDescription, 
            oa.ThumbnailImage,
            oa.Sku,
            sc.UserID, 
            sc.CartID, 
            sc.DateCreated, 
            sc.LastUpdated, 
            sc.DatedCheckedOut,
            oa.ContentItemId, 
            oa.Vdp,
            oa.Customizable,
			oa.ElectronicDeliveryFile,
			ci.ElectronicDelivery,
			p.PromotionCode,
			oa.PDFPreview
     order by oa.ContentItemId