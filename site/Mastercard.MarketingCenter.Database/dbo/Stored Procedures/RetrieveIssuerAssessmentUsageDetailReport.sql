﻿CREATE PROCEDURE [dbo].[RetrieveIssuerAssessmentUsageDetailReport] @StartDate DATETIME
	,@EndDate DATETIME
	,@FilterMasterCardUsers BIT
	,@FilterVendorProcessor BIT
AS
BEGIN
	SET NOCOUNT ON;

	--
	DECLARE @Local_StartDate DATETIME = @StartDate;
	DECLARE @Local_EndDate DATETIME = @EndDate;
	DECLARE @Local_FilterMasterCardUsers BIT = @FilterMasterCardUsers;
	DECLARE @Local_FilterVendorProcessor BIT = @FilterVendorProcessor;
	--
	EXEC [dbo].[UpdateSiteTrackingContext] @Local_StartDate;
	--
	SELECT u.[FullName] AS [User]
		,u.[UserName]
		,i.[IssuerID]
		,i.[Title] AS [Issuer]
		,s.[SegmentationName] AS [AudienceSegments]
		,p.[ProcessorID]
		,p.[Title] AS [Processor]
		,CASE 
			WHEN st.[What] = '/portal/optimization'
				THEN 'Optimization Dashboard'
			WHEN st.[What] LIKE '/portal/report/landing%'
				THEN 'Landing'
			WHEN st.[What] LIKE '/portal/optimization/optimizationresources%'
				THEN 'Optimization Resources'
			WHEN st.[From] LIKE '/portal/asset/%'
				AND st.[What] LIKE '/portal/file/librarydownload%'
				THEN 'Download'
			WHEN st.[What] LIKE '/portal/asset/%'
				THEN 'Asset'
			WHEN st.[What] LIKE '/portal/optimization/optimizationresource/%'
				THEN 'Recommendation'
			WHEN st.[What] LIKE '/portal/assessyourportfolio%'
				AND i.[IssuerID] NOT IN (
					SELECT [IssuerID]
					FROM [IssuerUnmatched]
					WHERE [IssuerID] <> 'a05'
						AND [Date] >= @Local_StartDate
						AND [Date] < @Local_EndDate
					)
				THEN 'Report'
			WHEN st.[What] LIKE '/portal/assessyourportfolio%'
				AND i.[IssuerID] IN (
					SELECT [IssuerID]
					FROM [IssuerUnmatched]
					WHERE [IssuerID] <> 'a05'
						AND [Date] >= @Local_StartDate
						AND [Date] < @Local_EndDate
					)
				THEN 'Report Error'
			WHEN st.[What] LIKE '/portal/report/%'
				AND i.[IssuerID] NOT IN (
					SELECT [IssuerID]
					FROM [IssuerUnmatched]
					WHERE [IssuerID] <> 'a05'
						AND [Date] >= @Local_StartDate
						AND [Date] < @Local_EndDate
					)
				THEN 'Report Admin'
			WHEN st.[What] LIKE '/portal/report/%'
				AND i.[IssuerID] IN (
					SELECT [IssuerID]
					FROM [IssuerUnmatched]
					WHERE [IssuerID] <> 'a05'
						AND [Date] >= @Local_StartDate
						AND [Date] < @Local_EndDate
					)
				THEN 'Report Admin Error'
			ELSE 'Page'
			END AS [ItemType]
		,st.[When] AS [Date]
		,st.[VisitID]
		,st.[What]
		,st.[From]
		,COALESCE(st.[How], 'generic') AS [Source]
	FROM [vSiteTrackingByContext] st
	JOIN [User] u ON u.[UserName] = st.[Who]
	JOIN [Issuer] i ON i.[IssuerID] = u.[IssuerId]
	LEFT JOIN [IssuerSegmentation] iss ON i.[IssuerID] = iss.[IssuerID]
	LEFT JOIN [Segmentation] s ON s.[SegmentationId] = iss.[SegmentationId]
	JOIN [IssuerProcessor] ipp ON ipp.[IssuerID] = i.[IssuerID]
	JOIN [Processor] p ON p.[ProcessorID] = ipp.[ProcessorID]
	WHERE st.[PageGroup] = 'reportpage'
		AND i.[IssuerID] <> 'a05'
		AND i.RegionId = 'us'
		AND st.[When] >= @Local_StartDate
		AND st.[When] < @Local_EndDate
		AND (
			@Local_FilterMasterCardUsers = 0
			OR i.IsMastercardIssuer = 0
			)
		AND (
			@Local_FilterVendorProcessor = 0
			OR p.[ProcessorID] <> '91b'
			)

	SET context_info 0x;
END;