﻿CREATE PROCEDURE [dbo].[RetrieveOrdersByStatus]
(
@OrderStatus  as nvarchar(255)
)
as
begin
if(@OrderStatus='unapproved')
	begin
		select	u.FullName,
				p.Title ProcessorName,
				u.Email,
				o.OrderNumber,
				o.OrderDate,
				o.OrderStatus,
				SUM(oi.ItemPrice) OrderPrice,
				SUM(oi.ItemQuantity) OrderQuantity,
				i.Title IssuerName,
				u.UserName,
				oi.OrderID
		from	[Order] o,
				[OrderItem] oi,
				[Processor] p,
				[Issuer] i,
				[IssuerProcessor] ip,
				[User] u
		where	o.OrderID = oi.OrderID and
				o.UserID = u.UserName and
				u.IssuerId = i.IssuerID and
				i.IssuerID = ip.IssuerID and
				ip.ProcessorID = p.ProcessorID
		group by u.FullName,
				p.Title,
				u.Email,
				o.OrderNumber,
				o.OrderDate,
				o.OrderStatus,
				i.Title,
				u.UserName,
				oi.OrderID
		having	o.OrderStatus is not null and o.OrderStatus in ('Pending Customization','Pending Correction') and o.OrderStatus <> ''
		order by oi.OrderID desc
	end
else
	begin
		select	u.FullName,
				p.Title ProcessorName,
				u.Email,
				o.OrderNumber,
				o.OrderDate,
				o.OrderStatus,
				SUM(oi.ItemPrice) OrderPrice,
				SUM(oi.ItemQuantity) OrderQuantity,
				i.Title IssuerName,
				u.UserName,
				oi.OrderID
		from	[Order] o,
				[OrderItem] oi,
				[Processor] p,
				[Issuer] i,
				[IssuerProcessor] ip,
				[User] u
		where	o.OrderID = oi.OrderID and
				o.UserID = u.UserName and
				u.IssuerId = i.IssuerID and
				i.IssuerID = ip.IssuerID and
				ip.ProcessorID = p.ProcessorID
		group by u.FullName,
				p.Title,
				u.Email,
				o.OrderNumber,
				o.OrderDate,
				o.OrderStatus,
				i.Title,
				u.UserName,
				oi.OrderID
		having	o.OrderStatus = 'Fulfillment'  and o.OrderStatus is not null and o.OrderStatus <> ''
		order by oi.OrderID desc
	end

end
