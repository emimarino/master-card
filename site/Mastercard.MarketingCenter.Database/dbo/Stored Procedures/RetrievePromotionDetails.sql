﻿
CREATE PROCEDURE [dbo].[RetrievePromotionDetails]
(
	@PromotionCode varchar(255)
)
as
select	p.PromoCodeID, p.StartDate, p.EndDate, p.Description, p.DiscountAmount, p.DiscountType,
		p.OrderMinimum, p.SingleUseOnlyPerIssuer, p.TotalFundingLimit, p.TotalFundingCurrentAmount,
		substring
		(
			(
				select (',' + pt.TagID)
				from	PromoCode p
						join PromoCodeTag pt
							on pt.PromoCodeID = p.PromoCodeID
				where	p.PromotionCode = @PromotionCode
				for xml path ('')
			), 2, 10000
		) Tags
from	PromoCode p
		left join PromoCodeTag pt
			on pt.PromoCodeID = p.PromoCodeID
where	p.PromotionCode = @PromotionCode
group by p.PromoCodeID, p.StartDate, p.EndDate, p.Description, p.DiscountAmount,
		p.DiscountType, p.OrderMinimum, p.SingleUseOnlyPerIssuer, p.TotalFundingLimit, p.TotalFundingCurrentAmount

