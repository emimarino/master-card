﻿CREATE PROCEDURE [dbo].[ProcessDomoActivityApi] @batchSize INT = 50000
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @minSiteTrackingID BIGINT
		,@retentionMonths TINYINT = 3 -- used to prevent a missed record from the last x months
		,@dt DATETIME = sysdatetime()
		,@rowsInserted BIGINT = - 1;
	DECLARE @insertedIDs TABLE (id BIGINT NOT NULL);

	IF object_id('tempdb..#Content', 'U') IS NOT NULL
		DROP TABLE #Content;

	IF object_id('tempdb..#temp', 'U') IS NOT NULL
		DROP TABLE #temp;

	IF object_id('tempdb..#ids', 'U') IS NOT NULL
		DROP TABLE #ids;

	CREATE TABLE #Content (
		id INT identity(1, 1)
		,ContentItemId VARCHAR(25) NOT NULL PRIMARY KEY CLUSTERED
		,Content_Type VARCHAR(25) NOT NULL
		,Content_Id VARCHAR(25) NULL
		,Content_Title NVARCHAR(510)
		);

	CREATE TABLE #temp (
		[SiteTrackingId] BIGINT NULL
		,[VisitId] BIGINT NULL
		,[User_UserId] INT
		,[User_LastName] NVARCHAR(255) NULL
		,[User_FirstName] NVARCHAR(255) NULL
		,[User_Email] NVARCHAR(256) NULL
		,[User_Region] NCHAR(6) NULL
		,[User_Country] NVARCHAR(255) NULL
		,[User_IssuerName] NVARCHAR(255) NULL
		,[User_ProcessorName] NVARCHAR(255) NULL
		,[When] DATETIME NULL
		,[From] NVARCHAR(MAX) NULL
		,[How] VARCHAR(1000) NULL
		,[Content_Type] VARCHAR(25) NULL
		,[Content_Id] VARCHAR(25) NULL
		,[Content_Title] NVARCHAR(255) NULL
		,[Content_RegionName] NCHAR(6) NULL
		,[Hidden_Result] BIT NOT NULL DEFAULT 0
		,[User_IssuerImportedName] NVARCHAR(255) NULL
		,[User_IssuerCID] VARCHAR(50) NULL
		);

	CREATE TABLE #ids (
		[SiteTrackingId] BIGINT NOT NULL PRIMARY KEY
		,[VisitId] BIGINT NULL
		,What NVARCHAR(2000) NOT NULL
		,Who NVARCHAR(2000) NULL
		,[When] DATETIME NOT NULL
		,[From] NVARCHAR(max) NULL
		,[How] NVARCHAR(2000) NULL
		,ContentItemID VARCHAR(25) NULL
		,Region NVARCHAR(200) NOT NULL
		);

	SET @minSiteTrackingID = (
			SELECT min(id)
			FROM (
				SELECT max(SiteTrackingID) AS id
				FROM SiteTracking
				WHERE [When] < cast(dateadd(month, - 1 * @retentionMonths, sysdatetime()) AS DATE)
				
				UNION
				
				SELECT isnull((
							SELECT max(SiteTrackingID) AS id
							FROM [DomoActivityApi]
							), 0)
				) AS a
			);

	INSERT INTO #ids (
		[SiteTrackingId]
		,VisitID
		,What
		,Who
		,[When]
		,[From]
		,How
		,ContentItemID
		,Region
		)
	SELECT DISTINCT TOP (@batchSize) st.[SiteTrackingId]
		,st.VisitID
		,st.What
		,st.Who
		,st.[When]
		,st.[From]
		,st.How
		,st.ContentItemID
		,st.Region AS [Content_RegionName]
	FROM [SiteTracking] AS st WITH (NOLOCK)
	LEFT JOIN [DomoActivityApi] AS daa WITH (NOLOCK) ON st.[SiteTrackingId] = daa.[SiteTrackingId]
	WHERE daa.Id IS NULL
		AND st.[SiteTrackingId] >= @minSiteTrackingID
	ORDER BY st.[SiteTrackingId] DESC;

	---- FOR DEBUG
	--declare @msg nvarchar(200) = (select ltrim(min(SiteTrackingID)) + ' - ' + ltrim(max(SiteTrackingID)) from #ids);
	--raiserror (@msg, 0, 1) with nowait;
	BEGIN TRY
		/*************************************************************************************************/
		/*    asset, assetFullWidth, orderableAsset, downloadableAsset, webinar, link, program, offer    */
		/*************************************************************************************************/
		INSERT INTO #Content (
			Content_Type
			,ContentItemId
			,Content_Id
			,Content_Title
			)
		SELECT DISTINCT 'asset' AS [Content_Type]
			,a.ContentItemId
			,a.ContentItemId AS [Content_Id]
			,a.Title AS [Content_Title]
		FROM Asset AS a WITH (NOLOCK)
		WHERE (
				a.ContentItemId IS NULL
				OR a.ContentItemId NOT LIKE '%-p'
				)
		
		UNION ALL
		
		SELECT DISTINCT 'assetFullWidth' AS [Content_Type]
			,a.ContentItemId
			,a.ContentItemId AS [Content_Id]
			,a.Title AS [Content_Title]
		FROM AssetFullWidth AS a WITH (NOLOCK)
		WHERE (
				a.ContentItemId IS NULL
				OR a.ContentItemId NOT LIKE '%-p'
				)
		
		UNION ALL
		
		SELECT DISTINCT 'orderableAsset' AS [Content_Type]
			,a.ContentItemId
			,a.ContentItemId AS [Content_Id]
			,a.Title AS [Content_Title]
		FROM OrderableAsset AS a WITH (NOLOCK)
		WHERE (
				a.ContentItemId IS NULL
				OR a.ContentItemId NOT LIKE '%-p'
				)
		
		UNION ALL
		
		SELECT DISTINCT 'downloadableAsset' AS [Content_Type]
			,a.ContentItemId
			,a.ContentItemId AS [Content_Id]
			,a.Title AS [Content_Title]
		FROM DownloadableAsset AS a WITH (NOLOCK)
		WHERE (
				a.ContentItemId IS NULL
				OR a.ContentItemId NOT LIKE '%-p'
				)
		
		UNION ALL
		
		SELECT DISTINCT 'webinar' AS [Content_Type]
			,a.ContentItemId
			,a.ContentItemId AS [Content_Id]
			,a.Title AS [Content_Title]
		FROM Webinar AS a WITH (NOLOCK)
		WHERE (
				a.ContentItemId IS NULL
				OR a.ContentItemId NOT LIKE '%-p'
				)
		
		UNION ALL
		
		SELECT DISTINCT 'link' AS [Content_Type]
			,a.ContentItemId
			,a.ContentItemId AS [Content_Id]
			,a.Title AS [Content_Title]
		FROM Link AS a WITH (NOLOCK)
		WHERE (
				a.ContentItemId IS NULL
				OR a.ContentItemId NOT LIKE '%-p'
				)
		
		UNION ALL
		
		SELECT DISTINCT 'program' AS [Content_Type]
			,a.ContentItemId
			,a.ContentItemId AS [Content_Id]
			,a.Title AS [Content_Title]
		FROM Program AS a WITH (NOLOCK)
		WHERE (
				a.ContentItemId IS NULL
				OR a.ContentItemId NOT LIKE '%-p'
				)
		
		UNION ALL
		
		SELECT DISTINCT 'offer' AS [Content_Type]
			,a.ContentItemId
			,a.ContentItemId AS [Content_Id]
			,a.Title AS [Content_Title]
		FROM Offer AS a WITH (NOLOCK)
		WHERE (
				a.ContentItemId IS NULL
				OR a.ContentItemId NOT LIKE '%-p'
				);

		INSERT INTO #temp (
			SiteTrackingId
			,VisitId
			,User_UserId
			,User_LastName
			,User_FirstName
			,User_Email
			,User_Region
			,User_Country
			,User_IssuerName
			,User_ProcessorName
			,[When]
			,[From]
			,How
			,Content_Type
			,Content_Id
			,Content_Title
			,Content_RegionName
			,Hidden_Result
			,User_IssuerImportedName
			,User_IssuerCID
			)
		SELECT DISTINCT st.SiteTrackingID
			,st.VisitID
			,u.UserId AS User_UserId
			,up.LastName AS User_LastName
			,up.FirstName AS User_FirstName
			,u.Email AS User_Email
			,i.RegionId AS User_Region
			,ttc.DisplayName AS User_Country
			,i.Title AS [User_IssuerName]
			,p.Title AS [User_ProcessorName]
			,st.[When] AS [When]
			,st.[From]
			,st.How
			,a.Content_Type AS [Content_Type]
			,a.ContentItemId AS [Content_Id]
			,a.Content_Title AS [Content_Title]
			,st.Region AS [Content_RegionName]
			,(u.ExcludedFromDomoApi | i.ExcludedFromDomoApi) AS ExcludedFromDomoApi
			,ISNULL(LegalName, i.Title) [User_IssuerImportedName]
			,ii.CID [User_IssuerCID]
		FROM #Content AS a
		INNER JOIN #ids AS st WITH (NOLOCK) ON a.ContentItemId = st.ContentItemID
		INNER JOIN ContentItem ci WITH (NOLOCK) ON a.ContentItemId = ci.ContentItemID
			AND ci.RegionId = st.Region
		INNER JOIN [User] u WITH (NOLOCK) ON u.UserName = st.Who
		INNER JOIN Issuer i WITH (NOLOCK) ON u.IssuerId = i.IssuerID
		LEFT JOIN IssuerProcessor ip WITH (NOLOCK) ON i.IssuerID = ip.IssuerID
		LEFT JOIN Processor p WITH (NOLOCK) ON ip.ProcessorID = p.ProcessorID
		LEFT JOIN ImportedIca ii ON ii.CID = i.CID
		LEFT JOIN Tag t WITH (NOLOCK) ON t.Identifier = u.Country
		LEFT JOIN UserProfile up ON up.UserId = u.UserId
		LEFT JOIN TagTranslatedContent ttc ON ttc.TagId = t.TagID
		INNER JOIN #ids ON st.[SiteTrackingID] = #ids.[SiteTrackingID]
		WHERE ttc.LanguageCode = 'en';

		/***********************************************************************************************************************/
		--PAGE
		INSERT INTO #temp (
			SiteTrackingId
			,VisitId
			,User_UserId
			,User_LastName
			,User_FirstName
			,User_Email
			,User_Region
			,User_Country
			,User_IssuerName
			,User_ProcessorName
			,[When]
			,[From]
			,How
			,Content_Type
			,Content_Id
			,Content_Title
			,Content_RegionName
			,Hidden_Result
			,User_IssuerImportedName
			,User_IssuerCID
			)
		SELECT DISTINCT st.SiteTrackingID
			,st.VisitID
			,u.UserId AS User_UserId
			,up.LastName AS User_LastName
			,up.FirstName AS User_FirstName
			,u.Email AS User_Email
			,i.RegionId AS User_Region
			,ttc.DisplayName AS User_Country
			,i.Title AS [User_IssuerName]
			,p.Title AS [User_ProcessorName]
			,st.[When] AS [When]
			,st.[From]
			,st.How
			,'page' AS [Content_Type]
			,a.ContentItemId AS [Content_Id]
			,a.Title AS [Content_Title]
			,st.Region AS [Content_RegionName]
			,(u.ExcludedFromDomoApi | i.ExcludedFromDomoApi) AS ExcludedFromDomoApi
			,ISNULL(LegalName, i.Title) [User_IssuerImportedName]
			,ii.CID [User_IssuerCID]
		FROM [Page] a WITH (NOLOCK)
		INNER JOIN #ids AS st ON LEFT(st.What, CHARINDEX('?', st.What + '?') - 1) = '/portal/' + a.Uri
		INNER JOIN ContentItem ci WITH (NOLOCK) ON a.ContentItemId = ci.ContentItemID
			AND ci.RegionId = st.Region
		INNER JOIN [User] u WITH (NOLOCK) ON u.UserName = st.Who
		INNER JOIN Issuer i WITH (NOLOCK) ON u.IssuerId = i.IssuerID
		LEFT JOIN IssuerProcessor ip WITH (NOLOCK) ON i.IssuerID = ip.IssuerID
		LEFT JOIN Processor p WITH (NOLOCK) ON ip.ProcessorID = p.ProcessorID
		LEFT JOIN ImportedIca ii ON ii.CID = i.CID
		LEFT JOIN Tag t WITH (NOLOCK) ON t.Identifier = u.Country
		LEFT JOIN UserProfile up ON up.UserId = u.UserId
		LEFT JOIN TagTranslatedContent ttc ON ttc.TagId = t.TagID
		INNER JOIN #ids ON st.[SiteTrackingID] = #ids.[SiteTrackingID]
		WHERE (
				a.ContentItemId IS NULL
				OR a.ContentItemId NOT LIKE '%-p'
				)
			AND ttc.LanguageCode = 'en';

		--OFFER LANDING
		INSERT INTO #temp (
			SiteTrackingId
			,VisitId
			,User_UserId
			,User_LastName
			,User_FirstName
			,User_Email
			,User_Region
			,User_Country
			,User_IssuerName
			,User_ProcessorName
			,[When]
			,[From]
			,How
			,Content_Type
			,Content_Id
			,Content_Title
			,Content_RegionName
			,Hidden_Result
			,User_IssuerImportedName
			,User_IssuerCID
			)
		SELECT DISTINCT st.SiteTrackingID
			,st.VisitID
			,u.UserId AS User_UserId
			,up.LastName AS User_LastName
			,up.FirstName AS User_FirstName
			,u.Email AS User_Email
			,i.RegionId AS User_Region
			,ttc.DisplayName AS User_Country
			,i.Title AS [User_IssuerName]
			,p.Title AS [User_ProcessorName]
			,st.[When] AS [When]
			,st.[From]
			,st.How
			,'offer' AS [ContentTypeId]
			,'' AS [Content_Id]
			,'Offer Landing' AS [Content_Title]
			,st.Region AS [Content_RegionName]
			,(u.ExcludedFromDomoApi | i.ExcludedFromDomoApi) AS ExcludedFromDomoApi
			,ISNULL(LegalName, i.Title) [User_IssuerImportedName]
			,ii.CID [User_IssuerCID]
		FROM #ids AS st
		INNER JOIN [User] u WITH (NOLOCK) ON u.UserName = st.Who
		INNER JOIN Issuer i WITH (NOLOCK) ON i.IssuerID = u.IssuerId
		INNER JOIN IssuerProcessor ip WITH (NOLOCK) ON ip.IssuerID = i.IssuerID
		INNER JOIN Processor p WITH (NOLOCK) ON p.ProcessorID = ip.ProcessorID
		LEFT JOIN ImportedIca ii ON ii.CID = i.CID
		LEFT JOIN Tag t WITH (NOLOCK) ON t.Identifier = u.Country
		LEFT JOIN UserProfile up ON up.UserId = u.UserId
		LEFT JOIN TagTranslatedContent ttc ON ttc.TagId = t.TagID
		INNER JOIN #ids ON st.[SiteTrackingID] = #ids.[SiteTrackingID]
		WHERE ttc.LanguageCode = 'en'
			AND st.[What] LIKE '/portal/offers%';

		--Tags
		INSERT INTO #temp (
			SiteTrackingId
			,VisitId
			,User_UserId
			,User_LastName
			,User_FirstName
			,User_Email
			,User_Region
			,User_Country
			,User_IssuerName
			,User_ProcessorName
			,[When]
			,[From]
			,How
			,Content_Type
			,Content_Id
			,Content_Title
			,Content_RegionName
			,Hidden_Result
			,User_IssuerImportedName
			,User_IssuerCID
			)
		SELECT DISTINCT st.SiteTrackingID
			,st.VisitID
			,u.UserId AS User_UserId
			,up.LastName AS User_LastName
			,up.FirstName AS User_FirstName
			,u.Email AS User_Email
			,i.RegionId AS User_Region
			,ttc.DisplayName AS User_Country
			,i.Title AS [User_IssuerName]
			,p.Title AS [User_ProcessorName]
			,st.[When] AS [When]
			,st.[From]
			,st.How
			,'tag' AS [Content_Type]
			,a.TagID AS [Content_Id]
			,COALESCE(a.DisplayName, tt.DisplayName, a.Identifier) AS [Content_Title]
			,st.Region AS [Content_RegionName]
			,(u.ExcludedFromDomoApi | i.ExcludedFromDomoApi) AS ExcludedFromDomoApi
			,ISNULL(LegalName, i.Title) [User_IssuerImportedName]
			,ii.CID [User_IssuerCID]
		FROM #ids AS st
		INNER JOIN [User] u WITH (NOLOCK) ON u.UserName = st.Who
		INNER JOIN Tag a WITH (NOLOCK) ON (
				st.What LIKE N'/portal/tagbrowser/%' + a.Identifier
				OR st.What LIKE N'/portal/tagbrowser%/' + a.Identifier + '/%'
				OR st.What LIKE N'http%//%/portal/tagbrowser/%' + a.Identifier
				OR st.What LIKE N'http%//%/portal/tagbrowser%/' + a.Identifier + '/%'
				)
			AND a.Identifier = u.Country
		INNER JOIN Issuer i WITH (NOLOCK) ON u.IssuerId = i.IssuerID
		LEFT JOIN IssuerProcessor ip WITH (NOLOCK) ON i.IssuerID = ip.IssuerID
		LEFT JOIN Processor p WITH (NOLOCK) ON ip.ProcessorID = p.ProcessorID
		LEFT JOIN TagTranslatedContent tt WITH (NOLOCK) ON a.TagID = tt.TagId
			AND tt.LanguageCode = 'en'
		LEFT JOIN ImportedIca ii ON ii.CID = i.CID
		LEFT JOIN UserProfile up ON up.UserId = u.UserId
		LEFT JOIN TagTranslatedContent ttc ON ttc.TagId = a.TagID
		INNER JOIN #ids ON st.[SiteTrackingID] = #ids.[SiteTrackingID]
		WHERE (
				st.What LIKE N'/portal/tagbrowser%'
				OR st.What LIKE N'http%//%/portal/tagbrowser%' --NEW
				)
			AND (
				st.What NOT LIKE N'/portal/tagbrowser%/[0-9]%'
				AND st.What NOT LIKE N'http%//%/portal/tagbrowser%/[0-9]%' --NEW
				)
			AND ttc.LanguageCode = 'en';

		/*******************************************************/
		/*                   The final cut                     */
		/*******************************************************/
		BEGIN TRANSACTION

		INSERT INTO [DomoActivityApi] (
			SiteTrackingId
			,VisitId
			,User_UserId
			,User_LastName
			,User_FirstName
			,User_Email
			,User_Region
			,User_Country
			,User_IssuerName
			,User_ProcessorName
			,[When]
			,[From]
			,How
			,Content_Type
			,Content_Id
			,Content_Title
			,Content_RegionName
			,Hidden_Result
			,User_IssuerImportedName
			,User_IssuerCID
			)
		OUTPUT inserted.id
		INTO @insertedIDs(id)
		SELECT SiteTrackingId
			,VisitId
			,User_UserId
			,User_LastName
			,User_FirstName
			,User_Email
			,User_Region
			,User_Country
			,User_IssuerName
			,User_ProcessorName
			,[When]
			,[From]
			,How
			,Content_Type
			,Content_Id
			,Content_Title
			,Content_RegionName
			,Hidden_Result
			,User_IssuerImportedName
			,User_IssuerCID
		FROM #temp;

		SET @rowsInserted = @@rowcount;

		UPDATE daa
		SET daa.Hidden_Result = (u.ExcludedFromDomoApi | i.ExcludedFromDomoApi)
		FROM [DomoActivityApi] AS daa
		-- INNER JOIN @insertedIDs AS insertedIDs ON insertedIDs.id = daa.Id -- to update only inserted records
		INNER JOIN [User] AS u ON u.UserId = daa.User_UserId
		INNER JOIN [Issuer] AS i ON i.Title = daa.User_IssuerName
		WHERE daa.Hidden_Result != (u.ExcludedFromDomoApi | i.ExcludedFromDomoApi) -- update only when different value
			;

		COMMIT;

		PRINT '-->>-- [dbo].[ProcessDomoActivityApi]: inserted ' + ltrim(@rowsInserted) + ' row(s) into [dbo].[DomoActivityApi] in ' + ltrim(datediff(ms, @dt, sysdatetime()) / 1000.0) + ' sec.';
	END TRY

	BEGIN CATCH
		IF xact_state() > 0
			ROLLBACK;

		PRINT '--!!-- ERROR in [dbo].[ProcessDomoActivityApi]';

		throw;
	END CATCH
END;