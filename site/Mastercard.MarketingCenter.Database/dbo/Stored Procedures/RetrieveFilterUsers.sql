﻿CREATE PROCEDURE [dbo].[RetrieveFilterUsers]
(
	@Name  as nvarchar(255),
	@Email as nvarchar(255),
	@Issuer as  nvarchar(255),
	@ProcessorID as varchar(25),
	@RegionId as nchar(6)
)
as
begin
	SELECT	p.Title ProcessorName, 
			u.Email,
			u.UserName, 
			i.Title IssuerName, 
			u.FullName
	FROM    [User] u inner join [Issuer] i on
				u.IssuerId = i.IssuerID left outer join
			[IssuerProcessor] ip on
				ip.IssuerID = i.IssuerID left outer join
			(select * from [Processor] where ProcessorID = coalesce(@ProcessorID, ProcessorID)) p on
				ip.ProcessorID = p.ProcessorID
	where	u.Email like coalesce(@Email, u.Email) + '%' and
			u.FullName like coalesce(@Name, u.FullName) + '%' and
			i.Title like coalesce(@Issuer, i.Title) + '%' and
			i.RegionId = @RegionId AND 
			u.IsDisabled = 0
end