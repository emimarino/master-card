﻿CREATE PROCEDURE [dbo].[RetrieveOrderDetailRangeReport]
(
@StartDate datetime,
@EndDate datetime
)
as	
	SELECT	o.OrderNumber, 
			o.OrderDate, 
			p.Title processor, 
			i.Title IssuerName,
			u.FullName, 
			sos.Cost + soship.Cost + ISNull(o.PriceAdjustment,0) AS Total,
			isnull(o.PromotionAmount, 0) PromotionAmount,
			isnull(cast(o.BillingID as varchar(50)), i.BillingID) BillingID,
			(select count(*)
			 from	SubOrder so2
			 where  so2.OrderID = o.OrderID and
					so2.DateCompleted >= @StartDate and 
					so2.DateCompleted < dateadd(day, 1, @EndDate)) CompleteSubOrders,
			(select count(*)
			 from	SubOrder so2
			 where  so2.OrderID = o.OrderID) NumberSubOrders,
			(select min(DateCompleted)
			 from	SubOrder so2
			 where  so2.OrderID = o.OrderID and
					o.OrderStatus = 'Shipped') FirstSubOrderDateCompleted,
			(select max(DateCompleted)
			 from	SubOrder so2
			 where  so2.OrderID = o.OrderID and
					o.OrderStatus = 'Shipped') LastSubOrderDateCompleted,
			LEFT(u.FullName + ' ordered ' + 
			dbo.Concat(cast(oi.ItemQuantity as varchar) + ' of the ' + RTRIM(oi.ItemName) + ', ') + 
			' for ' + i.Title, 175) ReportData
	FROM	[Order] o,
			[User] u,
			[IssuerProcessor] ip,
			[Issuer] i,
			[Processor] p,
			(select	so.OrderID,
					SUM(oi.ItemPrice) Cost
			 from	SubOrder so,
					[OrderItem] oi
			 where	so.SubOrderID = oi.SubOrderID and
					so.DateCompleted >= @StartDate and so.DateCompleted < dateadd(day, 1, @EndDate)
			 group by so.OrderID) sos,
			(select so.OrderID,
					SUM(isnull(so.PriceAdjustment,0)) + SUM(isnull(so.ShippingCost,0)) + SUM(isnull(so.PostageCost,0)) Cost
			 from	SubOrder so
			 where	so.DateCompleted >= @StartDate and so.DateCompleted < dateadd(day, 1, @EndDate)
			 group by so.OrderID) soship,
			[OrderItem] oi
	where	o.UserID = u.UserName and
			p.ProcessorID = ip.ProcessorID and
			ip.IssuerID = u.IssuerId and
			i.IssuerID = ip.IssuerID and
			sos.OrderID = o.OrderID	and
			soship.OrderID = o.OrderID and
			o.OrderStatus != 'Cancelled' and
			oi.OrderID = o.OrderID
	GROUP BY o.OrderNumber, o.DateCompleted, o.OrderDate, p.Title, i.Title, o.OrderID, o.BillingID,
			 u.FullName, i.Title, i.BillingID, o.OrderStatus, o.PromotionAmount, o.PriceAdjustment,o.ShippingCost,o.PostageCost, sos.Cost, soship.Cost
	order by p.Title, i.Title, u.FullName, o.OrderDate