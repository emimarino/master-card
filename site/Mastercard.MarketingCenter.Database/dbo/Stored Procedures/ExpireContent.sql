﻿
CREATE PROCEDURE [dbo].[ExpireContent] 
AS
BEGIN
	UPDATE ContentItem
	SET StatusID = 9	
	WHERE (ExpirationDate is not null and ExpirationDate < GETDATE() )
	AND StatusID in(3,4,1)--published or published and draft
END

