﻿CREATE PROCEDURE [dbo].[RetrieveOrderHistory]
(
@UserID AS NVARCHAR(255)
)
AS
--
SELECT
	[Order].OrderID,
	[Order].OrderNumber,
	[Order].UserID,
	[Order].OrderDate,
	[ShoppingCart].CartID,
	[ShoppingCart].LastUpdated,
	[ShoppingCart].DateCreated,
	[ShoppingCart].DatedCheckedOut,
	[Order].OrderStatus,
	dbo.GetOrderItems([Order].OrderID) AS 'Items',
	CASE
		WHEN SUM([OrderItem].ItemPrice) + ISNULL([Order].PriceAdjustment,0) + ISNULL([Order].ShippingCost,0) + ISNULL([Order].PostageCost,0) - ISNULL([Order].PromotionAmount,0) > 0
			THEN SUM([OrderItem].ItemPrice) + ISNULL([Order].PriceAdjustment,0) + ISNULL([Order].ShippingCost,0) + ISNULL([Order].PostageCost,0) - ISNULL([Order].PromotionAmount,0)
		ELSE
			0
	END AS Price
FROM
				[Order]
	INNER JOIN	[ShoppingCart] ON [Order].OrderID = [ShoppingCart].OrderID
	INNER JOIN	[OrderItem] ON [Order].OrderID = [OrderItem].OrderID
GROUP BY
	[Order].OrderID, [Order].OrderNumber, [Order].UserID,
	[Order].OrderDate,[ShoppingCart].CartID, [ShoppingCart].LastUpdated,
	[ShoppingCart].DateCreated, [ShoppingCart].DatedCheckedOut, [Order].OrderStatus,
	[Order].PriceAdjustment, [Order].ShippingCost, [Order].PostageCost, [Order].PromotionAmount
HAVING
	[Order].UserID = @UserID
ORDER BY
	[Order].OrderID DESC;