﻿CREATE PROCEDURE [dbo].[RetrieveClonedAssetsReport]
(
	@RegionId VARCHAR(255)
)
AS
BEGIN
		SELECT a.ContentType, a.Title, a.ContentItemId, a.ClonedBy, a.[Date]
		FROM
			(
					SELECT 'Asset' AS ContentType, originalContentItem.Title, originalContentItem.ContentItemId, clonedItem.[Name] AS ClonedBy, clonedItem.CreatedDate AS [Date]
					FROM	(
								SELECT		a.OriginalContentItemId, r.[Name], min(ci.CreatedDate) AS CreatedDate FROM  Asset a
								INNER JOIN	ContentItem ci ON a.ContentItemId = ci.ContentItemID
								INNER JOIN	Region r ON ci.RegionId = r.Id
								WHERE		a.OriginalContentItemId IS NOT NULL AND a.OriginalContentItemId <> ''
								GROUP BY	a.OriginalContentItemId, r.[Name]
							) clonedItem
					INNER JOIN Asset originalContentItem ON clonedItem.OriginalContentItemId = originalContentItem.ContentItemId
				UNION
					SELECT 'Asset Full Width' AS ContentType, originalContentItem.Title, originalContentItem.ContentItemId, clonedItem.[Name] AS ClonedBy, clonedItem.CreatedDate AS [Date]
					FROM	(
								SELECT		a.OriginalContentItemId, r.[Name], min(ci.CreatedDate) AS CreatedDate FROM  AssetFullWidth a
								INNER JOIN	ContentItem ci ON a.ContentItemId = ci.ContentItemID
								INNER JOIN	Region r ON ci.RegionId = r.Id
								WHERE		a.OriginalContentItemId IS NOT NULL AND a.OriginalContentItemId <> ''
								GROUP BY	a.OriginalContentItemId, r.[Name]
							) clonedItem
					INNER JOIN AssetFullWidth originalContentItem ON clonedItem.OriginalContentItemId = originalContentItem.ContentItemId
				UNION
					SELECT 'Downloadable Asset' AS ContentType, originalContentItem.Title, originalContentItem.ContentItemId, clonedItem.[Name] AS ClonedBy, clonedItem.CreatedDate AS [Date]
					FROM	(
								SELECT		a.OriginalContentItemId, r.[Name], min(ci.CreatedDate) AS CreatedDate
								FROM		DownloadableAsset a
								INNER JOIN	ContentItem ci ON a.ContentItemId = ci.ContentItemID
								INNER JOIN	Region r ON ci.RegionId = r.Id
								WHERE		a.OriginalContentItemId IS NOT NULL AND a.OriginalContentItemId <> ''
								GROUP BY	a.OriginalContentItemId, r.[Name]
							) clonedItem
					INNER JOIN DownloadableAsset originalContentItem ON clonedItem.OriginalContentItemId = originalContentItem.ContentItemId
			) a
		INNER JOIN ContentItem ci ON a.ContentItemId = ci.ContentItemID
		INNER JOIN Region r ON ci.RegionId = r.Id
		WHERE ci.RegionId = @RegionId
		ORDER BY Title
END