﻿
CREATE PROCEDURE [dbo].[RetrievePotentialBrokenContentLinkContent] 
AS
begin
	declare @SearchTerm varchar(4) = 'href';

	WITH CTE_SEARCH
		AS
		(
		SELECT
			PR.Title,
			CI.PrimaryContentItemID as Id,
			CI.CreatedDate,
			CI.ModifiedDate,
			'Program' as ItemType,
			CI.ContentTypeID as ContentTypeId,
			CI.FrontEndUrl,
			CI.RegionId,
			PR.ShortDescription Text1,
			PR.LongDescription Text2,
			PR.CallToAction Text3,
			null as IsFeatureOnSelected
		FROM
			dbo.ContentItem CI
			JOIN dbo.Program PR ON PR.ContentItemId = CI.PrimaryContentItemID
			--LEFT JOIN FREETEXTTABLE(dbo.Program, (ShortDescription, LongDescription, CallToAction), @SearchTerm) PRO ON PRO.[Key] = PR.ContentItemId
			where ci.StatusID = 3

		UNION

		SELECT
			PA.Title,
			CI.PrimaryContentItemID as Id,
			CI.CreatedDate,
			CI.ModifiedDate,
			'Page' as ItemType,
			CI.ContentTypeID as ContentTypeId,
			CI.FrontEndUrl,
			CI.RegionId,
			PA.IntroCopy Text1,
			PA.ShortDescription Text2,
			PA.AsideContent Text3,
			null as IsFeatureOnSelected
		FROM
			dbo.ContentItem CI
			JOIN dbo.Page PA ON PA.ContentItemId = CI.PrimaryContentItemID	
			--LEFT JOIN FREETEXTTABLE(dbo.Page, (IntroCopy, ShortDescription, AsideContent), @SearchTerm) LPO ON LPO.[Key] = PA.ContentItemId
			WHERE ci.StatusID = 3

		UNION
		SELECT
			AT.Title,
			CI.PrimaryContentItemID as Id,
			CI.CreatedDate,
			CI.ModifiedDate,
			'Asset' as ItemType,
			CI.ContentTypeID as ContentTypeId,
			CI.FrontEndUrl,
			CI.RegionId,
			AT.ShortDescription Text1,
			AT.LongDescription Text2,
			AT.CallToAction Text3,
			null as IsFeatureOnSelected
		FROM
			dbo.ContentItem CI
			JOIN dbo.Asset AT ON AT.ContentItemId = CI.PrimaryContentItemID
			--LEFT JOIN FREETEXTTABLE(dbo.Asset, (ShortDescription, LongDescription, CallToAction), @SearchTerm) ATO ON ATO.[Key] = AT.ContentItemId
			WHERE ci.StatusID = 3

		UNION
		SELECT
			AW.Title,
			CI.PrimaryContentItemID as Id,
			CI.CreatedDate,
			CI.ModifiedDate,
			'Asset Full Width' as ItemType,
			CI.ContentTypeID as ContentTypeId,
			CI.FrontEndUrl,
			CI.RegionId,
			AW.ShortDescription Text1,
			AW.LongDescription Text2,
			null Text3,
			null as IsFeatureOnSelected
		FROM
					dbo.ContentItem CI
			JOIN dbo.AssetFullWidth AW ON AW.ContentItemId = CI.PrimaryContentItemID	
			--LEFT JOIN FREETEXTTABLE(dbo.AssetFullWidth, (ShortDescription, LongDescription), @SearchTerm) AWO ON AWO.[Key] = AW.ContentItemId
			WHERE ci.StatusID = 3

		UNION
		SELECT
			DA.Title,
			CI.PrimaryContentItemID as Id,
			CI.CreatedDate,
			CI.ModifiedDate,
			'Downloadable Asset' as ItemType,
			CI.ContentTypeID as ContentTypeId,
			CI.FrontEndUrl,
			CI.RegionId,
			DA.ShortDescription Text1,
			DA.LongDescription Text2,
			DA.CallToAction Text3,
			null as IsFeatureOnSelected
		FROM
					dbo.ContentItem CI
			JOIN dbo.DownloadableAsset DA ON DA.ContentItemId = CI.PrimaryContentItemID
			--LEFT JOIN FREETEXTTABLE(dbo.DownloadableAsset, (ShortDescription, LongDescription, CallToAction), @SearchTerm) DAO ON DAO.[Key] = DA.ContentItemId
			WHERE ci.StatusID = 3

		UNION
		SELECT
			OA.Title,
			CI.PrimaryContentItemID as Id,
			CI.CreatedDate,
			CI.ModifiedDate,
			'Orderable Asset' as ItemType,
			CI.ContentTypeID as ContentTypeId,
			CI.FrontEndUrl,
			CI.RegionId,
			OA.ShortDescription Text1,
			OA.LongDescription Text2,
			OA.CallToAction Text3,
			null as IsFeatureOnSelected
		FROM
					dbo.ContentItem CI
			JOIN dbo.OrderableAsset OA ON OA.ContentItemId = CI.PrimaryContentItemID	
			--LEFT JOIN FREETEXTTABLE(dbo.OrderableAsset, (ShortDescription, LongDescription, CallToAction), @SearchTerm) OAO ON OAO.[Key] = OA.ContentItemId
			WHERE ci.StatusID = 3

		UNION
		SELECT
			WE.Title,
			CI.PrimaryContentItemID as Id,
			CI.CreatedDate,
			CI.ModifiedDate,
			'Webinar' as ItemType,
			CI.ContentTypeID as ContentTypeId,
			CI.FrontEndUrl,
			CI.RegionId,
			WE.ShortDescription Text1,
			WE.LongDescription Text2,
			null Text3,
			null as IsFeatureOnSelected
		FROM dbo.ContentItem CI
			JOIN dbo.Webinar WE ON WE.ContentItemId = CI.PrimaryContentItemID	
			--LEFT JOIN FREETEXTTABLE(dbo.Webinar, (ShortDescription, LongDescription), @SearchTerm) WEO ON WEO.[Key] = WE.ContentItemId
			WHERE ci.StatusID = 3

		UNION
		SELECT
			MS.Title,
			CI.PrimaryContentItemID as Id,
			CI.CreatedDate,
			CI.ModifiedDate,
			'Marquee Slide' as ItemType,
			CI.ContentTypeID as ContentTypeId,
			null as FrontEndUrl,
			CI.RegionId,
			MS.LongDescription Text1,
			MS.MainUrl Text2,
			null Text3,
			(SELECT COUNT(1)
				  FROM dbo.MarqueeSlide as m
				  join ContentItemFeatureLocation f on f.ContentItemID = m.ContentItemId
				  join FeatureLocation fl on fl.FeatureLocationID = f.FeatureLocationID
				  where m.ContentItemId = MS.ContentItemId) as IsFeatureOnSelected
		FROM dbo.ContentItem CI
			JOIN dbo.MarqueeSlide MS ON MS.ContentItemId = CI.PrimaryContentItemID	
			WHERE ci.StatusID = 3  
			and MS.MainUrl is not null and not exists (
			select 1 from ContentItem ici where ici.StatusID = 3 and MS.MainUrl like ('%' + ici.FrontEndUrl) ) and not exists (
			select 1 from VanityUrl vu where MS.MainUrl like ('%' + vu.VanityUrl) )
		


		UNION
		SELECT
			QL.Title,
			CI.PrimaryContentItemID as Id,
			CI.CreatedDate,
			CI.ModifiedDate,
			'Quick Link' as ItemType,
			CI.ContentTypeID as ContentTypeId,
			null as FrontEndUrl,
			CI.RegionId,
			QL.Url Text1,
			null Text2,
			null Text3,
			null as IsFeatureOnSelected
		FROM dbo.ContentItem CI
			JOIN dbo.QuickLink QL ON QL.ContentItemId = CI.PrimaryContentItemID	
			WHERE ci.StatusID = 3 and QL.Url is not null and not exists (
			select 1 from ContentItem ici where ici.StatusID = 3 and QL.Url like ('%' + ici.FrontEndUrl) ) and not exists (
			select 1 from VanityUrl vu where QL.Url like ('%' + vu.VanityUrl) )

		UNION
		SELECT
			S.Title,
			CI.PrimaryContentItemID as Id,
			CI.CreatedDate,
			CI.ModifiedDate,
			'Snippet' as ItemType,
			CI.ContentTypeID as ContentTypeId,
			null as FrontEndUrl,
			CI.RegionId,
			S.Html Text1,
			null Text2,
			null Text3,
			null as IsFeatureOnSelected
		FROM dbo.ContentItem CI
			JOIN dbo.Snippet S ON S.ContentItemId = CI.PrimaryContentItemID	
			--LEFT JOIN FREETEXTTABLE(dbo.Snippet, (Html), @SearchTerm) SO ON SO.[Key] = S.ContentItemId
			WHERE ci.StatusID = 3

		UNION
		SELECT
			ST.Title as Title,
			ST.SearchTermID Id,
			null as CreatedDate,
			null as ModifiedDate,
			'Search Term' as ItemType,
			null as ContentTypeId,
			null as FrontEndUrl,
			ST.RegionId,
			ST.FeaturedSearchResultHtml Text1,
			ST.Url Text2,
			null Text3,
			null as IsFeatureOnSelected
		FROM
			SearchTerm ST
			--LEFT JOIN FREETEXTTABLE(dbo.SearchTerm, (FeaturedSearchResultHtml), @SearchTerm) STO ON STO.[Key] = ST.SearchTermID
			WHERE ST.Url is not null

		UNION
		SELECT
			T.Identifier as Title,
			T.TagID Id,
			T.CreatedDate as CreatedDate,
			T.ModifiedDate as ModifiedDate,
			'Tag' as ItemType,
			null as ContentTypeId,
			null as FrontEndUrl,
			'global' RegionId,
			Description Text1,
			null Text2,
			null Text3,
			null as IsFeatureOnSelected
		FROM
			Tag T
			--LEFT JOIN FREETEXTTABLE(dbo.Tag, (Description), @SearchTerm) TA ON TA.[Key] = T.TagID
		)
		SELECT Distinct Title,
			Id,
			CreatedDate,
			ModifiedDate,
			ItemType,
			ContentTypeId,
			CASE WHEN SUBSTRING(FrontEndUrl, 1, 1) = '/' THEN '/portal' + FrontEndUrl ELSE '/portal/' + FrontEndUrl END AS FrontEndUrl,
			RegionId,
			Text1,
			Text2,
			Text3,
			IsFeatureOnSelected
		FROM CTE_SEARCH
		where RegionId != 'demo'
		and (IsFeatureOnSelected > 0 or IsFeatureOnSelected IS NULL)
end