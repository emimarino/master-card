﻿

CREATE PROCEDURE [dbo].[RetrieveUserInformation]
(
	@UserName nvarchar(1000)
)
as
	select	u.UserID,
			u.UserName,
			u.FullName,
			p.Title processor,
			p.CobrandLogo,
			p.CalendarTabs
			/*,i.Converting*/
	from 	[User] u, 
			Processor p,
			IssuerProcessor ip,
			Issuer i
	where	p.ProcessorID = ip.ProcessorID and
			ip.IssuerID = u.IssuerId and
			i.IssuerID = ip.IssuerID and
			u.UserName = @UserName


