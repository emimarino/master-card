﻿
CREATE PROCEDURE [dbo].[RetrieveOrdersForVDPFTPReminder]

as
	select	o.OrderID,
			o.OrderNumber,
			o.OrderDate
	from	[Order] o
	where	o.OrderID not in (select OrderID from FtpAccount) and
			o.VDPFTPReminderSent = 0 and
			o.OrderID in (select OrderID from [OrderItem] where VDP = 1) and
			o.OrderID not in (select OrderID from [VdpOrderInformation] where Completed = 1)
