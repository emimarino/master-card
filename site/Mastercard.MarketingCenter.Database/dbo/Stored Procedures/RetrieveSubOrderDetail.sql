﻿

CREATE PROCEDURE [dbo].[RetrieveSubOrderDetail]
(
	@SubOrderID int
)
as
begin
	SELECT	so.PriceAdjustment,
			so.PostageCost,
			so.ShippingCost,
			so.[Description],
			so.TrackingNumber,
			si.ContactName,
			si.Phone,
			si.[Address],
			si.ShippingInformationID,
			so.OrderID
	from	SubOrder so,
			[ShippingInformation] si
	where	so.ShippingInformationID = si.ShippingInformationID and
			so.SubOrderID = @SubOrderID
end
