﻿
CREATE PROCEDURE [dbo].[RetrieveOrderStatuses]
(
	@ProcessorID varchar(40)
)
as
begin
	if @ProcessorID <> ''
		SELECT	distinct replace(o.OrderStatus, 'Shipped', 'Shipped/Fulfilled') OrderStatus
		from    [Order] o,
				[IssuerProcessor] ip,
				[User] u
		where	u.IssuerId = ip.IssuerID and
				ip.ProcessorID = @ProcessorID and
				o.UserID = u.UserName
	else
		SELECT	distinct replace(o.OrderStatus, 'Shipped', 'Shipped/Fulfilled') OrderStatus
		from    [Order] o
end
