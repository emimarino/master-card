﻿CREATE PROCEDURE [dbo].[RetrieveContentActivityTagDetailReport] @TagId VARCHAR(255)
	,@StartDate DATETIME
	,@EndDate DATETIME
	,@AudienceSegmentation VARCHAR(MAX)
	,@FilterMasterCardUsers BIT
	,@FilterVendorProcessor BIT
	,@RegionId VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

	--
	DECLARE @Local_TagId VARCHAR(255) = @TagId;
	DECLARE @Local_StartDate DATETIME = @StartDate;
	DECLARE @Local_EndDate DATETIME = @EndDate;
	DECLARE @Local_AudienceSegmentation VARCHAR(MAX) = @AudienceSegmentation;
	DECLARE @Local_FilterMasterCardUsers BIT = @FilterMasterCardUsers;
	DECLARE @Local_FilterVendorProcessor BIT = @FilterVendorProcessor;
	DECLARE @Local_RegionId VARCHAR(255) = @RegionId;

	--
	EXEC [dbo].[UpdateSiteTrackingContext] @Local_StartDate;

	--
	IF object_id('tempdb..#issuerForSegmentations', 'U') IS NOT NULL
		DROP TABLE #issuerForSegmentations;

	CREATE TABLE #issuerForSegmentations (
		IssuerId VARCHAR(25)
		,Title NVARCHAR(255)
		,AudienceSegments NVARCHAR(255)
		,IsMastercardIssuer BIT
		);

	INSERT INTO #issuerForSegmentations (
		IssuerId
		,Title
		,AudienceSegments
		,IsMastercardIssuer
		)
	SELECT DISTINCT i.IssuerID
		,i.Title
		,s.SegmentationName AS AudienceSegments
		,i.IsMastercardIssuer
	FROM Issuer i WITH (NOLOCK)
	LEFT JOIN IssuerSegmentation iss WITH (NOLOCK) ON i.IssuerID = iss.IssuerID
	LEFT JOIN Segmentation s WITH (NOLOCK) ON s.SegmentationId = iss.SegmentationId
	WHERE iss.IssuerID IS NULL
		OR iss.SegmentationId IN (
			SELECT Token
			FROM Split(@Local_AudienceSegmentation, ',')
			)
		OR @Local_RegionId = 'fmm';

	--
	SELECT i.AudienceSegments
		,u.UserName
		,u.FullName 'Name'
		,i.IssuerID
		,i.Title Issuer
		,p.ProcessorID
		,COALESCE(p.Title, 'N/A') Processor
		,st.[When]
		,st.[From]
		,COALESCE(st.How, 'generic') 'Source'
		,t.DisplayName AS Title
	FROM vSiteTrackingByContext st
	INNER JOIN Tag t ON (
			t.TagID = '' + @Local_TagId + ''
			OR @Local_TagId = 'all-tags'
			)
	INNER JOIN [User] u ON LOWER(u.UserName) = LOWER(st.Who)
	INNER JOIN #issuerForSegmentations i ON u.IssuerId = i.IssuerID
	LEFT JOIN IssuerProcessor ipp ON ipp.IssuerID = i.IssuerID
	LEFT JOIN Processor p ON ipp.ProcessorID = p.ProcessorID
	LEFT JOIN Issuer iss ON ipp.IssuerID = iss.IssuerID
	WHERE (
			st.What LIKE N'/portal/tagbrowser/%' + t.Identifier
			OR st.What LIKE N'/portal/tagbrowser%/' + t.Identifier + '/%'
			OR st.What LIKE N'http%//%/portal/tagbrowser/%' + t.Identifier
			OR st.What LIKE N'http%//%/portal/tagbrowser%/' + t.Identifier + '/%'
			)
		AND What NOT LIKE N'/portal/tagbrowser%/[0-9]%'
		AND What NOT LIKE N'http%//%/portal/tagbrowser%/[0-9]%'
		AND i.IssuerID <> 'a05'
		AND [When] >= '' + CONVERT(VARCHAR, @Local_StartDate) + ''
		AND [When] < '' + CONVERT(VARCHAR, @Local_EndDate) + ''
		AND st.Region = '' + @Local_RegionId + ''
		AND (
			(
				p.ProcessorID IS NULL
				AND @Local_RegionId <> 'us'
				)
			OR (
				(
					@Local_FilterMasterCardUsers = 0
					OR i.IsMastercardIssuer = 0
					)
				AND (
					@Local_FilterVendorProcessor = 0
					OR p.ProcessorID <> '91b'
					)
				)
			)
	ORDER BY [When] DESC

	SET context_info 0x;
END;