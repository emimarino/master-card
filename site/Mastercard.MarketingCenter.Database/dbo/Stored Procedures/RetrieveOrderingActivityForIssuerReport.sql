﻿CREATE PROCEDURE [dbo].[RetrieveOrderingActivityForIssuerReport] @StartDate DATETIME
	,@EndDate DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	--
	DECLARE @Local_StartDate DATETIME = @StartDate;
	DECLARE @Local_EndDate DATETIME = @EndDate;
	--
	EXEC [dbo].[UpdateSiteTrackingContext] @Local_StartDate;
	--
	WITH Downloads
	AS (
		SELECT [When]
			,Who
			,ContentItemID
		FROM (
			SELECT *
			FROM vSiteTrackingByContext
			WHERE [From] LIKE '/portal/asset/%'
				AND What LIKE '/portal/file/librarydownload%'
			) tempdownloads
		)
	--
	SELECT IssuerID
		,IssuerName
		,COUNT(DISTINCT (Processor)) Processors
		,COUNT(DISTINCT (RelatedProgram)) RPs
		,COUNT(DISTINCT (UserName)) Users
		,SUM(Download) Downloads
		,SUM(ED) EDOrders
		,SUM(POD) PODOrders
		,SUM(ItemPrice) TotalCostOrdered
	FROM (
		SELECT [When] AS [Date]
			,U.[FullName] AS Requestor
			,P.ProcessorID AS Processor
			,I.IssuerID
			,I.Title AS 'IssuerName'
			,U.UserName
			,DA.title AS 'ItemName'
			,0 AS 'ItemPrice'
			,PR.Title AS RelatedProgram
			,0 AS ED
			,0 AS POD
			,1 AS Download
		FROM Downloads AS d
		INNER JOIN [DownloadableAsset] DA ON DA.[ContentItemId] = d.ContentItemID
		INNER JOIN [DownloadableAssetRelatedProgram] DARP ON DA.[ContentItemId] = DARP.[DownloadableAssetContentItemId]
		INNER JOIN [Program] PR ON DARP.[ProgramContentItemId] = PR.[ContentItemId]
		INNER JOIN [aspnet_Users] AU ON 'mastercardmembers:' + AU.[UserName] = d.[Who]
		INNER JOIN [User] U ON U.[UserName] = 'mastercardmembers:' + AU.[UserName]
		INNER JOIN [Issuer] I ON I.[IssuerID] = U.[IssuerId]
		INNER JOIN [IssuerProcessor] IP ON IP.[IssuerID] = I.[IssuerID]
		INNER JOIN [Processor] P ON P.[ProcessorID] = IP.[ProcessorID]
		WHERE d.[When] >= @Local_StartDate
			AND d.[When] < @Local_EndDate
			AND P.Title NOT IN ('A Plus')
			AND I.IsMastercardIssuer = 0
			AND I.[IssuerID] <> 'a05'
			AND I.RegionId = 'us'
			AND (
				PR.ContentItemId NOT LIKE '%-p'
				OR PR.ContentItemId IS NULL
				)
		
		UNION
		
		SELECT O.[OrderDate] AS [Date]
			,U.FullName AS Requestor
			,P.ProcessorID AS Processor
			,I.IssuerID
			,I.Title AS 'IssuerName'
			,U.UserName
			,OI.ItemName AS 'ItemName'
			,OI.ItemPrice AS 'ItemPrice'
			,PR.Title AS RelatedProgram
			,CASE 
				WHEN OI.ElectronicDelivery = 1
					THEN 1
				ELSE 0
				END AS 'ED'
			,CASE 
				WHEN OA.Vdp <> 1
					AND OI.ElectronicDelivery = 0
					THEN 1
				ELSE 0
				END AS 'POD'
			,0 AS Download
		FROM (
			SELECT *
			FROM [Order]
			WHERE [OrderDate] >= @Local_StartDate
				AND [OrderDate] < @Local_EndDate
			) O
		INNER JOIN [OrderItem] OI ON OI.[OrderID] = O.[OrderID]
		INNER JOIN [OrderableAsset] OA ON OA.[ContentItemId] = OI.[ItemID]
		LEFT JOIN [OrderableAssetRelatedProgram] OARP ON OA.[ContentItemId] = OARP.[OrderableAssetContentItemId]
		LEFT JOIN [Program] PR ON OARP.[ProgramContentItemId] = PR.[ContentItemId]
		INNER JOIN [User] U ON U.[UserName] = O.[UserID]
		INNER JOIN [Issuer] I ON I.[IssuerID] = U.[IssuerId]
		INNER JOIN [IssuerProcessor] IP ON IP.[IssuerID] = I.[IssuerID]
		INNER JOIN [Processor] P ON P.[ProcessorID] = IP.[ProcessorID]
		WHERE O.[OrderDate] >= @Local_StartDate
			AND O.[OrderDate] < @Local_EndDate
			AND P.Title NOT IN ('A Plus')
			AND I.IsMastercardIssuer = 0
			AND I.[IssuerID] <> 'a05'
			AND I.RegionId = 'us'
			AND (
				PR.ContentItemId NOT LIKE '%-p'
				OR PR.ContentItemId IS NULL
				)
			AND O.OrderStatus <> 'Cancelled'
		) A
	GROUP BY IssuerID
		,IssuerName

	SET context_info 0x;
END;