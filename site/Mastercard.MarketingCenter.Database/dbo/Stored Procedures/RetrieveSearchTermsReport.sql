﻿CREATE PROCEDURE [dbo].[RetrieveSearchTermsReport]
	@StartDate datetime,
	@EndDate datetime,
	@Region VARCHAR(255)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


SELECT s.SearchBoxEntry, s.SearchResultsQty
into #temp_dates_searchTerm
FROM SearchActivity s
WHERE s.SearchDateTime IN (SELECT max(s2.SearchDateTime) FROM SearchActivity s2 WHERE s2.SearchBoxEntry=s.SearchBoxEntry AND s2.SearchResultsQty IS NOT NULL)

select lower(sa.SearchBoxEntry) SearchTerm, count(SearchActivityID) Total, max(tt.SearchResultsQty) as SearchResults
from SearchActivity sa

join [User] u on lower(u.UserName) = lower(sa.UserName)

join Issuer i on u.IssuerId = i.IssuerID 

left join   IssuerProcessor ip on i.IssuerID = ip.IssuerID

left join Processor p on ip.ProcessorID = p.ProcessorID

left join #temp_dates_searchTerm tt on tt.SearchBoxEntry = sa.SearchBoxEntry
WHERE 
UrlVisited is null and
sa.SearchDateTime >= @StartDate and sa.SearchDateTime < @EndDate and
sa.Region=@Region and
((i.IsMastercardIssuer = 0 and
p.ProcessorID <> '91b') or p.ProcessorID is null )
group by lower(sa.SearchBoxEntry)
order by Total desc, lower(sa.SearchBoxEntry)

DROP TABLE #temp_dates_searchTerm

END