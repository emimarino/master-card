﻿
CREATE PROCEDURE [dbo].[RetrieveAllOrderItems]
(
@OrderID varchar(255)
)
as
	select	oi.ItemID,
			oi.ItemName,
			oi.ItemPrice,
			oi.ItemQuantity,
			oi.ProofPDF,
			oi.SKU,
			oi.ShippingInformationID
	from	[OrderItem] oi
	where	oi.OrderID = @OrderID
