﻿
CREATE PROCEDURE [dbo].[RetrieveIssuer] 
(
@Domain varchar(255),
@RegionId nchar(6)
)
as
begin
	SELECT	i.Title IssuerName, 
			i.DomainName, 
			i.BillingID, 
			i.IssuerID AS IssuerListItemId, 
            p.Title AS Processor, 
            p.CobrandLogo, 
            p.Modified
	FROM    dbo.IssuerProcessor ip INNER JOIN
            dbo.Processor p ON 
				ip.ProcessorID = p.ProcessorID RIGHT OUTER JOIN
            dbo.Issuer i ON 
				ip.IssuerID = i.IssuerID
	where	i.RegionId = @RegionId and
			@Domain in (select Token from dbo.Split(i.DomainName, ','))
end

