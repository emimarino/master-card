﻿CREATE PROCEDURE [dbo].[RetrieveIssuersByProcessor] 
(
    @ProcessorID varchar(25),
    @RegionId nchar(6)
)
as
	SELECT	i.Title IssuerName, 
			i.DomainName, 
			i.IssuerID IssuerID, 
			p.Title ProcessorName,
			usersCnt.cnt Users,
			ordersGrouped.cnt Orders,
			ordersGrouped.lastOrderUpdate LastOrderDate
	FROM  Issuer i
		inner join [IssuerProcessor] ip on i.IssuerID = ip.IssuerID
		inner join [Processor] p on ip.ProcessorID = p.ProcessorID
		left join (
			select IssuerId, count(*) as cnt
			from	[User]
			group by IssuerId
		) usersCnt on i.IssuerID = usersCnt.IssuerId
		left join 
		(
			select u.IssuerId, count(*) as cnt, max(OrderDate) as lastOrderUpdate
			from [User] u
			inner join [Order] o on u.UserName = o.UserID
			group by u.IssuerId
		) ordersGrouped on i.IssuerID = ordersGrouped.IssuerId
	where
        ip.ProcessorID = @ProcessorID and
        i.RegionId = @RegionId
    order by i.DateCreated