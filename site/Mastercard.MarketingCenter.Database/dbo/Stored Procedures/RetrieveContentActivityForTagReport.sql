﻿CREATE PROCEDURE [dbo].[RetrieveContentActivityForTagReport] @StartDate DATETIME
	,@EndDate DATETIME
	,@AudienceSegmentation VARCHAR(max)
	,@FilterMasterCardUsers BIT
	,@FilterVendorProcessor BIT
	,@RegionId VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Local_StartDate DATETIME = @StartDate;
	DECLARE @Local_EndDate DATETIME = @EndDate;
	DECLARE @Local_AudienceSegmentation VARCHAR(max) = @AudienceSegmentation;
	DECLARE @Local_FilterMasterCardUsers BIT = @FilterMasterCardUsers;
	DECLARE @Local_FilterVendorProcessor BIT = @FilterVendorProcessor;
	DECLARE @Local_RegionId VARCHAR(255) = @RegionId;

	--
	EXEC [dbo].[UpdateSiteTrackingContext] @Local_StartDate;

	--
	IF object_id('tempdb..#temp', 'U') IS NOT NULL
		DROP TABLE #temp;

	CREATE TABLE #temp (
		TagId VARCHAR(255)
		,Identifier VARCHAR(255)
		,DisplayName VARCHAR(255)
		,How VARCHAR(1000)
		,[When] DATETIME
		,IssuerId VARCHAR(25)
		,ProcessorId VARCHAR(25)
		,UserId INT
		);

	--
	IF object_id('tempdb..#issuerForSegmentations', 'U') IS NOT NULL
		DROP TABLE #issuerForSegmentations;

	CREATE TABLE #issuerForSegmentations (
		IssuerId VARCHAR(25)
		,Title NVARCHAR(255)
		,AudienceSegments NVARCHAR(255)
		,IsMastercardIssuer BIT
		);

	INSERT INTO #issuerForSegmentations (
		IssuerId
		,Title
		,AudienceSegments
		,IsMastercardIssuer
		)
	SELECT DISTINCT i.IssuerID
		,i.Title
		,s.SegmentationName AS AudienceSegments
		,i.IsMastercardIssuer
	FROM Issuer i WITH (NOLOCK)
	LEFT JOIN IssuerSegmentation iss WITH (NOLOCK) ON i.IssuerID = iss.IssuerID
	LEFT JOIN Segmentation s WITH (NOLOCK) ON s.SegmentationId = iss.SegmentationId
	WHERE iss.IssuerID IS NULL
		OR iss.SegmentationId IN (
			SELECT Token
			FROM Split(@Local_AudienceSegmentation, ',')
			)
		OR @Local_RegionId = 'fmm';

	--
	INSERT INTO #temp (
		TagId
		,Identifier
		,DisplayName
		,How
		,[When]
		,IssuerId
		,ProcessorId
		,UserId
		)
	SELECT t.TagID
		,t.Identifier
		,COALESCE(t.DisplayName, tt.DisplayName, t.Identifier) DisplayName
		,COALESCE(st.How, 'generic') How
		,st.[When]
		,i.IssuerID
		,p.ProcessorID
		,u.UserID
	FROM vSiteTrackingByContext AS st
	INNER JOIN Tag AS t ON (
			st.What LIKE N'/portal/tagbrowser/%' + t.Identifier
			OR st.What LIKE N'/portal/tagbrowser%/' + t.Identifier + '/%'
			OR st.What LIKE N'http%//%/portal/tagbrowser/%' + t.Identifier
			OR st.What LIKE N'http%//%/portal/tagbrowser%/' + t.Identifier + '/%'
			)
	INNER JOIN [User] AS u ON LOWER(u.UserName) = LOWER(st.Who)
	INNER JOIN #issuerForSegmentations AS i ON u.IssuerId = i.IssuerID
	LEFT JOIN IssuerProcessor AS ipp ON i.IssuerID = ipp.IssuerID
	LEFT JOIN Issuer AS iss ON ipp.IssuerID = iss.IssuerID
	LEFT JOIN Processor AS p ON ipp.ProcessorID = p.ProcessorID
	LEFT JOIN TagTranslatedContent AS tt ON t.TagID = tt.TagId
		AND tt.LanguageCode = 'en'
	WHERE [When] BETWEEN @Local_StartDate
			AND @Local_EndDate
		AND (
			What LIKE N'/portal/tagbrowser%'
			OR What LIKE N'http%//%/portal/tagbrowser%' --NEW
			)
		AND What NOT LIKE N'/portal/tagbrowser%/[0-9]%'
		AND What NOT LIKE N'http%//%/portal/tagbrowser%/[0-9]%' --NEW
		AND st.Region = @Local_RegionId
		AND (
			(
				p.ProcessorID IS NULL
				AND @Local_RegionId <> 'us'
				)
			OR (
				(
					@Local_FilterMasterCardUsers = 0
					OR i.IsMastercardIssuer = 0
					)
				AND (
					@Local_FilterVendorProcessor = 0
					OR p.ProcessorID <> '91b'
					)
				)
			);

	SELECT T.TagId
		,T.DisplayName AS [Title]
		,COUNT(DISTINCT (ProcessorId)) AS Processors
		,COUNT(DISTINCT (IssuerId)) AS Issuers
		,COUNT(DISTINCT (UserId)) AS Users
		,COUNT(T.Identifier) AS [TotalHits]
		,COALESCE(SUM(generic.c), 0) + COALESCE(SUM(back.c), 0) AS [Generic]
		,COALESCE(SUM(browseall.c), 0) AS [BrowseAll]
		,COALESCE(SUM(tagbrowser.c), 0) AS [TagBrowser]
		,COALESCE(SUM(back.c), 0) AS [Back]
	FROM #temp AS T
	LEFT JOIN (
		SELECT 1 c
			,'generic' generic
		) AS generic ON T.How = generic.generic
	LEFT JOIN (
		SELECT 1 c
			,'browse-all' browseall
		) AS browseall ON T.How = browseall.browseall
	LEFT JOIN (
		SELECT 1 c
			,'tag-browser' tagbrowser
		) AS tagbrowser ON T.How = tagbrowser.tagbrowser
	LEFT JOIN (
		SELECT 1 c
			,'back' back
		) AS back ON T.How = back.back
	WHERE [When] BETWEEN @Local_StartDate
			AND @Local_EndDate
		AND T.IssuerId <> ('a05')
	GROUP BY T.TagId
		,T.DisplayName
	ORDER BY [Title];
END;