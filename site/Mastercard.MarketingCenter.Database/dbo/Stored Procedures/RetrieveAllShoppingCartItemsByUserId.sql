﻿CREATE PROCEDURE [dbo].[RetrieveAllShoppingCartItemsByUserId]
(
	@UserId as nvarchar(255)
)
as
	SELECT	oa.Title, 
            oa.ShortDescription, 
            oa.ThumbnailImage, 
            oa.Sku,
            sc.UserID, 
            sc.CartID, 
            sc.DateCreated, 
            sc.LastUpdated, 
            sc.DatedCheckedOut,
            oa.ContentItemId, 
            case ci.ElectronicDelivery when 0 then oa.Vdp else cast(0 as bit) end VDP, 
            ci.Quantity,
            ci.ShippingInformationID,
            oa.Customizable,
			ci.ElectronicDelivery,
			oa.ElectronicDeliveryFile,
			oa.ApproverEmailAddress
	FROM	[CartItem] ci,
            [OrderableAsset] oa,
            [ContentItem] coi,
            [ShoppingCart] sc
	where	ci.ContentItemID = oa.ContentItemId and
			coi.ContentItemID = oa.ContentItemId and
			ci.CartID = sc.CartID and
			sc.UserID = @UserId and 
			sc.DatedCheckedOut is null