﻿CREATE PROCEDURE [dbo].[RetrieveUserEmailByText]
(
	@FilterText nvarchar(255),
	@ProcessorID varchar(25),
	@RegionId nchar(6)
)
as
begin
	if @ProcessorID = ''
		set @ProcessorID = null;

	SELECT	u.[Email] as Email
	from    [User] u join [Issuer] i on
				u.IssuerId = i.IssuerID left outer join
			[IssuerProcessor] ip on
				i.IssuerID = ip.IssuerID left outer join
			(select * from [Processor] where ProcessorID = coalesce(@ProcessorID, ProcessorID)) p on
				ip.ProcessorID = p.ProcessorID
	where	u.[Email] like  @FilterText + '%' and
			lower(u.UserName) like 'mastercardmembers:%' and
			u.IssuerId is not null and
			i.RegionId = @RegionId and
			u.IsDisabled = 0
	order by Email asc
end