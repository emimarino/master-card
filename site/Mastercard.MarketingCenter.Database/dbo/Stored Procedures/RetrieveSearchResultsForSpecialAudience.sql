﻿CREATE PROCEDURE [dbo].[RetrieveSearchResultsForSpecialAudience]
(
	@SearchQuery				NVARCHAR(200),
	@CurrentSegmentation		VARCHAR(MAX),
	@CurrentSpecialAudience		VARCHAR(255),
	@RegionId					NCHAR(6)
)
AS
BEGIN
--
DECLARE @vMatchedTags AS TABLE (TagID VARCHAR(25), [Rank] INT);
DECLARE @vSegmentationIds AS TABLE (SegmentationId VARCHAR(25));
DECLARE @containsQuery nvarchar(2000);
set @containsQuery = replace(ltrim(rtrim(@SearchQuery)), ' ', ' AND ');
DECLARE @result AS TABLE (ContentItemId VARCHAR(25), 
[CreatedDate] datetime,
[ModifiedDate] datetime,
[Title] nvarchar(255),
[DisplayUrl] varchar(500),
[Description] nvarchar(max),
[RelatedProgramContentItemId] varchar(25),
[IsLandingPageResult] bit,
[ContentTypePriority] int,
[IsContentIndex] bit,
[Order] int,
[TitleRank] int,
[TagRank] int,
[OtherRank] int,
[Rank] int,
[TitleContainsQuery] int,
[ExactMatch] bit,
[Visits] int
);
--
INSERT INTO @vMatchedTags
	SELECT [Key], [Rank] FROM FREETEXTTABLE(dbo.Tag, (Identifier), @SearchQuery);
--
INSERT INTO @vMatchedTags
	SELECT tt.TagId AS [KEY], ftt.[RANK] AS [Rank] FROM
	TagTranslatedContent tt
	JOIN Tag t ON t.TagID =tt.TagId
	JOIN FREETEXTTABLE(dbo.TagTranslatedContent, ( DisplayName), @SearchQuery) ftt ON ftt.[KEY] =tt.Id;

INSERT INTO @vSegmentationIds SELECT Token FROM Split(@CurrentSegmentation, ',');


--
WITH 
ContentItem AS
(
	SELECT * 
	FROM dbo.ContentItem 
	WHERE RegionId = @RegionId
	AND StatusID = 3
	AND (ContentItem.ExpirationDate > GETDATE() OR ExpirationDate IS NULL)
),
ContentItemForSegmentations AS (
			SELECT DISTINCT	ci.ContentItemID
			FROM			ContentItem ci
			LEFT JOIN		ContentItemSegmentation cis WITH(NOLOCK) ON ci.ContentItemID = cis.ContentItemId
			WHERE			cis.ContentItemId IS NULL OR cis.SegmentationId IN (SELECT SegmentationId FROM @vSegmentationIds)
),
CTE_SEARCH
AS
(
SELECT
	CI.ContentItemId,
	CI.CreatedDate,
	CI.ModifiedDate,
	PA.Title,
	PA.Uri AS FrontEndUrl,
	CASE WHEN (PA.ShortDescription IS NULL OR PA.ShortDescription = '') THEN PA.IntroCopy ELSE PA.ShortDescription END AS ShortDescription,
	null RelatedProgramContentItemId,
	LPT.[Rank] AS TitleRank,
	LPO.[Rank] AS OtherRank,
	MCT.[Rank] AS TagRank,
	(case when CONTAINS(PA.Title, @containsQuery) then 1 else 0 end) TitleContainsQuery,
	2 [ContentTypePriority]
FROM
	ContentItem CI
	INNER JOIN ContentItemForSegmentations CIFS WITH (NOLOCK) ON CIFS.ContentItemId = CI.ContentItemID
	JOIN dbo.Page PA ON PA.ContentItemId = CI.ContentItemID
	LEFT JOIN FREETEXTTABLE(dbo.Page, (Title), @SearchQuery) LPT ON LPT.[Key] = PA.ContentItemId
	LEFT JOIN FREETEXTTABLE(dbo.Page, (Uri, IntroCopy, Terms, ShortDescription), @SearchQuery) LPO ON LPO.[Key] = PA.ContentItemId
	OUTER APPLY (SELECT TOP 1 MT.TagID, MT.[Rank]
				 FROM @vMatchedTags MT JOIN ContentItemTag CIT ON CIT.TagID = MT.TagID
				 WHERE CIT.ContentItemID = PA.ContentItemId
				 ORDER BY [Rank] DESC) MCT	
WHERE
		PA.HideFromSearch <> 1
	AND (LPT.[Key] IS NOT NULL OR LPO.[Key] IS NOT NULL OR MCT.TagID IS NOT NULL)
	AND (PA.RestrictToSpecialAudience IS NULL OR PA.RestrictToSpecialAudience = '' OR
		 (PA.RestrictToSpecialAudience LIKE '%' + @CurrentSpecialAudience + '%') OR (@CurrentSpecialAudience LIKE '%' + PA.RestrictToSpecialAudience + '%'))
	AND CI.StatusID = 3
--		  
UNION
--
SELECT
	CI.ContentItemId,
	CI.CreatedDate,
	CI.ModifiedDate,
	AT.Title,
	CI.FrontEndUrl,
	AT.ShortDescription,
	(select top 1 replace(arp.ProgramContentItemId, '-p', '') from AssetRelatedProgram arp where arp.AssetContentItemId = CI.ContentItemID) RelatedProgramContentItemId,
	ATT.[Rank] AS TitleRank,
	ATO.[Rank] AS OtherRank,
	MCT.[Rank] AS TagRank,
	(case when CONTAINS(AT.Title, @containsQuery) then 1 else 0 end) TitleContainsQuery,
	4 [ContentTypePriority]
FROM
	ContentItem CI
	INNER JOIN ContentItemForSegmentations CIFS WITH (NOLOCK) ON CIFS.ContentItemId = CI.ContentItemID
	JOIN dbo.Asset AT ON AT.ContentItemId = CI.ContentItemID
	LEFT JOIN FREETEXTTABLE(dbo.Asset, (Title), @SearchQuery) ATT ON ATT.[Key] = AT.ContentItemId
	LEFT JOIN FREETEXTTABLE(dbo.Asset, (ShortDescription, LongDescription, BrowseAllMaterialsDisplayText), @SearchQuery) ATO ON ATO.[Key] = AT.ContentItemId
	OUTER APPLY (SELECT TOP 1 MT.TagID, MT.[Rank]
				 FROM @vMatchedTags MT JOIN ContentItemTag CIT ON CIT.TagID = MT.TagID
				 WHERE CIT.ContentItemID = AT.ContentItemId
				 ORDER BY [Rank] DESC) MCT	
WHERE
		(ATT.[Key] IS NOT NULL OR ATO.[Key] IS NOT NULL OR MCT.TagID IS NOT NULL)
	AND (AT.RestrictToSpecialAudience IS NULL OR AT.RestrictToSpecialAudience = '' OR
		 (AT.RestrictToSpecialAudience LIKE '%' + @CurrentSpecialAudience + '%') OR (@CurrentSpecialAudience LIKE '%' + AT.RestrictToSpecialAudience + '%'))
	AND CI.StatusID = 3
--		  
UNION
--
SELECT
	CI.ContentItemId,
	CI.CreatedDate,
	CI.ModifiedDate,
	AW.Title,
	CI.FrontEndUrl,
	AW.ShortDescription,
	(select top 1 replace(arp.ProgramContentItemId, '-p', '') from AssetFullWidthRelatedProgram arp where arp.AssetFullWidthContentItemId = CI.ContentItemID) RelatedProgramContentItemId,
	AWT.[Rank] AS TitleRank,
	AWO.[Rank] AS OtherRank,
	MCT.[Rank] AS TagRank,
	(case when CONTAINS(AW.Title, @containsQuery) then 1 else 0 end) TitleContainsQuery,
	6 [ContentTypePriority]
FROM
	ContentItem CI
	INNER JOIN ContentItemForSegmentations CIFS WITH (NOLOCK) ON CIFS.ContentItemId = CI.ContentItemID
	JOIN dbo.AssetFullWidth AW ON AW.ContentItemId = CI.ContentItemID
	LEFT JOIN FREETEXTTABLE(dbo.AssetFullWidth, (Title), @SearchQuery) AWT ON AWT.[Key] = AW.ContentItemId
	LEFT JOIN FREETEXTTABLE(dbo.AssetFullWidth, (ShortDescription, LongDescription), @SearchQuery) AWO ON AWO.[Key] = AW.ContentItemId
	OUTER APPLY (SELECT TOP 1 MT.TagID, MT.[Rank]
				 FROM @vMatchedTags MT JOIN ContentItemTag CIT ON CIT.TagID = MT.TagID
				 WHERE CIT.ContentItemID = AW.ContentItemId
				 ORDER BY [Rank] DESC) MCT
WHERE	
		(AWT.[Key] IS NOT NULL OR AWO.[Key] IS NOT NULL OR MCT.TagID IS NOT NULL)
	AND (AW.RestrictToSpecialAudience IS NULL OR AW.RestrictToSpecialAudience = '' OR
		 (AW.RestrictToSpecialAudience LIKE '%' + @CurrentSpecialAudience + '%') OR (@CurrentSpecialAudience LIKE '%' + AW.RestrictToSpecialAudience + '%'))
	AND CI.StatusID = 3
--		  
UNION
--
SELECT
	CI.ContentItemId,
	CI.CreatedDate,
	CI.ModifiedDate,
	DA.Title,
	CI.FrontEndUrl,
	DA.ShortDescription,
	(select top 1 replace(arp.ProgramContentItemId, '-p', '') from DownloadableAssetRelatedProgram arp where arp.DownloadableAssetContentItemId = CI.ContentItemID) RelatedProgramContentItemId,
	DAT.[Rank] AS TitleRank,
	DAO.[Rank] AS OtherRank,
	MCT.[Rank] AS TagRank,
	(case when CONTAINS(DA.Title, @containsQuery) then 1 else 0 end) TitleContainsQuery,
	3 [ContentTypePriority]
FROM
	ContentItem CI
	INNER JOIN ContentItemForSegmentations CIFS WITH (NOLOCK) ON CIFS.ContentItemId = CI.ContentItemID
	JOIN dbo.DownloadableAsset DA ON DA.ContentItemId = CI.ContentItemID
	LEFT JOIN FREETEXTTABLE(dbo.DownloadableAsset, (Title), @SearchQuery) DAT ON DAT.[Key] = DA.ContentItemId
	LEFT JOIN FREETEXTTABLE(dbo.DownloadableAsset, (ShortDescription, LongDescription), @SearchQuery) DAO ON DAO.[Key] = DA.ContentItemId
	OUTER APPLY (SELECT TOP 1 MT.TagID, MT.[Rank]
				 FROM @vMatchedTags MT JOIN ContentItemTag CIT ON CIT.TagID = MT.TagID
				 WHERE CIT.ContentItemID = DA.ContentItemId
				 ORDER BY [Rank] DESC) MCT
WHERE
		(DAT.[Key] IS NOT NULL OR DAO.[Key] IS NOT NULL OR MCT.TagID IS NOT NULL)
	AND (DA.RestrictToSpecialAudience IS NULL OR DA.RestrictToSpecialAudience = '' OR
		 (DA.RestrictToSpecialAudience LIKE '%' + @CurrentSpecialAudience + '%') OR (@CurrentSpecialAudience LIKE '%' + DA.RestrictToSpecialAudience + '%'))
	AND CI.StatusID = 3
	AND CI.ContentItemID NOT IN
	((SELECT 
     CASE 
       WHEN OrderableAssetContentItemId IS NOT NULL THEN OrderableAssetContentItemId
           WHEN DownloadableAssetContentItemId IS NOT NULL THEN DownloadableAssetContentItemId
           ELSE p.ContentItemId
     END
   FROM (  SELECT
           CASE
                   WHEN PrintedOrderPeriodEndDate IS NULL THEN OnlineOrderPeriodEndDate
                   WHEN OnlineOrderPeriodEndDate IS NULL THEN PrintedOrderPeriodEndDate
                   WHEN PrintedOrderPeriodEndDate >= OnlineOrderPeriodEndDate THEN PrintedOrderPeriodEndDate
                   WHEN OnlineOrderPeriodEndDate >= PrintedOrderPeriodEndDate THEN OnlineOrderPeriodEndDate
           END AS expdate, ContentItemId
       FROM Program) AS pd
       LEFT JOIN OrderableAssetRelatedProgram oa ON pd.ContentItemId = oa.ProgramContentItemId
       LEFT JOIN DownloadableAssetRelatedProgram da ON pd.ContentItemId = da.ProgramContentItemId
       LEFT JOIN Program p ON pd.ContentItemId = p.ContentItemId
   WHERE expdate < GETDATE())
   UNION
   (SELECT pd.ContentItemId
   FROM (  SELECT 
           CASE
                   WHEN PrintedOrderPeriodEndDate IS NULL THEN OnlineOrderPeriodEndDate
                   WHEN OnlineOrderPeriodEndDate IS NULL THEN PrintedOrderPeriodEndDate
                   WHEN PrintedOrderPeriodEndDate >= OnlineOrderPeriodEndDate THEN PrintedOrderPeriodEndDate
                   WHEN OnlineOrderPeriodEndDate >= PrintedOrderPeriodEndDate THEN OnlineOrderPeriodEndDate
           END AS expdate, ContentItemId
       FROM Program) AS pd
   WHERE expdate < GETDATE()))
--		  
UNION
--
SELECT
	CI.ContentItemId,
	CI.CreatedDate,
	CI.ModifiedDate,
	OA.Title,
	CI.FrontEndUrl,
	OA.ShortDescription,
	(select top 1 replace(arp.ProgramContentItemId, '-p', '') from OrderableAssetRelatedProgram arp where arp.OrderableAssetContentItemId = CI.ContentItemID) RelatedProgramContentItemId,
	OAT.[Rank] AS TitleRank,
	OAO.[Rank] AS OtherRank,
	MCT.[Rank] AS TagRank,
	(case when CONTAINS(OA.Title, @containsQuery) then 1 else 0 end) TitleContainsQuery,
	5 [ContentTypePriority]
FROM
	ContentItem CI
	INNER JOIN ContentItemForSegmentations CIFS WITH (NOLOCK) ON CIFS.ContentItemId = CI.ContentItemID
	JOIN dbo.OrderableAsset OA ON OA.ContentItemId = CI.ContentItemID
	LEFT JOIN FREETEXTTABLE(dbo.OrderableAsset, (Title), @SearchQuery) OAT ON OAT.[Key] = OA.ContentItemId
	LEFT JOIN FREETEXTTABLE(dbo.OrderableAsset, (ShortDescription, LongDescription, Sku), @SearchQuery) OAO ON OAO.[Key] = OA.ContentItemId
	OUTER APPLY (SELECT TOP 1 MT.TagID, MT.[Rank]
				 FROM @vMatchedTags MT JOIN ContentItemTag CIT ON CIT.TagID = MT.TagID
				 WHERE CIT.ContentItemID = OA.ContentItemId
				 ORDER BY [Rank] DESC) MCT
WHERE
		(OAT.[Key] IS NOT NULL OR OAO.[Key] IS NOT NULL OR MCT.TagID IS NOT NULL)
	AND (OA.RestrictToSpecialAudience IS NULL OR OA.RestrictToSpecialAudience = '' OR
		 (OA.RestrictToSpecialAudience LIKE '%' + @CurrentSpecialAudience + '%') OR (@CurrentSpecialAudience LIKE '%' + OA.RestrictToSpecialAudience + '%'))
	AND CI.StatusID = 3
	AND CI.ContentItemID NOT IN
	((SELECT 
     CASE 
       WHEN OrderableAssetContentItemId IS NOT NULL THEN OrderableAssetContentItemId
           WHEN DownloadableAssetContentItemId IS NOT NULL THEN DownloadableAssetContentItemId
           ELSE p.ContentItemId
     END
   FROM (  SELECT
           CASE
                   WHEN PrintedOrderPeriodEndDate IS NULL THEN OnlineOrderPeriodEndDate
                   WHEN OnlineOrderPeriodEndDate IS NULL THEN PrintedOrderPeriodEndDate
                   WHEN PrintedOrderPeriodEndDate >= OnlineOrderPeriodEndDate THEN PrintedOrderPeriodEndDate
                   WHEN OnlineOrderPeriodEndDate >= PrintedOrderPeriodEndDate THEN OnlineOrderPeriodEndDate
           END AS expdate, ContentItemId
       FROM Program) AS pd
       LEFT JOIN OrderableAssetRelatedProgram oa ON pd.ContentItemId = oa.ProgramContentItemId
       LEFT JOIN DownloadableAssetRelatedProgram da ON pd.ContentItemId = da.ProgramContentItemId
       LEFT JOIN Program p ON pd.ContentItemId = p.ContentItemId
   WHERE expdate < GETDATE())
   UNION
   (SELECT pd.ContentItemId
   FROM (  SELECT 
           CASE
                   WHEN PrintedOrderPeriodEndDate IS NULL THEN OnlineOrderPeriodEndDate
                   WHEN OnlineOrderPeriodEndDate IS NULL THEN PrintedOrderPeriodEndDate
                   WHEN PrintedOrderPeriodEndDate >= OnlineOrderPeriodEndDate THEN PrintedOrderPeriodEndDate
                   WHEN OnlineOrderPeriodEndDate >= PrintedOrderPeriodEndDate THEN OnlineOrderPeriodEndDate
           END AS expdate, ContentItemId
       FROM Program) AS pd
   WHERE expdate < GETDATE()))
--
UNION
--
SELECT
	CI.ContentItemId,
	CI.CreatedDate,
	CI.ModifiedDate,
	PR.Title,
	CI.FrontEndUrl,
	PR.ShortDescription,
	CI.ContentItemId RelatedProgramContentItemId,
	(PRT.[Rank] + (PRT.[Rank] * (4000/(datediff(dayofyear, CI.ModifiedDate, getdate()) + 50)))) AS TitleRank,
	PRO.[Rank] AS OtherRank,
	MCT.[Rank] AS TagRank,
	(case when CONTAINS(PR.Title, @containsQuery) then 1 else 0 end) TitleContainsQuery,
	1 [ContentTypePriority]
FROM
	ContentItem CI
	INNER JOIN ContentItemForSegmentations CIFS WITH (NOLOCK) ON CIFS.ContentItemId = CI.ContentItemID
	JOIN dbo.Program PR ON PR.ContentItemId = CI.ContentItemID
	LEFT JOIN FREETEXTTABLE(dbo.Program, (Title), @SearchQuery) PRT ON PRT.[Key] = PR.ContentItemId
	LEFT JOIN FREETEXTTABLE(dbo.Program, (ShortDescription, LongDescription), @SearchQuery) PRO ON PRO.[Key] = PR.ContentItemId
	OUTER APPLY (SELECT TOP 1 MT.TagID, MT.[Rank]
				 FROM @vMatchedTags MT JOIN ContentItemTag CIT ON CIT.TagID = MT.TagID
				 WHERE CIT.ContentItemID = PR.ContentItemId
				 ORDER BY [Rank] DESC) MCT
WHERE
		(PRT.[Key] IS NOT NULL OR PRO.[Key] IS NOT NULL OR MCT.TagID IS NOT NULL)
	AND (PR.RestrictToSpecialAudience IS NULL OR PR.RestrictToSpecialAudience = '' OR
		 (PR.RestrictToSpecialAudience LIKE '%' + @CurrentSpecialAudience + '%') OR (@CurrentSpecialAudience LIKE '%' + PR.RestrictToSpecialAudience + '%'))
	AND CI.StatusID = 3
--		  
UNION
--
SELECT
	CI.ContentItemId,
	CI.CreatedDate,
	CI.ModifiedDate,
	WE.Title,
	CI.FrontEndUrl,
	WE.ShortDescription,
	null RelatedProgramContentItemId,
	WET.[Rank] AS TitleRank,
	WEO.[Rank] AS OtherRank,
	MCT.[Rank] AS TagRank,
	(case when CONTAINS(WE.Title, @containsQuery) then 1 else 0 end) TitleContainsQuery,
	7 [ContentTypePriority]
FROM
	ContentItem CI
	INNER JOIN ContentItemForSegmentations CIFS WITH (NOLOCK) ON CIFS.ContentItemId = CI.ContentItemID
	JOIN dbo.Webinar WE ON WE.ContentItemId = CI.ContentItemID
	LEFT JOIN FREETEXTTABLE(dbo.Webinar, (Title), @SearchQuery) WET ON WET.[Key] = WE.ContentItemId
	LEFT JOIN FREETEXTTABLE(dbo.Webinar, (ShortDescription, LongDescription, Link), @SearchQuery) WEO ON WEO.[Key] = WE.ContentItemId
	OUTER APPLY (SELECT TOP 1 MT.TagID, MT.[Rank]
				 FROM @vMatchedTags MT JOIN ContentItemTag CIT ON CIT.TagID = MT.TagID
				 WHERE CIT.ContentItemID = WE.ContentItemId
				 ORDER BY [Rank] DESC) MCT
WHERE
		(WET.[Key] IS NOT NULL OR WEO.[Key] IS NOT NULL OR MCT.TagID IS NOT NULL)
	AND (WE.RestrictToSpecialAudience IS NULL OR WE.RestrictToSpecialAudience = '' OR
		 (WE.RestrictToSpecialAudience LIKE '%' + @CurrentSpecialAudience + '%') OR (@CurrentSpecialAudience LIKE '%' + WE.RestrictToSpecialAudience + '%'))
	AND CI.StatusID = 3
)
insert into @result
SELECT 
	c.ContentItemId,
	c.CreatedDate,
	c.ModifiedDate,
	c.Title,
	CASE WHEN SUBSTRING(c.FrontEndUrl, 1, 1) = '/' THEN '/portal' + c.FrontEndUrl ELSE '/portal/' + c.FrontEndUrl END AS DisplayUrl,
	'||HTML||' + c.ShortDescription AS [Description],
	c.RelatedProgramContentItemId,
	CAST(0 AS BIT) AS IsLandingPageResult,
	c.ContentTypePriority,
	CAST(0 AS BIT) AS IsContentIndex,
	0 AS [Order],
	c.TitleRank,
	c.TagRank,
	c.OtherRank,
	SQUARE(ISNULL(c.TitleRank + 100, 0)) + ISNULL(c.TagRank + 1000, 0) + ISNULL(c.OtherRank, 0) AS [Rank],
	c.TitleContainsQuery,
	(case when ltrim(rtrim(c.Title)) = ltrim(rtrim(@SearchQuery)) then 1 else 0 end) ExactMatch,
	isnull(v.Visits, 0) Visits
FROM
	CTE_SEARCH c left outer join ContentItemVisits v on
		c.ContentItemID = v.ContentItemId;

select	r1.*,  
		case ExactMatch when 0 then case isnull(r2.[TitleContainsQuery], r1.[TitleContainsQuery]) when 0 then isnull(r2.[Rank], r1.[Rank]) else 10000000 end else 10000000 end [ProgramRank],
		isnull(r2.[TitleContainsQuery], r1.[TitleContainsQuery]) [ProgramTitleContainsQuery]
from @result r1 left outer join  
	(select RelatedProgramContentItemId, max([Rank]) [Rank], max(TitleContainsQuery) TitleContainsQuery
	from @result 
	where RelatedProgramContentItemId is not null
	group by RelatedProgramContentItemId) as r2 on r2.RelatedProgramContentItemId = r1.RelatedProgramContentItemId 
ORDER BY
	[ExactMatch] DESC, [ProgramTitleContainsQuery] DESC, [ProgramRank] DESC, r1.ContentTypePriority asc, r1.[Rank] DESC, r1.Visits desc, r1.ModifiedDate desc, r1.CreatedDate desc;

END