﻿CREATE PROCEDURE [dbo].[RetrieveCustomizations]
(
	@UserID as nvarchar(255),
	@ShoppingCartID as int = null,
	@OrderID int = null,
	@RecalculateCartOptions bit
)
as
if(@ShoppingCartID is not null)
begin
	
	if(@RecalculateCartOptions = 1 or not exists(select  cd.CustomizationData from  CustomizationDetail cd where cd.ShoppingCartID = @ShoppingCartID))
	begin
		SELECT	co.InternalName, 
				co.Instructions, 
				co.FieldType, 
				co.MaxLength, 
				co.Modified, 
				co.FriendlyName, 
				co.CustomizationOptionID, 
				(select top 1 oacinner.Required 
				 from	OrderableAssetCustomizationOption oacinner
				 where	oacinner.ContentItemID in (select ContentItemID 
											  from CartItem InnerCi 
											  where InnerCi.CartID=@ShoppingCartID) and 
					    oac.CustomizationOptionID=oacinner.CustomizationOptionID 
				 order by oacinner.Required desc) as Required,
                (SELECT TOP (1) cd.CustomizationData  
                 FROM   ShoppingCart sc INNER JOIN 
						CustomizationDetail cd ON  
							sc.CartID =cd.ShoppingCartID INNER JOIN
		                OrderableAssetCustomizationOption as oacinner ON 
			                cd.CustomizationOptionID = oacinner.CustomizationOptionID
				 WHERE  (sc.UserID = @UserID) AND 
						(cd.CustomizationOptionID =  Co.CustomizationOptionID)
				 ORDER BY cd.CustomizationDetailID Desc,  
						 oacinner.Required  DESC) AS DefaultCustomizationData,
				dbo.Concat(oa.Title + ', ') ApplicableItems
		FROM	CustomizationOption as co INNER JOIN
				OrderableAssetCustomizationOption AS oac ON 
					co.CustomizationOptionID = oac.CustomizationOptionID inner join
				OrderableAsset oa on
					oa.ContentItemId = oac.ContentItemID
		where	oac.ContentItemID in (select ContentItemID from CartItem
								where CartID = @ShoppingCartID and
										ElectronicDelivery = 0) and
				oa.Customizable = 1
		group by co.InternalName, 
				co.Instructions, 
				co.FieldType, 
				co.MaxLength, 
				co.Modified, 
				co.FriendlyName, 
				co.CustomizationOptionID,
				oac.CustomizationOptionID
	end
	else
	begin
		SELECT	co.InternalName, 
				co.Instructions, 
				co.FieldType, 
				co.MaxLength, 
				co.Modified, 
				co.FriendlyName, 
				co.CustomizationOptionID, 
				isnull((select top 1 oacinner.Required 
						from	OrderableAssetCustomizationOption oacinner
						where	oacinner.ContentItemID in (select ContentItemID from CartItem InnerCi where InnerCi.CartID = cd.ShoppingCartID) and 
								oacinner.CustomizationOptionID = cd.CustomizationOptionID
						order by oacinner.Required desc),0) as Required, 
				cd.CustomizationData AS DefaultCustomizationData,
				'' ApplicableItems
		FROM    ShoppingCart AS sc INNER JOIN
                CustomizationDetail AS cd ON 
					sc.CartID = cd.ShoppingCartID INNER JOIN
                CustomizationOption AS co ON 
					cd.CustomizationOptionID = co.CustomizationOptionID
		WHERE   sc.CartID = @ShoppingCartID
	end
end
else
begin
	SELECT co.InternalName, 
		   co.Instructions, 
		   co.FieldType, 
		   co.MaxLength, 
		   co.Modified, 
		   co.FriendlyName, 
		   co.CustomizationOptionID, 
		   isnull((select	top 1 oacinner.Required 
		    from	OrderableAssetCustomizationOption oacinner
		    where	oacinner.ContentItemID in (select ItemID from OrderItem InnerOI where InnerOI.OrderID=cd.OrderID) and 
					oacinner.CustomizationOptionID = cd.CustomizationOptionID
			order by oacinner.Required desc), 0) as Required, 
           cd.CustomizationData AS DefaultCustomizationData,
           '' ApplicableItems
	FROM   [Order] AS odr INNER JOIN
           CustomizationDetail AS cd ON 
				odr.OrderID = Cd.OrderID INNER JOIN
           CustomizationOption AS co ON 
				cd.CustomizationOptionID = co.CustomizationOptionID
	WHERE  odr.OrderID = @OrderID
end





