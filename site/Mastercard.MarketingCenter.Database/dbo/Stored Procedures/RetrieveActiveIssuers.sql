﻿CREATE PROCEDURE [dbo].[RetrieveActiveIssuers]
AS
--
SELECT		i.IssuerID, 
			i.Title, 
			i.DomainName, 
			i.BillingID,
			i.CID, 
			i.MatchConfirmed, 
			p.Title Processor, 
			issuerUser.UserCount, 
			issuerUser.LastLoginDate, 
			i.HasOrdered, 
			i.DateCreated,
			i.AudienceSegments,
			REPLACE(i.SpecialAudienceAccess, ';#', '') SpecialAudienceAccess, 
			i.ModifiedDate,
			CASE
				WHEN i.CID IS NULL AND mh.CID IS NULL
				THEN ''
				WHEN mh.CID IS NOT NULL
				THEN 'Yes'
				ELSE 'No'
			END AS [HeatmapData]
FROM		(
				SELECT		i.*,
							s.SegmentationName AudienceSegments
				FROM		Issuer i
				LEFT JOIN	IssuerSegmentation iss ON i.IssuerID = iss.IssuerID
				LEFT JOIN	Segmentation s ON s.SegmentationId = iss.SegmentationId
				WHERE		i.RegionId = 'us'
			) i
			JOIN		IssuerProcessor ip on i.IssuerID = ip.IssuerID  
			JOIN		Processor p on p.ProcessorID = ip.ProcessorID
			LEFT OUTER JOIN
			(
				SELECT		i.IssuerID, 
							COUNT(u.IssuerId) UserCount, 
							MAX(mu.LastLoginDate) LastLoginDate
				FROM		Issuer i 
							JOIN	[User] u ON u.IssuerId = i.IssuerID
							JOIN	[aspnet_Membership] mu ON u.Email = mu.Email
				GROUP BY	i.IssuerID
			) issuerUser ON issuerUser.IssuerID = i.IssuerID
			LEFT JOIN	MetricHeatmap mh ON i.CID = mh.CID
ORDER BY	LastLoginDate DESC