﻿CREATE PROCEDURE [dbo].[RetrieveUsersFromOrders]
(
	@FilterText nvarchar(255),
	@ProcessorID varchar(25)
)
as
begin
	if @ProcessorID <> ''
		SELECT	distinct(u.FullName) UserName
		from    [User] u,
				[IssuerProcessor] ip,
				[Order] o
		where	u.IssuerId = ip.IssuerID and
				ip.ProcessorID = @ProcessorID and
				o.UserID = u.UserName and
				u.FullName like @FilterText + '%' and
				lower(u.UserName) like 'mastercardmembers:%' and
				u.IssuerId is not null and
				u.IsDisabled = 0
		order by u.FullName  asc
	else
		SELECT	distinct(u.FullName) UserName
		from    [User] u,
				[Order] o
		where	u.FullName like @FilterText + '%' and
				o.UserID = u.UserName and
				lower(u.UserName) like 'mastercardmembers:%' and
				u.IssuerId is not null and
				u.IsDisabled = 0
		order by u.FullName  asc
end