﻿
CREATE PROCEDURE [dbo].[RetrieveVdpOrderItemsByOrderId]
(
	@OrderId int
)
as
	SELECT	oi.ItemName, 
            oi.SKU,
			oi.ItemQuantity,
			oi.ItemPrice
	FROM	[OrderItem] oi
	where	oi.OrderID = @OrderId and
			oi.VDP = 1
