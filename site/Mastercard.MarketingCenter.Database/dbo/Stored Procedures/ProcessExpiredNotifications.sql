﻿CREATE PROCEDURE [dbo].[ProcessExpiredNotifications]( @DaysToExpire int)
AS
BEGIN

DECLARE	@AboutToExpireTemp	TABLE 
							(
								[ContentItemId] NVARCHAR(25),
								[ContentTypeId] NVARCHAR(25),
								[RegionId] NCHAR(6), 
								[ExpirationDate] DATETIME
							);

INSERT INTO @AboutToExpireTemp
([ContentItemId], [ContentTypeId], [RegionId], [ExpirationDate])
(SELECT [ContentItemId], [ContentTypeId], [RegionId], [ExpirationDate]
FROM [dbo].[ContentItem]
WHERE DATEDIFF(DAY, GETDATE(), [ExpirationDate]) BETWEEN 0 AND @DaysToExpire
AND [StatusID] IN (3,4) AND [ContentItemID] NOT LIKE '%-p'
AND [ReviewStateId] = 0);

DELETE FROM ContentAboutToExpire;

-- If about to expire inserts each content type data

-- Asset
INSERT INTO [dbo].[ContentAboutToExpire]
([ContentItemId], [ContentTypeId], [RegionId], [Title], [ExpirationDate], [BusinessOwnerId])
(SELECT x.[ContentItemId], ci.[ContentTypeId], ci.[RegionId], x.[Title], ci.[ExpirationDate], COALESCE(x.[BusinessOwnerID], NULL) BusinessOwnerID
FROM [dbo].[Asset] x
JOIN @AboutToExpireTemp ci ON ci.[ContentItemId] = x.[ContentItemId]
WHERE NOT EXISTS(SELECT 1 FROM [dbo].[ContentAboutToExpire] cae WHERE cae.[ContentItemId] = ci.[ContentItemId] AND ci.[ExpirationDate] = cae.[ExpirationDate]));

-- Downloadable Asset
INSERT INTO [dbo].[ContentAboutToExpire]
([ContentItemId], [ContentTypeId], [RegionId], [Title], [ExpirationDate], [BusinessOwnerId])
(SELECT x.[ContentItemId], ci.[ContentTypeId], ci.[RegionId], x.[Title], ci.[ExpirationDate], COALESCE(x.[BusinessOwnerID], NULL) BusinessOwnerID
FROM [dbo].[DownloadableAsset] x
JOIN @AboutToExpireTemp ci ON ci.[ContentItemId] = x.[ContentItemId]
WHERE NOT EXISTS(SELECT 1 FROM [dbo].[ContentAboutToExpire] cae WHERE cae.[ContentItemId] = ci.[ContentItemId] AND ci.[ExpirationDate] = cae.[ExpirationDate]));

-- Orderable Asset
INSERT INTO [dbo].[ContentAboutToExpire]
([ContentItemId], [ContentTypeId], [RegionId], [Title], [ExpirationDate], [BusinessOwnerId])
(SELECT x.[ContentItemId], ci.[ContentTypeId], ci.[RegionId], x.[Title], ci.[ExpirationDate], COALESCE(x.[BusinessOwnerID], NULL) BusinessOwnerID
FROM [dbo].[OrderableAsset] x
JOIN @AboutToExpireTemp ci ON ci.[ContentItemId] = x.[ContentItemId]
WHERE NOT EXISTS(SELECT 1 FROM [dbo].[ContentAboutToExpire] cae WHERE cae.[ContentItemId] = ci.[ContentItemId] AND ci.[ExpirationDate] = cae.[ExpirationDate]));

-- Program
INSERT INTO [dbo].[ContentAboutToExpire]
([ContentItemId], [ContentTypeId], [RegionId], [Title], [ExpirationDate], [BusinessOwnerId])
(SELECT x.[ContentItemId], ci.[ContentTypeId], ci.[RegionId], x.[Title], ci.[ExpirationDate], COALESCE(x.[BusinessOwnerID], NULL) BusinessOwnerID
FROM [dbo].[Program] x
JOIN @AboutToExpireTemp ci ON ci.[ContentItemId] = x.[ContentItemId]
WHERE NOT EXISTS(SELECT 1 FROM [dbo].[ContentAboutToExpire] cae WHERE cae.[ContentItemId] = ci.[ContentItemId] AND ci.[ExpirationDate] = cae.[ExpirationDate]));

END