﻿
CREATE PROCEDURE [dbo].[RetrieveOrderMonths]
as
select OrderDate, ODate, AsDate
from (
SELECT distinct case when Month(OrderDate) = Month(getdate()) and  Year(OrderDate) = Year(getdate()) then DATENAME(month, OrderDate) + ' ' + DATENAME(year, Orderdate) + ' (incomplete)' else DATENAME(month, OrderDate) +' '+ DATENAME(year, OrderDate) end as OrderDate, 
DATENAME(month,OrderDate)+' '+ DATENAME(Year,OrderDate) as Odate, cast((cast(Month(OrderDate) as varchar) + '/1/' + cast(Year(OrderDate) as varchar)) as datetime) AsDate 
from [Order]
union
SELECT distinct case when Month(DateCompleted) = Month(getdate()) and  Year(DateCompleted) = Year(getdate()) then DATENAME(month, DateCompleted) + ' ' + DATENAME(year, DateCompleted) + ' (incomplete)' else DATENAME(month, DateCompleted) +' '+ DATENAME(year, DateCompleted) end as OrderDate, 
DATENAME(month,DateCompleted)+' '+DATENAME(Year,DateCompleted) as Odate, cast((cast(Month(DateCompleted) as varchar) + '/1/' + cast(Year(DateCompleted) as varchar)) as datetime) AsDate 
from [Order]
where DateCompleted is not null) allorders
order by AsDate desc


