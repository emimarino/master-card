﻿CREATE PROCEDURE [dbo].[RetrieveOrderingActivityForProgramDetailReport] @ProgramId VARCHAR(25)
	,@StartDate DATETIME
	,@EndDate DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	--
	DECLARE @Local_StartDate DATETIME = @StartDate;
	DECLARE @Local_EndDate DATETIME = @EndDate;
	DECLARE @Local_ProgramId VARCHAR(25) = @ProgramId;
	--
	EXEC [dbo].[UpdateSiteTrackingContext] @Local_StartDate;
	--
	WITH Downloads
	AS (
		SELECT [When]
			,Who
			,ContentItemID
		FROM (
			SELECT *
			FROM vSiteTrackingByContext
			WHERE [From] LIKE '/portal/asset/%'
				AND What LIKE '/portal/file/librarydownload%'
			) tempdownloads
		)
	--
	SELECT '' AS [OrderNumber]
		,[When] AS 'Date'
		,u.[FullName] AS [User]
		,u.[Email]
		,p.title AS Processor
		,i.title AS 'FinancialInstitution'
		,u.UserName
		,s.SegmentationName AS AudienceSegments
		,da.title AS 'ItemName'
		,'Download' AS 'ItemType'
		,NULL AS SKU
		,NULL AS Fulfillment
		,0 AS 'ItemPrice'
		,0 AS 'ItemQuantity'
		,NULL AS 'OrderStatus'
		,pr.ContentItemId AS RelatedProgramID
		,pr.Title AS RelatedProgram
		,0 AS ED
		,0 AS POD
		,1 AS Download
		,0 AS EstDistribution
		,CONVERT(BIT, 0) AS AdvisorsTag
		,CONVERT(BIT, 0) AS ConsentGiven
	FROM Downloads d
	JOIN DownloadableAsset da ON d.ContentItemID = da.ContentItemId
	LEFT JOIN downloadableassetrelatedprogram darp ON da.contentitemid = darp.downloadableassetcontentitemid
	LEFT JOIN program pr ON darp.programcontentitemid = pr.contentitemid
	JOIN aspnet_Users au ON 'mastercardmembers:' + au.UserName = d.Who
	JOIN [User] u ON u.UserName = 'mastercardmembers:' + au.UserName
	JOIN issuer i ON i.IssuerID = u.IssuerId
	LEFT JOIN IssuerSegmentation iss ON i.IssuerId = iss.IssuerId
	LEFT JOIN Segmentation s ON s.SegmentationId = iss.SegmentationId
	JOIN IssuerProcessor ipp ON ipp.IssuerID = i.IssuerID
	JOIN Processor p ON p.ProcessorID = ipp.ProcessorID
	WHERE p.Title NOT IN ('A Plus')
		AND i.IsMastercardIssuer = 0
		AND [when] >= @Local_StartDate
		AND [when] < @Local_EndDate
		AND i.IssuerId <> 'a05'
		AND (
			pr.contentitemid = @Local_ProgramId
			OR (
				@Local_ProgramId = 'all'
				AND pr.ContentItemId IS NOT NULL
				AND pr.ContentItemId NOT LIKE '%-p'
				)
			)
	
	UNION
	
	SELECT O.[OrderNumber]
		,O.[OrderDate] AS 'Date'
		,u.[FullName] AS [User]
		,u.[Email]
		,p.title AS Processor
		,i.title AS 'FinancialInstitution'
		,u.UserName
		,s.SegmentationName AS AudienceSegments
		,oi.itemname AS 'ItemName'
		,'Orderable' AS 'ItemType'
		,oa.SKU
		,CASE 
			WHEN oi.ElectronicDelivery = 1
				THEN 'Electronic Delivery'
			WHEN oa.vdp = 1
				THEN 'VDP'
			ELSE 'Print On Demand'
			END AS 'Fulfillment'
		,oi.ItemPrice AS 'ItemPrice'
		,oi.ItemQuantity AS 'ItemQuantity'
		,o.OrderStatus AS 'OrderStatus'
		,pr.contentitemid AS RelatedProgramID
		,pr.Title AS RelatedProgram
		,CASE 
			WHEN oi.ElectronicDelivery = 1
				THEN 1
			ELSE 0
			END AS 'ED'
		,CASE 
			WHEN oa.vdp <> 1
				AND oi.ElectronicDelivery = 0
				THEN 1
			ELSE 0
			END AS 'POD'
		,0 AS Download
		,COALESCE(oi.EstimatedDistribution, 0) AS EstDistribution
		,COALESCE(oi.HasAdvisorsTag, convert(BIT, 0)) AS AdvisorsTag
		,COALESCE(o.ConsentGiven, convert(BIT, 0)) AS ConsentGiven
	FROM orderableasset oa
	LEFT JOIN orderableassetrelatedprogram oarp ON oa.contentitemid = oarp.orderableassetcontentitemid
	LEFT JOIN program pr ON oarp.programcontentitemid = pr.contentitemid
	JOIN OrderItem oi ON oa.ContentItemId = oi.ItemID
	JOIN [Order] o ON o.OrderID = oi.OrderID
	JOIN [User] u ON u.UserName = o.UserID
	JOIN Issuer i ON i.IssuerID = u.IssuerId
	LEFT JOIN IssuerSegmentation iss ON i.IssuerId = iss.IssuerId
	LEFT JOIN Segmentation s ON s.SegmentationId = iss.SegmentationId
	JOIN IssuerProcessor ipp ON ipp.IssuerID = i.IssuerID
	JOIN Processor p ON p.ProcessorID = ipp.ProcessorID
	WHERE p.Title NOT IN ('A Plus')
		AND i.IsMastercardIssuer = 0
		AND orderdate >= @Local_StartDate
		AND orderdate < @Local_EndDate
		AND i.IssuerId <> 'a05'
		AND (
			pr.contentitemid = @Local_ProgramId
			OR (
				@Local_ProgramId = 'all'
				AND pr.ContentItemId IS NOT NULL
				AND pr.ContentItemId NOT LIKE '%-p'
				)
			)
		AND o.OrderStatus <> 'Cancelled'

	SET context_info 0x;
END;