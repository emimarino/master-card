﻿CREATE PROCEDURE [dbo].[RetrieveSearchResults]
(
	@SearchQuery			NVARCHAR(200),
	@CurrentSegmentation	VARCHAR(MAX),
	@RegionId				NCHAR(6)
)
AS
BEGIN
--
DECLARE @vMatchedTags AS TABLE (TagID VARCHAR(25), [Rank] INT);
DECLARE @vSegmentationIds AS TABLE (SegmentationId VARCHAR(25));
DECLARE @result AS TABLE (ContentItemId VARCHAR(25), 
[CreatedDate] datetime,
[ModifiedDate] datetime,
[Title] nvarchar(255),
[DisplayUrl] varchar(500),
[Description] nvarchar(max),
[RelatedProgramContentItemId] varchar(25),
[IsLandingPageResult] bit,
[ContentTypePriority] int,
[IsContentIndex] bit,
[Order] int,
[TitleRank] int,
[TagRank] int,
[OtherRank] int,
[Rank] int,
[Visits] int
);
--
INSERT INTO @vMatchedTags
	SELECT [Key], [Rank] FROM FREETEXTTABLE(dbo.Tag, (Identifier), @SearchQuery);
--
INSERT INTO @vMatchedTags
	SELECT tt.TagId AS [KEY], ftt.[RANK] AS [Rank] FROM
	TagTranslatedContent tt
	JOIN Tag t ON t.TagID =tt.TagId
	JOIN FREETEXTTABLE(dbo.TagTranslatedContent, ( DisplayName), @SearchQuery) ftt ON ftt.[KEY] =tt.Id;

INSERT INTO @vSegmentationIds SELECT Token FROM Split(@CurrentSegmentation, ',');
--

WITH 
ContentItem AS
(
	SELECT * 
	FROM dbo.ContentItem 
	WHERE RegionId = @RegionId
	AND StatusID = 3
	AND (ContentItem.ExpirationDate > GETDATE() OR ExpirationDate IS NULL)
),
ContentItemForSegmentations AS (
			SELECT DISTINCT	ci.ContentItemID
			FROM			ContentItem ci
			LEFT JOIN		ContentItemSegmentation cis WITH(NOLOCK) ON ci.ContentItemID = cis.ContentItemID
			WHERE			cis.ContentItemID IS NULL OR cis.SegmentationId IN (SELECT SegmentationId FROM @vSegmentationIds)
),
CTE_SEARCH
AS
(
SELECT
	CI.ContentItemId,
	CI.CreatedDate,
	CI.ModifiedDate,
	PA.Title,
	PA.Uri AS FrontEndUrl,
	CASE WHEN (PA.ShortDescription IS NULL OR PA.ShortDescription = '') THEN PA.IntroCopy ELSE PA.ShortDescription END AS ShortDescription,
	null RelatedProgramContentItemId,
	LPT.[Rank] AS TitleRank,
	LPO.[Rank] AS OtherRank,
	MCT.[Rank] AS TagRank,
	2 [ContentTypePriority]
FROM
	ContentItem CI
	INNER JOIN ContentItemForSegmentations CIFS WITH (NOLOCK) ON CIFS.ContentItemId = CI.ContentItemId
	JOIN dbo.Page PA ON PA.ContentItemId = CI.ContentItemID
	LEFT JOIN FREETEXTTABLE(dbo.Page, (Title), @SearchQuery) LPT ON LPT.[Key] = PA.ContentItemId
	LEFT JOIN FREETEXTTABLE(dbo.Page, (Uri, IntroCopy, Terms, ShortDescription), @SearchQuery) LPO ON LPO.[Key] = PA.ContentItemId
	OUTER APPLY (SELECT TOP 1 MT.TagID, MT.[Rank]
				 FROM @vMatchedTags MT JOIN ContentItemTag CIT ON CIT.TagID = MT.TagID
				 WHERE CIT.ContentItemID = PA.ContentItemId
				 ORDER BY [Rank] DESC) MCT	
WHERE
		PA.HideFromSearch <> 1
	AND (LPT.[Key] IS NOT NULL OR LPO.[Key] IS NOT NULL OR MCT.TagID IS NOT NULL)
	AND CI.StatusID = 3
--		  
UNION
--
SELECT
	CI.ContentItemId,
	CI.CreatedDate,
	CI.ModifiedDate,
	AT.Title,
	CI.FrontEndUrl,
	AT.ShortDescription,
	(select top 1 replace(arp.ProgramContentItemId, '-p', '') from AssetRelatedProgram arp where arp.AssetContentItemId = CI.ContentItemID) RelatedProgramContentItemId,
	ATT.[Rank] AS TitleRank,
	ATO.[Rank] AS OtherRank,
	MCT.[Rank] AS TagRank,
	5 [ContentTypePriority]
FROM
	ContentItem CI
	INNER JOIN ContentItemForSegmentations CIFS WITH (NOLOCK) ON CIFS.ContentItemId = CI.ContentItemId
	JOIN dbo.Asset AT ON AT.ContentItemId = CI.ContentItemID
	LEFT JOIN FREETEXTTABLE(dbo.Asset, (Title), @SearchQuery) ATT ON ATT.[Key] = AT.ContentItemId
	LEFT JOIN FREETEXTTABLE(dbo.Asset, (ShortDescription, LongDescription, BrowseAllMaterialsDisplayText), @SearchQuery) ATO ON ATO.[Key] = AT.ContentItemId
	OUTER APPLY (SELECT TOP 1 MT.TagID, MT.[Rank]
				 FROM @vMatchedTags MT JOIN ContentItemTag CIT ON CIT.TagID = MT.TagID
				 WHERE CIT.ContentItemID = AT.ContentItemId
				 ORDER BY [Rank] DESC) MCT	
WHERE
		(ATT.[Key] IS NOT NULL OR ATO.[Key] IS NOT NULL OR MCT.TagID IS NOT NULL)
	AND CI.StatusID = 3
--		  
UNION
--
SELECT
	CI.ContentItemId,
	CI.CreatedDate,
	CI.ModifiedDate,
	AW.Title,
	CI.FrontEndUrl,
	AW.ShortDescription,
	(select top 1 replace(arp.ProgramContentItemId, '-p', '') from AssetFullWidthRelatedProgram arp where arp.AssetFullWidthContentItemId = CI.ContentItemID) RelatedProgramContentItemId,
	AWT.[Rank] AS TitleRank,
	AWO.[Rank] AS OtherRank,
	MCT.[Rank] AS TagRank,
	6 [ContentTypePriority]
FROM
	ContentItem CI
	INNER JOIN ContentItemForSegmentations CIFS WITH (NOLOCK) ON CIFS.ContentItemId = CI.ContentItemId
	JOIN dbo.AssetFullWidth AW ON AW.ContentItemId = CI.ContentItemID
	LEFT JOIN FREETEXTTABLE(dbo.AssetFullWidth, (Title), @SearchQuery) AWT ON AWT.[Key] = AW.ContentItemId
	LEFT JOIN FREETEXTTABLE(dbo.AssetFullWidth, (ShortDescription, LongDescription), @SearchQuery) AWO ON AWO.[Key] = AW.ContentItemId
	OUTER APPLY (SELECT TOP 1 MT.TagID, MT.[Rank]
				 FROM @vMatchedTags MT JOIN ContentItemTag CIT ON CIT.TagID = MT.TagID
				 WHERE CIT.ContentItemID = AW.ContentItemId
				 ORDER BY [Rank] DESC) MCT
WHERE	
		(AWT.[Key] IS NOT NULL OR AWO.[Key] IS NOT NULL OR MCT.TagID IS NOT NULL)
	AND CI.StatusID = 3
--		  
UNION
--
SELECT
	CI.ContentItemId,
	CI.CreatedDate,
	CI.ModifiedDate,
	DA.Title,
	CI.FrontEndUrl,
	DA.ShortDescription,
	(select top 1 replace(arp.ProgramContentItemId, '-p', '') from DownloadableAssetRelatedProgram arp where arp.DownloadableAssetContentItemId = CI.ContentItemID) RelatedProgramContentItemId,
	DAT.[Rank] AS TitleRank,
	DAO.[Rank] AS OtherRank,
	MCT.[Rank] AS TagRank,
	4 [ContentTypePriority]
FROM
	ContentItem CI
	INNER JOIN ContentItemForSegmentations CIFS WITH (NOLOCK) ON CIFS.ContentItemId = CI.ContentItemId
	JOIN dbo.DownloadableAsset DA ON DA.ContentItemId = CI.ContentItemID
	LEFT JOIN FREETEXTTABLE(dbo.DownloadableAsset, (Title), @SearchQuery) DAT ON DAT.[Key] = DA.ContentItemId
	LEFT JOIN FREETEXTTABLE(dbo.DownloadableAsset, (ShortDescription, LongDescription), @SearchQuery) DAO ON DAO.[Key] = DA.ContentItemId
	OUTER APPLY (SELECT TOP 1 MT.TagID, MT.[Rank]
				 FROM @vMatchedTags MT JOIN ContentItemTag CIT ON CIT.TagID = MT.TagID
				 WHERE CIT.ContentItemID = DA.ContentItemId
				 ORDER BY [Rank] DESC) MCT
WHERE
		(DAT.[Key] IS NOT NULL OR DAO.[Key] IS NOT NULL OR MCT.TagID IS NOT NULL)
	AND CI.StatusID = 3
	AND CI.ContentItemID NOT IN
	((SELECT 
     CASE 
       WHEN orderableassetcontentitemid IS NOT NULL THEN orderableassetcontentitemid
           WHEN downloadableassetcontentitemid IS NOT NULL THEN downloadableassetcontentitemid
           ELSE p.contentitemid 
     END
   FROM (  SELECT
           CASE
                   WHEN printedorderperiodenddate IS NULL THEN onlineorderperiodenddate
                   WHEN onlineorderperiodenddate IS NULL THEN printedorderperiodenddate
                   WHEN printedorderperiodenddate >= onlineorderperiodenddate THEN printedorderperiodenddate
                   WHEN onlineorderperiodenddate >= printedorderperiodenddate THEN onlineorderperiodenddate
           END AS expdate, contentitemid
       FROM program) AS pd
       LEFT JOIN orderableassetrelatedprogram oa ON pd.contentitemid = oa.programcontentitemid
       LEFT JOIN downloadableassetrelatedprogram da ON pd.contentitemid = da.programcontentitemid
       LEFT JOIN program p ON pd.contentitemid = p.contentitemid
   WHERE expdate < GETDATE())
   UNION
   (SELECT pd.contentitemid
   FROM (  SELECT 
           CASE
                   WHEN printedorderperiodenddate IS NULL THEN onlineorderperiodenddate
                   WHEN onlineorderperiodenddate IS NULL THEN printedorderperiodenddate
                   WHEN printedorderperiodenddate >= onlineorderperiodenddate THEN printedorderperiodenddate
                   WHEN onlineorderperiodenddate >= printedorderperiodenddate THEN onlineorderperiodenddate
           END AS expdate, contentitemid
       FROM program) AS pd
   WHERE expdate < GETDATE()))
--		  
UNION
--
SELECT
	CI.ContentItemId,
	CI.CreatedDate,
	CI.ModifiedDate,
	OA.Title,
	CI.FrontEndUrl,
	OA.ShortDescription,
	(select top 1 replace(arp.ProgramContentItemId, '-p', '') from OrderableAssetRelatedProgram arp where arp.OrderableAssetContentItemId = CI.ContentItemID) RelatedProgramContentItemId,
	OAT.[Rank] AS TitleRank,
	OAO.[Rank] AS OtherRank,
	MCT.[Rank] AS TagRank,
	3 [ContentTypePriority]
FROM
	ContentItem CI
	INNER JOIN ContentItemForSegmentations CIFS WITH (NOLOCK) ON CIFS.ContentItemId = CI.ContentItemId
	JOIN dbo.OrderableAsset OA ON OA.ContentItemId = CI.ContentItemID
	LEFT JOIN FREETEXTTABLE(dbo.OrderableAsset, (Title), @SearchQuery) OAT ON OAT.[Key] = OA.ContentItemId
	LEFT JOIN FREETEXTTABLE(dbo.OrderableAsset, (ShortDescription, LongDescription, Sku), @SearchQuery) OAO ON OAO.[Key] = OA.ContentItemId
	OUTER APPLY (SELECT TOP 1 MT.TagID, MT.[Rank]
				 FROM @vMatchedTags MT JOIN ContentItemTag CIT ON CIT.TagID = MT.TagID
				 WHERE CIT.ContentItemID = OA.ContentItemId
				 ORDER BY [Rank] DESC) MCT
WHERE
		(OAT.[Key] IS NOT NULL OR OAO.[Key] IS NOT NULL OR MCT.TagID IS NOT NULL)
	AND CI.StatusID = 3
	AND CI.ContentItemID NOT IN
	((SELECT 
     CASE 
       WHEN orderableassetcontentitemid IS NOT NULL THEN orderableassetcontentitemid
           WHEN downloadableassetcontentitemid IS NOT NULL THEN downloadableassetcontentitemid
           ELSE p.contentitemid 
     END
   FROM (  SELECT
           CASE
                   WHEN printedorderperiodenddate IS NULL THEN onlineorderperiodenddate
                   WHEN onlineorderperiodenddate IS NULL THEN printedorderperiodenddate
                   WHEN printedorderperiodenddate >= onlineorderperiodenddate THEN printedorderperiodenddate
                   WHEN onlineorderperiodenddate >= printedorderperiodenddate THEN onlineorderperiodenddate
           END AS expdate, contentitemid
       FROM program) AS pd
       LEFT JOIN orderableassetrelatedprogram oa ON pd.contentitemid = oa.programcontentitemid
       LEFT JOIN downloadableassetrelatedprogram da ON pd.contentitemid = da.programcontentitemid
       LEFT JOIN program p ON pd.contentitemid = p.contentitemid
   WHERE expdate < GETDATE())
   UNION
   (SELECT pd.contentitemid
   FROM (  SELECT 
           CASE
                   WHEN printedorderperiodenddate IS NULL THEN onlineorderperiodenddate
                   WHEN onlineorderperiodenddate IS NULL THEN printedorderperiodenddate
                   WHEN printedorderperiodenddate >= onlineorderperiodenddate THEN printedorderperiodenddate
                   WHEN onlineorderperiodenddate >= printedorderperiodenddate THEN onlineorderperiodenddate
           END AS expdate, contentitemid
       FROM program) AS pd
   WHERE expdate < GETDATE()))
--
UNION
--
SELECT
	CI.ContentItemId,
	CI.CreatedDate,
	CI.ModifiedDate,
	PR.Title,
	CI.FrontEndUrl,
	PR.ShortDescription,
	null RelatedProgramContentItemId,
	PRT.[Rank] AS TitleRank,
	PRO.[Rank] AS OtherRank,
	MCT.[Rank] AS TagRank,
	1 [ContentTypePriority]
FROM
	ContentItem CI
	INNER JOIN ContentItemForSegmentations CIFS WITH (NOLOCK) ON CIFS.ContentItemId = CI.ContentItemId
	JOIN dbo.Program PR ON PR.ContentItemId = CI.ContentItemID
	LEFT JOIN FREETEXTTABLE(dbo.Program, (Title), @SearchQuery) PRT ON PRT.[Key] = PR.ContentItemId
	LEFT JOIN FREETEXTTABLE(dbo.Program, (ShortDescription, LongDescription), @SearchQuery) PRO ON PRO.[Key] = PR.ContentItemId
	OUTER APPLY (SELECT TOP 1 MT.TagID, MT.[Rank]
				 FROM @vMatchedTags MT JOIN ContentItemTag CIT ON CIT.TagID = MT.TagID
				 WHERE CIT.ContentItemID = PR.ContentItemId
				 ORDER BY [Rank] DESC) MCT
WHERE
		(PRT.[Key] IS NOT NULL OR PRO.[Key] IS NOT NULL OR MCT.TagID IS NOT NULL)
	AND CI.StatusID = 3
--		  
UNION
--
SELECT
	CI.ContentItemId,
	CI.CreatedDate,
	CI.ModifiedDate,
	WE.Title,
	CI.FrontEndUrl,
	WE.ShortDescription,
	null RelatedProgramContentItemId,
	WET.[Rank] AS TitleRank,
	WEO.[Rank] AS OtherRank,
	MCT.[Rank] AS TagRank,
	7 [ContentTypePriority]
FROM
	ContentItem CI
	INNER JOIN ContentItemForSegmentations CIFS WITH (NOLOCK) ON CIFS.ContentItemId = CI.ContentItemId
	JOIN dbo.Webinar WE ON WE.ContentItemId = CI.ContentItemID
	LEFT JOIN FREETEXTTABLE(dbo.Webinar, (Title), @SearchQuery) WET ON WET.[Key] = WE.ContentItemId
	LEFT JOIN FREETEXTTABLE(dbo.Webinar, (ShortDescription, LongDescription, Link), @SearchQuery) WEO ON WEO.[Key] = WE.ContentItemId
	OUTER APPLY (SELECT TOP 1 MT.TagID, MT.[Rank]
				 FROM @vMatchedTags MT JOIN ContentItemTag CIT ON CIT.TagID = MT.TagID
				 WHERE CIT.ContentItemID = WE.ContentItemId
				 ORDER BY [Rank] DESC) MCT
WHERE
		(WET.[Key] IS NOT NULL OR WEO.[Key] IS NOT NULL OR MCT.TagID IS NOT NULL)
	AND CI.StatusID = 3
)
insert into @result
SELECT 
	c.ContentItemId,
	c.CreatedDate,
	c.ModifiedDate,
	c.Title,
	CASE WHEN SUBSTRING(c.FrontEndUrl, 1, 1) = '/' THEN '/portal' + c.FrontEndUrl ELSE '/portal/' + c.FrontEndUrl END AS DisplayUrl,
	'||HTML||' + c.ShortDescription AS [Description],
	c.RelatedProgramContentItemId,
	CAST(0 AS BIT) AS IsLandingPageResult,
	c.ContentTypePriority,
	CAST(0 AS BIT) AS IsContentIndex,
	0 AS [Order],
	c.TitleRank,
	c.TagRank,
	c.OtherRank,
	ISNULL(c.TitleRank + 2000, 0) + ISNULL(c.TagRank + 1000, 0) + ISNULL(c.OtherRank, 0) AS [Rank],
	v.Visits
FROM
	CTE_SEARCH c, ContentItemVisits v
WHERE
	c.ContentItemID = v.ContentItemId;

select r1.*, coalesce(r2.[Rank], r1.[Rank]) [ProgramRank]
from @result r1 left outer join @result r2
	on r1.RelatedProgramContentItemId = r2.ContentItemId
ORDER BY
	[ProgramRank] DESC, r1.ContentTypePriority asc, r1.[Rank] DESC, r1.Visits desc, r1.ModifiedDate desc, r1.CreatedDate desc;

END