﻿CREATE PROCEDURE [dbo].[RetrieveIssuerAssessmentUsageLeadGensReport]
	@StartDate					DATETIME,
	@EndDate					DATETIME,
	@FilterMasterCardUsers	BIT,
	@FilterVendorProcessor		BIT
AS
SELECT		u.FullName AS [User],
			u.[UserName],	
			i.[IssuerID],
			i.[Title] AS [Issuer],
			s.[SegmentationName] AS [AudienceSegments],
			p.[ProcessorID],
			p.[Title] AS [Processor],
			md.[CreatedDate] AS [Date]
FROM		[MailDispatcher] md
			JOIN [User] u ON u.[UserID] = md.[UserId]
			JOIN [Issuer] i ON i.[IssuerID] = u.[IssuerId]
			LEFT JOIN [IssuerSegmentation] iss ON i.[IssuerID] = iss.[IssuerID]
			LEFT JOIN [Segmentation] s ON s.[SegmentationId] = iss.[SegmentationId]
			JOIN [IssuerProcessor] iPp ON ipp.[IssuerID] = i.[IssuerID]
			JOIN [Processor] p ON p.[ProcessorID] = ipp.[ProcessorID]
WHERE		i.[IssuerID] <> 'a05'
			AND i.[RegionId] = 'us'
			AND md.[TrackingType] = 'leadgen'
			AND (md.[MailDispatcherStatusId] = 2 OR md.[MailDispatcherStatusId] = 5)
			AND md.[CreatedDate] >= @StartDate
			AND md.[CreatedDate] < @EndDate
			AND (@FilterMasterCardUsers = 0 OR i.[IsMastercardIssuer] = 0)
			AND (@FilterVendorProcessor = 0 OR p.[ProcessorID] <> '91b')