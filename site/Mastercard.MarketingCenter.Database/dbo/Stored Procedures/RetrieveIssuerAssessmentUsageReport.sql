﻿CREATE PROCEDURE [dbo].[RetrieveIssuerAssessmentUsageReport] @StartDate DATETIME
	,@EndDate DATETIME
	,@FilterMasterCardUsers BIT
	,@FilterVendorProcessor BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--
	DECLARE @Local_StartDate DATETIME = @StartDate;
	DECLARE @Local_EndDate DATETIME = @EndDate;
	DECLARE @Local_FilterMasterCardUsers BIT = @FilterMasterCardUsers;
	DECLARE @Local_FilterVendorProcessor BIT = @FilterVendorProcessor;
	--
	EXEC [dbo].[UpdateSiteTrackingContext] @Local_StartDate;
	--
	SELECT COUNT(DISTINCT ([Issuer])) Issuers
		,COUNT(DISTINCT ([UserName])) Users
		,COUNT(DISTINCT ([VisitID])) Visits
		,COALESCE(SUM([Download]), 0) Downloads
		,COUNT(DISTINCT ([IssuerReport])) IssuerReports
		,COALESCE(SUM([LeadGen]), 0) LeadGens
		,COUNT(DISTINCT ([LeadGenUserId])) LeadGenUniqueUsers
		,COALESCE(SUM([MatchingError]), 0) MatchingErrors
		,COUNT(DISTINCT ([MatchingErrorUserId])) MatchingErrorsUniqueUsers
		,COUNT(DISTINCT ([MatchingErrorIssuerId])) MatchingErrorsUniqueIssuers
	FROM (
		SELECT st.[When] AS [Date]
			,u.FullName AS [User]
			,i.[Title] AS [Issuer]
			,u.[UserName]
			,st.[VisitID]
			,CASE 
				WHEN st.[From] LIKE '/portal/asset/%'
					AND st.[What] LIKE '/portal/file/librarydownload%'
					THEN 1
				ELSE 0
				END AS [Download]
			,CASE 
				WHEN st.[What] LIKE '/portal/assessyourportfolio%'
					AND i.[IssuerID] NOT IN (
						SELECT [IssuerID]
						FROM [IssuerUnmatched]
						WHERE [IssuerID] <> 'a05'
							AND [Date] >= @Local_StartDate
							AND [Date] < @Local_EndDate
						)
					THEN i.[IssuerID]
				ELSE NULL
				END AS [IssuerReport]
			,NULL AS [LeadGenUserId]
			,0 AS [LeadGen]
			,NULL AS [MatchingErrorUserId]
			,NULL AS [MatchingErrorIssuerId]
			,0 AS [MatchingError]
		FROM [vSiteTrackingByContext] st
		LEFT JOIN [User] u ON u.[UserName] = st.[Who]
		LEFT JOIN [Issuer] i ON u.[IssuerId] = i.[IssuerID]
		LEFT JOIN [IssuerProcessor] ip ON ip.[IssuerID] = i.[IssuerID]
		LEFT JOIN [Processor] p ON p.[ProcessorID] = ip.[ProcessorID]
		LEFT JOIN [Issuer] iss ON ip.[IssuerID] = iss.[IssuerID]
		WHERE st.[PageGroup] = 'reportpage'
			AND i.[IssuerID] <> 'a05'
			AND i.[RegionId] = 'us'
			AND st.[When] >= @Local_StartDate
			AND st.[When] < @Local_EndDate
			AND (
				@Local_FilterMasterCardUsers = 0
				OR iss.[IsMastercardIssuer] = 0
				)
			AND (
				@Local_FilterVendorProcessor = 0
				OR p.[ProcessorID] <> '91b'
				)
		
		UNION
		
		SELECT md.[CreatedDate] AS [Date]
			,u.FullName AS [User]
			,i.[Title] AS [Issuer]
			,u.[UserName]
			,NULL AS [VisitID]
			,0 AS [Download]
			,NULL AS [IssuerReport]
			,md.[UserId] AS [LeadGenUserId]
			,1 AS [LeadGen]
			,NULL AS [MatchingErrorUserId]
			,NULL AS [MatchingErrorIssuerId]
			,0 AS [MatchingError]
		FROM [MailDispatcher] md
		LEFT JOIN [User] u ON u.[UserID] = md.[UserId]
		LEFT JOIN [Issuer] i ON u.[IssuerId] = i.[IssuerID]
		LEFT JOIN [IssuerProcessor] ip ON ip.[IssuerID] = i.[IssuerID]
		LEFT JOIN [Processor] p ON p.[ProcessorID] = ip.[ProcessorID]
		WHERE i.[IssuerID] <> 'a05'
			AND i.[RegionId] = 'us'
			AND md.[TrackingType] = 'leadgen'
			AND (
				md.[MailDispatcherStatusId] = 2
				OR md.[MailDispatcherStatusId] = 5
				)
			AND md.[CreatedDate] >= @Local_StartDate
			AND md.[CreatedDate] < @Local_EndDate
			AND (
				@Local_FilterMasterCardUsers = 0
				OR i.[IsMastercardIssuer] = 0
				)
			AND (
				@Local_FilterVendorProcessor = 0
				OR p.[ProcessorID] <> '91b'
				)
		
		UNION
		
		SELECT iu.[Date]
			,u.FullName AS [User]
			,i.[Title] AS [Issuer]
			,u.[UserName]
			,NULL AS [VisitID]
			,0 AS [Download]
			,NULL AS [IssuerReport]
			,NULL AS [LeadGenUserId]
			,0 AS [LeadGen]
			,iu.[UserId] AS [MatchingErrorUserId]
			,iu.[IssuerID] AS [MatchingErrorIssuerId]
			,1 AS [MatchingError]
		FROM [IssuerUnmatched] iu
		LEFT JOIN [User] u ON u.[UserID] = iu.[UserId]
		LEFT JOIN [Issuer] i ON u.[IssuerId] = i.[IssuerID]
		LEFT JOIN [IssuerProcessor] ip ON ip.[IssuerID] = i.[IssuerID]
		LEFT JOIN [Processor] p ON p.[ProcessorID] = ip.[ProcessorID]
		WHERE i.[IssuerID] <> 'a05'
			AND i.[RegionId] = 'us'
			AND iu.[Date] >= @Local_StartDate
			AND iu.[Date] < @Local_EndDate
			AND (
				@Local_FilterMasterCardUsers = 0
				OR i.[IsMastercardIssuer] = 0
				)
			AND (
				@Local_FilterVendorProcessor = 0
				OR p.[ProcessorID] <> '91b'
				)
		) AS T

	SET context_info 0x;
END