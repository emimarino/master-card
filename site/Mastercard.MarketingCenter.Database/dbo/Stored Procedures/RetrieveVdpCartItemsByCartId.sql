﻿CREATE PROCEDURE [dbo].[RetrieveVdpCartItemsByCartId]
(
	@CartId int
)
as
	SELECT	oa.ContentItemId,
			oa.Title, 
            oa.Sku,
			ci.Quantity,
			ps.PricingTable,
			ps.MinimumPrice
	FROM	[CartItem] ci,
            [OrderableAsset] oa,
            [ShoppingCart] sc,
			[PricingSchedule] ps
	where	ci.ContentItemID = oa.ContentItemId and
			ci.CartID = sc.CartID and
			ps.PricingScheduleID = oa.PricingScheduleID and
			sc.CartID = @CartId and 
			sc.DatedCheckedOut is null and
			oa.Vdp = 1  and
			ci.ElectronicDelivery = 0
	order by oa.ContentItemId