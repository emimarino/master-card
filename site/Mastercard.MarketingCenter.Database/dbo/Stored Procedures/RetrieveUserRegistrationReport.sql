﻿CREATE PROCEDURE [dbo].[RetrieveUserRegistrationReport] (
	@StartDate DATETIME
	,@EndDate DATETIME
	,@Region VARCHAR(255)
	,@ExcludeFromDomoApi BIT = 0
	,@IncludeDemoRegion BIT = 0
	,@ExcludeMastercardUsers BIT = 0
	,@ExcludeExpiredUsers BIT = 0
	)
AS
--
DECLARE @StartDate_ DATETIME = @StartDate
DECLARE @EndDate_ DATETIME = @EndDate
DECLARE @Region_ VARCHAR(255) = @Region
DECLARE @ExcludeFromDomoApi_ BIT = @ExcludeFromDomoApi
DECLARE @IncludeDemoRegion_ BIT = @IncludeDemoRegion
DECLARE @ExcludeMastercardUsers_ BIT = @ExcludeMastercardUsers
DECLARE @ExcludeExpiredUsers_ BIT = @ExcludeExpiredUsers;

--
BEGIN
	SET NOCOUNT ON;

	IF object_id('tempdb..#Country', 'U') IS NOT NULL
		DROP TABLE #Country;

	CREATE TABLE #Country (
		Identifier NVARCHAR(255) PRIMARY KEY
		,[Name] NVARCHAR(255)
		);

	INSERT INTO #Country
	SELECT DISTINCT t.Identifier
		,(
			CASE 
				WHEN (
						tt.DisplayName IS NULL
						OR tt.DisplayName = ''
						)
					THEN t.DisplayName
				ELSE tt.DisplayName
				END
			) 'Name'
	FROM Tag t
		,TagTranslatedContent tt
		,TagHierarchyPosition thp
		,TagCategory tc
	WHERE tt.TagId = t.TagID
		AND tt.LanguageCode = 'en'
		AND thp.TagID = t.TagID
		AND thp.TagCategoryID = tc.TagCategoryID
		AND tc.Title = 'Markets'
		AND (
			CASE 
				WHEN (
						tt.DisplayName IS NULL
						OR tt.DisplayName = ''
						)
					THEN t.DisplayName
				ELSE tt.DisplayName
				END
			) IS NOT NULL;

	--
	SELECT u.UserId
		,au.UserName
		,am.LastLoginDate
		,am.CreateDate RegistrationDate
		,up.FirstName
		,up.LastName
		,t.[Name] Country
		,u.[FullName] FullName
		,am.Email
		,i.Title FI
		,COALESCE(p.Title, 'N/A') Processor
		,issuerSegments.AudienceSegments AS AudienceSegments
		,u.Region 'RegionId'
		,[Frequency]
		,u.IsDisabled
		,u.ExcludedFromDomoApi | i.ExcludedFromDomoApi ExcludedFromDomoApi
	FROM aspnet_Membership am
	INNER JOIN aspnet_Users au ON am.UserId = au.UserId
	INNER JOIN [User] u ON 'mastercardmembers:' + au.UserName = u.UserName
	INNER JOIN [UserProfile] up ON u.UserId = up.UserId
	INNER JOIN Issuer i ON u.IssuerId = i.IssuerID
	LEFT JOIN #Country t ON u.Country COLLATE DATABASE_DEFAULT = t.Identifier COLLATE DATABASE_DEFAULT
	LEFT JOIN IssuerProcessor ipp ON i.IssuerID = ipp.IssuerID
	LEFT JOIN Processor p ON ipp.ProcessorID = p.ProcessorID
	LEFT JOIN [UserSubscription] us ON us.UserId = u.UserID
	LEFT JOIN [UserSubscriptionFrequency] usf ON usf.UserSubscriptionFrequencyId = us.UserSubscriptionFrequencyId
	LEFT JOIN (
		SELECT ii.IssuerID AS IssuerID
			,STUFF((
					SELECT ', ' + s.SegmentationName
					FROM Segmentation s
					INNER JOIN IssuerSegmentation iss ON s.SegmentationId = iss.SegmentationId
					WHERE iss.IssuerID = ii.IssuerID
					FOR XML PATH('')
						,TYPE
					).value('(./text())[1]', 'VARCHAR(MAX)'), 1, 2, '') AS AudienceSegments
		FROM Issuer ii
		GROUP BY ii.IssuerID
		) issuerSegments ON i.IssuerID = issuerSegments.IssuerID
	WHERE am.CreateDate >= @StartDate_
		AND am.CreateDate < @EndDate_
		AND (
			@IncludeDemoRegion_ = 1
			OR u.Region != 'demo'
			)
		AND (
			rtrim(ltrim(@Region_)) IS NULL
			OR rtrim(ltrim(@Region_)) = ''
			OR @Region_ = COALESCE(u.Region, 'US')
			)
		AND (
			@ExcludeFromDomoApi_ = 0
			OR (
				i.ExcludedFromDomoApi = 0
				AND u.ExcludedFromDomoApi = 0
				)
			)
		AND (
			@ExcludeMastercardUsers_ = 0
			OR i.IsMastercardIssuer = 0
			)
		AND (
			@ExcludeExpiredUsers_ = 0
			OR u.IsDisabled = 0
			)
	ORDER BY RegistrationDate;
END