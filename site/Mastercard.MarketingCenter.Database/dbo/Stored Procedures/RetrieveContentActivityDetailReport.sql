﻿CREATE PROCEDURE [dbo].[RetrieveContentActivityDetailReport] @ContentItemId VARCHAR(255)
	,@StartDate DATETIME
	,@EndDate DATETIME
	,@AudienceSegmentation VARCHAR(MAX)
	,@FilterMasterCardUsers BIT
	,@FilterVendorProcessor BIT
	,@RegionId VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

	--
	DECLARE @Local_ContentItemId VARCHAR(255) = @ContentItemId;
	DECLARE @Local_StartDate DATETIME = @StartDate;
	DECLARE @Local_EndDate DATETIME = @EndDate;
	DECLARE @Local_AudienceSegmentation VARCHAR(MAX) = @AudienceSegmentation;
	DECLARE @Local_FilterMasterCardUsers BIT = @FilterMasterCardUsers;
	DECLARE @Local_FilterVendorProcessor BIT = @FilterVendorProcessor;
	DECLARE @Local_RegionId VARCHAR(255) = @RegionId;

	--
	EXEC [dbo].[UpdateSiteTrackingContext] @Local_StartDate;

	--
	IF object_id('tempdb..#issuerForSegmentations', 'U') IS NOT NULL
		DROP TABLE #issuerForSegmentations;

	CREATE TABLE #issuerForSegmentations (
		IssuerId VARCHAR(25)
		,Title NVARCHAR(255)
		,AudienceSegments NVARCHAR(255)
		,IsMastercardIssuer BIT
		);

	INSERT INTO #issuerForSegmentations (
		IssuerId
		,Title
		,AudienceSegments
		,IsMastercardIssuer
		)
	SELECT DISTINCT i.IssuerID
		,i.Title
		,s.SegmentationName AS AudienceSegments
		,i.IsMastercardIssuer
	FROM Issuer i WITH (NOLOCK)
	LEFT JOIN IssuerSegmentation iss WITH (NOLOCK) ON i.IssuerID = iss.IssuerID
	LEFT JOIN Segmentation s WITH (NOLOCK) ON s.SegmentationId = iss.SegmentationId
	WHERE iss.IssuerID IS NULL
		OR iss.SegmentationId IN (
			SELECT Token
			FROM Split(@Local_AudienceSegmentation, ',')
			)
		OR @Local_RegionId = 'fmm';

	--
	IF EXISTS (
			SELECT TOP (1) 1
			FROM [Page]
			WHERE (
					ContentItemId = @Local_ContentItemId
					OR @Local_ContentItemId = 'all-pages'
					)
			)
	BEGIN
		SELECT DISTINCT i.AudienceSegments
			,u.UserName
			,u.FullName 'Name'
			,i.IssuerId
			,i.Title Issuer
			,p.ProcessorId
			,coalesce(p.Title, 'N/A') Processor
			,st.[When]
			,st.[From]
			,COALESCE(st.How, 'generic') 'Source'
			,pa.Title
		FROM ContentItem ci
		JOIN [Page] pa ON pa.ContentItemId = ci.ContentItemId
		JOIN vSiteTrackingByContext st ON replace(LEFT(st.what, CHARINDEX('?', st.what + '?') - 1), '/', '') = 'portal' + replace(pa.Uri, '/', '')
		JOIN [User] u ON LOWER(u.UserName) = LOWER(st.Who)
		JOIN #issuerForSegmentations i ON i.IssuerId = u.IssuerId
		LEFT JOIN IssuerProcessor ipp ON ipp.IssuerId = i.IssuerId
		LEFT JOIN Processor p ON p.ProcessorId = ipp.ProcessorId
		WHERE ci.ContentItemId NOT LIKE '%-p'
			AND (
				ci.ContentItemId = @Local_ContentItemId
				OR @Local_ContentItemId = 'all-pages'
				)
			AND st.[When] >= @Local_StartDate
			AND st.[When] < @Local_EndDate
			AND @Local_RegionId = st.Region
			AND i.IssuerId <> 'a05'
			AND (
				(
					p.ProcessorId IS NULL
					AND @Local_RegionId <> 'us'
					)
				OR (
					(
						@Local_FilterMasterCardUsers = 0
						OR i.IsMastercardIssuer = 0
						)
					AND (
						@Local_FilterVendorProcessor = 0
						OR p.ProcessorId <> '91b'
						)
					)
				)
		ORDER BY [When] DESC;
	END

	IF EXISTS (
			SELECT TOP (1) 1
			FROM Program
			WHERE (
					ContentItemId = @Local_ContentItemId
					OR @Local_ContentItemId = 'all-programs'
					)
			)
	BEGIN
		SELECT i.AudienceSegments
			,u.UserName
			,u.FullName 'Name'
			,i.IssuerId
			,i.Title Issuer
			,p.ProcessorId
			,coalesce(p.Title, 'N/A') Processor
			,st.[When]
			,st.[From]
			,COALESCE(st.How, 'generic') 'Source'
			,pr.Title
		FROM ContentItem ci
		JOIN Program pr ON pr.ContentItemId = ci.ContentItemID
		JOIN vSiteTrackingByContext st ON st.ContentItemId = ci.ContentItemId
		JOIN [User] u ON LOWER(u.UserName) = LOWER(st.Who)
		JOIN #issuerForSegmentations i ON i.IssuerId = u.IssuerId
		LEFT JOIN IssuerProcessor ipp ON ipp.IssuerId = i.IssuerId
		LEFT JOIN Processor p ON p.ProcessorId = ipp.ProcessorId
		WHERE ci.ContentItemId NOT LIKE '%-p'
			AND (
				ci.ContentItemId = @Local_ContentItemId
				OR @Local_ContentItemId = 'all-programs'
				)
			AND st.[When] >= @Local_StartDate
			AND st.[When] < @Local_EndDate
			AND @Local_RegionId = st.Region
			AND i.IssuerId <> 'a05'
			AND (
				(
					p.ProcessorId IS NULL
					AND @Local_RegionId <> 'us'
					)
				OR (
					(
						@Local_FilterMasterCardUsers = 0
						OR i.IsMastercardIssuer = 0
						)
					AND (
						@Local_FilterVendorProcessor = 0
						OR p.ProcessorId <> '91b'
						)
					)
				)
		ORDER BY [When] DESC;
	END

	IF EXISTS (
			SELECT TOP (1) 1
			FROM Webinar
			WHERE (
					ContentItemId = @Local_ContentItemId
					OR @Local_ContentItemId = 'all-webinars'
					)
			)
	BEGIN
		SELECT i.AudienceSegments
			,u.UserName
			,u.FullName 'Name'
			,i.IssuerId
			,i.Title Issuer
			,p.ProcessorId
			,coalesce(p.Title, 'N/A') Processor
			,st.[When]
			,st.[From]
			,COALESCE(st.How, 'generic') 'Source'
			,w.Title
		FROM ContentItem ci
		JOIN Webinar w ON w.ContentItemId = ci.ContentItemID
		JOIN vSiteTrackingByContext st ON st.ContentItemId = ci.ContentItemId
		JOIN [User] u ON LOWER(u.UserName) = LOWER(st.Who)
		JOIN #issuerForSegmentations i ON i.IssuerId = u.IssuerId
		LEFT JOIN IssuerProcessor ipp ON ipp.IssuerId = i.IssuerId
		LEFT JOIN Processor p ON p.ProcessorId = ipp.ProcessorId
		WHERE ci.ContentItemId NOT LIKE '%-p'
			AND (
				ci.ContentItemId = @Local_ContentItemId
				OR @Local_ContentItemId = 'all-webinars'
				)
			AND st.[When] >= @Local_StartDate
			AND st.[When] < @Local_EndDate
			AND @Local_RegionId = st.Region
			AND i.IssuerId <> 'a05'
			AND (
				(
					p.ProcessorId IS NULL
					AND @Local_RegionId <> 'us'
					)
				OR (
					(
						@Local_FilterMasterCardUsers = 0
						OR i.IsMastercardIssuer = 0
						)
					AND (
						@Local_FilterVendorProcessor = 0
						OR p.ProcessorId <> '91b'
						)
					)
				)
		ORDER BY [When] DESC;
	END

	IF EXISTS (
			SELECT TOP (1) 1
			FROM Offer
			WHERE (
					ContentItemId = @Local_ContentItemId
					OR @Local_ContentItemId = 'all-offers'
					)
			)
	BEGIN
		SELECT i.AudienceSegments
			,u.UserName
			,u.FullName 'Name'
			,i.IssuerId
			,i.Title Issuer
			,p.ProcessorId
			,coalesce(p.Title, 'N/A') Processor
			,st.[When]
			,st.[From]
			,COALESCE(st.How, 'generic') 'Source'
			,o.Title
		FROM ContentItem ci
		JOIN Offer o ON o.ContentItemId = ci.ContentItemID
		JOIN vSiteTrackingByContext st ON st.ContentItemId = ci.ContentItemId
		JOIN [User] u ON LOWER(u.UserName) = LOWER(st.Who)
		JOIN #issuerForSegmentations i ON i.IssuerId = u.IssuerId
		LEFT JOIN IssuerProcessor ipp ON ipp.IssuerId = i.IssuerId
		LEFT JOIN Processor p ON p.ProcessorId = ipp.ProcessorId
		WHERE ci.ContentItemId NOT LIKE '%-p'
			AND (
				ci.ContentItemId = @Local_ContentItemId
				OR @Local_ContentItemId = 'all-offers'
				)
			AND st.[When] >= @Local_StartDate
			AND st.[When] < @Local_EndDate
			AND @Local_RegionId = st.Region
			AND i.IssuerId <> 'a05'
			AND (
				(
					p.ProcessorId IS NULL
					AND @Local_RegionId <> 'us'
					)
				OR (
					(
						@Local_FilterMasterCardUsers = 0
						OR i.IsMastercardIssuer = 0
						)
					AND (
						@Local_FilterVendorProcessor = 0
						OR p.ProcessorId <> '91b'
						)
					)
				)
		ORDER BY [When] DESC;
	END
	ELSE
	BEGIN
		SELECT i.AudienceSegments
			,u.UserName
			,u.FullName 'Name'
			,i.IssuerId
			,i.Title Issuer
			,p.ProcessorId
			,coalesce(p.Title, 'N/A') Processor
			,st.[When]
			,st.[From]
			,COALESCE(st.How, 'generic') 'Source'
		FROM ContentItem ci
		JOIN vSiteTrackingByContext st ON st.ContentItemId = ci.ContentItemId
		JOIN [User] u ON LOWER(u.UserName) = LOWER(st.Who)
		JOIN #issuerForSegmentations i ON i.IssuerId = u.IssuerId
		LEFT JOIN IssuerProcessor ipp ON ipp.IssuerId = i.IssuerId
		LEFT JOIN Processor p ON p.ProcessorId = ipp.ProcessorId
		WHERE ci.ContentItemId NOT LIKE '%-p'
			AND ci.ContentItemId = @Local_ContentItemId
			AND st.[When] >= @Local_StartDate
			AND st.[When] < @Local_EndDate
			AND @Local_RegionId = st.Region
			AND i.IssuerId <> 'a05'
			AND (
				(
					p.ProcessorId IS NULL
					AND @Local_RegionId <> 'us'
					)
				OR (
					(
						@Local_FilterMasterCardUsers = 0
						OR i.IsMastercardIssuer = 0
						)
					AND (
						@Local_FilterVendorProcessor = 0
						OR p.ProcessorId <> '91b'
						)
					)
				)
		ORDER BY [When] DESC;
	END

	SET context_info 0x;
END;