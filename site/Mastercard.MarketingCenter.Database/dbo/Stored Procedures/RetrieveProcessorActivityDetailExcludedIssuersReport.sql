﻿------ GMC- FI-User login Report
CREATE PROCEDURE [dbo].[RetrieveProcessorActivityDetailExcludedIssuersReport]
	@ProcessorId varchar(255),
	@StartDate datetime,
	@EndDate datetime,
	@Region VARCHAR(255) null
As
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	

	if (@ProcessorId<>'' and @ProcessorId is not null)
		begin
			select count(*) from (
				select	i2.IssuerID, i2.Title Issuer,
					(select	count(u.UserName)
					 from	[User] u,
							Issuer i,
							IssuerProcessor ip,
							aspnet_Membership m,
							aspnet_Users au
					 where	i.IssuerID = ip.IssuerID and
							ip.ProcessorID = p.ProcessorID and
							u.IssuerId = i.IssuerID and
							m.UserId = au.UserId and
							'mastercardmembers:' + au.UserName = u.UserName and
							i.IssuerID = i2.IssuerID and
							m.CreateDate >= @StartDate and
							m.CreateDate < @EndDate  and
							i.RegionId = @Region
					) UserRegistrations,
					(select	count(distinct(u.UserName))
					 from	[User] u,
							Issuer i,
							IssuerProcessor ip,
							LoginTracking lt
					 where	i.IssuerID = ip.IssuerID and
							ip.ProcessorID = p.ProcessorID and
							u.IssuerId = i.IssuerID and
							lt.UserName = u.UserName and
							i.IssuerID = i2.IssuerID and
							lt.Date >= @StartDate and
							lt.Date < @EndDate  and
							i.RegionId = @Region
					) ActiveUsers,
					(select	count(u.UserName)
					 from	[User] u,
							Issuer i,
							IssuerProcessor ip,
							LoginTracking lt
					 where	i.IssuerID = ip.IssuerID and
							ip.ProcessorID = p.ProcessorID and
							u.IssuerId = i.IssuerID and
							lt.UserName = u.UserName and
							i.IssuerID = i2.IssuerID and
							lt.Date >= @StartDate and
							lt.Date < @EndDate	 and
							i.RegionId = @Region
					) UserLogins
				from	Processor p, IssuerProcessor ip, Issuer i2
				where p.ProcessorID = ip.ProcessorID and ip.IssuerID = i2.IssuerID and p.ProcessorID = @ProcessorId) t
				where userregistrations = 0 and activeusers = 0 and userlogins = 0
		end

	else
		begin
			select count(*) from (
				select	i2.IssuerID, i2.Title Issuer,
					(select	count(u.UserName)
					 from	[User] u,
							Issuer i,
							aspnet_Membership m,
							aspnet_Users au
					 where	u.IssuerId = i.IssuerID and
							m.UserId = au.UserId and
							'mastercardmembers:' + au.UserName = u.UserName and
							i.IssuerID = i2.IssuerID and
							m.CreateDate >= @StartDate and
							m.CreateDate < @EndDate   and
							i.RegionId = @Region
					) UserRegistrations,
					(select	count(distinct(u.UserName))
					 from	[User] u,
							Issuer i,
							LoginTracking lt
					 where	u.IssuerId = i.IssuerID and
							lt.UserName = u.UserName and
							i.IssuerID = i2.IssuerID and
							lt.Date >= @StartDate and
							lt.Date < @EndDate    and
							i.RegionId = @Region
					) ActiveUsers,
					(select	count(u.UserName)
					 from	[User] u,
							Issuer i,
							LoginTracking lt
					 where	u.IssuerId = i.IssuerID and
							lt.UserName = u.UserName and
							i.IssuerID = i2.IssuerID and
							lt.Date >= @StartDate and
							lt.Date < @EndDate	and
							i.RegionId = @Region
					) UserLogins
				from	 Issuer i2
				where  i2.RegionId=@Region
				 ) t
				where userregistrations = 0 and activeusers = 0 and userlogins = 0
		end
END