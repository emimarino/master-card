﻿CREATE PROCEDURE [dbo].[RetrieveContentActivityForOfferReport] @StartDate DATETIME
	,@EndDate DATETIME
	,@AudienceSegmentation VARCHAR(MAX)
	,@FilterMasterCardUsers BIT
	,@FilterVendorProcessor BIT
	,@RegionId VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

	--
	DECLARE @Local_StartDate DATETIME = @StartDate;
	DECLARE @Local_EndDate DATETIME = @EndDate;
	DECLARE @Local_AudienceSegmentation VARCHAR(MAX) = @AudienceSegmentation;
	DECLARE @Local_FilterMasterCardUsers BIT = @FilterMasterCardUsers;
	DECLARE @Local_FilterVendorProcessor BIT = @FilterVendorProcessor;
	DECLARE @Local_RegionId VARCHAR(255) = @RegionId;

	--
	EXEC [dbo].[UpdateSiteTrackingContext] @Local_StartDate;

	--
	IF object_id('tempdb..#issuerForSegmentations', 'U') IS NOT NULL
		DROP TABLE #issuerForSegmentations;

	CREATE TABLE #issuerForSegmentations (
		IssuerId VARCHAR(25)
		,Title NVARCHAR(255)
		,AudienceSegments NVARCHAR(255)
		,IsMastercardIssuer BIT
		);

	INSERT INTO #issuerForSegmentations (
		IssuerId
		,Title
		,AudienceSegments
		,IsMastercardIssuer
		)
	SELECT DISTINCT i.IssuerID
		,i.Title
		,s.SegmentationName AS AudienceSegments
		,i.IsMastercardIssuer
	FROM Issuer i WITH (NOLOCK)
	LEFT JOIN IssuerSegmentation iss WITH (NOLOCK) ON i.IssuerID = iss.IssuerID
	LEFT JOIN Segmentation s WITH (NOLOCK) ON s.SegmentationId = iss.SegmentationId
	WHERE iss.IssuerID IS NULL
		OR iss.SegmentationId IN (
			SELECT Token
			FROM Split(@Local_AudienceSegmentation, ',')
			)
		OR @Local_RegionId = 'fmm';

	--
	SELECT ci.ContentItemID
		,o.Title
		,COUNT(DISTINCT (p.ProcessorID)) Processors
		,COUNT(DISTINCT (i.IssuerID)) Issuers
		,COUNT(DISTINCT (u.UserID)) Users
		,COUNT(o.Title) 'TotalHits'
		,COALESCE(SUM(generic.c), 0) + COALESCE(SUM(thumbnail.c), 0) + COALESCE(SUM(back.c), 0) 'Generic'
		,COALESCE(SUM(relateditem.c), 0) + COALESCE(SUM(thumbnailrelateditem.c), 0) 'RelatedItem'
		,COALESCE(SUM(back.c), 0) 'Back'
		,COALESCE(SUM(subscriptiondaily.c), 0) + COALESCE(SUM(subscriptionweekly.c), 0) 'Email'
		,COALESCE(SUM(mostpopular.c), 0) 'MostPopular'
	FROM Offer o
	INNER JOIN vSiteTrackingByContext st ON st.ContentItemID = o.ContentItemId
	INNER JOIN [User] u ON LOWER(u.UserName) = LOWER(st.Who)
	INNER JOIN #issuerForSegmentations i ON i.IssuerID = u.IssuerId
	LEFT JOIN IssuerProcessor ip ON ip.IssuerID = i.IssuerID
	LEFT JOIN Processor p ON p.ProcessorID = ip.ProcessorID
	INNER JOIN ContentItem ci ON ci.ContentItemID = o.ContentItemId
	INNER JOIN ContentType ct ON ct.ContentTypeID = ci.ContentTypeID
	LEFT JOIN (
		SELECT 1 c
			,'generic' generic
		) generic ON COALESCE(st.How, 'generic') = generic.generic
	LEFT JOIN (
		SELECT 1 c
			,'related-item' relateditem
		) relateditem ON COALESCE(st.How, 'generic') = relateditem.relateditem
	LEFT JOIN (
		SELECT 1 c
			,'thumbnail related-item' thumbnailrelateditem
		) thumbnailrelateditem ON COALESCE(st.How, 'generic') = thumbnailrelateditem.thumbnailrelateditem
	LEFT JOIN (
		SELECT 1 c
			,'thumbnail' thumbnail
		) thumbnail ON COALESCE(st.How, 'generic') = thumbnail.thumbnail
	LEFT JOIN (
		SELECT 1 c
			,'back' back
		) back ON COALESCE(st.How, 'generic') = back.back
	LEFT JOIN (
		SELECT 1 c
			,'subscriptiondaily' subscriptiondaily
		) subscriptiondaily ON COALESCE(st.How, 'generic') LIKE (
			CONCAT (
				subscriptiondaily.subscriptiondaily
				,'%'
				)
			)
	LEFT JOIN (
		SELECT 1 c
			,'subscriptionweekly' subscriptionweekly
		) subscriptionweekly ON COALESCE(st.How, 'generic') LIKE (
			CONCAT (
				subscriptionweekly.subscriptionweekly
				,'%'
				)
			)
	LEFT JOIN (
		SELECT 1 c
			,'mostpopular' mostpopular
		) mostpopular ON COALESCE(st.How, 'generic') = mostpopular.mostpopular
	WHERE i.IssuerID <> 'a05'
		AND st.[When] >= @Local_StartDate
		AND st.[When] < @Local_EndDate
		AND st.Region = @Local_RegionId
		AND (
			(
				p.ProcessorID IS NULL
				AND @Local_RegionId <> 'us'
				)
			OR (
				(
					@Local_FilterMasterCardUsers = 0
					OR i.IsMastercardIssuer = 0
					)
				AND (
					@Local_FilterVendorProcessor = 0
					OR p.ProcessorID <> '91b'
					)
				)
			)
		AND ci.ContentItemID NOT LIKE '%-p'
	GROUP BY ci.ContentItemID
		,ct.Title
		,o.Title
	ORDER BY o.Title;

	SET context_info 0x;
END;