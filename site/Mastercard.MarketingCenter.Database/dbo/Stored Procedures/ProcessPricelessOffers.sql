﻿CREATE PROCEDURE [ProcessPricelessOffers] @businessOwnerEmail VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @startDate DATETIME = SYSDATETIME();
	DECLARE @nextId VARCHAR(10);
	DECLARE @nextSeedId INT;
	DECLARE @offerId VARCHAR(10);
	DECLARE @offersCreated INT = 0;
	DECLARE @offersUpdated INT = 0;
	DECLARE @offersDeleted INT = 0;
	DECLARE @offersUnmatched INT = 0;
	DECLARE @offersCreatedTable TABLE (
		OfferId VARCHAR(25) NOT NULL
		,PricelessOfferId NVARCHAR(MAX) NOT NULL
		);
	DECLARE @offersUpdatedTable TABLE (
		OfferId VARCHAR(25) NOT NULL
		,PricelessOfferId NVARCHAR(MAX) NOT NULL
		);
	DECLARE @offersDeletedTable TABLE (
		OfferId VARCHAR(25) NOT NULL
		,PricelessOfferId NVARCHAR(MAX) NOT NULL
		);
	DECLARE @offersUnmatchedTable TABLE (
		PricelessOfferId NVARCHAR(MAX) NOT NULL
		,PricelessField NVARCHAR(MAX) NOT NULL
		,PricelessValue NVARCHAR(MAX) NOT NULL
		);
	DECLARE @continue INT = 1;
	DECLARE @productId NVARCHAR(MAX);
	DECLARE @title NVARCHAR(MAX);
	DECLARE @evergreen INT = 1;
	DECLARE @businessOwnerId INT = 1917 -- System Admin;
	DECLARE @validityPeriodFromDate DATETIME = NULL;
	DECLARE @validityPeriodToDate DATETIME = NULL;
	DECLARE @eventDate DATETIME;
	DECLARE @bookByDate DATETIME;
	DECLARE @mainImage NVARCHAR(MAX);
	DECLARE @logoImage NVARCHAR(MAX);
	DECLARE @secondaryImage NVARCHAR(MAX);
	DECLARE @shortDescription NVARCHAR(MAX);
	DECLARE @longDescription NVARCHAR(MAX);
	DECLARE @termsAndConditions NVARCHAR(MAX);
	DECLARE @pricelessCategoryId NVARCHAR(MAX);
	DECLARE @programName NVARCHAR(MAX);
	DECLARE @pricelessCardExclusivity NVARCHAR(MAX);
	DECLARE @pricelessCardExclusivityTable TABLE (PricelessCardExclusivity NVARCHAR(MAX) NOT NULL);
	DECLARE @pricelessCardExclusivityUnmatchedTable TABLE (PricelessCardExclusivity NVARCHAR(MAX) NOT NULL);
	DECLARE @pricelessEventCity NVARCHAR(MAX);
	DECLARE @productUrl NVARCHAR(MAX);

	BEGIN TRY
		IF EXISTS (
				SELECT TOP 1 1
				FROM [User]
				WHERE [Email] = @businessOwnerEmail
				)
		BEGIN
			SELECT TOP 1 @businessOwnerId = [UserId]
			FROM [User]
			WHERE [Email] = @businessOwnerEmail;
		END

		WHILE @continue = 1
		BEGIN
			SELECT TOP 1 @productId = [ProductId]
				,@title = [DisplayName]
				,@mainImage = [ImageUrl]
				,@logoImage = [CelebImageUrl]
				,@secondaryImage = [AltImageUrl]
				,@shortDescription = [Description]
				,@longDescription = [LongDescription]
				,@termsAndConditions = [FinePrint]
				,@pricelessCategoryId = [CatId]
				,@programName = [ProgramName]
				,@pricelessCardExclusivity = [CardExclusivity]
				,@pricelessEventCity = [EventCity]
				,@productUrl = [ProductUrl]
				,@eventDate = CASE 
					WHEN [IsEvent] = 1
						THEN [EventTime]
					ELSE NULL
					END
				,@bookByDate = CASE 
					WHEN ISDATE([EndDate]) = 1
						THEN [EndDate]
					ELSE NULL
					END
			FROM [dbo].[ImportedOffer]
			WHERE [IsLastImportedVersion] = 1
				AND [IsProcessed] = 0
			ORDER BY [ProductId];

			IF @@ROWCOUNT > 0
			BEGIN
				BEGIN TRANSACTION

				DELETE
				FROM @pricelessCardExclusivityTable;

				INSERT INTO @pricelessCardExclusivityTable
				SELECT Token
				FROM Split(@pricelessCardExclusivity, '","')
				WHERE Token NOT IN (
						'['
						,']'
						,','
						,''
						);

				DELETE
				FROM @pricelessCardExclusivityUnmatchedTable;

				INSERT INTO @pricelessCardExclusivityUnmatchedTable
				SELECT pce.[PricelessCardExclusivity]
				FROM [dbo].[CardExclusivity] ce
				RIGHT JOIN @pricelessCardExclusivityTable pce ON ce.PricelessCardExclusivity COLLATE SQL_Latin1_General_CP1_CI_AS = pce.PricelessCardExclusivity COLLATE SQL_Latin1_General_CP1_CI_AS
				WHERE ce.PricelessCardExclusivity IS NULL;

				IF (
						@title != ''
						AND @mainImage != ''
						AND @shortDescription != ''
						AND @longDescription != ''
						AND @pricelessCategoryId != ''
						AND EXISTS (
							SELECT 1
							FROM [dbo].[Category]
							WHERE [PricelessCategoryId] = @pricelessCategoryId
							)
						AND @programName != ''
						AND EXISTS (
							SELECT 1
							FROM [dbo].[OfferType]
							WHERE [PricelessProgramName] = @programName
							)
						AND (
							@pricelessCardExclusivity = ''
							OR NOT EXISTS (
								SELECT 1
								FROM @pricelessCardExclusivityUnmatchedTable
								)
							)
						AND (
							@pricelessEventCity = ''
							OR EXISTS (
								SELECT 1
								FROM [dbo].[EventLocation]
								WHERE [PricelessEventCity] = @pricelessEventCity
								)
							)
						)
				BEGIN
					IF NOT EXISTS (
							SELECT 1
							FROM [dbo].[Offer]
							WHERE [PricelessOfferId] = @productId
							)
					BEGIN
						SELECT @nextSeedId = ([IdSeed] + 1)
						FROM [dbo].[IdSeed]

						SELECT @nextId = LOWER(CONVERT(VARCHAR, CONVERT(VARBINARY(2), @nextSeedId), 2))

						UPDATE [dbo].[IdSeed]
						SET [IdSeed] = @nextSeedId

						INSERT INTO [dbo].[ContentItem] (
							[ContentItemID]
							,[PrimaryContentItemID]
							,[ContentTypeID]
							,[FrontEndUrl]
							,[StatusID]
							,[CreatedByUserID]
							,[ModifiedByUserID]
							,[CreatedDate]
							,[ModifiedDate]
							,[RegionId]
							,[ExpirationDate]
							,[ReviewStateId]
							)
						VALUES (
							@nextId
							,@nextId
							,'o' -- Offer
							,'/offer/' + @nextId + '/' + @productId
							,3 -- Published
							,1917 -- System Admin
							,1917 -- System Admin
							,GETDATE()
							,GETDATE()
							,'fmm' -- Global region
							,NULL -- No Expiration Date
							,0
							)

						INSERT INTO [dbo].[Offer] (
							[ContentItemId]
							,[Title]
							,[BusinessOwnerID]
							,[Evergreen]
							,[ValidityPeriodFromDate]
							,[ValidityPeriodToDate]
							,[EventDate]
							,[BookByDate]
							,[MainImage]
							,[ShortDescription]
							,[LongDescription]
							,[TermsAndConditions]
							,[PreviewFiles]
							,[DownloadZipFile]
							,[PricelessOfferId]
							,[LogoImage]
							,[OfferTypeId]
							,[ProductUrl]
							)
						VALUES (
							@nextId
							,@title
							,@businessOwnerId
							,@evergreen
							,@validityPeriodFromDate
							,@validityPeriodToDate
							,@eventDate
							,@bookByDate
							,@mainImage
							,@shortDescription
							,@longDescription
							,@termsAndConditions
							,0
							,''
							,@productId
							,@logoImage
							,(SELECT TOP 1 [OfferTypeId] FROM [dbo].[OfferType] WHERE [PricelessProgramName] = @programName)
							,@productUrl
							)

						INSERT INTO [dbo].[ContentItemTag] (
							[ContentItemID]
							,[TagID]
							)
						SELECT @nextId
							,[TagId]
						FROM [dbo].[Region]
						WHERE [Id] NOT IN (
								'global' -- Global Settings region
								)

						INSERT INTO [dbo].[ContentItemCrossBorderRegion] (
							[ContentItemId]
							,[RegionId]
							)
						SELECT @nextId
							,[Id]
						FROM [dbo].[Region]
						WHERE [Id] NOT IN (
								'fmm' -- Global region
								,'global' -- Global Settings region
								)

						INSERT INTO [dbo].[ContentItemRegionVisibility] (
							[ContentItemId]
							,[RegionId]
							,[Value]
							)
						VALUES (
							@nextId
							,'fmm' -- Global region
							,0 -- No
							)

						INSERT INTO [dbo].[ContentItemRegionVisibility] (
							[ContentItemId]
							,[RegionId]
							,[Value]
							)
						SELECT @nextId
							,[Id]
							,2 -- Pending
						FROM [dbo].[Region]
						WHERE [Id] NOT IN (
								'fmm' -- Global region
								,'global' -- Global Settings region
								)

						SET @offersCreated = @offersCreated + 1

						INSERT INTO @offersCreatedTable
						VALUES (
							@nextId
							,@productId
							);
					END
					ELSE
					BEGIN
						SELECT TOP 1 @offerId = [ContentItemId]
						FROM [dbo].[Offer]
						WHERE [PricelessOfferId] = @productId;

						UPDATE [dbo].[ContentItem]
						SET [StatusID] = 3 -- Published
							,[ModifiedByUserID] = 1917 -- System Admin
							,[ModifiedDate] = GETDATE()
						WHERE [ContentItemID] = @offerId

						UPDATE [dbo].[Offer]
						SET [Title] = @title
							,[Evergreen] = @evergreen
							,[ValidityPeriodFromDate] = @validityPeriodFromDate
							,[ValidityPeriodToDate] = @validityPeriodToDate
							,[EventDate] = @eventDate
							,[BookByDate] = @bookByDate
							,[MainImage] = @mainImage
							,[ShortDescription] = @shortDescription
							,[LongDescription] = @longDescription
							,[TermsAndConditions] = @termsAndConditions
							,[LogoImage] = @logoImage
							,[OfferTypeId] = (SELECT TOP 1 [OfferTypeId] FROM [dbo].[OfferType] WHERE [PricelessProgramName] = @programName)
							,[ProductUrl] = @productUrl
						WHERE [ContentItemID] = @offerId

						SET @offersUpdated = @offersUpdated + 1

						INSERT INTO @offersUpdatedTable
						VALUES (
							@offerId
							,@productId
							);
					END

					SELECT TOP 1 @offerId = [ContentItemId]
					FROM [dbo].[Offer]
					WHERE [PricelessOfferId] = @productId;

					DELETE
					FROM [dbo].[ContentItemCategory]
					WHERE [ContentItemId] = @offerId;

					INSERT INTO [dbo].[ContentItemCategory] (
						[ContentItemId]
						,[CategoryId]
						)
					VALUES (
						@offerId
						,(
							SELECT TOP 1 [CategoryId]
							FROM [dbo].[Category]
							WHERE [PricelessCategoryId] = @pricelessCategoryId
							)
						);

					DELETE
					FROM [dbo].[ContentItemCardExclusivity]
					WHERE [ContentItemId] = @offerId;

					IF (
							@pricelessCardExclusivity != ''
							AND NOT EXISTS (
								SELECT 1
								FROM @pricelessCardExclusivityUnmatchedTable
								)
							)
					BEGIN
						INSERT INTO [dbo].[ContentItemCardExclusivity] (
							[ContentItemId]
							,[CardExclusivityId]
							)
						SELECT @offerId
							,[CardExclusivityId]
						FROM [dbo].[CardExclusivity] ce
						JOIN @pricelessCardExclusivityTable pce ON ce.[PricelessCardExclusivity] COLLATE SQL_Latin1_General_CP1_CI_AS = pce.[PricelessCardExclusivity] COLLATE SQL_Latin1_General_CP1_CI_AS
					END

					DELETE
					FROM [dbo].[ContentItemEventLocation]
					WHERE [ContentItemId] = @offerId;

					IF (
							@pricelessEventCity != ''
							AND EXISTS (
								SELECT 1
								FROM [dbo].[EventLocation]
								WHERE [PricelessEventCity] = @pricelessEventCity
								)
							)
					BEGIN
						INSERT INTO [dbo].[ContentItemEventLocation] (
							[ContentItemId]
							,[EventLocationId]
							)
						VALUES (
							@offerId
							,(
								SELECT TOP 1 [EventLocationId]
								FROM [dbo].[EventLocation]
								WHERE [PricelessEventCity] = @pricelessEventCity
								)
							);
					END

					DELETE
					FROM [dbo].[OfferSecondaryImage]
					WHERE [ContentItemId] = @offerId;

					IF (@secondaryImage != '' AND CHARINDEX(',', @secondaryImage) > 0)
					BEGIN
						INSERT INTO [dbo].[OfferSecondaryImage] (
							[ContentItemId]
							,[ImageUrl]
							)
						SELECT @offerId
							,Token
						FROM Split(SUBSTRING(@secondaryImage, CHARINDEX(',', @secondaryImage) + 1, LEN(@secondaryImage)), ',');
					END
				END
				ELSE
				BEGIN
					SET @offersUnmatched = @offersUnmatched + 1;

					IF @title = ''
						INSERT INTO @offersUnmatchedTable
						VALUES (
							@productId
							,'Title'
							,''
							);

					IF @mainImage = ''
						INSERT INTO @offersUnmatchedTable
						VALUES (
							@productId
							,'ImageUrl'
							,''
							);

					IF @shortDescription = ''
						INSERT INTO @offersUnmatchedTable
						VALUES (
							@productId
							,'Description'
							,''
							);

					IF @longDescription = ''
						INSERT INTO @offersUnmatchedTable
						VALUES (
							@productId
							,'LongDescription'
							,''
							);

					IF (
							@pricelessCategoryId = ''
							OR NOT EXISTS (
								SELECT 1
								FROM [dbo].[Category]
								WHERE [PricelessCategoryId] = @pricelessCategoryId
								)
							)
						INSERT INTO @offersUnmatchedTable
						VALUES (
							@productId
							,'CatId'
							,@pricelessCategoryId
							);

					IF (
							@programName = ''
							OR NOT EXISTS (
								SELECT 1
								FROM [dbo].[OfferType]
								WHERE [PricelessProgramName] = @programName
								)
							)
						INSERT INTO @offersUnmatchedTable
						VALUES (
							@productId
							,'ProgramName'
							,@programName
							);

					IF (
							@pricelessCardExclusivity != ''
							AND EXISTS (
								SELECT 1
								FROM @pricelessCardExclusivityUnmatchedTable
								)
							)
						INSERT INTO @offersUnmatchedTable
						SELECT @productId
							,'CardExclusivity'
							,[PricelessCardExclusivity]
						FROM @pricelessCardExclusivityUnmatchedTable;

					IF (
							@pricelessEventCity != ''
							AND NOT EXISTS (
								SELECT 1
								FROM [dbo].[EventLocation]
								WHERE [PricelessEventCity] = @pricelessEventCity
								)
							)
						INSERT INTO @offersUnmatchedTable
						VALUES (
							@productId
							,'EventCity'
							,@pricelessEventCity
							);
				END

				UPDATE [dbo].[ImportedOffer]
				SET [IsProcessed] = 1
				WHERE [IsLastImportedVersion] = 1
					AND [IsProcessed] = 0
					AND [ProductId] = @productId

				COMMIT;
			END
			ELSE
			BEGIN
				SET @continue = 0
			END
		END

		BEGIN TRANSACTION

		UPDATE ci
		SET [StatusID] = 8 -- Deleted
			,[ModifiedByUserID] = 1917 -- System Admin
			,[ModifiedDate] = GETDATE()
		OUTPUT inserted.[ContentItemID]
			,o.[PricelessOfferId]
		INTO @offersDeletedTable
		FROM [dbo].[ContentItem] ci
		JOIN [dbo].[Offer] o ON ci.[ContentItemID] = o.[ContentItemId]
		WHERE o.[PricelessOfferId] IS NOT NULL
			AND [StatusID] != 8
			AND [PricelessOfferId] NOT IN (
				SELECT [ProductId]
				FROM [dbo].[ImportedOffer]
				WHERE [IsLastImportedVersion] = 1
				)

		SET @offersDeleted = @@ROWCOUNT;

		COMMIT;
	END TRY

	BEGIN CATCH
		IF XACT_STATE() <> 0
			ROLLBACK;

		PRINT 'ERROR in ProcessPricelessOffers';

		THROW;
	END CATCH

	INSERT INTO [dbo].[PricelessOffersProcess] (
		[ProcessStartDate]
		,[ProcessEndDate]
		,[OffersCreated]
		,[OffersUpdated]
		,[OffersDeleted]
		,[OffersUnmatched]
		,[OffersCreatedData]
		,[OffersUpdatedData]
		,[OffersDeletedData]
		,[OffersUnmatchedData]
		)
	VALUES (
		@startDate
		,SYSDATETIME()
		,@offersCreated
		,@offersUpdated
		,@offersDeleted
		,@offersUnmatched
		,CASE 
			WHEN EXISTS (
					SELECT TOP 1 1
					FROM @offersCreatedTable
					)
				THEN (
						SELECT *
						FROM @offersCreatedTable
						FOR XML RAW('PricelessOfferCreated')
							,ELEMENTS
							,ROOT('OffersCreated')
						)
			ELSE ''
			END
		,CASE 
			WHEN EXISTS (
					SELECT TOP 1 1
					FROM @offersUpdatedTable
					)
				THEN (
						SELECT *
						FROM @offersUpdatedTable
						FOR XML RAW('PricelessOfferUpdated')
							,ELEMENTS
							,ROOT('OffersUpdated')
						)
			ELSE ''
			END
		,CASE 
			WHEN EXISTS (
					SELECT TOP 1 1
					FROM @offersDeletedTable
					)
				THEN (
						SELECT *
						FROM @offersDeletedTable
						FOR XML RAW('PricelessOfferDeleted')
							,ELEMENTS
							,ROOT('OffersDeleted')
						)
			ELSE ''
			END
		,CASE 
			WHEN EXISTS (
					SELECT TOP 1 1
					FROM @offersUnmatchedTable
					)
				THEN (
						SELECT *
						FROM @offersUnmatchedTable
						FOR XML RAW('PricelessOfferUnmatched')
							,ELEMENTS
							,ROOT('OffersUnmatched')
						)
			ELSE ''
			END
		)
END;