﻿
CREATE PROCEDURE  [dbo].[RetrieveShippingInformationForOrderApproval]
(
	@OrderID int = null
)
as

	SELECT	distinct(SI.ShippingInformationID),
			SI.ContactName,
			SI.Phone, 
			SI.[Address],
			SI.OrderTrackingNumber
	FROM    [ShippingInformation] SI,
			[OrderItem] oi
	where	SI.ShippingInformationID = oi.ShippingInformationID and
			oi.OrderID = @OrderID and
			oi.ApproverEmailAddress is not null
	order by SI.ShippingInformationID
