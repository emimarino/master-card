﻿CREATE PROCEDURE [dbo].[RetrieveShoppingCartItemsForShippingInformation]
(
	@ShippingInformationID int
)
as
	SELECT	oa.Title, 
            oa.Sku,
            oa.ContentItemId,
            ci.Quantity
	FROM	[CartItem] ci,
            [OrderableAsset] oa,
            [ShippingInformation] si
	where	ci.ContentItemID = oa.ContentItemId and
			ci.ShippingInformationID = si.ShippingInformationID and
			ci.ShippingInformationID = @ShippingInformationID and
			ci.ElectronicDelivery = 0 and
			oa.Vdp = 0
	order by oa.ContentItemId