﻿CREATE PROCEDURE [dbo].[UpdateSiteTrackingContext] @StartDate DATETIME
	,@ResetContext BIT = 0
AS
BEGIN
	SET NOCOUNT ON;

	IF (@ResetContext = 0)
	BEGIN
		/**************************************************************************/
		/*       Choosing what table to query:                                    */
		/*             [dbo].[SiteTracking] or [Archive].[SiteTracking]           */
		/*       0x2128506 - a "magic value" from vSiteTrackingByContext          */
		/**************************************************************************/
		DECLARE @Local_StartDate DATETIME = @StartDate;
		DECLARE @minDT DATETIME;

		SELECT @minDT = min(st.[When])
		FROM dbo.SiteTracking AS st;

		IF (@Local_StartDate >= @minDT)
		BEGIN
			-- query [dbo].[SiteTracking]
			SET context_info 0x2128506;
		END
		ELSE
		BEGIN
			-- query [Archive].[SiteTracking]
			SET context_info 0x;
		END;
	END;
	ELSE
	BEGIN
		SET context_info 0x;
	END;
END;