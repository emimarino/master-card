﻿CREATE PROCEDURE [dbo].[RetrieveOrderingActivityForNonProgramReport] @StartDate DATETIME
	,@EndDate DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	--
	DECLARE @Local_StartDate DATETIME = @StartDate;
	DECLARE @Local_EndDate DATETIME = @EndDate;

	--
	EXEC [dbo].[UpdateSiteTrackingContext] @Local_StartDate;

	--
	IF object_id('tempdb..#issuerForSegmentations', 'U') IS NOT NULL
		DROP TABLE #issuerForSegmentations;

	CREATE TABLE #issuerForSegmentations (
		IssuerId VARCHAR(25)
		,Title NVARCHAR(255)
		,AudienceSegments NVARCHAR(255)
		,IsMastercardIssuer BIT
		);

	INSERT INTO #issuerForSegmentations (
		IssuerId
		,Title
		,AudienceSegments
		,IsMastercardIssuer
		)
	SELECT DISTINCT i.IssuerID
		,i.Title
		,s.SegmentationName AS AudienceSegments
		,i.IsMastercardIssuer
	FROM Issuer i WITH (NOLOCK)
	LEFT JOIN IssuerSegmentation iss WITH (NOLOCK) ON i.IssuerID = iss.IssuerID
	LEFT JOIN Segmentation s WITH (NOLOCK) ON s.SegmentationId = iss.SegmentationId;

	WITH Downloads
	AS (
		SELECT [When]
			,Who
			,ContentItemID
		FROM (
			SELECT *
			FROM vSiteTrackingByContext
			WHERE [From] LIKE '/portal/asset/%'
				AND What LIKE '/portal/file/librarydownload%'
			) tempdownloads
		)
	--
	SELECT RelatedProgramID
		,RelatedProgram
		,COUNT(DISTINCT (Processor)) Processors
		,COUNT(DISTINCT (FinancialInstitution)) FIs
		,COUNT(DISTINCT (UserName)) Users
		,SUM(Download) Downloads
		,SUM(ED) EDOrders
		,SUM(POD) PODOrders
		,SUM(ItemPrice) TotalCostOrdered
	FROM (
		SELECT [When] AS DATE
			,FullName AS Requestor
			,p.Title AS Processor
			,i.Title AS 'FinancialInstitution'
			,u.UserName
			,da.Title AS 'ItemName'
			,'Download' AS 'ItemType'
			,NULL AS SKU
			,NULL AS Fulfillment
			,0 AS 'ItemPrice'
			,0 AS 'ItemQuantity'
			,NULL AS 'OrderStatus'
			,pr.ContentItemId AS RelatedProgramID
			,pr.Title AS RelatedProgram
			,0 AS ED
			,0 AS POD
			,1 AS Download
		FROM Downloads d
		JOIN DownloadableAsset da ON d.ContentItemID = da.ContentItemId
		LEFT JOIN DownloadableAssetRelatedProgram darp ON da.ContentItemId = darp.DownloadableAssetContentItemId
		LEFT JOIN Program pr ON darp.ProgramContentItemId = pr.ContentItemId
		JOIN aspnet_Users au ON 'mastercardmembers:' + au.UserName = d.Who
		JOIN [User] u ON u.UserName = 'mastercardmembers:' + au.UserName
		JOIN #issuerForSegmentations i ON i.IssuerID = u.IssuerId
		JOIN IssuerProcessor ipp ON ipp.IssuerID = i.IssuerID
		JOIN Processor p ON p.ProcessorID = ipp.ProcessorID
		WHERE p.Title NOT IN ('A Plus')
			AND i.IsMastercardIssuer = 0
			AND [When] >= @Local_StartDate
			AND [When] < @Local_EndDate
			AND i.IssuerID <> 'a05'
			AND pr.ContentItemId IS NULL
		
		UNION
		
		SELECT OrderDate AS DATE
			,FullName AS Requestor
			,p.Title AS Processor
			,i.Title AS 'FinancialInstitution'
			,u.UserName
			,oi.ItemName AS 'ItemName'
			,'Orderable' AS 'ItemType'
			,oa.Sku
			,CASE 
				WHEN oi.ElectronicDelivery = 1
					THEN 'Electronic Delivery'
				WHEN oa.Vdp = 1
					THEN 'VDP'
				ELSE 'Print On Demand'
				END AS 'Fulfillment'
			,oi.ItemPrice AS 'ItemPrice'
			,oi.ItemQuantity AS 'ItemQuantity'
			,o.OrderStatus AS 'OrderStatus'
			,pr.ContentItemId AS RelatedProgramID
			,pr.Title AS RelatedProgram
			,CASE 
				WHEN oi.ElectronicDelivery = 1
					THEN 1
				ELSE 0
				END AS 'ED'
			,CASE 
				WHEN oa.Vdp <> 1
					AND oi.ElectronicDelivery = 0
					THEN 1
				ELSE 0
				END AS 'POD'
			,0 AS Download
		FROM OrderableAsset oa
		LEFT JOIN OrderableAssetRelatedProgram oarp ON oa.ContentItemId = oarp.OrderableAssetContentItemId
		LEFT JOIN Program pr ON oarp.ProgramContentItemId = pr.ContentItemId
		JOIN OrderItem oi ON oa.ContentItemId = oi.ItemID
		JOIN [Order] o ON o.OrderID = oi.OrderID
		JOIN [User] u ON u.UserName = o.UserID
		JOIN #issuerForSegmentations i ON i.IssuerID = u.IssuerId
		JOIN IssuerProcessor ipp ON ipp.IssuerID = i.IssuerID
		JOIN Processor p ON p.ProcessorID = ipp.ProcessorID
		WHERE p.Title NOT IN ('A Plus')
			AND i.IsMastercardIssuer = 0
			AND OrderDate >= @Local_StartDate
			AND OrderDate < @Local_EndDate
			AND i.IssuerID <> 'a05'
			AND pr.ContentItemId IS NULL
			AND o.OrderStatus <> 'Cancelled'
		) AS T
	GROUP BY RelatedProgramID
		,RelatedProgram

	SET context_info 0x;
END;