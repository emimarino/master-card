﻿CREATE PROCEDURE [dbo].[RetrieveContentActivityForWebinarReport] @StartDate DATETIME
	,@EndDate DATETIME
	,@AudienceSegmentation VARCHAR(MAX)
	,@FilterMasterCardUsers BIT
	,@FilterVendorProcessor BIT
	,@RegionId VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

	--
	DECLARE @Local_StartDate DATETIME = @StartDate;
	DECLARE @Local_EndDate DATETIME = @EndDate;
	DECLARE @Local_AudienceSegmentation VARCHAR(MAX) = @AudienceSegmentation;
	DECLARE @Local_FilterMasterCardUsers BIT = @FilterMasterCardUsers;
	DECLARE @Local_FilterVendorProcessor BIT = @FilterVendorProcessor;
	DECLARE @Local_RegionId VARCHAR(255) = @RegionId;

	--
	EXEC [dbo].[UpdateSiteTrackingContext] @Local_StartDate;

	--
	IF object_id('tempdb..#issuerForSegmentations', 'U') IS NOT NULL
		DROP TABLE #issuerForSegmentations;

	CREATE TABLE #issuerForSegmentations (
		IssuerId VARCHAR(25)
		,Title NVARCHAR(255)
		,AudienceSegments NVARCHAR(255)
		,IsMastercardIssuer BIT
		);

	INSERT INTO #issuerForSegmentations (
		IssuerId
		,Title
		,AudienceSegments
		,IsMastercardIssuer
		)
	SELECT DISTINCT i.IssuerID
		,i.Title
		,s.SegmentationName AS AudienceSegments
		,i.IsMastercardIssuer
	FROM Issuer i WITH (NOLOCK)
	LEFT JOIN IssuerSegmentation iss WITH (NOLOCK) ON i.IssuerID = iss.IssuerID
	LEFT JOIN Segmentation s WITH (NOLOCK) ON s.SegmentationId = iss.SegmentationId
	WHERE iss.IssuerID IS NULL
		OR iss.SegmentationId IN (
			SELECT Token
			FROM Split(@Local_AudienceSegmentation, ',')
			)
		OR @Local_RegionId = 'fmm';

	--
	SELECT ci.ContentItemID
		,w.Title
		,COUNT(DISTINCT (p.ProcessorID)) Processors
		,COUNT(DISTINCT (i.IssuerID)) Issuers
		,COUNT(DISTINCT (u.UserID)) Users
		,COUNT(w.Title) 'TotalHits'
		,COALESCE(SUM(subscriptiondaily.c), 0) + COALESCE(SUM(subscriptionweekly.c), 0) 'Email'
		,COALESCE(SUM(myfavorites.c), 0) 'MyFavorites'
		,COALESCE(SUM(mostpopular.c), 0) 'MostPopular'
	FROM Webinar w
	INNER JOIN vSiteTrackingByContext st ON st.ContentItemID = w.ContentItemId
	INNER JOIN [User] u ON LOWER(u.UserName) = LOWER(st.Who)
	INNER JOIN #issuerForSegmentations i ON i.IssuerID = u.IssuerId
	LEFT JOIN IssuerProcessor ip ON ip.IssuerID = i.IssuerID
	LEFT JOIN Processor p ON p.ProcessorID = ip.ProcessorID
	INNER JOIN ContentItem ci ON ci.ContentItemID = w.ContentItemId
	INNER JOIN ContentType ct ON ct.ContentTypeID = ci.ContentTypeID
	LEFT JOIN (
		SELECT 1 c
			,'subscriptiondaily' subscriptiondaily
		) subscriptiondaily ON st.How LIKE (
			CONCAT (
				subscriptiondaily.subscriptiondaily
				,'%'
				)
			)
	LEFT JOIN (
		SELECT 1 c
			,'subscriptionweekly' subscriptionweekly
		) subscriptionweekly ON st.How LIKE (
			CONCAT (
				subscriptionweekly.subscriptionweekly
				,'%'
				)
			)
	LEFT JOIN (
		SELECT 1 c
			,'myfavorites' myfavorites
		) myfavorites ON st.How = myfavorites.myfavorites
	LEFT JOIN (
		SELECT 1 c
			,'mostpopular' mostpopular
		) mostpopular ON st.How = mostpopular.mostpopular
	WHERE w.ContentItemId NOT LIKE '%-p'
		AND i.IssuerID <> 'a05'
		AND st.[When] >= @Local_StartDate
		AND st.[When] < @Local_EndDate
		AND st.Region = @Local_RegionId
		AND (
			(
				p.ProcessorID IS NULL
				AND @Local_RegionId <> 'us'
				)
			OR (
				(
					@Local_FilterMasterCardUsers = 0
					OR i.IsMastercardIssuer = 0
					)
				AND (
					@Local_FilterVendorProcessor = 0
					OR p.ProcessorID <> '91b'
					)
				)
			)
	GROUP BY ci.ContentItemID
		,ct.Title
		,w.Title
	ORDER BY w.Title;

	SET context_info 0x;
END;