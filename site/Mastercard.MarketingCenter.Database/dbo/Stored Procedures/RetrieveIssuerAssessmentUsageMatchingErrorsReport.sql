﻿CREATE PROCEDURE [dbo].[RetrieveIssuerAssessmentUsageMatchingErrorsReport]
	@StartDate					DATETIME,
	@EndDate					DATETIME,
	@FilterMasterCardUsers	BIT,
	@FilterVendorProcessor		BIT
AS
SELECT		u.FullName AS [User],
			u.[UserName],	
			i.[IssuerID],
			i.[Title] AS [Issuer],
			s.[SegmentationName] AS [AudienceSegments],
			p.[ProcessorID],
			p.[Title] AS [Processor],
			iu.[Date],
			iu.[Error],
			iu.[Message],
			iu.[ImportedIcaId],
			ic.[LegalName],
			ic.[CID]
FROM		[IssuerUnmatched] iu
			JOIN [User] u ON u.[UserID] = iu.[UserId]
			JOIN [Issuer] i ON i.[IssuerID] = iu.[IssuerID]
			LEFT JOIN [IssuerSegmentation] iss ON i.[IssuerID] = iss.[IssuerID]
			LEFT JOIN [Segmentation] s ON s.[SegmentationId] = iss.[SegmentationId]
			JOIN [IssuerProcessor] ipp ON ipp.[IssuerID] = i.[IssuerID]
			JOIN [Processor] p ON p.[ProcessorID] = ipp.[ProcessorID]
			LEFT JOIN [ImportedIca] ic ON ic.[ImportedIcaId] = iu.[ImportedIcaId]
WHERE		i.[IssuerID] <> 'a05'
			AND i.[RegionId] = 'us'
			AND iu.[Date] >= @StartDate
			AND iu.[Date] < @EndDate
			AND (@FilterMasterCardUsers = 0 OR i.IsMastercardIssuer=0)
			AND (@FilterVendorProcessor = 0 OR p.[ProcessorID] <> '91b')