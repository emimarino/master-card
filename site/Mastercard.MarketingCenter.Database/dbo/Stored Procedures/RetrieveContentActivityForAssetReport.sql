﻿CREATE PROCEDURE [dbo].[RetrieveContentActivityForAssetReport] @StartDate DATETIME
	,@EndDate DATETIME
	,@AudienceSegmentation VARCHAR(MAX)
	,@FilterMasterCardUsers BIT
	,@FilterVendorProcessor BIT
	,@RegionId VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

	--
	DECLARE @Local_StartDate DATETIME = @StartDate;
	DECLARE @Local_EndDate DATETIME = @EndDate;
	DECLARE @Local_AudienceSegmentation VARCHAR(MAX) = @AudienceSegmentation;
	DECLARE @Local_FilterMasterCardUsers BIT = @FilterMasterCardUsers;
	DECLARE @Local_FilterVendorProcessor BIT = @FilterVendorProcessor;
	DECLARE @Local_RegionId VARCHAR(255) = @RegionId;

	--
	EXEC [dbo].[UpdateSiteTrackingContext] @Local_StartDate;

	--
	IF object_id('tempdb..#issuerForSegmentations', 'U') IS NOT NULL
		DROP TABLE #issuerForSegmentations;

	CREATE TABLE #issuerForSegmentations (
		IssuerId VARCHAR(25)
		,Title NVARCHAR(255)
		,AudienceSegments NVARCHAR(255)
		,IsMastercardIssuer BIT
		);

	INSERT INTO #issuerForSegmentations (
		IssuerId
		,Title
		,AudienceSegments
		,IsMastercardIssuer
		)
	SELECT DISTINCT i.IssuerID
		,i.Title
		,s.SegmentationName AS AudienceSegments
		,i.IsMastercardIssuer
	FROM Issuer i WITH (NOLOCK)
	LEFT JOIN IssuerSegmentation iss WITH (NOLOCK) ON i.IssuerID = iss.IssuerID
	LEFT JOIN Segmentation s WITH (NOLOCK) ON s.SegmentationId = iss.SegmentationId
	WHERE iss.IssuerID IS NULL
		OR iss.SegmentationId IN (
			SELECT Token
			FROM Split(@Local_AudienceSegmentation, ',')
			)
		OR @Local_RegionId = 'fmm';

	--
	DECLARE @temp TABLE (
		ContentItemId VARCHAR(25)
		,Title VARCHAR(255)
		,How VARCHAR(1000)
		,Who VARCHAR(1000)
		);

	--
	INSERT INTO @temp
	SELECT st.ContentItemID
		,a.Title Title
		,COALESCE(st.How, 'generic') How
		,st.Who
	FROM vSiteTrackingByContext st
	JOIN Asset a ON a.ContentItemId = st.ContentItemID
	WHERE st.ContentItemID IS NOT NULL
		AND st.[When] >= @Local_StartDate
		AND st.[When] < @Local_EndDate
		AND st.Region = @Local_RegionId;

	INSERT INTO @temp
	SELECT st.ContentItemID
		,afw.Title Title
		,COALESCE(st.How, 'generic') How
		,st.Who
	FROM vSiteTrackingByContext st
	JOIN AssetFullWidth afw ON afw.ContentItemId = st.ContentItemID
	WHERE st.ContentItemID IS NOT NULL
		AND st.[When] >= @Local_StartDate
		AND st.[When] < @Local_EndDate
		AND st.Region = @Local_RegionId;

	INSERT INTO @temp
	SELECT st.ContentItemID
		,oa.Title Title
		,COALESCE(st.How, 'generic') How
		,st.Who
	FROM vSiteTrackingByContext st
	JOIN OrderableAsset oa ON oa.ContentItemId = st.ContentItemID
	WHERE st.ContentItemID IS NOT NULL
		AND st.[When] >= @Local_StartDate
		AND st.[When] < @Local_EndDate
		AND st.Region = @Local_RegionId;

	INSERT INTO @temp
	SELECT st.ContentItemID
		,da.Title Title
		,COALESCE(st.How, 'generic') How
		,st.Who
	FROM vSiteTrackingByContext st
	JOIN DownloadableAsset da ON da.ContentItemId = st.ContentItemID
	WHERE st.ContentItemID IS NOT NULL
		AND st.[When] >= @Local_StartDate
		AND st.[When] < @Local_EndDate
		AND st.Region = @Local_RegionId;

	INSERT INTO @temp
	SELECT st.ContentItemID
		,w.Title Title
		,COALESCE(st.How, 'generic') How
		,st.Who
	FROM vSiteTrackingByContext st
	JOIN Webinar w ON w.ContentItemId = st.ContentItemID
	WHERE st.ContentItemID IS NOT NULL
		AND st.[When] >= @Local_StartDate
		AND st.[When] < @Local_EndDate
		AND st.Region = @Local_RegionId;

	INSERT INTO @temp
	SELECT st.ContentItemID
		,l.Title Title
		,COALESCE(st.How, 'generic') How
		,st.Who
	FROM vSiteTrackingByContext st
	JOIN Link l ON l.ContentItemId = st.ContentItemID
	WHERE st.ContentItemID IS NOT NULL
		AND st.[When] >= @Local_StartDate
		AND st.[When] < @Local_EndDate
		AND st.Region = @Local_RegionId;

	--
	SELECT ct.Title 'ContentType'
		,ci.ContentItemID
		,Assets.Title
		,COUNT(DISTINCT (p.ProcessorID)) Processors
		,COUNT(DISTINCT (i.IssuerID)) Issuers
		,COUNT(DISTINCT (u.UserID)) Users
		,COUNT(Assets.Title) 'TotalHits'
		,COALESCE(SUM(generic.c), 0) + COALESCE(SUM(thumbnail.c), 0) + COALESCE(SUM(back.c), 0) 'Generic'
		,COALESCE(SUM(marquee.c), 0) 'Marquee'
		,COALESCE(SUM(productfinder.c), 0) 'ProductFinder'
		,COALESCE(SUM(crosssellbox.c), 0) + COALESCE(SUM(thumbnailcrosssellbox.c), 0) 'CrossSellBox'
		,COALESCE(SUM(relatedasset.c), 0) 'RelatedAsset'
		,COALESCE(SUM(relateditem.c), 0) + COALESCE(SUM(thumbnailrelateditem.c), 0) 'RelatedItem'
		,COALESCE(SUM(back.c), 0) 'Back'
		,COALESCE(SUM(subscriptiondaily.c), 0) + COALESCE(SUM(subscriptionweekly.c), 0) 'Email'
		,COALESCE(SUM(myfavorites.c), 0) 'MyFavorites'
		,COALESCE(SUM(mostpopular.c), 0) 'MostPopular'
	FROM @temp Assets
	JOIN ContentItem ci ON Assets.ContentItemId = ci.ContentItemID
	JOIN ContentType ct ON ci.ContentTypeID = ct.ContentTypeID
	JOIN [User] u ON LOWER(u.UserName) = LOWER(Assets.Who)
	JOIN #issuerForSegmentations i ON i.IssuerID = u.IssuerId
	LEFT JOIN IssuerProcessor ip ON ip.IssuerID = i.IssuerID
	LEFT JOIN Processor p ON p.ProcessorID = ip.ProcessorID
	LEFT JOIN (
		SELECT 1 c
			,'generic' generic
		) generic ON Assets.How = generic.generic
	LEFT JOIN (
		SELECT 1 c
			,'marquee' marquee
		) marquee ON Assets.How = marquee.marquee
	LEFT JOIN (
		SELECT 1 c
			,'product-finder' productfinder
		) productfinder ON Assets.How = productfinder.productfinder
	LEFT JOIN (
		SELECT 1 c
			,'related-program' relatedprogram
		) relatedprogram ON Assets.How = relatedprogram.relatedprogram
	LEFT JOIN (
		SELECT 1 c
			,'related-item' relateditem
		) relateditem ON Assets.How = relateditem.relateditem
	LEFT JOIN (
		SELECT 1 c
			,'thumbnail related-item' thumbnailrelateditem
		) thumbnailrelateditem ON Assets.How = thumbnailrelateditem.thumbnailrelateditem
	LEFT JOIN (
		SELECT 1 c
			,'cross-sell-box' crosssellbox
		) crosssellbox ON Assets.How = crosssellbox.crosssellbox
	LEFT JOIN (
		SELECT 1 c
			,'thumbnail cross-sell-box' thumbnailcrosssellbox
		) thumbnailcrosssellbox ON Assets.How = thumbnailcrosssellbox.thumbnailcrosssellbox
	LEFT JOIN (
		SELECT 1 c
			,'related-asset' relatedasset
		) relatedasset ON Assets.How = relatedasset.relatedasset
	LEFT JOIN (
		SELECT 1 c
			,'back' back
		) back ON Assets.How = back.back
	LEFT JOIN (
		SELECT 1 c
			,'thumbnail' thumbnail
		) thumbnail ON Assets.How = thumbnail.thumbnail
	LEFT JOIN (
		SELECT 1 c
			,'subscriptiondaily' subscriptiondaily
		) subscriptiondaily ON Assets.How LIKE (
			CONCAT (
				subscriptiondaily.subscriptiondaily
				,'%'
				)
			)
	LEFT JOIN (
		SELECT 1 c
			,'subscriptionweekly' subscriptionweekly
		) subscriptionweekly ON Assets.How LIKE (
			CONCAT (
				subscriptionweekly.subscriptionweekly
				,'%'
				)
			)
	LEFT JOIN (
		SELECT 1 c
			,'myfavorites' myfavorites
		) myfavorites ON Assets.How = myfavorites.myfavorites
	LEFT JOIN (
		SELECT 1 c
			,'mostpopular' mostpopular
		) mostpopular ON Assets.How = mostpopular.mostpopular
	WHERE i.IssuerID <> 'a05'
		AND (
			(
				p.ProcessorID IS NULL
				AND @Local_RegionId <> 'us'
				)
			OR (
				(
					@Local_FilterMasterCardUsers = 0
					OR i.IsMastercardIssuer = 0
					)
				AND (
					@Local_FilterVendorProcessor = 0
					OR p.ProcessorID <> '91b'
					)
				)
			)
		AND ci.ContentItemID NOT LIKE '%-p'
		AND ci.RegionId = @Local_RegionId
	GROUP BY ci.ContentItemID
		,ct.Title
		,Assets.Title
	ORDER BY Assets.Title;

	SET context_info 0x;
END;