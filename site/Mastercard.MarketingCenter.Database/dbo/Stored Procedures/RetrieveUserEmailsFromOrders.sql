﻿CREATE PROCEDURE [dbo].[RetrieveUserEmailsFromOrders]
(
	@FilterText nvarchar(255),
	@ProcessorID varchar(25)
)
as
begin
	if @ProcessorID <> ''
		SELECT	distinct(u.[Email]) as Email
		from    [User] u,
				[IssuerProcessor] ip,
				[Order] o
		where	u.IssuerId = ip.IssuerID and
				o.UserID = u.UserName and
				ip.ProcessorID = @ProcessorID and
				u.[Email] like  @FilterText + '%' and
				lower(u.UserName) like 'mastercardmembers:%' and
				u.IssuerId is not null and
				u.IsDisabled = 0
		order by Email asc
	else
		SELECT	distinct(u.[Email]) as Email
		from    [User] u,
				[Order] o
		where	u.[Email] like  @FilterText + '%' and
				o.UserID = u.UserName and
				lower(u.UserName) like 'mastercardmembers:%' and
				u.IssuerId is not null and
				u.IsDisabled = 0
		order by Email asc
end