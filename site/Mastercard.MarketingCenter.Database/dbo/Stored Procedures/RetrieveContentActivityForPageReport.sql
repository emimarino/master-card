﻿CREATE PROCEDURE [dbo].[RetrieveContentActivityForPageReport] @StartDate DATETIME
	,@EndDate DATETIME
	,@AudienceSegmentation VARCHAR(MAX)
	,@FilterMasterCardUsers BIT
	,@FilterVendorProcessor BIT
	,@RegionId VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;
	--
	DECLARE @Local_StartDate DATETIME = @StartDate;
	DECLARE @Local_EndDate DATETIME = @EndDate;
	DECLARE @Local_AudienceSegmentation VARCHAR(MAX) = @AudienceSegmentation;
	DECLARE @Local_FilterMasterCardUsers BIT = @FilterMasterCardUsers;
	DECLARE @Local_FilterVendorProcessor BIT = @FilterVendorProcessor;
	DECLARE @Local_RegionId VARCHAR(255) = @RegionId;
	--
	EXEC [dbo].[UpdateSiteTrackingContext] @Local_StartDate;
	--
	CREATE TABLE #vSegmentationIds (SegmentationId VARCHAR(25) COLLATE DATABASE_DEFAULT);

	CREATE CLUSTERED INDEX ix_vSegmentationIds ON #vSegmentationIds (SegmentationId);

	CREATE TABLE #vSeg (
		IssuerId VARCHAR(255) COLLATE DATABASE_DEFAULT NOT NULL
		,Title VARCHAR(255) COLLATE DATABASE_DEFAULT NOT NULL
		,AudienceSegments VARCHAR(255) COLLATE DATABASE_DEFAULT NULL
		,IsMastercardIssuer BIT NOT NULL
		)

	CREATE CLUSTERED INDEX ix_vSeg ON #vSeg (IssuerId);

	CREATE TABLE #vTemp (
		Uri VARCHAR(255) COLLATE DATABASE_DEFAULT NULL
		,ContentItemId VARCHAR(255) COLLATE DATABASE_DEFAULT NOT NULL
		,Title VARCHAR(255) COLLATE DATABASE_DEFAULT NULL
		,How VARCHAR(255) COLLATE DATABASE_DEFAULT NOT NULL
		,[When] DATETIME
		,IssuerId VARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
		,ProcessorId VARCHAR(255) COLLATE DATABASE_DEFAULT NULL
		,UserId INT NOT NULL
		,OrderNumber INT NULL
		)

	CREATE CLUSTERED INDEX ix_vTemp ON #vTemp (
		ContentItemId
		,ProcessorId
		,UserId
		);

	--
	INSERT INTO #vSegmentationIds
	SELECT Token
	FROM Split(@Local_AudienceSegmentation, ',');

	INSERT INTO #vSeg
	SELECT DISTINCT i.IssuerId
		,i.Title
		,s.SegmentationName AS AudienceSegments
		,i.IsMastercardIssuer
	FROM Issuer i WITH (NOLOCK)
	LEFT JOIN IssuerSegmentation iss WITH (NOLOCK) ON i.IssuerId = iss.IssuerId
	LEFT JOIN Segmentation s WITH (NOLOCK) ON s.SegmentationId = iss.SegmentationId
	WHERE iss.IssuerId IS NULL
		OR iss.SegmentationId IN (
			SELECT SegmentationId
			FROM #vSegmentationIds
			)
		OR @Local_RegionId = 'fmm';

	INSERT INTO #vTemp
	SELECT pa.Uri
		,pa.ContentItemId
		,pa.Title
		,COALESCE(st.How, 'generic') How
		,st.[When]
		,i.IssuerId
		,p.ProcessorId
		,u.UserId
		,pa.OrderNumber
	FROM [Page] pa
	INNER JOIN vSiteTrackingByContext st ON replace(LEFT(st.what, CHARINDEX('?', st.what + '?') - 1), '/', '') = 'portal' + replace(pa.Uri, '/', '')
	JOIN ContentItem ci ON pa.ContentItemId = ci.ContentItemID
		AND ci.RegionId = st.Region
	JOIN [User] u ON LOWER(u.UserName) = LOWER(st.Who)
	JOIN #vSeg i ON u.IssuerId = i.IssuerId
	LEFT JOIN IssuerProcessor ip ON i.IssuerId = ip.IssuerId
	LEFT JOIN Processor p ON ip.ProcessorId = p.ProcessorId
	WHERE pa.ContentItemId NOT LIKE '%-p'
		AND st.Region = @Local_RegionId
		AND [When] >= @Local_StartDate
		AND [When] < @Local_EndDate
		AND (
			(
				p.ProcessorId IS NULL
				AND @Local_RegionId <> 'us'
				)
			OR (
				(
					@Local_FilterMasterCardUsers = 0
					OR i.IsMastercardIssuer = 0
					)
				AND (
					@Local_FilterVendorProcessor = 0
					OR p.ProcessorId <> '91b'
					)
				)
			)

	--
	SELECT ci.ContentItemId
		,T.Title
		,COUNT(DISTINCT (ProcessorId)) Processors
		,COUNT(DISTINCT (IssuerId)) Issuers
		,COUNT(DISTINCT (UserId)) Users
		,COUNT(T.Title) 'TotalHits'
		,COALESCE(SUM(generic.c), 0) + COALESCE(SUM(back.c), 0) 'Generic'
		,COALESCE(SUM(marquee.c), 0) 'Marquee'
		,COALESCE(SUM(tab.c), 0) 'Tab'
		,COALESCE(SUM(navigation.c), 0) 'Navigation'
		,COALESCE(SUM(crosssellbox.c), 0) + COALESCE(SUM(thumbnailcrosssellbox.c), 0) 'CrossSellBox'
		,COALESCE(SUM(productfinder.c), 0) 'ProductFinder'
		,COALESCE(SUM(back.c), 0) 'Back'
		,COALESCE(SUM(subscriptiondaily.c), 0) + COALESCE(SUM(subscriptionweekly.c), 0) 'Email'
		,COALESCE(SUM(myfavorites.c), 0) 'MyFavorites'
		,COALESCE(SUM(mostpopular.c), 0) 'MostPopular'
	FROM #vTemp T WITH (
			TABLOCK
			,HOLDLOCK
			)
	INNER JOIN ContentItem ci ON T.ContentItemId = ci.ContentItemId
	INNER JOIN ContentType ct ON ci.ContentTypeID = ct.ContentTypeID
	LEFT JOIN (
		SELECT 1 c
			,'generic' generic
		) generic ON T.How = generic.generic
	LEFT JOIN (
		SELECT 1 c
			,'marquee' marquee
		) marquee ON T.How = marquee.marquee
	LEFT JOIN (
		SELECT 1 c
			,'tab' tab
		) tab ON T.How = tab.tab
	LEFT JOIN (
		SELECT 1 c
			,'navigation' navigation
		) navigation ON T.How = navigation.navigation
	LEFT JOIN (
		SELECT 1 c
			,'cross-sell-box' crosssellbox
		) crosssellbox ON T.How = crosssellbox.crosssellbox
	LEFT JOIN (
		SELECT 1 c
			,'thumbnail cross-sell-box' thumbnailcrosssellbox
		) thumbnailcrosssellbox ON T.How = thumbnailcrosssellbox.thumbnailcrosssellbox
	LEFT JOIN (
		SELECT 1 c
			,'product-finder' productfinder
		) productfinder ON T.How = productfinder.productfinder
	LEFT JOIN (
		SELECT 1 c
			,'back' back
		) back ON T.How = back.back
	LEFT JOIN (
		SELECT 1 c
			,'subscriptiondaily' subscriptiondaily
		) subscriptiondaily ON T.How LIKE (
			CONCAT (
				subscriptiondaily.subscriptiondaily
				,'%'
				)
			)
	LEFT JOIN (
		SELECT 1 c
			,'subscriptionweekly' subscriptionweekly
		) subscriptionweekly ON T.How LIKE (
			CONCAT (
				subscriptionweekly.subscriptionweekly
				,'%'
				)
			)
	LEFT JOIN (
		SELECT 1 c
			,'myfavorites' myfavorites
		) myfavorites ON T.How = myfavorites.myfavorites
	LEFT JOIN (
		SELECT 1 c
			,'mostpopular' mostpopular
		) mostpopular ON T.How = mostpopular.mostpopular
	WHERE [When] >= @Local_StartDate
		AND [When] < @Local_EndDate
		AND (
			T.ContentItemId IN (
				SELECT first_value(ContentItemId) OVER (
						PARTITION BY Uri
						,title ORDER BY ContentItemId
						)
				FROM #vTemp
				)
			)
		AND T.IssuerId NOT IN ('a05')
		AND ci.ContentItemId NOT LIKE '%-p'
	GROUP BY ci.ContentItemId
		,ct.Title
		,T.Title
		,OrderNumber
	ORDER BY OrderNumber
	OPTION (OPTIMIZE FOR UNKNOWN)

	SET context_info 0x;

	DROP TABLE #vSegmentationIds

	DROP TABLE #vSeg

	DROP TABLE #vTemp
END;