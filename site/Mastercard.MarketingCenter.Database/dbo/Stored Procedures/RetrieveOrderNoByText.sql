﻿CREATE PROCEDURE [dbo].[RetrieveOrderNoByText]
(
	@FilterText varchar(255),
	@ProcessorID varchar(40)
)
as
	if @ProcessorID <> ''
		SELECT	o.OrderNumber
		from    [Order] o,
				[IssuerProcessor] ip,
				[User] u
		where	u.IssuerId = ip.IssuerID and
				ip.ProcessorID = @ProcessorID and
				o.UserID = u.UserName and
				o.OrderNumber like @FilterText + '%' and
				u.IsDisabled = 0
		order by o.OrderNumber asc
	else
		SELECT	o.OrderNumber
		from    [Order] o
		where	o.OrderNumber like @FilterText + '%'
		order by o.OrderNumber asc
