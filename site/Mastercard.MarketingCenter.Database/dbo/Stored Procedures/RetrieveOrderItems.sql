﻿
CREATE PROCEDURE [dbo].[RetrieveOrderItems]
(
@OrderID varchar(255)
)
as
	select	oi.ItemID,
			oi.ItemName,
			sum(oi.ItemPrice) ItemPrice,
			sum(oi.ItemQuantity) ItemQuantity,
			oi.ProofPDF,
			oi.SKU,
			oi.Customizations,
			oi.ElectronicDelivery,
			oi.VDP,
			ed.ElectronicDeliveryID,
			ed.ExpirationDate,
			oi.ApproverEmailAddress
	from	[OrderItem] oi
			left join [ElectronicDelivery] ed
				on ed.OrderItemID = oi.OrderItemID
	where	oi.OrderID = @OrderID
	group by oi.ItemID,
			oi.ItemName,
			oi.ProofPDF,
			oi.SKU,
			oi.Customizations,
			oi.ElectronicDelivery,
			oi.VDP,
			ed.ElectronicDeliveryID,
			ed.ExpirationDate,
			oi.ApproverEmailAddress
	order by oi.SKU
