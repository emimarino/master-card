﻿
CREATE PROCEDURE [dbo].[RetrieveUserLatestShippingInformation]
(
@UserID varchar(255)
)
as
	SELECT	top 1 SI.ContactName,
			SI.Phone, 
			SI.[Address]
	FROM    [ShippingInformation] SI,
			[OrderItem] oi,
			[Order] o
	where	SI.ShippingInformationID = oi.ShippingInformationID and
			oi.OrderID = o.OrderID and
			o.UserID = @UserID 
	order by SI.ShippingInformationID asc
