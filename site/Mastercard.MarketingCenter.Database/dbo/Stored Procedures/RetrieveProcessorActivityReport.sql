﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[RetrieveProcessorActivityReport]
	@StartDate datetime,
	@EndDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select	p.ProcessorID, p.Title Processor,
		(select count(i.IssuerId) 
		 from	Issuer i,
				IssuerProcessor ip
		 where	i.IssuerID = ip.IssuerID and
				ip.ProcessorID = p.ProcessorID and
				i.DateCreated >= @StartDate and
				i.DateCreated < @EndDate
		) FIsAdded,
		(select	count(u.UserName)
		 from	[User] u,
				Issuer i,
				IssuerProcessor ip,
				aspnet_Membership m,
				aspnet_Users au
		 where	i.IssuerID = ip.IssuerID and
				ip.ProcessorID = p.ProcessorID and
				u.IssuerId = i.IssuerID and
				m.UserId = au.UserId and
				'mastercardmembers:' + au.UserName = u.UserName and
				m.CreateDate >= @StartDate and
				m.CreateDate < @EndDate
		) UserRegistrations,
		(select	count(distinct(u.UserName))
		 from	[User] u,
				Issuer i,
				IssuerProcessor ip,
				LoginTracking lt
		 where	i.IssuerID = ip.IssuerID and
				ip.ProcessorID = p.ProcessorID and
				u.IssuerId = i.IssuerID and
				lt.UserName = u.UserName and
				lt.Date >= @StartDate and
				lt.Date < @EndDate
	) ActiveUsers,
		(select	count(u.UserName)
		 from	[User] u,
				Issuer i,
				IssuerProcessor ip,
				LoginTracking lt
		 where	i.IssuerID = ip.IssuerID and
				ip.ProcessorID = p.ProcessorID and
				u.IssuerId = i.IssuerID and
				lt.UserName = u.UserName and
				lt.Date >= @StartDate and
				lt.Date < @EndDate
		) UserLogins
from	Processor p
order by p.Title

END