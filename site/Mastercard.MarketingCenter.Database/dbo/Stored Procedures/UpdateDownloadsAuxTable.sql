﻿CREATE PROCEDURE [dbo].[UpdateDownloadsAuxTable]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Max_DataToBeDecodedAux_SiteTrackingID BIGINT = 0
		,@Max_DomoDownloadsApi_SiteTrackingID BIGINT = 0
		,@MaxSiteTrackingID BIGINT = 0;

	BEGIN TRY
		SET @Max_DataToBeDecodedAux_SiteTrackingID = (
				SELECT ISNULL(Max(SiteTrackingID), 0) AS _max
				FROM DataToBeDecodedAux
				);
		SET @Max_DomoDownloadsApi_SiteTrackingID = (
				SELECT ISNULL(Max(SiteTrackingID), 0) AS _max
				FROM DomoDownloadsApi
				);
		SET @MaxSiteTrackingID = CASE 
				WHEN @Max_DomoDownloadsApi_SiteTrackingID = 0
					THEN - 1
				WHEN @Max_DomoDownloadsApi_SiteTrackingID > @Max_DataToBeDecodedAux_SiteTrackingID
					THEN @Max_DomoDownloadsApi_SiteTrackingID
				ELSE @Max_DataToBeDecodedAux_SiteTrackingID
				END;

		BEGIN TRANSACTION

		TRUNCATE TABLE DataToBeDecodedAux;

		INSERT INTO DataToBeDecodedAux (
			[What]
			,[ContentItemID]
			,[From]
			,[How]
			,[RegionId]
			,[SiteTrackingID]
			,[VisitID]
			,[When]
			,[Who]
			)
		SELECT TOP 10000 *
		FROM (
			SELECT st.[What]
				,st.[ContentItemID]
				,st.[From]
				,st.[How]
				,st.[Region]
				,st.[SiteTrackingID]
				,st.[VisitID]
				,st.[When]
				,st.[Who]
			FROM SiteTracking AS st WITH (NOLOCK)
			WHERE (
					[What] LIKE '%electronicdownload/%'
					OR [What] LIKE '%electronicdelivery/%'
					OR [What] LIKE '%/HtmlDownload%'
					OR [What] LIKE '%/download%'
					OR [What] LIKE '%/InternalDownload%'
					OR [What] LIKE '%/LibraryDownload%'
					)
				AND [What] NOT LIKE '%registration/login.aspx?%'
				AND [What] NOT LIKE '/portal/error404%'
				AND [What] IS NOT NULL
				AND [What] <> 'null'
				AND (
					[From] NOT LIKE '%weblinkvalidator%'
					OR [From] IS NULL
					)
				AND st.SiteTrackingID > @MaxSiteTrackingID
			) AS a
		ORDER BY SiteTrackingID;

		COMMIT;
	END TRY

	BEGIN CATCH
		IF xact_state() > 0
			ROLLBACK;

		throw;
	END CATCH;
END;