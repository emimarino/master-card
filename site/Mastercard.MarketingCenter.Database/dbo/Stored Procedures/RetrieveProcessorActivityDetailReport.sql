﻿CREATE PROCEDURE [dbo].[RetrieveProcessorActivityDetailReport]
	@ProcessorId varchar(255) null,
	@StartDate datetime,
	@EndDate datetime,
	@Region VARCHAR(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if (@ProcessorId<>'' and @ProcessorId is not null)
	begin
		select * from (
			select	i2.IssuerID, i2.Title Issuer,
				(select count(i.IssuerId) 
				 from	Issuer i,
						IssuerProcessor ip
				 where	i.IssuerID = ip.IssuerID and
						ip.ProcessorID = p.ProcessorID and
						i.IssuerID = i2.IssuerID and
						i.DateCreated >= @StartDate and
						i.DateCreated < @EndDate and
						@Region = i.RegionId
				) FIsAdded,
				(select	count(u.UserName)
				 from	[User] u,
						Issuer i,
						IssuerProcessor ip,
						aspnet_Membership m,
						aspnet_Users au
				 where	i.IssuerID = ip.IssuerID and
						ip.ProcessorID = p.ProcessorID and
						u.IssuerId = i.IssuerID and
						m.UserId = au.UserId and
						'mastercardmembers:' + au.UserName = u.UserName and
						i.IssuerID = i2.IssuerID and
						m.CreateDate >= @StartDate and
						m.CreateDate < @EndDate and
						@Region = i.RegionId
				) UserRegistrations,
				(select	count(distinct(u.UserName))
				 from	[User] u,
						Issuer i,
						IssuerProcessor ip,
						LoginTracking lt
				 where	i.IssuerID = ip.IssuerID and
						ip.ProcessorID = p.ProcessorID and
						u.IssuerId = i.IssuerID and
						lt.UserName = u.UserName and
						i.IssuerID = i2.IssuerID and
						lt.Date >= @StartDate and
						lt.Date < @EndDate and
						@Region = i.RegionId
				) ActiveUsers,
				(select	count(u.UserName)
				 from	[User] u,
						Issuer i,
						IssuerProcessor ip,
						LoginTracking lt
				 where	i.IssuerID = ip.IssuerID and
						ip.ProcessorID = p.ProcessorID and
						u.IssuerId = i.IssuerID and
						lt.UserName = u.UserName and
						i.IssuerID = i2.IssuerID and
						lt.Date >= @StartDate and
						lt.Date < @EndDate	and
						@Region = i.RegionId
				) UserLogins
		from	Processor p, IssuerProcessor ip, Issuer i2
		where p.ProcessorID = ip.ProcessorID and ip.IssuerID = i2.IssuerID and p.ProcessorID = @ProcessorId) t
		where (userregistrations > 0 or activeusers > 0 or userlogins > 0)
		order by Issuer
	end
else 
	begin
		select * from (
			select	i2.IssuerID, i2.Title Issuer,
				(select count(i.IssuerId) 
				 from	Issuer i						
				 where	i.IssuerID = i2.IssuerID and
						i.DateCreated >= @StartDate and
						i.DateCreated < @EndDate and
						@Region = i.RegionId
				) FIsAdded,
				(select	count(u.UserName)
				 from	[User] u,
						Issuer i,
						aspnet_Membership m,
						aspnet_Users au
				 where	u.IssuerId = i.IssuerID and
						m.UserId = au.UserId and
						'mastercardmembers:' + au.UserName = u.UserName and
						i.IssuerID = i2.IssuerID and
						m.CreateDate >= @StartDate and
						m.CreateDate < @EndDate and
						@Region = i.RegionId
				) UserRegistrations,
				(select	count(distinct(u.UserName))
				 from	[User] u,
						Issuer i,
						LoginTracking lt
				 where	u.IssuerId = i.IssuerID and
						lt.UserName = u.UserName and
						i.IssuerID = i2.IssuerID and
						lt.Date >= @StartDate and
						lt.Date < @EndDate and
						@Region = i.RegionId
				) ActiveUsers,
				(select	count(u.UserName)
				 from	[User] u,
						Issuer i,
						LoginTracking lt
				 where	u.IssuerId = i.IssuerID and
						lt.UserName = u.UserName and
						i.IssuerID = i2.IssuerID and
						lt.Date >= @StartDate and
						lt.Date < @EndDate	and
						@Region = i.RegionId
				) UserLogins
		from	Issuer i2
		where  i2.RegionId=@Region
		) t
		where (userregistrations > 0 or activeusers > 0 or userlogins > 0)
		order by Issuer
	end
END