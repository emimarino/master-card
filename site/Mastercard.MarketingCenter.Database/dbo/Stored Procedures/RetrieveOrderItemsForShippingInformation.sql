﻿
CREATE PROCEDURE [dbo].[RetrieveOrderItemsForShippingInformation]
(
	@ShippingInformationID int
)
as
	SELECT	oi.ItemName, 
            oi.SKU,
            oi.ItemQuantity,
            oi.ItemPrice
	FROM	[OrderItem] oi
	where	oi.ShippingInformationID = @ShippingInformationID
	order by oi.SKU
