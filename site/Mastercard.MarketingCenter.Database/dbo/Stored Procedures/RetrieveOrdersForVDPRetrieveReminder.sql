﻿
CREATE PROCEDURE [dbo].[RetrieveOrdersForVDPRetrieveReminder]

as
	select	o.OrderID,
			o.OrderNumber,
			u.UserName,
			u.[Email],
			o.OrderDate
	from	[Order] o,
			[User] u
	where	o.OrderID not in (select OrderID from FtpAccount where [Server] = '') and
			o.OrderID in (select OrderID from [OrderItem] where VDP = 1) and
			o.VDPRetrieveReminderSent = 0 and
			o.OrderID not in (select OrderID from [VdpOrderInformation] where Completed = 1) and
			o.UserID = u.UserName
