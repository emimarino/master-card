﻿

CREATE PROCEDURE [dbo].[RetrieveIssuerByText]
(
	@FilterText nvarchar(255),
	@ProcessorID varchar(25),
	@RegionId nchar(6)
)
as
	if @ProcessorID <> ''
		SELECT	i.Title IssuerName
		from    [Issuer] i,
				[IssuerProcessor] ip
		where	i.IssuerID = ip.IssuerID and
				ip.ProcessorID = @ProcessorID and
				i.Title like @FilterText + '%' and
				i.RegionId = @RegionId
		order by i.Title
	else
		SELECT	i.Title IssuerName
		from    [Issuer] i
		where	i.Title like @FilterText + '%' and
				i.RegionId = @RegionId
		order by i.Title


