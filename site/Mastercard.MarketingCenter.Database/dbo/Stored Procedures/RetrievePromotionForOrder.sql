﻿
Create PROCEDURE [dbo].[RetrievePromotionForOrder]
(
	@OrderID int
)
as
	select	o.PromotionDescription,
			o.PromotionAmount
	from	[Order] o
	where	o.OrderID = @OrderID and
			o.PromotionDescription is not null and
			o.PromotionAmount is not null

