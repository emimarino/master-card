﻿
CREATE PROCEDURE [dbo].[RetrieveElectronicDeliveryCartItemsByCartId]
(
	@CartId int
)
as
	SELECT	oa.Title, 
            oa.Sku,
			ps.DownloadPrice
	FROM	[CartItem] ci,
            [OrderableAsset] oa,
            [ShoppingCart] sc,
			[PricingSchedule] ps
	where	ci.ContentItemID = oa.ContentItemId and
			ci.CartID = sc.CartID and
			ps.PricingScheduleID = oa.PricingScheduleID and
			sc.CartID = @CartId and 
			sc.DatedCheckedOut is null and
			ci.ElectronicDelivery = 1
	order by oa.ContentItemId
