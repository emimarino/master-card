﻿CREATE PROCEDURE PopulateCloneSourceNotification 
				AS
				BEGIN
				INSERT INTO CloneSourceNotification (CloneContentItemId,CloneRegionId,SourceStatusName,CreationDate,SourceModificationDate,CloneTitle  )
				--Begin Asset
				SELECT distinct CloneCI.ContentItemID CloneContentItemId
					,CloneCI.RegionId CloneRegionId
					,case when SourceStatus.StatusID in (3,4) then (SELECT Name FROM Status where StatusID=10 ) else SourceStatus.Name end as SourceStatusName /* If the status is in one of the "Published" status it assigns the RePublish state in the report */
					,GETDATE() CreationDate
					,SourceCI.ModifiedDate ModificationDate
					,CloneAsset.Title CloneTitle
					 FROM Asset as CloneAsset Join ContentItem CloneCI on CloneCI.ContentItemID=CloneAsset.ContentItemId
					JOIN ContentItem as SourceCI on CloneAsset.OriginalContentItemId=SourceCI.ContentItemID
					JOIN Status SourceStatus on SourceCI.StatusID=SourceStatus.StatusID
						WHERE SourceCI.ModifiedDate> DATEADD(HOUR,-24, GETDATE()) AND SourceCI.ModifiedDate>CloneCI.CreatedDate /* gets updates less then 24hs old and newer than the creation date of the clone*/
						and SourceCI.ContentItemID not like '%-p' /*avoids draft state items while on published and draft state */
						and ( SourceCI.StatusID in (3,4,6,8,9) )  /*gets only the reuired status Published, Published and draft,Archived,Deleted,Expired*/
						and ( CloneCI.StatusID in (1,2,3,5) or  (CloneCI.StatusID=4 and CloneCI.ContentItemID not like '%-p'  ) )
						and CloneAsset.NotifyWhenSourceChanges=1 /*Only create notifications if NotifyWhenSourceChanges is selected*/
						and not EXISTS(select 1 from  CloneSourceNotification where CloneCI.ContentItemID=CloneContentItemId and SourceCI.ModifiedDate=SourceModificationDate) --to avoid sendid the same update twice
				--End Asset
				UNION
				--Begin DownloadableAsset
				SELECT distinct CloneCI.ContentItemID CloneContentItemId
					,CloneCI.RegionId CloneRegionId
					,case when SourceStatus.StatusID in (3,4) then (SELECT Name FROM Status where StatusID=10 ) else SourceStatus.Name end as SourceStatusName
					,GETDATE() CreationDate	
					,SourceCI.ModifiedDate ModificationDate
					,CloneDownloadableAsset.Title CloneTitle
					 FROM DownloadableAsset as CloneDownloadableAsset Join ContentItem CloneCI on CloneCI.ContentItemID=CloneDownloadableAsset.ContentItemId
					JOIN ContentItem as SourceCI on CloneDownloadableAsset.OriginalContentItemId=SourceCI.ContentItemID
					JOIN Status SourceStatus on SourceCI.StatusID=SourceStatus.StatusID
						WHERE SourceCI.ModifiedDate> DATEADD(HOUR,-24, GETDATE()) AND SourceCI.ModifiedDate>CloneCI.CreatedDate
						and SourceCI.ContentItemID not like '%-p'
						and ( SourceCI.StatusID in (3,4,6,8,9) )	
						and ( CloneCI.StatusID in (1,2,3,5) or  (CloneCI.StatusID=4 and CloneCI.ContentItemID not like '%-p'  ) )	
						and CloneDownloadableAsset.NotifyWhenSourceChanges=1 /*Only create notifications if NotifyWhenSourceChanges is selected*/
						and not EXISTS(select 1 from  CloneSourceNotification where CloneCI.ContentItemID=CloneContentItemId and SourceCI.ModifiedDate=SourceModificationDate)	
				--End DownloadableAsset
				UNION
				--Begin AssetFullWidth
				SELECT distinct CloneCI.ContentItemID CloneContentItemId
					,CloneCI.RegionId CloneRegionId
					,case when SourceStatus.StatusID in (3,4) then (SELECT Name FROM Status where StatusID=10 ) else SourceStatus.Name end as SourceStatusName
					,GETDATE() CreationDate	
					,SourceCI.ModifiedDate ModificationDate
					,CloneAssetFullWidth.Title CloneTitle
					 FROM AssetFullWidth as CloneAssetFullWidth Join ContentItem CloneCI on CloneCI.ContentItemID=CloneAssetFullWidth.ContentItemId
					JOIN ContentItem as SourceCI on CloneAssetFullWidth.OriginalContentItemId=SourceCI.ContentItemID
					JOIN Status SourceStatus on SourceCI.StatusID=SourceStatus.StatusID
						WHERE SourceCI.ModifiedDate> DATEADD(HOUR,-24, GETDATE()) AND SourceCI.ModifiedDate>CloneCI.CreatedDate
						and SourceCI.ContentItemID not like '%-p'
						and ( SourceCI.StatusID in (3,4,6,8,9) )
						and ( CloneCI.StatusID in (1,2,3,5) or  (CloneCI.StatusID=4 and CloneCI.ContentItemID not like '%-p'  ) )
						and CloneAssetFullWidth.NotifyWhenSourceChanges=1 /*Only create notifications if NotifyWhenSourceChanges is selected*/
						and not EXISTS(select 1 from  CloneSourceNotification where CloneCI.ContentItemID=CloneContentItemId and SourceCI.ModifiedDate=SourceModificationDate)
				--End AssetFullWidth
				END