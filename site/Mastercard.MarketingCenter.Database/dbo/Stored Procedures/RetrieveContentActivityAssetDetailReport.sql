﻿CREATE PROCEDURE [dbo].[RetrieveContentActivityAssetDetailReport] @ContentItemId VARCHAR(255)
	,@StartDate DATETIME
	,@EndDate DATETIME
	,@AudienceSegmentation VARCHAR(MAX)
	,@FilterMasterCardUsers BIT
	,@FilterVendorProcessor BIT
	,@RegionId VARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON;

	--
	DECLARE @Local_ContentItemId VARCHAR(255) = @ContentItemId;
	DECLARE @Local_StartDate DATETIME = @StartDate;
	DECLARE @Local_EndDate DATETIME = @EndDate;
	DECLARE @Local_AudienceSegmentation VARCHAR(MAX) = @AudienceSegmentation;
	DECLARE @Local_FilterMasterCardUsers BIT = @FilterMasterCardUsers;
	DECLARE @Local_FilterVendorProcessor BIT = @FilterVendorProcessor;
	DECLARE @Local_RegionId VARCHAR(255) = @RegionId;

	--
	EXEC [dbo].[UpdateSiteTrackingContext] @Local_StartDate;

	--
	IF object_id('tempdb..#temp', 'U') IS NOT NULL
		DROP TABLE #temp;

	CREATE TABLE #temp (
		ContentItemId VARCHAR(25)
		,Title VARCHAR(255)
		);

	--
	IF object_id('tempdb..#issuerForSegmentations', 'U') IS NOT NULL
		DROP TABLE #issuerForSegmentations;

	CREATE TABLE #issuerForSegmentations (
		IssuerId VARCHAR(25)
		,Title NVARCHAR(255)
		,AudienceSegments NVARCHAR(255)
		,IsMastercardIssuer BIT
		);

	INSERT INTO #issuerForSegmentations (
		IssuerId
		,Title
		,AudienceSegments
		,IsMastercardIssuer
		)
	SELECT DISTINCT i.IssuerID
		,i.Title
		,s.SegmentationName AS AudienceSegments
		,i.IsMastercardIssuer
	FROM Issuer i WITH (NOLOCK)
	LEFT JOIN IssuerSegmentation iss WITH (NOLOCK) ON i.IssuerID = iss.IssuerID
	LEFT JOIN Segmentation s WITH (NOLOCK) ON s.SegmentationId = iss.SegmentationId
	WHERE iss.IssuerID IS NULL
		OR iss.SegmentationId IN (
			SELECT Token
			FROM Split(@Local_AudienceSegmentation, ',')
			)
		OR @Local_RegionId = 'fmm';

	--
	INSERT INTO #temp
	SELECT DISTINCT st.ContentItemID
		,a.Title Title
	FROM vSiteTrackingByContext st
	JOIN Asset a ON a.ContentItemId = st.ContentItemID
	WHERE st.ContentItemID IS NOT NULL
		AND st.[When] >= @Local_StartDate
		AND st.[When] < @Local_EndDate
		AND st.Region = @Local_RegionId;

	INSERT INTO #temp
	SELECT DISTINCT st.ContentItemID
		,afw.Title Title
	FROM vSiteTrackingByContext st
	JOIN AssetFullWidth afw ON afw.ContentItemId = st.ContentItemID
	WHERE st.ContentItemID IS NOT NULL
		AND st.[When] >= @Local_StartDate
		AND st.[When] < @Local_EndDate
		AND st.Region = @Local_RegionId;

	INSERT INTO #temp
	SELECT DISTINCT st.ContentItemID
		,oa.Title Title
	FROM vSiteTrackingByContext st
	JOIN OrderableAsset oa ON oa.ContentItemId = st.ContentItemID
	WHERE st.ContentItemID IS NOT NULL
		AND st.[When] >= @Local_StartDate
		AND st.[When] < @Local_EndDate
		AND st.Region = @Local_RegionId;

	INSERT INTO #temp
	SELECT DISTINCT st.ContentItemID
		,da.Title Title
	FROM vSiteTrackingByContext st
	JOIN DownloadableAsset da ON da.ContentItemId = st.ContentItemID
	WHERE st.ContentItemID IS NOT NULL
		AND st.[When] >= @Local_StartDate
		AND st.[When] < @Local_EndDate
		AND st.Region = @Local_RegionId;

	INSERT INTO #temp
	SELECT DISTINCT st.ContentItemID
		,w.Title Title
	FROM vSiteTrackingByContext st
	JOIN Webinar w ON w.ContentItemId = st.ContentItemID
	WHERE st.ContentItemID IS NOT NULL
		AND st.[When] >= @Local_StartDate
		AND st.[When] < @Local_EndDate
		AND st.Region = @Local_RegionId;

	INSERT INTO #temp
	SELECT DISTINCT st.ContentItemID
		,l.Title Title
	FROM vSiteTrackingByContext st
	JOIN Link l ON l.ContentItemId = st.ContentItemID
	WHERE st.ContentItemID IS NOT NULL
		AND st.[When] >= @Local_StartDate
		AND st.[When] < @Local_EndDate
		AND st.Region = @Local_RegionId;

	SELECT DISTINCT i.AudienceSegments
		,u.UserName
		,u.[FullName] 'Name'
		,i.IssuerId
		,i.Title Issuer
		,p.ProcessorId
		,coalesce(p.Title, 'N/A') Processor
		,st.[When]
		,st.[From]
		,COALESCE(st.How, 'generic') 'Source'
		,Assets.Title
	FROM #temp Assets
	JOIN vSiteTrackingByContext st ON st.ContentItemId = Assets.ContentItemId
	JOIN [User] u ON LOWER(u.UserName) = LOWER(st.Who)
	JOIN #issuerForSegmentations i ON i.IssuerId = u.IssuerId
	LEFT JOIN IssuerProcessor ipp ON ipp.IssuerId = i.IssuerId
	LEFT JOIN Processor p ON p.ProcessorId = ipp.ProcessorId
	WHERE Assets.ContentItemId NOT LIKE '%-p'
		AND (
			Assets.ContentItemId = @Local_ContentItemId
			OR @Local_ContentItemId = 'all-assets'
			)
		AND st.[When] >= @Local_StartDate
		AND st.[When] < @Local_EndDate
		AND @Local_RegionId = st.Region
		AND i.IssuerId <> 'a05'
		AND (
			(
				p.ProcessorId IS NULL
				AND @Local_RegionId <> 'us'
				)
			OR (
				(
					@Local_FilterMasterCardUsers = 0
					OR i.IsMastercardIssuer = 0
					)
				AND (
					@Local_FilterVendorProcessor = 0
					OR p.ProcessorId <> '91b'
					)
				)
			)
	ORDER BY [When] DESC;

	SET context_info 0x;
END;