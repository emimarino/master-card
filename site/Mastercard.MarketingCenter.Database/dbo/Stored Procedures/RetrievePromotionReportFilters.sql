﻿
CREATE PROCEDURE [dbo].[RetrievePromotionReportFilters]	
AS
BEGIN

	SELECT	DISTINCT PromotionCode Promotion 
	FROM	[Order]
	WHERE (PromotionCode + ' - ' + PromotionDescription) IS NOT NULL
	ORDER BY Promotion
	
END

