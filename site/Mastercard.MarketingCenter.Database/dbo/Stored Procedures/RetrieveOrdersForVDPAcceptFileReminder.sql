﻿
CREATE PROCEDURE [dbo].[RetrieveOrdersForVDPAcceptFileReminder]

as
	select	o.OrderID,
			o.OrderNumber,
			o.OrderDate
	from	[Order] o
	where	o.OrderID in (select OrderID from FtpAccount where [Server] = '') and
			o.VDPAcceptFileReminderSent = 0 and
			o.OrderID in (select OrderID from [OrderItem] where VDP = 1) and
			o.OrderID not in (select OrderID from [VdpOrderInformation] where Completed = 1)
