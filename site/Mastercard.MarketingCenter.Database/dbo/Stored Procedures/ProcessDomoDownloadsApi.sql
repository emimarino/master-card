﻿CREATE PROCEDURE [dbo].[ProcessDomoDownloadsApi]
	WITH RECOMPILE
AS
IF (
		NOT EXISTS (
			SELECT 1
			FROM INFORMATION_SCHEMA.TABLES
			WHERE TABLE_NAME = '#DomoDownloadsApiTemp'
			)
		)
BEGIN
	CREATE TABLE #DomoDownloadsApiTemp (
		url VARCHAR(255)
		,RegionId NCHAR(6)
		,ContentItemId VARCHAR(25)
		,ContentItemTitle NVARCHAR(225)
		,ContentTypeId VARCHAR(25)
		,ContentItemRegion NCHAR(6)
		,DownloadDate DATETIME
		,[FileName] NVARCHAR(MAX)
		,SiteTrackingId BIGINT
		,VisitId BIGINT
		,[Who] NVARCHAR(100)
		)
END

--normalization of Urls to allow for indexing
UPDATE DataToBeDecodedAux
SET [What] = SUBSTRING([What], CASE 
			WHEN CHARINDEX('/portal/', [What]) > 0
				THEN CHARINDEX('/portal/', [What]) + LEN('/portal/')
			ELSE 0
			END, LEN([What]))

--Updated Coentnet ITem ID's to Avoid dplicates
UPDATE DataToBeDecodedAux
SET ContentItemID = NullIf(left(right([From], ISNULL(NULLIF(charindex(REVERSE('/portal/asset/'), REVERSE([From])) - 1, - 1), len([From]))), coalesce(NULLIF(CHARINDEX('/', right([From], ISNULL(NULLIF(charindex(REVERSE('/portal/asset/'), REVERSE([From])) - 1, - 1), len([From])))) - 1, - 1), NULLIF(CHARINDEX('?', right([From], ISNULL(NULLIF(charindex(REVERSE('/portal/asset/'), REVERSE([From])) - 1, - 1), len([From])))) - 1, - 1), len(right([From], ISNULL(NULLIF(charindex(REVERSE('/portal/asset/'), REVERSE([From])) - 1, - 1), len([From])))))), '')
WHERE ContentItemID IS NULL
	AND ([From] LIKE '%/portal/asset/%');

--Updated Coentnet ITem ID's to Avoid dplicates
UPDATE DataToBeDecodedAux
SET ContentItemID = NullIf(left(right([From], ISNULL(NULLIF(charindex(REVERSE('/portal/program/'), REVERSE([From])) - 1, - 1), len([From]))), coalesce(NULLIF(CHARINDEX('/', right([From], ISNULL(NULLIF(charindex(REVERSE('/portal/program/'), REVERSE([From])) - 1, - 1), len([From])))) - 1, - 1), NULLIF(CHARINDEX('?', right([From], ISNULL(NULLIF(charindex(REVERSE('/portal/program/'), REVERSE([From])) - 1, - 1), len([From])))) - 1, - 1), len(right([From], ISNULL(NULLIF(charindex(REVERSE('/portal/program/'), REVERSE([From])) - 1, - 1), len([From])))))), '')
WHERE ContentItemID IS NULL
	AND ([From] LIKE '%/portal/program/%');

-------- begin final SP/script 
DROP INDEX IX_Decoded_Data ON DataToBeDecodedAux;

CREATE INDEX IX_Decoded_Data ON DataToBeDecodedAux (
	[What]
	,[Who]
	,[ContentItemID]
	)

----Attachments
--Asset Attachments
INSERT INTO #DomoDownloadsApiTemp (
	url
	,RegionId
	,ContentItemId
	,ContentItemTitle
	,ContentTypeId
	,ContentItemRegion
	,DownloadDate
	,[FileName]
	,SiteTrackingId
	,VisitId
	,Who
	)
SELECT CAST(d.What AS VARCHAR(255))
	,ci.RegionId
	,aa.ContentItemId
	,a.Title ContentItemTitle
	,ci.ContentTypeID
	,ci.RegionId ContentItemRegion
	,d.[When] DownloadDate
	,substring(aa.FileUrl, len(aa.FileUrl) + 2 - charindex('/', REVERSE(aa.FileUrl)), len(aa.FileUrl)) [Filename]
	,d.SiteTrackingID
	,d.VisitID
	,d.Who
FROM DataToBeDecodedAux d
JOIN AssetAttachment aa WITH (NOLOCK) ON (
		d.ContentItemID = aa.ContentItemId
		OR d.ContentItemID IS NULL
		)
	AND d.[What] LIKE 'file/download?url=' + aa.FileUrl + '%' --
JOIN Asset a WITH (NOLOCK) ON a.ContentItemId = aa.ContentItemId
JOIN ContentItem ci WITH (NOLOCK) ON ci.ContentItemID = aa.ContentItemId

--AssetFW Attachments
INSERT INTO #DomoDownloadsApiTemp (
	url
	,RegionId
	,ContentItemId
	,ContentItemTitle
	,ContentTypeId
	,ContentItemRegion
	,DownloadDate
	,[FileName]
	,SiteTrackingId
	,VisitId
	,Who
	)
SELECT CAST(d.What AS VARCHAR(255))
	,ci.RegionId
	,aa.ContentItemId
	,a.Title ContentItemTitle
	,ci.ContentTypeID
	,ci.RegionId ContentItemRegion
	,d.[When] DownloadDate
	,substring(aa.FileUrl, len(aa.FileUrl) + 2 - charindex('/', REVERSE(aa.FileUrl)), len(aa.FileUrl)) [Filename]
	,d.SiteTrackingID
	,d.VisitID
	,d.Who
FROM DataToBeDecodedAux d
JOIN AssetFullWidthAttachment aa WITH (NOLOCK) ON (
		d.ContentItemID = aa.ContentItemId
		OR d.ContentItemID IS NULL
		)
	AND d.[What] LIKE 'file/download?url=' + aa.FileUrl + '%'
JOIN AssetFullWidth a WITH (NOLOCK) ON a.ContentItemId = aa.ContentItemId
JOIN ContentItem ci WITH (NOLOCK) ON ci.ContentItemID = aa.ContentItemId

--DAsset Attachments
INSERT INTO #DomoDownloadsApiTemp (
	url
	,RegionId
	,ContentItemId
	,ContentItemTitle
	,ContentTypeId
	,ContentItemRegion
	,DownloadDate
	,[FileName]
	,SiteTrackingId
	,VisitId
	,Who
	)
SELECT CAST(d.What AS VARCHAR(255))
	,ci.RegionId
	,aa.ContentItemId
	,a.Title ContentItemTitle
	,ci.ContentTypeID
	,ci.RegionId ContentItemRegion
	,d.[When] DownloadDate
	,substring(aa.FileUrl, len(aa.FileUrl) + 2 - charindex('/', REVERSE(aa.FileUrl)), len(aa.FileUrl)) [Filename]
	,d.SiteTrackingID
	,d.VisitID
	,d.Who
FROM DataToBeDecodedAux d
JOIN DownloadableAssetAttachment aa WITH (NOLOCK) ON (
		d.ContentItemID = aa.ContentItemId
		OR d.ContentItemID IS NULL
		)
	AND d.[What] LIKE 'file/download?url=' + aa.FileUrl + '%'
JOIN DownloadableAsset a WITH (NOLOCK) ON a.ContentItemId = aa.ContentItemId
JOIN ContentItem ci WITH (NOLOCK) ON ci.ContentItemID = aa.ContentItemId

--OAsset Attachments
INSERT INTO #DomoDownloadsApiTemp (
	url
	,RegionId
	,ContentItemId
	,ContentItemTitle
	,ContentTypeId
	,ContentItemRegion
	,DownloadDate
	,[FileName]
	,SiteTrackingId
	,VisitId
	,Who
	)
SELECT CAST(d.What AS VARCHAR(255))
	,ci.RegionId
	,aa.ContentItemId
	,a.Title ContentItemTitle
	,ci.ContentTypeID
	,ci.RegionId ContentItemRegion
	,d.[When] DownloadDate
	,substring(aa.FileUrl, len(aa.FileUrl) + 2 - charindex('/', REVERSE(aa.FileUrl)), len(aa.FileUrl)) [Filename]
	,d.SiteTrackingID
	,d.VisitID
	,d.Who
FROM DataToBeDecodedAux d
JOIN OrderableAssetAttachment aa WITH (NOLOCK) ON (
		d.ContentItemID = aa.ContentItemId
		OR d.ContentItemID IS NULL
		)
	AND d.[What] LIKE 'file/download?url=' + aa.FileUrl + '%'
JOIN OrderableAsset a WITH (NOLOCK) ON a.ContentItemId = aa.ContentItemId
JOIN ContentItem ci WITH (NOLOCK) ON ci.ContentItemID = aa.ContentItemId

--OAsset Attachments
INSERT INTO #DomoDownloadsApiTemp (
	url
	,RegionId
	,ContentItemId
	,ContentItemTitle
	,ContentTypeId
	,ContentItemRegion
	,DownloadDate
	,[FileName]
	,SiteTrackingId
	,VisitId
	,Who
	)
SELECT CAST(d.What AS VARCHAR(255))
	,ci.RegionId
	,aa.ContentItemId
	,a.Title ContentItemTitle
	,ci.ContentTypeID
	,ci.RegionId ContentItemRegion
	,d.[When] DownloadDate
	,substring(aa.FileUrl, len(aa.FileUrl) + 2 - charindex('/', REVERSE(aa.FileUrl)), len(aa.FileUrl)) [FileName]
	,d.SiteTrackingID
	,d.VisitID
	,d.Who
FROM DataToBeDecodedAux d
JOIN ProgramAttachment aa WITH (NOLOCK) ON (
		d.ContentItemID = aa.ContentItemId
		OR d.ContentItemID IS NULL
		)
	AND d.[What] LIKE 'file/download?url=' + aa.FileUrl + '%'
JOIN Program a WITH (NOLOCK) ON a.ContentItemId = aa.ContentItemId
JOIN ContentItem ci WITH (NOLOCK) ON ci.ContentItemID = aa.ContentItemId

--Offer Attachments
INSERT INTO #DomoDownloadsApiTemp (
	url
	,RegionId
	,ContentItemId
	,ContentItemTitle
	,ContentTypeId
	,ContentItemRegion
	,DownloadDate
	,[FileName]
	,SiteTrackingId
	,VisitId
	,Who
	)
SELECT CAST(d.What AS VARCHAR(255))
	,ci.RegionId
	,aa.ContentItemId
	,a.Title ContentItemTitle
	,ci.ContentTypeID
	,ci.RegionId ContentItemRegion
	,d.[When] DownloadDate
	,substring(aa.FileUrl, len(aa.FileUrl) + 2 - charindex('/', REVERSE(aa.FileUrl)), len(aa.FileUrl)) [FileName]
	,d.SiteTrackingID
	,d.VisitID
	,d.Who
FROM DataToBeDecodedAux d
JOIN OfferAttachment aa WITH (NOLOCK) ON (
		d.ContentItemID = aa.ContentItemId
		OR d.ContentItemID IS NULL
		)
	AND d.[What] LIKE 'file/download?url=' + aa.FileUrl + '%'
JOIN Offer a WITH (NOLOCK) ON a.ContentItemId = aa.ContentItemId
JOIN ContentItem ci WITH (NOLOCK) ON ci.ContentItemID = aa.ContentItemId

----part two not attachments :Electronic Delivery
---Downlaodable Asset
------electronic DEliveries RElated To the DownloadableFile
SELECT CAST(d.What AS VARCHAR(255)) [What]
	,ci.RegionId
	,isnull(edcd.ContentITemId, aa.ContentItemId) ContentItemId
	,aa.Title ContentItemTitle
	,ci.ContentTypeID
	,ci.RegionId ContentItemRegion
	,d.[When] DownloadDate
	,substring(aa.[Filename], len(aa.[Filename]) + 2 - charindex('/', REVERSE(aa.[Filename])), len(aa.[Filename])) [FileName]
	,d.SiteTrackingID
	,d.VisitID
	,IsNull(d.Who, substring(IsNull(edcd.UserName, mailUser.UserName), charindex(':', IsNull(edcd.UserName, mailUser.UserName)) + 1, len(IsNull(edcd.UserName, mailUser.UserName)))) [Who]
INTO #ED_Find_user_AUX
FROM DataToBeDecodedAux d
JOIN ElectronicDelivery ed ON (
		d.[What] LIKE 'electronicdownload/' + cast(ed.ElectronicDeliveryID AS NVARCHAR(MAX)) + '%'
		OR d.[What] LIKE 'electronicdelivery/' + cast(ed.ElectronicDeliveryID AS NVARCHAR(MAX)) + '%'
		)
JOIN DownloadableAsset aa WITH (NOLOCK) ON (
		d.ContentItemID = aa.ContentItemId
		OR d.ContentItemID IS NULL
		)
	AND aa.[Filename] = ed.FileLocation
JOIN ContentItem ci WITH (NOLOCK) ON ci.ContentItemID = aa.ContentItemId
LEFT JOIN ElectronicDeliveryCreateData edcd WITH (NOLOCK) ON ed.ElectronicDeliveryID = edcd.ElectronicDeliveryID
LEFT JOIN MailDispatcher md WITH (NOLOCK) ON edcd.ElectronicDeliveryID IS NULL
	AND md.TrackingType = 'electronicdeliveryorderapproved'
	AND md.Body LIKE '%' + CAST(ed.ElectronicDeliveryID AS VARCHAR(255)) + '%'
	AND (
		ed.OrderItemID = 0
		OR ed.OrderItemID IS NULL
		)
	AND (
		md.UserId IS NOT NULL
		AND md.UserId > 0
		)
LEFT JOIN [User] mailUser ON mailUser.UserID = md.UserId

INSERT INTO #DomoDownloadsApiTemp (
	url
	,RegionId
	,ContentItemId
	,ContentItemTitle
	,ContentTypeId
	,ContentItemRegion
	,DownloadDate
	,[FileName]
	,SiteTrackingId
	,VisitId
	,Who
	)
SELECT d.What
	,RegionId
	,d.ContentItemId
	,d.ContentItemTitle
	,d.ContentTypeID
	,d.RegionId ContentItemRegion
	,d.DownloadDate
	,d.[FileName]
	,d.SiteTrackingID
	,d.VisitID
	,d.Who
FROM #ED_Find_user_AUX d

DROP TABLE #ED_Find_user_AUX

-----DownlaodableFile download 
INSERT INTO #DomoDownloadsApiTemp (
	url
	,RegionId
	,ContentItemId
	,ContentItemTitle
	,ContentTypeId
	,ContentItemRegion
	,DownloadDate
	,[FileName]
	,SiteTrackingId
	,VisitId
	,Who
	)
SELECT CAST(d.What AS VARCHAR(255))
	,ci.RegionId
	,aa.ContentItemId
	,aa.Title ContentItemTitle
	,ci.ContentTypeID
	,ci.RegionId ContentItemRegion
	,d.[When] DownloadDate
	,CASE 
		WHEN charindex('/', aa.[Filename]) > 0
			THEN substring(aa.[Filename], len(aa.[Filename]) + 2 - charindex('/', REVERSE(aa.[Filename])), len(aa.[Filename]))
		ELSE aa.[Filename]
		END [FileName]
	,d.SiteTrackingID
	,d.VisitID
	,d.Who
FROM DataToBeDecodedAux d
JOIN DownloadableAsset aa WITH (NOLOCK) ON (
		d.ContentItemID = aa.ContentItemId
		OR d.ContentItemID IS NULL
		)
	AND (
		d.What LIKE 'file/download?url=' + aa.[Filename] + '%'
		OR d.What LIKE 'file/LibraryDownload?url=' + aa.[Filename] + '%'
		)
JOIN ContentItem ci WITH (NOLOCK) ON ci.ContentItemID = aa.ContentItemId

--********------
--Orderable Assets Electronic Delivery
DECLARE @cleanEd TABLE (
	UserName NVARCHAR(255)
	,ContentItemId VARCHAR(25)
	,ElectronicDeliveryFile VARCHAR(max)
	,Title VARCHAR(max)
	)

INSERT INTO @cleanEd (
	UserName
	,ContentItemId
	,ElectronicDeliveryFile
	,Title
	)
SELECT substring(o.UserID, CHARINDEX(':', o.UserID) + 1, len(o.UserID)) userName
	,ItemID ContentItemId
	,cast(ed.ElectronicDeliveryID AS VARCHAR(max)) + '/' + oi.SKU ElectronicDeliveryFile
	,substring(ed.[FileLocation], len(ed.[FileLocation]) + 2 - charindex('/', REVERSE(ed.[FileLocation])), len(ed.[FileLocation])) title
FROM ElectronicDelivery ed WITH (NOLOCK)
INNER JOIN OrderItem oi WITH (NOLOCK) ON oi.OrderItemID = ed.OrderItemID
INNER JOIN [Order] o WITH (NOLOCK) ON oi.OrderID = o.OrderID

SELECT *
INTO #EdTemp
FROM (
	SELECT d.What
		,d.[ContentItemID]
		,d.[From]
		,d.[How]
		,d.RegionId
		,d.SiteTrackingID
		,d.VisitID
		,d.[When]
		,ISNULL(d.Who, CED.UserName) [Who]
	FROM DataToBeDecodedAux d
	JOIN @cleanEd CED ON (
			d.[What] LIKE 'electronicdelivery/' + CED.ElectronicDeliveryFile + '%'
			OR d.[What] LIKE 'electronicdownload/' + CED.ElectronicDeliveryFile + '%'
			)
	WHERE [What] LIKE '%electronic%'
	) b

INSERT INTO #DomoDownloadsApiTemp (
	url
	,RegionId
	,ContentItemId
	,ContentItemTitle
	,ContentTypeId
	,ContentItemRegion
	,DownloadDate
	,[FileName]
	,SiteTrackingId
	,VisitId
	,Who
	)
SELECT CAST(d.What AS VARCHAR(255))
	,ci.RegionId
	,aa.ContentItemId
	,oa.Title ContentItemTitle
	,ci.ContentTypeID
	,ci.RegionId ContentItemRegion
	,d.[When] DownloadDate
	,aa.Title [FileName]
	,d.SiteTrackingID
	,d.VisitID
	,d.Who
FROM #EdTemp d
JOIN @cleanEd aa ON (
		d.ContentItemID = aa.ContentItemId
		OR d.ContentItemID IS NULL
		)
	AND (
		d.[What] LIKE 'electronicdelivery/' + aa.ElectronicDeliveryFile + '%'
		OR d.[What] LIKE 'electronicdownload/' + aa.ElectronicDeliveryFile + '%'
		)
JOIN ContentItem ci WITH (NOLOCK) ON ci.ContentItemID = aa.ContentItemId
JOIN OrderableAsset oa WITH (NOLOCK) ON oa.ContentItemId = ci.ContentItemID

DROP TABLE #EdTemp;

---END Related to ED
---htmldownloads
INSERT INTO #DomoDownloadsApiTemp (
	url
	,RegionId
	,ContentItemId
	,ContentItemTitle
	,ContentTypeId
	,ContentItemRegion
	,DownloadDate
	,[FileName]
	,SiteTrackingId
	,VisitId
	,Who
	)
SELECT CAST(d.What AS VARCHAR(255))
	,ci.RegionId
	,aa.ContentItemID
	,aa.Title ContentItemTitle
	,ci.ContentTypeID
	,ci.RegionId ContentItemRegion
	,d.[When] DownloadDate
	,substring(aa.[Filename], len(aa.[Filename]) + 2 - charindex('/', REVERSE(aa.[Filename])), len(aa.[Filename])) [FileName]
	,d.SiteTrackingID
	,d.VisitID
	,d.Who
FROM DataToBeDecodedAux d
JOIN HtmlDownload aa WITH (NOLOCK) ON d.[What] LIKE 'file/htmldownload/' + aa.ContentItemID + '%'
JOIN ContentItem ci ON ci.ContentItemID = aa.ContentItemID

-----
INSERT INTO DomoDownloadsApi (
	ContentItemId
	,ContentItemTitle
	,ContentItemRegion
	,ContentTypeId
	,ContentTypeTitle
	,UserRegion
	,DownloadDate
	,[FileName]
	,SiteTrackingId
	,VisitId
	,Country
	,UserEmail
	,UserFinancialInstitution
	,UserFinancialInstitutionID
	,UserFinancialInstitutionCID
	,UserId
	,HiddenResult
	,UserFinancialInstitutionImportedName
	,UserFirstName
	,UserLastName
	)
SELECT DISTINCT d.ContentItemId
	,d.ContentItemTitle
	,d.ContentItemRegion
	,ct.ContentTypeID ContentTypeId
	,ct.Title ContentTypeTitle
	,ISNULL(u.Region, '') RegionId
	,d.DownloadDate
	,d.[FileName]
	,d.SiteTrackingId
	,d.VisitId
	,coalesce(t.DisplayName, tt.DisplayName, '') Country
	,coalesce(u.Email, '') UserEmail
	,coalesce(i.Title, '') UserFinancialInstitution
	,i.IssuerID
	,i.CID
	,u.UserID
	,(isnull(i.ExcludedFromDomoApi, 0) | isnull(u.ExcludedFromDomoApi, 0) | IIF(CHARINDEX('@', u.Email) > 0, 0, 1)) HiddenResult
	,ISNULL(LegalName, i.Title) [UserIssuerImportedName]
	,p.FirstName
	,p.LastName
FROM #DomoDownloadsApiTemp d
LEFT JOIN [User] u WITH (NOLOCK) ON d.Who = u.UserName collate SQL_Latin1_General_CP1_CI_AS
LEFT JOIN UserProfile p WITH (NOLOCK) ON p.UserId = u.UserId
LEFT JOIN Issuer i WITH (NOLOCK) ON i.IssuerID = u.IssuerId collate SQL_Latin1_General_CP1_CI_AS
LEFT JOIN ContentType ct WITH (NOLOCK) ON ct.ContentTypeID = d.ContentTypeId collate SQL_Latin1_General_CP1_CI_AS
LEFT JOIN Tag t WITH (NOLOCK) ON t.Identifier = u.Country collate SQL_Latin1_General_CP1_CI_AS
LEFT JOIN TagTranslatedContent tt WITH (NOLOCK) ON t.TagID = tt.TagId
	AND LanguageCode = 'en' collate SQL_Latin1_General_CP1_CI_AS
LEFT JOIN ImportedIca ii ON i.CID = ii.CID
WHERE SiteTrackingId NOT IN (
		SELECT SiteTrackingId
		FROM DomoDownloadsApi
		)
	AND d.ContentItemId NOT LIKE '%-p'

DROP TABLE #DomoDownloadsApiTemp;

UPDATE dd
SET dd.HiddenResult = (u.ExcludedFromDomoApi | i.ExcludedFromDomoApi)
FROM [DomoDownloadsApi] dd
JOIN [User] u ON u.Email = dd.UserEmail
JOIN [Issuer] i ON i.IssuerID = dd.UserFinancialInstitutionID

RETURN 0