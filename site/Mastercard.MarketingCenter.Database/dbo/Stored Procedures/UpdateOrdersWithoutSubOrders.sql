﻿
CREATE PROCEDURE [dbo].[UpdateOrdersWithoutSubOrders]

as
begin
	
	insert into SubOrder
	select	distinct o.OrderID,
			oi.ShippingInformationID,
			o.PriceAdjustment,
			o.ShippingCost,
			o.PostageCost,
			o.[Description],
			si.OrderTrackingNumber TrackingNumber,
			case 
				when si.OrderTrackingNumber is not null and
				si.OrderTrackingNumber != '' then cast(1 as bit)
				else cast(0 as bit)
				end Completed,
			case 
				when si.OrderTrackingNumber is not null and
				si.OrderTrackingNumber != '' then o.OrderDate
				else null
			end DateCompleted
	from	[Order] o,
			[OrderItem] oi,
			[ShippingInformation] si
	where	oi.OrderID = o.OrderID and
			oi.ShippingInformationID = si.ShippingInformationID and
			not exists(			
			select	OrderID
			from	SubOrder so
			where	so.OrderID = o.OrderID)
	order by o.OrderID
	
	update oi
	set	SubOrderID = so.SubOrderID
	from [OrderItem] oi,
			SubOrder so
	where	oi.ShippingInformationID = so.ShippingInformationID and
			oi.OrderID = so.OrderID
	
	
end
