﻿CREATE PROCEDURE [dbo].[RetrieveIssuerMatchingReport]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT
ica.CID,
ica.LegalName AS 'IssuerLegalName',
ica.AffiliateBIN AS 'BIN', 
CASE
	WHEN i.CID IS NULL
	THEN 'No'
	ELSE 'Yes'
END AS 'Matched',
i.Title AS 'IssuerName',
CASE
	WHEN mh.CID IS NULL
	THEN 'No'
	ELSE 'Yes'
END AS 'HeatmapData'
 
FROM (select * from Issuer WHERE RegionId = 'us') i
RIGHT JOIN ImportedIca ica on i.CID = ica.CID
LEFT JOIN MetricHeatmap mh ON ica.CID = mh.CID

GROUP BY ica.CID, ica.LegalName, ica.AffiliateBIN, i.CID, i.Title, mh.CID
ORDER BY ica.CID

END

