﻿CREATE PROCEDURE [dbo].[RetrievePromotionReport]	
	@Promotion VARCHAR(500)
AS
BEGIN

	SELECT	o.OrderNumber,			
			u.FullName,
			OrderDate,
			ISNULL(SUM(oi.ItemPrice) + ISNULL(sos.Cost, 0) + ISNull(o.PriceAdjustment,0) + ISnull(o.ShippingCost,0) + ISnull(o.PostageCost,0), 0) AS Total,
			p.Title Processor,
			i.IssuerID,
			i.Title Issuer,
			ISNULL(PromotionAmount,0) DiscountTotal,
			PromotionCode Promotion
	FROM	[Order] o join
			[User] u on 
				o.UserID = u.UserName join
			[IssuerProcessor] ip on
				u.IssuerId = ip.IssuerID join
			[Processor] p on 
				ip.ProcessorID = p.ProcessorID join 
			[Issuer] i on u.IssuerId = i.IssuerID left outer join
			[OrderItem] oi on
				oi.OrderID = o.OrderID left outer join
			(SELECT	so.OrderID,
					SUM(isnull(so.PriceAdjustment,0)) + SUM(isnull(so.ShippingCost,0)) + SUM(isnull(so.PostageCost,0)) Cost
			 FROM		[SubOrder] so
			 GROUP BY	so.OrderID) sos on
				sos.OrderID = o.OrderID
	WHERE PromotionCode = @Promotion
	GROUP BY o.OrderNumber,
			 u.FullName,
			 OrderDate,
			 p.Title,			
			 i.IssuerID,
			 i.Title,	
			 PromotionAmount,		 
			 PromotionCode,
			 PromotionDescription,
			 o.PriceAdjustment,
			 o.ShippingCost,
			 o.PostageCost,
			 sos.Cost
END