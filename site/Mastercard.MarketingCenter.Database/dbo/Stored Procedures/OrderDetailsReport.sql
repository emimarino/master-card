﻿CREATE PROCEDURE [dbo].[OrderDetailsReport]
(
@Date datetime
)
as
	
	SELECT	o.OrderNumber, 
			o.OrderDate, 
			p.Title processor, 
			i.Title IssuerName,
			u.FullName, 
			sos.Cost + soship.Cost + ISNull(o.PriceAdjustment,0) AS Total,
			isnull(o.PromotionAmount, 0) PromotionAmount,
			i.BillingID,
			(select count(*)
			 from	SubOrder so2
			 where  so2.OrderID = o.OrderID and
					Month(so2.DateCompleted) = Month(@Date) and 
					Year(so2.DateCompleted) = Year(@Date)) CompleteSubOrders,
			(select count(*)
			 from	SubOrder so2
			 where  so2.OrderID = o.OrderID) NumberSubOrders,
			(select min(DateCompleted)
			 from	SubOrder so2
			 where  so2.OrderID = o.OrderID and
					o.OrderStatus = 'Shipped') FirstSubOrderDateCompleted,
			(select max(DateCompleted)
			 from	SubOrder so2
			 where  so2.OrderID = o.OrderID and
					o.OrderStatus = 'Shipped') LastSubOrderDateCompleted
	FROM	[Order] o,
			[User] u,
			[IssuerProcessor] ip,
			[Issuer] i,
			[Processor] p,
			(select	so.OrderID,
					SUM(oi.ItemPrice) Cost
			 from	SubOrder so,
					[OrderItem] oi
			 where	so.SubOrderID = oi.SubOrderID and
					Month(so.DateCompleted) = Month(@Date) and Year(so.DateCompleted) = Year(@Date)
			 group by so.OrderID) sos,
			(select	so.OrderID,
					SUM(isnull(so.PriceAdjustment,0)) + SUM(isnull(so.ShippingCost,0)) + SUM(isnull(so.PostageCost,0)) Cost
			 from	SubOrder so
			 where	Month(so.DateCompleted) = Month(@Date) and Year(so.DateCompleted) = Year(@Date)
			 group by so.OrderID) soship
	where	o.UserID = u.UserName and
			p.ProcessorID = ip.ProcessorID and
			ip.IssuerID = u.IssuerId and
			i.IssuerID = ip.IssuerID and
			sos.OrderID = o.OrderID and
			soship.OrderID = o.OrderID and
			o.OrderStatus != 'Cancelled'
	GROUP BY o.OrderNumber, o.DateCompleted, o.OrderDate, p.Title, i.Title, o.OrderID,
			 u.FullName, i.Title, i.BillingID, o.PromotionAmount, o.OrderStatus, o.PriceAdjustment,o.ShippingCost,o.PostageCost, sos.Cost, soship.Cost
	order by p.Title, i.Title, u.FullName, o.OrderDate
