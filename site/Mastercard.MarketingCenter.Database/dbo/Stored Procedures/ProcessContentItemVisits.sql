﻿CREATE PROCEDURE [dbo].[ProcessContentItemVisits]
	WITH RECOMPILE
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @retentionMonths INT = 6
		,@minSiteTrackingID BIGINT
		,@maxSiteTrackingID BIGINT
		,@borderDate DATETIME
		,@rows BIGINT = 0
		,@dt DATETIME = sysdatetime();

	BEGIN TRY
		SET @borderdate = (
				SELECT min([When])
				FROM SiteTracking
				WHERE [When] >= cast(dateadd(month, - 1 * @retentionMonths, sysdatetime()) AS DATE)
				);
		SET @borderdate = isnull(@borderDate, cast(dateadd(month, - 1 * @retentionMonths, sysdatetime()) AS DATE));
		SET @minSiteTrackingID = isnull((
					SELECT min(SiteTrackingID)
					FROM SiteTracking
					WHERE [When] = @borderDate
					), 0);
		SET @maxSiteTrackingID = isnull((
					SELECT max(SiteTrackingID)
					FROM SiteTracking
					), 0);

		BEGIN TRANSACTION

		TRUNCATE TABLE ContentItemVisits;

		INSERT INTO ContentItemVisits (
			ContentItemId
			,Visits
			)
		SELECT ci.ContentItemID
			,count(ci.ContentItemId) AS Visits
		FROM ContentItem AS ci
		INNER JOIN SiteTracking AS st ON ci.ContentItemID = st.ContentItemID
		WHERE ci.ContentTypeID != '1'
			AND st.SiteTrackingID BETWEEN @minSiteTrackingID
				AND @maxSiteTrackingID
		GROUP BY ci.ContentItemID
		
		UNION
		
		SELECT pa.ContentItemId
			,count([What]) AS Visits
		FROM [Page] AS pa
		INNER JOIN SiteTracking AS st ON replace(LEFT(st.what, CHARINDEX('?', st.what + '?') - 1), '/', '') = 'portal' + replace(pa.Uri, '/', '')
		WHERE st.SiteTrackingID BETWEEN @minSiteTrackingID
				AND @maxSiteTrackingID
		GROUP BY pa.ContentItemId;

		SET @rows = @@rowcount;

		COMMIT;

		PRINT '-->>-- dbo.ProcessContentItemVisits: ' + ltrim(@rows) + ' row(s) inserted in ' + ltrim(datediff(ms, @dt, sysdatetime()) / 1000.0) + ' sec.';
	END TRY

	BEGIN CATCH
		IF xact_state() > 0
			ROLLBACK;

		PRINT '--!!-- ERROR in dbo.ProcessContentItemVisits';

		throw;
	END CATCH;
END;