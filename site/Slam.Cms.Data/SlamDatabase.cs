﻿using Mastercard.MarketingCenter.Common.Extensions;
using Slam.Cms.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Text;

namespace Slam.Cms.Data
{
    public class SlamDatabase
    {
        private readonly SlamContext _slamContext;
        private TimeSpan _lastQueryExecutionTime;

        internal SlamDatabase(SlamContext slamContext)
        {
            Ensure.ArgumentNotNull(slamContext, "slamContext");
            _slamContext = slamContext;
        }

        public TimeSpan LastCommandExecutionTime
        {
            get
            {
                return _lastQueryExecutionTime;
            }
        }

        public void ExecuteReader(string sql, Action<IDataReader> action)
        {
            Ensure.ArgumentNotNullOrEmpty(sql, "sql");
            Ensure.ArgumentNotNull(action, "action");

            DoWithConnection(sql, null, connection =>
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = sql;

                    using (var reader = cmd.ExecuteReader())
                    {
                        action(reader);
                    }
                }
            });
        }

        public IList<T> ExecuteReader<T>(string sql) where T : class, new()
        {
            Ensure.ArgumentNotNullOrEmpty(sql, "sql");

            IList<T> result = new List<T>();

            DoWithConnection(sql, null, connection =>
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = sql;

                    using (var reader = cmd.ExecuteReader())
                    {
                        var deserializer = Deserializer.GetClassDeserializer<T>(reader);

                        while (reader.Read())
                        {
                            result.Add(deserializer(reader));
                        }
                    }
                }
            });

            return result;
        }

        public object ExecuteScalar(string sql)
        {
            Ensure.ArgumentNotNullOrEmpty(sql, "sql");

            object result = null;

            DoWithConnection(sql, null, connection =>
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = sql;
                    result = cmd.ExecuteScalar();
                }
            });

            return result;
        }

        public int ExecuteNonQuery(string sql)
        {
            Ensure.ArgumentNotNullOrEmpty(sql, "sql");

            int result = 0;

            DoWithConnection(sql, null, connection =>
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = sql;
                    result = cmd.ExecuteNonQuery();
                }
            });

            return result;
        }

        public IList<T> RetrieveAll<T>(string tableName = null, string orderBy = "") where T : class, new()
        {
            if (tableName == null)
            {
                tableName = typeof(T).Name;
            }

            if (tableName.IsNullOrEmpty())
            {
                throw new ArgumentException("The table name is not valid. If you pass null the method will use the generic type name as table name.", "tableName");
            }

            tableName = tableName.Trim();

            if (!tableName.StartsWith("["))
            {
                tableName = "[{0}]".F(tableName);
            }

            var sb = new StringBuilder();
            sb.Append(@"select * from {0} with (nolock)".F(tableName));

            if (!orderBy.IsNullOrEmpty())
            {
                sb.Append(@" order by {0}".F(orderBy));
            }

            return Retrieve<T>(sb.ToString());
        }

        public IList<T> Retrieve<T>(string sql) where T : class, new()
        {
            return ExecuteReader<T>(sql);
        }

        protected void DoWithConnection(string sql, object param, Action<IDbConnection> action, Action<DbException> onError = null)
        {
            Ensure.ArgumentNotNull(action, "action");

            var connection = GetConnection();

            var stopwatch = Stopwatch.StartNew();

            using (connection.EnsureOpen())
            {
                try
                {
                    action(connection);
                    stopwatch.Stop();
                }
                catch (DbException ex)
                {
                    stopwatch.Stop();
                    ex.Data.Add("SQL", sql);
                    ex.Data.Add("SQLParam", param);

                    if (onError != null)
                    {
                        onError(ex);
                    }
                    else
                    {
                        throw;
                    }
                }
                finally
                {
                    _lastQueryExecutionTime = stopwatch.Elapsed;
                }
            }
        }

        protected IDbConnection GetConnection()
        {
            var connectionFactory = _slamContext.ConnectionFactory;

            if (connectionFactory == null)
            {
                throw new Exception("No database connection factory was specified");
            }

            var connection = connectionFactory();

            if (connection == null)
            {
                throw new Exception("The database connection could not be opened");
            }

            return connection;
        }
    }
}