﻿namespace Slam.Cms.Data
{
    public enum SlamQueryCacheBehavior
    {
        Default = 0,
        NoCache = 1
    }
}