﻿namespace Slam.Cms.Data
{
    internal static class Constants
    {
        public const string ContentTypeDiscriminatorFieldName = "ContentType";

        public static class Cache
        {
            public const string Query = "Query";
        }
    }
}