﻿using System;

namespace Slam.Cms.Data
{
    [AttributeUsage(AttributeTargets.Class)]
    public class SlamTableAttribute : Attribute
    {
        public string TableName { get; set; }
    }
}