﻿using Mastercard.MarketingCenter.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Slam.Cms.Data
{
    internal class SlamQueryBuilder
    {
        private StringBuilder tempTablesBuilder;
        private StringBuilder firstStepSelectBuilder;
        private StringBuilder firstStepFromBuilder;
        private StringBuilder firstStepWhereBuilder;
        private StringBuilder firstStepOrderBuilder;
        private StringBuilder secondStepBuilder;
        private TimeSpan queryBuildElapsedTime;
        private readonly SlamContext slamContext;
        private readonly ICachingService cachingService;
        private SlamQueryParameters parameters;

        public SlamQueryBuilder(SlamContext slamContext)
        {
            this.slamContext = slamContext;
            this.cachingService = slamContext.CachingService;
        }

        public TimeSpan QueryBuildElapsedTime
        {
            get
            {
                return this.queryBuildElapsedTime;
            }
        }

        public string BuildQuery(SlamQueryParameters parameters)
        {
            this.parameters = parameters;

            var stopWatch = Stopwatch.StartNew();

            // temp tables
            this.tempTablesBuilder = new StringBuilder();
            this.AddContentItemForTags();
            this.AddContentItemForFeatureTags();
            this.AddContentItemForFeatureLocations();
            this.AddContentItemForSegmentations();
            this.AddRelatedToContentItem();
            this.AddRelatedToTags();

            var firstStep = this.FirtStep();
            var secondStep = this.SecondStep();

            // render
            var tempTables = this.tempTablesBuilder.Length > 0 ? "with {0}".F(this.tempTablesBuilder.ToString().TrimStart(',')) : string.Empty;
            var sql = tempTables +
                      Environment.NewLine +
                      firstStep +
                      Environment.NewLine +
                      secondStep;

            stopWatch.Stop();

            this.queryBuildElapsedTime = stopWatch.Elapsed;

            return sql;
        }

        protected string FirtStep()
        {
            // select
            this.firstStepSelectBuilder = new StringBuilder();
            this.firstStepSelectBuilder.Append(@"select ");

            if (this.parameters.CountOnly)
            {
                this.firstStepSelectBuilder.AppendLine(@" count(1)");
            }
            else
            {
                this.AddTake();
                this.AddSelectContentTypeDiscriminator();
                this.AddSelectFields(typeof(ContentItem));

                foreach (var contentType in this.slamContext.KnownContentTypes)
                {
                    this.AddSelectFields(contentType);
                }

                this.AddSelectFeatureLevelForOrdering();
            }

            // into - from
            this.firstStepFromBuilder = new StringBuilder();
            this.firstStepFromBuilder.AppendLine(@"from ContentItem with (nolock)");

            if (this.parameters.TaggedWithAll.Any() || this.parameters.TaggedWithAny.Any())
            {
                this.firstStepFromBuilder.AppendLine(@"inner join ContentItemWithTags cift with (nolock) on cift.ContentItemId = ContentItem.ContentItemId");
            }

            if (this.parameters.FeatureTaggedWithAny.Any() || this.parameters.FeatureTaggedWithAll.Any())
            {
                this.firstStepFromBuilder.AppendLine(@"inner join ContentItemForFeatureTags cifft with (nolock) on cifft.ContentItemId = ContentItem.ContentItemId");
            }

            if (this.parameters.FeatureLocatedWith != null)
            {
                this.firstStepFromBuilder.AppendLine(@"inner join ContentItemForFeatureLocations cifl with (nolock) on cifl.ContentItemId = ContentItem.ContentItemId");
            }

            if (!this.parameters.RelatedToContentItemId.IsNullOrWhiteSpace() || this.parameters.RelatedToTags != null)
            {
                this.firstStepFromBuilder.AppendLine(@"inner join RelatedContentItems rci with (nolock) on rci.ContentItemId = ContentItem.ContentItemId");
            }

            if (this.parameters.SegmentedWithAny.Any() || this.parameters.SegmentedWithAll.Any() || this.parameters.IncludeOnlyEmptySegmentations)
            {
                this.firstStepFromBuilder.AppendLine(@"inner join ContentItemForSegmentations cifs with (nolock) on cifs.ContentItemId = ContentItem.ContentItemId");
            }

            foreach (var contentType in this.slamContext.KnownContentTypes)
            {
                this.AddContentItemChildTable(contentType);
            }

            // where
            this.firstStepWhereBuilder = new StringBuilder();
            this.FilterStatus();
            this.FilterContentTypes();
            this.FilterTagCategory();
            this.FilterFeaturedOnTag();
            this.FilterFeaturedOnLocation();
            this.FilterAdditional();
            this.FilterPrimaryContentItemsIds();
            this.FilterRegion();
            this.FilterExpired();

            // order by
            this.firstStepOrderBuilder = new StringBuilder();
            this.AddOrder();

            return this.firstStepSelectBuilder.ToString().TrimEnd().TrimEnd(',') + Environment.NewLine +
                   this.firstStepFromBuilder.ToString() +
                   this.firstStepWhereBuilder.ToString() + Environment.NewLine +
                   this.firstStepOrderBuilder.ToString();
        }

        protected string SecondStep()
        {
            this.secondStepBuilder = new StringBuilder();

            this.AddTags();
            this.AddFeatureTags();
            this.AddFeatureLocations();
            this.AddSegmentations();

            return this.secondStepBuilder.ToString();
        }

        protected void AddTake()
        {
            if (this.parameters.Take > 0)
            {
                this.firstStepSelectBuilder.AppendLine(@"top {0}".F(this.parameters.Take));
            }
        }

        protected string GetTableName(Type type)
        {
            if (type == null)
            {
                return null;
            }

            var slamTableAttribute = type.GetCustomAttribute<SlamTableAttribute>();
            if (slamTableAttribute != null)
            {
                return slamTableAttribute.TableName;
            }

            return type.Name;
        }

        protected void AddContentItemChildTable(Type type)
        {
            var tableName = this.GetTableName(type);
            this.firstStepFromBuilder.AppendLine(@"left join {0} with (nolock) on {0}.ContentItemId = ContentItem.ContentItemId".F(tableName));
        }

        protected void AddSelectContentTypeDiscriminator()
        {
            var discriminatorBuilder = new StringBuilder();
            discriminatorBuilder.Append(@"case ");

            foreach (var contentType in this.slamContext.KnownContentTypes.GetTypes())
            {
                var tableName = this.GetTableName(contentType);
                discriminatorBuilder.AppendLine(@"when {0}.ContentItemId is not null then '{1}' ".F(tableName, contentType.Name));
            }

            discriminatorBuilder.AppendLine(@"end {0},".F(Constants.ContentTypeDiscriminatorFieldName));

            this.firstStepSelectBuilder.Append(discriminatorBuilder.ToString());
        }

        protected void AddSelectFields(Type type)
        {
            var tableName = this.GetTableName(type);
            foreach (var property in this.GetContentItemProperties(type))
            {
                this.firstStepSelectBuilder.AppendLine(@"[{0}].[{1}] '{0}.{1}',".F(tableName, property));
            }
        }

        protected void AddSelectFeatureLevelForOrdering()
        {
            if (!this.parameters.OrderByFeatureOnTagLevel.IsNullOrWhiteSpace())
            {
                this.firstStepSelectBuilder.AppendLine(@"isnull((select FeatureLevel 
                                                    from ContentItemFeatureOnTag cift with (nolock)
                                                    inner join Tag t with (nolock) on t.TagID = cift.TagID
                                                    where cift.ContentItemId = ContentItem.ContentItemId 
                                                    and t.Identifier = '{0}'), -10) FeatureLevel,".F(this.parameters.OrderByFeatureOnTagLevel));
            }
            else if (!this.parameters.OrderByFeatureOnLocationLevel.IsNullOrWhiteSpace())
            {
                this.firstStepSelectBuilder.AppendLine(@"isnull((select FeatureLevel
                                                    from ContentItemFeatureLocation cifl with (nolock)
                                                    where cifl.ContentItemId = ContentItem.ContentItemId
                                                    and cifl.FeatureLocationId = '{0}'), -10) FeatureLevel,".F(this.parameters.OrderByFeatureOnLocationLevel));
            }
            else
            {
                this.firstStepSelectBuilder.AppendLine(@"-10 FeatureLevel,");
            }
        }

        protected void AddSelect()
        {
            if (this.parameters.RelatedToContentItemId.IsNullOrWhiteSpace())
            {
                this.firstStepSelectBuilder.AppendLine(@"0 SharedTags,");
            }
            else
            {
                this.firstStepSelectBuilder.AppendLine(@"rci.SharedTags,");
            }
        }

        protected void AddTags()
        {
            if (!this.parameters.IncludeTags)
            {
                return;
            }

            this.secondStepBuilder.AppendLine(@"select cit.ContentItemId, cit.TagID
                                               from ContentItemTag cit");
        }

        protected void AddFeatureTags()
        {
            if (!this.parameters.IncludeFeatureTags)
            {
                return;
            }

            this.secondStepBuilder.AppendLine(@"select cit.ContentItemId, cit.TagID, cit.FeatureLevel
                                               from ContentItemFeatureOnTag cit");
        }

        protected void AddFeatureLocations()
        {
            if (!this.parameters.IncludeFeatureLocations)
            {
                return;
            }

            this.secondStepBuilder.AppendLine(@"select cifl.ContentItemId, cifl.FeatureLocationId, cifl.FeatureLevel
                                                from ContentItemFeatureLocation cifl");
        }

        protected void AddSegmentations()
        {
            if (!this.parameters.IncludeSegmentations)
            {
                return;
            }

            this.secondStepBuilder.AppendLine(@"select cis.ContentItemId, cis.SegmentationId
                                                from ContentItemSegmentation cis");
        }

        protected void AddOrder()
        {
            if (this.parameters.CountOnly)
            {
                return;
            }

            if (!this.parameters.OrderBy.IsNullOrWhiteSpace())
            {
                this.firstStepOrderBuilder.AppendLine(@"order by {0}".F(this.parameters.OrderBy));
            }
            else if (!this.parameters.RelatedToContentItemId.IsNullOrWhiteSpace() || this.parameters.RelatedToTags != null)
            {
                this.firstStepOrderBuilder.AppendLine(@"order by SharedTags desc, 'ContentItem.ModifiedDate' desc");
            }
            else if (!this.parameters.OrderByFeatureOnTagLevel.IsNullOrWhiteSpace() || !this.parameters.OrderByFeatureOnLocationLevel.IsNullOrWhiteSpace())
            {
                this.firstStepOrderBuilder.AppendLine(@"order by FeatureLevel desc, 'ContentItem.ModifiedDate' desc");
            }
            else
            {
                this.firstStepOrderBuilder.AppendLine(@"order by 'ContentItem.ModifiedDate' desc");
            }
        }

        protected void FilterStatus()
        {
            switch (this.parameters.Status)
            {
                case Data.FilterStatus.None:
                    this.firstStepWhereBuilder.AppendLine(@"where 1 = 1");
                    break;
                case Data.FilterStatus.Live:
                    this.firstStepWhereBuilder.AppendLine(string.Format(@"where ContentItem.StatusID = {0}", Status.Published));
                    break;
                case Data.FilterStatus.PreviewMode:
                    this.firstStepWhereBuilder.AppendLine(
                        string.Format(@"where (ContentItem.StatusID not in ({0},{1}) or not exists (select 1 from ContentItem c1 with (nolock) where c1.PrimaryContentItemId = ContentItem.ContentItemId and c1.StatusID != {0} and c1.StatusID != {2}))",
                            Status.Published, Status.Archived, Status.Deleted));
                    break;
                case Data.FilterStatus.LiveOrRetired:
                    this.firstStepWhereBuilder.AppendLine(
                        string.Format(@"where ContentItem.StatusID in ({0}, {1})",
                        Status.Published, Status.Archived));
                    break;
                case Data.FilterStatus.LiveOrExpired:
                    this.firstStepWhereBuilder.AppendLine(
                        string.Format(@"where ContentItem.StatusID in ({0}, {1})",
                        Status.Published, Status.Expired));
                    break;
                case Data.FilterStatus.LiveOrLiveAndDraft:
                    this.firstStepWhereBuilder.AppendLine(
                        string.Format(@"where (ContentItem.StatusID in ({0}, {1}) and ContentItem.ContentItemId not like '%-p')",
                        Status.Published, Status.PublishedAndDraft));
                    break;
                case Data.FilterStatus.DraftIfCurrent: /* Will only retrieve the draft if there is was no publish done */
                    this.firstStepWhereBuilder.AppendLine(
                           string.Format(@"where ( (ContentItem.StatusID ={0} or (ContentItem.StatusID= {1} and   ContentItem.ContentItemId like '%-p')) and not exists( select 1 from ContentItem c1 with (nolock) where 
                                c1.ContentItemId <> ContentItem.ContentItemId and
                            c1.PrimaryContentItemId = ContentItem.PrimaryContentItemId and c1.ModifiedDate >= ContentItem.ModifiedDate )  )",
                           Status.DraftOnly, Status.PublishedAndDraft));
                    break;
                case Data.FilterStatus.DraftOnlyOrLiveAndDraft:
                    this.firstStepWhereBuilder.AppendLine(
                        string.Format(@"where (ContentItem.StatusID = {0} or ( ContentItem.StatusID={1} and ContentItem.ContentItemId like '%-p' ) )",
                        Status.DraftOnly, Status.PublishedAndDraft));
                    break;
                case Data.FilterStatus.LatestVersion: /* Retrives the latest Version of the Content Item Wich is not either Archived, Deleted, or Expired.*/
                    this.firstStepWhereBuilder.AppendLine(
                        $@"where (ContentItem.ContentItemId=  ( select top 1 ContentItemId from ContentItem c1 with (nolock) where 
                            c1.PrimaryContentItemId = ContentItem.PrimaryContentItemId and c1.StatusID not in({ Status.Deleted},{Status.Archived},{Status.Expired}) order by c1.ModifiedDate desc) )"
                                  );
                    break;
                case Data.FilterStatus.GetAll:
                    this.firstStepWhereBuilder.AppendLine(@"where 1 = 1");
                    return;
                default:
                    this.firstStepWhereBuilder.AppendLine(@"where 1 = 1");
                    break;
            }

            if (this.parameters.isExpired == null || !this.parameters.isExpired.Value)
            {
                this.firstStepWhereBuilder.AppendLine(
                    string.Format(@" and ContentItem.StatusID not in ({0},{1})",
                    Status.Deleted, Status.Archived));
            }
            else
            {
                this.firstStepWhereBuilder.AppendLine(
                    string.Format(@" and ContentItem.StatusID not in({0},{1},{2})",
                    Status.Deleted, Status.Archived, Status.Expired));
            }
        }

        protected void FilterContentTypes()
        {
            if (this.parameters.ContentTypes.Count == 0)
            {
                return;
            }

            var items = this.parameters.ContentTypes.Select(i => string.Format("{0}.ContentItemId", this.GetTableName(i.Value)));
            var serializedContentTypes = this.SerializeIn(items, false);
            this.firstStepWhereBuilder.AppendLine(@"and coalesce({0}, '') <> ''".F(serializedContentTypes));
        }
        protected void FilterTagCategory()
        {
            if (this.parameters.TagCategoryIds.Count == 0)
            {
                return;
            }

            var serializedTags = this.SerializeIn(this.parameters.TagCategoryIds);
            string filterTagCategory = @" and exists(select 1
                                                     from ContentItemFeatureOnTag cift with(nolock)
                                                     inner join Tag t with(nolock) on t.TagID = cift.TagID
                                                     inner join TagHierarchyPosition tp  with(nolock) on t.TagID = tp.TagID
                                                     and (TagCategoryID in ({0}))
                                                     and cift.FeatureLevel >= 0
                                                     and
                                                     (AssessmentCriteria.ContentItemId is not null
                                                     or Asset.ContentItemId is not null
                                                     or AssetFullWidth.ContentItemId is not null
                                                     or Recommendation.ContentItemId is not null
                                                     or DownloadableAsset.ContentItemId is not null
                                                     or HtmlDownload.ContentItemId is not null
                                                     or QuickLink.ContentItemId is not null
                                                     or Link.ContentItemId is not null
                                                     or MarqueeSlide.ContentItemId is not null
                                                     or OrderableAsset.ContentItemId is not null
                                                     or Page.ContentItemId is not null
                                                     or Program.ContentItemId is not null
                                                     or Snippet.ContentItemId is not null
                                                     or UpcomingEvent.ContentItemId is not null
                                                     or Webinar.ContentItemId is not null
                                                     or YoutubeVideo.ContentItemId is not null)
                                                     )".F(serializedTags);

            this.firstStepWhereBuilder.AppendLine(filterTagCategory);
        }

        protected void FilterFeaturedOnTag()
        {
            if (this.parameters.FeaturedOnTag.Count == 0)
            {
                return;
            }

            foreach (var featuredOnTag in this.parameters.FeaturedOnTag)
            {
                var filterOperator = this.GetOperator(featuredOnTag.FilterOperator);

                this.firstStepWhereBuilder.AppendLine(@"and exists (select 1 
                                                    from ContentItemFeatureOnTag cift with (nolock) 
                                                    inner join Tag t with (nolock) on t.TagID = cift.TagID
                                                    where cift.ContentItemId = ContentItem.ContentItemId 
                                                    and t.Identifier = '{0}' and cift.FeatureLevel {1} {2})".F(featuredOnTag.TagIdentifier, filterOperator, featuredOnTag.FeatureLevel));
            }

            if (this.parameters.FeaturedOnAnyTag.Count == 0)
            {
                return;
            }

            var serialized = this.SerializeIn(this.parameters.FeaturedOnAnyTag);

            this.firstStepWhereBuilder.AppendLine(@"and exists (select 1 
                                                    from ContentItemFeatureOnTag cift with (nolock) 
                                                    inner join Tag t with (nolock) on t.TagID = cift.TagID
                                                    where cift.ContentItemId = ContentItem.ContentItemId 
                                                    and t.Identifier in ({0})  )".F(serialized));
        }

        protected void FilterFeaturedOnLocation()
        {
            if (this.parameters.FeaturedOnLocation.Count == 0)
            {
                return;
            }

            foreach (var featuredOnLocation in this.parameters.FeaturedOnLocation)
            {
                var filterOperator = this.GetOperator(featuredOnLocation.FilterOperator);
                if (this.parameters.FilterLocationById)
                {
                    this.firstStepWhereBuilder.AppendLine(@"and exists (select 1 
                                                    from ContentItemFeatureLocation cifl with (nolock) 
                                                    where cifl.ContentItemId = ContentItem.ContentItemId 
                                                    and cifl.FeatureLocationId = '{0}' and cifl.FeatureLevel {1} {2})".F(featuredOnLocation.Location, filterOperator, featuredOnLocation.FeatureLevel));
                }
                else
                {
                    this.firstStepWhereBuilder.AppendLine(@"and exists (select 1 
                                                    from ContentItemFeatureLocation cifl with (nolock) 
                                                    join FeatureLocation fl1 on fl1.FeatureLocationID=cifl.FeatureLocationID
                                                    where cifl.ContentItemId = ContentItem.ContentItemId                                                      
                                                    and fl1.Title = '{0}' and cifl.FeatureLevel {1} {2})".F(featuredOnLocation.Location, filterOperator, featuredOnLocation.FeatureLevel));
                }
            }
        }

        protected void AddContentItemForFeatureTags()
        {
            if (!this.parameters.FeatureTaggedWithAll.Any() && !this.parameters.FeatureTaggedWithAny.Any())
            {
                return;
            }

            var sql = string.Empty;

            if (this.parameters.FeatureTaggedWithAll.Any())
            {
                string serializedTags = this.SerializeIn(this.parameters.FeatureTaggedWithAll);
                sql = @", ContentItemForFeatureTags as (
                        select ContentItemId
                        from ContentItemFeatureOnTag cifft with(nolock)
                        inner join Tag t with(nolock) on t.TagID = cifft.TagID
                        where t.identifier in ({0}) 
                        group by ContentItemID
                        having COUNT(1) >= {1})"
                        .F(serializedTags, this.parameters.FeatureTaggedWithAll.Count);

                this.tempTablesBuilder.AppendLine(sql);
            }
            else if (this.parameters.FeatureTaggedWithAny.Any())
            {
                string serializedTags = this.SerializeIn(this.parameters.FeatureTaggedWithAny);
                sql = @", ContentItemForFeatureTags as (
                        select ContentItemId
                        from ContentItemFeatureOnTag cifft with(nolock)
                        inner join Tag t with(nolock) on t.TagID = cifft.TagID
                        where t.identifier in ({0})
                        group by ContentItemID)"
                        .F(serializedTags);

                this.tempTablesBuilder.AppendLine(sql);
            }
        }

        protected void AddContentItemForFeatureLocations()
        {
            if (this.parameters.FeatureLocatedWith == null)
            {
                return;
            }

            var serializedLocations = this.SerializeIn(this.parameters.FeatureLocatedWith.Locations);
            string sql;
            if (!this.parameters.FeatureLocatedWith.Any)
            {
                sql = @", ContentItemForFeatureLocations as (
                        select ContentItemId
                        from ContentItemFeatureLocation cifl with (nolock)
                        inner join FeatureLocation fl with (nolock) on fl.FeatureLocationId = cifl.FeatureLocationId
                        where cifl.FeatureLocationId in ({0})
                        group by ContentItemID
                        having COUNT(1) >= {1})"
                        .F(serializedLocations, this.parameters.FeatureLocatedWith.Locations.Length);

                this.tempTablesBuilder.AppendLine(sql);
            }
            else
            {
                sql = @", ContentItemForFeatureLocations as (
                        select ContentItemId
                        from ContentItemFeatureLocation cifl with (nolock)
                        inner join FeatureLocation fl with (nolock) on fl.FeatureLocationId = cifl.FeatureLocationId
                        where cifl.FeatureLocationId in ({0})
                        group by ContentItemID)"
                        .F(serializedLocations);

                this.tempTablesBuilder.AppendLine(sql);
            }
        }

        protected void AddContentItemForSegmentations()
        {
            if (!this.parameters.SegmentedWithAll.Any() && !this.parameters.SegmentedWithAny.Any() && !this.parameters.IncludeOnlyEmptySegmentations)
            {
                return;
            }

            var sql = string.Empty;

            if (this.parameters.IncludeOnlyEmptySegmentations)
            {
                sql = @", ContentItemForSegmentations as (
                        select distinct ci.ContentItemId
                        from ContentItem ci
                        left join ContentItemSegmentation cis with(nolock) on ci.ContentItemId = cis.ContentItemId
                        where cis.ContentItemId is null)";

                this.tempTablesBuilder.AppendLine(sql);
            }
            else if (this.parameters.SegmentedWithAll.Any())
            {
                string serializedSegmentations = this.SerializeIn(this.parameters.SegmentedWithAll);
                sql = @", ContentItemForSegmentations as (
                        select ci.ContentItemId
                        from ContentItem ci
                        left join ContentItemSegmentation cis with(nolock) on ci.ContentItemId = cis.ContentItemId
                        where cis.SegmentationId in ({0}) {1}
                        group by ci.ContentItemID
                        having COUNT(1) >= {2})"
                        .F(serializedSegmentations, this.parameters.IncludeEmptySegmentations ? "or cis.ContentItemId is null" : string.Empty, this.parameters.SegmentedWithAll.Count);

                this.tempTablesBuilder.AppendLine(sql);
            }
            else if (this.parameters.SegmentedWithAny.Any())
            {
                string serializedSegmentations = this.SerializeIn(this.parameters.SegmentedWithAny);
                sql = @", ContentItemForSegmentations as (
                        select distinct ci.ContentItemId
                        from ContentItem ci
                        left join ContentItemSegmentation cis with(nolock) on ci.ContentItemId = cis.ContentItemId
                        where cis.SegmentationId in ({0}) {1})"
                        .F(serializedSegmentations, this.parameters.IncludeEmptySegmentations ? "or cis.ContentItemId is null" : string.Empty);

                this.tempTablesBuilder.AppendLine(sql);
            }
        }

        protected void FilterAdditional()
        {
            foreach (var additionalFilter in this.parameters.AdditionalFilters)
            {
                this.firstStepWhereBuilder.AppendLine(@" and {0}".F(additionalFilter));
            }
        }

        protected void FilterPrimaryContentItemsIds()
        {
            if (this.parameters.ContentItemIds.Count == 0)
            {
                return;
            }

            var serializedContentItemIds = this.SerializeIn(this.parameters.ContentItemIds);

            this.firstStepWhereBuilder.AppendLine(@" and ContentItem.PrimaryContentItemId in ({0})".F(serializedContentItemIds));
        }

        protected void FilterRegion()
        {
            if (this.parameters.Region.IsNullOrEmpty())
            {
                return;
            }

            this.firstStepWhereBuilder.AppendLine(@" and ContentItem.RegionId = '{0}'".F(this.parameters.Region.ToLower()));
        }


        protected void FilterExpired()
        {
            if (this.parameters.isExpired == null)
            {
                return;
            }

            bool isExpired = this.parameters.isExpired ?? false;

            if (!this.parameters.Region.IsNullOrEmpty())
            {
                this.firstStepWhereBuilder.AppendLine(@" and ContentItem.RegionId = '{0}'".F(this.parameters.Region.ToLower()));
            }

            if (isExpired)
            {
                this.firstStepWhereBuilder.AppendLine(@" and (ContentItem.ExpirationDate < GETDATE())");
            }
            else
            {
                this.firstStepWhereBuilder.AppendLine(@" and (ContentItem.ExpirationDate > GETDATE() or ContentItem.ExpirationDate is null )");
            }
        }

        protected void AddTempTable(string sql)
        {
            this.tempTablesBuilder.Append(sql);
        }

        protected void AddTempTable<T>(StringBuilder sb, string name, T[] values)
        {
            sb.AppendLine(@"create table #{0} ( Value {1} not null )".F(name, ""));
            foreach (var value in values)
            {
                sb.AppendLine(@"insert into #{0} values ({1})".F(name, value));
            }
        }

        protected void AddContentItemForTags()
        {
            string sql = string.Empty;
            string contentItemTag = GetContentItemTagWithPseudoTags();

            if (this.parameters.TaggedWithAll.Count > 0)
            {
                var serializedTags = this.SerializeIn(this.parameters.TaggedWithAll);
                sql = @"ContentItemWithTags as (
                        select ContentItemId {3}
                        from {2}
                        inner join Tag t with (nolock) on t.TagID = cit.TagID
                        where t.Identifier in ({0})
                        group by ContentItemID {3}
                        having COUNT(1) >= {1})"
                        .F(serializedTags, this.parameters.TaggedWithAll.Count, contentItemTag, this.parameters.IncludeTagIdentifiers ? ", t.Identifier" : string.Empty);

                this.tempTablesBuilder.AppendLine(sql);
            }
            else if (this.parameters.TaggedWithAny.Count > 0)
            {
                var serializedTags = this.SerializeIn(this.parameters.TaggedWithAny);
                sql = @"ContentItemWithTags as (
                        select ContentItemId {3}
                        from {2}
                        inner join Tag t with (nolock) on t.TagID = cit.TagID
                        where t.Identifier in ({0})
                        group by ContentItemID {3})"
                        .F(serializedTags, this.parameters.TaggedWithAny.Count, contentItemTag, this.parameters.IncludeTagIdentifiers ? ", t.Identifier" : string.Empty);

                this.tempTablesBuilder.AppendLine(sql);
            }
        }

        protected void AddRelatedToContentItem()
        {
            if (this.parameters.RelatedToContentItemId.IsNullOrWhiteSpace())
            {
                return;
            }

            bool isUnion = !(this.parameters.RelatedToTags == null);

            var sql = @", RelatedContentItems as {0} 
                        select cit.ContentItemId, count(cit.TagID) SharedTags
                        from {2}
                        inner join ContentItemTag citr with (nolock) on citr.TagID = cit.TagID and citr.ContentItemID = '{0}' 
                        inner join (select distinct TagId
                                    from TagHierarchyPosition thp with (nolock) 
                                    inner join TagCategory tc with (nolock) on tc.TagCategoryId = thp.TagCategoryId
                                    where tc.IsRelatable = 1) X on X.TagID = citr.TagId
                        where cit.ContentItemID != '{1}'
                        group by cit.ContentItemID )".F(isUnion ? "((" : "(", this.parameters.RelatedToContentItemId, GetContentItemTagWithPseudoTags());

            this.tempTablesBuilder.AppendLine(sql);
        }

        protected void AddRelatedToTags()
        {
            if (this.parameters.RelatedToTags == null)
            {
                return;
            }

            var serializedTags = this.SerializeIn(this.parameters.RelatedToTags, true);
            bool isUnion = !this.parameters.RelatedToContentItemId.IsNullOrWhiteSpace();

            string requiredCondition = string.Empty;

            if (this.parameters.RelatedToRequiredTags != null)
            {
                requiredCondition = "and exists (select 1 from ContentItemTag cit2, Tag t1 where cit2.ContentItemID = cit.ContentItemID and cit2.TagID = t1.TagID and t1.Identifier in ({0}))".F(this.SerializeIn(this.parameters.RelatedToRequiredTags, true));
            }

            var sql = @"{0} ( 
                        select cit.ContentItemId, count(cit.TagID) SharedTags
                        from {3}
                        inner join Tag t with (nolock) on t.TagId = cit.TagId
                        where t.Identifier in ({1}) {4}
                        group by cit.ContentItemID {2}".F(isUnion ? "UNION " : ", RelatedContentItems as", serializedTags, isUnion ? "))" : ")", GetContentItemTagWithPseudoTags(), requiredCondition);

            this.tempTablesBuilder.AppendLine(sql);
        }

        protected string GetContentItemTagWithPseudoTags()
        {
            string contentItemTag = "ContentItemTag cit with (nolock) ";

            if (this.parameters.PseudoTagParents.Count > 0)
            {
                contentItemTag = @"(select	cit1.ContentItemID, cit1.TagID
                                from	ContentItemTag cit1
                                union
                                select	cit2.ContentItemID, thp1.TagID
                                from	ContentItemTag cit2, 
                                        TagHierarchyPosition thp1, 
                                        TagHierarchyPosition thp2, 
                                        Tag t2
                                where	cit2.TagID = thp2.TagID and 
                                        thp2.TagID = t2.TagID and 
                                        t2.Identifier in ({0}) and
                                        thp2.PositionID = thp1.ParentPositionID) cit "
                                .F(this.SerializeIn(this.parameters.PseudoTagParents));
            }

            return contentItemTag;
        }

        protected string[] GetContentItemProperties(Type type)
        {
            var excludedProperties = new String[] { "FeatureLevel" };

            var properties = this.GetProperties(type.Name);
            if (properties == null)
            {
                properties = type.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
                                 .Where(p => this.IsSystemType(p.PropertyType)
                                             && !p.Name.In(excludedProperties))
                                 .Select(p => p.Name)
                                 .ToArray();

                SaveProperties(type.Name, properties);
            }

            return properties;
        }

        protected string SerializeIn<T>(IEnumerable<T> items, bool? asStrings = null)
        {
            IEnumerable<string> values;
            if ((asStrings.HasValue && asStrings.Value) || (!asStrings.HasValue && typeof(T).In(typeof(string), typeof(char))))
            {
                values = items.Select(i => @"'{0}'".F(i));
            }
            else
            {
                values = items.Select(i => @"{0}".F(i));
            }

            return string.Join(@",", values);
        }

        protected string GetOperator(FilterOperator filterOperator)
        {
            if (filterOperator == FilterOperator.Equal)
                return "=";
            else if (filterOperator == FilterOperator.Greater)
                return ">";
            else if (filterOperator == FilterOperator.GreaterOrEqual)
                return ">=";
            else if (filterOperator == FilterOperator.Lower)
                return "<";
            else if (filterOperator == FilterOperator.LowerOrEqual)
                return "<=";

            return "=";
        }

        protected bool IsSystemType(Type type)
        {
            return type.In(typeof(short),
                           typeof(short?),
                           typeof(ushort),
                           typeof(ushort?),
                           typeof(int),
                           typeof(int?),
                           typeof(uint),
                           typeof(uint?),
                           typeof(long),
                           typeof(long?),
                           typeof(ulong),
                           typeof(ulong?),
                           typeof(bool),
                           typeof(bool?),
                           typeof(decimal),
                           typeof(decimal?),
                           typeof(float),
                           typeof(float?),
                           typeof(double),
                           typeof(double?),
                           typeof(DateTime),
                           typeof(DateTime?),
                           typeof(char),
                           typeof(char?),
                           typeof(string));
        }

        private string[] GetProperties(string key)
        {
            if (this.cachingService == null)
            {
                return null;
            }

            return this.cachingService.Get<string[]>("SLAM_{0}_properties".F(key));
        }

        private void SaveProperties(string key, string[] properties)
        {
            if (this.cachingService == null)
            {
                return;
            }

            this.cachingService.Save("SLAM_{0}_properties".F(key), properties);
        }
    }
}