﻿using System;

namespace Slam.Cms.Data
{
    [Serializable]
    public partial class FeatureLocation : ICloneable
    {
        public string FeatureLocationId { get; set; }
        public string Title { get; set; }
        public int FeatureLevel { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}