﻿using System;

namespace Slam.Cms.Data
{
    public class SqlCache
    {
        public string Id { get; set; }
        public DateTime LastInvalidationCalled { get; set; }
        public bool MustInvalidate { get; set; }
    }
}