﻿using System;
using System.Collections.Generic;

namespace Slam.Cms.Data
{
    [Serializable]
    public abstract partial class ContentItem : ICloneable
    {
        public string ContentItemId { get; set; }
        public string PrimaryContentItemId { get; set; }
        public int StatusID { get; set; }
        public virtual Status Status { get; set; }
        public int FeatureLevel { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedByUserId { get; set; }
        public virtual User CreatedByUser { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int ModifiedByUserId { get; set; }
        public virtual User ModifiedByUser { get; set; }
        public ICollection<Tag> Tags { get; set; }
        public ICollection<Tag> FeatureTags { get; set; }
        public ICollection<FeatureLocation> FeatureLocations { get; set; }
        public ICollection<Segmentation> Segmentations { get; set; }
        public string ContentTypeId { get; set; }
        public string FrontEndUrl { get; set; }
        public int ReviewStateId { get; set; }
        public string RegionId { get; set; }
        public DateTime? ExpirationDate { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}