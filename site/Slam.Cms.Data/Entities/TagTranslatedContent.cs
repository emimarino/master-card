﻿using System;

namespace Slam.Cms.Data
{
    [Serializable]
    public partial class TagTranslatedContent : ICloneable
    {
        public string TagId { get; set; }
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public string PageContent { get; set; }
        private string _languageCode;
        public string LanguageCode
        {
            get { return _languageCode; }
            set { _languageCode = value.Trim(); }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}