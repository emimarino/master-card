﻿using Mastercard.MarketingCenter.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Slam.Cms.Data
{
    [Serializable]
    public partial class Segmentation : ICloneable
    {
        public string SegmentationId { get; set; }
        public string SegmentationName { get; set; }
        public string RegionId { get; set; }

        public IList<SegmentationTranslatedContent> SegmentationTranslatedContent { get; set; }

        public void Translate(string languageCode)
        {
            if (SegmentationTranslatedContent != null)
            {
                var content = SegmentationTranslatedContent.FirstOrDefault(c => c.LanguageCode.Equals(languageCode, StringComparison.OrdinalIgnoreCase));
                if (content != null && !(content.SegmentationName?.Trim()?.IsNullOrEmpty() ?? true))
                {
                    SegmentationName = content.SegmentationName;
                }
            }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}