﻿using System;

namespace Slam.Cms.Data
{
    [Serializable]
    public partial class SegmentationTranslatedContent : ICloneable
    {
        public int Id { get; set; }
        public string SegmentationId { get; set; }
        public string SegmentationName { get; set; }
        private string _languageCode;
        public string LanguageCode
        {
            get { return _languageCode; }
            set { _languageCode = value.Trim(); }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}