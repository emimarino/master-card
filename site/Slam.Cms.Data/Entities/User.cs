﻿using System;
using System.Collections.Generic;

namespace Slam.Cms.Data
{
	[Serializable]
	public partial class User : ICloneable
	{
		public int UserId { get; set; }
		public string UserName { get; set; }
        public string Name { get; set; }
        public string Region { get; set; }
        public string IssuerId { get; set; }
        public string Email { get; set; }

        public virtual ICollection<UserGroup> Groups { get; set; }

		public object Clone()
		{
			return this.MemberwiseClone();
		}
	}
}
