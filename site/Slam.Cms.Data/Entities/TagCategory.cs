﻿using System;

namespace Slam.Cms.Data
{
    [Serializable]
    public partial class TagCategory : ICloneable
    {
        public string TagCategoryId { get; set; }
        public string Title { get; set; }
        public int DisplayOrder { get; set; }
        public string Featurable { get; set; }
        public string ListLocation { get; set; }
        public bool IsRelatable { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}