﻿using System;
using System.Collections.Generic;

namespace Slam.Cms.Data
{
	[Serializable]
	public partial class UserGroup : ICloneable
	{
		public int GroupId { get; set; }
		public string Name { get; set; }

		public virtual ICollection<User> Users { get; set; }

		public object Clone()
		{
			return this.MemberwiseClone();
		}
	}
}