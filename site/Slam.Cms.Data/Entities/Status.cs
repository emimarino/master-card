﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Slam.Cms.Data
{
    [Serializable]
    public partial class Status : ICloneable
    {
        public const int DraftOnly = 1;
        public const int Published = 3;
        public const int PublishedAndDraft = 4;
        public const int Archived = 6;
        public const int Deleted = 8;
        public const int Expired = 9;

        [Key]
        public int StatusID { get; set; }
        public string Name { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}