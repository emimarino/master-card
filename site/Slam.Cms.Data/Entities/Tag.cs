﻿using System.Collections.Generic;
using System;
using System.Linq;
using Mastercard.MarketingCenter.Common.Extensions;

namespace Slam.Cms.Data
{
    [Serializable]
    public partial class Tag : ICloneable
    {
        public string TagId { get; set; }
        public string Identifier { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public string PageContent { get; set; }
        public bool RestrictAccess { get; set; }
        public bool Featured { get; set; }
        public int FeatureLevel { get; set; }
        public string DefaultPageUri { get; set; }
        public IList<TagTranslatedContent> TagTranslatedContent { get; set; }
        public void Translate(string languageCode)
        {
            if (TagTranslatedContent != null)
            {
                var content = TagTranslatedContent.FirstOrDefault(c => c.LanguageCode.Equals(languageCode, StringComparison.OrdinalIgnoreCase));
                if (content != null && !(content.DisplayName?.Trim()?.IsNullOrEmpty() ?? true))
                {
                    DisplayName = content.DisplayName;
                    Description = content.Description;
                    PageContent = content.PageContent;
                }
            }
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}