﻿namespace Slam.Cms.Data
{
    internal class TagHierarchyPosition
    {
        public int PositionId { get; set; }
        public int? ParentPositionId { get; set; }
        public string TagId { get; set; }
        public string TagCategoryId { get; set; }
        public int SortOrder { get; set; }
    }
}
