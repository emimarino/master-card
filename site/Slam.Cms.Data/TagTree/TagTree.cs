﻿using StackExchange.Profiling;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Slam.Cms.Data
{
    [Serializable]
    public class TagTree : ICloneable
    {
        public TagTreeNode Root { get; set; }

        public TagTreeNode FindNode(string id)
        {
            return FindNode(n => n.Id == id);
        }

        public TagTreeNode FindNode(Predicate<TagTreeNode> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException();
            }

            if (Root == null)
            {
                return null;
            }

            return FindNode(Root, predicate);
        }

        public IEnumerable<TagTreeNode> FindNodes(Predicate<TagTreeNode> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException();
            }

            if (Root == null)
            {
                return new List<TagTreeNode>();
            }

            return FindNodes(Root, predicate);
        }

        protected TagTreeNode FindNode(TagTreeNode node, Predicate<TagTreeNode> predicate)
        {
            return FindNodes(node, predicate).FirstOrDefault();
        }

        public static IEnumerable<TagTreeNode> GetLeast(TagTreeNode node)
        {
            List<TagTreeNode> result = new List<TagTreeNode>();
            if (!(node?.Children.Any() ?? false))
            {
                result.Add(node);
            }
            else
            {
                foreach (var child in node?.Children)
                {
                    result.AddRange(GetLeast(child));
                }
            }

            return result;
        }

        protected IEnumerable<TagTreeNode> FindNodes(TagTreeNode node, Predicate<TagTreeNode> predicate)
        {
            var result = new List<TagTreeNode>();
            if (predicate(node))
            {
                result.Add(node);
            }

            foreach (var childNode in node.Children)
            {
                result.AddRange(FindNodes(childNode, predicate));
            }

            return result;
        }

        public TagTree Translate(string languageCode)
        {
            var profiler = MiniProfiler.Current;
            using (profiler.Step("Translate"))
            {
                TranslateNode(Root, languageCode);
            }

            return this;
        }

        private void TranslateNode(TagTreeNode node, string languageCode)
        {
            if (node.Tag != null)
            {
                node.Tag.Translate(languageCode);
                node.Text = node.Tag.DisplayName;
            }

            foreach (var child in node.Children)
            {
                TranslateNode(child, languageCode);
            }
        }

        public object Clone()
        {
            return this.DeepClone();
        }
    }
}