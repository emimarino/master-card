﻿namespace Slam.Cms.Data
{
    public interface ITagTreeBuilder
    {
        TagTree Build();
    }
}
