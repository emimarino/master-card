﻿using System;
using System.Collections.Generic;

namespace Slam.Cms.Data
{
	public enum TagTreeNodeType
	{ 
		Root,
		TagCategory,
		Tag,
		Special
	}

	[Serializable]
	public class TagTreeNode : ICloneable
	{
		public TagTreeNode()
		{
			this.Children = new List<TagTreeNode>();
			this.Identifiers = new string[] { };
		}

		public string Id { get; set; }
		public string Text { get; set; }

		public string Url { get; set; }
		public bool IsSelected { get; set; }
		public int ContentItemCount { get; set; }

		public string Identifier { get; set; } 
		public string[] Identifiers { get; set; }
		public string[] IdentifiersToRemove { get; set; }

		public TagTreeNodeType Type { get; set; }
		public virtual Tag Tag { get; set; }
		public virtual TagCategory TagCategory { get; set; }
		public virtual TagTreeNode Parent { get; set; }
		public IList<TagTreeNode> Children { get; private set; }

		public object Clone()
		{
			return this.MemberwiseClone();
		}
	}
}
