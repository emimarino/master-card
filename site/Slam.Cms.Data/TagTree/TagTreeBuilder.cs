﻿using Mastercard.MarketingCenter.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Slam.Cms.Data
{
	public class TagTreeBuilder : ITagTreeBuilder
	{
		private readonly SlamContext slamContext;

		public TagTreeBuilder(SlamContext slamContext)
		{
			this.slamContext = slamContext;
		}

		public TagTree Build()
		{
			var tagTree = new TagTree();

			var rootNode = new TagTreeNode()
			{
				Id = "root",
				Text = "root",
				Type = TagTreeNodeType.Root
			};

			tagTree.Root = rootNode;

			this.AddTagCategoryNodes(tagTree);

			this.AddTagHierarchyNodes(tagTree);

			this.SetIdentifiersToRemove(rootNode);

            tagTree.Translate("en");

			return tagTree;
		}

		protected void AddTagCategoryNodes(TagTree tagTree)
		{
			var tagCategories = this.slamContext.GetTagCategories();

			foreach (var tagCategory in tagCategories)
			{
				var node = new TagTreeNode()
				{
					Id = tagCategory.TagCategoryId,
					Type = TagTreeNodeType.TagCategory,
					Text = tagCategory.Title,
					TagCategory = tagCategory
				};

				tagTree.Root.Children.Add(node);
			}
		}
 
		protected void AddTagHierarchyNodes(TagTree tagTree)
		{ 
			var tags = this.slamContext.GetTags().ToDictionary(t => t.TagId);
			var tagHierarchy = this.GetTagHierarchy();

			Tag tag;
			TagTreeNode parentNode = null;

			foreach (var position in tagHierarchy)
			{
				if (!tags.TryGetValue(position.TagId, out tag))
					continue;

				if (!position.ParentPositionId.HasValue)
				{
					// top level tag
					parentNode = tagTree.Root.Children.FirstOrDefault(n => n.TagCategory.TagCategoryId == position.TagCategoryId);
					if (parentNode == null)
						throw new Exception("Node not found for tag category {0}".F(position.TagCategoryId));
				}
				else
				{
					parentNode = tagTree.FindNode("p{0}".F(position.ParentPositionId));
					if (parentNode == null)
						throw new Exception("Parent node not found with id p{0}".F(position.ParentPositionId));
				}

				var node = new TagTreeNode()
				{
					Id = "p{0}".F(position.PositionId),
					Parent = parentNode,
					Type = TagTreeNodeType.Tag,
					Text = tag.DisplayName,
					Identifier = tag.Identifier,
					Tag = tag
				};

				node.Identifiers = this.GetIdentifiers(node);

				parentNode.Children.Add(node);
			}
		}

		private string[] GetIdentifiers(TagTreeNode node)
		{ 
			var identifiers = new List<string>();

			while (node != null)
			{
				if (node.Type == TagTreeNodeType.Tag)
					identifiers.Add(node.Identifier);

				node = node.Parent;
			}

			identifiers.Reverse();

			return identifiers.ToArray();
		}

		private string[] SetIdentifiersToRemove(TagTreeNode node)
		{
			var identifiers = new List<string>();
			identifiers.Add(node.Identifier);

			foreach (var childNode in node.Children)
			{
				var childIdentifiers = SetIdentifiersToRemove(childNode);
				identifiers.AddRange(childIdentifiers);
			}

			node.IdentifiersToRemove = identifiers.Distinct().ToArray();
			return node.IdentifiersToRemove;
		}

		private IList<TagHierarchyPosition> GetTagHierarchy()
		{
			var sql = @"select PositionId, ParentPositionId, TagId, TagCategoryId, SortOrder 
						from TagHierarchyPosition 
						order by ParentPositionId, TagCategoryId, SortOrder";
			return this.slamContext.Database.Retrieve<TagHierarchyPosition>(sql);
		}
	}
}
