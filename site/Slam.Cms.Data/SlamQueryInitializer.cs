﻿namespace Slam.Cms.Data
{
    public class SlamQueryInitializer
    {
        private readonly string _selectedRegion;

        public SlamQueryInitializer(string selectedRegion)
        {
            _selectedRegion = selectedRegion;
        }

        public void Initialize(SlamQuery slamQuery)
        {
            slamQuery.FilterRegion(_selectedRegion);
        }
    }
}