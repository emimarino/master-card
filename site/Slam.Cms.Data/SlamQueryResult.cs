﻿using Mastercard.MarketingCenter.Common.Extensions;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Slam.Cms.Data
{
    public class SlamQueryResult<T> : IEnumerable<T> where T : ContentItem
    {
        // TODO defer query execution until enumeration
        // TODO make the paginator deserialize only the needed page

        #region Private Variables

        private IDictionary<string, Tag> tags;
        private IDictionary<string, Tag> featureTags;
        private IDictionary<string, FeatureLocation> featureLocations;
        private IDictionary<string, Segmentation> segmentations;
        private IDictionary<int, User> users;
        private IDictionary<int, Status> status;
        private bool dirtyUsers = false;
        private bool dirtyStatus = false;

        private IDataReader dataReader;
        private SlamContext slamContext;
        private SlamQueryParameters parameters;
        protected List<T> results;
        protected IOrderedEnumerable<T> lastOrderedResult;
        private static ConcurrentDictionary<string, object> deserializers = new ConcurrentDictionary<string, object>();

        #endregion

        #region Constructors

        internal SlamQueryResult(SlamContext slamContext, SlamQueryParameters parameters)
        {
            this.results = new List<T>();
            this.lastOrderedResult = null;
            this.slamContext = slamContext;
            this.parameters = parameters;
            this.Execute();
        }

        private SlamQueryResult(List<T> results, IOrderedEnumerable<T> lastOrderedResult)
        {
            if (results != null)
            {
                this.results = new List<T>();
                foreach (var result in results)
                {
                    if (result != null)
                    {
                        var t_result = (T)result.Clone();
                        this.results.Add(t_result);
                    }
                }

                this.lastOrderedResult = lastOrderedResult != null ? lastOrderedResult : this.results.OrderBy(a => 1);
            }
        }

        #endregion

        #region Public Properties

        internal SlamQueryParameters Parameters
        {
            get
            {
                return this.parameters;
            }
        }

        public ContentSearchStatistics Statistics { get; protected set; }

        #endregion

        #region Public Methods

        public SlamQueryResult<T> Clone()
        {
            return new SlamQueryResult<T>(this.results, this.lastOrderedResult);
        }

        public SlamQueryResult<T> Filter(Func<T, bool> filterAction)
        {
            if (results == null)
                return this.Clone();
            SlamQueryResult<T> temp = this.Clone();
            temp.results = temp.Where(filterAction).ToList();
            return temp;
        }

        public SlamQueryResult<T> Filter(Func<T, int, bool> filterAction)
        {
            if (results == null)
                return this.Clone();
            SlamQueryResult<T> temp = this.Clone();
            temp.results = temp.Where(filterAction).ToList();
            return temp;
        }

        #endregion

        #region Prepare

        protected void Prepare()
        {
            var stopWatch = Stopwatch.StartNew();

            var tags = this.slamContext.GetTags();
            this.tags = tags.ToDictionary(t => t.TagId);

            var featureTags = this.slamContext.GetFeatureTags();
            this.featureTags = featureTags.ToDictionary(t => t.TagId);

            var featureLocations = this.slamContext.GetFeatureLocations();
            this.featureLocations = featureLocations.ToDictionary(fl => fl.FeatureLocationId);

            var segmentations = this.slamContext.GetSegmentations();
            this.segmentations = segmentations.ToDictionary(s => s.SegmentationId);

            var status = this.slamContext.GetStatus();
            this.status = status.ToDictionary(s => s.StatusID);

            var users = this.slamContext.GetUsers();
            this.users = users.ToDictionary(u => u.UserId);

            stopWatch.Stop();
            this.Statistics.Preparation = stopWatch.Elapsed;
        }

        #endregion

        #region Query Execution

        protected void Execute()
        {
            this.ResetStatistics();

            var stopWatch = Stopwatch.StartNew();

            this.Prepare();

            this.ExecuteQuery();

            stopWatch.Stop();
            this.Statistics.Total = stopWatch.Elapsed;
        }

        protected void ExecuteQuery()
        {
            // Build the query
            var slamQueryBuilder = new SlamQueryBuilder(slamContext);

            var sql = slamQueryBuilder.BuildQuery(this.parameters);

            slamContext.Database.ExecuteReader(sql, reader =>
            {
                this.dataReader = reader;
                this.Statistics.QueryExecution = slamContext.Database.LastCommandExecutionTime;
                this.DeserializeResults();
            });

            this.CheckDirtyCollections();
        }

        #endregion

        #region Deserialization

        protected void DeserializeResults()
        {
            var stopWatch = Stopwatch.StartNew();

            this.DeserializeContentItems();

            if (this.parameters.IncludeTags)
            {
                this.DeserializeTags();
            }

            if (this.parameters.IncludeFeatureTags)
            {
                this.DeserializeFeatureTags();
            }

            if (this.parameters.IncludeFeatureLocations)
            {
                this.DeserializeFeatureLocations();
            }

            if (this.parameters.IncludeSegmentations)
            {
                this.DeserializeSegmentations();
            }

            stopWatch.Stop();
            this.Statistics.Deserialization = stopWatch.Elapsed;
        }

        protected void DeserializeContentItems()
        {
            var reader = this.dataReader;
            var discriminatorIndex = reader.GetOrdinal(Constants.ContentTypeDiscriminatorFieldName);

            while (reader.Read())
            {
                if (!reader.IsDBNull(discriminatorIndex)) //If no ContentType is in the row, skip deserializing it
                {
                    var typeName = reader.GetString(discriminatorIndex);

                    var deserializer = this.GetDeserializer<T>(typeName);

                    var contentItem = deserializer(reader);

                    this.LoadContentItemRelationships(contentItem);

                    this.results.Add(contentItem);
                }
            }
        }

        protected void LoadContentItemRelationships(T contentItem)
        {
            if (contentItem.Tags == null)
                contentItem.Tags = new List<Tag>();

            if (contentItem.FeatureTags == null)
                contentItem.FeatureTags = new List<Tag>();

            if (contentItem.FeatureLocations == null)
                contentItem.FeatureLocations = new List<FeatureLocation>();

            if (contentItem.Segmentations == null)
                contentItem.Segmentations = new List<Segmentation>();

            User user;
            if (contentItem.ModifiedByUser == null)
            {
                if (this.users.TryGetValue(contentItem.ModifiedByUserId, out user))
                    contentItem.ModifiedByUser = user;
                else
                    dirtyUsers = true;
            }

            if (contentItem.CreatedByUser == null)
            {
                if (this.users.TryGetValue(contentItem.CreatedByUserId, out user))
                    contentItem.CreatedByUser = user;
                else
                    dirtyUsers = true;
            }

            if (contentItem.Status == null)
            {
                Status status;
                if (this.status.TryGetValue(contentItem.StatusID, out status))
                    contentItem.Status = status;
                else
                    dirtyStatus = true;
            }
        }

        protected void CheckDirtyCollections()
        {
            if (!dirtyUsers && !dirtyStatus)
                return;

            if (dirtyUsers)
                this.slamContext.InvalidateUsers();

            if (dirtyStatus)
                this.slamContext.InvalidateStatus();

            // reload collections again
            this.Prepare();

            this.LoadNullContentItemRelationships();
        }

        protected void LoadNullContentItemRelationships()
        {
            foreach (var contentItem in results.Where(ci => ci.Status == null || ci.CreatedByUser == null || ci.ModifiedByUser == null))
            {
                LoadContentItemRelationships(contentItem);
            }
        }

        protected void DeserializeTags()
        {
            var reader = this.dataReader;
            reader.NextResult();

            var contentItemIdIndex = reader.GetOrdinal("ContentItemId");
            var tagIdIndex = reader.GetOrdinal("TagID");

            Tag tag;

            while (reader.Read())
            {
                var contentItemId = reader.GetString(contentItemIdIndex);
                var tagId = reader.GetString(tagIdIndex);

                var contentItem = this.FirstOrDefault(ci => ci.ContentItemId == contentItemId);

                if (contentItem != null)
                {
                    if (contentItem.Tags == null)
                        contentItem.Tags = new List<Tag>();

                    if (this.tags.TryGetValue(tagId, out tag))
                        contentItem.Tags.Add(tag);
                }
            }
        }

        protected void DeserializeFeatureTags()
        {
            var reader = this.dataReader;
            reader.NextResult();

            var contentItemIdIndex = reader.GetOrdinal("ContentItemId");
            var tagIdIndex = reader.GetOrdinal("TagID");
            var featureLevelIndex = reader.GetOrdinal("FeatureLevel");

            Tag tag;

            while (reader.Read())
            {
                var contentItemId = reader.GetString(contentItemIdIndex);
                var tagId = reader.GetString(tagIdIndex);
                var featureLevel = reader.GetInt32(featureLevelIndex);

                var contentItem = this.FirstOrDefault(ci => ci.ContentItemId == contentItemId);

                if (contentItem != null)
                {
                    if (contentItem.FeatureTags == null)
                        contentItem.FeatureTags = new List<Tag>();

                    if (this.featureTags.TryGetValue(tagId, out tag))
                    {
                        tag.Featured = true;
                        tag.FeatureLevel = featureLevel;
                        contentItem.FeatureTags.Add((Tag)tag.Clone());
                    }
                }
            }
        }

        protected void DeserializeFeatureLocations()
        {
            var reader = this.dataReader;
            reader.NextResult();

            var contentItemIdIndex = reader.GetOrdinal("ContentItemId");
            var featureLocationIdIndex = reader.GetOrdinal("FeatureLocationId");
            var featureLevelIndex = reader.GetOrdinal("FeatureLevel");

            FeatureLocation featureLocation;

            while (reader.Read())
            {
                var contentItemId = reader.GetString(contentItemIdIndex);
                var featureLocationId = reader.GetString(featureLocationIdIndex);
                var featureLevel = reader.GetInt32(featureLevelIndex);

                var contentItem = this.FirstOrDefault(ci => ci.ContentItemId == contentItemId);

                if (contentItem != null)
                {
                    if (contentItem.FeatureLocations == null)
                        contentItem.FeatureLocations = new List<FeatureLocation>();

                    if (this.featureLocations.TryGetValue(featureLocationId, out featureLocation))
                    {
                        featureLocation.FeatureLevel = featureLevel;
                        contentItem.FeatureLocations.Add((FeatureLocation)featureLocation.Clone());
                    }
                }
            }
        }

        protected void DeserializeSegmentations()
        {
            var reader = this.dataReader;
            reader.NextResult();

            var contentItemIdIndex = reader.GetOrdinal("ContentItemId");
            var segmentationIdIndex = reader.GetOrdinal("SegmentationId");

            Segmentation segmentation;

            while (reader.Read())
            {
                var contentItemId = reader.GetString(contentItemIdIndex);
                var segmentationId = reader.GetString(segmentationIdIndex);

                var contentItem = this.FirstOrDefault(ci => ci.ContentItemId == contentItemId);

                if (contentItem != null)
                {
                    if (contentItem.Segmentations == null)
                        contentItem.Segmentations = new List<Segmentation>();

                    if (this.segmentations.TryGetValue(segmentationId, out segmentation))
                        contentItem.Segmentations.Add(segmentation);
                }
            }
        }

        protected Func<IDataReader, U> GetDeserializer<U>(string typeName)
        {
            var deserializer = this.GetDeserializerFromCache(typeName) as Func<IDataReader, U>;
            if (deserializer == null)
            {
                var concreteContentType = this.slamContext.KnownContentTypes.GetTypeByName(typeName);

                if (concreteContentType == null)
                    throw new Exception("There was a problem trying to build deserializer for {0}".F(typeName));

                deserializer = Deserializer.GetClassDeserializer<U>(this.dataReader, concreteContentType);
                this.SaveDeserializerToCache(typeName, deserializer);
            }

            return deserializer;
        }

        #endregion

        #region Caching

        private object GetDeserializerFromCache(string key)
        {
            object deserializer;
            if (!deserializers.TryGetValue(key, out deserializer))
                return null;

            return deserializer;
        }

        private void SaveDeserializerToCache(string key, object deserializer)
        {
            deserializers[key] = deserializer;
        }

        #endregion

        #region Enumerator

        public IEnumerator<T> GetEnumerator()
        {
            return this.results.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.results.GetEnumerator();
        }

        #endregion

        #region Ranking and Ordering

        public SlamQueryResult<T> Rank(Func<T, int> rankingFunction)
        {
            if (rankingFunction == null)
                throw new ArgumentNullException("rankingFunction");

            var newList = new ConcurrentBag<KeyValuePair<int, T>>();

            foreach (var contentItem in this.results.AsParallel())
            {
                var rank = rankingFunction(contentItem);
                newList.Add(new KeyValuePair<int, T>(rank, contentItem));
            }

            this.results = newList.OrderByDescending(x => x.Key).Select(x => x.Value).ToList();

            return this;
        }

        public SlamQueryResult<T> OrderByDescending(Func<T, int> action)
        {
            if (action == null)
                throw new ArgumentNullException("action");

            this.lastOrderedResult = this.results.OrderByDescending(action);
            this.results = this.lastOrderedResult.ToList();

            return this;
        }

        public SlamQueryResult<T> OrderBy(Func<T, int> action)
        {
            if (action == null)
                throw new ArgumentNullException("action");

            this.lastOrderedResult = this.results.OrderBy(action);
            this.results = this.lastOrderedResult.ToList();

            return this;
        }

        public SlamQueryResult<T> ThenBy(Func<T, int> action)
        {
            if (action == null)
                throw new ArgumentNullException("action");
            if (lastOrderedResult == null)
                return this;

            this.lastOrderedResult = this.lastOrderedResult.ThenBy(action);
            this.results = this.lastOrderedResult.ToList();

            return this;
        }

        public SlamQueryResult<T> ThenByDescending(Func<T, DateTime> action)
        {
            if (action == null)
                throw new ArgumentNullException("action");
            if (lastOrderedResult == null)
                return this;

            this.lastOrderedResult = this.lastOrderedResult.ThenByDescending(action);
            this.results = this.lastOrderedResult.ToList();

            return this;
        }

        public SlamQueryResult<T> ThenByDescending(Func<T, int> action)
        {
            if (action == null)
                throw new ArgumentNullException("action");
            if (lastOrderedResult == null)
                return this;

            this.lastOrderedResult = this.lastOrderedResult.ThenByDescending(action);
            this.results = this.lastOrderedResult.ToList();

            return this;
        }

        public Func<int, int, bool> GetOperator(FilterOperator filterOperator)
        {
            if (filterOperator == FilterOperator.Equal)
                return (a, b) => a == b;
            else if (filterOperator == FilterOperator.Greater)
                return (a, b) => a > b;
            else if (filterOperator == FilterOperator.GreaterOrEqual)
                return (a, b) => a >= b;
            else if (filterOperator == FilterOperator.Lower)
                return (a, b) => a < b;
            else if (filterOperator == FilterOperator.LowerOrEqual)
                return (a, b) => a <= b;

            return (a, b) => a == b;
        }

        #endregion

        #region Pagination

        public SlamQueryPagedResult<T> GetPage(int pageNumber, int pageSize = 10, Func<T, string> orderByKeySelector = null)
        {
            var pagedResults = new SlamQueryPagedResult<T>(this, pageNumber, pageSize, orderByKeySelector);
            return pagedResults;
        }

        #endregion

        #region Statistics

        protected void ResetStatistics()
        {
            this.Statistics = new ContentSearchStatistics();
        }

        public class ContentSearchStatistics
        {
            internal ContentSearchStatistics()
            {
            }

            public TimeSpan Preparation { get; internal set; }
            public TimeSpan QueryBuilding { get; internal set; }
            public TimeSpan QueryExecution { get; internal set; }
            public TimeSpan Deserialization { get; internal set; }
            public TimeSpan ResultsRanking { get; internal set; }
            public TimeSpan Total { get; internal set; }

            public override string ToString()
            {
                var sb = new StringBuilder();
                sb.AppendLine(this.AddDuration("Preparation", this.Preparation));
                sb.AppendLine(this.AddDuration("Query Building", this.QueryBuilding));
                sb.AppendLine(this.AddDuration("Query Execution", this.QueryExecution));
                sb.AppendLine(this.AddDuration("Deserialization", this.Deserialization));
                sb.AppendLine(this.AddDuration("Results Ranking", this.ResultsRanking));
                sb.AppendLine("Total: {0}".F(this.Total));
                return sb.ToString();
            }

            protected string AddDuration(string name, TimeSpan duration)
            {
                return "{0}: {1} - {2}%".F(name, duration, this.GetPercent(duration));
            }

            protected string GetPercent(TimeSpan duration)
            {
                return (duration.TotalMilliseconds / this.Total.TotalMilliseconds * 100d).ToString("0.00");
            }
        }

        #endregion
    }
}