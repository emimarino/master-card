﻿using System.Collections.Generic;

namespace Slam.Cms.Data
{
    public interface ISqlCacheRepository
    {
        IEnumerable<SqlCache> GetAllInvalidators();
        void SaveRegionInvalidator(string cacheId);
        void MarkInvalidated(IEnumerable<string> ids);
    }
}