﻿using System.Collections.Generic;
using System.Linq;

namespace Slam.Cms.Data
{
	public static class SlamQueryExtensions
	{
		public static SlamQuery FilterRelatedToSpecificTagsOf(this SlamQuery slamQuery, ContentItem contentItem)
		{
			return slamQuery.FilterRelatedToSpecificTags(contentItem.Tags.Select(t => t.Identifier));
		}

		public static SlamQuery FilterRelatedToSpecificTags(this SlamQuery slamQuery, IEnumerable<string> tagIdentifiers)
		{
			var tagTree = slamQuery.Context.GetTagTree();

			var tagsWithNoChildren = tagTree.FindNodes(n => n.Type == TagTreeNodeType.Tag && !n.Children.Any());
			var specificTagIdentifiers = tagsWithNoChildren.Select(n => n.Identifier).Distinct();
			var finalIdentifiers = specificTagIdentifiers.Intersect(tagIdentifiers);

			return slamQuery.FilterRelatedToTags(finalIdentifiers);
		}
	}
}