﻿using Mastercard.MarketingCenter.Common.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Slam.Cms.Data
{
    public class ContentTypeList : IEnumerable<Type>
    {
        private IDictionary<string, Type> contentTypes;

        public ContentTypeList()
        {
            this.contentTypes = new Dictionary<string, Type>();
        }

        public IEnumerable<Type> GetTypes()
        {
            return this.contentTypes.Values;
        }

        public Type GetTypeByName(string name)
        {
            return this.contentTypes[name];
        }

        public IList<Type> GetTypeByNames(string[] names)
        {
            return this.contentTypes.Where(ct => ct.Key.In(names)).Select(ct => ct.Value).ToList();
        }

        public ContentTypeList Add<T>() where T : ContentItem, new()
        {
            return this.Add(typeof(T));
        }

        public ContentTypeList Add<T1, T2>()
            where T1 : ContentItem, new()
            where T2 : ContentItem, new()
        {
            return this.Add(typeof(T1), typeof(T2));
        }

        public ContentTypeList Add<T1, T2, T3>()
            where T1 : ContentItem, new()
            where T2 : ContentItem, new()
            where T3 : ContentItem, new()
        {
            return this.Add(typeof(T1), typeof(T2), typeof(T3));
        }

        public ContentTypeList Add<T1, T2, T3, T4>()
            where T1 : ContentItem, new()
            where T2 : ContentItem, new()
            where T3 : ContentItem, new()
            where T4 : ContentItem, new()
        {
            return this.Add(typeof(T1), typeof(T2), typeof(T3), typeof(T4));
        }

        public ContentTypeList Add<T1, T2, T3, T4, T5>()
            where T1 : ContentItem, new()
            where T2 : ContentItem, new()
            where T3 : ContentItem, new()
            where T4 : ContentItem, new()
            where T5 : ContentItem, new()
        {
            return this.Add(typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5));
        }

        public ContentTypeList Add<T1, T2, T3, T4, T5, T6>()
            where T1 : ContentItem, new()
            where T2 : ContentItem, new()
            where T3 : ContentItem, new()
            where T4 : ContentItem, new()
            where T5 : ContentItem, new()
            where T6 : ContentItem, new()
        {
            return this.Add(typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6));
        }

        public ContentTypeList Add<T1, T2, T3, T4, T5, T6, T7>()
            where T1 : ContentItem, new()
            where T2 : ContentItem, new()
            where T3 : ContentItem, new()
            where T4 : ContentItem, new()
            where T5 : ContentItem, new()
            where T6 : ContentItem, new()
            where T7 : ContentItem, new()
        {
            return this.Add(typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7));
        }

        public ContentTypeList Add<T1, T2, T3, T4, T5, T6, T7, T8>()
            where T1 : ContentItem, new()
            where T2 : ContentItem, new()
            where T3 : ContentItem, new()
            where T4 : ContentItem, new()
            where T5 : ContentItem, new()
            where T6 : ContentItem, new()
            where T7 : ContentItem, new()
            where T8 : ContentItem, new()
        {
            return this.Add(typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8));
        }

        public ContentTypeList Add(params Type[] types)
        {
            return this.Add(types.AsEnumerable());
        }

        public ContentTypeList Add(IEnumerable<Type> types)
        {
            if (!types.Any())
                throw new ArgumentException("No ContentTypes specified", "types");

            foreach (var type in types)
            {
                if (type.BaseType != typeof(ContentItem))
                    throw new Exception("The type {0} does not inherit from ContentItem.".F(type.Name));

                if (type.IsAbstract)
                    throw new Exception("The type {0} is abstract".F(type.Name));

                if (!this.contentTypes.ContainsKey(type.Name))
                    this.contentTypes.Add(type.Name, type);
            }

            return this;
        }

        public ContentTypeList Clear()
        {
            this.contentTypes.Clear();
            return this;
        }

        public IEnumerator<Type> GetEnumerator()
        {
            return this.contentTypes.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.contentTypes.Values.GetEnumerator();
        }
    }
}
