﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Slam.Cms.Data
{
    internal class SlamQueryParameters
    {
        #region Constructors

        internal SlamQueryParameters()
        {
            this.ContentTypes = new Dictionary<string, Type>();
            this.AdditionalFilters = new List<string>();
            this.ContentItemIds = new List<string>();
            this.TagCategoryIds = new List<string>();
            this.FeaturedOnTag = new List<FeaturedOnTag>();
            this.FeaturedOnLocation = new List<FeaturedOnLocation>();
            this.Status = FilterStatus.Live;
            this.Cache = new QueryCaching() { Duration = 10 };
            this.FeaturedOnAnyTag = new List<string>();
            this.TaggedWithAll = new List<string>();
            this.TaggedWithAny = new List<string>();
            this.PseudoTagParents = new List<string>();
            this.FeatureTaggedWithAny = new List<string>();
            this.FeatureTaggedWithAll = new List<string>();
            this.SegmentedWithAll = new List<string>();
            this.SegmentedWithAny = new List<string>();
        }

        #endregion

        #region Public Properties

        public string Name { get; set; }
        public bool CountOnly { get; set; }
        public List<string> ContentItemIds { get; set; }
        public string RelatedToContentItemId { get; set; }
        public string[] RelatedToTags { get; set; }
        public string[] RelatedToRequiredTags { get; set; }
        public Dictionary<string, Type> ContentTypes { get; set; }
        public FilterStatus Status { get; set; }
        public List<string> FeaturedOnAnyTag { get; set; }
        public List<string> TaggedWithAll { get; set; }
        public List<string> TaggedWithAny { get; set; }
        public bool IncludeTagIdentifiers { get; set; }
        public List<string> FeatureTaggedWithAll { get; set; }
        public List<string> FeatureTaggedWithAny { get; set; }
        public List<string> PseudoTagParents { get; set; }
        public LocatedWith FeatureLocatedWith { get; set; }
        public List<string> SegmentedWithAll { get; set; }
        public List<string> SegmentedWithAny { get; set; }
        public bool IncludeTags { get; set; }
        public bool IncludeFeatureLocations { get; set; }
        public bool IncludeFeatureTags { get; set; }
        public bool IncludeSegmentations { get; set; }
        public bool IncludeEmptySegmentations { get; set; }
        public bool IncludeOnlyEmptySegmentations { get; set; }
        public int Take { get; set; }
        public List<string> TagCategoryIds { get; set; }
        public List<FeaturedOnTag> FeaturedOnTag { get; set; }
        public List<FeaturedOnLocation> FeaturedOnLocation { get; set; }
        public string OrderByFeatureOnTagLevel { get; set; }
        public string OrderByFeatureOnLocationLevel { get; set; }
        public string OrderBy { get; set; }
        public List<string> AdditionalFilters { get; set; }
        public string Region { get; set; }
        public bool FilterLocationById { get; internal set; } = true;
        public bool? isExpired { get; set; } = false;
        public QueryCaching Cache { get; set; }

        #endregion

        #region Public Methods

        public void AddContentType(Type type)
        {
            this.ContentTypes.Add(type.Name, type);
        }

        public void ClearContentTypes()
        {
            this.ContentTypes.Clear();
        }

        #endregion

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hash = 17;
                hash = hash * 23 + Status.GetHashCode();
                hash = hash * 23 + IncludeTags.GetHashCode();
                hash = hash * 23 + IncludeFeatureLocations.GetHashCode();
                hash = hash * 23 + IncludeFeatureTags.GetHashCode();
                hash = hash * 23 + IncludeSegmentations.GetHashCode();
                hash = hash * 23 + Take.GetHashCode();
                hash = hash * 23 + CountOnly.GetHashCode();
                hash = hash * 23 + IncludeEmptySegmentations.GetHashCode();

                if (Name != null)
                    hash = hash * 23 + Name.GetHashCode();
                if (Region != null)
                    hash = hash * 23 + Region.GetHashCode();
                if (ContentItemIds != null)
                    hash = ContentItemIds.Aggregate(hash, (current, item) => current * 23 + item.GetHashCode());
                if (ContentTypes != null)
                    hash = ContentTypes.Aggregate(hash, (current, item) => current * 23 + item.GetHashCode());
                if (FeaturedOnAnyTag != null)
                    hash = FeaturedOnAnyTag.Aggregate(hash, (current, item) => current * 23 + item.GetHashCode());
                if (TaggedWithAll != null)
                    hash = TaggedWithAll.Aggregate(hash, (current, item) => current * 23 + item.GetHashCode());
                if (TaggedWithAny != null)
                    hash = TaggedWithAny.Aggregate(hash, (current, item) => current * 23 + item.GetHashCode());
                if (FeatureLocatedWith != null && FeatureLocatedWith.Locations != null)
                    hash = FeatureLocatedWith.Locations.Aggregate(hash, (current, item) => current * 23 + item.GetHashCode());
                if (FeatureTaggedWithAll != null)
                    hash = FeatureTaggedWithAll.Aggregate(hash, (current, item) => current * 23 + item.GetHashCode());
                if (FeatureTaggedWithAny != null)
                    hash = FeatureTaggedWithAny.Aggregate(hash, (current, item) => current * 23 + item.GetHashCode());
                if (SegmentedWithAll != null)
                    hash = SegmentedWithAll.Aggregate(hash, (current, item) => current * 23 + item.GetHashCode());
                if (SegmentedWithAny != null)
                    hash = SegmentedWithAny.Aggregate(hash, (current, item) => current * 23 + item.GetHashCode());
                if (TagCategoryIds != null)
                    hash = TagCategoryIds.Aggregate(hash, (current, item) => current * 23 + item.GetHashCode());
                if (RelatedToContentItemId != null)
                    hash = hash * 23 + RelatedToContentItemId.GetHashCode();
                if (FeaturedOnTag != null)
                    hash = FeaturedOnTag.Aggregate(hash, (current, item) => current * 23 + item.GetHashCode());
                if (FeaturedOnLocation != null)
                    hash = FeaturedOnLocation.Aggregate(hash, (current, item) => current * 23 + item.GetHashCode());
                if (AdditionalFilters != null)
                    hash = AdditionalFilters.Aggregate(hash, (current, item) => current * 23 + item.GetHashCode());
                if (OrderByFeatureOnTagLevel != null)
                    hash = hash * 23 + OrderByFeatureOnTagLevel.GetHashCode();
                if (OrderByFeatureOnLocationLevel != null)
                    hash = hash * 23 + OrderByFeatureOnLocationLevel.GetHashCode();
                if (OrderBy != null)
                    hash = hash * 23 + OrderBy.GetHashCode();
                if (RelatedToTags != null)
                    hash = RelatedToTags.Aggregate(hash, (current, item) => current * 23 + item.GetHashCode());

                return hash;
            }
        }
    }

    internal class LocatedWith
    {
        public static LocatedWith Create(string[] featureLocations, bool any)
        {
            return new LocatedWith()
            {
                Locations = featureLocations,
                Any = any,
            };
        }

        public string[] Locations { get; set; }
        public bool Any { get; set; }
    }

    internal class FeaturedOnTag
    {
        public static FeaturedOnTag Create(string tagIdentifier, FilterOperator filterOperator, int featureLevel)
        {
            return new FeaturedOnTag()
            {
                TagIdentifier = tagIdentifier,
                FilterOperator = filterOperator,
                FeatureLevel = featureLevel
            };
        }

        public string TagIdentifier { get; set; }
        public FilterOperator FilterOperator { get; set; }
        public int FeatureLevel { get; set; }

        public object Clone()
        {
            return this.DeepClone();
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hash = 17;

                hash = hash * 23 + TagIdentifier.GetHashCode();
                hash = hash * 23 + FilterOperator.GetHashCode();
                hash = hash * 23 + FeatureLevel.GetHashCode();

                return hash;
            }
        }
    }

    internal class FeaturedOnLocation
    {
        public static FeaturedOnLocation Create(string location, FilterOperator filterOperator, int featureLevel)
        {
            return new FeaturedOnLocation()
            {
                Location = location,
                FilterOperator = filterOperator,
                FeatureLevel = featureLevel
            };
        }

        public string Location { get; set; }
        public FilterOperator FilterOperator { get; set; }
        public int FeatureLevel { get; set; }

        public object Clone()
        {
            return this.DeepClone();
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hash = 17;

                hash = hash * 23 + Location.GetHashCode();
                hash = hash * 23 + FilterOperator.GetHashCode();
                hash = hash * 23 + FeatureLevel.GetHashCode();

                return hash;
            }
        }
    }

    internal class PageFilter
    {
        public static PageFilter Create(int pageNumber, int pageSize)
        {
            return new PageFilter()
            {
                Number = pageNumber,
                PageSize = pageSize
            };
        }

        public int Number { get; set; }
        public int PageSize { get; set; }
    }

    [Serializable]
    public class QueryCaching : ICloneable
    {
        public SlamQueryCacheBehavior Behavior { get; set; }
        public int Duration { get; set; }

        public object Clone()
        {
            return this.DeepClone();
        }
    }
}