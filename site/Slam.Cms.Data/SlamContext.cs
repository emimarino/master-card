﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using Slam.Cms.Common.Interfaces;
using Mastercard.MarketingCenter.Common.Extensions;

namespace Slam.Cms.Data
{
    public class SlamContext
    {
        private ContentTypeList _knownContentTypeList;
        private SlamDatabase _database;
        private SlamCache _cache;
        private readonly IPreviewMode _previewMode;

        public SlamContext(IPreviewMode previewMode)
        {
            _previewMode = previewMode;
            _knownContentTypeList = new ContentTypeList();
            TagTreeBuilder = new TagTreeBuilder(this);
        }

        #region Public Properties

        public SlamDatabase Database
        {
            get
            {
                if (_database == null)
                {
                    _database = new SlamDatabase(this);
                }

                return _database;
            }
        }

        public SlamCache Cache
        {
            get
            {
                if (_cache == null)
                {
                    _cache = new SlamCache(this);
                }

                return _cache;
            }
        }

        public Func<IDbConnection> ConnectionFactory { get; set; }
        public ICachingService CachingService { get; set; }
        public ITagTreeBuilder TagTreeBuilder { get; set; }
        public Action<SlamQuery> SlamQueryInitializer { get; set; }

        public ContentTypeList KnownContentTypes
        {
            get
            {
                if (_knownContentTypeList == null)
                {
                    throw new Exception("There KnownContentTypes list is null. Please define one using .SetKnownContentTypes() in the SlamContext initialization.");
                }

                if (!_knownContentTypeList.Any())
                {
                    throw new Exception("The KnownContentTypes list is empty. Please use the .SetKnownContentTypes() in the SlamContext initialization to specify the assembly that includes the concrete ContentItems.");
                }

                return _knownContentTypeList;
            }
        }

        #endregion

        #region Properties in Methods

        public SlamContext SetConnectionFactory(Func<IDbConnection> connectionFactory)
        {
            ConnectionFactory = connectionFactory;
            return this;
        }

        public SlamContext SetCachingService(ICachingService cachingService)
        {
            CachingService = cachingService;
            return this;
        }

        public SlamContext SetKnownContentTypes(Assembly assembly)
        {
            foreach (var contentType in assembly.GetTypes().Where(t => t.BaseType == typeof(ContentItem)))
            {
                _knownContentTypeList.Add(contentType);
            }

            return this;
        }

        public SlamContext SetSlamQueryInitializer(Action<SlamQuery> initializer)
        {
            SlamQueryInitializer = initializer;

            return this;
        }

        #endregion

        #region Public Methods

        public SlamQuery CreateQuery()
        {
            var slamQuery = new SlamQuery(this, _previewMode);
            SlamQueryInitializer?.Invoke(slamQuery);

            return slamQuery;
        }

        public IList<TagCategory> GetTagCategories()
        {
            return GetAll<TagCategory>("DisplayOrder");
        }

        public IList<Tag> GetTags()
        {
            bool isCached = Cache.Get<Tag>() != null;
            var tags = GetAll<Tag>();
            if (!isCached) //If the Tag collection was not cached yet, add translations
            {
                var tagTranslatedContent = GetAll<TagTranslatedContent>();
                if (tagTranslatedContent != null)
                {
                    foreach (var tag in tags)
                    {
                        tag.TagTranslatedContent = tagTranslatedContent.Where(t => t.TagId == tag.TagId).ToList();
                    }

                    Cache.SaveList<Tag>(tags); //ensure tag collection is cached with translations
                }
            }

            return tags;
        }

        public IList<Tag> GetFeatureTags()
        {
            return GetAll<Tag>();
        }

        public IList<FeatureLocation> GetFeatureLocations()
        {
            return GetAll<FeatureLocation>();
        }

        public IList<Segmentation> GetSegmentations()
        {
            return GetAll<Segmentation>();
        }

        public IList<User> GetUsers()
        {
            return GetAll<User>();
        }

        public IList<Status> GetStatus()
        {
            return GetAll<Status>();
        }

        public IList<T> GetAll<T>(string orderBy = "", string tableName = null) where T : class, new()
        {
            if (tableName == string.Empty)
            {
                throw new ArgumentException("The parameter cannot be empty", "tableName");
            }

            var result = Cache.GetList<T>();
            if (result == null)
            {
                result = Database.RetrieveAll<T>(tableName ?? typeof(T).Name, orderBy);
                Cache.SaveList(result);
            }

            return result;
        }

        public void Invalidate<T>(string key = null) where T : class
        {
            Cache.Clear<T>(key);
        }

        public void InvalidateTags()
        {
            Invalidate<TagTree>();
            Invalidate<Tag>();
            Invalidate<TagTranslatedContent>();
            Invalidate<TagCategory>();
        }

        public void InvalidateSegmentations()
        {
            Invalidate<Segmentation>();
        }

        public void InvalidateAllTags(string key = null)
        {
            Invalidate<TagTree>(key);
            Invalidate<Tag>(key);
            Invalidate<TagTranslatedContent>(key);
            Invalidate<TagCategory>(key);
            Invalidate<TagTreeNode>(key);
        }

        public void InvalidateAllTagsWithType()
        {
            InvalidateKeysWithType<TagTree>();
            InvalidateKeysWithType<Tag>();
            InvalidateKeysWithType<TagTranslatedContent>();
            InvalidateKeysWithType<TagCategory>();
            InvalidateKeysWithType<TagTreeNode>();
        }

        public void InvalidateKeysWithType<T>() where T : class
        {
            var keys = CachingService.Keys.Where(x => x.Contains(typeof(T).Name));
            foreach (var key in keys)
            {
                CachingService.Delete(key);
            }
        }

        public void InvalidateUsers()
        {
            Invalidate<User>();
        }

        public void InvalidateStatus()
        {
            Invalidate<Status>();
        }

        public void InvalidateQueries()
        {
            InvalidateKeys("SLAM_{0}".F(Constants.Cache.Query));
        }

        private void InvalidateKeys(string startingWith)
        {
            var keys = CachingService.Keys.Where(x => x.StartsWith(startingWith, StringComparison.InvariantCultureIgnoreCase));
            foreach (var key in keys)
            {
                CachingService.Delete(key);
            }
        }

        #endregion

        #region Tag Tree

        public TagTree GetTagTree()
        {
            var tagTree = Cache.Get<TagTree>();
            if (tagTree == null)
            {
                if (TagTreeBuilder == null)
                {
                    throw new Exception("There is no TagTreeBuilder specified");
                }

                tagTree = TagTreeBuilder.Build();

                if (tagTree != null)
                {
                    Cache.Save(tagTree);
                }
            }

            return (TagTree)tagTree?.Clone(); //All further modifications should not change cached object
        }

        #endregion
    }
}