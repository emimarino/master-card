﻿using Mastercard.MarketingCenter.Common.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Slam.Cms.Data
{
    public class SqlCacheRepository : ISqlCacheRepository
    {
        private readonly SlamContext _slamContext;

        public SqlCacheRepository(SlamContext slamContext)
        {
            _slamContext = slamContext;
        }

        public IEnumerable<SqlCache> GetAllInvalidators()
        {
            var sql = @"select Id, LastInvalidationCalled, MustInvalidate 
						from [Cache] 
						where MustInvalidate = 1
						and datediff(ss, LastInvalidationCalled, getdate()) > 0.1";

            return _slamContext.Database.ExecuteReader<SqlCache>(sql);
        }

        public void SaveRegionInvalidator(string cacheId)
        {
            var sql = @"IF EXISTS (SELECT 1 FROM Cache WHERE Id = '{0}')
		                    UPDATE Cache SET LastInvalidationCalled = GETDATE(), MustInvalidate = 1 WHERE Id = '{0}'
	                    ELSE	
		                    INSERT INTO Cache (Id, LastInvalidationCalled, MustInvalidate) VALUES ('{0}', GETDATE(), 1)"
                      .F(cacheId);

            _slamContext.Database.ExecuteNonQuery(sql);
        }

        public void MarkInvalidated(IEnumerable<string> ids)
        {
            var commaSeparatedIds = string.Join(",", ids.Select(id => "'{0}'".F(id)));
            var sql = @"update [Cache] set MustInvalidate = 0 where Id in ({0})".F(commaSeparatedIds);

            _slamContext.Database.ExecuteNonQuery(sql);
        }
    }
}