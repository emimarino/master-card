﻿using Mastercard.MarketingCenter.Common.Extensions;
using System;
using System.Collections.Generic;

namespace Slam.Cms.Data
{
    public class SlamCache
    {
        private readonly SlamContext slamContext;

        internal SlamCache(SlamContext slamContext)
        {
            this.slamContext = slamContext;
        }

        public IList<T> GetList<T>(string key = null) where T : class
        {
            return this.Get<IList<T>>(GetKeyByType<T>(key));
        }

        public void SaveList<T>(IList<T> list, string key = null) where T : class
        {
            this.Save(list, GetKeyByType<T>(key));
        }

        public T Get<T>(string key = null) where T : class
        {
            if (this.slamContext.CachingService == null)
                return (T)null;

            key = key ?? typeof(T).Name;

            return this.slamContext.CachingService.Get<T>("SLAM_{0}".F(key));
        }

        public void Save<T>(T instance, string key = null, TimeSpan? timeout = null) where T : class
        {
            if (this.slamContext.CachingService == null)
                return;

            key = key ?? typeof(T).Name;

            this.slamContext.CachingService.Save("SLAM_{0}".F(key), instance, timeout);
        }

        public void Clear<T>(string key = null) where T : class
        {
            if (this.slamContext.CachingService == null)
                return;

            this.slamContext.CachingService.Delete("SLAM_{0}".F(GetKeyByType<T>(key)));
        }

        public bool Contains(string key)
        {
            if (this.slamContext.CachingService == null)
                return false;

            return this.slamContext.CachingService.Contains("SLAM_{0}".F(key));
        }

        private static string GetKeyByType<T>(string key) where T : class
        {
            return (key ?? string.Empty) + typeof(T).Name;
        }
    }
}