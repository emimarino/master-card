﻿using Autofac;
using Slam.Cms.Common.Interfaces;
using System;
using System.Data;
using Assembly = System.Reflection.Assembly;

namespace Slam.Cms.Data
{
    public class SlamCmsDataModule : Module
    {
        private readonly bool _filterByCurrentRegion = false;
        private readonly Assembly _assemblyWithContentTypes;

        public SlamCmsDataModule(bool filterByCurrentRegion, Assembly assemblyWithContentTypes)
        {
            _filterByCurrentRegion = filterByCurrentRegion;
            _assemblyWithContentTypes = assemblyWithContentTypes;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SqlCacheRepository>().As<ISqlCacheRepository>();
            builder.RegisterType<HttpContextCachingService>().As<ICachingService>();

            // CmsContext
            builder.Register(c =>
            {
                var context = new SlamContext(c.Resolve<IPreviewMode>())
                    .SetKnownContentTypes(_assemblyWithContentTypes)
                    .SetCachingService(c.Resolve<ICachingService>())
                    .SetConnectionFactory(c.Resolve<Func<IDbConnection>>());

                if (_filterByCurrentRegion)
                {
                    context = context.SetSlamQueryInitializer(c.Resolve<SlamQueryInitializer>().Initialize);
                }

                return context;
            }).InstancePerLifetimeScope();
        }
    }
}