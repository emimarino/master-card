﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Slam.Cms.Data
{
    public class HttpContextCachingService : ICachingService
    {
        private static IDictionary<string, CacheItem> cache = new ConcurrentDictionary<string, CacheItem>();
        public HttpContextCachingService()
        {
        }

        public T Get<T>() where T : class
        {
            return this.Get<T>(typeof(T).Name);
        }

        public T Get<T>(string key) where T : class
        {
            CacheItem cacheItem;
            if (!cache.TryGetValue(key, out cacheItem))
                return null;
            if (DateTime.Now > cacheItem.ExpirationDate)
            {
                cache.Remove(key);
                return null;
            }
            return cacheItem.Value as T;
        }

        public void Save<T>(T instance, TimeSpan? timeout = null) where T : class
        {
            this.Save(typeof(T).Name, instance, timeout);
        }

        public void Save<T>(string key, T instance, TimeSpan? timeout = null) where T : class
        {
            var expirationDate = timeout.HasValue ? DateTime.Now.Add(timeout.Value) : (DateTime?)null;
            var cacheItem = new CacheItem
            {
                ExpirationDate = expirationDate,
                Value = instance
            };

            if (cache.ContainsKey(key))
            {
                cache[key] = cacheItem;
            }
            else
            {
                ((ConcurrentDictionary<string, CacheItem>)cache).TryAdd(key, cacheItem);
            }
        }

        public void Delete<T>() where T : class
        {
            this.Delete(typeof(T).Name);
        }

        public void Delete(string key)
        {
            cache.Remove(key);
        }

        public bool Contains(string key)
        {
            CacheItem cacheItem;
            if (cache.TryGetValue(key, out cacheItem))
                return true;

            return false;
        }

        public IEnumerable<string> Keys
        {
            get { return cache.Keys; }
        }

        private class CacheItem
        {
            public DateTime? ExpirationDate { get; set; }
            public object Value { get; set; }
        }
    }
}