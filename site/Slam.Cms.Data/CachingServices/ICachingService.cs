﻿using System;
using System.Collections.Generic;

namespace Slam.Cms.Data
{
    public interface ICachingService
    {
        T Get<T>() where T : class;
        T Get<T>(string key) where T : class;
        void Save<T>(T instance, TimeSpan? timeout = null) where T : class;
        void Save<T>(string key, T instance, TimeSpan? timeout = null) where T : class;
        void Delete<T>() where T : class;
        void Delete(string key);
        bool Contains(string key);
        IEnumerable<string> Keys { get; }
    }
}