﻿using Slam.Cms.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using Slam.Cms.Common.Interfaces;
using Mastercard.MarketingCenter.Common.Extensions;

namespace Slam.Cms.Data
{
    public enum FilterOperator
    {
        Equal,
        Greater,
        GreaterOrEqual,
        Lower,
        LowerOrEqual
    }

    public enum FilterStatus
    {
        None,
        Live,
        PreviewMode,
        LiveOrRetired,
        LiveOrLiveAndDraft,
        DraftOnlyOrLiveAndDraft,
        DraftIfCurrent,
        LatestVersion,
        LiveOrExpired,
        GetAll
    }

    // TODO Include Features (include a list of the features for each content item)

    public class SlamQuery
    {
        private SlamQueryParameters parameters;
        private readonly IPreviewMode _previewMode;

        internal SlamQuery(SlamContext slamContext, IPreviewMode previewMode)
        {
            this.Context = slamContext;
            this.parameters = new SlamQueryParameters();
            this._previewMode = previewMode;

            if (this._previewMode.Enabled)
            {
                this.FilterStatus(Data.FilterStatus.PreviewMode);
                this.Cache(SlamQueryCacheBehavior.NoCache);
            }
        }

        #region Public Properties

        public ICachingService CachingService
        {
            get
            {
                return this.Context.CachingService;
            }
        }

        public SlamContext Context { get; private set; }

        public IDictionary<string, Type> ContentTypes
        {
            get
            {
                return this.parameters.ContentTypes;
            }
        }

        public bool TagsIncluded
        {
            get
            {
                return this.parameters.IncludeTags;
            }
        }

        public bool FeatureLocationsIncluded
        {
            get
            {
                return this.parameters.IncludeFeatureLocations;
            }
        }

        public bool SegmentationsIncluded
        {
            get
            {
                return this.parameters.IncludeSegmentations;
            }
        }

        internal SlamQueryParameters Parameters
        {
            get
            {
                return this.parameters;
            }
        }

        #endregion

        #region Cache

        public SlamQuery Cache(SlamQueryCacheBehavior cacheBehavior)
        {
            this.parameters.Cache.Behavior = cacheBehavior;
            return this;
        }

        public SlamQuery Cache(int duration)
        {
            this.parameters.Cache.Duration = duration;
            return this;
        }

        #endregion

        #region Named

        public SlamQuery Named(string name)
        {
            this.parameters.Name = name;
            return this;
        }

        #endregion

        #region Take

        public SlamQuery Take(int take)
        {
            this.parameters.Take = take;
            return this;
        }

        //public SlamQuery Page(int pageNumber, int pageSize)
        //{
        //    this.filters.PageFilter = PageFilter.Create(pageNumber, pageSize);
        //    return this;
        //}

        #endregion

        #region Include Tags

        public SlamQuery IncludeTags()
        {
            this.parameters.IncludeTags = true;
            return this;
        }

        #endregion

        #region Include Feature Tags

        public SlamQuery IncludeFeatureTags()
        {
            this.parameters.IncludeFeatureTags = true;
            return this;
        }

        #endregion

        #region Include Feature Locations

        public SlamQuery IncludeFeatureLocations()
        {
            this.parameters.IncludeFeatureLocations = true;
            return this;
        }

        #endregion

        #region Include Segmentations

        public SlamQuery IncludeSegmentations()
        {
            this.parameters.IncludeSegmentations = true;
            return this;
        }

        #endregion

        #region Filter Status

        public SlamQuery FilterStatus(FilterStatus filterStatus)
        {
            this.parameters.Status = filterStatus;
            return this;
        }

        #endregion

        #region Filter

        public SlamQuery Filter(string filter)
        {
            if (filter.IsNullOrWhiteSpace())
                return this;

            this.parameters.AdditionalFilters.Add(filter);
            return this;
        }

        #endregion

        #region Filter ContentItemId
        /// <summary>
        /// This function takes PrimaryContentIDs only as input wich are the same as the live ID. Draft ids containing "-p" won't work.
        /// </summary>
        /// <param name="primaryContentItemIds">Takes primary content ids as separated strings or collection,draft ids containing "-p" won't work </param>
        /// <returns></returns>

        public SlamQuery FilterContentItemId(params string[] primaryContentItemIds)
        {
            if (primaryContentItemIds == null)
                return this;

            this.parameters.ContentItemIds.AddRange(primaryContentItemIds);
            return this;
        }

        #endregion

        #region Filters Content Types

        public SlamQuery FilterContentTypes<T>() where T : ContentItem, new()
        {
            return this.FilterContentTypes(typeof(T));
        }

        public SlamQuery FilterContentTypes<T1, T2>()
            where T1 : ContentItem, new()
            where T2 : ContentItem, new()
        {
            return this.FilterContentTypes(typeof(T1), typeof(T2));
        }

        public SlamQuery FilterContentTypes<T1, T2, T3>()
            where T1 : ContentItem, new()
            where T2 : ContentItem, new()
            where T3 : ContentItem, new()
        {
            return this.FilterContentTypes(typeof(T1), typeof(T2), typeof(T3));
        }

        public SlamQuery FilterContentTypes<T1, T2, T3, T4>()
            where T1 : ContentItem, new()
            where T2 : ContentItem, new()
            where T3 : ContentItem, new()
            where T4 : ContentItem, new()
        {
            return this.FilterContentTypes(typeof(T1), typeof(T2), typeof(T3), typeof(T4));
        }

        public SlamQuery FilterContentTypes<T1, T2, T3, T4, T5>()
            where T1 : ContentItem, new()
            where T2 : ContentItem, new()
            where T3 : ContentItem, new()
            where T4 : ContentItem, new()
            where T5 : ContentItem, new()
        {
            return this.FilterContentTypes(typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5));
        }

        public SlamQuery FilterContentTypes(params Type[] types)
        {
            return this.FilterContentTypes(types.AsEnumerable());
        }

        public SlamQuery FilterContentTypes(IEnumerable<Type> types)
        {
            if (!types.Any())
                throw new ArgumentException("No ContentTypes specified", "types");

            foreach (var type in types)
            {
                if (type.BaseType != typeof(ContentItem))
                    throw new Exception("The type {0} does not inherit from ContentItem.".F(type.Name));

                if (type.IsAbstract)
                    throw new Exception("The type {0} is abstract".F(type.Name));

                if (this.parameters.ContentTypes.ContainsKey(type.Name))
                    throw new Exception("The type {0} has been already added.".F(type.Name));

                this.parameters.AddContentType(type);
            }

            return this;
        }

        #endregion

        #region Filter Tagged

        public SlamQuery FilterPseudoTaggedByParentTags(IEnumerable<string> parentTagIdentifiers)
        {
            if (parentTagIdentifiers != null)
            {
                this.parameters.PseudoTagParents.AddRange(parentTagIdentifiers);
            }

            return this;
        }

        public SlamQuery FilterTaggedWith(params string[] tagIdentifiers)
        {
            if (tagIdentifiers != null && tagIdentifiers.Length > 0)
            {
                this.FilterTaggedWith(tagIdentifiers.AsEnumerable());
            }

            return this;
        }

        public SlamQuery FilterTaggedWith(IEnumerable<string> tagIdentifiers, bool includeTagIdentifiers = false)
        {
            if (tagIdentifiers != null)
            {
                this.parameters.TaggedWithAll.AddRange(tagIdentifiers);
            }

            this.parameters.IncludeTagIdentifiers = includeTagIdentifiers;

            return this;
        }

        public SlamQuery FilterLocationByName()
        {
            this.parameters.FilterLocationById = false;
            return this;
        }

        public SlamQuery FilterTaggedWithAny(params string[] tagIdentifiers)
        {
            if (tagIdentifiers != null && tagIdentifiers.Length > 0)
            {
                this.FilterTaggedWithAny(tagIdentifiers.AsEnumerable());
            }

            return this;
        }

        public SlamQuery FilterTaggedWithAny(IEnumerable<string> tagIdentifiers, bool includeTagIdentifiers = false)
        {
            if (tagIdentifiers != null)
            {
                this.parameters.TaggedWithAny.AddRange(tagIdentifiers);
            }

            this.parameters.IncludeTagIdentifiers = includeTagIdentifiers;

            return this;
        }

        #endregion

        #region Filter Feature Tagged

        public SlamQuery FilterFeatureTaggedWith(params string[] tagIdentifiers)
        {
            if (tagIdentifiers != null && tagIdentifiers.Length > 0)
                this.FilterFeatureTaggedWith(tagIdentifiers.AsEnumerable());
            return this;
        }

        public SlamQuery FilterFeatureTaggedWith(IEnumerable<string> tagIdentifiers)
        {
            if (tagIdentifiers != null)
                this.parameters.FeatureTaggedWithAll.AddRange(tagIdentifiers);
            return this;
        }

        public SlamQuery FilterFeatureTaggedWithAny(params string[] tagIdentifiers)
        {
            if (tagIdentifiers != null && tagIdentifiers.Length > 0)
                this.FilterFeatureTaggedWithAny(tagIdentifiers.AsEnumerable());
            return this;
        }

        public SlamQuery FilterFeatureTaggedWithAny(IEnumerable<string> tagIdentifiers)
        {
            if (tagIdentifiers != null)
                this.parameters.FeatureTaggedWithAny.AddRange(tagIdentifiers);
            return this;
        }

        #endregion

        #region Filter Featured

        public SlamQuery FilterFeaturedOnTag(string tagIdentifier, FilterOperator filterOperator, int featureLevel)
        {
            if (tagIdentifier.IsNullOrWhiteSpace())
                return this;

            this.parameters.FeaturedOnTag.Add(FeaturedOnTag.Create(tagIdentifier, filterOperator, featureLevel));
            return this;
        }
        public SlamQuery FilterFeaturedOnAnyTag(string[] tagIdentifiers, FilterOperator filterOperator, int featureLevel)
        {
            if (tagIdentifiers == null)
                return this;
            foreach (var tagid in tagIdentifiers)
                this.parameters.FeaturedOnAnyTag.Add(tagid);

            return this;
        }

        public SlamQuery FilterFeaturedOnAnyTag(string tagIdentifiers, FilterOperator filterOperator, int featureLevel)
        {
            return FilterFeaturedOnAnyTag(tagIdentifiers.Split(','), filterOperator, featureLevel);
        }

        public SlamQuery FilterFeaturedOnAnyTag(List<string> tagIdentifiers, FilterOperator filterOperator, int featureLevel)
        {
            return FilterFeaturedOnAnyTag(tagIdentifiers.ToArray(), filterOperator, featureLevel);
        }

        public SlamQuery FilterFeaturedOnLocation(string location, FilterOperator filterOperator, int featureLevel)
        {
            if (location.IsNullOrWhiteSpace())
                return this;

            this.parameters.FeaturedOnLocation.Add(FeaturedOnLocation.Create(location, filterOperator, featureLevel));
            return this;
        }

        #endregion

        #region Filter Feature Located

        public SlamQuery FilterFeatureLocatedWith(string[] featureLocations)
        {
            if (featureLocations != null && featureLocations.Length > 0)
                this.parameters.FeatureLocatedWith = LocatedWith.Create(featureLocations, false);
            return this;
        }

        public SlamQuery FilterFeatureLocatedWith(IEnumerable<string> featureLocations)
        {
            if (featureLocations != null)
                this.FilterFeatureLocatedWith(featureLocations.ToArray());
            return this;
        }

        public SlamQuery FilterFeatureLocatedWithAny(string[] featureLocations)
        {
            if (featureLocations != null && featureLocations.Length > 0)
                this.parameters.FeatureLocatedWith = LocatedWith.Create(featureLocations, true);
            return this;
        }

        public SlamQuery FilterFeatureLocatedWithAny(IEnumerable<string> featureLocations)
        {
            if (featureLocations != null)
                this.FilterFeatureLocatedWithAny(featureLocations.ToArray());
            return this;
        }

        #endregion

        #region Filter Related To Content Item

        public SlamQuery FilterRelatedTo(string contentItemId)
        {
            if (contentItemId.IsNullOrWhiteSpace())
                return this;

            this.parameters.RelatedToContentItemId = contentItemId;
            return this;
        }

        #endregion

        #region Filter Related To Tags

        public SlamQuery FilterRelatedToTags(params string[] tagIdentifiers)
        {
            if (tagIdentifiers != null && tagIdentifiers.Length > 0)
                this.parameters.RelatedToTags = tagIdentifiers;
            return this;
        }

        public SlamQuery FilterRelatedToTags(IEnumerable<string> tagIdentifiers)
        {
            if (tagIdentifiers != null)
                this.FilterRelatedToTags(tagIdentifiers.ToArray());
            return this;
        }

        public SlamQuery FilterRelatedToRequiredTags(params string[] tagIdentifiers)
        {
            if (tagIdentifiers != null && tagIdentifiers.Length > 0)
                this.parameters.RelatedToRequiredTags = tagIdentifiers;
            return this;
        }

        public SlamQuery FilterRelatedToRequiredTags(IEnumerable<string> tagIdentifiers)
        {
            if (tagIdentifiers != null)
                this.FilterRelatedToRequiredTags(tagIdentifiers.ToArray());
            return this;
        }

        #endregion

        #region Expired

        public SlamQuery AllowExpired()
        {
            this.parameters.isExpired = null;
            if (this.parameters.Status == Data.FilterStatus.Live) //if Live status filter is applied, change it to include Expired
                this.parameters.Status = Data.FilterStatus.LiveOrExpired;
            return this;
        }

        public SlamQuery GetNonExpired()
        {
            this.parameters.isExpired = false;
            return this;
        }

        #endregion

        #region Filter Region

        public SlamQuery FilterRegion(string region)
        {
            if (region.IsNullOrWhiteSpace())
                return this;

            this.parameters.Region = region;
            return this;
        }

        public SlamQuery IncludeAllRegions()
        {
            this.parameters.Region = null;
            return this;
        }

        #endregion

        #region Filter Segmented

        public SlamQuery FilterSegmentedWith(IEnumerable<string> segmentationIdentifiers, bool includeEmptySegmentations = true)
        {
            if (segmentationIdentifiers != null && segmentationIdentifiers.Any(s => !s.Equals(RegionConfigManager.DefaultSegmentation, StringComparison.InvariantCultureIgnoreCase)))
            {
                this.parameters.SegmentedWithAll.AddRange(segmentationIdentifiers);
            }

            this.parameters.IncludeEmptySegmentations = includeEmptySegmentations;

            return this;
        }

        public SlamQuery FilterSegmentations(IEnumerable<string> segmentationIdentifiers)
        {
            if (segmentationIdentifiers != null && segmentationIdentifiers.Any(s => s.Equals(RegionConfigManager.NoneSegmentation, StringComparison.InvariantCultureIgnoreCase)))
            {
                return this.FilterOnlyIncludeEmptySegmentations();
            }
            else
            {
                return this.FilterSegmentedWithAny(segmentationIdentifiers);
            }
        }

        public SlamQuery FilterSegmentedWithAny(IEnumerable<string> segmentationIdentifiers)
        {
            if (segmentationIdentifiers != null && segmentationIdentifiers.Any(s => !s.Equals(RegionConfigManager.DefaultSegmentation, StringComparison.InvariantCultureIgnoreCase)))
            {
                this.parameters.SegmentedWithAny.AddRange(segmentationIdentifiers);
            }

            this.parameters.IncludeEmptySegmentations = true;

            return this;
        }

        public SlamQuery FilterOnlyIncludeEmptySegmentations()
        {
            this.parameters.IncludeOnlyEmptySegmentations = true;

            return this;
        }

        #endregion

        #region Order By

        public SlamQuery OrderByFeaturedOnTag(string tagIdentifier)
        {
            if (tagIdentifier.IsNullOrWhiteSpace())
                return this;

            this.parameters.OrderByFeatureOnTagLevel = tagIdentifier;
            return this;
        }

        public SlamQuery OrderByFeaturedOnLocation(string featureLocationId)
        {
            if (featureLocationId.IsNullOrWhiteSpace())
                return this;

            this.parameters.OrderByFeatureOnLocationLevel = featureLocationId;
            return this;
        }

        public SlamQuery FilterByTagCategory(string tagCategory)
        {
            this.parameters.TagCategoryIds = new List<string>() { tagCategory };
            this.parameters.IncludeFeatureTags = true;
            return this;
        }

        public SlamQuery FilterByTagCategory(List<string> tagCategory)
        {
            this.parameters.TagCategoryIds = tagCategory;
            this.parameters.IncludeFeatureTags = true;
            return this;
        }

        public SlamQuery FilterByTagCategory(string[] tagCategory)
        {
            this.parameters.TagCategoryIds = tagCategory.ToList();
            this.parameters.IncludeFeatureTags = true;
            return this;
        }

        public SlamQuery OrderBy(string orderBy)
        {
            this.parameters.OrderBy = orderBy;
            return this;
        }

        #endregion

        #region Query Construction

        public override string ToString()
        {
            var builder = new SlamQueryBuilder(this.Context);
            return builder.BuildQuery(this.parameters);
        }

        #endregion

        #region QueryExecution

        public SlamQueryResult<T> Get<T>(string[] queryKeys = null) where T : ContentItem, new()
        {
            return this.GetInternal<T>(queryKeys);
        }

        public SlamQueryResult<ContentItem> Get(string[] queryKeys = null)
        {
            return this.GetInternal<ContentItem>(queryKeys);
        }

        public T GetFirstOrDefault<T>(string[] queryKeys = null) where T : ContentItem, new()
        {
            this.Take(1);
            return this.GetInternal<T>(queryKeys).FirstOrDefault();
        }

        public ContentItem GetFirstOrDefault(string[] queryKeys = null)
        {
            this.Take(1);
            return this.GetInternal<ContentItem>(queryKeys).FirstOrDefault();
        }

        public int Count()
        {
            this.parameters.CountOnly = true;

            var sql = this.ToString();

            this.parameters.CountOnly = false;

            return (int)this.Context.Database.ExecuteScalar(sql);
        }

        protected SlamQueryResult<T> GetInternal<T>(string[] queryKeys = null) where T : ContentItem
        {
            string queryKey = string.Empty;
            if (queryKeys != null)
                queryKeys.ToList().ForEach(k => queryKey += ("{0}_".F(k)));

            var queryCacheKey = "{0}_{1}{2}".F(Constants.Cache.Query, queryKey, this.parameters.GetHashCode());
            if (this.parameters.Cache.Behavior != SlamQueryCacheBehavior.NoCache && this.Context.Cache.Contains(queryCacheKey))
            {
                var cachedResult = this.Context.Cache.Get<SlamQueryResult<T>>(queryCacheKey);
                if (cachedResult != null)
                    return cachedResult.Clone();
            }

            var contentSearchResults = new SlamQueryResult<T>(this.Context, this.parameters);

            if (contentSearchResults != null)
                this.Context.Cache.Save(contentSearchResults, queryCacheKey, TimeSpan.FromMinutes(this.parameters.Cache.Duration));

            return contentSearchResults.Clone();
        }

        #endregion
    }
}