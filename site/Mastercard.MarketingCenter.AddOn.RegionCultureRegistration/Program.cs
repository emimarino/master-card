﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using System;
using System.Diagnostics;

namespace Mastercard.MarketingCenter.AddOn.RegionCultureRegistration
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 1 && args[0].Equals("default", StringComparison.InvariantCultureIgnoreCase))
            {
                SetRegionCultures("en", "English", "en-US");
                SetRegionCultures("es", "Spanish", "es-ES");
                SetRegionCultures("pt", "Portuguese", "pt-PT");
            }
            else if (args.Length < 3)
            {
                Log("Any of the following arguments is missed: Language Code | Language Name | Base Culture. For example: en English en-US - Or just write default to register the default languages: English, Spanish and Portuguese.");
            }
            else if (args.Length == 4 && args[3].Equals("true", StringComparison.InvariantCultureIgnoreCase))
            {
                Log($"Start: {args[0]} - {args[1]} - {args[2]}");
                RegionalizeService.SetRegionCulture($"{args[0]}", $"{args[1]}", args[2]);
                Log($"Completed: {args[0]} - {args[1]} - {args[2]}");
            }
            else
            {
                SetRegionCultures(args[0], args[1], args[2]);
            }
        }

        private static void SetRegionCultures(string languageCode, string languageName, string baseCulture)
        {
            Log($"Start: {languageName}");
            using (var context = new MarketingCenterDbContext())
            {
                var regionService = new RegionService(new RegionRepository(context));
                foreach (var region in regionService.GetAll())
                {
                    try
                    {
                        Log($"Start: {region.Name}");
                        RegionalizeService.SetRegionCulture($"{languageCode}-{region.IdTrimmed.ToUpper()}", $"{region.Name} {languageName}", baseCulture);
                        Log($"Completed: {region.Name}");
                    }
                    catch (Exception ex)
                    {
                        Log($"Failed: {ex.Message}");
                    }
                }
            }
            Log($"Completed: {languageName}");
        }

        static void Log(string Message)
        {
            Trace.TraceInformation($"{DateTime.Now}: {Message}");
        }
    }
}