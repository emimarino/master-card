﻿This project is a helper to register Custom Region Cultures on the Server.

The application expects 4 parameters in the following order: Language Code | Language Name | Base Culture | Ignore Regions (optional)
Or the default parameter to register the default languages: English, Spanish and Portuguese
The application will register the selected language for each site region or just create the new language if the Ignore Regions parameter is true.

Examples:
-- Mastercard.MarketingCenter.AddOn.RegionCultureRegistration.exe default
-- Mastercard.MarketingCenter.AddOn.RegionCultureRegistration.exe en English en-US
-- Mastercard.MarketingCenter.AddOn.RegionCultureRegistration.exe es Spanish es-ES
-- Mastercard.MarketingCenter.AddOn.RegionCultureRegistration.exe pt Portuguese pt-PT
-- Mastercard.MarketingCenter.AddOn.RegionCultureRegistration.exe deu Deutsch de-DE true

The application must be run as administrator.