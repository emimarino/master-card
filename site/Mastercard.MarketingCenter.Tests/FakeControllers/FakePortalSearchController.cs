﻿using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastercard.MarketingCenter.Tests.FakeControllers
{
    public class FakePortalSearchController : SearchController
    {
        public FakePortalSearchController(IMarketingCenterWebApplicationService marketingCenterWebApplicationService, IUrlService _urlService) : base(marketingCenterWebApplicationService, _urlService)
        {
            
        }
    }
}
