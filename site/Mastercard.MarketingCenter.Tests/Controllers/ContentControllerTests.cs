﻿using Mastercard.MarketingCenter.Cms.Controllers;
using Mastercard.MarketingCenter.Cms.Models;
using Mastercard.MarketingCenter.Cms.Services.Infrastructure;
using Mastercard.MarketingCenter.Tests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace Mastercard.MarketingCenter.Tests.Controllers
{
    [TestClass]
    public class ContentControllerTests : BaseTests
    {
        [TestMethod]
        [Ignore]
        public void Return_Validation_Error_When_Updating_With_Empty_Title()
        {
            var controller = GetController();
            var model = new ContentModel
            {
                ContentItemData = new Dictionary<string, object> { { "Title", string.Empty }, { "ContentItemId", "x" } }
            };
            var snippetContentType = ContentTypeDao.GetById("e8");
            model.ContentTypeFieldDefinitions = snippetContentType.ContentTypeFieldDefinitions;
            model.ContentTypeId = snippetContentType.ContentTypeId;

            var result = controller.Update(model, ContentActions.SaveLive) as ViewResult;

            Assert.IsTrue(result.ViewData.ModelState["Title"].Errors.Any());
        }

        [TestMethod]
        [Ignore]
        public void When_Validation_Is_Ok_Should_Redirect_To_List()
        {
            var controller = GetController();
            var model = new ContentModel
            {
                ContentItemData = new Dictionary<string, object> { { "Title", "Title" }, { "ContentItemId", "10" } }
            };
            var snippetContentType = ContentTypeDao.GetById("e8");
            model.ContentTypeFieldDefinitions = snippetContentType.ContentTypeFieldDefinitions;
            model.ContentTypeId = snippetContentType.ContentTypeId;

            var result = controller.Update(model, ContentActions.SaveLive) as RedirectToRouteResult;

            Assert.AreEqual(result.RouteValues["action"], "List");
            Assert.AreEqual(result.RouteValues["controller"], "Content");
            Assert.AreEqual(result.RouteValues["id"], "e8");
        }

        public ContentController GetController()
        {
            var controller = new ContentController(ContentTypeDao, ContentService, ContentItemDao, CloningServices, ContentItemService, UnitOfWork, SettingsService, PreviewMode, TagServices, AttachmentServices, SecondaryImageServices, FeatureOnServices, LookupServices, FileUploadService);
            var context = new HttpContextWrapper(HttpContext.Current)
            {
                User = new RolePrincipal(new GenericIdentity("mastercardmembers:6f0fe4e0-3ddf-416d-82e1-f1b9f2ecf507"))
            };
            controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

            return controller;
        }
    }
}