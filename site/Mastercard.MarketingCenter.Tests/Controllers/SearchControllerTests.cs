﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Tests.Common;
using Mastercard.MarketingCenter.Tests.FakeControllers;
using Mastercard.MarketingCenter.Web.Core.Models.Search;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Mastercard.MarketingCenter.Tests.Controllers
{
    [TestClass]
    public class SearchControllerTests : BaseTests
    {
        [TestMethod]
        public void When_Search_Keywords_Without_Matchs_Should_Return_Empty_Results()
        {
            // Arrange

            var currentSegmentationsId = new List<string>() { };
            MockMarketingCenterWebApplicationService.Setup(x => x.GetCurrentSegmentationIds())
                .Returns(currentSegmentationsId);
            //TODO IMPORT URLSERVICE
            var controller = new FakePortalSearchController(MockMarketingCenterWebApplicationService.Object, null);
            string keywords = "test";

            // Act
            var result = controller.Index(keywords) as ViewResult;
            var model = result.Model as IEnumerable<SearchViewModel>;

            // Assert
            Assert.IsFalse(model.Any());
        }

        [TestMethod]
        public void When_Search_Keywords_With_Matchs_Should_Return_Populated_Results()
        {
            // Arrange

            var currentSegmentationsId = new List<string>() { };
            MockMarketingCenterWebApplicationService.Setup(x => x.GetCurrentSegmentationIds())
                .Returns(currentSegmentationsId);
            //TODO IMPORT URLSERVICE
            var controller = new FakePortalSearchController(MockMarketingCenterWebApplicationService.Object, null);
            string keywords = "test";

            // Act
            var result = controller.Index(keywords) as ViewResult;
            var model = result.Model as IEnumerable<SearchViewModel>;

            // Assert
            Assert.IsTrue(model.Any());
        }
    }
}
