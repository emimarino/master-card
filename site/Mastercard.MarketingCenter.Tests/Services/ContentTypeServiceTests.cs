﻿using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Mastercard.MarketingCenter.Tests.Services
{
    [TestClass]
    [Ignore]
    public class ContentTypeServicesTests : RolledBackDataContextTests<MarketingCenterDbContext>
    {
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Cant_Delete_Content_Type_When_Is_Referenced()
        {
            var contentTypeDao = new ContentTypeDao(Context);
            var contentItemDao = new ContentItemDao(Context);

            var service = new ContentTypeService(contentItemDao, contentTypeDao);

            service.DeleteContentType("e8");
        }

        [TestMethod]
        public void Cant_Delete_Content_Type_When_Is_Not_Referenced()
        {
            const string contentTypeId = "6IF_Z_WrQ0WGmf0qlPnV2w";

            var contentTypeDao = new ContentTypeDao(Context);
            var contentItemDao = new ContentItemDao(Context);

            var service = new ContentTypeService(contentItemDao, contentTypeDao);
            service.DeleteContentType(contentTypeId);

            Context.SaveChanges();

            Assert.IsNull(contentTypeDao.GetById(contentTypeId));
        }
    }
}