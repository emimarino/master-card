﻿using Mastercard.MarketingCenter.Cms.Services.Infrastructure;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Tests.Services
{
    [TestClass]
    public class TagServicesTests : RolledBackDataContextTests<MarketingCenterDbContext>
    {
        [TestMethod]
        public void Can_Read_Tags_Values_For_Item()
        {
            var service = GetService();

            var items = service.GetSelectedTags("1");

            Assert.IsTrue(items.Any(o => o.Value == "Merchant Offers"));
            Assert.IsTrue(items.Any(o => o.Id == "merchant-offers"));
        }

        [TestMethod]
        public void Can_Get_Tags_For_Item_And_Category()
        {
            var service = GetService();

            var items = service.GetItemsTagsForCategory("3", "23c");

            Assert.AreEqual(items.Count(), 1);
            Assert.IsTrue(items.Any(o => o.Value == "Lifecycle Marketing"));
            Assert.IsTrue(items.Any(o => o.Id == "lifecycle-marketing"));
        }

        [TestMethod]
        public void Can_Set_Tag_Value_To_Item()
        {
            var service = GetService();

            var contentData = new Dictionary<string, object> { { "Marketing", "financial-literacy" } };

            service.SetTags("1", contentData);

            var items = service.GetSelectedTags("1");
            Assert.IsTrue(items.Any(o => o.Id == "financial-literacy"));
        }

        [TestMethod]
        public void Can_Set_Empty_Tag_Value_To_Item()
        {
            var service = GetService();

            var contentData = new Dictionary<string, object> { { "Marketing", string.Empty } };

            service.SetTags("1", contentData);

            var items = service.GetSelectedTags("1");

            Assert.IsFalse(items.Any());
        }

        [TestMethod]
        [Ignore]
        public void Can_Set_Tag_Values_To_Item()
        {
            var service = GetService();

            var contentData = new Dictionary<string, object> { { "Marketing", "financial-literacy, mastercard-marketplace, priceless-partnerships" } };

            service.SetTags("1", contentData);

            var items = service.GetSelectedTags("1");

            Assert.AreEqual(items.Count(), 3);
            Assert.IsTrue(items.Any(o => o.Id == "financial-literacy"));
            Assert.IsTrue(items.Any(o => o.Id == "mastercard-marketplace"));
            Assert.IsTrue(items.Any(o => o.Id == "priceless-partnerships"));
        }

        [TestMethod]
        [Ignore]
        public void Can_Set_Tag_Values_Of_Different_Categories_To_Item()
        {
            var service = GetService();

            var contentData = new Dictionary<string, object> { { "Marketing", "financial-literacy, mastercard-marketplace, priceless-partnerships" }, { "SegmentsAudiences", "mass-market, small-business" } };

            service.SetTags("1", contentData);

            var items = service.GetSelectedTags("1");

            Assert.AreEqual(items.Count(), 5);
            Assert.IsTrue(items.Any(o => o.Id == "financial-literacy"));
            Assert.IsTrue(items.Any(o => o.Id == "mastercard-marketplace"));
            Assert.IsTrue(items.Any(o => o.Id == "priceless-partnerships"));

            Assert.IsTrue(items.Any(o => o.Id == "mass-market"));
            Assert.IsTrue(items.Any(o => o.Id == "small-business"));
        }

        [TestMethod]
        public void Can_Set_Same_Tag_Value_Of_Different_Categories_To_Item()
        {
            var service = GetService();

            var contentData = new Dictionary<string, object> { { "Marketing", "seasonal" }, { "SegmentsAudiences", "seasonal" } };

            service.SetTags("1", contentData);

            var items = service.GetSelectedTags("1");

            Assert.AreEqual(items.Count(), 1);
            Assert.IsTrue(items.Any(o => o.Id == "seasonal"));
        }

        private TagServices GetService()
        {
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            return new TagServices(cmsDbConnection,
                                   new ContentTypeDao(Context),
                                   new TagCategoryDao(Context),
                                   new TagDao(Context),
                                   new TagHierarchyPositionDao(Context),
                                   new CommonService(cmsDbConnection));
        }
    }
}