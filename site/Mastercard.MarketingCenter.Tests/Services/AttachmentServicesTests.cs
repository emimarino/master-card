﻿using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Mastercard.MarketingCenter.Tests.Services
{
    [TestClass]
    public class AttachmentServicesTests : RolledBackDataContextTests<MarketingCenterDbContext>
    {
        [TestMethod]
        [Ignore]
        public void Can_Get_Attachments_For_Program_ContentItem()
        {
            var attachments = AttachmentServices.GetAttachments("1c1");

            Assert.AreEqual(attachments.Count(), 2);
            Assert.IsTrue(attachments.Any(o => o.FileUrl == "/Lists/Programs/Attachments/9/Fact_Sheet.pdf"));
            Assert.IsTrue(attachments.Any(o => o.FileUrl == "/Lists/Programs/Attachments/9/Sample_Creative.pdf"));
        }

        [TestMethod]
        public void Can_Save_Attachment_For_Program_Without_Existing_Attachment()
        {
            var attachments = new[] { "Attach1.txt", "Attach2.txt" };

            AttachmentServices.SaveAttachments("1bd", attachments);

            var allAttachments = AttachmentServices.GetAttachments("1bd");

            Assert.AreEqual(allAttachments.Count(), 2);
            Assert.IsTrue(allAttachments.Any(o => o.FileUrl == "Attach1.txt"));
            Assert.IsTrue(allAttachments.Any(o => o.FileUrl == "Attach2.txt"));
        }
    }
}