﻿using Mastercard.MarketingCenter.Cms.Services.Infrastructure;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Tests.Services
{
    [TestClass]
    public class LookupServicesTests : RolledBackDataContextTests<MarketingCenterDbContext>
    {
        [TestMethod]
        public void Can_Get_Simple_Lookup_Data_From_Settings()
        {
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            var service = new LookupServices(cmsDbConnection);
            var settings = new LookupSettings { TableIdField = "IconID", TableName = "Icon", TableValueField = "Name" };

            var values = service.GetLookupValues(settings);

            Assert.IsTrue(values.Any(o => o.Id == "2e"));
            Assert.IsTrue(values.Any(o => o.Value == "Faq"));
        }

        [TestMethod]
        public void Getting_Lookup_Items_For_ContentType_Lookup_Should_Not_Return_Draft_Items()
        {
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            var service = new LookupServices(cmsDbConnection);
            var settings = new LookupSettings
            {
                TableIdField = "ContentItemId",
                TableName = "Program",
                TableValueField = "Title",
                AssociationTableName = "OrderableAssetRelatedProgram",
                AssociationKeysMap = new[] { "ProgramContentItemId", "OrderableAssetContentItemId" },
                ForceDisableMultiSelection = true,
                ContentTypeLookup = true
            };

            var values = service.GetLookupValues(settings);

            Assert.IsFalse(values.Any(o => o.Id == "42e8-p")); // should not contain draft items
        }

        [TestMethod]
        [Ignore]
        public void Can_Get_Complex_Lookup_Data_From_Settings()
        {
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            var service = new LookupServices(cmsDbConnection);
            var settings = new LookupSettings { TableIdField = "TagID", TableName = "Tag", TableValueField = "DisplayName" };

            var values = service.GetLookupValues(settings);

            Assert.IsTrue(values.Any(o => o.Id == "45fe"));
            Assert.IsTrue(values.Any(o => o.Value == "Q4 Holiday Peace of Mind"));
        }

        [TestMethod]
        [Ignore]
        public void Can_Get_Complex_Lookup_Values()
        {
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            var service = new LookupServices(cmsDbConnection);
            var settings = new LookupSettings { TableIdField = "TagID", TableName = "Tag", TableValueField = "DisplayName", AssociationTableName = "ProgramBrowseAllMaterialTag", AssociationKeysMap = new[] { "TagID", "ContentItemId" } };

            var values = service.GetSelectedLookupValues("430d-p", settings);

            Assert.AreEqual(values.Count(), 1);
            Assert.IsTrue(values.Any(o => o == "45fe"));
        }

        [TestMethod]
        public void Can_Get_Complex_Lookup_Values2()
        {
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            var service = new LookupServices(cmsDbConnection);
            var settings = new LookupSettings { TableIdField = "ContentItemId", TableName = "Program", TableValueField = "Title", AssociationTableName = "OrderableAssetRelatedProgram", AssociationKeysMap = new[] { "ProgramContentItemId", "OrderableAssetContentItemId" } };

            var values = service.GetSelectedLookupValues("19", settings);

            Assert.IsTrue(values.Any());
        }

        [TestMethod]
        public void Can_Update_Selected_Lookup_Values()
        {
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            var service = new LookupServices(cmsDbConnection);
            var settings = new LookupSettings { TableIdField = "ContentItemId", TableName = "Program", TableValueField = "Title", AssociationTableName = "OrderableAssetRelatedProgram", AssociationKeysMap = new[] { "ProgramContentItemId", "OrderableAssetContentItemId" } };
            var values = new List<string> { "1c0", "240", "f1" };

            service.SetLookupValues("19", settings, values);

            var selectedValues = service.GetSelectedLookupValues("19", settings).ToList();

            Assert.AreEqual(selectedValues.Count(), 3);
            Assert.IsTrue(selectedValues.Any(o => o == "1c0"));
            Assert.IsTrue(selectedValues.Any(o => o == "240"));
            Assert.IsTrue(selectedValues.Any(o => o == "f1"));
        }
    }
}