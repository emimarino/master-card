﻿using Mastercard.MarketingCenter.Cms.Services.Infrastructure;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Mastercard.MarketingCenter.Tests.Services
{
    [TestClass]
    public class CustomizationServicesTests : RolledBackDataContextTests<MarketingCenterDbContext>
    {
        [TestMethod]
        public void Can_Get_Customizations_For_Asset()
        {
            var service = GetService();

            var customizations = service.GetContentItemCustomizations("1a");

            var firstCustomization = customizations.First(o => o.CustomizationOptionId == "12d");
            Assert.AreEqual(firstCustomization.State, CustomizationState.Required);

            var secondCustomization = customizations.First(o => o.CustomizationOptionId == "135");
            Assert.AreEqual(secondCustomization.State, CustomizationState.Required);

            var thirdCustomization = customizations.First(o => o.CustomizationOptionId == "14e");
            Assert.AreEqual(thirdCustomization.State, CustomizationState.Required);

            Assert.IsTrue(customizations.Except(new[] { firstCustomization, secondCustomization, thirdCustomization }).All(o => o.State != CustomizationState.Required));
        }

        [TestMethod]
        public void Can_Save_Customizations_For_Asset_Without_Any_Prior_Customization()
        {
            var service = GetService();

            service.SaveContentItemCustomizations("20d", new[] { "12d", "12e" }, new[] { "135" });

            var customizations = service.GetContentItemCustomizations("20d");

            var firstCustomization = customizations.First(o => o.CustomizationOptionId == "12d");
            Assert.AreEqual(firstCustomization.State, CustomizationState.Required);

            var secondCustomization = customizations.First(o => o.CustomizationOptionId == "12e");
            Assert.AreEqual(secondCustomization.State, CustomizationState.Required);

            var thirdCustomization = customizations.First(o => o.CustomizationOptionId == "12d");
            Assert.AreEqual(thirdCustomization.State, CustomizationState.Required);
        }

        [TestMethod]
        public void Can_Save_Customizations_For_Asset_With_Customizations()
        {
            var service = GetService();

            service.SaveContentItemCustomizations("1a", new[] { "12e" }, new[] { "135" });

            var customizations = service.GetContentItemCustomizations("1a");

            var firstCustomization = customizations.First(o => o.CustomizationOptionId == "12e");
            Assert.AreEqual(firstCustomization.State, CustomizationState.Required);

            var secondCustomization = customizations.First(o => o.CustomizationOptionId == "135");
            Assert.AreEqual(secondCustomization.State, CustomizationState.Optional);

            Assert.IsTrue(customizations.Except(new[] { firstCustomization, secondCustomization }).All(o => o.State != CustomizationState.Required || o.State != CustomizationState.Optional));
        }

        private CustomizationServices GetService()
        {
            return new CustomizationServices(new CmsDbConnection(Context.Database.Connection, new SettingsService()));
        }
    }
}