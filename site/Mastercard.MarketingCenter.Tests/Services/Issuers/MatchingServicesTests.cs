﻿using Mastercard.MarketingCenter.Cms.Services.Infrastructure;
using Mastercard.MarketingCenter.Cms.Services.Services.Issuers;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Mastercard.MarketingCenter.Tests.Services.Issuers
{
    [TestClass]
    public class MatchingServicesTests : RolledBackDataContextTests<MarketingCenterDbContext>
    {
        [TestMethod]
        [Ignore] // Because we are not matching by ICA number anymore
        public void Can_Auto_Match_By_Number()
        {
            var issuerRepository = GetIssuerRepository();
            var service = GetService(issuerRepository);

            service.RunAutoMatching();

            var matchesByNumber = issuerRepository.GetAll().Where(o => !string.IsNullOrEmpty(o.Cid));

            Assert.IsTrue(matchesByNumber.Any());

            matchesByNumber.ToList().ForEach(match =>
            {
                Assert.IsTrue(match.MatchDate.HasValue);
                Assert.IsFalse(string.IsNullOrEmpty(match.Cid));
            });
        }

        [TestMethod]
        public void Can_Auto_Match_By_Name()
        {
            var issuerRepository = GetIssuerRepository();
            var service = GetService(issuerRepository);

            service.RunAutoMatching();

            var matchesByName = issuerRepository.GetAll().Where(o => o.MatchConfirmed == false);

            Assert.IsTrue(matchesByName.Any());

            matchesByName.ToList().ForEach(match =>
            {
                Assert.IsTrue(match.MatchDate.HasValue);
                Assert.IsTrue(string.IsNullOrEmpty(match.Cid));
                Assert.IsFalse(match.MatchConfirmed.GetValueOrDefault());
            });
        }

        [TestMethod]
        public void Can_UnMatch()
        {
            var issuerRepository = GetIssuerRepository();
            var service = GetService(issuerRepository);

            service.RunAutoMatching();

            var issuer = issuerRepository.GetIssuerById("997");

            service.ClearMatch("997");

            Assert.IsFalse(issuer.MatchConfirmed.HasValue);
            Assert.IsTrue(string.IsNullOrEmpty(issuer.Cid));
            Assert.IsFalse(issuer.MatchDate.HasValue);
        }

        private MatchingServices GetService(IssuerRepository issuerRepository)
        {
            var marketingCenterDbContext = new MarketingCenterDbContext();
            var membershipProvider = new StandaloneMembershipProvider();
            var unitOfWork = new UnitOfWork(marketingCenterDbContext);
            var userService = new UserService(new UserRepository(marketingCenterDbContext, membershipProvider), new AuthorizationRepository(marketingCenterDbContext), new ConsentFileDataRepository(marketingCenterDbContext), new UserSubscriptionRepository(marketingCenterDbContext), unitOfWork);
            var processorService = new ProcessorService(new ProcessorRepository(marketingCenterDbContext));
            var segmentationService = new SegmentationService(Mapper, new SegmentationRepository(marketingCenterDbContext));
            return new MatchingServices(new CmsDbConnection(Context.Database.Connection, new SettingsService()), new IssuerService(new IdGeneratorService(Context.Database.Connection), issuerRepository, userService, processorService, segmentationService, unitOfWork), new ImportedIcaRepository(marketingCenterDbContext));
        }

        private IssuerRepository GetIssuerRepository()
        {
            var marketingCenterDbContext = new MarketingCenterDbContext();
            return new IssuerRepository(marketingCenterDbContext);
        }
    }
}