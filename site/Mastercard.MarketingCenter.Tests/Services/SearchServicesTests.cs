﻿using Mastercard.MarketingCenter.Cms.Services.Infrastructure;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;

namespace Mastercard.MarketingCenter.Tests.Services
{
    [TestClass]
    public class SearchServicesTests : RolledBackDataContextTests<MarketingCenterDbContext>
    {
        [TestMethod]
        [Ignore]
        public void Can_Get_Results_For_Search()
        {
            var userContext = new UserContext("us", "en");

            IEnumerable<SearchResult> results;
            using (var dbContext = new Data.MarketingCenterDbContext())
            {
                var service = new SearchServices(
                    new CmsDbConnection(Context.Database.Connection, new SettingsService()),
                    new MastercardAuthorizationService(
                        new CmsRoleAccessRepository(dbContext),
                        new AuthorizationRepository(dbContext),
                        new GenericPrincipal(new GenericIdentity("username"), new string[0]),
                        new AssetRepository(dbContext),
                        new RegionService(new RegionRepository(dbContext)), 
                        userContext,
                        new SettingsService())
                    );

                results = service.SearchFor("apple", "us");
            }

            Assert.IsTrue(results.Any());
        }
    }
}