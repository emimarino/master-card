﻿using Mastercard.MarketingCenter.Cms.Services.Services.ValueProviders;
using Mastercard.MarketingCenter.Data.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Tests.Services
{
    [TestClass]
    public class ValueProviderTests
    {
        [TestMethod]
        public void Can_Parse_Checkbox_Field_Value()
        {
            var checkboxField = new FieldDefinition
            {
                Data = "IBCU",
                FieldType = new FieldType { FieldTypeCode = FieldType.Checkbox },
                FieldTypeCode = FieldType.Checkbox
            };

            var valueProvider = ValueProviderFactory.Create(checkboxField.FieldType);
            var data = valueProvider.GetData(checkboxField.Data);

            Assert.AreEqual(data, ";#IBCU;#");
        }

        [TestMethod]
        public void Should_Return_Null_With_Null_Data_For_Checkbox_Field_Value()
        {
            var checkboxField = new FieldDefinition
            {
                Data = null,
                FieldType = new FieldType { FieldTypeCode = FieldType.Checkbox },
                FieldTypeCode = FieldType.Checkbox
            };

            var valueProvider = ValueProviderFactory.Create(checkboxField.FieldType);
            var data = valueProvider.GetData(checkboxField.Data);

            Assert.AreEqual(data, string.Empty);
        }

        [TestMethod]
        public void Can_Resolve_Values_For_ContentData_Based_On_Field_Definitions()
        {
            IDictionary<string, object> contentData = new Dictionary<string, object>
            {
                {"Title", "Title"},
                {"RestrictToSegments", "Option1,Option2,Option3"}
            };
            var fieldDefs = new List<FieldDefinition>
            {
                new FieldDefinition
                {
                    FieldType = new FieldType {FieldTypeCode = FieldType.SingleLineText},
                    Name = "Title"
                },
                new FieldDefinition
                {
                    FieldType = new FieldType {FieldTypeCode = FieldType.Checkbox},
                    Name = "RestrictToSegments"
                }
            };

            var valueProvider = new ContentDataValueProvider();

            contentData = valueProvider.GetContentData(fieldDefs, contentData);

            Assert.AreEqual(contentData["RestrictToSegments"], ";#Option1;#Option2;#Option3;#");
            Assert.AreEqual(contentData["Title"], "Title");
        }
    }
}
