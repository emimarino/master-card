﻿using Mastercard.MarketingCenter.Cms.Services.Infrastructure;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Mastercard.MarketingCenter.Tests.Services
{
    [TestClass]
    public class ListItemTrackingServicesTests : RolledBackDataContextTests<MarketingCenterDbContext>
    {
        [TestMethod]
        [Ignore]
        public void Can_Get_TrackingFields_For_ListItem()
        {
            var service = new ListItemTrackingServices(new CmsDbConnection(Context.Database.Connection, new SettingsService()), new ListDao(Context));

            var trackingFieldInfo = service.GetTrackingFieldsInfo("100e", 3);

            Assert.AreEqual(trackingFieldInfo.CreatedDate.Date, new DateTime(2011, 9, 21));
            Assert.AreEqual(trackingFieldInfo.ModifiedDate.GetValueOrDefault().Date, new DateTime(2015, 12, 10));
            Assert.AreEqual(trackingFieldInfo.CreatedBy, "Brian Jenkins");
            Assert.AreEqual(trackingFieldInfo.ModifiedBy, "Brian Jenkins");
        }
    }
}