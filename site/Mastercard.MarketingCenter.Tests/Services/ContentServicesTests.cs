﻿using Mastercard.MarketingCenter.Cms.Services.Infrastructure;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Data.Extensions;
using Mastercard.MarketingCenter.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Slam.Cms;
using Slam.Cms.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using MMCServices = Mastercard.MarketingCenter.Services;
using SlamData = Slam.Cms.Data;

namespace Mastercard.MarketingCenter.Tests.Services
{
    [TestClass]
    public class ContentServiceTests : RolledBackDataContextTests<MarketingCenterDbContext>
    {
        [TestMethod]
        public void Can_Get_Content_For_ContentType()
        {
            var service = GetService();
            var contentTypeDao = new ContentTypeDao(Context);
            var contentType = contentTypeDao.FirstOrDefault(o => o.Title == "Snippet");

            var result = service.GetContentFor(contentType, "us", 0, 10);

            Assert.IsTrue(result.Data.Any());
            Assert.AreEqual(result.Data.First().StatusId, SlamData.Status.Published);
        }

        [TestMethod]
        public void Can_Get_Content_For_Asset_ContentType()
        {
            var service = GetService();
            var contentTypeDao = new ContentTypeDao(Context);
            var contentType = contentTypeDao.FirstOrDefault(o => o.Title == "Asset");

            var result = service.GetContentFor(contentType, "us", 0, 10);

            Assert.IsTrue(result.Data.Any());
        }

        [TestMethod]
        public void Can_Get_Archived_Items_For_Snippet_ContentType()
        {
            var service = GetService();
            var contentTypeDao = new ContentTypeDao(Context);
            var contentType = contentTypeDao.FirstOrDefault(o => o.Title == "Snippet");

            var result = service.GetContentFor(contentType, "us", 0, 10, statusId: SlamData.Status.Archived);

            Assert.AreEqual(3, result.Data.Count());
        }

        [TestMethod]
        [Ignore]
        public void Count_Items_Should_Match()
        {
            var service = GetService();
            var contentTypeDao = new ContentTypeDao(Context);
            var contentType = contentTypeDao.FirstOrDefault(o => o.Title == "Snippet");

            var result = service.GetContentFor(contentType, "us", 0, 100);

            Assert.AreEqual(result.Data.Count(), result.Total);
        }

        [TestMethod]
        public void Can_Get_Tracking_Fields_For_ContentItems()
        {
            var service = GetService();
            var contentTypeDao = new ContentTypeDao(Context);
            var contentType = contentTypeDao.FirstOrDefault(o => o.Title == "Snippet");

            var result = service.GetContentFor(contentType, "us", 0, 100);

            var firstItem = result.Data.First();
            Assert.IsNotNull(firstItem.CreatedDate);
            Assert.IsNotNull(firstItem.ModifiedDate);
        }

        [TestMethod]
        public void Can_Delete_ListItem()
        {
            var service = GetService();

            service.DeleteListItem(1, "3b");

            var item = service.GetListItemData(1, "3b");

            Assert.IsNull(item);
        }

        [TestMethod]
        public void Archived_Items_Should_Not_Surface_By_Default()
        {
            var service = GetService();
            var contentTypeDao = new ContentTypeDao(Context);
            var contentType = contentTypeDao.FirstOrDefault(o => o.Title == "Snippet");

            var result = service.GetContentFor(contentType, "us", 0, 100);

            Assert.IsTrue(result.Data.All(o => o.StatusId != SlamData.Status.Archived));
        }

        [TestMethod]
        public void Can_Get_Paged_Content_For_ContentType()
        {
            var service = GetService();
            var contentTypeDao = new ContentTypeDao(Context);
            var contentType = contentTypeDao.FirstOrDefault(o => o.Title == "Snippet");

            var result = service.GetContentFor(contentType, "us", 0, 10);

            Assert.IsTrue(result.Data.Any());
            Assert.AreEqual(result.Data.Count(), 10);
            Assert.IsTrue(result.Total > 10);
        }

        [TestMethod]
        [Ignore]
        public void Can_Search_Content_For_ContentType()
        {
            var service = GetService();
            var contentTypeDao = new ContentTypeDao(Context);
            var contentType = contentTypeDao.GetById("e");

            var result = service.GetContentFor(contentType, "us", 0, 10, "box");

            Assert.IsTrue(result.Data.Any());

            var firstItem = ((IDictionary<string, object>)result.Data.First());
            Assert.IsTrue(Convert.ToString(firstItem["Title"]).ToLowerInvariant().Contains("box"));
        }

        [TestMethod]
        public void Can_Search_Content_For_ListItem()
        {
            var service = GetService();
            var listDao = new ListDao(Context);
            var list = listDao.FirstOrDefault(o => o.Name == "Icons");

            var result = service.GetContentFor(list, "us", 0, 10, "faq");

            Assert.IsTrue(result.Data.Any());

            var firstItem = ((IDictionary<string, object>)result.Data.First());
            Assert.IsTrue(Convert.ToString(firstItem["Name"]).ToLowerInvariant().Contains("faq"));
        }


        [TestMethod]
        [Ignore]
        public void Can_Get_ListItems_With_Where_Clause()
        {
            var service = GetService();
            var listDao = new ListDao(Context);
            var list = listDao.FirstOrDefault(o => o.TableName == "Issuer");

            var result = service.GetContentFor(list, "us", 0, 100);

            Assert.AreEqual(result.Data.Count(), 37);
            Assert.AreEqual(result.Total, 37);

            var firstItem = ((IDictionary<string, object>)result.Data.First());
            Assert.IsTrue(Convert.ToBoolean(firstItem["HasOrdered"]));
            Assert.IsTrue(string.IsNullOrEmpty(Convert.ToString(firstItem["BillingID"])));
        }

        [TestMethod]
        [Ignore]
        public void Can_Get_ListItems_With_Where_Clause_And_Search()
        {
            var service = GetService();
            var listDao = new ListDao(Context);
            var list = listDao.FirstOrDefault(o => o.TableName == "Issuer");

            var result = service.GetContentFor(list, "us", 0, 100, "0");

            Assert.AreEqual(result.Data.Count(), 4);
            Assert.AreEqual(result.Total, 4);
        }

        [TestMethod]
        public void Can_Get_Paged_Content_For_ContentType_2()
        {
            var service = GetService();
            var contentTypeDao = new ContentTypeDao(Context);
            var contentType = contentTypeDao.FirstOrDefault(o => o.Title == "Snippet");

            var result = service.GetContentFor(contentType, "us", 10, 10);

            Assert.IsTrue(result.Data.Any());
            Assert.AreEqual(result.Data.Count(), 10);
            Assert.IsTrue(result.Total > 20);
        }

        [TestMethod]
        public void Listing_Content_Items_Should_Not_Include_Deleted_Items()
        {
            var service = GetService();
            var contentTypeDao = new ContentTypeDao(Context);
            var contentType = contentTypeDao.FirstOrDefault(o => o.Title == "Snippet");

            var result = service.GetContentFor(contentType, "us", 0, 100);

            Assert.IsTrue(result.Data.All(o => o.StatusId != SlamData.Status.Deleted));
        }

        [TestMethod]
        [Ignore]
        public void Can_Get_Sorted_Content_For_ContentType()
        {
            var service = GetService();
            var contentTypeDao = new ContentTypeDao(Context);
            var contentType = contentTypeDao.FirstOrDefault(o => o.Title == "Snippet");

            var sortColumns = new Dictionary<string, bool> { { "Title", true }, { "ContentItemID", false } };
            var result = service.GetContentFor(contentType, "us", 0, 10, null, sortColumns);

            Assert.IsTrue(result.Data.Any());

            var firstItem = ((IDictionary<string, object>)result.Data.First());
            Assert.AreEqual(firstItem["ContentItemId"], "13");
        }

        [TestMethod]
        [Ignore]
        public void Can_Sort_By_CreatedDate_For_ContentType()
        {
            var service = GetService();
            var contentTypeDao = new ContentTypeDao(Context);
            var contentType = contentTypeDao.FirstOrDefault(o => o.Title == "Snippet");

            var sortColumns = new Dictionary<string, bool> { { "CreatedDate", false } };
            var result = service.GetContentFor(contentType, "us", 0, 10, null, sortColumns);

            Assert.IsTrue(result.Data.Any());

            var firstItem = ((IDictionary<string, object>)result.Data.First());
            Assert.AreEqual(Convert.ToDateTime(firstItem["CreatedDate"]).ToShortDateString(), new DateTime(2015, 8, 11).ToShortDateString());
        }

        [TestMethod]
        [Ignore]
        public void Can_Sort_By_CreatedDate_For_ListItem()
        {
            var service = GetService();
            var listDao = new ListDao(Context);
            var list = listDao.FirstOrDefault(o => o.TableName == "Issuer");

            var sortColumns = new Dictionary<string, bool> { { "DateCreated", false } };
            var result = service.GetContentFor(list, "us", 0, 10, null, sortColumns);

            Assert.IsTrue(result.Data.Any());

            var firstItem = ((IDictionary<string, object>)result.Data.First());
            Assert.AreEqual(firstItem["IssuerID"], "96");
        }

        [TestMethod]
        [Ignore]
        public void Can_Sort_By_ModifiedDate_For_ContentType()
        {
            var service = GetService();
            var contentTypeDao = new ContentTypeDao(Context);
            var contentType = contentTypeDao.FirstOrDefault(o => o.Title == "Snippet");

            var sortColumns = new Dictionary<string, bool> { { "ModifiedDate", false } };
            var result = service.GetContentFor(contentType, "us", 0, 10, null, sortColumns);

            Assert.IsTrue(result.Data.Any());

            var firstItem = ((IDictionary<string, object>)result.Data.First());
            Assert.AreEqual(Convert.ToDateTime(firstItem["ModifiedDate"]).ToShortDateString(), new DateTime(2015, 11, 24).ToShortDateString());
        }

        [TestMethod]
        public void Can_Get_FrontEndUrl_Content_ContentType()
        {
            var service = GetService();
            var contentTypeDao = new ContentTypeDao(Context);
            var contentType = contentTypeDao.FirstOrDefault(o => o.Title == "Snippet");

            var sortColumns = new Dictionary<string, bool> { { "Title", true }, { "ContentItemID", false } };
            var result = service.GetContentFor(contentType, "us", 0, 10, null, sortColumns);

            Assert.IsTrue(result.Data.Any());

            var firstItem = ((IDictionary<string, object>)result.Data.First());
            Assert.IsNotNull(firstItem["FrontEndUrl"]);
        }

        [TestMethod]
        public void Can_Sort_Content_By_Status_For_ContentType()
        {
            var service = GetService();
            var contentTypeDao = new ContentTypeDao(Context);
            var contentType = contentTypeDao.FirstOrDefault(o => o.Title == "Snippet");

            var sortColumns = new Dictionary<string, bool> { { "StatusName", true } };
            var result = service.GetContentFor(contentType, "us", 0, 10, null, sortColumns);

            Assert.IsTrue(result.Data.Any());

            var firstItem = ((IDictionary<string, object>)result.Data.First());
            Assert.AreEqual(firstItem["StatusName"], "Draft Only");
            Assert.AreEqual(firstItem["StatusId"], SlamData.Status.DraftOnly);
        }

        [TestMethod]
        public void Can_Get_Sorted_Content_For_ContentType2()
        {
            var service = GetService();
            var contentTypeDao = new ContentTypeDao(Context);
            var contentType = contentTypeDao.FirstOrDefault(o => o.Title == "Snippet");

            var sortColumns = new Dictionary<string, bool> { { "Title", false }, { "ContentItemID", false } };
            var result = service.GetContentFor(contentType, "us", 0, 10, null, sortColumns);

            Assert.IsTrue(result.Data.Any());

            var firstItem = ((IDictionary<string, object>)result.Data.First());
            Assert.AreEqual(firstItem["ContentItemId"], "42b3");
        }

        [TestMethod]
        public void Can_Get_Content_For_ListContent()
        {
            var service = GetService();
            var listDao = new ListDao(Context);
            var list = listDao.FirstOrDefault(o => o.Name == "Icons");

            var result = service.GetContentFor(list, "us", 0, 10, "product");

            Assert.IsTrue(result.Data.Any());

            var firstItem = ((IDictionary<string, object>)result.Data.First());
            Assert.IsTrue(Convert.ToString(firstItem["Name"]).ToLowerInvariant().Contains("product"));
        }

        [TestMethod]
        public void Can_Get_Paged_Content_For_ListContent()
        {
            var service = GetService();
            var listDao = new ListDao(Context);
            var list = listDao.FirstOrDefault(o => o.Name == "Icons");

            var result = service.GetContentFor(list, "us", 0, 10);

            Assert.AreEqual(result.Data.Count(), 10);
        }

        [TestMethod]
        [Ignore]
        public void Can_Get_Snippet_Content_Item()
        {
            var service = GetService();

            var snippetData = service.GetContentItemData("4595");

            Assert.AreEqual(snippetData["ContentItemId"], "4595");
            Assert.AreEqual(snippetData["Title"], "Video-player-test");
        }

        [TestMethod]
        public void Can_Get_List_Item_Data()
        {
            var service = GetService();

            var snippetData = service.GetListItemData(1, "165");

            Assert.AreEqual(snippetData["IconID"], "165");
            Assert.AreEqual(snippetData["Name"], "Product");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Getting_Content_When_No_IdField_Is_Defined_Should_Throw()
        {
            var service = GetService();
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());

            cmsDbConnection.Execute("DELETE ContentTypeFields WHERE ContentTypeId = 'e8' and FieldDefinitionId = 2");

            service.GetContentItemData("10");
        }

        [TestMethod]
        [Ignore]
        public void Can_Get_Content_Item_Data_When_Has_MultipleLookup_Fields()
        {
            var service = GetService();

            var snippetData = service.GetContentItemData("4595");

            Assert.AreEqual(snippetData["ContentItemId"], "4595");
            Assert.AreEqual(snippetData["Title"], "Video-player-test");
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Content_For_Multiple_Lookup()
        {
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            var lookupService = new LookupServices(cmsDbConnection);
            var service = GetService(lookupService);

            var asseetData = service.GetContentItemData("17d");
            asseetData["BrowseAllMaterials"] = "1e,42";

            service.UpdateContent(asseetData, ContentActions.SaveLive, 1, "us");

            var settings = new LookupSettings { TableIdField = "TagID", TableName = "Tag", TableValueField = "DisplayName", AssociationTableName = "AssetBrowseAllMaterialTag", AssociationKeysMap = new[] { "TagID", "ContentItemId" } };
            var values = lookupService.GetSelectedLookupValues("17d", settings);

            Assert.AreEqual(values.Count(), 2);
            Assert.IsTrue(values.Any(o => o == "1e"));
            Assert.IsTrue(values.Any(o => o == "42"));
        }

        [TestMethod]
        [Ignore]
        public void Creating_Program_Draft_Should_Update_Lookup_Relationship_With_OrderableAsset()
        {
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            var lookupService = new LookupServices(cmsDbConnection);
            var service = GetService(lookupService);
            const string expectedDraftId = "1c1-p";

            var settings = new LookupSettings
            {
                TableIdField = "ContentItemId",
                TableName = "Program",
                TableValueField = "Title",
                AssociationTableName = "OrderableAssetRelatedProgram",
                AssociationKeysMap = new[] { "ProgramContentItemId", "OrderableAssetContentItemId" },
                ForceDisableMultiSelection = true
            };

            var values = lookupService.GetSelectedLookupValues("8b4", settings);
            Assert.AreEqual(values.Count(), 1);

            var programData = service.GetContentItemData("1c1");
            programData["Title"] = "Test";

            service.UpdateContent(programData, ContentActions.SaveAsDraft, 1, "us");

            values = lookupService.GetSelectedLookupValues("8b4", settings);

            Assert.AreEqual(values.Count(), 2);
            Assert.IsTrue(values.Any(o => o == "1c1"));
            Assert.IsTrue(values.Any(o => o == expectedDraftId));
        }

        [TestMethod]
        [Ignore]
        public void Updating_A_Orderable_Asset_Item_Should_Update_Related_Program_With_Draft_Relationship()
        {
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            var lookupService = new LookupServices(cmsDbConnection);
            var service = GetService(lookupService);
            const string assetId = "19";
            const string liveProgramId = "42e9";
            const string draftProgramId = "42e9-p";

            var settings = new LookupSettings
            {
                TableIdField = "ContentItemId",
                TableName = "Program",
                TableValueField = "Title",
                AssociationTableName = "OrderableAssetRelatedProgram",
                AssociationKeysMap = new[] { "ProgramContentItemId", "OrderableAssetContentItemId" },
                ForceDisableMultiSelection = true
            };

            var assetData = service.GetContentItemData(assetId);
            assetData["Title"] = "Test";
            assetData["RelatedProgram"] = liveProgramId;

            service.UpdateContent(assetData, ContentActions.SaveAsDraft, 1, "us");

            var values = lookupService.GetSelectedLookupValues(assetId, settings);

            Assert.AreEqual(values.Count(), 2);
            Assert.IsTrue(values.Any(o => o == liveProgramId));
            Assert.IsTrue(values.Any(o => o == draftProgramId));
        }

        [TestMethod]
        [Ignore]
        public void Updating_A_Orderable_Asset_Item_Should_Update_Related_Program_With_Relationship()
        {
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            var lookupService = new LookupServices(cmsDbConnection);
            var service = GetService(lookupService);
            const string assetId = "211";
            const string liveProgramId = "23c";

            var settings = new LookupSettings
            {
                TableIdField = "ContentItemId",
                TableName = "Program",
                TableValueField = "Title",
                AssociationTableName = "OrderableAssetRelatedProgram",
                AssociationKeysMap = new[] { "ProgramContentItemId", "OrderableAssetContentItemId" },
                ForceDisableMultiSelection = true
            };

            var assetData = service.GetContentItemData(assetId);
            assetData["Title"] = "Test";
            assetData["RelatedProgram"] = liveProgramId;

            service.UpdateContent(assetData, ContentActions.SaveLive, 1, "us");

            var values = lookupService.GetSelectedLookupValues(assetId, settings);

            Assert.AreEqual(values.Count(), 1);
            Assert.IsTrue(values.Any(o => o == liveProgramId));
        }

        [TestMethod]
        [Ignore]
        public void Creating_Program_Draft_Should_Update_Lookup_Relationship_With_DownloadableAsset()
        {
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            var lookupService = new LookupServices(cmsDbConnection);
            var service = GetService(lookupService);
            const string draftItemId = "45d5-p";
            const string liveItemId = "45d5";

            var settings = new LookupSettings
            {
                TableIdField = "ContentItemId",
                TableName = "Program",
                TableValueField = "Title",
                AssociationTableName = "DownloadableAssetRelatedProgram",
                AssociationKeysMap = new[] { "ProgramContentItemId", "DownloadableAssetContentItemId" },
                ForceDisableMultiSelection = true
            };

            var values = lookupService.GetSelectedLookupValues("45f8", settings);
            Assert.AreEqual(values.Count(), 1);

            var programData = service.GetContentItemData(liveItemId);
            programData["Title"] = "Test";

            service.UpdateContent(programData, ContentActions.SaveAsDraft, 1, "us");

            values = lookupService.GetSelectedLookupValues("45f8", settings);

            Assert.AreEqual(values.Count(), 2);
            Assert.IsTrue(values.Any(o => o == liveItemId));
            Assert.IsTrue(values.Any(o => o == draftItemId));
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Content_For_Item_With_Tag_Field()
        {
            var service = GetService();

            var item = service.GetContentItemData("1");
            item["Title"] = "Test Update";

            service.UpdateContent(item, ContentActions.SaveLive, 1, "us");

            item = service.GetContentItemData("1");

            Assert.AreEqual(item["Title"], "Test Update");
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Page_Content_With_Url_Field()
        {
            var service = GetService();

            var item = service.GetContentItemData("15f");
            item["Uri"] = "page/test/url";

            service.UpdateContent(item, ContentActions.SaveLive, 1, "us");

            var contentItemDao = new ContentItemDao(Context);
            var contentItem = contentItemDao.GetById("15f");

            Assert.AreEqual(contentItem.FrontEndUrl, "page/test/url");
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Url_Field_Of_Page_Item_With_Draft()
        {
            var service = GetService();

            var item = service.GetContentItemData("155");
            item["Uri"] = "page/test/url";

            service.UpdateContent(item, ContentActions.SaveLive, 1, "us");

            var contentItemDao = new ContentItemDao(Context);
            var contentItem = contentItemDao.GetById("155");
            var contentItemDraft = contentItemDao.GetById("155-p");

            Assert.AreEqual(contentItem.FrontEndUrl, "page/test/url");
            Assert.AreEqual(contentItemDraft.FrontEndUrl, "page/test/url");
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Content_For_Program_Item_With_Attachment_Field()
        {
            var service = GetService();

            var item = service.GetContentItemData("1c1");
            item["ProgramAttachment"] = "TestAttachment1.txt,TestAttachment2.txt";

            service.UpdateContent(item, ContentActions.SaveLive, 1, "us");

            var attachments = AttachmentServices.GetAttachments("1c1");

            Assert.AreEqual(attachments.Count(), 2);
            Assert.IsTrue(attachments.Any(o => o.FileUrl == "TestAttachment1.txt"));
            Assert.IsTrue(attachments.Any(o => o.FileUrl == "TestAttachment2.txt"));
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Live_Content_And_Set_Tracking_Fields()
        {
            var service = GetService();

            var item = service.GetContentItemData("1c1");
            item["Title"] = "Test";

            service.UpdateContent(item, ContentActions.SaveLive, 1, "us");

            var contentItemDao = new ContentItemDao(Context);
            var updatedItem = contentItemDao.GetById("1c1");

            Assert.AreEqual(updatedItem.ModifiedByUserId, 1);
            Assert.AreEqual(updatedItem.ModifiedDate.ToShortDateString(), DateTime.Now.ToShortDateString());
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Draft_Content_And_Set_Tracking_Fields()
        {
            var service = GetService();

            var item = service.GetContentItemData("1c1");
            item["Title"] = "Test";

            service.UpdateContent(item, ContentActions.SaveAsDraft, 1, "us");

            var contentItemDao = new ContentItemDao(Context);
            var updatedItem = contentItemDao.GetById("1c1-p");

            Assert.AreEqual(updatedItem.ModifiedByUserId, 1);
            Assert.AreEqual(updatedItem.ModifiedDate.ToShortDateString(), DateTime.Now.ToShortDateString());
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Content_For_Item_With_Customization_Field()
        {
            var service = GetService();
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            var customizationService = new CustomizationServices(cmsDbConnection);

            var item = service.GetContentItemData("19");
            item["Customizable"] = new CustomizationField
            {
                Customizable = true,
                OptionalCustomizableOptions = new[] { "12d", "12f" },
                RequiredCustomizableOptions = new[] { "13c" }
            };

            service.UpdateContent(item, ContentActions.SaveLive, 1, "us");

            var itemCustomizations = customizationService.GetContentItemCustomizations("19");

            var optCustom1 = itemCustomizations.First(o => o.CustomizationOptionId == "12d");
            var optCustom2 = itemCustomizations.First(o => o.CustomizationOptionId == "12f");
            var reqCustom1 = itemCustomizations.First(o => o.CustomizationOptionId == "13c");

            Assert.AreEqual(optCustom1.State, CustomizationState.Optional);
            Assert.AreEqual(optCustom2.State, CustomizationState.Optional);
            Assert.AreEqual(reqCustom1.State, CustomizationState.Required);

            var restOfCustomizations = itemCustomizations.Except(new[] { optCustom1, optCustom2, reqCustom1 });

            Assert.IsTrue(restOfCustomizations.All(o => o.State == CustomizationState.Off));
        }

        [TestMethod]
        [Ignore]
        public void Can_Add_Item_With_Customization_Field()
        {
            var service = GetService();
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            var customizationService = new CustomizationServices(cmsDbConnection);

            const string contentTypeId = "d";
            var orderableAssetData = new Dictionary<string, object>
            {
                {"Title", "New Title"},
                {"ShortDescription", "Desc"},
                {"ThumbnailImage", "Test"},
                {"LongDescription", "Test"},
                {"MainImage", "Test"},
                {"Sku", "Test"},
                {"IconID", "165"},
                {"PricingScheduleID", "107"},
                {"AssetType", "Test"},
                {"PDFPreview", "Test"},
                {
                    "Customizable", new CustomizationField
                    {
                        Customizable = true,
                        OptionalCustomizableOptions = new[] {"12d", "12f"},
                        RequiredCustomizableOptions = new[] {"13c"}
                    }
                }
            };

            var contentItemId = service.AddContent(contentTypeId, orderableAssetData, 1, "us", ContentActions.SaveLive);

            var itemCustomizations = customizationService.GetContentItemCustomizations(contentItemId);

            var optCustom1 = itemCustomizations.First(o => o.CustomizationOptionId == "12d");
            var optCustom2 = itemCustomizations.First(o => o.CustomizationOptionId == "12f");
            var reqCustom1 = itemCustomizations.First(o => o.CustomizationOptionId == "13c");

            Assert.AreEqual(optCustom1.State, CustomizationState.Optional);
            Assert.AreEqual(optCustom2.State, CustomizationState.Optional);
            Assert.AreEqual(reqCustom1.State, CustomizationState.Required);

            var restOfCustomizations = itemCustomizations.Except(new[] { optCustom1, optCustom2, reqCustom1 });

            Assert.IsTrue(restOfCustomizations.All(o => o.State == CustomizationState.Off));
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Content_For_Item_With_FeatureOn_Field()
        {
            var service = GetService();
            const string contentItemId = "17b";

            var featureTagItems = new List<FeatureTagItem>
            {
                new FeatureTagItem {FeatureLevel = 1, Type = FeatureType.FeatureLocation, Id = "23"},
                new FeatureTagItem {FeatureLevel = 1, Id = "2d", Type = FeatureType.Tag},
                new FeatureTagItem {FeatureLevel = 0, Id = "42", Type = FeatureType.Tag}
            };

            var item = service.GetContentItemData(contentItemId);
            item["FeatureOn"] = featureTagItems;

            service.UpdateContent(item, ContentActions.SaveLive, 1, "us");

            Context.SaveChanges();

            var tagCatDao = new TagCategoryDao(Context);
            var featureLocationDao = new FeatureLocationDao(Context);
            var tagHierarchyPositionDao = new TagHierarchyPositionDao(Context);
            var tagDao = new TagDao(Context);
            var featureOnService = new FeatureOnServices(tagCatDao, featureLocationDao, tagHierarchyPositionDao, tagDao);

            var featureItems = featureOnService.GetContentItemFeatureItems(contentItemId);

            Assert.AreEqual(featureItems.Count(), 3);
            Assert.IsTrue(featureItems.Any(o => o.Id == "2d" && o.Type == FeatureType.Tag));
            Assert.IsTrue(featureItems.Any(o => o.Id == "42" && o.Type == FeatureType.Tag));
            Assert.IsTrue(featureItems.Any(o => o.Id == "23" && o.Type == FeatureType.FeatureLocation));
        }

        [TestMethod]
        [Ignore]
        public void Can_Save_Item_With_Draft_As_Live_With_FeatureOn_Field()
        {
            var service = GetService();
            const string draftItemId = "42b9-p";
            const string liveItemId = "42b9";

            var featureTagItems = new List<FeatureTagItem>
            {
                new FeatureTagItem {FeatureLevel = 1, Type = FeatureType.FeatureLocation, Id = "23"},
                new FeatureTagItem {FeatureLevel = 1, Id = "2d", Type = FeatureType.Tag},
                new FeatureTagItem {FeatureLevel = 0, Id = "42", Type = FeatureType.Tag}
            };

            var item = service.GetContentItemData(draftItemId);
            item["FeatureOn"] = featureTagItems;

            service.UpdateContent(item, ContentActions.SaveLive, 1, "us");

            Context.SaveChanges();

            var tagCatDao = new TagCategoryDao(Context);
            var featureLocationDao = new FeatureLocationDao(Context);
            var tagHierarchyPositionDao = new TagHierarchyPositionDao(Context);
            var tagDao = new TagDao(Context);
            var featureOnService = new FeatureOnServices(tagCatDao, featureLocationDao, tagHierarchyPositionDao, tagDao);

            var featureItems = featureOnService.GetContentItemFeatureItems(liveItemId);

            Assert.AreEqual(featureItems.Count(), 3);
            Assert.IsTrue(featureItems.Any(o => o.Id == "2d" && o.Type == FeatureType.Tag));
            Assert.IsTrue(featureItems.Any(o => o.Id == "42" && o.Type == FeatureType.Tag));
            Assert.IsTrue(featureItems.Any(o => o.Id == "23" && o.Type == FeatureType.FeatureLocation));
        }

        [TestMethod]
        [Ignore]
        public void Can_Add_Content_For_Item_With_FeatureOn_Field()
        {
            var service = GetService();

            var featureTagItems = new List<FeatureTagItem>
            {
                new FeatureTagItem {FeatureLevel = 1, Type = FeatureType.FeatureLocation, Id = "23"},
                new FeatureTagItem {FeatureLevel = 1, Id = "2d", Type = FeatureType.Tag},
                new FeatureTagItem {FeatureLevel = 0, Id = "42", Type = FeatureType.Tag}
            };

            var assetData = new Dictionary<string, object>
            {
                {"Title", "New Title"},
                {"ShortDescription", "Desc"},
                {"ThumbnailImage", "Test"},
                {"LongDescription", "Test"},
                {"MainImage", "Test"},
                {"IconID", "165"},
                {"AssetType", "Test"},
                {"FeatureOn", featureTagItems}
            };

            var newItemId = service.AddContent("b", assetData, 1, "us", ContentActions.SaveLive);

            Context.SaveChanges();

            var tagCatDao = new TagCategoryDao(Context);
            var featureLocationDao = new FeatureLocationDao(Context);
            var tagHierarchyPositionDao = new TagHierarchyPositionDao(Context);
            var tagDao = new TagDao(Context);
            var featureOnService = new FeatureOnServices(tagCatDao, featureLocationDao, tagHierarchyPositionDao, tagDao);

            var featureItems = featureOnService.GetContentItemFeatureItems(newItemId);

            Assert.AreEqual(featureItems.Count(), 3);
            Assert.IsTrue(featureItems.Any(o => o.Id == "2d" && o.Type == FeatureType.Tag));
            Assert.IsTrue(featureItems.Any(o => o.Id == "42" && o.Type == FeatureType.Tag));
            Assert.IsTrue(featureItems.Any(o => o.Id == "23" && o.Type == FeatureType.FeatureLocation));
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Tag_Field_Content()
        {
            var tagService = GetTagService();
            var service = GetService(null, tagService);

            var item = service.GetContentItemData("1");
            item["Marketing"] = "merchant-offers, sams-club-ibcu";

            service.UpdateContent(item, ContentActions.SaveLive, 1, "us");

            var values = tagService.GetSelectedTags("1");

            Assert.IsTrue(values.Any(o => o.Id == "merchant-offers"));
            Assert.IsTrue(values.Any(o => o.Id == "sams-club-ibcu"));
        }

        [TestMethod]
        [Ignore]
        public void Can_Create_Content_For_Multiple_Lookup()
        {
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            var lookupService = new LookupServices(cmsDbConnection);
            var service = GetService(lookupService);

            var contentData = new Dictionary<string, object>
            {
                {"Title", "Title"},
                {"ShortDescription", "Test"},
                {"LongDescription", "Test"},
                {"ThumbnailImage", "Test"},
                {"MainImage", "Test"},
                {"IconID", "165"},
                {"AssetType", "Asset"},
                {"BrowseAllMaterials", "1e,42"}
            };

            var newContentItemId = service.AddContent("b", contentData, 1, "us", ContentActions.SaveLive);

            var settings = new LookupSettings { TableIdField = "TagID", TableName = "Tag", TableValueField = "DisplayName", AssociationTableName = "AssetBrowseAllMaterialTag", AssociationKeysMap = new[] { "TagID", "ContentItemId" } };
            var values = lookupService.GetSelectedLookupValues(newContentItemId, settings);

            Assert.AreEqual(values.Count(), 2);
            Assert.IsTrue(values.Any(o => o == "1e"));
            Assert.IsTrue(values.Any(o => o == "42"));
        }

        [TestMethod]
        [Ignore]
        public void Can_Reset_Content_For_Complex_Lookup()
        {
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            var lookupService = new LookupServices(cmsDbConnection);
            var service = GetService(lookupService);

            var asseetData = new Dictionary<string, object> { { "ContentItemId", "17d" }, { "Title", "Title" } }; // We dont include the MultipleLookup field in the contentData

            service.UpdateContent(asseetData, ContentActions.SaveLive, 1, "us");

            var settings = new LookupSettings { TableIdField = "TagID", TableName = "Tag", TableValueField = "DisplayName", AssociationTableName = "AssetBrowseAllMaterialTag", AssociationKeysMap = new[] { "TagID", "ContentItemId" } };
            var values = lookupService.GetSelectedLookupValues("17d", settings);

            Assert.AreEqual(values.Count(), 0);
        }

        [TestMethod]
        public void Can_Validate_Item_Data()
        {
            var fieldDefinitions = new List<FieldDefinition>
            {
                new FieldDefinition {Required = true, FieldTypeCode = FieldType.SingleLineText, Name = "Title", FieldType = new FieldType { FieldTypeCode = FieldType.SingleLineText}},
                new FieldDefinition {Length = 10, Name = "Name", FieldTypeCode = FieldType.SingleLineText, FieldType = new FieldType { FieldTypeCode = FieldType.SingleLineText}},
                new FieldDefinition {FieldTypeCode = FieldType.DateTime, Name = "Date", FieldType = new FieldType { FieldTypeCode = FieldType.DateTime}}
            };

            var data = new Dictionary<string, object>
            {
                {"Name", "Exceed 10 chars length"},
                {"Title", string.Empty},
                {"Date", "31-31-2010 10:44"}
            };

            var service = GetService();

            var validationErrors = service.ValidateData(fieldDefinitions, data);

            Assert.AreEqual(validationErrors.Count(), 3);
            Assert.IsTrue(validationErrors.Any(o => o.Key == "Name"));
            Assert.IsTrue(validationErrors.Any(o => o.Key == "Date"));
            Assert.IsTrue(validationErrors.Any(o => o.Key == "Title"));
        }

        [TestMethod]
        public void Can_Validate_Item_With_DateTime_Range()
        {
            var fieldDefinitions = new List<FieldDefinition>
            {
                new FieldDefinition {Required = true, Description = "Start Date", FieldTypeCode = FieldType.DateTime, CustomSettings = "{ 'LowerThanFieldName' : 'EndDate' }", Name = "StartDate", FieldType = new FieldType { FieldTypeCode = FieldType.DateTime}},
                new FieldDefinition {Required = true, Description = "End Date", Name = "EndDate", FieldTypeCode = FieldType.DateTime, FieldType = new FieldType { FieldTypeCode = FieldType.DateTime}}
            };

            var data = new Dictionary<string, object>
            {
                {"StartDate", new DateTime(2016, 1, 10)},
                {"EndDate", new DateTime(2016, 1, 5)}
            };

            var service = GetService();

            var validationErrors = service.ValidateData(fieldDefinitions, data);

            Assert.AreEqual(validationErrors.Count(), 1);
            Assert.IsTrue(validationErrors.Any(o => o.Key == "StartDate"));
        }

        [TestMethod]
        public void Can_Validate_Item_Data_With_Int_Field()
        {
            var fieldDefinitions = new List<FieldDefinition>
            {
                new FieldDefinition {Required = true, FieldTypeCode = FieldType.Number, Name = "Number", FieldType = new FieldType { FieldTypeCode = FieldType.Number}}
            };

            var overflowNumber = Convert.ToInt64(int.MaxValue) + 20;

            var data = new Dictionary<string, object>
            {
                {"Number", overflowNumber}
            };

            var service = GetService();

            var validationErrors = service.ValidateData(fieldDefinitions, data);

            Assert.AreEqual(validationErrors.Count(), 1);
            Assert.IsTrue(validationErrors.Any(o => o.Key == "Number"));
        }

        [TestMethod]
        [Ignore]
        public void Can_Create_Content_With_Tag_Fiekd()
        {
            var service = GetService();
            var tagService = GetTagService();

            var contentData = new Dictionary<string, object>
            {
                {"Title", "Title"},
                {"ShortDescription", "Test"},
                {"LongDescription", "Test"},
                {"ThumbnailImage", "Test"},
                {"MainImage", "Test"},
                {"IconID", "165"},
                {"AssetType", "Asset"},
                {"Marketing", "merchant-offers, sams-club-ibcu"}
            };

            var newContentItemId = service.AddContent("e", contentData, 1, "us", ContentActions.SaveLive);

            var tags = tagService.GetSelectedTags(newContentItemId);
            Assert.AreEqual(tags.Count(), 2);
            Assert.IsTrue(tags.Any(o => o.Id == "merchant-offers"));
            Assert.IsTrue(tags.Any(o => o.Id == "sams-club-ibcu"));
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Snippet_Title_Content_Type()
        {
            var service = GetService();

            var snippetData = service.GetContentItemData("4595");

            snippetData["Title"] = "Updated Title";

            service.UpdateContent(snippetData, ContentActions.SaveLive, 1, "us");

            snippetData = service.GetContentItemData("4595");

            Assert.AreEqual(snippetData["Title"], "Updated Title");
        }

        [TestMethod]
        public void Can_Update_List_Item()
        {
            var service = GetService();

            var itemData = new Dictionary<string, object> { { "IconID", "165" } };

            itemData["Name"] = "Updated Name";

            service.UpdateListItem(1, itemData, 1, string.Empty);

            Assert.AreEqual(itemData["Name"], "Updated Name");
        }

        [TestMethod]
        [Ignore]
        public void Updating_List_Item_With_Tracking_Field()
        {
            var service = GetService();

            var itemData = new Dictionary<string, object> { { "TagID", "4580" } };

            itemData["DisplayName"] = "Test";

            service.UpdateListItem(2009, itemData, 1, string.Empty);

            var updatedItemData = service.GetListItemData(2009, "4580");

            Assert.AreEqual(Convert.ToDateTime(updatedItemData["ModifiedDate"]).ToShortDateString(), DateTime.Today.ToShortDateString());
            Assert.AreNotEqual(Convert.ToDateTime(updatedItemData["CreatedDate"]).ToShortDateString(), DateTime.Today.ToShortDateString());
        }

        [TestMethod]
        [Ignore]
        public void Creating_List_Item_With_Tracking_Field()
        {
            var service = GetService();

            var itemData = new Dictionary<string, object>();

            itemData["DisplayName"] = "Test";
            itemData["PageContent"] = "Test";
            itemData["Identifier"] = "Test";

            var itemId = service.AddItemContent(2009, itemData, 1);

            var createdItemData = service.GetListItemData(2009, itemId);

            Assert.AreEqual(Convert.ToDateTime(createdItemData["ModifiedDate"]).ToShortDateString(), DateTime.Today.ToShortDateString());
            Assert.AreEqual(Convert.ToDateTime(createdItemData["CreatedDate"]).ToShortDateString(), DateTime.Today.ToShortDateString());
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Checkbox_FieldType_For_Content()
        {
            var service = GetService();

            var snippetData = service.GetContentItemData("10");

            snippetData["RestrictToSegments"] = "IBCU";

            service.UpdateContent(snippetData, ContentActions.SaveLive, 1, "us");

            snippetData = service.GetContentItemData("10");

            Assert.AreEqual(snippetData["RestrictToSegments"], ";#IBCU;#");
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Not_Live_Content()
        {
            var service = GetService();

            var snippetData = service.GetContentItemData("15");

            snippetData["Title"] = "Test";

            service.UpdateContent(snippetData, ContentActions.SaveAsDraft, 1, "us");


            var contentItemDao = new ContentItemDao(Context);

            var item = contentItemDao.GetById("15");
            var draftItem = contentItemDao.GetById("15-p");

            snippetData = service.GetContentItemData("15");

            Assert.IsNotNull(item);
            Assert.IsNull(draftItem);
            Assert.AreEqual(snippetData["Title"], "Test");
            Assert.AreEqual(item.StatusID, SlamData.Status.DraftOnly);
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Live_Content_With_Draft_To_Draft_Status()
        {
            var service = GetService();

            var snippetData = service.GetContentItemData("42a3");

            snippetData["Title"] = "Test";

            service.UpdateContent(snippetData, ContentActions.SaveAsDraft, 1, "us");

            var contentItemDao = new ContentItemDao(Context);

            var item = contentItemDao.GetById("42a3");
            var draftItem = contentItemDao.GetById("42a3-p");

            snippetData = service.GetContentItemData("42a3-p");

            Assert.IsNotNull(item);
            Assert.IsNotNull(draftItem);
            Assert.AreEqual(snippetData["Title"], "Test");
            Assert.AreEqual(draftItem.StatusID, SlamData.Status.PublishedAndDraft);
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Live_Content_Without_Draft_To_Draft_Status()
        {
            var service = GetService();

            var snippetData = service.GetContentItemData("10");

            snippetData["Title"] = "Test";

            service.UpdateContent(snippetData, ContentActions.SaveAsDraft, 1, "us");

            var contentItemDao = new ContentItemDao(Context);

            var item = contentItemDao.GetById("10");
            var draftItem = contentItemDao.GetById("10-p");

            snippetData = service.GetContentItemData("10-p");

            const string expectedFrontEndUrl = "/snippet/10/Test";

            Assert.AreEqual(draftItem.FrontEndUrl, expectedFrontEndUrl);

            Assert.IsNotNull(item);
            Assert.IsNotNull(draftItem);
            Assert.AreEqual(snippetData["Title"], "Test");
            Assert.AreEqual(draftItem.StatusID, SlamData.Status.PublishedAndDraft);
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Draft_FrontEndUrl_For_Asset()
        {
            var service = GetService();

            var snippetData = service.GetContentItemData("10");

            snippetData["Title"] = "Test Title with spaces";

            service.UpdateContent(snippetData, ContentActions.SaveAsDraft, 1, "us");

            var contentItemDao = new ContentItemDao(Context);

            var draftItem = contentItemDao.GetById("10-p");

            const string expectedFrontEndUrl = "/snippet/10/test-title-with-spaces";

            Assert.AreEqual(draftItem.FrontEndUrl, expectedFrontEndUrl);
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Live_FrontEndUrl_For_Asset()
        {
            var service = GetService();

            var snippetData = service.GetContentItemData("10");

            snippetData["Title"] = "Test Title with spaces";

            service.UpdateContent(snippetData, ContentActions.SaveLive, 1, "us");

            var contentItemDao = new ContentItemDao(Context);

            var item = contentItemDao.GetById("10");

            const string expectedFrontEndUrl = "/snippet/10/test-title-with-spaces";

            Assert.AreEqual(item.FrontEndUrl, expectedFrontEndUrl);
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Live_Content_Without_Draft_To_Save_Preview_Status()
        {
            var service = GetService();

            var contentItemDao = new ContentItemDao(Context);
            var item = contentItemDao.GetById("10");
            var snippetData = service.GetContentItemData("10");

            snippetData["Title"] = "Test";

            service.UpdateContent(snippetData, ContentActions.SaveAndPreview, 1, "us");
            var draftItem = contentItemDao.GetById("10-p");

            snippetData = service.GetContentItemData("10");

            Assert.IsNotNull(item);
            Assert.IsNull(draftItem);
            Assert.AreEqual(snippetData["Title"], "Test");
            Assert.AreEqual(item.StatusID, SlamData.Status.Published);
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Content_With_Draft_To_Live_Status()
        {
            var service = GetService();
            var contentItemDao = new ContentItemDao(Context);
            var snippetData = service.GetContentItemData("42a3-p");

            snippetData["Title"] = "Test";

            service.UpdateContent(snippetData, ContentActions.SaveLive, 1, "us");

            var item = contentItemDao.GetById("42a3");
            var draftItem = contentItemDao.GetById("42a3-p");

            snippetData = service.GetContentItemData("42a3");

            Assert.IsNotNull(item);
            Assert.IsNotNull(draftItem);
            Assert.AreEqual(draftItem.StatusID, SlamData.Status.PublishedAndDraft);
            Assert.AreEqual(snippetData["Title"], "Test");
            Assert.AreEqual(item.StatusID, SlamData.Status.Published);
        }

        [TestMethod]
        public void Can_Delete_Draft_When_ContentItem_Is_Published_And_Draft()
        {
            var service = GetService();
            var contentItemService = GetContentItemService();
            var contentItemDao = new ContentItemDao(Context);
            const string itemWithDraft = "42a3-p";

            var ci = contentItemService.GetContentItem(itemWithDraft);
            contentItemService.DeleteOnlyDraft(ci, 1);

            var item = contentItemDao.GetById(itemWithDraft.GetAsLiveContentItemId());
            var draftItem = contentItemDao.GetById(itemWithDraft.GetAsDraftContentItemId());

            Assert.IsNotNull(item);
            Assert.IsNotNull(draftItem);
            Assert.AreEqual(draftItem.StatusID, SlamData.Status.Deleted);
            Assert.AreEqual(item.StatusID, SlamData.Status.Published);
        }

        [TestMethod]
        [Ignore]
        public void Saving_As_Live_An_Item_With_Deleted_Draft_Should_Set_Item_As_Live_Only()
        {
            var service = GetService();
            var contentItemService = GetContentItemService();
            var contentItemDao = new ContentItemDao(Context);
            const string itemDraft = "42a3-p";
            const string itemLive = "42a3";

            var ci = contentItemService.GetContentItem(itemDraft);
            contentItemService.DeleteOnlyDraft(ci, 1);

            var data = service.GetContentItemData(itemLive);

            data["Title"] = "Test";

            service.UpdateContent(data, ContentActions.SaveLive, 1, "us");

            var item = contentItemDao.GetById(itemLive);
            var draftItem = contentItemDao.GetById(itemDraft);

            Assert.AreEqual(item.StatusID, SlamData.Status.Published);
            Assert.AreEqual(draftItem.StatusID, SlamData.Status.Deleted);
        }

        [TestMethod]
        [Ignore]
        public void Can_Overwrite_Deleted_Draft_Item_When_Saving_Item_As_Draft()
        {
            var service = GetService();
            var contentItemService = GetContentItemService();
            var contentItemDao = new ContentItemDao(Context);
            const string itemDraft = "42a3-p";
            const string itemLive = "42a3";

            var ci = contentItemService.GetContentItem(itemDraft);
            contentItemService.DeleteOnlyDraft(ci, 1);

            var data = service.GetContentItemData(itemLive);

            data["Title"] = "Test";

            service.UpdateContent(data, ContentActions.SaveAsDraft, 1, "us");

            var item = contentItemDao.GetById(itemLive);
            var draftItem = contentItemDao.GetById(itemDraft);

            Assert.AreEqual(item.StatusID, SlamData.Status.Published);
            Assert.AreEqual(draftItem.StatusID, SlamData.Status.PublishedAndDraft);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void Cannot_Delete_Draft_When_ContentItem_Status_Is_Not_Published_And_Draft()
        {
            var contentItemService = GetContentItemService();
            const string itemWithDraft = "42e7";

            var ci = contentItemService.GetContentItem(itemWithDraft);
            contentItemService.DeleteOnlyDraft(ci, 1);
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Live_Content_Without_Draft_To_Live_Status()
        {
            var service = GetService();

            var snippetData = service.GetContentItemData("10");

            snippetData["Title"] = "Test";

            service.UpdateContent(snippetData, ContentActions.SaveLive, 1, "us");

            var contentItemDao = new ContentItemDao(Context);

            var item = contentItemDao.GetById("10");
            var draftItem = contentItemDao.GetById("10-p");

            snippetData = service.GetContentItemData("10");

            Assert.IsNotNull(item);
            Assert.IsNull(draftItem);
            Assert.AreEqual(snippetData["Title"], "Test");
            Assert.AreEqual(item.StatusID, SlamData.Status.Published);
        }

        [TestMethod]
        [Ignore]
        public void Can_Unselect_Checkbox_FieldType_For_Content()
        {
            var service = GetService();

            var snippetData = service.GetContentItemData("10");

            snippetData["RestrictToSegments"] = string.Empty; // unselect checkbox

            service.UpdateContent(snippetData, ContentActions.SaveLive, 1, "us");

            snippetData = service.GetContentItemData("10");

            Assert.AreEqual(snippetData["RestrictToSegments"], string.Empty);
        }

        [TestMethod]
        [Ignore]
        public void Can_Select_More_Than_One_Checkbox_For_Content()
        {
            var service = GetService();

            var snippetData = service.GetContentItemData("10");

            snippetData["RestrictToSegments"] = "IBCU,Test Option"; // unselect checkbox

            service.UpdateContent(snippetData, ContentActions.SaveLive, 1, "us");

            snippetData = service.GetContentItemData("10");

            Assert.AreEqual(snippetData["RestrictToSegments"], ";#IBCU;#Test Option;#");
        }

        [TestMethod]
        [Ignore]
        public void Can_Update_Content_When_Data_Contains_Less_Data_Than_ContentType_Field_Definitions()
        {
            var service = GetService();

            var snippetData = new Dictionary<string, object> { { "ContentItemId", "10" }, { "Title", "Updated Title" } };

            snippetData["Title"] = "Updated Title";

            service.UpdateContent(snippetData, ContentActions.SaveLive, 1, "us");

            var data = service.GetContentItemData("10");

            Assert.AreEqual(data["Title"], "Updated Title");
            Assert.AreEqual(data["RestrictToSegments"], string.Empty);
        }

        [TestMethod]
        [Ignore]
        public void Can_Save_New_Content()
        {
            var service = GetService();
            const string contentTypeId = "e8";
            var snippetData = new Dictionary<string, object>
            {
                {"Title", "New Title"},
                {"RestrictToSegments", "IBCU,Test Option"}
            };

            var contentItemId = service.AddContent(contentTypeId, snippetData, 1, "us", ContentActions.SaveLive);

            var data = service.GetContentItemData(contentItemId);

            Assert.AreEqual(data["Title"], "New Title");
            Assert.AreEqual(data["RestrictToSegments"], ";#IBCU;#Test Option;#");
        }

        [TestMethod]
        [Ignore]
        public void Can_Save_New_Content_With_Vanity_Url_Field()
        {
            var service = GetService();
            const string contentTypeId = "dl";
            var itemData = new Dictionary<string, object>
            {
                {"Url", "/test/{id}/test-html"},
                {"Title", "Test"},
                {"Filename", "Test"}
            };

            var contentItemId = service.AddContent(contentTypeId, itemData, 1, "us", ContentActions.SaveLive);

            var data = service.GetContentItemData(contentItemId);

            Assert.AreEqual(data["Url"], "/test/" + contentItemId + "/test-html");
        }

        [TestMethod]
        [Ignore]
        public void Can_Save_New_Content_With_Empty_Vanity_Url_Field()
        {
            var service = GetService();
            const string contentTypeId = "dl";
            var itemData = new Dictionary<string, object>
            {
                {"Title", "Test"},
                {"Filename", "Test"}
            };

            var contentItemId = service.AddContent(contentTypeId, itemData, 1, "us", ContentActions.SaveLive);

            var data = service.GetContentItemData(contentItemId);

            Assert.AreEqual(data["Url"], "/file/htmldownload/" + contentItemId + "/test");
        }

        [TestMethod]
        [Ignore]
        public void Can_Save_New_Content_With_Empty_Vanity_Url_Field_And_Empty_Title()
        {
            var service = GetService();
            const string contentTypeId = "dl";
            var itemData = new Dictionary<string, object>
            {
                {"Title", ""},
                {"Filename", "TestFilename"}
            };

            var contentItemId = service.AddContent(contentTypeId, itemData, 1, "us", ContentActions.SaveLive);

            var data = service.GetContentItemData(contentItemId);

            Assert.AreEqual(data["Url"], "/file/htmldownload/" + contentItemId + "/testfilename");
        }

        [TestMethod]
        [Ignore]
        public void Can_Save_New_ListItem()
        {
            var service = GetService();
            const int iconListId = 1;
            var dataItem = new Dictionary<string, object>
            {
                {"Name", "Name"},
                {"Extension", "Extension"},
                {"Icon", "Icon"}
            };

            var itemId = service.AddItemContent(iconListId, dataItem, 1);

            var data = service.GetListItemData(iconListId, itemId);

            Assert.AreEqual(data["Name"], "Name");
            Assert.AreEqual(data["Extension"], "Extension");
            Assert.AreEqual(data["Icon"], "Icon");
        }

        [TestMethod]
        [Ignore]
        public void Can_Save_New_Content2()
        {
            var service = GetService();
            const string contentTypeId = "e8";
            var snippetData = new Dictionary<string, object>
            {
                {"Title", "New Title"},
                {"RestrictToSegments", "IBCU,Test Option"}
            };

            var contentItemId = service.AddContent(contentTypeId, snippetData, 1, "us", ContentActions.SaveLive);

            var contentItemDao = new ContentItemDao(Context);
            var contentItem = contentItemDao.GetById(contentItemId);

            var expectedFrontEndUrl = "/snippet/" + contentItemId + "/new-title";
            Assert.AreEqual(contentItem.FrontEndUrl, expectedFrontEndUrl);
            Assert.AreEqual(contentItem.ContentItemId, contentItemId);
            Assert.AreEqual(contentItem.StatusID, SlamData.Status.Published);
            Assert.AreEqual(contentItem.CreatedByUserId, 1);
            Assert.AreEqual(contentItem.ModifiedByUserId, 1);
        }

        [TestMethod]
        [Ignore]
        public void Can_Save_Page_Content_With_Custom_Url()
        {
            var service = GetService();
            const string contentTypeId = "1";
            var data = new Dictionary<string, object>
            {
                {"Title", "New Title"},
                {"Uri", "page/test-uri"}
            };

            var contentItemId = service.AddContent(contentTypeId, data, 1, "us", ContentActions.SaveLive);

            var contentItemDao = new ContentItemDao(Context);
            var contentItem = contentItemDao.GetById(contentItemId);

            const string expectedFrontEndUrl = "page/test-uri";
            Assert.AreEqual(contentItem.FrontEndUrl, expectedFrontEndUrl);
        }

        [TestMethod]
        [Ignore]
        public void Can_Save_New_Content_As_Draft()
        {
            var service = GetService();
            const string contentTypeId = "e8";
            var snippetData = new Dictionary<string, object>
            {
                {"Title", "New Title"},
                {"RestrictToSegments", "IBCU,Test Option"}
            };

            var contentItemId = service.AddContent(contentTypeId, snippetData, 1, "us", ContentActions.SaveAsDraft);

            var contentItemDao = new ContentItemDao(Context);

            var contentItem = contentItemDao.GetById(contentItemId);
            var expectedFrontEndUrl = "/snippet/" + contentItemId + "/new-title";

            Assert.AreEqual(contentItem.FrontEndUrl, expectedFrontEndUrl);
            Assert.AreEqual(contentItem.StatusID, SlamData.Status.DraftOnly);
        }

        [TestMethod]
        [Ignore]
        public void Can_Save_New_Content_As_PreviewDraft()
        {
            var service = GetService();
            const string contentTypeId = "e8";
            var snippetData = new Dictionary<string, object>
            {
                {"Title", "New Title"},
                {"RestrictToSegments", "IBCU,Test Option"}
            };

            var contentItemId = service.AddContent(contentTypeId, snippetData, 1, "us", ContentActions.SaveAndPreview);

            var contentItemDao = new ContentItemDao(Context);

            var contentItem = contentItemDao.GetById(contentItemId);
            var expectedFrontEndUrl = "/snippet/" + contentItemId + "/new-title";

            Assert.AreEqual(contentItem.FrontEndUrl, expectedFrontEndUrl);
            Assert.AreEqual(contentItem.StatusID, SlamData.Status.DraftOnly);
        }

        [TestMethod]
        [Ignore]
        public void Can_Save_New_Content_Draft_Then_As_Preview()
        {
            var service = GetService();
            const string contentTypeId = "e8";
            var snippetData = new Dictionary<string, object>
            {
                {"Title", "New Title"},
                {"RestrictToSegments", "IBCU,Test Option"}
            };

            var contentItemId = service.AddContent(contentTypeId, snippetData, 1, "us", ContentActions.SaveAsDraft);

            snippetData["Title"] = "Updated";

            service.UpdateContent(snippetData, ContentActions.SaveAndPreview, 1, "us");

            var contentItemDao = new ContentItemDao(Context);

            var contentItem = contentItemDao.GetById(contentItemId);

            Assert.AreEqual(contentItem.StatusID, SlamData.Status.DraftOnly);
        }

        [TestMethod]
        [Ignore]
        public void Can_Save_New_Content_As_Live()
        {
            var service = GetService();
            const string contentTypeId = "e8";
            var snippetData = new Dictionary<string, object>
            {
                {"Title", "New Title"},
                {"RestrictToSegments", "IBCU,Test Option"}
            };

            var contentItemId = service.AddContent(contentTypeId, snippetData, 1, "us", ContentActions.SaveLive);

            var contentItemDao = new ContentItemDao(Context);

            var contentItem = contentItemDao.GetById(contentItemId);

            var expectedFrontEndUrl = "/snippet/" + contentItemId + "/new-title";
            Assert.AreEqual(contentItem.FrontEndUrl, expectedFrontEndUrl);
            Assert.AreEqual(contentItem.StatusID, SlamData.Status.Published);
        }

        [TestMethod]
        [Ignore]
        public void Can_Save_New_Content_With_ContentItemId_As_Live()
        {
            var service = GetService();
            const string contentTypeId = "e8";
            var itemId = "c356";
            var snippetData = new Dictionary<string, object>
            {
                {"Title", "New Title"},
                {"RestrictToSegments", "IBCU,Test Option"}
            };

            snippetData.SetContentItemId(itemId);

            var contentItemId = service.AddContent(contentTypeId, snippetData, 1, "us", ContentActions.SaveLive);

            var contentItemDao = new ContentItemDao(Context);

            var contentItem = contentItemDao.GetById(contentItemId);

            var expectedFrontEndUrl = "/snippet/" + contentItemId + "/new-title";
            Assert.AreEqual(contentItemId, itemId);
            Assert.AreEqual(contentItem.FrontEndUrl, expectedFrontEndUrl);
            Assert.AreEqual(contentItem.StatusID, SlamData.Status.Published);
        }


        [TestMethod]
        [Ignore]
        public void Can_Save_New_Content_With_Attachments()
        {
            var service = GetService();
            const string contentTypeId = "e";

            var programData = new Dictionary<string, object>
            {
                {"Title", "New Title"},
                {"RestrictToSegments", "IBCU,Test Option"},
                {"ProgramAttachment", "Test.txt"},
                {"ThumbnailImage", "Test"},
                {"LongDescription", "Test"},
                {"MainImage", "Test"},
                {"ShortDescription", "Test"}
            };

            var contentItemId = service.AddContent(contentTypeId, programData, 1, "us", ContentActions.SaveLive);

            var attachments = AttachmentServices.GetAttachments(contentItemId);

            Assert.AreEqual(attachments.Count(), 1);
            Assert.IsTrue(attachments.Any(o => o.FileUrl == "Test.txt"));
        }


        [TestMethod]
        [Ignore]
        public void Saving_Content_Items_Should_Increase_Unique_Id()
        {
            var service = GetService();
            var idService = GetIdGeneratorService();

            var uniqueId = idService.GetCurrentId();

            const string contentTypeId = "e8";
            var snippetData = new Dictionary<string, object>
            {
                {"Title", "New Title"},
                {"RestrictToSegments", "IBCU,Test Option"}
            };

            service.AddContent(contentTypeId, snippetData, 1, "us", ContentActions.SaveLive);

            var newId = (int.Parse(uniqueId, NumberStyles.HexNumber) + 1).ToString("X").ToLowerInvariant();

            Assert.AreEqual(newId, idService.GetCurrentId());
        }

        [TestMethod]
        [Ignore]
        public void Saving_List_Item_Should_Increase_Unique_Id()
        {
            var service = GetService();
            var idService = GetIdGeneratorService();

            const int iconListId = 1;
            var dataItem = new Dictionary<string, object>
            {
                {"Name", "Name"},
                {"Extension", "Extension"},
                {"Icon", "Icon"}
            };

            var uniqueId = idService.GetCurrentId();

            service.AddItemContent(iconListId, dataItem, 1);

            var newId = (int.Parse(uniqueId, NumberStyles.HexNumber) + 1).ToString("X").ToLowerInvariant();

            Assert.AreEqual(newId, idService.GetCurrentId());
        }

        [TestMethod]
        [Ignore]
        public void Saving_List_Item_Should_Return_Unique_Id()
        {
            var service = GetService();
            var idService = GetIdGeneratorService();
            const int iconListId = 1;
            var dataItem = new Dictionary<string, object>
            {
                {"Name", "Name"},
                {"Extension", "Extension"},
                {"Icon", "Icon"}
            };

            var currentId = idService.GetCurrentId();

            var newItemId = service.AddItemContent(iconListId, dataItem, 1);

            var newId = (int.Parse(currentId, NumberStyles.HexNumber) + 1).ToString("X").ToLowerInvariant();

            Assert.AreEqual(newId, newItemId);
        }

        [TestMethod]
        [Ignore]
        public void Saving_Content_Items_Should_Return_Unique_Id()
        {
            var service = GetService();
            var idService = GetIdGeneratorService();

            const string contentTypeId = "e8";
            var snippetData = new Dictionary<string, object>
            {
                {"Title", "New Title"},
                {"RestrictToSegments", "IBCU,Test Option"}
            };

            var currentId = idService.GetCurrentId();

            var newItemId = service.AddContent(contentTypeId, snippetData, 1, "us", ContentActions.SaveLive);

            var newId = (int.Parse(currentId, NumberStyles.HexNumber) + 1).ToString("X").ToLowerInvariant();

            Assert.AreEqual(newId, newItemId);
        }

        private static IDbConnection GetDbConnection()
        {
            return new SqlConnection(ConfigurationManager.ConnectionStrings["MasterCardMarketingCenter"].ConnectionString);
        }

        private static IdGeneratorService GetIdGeneratorService()
        {
            return new IdGeneratorService(GetDbConnection());
        }

        private ContentService GetService(LookupServices lookupServices = null, TagServices tagServices = null)
        {
            var marketingCenterDbContext = new MarketingCenterDbContext();
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            var contentItemDao = new ContentItemDao(marketingCenterDbContext);
            var contentTypeDao = new ContentTypeDao(marketingCenterDbContext);
            var contentTypeService = new ContentTypeService(contentItemDao, contentTypeDao);
            var fieldDefinitionDao = new FieldDefinitionDao(Context);
            var tagService = tagServices != null ? tagServices : GetTagService();
            var contentItemVersionDao = new ContentItemVersionDao(Context);
            var listDao = new ListDao(Context);
            var idGeneratorService = new IdGeneratorService(Context.Database.Connection);
            var customizationServices = new CustomizationServices(cmsDbConnection);
            var tagTranslatedContentDao = new TagTranslatedContentDao(Context);
            var segmentationTranslatedContentDao = new SegmentationTranslatedContentDao(Context);
            var translationServices = new TranslationServices(cmsDbConnection, tagTranslatedContentDao, segmentationTranslatedContentDao, fieldDefinitionDao);
            var userContext = new UserContext("us", "en");
            var fieldDataNormalizationContext = new FieldDataNormalizationContext(new Lazy<IEnumerable<MMCServices.Interfaces.IFieldDataRetrieval>>());

            return new ContentService(cmsDbConnection, contentTypeService, fieldDefinitionDao, lookupServices, tagService, contentItemVersionDao,
                listDao, idGeneratorService, customizationServices, GetFeatureOnServices(), AttachmentServices, SecondaryImageServices, TriggerMarketingServices,
                translationServices, userContext, ContentItemService, fieldDataNormalizationContext, CommonService, UrlService, EventLocationService);
        }

        private TagServices GetTagService()
        {
            var tagDao = new TagDao(Context);
            var tagHierarchyPosition = new TagHierarchyPositionDao(Context);
            var tagCategoryDao = new TagCategoryDao(Context);
            var cmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            var commonService = new CommonService(cmsDbConnection);

            return new TagServices(cmsDbConnection, new ContentTypeDao(Context), tagCategoryDao, tagDao, tagHierarchyPosition, commonService);
        }

        private FeatureOnServices GetFeatureOnServices()
        {
            var tagCatDao = new TagCategoryDao(Context);
            var featureLocationDao = new FeatureLocationDao(Context);
            var tagHierarchyPositionDao = new TagHierarchyPositionDao(Context);
            var tagDao = new TagDao(Context);

            return new FeatureOnServices(tagCatDao, featureLocationDao, tagHierarchyPositionDao, tagDao);
        }

        private ContentItemService GetContentItemService()
        {
            var marketingCenterDbContext = new MarketingCenterDbContext();
            var regionRepository = new RegionRepository(marketingCenterDbContext);
            var contentItemRepository = new ContentItemRepository(marketingCenterDbContext, regionRepository);
            var previewMode = new CookieBasedPreviewMode(null);
            var slamContext = new SlamData.SlamContext(previewMode);
            var migratedContentRepository = new MigratedContentRepository(marketingCenterDbContext);
            var offerRepository = new OfferRepository(Context);
            return new ContentItemService(contentItemRepository, slamContext, null, migratedContentRepository, SettingsService, offerRepository);
        }
    }
}