﻿using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Tests.Services
{
    [TestClass]
    public class FeatureOnServicesTests : RolledBackDataContextTests<MarketingCenterDbContext>
    {
        [TestMethod]
        [Ignore]
        public void Can_Add_New_Feature_Items_To_ContentItem_Without_Any_Tags_Or_Features()
        {
            var tagCatDao = new TagCategoryDao(Context);
            var featureLocationDao = new FeatureLocationDao(Context);
            var tagHierarchyPositionDao = new TagHierarchyPositionDao(Context);
            var tagDao = new TagDao(Context);
            var service = new FeatureOnServices(tagCatDao, featureLocationDao, tagHierarchyPositionDao, tagDao);
            const string contentItemId = "17c";

            var featureTagItems = new List<FeatureTagItem>
            {
                new FeatureTagItem {FeatureLevel = 1, Type = FeatureType.FeatureLocation, Id = "23"},
                new FeatureTagItem {FeatureLevel = 1, Id = "2d", Type = FeatureType.Tag},
                new FeatureTagItem {FeatureLevel = 0, Id = "42", Type = FeatureType.Tag}
            };

            service.SaveContentItemFeatureOn(contentItemId, featureTagItems);

            Context.SaveChanges();

            Assert.IsTrue(
                tagDao.GetAll()
                    .Any(o => o.TagID == "2d" && o.ContentItemFeatureOnTags.Any(cift => cift.ContentItemId == contentItemId)));

            Assert.IsTrue(
                tagDao.GetAll()
                    .Any(o => o.TagID == "42" && o.ContentItemFeatureOnTags.Any(cift => cift.ContentItemId == contentItemId)));

            Assert.IsTrue(
                featureLocationDao.GetAll()
                    .Any(
                        o =>
                            o.FeatureLocationId == "23" &&
                            o.ContentItemFeatureLocations.Any(cifl => cifl.ContentItemId == contentItemId)));


        }

        [TestMethod]
        [Ignore]
        public void Can_Add_New_Feature_Items_To_ContentItem_With_Tags_And_Features()
        {
            var tagCatDao = new TagCategoryDao(Context);
            var featureLocationDao = new FeatureLocationDao(Context);
            var tagHierarchyPositionDao = new TagHierarchyPositionDao(Context);
            var tagDao = new TagDao(Context);
            var service = new FeatureOnServices(tagCatDao, featureLocationDao, tagHierarchyPositionDao, tagDao);
            const string contentItemId = "17b";

            var featureTagItems = new List<FeatureTagItem>
            {
                new FeatureTagItem {FeatureLevel = 1, Type = FeatureType.FeatureLocation, Id = "23"},
                new FeatureTagItem {FeatureLevel = 1, Id = "2d", Type = FeatureType.Tag},
                new FeatureTagItem {FeatureLevel = 0, Id = "42", Type = FeatureType.Tag}
            };

            service.SaveContentItemFeatureOn(contentItemId, featureTagItems);

            Context.SaveChanges();

            Assert.IsFalse(featureLocationDao.GetAll()
                    .Any(
                        o =>
                            o.FeatureLocationId == "1a" &&
                            o.ContentItemFeatureLocations.Any(cifl => cifl.ContentItemId == contentItemId)));

            Assert.IsFalse(
                tagDao.GetAll()
                    .Any(o => o.TagID == "4f" && o.ContentItemFeatureOnTags.Any(cift => cift.ContentItemId == contentItemId)));

            Assert.IsFalse(
                tagDao.GetAll()
                    .Any(o => o.TagID == "8e" && o.ContentItemFeatureOnTags.Any(cift => cift.ContentItemId == contentItemId)));

            Assert.IsTrue(
                tagDao.GetAll()
                    .Any(o => o.TagID == "2d" && o.ContentItemFeatureOnTags.Any(cift => cift.ContentItemId == contentItemId)));

            Assert.IsTrue(
                tagDao.GetAll()
                    .Any(o => o.TagID == "42" && o.ContentItemFeatureOnTags.Any(cift => cift.ContentItemId == contentItemId)));

            Assert.IsTrue(
                featureLocationDao.GetAll()
                    .Any(
                        o =>
                            o.FeatureLocationId == "23" &&
                            o.ContentItemFeatureLocations.Any(cifl => cifl.ContentItemId == contentItemId)));


        }
    }
}
