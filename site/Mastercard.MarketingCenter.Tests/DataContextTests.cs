using Mastercard.MarketingCenter.Tests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Data.Entity;
using System.Transactions;

namespace Mastercard.MarketingCenter.Tests
{
    /// <summary>
    /// The base class will allow LINQ to SQL integration testing with automatic rollbacks after each test
    /// </summary>
    /// <typeparam name="TContext">The type of data context to use</typeparam>
    public abstract class RolledBackDataContextTests<TContext> : BaseTests where TContext : DbContext
    {
        private TContext _context;
        private IDisposable _transaction;

        [TestInitialize]
        public void InitDataContext()
        {
            _context = (TContext)Activator.CreateInstance(typeof(TContext));
            _transaction = new TransactionScope();
        }

        /// <summary>
        /// Provides access to the internal data context. Any changes made will be automatically rolled back
        /// </summary>
        protected new TContext Context
        {
            get
            {
                if (_context == null)
                {
                    InitDataContext();
                }

                return _context;
            }
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _transaction.Dispose();
            _context.Dispose();
            _context = null;
        }
    }
}