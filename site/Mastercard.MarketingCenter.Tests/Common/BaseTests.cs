﻿using AutoMapper;
using Mastercard.MarketingCenter.Cms.Services.Infrastructure;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Interfaces;
using Mastercard.MarketingCenter.Data.Repositories;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Slam.Cms;
using Slam.Cms.Common.Interfaces;
using Slam.Cms.Data;
using System.Collections.Generic;
using System.IO;
using System.Security.Principal;
using System.Web;

namespace Mastercard.MarketingCenter.Tests.Common
{
    public abstract class BaseTests
    {
        public MarketingCenterDbContext Context { get; set; }
        public MasterCardPortalDataContext DataContext { get; private set; }
        public SlamContext SlamContext { get; private set; }
        public ICmsDbConnection CmsDbConnection { get; private set; }
        public IUnitOfWork UnitOfWork { get; private set; }
        public ContentService ContentService { get; private set; }
        public ICloningService CloningServices { get; private set; }
        public LookupServices LookupServices { get; private set; }
        public AttachmentServices AttachmentServices { get; private set; }
        public SecondaryImageServices SecondaryImageServices { get; private set; }
        public TagServices TagServices { get; private set; }
        public ContentItemDao ContentItemDao { get; private set; }
        public TagCategoryDao TagCategoryDao { get; private set; }
        public ContentTypeDao ContentTypeDao { get; private set; }
        public ContentTypeService ContentTypeService { get; private set; }
        public TagDao TagDao { get; private set; }
        public FeatureLocationDao FeatureLocationDao { get; private set; }
        public FieldDefinitionDao FieldDefinitionDao { get; private set; }
        public ContentItemVersionDao ContentItemVersionDao { get; set; }
        public TagHierarchyPositionDao TagHierarchyPositionDao { get; set; }
        public ListDao ListDao { get; set; }
        public IdGeneratorService IdGeneratorService { get; set; }
        public CustomizationServices CustomizationServices { get; set; }
        public CommonService CommonService { get; set; }
        public FeatureOnServices FeatureOnServices { get; set; }
        public TriggerMarketingServices TriggerMarketingServices { get; private set; }
        public TranslationServices TranslationServices { get; set; }
        public IRegionService RegionService { get; set; }
        public IEventLocationService EventLocationService { get; set; }
        public IOfferService OfferService { get; set; }
        public IEventLocationRepository EventLocationRepository { get; set; }
        public HttpContext HttpContext { get; private set; }
        public ContentItemRepository ContentItemRepository { get; private set; }
        public RegionRepository RegionRepository { get; private set; }
        public IOfferRepository OfferRepository { get; private set; }
        public UserContext UserContext { get; set; }
        public ContentItemService ContentItemService { get; set; }
        public ICachingService CachingService { get; set; }
        public IEmailService EmailService { get; set; }
        public SettingsService SettingsService { get; set; }
        public UserService UserService { get; set; }
        protected IPreviewMode PreviewMode { get; set; }
        protected IssuerService IssuerService { get; set; }
        public MigratedContentRepository MigratedContentRepository { get; private set; }
        public IFieldDataNormalizationContext fieldDataNormalizationContext { get; private set; }
        protected IUrlService UrlService { get; set; }
        protected IMapper Mapper { get; set; }
        public IPricelessApiService PricelessApiService { get; private set; }
        public IZipFileService ZipFileService { get; private set; }
        public ICsvFileService CsvFileService { get; private set; }
        public IFileUploadService FileUploadService { get; private set; }

        [TestInitialize]
        public void SetUp()
        {
            HttpContext.Current = new HttpContext(
                new HttpRequest("", "http://tempuri.org", ""),
                new HttpResponse(new StringWriter())
                )
            {
                User = new GenericPrincipal(new GenericIdentity("username"), new string[0])
            };
            HttpContext = HttpContext.Current;

            Mapper = new Mapper(null);
            Context = new MarketingCenterDbContext();
            DataContext = new MasterCardPortalDataContext();
            CmsDbConnection = new CmsDbConnection(Context.Database.Connection, new SettingsService());
            ContentItemDao = new ContentItemDao(Context);
            SlamContext = new SlamContext(new CookieBasedPreviewMode(HttpContext));
            UnitOfWork = new UnitOfWork(Context);
            ContentTypeDao = new ContentTypeDao(Context);
            ContentTypeService = new ContentTypeService(ContentItemDao, ContentTypeDao);
            TagCategoryDao = new TagCategoryDao(Context);
            ContentItemVersionDao = new ContentItemVersionDao(Context);
            FeatureLocationDao = new FeatureLocationDao(Context);
            TagDao = new TagDao(Context);
            FieldDefinitionDao = new FieldDefinitionDao(Context);
            LookupServices = new LookupServices(CmsDbConnection);
            TagHierarchyPositionDao = new TagHierarchyPositionDao(Context);
            CommonService = new CommonService(CmsDbConnection);
            TagServices = new TagServices(CmsDbConnection, ContentTypeDao, TagCategoryDao, TagDao, TagHierarchyPositionDao, CommonService);
            IdGeneratorService = new IdGeneratorService(Context.Database.Connection);
            CustomizationServices = new CustomizationServices(CmsDbConnection);
            FeatureOnServices = new FeatureOnServices(TagCategoryDao, FeatureLocationDao, TagHierarchyPositionDao, TagDao);
            AttachmentServices = new AttachmentServices(CmsDbConnection, ContentTypeDao, ContentItemService, CommonService);
            SecondaryImageServices = new SecondaryImageServices(CmsDbConnection, ContentTypeDao, ContentItemService, CommonService);
            TriggerMarketingServices = new TriggerMarketingServices(CmsDbConnection, ContentTypeDao, ContentItemService, CommonService);
            RegionService = new RegionService(RegionRepository);
            PricelessApiService = new PricelessApiService(UrlService, SettingsService);
            ZipFileService = new ZipFileService(SettingsService);
            CsvFileService = new CsvFileService();
            OfferService = new OfferService(Mapper, PreviewMode, CachingService, OfferRepository, RegionService, EmailService, PricelessApiService, ZipFileService, CsvFileService, SettingsService);
            OfferRepository = new OfferRepository(Context);
            EventLocationService = new EventLocationService(EventLocationRepository, CachingService, Mapper);
            EventLocationRepository = new EventLocationRepository(Context);
            RegionRepository = new RegionRepository(Context);
            ContentItemRepository = new ContentItemRepository(Context, RegionRepository);
            UserContext = new UserContext("us", "en");
            CachingService = new InMemoryCachingService();
            MigratedContentRepository = new MigratedContentRepository(Context);
            MigratedContentRepository = new MigratedContentRepository(Context);
            FileUploadService = new FileUploadService(new UploadActivityRepository(Context), Mapper);

            var regionalizeService = new RegionalizeService();
            var mailDispatcherRepository = new MailDispatcherRepository(Context);
            UrlService = new UrlService(SettingsService);
            var mailDispatcherService = new MailDispatcherService(mailDispatcherRepository, UrlService);
            PreviewMode = new CookieBasedPreviewMode(HttpContext);
            EmailService = new EmailService(CachingService, regionalizeService, mailDispatcherService, SettingsService, UrlService, UserService);
            var issuerRepository = new IssuerRepository(Context);
            var processorService = new ProcessorService(new ProcessorRepository(Context));
            var segmentationService = new SegmentationService(Mapper, new SegmentationRepository(Context));
            IssuerService = new IssuerService(new IdGeneratorService(Context.Database.Connection), issuerRepository, UserService, processorService, segmentationService, UnitOfWork);
            var defaultValueCalculators = (IEnumerable<IDefaultValueCalculator>)new List<IDefaultValueCalculator>();
            fieldDataNormalizationContext = new FieldDataNormalizationContext(new System.Lazy<IEnumerable<IFieldDataRetrieval>>());
            var offerRepository = new OfferRepository(Context);
            ContentItemService = new ContentItemService(
                ContentItemRepository,
                SlamContext,
                defaultValueCalculators,
                MigratedContentRepository,
                SettingsService,
                offerRepository);

            ListDao = new ListDao(Context);
            ContentService = new ContentService(
                CmsDbConnection,
                ContentTypeService,
                FieldDefinitionDao,
                LookupServices,
                TagServices,
                ContentItemVersionDao,
                ListDao,
                IdGeneratorService,
                CustomizationServices,
                FeatureOnServices,
                AttachmentServices,
                SecondaryImageServices,
                TriggerMarketingServices,
                TranslationServices,
                UserContext,
                ContentItemService,
                fieldDataNormalizationContext,
                CommonService,
                UrlService,
                EventLocationService);

            UserContext = new UserContext("US", "EN");
        }
    }
}