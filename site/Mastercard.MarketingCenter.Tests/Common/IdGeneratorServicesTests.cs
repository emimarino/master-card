﻿using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Web.Configuration;

namespace Mastercard.MarketingCenter.Tests.Common
{
    [TestClass]
    public class IdGeneratorServicesTests : RolledBackDataContextTests<MarketingCenterDbContext>
    {
        [TestMethod]
        [Ignore]
        public void Can_Get_Current_Id()
        {
            var service = GetService();

            var id = service.GetCurrentId();

            var configId =
                Convert.ToInt32(WebConfigurationManager.AppSettings["InitialIdSeed"]).ToString("X").ToLowerInvariant();

            Assert.AreEqual(id, configId);
        }

        [TestMethod]
        [Ignore]
        public void Can_Get_New_Id()
        {
            var service = GetService();

            service.GetCurrentId();
            var newId = service.GetNewUniqueId();

            var configId = (Convert.ToInt32(WebConfigurationManager.AppSettings["InitialIdSeed"]) + 1)
                .ToString("X")
                .ToLowerInvariant();

            Assert.AreEqual(newId, configId);
        }

        private IdGeneratorService GetService()
        {
            return new IdGeneratorService(Context.Database.Connection);
        }
    }
}
