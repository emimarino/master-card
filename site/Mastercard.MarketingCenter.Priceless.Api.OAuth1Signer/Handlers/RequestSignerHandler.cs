﻿using Mastercard.MarketingCenter.Priceless.Api.OAuth1Signer.Signers;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;

namespace Mastercard.MarketingCenter.Priceless.Api.OAuth1Signer.Handlers
{
    public class RequestSignerHandler : HttpClientHandler
    {
        private readonly NetHttpClientSigner signer;

        public RequestSignerHandler(string consumerKey, RSACryptoServiceProvider signingKey)
        {
            signer = new NetHttpClientSigner(consumerKey, signingKey);
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            signer.Sign(request);
            return base.SendAsync(request, cancellationToken);
        }
    }
}