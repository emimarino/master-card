﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;

namespace Mastercard.MarketingCenter.Data
{
    public class CustomDynamicObject : DynamicObject, IDictionary<string, object>
    {
        private readonly Dictionary<string, object> _data = new Dictionary<string, object>();

        public ICollection<string> Keys => ((IDictionary<string, object>)_data).Keys;

        public ICollection<object> Values => ((IDictionary<string, object>)_data).Values;

        public int Count => _data.Count;

        public bool IsReadOnly => ((IDictionary<string, object>)_data).IsReadOnly;

        public object this[string key] { get { return _data[key]; } set { _data[key] = value; } }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            _data[binder.Name] = value;
            return true;
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            object value;
            if (_data.TryGetValue(binder.Name, out value))
            {
                var getter = value as Getter;
                result = (getter == null) ? value : getter(this);
                return true;
            }
            result = null;

            return true;
        }

        public bool ContainsKey(string key)
        {
            return _data.ContainsKey(key);
        }

        public void Add(string key, object value)
        {
            _data.Add(key, value);
        }

        public bool Remove(string key)
        {
            return _data.Remove(key);
        }

        public bool TryGetValue(string key, out object value)
        {
            return _data.TryGetValue(key, out value);
        }

        public void Add(KeyValuePair<string, object> item)
        {
            ((IDictionary<string, object>)_data).Add(item);
        }

        public void Clear()
        {
            _data.Clear();
        }

        public bool Contains(KeyValuePair<string, object> item)
        {
            return ((IDictionary<string, object>)_data).Contains(item);
        }

        public void CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
        {
            ((IDictionary<string, object>)_data).CopyTo(array, arrayIndex);
        }

        public bool Remove(KeyValuePair<string, object> item)
        {
            return ((IDictionary<string, object>)_data).Remove(item);
        }

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            return ((IDictionary<string, object>)_data).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IDictionary<string, object>)_data).GetEnumerator();
        }

        public delegate object Getter(dynamic target);
    }
}