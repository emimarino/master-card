﻿using System;

namespace Mastercard.MarketingCenter.Data.Metadata
{
    public class ConnectionStateAction : IDisposable
    {
        public Action Action { get; private set; }
        public Action DisposeAction { get; private set; }

        public ConnectionStateAction(Action action, Action disposeAction = null)
        {
            Action = action;
            DisposeAction = disposeAction;
        }

        public void Dispose()
        {
            Action = null;

            if (DisposeAction != null)
            {
                DisposeAction();
                DisposeAction = null;
            }
        }
    }
}