﻿namespace Mastercard.MarketingCenter.Data.Metadata.GDP
{
    public static class ConsentDataType
    {
        public const string Document = "document";
        public const string ConsentLanguageText = "consentlanguagetext";
        public const string LegalContent = "legalcontent";
    }
}