﻿namespace Mastercard.MarketingCenter.Data.Metadata.GDP
{
    public static class ResponseCode
    {
        public const string Success = "SUCCESS";
        public const string NotFound = "NO_DATA_FOUND";
    }
}