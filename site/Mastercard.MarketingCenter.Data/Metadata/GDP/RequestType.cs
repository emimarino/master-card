﻿namespace Mastercard.MarketingCenter.Data.Metadata.GDP
{
    public static class RequestType
    {
        public const string ConsentHistory = "C";
        public const string Delete = "D";
        public const string View = "V";
    }
}