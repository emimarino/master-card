﻿namespace Mastercard.MarketingCenter.Data.Metadata.GDP
{
    public class ConsentDocumentType
    {
        public const string Document = "pdf-document";
        public const string ConsentLanguageText = "consentlanguagetext";
        public const string LegalContent = "legalcontent";
    }
}