﻿using Slam.Cms.Data;
using System;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    [SlamTable(TableName = MarketingCenterDbConstants.Tables.UpcomingEvent)]
    public class UpcomingEventDTO : ContentItem, IContentItemWithTitle
    {
        public string Title { get; set; }
        public DateTime? Date { get; set; }
        public string Location { get; set; }
        public string Link { get; set; }
        public int Ordering { get; set; }
        public string Image { get; set; }
    }
}