﻿using Slam.Cms.Data;
using System;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    [SlamTable(TableName = MarketingCenterDbConstants.Tables.Program)]
    public class ProgramDTO : ContentItem, IContentItemWithTitle
    {
        // Common Tag Browser Item fields
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string ThumbnailImage { get; set; }
        public string LongDescription { get; set; }
        public string ShowInCrossSellBox { get; set; }
        public string PriorityLevel { get; set; }
        public bool? ShowInTagBrowser { get; set; }
        public string MainImage { get; set; }
        public string CallToAction { get; set; }
        public DateTime? PrintedOrderPeriodStartDate { get; set; }
        public DateTime? PrintedOrderPeriodEndDate { get; set; }
        public string PrintedOrderPeriodAlternateDisplayText { get; set; }
        public DateTime? OnlineOrderPeriodStartDate { get; set; }
        public DateTime? OnlineOrderPeriodEndDate { get; set; }
        public string OnlineOrderPeriodAlternateDisplayText { get; set; }
        public DateTime? InMarketStartDate { get; set; }
        public DateTime? InMarketEndDate { get; set; }
        public string InMarketAlternateDisplayText { get; set; }
        public string PrintedOrderPeriodAlternateDateText { get; set; }
        public string OnlineOrderPeriodAlternateDateText { get; set; }
        public string InMarketAlternateDateText { get; set; }
        public bool? ShowOrderPeriods { get; set; }
        public string IconID { get; set; }
        public string AssetType { get; set; }
        public string RedirectURL { get; set; }
        public string RestrictToSpecialAudience { get; set; }
        public bool EnableFavoriting { get; set; }
        public bool ShowInMarketingCalendar { get; set; }
        public bool PreviewFiles { get; set; }
        public int BusinessOwnerID { get; set; }
    }
}