﻿using Mastercard.MarketingCenter.Data.Interfaces;
using Slam.Cms.Data;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    [SlamTable(TableName = MarketingCenterDbConstants.Tables.MarqueeSlide)]
    public class MarqueeSlideDTO : ContentItem, IContentItemWithTitle
    {
        public string Title { get; set; }
        public string Image { get; set; }
        public string LongDescription { get; set; }
        public string MainUrl { get; set; }
        public int OrderNumber { get; set; }
        public string RestrictToSpecialAudience { get; set; }
        public string ButtonText { get; set; }
        public string InnerDescription { get; set; }
    }
}