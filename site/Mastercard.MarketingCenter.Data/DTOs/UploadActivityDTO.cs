﻿namespace Mastercard.MarketingCenter.Data.DTOs
{
    public class UploadActivityDTO
    {
        public string ContentItemId { get; set; }
        public string Filename { get; set; }
        public string FileReference { get; set; }
        public decimal Size { get; set; }
        public string FieldDefinitionName { get; set; }
        public int ActivityUserId { get; set; }
        public int Action { get; set; }
        public int Status { get; set; }
    }
}