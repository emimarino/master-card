﻿using Slam.Cms.Data;
using System;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    [SlamTable(TableName = MarketingCenterDbConstants.Tables.Offer)]
    public class OfferDTO : ContentItem, IContentItemWithTitle
    {
        public string Title { get; set; }
        public int BusinessOwnerID { get; set; }
        public bool Evergreen { get; set; }
        public DateTime? ValidityPeriodFromDate { get; set; }
        public DateTime? ValidityPeriodToDate { get; set; }
        public DateTime? EventDate { get; set; }
        public DateTime? BookByDate { get; set; }
        public string MainImage { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string TermsAndConditions { get; set; }
        public bool PreviewFiles { get; set; }
        public string DownloadZipFile { get; set; }
    }
}