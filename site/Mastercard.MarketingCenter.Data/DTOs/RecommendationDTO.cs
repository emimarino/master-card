﻿using Slam.Cms.Data;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    [SlamTable(TableName = MarketingCenterDbConstants.Tables.Recommendation)]
    public class RecommendationDTO : ContentItem
    {
        public string DisplayName { get; set; }
        public string ShortDescription { get; set; }
    }
}