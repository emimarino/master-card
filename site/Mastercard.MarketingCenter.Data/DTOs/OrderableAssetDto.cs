﻿using Slam.Cms.Data;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    [SlamTable(TableName = MarketingCenterDbConstants.Tables.OrderableAsset)]
    public class OrderableAssetDTO : ContentItem, IContentItemWithTitle
    {
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string ThumbnailImage { get; set; }
        public string LongDescription { get; set; }
        public string ShowInCrossSellBox { get; set; }
        public string PriorityLevel { get; set; }
        public bool? ShowInTagBrowser { get; set; }
        public string MainImage { get; set; }
        public string CallToAction { get; set; }
        public string Sku { get; set; }
        public bool? Vdp { get; set; }
        public bool? Customizable { get; set; }
        public string ElectronicDeliveryFile { get; set; }
        public string ApproverEmailAddress { get; set; }
        public string IconID { get; set; }
        public string PricingScheduleID { get; set; }
        public string AssetType { get; set; }
        public string PDFPreview { get; set; }
        public string RestrictToSpecialAudience { get; set; }
        public bool EnableFavoriting { get; set; }
        public int BusinessOwnerID { get; set; }
    }
}