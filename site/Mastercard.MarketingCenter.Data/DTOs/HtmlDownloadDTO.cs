﻿using Mastercard.MarketingCenter.Data.Interfaces;
using Slam.Cms.Data;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    [SlamTable(TableName = MarketingCenterDbConstants.Tables.HtmlDownload)]
    public class HtmlDownloadDTO : ContentItem, IContentItemWithTitle
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string Filename { get; set; }
    }
}