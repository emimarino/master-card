﻿using Slam.Cms.Data;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    [SlamTable(TableName = MarketingCenterDbConstants.Tables.YoutubeVideo)]
    public class YoutubeVideoDTO : ContentItem, IContentItemWithTitle
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public int OrderNumber { get; set; }
        public string StaticVideoImage { get; set; }
    }
}