﻿using Slam.Cms.Data;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    [SlamTable(TableName = MarketingCenterDbConstants.Tables.AssessmentCriteria)]
    public class AssessmentCriteriaDTO : ContentItem, IContentItemWithTitle
    {
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string AssessmentColumnName { get; set; }
    }
}