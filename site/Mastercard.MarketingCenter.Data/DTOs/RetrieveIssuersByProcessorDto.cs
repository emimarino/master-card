﻿using System;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    public class RetrieveIssuersByProcessorDTO
    {
        public string IssuerName { get; set; }
        public string DomainName { get; set; }
        public string IssuerID { get; set; }
        public string ProcessorName { get; set; }
        public int? Users { get; set; }
        public int? Orders { get; set; }
        public DateTime? LastOrderDate { get; set; }
    }
}