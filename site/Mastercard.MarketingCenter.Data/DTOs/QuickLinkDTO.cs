﻿using Slam.Cms.Data;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    [SlamTable(TableName = MarketingCenterDbConstants.Tables.QuickLink)]
    public class QuickLinkDTO : ContentItem, IContentItemWithTitle
    {
        public string Title { get; set; }
        public string Paragraph { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }
        public int OrderNumber { get; set; }
    }
}