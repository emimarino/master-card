﻿using Slam.Cms.Data;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    [SlamTable(TableName = MarketingCenterDbConstants.Tables.AssetFullWidth)]
    public class AssetFullWidthDTO : ContentItem, IContentItemWithTitle
    {
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string ThumbnailImage { get; set; }
        public string LongDescription { get; set; }
        public string BottomAreaDescription { get; set; }
        public string ShowInCrossSellBox { get; set; }
        public string PriorityLevel { get; set; }
        public bool? ShowInTagBrowser { get; set; }
        public string IconID { get; set; }
        public string AssetType { get; set; }
        public string RestrictToSpecialAudience { get; set; }
        public bool EnableFavoriting { get; set; }
        public int BusinessOwnerID { get; set; }
        public bool AllowCloning { get; set; }
        public bool NotifyWhenSourceChanges { get; set; }
    }
}