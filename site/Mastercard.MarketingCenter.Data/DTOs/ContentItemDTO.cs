﻿using System;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    public class ContentItemDTO
    {
        public string ContentItemId { get; set; }
        public string ContentTypeId { get; set; }
        public string Title { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}