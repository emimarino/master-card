﻿using Slam.Cms.Data;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    [SlamTable(TableName = MarketingCenterDbConstants.Tables.Snippet)]
    public class SnippetDTO : ContentItem, IContentItemWithTitle
    {
        public string Title { get; set; }
        public string Html { get; set; }
        public string RestrictToSpecialAudience { get; set; }
    }
}