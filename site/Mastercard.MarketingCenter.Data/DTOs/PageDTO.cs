﻿using Slam.Cms.Data;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    [SlamTable(TableName = MarketingCenterDbConstants.Tables.Page)]
    public class PageDTO : ContentItem, IContentItemWithTitle
    {
        public string Title { get; set; }
        public string Uri { get; set; }
        public string IntroCopy { get; set; }
        public string Terms { get; set; }
        public string RestrictToSpecialAudience { get; set; }
        public string HeaderTitle { get; set; }
        public string HeaderParagraph { get; set; }
        public string HeaderBackgroundImage { get; set; }
        public string HeaderAsideImage { get; set; }
        public string HeaderUrl { get; set; }
        public string BottomTitle { get; set; }
        public string BottomParagraph { get; set; }
        public string BottomBackgroundImage { get; set; }
        public string BottomAsideImage { get; set; }
        public string BottomUrl { get; set; }
        public string ShortDescription { get; set; }
        public string AsideContent { get; set; }
        public bool EnableFavoriting { get; set; }
    }
}