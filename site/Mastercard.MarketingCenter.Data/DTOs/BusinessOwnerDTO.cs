﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    public class BusinessOwnerDTO
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsDisabled { get; set; }

        public IEnumerable<ContentItemDTO> ContentItems { get; set; }
    }
}