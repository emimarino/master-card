﻿using System;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    public class ContentReportDTO
    {
        public string ContentType { get; set; }
        public string Title { get; set; }
        public string RegionName { get; set; }
        public DateTime CreationDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        public string StatusName { get; set; }
        public string BusinessOwner { get; set; }
        public string LiveContentUrl { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string RegionId { get; set; }
        public string ContentItemId { get; set; }
    }
}