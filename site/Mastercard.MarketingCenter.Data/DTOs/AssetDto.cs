﻿using Slam.Cms.Data;
using System;

namespace Mastercard.MarketingCenter.Data.DTOs
{
    [SlamTable(TableName = MarketingCenterDbConstants.Tables.Asset)]
    public class AssetDTO : ContentItem, IContentItemWithTitle
    {
        // Common Tag Browser Item fields
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string ThumbnailImage { get; set; }
        public string LongDescription { get; set; }
        public string ShowInCrossSellBox { get; set; }
        public string PriorityLevel { get; set; }
        public bool? ShowInTagBrowser { get; set; }
        public string MainImage { get; set; }
        public string CallToAction { get; set; }
        public string IconID { get; set; }
        public string AssetType { get; set; }
        public string RestrictToSpecialAudience { get; set; }
        public bool EnableFavoriting { get; set; }
        public int BusinessOwnerID { get; set; }
        public bool AllowCloning { get; set; }
        public bool NotifyWhenSourceChanges { get; set; }
        public bool PreviewFiles { get; set; }
        public bool ShowInMarketingCalendar { get; set; }
        public DateTime? InMarketStartDate { get; set; }
        public DateTime? InMarketEndDate { get; set; }
    }
}