﻿namespace Mastercard.MarketingCenter.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MarketingCenterDbContext _marketingCenterDbContext;

        public UnitOfWork(MarketingCenterDbContext marketingCenterDbContext)
        {
            _marketingCenterDbContext = marketingCenterDbContext;
        }

        public void Commit()
        {
            _marketingCenterDbContext.SaveChanges();
        }
    }
}