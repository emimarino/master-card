﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Extensions
{
    public static class CollectionExtensions
    {
        public static object GetData(this IDictionary<string, object> contentItemData, string fieldName)
        {
            return contentItemData.ContainsKey(fieldName) ? contentItemData[fieldName] : null;
        }

        public static void AddOrUpdate(this IDictionary<string, object> contentItemData, string fieldName, object value)
        {
            if (!contentItemData.ContainsKey(fieldName))
            {
                contentItemData.Add(fieldName, value);
            }
            else
            {
                contentItemData[fieldName] = value;
            }
        }

        public static int GetDataAsInt(this IDictionary<string, object> contentItemData, string fieldName)
        {
            int intData;
            int.TryParse(contentItemData.GetData(fieldName)?.ToString(), out intData);
            return intData;
        }

        public static string GetContentItemId(this IDictionary<string, object> contentItemData)
        {
            return Convert.ToString(contentItemData["ContentItemId"]);
        }

        public static void SetContentItemId(this IDictionary<string, object> contentItemData, string contentItemId)
        {
            contentItemData["ContentItemId"] = contentItemId;
        }

        public static bool HasContentItemId(this IDictionary<string, object> contentItemData)
        {
            return contentItemData.ContainsKey("ContentItemId");
        }

        public static string GetItemId(this IDictionary<string, object> data,
            IEnumerable<FieldDefinition> fieldDefinitions)
        {
            var idField =
                fieldDefinitions.Where(
                    o => o.FieldTypeCode == FieldType.GuidPrimaryKey || o.FieldTypeCode == FieldType.IntPrimaryKey);

            if (idField.Count() > 1)
                throw new InvalidOperationException(
                    string.Format("Multiple Primary Keys defined for fields: {0}(IDs: {1})",
                        string.Join("/", idField.Select(o => o.Name)),
                        string.Join(",", idField.Select(o => o.FieldDefinitionId))));

            if (!idField.Any())
                throw new InvalidOperationException("No ID field defined!");

            return Convert.ToString(data[idField.First().Name]);
        }

        public static bool IsContentItem(this IDictionary<string, object> data)
        {
            return data.ContainsKey("ContentItemId");
        }

        public static IEnumerable<FieldDefinition> GetTrackingFields(this IEnumerable<ListFieldDefinition> listFieldDefinitions)
        {
            return listFieldDefinitions.Where(o => FieldType.TrackingFieldsTypes.Any(tf => o.FieldDefinition.FieldTypeCode == tf))
                    .Select(o => o.FieldDefinition);
        }
    }
}