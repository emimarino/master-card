﻿using System.Text;
using System.Text.RegularExpressions;
using Mastercard.MarketingCenter.Common.Extensions;

namespace Mastercard.MarketingCenter.Data.Extensions
{
    public static class StringExtension
    {
        public static string GenerateSlug(this string phrase)
        {
            if (phrase.IsNullOrEmpty())
                return string.Empty;

            var str = phrase.RemoveAccent();

            // invalid chars, make into spaces
            str = Regex.Replace(str, @"[^a-zA-Z0-9\s]", " ");

            // convert multiple spaces/hyphens into one space       
            str = Regex.Replace(str, @"[\s]+", " ").Trim();

            // trim it
            str = str.Trim();

            return str;
        }

        public static string RemoveAccent(this string text)
        {
            if (text.IsNullOrEmpty())
                return string.Empty;

            var bytes = Encoding.GetEncoding("Cyrillic").GetBytes(text);
            return Encoding.ASCII.GetString(bytes);
        }

        public static string EncodeForPathWithoutHttpContext(this string inputString)
        {
            return inputString.Replace("\r\n", "-").Replace(" ", "-").Replace("%20", "-").Replace(".", "").Replace(":", "").Replace("&", "").Replace("/", "-");
        }
    }
}
