﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Extensions
{
    public static class OfferRepositoryExtension
    {
        public static IQueryable<Offer> FilterByRegion(this IQueryable<Offer> offers, string regionId)
        {
            return offers.Include(o => o.CrossBorderRegions)
                         .Where(o => o.ContentItem.RegionId.Trim().Equals(regionId.Trim(), StringComparison.InvariantCultureIgnoreCase)
                                  || o.CrossBorderRegions.Any(r => r.RegionId.Trim().Equals(regionId.Trim(), StringComparison.InvariantCultureIgnoreCase)));
        }

        public static IQueryable<Offer> FilterByVisibility(this IQueryable<Offer> offers, string regionId)
        {
            return offers.Include(o => o.RegionVisibilities)
                         .Where(o => o.RegionVisibilities.Any(r => r.RegionId.Trim().Equals(regionId.Trim(), StringComparison.InvariantCultureIgnoreCase) && r.Value == 1));
        }

        public static IQueryable<Offer> FilterByDate(this IQueryable<Offer> offers)
        {
            return offers.Where(o => o.Evergreen || o.ValidityPeriodToDate.Value >= DateTime.Today);
        }

        public static IQueryable<Offer> FilterByStatus(this IQueryable<Offer> offers, int status)
        {
            return offers.FilterByStatus(new int[] { status });
        }

        public static IQueryable<Offer> FilterByDraftView(this IQueryable<Offer> offers)
        {
            return offers.GroupBy(o => o.ContentItem.PrimaryContentItemId)
                         .Select(o => o.OrderByDescending(y => y.ContentItemId).FirstOrDefault())
                         .Include(o => o.ContentItem.Tags.Select(t => t.TagHierarchyPositions))
                         .Include(o => o.EventLocations)
                         .Include(o => o.CardExclusivities)
                         .Include(o => o.Categories)
                         .Include(o => o.OfferType);
        }

        public static IQueryable<Offer> FilterByStatus(this IQueryable<Offer> offers, int[] status)
        {
            return offers.Where(o => status.Contains(o.ContentItem.StatusID));
        }

        public static Offer GetById(this IQueryable<Offer> offers, string contentItemId, bool isPreviewMode = false)
        {
            contentItemId = contentItemId.GetAsLiveContentItemId();
            var extendedOffers = offers.Include(o => o.RelatedItems)
                                       .Include(o => o.Attachments)
                                       .Include(o => o.SecondaryImages)
                                       .Include(o => o.EventLocations)
                                       .Include(o => o.CardExclusivities)
                                       .Include(o => o.Categories)
                                       .Include(o => o.OfferType);

            if (isPreviewMode)
            {
                return extendedOffers.Where(o => o.ContentItem.PrimaryContentItemId.Equals(contentItemId, StringComparison.InvariantCultureIgnoreCase))
                                     .OrderByDescending(o => o.ContentItemId)
                                     .FirstOrDefault();
            }

            return extendedOffers.FirstOrDefault(o => o.ContentItemId.Equals(contentItemId, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}