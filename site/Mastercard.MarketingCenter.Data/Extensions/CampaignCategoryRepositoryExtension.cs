﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Extensions
{
    public static class CampaignCategoryRepositoryExtension
    {
        public static IQueryable<CampaignCategory> FilterByRegion(this IQueryable<CampaignCategory> categories, string regionId)
        {
            return categories.Where(c => c.RegionId.Trim().Equals(regionId.Trim(), StringComparison.InvariantCultureIgnoreCase));
        }

        public static CampaignCategory GetById(this IQueryable<CampaignCategory> categories, string campaignCategoryId)
        {
            return categories.FirstOrDefault(c => c.CampaignCategoryId.Equals(campaignCategoryId, StringComparison.InvariantCultureIgnoreCase));
        }

        public static IQueryable<CampaignCategory> FilterByIds(this IQueryable<CampaignCategory> categories, string[] campaignCategoryIds)
        {
            return categories.Where(c => campaignCategoryIds.Contains(c.CampaignCategoryId));
        }
    }
}