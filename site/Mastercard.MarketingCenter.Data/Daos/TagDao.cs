﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Daos
{
    public class TagDao : Dao<Tag, string>
    {
        public TagDao(MarketingCenterDbContext context) : base(context)
        {
        }

        public override IQueryable<Tag> GetAll()
        {
            return base.GetAll().Include(t => t.ContentItemFeatureOnTags);
        }

        public override Tag GetById(string id)
        {
            return Entities.Include(t => t.ContentItemFeatureOnTags).FirstOrDefault(t => t.TagID == id);
        }
    }
}