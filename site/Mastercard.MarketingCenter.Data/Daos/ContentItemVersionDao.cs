﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Daos
{
    public class ContentItemVersionDao : Dao<ContentItemVersion, int>
    {
        public ContentItemVersionDao(MarketingCenterDbContext context) : base(context)
        {
        }

        public override ContentItemVersion GetById(int id)
        {
            return Entities.FirstOrDefault(o => o.ContentItemVersionId == id);
        }

        public IQueryable<ContentItemVersion> GetByContentItemId(string contentItemId)
        {
            return Entities.Include(x => x.ModifiedByUser).Where(o => o.ContentItemId == contentItemId);
        }
    }
}