﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Daos
{
    public class ContentItemDao : Dao<ContentItemBase, string>
    {
        public ContentItemDao(MarketingCenterDbContext context) : base(context)
        {
        }

        public override ContentItemBase GetById(string id)
        {
            return Entities.Include(ci => ci.CreatedByUser)
                           .Include(ci => ci.ModifiedByUser)
                           .Include(ci => ci.ContentType.ContentTypeFieldDefinitions.Select(ctfd => ctfd.FieldDefinition.FieldType))
                           .FirstOrDefault(o => o.ContentItemId == id);
        }
    }
}