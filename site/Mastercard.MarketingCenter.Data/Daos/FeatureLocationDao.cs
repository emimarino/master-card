﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Daos
{
    public class FeatureLocationDao : Dao<FeatureLocation, string>
    {
        public FeatureLocationDao(MarketingCenterDbContext context) : base(context)
        {
        }

        public override IQueryable<FeatureLocation> GetAll()
        {
            return base.GetAll().Include(fl => fl.ContentItemFeatureLocations);
        }

        public IQueryable<FeatureLocation> GetAllById(params string[] ids)
        {
            return Entities.Include(fl => fl.ContentItemFeatureLocations)
                           .Where(o => ids.Contains(o.FeatureLocationId));
        }

        public override FeatureLocation GetById(string id)
        {
            return Entities.Include(fl => fl.ContentItemFeatureLocations)
                           .FirstOrDefault(o => o.FeatureLocationId == id);
        }
    }
}