﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Daos
{
    public class SegmentationTranslatedContentDao : Dao<SegmentationTranslatedContent, string>
    {
        public SegmentationTranslatedContentDao(MarketingCenterDbContext context) : base(context)
        {
        }

        public override SegmentationTranslatedContent GetById(string id)
        {
            return Entities.FirstOrDefault(o => o.SegmentationId == id);
        }
    }
}