﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Daos
{
    public class ListDao : Dao<List, int>
    {
        public ListDao(MarketingCenterDbContext context) : base(context)
        {
        }

        public override List GetById(int id)
        {
            return Entities.Include(ct => ct.ListFieldDefinitions.Select(ctfd => ctfd.FieldDefinition.FieldType))
                           .Include(l => l.ListClientEvents)
                           .FirstOrDefault(o => o.ListId == id);
        }

        public List GetByTableName(string tableName)
        {
            return Entities.FirstOrDefault(o => o.TableName == tableName);
        }
    }
}