﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Daos
{
    public class FieldDefinitionDao : Dao<FieldDefinition, int>
    {
        public FieldDefinitionDao(MarketingCenterDbContext context) : base(context)
        {
        }

        public override IQueryable<FieldDefinition> GetAll()
        {
            return base.GetAll().Include(fd => fd.FieldType).Include(fd => fd.FieldGroup);
        }

        public override FieldDefinition GetById(int id)
        {
            return Entities.FirstOrDefault(o => o.FieldDefinitionId == id);
        }

        public FieldDefinition GetByName(string name)
        {
            return Entities.FirstOrDefault(o => o.Name == name);
        }
    }
}