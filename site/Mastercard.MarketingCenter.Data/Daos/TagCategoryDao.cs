﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Daos
{
    public class TagCategoryDao : Dao<TagCategory, string>
    {
        public TagCategoryDao(MarketingCenterDbContext context) : base(context)
        {
        }

        public override TagCategory GetById(string id)
        {
            return Entities.Include(tc => tc.TagHierarchyPositions.Select(thp => thp.Tag.ContentItems))
                           .FirstOrDefault(tc => tc.TagCategoryId == id);
        }
        public TagCategory GetByTitle(string title)
        {
            return Entities.Include(tc => tc.TagHierarchyPositions.Select(thp => thp.Tag.ContentItems))
                           .FirstOrDefault(tc => tc.Title.Equals(title, StringComparison.OrdinalIgnoreCase));
        }
    }
}