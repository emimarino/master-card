﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Daos
{
    public class TagHierarchyPositionDao : Dao<TagHierarchyPosition, int>
    {
        public TagHierarchyPositionDao(MarketingCenterDbContext context) : base(context)
        {
        }

        public override TagHierarchyPosition GetById(int id)
        {
            return Entities.FirstOrDefault(o => o.PositionId == id);
        }
    }
}