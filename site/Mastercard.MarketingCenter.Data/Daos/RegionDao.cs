﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Daos
{
    public class RegionDao : Dao<Region, string>
    {
        public RegionDao(MarketingCenterDbContext context)
            : base(context)
        {
        }

        public override Region GetById(string id)
        {
#pragma warning disable CS0618 // Type or member is obsolete
            return Entities.FirstOrDefault(r => r.Id.Trim() == id);
#pragma warning restore CS0618 // Type or member is obsolete
        }
    }
}