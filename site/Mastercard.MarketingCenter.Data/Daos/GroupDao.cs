﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Daos
{
    public class GroupDao : Dao<Group, int>
    {
        public GroupDao(MarketingCenterDbContext context)
            : base(context)
        {
        }

        public override Group GetById(int id)
        {
            return Entities.FirstOrDefault(o => o.GroupId == id);
        }
    }
}