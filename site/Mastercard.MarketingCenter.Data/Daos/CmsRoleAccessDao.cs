﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Daos
{
    public class CmsRoleAccessDao : Dao<CmsRoleAccess, long>
    {
        public CmsRoleAccessDao(MarketingCenterDbContext context) : base(context)
        {
        }

        public static IList<CmsRoleAccess> _entities { get; set; }

        private IList<CmsRoleAccess> GetEntities()
        {
            if (_entities == null)
                _entities = Entities.ToList();

            return _entities;
        }

        public override CmsRoleAccess GetById(long id)
        {
            return GetEntities().FirstOrDefault(x => x.CmsRoleAccessId == id);
        }
    }
}