﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Daos
{
    public class TagTranslatedContentDao : Dao<TagTranslatedContent, string>
    {
        public TagTranslatedContentDao(MarketingCenterDbContext context) : base(context)
        {
        }

        public override TagTranslatedContent GetById(string id)
        {
            return Entities.FirstOrDefault(o => o.TagId == id);
        }
    }
}