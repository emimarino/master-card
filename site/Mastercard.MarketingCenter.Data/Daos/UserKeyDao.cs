﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Daos
{
    public class UserKeyDao : Dao<UserKey, int>
    {
        public UserKeyDao(MarketingCenterDbContext context) : base(context)
        {
        }

        public override UserKey GetById(int id)
        {
            return Entities.Where(x => x.UserKeyID == id).FirstOrDefault();
        }
    }
}