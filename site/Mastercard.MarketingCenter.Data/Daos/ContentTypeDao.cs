﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Daos
{
    public class ContentTypeDao : Dao<ContentType, string>
    {
        public ContentTypeDao(MarketingCenterDbContext context) : base(context)
        {
        }

        public override ContentType GetById(string id)
        {
            return Entities.AsNoTracking() //ContentType should never be updated by the application, do not track
                           .Include(ct => ct.ContentTypeFieldDefinitions.Select(ctfd => ctfd.FieldDefinition.FieldType))
                           .Include(ct => ct.ContentTypeClientEvents)
                           .FirstOrDefault(ct => ct.ContentTypeId == id);
        }
    }
}