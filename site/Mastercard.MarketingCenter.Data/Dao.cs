﻿using System;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public abstract class Dao<TEntity, TId> : IDao<TEntity, TId> where TEntity : class
    {
        private readonly MarketingCenterDbContext _context;
        protected IDbSet<TEntity> Entities => _context.Set<TEntity>();

        protected Dao(MarketingCenterDbContext context)
        {
            _context = context;
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            return _context.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> GetAll(params string[] paths)
        {
            if (paths != null && paths.Length > 0)
            {
                foreach (var path in paths)
                {
                    _context.Set<TEntity>().Include(path);
                }
            }

            return GetAll();
        }

        public TEntity FirstOrDefault(Func<TEntity, bool> expression)
        {
            return GetAll().FirstOrDefault(expression);
        }

        public abstract TEntity GetById(TId id);

        public void Add(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
        }

        public void Delete(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
        }

        public void Refresh(TEntity entity)
        {
            _context.Entry<TEntity>(entity).Reload();
        }
    }

    public interface IDao<TEntity, in TId> where TEntity : class
    {
        IQueryable<TEntity> GetAll();
        TEntity FirstOrDefault(Func<TEntity, bool> expression);
        TEntity GetById(TId id);
        void Add(TEntity entity);
        void Delete(TEntity entity);
    }
}