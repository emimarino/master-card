﻿using Autofac;
using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Data.Modules
{
    public class CmsDataModule : Module
    {
        public CmsDataModule()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MarketingCenterDbContext>().InstancePerLifetimeScope();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();

            builder.RegisterAssemblyTypes(typeof(ContentType).Assembly)
                .AsClosedTypesOf(typeof(Dao<,>)).AsSelf();
        }
    }
}