﻿using Autofac;
using Mastercard.MarketingCenter.Data.Interfaces;
using Mastercard.MarketingCenter.Data.Interfaces.GDP;
using Mastercard.MarketingCenter.Data.Repositories;
using Mastercard.MarketingCenter.Data.Repositories.GDP;
using Mastercard.MarketingCenter.Data.Repositories.GDP.Authorization;
using StackExchange.Profiling;
using StackExchange.Profiling.Data;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Mastercard.MarketingCenter.Data.Modules
{
    public class PersistenceModule : Module
    {
        private readonly bool _profiledSqlConnenction = false;

        public PersistenceModule(bool profiledSqlConnenction)
        {
            _profiledSqlConnenction = profiledSqlConnenction;
        }

        protected override void Load(ContainerBuilder builder)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["MasterCardMarketingCenter"].ConnectionString;

            builder.Register(c =>
            {
                var connection = new SqlConnection(connectionString);
                connection.Open();

                return _profiledSqlConnenction ? new ProfiledDbConnection(connection, MiniProfiler.Current) as IDbConnection : connection;
            }).As<IDbConnection>().InstancePerLifetimeScope();

            builder.RegisterType<MarketingCenterDbContext>().InstancePerLifetimeScope();

            builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<ContentCommentRepository>();
            builder.RegisterType<ContentItemRepository>();
            builder.RegisterType<RegionRepository>();
            builder.RegisterType<PendingRegistrationRepository>().As<IPendingRegistrationRepository>();
            builder.RegisterType<CmsRoleAccessRepository>();
            builder.RegisterType<IssuerRepository>().As<IIssuerRepository>();
            builder.RegisterType<ContentReviewRepository>().As<IContentReviewRepository>();
            builder.RegisterType<TagHitRepository>();
            builder.RegisterType<UserKeyRepository>().As<IUserKeyRepository>();
            builder.RegisterType<MigratedContentRepository>();
            builder.RegisterType<ProcessorRepository>();
            builder.RegisterType<TagRepository>();
            builder.RegisterType<VanityUrlRepository>();
            builder.RegisterType<IssuerRepository>().As<IIssuerRepository>();
            builder.RegisterType<ImportedIcaRepository>().As<IImportedIcaRepository>();
            builder.RegisterType<MailSentRepository>();
            builder.RegisterType<IssuerUnmatchedRepository>();
            builder.RegisterType<AuthorizationRepository>().As<IAuthorizationRepository>();
            builder.RegisterType<WebRoleAccessRepository>();
            builder.RegisterType<UserConsentRepository>().As<IUserConsentRepository>();
            builder.RegisterType<GdpRequestRepository>().As<IGdpRequestRepository>();
            builder.RegisterType<GdpExternalRepository>().As<IGdpExternalRepository>();
            builder.RegisterType<ConsentManagementRepository>().As<IConsentManagementRepository>();
            builder.RegisterType<ConsentFileDataRepository>().As<IConsentFileDataRepository>();
            builder.RegisterType<ReportRepository>();
            builder.RegisterType<UserSubscriptionRepository>();
            builder.RegisterType<TriggerMarketingRepository>();
            builder.RegisterType<ClonesRepository>().As<IClonesRepository>();
            builder.RegisterType<BrokenContentLinkRepository>();
            builder.RegisterType<AssetRepository>();
            builder.RegisterType<IconRepository>();
            builder.RegisterType<MostPopularRepository>().As<IMostPopularRepository>();
            builder.RegisterType<CampaignEventRepository>().As<ICampaignEventRepository>();
            builder.RegisterType<CampaignCategoryRepository>().As<ICampaignCategoryRepository>();
            builder.RegisterType<DownloadCalendarKeyRepository>().As<IDownloadCalendarKeyRepository>();
            builder.RegisterType<ErrorMessageRepository>().As<IErrorMessageRepository>();
            builder.RegisterType<GdprAuthorizationApiService>().As<IGdprAuthorizationApiService>();
            builder.RegisterType<MailDispatcherRepository>();
            builder.RegisterType<SiteTrackingRepository>().As<ISiteTrackingRepository>();
            builder.RegisterType<AnonymousTokenRepository>();
            builder.RegisterType<SegmentationRepository>().As<ISegmentationRepository>();
            builder.RegisterType<OfferRepository>().As<IOfferRepository>();
            builder.RegisterType<EventLocationRepository>().As<IEventLocationRepository>();
            builder.RegisterType<SearchRepository>().As<ISearchRepository>();
            builder.RegisterType<CountryRepository>().As<ICountryRepository>();
            builder.RegisterType<LanguageRepository>().As<ILanguageRepository>();
            builder.RegisterType<UploadActivityRepository>().As<IUploadActivityRepository>();
            builder.RegisterType<SalesforceRepository>().As<ISalesforceRepository>();
            builder.RegisterType<FileRepository>().As<IFileRepository>();
        }
    }
}