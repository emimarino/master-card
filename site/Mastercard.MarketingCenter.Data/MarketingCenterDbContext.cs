﻿using CodeFirstStoreFunctions;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Metadata;
using Mastercard.MarketingCenter.Domo.Domain;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Mastercard.MarketingCenter.Data
{
    public class MarketingCenterDbContext : DbContext
    {
        public ConnectionStateAction ConnectionStateOpenAction;
        public DbSet<Aspnet_Membership> Aspnet_Membership { get; set; }
        public DbSet<Aspnet_Users> Aspnet_Users { get; set; }
        public DbSet<AnonymousTokenData> AnonymousTokenData { get; set; }
        public DbSet<AssessmentReport> AssessmentReports { get; set; }
        public DbSet<Asset> Assets { get; set; }
        public DbSet<AssetAttachment> AssetAttachments { get; set; }
        public DbSet<AssetBrowseAllMaterialTag> AssetBrowseAllMaterialTags { get; set; }
        public DbSet<AssetFullWidth> AssetFullWidths { get; set; }
        public DbSet<AssetFullWidthBrowseAllMaterialTag> AssetFullWidthBrowseAllMaterialTags { get; set; }
        public DbSet<AssetFullWidthRelatedProgram> AssetFullWidthRelatedPrograms { get; set; }
        public DbSet<AssetRelatedProgram> AssetRelatedPrograms { get; set; }
        public DbSet<BrokenContentLink> BrokenContentLinks { get; set; }
        public DbSet<CampaignEvent> CampaignEvents { get; set; }
        public DbSet<CampaignCategory> CampaignCategories { get; set; }
        public DbSet<CardExclusivity> CardExclusivities { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<CloneSourceNotification> CloneSourceNotification { get; set; }
        public DbSet<CmsRoleAccess> CmsRoleAccesses { get; set; }
        public DbSet<ConsentFileData> ConsentFileData { get; set; }
        public DbSet<ContentAboutToExpire> ContentAboutToExpire { get; set; }
        public DbSet<ContentComment> ContentComment { get; set; }
        public DbSet<ContentItemBase> ContentItems { get; set; }
        public DbSet<ContentItemCloneToRegion> ContentItemCloneToRegions { get; set; }
        public DbSet<ContentItemCrossBorderRegion> ContentItemCrossBorderRegions { get; set; }
        public DbSet<ContentItemFeatureLocation> ContentItemFeatureLocations { get; set; }
        public DbSet<ContentItemRelatedItem> ContentItemRelatedItems { get; set; }
        public DbSet<ContentItemSegmentation> ContentItemSegmentations { get; set; }
        public DbSet<ContentItemTriggerMarketing> ContentItemTriggerMarketings { get; set; }
        public DbSet<ContentItemUserSubscription> ContentItemUserSubscriptions { get; set; }
        public DbSet<ContentItemVersion> ContentItemVersion { get; set; }
        public DbSet<UploadActivity> UploadActivities { get; set; }
        public DbSet<ContentType> ContentTypes { get; set; }
        public DbSet<ContentTypeClientEvent> ContentTypeClientEvents { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<DeletedOrphanFile> DeletedOrphanFiles { get; set; }
        public DbSet<DomainBlacklist> DomainBlacklists { get; set; }
        public DbSet<DomoActivityApi> DomoActivityApis { get; set; }
        public DbSet<DomoDownloadsApi> DomoDownloadsApis { get; set; }
        public DbSet<DownloadableAsset> DownloadableAssets { get; set; }
        public DbSet<DownloadableAssetAttachment> DownloadableAssetAttachments { get; set; }
        public DbSet<DownloadableAssetBrowseAllMaterialTag> DownloadableAssetBrowseAllMaterialTags { get; set; }
        public DbSet<DownloadableAssetEstimatedDistribution> DownloadableAssetEstimatedDistributions { get; set; }
        public DbSet<DownloadableAssetRelatedProgram> DownloadableAssetRelatedPrograms { get; set; }
        public DbSet<DownloadCalendarKey> DownloadCalendarKeys { get; set; }
        public DbSet<ElectronicDelivery> ElectronicDeliveries { get; set; }
        public DbSet<ElectronicDeliveryCreateData> ElectronicDeliveryCreateDatas { get; set; }
        public DbSet<ErrorMessage> ErrorMessage { get; set; }
        public DbSet<EventLocation> EventLocations { get; set; }
        public DbSet<EventLocationPositionHierarchy> EventLocationPositionHierarchies { get; set; }
        public DbSet<ExpirationReviewState> ExpirationReviewStates { get; set; }
        public DbSet<FeatureLocation> FeatureLocations { get; set; }
        public DbSet<FieldDefinition> FieldDefinitions { get; set; }
        public DbSet<FieldGroup> FieldGroups { get; set; }
        public DbSet<FieldType> FieldTypes { get; set; }
        public DbSet<FileReviewProcess> FileReviewProcesses { get; set; }
        public DbSet<GdpCorrelation> GdpCorrelation { get; set; }
        public DbSet<GdpRequest> GdpRequest { get; set; }
        public DbSet<GdpResponse> GdpResponse { get; set; }
        public DbSet<GdprFollowUpStatus> GdprFollowUpStatuses { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Icon> Icons { get; set; }
        public DbSet<ImportedIca> ImportedIcas { get; set; }
        public DbSet<ImportedOffer> ImportedOffers { get; set; }
        public DbSet<Issuer> Issuers { get; set; }
        public DbSet<IssuerSegmentation> IssuerSegmentations { get; set; }
        public DbSet<IssuerUnmatched> IssuerUnmatcheds { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<List> Lists { get; set; }
        public DbSet<ListClientEvent> ListClientEvents { get; set; }
        public DbSet<LoginToken> LoginTokens { get; set; }
        public DbSet<LoginTracking> LoginTrackings { get; set; }
        public DbSet<MailDispatcher> MailDispatchers { get; set; }
        public DbSet<MailSent> MailSents { get; set; }
        public DbSet<MasterICA> MasterICAs { get; set; }
        public DbSet<MetricHeatmap> MetricHeatmaps { get; set; }
        public DbSet<MigratedContentId> MigratedContentIds { get; set; }
        public DbSet<MostPopularActivity> MostPopularActivities { get; set; }
        public DbSet<MostPopularProcess> MostPopularProcesses { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<OfferAttachment> OfferAttachments { get; set; }
        public DbSet<OfferSecondaryImage> OfferSecondaryImages { get; set; }
        public DbSet<OfferType> OfferTypes { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderableAsset> OrderableAssets { get; set; }
        public DbSet<OrderableAssetAttachment> OrderableAssetAttachments { get; set; }
        public DbSet<OrderableAssetBrowseAllMaterialTag> OrderableAssetBrowseAllMaterialTags { get; set; }
        public DbSet<OrderableAssetRelatedProgram> OrderableAssetRelatedPrograms { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<PendingRegistration> PendingRegistrations { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<PricelessOffersProcess> PricelessOffersProcesses { get; set; }
        public DbSet<PricingSchedule> PricingSchedules { get; set; }
        public DbSet<Processor> Processors { get; set; }
        public DbSet<Program> Programs { get; set; }
        public DbSet<ProgramAttachment> ProgramAttachments { get; set; }
        public DbSet<ProgramBrowseAllMaterialTag> ProgramBrowseAllMaterialTags { get; set; }
        public DbSet<ContentItemCampaignEvent> ContentItemCampaignEvents { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<ReviewedFile> ReviewedFiles { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RolePermissionRegion> RolePermissionRegions { get; set; }
        public DbSet<RoleRegion> RoleRegions { get; set; }
        public DbSet<SalesforceAccessToken> SalesforceAccessTokens { get; set; }
        public DbSet<SalesforceActivityReport> SalesforceActivityReports { get; set; }
        public DbSet<SalesforceActivityType> SalesforceActivityTypes { get; set; }
        public DbSet<SalesforceTask> SalesforceTasks { get; set; }
        public DbSet<SalesforceTaskStatus> SalesforceTaskStatuses { get; set; }
        public DbSet<SearchActivity> SearchActivities { get; set; }
        public DbSet<SearchTerm> SearchTerms { get; set; }
        public DbSet<SearchTermSegmentation> SearchTermSegmentations { get; set; }
        public DbSet<Segmentation> Segmentations { get; set; }
        public DbSet<SegmentationTranslatedContent> SegmentationTranslatedContents { get; set; }
        public DbSet<ShareKey> ShareKeys { get; set; }
        public DbSet<SiteTracking> SiteTracking { get; set; }
        public DbSet<SiteVisit> SiteVisits { get; set; }
        public DbSet<Slam.Cms.Data.Status> Statuses { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<TagCategory> TagCategories { get; set; }
        public DbSet<TagHierarchyPosition> TagHierarchyPositions { get; set; }
        public DbSet<TagHit> TagHits { get; set; }
        public DbSet<TagTranslatedContent> TagTranslatedContents { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserConsent> UserConsents { get; set; }
        public DbSet<UserKey> UserKeys { get; set; }
        public DbSet<UserRoleRegion> UserRoleRegions { get; set; }
        public DbSet<UserSubscription> UserSubscriptions { get; set; }
        public DbSet<UserSubscriptionFrequency> UserSubscriptionFrequencies { get; set; }
        public DbSet<VanityUrl> VanityUrls { get; set; }
        public DbSet<ViewSiteTrackingByContext> ViewSiteTrackingByContext { get; set; }
        public DbSet<Webinar> Webinars { get; set; }
        public DbSet<WebRoleAccess> WebRoleAccesses { get; set; }
        public DbSet<ContentItemForCalendar> AbstractContentItemForCalendars { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<Snippet> Snippets { get; set; }
        public DbSet<MarqueeSlide> MarqueeSlides { get; set; }
        public DbSet<AssessmentCriteria> AssessmentCriterias { get; set; }
        public DbSet<Recommendation> Recommendations { get; set; }
        public DbSet<HtmlDownload> HtmlDownloads { get; set; }
        public DbSet<QuickLink> QuickLinks { get; set; }
        public DbSet<UpcomingEvent> UpcomingEvents { get; set; }
        public DbSet<YoutubeVideo> YoutubeVideos { get; set; }

        public MarketingCenterDbContext() : base("MasterCardMarketingCenter")
        {
            Configuration.LazyLoadingEnabled = false;
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 180;
            Database.Connection.StateChange += OnConnectionStateChange;
        }

        public MarketingCenterDbContext(DbConnection connection) : base(connection, false)
        {
            Configuration.LazyLoadingEnabled = false;
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 180;
            Database.Connection.StateChange += OnConnectionStateChange;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add(new FunctionsConvention("dbo", GetType()));
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<User>().HasRequired(u => u.Profile)
                                       .WithRequiredPrincipal(p => p.User);

            modelBuilder.Entity<Asset>().Map(m => m.MapInheritedProperties());
            modelBuilder.Entity<DownloadableAsset>().Map(m => m.MapInheritedProperties());
            modelBuilder.Entity<Program>().Map(m => m.MapInheritedProperties());
            modelBuilder.Entity<Page>().Map(m => m.MapInheritedProperties());

            modelBuilder.Entity<CampaignEvent>()
                        .HasMany(o => o.CampaignCategories)
                        .WithMany(c => c.CampaignEvents)
                        .Map(oc => oc.ToTable(MarketingCenterDbConstants.Tables.CampaignEventCampaignCategory).MapLeftKey("CampaignEventId").MapRightKey("CampaignCategoryId"));

            modelBuilder.Entity<FeatureLocation>()
                        .HasMany(o => o.Scopes)
                        .WithMany()
                        .Map(m => m.ToTable(MarketingCenterDbConstants.Tables.FeatureLocationScope).MapLeftKey("FeatureLocationID").MapRightKey("ContentTypeID"));
            modelBuilder.Entity<FeatureLocation>()
                        .HasMany(o => o.Users)
                        .WithMany()
                        .Map(m => m.ToTable(MarketingCenterDbConstants.Tables.UserFeatureLocation).MapLeftKey("UserID").MapRightKey("FeatureLocationID"));

            modelBuilder.Entity<Group>()
                        .HasMany(e => e.Users)
                        .WithMany(e => e.Groups)
                        .Map(m => m.ToTable(MarketingCenterDbConstants.Tables.UserGroup).MapLeftKey("GroupID").MapRightKey("UserID"));

            modelBuilder.Entity<Issuer>()
                        .HasMany(i => i.Processors)
                        .WithMany(p => p.Issuers)
                        .Map(ip => ip.ToTable(MarketingCenterDbConstants.Tables.IssuerProcessor).MapLeftKey("IssuerId").MapRightKey("ProcessorId"));
            modelBuilder.Entity<Issuer>()
                        .HasMany(i => i.Users)
                        .WithOptional(u => u.Issuer);

            modelBuilder.Entity<Offer>()
                        .HasMany(o => o.CardExclusivities)
                        .WithMany(c => c.Offers)
                        .Map(oc => oc.ToTable(MarketingCenterDbConstants.Tables.ContentItemCardExclusivity).MapLeftKey("ContentItemId").MapRightKey("CardExclusivityId"));
            modelBuilder.Entity<Offer>()
                        .HasMany(o => o.Categories)
                        .WithMany(c => c.Offers)
                        .Map(oc => oc.ToTable(MarketingCenterDbConstants.Tables.ContentItemCategory).MapLeftKey("ContentItemId").MapRightKey("CategoryId"));
            modelBuilder.Entity<Offer>()
                        .HasMany(o => o.EventLocations)
                        .WithMany(c => c.Offers)
                        .Map(oc => oc.ToTable(MarketingCenterDbConstants.Tables.ContentItemEventLocation).MapLeftKey("ContentItemId").MapRightKey("EventLocationId"));

            modelBuilder.Entity<Tag>()
                        .HasMany(o => o.ContentTypes)
                        .WithMany()
                        .Map(m => m.ToTable(MarketingCenterDbConstants.Tables.TagScope).MapLeftKey("TagId").MapRightKey("ContentTypeId"));
            modelBuilder.Entity<Tag>()
                        .HasMany(o => o.FeaturingUsers)
                        .WithMany()
                        .Map(m => m.ToTable(MarketingCenterDbConstants.Tables.UserToFeaturableTag).MapLeftKey("TagId").MapRightKey("UserId"));
            modelBuilder.Entity<Tag>()
                        .HasMany(t => t.ContentItems)
                        .WithMany(ci => ci.Tags)
                        .Map(tci => tci.ToTable(MarketingCenterDbConstants.Tables.ContentItemTag).MapLeftKey("TagID").MapRightKey("ContentItemId"));
        }

        private void OnConnectionStateChange(object sender, StateChangeEventArgs e)
        {
            if (e.CurrentState == ConnectionState.Open && ConnectionStateOpenAction?.Action != null)
            {
                ConnectionStateOpenAction.Action();
            }
        }
    }
}