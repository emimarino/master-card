﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class Recommendation
    {
        [Key]
        public string ContentItemId { get; set; }
        [Column("DisplayName")]
        public string Title { get; set; }
        public string ShortDescription { get; set; }
    }
}
