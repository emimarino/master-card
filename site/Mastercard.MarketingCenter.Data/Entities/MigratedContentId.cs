﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.MigratedContentId)]
    public class MigratedContentId
    {
        [Key, Column(Order = 0)]
        public string Id { get; set; }
        [Key, Column(Order = 1)]
        public string MigratedId { get; set; }
    }
}