﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.UserSubscription)]
    public class UserSubscription
    {
        [Key]
        public int UserSubscriptionId { get; set; }
        public int UserId { get; set; }
        public int UserSubscriptionFrequencyId { get; set; }
        public DateTime SavedDate { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        [ForeignKey("UserSubscriptionFrequencyId")]
        public virtual UserSubscriptionFrequency UserSubscriptionFrequency { get; set; }
    }
}