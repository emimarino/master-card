﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class IssuerData
    {
        public string IssuerId { get; set; }
        public string BillingId { get; set; }
        public bool? MatchConfirmed { get; set; }
        public string Title { get; set; }
        public string AudienceSegments { get; set; }
        public string Processor { get; set; }
        public int UserCount { get; set; }
        public DateTime ModifiedData { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public string Cid { get; set; }
        public string HeatmapData { get; set; }

        public bool HasMatch
        {
            get
            {
                return !string.IsNullOrEmpty(Cid) || MatchConfirmed.HasValue;
            }
        }
    }
}