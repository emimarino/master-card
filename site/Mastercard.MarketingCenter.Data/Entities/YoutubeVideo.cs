﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class YoutubeVideo
    {
        [Key]
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public int OrderNumber { get; set; }
        public string StaticVideoImage { get; set; }
    }
}
