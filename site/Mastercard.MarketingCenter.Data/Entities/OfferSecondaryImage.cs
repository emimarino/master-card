﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.OfferSecondaryImage)]
    public class OfferSecondaryImage : SecondaryImage
    {
    }
}