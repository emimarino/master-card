﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.TagCategory)]
    public class TagCategory
    {
        [Key]
        public string TagCategoryId { get; set; }
        public string Title { get; set; }
        public int DisplayOrder { get; set; }
        public string Featurable { get; set; }
        public string ListLocation { get; set; }
        public bool IsRelatable { get; set; }
        public string TagBrowser { get; set; }
        public virtual ICollection<TagHierarchyPosition> TagHierarchyPositions { get; set; }
    }
}