﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.IssuerSegmentation)]
    public class IssuerSegmentation
    {
        [Key, Column(Order = 0)]
        public string IssuerId { get; set; }
        [Key, Column(Order = 4)]
        public string SegmentationId { get; set; }

        public virtual Issuer Issuer { get; set; }
        public virtual Segmentation Segmentation { get; set; }
    }
}