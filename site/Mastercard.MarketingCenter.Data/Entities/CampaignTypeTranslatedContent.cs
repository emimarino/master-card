﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.CampaignCategoryTranslatedContent)]
    public class CampaignCategoryTranslatedContent
    {
        [Key]
        public int Id { get; set; }
        public string CampaignCategoryId { get; set; }
        public string Title { get; set; }

        private string _languageCode;
        public string LanguageCode
        {
            get { return _languageCode; }
            set { _languageCode = value.Trim(); }
        }
    }
}