﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ContentItemTriggerMarketing)]
    public class ContentItemTriggerMarketing
    {
        [Key]
        public long ContentItemTriggerMarketingId { get; set; }
        public string ContentItemId { get; set; }
        public bool Notify { get; set; }
        public bool Featured { get; set; }
        public string Reasons { get; set; }
        public int StatusId { get; set; }
        public bool IsNew { get; set; }
        public DateTime SavedDate { get; set; }
        public DateTime? SentDailyDate { get; set; }
        public DateTime? SentWeeklyDate { get; set; }

        [ForeignKey("ContentItemId")]
        public virtual ContentItemBase ContentItem { get; set; }
    }
}