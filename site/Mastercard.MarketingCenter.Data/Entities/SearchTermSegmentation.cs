﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.SearchTermSegmentation)]
    public class SearchTermSegmentation
    {
        [Key, Column(Order = 0)]
        public string SearchTermId { get; set; }
        [Key, Column(Order = 1)]
        public string SegmentationId { get; set; }

        public virtual Segmentation Segmentation { get; set; }
    }
}