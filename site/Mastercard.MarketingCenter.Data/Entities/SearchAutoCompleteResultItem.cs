﻿namespace Mastercard.MarketingCenter.Data.Entities
{
    public class SearchAutoCompleteResultItem
    {
        public string id { get; set; }
        public string value { get; set; }
        public string info { get; set; }
    }
}