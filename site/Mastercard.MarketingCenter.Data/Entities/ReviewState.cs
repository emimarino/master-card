﻿using System.ComponentModel.DataAnnotations;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class ExpirationReviewState
    {
        [Key]
        public int ReviewStateId { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
    }
}