﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ConsentFileData)]
    public class ConsentFileData
    {
        [Key]
        public int ConsentFileDataId { get; set; }
        public Guid Uuid { get; set; }
        public string ServiceFunctionCode { get; set; }
        public string UseCategoryCode { get; set; }
        public string UseCategoryValue { get; internal set; }
        public string UseCategoryDescription { get; set; }
        public string DocumentType { get; set; }
        public string ConsentData { get; set; }
        public string CurrentVersion { get; set; }
        public string Status { get; set; }
        public string Locale { get; set; }
        public string ReacceptedVersion { get; set; }
        public string NotificationVersion { get; set; }
        public bool ReacceptanceRequired { get; set; }
        public bool EmailNotificationRequired { get; set; }
        public DateTime PublishedDate { get; set; }

        public bool IsExpirable
        {
            get
            {
                return DocumentType != "consentlanguagetext";
            }
        }
    }
}