﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.BrokenContentLink)]
    public class BrokenContentLink
    {
        [Key]
        public int BrokenContentLinkId { get; set; }
        public string Title { get; set; }
        public string BrokenContentStatus { get; set; }
        public string BrokenLinkReference { get; set; }
        public string BrokenLinkText { get; set; }
        public string FrontEndUrl { get; set; }
        public string BackEndUrl { get; set; }
        public string ReferrerUrl { get; set; }
        public string SourceItemId { get; set; }
        public string ContentType { get; set; }
        public string ContentTypeId { get; set; }
        public DateTime LastRequested { get; set; }
        public string RegionId { get; set; }
    }
}