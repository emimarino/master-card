﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ImportedIca)]
    public class ImportedIca
    {
        [Key]
        public int ImportedIcaId { get; set; }
        public string Cid { get; set; }
        public string LegalName { get; set; }
        public string HqAddress { get; set; }
        public string HqCityName { get; set; }
        public string HqProvinceName { get; set; }
        public string HqCountryCode { get; set; }
        public string HqCountryName { get; set; }
        public string StatusCode { get; set; }
        public string AffiliateBin { get; set; }

        [ForeignKey("Cid")]
        public virtual MetricHeatmap MetricHeatmap { get; set; }

        [NotMapped]
        public string HeatmapData
        {
            get
            {
                return MetricHeatmap == null ? "No" : "Yes";
            }
        }
    }
}