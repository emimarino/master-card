﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.UserConsent)]
    public class UserConsent
    {
        [Key]
        public int UserConsentId { get; set; }
        public int UserId { get; set; }
        public DateTime ConsentDate { get; set; }
        public int ConsentFileDataId { get; set; }

        [ForeignKey("ConsentFileDataId")]
        public virtual ConsentFileData ConsentFileData { get; set; }
    }
}