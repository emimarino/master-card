﻿using Slam.Cms.Data;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class Link : ContentItem, IContentItemWithTitle
    {
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string ThumbnailImage { get; set; }
        public string ShowInCrossSellBox { get; set; }
        public string PriorityLevel { get; set; }
        public bool? ShowInTagBrowser { get; set; }
        public string AssetType { get; set; }
    }
}