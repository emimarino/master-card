﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.DownloadableAssetEstimatedDistribution)]
    public class DownloadableAssetEstimatedDistribution
    {
        [Key]
        public int Id { get; set; }
        public string ContentItemId { get; set; }
        public Guid? ElectronicDeliveryId { get; set; }
        public int EstimatedDistribution { get; set; }
        public int? UserId { get; set; }
        public DateTime DownloadDate { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [ForeignKey("ElectronicDeliveryId")]
        public virtual ElectronicDelivery ElectronicDelivery { get; set; }
    }
}