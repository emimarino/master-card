﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.PricelessOffersProcess)]
    public class PricelessOffersProcess
    {
        [Key]
        public long Id { get; set; }
        public DateTime ProcessStartDate { get; set; }
        public DateTime ProcessEndDate { get; set; }
        public int OffersCreated { get; set; }
        public int OffersUpdated { get; set; }
        public int OffersDeleted { get; set; }
        public int OffersUnmatched { get; set; }
        public string OffersCreatedData { get; set; }
        public string OffersUpdatedData { get; set; }
        public string OffersDeletedData { get; set; }
        public string OffersUnmatchedData { get; set; }
    }
}