﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class SearchResult
    {
        public string Title { get; set; }
        public string DisplayUrl { get; set; }        
        public bool? IsLandingPageResult { get; set; }
        public bool? IsContentIndex { get; set; }
        public int Order { get; set; }
        public int Visits { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        private string _description;

        public string Description {
            get
            {
                return _description.Replace("||HTML||", "");
            }
            set
            {
                _description = value;
            }
        }

        public int DaysSinceCreated
        {
            get
            {
                return (int)DateTime.Now.Subtract(CreatedDate).TotalDays;
            }
        }

        public bool New
        {
            get
            {
                return DaysSinceCreated <= 90;
            }
        }

        public int DaysSinceUpdated
        {
            get
            {
                return (int)DateTime.Now.Subtract(ModifiedDate).TotalDays;
            }
        }

        public bool Updated
        {
            get
            {
                return DaysSinceUpdated <= 90;
            }
        }
    }
}