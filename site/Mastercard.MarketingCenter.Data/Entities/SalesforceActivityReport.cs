﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.SalesforceActivityReport)]
    public class SalesforceActivityReport
    {
        [Key]
        public int SalesforceActivityReportId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int Sent { get; set; }
        public int Failed { get; set; }
        public bool IsSuccessed { get; set; }
        public string LoggingEventId { get; set; }
        public int? SalesforceTokenId { get; set; }

        [ForeignKey("SalesforceTokenId")]
        public SalesforceAccessToken SalesforceAccessToken { get; set; }
    }
}