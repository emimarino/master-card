﻿using Mastercard.MarketingCenter.Common.Models;
using System;
using System.ComponentModel.DataAnnotations;


namespace Mastercard.MarketingCenter.Data.Entities
{
    public class AnonymousTokenData : AnonymousUserProxy
    {
        [Key]
        public long AnonymousTokenId { get; set; }
        public Guid AnonymousToken { get; set; }
    }
}
