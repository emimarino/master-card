﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.AssessmentReport)]
    public class AssessmentReport
    {
        [Key]
        public string Cid { get; set; }
        public string Activation { get; set; }
        public string FraudPrevention { get; set; }
        public string Authorization { get; set; }
    }
}