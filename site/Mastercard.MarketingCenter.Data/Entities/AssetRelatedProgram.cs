﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.AssetRelatedProgram)]
    public class AssetRelatedProgram
    {
        [Key, Column(Order = 0)]
        public string AssetContentItemId { get; set; }
        [Key, Column(Order = 1)]
        public string ProgramContentItemId { get; set; }
    }
}