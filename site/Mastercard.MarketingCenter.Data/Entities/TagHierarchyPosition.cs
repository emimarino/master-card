﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.TagHierarchyPosition)]
    public class TagHierarchyPosition
    {
        [Key]
        public int PositionId { get; set; }
        public int? ParentPositionId { get; set; }
        public string TagId { get; set; }
        public string TagCategoryId { get; set; }
        public int SortOrder { get; set; }

        [ForeignKey("ParentPositionId")]
        public virtual TagHierarchyPosition Parent { get; set; }
        [ForeignKey("TagId")]
        public virtual Tag Tag { get; set; }
        [ForeignKey("TagCategoryId")]
        public virtual TagCategory TagCategory { get; set; }
    }
}