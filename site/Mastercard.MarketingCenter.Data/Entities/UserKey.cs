﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.UserKey)]
    public class UserKey
    {
        [Key]
        public int UserKeyID { get; set; }
        public int UserID { get; set; }
        public int UserKeyStatusID { get; set; }
        public string Key { get; set; }
        public DateTime DateKeyGenerated { get; set; }
        public virtual User User { get; set; }
    }
}