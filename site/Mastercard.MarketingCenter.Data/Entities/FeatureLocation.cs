﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.FeatureLocation)]
    public class FeatureLocation
    {
        [Key]
        public string FeatureLocationId { get; set; }
        public string Title { get; set; }

        public virtual ICollection<ContentType> Scopes { get; set; }

        public virtual ICollection<ContentItemFeatureLocation> ContentItemFeatureLocations { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}