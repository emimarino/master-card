﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.Offer)]
    public class Offer
    {
        [Key]
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public int BusinessOwnerID { get; set; }
        public bool Evergreen { get; set; }
        public DateTime? ValidityPeriodFromDate { get; set; }
        public DateTime? ValidityPeriodToDate { get; set; }
        public DateTime? EventDate { get; set; }
        public DateTime? BookByDate { get; set; }
        public string MainImage { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string TermsAndConditions { get; set; }
        public bool PreviewFiles { get; set; }
        public string DownloadZipFile { get; set; }
        public string PricelessOfferId { get; set; }
        public string LogoImage { get; set; }
        public string OfferTypeId { get; set; }
        public string ProductUrl { get; set; }

        [ForeignKey("ContentItemId")]
        public ContentItemBase ContentItem { get; set; }

        [ForeignKey("BusinessOwnerID")]
        public virtual User BusinessOwner { get; set; }

        [ForeignKey("OfferTypeId")]
        public virtual OfferType OfferType { get; set; }

        [ForeignKey("ContentItemId")]
        public virtual ICollection<ContentItemCrossBorderRegion> CrossBorderRegions { get; set; }

        [ForeignKey("ContentItemId")]
        public virtual ICollection<OfferAttachment> Attachments { get; set; }

        [ForeignKey("ContentItemId")]
        public virtual ICollection<OfferSecondaryImage> SecondaryImages { get; set; }

        [ForeignKey("ContentItemId")]
        public virtual ICollection<ContentItemRelatedItem> RelatedItems { get; set; }

        [ForeignKey("ContentItemId")]
        public virtual ICollection<ContentItemRegionVisibility> RegionVisibilities { get; set; }
        public virtual ICollection<EventLocation> EventLocations { get; set; }
        public virtual ICollection<CardExclusivity> CardExclusivities { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
    }
}