﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.SearchTerm)]
    public class SearchTerm
    {
        [Key]
        public string SearchTermID { get; set; }
        public string Title { get; set; }
        public string Synonyms { get; set; }
        public string RestrictToSpecialAudience { get; set; }
        public string Copy { get; set; }
        public string Image { get; set; }
        public string Url { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public string RegionId { get; set; }
        private string _searchLanguage;
        public string SearchLanguage
        {
            get { return _searchLanguage; }
            set { _searchLanguage = value.Trim(); }
        }
        public virtual ICollection<SearchTermSegmentation> SearchTermSegmentations { get; set; }
    }
}