﻿namespace Mastercard.MarketingCenter.Data.Entities
{
    public enum IdentityType
    {
        User = 1,
        Group = 2
    }
}