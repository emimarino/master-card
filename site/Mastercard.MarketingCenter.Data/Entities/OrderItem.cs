﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class OrderItem
    {
        [Key]
        public int OrderItemId { get; set; }
        public string ItemId { get; set; }
        public int OrderId { get; set; }
        public int? ShippingInformationID { get; set; }
        public string ItemName { get; set; }
        public decimal ItemPrice { get; set; }
        public int ItemQuantity { get; set; }
        public string ProofPDF { get; set; }
        public string SKU { get; set; }
        public bool Customizations { get; set; }
        public int SubOrderID { get; set; }
        public bool VDP { get; set; }
        public string ApproverEmailAddress { get; set; }
        public DateTime Modified { get; set; }
        public int EstimatedDistribution { get; set; }
        public bool HasAdvisorsTag { get; set; }
        public bool ElectronicDelivery { get; set; }

        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }
    }
}