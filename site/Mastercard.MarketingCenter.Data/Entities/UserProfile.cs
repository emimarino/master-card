﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.UserProfile)]
    public class UserProfile
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string IssuerName { get; set; }
        public string Processor { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public bool FeedbackRequestDisplayed { get; set; }
        public bool ContentPreferencesMessageDisplayed { get; set; }
        public bool NeverFrecuencyMessageDisplayed { get; set; }
        public DateTime? NeverFrecuencyMessageDisplayedDateTime { get; set; }
        public int? GdprFollowUpStatusId { get; set; }
        public virtual User User { get; set; }
    }
}