﻿namespace Mastercard.MarketingCenter.Data.Entities
{
    public class ViewAllFiles
    {
        public string TableId { get; set; }
        public string Table { get; set; }
        public string Column { get; set; }
        public string Filename { get; set; }
    }
}