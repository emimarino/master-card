﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class ContentAboutToExpire
    {
        [Key]
        public string ContentItemId { get; set; }
        public string ContentTypeId { get; set; }
        public string RegionId { get; set; }
        public string Title { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int? BusinessOwnerId { get; set; }

        [ForeignKey("BusinessOwnerId")]
        public virtual User BusinessOwner { get; set; }
    }
}