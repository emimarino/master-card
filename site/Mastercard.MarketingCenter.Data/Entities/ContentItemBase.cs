﻿using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ContentItem)]
    public class ContentItemBase
    {
        [Key]
        public string ContentItemId { get; set; }
        public string PrimaryContentItemId { get; set; }

        public int StatusID { get; set; }

        public DateTime CreatedDate { get; set; }
        public int CreatedByUserId { get; set; }

        public DateTime ModifiedDate { get; set; }
        public int ModifiedByUserId { get; set; }

        public string ContentTypeId { get; set; }
        private string _frontEndUrl;
        public string FrontEndUrl { get { return _frontEndUrl ?? string.Empty; } set { _frontEndUrl = value; } }

        public int ReviewStateId { get; set; }
        [ForeignKey("ReviewStateId")]
        public ExpirationReviewState ReviewState { get; set; }


        public virtual ICollection<ContentItemFeatureLocation> ContentItemFeatureLocations { get; set; }

        private string _regionId;
        public string RegionId { get { return _regionId?.Trim() ?? string.Empty; } set { _regionId = value; } }
        public DateTime? ExpirationDate { get; set; }

        [Required]
        [ForeignKey("ModifiedByUserId")]
        public virtual User ModifiedByUser { get; set; }
        [ForeignKey("CreatedByUserId")]
        public virtual User CreatedByUser { get; set; }
        [ForeignKey("StatusID")]
        public virtual Status Status { get; set; }
        [ForeignKey("ContentTypeId")]
        public virtual ContentType ContentType { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
        public virtual Region Region { get; set; }
    }
}