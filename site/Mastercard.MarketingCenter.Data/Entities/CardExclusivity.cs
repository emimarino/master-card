﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.CardExclusivity)]
    public class CardExclusivity
    {
        [Key]
        public string CardExclusivityId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string PricelessCardExclusivity { get; set; }

        public virtual ICollection<Offer> Offers { get; set; }
    }
}