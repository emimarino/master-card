﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.FieldDefinition)]
    public class FieldDefinition
    {
        [Key]
        public int FieldDefinitionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FieldTypeCode { get; set; }
        public int Length { get; set; }
        public bool Required { get; set; }
        public string Data { get; set; }
        public string DefaultValue { get; set; }
        public int FieldGroupId { get; set; }
        public string CustomSettings { get; set; }

        [ForeignKey("FieldGroupId")]
        public virtual FieldGroup FieldGroup { get; set; }
        [ForeignKey("FieldTypeCode")]
        public virtual FieldType FieldType { get; set; }
    }
}