﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public abstract class Attachment
    {
        [Key]
        public int AttachmentID { get; set; }
        public string ContentItemId { get; set; }
        public string FileUrl { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string DisplayName
        {
            get
            {
                string[] tokens = FileUrl.Split('/');
                var fileName = tokens[tokens.Length - 1];
                fileName = fileName.Substring(0, fileName.LastIndexOf('.'));
                return fileName.Replace("\"", "‘’");
            }
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string Extension
        {
            get
            {
                string[] tokens = FileUrl.Split('.');
                return tokens[tokens.Length - 1];
            }
        }
    }
}