﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class ElectronicDelivery
    {
        public Guid ElectronicDeliveryID { get; set; }
        public int? OrderItemID { get; set; }
        public string FileLocation { get; set; }
        public int DownloadCount { get; set; }
        public DateTime ExpirationDate { get; set; }

        [ForeignKey("OrderItemID")]
        public OrderItem OrderItem { get; set; }
    }
}