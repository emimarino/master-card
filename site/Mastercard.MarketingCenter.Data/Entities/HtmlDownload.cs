﻿using System.ComponentModel.DataAnnotations;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class HtmlDownload
    {
        [Key]
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Filename { get; set; }
    }
}
