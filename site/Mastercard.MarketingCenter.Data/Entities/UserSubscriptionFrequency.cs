﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.UserSubscriptionFrequency)]
    public class UserSubscriptionFrequency
    {
        [Key]
        public int UserSubscriptionFrequencyId { get; set; }
        public string Frequency { get; set; }
    }
}