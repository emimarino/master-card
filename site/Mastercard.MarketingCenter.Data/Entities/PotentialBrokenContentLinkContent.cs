﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class PotentialBrokenContentLinkContent
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string FrontEndUrl { get; set; }
        public string ContentTypeId { get; set; }
        public string ItemType { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string RegionId { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        public string Text3 { get; set; }
    }
}