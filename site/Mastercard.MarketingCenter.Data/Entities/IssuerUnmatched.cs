﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.IssuerUnmatched)]
    public class IssuerUnmatched
    {
        [Key]
        public int IssuerUnmatchedId { get; set; }
        public string Error { get; set; }
        public string Message { get; set; }
        public string IssuerID { get; set; }
        public int? ImportedIcaId { get; set; }
        public int UserId { get; set; }
        public DateTime Date { get; set; }

        [ForeignKey("IssuerID")]
        public virtual Issuer Issuer { get; set; }
        [ForeignKey("ImportedIcaId")]
        public virtual ImportedIca ImportedIca { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}