﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.Segmentation)]
    public class Segmentation
    {
        [Key]
        public string SegmentationId { get; set; }
        public string SegmentationName { get; set; }
        public string RegionId { get; set; }

        public virtual ICollection<SegmentationTranslatedContent> SegmentationTranslatedContents { get; set; }
        public virtual ICollection<IssuerSegmentation> IssuerSegmentations { get; set; }
    }
}