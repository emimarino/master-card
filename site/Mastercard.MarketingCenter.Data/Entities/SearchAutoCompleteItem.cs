﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Serializable]
    public class SearchAutoCompleteItem
    {
        private string[] mSegmentationIds;
        private string mTitle, mSearchParameter, mRestrictToSpecialAudience, mLanguage;
        private int mRank, mSortOrder;

        public string Language
        {
            get { return mLanguage; }
            set { mLanguage = value; }
        }

        public string Title
        {
            get { return mTitle; }
            set { mTitle = value; }
        }

        public string SearchParameter
        {
            get { return mSearchParameter; }
            set { mSearchParameter = value; }
        }

        public string[] SegmentationIds
        {
            get { return mSegmentationIds; }
            set { SegmentationIds = value; }
        }

        public string RestrictToSpecialAudience
        {
            get { return mRestrictToSpecialAudience; }
            set { RestrictToSpecialAudience = value; }
        }

        public int Rank
        {
            get { return mRank; }
            set { mRank = value; }
        }

        public int SortOrder
        {
            get { return mSortOrder; }
            set { mSortOrder = value; }
        }

        public bool IsSynonym { get; set; }

        public SearchAutoCompleteItem() { }

        public SearchAutoCompleteItem(string title, string searchParameter, int rank, int sortOrder, string[] segmentationIds, string restrictToSpecialAudience, string language, bool isSynonym)
        {
            mTitle = title;
            mSearchParameter = searchParameter;
            mRank = rank;
            mSortOrder = sortOrder;
            mSegmentationIds = segmentationIds;
            mRestrictToSpecialAudience = restrictToSpecialAudience;
            mLanguage = language;
            IsSynonym = isSynonym;
        }
    }
}