﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.SalesforceAccessToken)]
    public class SalesforceAccessToken
    {
        [Key]
        public int SalesforceTokenId { get; set; }
        public string AccessToken { get; set; }
        public string Scope { get; set; }
        public string InstanceUrl { get; set; }
        public string TokenId { get; set; }
        public string TokenType { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}