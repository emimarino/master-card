﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.UploadActivity)]
    public class UploadActivity
    {
        [Key]
        public int UploadActivityId { get; set; }
        public string ContentItemId { get; set; }
        public string Filename { get; set; }
        public string FileReference { get; set; }
        public decimal Size { get; set; }
        public string FieldDefinitionName { get; set; }
        public DateTime ActivityDate { get; set; }
        public int ActivityUserId { get; set; }
        public int UploadActivityActionId { get; set; }
        public int UploadActivityStatusId { get; set; }
        public DateTime? DateSaved { get; set; }

        [ForeignKey("ActivityUserId")]
        public virtual User ActivityUser { get; set; }
    }
}