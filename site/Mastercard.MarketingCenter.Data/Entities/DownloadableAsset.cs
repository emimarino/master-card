﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.DownloadableAsset)]
    public class DownloadableAsset : ContentItemForCalendar
    {
        public string ThumbnailImage { get; set; }
        public string LongDescription { get; set; }
        public string ShowInCrossSellBox { get; set; }
        public string PriorityLevel { get; set; }
        public string MainImage { get; set; }
        public string CallToAction { get; set; }
        public string BrowseAllMaterialsDisplayText { get; set; }
        public string BrowseAllMaterialsTags { get; set; }
        public string Filename { get; set; }
        public string IconID { get; set; }
        public string AssetType { get; set; }
        public bool RequestEstimatedDistribution { get; set; }
        public string OriginalContentItemId { get; set; }
        public bool NotifyWhenSourceChanges { get; set; }
        public bool AllowCloning { get; set; }
        public int? BusinessOwnerID { get; set; }
        public bool EnableFavoriting { get; set; }
        public bool SetForElectronicDelivery { get; set; }
        public string RestrictToSpecialAudience { get; set; }
        public bool PreviewFiles { get; set; }

        [ForeignKey("BusinessOwnerID")]
        public virtual User BusinessOwner { get; set; }
    }
}