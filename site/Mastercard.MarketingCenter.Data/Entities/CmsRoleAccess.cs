﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.CmsRoleAccess)]
    public class CmsRoleAccess
    {
        [Key]
        public long CmsRoleAccessId { get; set; }
        public string RoleType { get; set; }
        public string RequiredRolePermissionName { get; set; }
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Id { get; set; }
        public string QueryString { get; set; }
        public string ContentAction { get; set; }
    }
}