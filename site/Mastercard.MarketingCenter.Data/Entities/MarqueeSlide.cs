﻿using System.ComponentModel.DataAnnotations;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class MarqueeSlide
    {
        [Key]
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public string LongDescription { get; set; }
        public string MainUrl { get; set; }
        public int OrderNumber { get; set; }
        public string RestrictToSpecialAudience { get; set; }
        public string ButtonText { get; set; }
        public string InnerDescription { get; set; }
    }
}
