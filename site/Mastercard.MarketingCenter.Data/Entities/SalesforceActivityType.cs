﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.SalesforceActivityType)]
    public class SalesforceActivityType
    {
        [Key]
        public int SalesforceActivityTypeId { get; set; }
        public string Name { get; set; }
        public string FieldName { get; set; }
    }
}