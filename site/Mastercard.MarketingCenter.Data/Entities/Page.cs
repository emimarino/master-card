﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.Page)]
    public class Page : ContentItemForCalendar
    {
        [Key]
        public string Uri { get; set; }
        public string IntroCopy { get; set; }
        public string Terms { get; set; }
        public string RestrictToSpecialAudience { get; set; }
        public string HeaderTitle { get; set; }
        public string HeaderParagraph { get; set; }
        public string HeaderBackgroundImage { get; set; }
        public string HeaderAsideImage { get; set; }
        public string HeaderUrl { get; set; }
        public string BottomTitle { get; set; }
        public string BottomParagraph { get; set; }
        public string BottomBackgroundImage { get; set; }
        public string BottomAsideImage { get; set; }
        public string BottomUrl { get; set; }
        public string AsideContent { get; set; }
        public bool EnableFavoriting { get; set; }
    }
}
