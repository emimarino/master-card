﻿namespace Mastercard.MarketingCenter.Data.Entities
{
    public class Icon
    {
        public string IconID { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
    }
}