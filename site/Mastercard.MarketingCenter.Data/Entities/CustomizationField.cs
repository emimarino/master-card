﻿using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class CustomizationField
    {
        public bool Customizable { get; set; }
        public IEnumerable<string> OptionalCustomizableOptions { get; set; }
        public IEnumerable<string> RequiredCustomizableOptions { get; set; }

        public CustomizationField()
        {
            OptionalCustomizableOptions = Enumerable.Empty<string>();
            RequiredCustomizableOptions = Enumerable.Empty<string>();
        }
    }
}