﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ContentType)]
    public class ContentType
    {
        [Key]
        public string ContentTypeId { get; set; }
        public string Title { get; set; }
        public string DefaultIcon { get; set; }
        public string MatchExtension { get; set; }
        public string TableName { get; set; }
        public string FrontEndUrl { get; set; }
        public bool Clonable { get; set; }
        public bool CanCopy { get; set; }
        public bool CanPreview { get; set; }
        public bool MustExpire { get; set; }
        public bool CanExpire { get; set; }
        public bool CanComment { get; set; }
        public bool CanBePending { get; set; }
        public bool HasDownloads { get; set; }
        public bool BusinessOwnerCanRequest { get; set; }
        public bool HasCrossBorderRegions { get; set; }

        [ForeignKey("ContentTypeId")]
        public virtual ICollection<ContentTypeClientEvent> ContentTypeClientEvents { get; set; }
        [ForeignKey("ContentTypeId")]
        public virtual ICollection<ContentTypeFieldDefinition> ContentTypeFieldDefinitions { get; set; }

        public FieldDefinition GetIdField()
        {
            var idField = ContentTypeFieldDefinitions.FirstOrDefault(o => o.FieldDefinition.FieldType.IsIdentifierField());

            return idField != null ? idField.FieldDefinition : null;
        }
    }
}