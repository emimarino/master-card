﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.PricingSchedule)]
    public class PricingSchedule
    {
        [Key]
        public string PricingScheduleID { get; set; }
        public string Title { get; set; }
        public string PricingTable { get; set; }
        public string Details { get; set; }
        public decimal? MinimumPrice { get; set; }
        public decimal? DownloadPrice { get; set; }
        public bool? ContactPrinterForMore { get; set; }
        public string TableHeader { get; set; }
        public string UnitSize { get; set; }
        public decimal? UnitPrice { get; set; }

        public PricingTable GetProcessedPricingTable()
        {
            if (string.IsNullOrEmpty(PricingTable) || string.IsNullOrWhiteSpace(PricingTable))
                return null;

            var list = new List<PricingTableItem>();
            var tokens = Regex.Replace(PricingTable.Replace("<br />", "|"), @"<(.|\n)*?>", string.Empty).Split('|');

            foreach (var token in tokens)
            {
                var line = token.Split(',');
                if (line.Length == 2)
                {
                    list.Add(new PricingTableItem() { Quantity = int.Parse(line[0], new CultureInfo("en-US")), UnitPrice = decimal.Parse(line[1], new CultureInfo("en-US")) });
                }
            }
            return new PricingTable() { Items = list };
        }
    }

    public class PricingTable
    {
        public IList<PricingTableItem> Items { get; set; }
    }

    public class PricingTableItem
    {
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
    }
}