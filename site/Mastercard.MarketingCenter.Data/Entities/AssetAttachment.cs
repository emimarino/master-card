﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.AssetAttachment)]
    public class AssetAttachment : Attachment
    {
    }
}