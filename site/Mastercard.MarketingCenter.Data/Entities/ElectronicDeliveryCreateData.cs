﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class ElectronicDeliveryCreateData
    {
        [Key]
        public Guid ElectronicDeliveryID { get; set; }
        public string ContentItemId { get; set; }
        public string UserName { get; set; }
        public string Region { get; set; }
    }
}