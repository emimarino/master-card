﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class ErrorMessage
    {
        [Key, Column(Order = 0)]
        public int ListId { get; set; }
        [Key, Column(Order = 1)]
        public string EventName { get; set; }
        public string Message { get; set; }
    }
}