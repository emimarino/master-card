﻿namespace Mastercard.MarketingCenter.Data.Entities
{
    public class FeaturedSearchResult
    {
        public string Title { get; set; }
        public string FeaturedSearchResultHTML { get; set; }
        public string Url { get; set; }
        public string Copy { get; set; }
        public string Image { get; set; }
        public string RegionId { get; set; }
        public string SearchLanguage { get; set; }
    }
}