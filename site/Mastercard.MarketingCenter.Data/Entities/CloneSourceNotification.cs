﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.CloneSourceNotification)]
    public class CloneSourceNotification
    {
        [Key, Column(Order = 0)]
        public int CloneSourceNotificationId { get; set; }
        [Key, Column(Order = 1)]
        public string CloneContentItemId { get; set; }
        [Key, Column(Order = 2)]
        public string CloneRegionId { get; set; }
        public string SourceStatusName { get; set; }
        public DateTime CreationDate { get; set; }
        public string CloneTitle { get; set; }
        public bool HasBeenSent { get; set; }
        public DateTime SourceModificationDate { get; set; }
    }
}