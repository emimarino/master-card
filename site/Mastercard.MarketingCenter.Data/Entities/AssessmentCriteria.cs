﻿using System.ComponentModel.DataAnnotations;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class AssessmentCriteria
    {
        [Key]
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string AssessmentColumnName { get; set; }
    }
}
