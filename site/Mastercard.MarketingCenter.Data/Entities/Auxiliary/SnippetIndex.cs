﻿namespace Mastercard.MarketingCenter.Data
{
    public class SnippetIndex
    {
        public string ReferenceId { get; set; }
        public int StatusId { get; set; }
        public string ContentTypeId { get; set; }
        public string FrontEndUrl { get; set; }
        public string RegionId { get; set; }
        public string SnippetId { get; set; }
        public string SnippetTitle { get; set; }
        public string SnippetFrontendUrl { get; set; }
    }
}