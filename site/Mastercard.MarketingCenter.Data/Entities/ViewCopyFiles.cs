﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class ViewCopyFiles
    {
        public DateTime UploadedDate { get; set; }
        public string FullPath { get; set; }
        public decimal Size { get; set; }
    }
}