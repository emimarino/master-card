﻿namespace Mastercard.MarketingCenter.Data.Entities
{
    public class SegmentationTranslatedContent
    {
        public int Id { get; set; }
        public string SegmentationId { get; set; }
        public string SegmentationName { get; set; }
        private string _languageCode;
        public string LanguageCode
        {
            get { return _languageCode; }
            set { _languageCode = value.Trim(); }
        }
    }
}