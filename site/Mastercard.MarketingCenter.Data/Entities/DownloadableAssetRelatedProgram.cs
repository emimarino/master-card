﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.DownloadableAssetRelatedProgram)]
    public class DownloadableAssetRelatedProgram
    {
        [Key, Column(Order = 0)]
        public string DownloadableAssetContentItemId { get; set; }
        [Key, Column(Order = 1)]
        public string ProgramContentItemId { get; set; }
    }
}