﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.OrderableAssetBrowseAllMaterialTag)]
    public class OrderableAssetBrowseAllMaterialTag : BrowseAllMaterialTag
    {
    }
}