﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class ContentActivityAssetDetailReportResultItem
    {
        public string Title { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string IssuerId { get; set; }
        public string Issuer { get; set; }
        public string ProcessorId { get; set; }
        public string Processor { get; set; }
        public DateTime When { get; set; }
        public string From { get; set; }
        public string Source { get; set; }
        public string AudienceSegments { get; set; }
    }
}