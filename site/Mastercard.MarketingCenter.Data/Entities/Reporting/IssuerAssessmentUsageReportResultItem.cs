﻿namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class IssuerAssessmentUsageReportResultItem
    {
		public int? Issuers { get; set; }
        public int? Users { get; set; }
		public int? Visits { get; set; }
        public int? Downloads { get; set; }
		public int? IssuerReports { get; set; }		
		public int? LeadGens { get; set; }
		public int? LeadGenUniqueUsers { get; set; }
		public int? MatchingErrors { get; set; }
		public int? MatchingErrorsUniqueUsers { get; set; }
		public int? MatchingErrorsUniqueIssuers { get; set; }
    }
}