﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class GdprFollowUpReportResultItem
    {
        public string Email { get; set; }
        public string FullName { get; set; }
        public string NotificationFrequency { get; set; }
        public DateTime RegistrationDate { get; set; }
        public DateTime LastLoginDate { get; set; }
        public string GdprFollowUpStatus { get; set; }
        public DateTime SentDate { get; set; }
        public DateTime? OpenedDate { get; set; }
    }
}