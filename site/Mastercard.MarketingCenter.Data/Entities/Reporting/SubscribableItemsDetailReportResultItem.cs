﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class SubscribableItemsDetailReportResultItem
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string IssuerId { get; set; }
        public string IssuerName { get; set; }
        public string Frequency { get; set; }
        public DateTime SavedDate { get; set; }
        public string ContentItemId { get; set; }
        public string ContentItemName { get; set; }
        public int Favorites { get; set; }
    }
}