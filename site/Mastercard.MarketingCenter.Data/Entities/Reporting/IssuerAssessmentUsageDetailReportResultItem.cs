﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class IssuerAssessmentUsageDetailReportResultItem
    {
        public string User { get; set; }
        public string UserName { get; set; }
        public string IssuerId { get; set; }
        public string Issuer { get; set; }
        public string AudienceSegments { get; set; }
        public string ProcessorId { get; set; }
        public string Processor { get; set; }
        public string ItemType { get; set; }
        public DateTime Date { get; set; }
        public long VisitID { get; set; }
        public string What { get; set; }
        public string From { get; set; }
        public string Source { get; set; }
    }
}