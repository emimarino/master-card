﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class TriggerMarketingNotificationsReportResultItem
    {
        public DateTime NotificationDate { get; set; }
        public string NotificationFrequency { get; set; }
        public string TrackingType { get; set; }
        public int Recipients { get; set; }
        public decimal OpenEmailRate { get; set; }
        public decimal ClickRate { get; set; }
    }
}