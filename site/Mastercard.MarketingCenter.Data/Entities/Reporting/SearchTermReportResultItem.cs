﻿namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class SearchTermReportResultItem
    {
        public string SearchTerm { get; set; }
        public int Total { get; set; }
        public int? SearchResults { get; set; }
        public string SearchResultsDisplay { 
            get 
            {
                if (!SearchResults.HasValue)
                {
                    return MarketingCenterDbConstants.Reports.UntrackedLabel;
                }

                return SearchResults.Value.ToString();
            }
        }

    }
}