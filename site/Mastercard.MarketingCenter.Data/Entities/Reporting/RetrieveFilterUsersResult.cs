﻿namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class RetrieveFilterUsersResult
    {
        public string ProcessorName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string IssuerName { get; set; }
        public string FullName { get; set; }
    }
}