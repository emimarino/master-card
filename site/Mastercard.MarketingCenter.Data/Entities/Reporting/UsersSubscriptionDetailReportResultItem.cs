﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class UsersSubscriptionDetailReportResultItem
    {
        public string UserFullName { get; set; }
        public string Email { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public DateTime SavedDate { get; set; }
    }
}