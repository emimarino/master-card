﻿namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class ProcessorActivityDetailReportResultItem
    {
        public string IssuerId { get; set; }
        public string Issuer { get; set; }
        public int FIsAdded { get; set; }
        public int UserRegistrations { get; set; }
        public int ActiveUsers { get; set; }
        public int UserLogins { get; set; }
    }
}