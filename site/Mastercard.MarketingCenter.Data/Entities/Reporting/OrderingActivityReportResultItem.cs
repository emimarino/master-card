﻿namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class OrderingActivityReportResultItem
    {
        public string RelatedProgramID { get; set; }
        public string RelatedProgram { get; set; }
        public int Processors { get; set; }
        public int FIs { get; set; }
        public int Users { get; set; }
        public int Downloads { get; set; }
        public int EDOrders { get; set; }
        public int PODOrders { get; set; }
        public decimal TotalCostOrdered { get; set; }
    }
}