﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class OrderingActivityDetailReportResultItem
    {
        public string OrderNumber { get; set; }
        public DateTime Date { get; set; }
        public string Processor { get; set; }
        public string RelatedProgram { get; set; }
        public string FinancialInstitution { get; set; }
        public string AudienceSegments { get; set; }
        public string User { get; set; }
        public string Email { get; set; }
        public string ItemName { get; set; }
        public string ItemType { get; set; }
        public string SKU { get; set; }
        public string Fulfillment { get; set; }
        public int EstDistribution { get; set; }
        public decimal ItemPrice { get; set; }
        public int ItemQuantity { get; set; }
        public string OrderStatus { get; set; }
        public bool? AdvisorsTag { get; set; }
        public bool? ConsentGiven { get; set; }
    }
}