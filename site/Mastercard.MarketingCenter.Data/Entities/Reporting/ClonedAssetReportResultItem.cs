﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class ClonedAssetReportResultItem
    {
        public string ContentItemId { get; set; }
        public string ContentType { get; set; }
        public string Title { get; set; }
        public string ClonedBy { get; set; }
        public DateTime Date { get; set; }
    }
}