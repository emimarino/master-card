﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class UsersSubscriptionReportBaseItem
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string IssuerId { get; set; }
        public string IssuerName { get; set; }
        public string Frequency { get; set; }
        public string ContentItemId { get; set; }
        public DateTime SavedDate { get; set; }
    }
}