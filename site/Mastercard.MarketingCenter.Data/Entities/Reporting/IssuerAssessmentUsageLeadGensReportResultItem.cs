﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class IssuerAssessmentUsageLeadGensReportResultItem
    {
        public string User { get; set; }
        public string UserName { get; set; }
        public string IssuerId { get; set; }
        public string Issuer { get; set; }
        public string AudienceSegments { get; set; }
        public string ProcessorId { get; set; }
        public string Processor { get; set; }
        public DateTime Date { get; set; }
    }
}