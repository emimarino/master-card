﻿namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class ContentActivityTagReportResultItem
    {
        public string TagId { get; set; }
        public string Title { get; set; }
        public int TotalHits { get; set; }
        public int Generic { get; set; }
        public int BrowseAll { get; set; }
        public int TagBrowser { get; set; }
        public int Back { get; set; }
        public int Email { get; set; }
    }
}