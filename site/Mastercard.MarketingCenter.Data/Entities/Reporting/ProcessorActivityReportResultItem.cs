﻿namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class ProcessorActivityReportResultItem
    {
        public string ProcessorId { get; set; }
        public string Processor { get; set; }
        public int FIsAdded { get; set; }
        public int UserRegistrations { get; set; }
        public int ActiveUsers { get; set; }
        public int UserLogins { get; set; }
    }
}