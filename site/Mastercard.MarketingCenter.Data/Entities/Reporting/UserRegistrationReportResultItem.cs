﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class UserRegistrationReportResultItem
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public DateTime LastLoginDate { get; set; }
        public DateTime RegistrationDate { get; set; }
        private string _firstName;
        public string FirstName
        {
            get { return IsDisabled ? Constants.UserRegistrationReport.DisabledMessage : _firstName; }
            set { _firstName = value; }
        }
        private string _lastName;
        public string LastName
        {
            get { return IsDisabled ? Constants.UserRegistrationReport.DisabledMessage : _lastName; }
            set { _lastName = value; }
        }
        private string _fullName;
        public string FullName
        {
            get { return IsDisabled ? Constants.UserRegistrationReport.DisabledMessage : _fullName; }
            set { _fullName = value; }
        }
        private string _email;
        public string Email
        {
            get { return IsDisabled ? Constants.UserRegistrationReport.DisabledMessage : _email; }
            set { _email = value; }
        }
        public string FI { get; set; }
        public string Processor { get; set; }
        public string AudienceSegments { get; set; }
        public string Country { get; set; }
        public string Frequency { get; set; }
        public string RegionId { get; set; }
        public bool IsDisabled { get; set; }
        public bool ExcludedFromDomoApi { get; set; }

        public string AudienceSegmentDisplay
        {
            get
            {
                if (!string.IsNullOrEmpty(AudienceSegments))
                {
                    if (AudienceSegments.ToLower().Contains("ibcu"))
                    {
                        return "IBCU";
                    }
                    if (AudienceSegments.ToLower().Contains("large"))
                    {
                        return "LMT";
                    }
                }

                return "BOTH";
            }
        }

        public string EmailConsent
        {
            get
            {
                if (string.IsNullOrEmpty(Frequency))
                {
                    return "Never";
                }

                return Frequency;
            }
        }

        public string RealFirstName { get { return _firstName; } }
        public string RealLastName { get { return _lastName; } }
        public string RealFullName { get { return _fullName; } }
        public string RealEmail { get { return _email; } }
    }
}