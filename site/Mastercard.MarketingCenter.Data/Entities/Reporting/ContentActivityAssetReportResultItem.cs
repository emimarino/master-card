﻿namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class ContentActivityAssetReportResultItem
    {
        public string ContentType { get; set; }
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public int TotalHits { get; set; }
        public int Generic { get; set; }
        public int Marquee { get; set; }
        public int ProductFinder { get; set; }
        public int CrossSellBox { get; set; }
        public int RelatedAsset { get; set; }
        public int RelatedItem { get; set; }
        public int Back { get; set; }
        public int Email { get; set; }
        public int MyFavorites { get; set; }
        public int MostPopular { get; set; }
    }
}