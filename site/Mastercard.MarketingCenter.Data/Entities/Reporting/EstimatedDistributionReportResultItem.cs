﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class EstimatedDistributionReportResultItem
    {
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public DownloadableAssetRelatedProgram RelatedProgram { get; set; }
        public int? TotalDownloads { get; set; }
        public int TotalEstimatedDistribution { get; set; }
        public DateTime? MostRecentDownload { get; set; }
        public string RelatedProgramTitle { get; set; }
    }
}