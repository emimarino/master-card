﻿namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class RetrieveOrderStatusesResult
    {
        public string OrderStatus { get; set; }
    }
}