﻿namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class ContentActivityWebinarReportResultItem
    {
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public int Processors { get; set; }
        public int Issuers { get; set; }
        public int Users { get; set; }
        public int TotalHits { get; set; }
        public int Email { get; set; }
        public int MyFavorites { get; set; }
        public int MostPopular { get; set; }
    }
}