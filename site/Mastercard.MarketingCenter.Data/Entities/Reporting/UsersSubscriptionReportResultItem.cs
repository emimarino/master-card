﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class UsersSubscriptionReportResultItem
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string IssuerName { get; set; }
        public int Favorites { get; set; }
        public string Frequency { get; set; }
        public DateTime LastSavedDate { get; set; }
    }
}