﻿namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class IssuerMatchingReportResultItem
	{
		public string CID { get; set; }
		public string IssuerLegalName { get; set; }
		public string BIN { get; set; }
		public string IssuerName { get; set; }
		public string Matched { get; set; }
		public string HeatmapData { get; set; }
	}
}