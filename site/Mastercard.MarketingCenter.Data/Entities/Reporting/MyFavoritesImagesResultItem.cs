﻿namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class MyFavoritesImagesResultItem
    {
        public string ContentItemId { get; set; }
        public string ContentType { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageType { get; set; }
        public string ImageUrl { get; set; }
        public int ImageWidth { get; set; }
        public int ImageHeight { get; set; }
        public string HasRightResolution { get; set; }
        public string HasUserSubscriptions { get; set; }
    }
}