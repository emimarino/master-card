﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class RetrieveFilterOrdersResult
    {
        public string FullName { get; set; }
        public string ProcessorName { get; set; }
        public string Email { get; set; }
        public string OrderNumber { get; set; }
        public DateTime? OrderDate { get; set; }
        public string OrderStatus { get; set; }
        public decimal? OrderPrice { get; set; }
        public int? OrderQuantity { get; set; }
        public string IssuerName { get; set; }
        public string UserName { get; set; }
        public int OrderID { get; set; }
    }
}