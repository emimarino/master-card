﻿using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class EstimatedDistributionDetailReportResultItem
    {
        public string ContentItemId { get; set; }
        public string AssetTitle { get; set; }
        public Issuer Issuer { get; set; }
        public string IssuerName { get; set; }
        public string AudienceSegments { get; set; }
        public virtual IEnumerable<Segmentation> Segmentations { get; set; }
        public string User { get; set; }
        public string Program { get; set; }
        public DownloadableAssetRelatedProgram RelatedProgram { get; set; }
        public int? EstimatedDistribution { get; set; }
        public DateTime? LastRequestDate { get; set; }
        public DateTime? MostRecentDownload { get; set; }
    }
}