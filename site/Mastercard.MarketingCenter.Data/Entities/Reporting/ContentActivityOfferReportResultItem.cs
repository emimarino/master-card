﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class ContentActivityOfferReportResultItem
    {
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public int TotalHits { get; set; }
        public int Generic { get; set; }        
        public int RelatedItem { get; set; }
        public int Back { get; set; }
        public int Email { get; set; }
        public int MostPopular { get; set; }
    }
}
