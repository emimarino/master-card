﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class ProgramPrintOrderDateExpirationReportResultItem
    {
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public DateTime? PrintedOrderPeriodEndDate { get; set; }
        public DateTime? InMarketEndDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}