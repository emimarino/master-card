﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class EstimatedDistributionDownload
    {
        public string ContentItemId { get; set; }
        public int? TotalDownloads { get; set; }
        public DateTime? MostRecentDownload { get; set; }
    }
}