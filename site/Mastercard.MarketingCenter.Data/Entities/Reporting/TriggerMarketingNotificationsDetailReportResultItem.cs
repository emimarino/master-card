﻿namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class TriggerMarketingNotificationsDetailReportResultItem
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string IssuerId { get; set; }
        public string IssuerName { get; set; }
        public int MailDispatcherId { get; set; }
        public bool Opened { get; set; }
        public int TotalLinks { get; set; }
        public decimal ClickRate { get; set; }
    }
}