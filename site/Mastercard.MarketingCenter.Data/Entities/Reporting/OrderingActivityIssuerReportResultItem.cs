﻿namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class OrderingActivityIssuerReportResultItem
    {
        public string IssuerID { get; set; }
        public string IssuerName { get; set; }
        public int Processors { get; set; }
        public int RPs { get; set; }
        public int Users { get; set; }
        public int Downloads { get; set; }
        public int EDOrders { get; set; }
        public int PODOrders { get; set; }
        public decimal TotalCostOrdered { get; set; }
    }
}