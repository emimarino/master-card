﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class TriggerMarketingNotificationsReportBaseItem
    {
        public int MailDispatcherId { get; set; }
        public DateTime Date { get; set; }
        public string TrackingType { get; set; }
        public int UserId { get; set; }
        public bool Opened { get; set; }
        public int TotalLinks { get; set; }
    }
}