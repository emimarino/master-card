﻿namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class SubscribableItemsReportResultItem
    {
        public string ItemId { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public int Favorites { get; set; }
    }
}