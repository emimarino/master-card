﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class TriggerMarketingNotificationClickedLink
    {
        public int UserId { get; set; }
        public string Link { get; set; }
        public string TrackingType { get; set; }
        public string TriggerMarketingSubscription { get; set; }
        public string MailDispatcherId { get; set; }
        public string LinkNumber { get; set; }
        public DateTime Date { get; set; }
    }
}