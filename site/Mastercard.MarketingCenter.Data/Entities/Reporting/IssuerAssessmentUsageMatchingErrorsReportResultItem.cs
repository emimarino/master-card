﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities.Reporting
{
    public class IssuerAssessmentUsageMatchingErrorsReportResultItem
    {
        public string User { get; set; }
        public string UserName { get; set; }
        public string IssuerId { get; set; }
        public string Issuer { get; set; }
        public string AudienceSegments { get; set; }
        public string ProcessorId { get; set; }
        public string Processor { get; set; }
        public DateTime Date { get; set; }
        public string Error { get; set; }
        public string Message { get; set; }
        public Int32? ImportedIcaId { get; set; }
        public string LegalName { get; set; }
        public string CID { get; set; }
    }
}