﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ViewSiteTrackingByContext)]
    public class ViewSiteTrackingByContext
    {
        [Key]
        public long SiteTrackingID { get; set; }
        public long VisitID { get; set; }
        public string What { get; set; }
        public string Who { get; set; }
        public DateTime When { get; set; }
        public string From { get; set; }
        public string How { get; set; }
        public string ContentItemID { get; set; }
        public string PageGroup { get; set; }
        public string Region { get; set; }
        public bool IsArchived { get; set; }
    }
}