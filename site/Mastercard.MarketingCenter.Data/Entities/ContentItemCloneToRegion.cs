﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ContentItemCloneToRegion)]
    public class ContentItemCloneToRegion
    {
        [Key, Column(Order = 0)]
        public string ContentItemId { get; set; }
        [Key, Column(Order = 1)]
        public string RegionId { get; set; }
    }
}