﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.RolePermissionRegion)]
    public class RolePermissionRegion
    {
        [Key, Column(Order = 0)]
        public int RoleId { get; set; }
        [Key, Column(Order = 1)]
        public int PermissionId { get; set; }
        [Key, Column(Order = 2)]
        public string RegionId { get; set; }

        [ForeignKey("RoleId")]
        public virtual Role Role { get; set; }
        [ForeignKey("PermissionId")]
        public virtual Permission Permission { get; set; }
        [ForeignKey("RegionId")]
        public virtual Region Region { get; set; }
    }
}