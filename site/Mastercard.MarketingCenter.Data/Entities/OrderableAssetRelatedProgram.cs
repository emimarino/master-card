﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.OrderableAssetRelatedProgram)]
    public class OrderableAssetRelatedProgram
    {
        [Key, Column(Order = 0)]
        public string OrderableAssetContentItemId { get; set; }
        [Key, Column(Order = 1)]
        public string ProgramContentItemId { get; set; }
    }
}