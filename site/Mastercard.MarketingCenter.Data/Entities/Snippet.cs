﻿using System.ComponentModel.DataAnnotations;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class Snippet
    {
        [Key]
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public string Html { get; set; }
        public string RestrictToSpecialAudience { get; set; }
    }
}
