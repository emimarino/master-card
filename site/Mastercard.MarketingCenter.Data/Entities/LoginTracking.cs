﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.LoginTracking)]
    public class LoginTracking
    {
        [Key]
        public int LoginTrackingID { get; set; }
        public string UserName { get; set; }
        public DateTime Date { get; set; }
    }
}