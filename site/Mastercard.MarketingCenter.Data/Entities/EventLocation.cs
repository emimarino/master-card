﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.EventLocation)]
    public class EventLocation
    {
        [Key]
        public int EventLocationId { get; set; }
        public string Title { get; set; }
        public string PricelessEventCity { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        public ICollection<EventLocationPositionHierarchy> EventLocationPositionHierarchy { get; set; }
        public virtual ICollection<Offer> Offers { get; set; }
    }
}