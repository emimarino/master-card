﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.List)]
    public class List
    {
        [Key]
        public int ListId { get; set; }
        public string Name { get; set; }
        public string TableName { get; set; }
        public string WhereClause { get; set; }
        public bool ShowInNavigation { get; set; }
        public bool CanExpire { get; set; }

        public virtual ICollection<ListClientEvent> ListClientEvents { get; set; }
        public virtual ICollection<ListFieldDefinition> ListFieldDefinitions { get; set; }

        public FieldDefinition GetIdField()
        {
            var idField = ListFieldDefinitions
                .FirstOrDefault(o => o.FieldDefinition.FieldType.IsIdentifierField());

            return idField != null ? idField.FieldDefinition : null;
        }

        public FieldDefinition GetRegionField()
        {
            var regionField = ListFieldDefinitions
                .FirstOrDefault(o => o.FieldDefinition.FieldType.FieldTypeCode == FieldType.Region);

            return regionField != null ? regionField.FieldDefinition : null;
        }
    }
}