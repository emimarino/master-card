﻿namespace Mastercard.MarketingCenter.Data.Entities
{
    public class State
    {
        public int StateID { get; set; }
        public string StateName { get; set; }
    }
}