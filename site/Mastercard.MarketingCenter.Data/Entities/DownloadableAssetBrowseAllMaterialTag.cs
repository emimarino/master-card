﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.DownloadableAssetBrowseAllMaterialTag)]
    public class DownloadableAssetBrowseAllMaterialTag : BrowseAllMaterialTag
    {
    }
}