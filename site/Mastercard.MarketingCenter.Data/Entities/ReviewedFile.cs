﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ReviewedFile)]
    public class ReviewedFile
    {
        [Key]
        public long Id { get; set; }
        public string TableId { get; set; }
        public string Table { get; set; }
        public string Column { get; set; }
        public string Filename { get; set; }
        public string FullPath { get; set; }
        public string Type { get; set; }
        public decimal Size { get; set; }
        public DateTime UploadedDate { get; set; }
    }
}