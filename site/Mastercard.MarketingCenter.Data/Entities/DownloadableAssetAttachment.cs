﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.DownloadableAssetAttachment)]
    public class DownloadableAssetAttachment : Attachment
    {
    }
}