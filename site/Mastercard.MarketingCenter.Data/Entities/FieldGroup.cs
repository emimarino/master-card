﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.FieldGroup)]
    public class FieldGroup
    {
        [Key]
        public int FieldGroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}