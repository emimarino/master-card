﻿namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class ColorChoiceSettings
    {
        public ColorChoice[] ColorChoices { get; set; }
    }

    public class ColorChoice
    {
        public string Value { get; set; }
        public string Color { get; set; }
    }
}