﻿namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class FileTypeSettings
    {
        public string[] AcceptFiles { get; set; }
        public string VideosFolder { get; set; }
        public float MaxSize { get; set; }
        public string Recommendations { get; set; }
    }
}