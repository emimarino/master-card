﻿namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class RegionTitleSettings
    {
        public string TableName { get; set; }
        public string TableRegionField { get; set; }
        public string TableIdField { get; set; }
        public string TableIdValue { get; set; }
        public string CurrentRegionId { get; set; }
    }
}