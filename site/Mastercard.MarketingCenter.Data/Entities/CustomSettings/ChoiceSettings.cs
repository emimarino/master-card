﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class ChoiceSettings
    {
        public IDictionary<string, string> Options { get; set; } = new Dictionary<string, string>();
        public IDictionary<string, string> BusinessOwnerOptions { get; set; } = new Dictionary<string, string>();
        public bool IncludeEmptyChoice { get; set; }
        public string EmptyChoiceText { get; set; }
    }
}