﻿namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class RegionDefaultValueSettings
    {
        public RegionDefaultValues[] RegionDefaultValues { get; set; }
    }

    public class RegionDefaultValues
    {
        public string Region { get; set; }
        public string DefaultValue { get; set; }
    }
}