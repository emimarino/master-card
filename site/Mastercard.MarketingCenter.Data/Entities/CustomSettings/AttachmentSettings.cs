﻿namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class AttachmentSettings : FileTypeSettings
    {
        public string TableName { get; set; }
    }
}