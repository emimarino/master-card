﻿namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class UniqueFieldSettings
    {
        public string TableName { get; set; }
        public string[] FieldNames { get; set; }
        public bool IgnoreWhiteSpaces { get; set; }
        public bool IgnoreNullOrEmpty { get; set; }
    }
}