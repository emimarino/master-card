﻿namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class NumberSettings
    {
        public bool IsDecimal { get; set; }
    }
}