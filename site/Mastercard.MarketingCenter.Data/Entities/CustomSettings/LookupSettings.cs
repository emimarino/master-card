﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class LookupSettings
    {
        public string TableName { get; set; }
        public string TableIdField { get; set; }
        public string TableValueField { get; set; }
        public string TableRegionField { get; set; }
        public string AssociationTableName { get; set; }
        public string RegionId { get; set; }
        public IEnumerable<string> AssociationKeysMap { get; set; }
        public bool ForceDisableMultiSelection { get; set; }
        public bool ContentTypeLookup { get; set; }
        public bool ExcludeCurrentRegionId { get; set; }
        public IEnumerable<string> IdsToExclude { get; set; }
        public bool CleanUp { get; set; } = false;
        public bool IncludeSelectAllChoice { get; set; }
        public string SelectAllChoiceText { get; set; }
        public bool IncludeCrossBorderRegions { get; set; }
        public string RegionApplicabilityTableName { get; set; }
        public string RegionVisibilityTableName { get; set; }
    }
}