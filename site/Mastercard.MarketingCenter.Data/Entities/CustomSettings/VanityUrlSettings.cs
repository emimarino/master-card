﻿namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class VanityUrlSettings
    {
        public string UrlPath { get; set; }
        public string DefaultTitleField { get; set; }
    }
}