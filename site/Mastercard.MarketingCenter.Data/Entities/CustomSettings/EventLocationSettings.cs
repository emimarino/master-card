﻿namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class EventLocationSettings
    {
        public string AssociationTable { get; set; }
        public string Editor { get; set; }
    }
}