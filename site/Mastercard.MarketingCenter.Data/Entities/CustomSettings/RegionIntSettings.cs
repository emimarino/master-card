﻿namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class RegionIntSettings : NumberSettings
    {
        public string TableName { get; set; }
        public string RelatedTableField { get; set; }
        public string ContentItemId { get; set; }
        public string RegionId { get; set; }
    }
}