﻿namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class TranslationsSettings
    {
        public int[] FieldDefinitionIds { get; set; }
        public string TranslatedContentTableName { get; set; }
        public int[] RequiredFieldDefinitionIds { get; set; }
        public TranslationsLanguage[] Languages { get; set; }
        public string ListViewFieldName { get; set; }
    }

    public class TranslationsLanguage
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}