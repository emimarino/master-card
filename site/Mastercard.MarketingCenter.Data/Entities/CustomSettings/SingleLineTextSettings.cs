﻿namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class SingleLineTextSettings
    {
        public string GetAllTextFromTheRightOf { get; set; }
        public string RemoveAllOccurrencesOf { get; set; }
        public bool UriRemoveDomainSegment { get; set; }
        public string HelpText { get; set; }
        public bool IsYoutubeVideoUrl { get; set; }
    }
}