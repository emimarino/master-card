﻿namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class DateTimeSettings
    {
        public string LowerThanFieldName { get; set; }
        public string GreaterThanFieldName { get; set; }
        public bool MustBeAfterNow { get; set; }

        public string UniqueSpanInTableName { get; set; }
        public string[] UniqueSpanInFieldNames { get; set; }
    }
}