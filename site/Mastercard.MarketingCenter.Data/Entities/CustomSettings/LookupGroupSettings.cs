﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class LookupGroupSettings : LookupSettings
    {
        public string TableGroupField { get; set; }
        public string GroupTableName { get; set; }
        public string GroupTableIdField { get; set; }
        public string GroupTableValueField { get; set; }
        public bool ContentItemTableValueField { get; set; }
        public IEnumerable<string> ContentItemTableNames { get; set; }
    }
}