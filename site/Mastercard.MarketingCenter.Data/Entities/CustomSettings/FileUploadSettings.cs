﻿namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class FileUploadSettings : FileTypeSettings
    {
        public string UploadLocationSetting { get; set; }
    }
}