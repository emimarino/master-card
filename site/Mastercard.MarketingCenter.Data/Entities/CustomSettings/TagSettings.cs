﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class TagSettings
    {
        public string TagCategoryId { get; set; }
        public string Editor { get; set; }
        public IEnumerable<string> IdsToExclude { get; set; }

        public static class Editors
        {
            public const string CrossBorderMarketTag = "CrossBorderMarketTag";
            public const string MarketTag = "MarketTag";
            public const string Tag = "Tag";
        }
    }
}