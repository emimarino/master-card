﻿namespace Mastercard.MarketingCenter.Data.Entities.CustomSettings
{
    public class BooleanSettings
    {
        public string[] DataToggle { get; set; }
        public bool DataToggleHide { get; set; }
        public bool DataToggleResetValues { get; set; }
        public string SwitchDescription { get; set; }
    }
}