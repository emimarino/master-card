﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.OfferType)]
    public class OfferType
    {
        [Key]
        public string OfferTypeId { get; set; }
        public string Title { get; set; }
        public string PricelessProgramName { get; set; }
        public bool ShowProductUrl { get; set; }
    }
}