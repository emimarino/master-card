﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.MostPopularProcess)]
    public class MostPopularProcess
    {
        [Key]
        public long Id { get; set; }
        public DateTime ProcessDate { get; set; }
    }
}