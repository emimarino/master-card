﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.EventLocationPositionHierarchy)]
    public class EventLocationPositionHierarchy
    {
        [Key]
        public int PositionId { get; set; }
        public int? ParentPositionId { get; set; }
        public int EventLocationId { get; set; }

        public virtual EventLocation EventLocation { get; set; }
    }
}