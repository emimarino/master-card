﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.Tag)]
    public class Tag
    {
        [Key]
        public string TagID { get; set; }
        public string Identifier { get; set; }
        public string DisplayName { get; set; }
        public bool? RestrictAccess { get; set; }
        public bool? Featured { get; set; }
        public string Description { get; set; }
        public string PageContent { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool? HideFromSearch { get; set; }
        public string SpecialAudienceAccess { get; set; }
        public string RestrictToSpecialAudience { get; set; }

        public virtual ICollection<ContentType> ContentTypes { get; set; }
        public virtual ICollection<User> FeaturingUsers { get; set; }
        [JsonIgnore]
        public virtual ICollection<ContentItemBase> ContentItems { get; set; }
        [JsonIgnore]
        public virtual ICollection<TagHierarchyPosition> TagHierarchyPositions { get; set; }
        [JsonIgnore]
        public virtual ICollection<TagTranslatedContent> TagTranslatedContent { get; set; }
        [JsonIgnore]
        public virtual ICollection<ContentItemFeatureOnTag> ContentItemFeatureOnTags { get; set; }
    }
}