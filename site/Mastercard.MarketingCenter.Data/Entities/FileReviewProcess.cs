﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.FileReviewProcess)]
    public class FileReviewProcess
    {
        [Key]
        public long Id { get; set; }
        public string Folder { get; set; }
        public DateTime ProcessDate { get; set; }
        public int FilesReviewed { get; set; }
        public int FilesMatched { get; set; }
        public int FilesUnmatched { get; set; }
        public int FilesReferenced { get; set; }
    }
}