﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ContentItemFeatureLocation)]
    public class ContentItemFeatureLocation
    {
        [Key, Column(Order = 0)]
        public string ContentItemId { get; set; }
        [Key, Column(Order = 1)]
        public string FeatureLocationId { get; set; }
        public int FeatureLevel { get; set; }
        [ForeignKey("FeatureLocationId")]
        public virtual FeatureLocation FeatureLocation { get; set; }
        [ForeignKey("ContentItemId")]
        public virtual ContentItemBase ContentItem { get; set; }
    }
}