﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ProgramBrowseAllMaterialTag)]
    public class ProgramBrowseAllMaterialTag : BrowseAllMaterialTag
    {
    }
}