﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ImportedOffer)]
    public class ImportedOffer
    {
        [Key]
        public int ImportedOfferId { get; set; }
        public string ProductId { get; set; }
        public string DisplayName { get; set; }
        public string ProductName { get; set; }
        public string LongDescription { get; set; }
        public string Description { get; set; }
        public string SpecialOffer { get; set; }
        public string CelebId { get; set; }
        public string LocaleId { get; set; }
        public string CelebName { get; set; }
        public string CelebUrl { get; set; }
        public string CelebImageUrl { get; set; }
        public string CharityId { get; set; }
        public string CharityName { get; set; }
        public string CharityUrl { get; set; }
        public string FinePrint { get; set; }
        public string Details { get; set; }
        public string ProductTerms { get; set; }
        public string IsEvent { get; set; }
        public string MaxQuantityPerOrder { get; set; }
        public string Experience { get; set; }
        public string EventTime { get; set; }
        public string EndDate { get; set; }
        public string EventCity { get; set; }
        public string EventState { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string GeographicId { get; set; }
        public string SharedPeoplePerItem { get; set; }
        public string EventVenue { get; set; }
        public string CatId { get; set; }
        public string CatName { get; set; }
        public string CatUrl { get; set; }
        public string AllCategories { get; set; }
        public string EventDuration { get; set; }
        public string ImageUrl { get; set; }
        public string ProductUrl { get; set; }
        public string AltImageUrl { get; set; }
        public string SourcedSiteName { get; set; }
        public string InitiativeName { get; set; }
        public string VoiceSkillDescription { get; set; }
        public string Variants { get; set; }
        public string ProgramName { get; set; }
        public string CardExclusivity { get; set; }
        public string IssuerExclusivity { get; set; }
        public string PunchoutInfo { get; set; }
        public string GeographicInclusions { get; set; }
        public string GeographicExclusions { get; set; }
        public string MastercardGlobal { get; set; }
        public string Price { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyCharacter { get; set; }
        public string InventoryCount { get; set; }
        public string MasterProductIds { get; set; }
        public string LuminaryHtml { get; set; }
        public string IsShippable { get; set; }
        public string ProcessingDays { get; set; }
        public string ProcessingMsg { get; set; }
        public string Restriction { get; set; }
        public DateTime ImportedDate { get; set; }
        public bool IsLastImportedVersion { get; set; }
        public bool IsProcessed { get; set; }
        public bool IsArchived { get; set; }
    }
}