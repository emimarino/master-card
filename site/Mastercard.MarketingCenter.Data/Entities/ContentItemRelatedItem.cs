﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ContentItemRelatedItem)]
    public class ContentItemRelatedItem
    {
        [Key, Column(Order = 0)]
        public string ContentItemId { get; set; }
        [Key, Column(Order = 1)]
        public string RelatedContentItemId { get; set; }
    }
}