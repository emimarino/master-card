﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class UserData
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime LastLoginDate { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Email { get; set; }
    }
}