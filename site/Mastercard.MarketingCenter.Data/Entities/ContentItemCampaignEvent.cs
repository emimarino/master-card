﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ContentItemCampaignEvent)]
    public class ContentItemCampaignEvent
    {
        [Key, Column(Order = 0)]
        public string ContentItemId { get; set; }
        [Key, Column(Order = 1)]
        public string CampaignEventId { get; set; }

        [ForeignKey("ContentItemId")]
        public virtual ContentItemForCalendar ContentItemForCalendar { get; set; } = null;
        [ForeignKey("CampaignEventId")]
        public virtual CampaignEvent CampaignEvent { get; set; }
    }
}