﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.SiteVisits)]
    public class SiteVisit
    {
        [Key]
        public long VisitID { get; set; }
        public string UrlReferrer { get; set; }
    }
}