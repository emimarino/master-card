﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.AssetFullWidth)]
    public class AssetFullWidth
    {
        [Key]
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string ThumbnailImage { get; set; }
        public string LongDescription { get; set; }
        public string ShowInCrossSellBox { get; set; }
        public string PriorityLevel { get; set; }
        public string IconID { get; set; }
        public string AssetType { get; set; }
        public string BottomAreaDescription { get; set; }
        public string BrowseAllMaterialsDisplayText { get; set; }
        public string RestrictToSpecialAudience { get; set; }
        public string OriginalContentItemId { get; set; }
        public bool NotifyWhenSourceChanges { get; set; }
        public bool AllowCloning { get; set; }
        public int? BusinessOwnerID { get; set; }
        public bool EnableFavoriting { get; set; }

        [ForeignKey("BusinessOwnerID")]
        public User BusinessOwner { get; set; }
    }
}