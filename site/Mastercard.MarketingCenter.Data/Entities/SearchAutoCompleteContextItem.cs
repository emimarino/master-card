﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class SearchAutoCompleteContextItem
    {
        public string Language { get; set; }
        public string Title { get; set; }
        public string Synonyms { get; set; }
        public string SearchParameter { get; set; }
        public IEnumerable<string> SegmentationIds { get; set; }
        public string RestrictToSpecialAudience { get; set; }
        public int Rank { get; set; }
        public int SortOrder { get; set; }
        public string RegionId { get; set; }

        public SearchAutoCompleteContextItem()
        {
            SegmentationIds = new List<string>();
        }
    }
}