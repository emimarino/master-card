﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.VanityUrl)]
    public class VanityUrl
    {
        [Key]
        public string VanityUrlId { get; set; }
        public string Url { get; set; }
        public string VanityURL { get; set; }
        public string RegionId { get; set; }
    }
}