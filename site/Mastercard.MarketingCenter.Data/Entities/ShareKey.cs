﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ShareKey)]
    public class ShareKey
    {
        [Key]
        public int ShareKeyID { get; set; }
        public int UserID { get; set; }
        public int ShareKeyStatusID { get; set; }
        public string Key { get; set; }
        public string Cid { get; set; }
        public string EmailTo { get; set; }
        public DateTime DateKeyGenerated { get; set; }
    }
}