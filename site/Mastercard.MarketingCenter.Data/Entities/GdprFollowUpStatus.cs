﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.GdprFollowUpStatus)]
    public class GdprFollowUpStatus
    {
        [Key]
        public int GdprFollowUpStatusId { get; set; }
        public string Name { get; set; }
    }
}