﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.GdpRequest)]
    public class GdpRequest
    {
        [Key]
        public int GdpRequestId { get; set; }
        public int RequestId { get; set; }
        public string RequestType { get; set; }
        public string RequestContext { get; set; }
        public string SearchKey { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public int GdpCorrelationId { get; set; }

        public virtual GdpCorrelation GdpCorrelation { get; set; }
        public virtual ICollection<GdpResponse> GdpResponses { get; set; }

        public string GetEmail()
        {
            return SearchKey;
        }
    }
}