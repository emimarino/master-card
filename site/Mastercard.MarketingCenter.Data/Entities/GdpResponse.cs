﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.GdpResponse)]
    public class GdpResponse
    {
        [Key]
        public int GdpResponseId { get; set; }
        public int GdpCorrelationId { get; set; }
        public int GdpRequestId { get; set; }
        public string ServiceFunctionCode { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseData { get; set; }

        public virtual GdpCorrelation GdpCorrelation { get; set; }
        public virtual GdpRequest GdpRequest { get; set; }
    }
}