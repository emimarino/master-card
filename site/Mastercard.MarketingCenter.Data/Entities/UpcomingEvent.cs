﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class UpcomingEvent
    {
        [Key]
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public DateTime? Date { get; set; }
        public string Location { get; set; }
        public string Link { get; set; }
        public int Ordering { get; set; }
        public string Image { get; set; }
    }
}
