﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.OrderableAsset)]
    public class OrderableAsset
    {
        [Key]
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string ThumbnailImage { get; set; }
        public string LongDescription { get; set; }
        public string ShowInCrossSellBox { get; set; }
        public string PriorityLevel { get; set; }
        public string MainImage { get; set; }
        public string CallToAction { get; set; }
        public string BrowseAllMaterialsDisplayText { get; set; }
        public string BrowseAllMaterialsTags { get; set; }
        public string Sku { get; set; }
        public bool? Vdp { get; set; }
        public bool? Customizable { get; set; }
        public string ElectronicDeliveryFile { get; set; }
        public string ApproverEmailAddress { get; set; }
        public string IconID { get; set; }
        public string PricingScheduleID { get; set; }
        public string AssetType { get; set; }
        public string PDFPreview { get; set; }
        public string RestrictToSpecialAudience { get; set; }
        public string OriginalContentItemId { get; set; }
        public int? BusinessOwnerID { get; set; }
        public bool EnableFavoriting { get; set; }

        [ForeignKey("BusinessOwnerID")]
        public virtual User BusinessOwner { get; set; }
        [ForeignKey("PricingScheduleID")]
        public virtual PricingSchedule PricingSchedule { get; set; }
    }
}