﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.RoleRegion)]
    public class RoleRegion
    {
        [Key, Column(Order = 0)]
        public int RoleId { get; set; }
        [Key, Column(Order = 1)]
        public string RegionId { get; set; }
        public string PermissionName { get; set; }

        public virtual Role Role { get; set; }
        public virtual Region Region { get; set; }
    }
}