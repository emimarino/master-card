﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ContentItemFeatureOnTag)]
    public class ContentItemFeatureOnTag
    {
        [Key, Column(Order = 0)]
        public string ContentItemId { get; set; }
        [Key, Column(Order = 1)]
        public string TagId { get; set; }
        public int FeatureLevel { get; set; }

        [ForeignKey("TagId")]
        public virtual Tag Tag { get; set; }
    }
}