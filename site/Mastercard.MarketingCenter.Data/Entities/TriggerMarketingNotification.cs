﻿using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class UserTriggerMarketingNotification
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        public string UserEmail { get; set; }
        public int UserSubscriptionFrequencyId { get; set; }
        public string UserRegionid { get; set; }
        public IEnumerable<ContentItemTriggerMarketingNotification> Notifications { get; set; }
    }

    public class ContentItemTriggerMarketingNotification
    {
        public string ContentItemId { get; set; }
        public string ParentContentItemId { get; set; }
        public bool IsKnownParent { get; set; }
        public bool IsNew { get; set; }
        public DateTime SavedDate { get; set; }
        public TriggerMarketingNotification Notification { get; set; }
        public IEnumerable<ContentItemTriggerMarketingNotification> Children { get; set; }
    }

    public class TriggerMarketingNotification
    {
        public string ContentItemId { get; set; }
        public string RegionId { get; set; }
        public bool Notify { get; set; }
        public bool Featured { get; set; }
        public string Reasons { get; set; }
        public bool IsNew { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        public string UserEmail { get; set; }
        public int UserSubscriptionFrequencyId { get; set; }
        public DateTime SavedDate { get; set; }
        public DateTime? SentDailyDate { get; set; }
        public DateTime? SentWeeklyDate { get; set; }
        public string ParentContentItemId { get; set; }
        public string UserSpecialAudienceAccess { get; set; }
    }
}