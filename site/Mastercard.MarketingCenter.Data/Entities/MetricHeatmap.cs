﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.MetricHeatmap)]
    public class MetricHeatmap
    {
        [Key]
        public string Cid { get; set; }
        public string SpendPerActive { get; set; }
        public string PerXbSpend { get; set; }
        public string XbActive { get; set; }
        public string TrxPerActive { get; set; }
        public string HighMcc { get; set; }
        public string EcActive { get; set; }
        public string DeclineRate { get; set; }
        public string DivCategorySpend { get; set; }
    }
}