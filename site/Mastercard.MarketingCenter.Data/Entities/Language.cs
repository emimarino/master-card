﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.Language)]
    public class Language
    {
        [Key]
        public long LanguageId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string TagIdentifier { get; set; }
    }
}