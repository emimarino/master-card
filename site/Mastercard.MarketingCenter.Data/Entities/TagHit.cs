﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.TagHit)]
    public class TagHit
    {
        [Key]
        public int TagHitID { get; set; }
        public string TagID { get; set; }
        public string UserName { get; set; }
        public DateTime Date { get; set; }
        public long VisitID { get; set; }
    }
}