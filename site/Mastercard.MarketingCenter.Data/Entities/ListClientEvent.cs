﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ListClientEvent)]
    public class ListClientEvent
    {
        [Key]
        public int ListClientEventId { get; set; }
        public int ListId { get; set; }
        public string Name { get; set; }
        public string FiredOn { get; set; }
        public string ClientEventCode { get; set; }
        public string ContentAction { get; set; }

        [ForeignKey("ListId")]
        public List List { get; set; }
    }
}