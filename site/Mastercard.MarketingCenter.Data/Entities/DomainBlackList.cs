﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.DomainBlacklist)]
    public class DomainBlacklist
    {
        [Key]
        public string DomainBlacklistID { get; set; }
        public string Domain { get; set; }
    }
}