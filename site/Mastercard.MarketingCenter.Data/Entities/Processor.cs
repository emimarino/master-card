﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.Processor)]
    public class Processor
    {
        [Key]
        public string ProcessorId { get; set; }
        public string Title { get; set; }
        public bool? Special { get; set; }
        public string UserRole { get; set; }

        public virtual ICollection<Issuer> Issuers { get; set; }
    }
}