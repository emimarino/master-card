﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.MasterICA)]
    public class MasterICA
    {
        [Key, Column(Order = 0)]
        public int MasterIca { get; set; }
        [Key, Column(Order = 1)]
        public int Ica { get; set; }
    }
}