﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ContentItemVersion)]
    public class ContentItemVersion
    {
        [Key]
        public long ContentItemVersionId { get; set; }
        public string ContentItemId { get; set; }
        public string SerializationData { get; set; }
        public DateTime DateSaved { get; set; }
        public int? LastModificationByUserId { get; set; }
        public DateTime? LastModifyDate { get; set; }

        [ForeignKey("LastModificationByUserId")]
        public virtual User ModifiedByUser { get; set; }
    }
}