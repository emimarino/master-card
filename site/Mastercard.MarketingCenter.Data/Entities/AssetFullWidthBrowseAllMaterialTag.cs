using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.AssetFullWidthBrowseAllMaterialTag)]
    public class AssetFullWidthBrowseAllMaterialTag : BrowseAllMaterialTag
    {
    }
}