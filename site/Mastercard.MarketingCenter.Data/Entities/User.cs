﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.User)]
    public class User
    {
        [Key]
        public int UserId { get; set; }
        public string UserName { get; set; }
        private string _fullName;
        public string FullName { get { return Profile == null ? _fullName : $"{Profile.FirstName} {Profile.LastName}"; } protected set { _fullName = value; } }
        public string Email { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }

        private string _language;
        public string Language
        {
            get { return _language; }
            set { _language = value.Trim(); }
        }
        public string IssuerId { get; set; }
        public bool IsDisabled { get; set; }
        public bool IsBlocked { get; set; }
        public bool ExcludedFromDomoApi { get; set; }
        public DateTime ModifiedDate { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string Locale { get { return $"{Language.Trim()}-{Region.Substring(0, 2)}"; } }

        [Required]
        public virtual UserProfile Profile { get; set; }
        [ForeignKey("IssuerId")]
        public virtual Issuer Issuer { get; set; }
        public virtual ICollection<Group> Groups { get; set; }
    }
}