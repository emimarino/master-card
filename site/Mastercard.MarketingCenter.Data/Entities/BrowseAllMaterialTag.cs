﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class BrowseAllMaterialTag
    {
        [Key, Column(Order = 0)]
        public string ContentItemID { get; set; }
        [Key, Column(Order = 1)]
        public string TagID { get; set; }
    }
}