﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.OrderableAssetAttachment)]
    public class OrderableAssetAttachment : Attachment
    {
    }
}