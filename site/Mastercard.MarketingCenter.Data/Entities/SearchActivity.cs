﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.SearchActivity)]
    public class SearchActivity
    {
        [Key]
        public int SearchActivityID { get; set; }
        public string UserName { get; set; }
        public DateTime SearchDateTime { get; set; }
        public string SearchBoxEntry { get; set; }
        public string UrlVisited { get; set; }
        public string Region { get; set; }
        public int? SearchResultsQty { get; set; }
    }
}