﻿using System.ComponentModel.DataAnnotations;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class QuickLink
    {
        [Key]
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public string Paragraph { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }
        public int OrderNumber { get; set; }
    }
}
