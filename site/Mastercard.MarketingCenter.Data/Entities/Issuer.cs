﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.Issuer)]
    public class Issuer
    {
        [Key]
        public string IssuerId { get; set; }
        public string Title { get; set; }
        public string DomainName { get; set; }
        public string BillingId { get; set; }
        public bool HasOrdered { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string SpecialAudienceAccess { get; set; }
        public string Cid { get; set; }
        public bool? MatchConfirmed { get; set; }
        public DateTime? MatchDate { get; set; }
        public string RegionId { get; set; }
        public bool ExcludedFromDomoApi { get; set; }
        public bool IsMastercardIssuer { get; set; }
        public string UnconfirmedCid { get; set; }
        public bool RequiresRegistrationReview { get; set; }
        public int? MatchedByUserId { get; set; }
        public int CreatedByUserId { get; set; }
        public int ModifiedByUserId { get; set; }

        [ForeignKey("MatchedByUserId")]
        public virtual User MatchedBy { get; set; }
        public virtual ICollection<Processor> Processors { get; set; }
        public virtual ICollection<IssuerSegmentation> IssuerSegmentations { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}