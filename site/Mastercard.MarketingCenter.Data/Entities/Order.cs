﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class Order
    {
        [Key]
        public int OrderId { get; set; }
        public string OrderNumber { get; set; }
        public string UserId { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderStatus { get; set; }
        public decimal? PriceAdjustment { get; set; }
        public decimal? ShippingCost { get; set; }
        public decimal? PostageCost { get; set; }
        public string Description { get; set; }
        public string SpecialInstructions { get; set; }
        public bool RushOrder { get; set; }
        public bool ApprovalReminderSent { get; set; }
        public DateTime DateCompleted { get; set; }
        public string PromotionCode { get; set; }
        public string PromotionDescription { get; set; }
        public decimal? PromotionAmount { get; set; }
        public bool VDPFTPReminderSent { get; set; }
        public bool VDPRetrieveReminderSent { get; set; }
        public bool VDPAcceptFileReminderSent { get; set; }
        public int? BillingID { get; set; }
        public bool? ConsentGiven { get; set; }
    }
}