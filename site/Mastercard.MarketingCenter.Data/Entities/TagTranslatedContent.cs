﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.TagTranslatedContent)]
    public partial class TagTranslatedContent
    {
        [Key]
        public int Id { get; set; }
        public string TagId { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public string PageContent { get; set; }
        private string _languageCode;
        public string LanguageCode
        {
            get { return _languageCode; }
            set { _languageCode = value.Trim(); }
        }

        [ForeignKey("TagId")]
        public virtual Tag Tag { get; set; }
    }
}