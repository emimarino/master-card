﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.CampaignCategory)]
    public class CampaignCategory
    {
        [Key]
        public string CampaignCategoryId { get; set; }
        public int OrderNumber { get; set; }
        public string MarketingCalendarUrl { get; set; }
        public string RegionId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int ModifiedByUserId { get; set; }

        public virtual ICollection<CampaignCategoryTranslatedContent> CampaignCategoryTranslatedContents { get; set; }
        public virtual ICollection<CampaignEvent> CampaignEvents { get; set; }
    }
}