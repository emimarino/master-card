﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.AssetBrowseAllMaterialTag)]
    public class AssetBrowseAllMaterialTag : BrowseAllMaterialTag
    {
    }
}