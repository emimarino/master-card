﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ContentItemUserSubscription)]
    public class ContentItemUserSubscription
    {
        [Key, Column(Order = 0)]
        public string ContentItemId { get; set; }
        [Key, Column(Order = 1)]
        public int UserId { get; set; }
        public bool Subscribed { get; set; }
        public DateTime SavedDate { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        [ForeignKey("ContentItemId")]
        public virtual ContentItemBase ContentItem { get; set; }
    }
}