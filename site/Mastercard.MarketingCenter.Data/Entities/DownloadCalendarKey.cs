﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.DownloadCalendarKey)]
    public class DownloadCalendarKey
    {
        [Key]
        public int DownloadCalendarKeyId { get; set; }
        public string Key { get; set; }
        public int DownloadCalendarKeyStatusId { get; set; }
        public int UserId { get; set; }
        public string RegionId { get; set; }
        private string _language;
        public string Language
        {
            get { return _language; }
            set { _language = value.Trim(); }
        }
        public string SpecialAudience { get; set; }
        public string SegmentationIds { get; set; }
        public string MarketIdentifiers { get; set; }
        public string CampaignCategoryIds { get; set; }
        public DateTime DateKeyGenerated { get; set; }
        public int DownloadYear { get; set; }
    }
}