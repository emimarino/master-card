﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.GdpCorrelation)]
    public class GdpCorrelation
    {
        [Key]
        public int GdpCorrelationId { get; set; }
        public int HttpStatusCode { get; set; }
        public int XTotalCount { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime? UpdatedDateTime { get; set; }

        public virtual ICollection<GdpRequest> Requests { get; set; }
        public virtual ICollection<GdpResponse> Responses { get; set; }
    }
}