﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.AssetFullWidthRelatedProgram)]
    public class AssetFullWidthRelatedProgram
    {
        [Key, Column(Order = 0)]
        public string AssetFullWidthContentItemId { get; set; }
        [Key, Column(Order = 1)]
        public string ProgramContentItemId { get; set; }
    }
}