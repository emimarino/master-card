﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ContentTypeClientEvent)]
    public class ContentTypeClientEvent
    {
        [Key]
        public int ContentTypeClientEventId { get; set; }
        public string ContentTypeId { get; set; }
        public string Name { get; set; }
        public string FiredOn { get; set; }
        public string ClientEventCode { get; set; }
        public string ContentAction { get; set; }

        [ForeignKey("ContentTypeId")]
        public virtual ContentType ContentType { get; set; }
    }
}