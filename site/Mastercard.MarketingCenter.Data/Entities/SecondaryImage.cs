﻿using System.ComponentModel.DataAnnotations;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public abstract class SecondaryImage
    {
        [Key]
        public int SecondaryImageID { get; set; }
        public string ContentItemId { get; set; }
        public string ImageUrl { get; set; }
    }
}