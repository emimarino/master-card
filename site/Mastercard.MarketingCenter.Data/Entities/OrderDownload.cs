﻿using System;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class OrderDownload
    {
        public string IndividualFirstName { get; set; }
        public string IndividualLastName { get; set; }
        public string IndividualEmail { get; set; }
        public string LocalIssuerName { get; set; }
        public string NoteComment { get; set; }
        public string CID { get; set; }
        public string ImportedIssuerName { get; set; }
        public string RegionId { get; set; }
        public string RelatedProgram { get; set; }
        public string ItemName { get; set; }
        public string FulfilmentType { get; set; }
        public int ItemQuantity { get; set; }
        public int EstimatedDistribution { get; set; }
        public DateTime ActivityDate { get; set; }
    }
}