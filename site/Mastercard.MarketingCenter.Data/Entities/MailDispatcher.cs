﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.MailDispatcher)]
    public class MailDispatcher
    {
        [Key]
        public int MailDispatcherId { get; set; }
        public string FromAddress { get; set; }
        public string FromName { get; set; }
        public string ToAddress { get; set; }
        public string CCAddress { get; set; }
        public string BCCAddress { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string MailData { get; set; }
        public int UserId { get; set; }
        public string RegionId { get; set; }
        public int MailDispatcherStatusId { get; set; }
        public int FailedSendAttemptCount { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string TrackingType { get; set; }
        public int TotalLinks { get; set; }
        public bool IsBodyHtml { get; set; }
        public string AttachmentFilename { get; set; }
        public bool IsMultipart { get; set; }
        public string TextPlainBody { get; set; }
    }
}