﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ContentTypeFieldDefinition)]
    public class ContentTypeFieldDefinition
    {
        [Key, Column(Order = 0)]
        public string ContentTypeId { get; set; }
        [Key, Column(Order = 1)]
        public int FieldDefinitionId { get; set; }
        public int Order { get; set; }
        public bool VisibleInList { get; set; }
        public bool HiddenInNew { get; set; }
        public bool Searcheable { get; set; }
        public string DefaultOrder { get; set; }
        public string Permission { get; set; }
        public bool HiddenInHistory { get; set; }
        public bool InterRegionCloning { get; set; }
        public int? BusinessOwnerOrder { get; set; }
        public string EnabledBySetting { get; set; }
        public bool? Clonable { get; set; }
        public bool ReadOnlyInCrossBorderRegion { get; set; }

        [ForeignKey("FieldDefinitionId")]
        public virtual FieldDefinition FieldDefinition { get; set; }
    }
}