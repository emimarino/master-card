﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.CampaignEvent)]
    public class CampaignEvent
    {
        [Key]
        public string CampaignEventId { get; set; }
        public string Title { get; set; }
        private string _languageCode;
        public string LanguageCode
        {
            get { return _languageCode; }
            set { _languageCode = value.Trim(); }
        }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string RegionId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int ModifiedByUserId { get; set; }
        public bool AlwaysOn { get; set; }

        public virtual ICollection<CampaignCategory> CampaignCategories { get; set; }
        public virtual ICollection<ContentItemCampaignEvent> ContentItemCampaignEvents { get; set; }
    }
}