﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.Permission)]
    public class Permission
    {
        [Key]
        public int PermissionId { get; set; }
        public string Group { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}