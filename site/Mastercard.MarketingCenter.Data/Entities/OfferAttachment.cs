﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.OfferAttachment)]
    public class OfferAttachment : Attachment
    {
    }
}