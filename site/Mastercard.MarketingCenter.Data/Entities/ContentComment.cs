﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    public class ContentComment
    {
        public int ContentCommentId { get; set; }
        public DateTime CreationDate { get; set; }
        public string CommentBody { get; set; }

        public string ContentItemId { get; set; }
        [ForeignKey("ContentItemId")]
        public virtual ContentItemBase ContentItem { get; set; }

        public virtual int SenderUserId { get; set; }
        [ForeignKey("SenderUserId")]
        public virtual User SenderUser { get; set; }
    }
}