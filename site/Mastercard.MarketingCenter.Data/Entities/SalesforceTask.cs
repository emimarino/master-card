﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.SalesforceTask)]
    public class SalesforceTask
    {
        [Key]
        public int SalesforceTaskId { get; set; }
        public string ContactEmail { get; set; }
        public string ContactId { get; set; }
        public string AccountId { get; set; }
        public string OwnerId { get; set; }
        public string Status { get; set; }
        public string TaskActivityType { get; set; }
        public DateTime ActivityDate { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public string TaskId { get; set; }
        public int SalesforceActivityTypeFieldValue { get; set; }
        public int SalesforceActivityTypeId { get; set; }
        public int SalesforceTaskStatusId { get; set; }
        public int? SalesforceActivityReportId { get; set; }
        public string SalesforceContactResponse { get; set; }
        public string SalesforceTaskResponse { get; set; }
        public int FailedSendAttemptCount { get; set; }
        public string LoggingEventId { get; set; }

        [ForeignKey("SalesforceActivityTypeId")]
        public virtual SalesforceActivityType SalesforceActivityType { get; set; }
        [ForeignKey("SalesforceTaskStatusId")]
        public virtual SalesforceTaskStatus SalesforceTaskStatus { get; set; }
        [ForeignKey("SalesforceActivityReportId")]
        public virtual SalesforceActivityReport SalesforceActivityReport { get; set; }
    }
}