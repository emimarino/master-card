﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.Webinar)]
    public class Webinar
    {
        [Key]
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string ThumbnailImage { get; set; }
        public string LongDescription { get; set; }
        public string ShowInCrossSellBox { get; set; }
        public string PriorityLevel { get; set; }
        public bool? ShowInTagBrowser { get; set; }
        public DateTime? Date { get; set; }
        public string Link { get; set; }
        public string LinkText { get; set; }
        public string IconID { get; set; }
        public string AssetType { get; set; }
        public string VideoFile { get; set; }
        public string RestrictToSpecialAudience { get; set; }
        public string StaticVideoImage { get; set; }
        public bool? ShowImageInsteadVideo { get; set; }
        public bool EnableFavoriting { get; set; }
    }
}