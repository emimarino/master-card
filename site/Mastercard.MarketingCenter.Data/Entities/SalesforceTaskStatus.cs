﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.SalesforceTaskStatus)]
    public class SalesforceTaskStatus
    {
        [Key]
        public int SalesforceTaskStatusId { get; set; }
        public string Name { get; set; }
    }
}