﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.DeletedOrphanFile)]
    public class DeletedOrphanFile
    {
        [Key]
        public int DeletedOrphanFileId { get; set; }
        public string FolderKey { get; set; }
        public string FileFullName { get; set; }
        public DateTime FileDeletedDate { get; set; }
    }
}