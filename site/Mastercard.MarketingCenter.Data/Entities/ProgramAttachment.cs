﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ProgramAttachment)]
    public class ProgramAttachment : Attachment
    {
    }
}