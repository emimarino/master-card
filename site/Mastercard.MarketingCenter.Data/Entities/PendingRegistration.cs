﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.PendingRegistration)]
    public class PendingRegistration
    {
        [Key]
        public int PendingRegistrationID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FinancialInstitution { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public int StateID { get; set; }
        public string Zip { get; set; }
        public DateTime DateAdded { get; set; }
        public Guid GUID { get; set; }
        public string Status { get; set; }
        public DateTime? DateStatusChanged { get; set; }
        public string RejectionReason { get; set; }
        public string StatusChangedBy { get; set; }
        public string Title { get; set; }
        public string Processor { get; set; }
        public string Province { get; set; }
        public string Mobile { get; set; }
        public string Country { get; set; }

        private string _language;
        public string Language
        {
            get { return _language; }
            set { _language = value?.Trim(); }
        }
        public string RegionId { get; set; }
        public string AcceptedConsents { get; set; }
        public bool AcceptedEmailSubscriptions { get; set; }

        public virtual State State { get; set; }
    }
}