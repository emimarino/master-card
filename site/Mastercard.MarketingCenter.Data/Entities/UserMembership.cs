﻿namespace Mastercard.MarketingCenter.Data.Entities
{
    public class UserMembership
    {
        public User User { get; set; }
        public UserProfile Profile { get; set; }
        public Aspnet_Membership Membership { get; set; }
    }
}