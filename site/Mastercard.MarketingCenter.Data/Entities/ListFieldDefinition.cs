﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.ListFieldDefinition)]
    public class ListFieldDefinition
    {
        [Key, Column(Order = 0)]
        public int ListId { get; set; }
        [Key, Column(Order = 1)]
        public int FieldDefinitionId { get; set; }
        public int Order { get; set; }
        public bool VisibleInList { get; set; }
        public bool HiddenInNew { get; set; }
        public bool Searcheable { get; set; }
        public string DefaultOrder { get; set; }
        public string Permission { get; set; }
        public string RequiredInRegions { get; set; }
        public string EnabledBySetting { get; set; }

        [ForeignKey("FieldDefinitionId")]
        public virtual FieldDefinition FieldDefinition { get; set; }
    }
}