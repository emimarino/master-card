﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.MostPopularActivity)]
    public class MostPopularActivity
    {
        [Key]
        public long Id { get; set; }
        public string ContentTypeId { get; set; }
        public string ContentItemId { get; set; }
        public string RegionId { get; set; }
        public DateTime LastVisitDate { get; set; }
        public int VisitCount { get; set; }
        public long MostPopularProcessId { get; set; }

        public virtual MostPopularProcess MostPopularProcess { get; set; }
    }
}