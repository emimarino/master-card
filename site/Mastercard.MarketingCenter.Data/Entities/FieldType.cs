﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.FieldType)]
    public class FieldType
    {
        [Key]
        public string FieldTypeCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool? ShouldContainData { get; set; }

        public bool IsIdentifierField()
        {
            return FieldTypeCode == GuidPrimaryKey || FieldTypeCode == IntPrimaryKey;
        }

        #region FieldTypes

        public const string HtmlText = "HtmlText";
        public const string Checkbox = "Checkbox";
        public const string SingleLineText = "SingleLineText";
        public const string MultiLineText = "MultiLineText";
        public const string SingleLookup = "SingleLookup";
        public const string MultipleLookup = "MultipleLookup";
        public const string MultipleLookupGroup = "MultipleLookupGroup";
        public const string Lookup = "Lookup";
        public const string GuidPrimaryKey = "GuidPrimaryKey";
        public const string IntPrimaryKey = "IntPrimaryKey";
        public const string Image = "Image";
        public const string DateTime = "DateTime";
        public const string Tag = "Tag";
        public const string Boolean = "Boolean";
        public const string Number = "Number";
        public const string FileUpload = "FileUpload";
        public const string Customization = "Customization";
        public const string FeatureOn = "FeatureOn";
        public const string Attachment = "Attachment";
        public const string VanityUrl = "VanityUrl";
        public const string TrackingCreatedDate = "TrackingCreatedDate";
        public const string TrackingUpdatedDate = "TrackingUpdatedDate";
        public const string TrackingCreatedBy = "TrackingCreatedBy";
        public const string TrackingModifiedBy = "TrackingModifiedBy";
        public const string Translations = "Translations";
        public const string Region = "Region";
        public const string RegionInt = "RegionInt";
        public const string RegionTitle = "RegionTitle";
        public const string TriggerMarketing = "TriggerMarketing";
        public const string Hidden = "Hidden";
        public const string GuidHiddenIfEmpty = "GuidHiddenIfEmpty";
        public const string CampaignEvents = "CampaignEvents";
        public const string ColorChoice = "ColorChoice";
        public const string UserLookup = "UserLookup";
        public const string SecondaryImage = "SecondaryImage";
        public const string Location = "Location";

        public static readonly IEnumerable<string> FileFieldsTypes = new HashSet<string> { Attachment, FileUpload, Image, SecondaryImage };
        public static readonly IEnumerable<string> FileListFieldsTypes = new HashSet<string> { Attachment, SecondaryImage };
        public static readonly IEnumerable<string> TrackingFieldsTypes = new HashSet<string> { TrackingCreatedDate, TrackingUpdatedDate, TrackingModifiedBy, TrackingCreatedBy };

        #endregion
    }
}