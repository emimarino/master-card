﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mastercard.MarketingCenter.Data.Entities
{
    [Table(MarketingCenterDbConstants.Tables.Region)]
    public class Region
    {
        [Key]
        [Obsolete("Use IdTrimmed instead")]
        public string Id { get; set; }
        public string Name { get; set; }
        public string TagId { get; set; }

        [ForeignKey("TagId")]
        public virtual Tag Tag { get; set; }

        [NotMapped]
        public string IdTrimmed
        {
            get
            {
#pragma warning disable 0618
                return !string.IsNullOrEmpty(Id) ? Id.Trim() : null;
#pragma warning restore 0618
            }
        }

        public virtual ICollection<RoleRegion> Roles { get; set; }
    }
}