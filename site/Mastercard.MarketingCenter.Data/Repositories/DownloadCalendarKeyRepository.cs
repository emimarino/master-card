﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class DownloadCalendarKeyRepository : Repository<MarketingCenterDbContext>, IDownloadCalendarKeyRepository
    {
        public DownloadCalendarKeyRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public DownloadCalendarKey GetDownloadCalendarKeyByKey(string key)
        {
            return Context.DownloadCalendarKeys.FirstOrDefault(dck => dck.Key.Equals(key, StringComparison.OrdinalIgnoreCase));
        }

        public DownloadCalendarKey InsertDownloadCalendarKey(DownloadCalendarKey downloadCalendarKey)
        {
            Insert(downloadCalendarKey);
            Commit();

            return downloadCalendarKey;
        }
    }
}