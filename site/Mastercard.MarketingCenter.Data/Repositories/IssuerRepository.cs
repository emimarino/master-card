﻿using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class IssuerRepository : Repository<MarketingCenterDbContext>, IIssuerRepository
    {
        public IssuerRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public IQueryable<Issuer> GetAll()
        {
            return Context.Issuers;
        }

        public IQueryable<Issuer> GetAllByRegionId(string regionId)
        {
            return GetAll().Include(x => x.Processors)
                           .Where(i => i.RegionId == regionId);
        }

        public Issuer GetIssuerById(string id)
        {
            return GetAll().Include(x => x.Users.Select(u => u.Profile))
                           .Include(x => x.Processors)
                           .FirstOrDefault(i => i.IssuerId == id);
        }

        public Issuer GetIssuerByDomain(string domain)
        {
            return GetAll().FirstOrDefault(i => i.DomainName.ToLower().Contains(domain.ToLower()));
        }

        public Issuer GetIssuerByCid(string cid)
        {
            return GetAll().FirstOrDefault(i => i.Cid == cid);
        }

        public IQueryable<IssuerSegmentation> GetAllIssuerSegmentations()
        {
            return Context.IssuerSegmentations;
        }

        public IQueryable<Segmentation> GetAllSegmentations()
        {
            return Context.Segmentations;
        }

        public IEnumerable<RetrieveIssuersByProcessorDTO> GetIssuersByProcessor(string processorId, string regionId)
        {
            var sqlParams = new SqlParameter[]
            {
                new SqlParameter { ParameterName = "@processorId",  Value = processorId, Direction = System.Data.ParameterDirection.Input},
                new SqlParameter { ParameterName = "@regionId",  Value = regionId, Direction = System.Data.ParameterDirection.Input }
            };

            return Context.Database.SqlQuery<RetrieveIssuersByProcessorDTO>("exec RetrieveIssuersByProcessor @processorId, @regionId", sqlParams);
        }

        public IQueryable<DomainBlacklist> GetDomainBlackLists()
        {
            return Context.DomainBlacklists;
        }

        public void DeleteIssuer(Issuer issuer)
        {
            Delete(issuer);
        }

        public IQueryable<MasterICA> GetAllMasterICAs()
        {
            return Context.MasterICAs;
        }

        public MasterICA GetMasterICAByIca(int ica)
        {
            return GetAllMasterICAs().FirstOrDefault(i => i.Ica == ica);
        }

        public IQueryable<MasterICA> GetMasterICAByMasterIca(int masterIca)
        {
            return GetAllMasterICAs().Where(i => i.MasterIca == masterIca);
        }

        public void DeleteMasterICA(MasterICA masterIca)
        {
            Delete(masterIca);
        }
    }
}