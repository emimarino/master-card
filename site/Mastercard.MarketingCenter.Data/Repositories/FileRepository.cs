﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class FileRepository : Repository<MarketingCenterDbContext>, IFileRepository
    {
        public FileRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public IEnumerable<ViewAllFiles> GetViewAllFiles()
        {
            return Context.Database.SqlQuery<ViewAllFiles>($"SELECT * FROM {MarketingCenterDbConstants.Tables.ViewAllFiles}");
        }

        public IEnumerable<ViewCopyFiles> GetViewCopyFiles()
        {
            return Context.Database.SqlQuery<ViewCopyFiles>($"SELECT * FROM {MarketingCenterDbConstants.Tables.ViewCopyFiles}");
        }

        public IQueryable<ReviewedFile> GetReviewedFiles()
        {
            return Context.ReviewedFiles;
        }

        public void InsertDeletedOrphanFile(DeletedOrphanFile deletedOrphanFile)
        {
            Insert(deletedOrphanFile);
        }

        public void InsertFileReviewProcess(FileReviewProcess fileReviewProcess)
        {
            Insert(fileReviewProcess);
        }

        public void InsertReviewedFile(ReviewedFile reviewedFile)
        {
            Insert(reviewedFile);
        }

        public void CleanUpReviewedFiles()
        {
            Context.Database.ExecuteSqlCommand($"TRUNCATE TABLE {MarketingCenterDbConstants.Tables.ReviewedFile}");
        }
    }
}