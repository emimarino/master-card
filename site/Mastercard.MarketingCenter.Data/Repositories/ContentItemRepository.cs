﻿using Mastercard.MarketingCenter.Data.Entities;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class ContentItemRepository : Repository<MarketingCenterDbContext>
    {
        readonly private RegionRepository _regionRepository;
        public ContentItemRepository(MarketingCenterDbContext context, RegionRepository regionRepository) : base(context)
        {
            _regionRepository = regionRepository;
        }

        public IQueryable<ContentItemBase> GetAll()
        {
            return Context.ContentItems;
        }

        public void SetReviewState(string contentItemId, int state)
        {
            var contentItem = GetById(contentItemId.Replace("-p", string.Empty));
            contentItem.ReviewStateId = state;
        }

        public IEnumerable<string> GetRegionNames(IEnumerable<ContentItemCloneToRegion> ApplicableCloneToRegion)
        {
            return _regionRepository.GetAll()
                                    .Where(r => ApplicableCloneToRegion.Any(actr => actr.RegionId.Trim().Equals(r.IdTrimmed, StringComparison.CurrentCultureIgnoreCase)))
                                    .Select(r => r.Name);
        }

        public IQueryable<ContentItemCloneToRegion> GetApplicableCloneToRegions(string contentItemId)
        {
            return Context.ContentItemCloneToRegions.Where(cictr => cictr.ContentItemId.Equals(contentItemId));
        }

        public bool IsApplicableCrossBorderRegion(string contentItemId, string regionId)
        {
            return Context.ContentItemCrossBorderRegions.Any(cicbr => cicbr.ContentItemId.Equals(contentItemId, StringComparison.InvariantCultureIgnoreCase)
                                                                   && cicbr.RegionId.Trim().Equals(regionId.Trim(), StringComparison.InvariantCultureIgnoreCase));
        }

        public ContentItemBase GetOnlyDraftById(string contentItemId)
        {
            var items = Context.ContentItems.FirstOrDefault(ci => ci.PrimaryContentItemId.Equals(contentItemId.Replace("-p", ""), StringComparison.OrdinalIgnoreCase) &&
                            (ci.StatusID == Status.DraftOnly || (ci.StatusID == Status.PublishedAndDraft && ci.ContentItemId.EndsWith("-p"))));

            if (items == null)
            {
                items = GetById(contentItemId);
            }

            return items;
        }

        public ContentItemBase GetById(string contentItemId)
        {
            return Context.ContentItems.FirstOrDefault(ci => ci.ContentItemId.Equals(contentItemId, StringComparison.OrdinalIgnoreCase));
        }

        public ContentItemBase GetByFrontEndUrl(string url)
        {
            return Context.ContentItems.FirstOrDefault(ci => ci.FrontEndUrl.Equals(url, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// This Method is not only a get but It also updates or pouplates the table ContentAboutToExpire before retrieving the data
        /// </summary>
        /// <param name="toExpireInDays"></param>
        /// <returns></returns>
        public IEnumerable<ContentAboutToExpire> GetContentAboutToExpire(int toExpireInDays)
        {
            Context.Database.ExecuteSqlCommand("Exec ProcessExpiredNotifications @DaysToExpire", new SqlParameter() { ParameterName = "DaysToExpire", Value = toExpireInDays });
            //Retrieves data from newly update table.
            return Context.ContentAboutToExpire.Include(a => a.BusinessOwner); //0 is the ReviewStateId of the not reviewd Asset
        }

        /// <summary>
        /// This method intends to set the asset as "reviewed" or "not reviewed"
        /// </summary>
        /// <param name="contentItemId"></param>
        /// <param name="state">It defaults to teh non available state as if the asset is not expired nor reviewed</param>
        /// <returns></returns>
        public bool UpdateReviewState(ContentItemBase contentItem, int state = 0)
        {
            if (contentItem != null)
            {
                contentItem.ReviewStateId = state;
                return true; ///in case the asset is not valid anymore
            }

            return false;
        }

        public void UpdateStatus(ContentItemBase contentItem, int statusId, int userId)
        {
            contentItem.StatusID = statusId;
            contentItem.ModifiedDate = DateTime.Now;
            contentItem.ModifiedByUserId = userId;
        }

        public void UpdateStatus(string ContentItemId, int statusId, int userId)
        {
            var contentItem = Context.ContentItems.FirstOrDefault(ci => ci.ContentItemId.Equals(ContentItemId));
            if (contentItem != null)
            {
                UpdateStatus(contentItem, statusId, userId);
            }
        }

        public ContentType GetContentType(string contentTypeId)
        {
            return Context.ContentTypes.FirstOrDefault(x => x.ContentTypeId == contentTypeId);
        }

        public IEnumerable<ContentItemSegmentation> GetSegmentation(string contentItemId)
        {
            return Context.ContentItemSegmentations.Where(x => x.ContentItemId == contentItemId);
        }

        public IEnumerable<Entities.Tag> GetTags(string contentItemId)
        {
            return Context.Tags.Where(t => t.ContentItems.Any(ci => ci.ContentItemId == contentItemId));
        }

        public IQueryable<Program> GetAllPrograms()
        {
            return Context.Programs.Include(p => p.ContentItem.Tags.Select(t => t.TagHierarchyPositions));
        }

        public AssetFullWidth GetAssetFullWidth(string contentItemId)
        {
            return Context.AssetFullWidths.FirstOrDefault(x => x.ContentItemId == contentItemId);
        }

        public Asset GetAsset(string contentItemId)
        {
            return Context.Assets.FirstOrDefault(x => x.ContentItemId == contentItemId);
        }

        public OrderableAsset GetOrderableAsset(string contentItemId)
        {
            return Context.OrderableAssets.Include(oa => oa.PricingSchedule)
                                          .FirstOrDefault(oa => oa.ContentItemId == contentItemId);
        }

        public DownloadableAsset GetDownloadableAsset(string contentItemId)
        {
            return Context.DownloadableAssets.FirstOrDefault(x => x.ContentItemId == contentItemId);
        }

        public Program GetProgram(string contentItemId)
        {
            return Context.Programs.Include(p => p.ContentItem.Tags.Select(t => t.TagHierarchyPositions))
                                   .FirstOrDefault(x => x.ContentItemId == contentItemId);
        }

        public Webinar GetWebinar(string contentItemId)
        {
            return Context.Webinars.FirstOrDefault(x => x.ContentItemId == contentItemId);
        }

        public IQueryable<ContentItemBase> GetAllByPrimaryContentItemId(string primaryContentItemId)
        {
            return Context.ContentItems.Where(x => x.PrimaryContentItemId == primaryContentItemId);
        }

        public IQueryable<ContentItemRelatedItem> GetContentItemRelatedItems(string contentItemId)
        {
            return Context.ContentItemRelatedItems.Where(ciri => ciri.ContentItemId.Equals(contentItemId));
        }

        public IQueryable<Asset> GetAssets()
        {
            return Context.Assets.Include(x => x.BusinessOwner);
        }

        public IQueryable<AssetFullWidth> GetAssetsFullWidth()
        {
            return Context.AssetFullWidths.Include(x => x.BusinessOwner);
        }

        public IQueryable<DownloadableAsset> GetDownloadableAssets()
        {
            return Context.DownloadableAssets.Include(x => x.BusinessOwner);
        }

        public IQueryable<OrderableAsset> GetOrderableAssets()
        {
            return Context.OrderableAssets.Include(x => x.BusinessOwner);
        }

        public IQueryable<Page> GetPages()
        {
            return Context.Pages;
        }

        public IQueryable<Program> GetPrograms()
        {
            return Context.Programs.Include(x => x.BusinessOwner);
        }

        public IQueryable<ContentItemBase> GetContentItems()
        {
            return Context.ContentItems
                .Include(ci => ci.Status)
                .Include(ci => ci.ContentType)
                .Include(ci => ci.Region)
                .Include(ci => ci.CreatedByUser);
        }
    }
}