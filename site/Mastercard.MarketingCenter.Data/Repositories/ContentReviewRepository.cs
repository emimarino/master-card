﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class ContentReviewRepository : IContentReviewRepository
    {
        private readonly MarketingCenterDbContext _context;
        private IEnumerable<ExpirationReviewState> _entities = null;

        public ContentReviewRepository(MarketingCenterDbContext context)
        {
            _context = context;
        }

        public IEnumerable<ExpirationReviewState> GetExpirationReviewStates()
        {
            if (_entities == null || !_entities.Any())
            {
                _entities = _context.ExpirationReviewStates;
            }

            return _entities;
        }
    }
}