﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class ConsentFileDataRepository : Repository<MarketingCenterDbContext>, IConsentFileDataRepository
    {
        public ConsentFileDataRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public void InsertConsentFileData(ConsentFileData consentFile)
        {
            Insert(consentFile);
            Commit();
        }

        public void InsertUserConsent(UserConsent userConsent)
        {
            Insert(userConsent);
        }

        public void UpdateConsentFileData(ConsentFileData consentFile)
        {
            Update(consentFile);
            Commit();
        }

        public IQueryable<ConsentFileData> GetAll()
        {
            return Context.ConsentFileData;
        }

        public IQueryable<ConsentFileData> GetByIds(IEnumerable<int> IDs)
        {
            return Context.ConsentFileData.Where(cfd => IDs.Contains(cfd.ConsentFileDataId));
        }

        public void Insert(IList<ConsentFileData> consentFiles)
        {
            foreach (var consentFile in consentFiles)
            {
                Context.ConsentFileData.Add(consentFile);
            }

            Commit();
        }

        public IQueryable<ConsentFileData> GetLatestConsents(string locale = null, string functionCode = null, string useCode = null)
        {
            var grouped = Context.ConsentFileData.Where(cfd => (string.IsNullOrEmpty(locale) || cfd.Locale.Equals(locale, StringComparison.OrdinalIgnoreCase)) &&
                                                               (string.IsNullOrEmpty(functionCode) || cfd.ServiceFunctionCode.Equals(functionCode, StringComparison.OrdinalIgnoreCase)) &&
                                                               (string.IsNullOrEmpty(useCode) || cfd.UseCategoryCode.Equals(useCode, StringComparison.OrdinalIgnoreCase)))
                                                 .GroupBy(cfd => new
                                                 {
                                                     cfd.Locale,
                                                     cfd.UseCategoryCode,
                                                     cfd.DocumentType,
                                                     cfd.ServiceFunctionCode
                                                 });

            return grouped.Select(t => t.FirstOrDefault(i => i.CurrentVersion.Equals(t.Max(t2 => t2.CurrentVersion))));
        }
        public ConsentFileData GetLatestConsentLanguageText(string locale = null, string functionCode = null)
        {
            return GetLatestConsents(locale, functionCode, null).FirstOrDefault(cfd => cfd.DocumentType.Equals("consentlanguagetext", StringComparison.OrdinalIgnoreCase));
        }

        public ConsentFileData GetLatestLegalConsentData(string locale = null, string useCode = null, string functionCode = null)
        {
            return GetLatestConsents(locale, null, useCode).FirstOrDefault(cfd => !cfd.DocumentType.Equals("consentlanguagetext", StringComparison.OrdinalIgnoreCase));
        }

    }
}