﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class MailDispatcherRepository : Repository<MarketingCenterDbContext>
    {
        private readonly MarketingCenterDbContext _context;

        public MailDispatcherRepository(MarketingCenterDbContext context) : base(context)
        {
            _context = context;
        }

        public IQueryable<MailDispatcher> GetAll()
        {
            return _context.MailDispatchers;
        }

        public MailDispatcher GetMailDispatcherById(int mailDispatcherId)
        {
            return _context.MailDispatchers.FirstOrDefault(md => md.MailDispatcherId == mailDispatcherId);
        }
    }
}