﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class WebRoleAccessRepository : Repository<MarketingCenterDbContext>
    {
        public WebRoleAccessRepository(MarketingCenterDbContext context)
            : base(context)
        { }

        public IEnumerable<WebRoleAccess> GetByRoute(string area, string controller, string action, string id)
        {
            return Context.WebRoleAccesses.Where(x => (string.IsNullOrEmpty(area) || string.Compare(x.Area, area, StringComparison.InvariantCultureIgnoreCase) == 0)
                && string.Compare(x.Controller, controller, StringComparison.InvariantCultureIgnoreCase) == 0
                && (string.IsNullOrEmpty(action) || string.IsNullOrEmpty(x.Action) || string.Compare(x.Action, action, StringComparison.InvariantCultureIgnoreCase) == 0)
                && (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(x.Id) || string.Compare(x.Id, id, StringComparison.InvariantCultureIgnoreCase) == 0));
        }

        public IQueryable<WebRoleAccess> GetAll()
        {
            return Context.WebRoleAccesses;
        }
    }
}