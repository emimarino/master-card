﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class CampaignEventRepository : Repository<MarketingCenterDbContext>, ICampaignEventRepository
    {
        public CampaignEventRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public IQueryable<CampaignEvent> GetCampaignEvents()
        {
            return Context.CampaignEvents;
        }

        public IQueryable<ContentItemCampaignEvent> GetContentItemCampaignEvents()
        {
            return Context.ContentItemCampaignEvents.Include(pce => pce.ContentItemForCalendar.ContentItem.Tags.Select(m => m.TagHierarchyPositions))
                                                    .Include(pce => pce.CampaignEvent.CampaignCategories.Select(ct => ct.CampaignCategoryTranslatedContents))
                                                    .Include(pce => pce.ContentItemForCalendar.ContentItem.ContentType);
        }

        public IQueryable<ContentItemCampaignEvent> FilterMarketingCalendarEnabled(IQueryable<ContentItemCampaignEvent> events, bool isEnabled = true)
        {
            var result = events.Include(e => e.ContentItemForCalendar);
            if (isEnabled)
            {
                result = result.Where(r => r.ContentItemForCalendar.ShowInMarketingCalendar);
            }

            return result;
        }
    }
}