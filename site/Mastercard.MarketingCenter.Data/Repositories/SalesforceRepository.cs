﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Interfaces;
using Mastercard.MarketingCenter.DTO.Salesforce;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Repositories
{
    public class SalesforceRepository : Repository<MarketingCenterDbContext>, ISalesforceRepository
    {
        private readonly IUserRepository _userRepository;

        public SalesforceRepository(MarketingCenterDbContext context, IUserRepository userRepository) : base(context)
        {
            _userRepository = userRepository;
        }

        public int CreateSalesforceActivityReport()
        {
            var report = Context.SalesforceActivityReports.Add(new SalesforceActivityReport { StartDate = DateTime.Now, });
            Commit();

            return report.SalesforceActivityReportId;
        }

        public int SaveSalesforceToken(SalesforceAccessToken salesforceAccessToken)
        {
            var token = Context.SalesforceAccessTokens.FirstOrDefault(t => t.AccessToken.Equals(salesforceAccessToken.AccessToken));
            if (token == null)
            {
                token = Context.SalesforceAccessTokens.Add(salesforceAccessToken);
            }
            else
            {
                token.Scope = salesforceAccessToken.Scope;
                token.InstanceUrl = salesforceAccessToken.InstanceUrl;
                token.TokenId = salesforceAccessToken.TokenId;
                token.TokenType = salesforceAccessToken.TokenType;
                token.ModifiedDate = DateTime.Now;
            }

            Commit();

            return token.SalesforceTokenId;
        }

        public SalesforceActivityReport GetSalesforceActivityReportById(int salesforceActivityReportId)
        {
            return Context.SalesforceActivityReports.FirstOrDefault(r => r.SalesforceActivityReportId.Equals(salesforceActivityReportId));
        }

        public IEnumerable<SalesforceDownloadActivityDTO> GetDownloadActivities(DateTime startDateTime, DateTime endDateTime)
        {
            int downloadId = GetLastIdByType(MarketingCenterDbConstants.Salesforce.ActivityType.Download);
            return from d in Context.DomoDownloadsApis
                   join ci in Context.ContentItems on d.ContentItemId equals ci.ContentItemId
                   where d.DownloadDate >= startDateTime && d.DownloadDate < endDateTime && d.SiteTrackingId > downloadId
                   select new SalesforceDownloadActivityDTO
                   {
                       Id = d.SiteTrackingId,
                       Email = d.UserEmail,
                       Region = d.ContentItemRegion,
                       Date = d.DownloadDate,
                       Name = d.ContentItemTitle,
                       File = d.FileName,
                       Url = ci.FrontEndUrl
                   };
        }

        public IEnumerable<SalesforceOrderActivityDTO> GetOrderActivities(DateTime startDateTime, DateTime endDateTime)
        {
            int orderId = GetLastIdByType(MarketingCenterDbConstants.Salesforce.ActivityType.Order);
            return from o in Context.Orders
                   join oi in Context.OrderItems on o.OrderId equals oi.OrderId
                   join ci in Context.ContentItems on oi.ItemId equals ci.ContentItemId
                   join u in _userRepository.GetAll().Include(u => u.Issuer.Processors)
                                            .Where(u => !u.Issuer.IsMastercardIssuer
                                                     && !u.Issuer.Processors.Any(ip => ip.ProcessorId == MarketingCenterDbConstants.Processors.APlus))
                   on o.UserId equals u.UserName
                   where o.OrderDate >= startDateTime && o.OrderDate <= endDateTime && o.OrderStatus != MarketingCenterDbConstants.OrderStatus.Cancelled
                   && !oi.ElectronicDelivery && oi.OrderItemId > orderId
                   select new SalesforceOrderActivityDTO
                   {
                       Id = oi.OrderItemId,
                       Region = ci.RegionId,
                       Email = u.Email,
                       Date = o.OrderDate,
                       Name = oi.ItemName,
                       Quantity = oi.ItemQuantity,
                       Url = ci.FrontEndUrl
                   };
        }

        public IEnumerable<SalesforceUserActivityDTO> GetUserActivities(DateTime startDateTime, DateTime endDateTime)
        {
            int userId = GetLastIdByType(MarketingCenterDbConstants.Salesforce.ActivityType.User);
            return from am in Context.Aspnet_Membership
                   join u in _userRepository.GetAll() on am.Email equals u.Email
                   where am.CreateDate >= startDateTime && am.CreateDate <= endDateTime && u.UserId > userId
                   select new SalesforceUserActivityDTO
                   {
                       Id = u.UserId,
                       Region = u.Region,
                       Email = u.Email,
                       Date = am.CreateDate
                   };
        }

        private int GetLastIdByType(int salesforceActivityTypeId)
        {
            return GetSalesforceTasks().Where(x => x.SalesforceActivityTypeId == salesforceActivityTypeId)
                                       .Max(x => (int?)x.SalesforceActivityTypeFieldValue) ?? 0;
        }

        public void UpdateSalesforceActivityReportById(int salesforceActivityReportId, DateTime? endDate = null, int? sent = null, int? failed = null, bool isSuccessed = false, int? salesforceTokenId = null, string loggingEventId = null)
        {
            var salesforceActivityReport = GetSalesforceActivityReportById(salesforceActivityReportId);
            if (salesforceActivityReport != null)
            {
                salesforceActivityReport.EndDate = endDate ?? salesforceActivityReport.EndDate;
                salesforceActivityReport.Sent = sent ?? salesforceActivityReport.Sent;
                salesforceActivityReport.Failed = failed ?? salesforceActivityReport.Failed;
                salesforceActivityReport.IsSuccessed = isSuccessed;
                salesforceActivityReport.SalesforceTokenId = salesforceTokenId ?? salesforceActivityReport.SalesforceTokenId;
                salesforceActivityReport.LoggingEventId = loggingEventId ?? salesforceActivityReport.LoggingEventId;

                Commit();
            }
        }

        public void AddSalesforceTasks(IEnumerable<SalesforceTask> salesforceTasks)
        {
            Context.SalesforceTasks.AddRange(salesforceTasks);
            Commit();
        }

        public IQueryable<SalesforceTask> GetSalesforceTasks()
        {
            return Context.SalesforceTasks;
        }

        public IQueryable<SalesforceTask> GetSalesforceTasks(DateTime from, DateTime to)
        {
            to = to.Date.AddDays(1);
            return GetSalesforceTasks().Where(st => st.ActivityDate >= from && st.ActivityDate < to);
        }
    }
}