﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Interfaces;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Repositories
{
    public class EventLocationRepository : Repository<MarketingCenterDbContext>, IEventLocationRepository
    {
        public EventLocationRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public IQueryable<EventLocation> GetEventLocations()
        {
            return Context.EventLocations
                .Include(e => e.EventLocationPositionHierarchy)
                .Include(e => e.Offers.Select(o => o.ContentItem));
        }

        public EventLocation AddEventLocation(EventLocation eventLocation)
        {
            return Insert(eventLocation);
        }

        public int? GetEventLocationHierarchyPositionId(int parentEventLocationId)
        {
            return Context.EventLocationPositionHierarchies.FirstOrDefault(h => h.EventLocationId.Equals(parentEventLocationId))?.PositionId;
        }

        public EventLocationPositionHierarchy AddEventLocationToHierarchy(EventLocationPositionHierarchy eventLocationHierarchy)
        {
            return Insert(eventLocationHierarchy);
        }

        public void DeleteEventLocationById(int id)
        {
            var eventLocation = GetEventLocationById(id);
            if (eventLocation.Equals(null))
            {
                return;
            }

            eventLocation.Offers.ToList().ForEach(o => eventLocation.Offers.Remove(o));
            Context.EventLocations.Remove(eventLocation);
        }

        public EventLocationPositionHierarchy GetHierarchyByEventLocationId(int id)
        {
            return Context.EventLocationPositionHierarchies
                .Include(h => h.EventLocation)
                .FirstOrDefault(e => e.EventLocationId.Equals(id));
        }

        public EventLocation GetEventLocationById(int id)
        {
            return Context.EventLocations.Include(e => e.Offers).FirstOrDefault(e => e.EventLocationId.Equals(id));
        }

        public EventLocationPositionHierarchy UpdateEventLocationHierarchy(EventLocationPositionHierarchy eventLocationHierarchy)
        {
            return Update(eventLocationHierarchy);
        }

        public IQueryable<EventLocation> GetRelatedEventLocationsByContentItemId(string contentItemId)
        {
            return Context.EventLocations.Where(e => e.Offers.Any(o => o.ContentItemId.Equals(contentItemId)));
        }
    }
}