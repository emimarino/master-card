﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class TagRepository : Repository<MarketingCenterDbContext>
    {
        public TagRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public IQueryable<Tag> GetTags()
        {
            return Context.Tags;
        }

        public IQueryable<Tag> GetTags(params string[] tagIds)
        {
            return Context.Tags.Where(t => tagIds.Contains(t.TagID));
        }

        public IEnumerable<Tag> GetTagsByIdentifier(string[] identifiers)
        {
            return Context.Tags.Include(t => t.TagTranslatedContent)
                               .Where(t => identifiers.Contains(t.Identifier));
        }

        public IEnumerable<Tag> GetTagsByTagCategory(string tagCategoryId)
        {
            return Context.TagHierarchyPositions.Include(thp => thp.Tag)
                                                .Where(thp => thp.TagCategoryId == tagCategoryId)
                                                .Select(thp => thp.Tag);
        }
    }
}