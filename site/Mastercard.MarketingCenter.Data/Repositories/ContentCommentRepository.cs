﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class ContentCommentRepository : Repository<MarketingCenterDbContext>
    {
        public ContentCommentRepository(MarketingCenterDbContext context) : base(context) { }

        public IQueryable<ContentComment> GetCommentsPaged(string contentItemId, int pageNumber, int pageSize)
        {
            return Context.ContentComment.Include(cc => cc.SenderUser)
                .Where(cc => cc.ContentItemId.Equals(contentItemId, StringComparison.OrdinalIgnoreCase))
                .OrderByDescending(cc => cc.ContentCommentId)
                .Skip((pageNumber) * pageSize)
                .Take(pageSize);
        }

        //this method is for demonstrations examples only
        public IQueryable<IGrouping<string, ContentComment>> GetCommentsCount(int userId)
        {
            return Context.ContentComment
                .GroupBy(cc => cc.ContentItemId);
        }

        public int GetCommentsByContent(string contentItemId)
        {
            return Context.ContentComment.Include(cc => cc.SenderUser)
                .Where(cc => cc.ContentItemId.Equals(contentItemId.Replace("-p",""), StringComparison.OrdinalIgnoreCase))
                .Count();
        }

        public ContentComment Add(ContentComment contentComment)
        {
            return base.Insert(contentComment);
        }
    }
}