﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class CountryRepository : Repository<MarketingCenterDbContext>, ICountryRepository
    {
        public CountryRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public IQueryable<Country> GetAll()
        {
            return Context.Countries;
        }

        public Country GetByTagIdentifier(string tagIdentifier)
        {
            return GetAll().FirstOrDefault(u => u.TagIdentifier == tagIdentifier);
        }
    }
}