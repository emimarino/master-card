﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class IconRepository : Repository<MarketingCenterDbContext>
    {
        public IconRepository(MarketingCenterDbContext context) : base(context)
        { }

        public IEnumerable<Icon> GetAll()
        {
            return Context.Icons.ToList();
        }

        public Icon GetIcon(string iconId)
        {
            return Context.Icons.Where(x => x.IconID == iconId).FirstOrDefault();
        }

        /// <summary>
        /// Returns icon URL for an icon. Ideally icons should be stored in both CMS and Web projects and a root path is taken out ISettingsService
        /// </summary>
        /// <param name="icon"></param>
        /// <returns></returns>
        public string GetIconUrl(Icon icon)
        {
            if (icon == null)
            {
                return null;
            }

            return $"/portal/content/images/icons/{icon.Extension}";
        }
    }
}