﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class AssetRepository : Repository<MarketingCenterDbContext>
    {
        public AssetRepository(MarketingCenterDbContext context) : base(context)
        { }

        public IQueryable<AssetAttachment> GetAssetAttachments(string id)
        {
            return from aa in Context.AssetAttachments
                   where aa.ContentItemId == id
                   select aa;
        }

        public IQueryable<Tag> GetAssetBrowseAllMaterialTags(string id)
        {
            return from b in Context.AssetBrowseAllMaterialTags
                   join t in Context.Tags on b.TagID equals t.TagID
                   where b.ContentItemID == id
                   select t;
        }

        public AssetRelatedProgram GetAssetRelatedProgram(string id)
        {
            return (from d in Context.AssetRelatedPrograms
                    where d.AssetContentItemId == id
                    select d).FirstOrDefault();
        }

        public AssetFullWidthRelatedProgram GetAssetFullWidthRelatedProgram(string id)
        {
            return (from d in Context.AssetFullWidthRelatedPrograms
                    where d.AssetFullWidthContentItemId == id
                    select d).FirstOrDefault();
        }

        public IQueryable<DownloadableAssetAttachment> GetDownloadableAssetAttachments(string id)
        {
            return from aa in Context.DownloadableAssetAttachments
                   where aa.ContentItemId == id
                   select aa;
        }

        public DownloadableAssetEstimatedDistribution GetDownloadableAssetEstimatedDistributionById(int id)
        {
            return Context.DownloadableAssetEstimatedDistributions.FirstOrDefault(ed => ed.Id == id);
        }

        public DownloadableAssetEstimatedDistribution GetDownloadableAssetEstimatedDistributionByElectronicDeliveryId(string electronicDeliveryId)
        {
            if (Guid.TryParse(electronicDeliveryId, out Guid id))
            {
                return Context.DownloadableAssetEstimatedDistributions.Include(ed => ed.ElectronicDelivery.OrderItem.Order)
                                                                      .FirstOrDefault(ed => ed.ElectronicDeliveryId == id);
            }

            return null;
        }

        public DownloadableAssetEstimatedDistribution UpdateDownloadableAssetEstimatedDistribution(DownloadableAssetEstimatedDistribution downloadableAssetEstimatedDistribution)
        {
            return Update(downloadableAssetEstimatedDistribution);
        }

        public IQueryable<Tag> GetDownloadableAssetBrowseAllMaterialTags(string id)
        {
            return from b in Context.DownloadableAssetBrowseAllMaterialTags
                   join t in Context.Tags on b.TagID equals t.TagID
                   where b.ContentItemID == id
                   select t;
        }

        public IQueryable<DownloadableAssetRelatedProgram> GetDownloadableAssetRelatedPrograms()
        {
            return Context.DownloadableAssetRelatedPrograms;
        }

        public IQueryable<OrderableAssetRelatedProgram> GetOrderableAssetRelatedPrograms()
        {
            return Context.OrderableAssetRelatedPrograms;
        }

        public DownloadableAssetRelatedProgram GetDownloadableAssetRelatedProgram(string id)
        {
            return (from d in Context.DownloadableAssetRelatedPrograms
                    where d.DownloadableAssetContentItemId == id
                    select d).FirstOrDefault();
        }

        public IQueryable<OrderableAssetAttachment> GetOrderableAssetAttachments(string id)
        {
            return from aa in Context.OrderableAssetAttachments
                   where aa.ContentItemId == id
                   select aa;
        }

        public IQueryable<Tag> GetOrderableAssetBrowseAllMaterialTags(string id)
        {
            return from b in Context.OrderableAssetBrowseAllMaterialTags
                   join t in Context.Tags on b.TagID equals t.TagID
                   where b.ContentItemID == id
                   select t;
        }

        public OrderableAssetRelatedProgram GetOrderableAssetRelatedProgram(string id)
        {
            return (from d in Context.OrderableAssetRelatedPrograms
                    where d.OrderableAssetContentItemId == id
                    select d).FirstOrDefault();
        }

        public IQueryable<ProgramAttachment> GetProgramAttachments(string id)
        {
            return from aa in Context.ProgramAttachments
                   where aa.ContentItemId == id
                   select aa;
        }

        public IEnumerable<string> GetProgramRelatedAssetContentItemIds(string programContentItemId)
        {
            return (from d in Context.DownloadableAssetRelatedPrograms
                    where d.ProgramContentItemId == programContentItemId
                    select d.DownloadableAssetContentItemId)
                .Union(from o in Context.OrderableAssetRelatedPrograms
                       where o.ProgramContentItemId == programContentItemId
                       select o.OrderableAssetContentItemId)
                .Union(from o in Context.AssetRelatedPrograms
                       where o.ProgramContentItemId == programContentItemId
                       select o.AssetContentItemId)
                .Union(from o in Context.AssetFullWidthRelatedPrograms
                       where o.ProgramContentItemId == programContentItemId
                       select o.AssetFullWidthContentItemId);
        }

        public IQueryable<Tag> GetProgramBrowseAllMaterialTags(string id)
        {
            return from b in Context.ProgramBrowseAllMaterialTags
                   join t in Context.Tags on b.TagID equals t.TagID
                   where b.ContentItemID == id
                   select t;
        }

        public IQueryable<Tag> GetContentItemTags(string id)
        {
            return Context.Tags.Where(t => t.ContentItems.Any(ci => ci.ContentItemId == id));
        }

        public IQueryable<Tag> GetAssetFullWidthBrowseAllMaterialTags(string id)
        {
            return from b in Context.AssetFullWidthBrowseAllMaterialTags
                   join t in Context.Tags on b.TagID equals t.TagID
                   where b.ContentItemID == id
                   select t;
        }

        public DownloadableAssetEstimatedDistribution SaveDownloadableAssetEstimatedDistribution(string contentItemId, int estimatedDistribution, int userId, Guid? electronicDeliveryId)
        {
            var downloadableAssetEstimatedDistribution = new DownloadableAssetEstimatedDistribution
            {
                ContentItemId = contentItemId,
                UserId = userId,
                EstimatedDistribution = estimatedDistribution,
                ElectronicDeliveryId = electronicDeliveryId,
                DownloadDate = DateTime.UtcNow
            };

            Context.DownloadableAssetEstimatedDistributions.Add(downloadableAssetEstimatedDistribution);
            Context.SaveChanges();

            return downloadableAssetEstimatedDistribution;
        }

        public ElectronicDelivery CreateElectronicDelivery(string fileLocation, string contentItemId, string userName, string region)
        {
            var electronicDeliveryID = Guid.NewGuid();
            var electronicDelivery = new ElectronicDelivery
            {
                ElectronicDeliveryID = electronicDeliveryID,
                FileLocation = fileLocation,
                DownloadCount = Convert.ToInt32(ConfigurationManager.AppSettings["ElectronicDelivery.MaxDownload"]),
                ExpirationDate = DateTime.UtcNow.AddDays(Convert.ToInt32(ConfigurationManager.AppSettings["ElectronicDelivery.ExpirationDays"]))
            };

            Context.ElectronicDeliveries.Add(electronicDelivery);
            Context.ElectronicDeliveryCreateDatas.Add(
                new ElectronicDeliveryCreateData
                {
                    ElectronicDeliveryID = electronicDeliveryID,
                    ContentItemId = contentItemId,
                    UserName = userName,
                    Region = region
                }
            );

            Context.SaveChanges();

            return electronicDelivery;
        }

        public ElectronicDelivery GetElectronicDelivery(string electronicDeliveryId)
        {
            if (Guid.TryParse(electronicDeliveryId, out Guid id))
            {
                return Context.ElectronicDeliveries.Include(ed => ed.OrderItem.Order)
                                                   .FirstOrDefault(ed => ed.ElectronicDeliveryID == id);
            }

            return null;
        }

        public ElectronicDeliveryCreateData GetElectronicDeliveryCreateData(string electronicDeliveryId)
        {
            if (Guid.TryParse(electronicDeliveryId, out Guid id))
            {
                return Context.ElectronicDeliveryCreateDatas.FirstOrDefault(ed => ed.ElectronicDeliveryID == id);
            }

            return null;
        }

        public DownloadableAsset GetDownloadableAssetByFilename(string filename)
        {
            return Context.DownloadableAssets.Where(da => da.Filename.Contains(filename)).OrderBy(da => da.ContentItemId.EndsWith("-p") ? 1 : 0).FirstOrDefault();
        }

        public void UpdateBusinessOwnerAssets(int userId, int businessOwnerUserId)
        {
            foreach (var asset in Context.Assets.Where(a => a.BusinessOwnerID == userId))
            {
                asset.BusinessOwnerID = businessOwnerUserId;
            }
            foreach (var assetFullWidth in Context.AssetFullWidths.Where(afw => afw.BusinessOwnerID == userId))
            {
                assetFullWidth.BusinessOwnerID = businessOwnerUserId;
            }
            foreach (var downloadableAsset in Context.DownloadableAssets.Where(da => da.BusinessOwnerID == userId))
            {
                downloadableAsset.BusinessOwnerID = businessOwnerUserId;
            }
            foreach (var offer in Context.Offers.Where(o => o.BusinessOwnerID == userId))
            {
                offer.BusinessOwnerID = businessOwnerUserId;
            }
            foreach (var orderableAsset in Context.OrderableAssets.Where(oa => oa.BusinessOwnerID == userId))
            {
                orderableAsset.BusinessOwnerID = businessOwnerUserId;
            }
            foreach (var program in Context.Programs.Where(p => p.BusinessOwnerID == userId))
            {
                program.BusinessOwnerID = businessOwnerUserId;
            }
        }

        public bool IsUserABusinessOwner(int userID, string contentItemId = null)
        {
            var liveContentItemId = !string.IsNullOrEmpty(contentItemId) ? contentItemId.GetAsLiveContentItemId() : null;
            var assets = Context.Assets.Where(a => a.BusinessOwnerID == userID && (string.IsNullOrEmpty(liveContentItemId) || a.ContentItemId == liveContentItemId));
            var assetFullWidths = Context.AssetFullWidths.Where(a => a.BusinessOwnerID == userID && (string.IsNullOrEmpty(liveContentItemId) || a.ContentItemId == liveContentItemId));
            var orderableAssets = Context.OrderableAssets.Where(a => a.BusinessOwnerID == userID && (string.IsNullOrEmpty(liveContentItemId) || a.ContentItemId == liveContentItemId));
            var downloadableAsset = Context.DownloadableAssets.Where(a => a.BusinessOwnerID == userID && (string.IsNullOrEmpty(liveContentItemId) || a.ContentItemId == liveContentItemId));
            var program = Context.Programs.Where(a => a.BusinessOwnerID == userID && (string.IsNullOrEmpty(liveContentItemId) || a.ContentItemId == liveContentItemId));

            return assets.Any() || assetFullWidths.Any() || orderableAssets.Any() || downloadableAsset.Any() || program.Any();
        }
    }
}