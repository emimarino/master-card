﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class MigratedContentRepository : Repository<MarketingCenterDbContext>
    {
        public MigratedContentRepository(MarketingCenterDbContext context) : base(context)
        { }

        public MigratedContentId GetMigratedContent(string migratedId)
        {
            return (from mc in Context.MigratedContentIds
                    where mc.MigratedId == migratedId
                    select mc).FirstOrDefault();
        }
    }
}