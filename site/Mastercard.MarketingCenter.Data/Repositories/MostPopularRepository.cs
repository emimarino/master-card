﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class MostPopularRepository : Repository<MarketingCenterDbContext>, IMostPopularRepository
    {
        public MostPopularRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public IQueryable<MostPopularActivity> GetAllMostPopularActivities()
        {
            return Context.MostPopularActivities;
        }

        public IQueryable<MostPopularActivity> GetMostPopularActivitiesByRegion(string regionId)
        {
            return Context.MostPopularActivities.Where(mpa => mpa.RegionId.Trim().Equals(regionId.Trim(), StringComparison.OrdinalIgnoreCase));
        }

        public IQueryable<MostPopularActivity> GetMostPopularActivitiesByProcessId(long mostPopularProcessId)
        {
            return Context.MostPopularActivities.Where(mpa => mpa.MostPopularProcessId == mostPopularProcessId);
        }

        public IQueryable<MostPopularProcess> GetAllMostPopularProcesses()
        {
            return Context.MostPopularProcesses;
        }

        public MostPopularProcess Insert(MostPopularProcess mostPopularProcess)
        {
            return base.Insert(mostPopularProcess);
        }

        public void DeleteMostPopularActivity(MostPopularActivity mostPopularActivity)
        {
            Delete(mostPopularActivity);
        }

        public void DeleteMostPopularProcess(MostPopularProcess mostPopularProcess)
        {
            Delete(mostPopularProcess);
        }

        public void ProcessMostPopularActivity(DateTime startDate, DateTime endDate, string contentTypeIds, long mostPopularProcessId)
        {
            Context.Database.ExecuteSqlCommand("EXEC ProcessMostPopularActivity @StartDate = {0}, @EndDate = {1}, @ContentTypeIds = {2}, @MostPopularProcessId = {3}",
                startDate,
                endDate,
                contentTypeIds,
                mostPopularProcessId);
        }
    }
}