﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class TagHitRepository : Repository<MarketingCenterDbContext>
    {
        public TagHitRepository(MarketingCenterDbContext context)
            : base(context)
        { }

        public void TrackTagHit(string[] tags, long visitId, string userName)
        {
            foreach (var identifier in tags)
            {
                var tag = Context.Tags.FirstOrDefault(t => t.Identifier == identifier);

                if (tag != null && !Context.TagHits.Any(th => th.TagID == tag.TagID && th.VisitID == visitId))
                {
                    var tagHit = new TagHit { TagID = tag.TagID, Date = DateTime.Now, VisitID = visitId, UserName = userName };

                    Context.TagHits.Add(tagHit);
                }
            }

            Context.SaveChanges();
        }
    }
}