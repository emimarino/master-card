﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Extensions;
using Mastercard.MarketingCenter.Domo.Domain;
using Slam.Cms.Configuration;
using Slam.Cms.Data;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class SearchRepository : Repository<MarketingCenterDbContext>, ISearchRepository
    {
        public SearchRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public IDictionary<string, List<SearchAutoCompleteItem>> GetSearchAutoCompleteItems()
        {
            var searchAutoCompleteItems = new ConcurrentDictionary<string, List<SearchAutoCompleteItem>>();

            var tagHits = from th in Context.TagHits
                          join u in Context.Users.Include(u => u.Issuer) on th.UserName equals u.UserName
                          where !u.ExcludedFromDomoApi && !u.Issuer.ExcludedFromDomoApi
                          select th;

            var tags = (from tag in Context.Tags
                        join tagTranslated in Context.TagTranslatedContents on tag.TagID equals tagTranslated.TagId
                        where !tag.HideFromSearch.HasValue || !tag.HideFromSearch.Value
                        select new SearchAutoCompleteContextItem
                        {
                            Title = tagTranslated.DisplayName ?? tag.DisplayName ?? tag.Identifier,
                            SortOrder = 1,
                            Synonyms = string.Empty,
                            Rank = tagHits.Count(th => th.TagID == tag.TagID),
                            RestrictToSpecialAudience = tag.RestrictToSpecialAudience,
                            SearchParameter = tag.Identifier,
                            Language = tagTranslated.LanguageCode,
                            RegionId = string.Empty
                        })
                        .ToList();

            var terms = (from term in Context.SearchTerms
                         where !term.ExpirationDate.HasValue || term.ExpirationDate.Value >= DateTime.Today
                         select new SearchAutoCompleteContextItem
                         {
                             Title = term.Title,
                             SortOrder = 1,
                             Synonyms = term.Synonyms,
                             Rank = 0,
                             SegmentationIds = term.SearchTermSegmentations.Select(s => s.SegmentationId),
                             RestrictToSpecialAudience = term.RestrictToSpecialAudience,
                             SearchParameter = term.Title,
                             Language = term.SearchLanguage ?? string.Empty,
                             RegionId = term.RegionId
                         })
                         .ToList();

            var list = tags.Concat(terms).ToList();

            foreach (var item in list)
            {
                string itemReg = (string.IsNullOrEmpty(item.RegionId) ? "all" : item.RegionId.Trim());
                List<SearchAutoCompleteItem> regList = null;
                searchAutoCompleteItems.TryGetValue(itemReg, out regList);
                if (regList == null)
                {
                    searchAutoCompleteItems.TryAdd(itemReg, new List<SearchAutoCompleteItem>());
                    regList = searchAutoCompleteItems[itemReg];
                }

                regList.Add(new SearchAutoCompleteItem(item.Title, item.SearchParameter.EncodeForPathWithoutHttpContext(), item.Rank, item.SortOrder, item.SegmentationIds.ToArray(), item.RestrictToSpecialAudience, item.Language, false));

                if (item.Synonyms.Length > 0)
                {
                    foreach (string synonym in item.Synonyms.Split(new string[] { ",", "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Trim()))
                    {
                        if (!regList.Any(i => i.IsSynonym && i.Title.Equals(synonym)))
                        {
                            regList.Add(new SearchAutoCompleteItem(item.Title + " (" + synonym.Replace("\r\n", " ") + ")", synonym.EncodeForPathWithoutHttpContext(), item.Rank, item.SortOrder, item.SegmentationIds.ToArray(), item.RestrictToSpecialAudience, item.Language, true));
                        }
                    }
                }
            }

            return (IDictionary<string, List<SearchAutoCompleteItem>>)searchAutoCompleteItems;
        }

        public List<FeaturedSearchResult> GetFeaturedSearchResults(string searchQuery, IEnumerable<string> currentSegmentationIds, string regionId)
        {
            var command = Context.Database.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "RetrieveFeaturedSearchResults";
            command.Parameters.Add(new SqlParameter("SearchQuery", searchQuery));
            command.Parameters.Add(new SqlParameter("CurrentSegmentation", GetCurrentSegmentationCommandValue(currentSegmentationIds)));
            command.Parameters.Add(new SqlParameter("RegionId", regionId));

            return ExecuteReader<FeaturedSearchResult>(command);
        }

        public List<SearchResult> GetSearchResults(string searchQuery, IEnumerable<string> currentSegmentationIds, string regionId)
        {
            var command = Context.Database.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "RetrieveSearchResults";
            command.Parameters.Add(new SqlParameter("SearchQuery", searchQuery));
            command.Parameters.Add(new SqlParameter("CurrentSegmentation", GetCurrentSegmentationCommandValue(currentSegmentationIds)));
            command.Parameters.Add(new SqlParameter("RegionId", regionId));

            return ExecuteReader<SearchResult>(command);
        }

        public List<FeaturedSearchResult> GetFeaturedSearchResults(string searchQuery, IEnumerable<string> currentSegmentationIds, string specialAudienceAccess, string regionId)
        {
            if (string.IsNullOrWhiteSpace(specialAudienceAccess))
            {
                specialAudienceAccess = "No Special Audience";
            }

            var command = Context.Database.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "RetrieveFeaturedSearchResultsForSpecialAudience";
            command.Parameters.Add(new SqlParameter("SearchQuery", searchQuery));
            command.Parameters.Add(new SqlParameter("CurrentSegmentation", GetCurrentSegmentationCommandValue(currentSegmentationIds)));
            command.Parameters.Add(new SqlParameter("CurrentSpecialAudience", specialAudienceAccess));
            command.Parameters.Add(new SqlParameter("RegionId", regionId));

            return ExecuteReader<FeaturedSearchResult>(command);
        }

        public List<SearchResult> GetSearchResults(string searchQuery, IEnumerable<string> currentSegmentationIds, string specialAudienceAccess, string regionId)
        {
            if (string.IsNullOrWhiteSpace(specialAudienceAccess))
            {
                specialAudienceAccess = "No Special Audience";
            }

            var command = Context.Database.Connection.CreateCommand();
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.CommandText = "RetrieveSearchResultsForSpecialAudience";
            command.Parameters.Add(new SqlParameter("SearchQuery", searchQuery));
            command.Parameters.Add(new SqlParameter("CurrentSegmentation", GetCurrentSegmentationCommandValue(currentSegmentationIds)));
            command.Parameters.Add(new SqlParameter("CurrentSpecialAudience", specialAudienceAccess));
            command.Parameters.Add(new SqlParameter("RegionId", regionId));

            return ExecuteReader<SearchResult>(command);
        }

        public void AddNewSearchActivity(string userName, DateTime searchDateTime, string searchBoxEntry, string urlVisited, string Region, int? searchResultsQty)
        {
            var searchActivity = new SearchActivity { UserName = userName, SearchDateTime = searchDateTime, SearchBoxEntry = searchBoxEntry, UrlVisited = urlVisited, Region = Region, SearchResultsQty = searchResultsQty };

            Context.SearchActivities.Add(searchActivity);

            Context.SaveChanges();
        }

        public static string GetCurrentSegmentationCommandValue(IEnumerable<string> currentSegmentationIds)
        {
            return currentSegmentationIds.Any(s => !s.Equals(RegionConfigManager.DefaultSegmentation, StringComparison.InvariantCultureIgnoreCase)) ? string.Join(@",", currentSegmentationIds.Select(s => $"{s}")) : DBNull.Value.ToString();
        }

        private static List<T> ExecuteReader<T>(System.Data.Common.DbCommand command) where T : class, new()
        {
            List<T> result = new List<T>();
            try
            {
                command.Connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    var deserializer = Deserializer.GetClassDeserializer<T>(reader);

                    while (reader.Read())
                    {
                        result.Add(deserializer(reader));
                    }
                }
            }
            catch
            {
                command.Connection.Close();
                throw;
            }

            command.Connection.Close();

            return result;
        }

        public IEnumerable<DomoSearchActivity> GetActivity(DateTime from, DateTime to, int? SearchActivityID = null, string UserFullName = null, string UserEmail = null, string UserTitle = null, string UserRegion = null, string UserCountry = null, string SearchDateTime = null, string SearchTerm = null, string UrlVisited = null, string RegionalSite = null, bool includeExcludedResults = false)
        {
            to = to.Date.AddDays(1);
            var query = from user in Context.Users.Include(u => u.Profile).Include(u => u.Issuer)
                        join searchAct in Context.SearchActivities on user.UserName equals searchAct.UserName
                        join tag in Context.Tags on user.Country equals tag.Identifier
                        where
                        (includeExcludedResults || (!user.ExcludedFromDomoApi && !user.Issuer.ExcludedFromDomoApi))
                        select new DomoSearchActivity
                        {
                            SearchActivityID = searchAct.SearchActivityID,
                            RegionalSite = searchAct.Region,
                            SearchDateTime = searchAct.SearchDateTime,
                            SearchTerm = searchAct.SearchBoxEntry.ToLower(),
                            UrlVisited = searchAct.UrlVisited,
                            UserId = user.UserId,
                            UserFullName = user.FullName,
                            UserRegion = user.Region,
                            UserEmail = user.Email,
                            UserCountry = tag.DisplayName,
                            UserTitle = user.Profile.Title,
                            UserFinancialInstitution = user.Issuer.Title,
                            HiddenResult = user.ExcludedFromDomoApi || user.Issuer.ExcludedFromDomoApi
                        };

            query = query.Where(sa => from < sa.SearchDateTime && sa.SearchDateTime < to);

            if (SearchActivityID != null)
            {
                query = query.Where(sa => sa.SearchActivityID == SearchActivityID);
            }
            if (!string.IsNullOrEmpty(UserFullName))
            {
                query = query.Where(sa => sa.UserFullName == UserFullName);
            }
            if (!string.IsNullOrEmpty(UserEmail))
            {
                query = query.Where(sa => sa.UserEmail == UserEmail);
            }
            if (!string.IsNullOrEmpty(UserTitle))
            {
                query = query.Where(sa => sa.UserTitle == UserTitle);
            }
            if (!string.IsNullOrEmpty(UserRegion))
            {
                query = query.Where(sa => sa.UserRegion == UserRegion);
            }
            if (!string.IsNullOrEmpty(UserCountry))
            {
                query = query.Where(sa => sa.UserCountry == UserCountry);
            }
            if (!string.IsNullOrEmpty(SearchTerm))
            {
                query = query.Where(sa => sa.SearchTerm == SearchTerm);
            }
            if (!string.IsNullOrEmpty(UrlVisited))
            {
                query = query.Where(sa => sa.UrlVisited == UrlVisited);
            }
            if (!string.IsNullOrEmpty(RegionalSite))
            {
                query = query.Where(sa => sa.RegionalSite == RegionalSite);
            }

            return query;
        }
    }
}