﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class LanguageRepository : Repository<MarketingCenterDbContext>, ILanguageRepository
    {
        public LanguageRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public IQueryable<Language> GetAll()
        {
            return Context.Languages;
        }

        public Language GetByTagIdentifier(string tagIdentifier)
        {
            return GetAll().FirstOrDefault(u => u.TagIdentifier == tagIdentifier);
        }
    }
}