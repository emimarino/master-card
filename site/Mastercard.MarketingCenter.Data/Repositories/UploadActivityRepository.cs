﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Interfaces;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Repositories
{
    public class UploadActivityRepository : Repository<MarketingCenterDbContext>, IUploadActivityRepository
    {
        public UploadActivityRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public int AddUploadActivity(UploadActivity uploadActivity)
        {
            var activity = Context.UploadActivities.Add(uploadActivity);
            Commit();

            return activity.UploadActivityId;
        }

        public UploadActivity GetUploadActivityById(int uploadActivityId)
        {
            return Context.UploadActivities.FirstOrDefault(ua => ua.UploadActivityId == uploadActivityId);
        }
    }
}