﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class AnonymousTokenRepository : Repository<MarketingCenterDbContext>
    {
        protected static IList<AnonymousTokenData> _entities { get; set; }

        public AnonymousTokenRepository(MarketingCenterDbContext context) : base(context)
        { }

        public AnonymousTokenData GetAnonymousTokenData(Guid token)
        {
            return Context.AnonymousTokenData.FirstOrDefault(td => td.AnonymousToken == token);
        }

        public AnonymousTokenData GetAnonymousTokenData(string token)
        {
            Guid _token;
            Guid.TryParse(token, out _token);
            return GetAnonymousTokenData(_token);
        }

        public Guid SetAnonymousTokenData(string claim, int userID, string regionId, string optionalParameter = "")
        {
            var exitingToken = Context.AnonymousTokenData.FirstOrDefault(td => td.ClaimsTo == claim && td.UserId == userID && td.OptionalParameter == optionalParameter);
            if (exitingToken == null)
            {
                var token = Guid.NewGuid();
                var newTokenData = new AnonymousTokenData { AnonymousToken = token, ClaimsTo = claim, OptionalParameter = optionalParameter, UserId = userID, RegionId = regionId };
                Context.AnonymousTokenData.Add(newTokenData);

                return token;
            }

            return exitingToken.AnonymousToken;
        }
    }
}