﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Interfaces.GDP;
using Mastercard.MarketingCenter.Data.Repositories.GDP.Authorization;
using Mastercard.MarketingCenter.DTO.GDP;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Mastercard.MarketingCenter.Data.Repositories.GDP
{
    public class GdpExternalRepository : IGdpExternalRepository
    {
        private readonly IGdpRequestRepository _gdpRequestRepository;
        private readonly IGdprAuthorizationApiService _gdprAuthorizationApiService;
        private readonly string gdpDataAccessPrivacyRequestUri;
        private readonly string gdpDataAccessPrivacyResponseUri;

        public GdpExternalRepository(
            IGdprAuthorizationApiService gdprAuthorizationApiService,
            IGdpRequestRepository gdpRequestRepository)
        {
            _gdpRequestRepository = gdpRequestRepository;
            _gdprAuthorizationApiService = gdprAuthorizationApiService;
            gdpDataAccessPrivacyRequestUri = ConfigurationManager.AppSettings["GdprDataAccessJob.GDPDataAccessPrivacyRequestUri"];
            gdpDataAccessPrivacyResponseUri = ConfigurationManager.AppSettings["GdprDataAccessJob.GDPDataAccessPrivacyResponseUri"];
        }

        public bool RetrieveRequests()
        {
            var correlation = _gdpRequestRepository.GetNewCorrelation();

            List<GdpRequestDTO> requests = null;
            int xTotalCount = 0, httpStatusCode = 0;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                //Move string to constant
                client.DefaultRequestHeaders.Add("Correlation-Id", correlation.GdpCorrelationId.ToString());
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                _gdprAuthorizationApiService.GdpAuthenticate(client);

                var task = client.GetAsync(gdpDataAccessPrivacyRequestUri)
                    .ContinueWith((response) =>
                    {
                        var result = response.Result;
                        httpStatusCode = (int)result.StatusCode;
                        if (!result.Headers.Contains("Correlation-Id"))
                        {
                            throw new Exception("Correlation-Id not found in response headers");
                        }
                        if (!result.Headers.Contains("X-Total-Count"))
                        {
                            throw new Exception("X-Total-Count not found in response headers");
                        }

                        var correlationId = result.Headers.GetValues("Correlation-Id").ElementAt(0);
                        xTotalCount = int.Parse(result.Headers.GetValues("X-Total-Count").ElementAt(0));

                        if (correlationId != correlation.GdpCorrelationId.ToString())
                        {
                            throw new Exception(string.Format("Correlation-Id {0} doesn't match with {1}", correlationId, correlation.GdpCorrelationId.ToString()));
                        }

                        var jsonString = result.Content.ReadAsStringAsync();
                        jsonString.Wait();
                        requests = JsonConvert.DeserializeObject<List<GdpRequestDTO>>(jsonString.Result);
                    });
                task.Wait();
            }

            correlation.XTotalCount = xTotalCount;
            correlation.HttpStatusCode = httpStatusCode;
            _gdpRequestRepository.UpdateGdpCorrelation(correlation);

            foreach (var RequestDto in requests)
            {
                var GdpRequest = MapToEntity(RequestDto); //AUTOMAPPER!!
                GdpRequest.GdpCorrelationId = correlation.GdpCorrelationId;
                _gdpRequestRepository.Insert(GdpRequest);
            }

            return requests.Any();
        }

        private GdpRequestDTO MapToDTO(GdpRequest gdpRequest)
        {
            return new GdpRequestDTO()
            {
                RequestContext = gdpRequest.RequestContext,
                RequestId = gdpRequest.RequestId,
                RequestType = gdpRequest.RequestType,
                SearchKey = new RequestSearchKeyDTO { Email = gdpRequest.SearchKey }
            };
        }

        private GdpRequest MapToEntity(GdpRequestDTO gdpRequestdto)
        {
            return new GdpRequest()
            {
                RequestContext = gdpRequestdto.RequestContext,
                RequestId = gdpRequestdto.RequestId,
                RequestType = gdpRequestdto.RequestType,
                SearchKey = gdpRequestdto.SearchKey.Email,
                CreatedDateTime = DateTime.Now
            };
        }

        public int SubmitReply(GdpResponseDTO serviceResponse)
        {
            var correlation = _gdpRequestRepository.GetNewCorrelation();
            int httpStatusCode = 0;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();

                client.DefaultRequestHeaders.Add("Correlation-Id", correlation.GdpCorrelationId.ToString()); //TODO: Move string to constant
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                _gdprAuthorizationApiService.GdpAuthenticate(client);

                var httpContent = new StringContent(JsonConvert.SerializeObject(serviceResponse), Encoding.UTF8, "application/json");

                var task = client.PostAsync(gdpDataAccessPrivacyResponseUri, httpContent)
                    .ContinueWith((response) =>
                    {
                        var result = response.Result;
                        httpStatusCode = (int)result.StatusCode;
                        correlation.HttpStatusCode = httpStatusCode;
                        _gdpRequestRepository.UpdateGdpCorrelation(correlation);
                        if (!result.Headers.Contains("Correlation-Id"))
                        {
                            throw new Exception("Correlation-Id header not found");
                        }
                        var correlationId = result.Headers.GetValues("Correlation-Id").ElementAt(0);
                        if (correlationId != correlation.GdpCorrelationId.ToString())
                        {
                            throw new Exception(string.Format("Correlation-Id {0} doesn't match with {1}", correlationId, correlation.GdpCorrelationId.ToString()));
                        }
                    });
                task.Wait();
            }

            return correlation.GdpCorrelationId;
        }
    }
}