﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Repositories.GDP
{
    public class GdpRequestRepository : IGdpRequestRepository
    {
        private readonly MarketingCenterDbContext _marketingCenterDbContext;

        public GdpRequestRepository(MarketingCenterDbContext marketingCenterDbContext)
        {
            _marketingCenterDbContext = marketingCenterDbContext;
        }

        public void CreateResponse(GdpResponse response)
        {
            _marketingCenterDbContext.GdpResponse.Add(response);
            _marketingCenterDbContext.SaveChanges();
        }

        public IEnumerable<GdpRequest> GetById(int RequestId)
        {
            return _marketingCenterDbContext.GdpRequest.Where(r => r.RequestId.Equals(RequestId));
        }

        public GdpCorrelation GetNewCorrelation()
        {
            var correlation = _marketingCenterDbContext.GdpCorrelation.Add(
                new GdpCorrelation
                {
                    CreatedDateTime = DateTime.Now
                });

            _marketingCenterDbContext.SaveChanges();

            return correlation;
        }

        public IEnumerable<GdpRequest> GetPending()
        {
            return _marketingCenterDbContext
                .GdpRequest
                .Where(r =>
                    !r.GdpResponses.Any()
                    || !r.GdpResponses.Any(resp => resp.GdpCorrelation.HttpStatusCode == 201)
                );
        }

        public void Insert(GdpRequest request)
        {
            _marketingCenterDbContext.GdpRequest.Add(request);
            _marketingCenterDbContext.SaveChanges();
        }

        public void UpdateGdpCorrelation(GdpCorrelation correlation)
        {
            correlation.UpdatedDateTime = DateTime.Now;
            var c = _marketingCenterDbContext.GdpCorrelation.Attach(correlation);
            _marketingCenterDbContext.Entry(c).State = EntityState.Modified;
            _marketingCenterDbContext.SaveChanges();
        }
    }
}