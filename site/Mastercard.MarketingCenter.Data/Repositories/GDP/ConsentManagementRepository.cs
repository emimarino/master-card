﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Interfaces.GDP;
using Mastercard.MarketingCenter.DTO.GDP;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using ExtraConfig = Slam.Cms.Configuration;

namespace Mastercard.MarketingCenter.Data.Repositories.GDP
{
    public class ConsentManagementRepository : IConsentManagementRepository
    {
        private readonly IConsentFileDataRepository _consentFileDataRepository;
        private readonly ICachingService _cachingService;
        private readonly SlamContext _slamContext;
        private readonly IList<string> _locales;
        private readonly IList<string> _legalContentPaths;
        private readonly string _ConsentApiUri;
        private readonly string _serviceCode;

        public ConsentManagementRepository(IUserConsentRepository userConsentRepository, IConsentFileDataRepository consentFileDataRepository, SlamContext slamContext, ICachingService cachingService)
        {
            _cachingService = cachingService;
            _consentFileDataRepository = consentFileDataRepository;
            _slamContext = slamContext;

            var missingConfigs = new List<string>();

            _serviceCode = ConfigurationManager.AppSettings["GdprDataAccessJob.ServiceCode"];
            if (string.IsNullOrEmpty(_serviceCode))
            {
                missingConfigs.Add("GdprDataAccessJob.ServiceCode");
            }

            _ConsentApiUri = ConfigurationManager.AppSettings["ConsentManagement.Uri"];
            if (string.IsNullOrEmpty(_ConsentApiUri))
            {
                missingConfigs.Add("ConsentManagement.Uri");
            }

            var legalContentPathsConfig = ConfigurationManager.AppSettings["LegalContent.Paths"];
            if (string.IsNullOrEmpty(legalContentPathsConfig))
            {
                missingConfigs.Add("LegalContent.Paths");
            }

            if (missingConfigs.Count > 0)
            {
                throw new SettingsPropertyNotFoundException($"Missing {string.Join(",", missingConfigs)} {(missingConfigs.Count > 1 ? "configs" : "config")}.");
            }

            _legalContentPaths = legalContentPathsConfig?.Split(',');

            var localesConfig = ConfigurationManager.AppSettings["ConsentManagement.Locales"];
            _locales = string.IsNullOrEmpty(localesConfig) ? GetCachedLocales().ToList() : localesConfig.Split(',').ToList();
        }

        public ConsentFileData GetLatestConsentLanguageText(string useCode, string locale, string functionCode)
        {
            return _consentFileDataRepository.GetLatestConsentLanguageText(locale, functionCode);
        }

        public ConsentFileData GetLatestLegalConsentData(string useCode, string locale, string functionCode = null)
        {
            return _consentFileDataRepository.GetLatestLegalConsentData(locale, useCode, null);
        }

        private byte[] LoadContentFromOrigin(string uri)
        {
            byte[] data = null;
            using (var client = new WebClient())
            {
                data = client.DownloadData(uri);
            }

            return data;
        }

        private IList<ConsentFileData> ProcessFile(ConsentFileDataDTO consentFileDataDTO)
        {
            var fetchFilesDataList = new List<ConsentFileData>();
            foreach (var child in consentFileDataDTO.ConsentUseData)
            {
                fetchFilesDataList.Add(new ConsentFileData
                {
                    DocumentType = child.ConsentData.DocumentType,
                    PublishedDate = DateTime.Now, //consentFileDataDTO.Date, TODO: moved to string to check what is being received
                    EmailNotificationRequired = consentFileDataDTO.EmailNotificationRequired,
                    Locale = consentFileDataDTO.Locale,
                    NotificationVersion = consentFileDataDTO.NotificationVersion,
                    ReacceptanceRequired = consentFileDataDTO.ReacceptanceRequired,
                    ReacceptedVersion = consentFileDataDTO.ReacceptedVersion,
                    Uuid = consentFileDataDTO.Uuid,
                    CurrentVersion = consentFileDataDTO.CurrentVersion,
                    ConsentData = child.ConsentData.Data,
                    Status = consentFileDataDTO.Status,
                    UseCategoryCode = child.UseCategory.UseCategoryCode,
                    UseCategoryValue = child.UseCategory.UseCategoryValue,
                    UseCategoryDescription = child.UseCategory.UseCategoryDescription
                });
            }

            return fetchFilesDataList;
        }

        private IEnumerable<string> GetCachedLocales()
        {
            bool isCached = _cachingService.Get<IList<string>>("locales") != null;
            IEnumerable<string> locales = null;
            if (!isCached)
            {
                var regionConfigs = new ExtraConfig.RegionConfigManager();
                var regionCategories = _slamContext.GetTagTree().FindNode(tt => (tt.TagCategory != null ? tt.TagCategory.TagCategoryId.Equals(MarketingCenterDbConstants.TagCategories.Markets, StringComparison.OrdinalIgnoreCase) : false)).Children;

                var _locales =
                    regionCategories.SelectMany(region =>
                {
                    var languages = regionConfigs.GetLanguages(region.Identifier);
                    return languages.Select(l => (l.Key + "-" + region.Identifier.Substring(0, 2)));
                });

                _cachingService.Save("locales", _locales, TimeSpan.FromDays(1));
            }
            locales = _cachingService.Get<IEnumerable<string>>("locales");

            return locales;
        }

        private IEnumerable<ConsentFileDataDTO> FetchFilesData()
        {
            var filesList = new List<ConsentFileDataDTO>();
            var serviceCodeRegister = string.Format("{0}-reg", _serviceCode);

            string separator = "/";

            var consentLanguageUrls = from l in _locales
                                      select string.Join(separator, "consentlanguage", _serviceCode, l, serviceCodeRegister);

            var legalContentUrls = from l in _locales
                                   from lcp in _legalContentPaths
                                   select string.Join(separator, "legalcontent", _serviceCode, l, serviceCodeRegister, lcp);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            foreach (var partialUrl in consentLanguageUrls)
            {
                using (var client = new HttpClient())//fetches all the remote files at once
                {
                    Uri uri;
                    uri = new Uri(new Uri(_ConsentApiUri), string.Join(separator, partialUrl));
                    try
                    {
                        var task = client.GetAsync(uri).ContinueWith(
                             (t) =>
                             {
                                 if (t.Result.IsSuccessStatusCode)
                                 {
                                     var content = t.Result.Content.ReadAsStringAsync();
                                     content.Wait();
                                     var settings = new JsonSerializerSettings
                                     {
                                         ContractResolver = new CamelCasePropertyNamesContractResolver()
                                     };

                                     var consentFilesDataDTO = JsonConvert.DeserializeObject<IList<ConsentFileDataDTO>>(content.Result, settings);
                                     if (consentFilesDataDTO != null)
                                     {
                                         filesList.AddRange(consentFilesDataDTO);
                                     }
                                 }
                             }
                             );

                        task.Wait();
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            foreach (var partialUrl in legalContentUrls)
            {
                using (var client = new HttpClient())//fetches all the remote files at once
                {
                    Uri uri;
                    uri = new Uri(new Uri(_ConsentApiUri), string.Join(separator, partialUrl));
                    try
                    {
                        var task = client.GetAsync(uri).ContinueWith(
                             (t) =>
                             {
                                 if (t.Result.IsSuccessStatusCode)
                                 {
                                     var content = t.Result.Content.ReadAsStringAsync();
                                     content.Wait();
                                     var settings = new JsonSerializerSettings
                                     {
                                         ContractResolver = new CamelCasePropertyNamesContractResolver()
                                     };

                                     var consentFilesDataDTO = JsonConvert.DeserializeObject<ConsentFileDataDTO>(content.Result, settings);
                                     if (consentFilesDataDTO != null)
                                     {
                                         filesList.Add(consentFilesDataDTO);
                                     }
                                 }
                             }
                             );
                        task.Wait();
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            return filesList;
        }

        public void RetrieveExternalFiles()
        {
            var consentFileDataDTOs = FetchFilesData();
            if (consentFileDataDTOs != null && consentFileDataDTOs.Any())
            {
                var newFiles = consentFileDataDTOs.Where(cfd => !_consentFileDataRepository.GetLatestConsents().Any(lc => lc.Uuid.Equals(cfd.Uuid)));
                IList<ConsentFileData> consentfiles = newFiles.SelectMany(cfd => ProcessFile(cfd)).ToList();
                _consentFileDataRepository.Insert(consentfiles);
            }
        }
    }
}