﻿using System.Configuration;
using System.Net.Http;

namespace Mastercard.MarketingCenter.Data.Repositories.GDP.Authorization
{
    public class GdprAuthorizationApiService : IGdprAuthorizationApiService
    {
        private string username;
        private string password;
        private string token;

        public GdprAuthorizationApiService() {
             username = ConfigurationManager.AppSettings["GdprDataAccessJob.UserName"];
             password = ConfigurationManager.AppSettings["GdprDataAccessJob.Password"];
             token = ConfigurationManager.AppSettings["GdprDataAccessJob.AuthToken"];
        }

        public void ConsentAuthenticate(HttpClient client)
        {
            return ;
        }

        public void GdpAuthenticate(HttpClient client)
        {
            return;
        }
    }
}