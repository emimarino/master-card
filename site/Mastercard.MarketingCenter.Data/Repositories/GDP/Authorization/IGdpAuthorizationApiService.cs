﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Mastercard.MarketingCenter.Data.Repositories.GDP.Authorization
{
    public interface IGdprAuthorizationApiService
    {
        void GdpAuthenticate(HttpClient client);
        void ConsentAuthenticate(HttpClient client);
    }
}
