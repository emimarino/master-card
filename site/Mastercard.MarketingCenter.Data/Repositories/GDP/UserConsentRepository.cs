﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Repositories.GDP
{
    public class UserConsentRepository : IUserConsentRepository
    {
        private readonly MarketingCenterDbContext _marketingCenterDbContext;

        public UserConsentRepository(MarketingCenterDbContext marketingCenterDbContext)
        {
            _marketingCenterDbContext = marketingCenterDbContext;
        }
        public IQueryable<UserConsent> GetByEmail(string email)
        {
            return _marketingCenterDbContext.UserConsents
                   .Where(uc => _marketingCenterDbContext.Users.Any(u => u.Email.Equals(email) && u.UserId.Equals(uc.UserId))).Include(uc => uc.ConsentFileData);

        }

        public void Insert(UserConsent userConsent)
        {
            _marketingCenterDbContext.UserConsents.Add(userConsent);
            _marketingCenterDbContext.SaveChanges();
        }

        public void Insert(IEnumerable<UserConsent> userConsents)
        {
            var uniqueUserConsents = userConsents.Where(uc => !_marketingCenterDbContext.UserConsents.Any(uci => uci.ConsentFileDataId.Equals(uc.ConsentFileDataId) && uci.UserId.Equals(uc.UserId)));
            foreach (var userConsent in uniqueUserConsents)
            {
                _marketingCenterDbContext.UserConsents.Add(userConsent);
            }
            _marketingCenterDbContext.SaveChanges();
        }

        public IEnumerable<ConsentFileData> GetExpiredConsentForUser(string userName, string locale)
        {
            var result = _marketingCenterDbContext.Database.SqlQuery<ConsentFileData>(@"
				WITH LastConsentFileData (LatestVersion,ServiceFunctionCode,UseCategoryCode,DocumentType,Locale)
				AS
				(
					SELECT MAX(cfd.CurrentVersion) as LatestVersion,cfd.ServiceFunctionCode,cfd.UseCategoryCode,cfd.DocumentType,cfd.Locale
					from ConsentFileData cfd
					where Locale = @locale
					group by cfd.ServiceFunctionCode,cfd.UseCategoryCode,cfd.DocumentType,cfd.Locale
					)
					,LastUserConsentFileData (LatestVersion,ServiceFunctionCode,UseCategoryCode,DocumentType,Locale)
				AS 
				(
					SELECT MAX(cfd.CurrentVersion) as LatestVersion,cfd.ServiceFunctionCode,cfd.UseCategoryCode,cfd.DocumentType,cfd.Locale
					from ConsentFileData cfd
						inner join UserConsent uc ON cfd.ConsentFileDataId = uc.ConsentFileDataId
						inner join [User] u on u.UserID = uc.UserId
					WHERE u.UserName = 'mastercardmembers:' + @userName
						AND Locale = @locale
					group by cfd.ServiceFunctionCode,cfd.UseCategoryCode,cfd.DocumentType,cfd.Locale
				)
				SELECT c.LatestVersion,c.ServiceFunctionCode,c.UseCategoryCode,c.DocumentType,c.Locale
				INTO #PendingConsents 
				FROM LastConsentFileData c
				LEFT JOIN LastUserConsentFileData uc 
					ON	c.LatestVersion = uc.LatestVersion 
						AND (
							c.ServiceFunctionCode =	uc.ServiceFunctionCode
							or (c.ServiceFunctionCode is null and uc.ServiceFunctionCode is null)
						)
						AND c.UseCategoryCode =	uc.UseCategoryCode
						AND c.DocumentType = uc.DocumentType
						AND c.Locale = uc.Locale
				WHERE uc.LatestVersion is null

				SELECT cfd.ConsentFileDataId
					,cfd.Uuid
					,cfd.ServiceFunctionCode
					,cfd.UseCategoryCode
					,cfd.DocumentType
					,cfd.ConsentData
					,cfd.CurrentVersion
					,cfd.Status
					,cfd.Locale
					,cfd.ReacceptedVersion
					,cfd.NotificationVersion
					,cfd.ReacceptanceRequired
					,cfd.EmailNotificationRequired
					,cfd.PublishedDate
                    ,cfd.UseCategoryValue
                    ,cfd.UseCategoryDescription
				FROM ConsentFileData cfd
					INNER JOIN #PendingConsents pc
						ON	cfd.CurrentVersion = pc.LatestVersion 
						AND (
							cfd.ServiceFunctionCode = pc.ServiceFunctionCode
							or (cfd.ServiceFunctionCode is null and pc.ServiceFunctionCode is null)
						)
						AND cfd.UseCategoryCode = pc.UseCategoryCode
						AND cfd.DocumentType = pc.DocumentType
						AND cfd.Locale = pc.Locale

				DROP TABLE #PendingConsents
                ",
                new SqlParameter("userName", userName),
                new SqlParameter("locale", locale));
            return result.ToList().AsEnumerable();
        }

        public int GetUserIdByEmail(string email)
        {
            return _marketingCenterDbContext.Users.First(u => u.Email.Equals(email)).UserId;
        }

        public IEnumerable<ConsentFileData> GetLatestConsents(string locale, string functionCode)
        {
            var result = _marketingCenterDbContext.Database.SqlQuery<ConsentFileData>(@"
				WITH LastConsentFileData (LatestVersion,ServiceFunctionCode,UseCategoryCode,DocumentType,Locale)
				AS
				(
					SELECT MAX(cfd.CurrentVersion) as LatestVersion,cfd.ServiceFunctionCode,cfd.UseCategoryCode,cfd.DocumentType,cfd.Locale
					from ConsentFileData cfd
					where Locale = @locale
						and ServiceFunctionCode = @functionCode
					group by cfd.ServiceFunctionCode,cfd.UseCategoryCode,cfd.DocumentType,cfd.Locale
					)

				SELECT c.LatestVersion,c.ServiceFunctionCode,c.UseCategoryCode,c.DocumentType,c.Locale
				INTO #PendingConsents 
				FROM LastConsentFileData c

				SELECT cfd.ConsentFileDataId
					,cfd.Uuid
					,cfd.ServiceFunctionCode
					,cfd.UseCategoryCode
					,cfd.DocumentType
					,cfd.ConsentData
					,cfd.CurrentVersion
					,cfd.Status
					,cfd.Locale
					,cfd.ReacceptedVersion
					,cfd.NotificationVersion
					,cfd.ReacceptanceRequired
					,cfd.EmailNotificationRequired
					,cfd.PublishedDate
                    ,cfd.UseCategoryValue
                    ,cfd.UseCategoryDescription
				FROM ConsentFileData cfd
					INNER JOIN #PendingConsents pc
						ON	cfd.CurrentVersion = pc.LatestVersion 
						AND (
							cfd.ServiceFunctionCode = pc.ServiceFunctionCode
							or (cfd.ServiceFunctionCode is null and pc.ServiceFunctionCode is null)
						)
						AND cfd.UseCategoryCode = pc.UseCategoryCode
						AND cfd.DocumentType = pc.DocumentType
						AND cfd.Locale = pc.Locale

				DROP TABLE #PendingConsents
            ",
            new SqlParameter("locale", locale),
            new SqlParameter("functionCode", functionCode));

            return result.ToList().AsEnumerable();
        }
    }
}