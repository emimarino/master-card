﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Data
{
    public class IssuerUnmatchedRepository : Repository<MarketingCenterDbContext>
    {
        public IssuerUnmatchedRepository(MarketingCenterDbContext context) : base(context)
        { }

        public void Add(IssuerUnmatched issuerUnmatched)
        {
            Context.IssuerUnmatcheds.Add(issuerUnmatched);

            Context.SaveChanges();
        }
    }
}