﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class ClonesRepository : IClonesRepository
    {
        private readonly MarketingCenterDbContext _context;
        public ClonesRepository(MarketingCenterDbContext context)
        {
            _context = context;
        }

        public IQueryable<CloneSourceNotification> GetCloneSourcesNotifications()
        {
            return _context.CloneSourceNotification.Where(n => !n.HasBeenSent && n.CreationDate > (SqlFunctions.DateAdd("hh", -24, SqlFunctions.GetDate())));
            //added checkion for creation dates less than 24 hours old to avoid using notifications from previous reports.
        }

        public IQueryable<IGrouping<string, CloneSourceNotification>> GetActiveCloneSourcesNotificationGroupedByRegion()
        {
            return GetCloneSourcesNotifications().GroupBy(n => n.CloneRegionId);
        }

        public void PopulateCloneSourceNotification()
        {
            //executes  sproc which will get updated assets and leave them aggregated in a table to futher used the metada in Mail building
            _context.Database.ExecuteSqlCommand("Exec PopulateCloneSourceNotification");
        }

        public void UpdateSate(IEnumerable<CloneSourceNotification> elements)
        {
            //update a group of elements
            elements.ToList().ForEach(e => e.HasBeenSent = true);
            _context.SaveChanges();
        }
    }
}