﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class RegionRepository : Repository<MarketingCenterDbContext>
    {
        public RegionRepository(MarketingCenterDbContext context) : base(context) { }

        public IEnumerable<Region> GetAll(bool includeGlobal = true)
        {
            return Context.Regions
#pragma warning disable 0618
                .Where(x => includeGlobal || x.Id != "global")
#pragma warning restore 0618
                .ToList();
        }

        public Region GetById(string id)
        {
            return Context.Regions.Include(r => r.Tag).ToList().FirstOrDefault(r => r.IdTrimmed == id);
        }

        public IQueryable<State> GetStates()
        {
            return Context.States;
        }
    }
}