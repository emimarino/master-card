﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class CmsRoleAccessRepository : Repository<MarketingCenterDbContext>
    {
        protected static IList<CmsRoleAccess> _entities { get; set; }

        public CmsRoleAccessRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public IQueryable<CmsRoleAccess> GetCmsRoleAccessByRoleType(string roleType)
        {
            return Context.CmsRoleAccesses.Where(cra => cra.RoleType == roleType);
        }

        private IList<CmsRoleAccess> GetEntities()
        {
            if (_entities == null)
            {
                _entities = Context.CmsRoleAccesses.ToList();
            }

            return _entities;
        }

        public IEnumerable<CmsRoleAccess> GetByRoute(string area, string controller, string action, string id, string queryString, string contentAction)
        {
            // basically filter works this way:
            // 1) null/empty value in DB matches everything
            // 2) DB value should either be equal or be included in the HTTP request
            // 3) if DB have values, we must search by the specific value.

            return GetEntities()
                .Where(x =>
                    string.Compare(x.Controller, controller, StringComparison.InvariantCultureIgnoreCase) == 0
                    && (string.IsNullOrEmpty(x.Area) || (!string.IsNullOrEmpty(x.Area) && string.Compare(x.Area, area, StringComparison.InvariantCultureIgnoreCase) == 0))
                    && (string.IsNullOrEmpty(x.Action) || (!string.IsNullOrEmpty(x.Action) && string.Compare(x.Action, action, StringComparison.InvariantCultureIgnoreCase) == 0))
                    && (string.IsNullOrEmpty(x.QueryString) || (!string.IsNullOrEmpty(x.QueryString) && !string.IsNullOrEmpty(queryString) && queryString.Contains(x.QueryString)))
                    && (string.IsNullOrEmpty(x.ContentAction) || (!string.IsNullOrEmpty(x.ContentAction) && string.Compare(x.ContentAction, contentAction, StringComparison.InvariantCultureIgnoreCase) == 0))
                    && (string.IsNullOrEmpty(x.Id) || (!string.IsNullOrEmpty(x.Id) && string.Compare(x.Id, id, StringComparison.InvariantCultureIgnoreCase) == 0))
                );
        }
    }
}