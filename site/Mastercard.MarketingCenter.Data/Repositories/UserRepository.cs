﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class UserRepository : Repository<MarketingCenterDbContext>, IUserRepository
    {
        protected const string MastercardUserEmailDomain = "@mastercard.com";
        private readonly IMembershipProvider _membershipProvider;
        private readonly string _usersMMC;
        private readonly string _usersMOS;

        public UserRepository()
        {
            _usersMMC = App_GlobalResources.NoteComments.UsersMMC;
            _usersMOS = App_GlobalResources.NoteComments.UsersMOS;
        }

        public UserRepository(MarketingCenterDbContext context, IMembershipProvider membershipProvider) : base(context)
        {
            _membershipProvider = membershipProvider;
        }

        public Aspnet_Membership UpdateMembershipEmail(Aspnet_Membership user, string newEmail)
        {
            return UpdateMembershipEmail(user.Email, newEmail);
        }

        public Aspnet_Membership UpdateMembershipEmail(string oldEmail, string newEmail)
        {
            var membershipUser = GetMembershipUserByEmail(oldEmail);
            membershipUser.Email = newEmail;

            return Update(membershipUser);
        }

        public Aspnet_Membership GetMembershipUserByEmail(string email)
        {
            return Context.Aspnet_Membership.FirstOrDefault(am => am.Email.Equals(email, StringComparison.OrdinalIgnoreCase));
        }

        public Aspnet_Membership GetMembershipUserByUserName(string userName)
        {
            return Context.Aspnet_Membership.FirstOrDefault(am => Context.Aspnet_Users.Any(au => au.UserId.Equals(am.UserId) && au.UserName.Equals(userName)));
        }

        public IQueryable<User> GetAll()
        {
            return Context.Users.Include(u => u.Profile)
                                .Include(u => u.Issuer);
        }

        public User GetUser(string userName)
        {
            userName = userName.IncludeMembershipProviderName();
            return GetAll().Include(u => u.Groups)
                           .FirstOrDefault(u => u.UserName.Equals(userName, StringComparison.InvariantCultureIgnoreCase));
        }

        public User GetUser(int userId)
        {
            return GetAll().Include(u => u.Groups)
                           .FirstOrDefault(u => u.UserId == userId);
        }

        public User GetByEmail(string email)
        {
            return GetAll().Include(u => u.Groups)
                           .FirstOrDefault(u => u.Email == email);
        }

        public IQueryable<string> GetExpiredUserEmails(int expirationWindowInMonths)
        {
            var expirationDate = DateTime.Now.AddMonths(-expirationWindowInMonths);
            return Context.Aspnet_Membership
                          .Where(m => m.LastLoginDate < expirationDate && m.Email.Contains("@"))
                          .Select(m => m.Email);
        }

        public IQueryable<UserMembership> GetExpiringFollowUpUsers(string regionId, int gdprFollowUpStatusId, int expirationWindowInMonths)
        {
            var expirationDate = DateTime.Now.AddMonths(-expirationWindowInMonths);
            return Context.Aspnet_Membership
                          .Join(GetAll(),
                                m => m.Email,
                                u => u.Email,
                                (m, u) => new { User = u, Membership = m })
                          .Where(mu => mu.User.Region.Equals(regionId, StringComparison.InvariantCultureIgnoreCase) &&
                                       mu.Membership.LastLoginDate < expirationDate && mu.User.Email.Contains("@") &&
                                       (mu.User.Profile.GdprFollowUpStatusId == null || mu.User.Profile.GdprFollowUpStatusId < gdprFollowUpStatusId))
                          .Select(mu => new UserMembership
                          {
                              User = mu.User,
                              Profile = mu.User.Profile,
                              Membership = mu.Membership
                          });
        }

        public IQueryable<UserData> GetActiveUsersByIssuerId(string issuerId)
        {
            return Context.Aspnet_Membership
                          .Join(GetAll(),
                                m => m.Email,
                                u => u.Email,
                                (m, u) => new { User = u, Membership = m })
                          .Where(mu => mu.User.IssuerId.Equals(issuerId, StringComparison.InvariantCultureIgnoreCase) &&
                                       !mu.User.IsDisabled && !mu.User.IsBlocked && mu.Membership.IsApproved && mu.User.Email.Contains("@"))
                          .Select(mu => new UserData
                          {
                              Email = mu.User.Email,
                              FirstName = mu.User.Profile.FirstName,
                              LastName = mu.User.Profile.LastName,
                              Address = mu.User.Profile.Address1,
                              City = mu.User.Profile.City,
                              State = mu.User.Profile.State,
                              LastLoginDate = mu.Membership.LastLoginDate
                          });
        }

        public void SetFeedbackRequestDisplayed(string userName)
        {
            var user = GetUser(userName);
            if (user != null)
            {
                user.Profile.FeedbackRequestDisplayed = true;
                Context.SaveChanges();
            }
        }

        public void SetContentPreferencesMessageDisplayed(string userName, bool displayContentPreferencesMessage)
        {
            var user = GetUser(userName);
            if (user != null)
            {
                user.Profile.ContentPreferencesMessageDisplayed = displayContentPreferencesMessage;
                Context.SaveChanges();
            }
        }

        public IQueryable<UserRegistration> GetUserRegistrations(
            string individualFirstName = null,
            string individualLastName = null,
            string individualEmail = null,
            string localIssuerName = null,
            string noteComment = null,
            string CID = null,
            string importedIssuerName = null,
            string regionId = null,
            DateTime? lastLoginStart = null,
            DateTime? lastLoginEnd = null,
            DateTime? registeredDateStart = null,
            DateTime? registeredDateEnd = null,
            bool includeLeftOutResults = false
        )
        {
            var result = from am in Context.Aspnet_Membership
                         join u in GetAll() on am.Email equals u.Email
                         select (
                         new UserRegistration
                         {
                             RegisteredDate = am.CreateDate,
                             IndividualFirstName = u.Profile.FirstName,
                             IndividualLastName = u.Profile.LastName,
                             IndividualEmail = am.Email,
                             LocalIssuerName = u.Issuer.Title,
                             CID = u.Issuer.Cid,
                             LastLogin = am.LastLoginDate,
                             RegionId = u.Region,
                             NoteComment = (u.Region.Equals("us") ? _usersMMC : _usersMOS),
                         });

            if (!string.IsNullOrEmpty(individualFirstName))
            {
                result = result.Where(r => r.IndividualFirstName.Equals(individualFirstName));
            }
            if (!string.IsNullOrEmpty(individualLastName))
            {
                result = result.Where(r => r.IndividualLastName.Equals(individualLastName));
            }
            if (!string.IsNullOrEmpty(individualEmail))
            {
                result = result.Where(r => r.IndividualEmail.Equals(individualEmail));
            }
            if (!string.IsNullOrEmpty(localIssuerName))
            {
                result = result.Where(r => r.LocalIssuerName.Equals(localIssuerName));
            }
            if (!string.IsNullOrEmpty(noteComment))
            {
                if (noteComment.ToLower().Contains("mos"))
                {
                    result = result.Where(r => r.RegionId.Equals("us") || r.RegionId == null);
                }
                else if (noteComment.ToLower().Contains("mmc"))
                {
                    result = result.Where(r => (!r.RegionId.Equals("us")) && r.RegionId != null);
                }
            }
            if (CID != null)
            {
                result = result.Where(r => r.CID.Equals(CID));
            }
            if (!string.IsNullOrEmpty(importedIssuerName))
            {
                result = result.Where(r => r.ImportedIssuerName.Equals(importedIssuerName));
            }
            if (!string.IsNullOrEmpty(regionId))
            {
                result = result.Where(r => r.RegionId.Equals(regionId));
            }
            if (lastLoginStart != null)
            {
                result = result.Where(r => r.LastLogin >= lastLoginStart);
            }
            if (lastLoginEnd != null)
            {
                result = result.Where(r => r.LastLogin <= lastLoginEnd);
            }

            if (registeredDateStart != null)
            {
                result = result.Where(r => r.RegisteredDate >= registeredDateStart);
            }
            if (registeredDateEnd != null)
            {
                result = result.Where(r => r.RegisteredDate <= registeredDateEnd);
            }

            return result;
        }
    }
}