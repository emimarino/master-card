﻿using Mastercard.MarketingCenter.Data.Entities;
using Slam.Cms.Data;
using System;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class UserSubscriptionRepository : Repository<MarketingCenterDbContext>
    {
        public UserSubscriptionRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public UserSubscription GetUserSubscription(int userId)
        {
            return Context.UserSubscriptions.FirstOrDefault(us => us.UserId == userId);
        }

        public UserSubscription SaveUserSubscription(int userId, int frequency)
        {
            var userSubscription = GetUserSubscription(userId);
            if (userSubscription == null)
            {
                userSubscription = new UserSubscription
                {
                    UserId = userId,
                    UserSubscriptionFrequencyId = frequency,
                    SavedDate = DateTime.Now
                };

                Insert(userSubscription);
            }
            else
            {
                userSubscription.UserSubscriptionFrequencyId = frequency;
                userSubscription.SavedDate = DateTime.Now;
            }

            Commit();

            return userSubscription;
        }

        public ContentItemUserSubscription GetContentItemUserSubscription(string contentItemId, int userId)
        {
            return Context.ContentItemUserSubscriptions.FirstOrDefault(us => us.ContentItemId == contentItemId &&
                                                                             us.UserId == userId);
        }

        public ContentItemUserSubscription SaveContentItemUserSubscription(string contentItemId, int userId, bool subscribed)
        {
            var userSubscription = GetContentItemUserSubscription(contentItemId, userId);
            if (userSubscription == null)
            {
                userSubscription = new ContentItemUserSubscription
                {
                    ContentItemId = contentItemId,
                    UserId = userId,
                    Subscribed = subscribed,
                    SavedDate = DateTime.Now
                };

                Insert(userSubscription);
            }
            else
            {
                userSubscription.Subscribed = subscribed;
                userSubscription.SavedDate = DateTime.Now;
            }

            Commit();

            return userSubscription;
        }

        public IQueryable<ContentItemUserSubscription> GetSubscribedContentItemUserSubscriptions()
        {
            return Context.ContentItemUserSubscriptions.Where(us => us.Subscribed);
        }

        public IQueryable<ContentItemUserSubscription> GetSubscribedContentItemUserSubscriptions(string regionId)
        {
            return Context.ContentItemUserSubscriptions.Include(us => us.ContentItem)
                                                       .Where(us => us.Subscribed &&
                                                                    us.ContentItem.RegionId.Trim().ToLower() == regionId.Trim().ToLower());
        }

        public IQueryable<ContentItemUserSubscription> GetSubscribedContentItemUserSubscriptions(DateTime startDate, DateTime endDate, string regionId)
        {
            endDate = endDate.Date.AddDays(1);
            return Context.ContentItemUserSubscriptions.Include(us => us.ContentItem)
                                                       .Include(us => us.User)
                                                       .Where(us => us.Subscribed &&
                                                                    us.SavedDate >= startDate &&
                                                                    us.SavedDate < endDate &&
                                                                    us.ContentItem.RegionId.Trim().ToLower() == regionId.Trim().ToLower());
        }

        public IQueryable<ContentItemUserSubscription> GetSubscribedContentItemUserSubscriptions(int userId, string regionId)
        {
            return Context.ContentItemUserSubscriptions.Include(us => us.ContentItem)
                                                       .Where(us => us.Subscribed &&
                                                                    us.UserId == userId &&
                                                                    us.ContentItem.RegionId.Trim().ToLower() == regionId.Trim().ToLower());
        }

        public IQueryable<ContentItemUserSubscription> GetContentItemUserSubscriptionsByUserId(int userId)
        {
            return Context.ContentItemUserSubscriptions.Where(us => us.UserId == userId);
        }

        public IQueryable<UserSubscription> GetEnabledUserSubscriptions()
        {
            return Context.UserSubscriptions.Include(us => us.User.Issuer)
                                            .Where(us => !us.User.IsDisabled);
        }

        public IQueryable<ContentItemTriggerMarketing> GetContentItemTriggerMarketings()
        {
            var expirationDate = DateTime.Today.AddYears(-1);
            return Context.ContentItemTriggerMarketings.Include(tm => tm.ContentItem)
                                                       .Where(tm => tm.Notify &&
                                                                    tm.StatusId == Status.Published &&
                                                                    tm.SavedDate > expirationDate &&
                                                                    tm.ContentItem.StatusID == Status.Published);
        }
    }
}