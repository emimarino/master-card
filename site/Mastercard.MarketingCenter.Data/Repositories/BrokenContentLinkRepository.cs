﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class BrokenContentLinkRepository : Repository<MarketingCenterDbContext>
    {
        public BrokenContentLinkRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public bool VerifyContentItemExistsByFrontEndUrl(string frontEndUrl)
        {
            if (!string.IsNullOrEmpty(frontEndUrl))
            {
                var cleanFrontEndUrl = Context.VanityUrls.Where(vu => vu.VanityURL.Equals(frontEndUrl) || DbFunctions.Like(vu.VanityURL, ("%" + frontEndUrl.Substring(1, frontEndUrl.Length - 1) + "%"))).Select(vu => vu.Url).FirstOrDefault();
                cleanFrontEndUrl = cleanFrontEndUrl ?? frontEndUrl;

                return Context.ContentItems.Any(ci => (ci.FrontEndUrl.Equals(cleanFrontEndUrl) || DbFunctions.Like(ci.FrontEndUrl, ("%" + cleanFrontEndUrl.Substring(1, cleanFrontEndUrl.Length - 1) + "%")) || DbFunctions.Like(ci.FrontEndUrl, ("%" + DbFunctions.Left(cleanFrontEndUrl, cleanFrontEndUrl.Length - DbFunctions.Reverse(cleanFrontEndUrl).IndexOf("/")) + "%"))) && ci.StatusID == 3);
            }
            else
            {
                return true;
            }
        }

        public BrokenContentLink Insert(BrokenContentLink brokenContentLink)
        {
            return base.Insert(brokenContentLink);
        }

        public IQueryable<BrokenContentLink> GetByRegion(string regionId)
        {
            return Context.BrokenContentLinks.Where(bcl => bcl.RegionId.Equals(regionId, StringComparison.InvariantCultureIgnoreCase));
        }

        public BrokenContentLink Update(BrokenContentLink brokenContentLink)
        {
            return base.Update(brokenContentLink);
        }

        public IEnumerable<PotentialBrokenContentLinkContent> GetPotentialBrokenContentLinkContent()
        {
            return Context.Database.SqlQuery<PotentialBrokenContentLinkContent>(@"EXEC dbo.RetrievePotentialBrokenContentLinkContent");
        }

        public IEnumerable<SnippetIndex> FindSnippetFrontEndReferences()
        {
            var allReferences = Context.Database.SqlQuery<SnippetIndex>(@"EXEC dbo.RetrieveSnippetReferences");
            var result = new List<SnippetIndex>();
            result.AddRange(allReferences.Where(r => !r.ContentTypeId.Equals(MarketingCenterDbConstants.ContentTypeIds.Snippet)
                            || (!allReferences.Any(al => al.SnippetId.Equals(r.ReferenceId) && !al.ReferenceId.Equals(r.ReferenceId)))));

            var snippets = allReferences.Where(r => r.ContentTypeId == MarketingCenterDbConstants.ContentTypeIds.Snippet
             && allReferences.Any(al => al.SnippetId.Equals(r.ReferenceId) && !al.ReferenceId.Equals(r.ReferenceId)));

            while (snippets.Any(r =>
                           result.Any(al => al.RegionId.Equals(r.RegionId, StringComparison.InvariantCultureIgnoreCase) && al.SnippetId.Equals(r.ReferenceId, StringComparison.InvariantCultureIgnoreCase)
                           && !al.ReferenceId.Equals(r.ReferenceId, StringComparison.InvariantCultureIgnoreCase))))
            {
                var auxResult = snippets.Join(result, s => new { Id = s.ReferenceId, Region = s.RegionId }, r => new { Id = r.SnippetId, Region = r.RegionId }, (s, r) => new SnippetIndex
                {
                    SnippetId = s.SnippetId,
                    ContentTypeId = r.ContentTypeId,
                    FrontEndUrl = r.FrontEndUrl,
                    ReferenceId = r.ReferenceId,
                    RegionId = r.RegionId,
                    StatusId = r.StatusId,
                    SnippetTitle = r.SnippetTitle,
                    SnippetFrontendUrl = r.SnippetFrontendUrl
                });
                snippets = snippets.Where(r =>
                           !result.Any(al => al.SnippetId.Equals(r.ReferenceId) && !al.ReferenceId.Equals(r.ReferenceId)));
                result.AddRange(auxResult);
            }
            result.AddRange(snippets);

            return result;
        }

        public IEnumerable<string> GetContentFeaturedLocations(string itemId)
        {
            return Context.ContentItems.Include(ci => ci.ContentItemFeatureLocations.Select(cif => cif.FeatureLocation)).FirstOrDefault(ci => ci.ContentItemId == itemId).ContentItemFeatureLocations.Select(cif => cif.FeatureLocation).Select(fl => fl.Title);
        }

        public string GetBrokenContentStatus(string contentItemId, string link)
        {
            string status;
            if(string.IsNullOrEmpty(contentItemId))
            {
                status = Context.ContentItems.Join(Context.Statuses, ci => ci.StatusID, s => s.StatusID, (ci, s) => new { Status = s.Name, ci.FrontEndUrl }).FirstOrDefault(cs => cs.FrontEndUrl.Contains(link))?.Status;
            }
            else
            {
                status = Context.ContentItems.Join(Context.Statuses, ci => ci.StatusID, s => s.StatusID, (ci, s) => new { Id = ci.ContentItemId, Status = s.Name }).FirstOrDefault(cs => cs.Id.Equals(contentItemId))?.Status;
            }
            return status ?? "Not Found";
        }
    }
}