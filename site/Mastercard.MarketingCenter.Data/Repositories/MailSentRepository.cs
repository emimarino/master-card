﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Data
{
    public class MailSentRepository : Repository<MarketingCenterDbContext>
    {
        public MailSentRepository(MarketingCenterDbContext context) : base(context)
        { }

        public void Add(MailSent mailSent)
        {
            Context.MailSents.Add(mailSent);
            Context.SaveChanges();
        }
    }
}