﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class UserKeyRepository : Repository<MarketingCenterDbContext>, IUserKeyRepository
    {
        public UserKeyRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public UserKey CreateUserKey(int userId)
        {
            var userKey = new UserKey();
            userKey.UserID = userId;
            userKey.UserKeyStatusID = MarketingCenterDbConstants.UserKeyStatus.Available;
            userKey.Key = Guid.NewGuid().ToString();
            userKey.DateKeyGenerated = DateTime.Now;

            Context.UserKeys.Add(userKey);
            Context.SaveChanges();

            return userKey;
        }

        public UserKey GetUserKeyByKey(string key)
        {
            return Context.UserKeys.FirstOrDefault(uk => uk.Key == key);
        }

        public UserKey GetUserKeyByUserName(string userName)
        {
            return Context.UserKeys.Include(uk => uk.User)
                                   .Where(uk => uk.User.UserName == userName)
                                   .OrderByDescending(uk => uk.DateKeyGenerated)
                                   .FirstOrDefault();
        }

        public UserKey GetUserKeyByUserNameAndStatus(string userName, int statusID)
        {
            return Context.UserKeys.Include(uk => uk.User)
                                   .Where(uk => uk.User.UserName == userName && uk.UserKeyStatusID == statusID)
                                   .OrderByDescending(uk => uk.DateKeyGenerated)
                                   .FirstOrDefault();
        }

        public void ChangeUserKeyStatus(string key, int userKeyStatusId)
        {
            var userKey = GetUserKeyByKey(key);
            if (userKey != null)
            {
                userKey.UserKeyStatusID = userKeyStatusId;
                Context.SaveChanges();
            }
        }

        public void ChangeUserKeyStatusForRelatedKeys(string key, int userKeyStatusId)
        {
            var userKey = GetUserKeyByKey(key);
            if (userKey != null)
            {
                foreach (var relUserKey in Context.UserKeys.Where(uk => uk.UserID == userKey.UserID && uk.UserKeyStatusID == MarketingCenterDbConstants.UserKeyStatus.Available))
                {
                    relUserKey.UserKeyStatusID = userKeyStatusId;
                }

                Context.SaveChanges();
            }
        }
    }
}