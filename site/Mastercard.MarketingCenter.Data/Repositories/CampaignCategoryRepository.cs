﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class CampaignCategoryRepository : Repository<MarketingCenterDbContext>, ICampaignCategoryRepository
    {
        public CampaignCategoryRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public void DeleteCampaignCategory(CampaignCategory campaignCategory)
        {
            Delete(campaignCategory);
        }

        public IQueryable<CampaignCategory> GetAll()
        {
            return Context.CampaignCategories.Include(c => c.CampaignCategoryTranslatedContents);
        }

        public CampaignCategory GetCampaignCategoryById(string campaignCategoryId)
        {
            return Context.CampaignCategories
                          .Include(ct => ct.CampaignEvents.Select(ce => ce.ContentItemCampaignEvents.Select(cice => cice.ContentItemForCalendar.ContentItem)))
                          .FirstOrDefault(ct => ct.CampaignCategoryId.Equals(campaignCategoryId, StringComparison.OrdinalIgnoreCase));
        }

        public IQueryable<CampaignCategory> GetCampaignCategoriesByRegionAndLanguage(string regionId, string languageCode)
        {
            return Context.CampaignCategories
                          .Include(ct => ct.CampaignCategoryTranslatedContents)
                          .Where(ct => ct.RegionId.Trim().Equals(regionId.Trim(), StringComparison.OrdinalIgnoreCase)
                                    && ct.CampaignCategoryTranslatedContents.Any(tc => tc.LanguageCode.Trim().Equals(languageCode.Trim(), StringComparison.OrdinalIgnoreCase)));
        }
    }
}