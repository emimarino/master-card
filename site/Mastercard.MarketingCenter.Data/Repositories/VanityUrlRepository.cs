﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Data
{
    public class VanityUrlRepository : Repository<MarketingCenterDbContext>
    {
        public VanityUrlRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public IEnumerable<VanityUrl> GetAll()
        {
            return Context.VanityUrls;
        }
    }
}