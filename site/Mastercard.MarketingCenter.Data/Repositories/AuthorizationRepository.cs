﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class AuthorizationRepository : Repository<MarketingCenterDbContext>, IAuthorizationRepository
    {
        public AuthorizationRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public void InsertUserRoleRegion(UserRoleRegion userRoleRegion)
        {
            Insert(userRoleRegion);
        }

        public void DeleteUserRoleRegion(UserRoleRegion userRoleRegion)
        {
            Delete(userRoleRegion);
        }

        public Role GetRoleByName(string name)
        {
            return Context.Roles.FirstOrDefault(r => name.Trim().Equals(r.Name, System.StringComparison.OrdinalIgnoreCase));
        }

        public IQueryable<UserRoleRegion> GetUserRoles(int userId)
        {
            return Context.UserRoleRegions
                          .Where(r => userId == r.UserId)
                          .Include(r => r.Region)
                          .Include(r => r.Role);
        }

        public IQueryable<RoleRegion> GetRolesByRegion(string regionId)
        {
            return Context.RoleRegions
                          .Include(r => r.Region)
                          .Include(r => r.Role.Regions.Select(reg => reg.Region))
                          .Where(r => regionId.Trim().Equals(r.RegionId, System.StringComparison.OrdinalIgnoreCase));
        }

        public IQueryable<RoleRegion> GetRegionsByRole(int roleId)
        {
            return Context.RoleRegions
                          .Include(r => r.Region)
                          .Include(r => r.Role.Regions.Select(reg => reg.Region))
                          .Where(r => roleId == r.RoleId);
        }

        public IQueryable<Permission> GetUserPermissionsByRegion(int userId, string regionId)
        {
            return Context.UserRoleRegions
                          .Join(Context.RolePermissionRegions.Include(r => r.Permission),
                                u => u.RoleId,
                                r => r.RoleId,
                                (u, r) => new { UserRoleRegion = u, RolePermissionRegion = r })
                          .Where(ur => userId == ur.UserRoleRegion.UserId &&
                                       regionId.Trim().Equals(ur.UserRoleRegion.RegionId, System.StringComparison.OrdinalIgnoreCase) &&
                                       regionId.Trim().Equals(ur.RolePermissionRegion.RegionId, System.StringComparison.OrdinalIgnoreCase))
                          .Select(ur => ur.RolePermissionRegion.Permission);
        }

        public IQueryable<Permission> GetRolePermissionsByRegion(int roleId, string regionId)
        {
            return Context.RolePermissionRegions
                          .Where(r => roleId == r.RoleId &&
                                       regionId.Trim().Equals(r.RegionId, System.StringComparison.OrdinalIgnoreCase))
                          .Select(r => r.Permission);
        }

        public IQueryable<UserRoleRegion> GetUsersInRole(string name)
        {
            return Context.UserRoleRegions
                          .Include(r => r.User)
                          .Where(r => name.Trim().Equals(r.Role.Name, System.StringComparison.OrdinalIgnoreCase));
        }
    }
}