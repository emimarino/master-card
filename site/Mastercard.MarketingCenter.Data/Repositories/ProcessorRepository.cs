﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class ProcessorRepository
    {
        private readonly MarketingCenterDbContext _context;

        public ProcessorRepository(MarketingCenterDbContext context)
        {
            _context = context;
        }

        public IQueryable<Processor> GetProcessors()
        {
            return _context.Processors;
        }

        public Processor GetProcessorById(string id)
        {
            return _context.Processors.FirstOrDefault(x => x.ProcessorId == id);
        }

        public IQueryable<Processor> GetProcessorsByIssuerId(string issuerId)
        {
            return _context.Processors.Where(x => x.Issuers.Any(i => i.IssuerId == issuerId));
        }
    }
}