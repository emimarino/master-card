﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class SegmentationRepository : Repository<MarketingCenterDbContext>, ISegmentationRepository
    {
        public SegmentationRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public IQueryable<Segmentation> GetAll()
        {
            return Context.Segmentations;
        }

        public IQueryable<Segmentation> GetByRegion(string regionId)
        {
            return GetAll().Where(s => s.RegionId.Equals(regionId, StringComparison.InvariantCultureIgnoreCase))
                           .Include(s => s.SegmentationTranslatedContents);
        }

        public IQueryable<Segmentation> GetByUserName(string userName)
        {
            return GetAll().Include(s => s.IssuerSegmentations.Select(i => i.Issuer.Users))
                           .Where(s => s.IssuerSegmentations.Any(i => i.Issuer.Users.Any(u => u.UserName.Contains(userName))));
        }

        public IQueryable<Segmentation> GetByEmail(string email)
        {
            return GetAll().Include(s => s.IssuerSegmentations.Select(i => i.Issuer.Users))
                           .Where(s => s.IssuerSegmentations.Any(i => i.Issuer.Users.Any(u => u.Email.Equals(email))));
        }
    }
}