﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Extensions;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class OfferRepository : Repository<MarketingCenterDbContext>, IOfferRepository
    {
        public OfferRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public IQueryable<Offer> GetAll()
        {
            return Context.Offers.Include(o => o.ContentItem.Region)
                                 .Include(o => o.CardExclusivities)
                                 .Include(o => o.Categories)
                                 .Include(o => o.OfferType)
                                 .Include(o => o.ContentItem.Tags.Select(t => t.TagHierarchyPositions))
                                 .Include(o => o.EventLocations);
        }

        public IQueryable<CardExclusivity> GetCardExclusivities()
        {
            return Context.CardExclusivities.Include(ce => ce.Offers);
        }

        public IQueryable<Category> GetCategories()
        {
            return Context.Categories.Include(c => c.Offers);
        }

        public IQueryable<Tag> GetMarketApplicabilities(string regionId)
        {
            return Context.TagHierarchyPositions.Where(thp => thp.TagCategoryId.Equals(MarketingCenterDbConstants.TagCategories.Markets)
                                                           && thp.ParentPositionId.HasValue && thp.Parent.Tag.Identifier.Equals(regionId))
                                                .Select(thp => thp.Tag)
                                                .Include(t => t.ContentItems);
        }

        public IQueryable<Offer> GetByRegion(string regionId)
        {
            return GetAll().FilterByRegion(regionId);
        }

        public Offer GetById(string contentItemId)
        {
            return GetAll().GetById(contentItemId);
        }

        public void InsertImportedOffers(IEnumerable<ImportedOffer> importedOffers)
        {
            var previousImportedOffers = Context.ImportedOffers.Where(io => io.IsLastImportedVersion).ToList();
            previousImportedOffers.ForEach(io => io.IsLastImportedVersion = false);
            Context.ImportedOffers.AddRange(importedOffers);

            Context.SaveChanges();
        }

        public void ProcessPricelessOffers(string businessOwnerEmail)
        {
            Context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "EXEC [ProcessPricelessOffers] @businessOwnerEmail = {0}", businessOwnerEmail);
        }

        public void ArchiveImportedOffers()
        {
            Context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "EXEC [Archive].[ArchiveImportedOffer_insert]");
        }

        public void DeleteArchivedImportedOffers()
        {
            Context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "EXEC [Archive].[ImportedOffer_deleteOld]");
        }

        public IQueryable<PricelessOffersProcess> GetPricelessOffersProcesses()
        {
            return Context.PricelessOffersProcesses;
        }
    }
}