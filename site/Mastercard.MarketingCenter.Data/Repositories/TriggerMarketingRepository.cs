﻿using Slam.Cms.Data;
using System;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class TriggerMarketingRepository : Repository<MarketingCenterDbContext>
    {
        public TriggerMarketingRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public void SaveSentDailyDate(DateTime sentDate)
        {
            var contentItemTriggerMarketings = (from ci in Context.ContentItemTriggerMarketings
                                                where !ci.SentDailyDate.HasValue
                                                && ci.SavedDate < sentDate
                                                && ci.StatusId == Status.Published
                                                select ci);

            foreach (var contentItem in contentItemTriggerMarketings)
            {
                contentItem.SentDailyDate = sentDate;
            }
        }

        public void SaveSentWeeklyDate(DateTime sentDate)
        {
            var contentItemTriggerMarketings = (from ci in Context.ContentItemTriggerMarketings
                                                where !ci.SentWeeklyDate.HasValue
                                                && ci.SavedDate < sentDate
                                                && ci.StatusId == Status.Published
                                                select ci);

            foreach (var contentItem in contentItemTriggerMarketings)
            {
                contentItem.SentWeeklyDate = sentDate;
            }
        }

        public int DeletePublishedTriggerMarketings(int months)
        {
            var deletionDate = DateTime.Today.AddMonths(-months);

            var triggerMarketingsToDelete = (from ci in Context.ContentItemTriggerMarketings
                                             where ci.StatusId == Status.Published
                                             && ci.SentDailyDate.HasValue && ci.SentWeeklyDate.HasValue
                                             && ci.SentDailyDate.Value < deletionDate
                                             && ci.SentWeeklyDate.Value < deletionDate
                                             select ci);

            int deletedContentItem = 0;
            foreach (var contentItem in triggerMarketingsToDelete)
            {
                Delete(contentItem);
                deletedContentItem++;
            }

            Context.SaveChanges();

            return deletedContentItem;
        }
    }
}