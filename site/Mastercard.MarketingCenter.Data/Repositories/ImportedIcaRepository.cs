﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class ImportedIcaRepository : Repository<MarketingCenterDbContext>, IImportedIcaRepository
    {
        public ImportedIcaRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public IQueryable<ImportedIca> GetAll()
        {
            return Context.ImportedIcas.Include(i => i.MetricHeatmap);
        }

        public IQueryable<ImportedIca> GetAllActive()
        {
            return Context.ImportedIcas.Include(i => i.MetricHeatmap).Where(o => o.HqCountryCode == "USA" && o.StatusCode == "L");
        }

        public ImportedIca GetById(int id)
        {
            return GetAllActive().FirstOrDefault(o => o.ImportedIcaId == id);
        }

        public ImportedIca GetByCid(string cid)
        {
            return GetAllActive().FirstOrDefault(o => o.Cid == cid);
        }
    }
}