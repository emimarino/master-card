﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class PendingRegistrationRepository : Repository<MarketingCenterDbContext>, IPendingRegistrationRepository
    {
        private readonly IUserRepository _userRepository;

        public PendingRegistrationRepository(MarketingCenterDbContext context, IUserRepository userRepository) : base(context)
        {
            _userRepository = userRepository;
        }

        public PendingRegistration GetByGuid(string guid)
        {
            return Context.PendingRegistrations.Include(pr => pr.State)
                                               .FirstOrDefault(pr => pr.GUID.ToString().Equals(guid, StringComparison.OrdinalIgnoreCase));
        }

        public IQueryable<PendingRegistration> GetByRegion(string regionId = null)
        {
            return Context.PendingRegistrations.Include(pr => pr.State)
                                               .Where(pr => regionId == null || pr.RegionId == regionId);
        }

        public IQueryable<PendingRegistration> GetByEmail(string email, string regionId = null)
        {
            return GetByRegion(regionId).Where(pr => pr.Email.Equals(email, StringComparison.OrdinalIgnoreCase));
        }

        public IQueryable<PendingRegistration> GetByStatus(string status, string regionId = null)
        {
            var users = _userRepository.GetAll();
            return GetByRegion(regionId).Where(pr => pr.Status.Equals(status, StringComparison.OrdinalIgnoreCase) &&
                                                     pr.Email.Contains("@") &&
                                                     !users.Any(u => u.Email.Equals(pr.Email, StringComparison.OrdinalIgnoreCase)));
        }

        public IQueryable<PendingRegistration> GetExpiredPendingRegistrations(DateTime expirationDate)
        {
            return from pr in Context.PendingRegistrations
                   join u in _userRepository.GetAll() on pr.Email equals u.Email into gj
                   from pr_u in gj.DefaultIfEmpty()
                   where pr.Email.Contains("@")
                         && pr.DateAdded < expirationDate
                         && pr_u.Email == null
                   select pr;
        }
    }
}