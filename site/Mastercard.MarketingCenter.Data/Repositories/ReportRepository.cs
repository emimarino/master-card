﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.Reporting;
using Mastercard.MarketingCenter.Data.Metadata;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public class ReportRepository : Repository<MarketingCenterDbContext>
    {
        private readonly IUserRepository _userRepository;
        private readonly IIssuerRepository _issuerRepository;
        private readonly ISiteTrackingRepository _siteTrackingRepository;
        public ReportRepository(MarketingCenterDbContext context, IUserRepository userRepository, IIssuerRepository issuerRepository, ISiteTrackingRepository siteTrackingRepository) : base(context)
        {
            _userRepository = userRepository;
            _issuerRepository = issuerRepository;
            _siteTrackingRepository = siteTrackingRepository;
        }

        public IEnumerable<ProcessorActivityReportResultItem> GetProcessorActivityReport(DateTime startDate, DateTime endDate)
        {
            return Context.Database.SqlQuery<ProcessorActivityReportResultItem>($"EXEC RetrieveProcessorActivityReport @StartDate = '{startDate}', @EndDate = '{endDate.AddDays(1)}'");
        }

        public IEnumerable<ProcessorActivityDetailReportResultItem> GetProcessorActivityDetailReport(string processorId, DateTime startDate, DateTime endDate, string Region)
        {
            return Context.Database.SqlQuery<ProcessorActivityDetailReportResultItem>("EXEC RetrieveProcessorActivityDetailReport @ProcessorId = {0}, @StartDate = {1}, @EndDate = {2} , @Region={3}", processorId, startDate, endDate.AddDays(1), Region);
        }

        public int GetProcessorActivityDetailExcludedIssuersReport(string processorId, DateTime startDate, DateTime endDate, string region)
        {
            return Context.Database.SqlQuery<int>("EXEC RetrieveProcessorActivityDetailExcludedIssuersReport @ProcessorId = {0}, @StartDate = {1}, @EndDate = {2}, @Region = {3}", processorId, startDate, endDate.AddDays(1), region).ElementAt(0);
        }

        public IEnumerable<ContentActivityAssetDetailReportResultItem> GetContentActivityDetailReport(string contentItemId, DateTime startDate, DateTime endDate, IEnumerable<string> segmentationIds, bool filterMasterCardUsers, bool filterVendorProcessor, string region)
        {
            return GroupForAudienceSegments(Context.Database.SqlQuery<ContentActivityAssetDetailReportResultItem>("EXEC RetrieveContentActivityDetailReport @ContentItemId = {0}, @StartDate = {1}, @EndDate = {2}, @AudienceSegmentation = {3}, @FilterMasterCardUsers = {4}, @FilterVendorProcessor = {5}, @RegionId = {6}",
                                            contentItemId,
                                            startDate,
                                            endDate.AddDays(1),
                                            SearchRepository.GetCurrentSegmentationCommandValue(segmentationIds),
                                            filterMasterCardUsers,
                                            filterVendorProcessor,
                                            region));
        }

        public IEnumerable<ContentActivityAssetDetailReportResultItem> GetContentActivityAssetDetailReport(string contentItemId, DateTime startDate, DateTime endDate, IEnumerable<string> segmentationIds, bool filterMasterCardUsers, bool filterVendorProcessor, string region)
        {
            return GroupForAudienceSegments(Context.Database.SqlQuery<ContentActivityAssetDetailReportResultItem>("EXEC RetrieveContentActivityAssetDetailReport @ContentItemId = {0}, @StartDate = {1}, @EndDate = {2}, @AudienceSegmentation = {3}, @FilterMasterCardUsers = {4}, @FilterVendorProcessor = {5}, @RegionId = {6}",
                                            contentItemId,
                                            startDate,
                                            endDate.AddDays(1),
                                            SearchRepository.GetCurrentSegmentationCommandValue(segmentationIds),
                                            filterMasterCardUsers,
                                            filterVendorProcessor,
                                            region));
        }

        public IEnumerable<ContentActivityAssetDetailReportResultItem> GetContentActivityTagDetailReport(string tagId, DateTime startDate, DateTime endDate, IEnumerable<string> segmentationIds, bool filterMasterCardUsers, bool filterVendorProcessor, string region)
        {
            return GroupForAudienceSegments(Context.Database.SqlQuery<ContentActivityAssetDetailReportResultItem>("EXEC RetrieveContentActivityTagDetailReport @TagId = {0}, @StartDate = {1}, @EndDate = {2}, @AudienceSegmentation = {3}, @FilterMasterCardUsers = {4}, @FilterVendorProcessor = {5}  , @RegionId = {6}",
                                            tagId,
                                            startDate,
                                            endDate.AddDays(1),
                                            SearchRepository.GetCurrentSegmentationCommandValue(segmentationIds),
                                            filterMasterCardUsers,
                                            filterVendorProcessor,
                                            region));
        }

        private IEnumerable<ContentActivityAssetDetailReportResultItem> GroupForAudienceSegments(IEnumerable<ContentActivityAssetDetailReportResultItem> resultItems)
        {
            return resultItems.GroupBy(i => new
            {
                i.UserName,
                i.Name,
                i.IssuerId,
                i.Issuer,
                i.ProcessorId,
                i.Processor,
                i.When,
                i.From,
                i.Source,
                i.Title
            })
            .Select(i => new ContentActivityAssetDetailReportResultItem
            {
                UserName = i.Key.UserName,
                Name = i.Key.Name,
                IssuerId = i.Key.IssuerId,
                Issuer = i.Key.Issuer,
                ProcessorId = i.Key.ProcessorId,
                Processor = i.Key.Processor,
                When = i.Key.When,
                From = i.Key.From,
                Source = i.Key.Source,
                Title = i.Key.Title,
                AudienceSegments = string.Join(", ", i.Select(s => s.AudienceSegments))
            });
        }

        public IEnumerable<ContentActivityProgramReportResultItem> GetContentActivityProgramReport(DateTime startDate, DateTime endDate, IEnumerable<string> segmentationIds, bool filterMasterCardUsers, bool filterVendorProcessor, string region)
        {
            return Context.Database.SqlQuery<ContentActivityProgramReportResultItem>("EXEC RetrieveContentActivityForProgramReport @StartDate = {0}, @EndDate = {1}, @AudienceSegmentation = {2}, @FilterMasterCardUsers = {3}, @FilterVendorProcessor = {4}, @RegionId = {5}",
                startDate,
                endDate.AddDays(1),
                SearchRepository.GetCurrentSegmentationCommandValue(segmentationIds),
                filterMasterCardUsers,
                filterVendorProcessor,
                region);
        }

        public IEnumerable<ContentActivityOfferReportResultItem> GetContentActivityOfferReport(DateTime startDate, DateTime endDate, IEnumerable<string> segmentationIds, bool filterMasterCardUsers, bool filterVendorProcessor, string region)
        {
            return Context.Database.SqlQuery<ContentActivityOfferReportResultItem>("EXEC RetrieveContentActivityForOfferReport @StartDate = {0}, @EndDate = {1}, @AudienceSegmentation = {2}, @FilterMasterCardUsers = {3}, @FilterVendorProcessor = {4}, @RegionId = {5}",
                startDate,
                endDate.AddDays(1),
                SearchRepository.GetCurrentSegmentationCommandValue(segmentationIds),
                filterMasterCardUsers,
                filterVendorProcessor,
                region);
        }

        public IEnumerable<ContentActivityAssetReportResultItem> GetContentActivityAssetReport(DateTime startDate, DateTime endDate, IEnumerable<string> segmentationIds, bool filterMasterCardUsers, bool filterVendorProcessor, string region)
        {
            return Context.Database.SqlQuery<ContentActivityAssetReportResultItem>("EXEC RetrieveContentActivityForAssetReport @StartDate = {0}, @EndDate = {1}, @AudienceSegmentation = {2}, @FilterMasterCardUsers = {3}, @FilterVendorProcessor = {4}, @RegionId={5}",
                startDate,
                endDate.AddDays(1),
                SearchRepository.GetCurrentSegmentationCommandValue(segmentationIds),
                filterMasterCardUsers,
                filterVendorProcessor,
                region);
        }

        public IEnumerable<ContentActivityPageReportResultItem> GetContentActivityPageReport(DateTime startDate, DateTime endDate, IEnumerable<string> segmentationIds, bool filterMasterCardUsers, bool filterVendorProcessor, string region)
        {
            return Context.Database.SqlQuery<ContentActivityPageReportResultItem>("EXEC RetrieveContentActivityForPageReport @StartDate = {0}, @EndDate = {1}, @AudienceSegmentation = {2}, @FilterMasterCardUsers = {3}, @FilterVendorProcessor = {4} , @RegionID = {5}",
                startDate,
                endDate.AddDays(1),
                SearchRepository.GetCurrentSegmentationCommandValue(segmentationIds),
                filterMasterCardUsers,
                filterVendorProcessor,
                region);
        }

        public IEnumerable<ContentActivityTagReportResultItem> GetContentActivityTagReport(DateTime startDate, DateTime endDate, IEnumerable<string> segmentationIds, bool filterMasterCardUsers, bool filterVendorProcessor, string region)
        {
            return Context.Database.SqlQuery<ContentActivityTagReportResultItem>("EXEC RetrieveContentActivityForTagReport @StartDate = {0}, @EndDate = {1}, @AudienceSegmentation = {2}, @FilterMasterCardUsers = {3}, @FilterVendorProcessor = {4} , @RegionId = {5}",
                startDate,
                endDate.AddDays(1),
                SearchRepository.GetCurrentSegmentationCommandValue(segmentationIds),
                filterMasterCardUsers,
                filterVendorProcessor,
                region);
        }

        public IEnumerable<ContentActivityWebinarReportResultItem> GetContentActivityWebinarReport(DateTime startDate, DateTime endDate, IEnumerable<string> segmentationIds, bool filterMasterCardUsers, bool filterVendorProcessor, string region)
        {
            return Context.Database.SqlQuery<ContentActivityWebinarReportResultItem>("EXEC RetrieveContentActivityForWebinarReport @StartDate = {0}, @EndDate = {1}, @AudienceSegmentation = {2}, @FilterMasterCardUsers = {3}, @FilterVendorProcessor = {4}, @RegionId = {5}",
                startDate,
                endDate.AddDays(1),
                SearchRepository.GetCurrentSegmentationCommandValue(segmentationIds),
                filterMasterCardUsers,
                filterVendorProcessor,
                region);
        }

        public IEnumerable<UserRegistrationReportResultItem> GetUserRegistrationReport(DateTime startDate, DateTime endDate, string region, bool excludeFromDomoApi = false, bool excludeMastercardUsers = false, bool excludeExpiredUsers = false, bool includeDemoRegion = false)
        {
            return Context.Database.SqlQuery<UserRegistrationReportResultItem>("EXEC RetrieveUserRegistrationReport @StartDate = {0}, @EndDate = {1} , @Region = {2}, @ExcludeFromDomoApi = {3}, @IncludeDemoRegion = {4}, @ExcludeMastercardUsers = {5}, @ExcludeExpiredUsers = {6}", startDate, endDate.AddDays(1), region, excludeFromDomoApi, includeDemoRegion, excludeMastercardUsers, excludeExpiredUsers);
        }

        public IEnumerable<OrderingActivityIssuerReportResultItem> GetOrderingActivityForIssuerReport(DateTime startDate, DateTime endDate)
        {
            return Context.Database.SqlQuery<OrderingActivityIssuerReportResultItem>("EXEC RetrieveOrderingActivityForIssuerReport @StartDate = {0}, @EndDate = {1}", startDate, endDate.AddDays(1));
        }

        public IEnumerable<OrderingActivityReportResultItem> GetOrderingActivityForProgramReport(DateTime startDate, DateTime endDate)
        {
            return Context.Database.SqlQuery<OrderingActivityReportResultItem>($"EXEC RetrieveOrderingActivityForProgramReport @StartDate = '{startDate}', @EndDate = '{endDate.AddDays(1)}'");
        }

        public IEnumerable<OrderingActivityReportResultItem> GetOrderingActivityForNonProgramReport(DateTime startDate, DateTime endDate)
        {
            return Context.Database.SqlQuery<OrderingActivityReportResultItem>($"EXEC RetrieveOrderingActivityForNonProgramReport @StartDate = '{startDate}', @EndDate = '{endDate.AddDays(1)}'");
        }

        public IEnumerable<OrderingActivityDetailReportResultItem> GetOrderingActivityForIssuerDetailReport(string isuerId, DateTime startDate, DateTime endDate)
        {
            return GroupForAudienceSegments(Context.Database.SqlQuery<OrderingActivityDetailReportResultItem>($"EXEC RetrieveOrderingActivityForIssuerDetailReport @IssuerID = '{isuerId}', @StartDate = '{startDate}', @EndDate = '{endDate.AddDays(1)}'"));
        }

        public IEnumerable<OrderingActivityDetailReportResultItem> GetOrderingActivityForProgramDetailReport(string programId, DateTime startDate, DateTime endDate)
        {
            return GroupForAudienceSegments(Context.Database.SqlQuery<OrderingActivityDetailReportResultItem>($"EXEC RetrieveOrderingActivityForProgramDetailReport @ProgramId = '{programId}', @StartDate = '{startDate}', @EndDate = '{endDate.AddDays(1)}'"));
        }

        public IEnumerable<OrderingActivityDetailReportResultItem> GetOrderingActivityForNonProgramDetailReport(DateTime startDate, DateTime endDate)
        {
            return GroupForAudienceSegments(Context.Database.SqlQuery<OrderingActivityDetailReportResultItem>($"EXEC RetrieveOrderingActivityForNonProgramDetailReport @StartDate = '{startDate}', @EndDate = '{endDate.AddDays(1)}'"));
        }

        private IEnumerable<OrderingActivityDetailReportResultItem> GroupForAudienceSegments(IEnumerable<OrderingActivityDetailReportResultItem> resultItems)
        {
            return resultItems.GroupBy(i => new
            {
                i.OrderNumber,
                i.Date,
                i.Processor,
                i.RelatedProgram,
                i.FinancialInstitution,
                i.User,
                i.Email,
                i.ItemName,
                i.ItemType,
                i.SKU,
                i.Fulfillment,
                i.EstDistribution,
                i.ItemPrice,
                i.ItemQuantity,
                i.OrderStatus,
                i.AdvisorsTag,
                i.ConsentGiven
            })
            .Select(i => new OrderingActivityDetailReportResultItem
            {
                OrderNumber = i.Key.OrderNumber,
                Date = i.Key.Date,
                Processor = i.Key.Processor,
                RelatedProgram = i.Key.RelatedProgram,
                FinancialInstitution = i.Key.FinancialInstitution,
                User = i.Key.User,
                Email = i.Key.Email,
                ItemName = i.Key.ItemName,
                ItemType = i.Key.ItemType,
                SKU = i.Key.SKU,
                Fulfillment = i.Key.Fulfillment,
                EstDistribution = i.Key.EstDistribution,
                ItemPrice = i.Key.ItemPrice,
                ItemQuantity = i.Key.ItemQuantity,
                OrderStatus = i.Key.OrderStatus,
                AdvisorsTag = i.Key.AdvisorsTag,
                ConsentGiven = i.Key.ConsentGiven,
                AudienceSegments = string.Join(", ", i.Select(s => s.AudienceSegments))
            });
        }

        public IEnumerable<SearchTermReportResultItem> GetSearchTermsReport(DateTime startDate, DateTime endDate, string Region)
        {
            return Context.Database.SqlQuery<SearchTermReportResultItem>("EXEC RetrieveSearchTermsReport @StartDate = {0}, @EndDate = {1} , @Region={2}", startDate, endDate.AddDays(1), Region);
        }

        public IEnumerable<IssuerAssessmentUsageReportResultItem> GetIssuerAssessmentUsageReport(DateTime startDate, DateTime endDate, bool filterMasterCardUsers, bool filterVendorProcessor)
        {
            return Context.Database.SqlQuery<IssuerAssessmentUsageReportResultItem>($"EXEC RetrieveIssuerAssessmentUsageReport @StartDate = '{startDate}', @EndDate = '{endDate.AddDays(1)}', @FilterMasterCardUsers = {filterMasterCardUsers}, @FilterVendorProcessor = {filterVendorProcessor}");
        }

        public IEnumerable<IssuerAssessmentUsageDetailReportResultItem> GetIssuerAssessmentUsageDetailReport(DateTime startDate, DateTime endDate, bool filterMasterCardUsers, bool filterVendorProcessor)
        {
            return GroupForAudienceSegments(Context.Database.SqlQuery<IssuerAssessmentUsageDetailReportResultItem>($"EXEC RetrieveIssuerAssessmentUsageDetailReport @StartDate = '{startDate}', @EndDate = '{endDate.AddDays(1)}', @FilterMasterCardUsers = {filterMasterCardUsers}, @FilterVendorProcessor = {filterVendorProcessor}"));
        }

        private IEnumerable<IssuerAssessmentUsageDetailReportResultItem> GroupForAudienceSegments(IEnumerable<IssuerAssessmentUsageDetailReportResultItem> resultItems)
        {
            return resultItems.GroupBy(i => new
            {
                User = i.User,
                UserName = i.UserName,
                IssuerId = i.IssuerId,
                Issuer = i.Issuer,
                ProcessorId = i.ProcessorId,
                Processor = i.Processor,
                ItemType = i.ItemType,
                Date = i.Date,
                VisitID = i.VisitID,
                What = i.What,
                From = i.From,
                Source = i.Source
            })
            .Select(i => new IssuerAssessmentUsageDetailReportResultItem
            {
                User = i.Key.User,
                UserName = i.Key.UserName,
                IssuerId = i.Key.IssuerId,
                Issuer = i.Key.Issuer,
                ProcessorId = i.Key.ProcessorId,
                Processor = i.Key.Processor,
                ItemType = i.Key.ItemType,
                Date = i.Key.Date,
                VisitID = i.Key.VisitID,
                What = i.Key.What,
                From = i.Key.From,
                Source = i.Key.Source,
                AudienceSegments = string.Join(", ", i.Select(s => s.AudienceSegments))
            });
        }

        public IEnumerable<IssuerAssessmentUsageLeadGensReportResultItem> GetIssuerAssessmentUsageLeadGensReport(DateTime startDate, DateTime endDate, bool filterMasterCardUsers, bool filterVendorProcessor)
        {
            return GroupForAudienceSegments(Context.Database.SqlQuery<IssuerAssessmentUsageLeadGensReportResultItem>($"EXEC RetrieveIssuerAssessmentUsageLeadGensReport @StartDate = '{startDate}', @EndDate = '{endDate.AddDays(1)}', @FilterMasterCardUsers = {filterMasterCardUsers}, @FilterVendorProcessor = {filterVendorProcessor}"));
        }

        private IEnumerable<IssuerAssessmentUsageLeadGensReportResultItem> GroupForAudienceSegments(IEnumerable<IssuerAssessmentUsageLeadGensReportResultItem> resultItems)
        {
            return resultItems.GroupBy(i => new
            {
                User = i.User,
                UserName = i.UserName,
                IssuerId = i.IssuerId,
                Issuer = i.Issuer,
                ProcessorId = i.ProcessorId,
                Processor = i.Processor,
                Date = i.Date,
            })
            .Select(i => new IssuerAssessmentUsageLeadGensReportResultItem
            {
                User = i.Key.User,
                UserName = i.Key.UserName,
                IssuerId = i.Key.IssuerId,
                Issuer = i.Key.Issuer,
                ProcessorId = i.Key.ProcessorId,
                Processor = i.Key.Processor,
                Date = i.Key.Date,
                AudienceSegments = string.Join(", ", i.Select(s => s.AudienceSegments))
            });
        }

        public IEnumerable<IssuerAssessmentUsageMatchingErrorsReportResultItem> GetIssuerAssessmentUsageMatchingErrorsReport(DateTime startDate, DateTime endDate, bool filterMasterCardUsers, bool filterVendorProcessor)
        {
            return GroupForAudienceSegments(Context.Database.SqlQuery<IssuerAssessmentUsageMatchingErrorsReportResultItem>($"EXEC RetrieveIssuerAssessmentUsageMatchingErrorsReport @StartDate = '{startDate}', @EndDate = '{endDate.AddDays(1)}', @FilterMasterCardUsers = {filterMasterCardUsers}, @FilterVendorProcessor = {filterVendorProcessor}"));
        }

        private IEnumerable<IssuerAssessmentUsageMatchingErrorsReportResultItem> GroupForAudienceSegments(IEnumerable<IssuerAssessmentUsageMatchingErrorsReportResultItem> resultItems)
        {
            return resultItems.GroupBy(i => new
            {
                User = i.User,
                UserName = i.UserName,
                IssuerId = i.IssuerId,
                Issuer = i.Issuer,
                ProcessorId = i.ProcessorId,
                Processor = i.Processor,
                Date = i.Date,
                Error = i.Error,
                Message = i.Message,
                ImportedIcaId = i.ImportedIcaId,
                LegalName = i.LegalName,
                CID = i.CID
            })
            .Select(i => new IssuerAssessmentUsageMatchingErrorsReportResultItem
            {
                User = i.Key.User,
                UserName = i.Key.UserName,
                IssuerId = i.Key.IssuerId,
                Issuer = i.Key.Issuer,
                ProcessorId = i.Key.ProcessorId,
                Processor = i.Key.Processor,
                Date = i.Key.Date,
                Error = i.Key.Error,
                Message = i.Key.Message,
                ImportedIcaId = i.Key.ImportedIcaId,
                LegalName = i.Key.LegalName,
                CID = i.Key.CID,
                AudienceSegments = string.Join(", ", i.Select(s => s.AudienceSegments))
            });
        }

        public IEnumerable<IssuerMatchingReportResultItem> GetIssuerMatchingReport()
        {
            return Context.Database.SqlQuery<IssuerMatchingReportResultItem>("EXEC RetrieveIssuerMatchingReport");
        }

        public IEnumerable<UsersSubscriptionReportBaseItem> GetContentItemUsersSubscriptions(DateTime startDate, DateTime endDate, bool filterMasterCardUsers, bool filterVendorProcessor, string regionId)
        {
            endDate = endDate.Date.AddDays(1);
            return from u in _userRepository.GetAll().Include(u => u.Issuer.Processors)
                                                      .Where(u => !u.IsDisabled
                                                               && (!filterMasterCardUsers || !u.Issuer.IsMastercardIssuer)
                                                               && (!filterVendorProcessor || !u.Issuer.Processors.Any(ip => ip.ProcessorId == MarketingCenterDbConstants.Processors.APlus)))
                   join s in Context.UserSubscriptions on u.UserId equals s.UserId into userSubscriptions
                   from us in userSubscriptions.DefaultIfEmpty()
                   join cius in Context.ContentItemUserSubscriptions on u.UserId equals cius.UserId
                   join ci in Context.ContentItems on cius.ContentItemId equals ci.ContentItemId
                   where cius.Subscribed
                   && cius.SavedDate >= startDate
                   && cius.SavedDate < endDate
                   && ci.RegionId.Trim().ToLower() == regionId.Trim().ToLower()
                   select new UsersSubscriptionReportBaseItem
                   {
                       UserId = u.UserId,
                       UserName = u.FullName,
                       IssuerId = u.Issuer.IssuerId,
                       IssuerName = u.Issuer.Title,
                       Frequency = us == null ? "Undefined" :
                                   us.UserSubscriptionFrequencyId == MarketingCenterDbConstants.UserSubscriptionFrequency.Daily ? "Daily" :
                                   us.UserSubscriptionFrequencyId == MarketingCenterDbConstants.UserSubscriptionFrequency.Weekly ? "Weekly" :
                                   "Never",
                       ContentItemId = cius.ContentItemId,
                       SavedDate = cius.SavedDate
                   };
        }

        private IQueryable<SubscribableItemsDetailReportResultItem> GetContentItemSuscribableQuery(DateTime startDate, DateTime endDate, string regionId)
        {
            return from u in _userRepository.GetAll().Where(u => !u.IsDisabled)
                   join s in Context.UserSubscriptions on u.UserId equals s.UserId into userSubscriptions
                   from us in userSubscriptions.DefaultIfEmpty()
                   join cius in Context.ContentItemUserSubscriptions on u.UserId equals cius.UserId
                   join ci in Context.ContentItems on cius.ContentItemId equals ci.ContentItemId
                   join la in Context.Assets on cius.ContentItemId equals la.ContentItemId into AssetContent
                   from contentAsset in AssetContent.DefaultIfEmpty()
                   join lb in Context.AssetFullWidths on cius.ContentItemId equals lb.ContentItemId into FullWidthContent
                   from contentFullWidths in FullWidthContent.DefaultIfEmpty()
                   join lc in Context.DownloadableAssets on cius.ContentItemId equals lc.ContentItemId into DownloadableContent
                   from contentDownloadable in DownloadableContent.DefaultIfEmpty()
                   join ld in Context.OrderableAssets on cius.ContentItemId equals ld.ContentItemId into OrderableContent
                   from contentOrderable in OrderableContent.DefaultIfEmpty()
                   join le in Context.Programs on cius.ContentItemId equals le.ContentItemId into ProgramContent
                   from contentProgram in ProgramContent.DefaultIfEmpty()
                   join lf in Context.Webinars on cius.ContentItemId equals lf.ContentItemId into WebinarContent
                   from contentWebinar in WebinarContent.DefaultIfEmpty()
                   join lg in Context.Pages on cius.ContentItemId equals lg.ContentItemId into PageContent
                   from contentPage in PageContent.DefaultIfEmpty()
                   join lh in Context.Snippets on cius.ContentItemId equals lh.ContentItemId into SnippetContent
                   from contentSnippet in SnippetContent.DefaultIfEmpty()
                   join li in Context.MarqueeSlides on cius.ContentItemId equals li.ContentItemId into MarqueeSlideContent
                   from contentMarqueeSlide in MarqueeSlideContent.DefaultIfEmpty()
                   join lj in Context.AssessmentCriterias on cius.ContentItemId equals lj.ContentItemId into AssessmentContent
                   from contentAssessmentSlide in AssessmentContent.DefaultIfEmpty()
                   join lk in Context.Recommendations on cius.ContentItemId equals lk.ContentItemId into RecommendationContent
                   from contentRecommendation in RecommendationContent.DefaultIfEmpty()
                   join ll in Context.HtmlDownloads on cius.ContentItemId equals ll.ContentItemId into HtmlContent
                   from contentHtml in HtmlContent.DefaultIfEmpty()
                   join lm in Context.QuickLinks on cius.ContentItemId equals lm.ContentItemId into QuickLinkContent
                   from contentQuickLink in QuickLinkContent.DefaultIfEmpty()
                   join ln in Context.UpcomingEvents on cius.ContentItemId equals ln.ContentItemId into UpcomingEventContent
                   from contentUpcomingEvent in UpcomingEventContent.DefaultIfEmpty()
                   join lo in Context.YoutubeVideos on cius.ContentItemId equals lo.ContentItemId into YoutubeVideoContent
                   from contentYoutubeVideo in YoutubeVideoContent.DefaultIfEmpty()

                   where cius.Subscribed
                   && cius.SavedDate >= startDate
                   && cius.SavedDate < endDate
                   && ci.RegionId.Trim().ToLower() == regionId.Trim().ToLower()
                   select new SubscribableItemsDetailReportResultItem
                   {
                       UserId = u.UserId,
                       UserName = u.FullName,
                       IssuerId = u.Issuer.IssuerId,
                       IssuerName = u.Issuer.Title,
                       Frequency = us == null ? "Undefined" :
                                   us.UserSubscriptionFrequencyId == MarketingCenterDbConstants.UserSubscriptionFrequency.Daily ? "Daily" :
                                   us.UserSubscriptionFrequencyId == MarketingCenterDbConstants.UserSubscriptionFrequency.Weekly ? "Weekly" :
                                   "Never",
                       SavedDate = cius.SavedDate,
                       ContentItemId = cius.ContentItemId,
                       ContentItemName = contentAsset.Title + contentFullWidths.Title + contentDownloadable.Title + contentOrderable.Title + contentProgram.Title + contentWebinar.Title + contentPage.Title + contentSnippet.Title + contentMarqueeSlide.Title + contentAssessmentSlide.Title + contentRecommendation.Title + contentHtml.Title + contentQuickLink.Title + contentUpcomingEvent.Title + contentYoutubeVideo.Title,
                       Favorites = userSubscriptions.Count()
                   };
        }

        public IQueryable GetContentItemPartBase(DateTime startDate, DateTime endDate, string regionId)
        {
            return from u in _userRepository.GetAll().Where(u => !u.IsDisabled)
                   join s in Context.UserSubscriptions on u.UserId equals s.UserId into userSubscriptions
                   from us in userSubscriptions.DefaultIfEmpty()
                   join cius in Context.ContentItemUserSubscriptions on u.UserId equals cius.UserId
                   join ci in Context.ContentItems on cius.ContentItemId equals ci.ContentItemId
                   where cius.Subscribed
                   && cius.SavedDate >= startDate
                   && cius.SavedDate < endDate
                   && ci.RegionId.Trim().ToLower() == regionId.Trim().ToLower()
                   select new SubscribableItemsDetailReportResultItem
                   {
                       UserId = u.UserId,
                       UserName = u.FullName,
                       IssuerId = u.Issuer.IssuerId,
                       IssuerName = u.Issuer.Title,
                       Frequency = us == null ? "Undefined" :
                                   us.UserSubscriptionFrequencyId == MarketingCenterDbConstants.UserSubscriptionFrequency.Daily ? "Daily" :
                                   us.UserSubscriptionFrequencyId == MarketingCenterDbConstants.UserSubscriptionFrequency.Weekly ? "Weekly" :
                                   "Never",
                       SavedDate = cius.SavedDate,
                       ContentItemId = cius.ContentItemId,
                       Favorites = userSubscriptions.Count()
                   };
        }

        public IEnumerable<SubscribableItemsDetailReportResultItem> GetContentItemSubscribableItems(DateTime startDate, DateTime endDate, string contentItemId, string regionId)
        {
            return GetContentItemSuscribableQuery(startDate, endDate.AddDays(1), regionId).Where(c => c.ContentItemId == contentItemId);
        }

        public IEnumerable<SubscribableItemsDetailReportResultItem> GetContentItemListSubscribableItems(DateTime startDate, DateTime endDate, string regionId)
        {
            return GetContentItemSuscribableQuery(startDate, endDate.AddDays(1), regionId).Where(c => c.Favorites > 0);
        }

        public IEnumerable<TriggerMarketingNotificationsReportBaseItem> GetTriggerMarketingNotifications(DateTime startDate, DateTime endDate, string regionId)
        {
            endDate = endDate.Date.AddDays(1);
            return (from md in Context.MailDispatchers
                    where (md.TrackingType == Constants.TrackingTypes.TriggerMarketingSubscriptionDaily || md.TrackingType == Constants.TrackingTypes.TriggerMarketingSubscriptionWeekly)
                    && (md.MailDispatcherStatusId == MarketingCenterDbConstants.MailDispatcherStatus.Sent || md.MailDispatcherStatusId == MarketingCenterDbConstants.MailDispatcherStatus.Opened)
                    && md.CreatedDate >= startDate
                    && md.CreatedDate < endDate
                    && md.RegionId.Trim().ToLower() == regionId.Trim().ToLower()
                    select new TriggerMarketingNotificationsReportBaseItem
                    {
                        MailDispatcherId = md.MailDispatcherId,
                        Date = md.CreatedDate,
                        TrackingType = md.TrackingType,
                        UserId = md.UserId,
                        Opened = md.MailDispatcherStatusId == MarketingCenterDbConstants.MailDispatcherStatus.Opened,
                        TotalLinks = md.TotalLinks
                    }
                );
        }

        public IEnumerable<TriggerMarketingNotificationsDetailReportResultItem> GetTriggerMarketingNotificationsDetail(string notificationTrackingType, DateTime startDate, DateTime endDate, string regionId)
        {
            endDate = endDate.Date.AddDays(1);
            return (from u in _userRepository.GetAll()
                    join md in Context.MailDispatchers on u.UserId equals md.UserId
                    where md.TrackingType == notificationTrackingType
                    && (md.MailDispatcherStatusId == MarketingCenterDbConstants.MailDispatcherStatus.Sent || md.MailDispatcherStatusId == MarketingCenterDbConstants.MailDispatcherStatus.Opened)
                    && md.CreatedDate >= startDate
                    && md.CreatedDate < endDate
                    && md.RegionId.Trim().ToLower() == regionId.Trim().ToLower()
                    select new TriggerMarketingNotificationsDetailReportResultItem
                    {
                        UserId = u.UserId,
                        UserName = u.FullName,
                        IssuerId = u.Issuer.IssuerId,
                        IssuerName = u.Issuer.Title,
                        MailDispatcherId = md.MailDispatcherId,
                        Opened = md.MailDispatcherStatusId == MarketingCenterDbConstants.MailDispatcherStatus.Opened,
                        TotalLinks = md.TotalLinks
                    });
        }

        public IEnumerable<TriggerMarketingNotificationClickedLink> GetTriggerMarketingNotificationClickedLinks(DateTime startDate)
        {
            using (Context.ConnectionStateOpenAction = new ConnectionStateAction(() => _siteTrackingRepository.UpdateSiteTrackingContext(startDate),
                                                                                 () => _siteTrackingRepository.UpdateSiteTrackingContext(startDate, true)))
            {
                return (from st in Context.ViewSiteTrackingByContext
                        join u in _userRepository.GetAll() on ("mastercardmembers:" + st.Who) equals u.UserName
                        where st.When >= startDate && (st.How.StartsWith(Constants.TrackingTypes.TriggerMarketingSubscriptionDaily) ||
                                                       st.How.StartsWith(Constants.TrackingTypes.TriggerMarketingSubscriptionWeekly))
                        select new TriggerMarketingNotificationClickedLink
                        {
                            UserId = u.UserId,
                            Link = st.What,
                            TrackingType = st.How,
                            Date = st.When
                        })
                    .ToArray() // To ensure that the ViewSiteTrackingByContext uses the right context.
                    .Select(tmn => new TriggerMarketingNotificationClickedLink
                    {
                        UserId = tmn.UserId,
                        Link = tmn.Link,
                        TrackingType = tmn.TrackingType,
                        TriggerMarketingSubscription = tmn.TrackingType.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0],
                        MailDispatcherId = tmn.TrackingType.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[1],
                        LinkNumber = tmn.TrackingType.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[2],
                        Date = tmn.Date
                    });
            }
        }

        public IEnumerable<EstimatedDistributionReportResultItem> GetEstimatedDistributionProgramReport(DateTime startDate, DateTime endDate, IEnumerable<string> segmentationIds, string regionId, bool filterMastercardUsers)
        {
            using (Context.ConnectionStateOpenAction = new ConnectionStateAction(() => _siteTrackingRepository.UpdateSiteTrackingContext(startDate),
                                                                                 () => _siteTrackingRepository.UpdateSiteTrackingContext(startDate, true)))
            {
                endDate = endDate.Date.AddDays(1);
                var users = GetFilteredUsers(segmentationIds, filterMastercardUsers);
                var downloads = GetEstimatedDistributionDownloads(startDate, endDate, users);

                return (from daed in Context.DownloadableAssetEstimatedDistributions
                        join ci in Context.ContentItems on daed.ContentItemId equals ci.ContentItemId
                        join u in users on daed.UserId equals u.UserId
                        join d in downloads on daed.ContentItemId equals d.ContentItemId
                        join darp in Context.DownloadableAssetRelatedPrograms on daed.ContentItemId equals darp.DownloadableAssetContentItemId
                        where ci.RegionId == regionId && daed.DownloadDate >= startDate && daed.DownloadDate < endDate && !darp.ProgramContentItemId.EndsWith("-p")
                        group daed by new { darp.ProgramContentItemId, d.TotalDownloads, d.MostRecentDownload } into ed
                        select new EstimatedDistributionReportResultItem
                        {
                            ContentItemId = ed.Key.ProgramContentItemId,
                            TotalDownloads = ed.Key.TotalDownloads,
                            TotalEstimatedDistribution = ed.Sum(x => x.EstimatedDistribution),
                            MostRecentDownload = ed.Key.MostRecentDownload
                        }
                       ).ToArray(); // To ensure that the ViewSiteTrackingByContext uses the right context.
            }
        }

        public IEnumerable<EstimatedDistributionDetailReportResultItem> GetEstimatedDistributionProgramDetailReport(DateTime startDate, DateTime endDate, IEnumerable<string> segmentationIds, string contentItemId, bool filterMastercardUsers)
        {
            using (Context.ConnectionStateOpenAction = new ConnectionStateAction(() => _siteTrackingRepository.UpdateSiteTrackingContext(startDate),
                                                                                 () => _siteTrackingRepository.UpdateSiteTrackingContext(startDate, true)))
            {
                endDate = endDate.Date.AddDays(1);
                var users = GetFilteredUsers(segmentationIds, filterMastercardUsers);
                var downloads = GetEstimatedDistributionDownloads(startDate, endDate, users);

                return (from daed in Context.DownloadableAssetEstimatedDistributions
                        join u in users on daed.UserId equals u.UserId
                        join d in downloads on daed.ContentItemId equals d.ContentItemId
                        join darp in Context.DownloadableAssetRelatedPrograms on daed.ContentItemId equals darp.DownloadableAssetContentItemId
                        where darp.ProgramContentItemId == contentItemId && daed.DownloadDate >= startDate && daed.DownloadDate < endDate
                        group daed by new { daed.ContentItemId, d.MostRecentDownload, User = u } into ed
                        select new EstimatedDistributionDetailReportResultItem
                        {
                            ContentItemId = ed.Key.ContentItemId,
                            LastRequestDate = ed.Max(x => x.DownloadDate),
                            MostRecentDownload = ed.Key.MostRecentDownload,
                            EstimatedDistribution = ed.Sum(x => x.EstimatedDistribution),
                            Issuer = ed.Key.User.Issuer,
                            Segmentations = ed.Key.User.Issuer.IssuerSegmentations.Select(s => s.Segmentation),
                            User = ed.Key.User.FullName
                        }
                       ).ToArray(); // To ensure that the ViewSiteTrackingByContext uses the right context.
            }
        }

        public IEnumerable<EstimatedDistributionReportResultItem> GetEstimatedDistributionAssetReport(DateTime startDate, DateTime endDate, IEnumerable<string> segmentationIds, string regionId, bool filterMastercardUsers)
        {
            using (Context.ConnectionStateOpenAction = new ConnectionStateAction(() => _siteTrackingRepository.UpdateSiteTrackingContext(startDate),
                                                                                 () => _siteTrackingRepository.UpdateSiteTrackingContext(startDate, true)))
            {
                endDate = endDate.Date.AddDays(1);
                var users = GetFilteredUsers(segmentationIds, filterMastercardUsers);
                var downloads = GetEstimatedDistributionDownloads(startDate, endDate, users);

                return (from daed in Context.DownloadableAssetEstimatedDistributions
                        join ci in Context.ContentItems on daed.ContentItemId equals ci.ContentItemId
                        join u in users on daed.UserId equals u.UserId
                        join d in downloads on daed.ContentItemId equals d.ContentItemId
                        join darp in Context.DownloadableAssetRelatedPrograms on daed.ContentItemId equals darp.DownloadableAssetContentItemId into downloadableAssetRelatedPrograms
                        from rp in downloadableAssetRelatedPrograms.DefaultIfEmpty()
                        where ci.RegionId == regionId && daed.DownloadDate >= startDate && daed.DownloadDate < endDate
                        group daed by new { daed.ContentItemId, d.TotalDownloads, d.MostRecentDownload, RelatedProgram = rp } into ed
                        select new EstimatedDistributionReportResultItem
                        {
                            ContentItemId = ed.Key.ContentItemId,
                            TotalDownloads = ed.Key.TotalDownloads,
                            TotalEstimatedDistribution = ed.Sum(x => x.EstimatedDistribution),
                            MostRecentDownload = ed.Key.MostRecentDownload,
                            RelatedProgram = ed.Key.RelatedProgram
                        }
                       ).ToArray(); // To ensure that the ViewSiteTrackingByContext uses the right context.
            }
        }

        public IEnumerable<EstimatedDistributionDetailReportResultItem> GetEstimatedDistributionAssetDetailReport(DateTime startDate, DateTime endDate, IEnumerable<string> segmentationIds, string contentItemId, bool filterMastercardUsers)
        {
            using (Context.ConnectionStateOpenAction = new ConnectionStateAction(() => _siteTrackingRepository.UpdateSiteTrackingContext(startDate),
                                                                                 () => _siteTrackingRepository.UpdateSiteTrackingContext(startDate, true)))
            {
                endDate = endDate.Date.AddDays(1);
                var users = GetFilteredUsers(segmentationIds, filterMastercardUsers);
                var downloads = GetEstimatedDistributionDownloads(startDate, endDate, users);

                return (from daed in Context.DownloadableAssetEstimatedDistributions
                        join ci in Context.ContentItems on daed.ContentItemId equals ci.ContentItemId
                        join u in users on daed.UserId equals u.UserId
                        join d in downloads on daed.ContentItemId equals d.ContentItemId
                        where ci.ContentItemId == contentItemId && daed.DownloadDate >= startDate && daed.DownloadDate < endDate
                        group daed by new { daed.ContentItemId, d.MostRecentDownload, User = u } into ed
                        select new EstimatedDistributionDetailReportResultItem
                        {
                            ContentItemId = ed.Key.ContentItemId,
                            LastRequestDate = ed.Max(x => x.DownloadDate),
                            MostRecentDownload = ed.Key.MostRecentDownload,
                            EstimatedDistribution = ed.Sum(x => x.EstimatedDistribution),
                            Issuer = ed.Key.User.Issuer,
                            Segmentations = ed.Key.User.Issuer.IssuerSegmentations.Select(s => s.Segmentation),
                            User = ed.Key.User.FullName
                        }
                       ).ToArray(); // To ensure that the ViewSiteTrackingByContext uses the right context.
            }
        }

        public IEnumerable<EstimatedDistributionReportResultItem> GetEstimatedDistributionIssuerReport(DateTime startDate, DateTime endDate, IEnumerable<string> segmentationIds, string regionId, bool filterMastercardUsers)
        {
            using (Context.ConnectionStateOpenAction = new ConnectionStateAction(() => _siteTrackingRepository.UpdateSiteTrackingContext(startDate),
                                                                                 () => _siteTrackingRepository.UpdateSiteTrackingContext(startDate, true)))
            {
                endDate = endDate.Date.AddDays(1);
                var users = GetFilteredUsers(segmentationIds, filterMastercardUsers);
                var downloads = GetEstimatedDistributionDownloads(startDate, endDate, users);

                return (from daed in Context.DownloadableAssetEstimatedDistributions
                        join ci in Context.ContentItems on daed.ContentItemId equals ci.ContentItemId
                        join u in users on daed.UserId equals u.UserId
                        join d in downloads on daed.ContentItemId equals d.ContentItemId
                        where ci.RegionId == regionId && daed.DownloadDate >= startDate && daed.DownloadDate < endDate
                        group daed by new { d.TotalDownloads, d.MostRecentDownload, u.Issuer } into ed
                        select new EstimatedDistributionReportResultItem
                        {
                            ContentItemId = ed.Key.Issuer.IssuerId,
                            Title = ed.Key.Issuer.Title,
                            TotalDownloads = ed.Key.TotalDownloads,
                            TotalEstimatedDistribution = ed.Sum(x => x.EstimatedDistribution),
                            MostRecentDownload = ed.Key.MostRecentDownload
                        }
                       ).ToArray(); // To ensure that the ViewSiteTrackingByContext uses the right context.
            }
        }

        public IEnumerable<EstimatedDistributionDetailReportResultItem> GetEstimatedDistributionIssuerDetailReport(DateTime startDate, DateTime endDate, IEnumerable<string> segmentationIds, string issuerId, bool filterMastercardUsers)
        {
            using (Context.ConnectionStateOpenAction = new ConnectionStateAction(() => _siteTrackingRepository.UpdateSiteTrackingContext(startDate),
                                                                                 () => _siteTrackingRepository.UpdateSiteTrackingContext(startDate, true)))
            {
                endDate = endDate.Date.AddDays(1);
                var users = GetFilteredUsers(segmentationIds, filterMastercardUsers);
                var downloads = GetEstimatedDistributionDownloads(startDate, endDate, users);

                return (from daed in Context.DownloadableAssetEstimatedDistributions
                        join u in users on daed.UserId equals u.UserId
                        join d in downloads on daed.ContentItemId equals d.ContentItemId
                        join darp in Context.DownloadableAssetRelatedPrograms on daed.ContentItemId equals darp.DownloadableAssetContentItemId into downloadableAssetRelatedPrograms
                        from rp in downloadableAssetRelatedPrograms.DefaultIfEmpty()
                        where u.IssuerId == issuerId && daed.DownloadDate >= startDate && daed.DownloadDate < endDate
                        group daed by new { daed.ContentItemId, d.MostRecentDownload, User = u, RelatedProgram = rp } into ed
                        select new EstimatedDistributionDetailReportResultItem
                        {
                            ContentItemId = ed.Key.ContentItemId,
                            LastRequestDate = ed.Max(x => x.DownloadDate),
                            MostRecentDownload = ed.Key.MostRecentDownload,
                            EstimatedDistribution = ed.Sum(x => x.EstimatedDistribution),
                            Issuer = ed.Key.User.Issuer,
                            User = ed.Key.User.FullName,
                            RelatedProgram = ed.Key.RelatedProgram
                        }
                       ).ToArray(); // To ensure that the ViewSiteTrackingByContext uses the right context.
            }
        }

        private IQueryable<User> GetFilteredUsers(IEnumerable<string> segmentationIds, bool filterMastercardUsers)
        {
            return from i in _issuerRepository.GetAll()
                   join u in _userRepository.GetAll() on i.IssuerId equals u.IssuerId
                   where (!filterMastercardUsers || !i.IsMastercardIssuer) &&
                         (!segmentationIds.Any() || !i.IssuerSegmentations.Any() || i.IssuerSegmentations.Any(s => segmentationIds.Contains(s.SegmentationId)))
                   select u;
        }

        private IQueryable<EstimatedDistributionDownload> GetEstimatedDistributionDownloads(DateTime startDate, DateTime endDate, IQueryable<User> users)
        {
            return from st in Context.ViewSiteTrackingByContext
                   join u in users on st.Who equals u.UserName
                   where st.When >= startDate && st.When < endDate &&
                         (st.PageGroup.Equals(Constants.SiteTracking.PageGroup.LibraryDownload) || st.PageGroup.Equals(Constants.SiteTracking.PageGroup.ElectronicDownload))
                   group st by st.ContentItemID into gstd
                   select new EstimatedDistributionDownload
                   {
                       ContentItemId = gstd.Key,
                       TotalDownloads = gstd.Count(),
                       MostRecentDownload = gstd.Max(x => x.When)
                   };
        }

        public IEnumerable<ClonedAssetReportResultItem> GetClonedAssetsReport(string regionId)
        {
            return Context.Database.SqlQuery<ClonedAssetReportResultItem>("RetrieveClonedAssetsReport @RegionId={0}", regionId);
        }

        public IEnumerable<RetrieveFilterUsersResult> GetFilterUsers(string name, string email, string issuer, string processorId, string regionId)
        {
            return Context.Database.SqlQuery<RetrieveFilterUsersResult>(
                "RetrieveFilterUsers @Name={0}, @Email={1}, @Issuer={2}, @ProcessorId={3}, @RegionId={4}",
                name,
                email,
                issuer,
                processorId,
                regionId);
        }

        public IEnumerable<RetrieveFilterOrdersResult> GetFilterOrders(string orderNo, string name, string email, string issuer, DateTime? startDate, DateTime? endDate, string status, string processorId)
        {
            return Context.Database.SqlQuery<RetrieveFilterOrdersResult>("" +
                "RetrieveFilterOrders @OrderNumber={0}, @Name={1}, @Email={2}, @Issuer={3}, @StartDate={4}, @EndDate={5}, @OrderStatus={6}, @ProcessorId={7}",
                orderNo, name, email, issuer, startDate, endDate, status, processorId);
        }

        public IEnumerable<RetrieveOrderStatusesResult> GetOrderStatuses(string processorId)
        {
            return Context.Database.SqlQuery<RetrieveOrderStatusesResult>("RetrieveOrderStatuses @ProcessorId={0}", processorId);
        }

        public IEnumerable<GdprFollowUpReportResultItem> GetGdprFollowUpReport(string region, string[] trackingTypes)
        {
            return from md in (from m in Context.MailDispatchers
                               where m.RegionId.Equals(region, StringComparison.InvariantCultureIgnoreCase) &&
                                     (m.MailDispatcherStatusId == MarketingCenterDbConstants.MailDispatcherStatus.Sent || m.MailDispatcherStatusId == MarketingCenterDbConstants.MailDispatcherStatus.Opened) &&
                                     trackingTypes.Contains(m.TrackingType)
                               group m by new { m.UserId } into md
                               select md.OrderByDescending(m => m.CreatedDate).FirstOrDefault())
                   join u in _userRepository.GetAll() on md.UserId equals u.UserId
                   join am in Context.Aspnet_Membership on u.Email equals am.Email
                   join us in Context.UserSubscriptions on u.UserId equals us.UserId
                   join usf in Context.UserSubscriptionFrequencies on us.UserSubscriptionFrequencyId equals usf.UserSubscriptionFrequencyId
                   join fs in Context.GdprFollowUpStatuses on u.Profile.GdprFollowUpStatusId equals fs.GdprFollowUpStatusId
                   where !u.IsDisabled
                         && u.Region.Equals(region, StringComparison.InvariantCultureIgnoreCase)
                         && u.Email.Contains("@")
                   select new GdprFollowUpReportResultItem
                   {
                       Email = u.Email,
                       FullName = u.FullName,
                       NotificationFrequency = usf.Frequency,
                       RegistrationDate = am.CreateDate,
                       LastLoginDate = am.LastLoginDate,
                       GdprFollowUpStatus = fs.Name,
                       SentDate = md.CreatedDate,
                       OpenedDate = md.MailDispatcherStatusId == MarketingCenterDbConstants.MailDispatcherStatus.Opened ? md.ModifiedDate : (DateTime?)null
                   };
        }
    }
}