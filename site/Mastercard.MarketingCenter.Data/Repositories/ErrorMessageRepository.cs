﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Repositories
{
    public class ErrorMessageRepository : IErrorMessageRepository
    {
        private readonly MarketingCenterDbContext _context;

        public ErrorMessageRepository(MarketingCenterDbContext context)
        {
            _context = context;
        }

        public ErrorMessage GetById(int listId, string eventName)
        {
            return _context.ErrorMessage.FirstOrDefault(e => e.ListId.Equals(listId) && e.EventName.Equals(eventName));
        }
    }
}