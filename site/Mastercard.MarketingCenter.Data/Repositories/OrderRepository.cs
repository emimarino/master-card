﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Repositories
{
    public class OrderRepository : Repository<MarketingCenterDbContext>, IOrderRepository
    {
        private readonly string _ordersMMC;
        private readonly string _ordersMOS;
        private readonly IUserRepository _userRepository;

        public OrderRepository(MarketingCenterDbContext context, IUserRepository userRepository) : base(context)
        {
            _userRepository = userRepository;
            _ordersMMC = App_GlobalResources.NoteComments.OrdersMMC;
            _ordersMOS = App_GlobalResources.NoteComments.OrdersMOS;
        }

        public IQueryable<OrderDownload> GetOrderDownloads(
            string individualFirstName = null,
            string individualLastName = null,
            string individualEmail = null,
            string localIssuerName = null,
            string noteComment = null,
            string CID = null,
            string importedIssuerName = null,
            string regionId = null,
            string relatedProgram = null,
            string itemName = null,
            string fulfilmentType = null,
            int? itemQuantityMin = null,
            int? itemQuantityMax = null,
            int? estimatedDistributionMin = null,
            int? estimatedDistributionMax = null,
            bool includeLeftOutResults = false,
            DateTime? startTime = null,
            DateTime? endTime = null
        )
        {
            var result = from downloads in Context.DomoDownloadsApis
                         join da in Context.DownloadableAssets on downloads.ContentItemId equals da.ContentItemId
                         /*left*/
                         join darpn in Context.DownloadableAssetRelatedPrograms on downloads.ContentItemId equals darpn.DownloadableAssetContentItemId into darpnull
                         from darp in darpnull.DefaultIfEmpty()
                             /*left*/
                         join prn in Context.Programs on darp.ProgramContentItemId equals prn.ContentItemId into prnull
                         from pr in prnull.DefaultIfEmpty()
                             // where downloads.DownloadDate < startTime && downloads.DownloadDate > endTime
                         where (includeLeftOutResults || !downloads.HiddenResult)
                         select (new OrderDownload
                         {
                             IndividualEmail = downloads.UserEmail,
                             IndividualFirstName = downloads.UserFirstName,
                             IndividualLastName = downloads.UserLastName,
                             CID = downloads.UserFinancialInstitutionCID,
                             ImportedIssuerName = downloads.UserFinancialInstitutionImportedName ?? downloads.UserFinancialInstitution,
                             EstimatedDistribution = 0,
                             FulfilmentType = "",
                             ItemName = downloads.ContentItemTitle,
                             ItemQuantity = 0,
                             LocalIssuerName = downloads.UserFinancialInstitution,
                             NoteComment = (downloads.ContentItemRegion.Equals("us") ? _ordersMMC : _ordersMOS).Replace("{0}", downloads.FileName)
                                                                                                               .Replace("{1}", downloads.ContentItemTitle)
                                                                                                               .Replace("{2}", "Asset")
                                                                                                               .Replace("{3}", downloads.ContentItemId),
                             RegionId = downloads.ContentItemRegion,
                             RelatedProgram = pr.Title ?? "",
                             ActivityDate = downloads.DownloadDate
                         });

            var downloadsRecords = from oa in Context.OrderableAssets
                                       /*left*/
                                   join oarpn in Context.OrderableAssetRelatedPrograms on oa.ContentItemId equals oarpn.OrderableAssetContentItemId into oarpnull
                                   from oarp in oarpnull.DefaultIfEmpty()
                                       /*left*/
                                   join prn in Context.Programs on oarp.ProgramContentItemId equals prn.ContentItemId into prnull
                                   from pr in prnull.DefaultIfEmpty()
                                   join oi in Context.OrderItems on oa.ContentItemId equals oi.ItemId
                                   join o in Context.Orders on oi.OrderId equals o.OrderId
                                   join u in _userRepository.GetAll() on o.UserId equals u.UserName
                                   join ci in Context.ContentItems on oa.ContentItemId equals ci.ContentItemId
                                   /*left*/
                                   join iin in Context.ImportedIcas on u.Issuer.Cid equals iin.Cid into iinull
                                   from ii in iinull.DefaultIfEmpty()
                                   where (o.OrderDate >= startTime && o.OrderDate <= endTime)
                                   && (includeLeftOutResults || (!u.ExcludedFromDomoApi && !u.Issuer.ExcludedFromDomoApi))
                                   select (new OrderDownload
                                   {
                                       IndividualEmail = u.Email,
                                       IndividualFirstName = u.Profile.FirstName,
                                       IndividualLastName = u.Profile.LastName,
                                       CID = ii.Cid,
                                       ImportedIssuerName = ii.LegalName ?? u.Issuer.Title,
                                       EstimatedDistribution = oi.EstimatedDistribution,
                                       FulfilmentType = oi.ElectronicDelivery ? "Electronic Delivery" : ((oa.Vdp != null && (bool)oa.Vdp) ? "VDP" : "Print On Demand"),
                                       ItemName = oa.Title,
                                       ItemQuantity = oi.ItemQuantity,
                                       LocalIssuerName = u.Issuer.Title,
                                       NoteComment = (ci.RegionId.Equals("us") ? _ordersMMC : _ordersMOS).Replace("{0}", oa.ElectronicDeliveryFile)
                                                                                                         .Replace("{1}", oa.Title)
                                                                                                         .Replace("{2}", "OrderableAsset")
                                                                                                         .Replace("{3}", oi.ItemId),
                                       RegionId = ci.RegionId,
                                       RelatedProgram = pr.Title ?? "",
                                       ActivityDate = o.OrderDate
                                   });

            result = result.Union(downloadsRecords);

            if (!string.IsNullOrEmpty(individualFirstName))
            {
                result = result.Where(r => r.IndividualFirstName.Equals(individualFirstName));
            }
            if (!string.IsNullOrEmpty(individualLastName))
            {
                result = result.Where(r => r.IndividualLastName.Equals(individualLastName));
            }
            if (!string.IsNullOrEmpty(individualEmail))
            {
                result = result.Where(r => r.IndividualEmail.Equals(individualEmail));
            }
            if (!string.IsNullOrEmpty(localIssuerName))
            {
                result = result.Where(r => r.LocalIssuerName.Equals(localIssuerName));
            }
            if (!string.IsNullOrEmpty(noteComment))
            {
                result = result.Where(r => r.NoteComment.Contains(noteComment));
            }
            if (CID != null)
            {
                result = result.Where(r => r.CID.Equals(CID));
            }
            if (!string.IsNullOrEmpty(importedIssuerName))
            {
                result = result.Where(r => r.ImportedIssuerName.Equals(importedIssuerName));
            }
            if (!string.IsNullOrEmpty(regionId))
            {
                result = result.Where(r => r.RegionId.Equals(regionId));
            }
            if (!string.IsNullOrEmpty(relatedProgram))
            {
                result = result.Where(r => r.RelatedProgram.Contains(relatedProgram));
            }
            if (!string.IsNullOrEmpty(itemName))
            {
                result = result.Where(r => r.ItemName.Contains(itemName));
            }
            if (!string.IsNullOrEmpty(fulfilmentType))
            {
                result = result.Where(r => r.FulfilmentType.Equals(fulfilmentType));
            }
            if (itemQuantityMin != null)
            {
                result = result.Where(r => r.ItemQuantity >= itemQuantityMin);
            }
            if (itemQuantityMax != null)
            {
                result = result.Where(r => r.ItemQuantity <= itemQuantityMax);
            }
            if (estimatedDistributionMin != null)
            {
                result = result.Where(r => r.EstimatedDistribution >= estimatedDistributionMin);
            }
            if (estimatedDistributionMax != null)
            {
                result = result.Where(r => r.EstimatedDistribution <= estimatedDistributionMax);
            }

            return result.Distinct();
        }
    }
}