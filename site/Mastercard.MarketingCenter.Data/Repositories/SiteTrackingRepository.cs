﻿using System;
using System.Data.Entity;

namespace Mastercard.MarketingCenter.Data
{
    public class SiteTrackingRepository : Repository<MarketingCenterDbContext>, ISiteTrackingRepository
    {
        public SiteTrackingRepository(MarketingCenterDbContext context) : base(context)
        {
        }

        public void ProcessContentItemVisits()
        {
            Context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "EXEC ProcessContentItemVisits");
        }

        public void ArchiveSiteTracking()
        {
            Context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "EXEC [Archive].[ArchiveSiteTracking_insert]");
        }

        public void DeleteArchivedSiteTracking()
        {
            Context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "EXEC [Archive].[SiteTracking_deleteOld]");
        }

        public void UpdateSiteTrackingContext(DateTime startDate, bool resetContext = false)
        {
            Context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, "EXEC UpdateSiteTrackingContext @StartDate = {0}, @ResetContext = {1}", startDate, resetContext);
        }
    }
}