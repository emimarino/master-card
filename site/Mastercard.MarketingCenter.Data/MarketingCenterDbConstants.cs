﻿namespace Mastercard.MarketingCenter.Data
{
    public static class MarketingCenterDbConstants
    {
        public static class AssetTypes
        {
            public const string Insight = "Insight";
            public const string InsightsThoughtLeadership = "Insights/Thought Leadership";
            public const string Marketing = "Marketing";
            public const string Product = "Product";
            public const string Products = "Products";
            public const string SafetySecurity = "Safety & Security";
            public const string Segment = "Segment";
            public const string Solution = "Solution";
        }

        public static class ContentItemBase
        {
            public const string ModifiedDate = "ModifiedDate";
            public const string ModifiedByUserID = "ModifiedByUserID";
        }

        public static class ContentTypeIds
        {
            public const string Asset = "b";
            public const string AssetFullWidth = "a";
            public const string DownloadableAsset = "c";
            public const string Faq = "10";
            public const string Offer = "o";
            public const string OrderableAsset = "d";
            public const string Page = "1";
            public const string Program = "e";
            public const string Snippet = "e8";
            public const string QuickLink = "ql";
            public const string MarqueeSlide = "11";
            public const string Webinar = "f";

            public static readonly string[] BusinessOwnerIds = { Asset, AssetFullWidth, DownloadableAsset, OrderableAsset, Program };
            public static readonly string[] BusinessOwnerNotificationIds = { Asset, DownloadableAsset };
        }

        public static class CrossSellBoxes
        {
            public const string Insights = "Insights";
            public const string Marketing = "Marketing";
            public const string Products = "Products";
            public const string SafetySecurity = "Safety & Security";
            public const string Segments = "Segments";
            public const string Solutions = "Solutions";
        }

        public static class FeatureLocations
        {
            public const string Home = "14";
            public const string HomeCarousel = "c39b";
            public const string InsightsRecentWebinarsWhitepapers = "rw";
            public const string Login = "103";
            public const string OptimizationDashboard = "repdash";
            public const string Optimize = "optimize";
            public const string Report = "rep";
            public const string ReportRecommendatiion = "rc";
            public const string TagBrowser = "2c";
            public const string TriggerMarketingEmailHeader = "tmeh";
        }

        public static class FieldNames
        {
            public const string AllowCloning = "AllowCloning";
            public const string AssetAttachment = "AssetAttachment";
            public const string AssetType = "AssetType";
            public const string BusinessOwnerId = "BusinessOwnerID";
            public const string ContentItemId = "ContentItemId";
            public const string ContentItemCrossBorderRegion = "ContentItemCrossBorderRegion";
            public const string CurrentExpirationDate = "CurrentExpirationDate";
            public const string DownloadableAssetAttachment = "DownloadableAssetAttachment";
            public const string EnableFavoriting = "EnableFavoriting";
            public const string ExpirationDate = "ExpirationDate";
            public const string Filename = "Filename";
            public const string IconId = "IconID";
            public const string InMarketEndDate = "InMarketEndDate";
            public const string InMarketStartDate = "InMarketStartDate";
            public const string Language = "Language";
            public const string LongDescription = "LongDescription";
            public const string MainImage = "MainImage";
            public const string NotifyWhenSourceChanges = "NotifyWhenSourceChanges";
            public const string OriginalContentItemId = "OriginalContentItemId";
            public const string RegionId = "RegionId";
            public const string RegionTitle = "RegionTitle";
            public const string ShortDescription = "ShortDescription";
            public const string ShowInMarketingCalendar = "ShowInMarketingCalendar";
            public const string ThumbnailImage = "ThumbnailImage";
            public const string Title = "Title";
            public const string Uri = "Uri";
        }

        public static class GdprFollowUpStatus
        {
            public const int First = 1;
            public const int Second = 2;
            public const int Deleted = 3;
        }

        public static class ListIds
        {
            public const int TagListId = 2009;
            public const int CampaignEventID = 2012;
        }

        public static class MarketingCalendarListIds
        {
            public const string CampaignCategory = "2010";
            public const string CampaignEvent = "2012";
            public const string Category = "10";
            public const string CardExclusivity = "11";
            public const string OfferType = "12";
            public const string SearchTerm = "2008";
        }

        public static class BrokenLinkSearchTerms
        {
            public const string ContentType = "Search Term";
        }

        public static class FileColumns
        {
            public const string VideoHtmlContent = "VideoHtmlContent";
        }

        public static class Processors
        {
            public const string APlus = "91b";
            public const string MasterCard = "918";
        }

        public static class Snippets
        {
            public const string ThoughLeadershipFeaturedSnippet = "[[TLLP_Webinar_Title]]";
        }

        public static class Tables
        {
            public const string Aspnet_Membership = "aspnet_Membership";
            public const string Aspnet_Roles = "aspnet_Roles";
            public const string Aspnet_Users = "aspnet_Users";
            public const string AssessmentCriteria = "AssessmentCriteria";
            public const string AssessmentReport = "AssessmentReport";
            public const string Asset = "Asset";
            public const string AssetAttachment = "AssetAttachment";
            public const string AssetBrowseAllMaterialTag = "AssetBrowseAllMaterialTag";
            public const string AssetFullWidth = "AssetFullWidth";
            public const string AssetFullWidthAttachment = "AssetFullWidthAttachment";
            public const string AssetFullWidthBrowseAllMaterialTag = "AssetFullWidthBrowseAllMaterialTag";
            public const string AssetFullWidthRelatedProgram = "AssetFullWidthRelatedProgram";
            public const string AssetRelatedProgram = "AssetRelatedProgram";
            public const string BrokenContentLink = "BrokenContentLink";
            public const string CampaignEvent = "CampaignEvent";
            public const string CampaignEventCampaignCategory = "CampaignEventCampaignCategory";
            public const string CampaignCategory = "CampaignCategory";
            public const string CampaignCategoryTranslatedContent = "CampaignCategoryTranslatedContent";
            public const string CardExclusivity = "CardExclusivity";
            public const string Category = "Category";
            public const string CloneSourceNotification = "CloneSourceNotification";
            public const string CmsRoleAccess = "CmsRoleAccess";
            public const string ConsentFileData = "ConsentFileData";
            public const string ContentItem = "ContentItem";
            public const string ContentItemCampaignEvent = "ContentItemCampaignEvent";
            public const string ContentItemCardExclusivity = "ContentItemCardExclusivity";
            public const string ContentItemCategory = "ContentItemCategory";
            public const string ContentItemCloneToRegion = "ContentItemCloneToRegion";
            public const string ContentItemCrossBorderRegion = "ContentItemCrossBorderRegion";
            public const string ContentItemFeatureLocation = "ContentItemFeatureLocation";
            public const string ContentItemFeatureOnTag = "ContentItemFeatureOnTag";
            public const string ContentItemRegionVisibility = "ContentItemRegionVisibility";
            public const string ContentItemRelatedItem = "ContentItemRelatedItem";
            public const string ContentItemSegmentation = "ContentItemSegmentation";
            public const string ContentItemTag = "ContentItemTag";
            public const string ContentItemTriggerMarketing = "ContentItemTriggerMarketing";
            public const string ContentItemUserSubscription = "ContentItemUserSubscription";
            public const string ContentItemVersion = "ContentItemVersion";
            public const string ContentType = "ContentType";
            public const string ContentTypeClientEvent = "ContentTypeClientEvent";
            public const string ContentTypeFieldDefinition = "ContentTypeFields";
            public const string Country = "Country";
            public const string DeletedOrphanFile = "DeletedOrphanFile";
            public const string ReviewedFile = "ReviewedFile";
            public const string DomainBlacklist = "DomainBlacklist";
            public const string DownloadableAsset = "DownloadableAsset";
            public const string DownloadableAssetAttachment = "DownloadableAssetAttachment";
            public const string DownloadableAssetBrowseAllMaterialTag = "DownloadableAssetBrowseAllMaterialTag";
            public const string DownloadableAssetEstimatedDistribution = "DownloadableAssetEstimatedDistribution";
            public const string DownloadableAssetRelatedProgram = "DownloadableAssetRelatedProgram";
            public const string DownloadCalendarKey = "DownloadCalendarKey";
            public const string FeatureLocation = "FeatureLocation";
            public const string FeatureLocationScope = "FeatureLocationScope";
            public const string FieldDefinition = "FieldDefinition";
            public const string FieldGroup = "FieldGroup";
            public const string FieldType = "FieldType";
            public const string GdpCorrelation = "GdpCorrelation";
            public const string GdpRequest = "GdpRequest";
            public const string GdpResponse = "GdpResponse";
            public const string GdprFollowUpStatus = "GdprFollowUpStatus";
            public const string Group = "Group";
            public const string HtmlDownload = "HtmlDownload";
            public const string ImportedIca = "ImportedIca";
            public const string ImportedOffer = "ImportedOffer";
            public const string Issuer = "Issuer";
            public const string IssuerProcessor = "IssuerProcessor";
            public const string IssuerSegmentation = "IssuerSegmentation";
            public const string IssuerUnmatched = "IssuerUnmatched";
            public const string Language = "Language";
            public const string List = "List";
            public const string ListClientEvent = "ListClientEvent";
            public const string ListFieldDefinition = "ListFields";
            public const string LoginToken = "LoginToken";
            public const string LoginTracking = "LoginTracking";
            public const string MailDispatcher = "MailDispatcher";
            public const string MailSent = "MailSent";
            public const string MarqueeSlide = "MarqueeSlide";
            public const string MasterICA = "MasterICA";
            public const string MetricHeatmap = "MetricHeatmap";
            public const string MigratedContentId = "MigratedContentId";
            public const string MostPopularActivity = "MostPopularActivity";
            public const string MostPopularProcess = "MostPopularProcess";
            public const string Offer = "Offer";
            public const string OfferAttachment = "OfferAttachment";
            public const string OfferSecondaryImage = "OfferSecondaryImage";
            public const string OfferType = "OfferType";
            public const string OrderableAsset = "OrderableAsset";
            public const string OrderableAssetAttachment = "OrderableAssetAttachment";
            public const string OrderableAssetBrowseAllMaterialTag = "OrderableAssetBrowseAllMaterialTag";
            public const string OrderableAssetRelatedProgram = "OrderableAssetRelatedProgram";
            public const string Page = "Page";
            public const string PendingRegistration = "PendingRegistrations";
            public const string Permission = "Permission";
            public const string PricelessOffersProcess = "PricelessOffersProcess";
            public const string FileReviewProcess = "FileReviewProcess";
            public const string PricingSchedule = "PricingSchedule";
            public const string Processor = "Processor";
            public const string Program = "Program";
            public const string ProgramAttachment = "ProgramAttachment";
            public const string ProgramBrowseAllMaterialTag = "ProgramBrowseAllMaterialTag";
            public const string QuickLink = "QuickLink";
            public const string Recommendation = "Recommendation";
            public const string Region = "Region";
            public const string Role = "Role";
            public const string RolePermissionRegion = "RolePermissionRegion";
            public const string RoleRegion = "RoleRegion";
            public const string SalesforceAccessToken = "SalesforceAccessToken";
            public const string SalesforceActivityReport = "SalesforceActivityReport";
            public const string SalesforceActivityType = "SalesforceActivityType";
            public const string SalesforceTask = "SalesforceTask";
            public const string SalesforceTaskStatus = "SalesforceTaskStatus";
            public const string SearchActivity = "SearchActivity";
            public const string SearchTerm = "SearchTerm";
            public const string SearchTermSegmentation = "SearchTermSegmentation";
            public const string Segmentation = "Segmentation";
            public const string ShareKey = "ShareKey";
            public const string SiteTracking = "SiteTracking";
            public const string SiteVisits = "SiteVisits";
            public const string Snippet = "Snippet";
            public const string Tag = "Tag";
            public const string TagCategory = "TagCategory";
            public const string TagCategoryUserSubscription = "TagCategoryUserSubscription";
            public const string TagHierarchyPosition = "TagHierarchyPosition";
            public const string TagHit = "TagHit";
            public const string TagScope = "TagScope";
            public const string TagTranslatedContent = "TagTranslatedContent";
            public const string TagUserSubscription = "TagUserSubscription";
            public const string UpcomingEvent = "UpcomingEvent";
            public const string User = "User";
            public const string UserConsent = "UserConsent";
            public const string UserFeatureLocation = "UserFeatureLocation";
            public const string UserGroup = "UserGroup";
            public const string UserKey = "UserKey";
            public const string UserProfile = "UserProfile";
            public const string UserRoleRegion = "UserRoleRegion";
            public const string UserSubscription = "UserSubscription";
            public const string UserSubscriptionFrequency = "UserSubscriptionFrequency";
            public const string UserToFeaturableTag = "UserToFeaturableTag";
            public const string VanityUrl = "VanityUrl";
            public const string ViewAllFiles = "vw_AllFiles";
            public const string ViewCopyFiles = "vw_CopyFiles";
            public const string ViewSiteTrackingByContext = "vSiteTrackingByContext";
            public const string Webinar = "Webinar";
            public const string WebRoleAccess = "WebRoleAccess";
            public const string YoutubeVideo = "YoutubeVideo";
            public const string ContentItemEventLocation = "ContentItemEventLocation";
            public const string EventLocation = "EventLocation";
            public const string EventLocationPositionHierarchy = "EventLocationPositionHierarchy";
            public const string UploadActivity = "UploadActivity";
        }

        public static class TagCategories
        {
            public const string BusinessObjectives = "5";
            public const string CardTypes = "7";
            public const string CommunicationChannels = "8";
            public const string ContentTypes = "9";
            public const string Insights = "1";
            public const string Languages = "ca8b";
            public const string Marketing = "3";
            public const string Markets = "ca8a";
            public const string Products = "2";
            public const string SegmentsAudiences = "6";
            public const string Solutions = "4";
        }

        public static class Tags
        {
            public const string AccountLevelManagement = "account-level-management";
            public const string Acquisition = "acquisition";
            public const string Activation = "activation";
            public const string BestPractices = "best-practices";
            public const string BillPayment = "bill-payment";
            public const string Charge = "charge";
            public const string Consumer = "consumer";
            public const string ConsumerInsights = "consumer-insights";
            public const string Conversion = "conversion";
            public const string Corporate = "corporate";
            public const string CorporatePayments = "corporate-payments";
            public const string Credit = "credit";
            public const string DataAnalysis = "data-analysis";
            public const string Debit = "debit";
            public const string Enhancements = "enhancements";
            public const string FinancialLiteracy = "financial-literacy";
            public const string FraudPrevention = "fraud-prevention";
            public const string GeneralTrends = "general-trends";
            public const string GlobalCardholderServices = "global-cardholder-services";
            public const string ImpactOfRegulation = "impact-of-regulation";
            public const string IndustryTrends = "industry-trends";
            public const string MacroTrends = "macro-trends";
            public const string MarketInsights = "market-insights";
            public const string MerchantOffers = "merchant-offers";
            public const string PortfolioInsights = "portfolio-insights";
            public const string PortfolioPerformance = "portfolio-performance";
            public const string Prepaid = "prepaid";
            public const string PromotionsSweepstakes = "promotions-and-sweepstakes";
            public const string PublicSector = "public-sector";
            public const string Retention = "retention";
            public const string Rewards = "rewards";
            public const string SafetySecurity = "safety-security";
            public const string SegmentTrends = "segment-trends";
            public const string SmallBusiness = "small-business";
            public const string Upgrade = "upgrade";
            public const string Usage = "usage";
        }

        public static class MailDispatcherStatus
        {
            public const int New = 1;
            public const int Sent = 2;
            public const int Failed = 3;
            public const int Error = 4;
            public const int Opened = 5;
            public const int Creating = 6;
        }

        public static class UserKeyStatus
        {
            public const int Available = 1;
            public const int Expired = 2;
            public const int Used = 3;
        }

        public static class UserSubscriptionFrequency
        {
            public const int Undefined = 0;
            public const int Never = 1;
            public const int Daily = 2;
            public const int Weekly = 3;

            public static int Default { get { return Weekly; } }
        }

        public static class ContentStatus
        {
            public const string Published = "Published";
            public const int PublishedId = 3;
        }

        public static class ContentItemRegionVisibilityValues
        {
            public const int Pending = 2;
        }

        public static class ExpirationReviewStates
        {
            /// <summary>
            /// <summary>
            /// The Business Owner created a new draft that has not yet been reviewed for a published asset
            /// </summary>
            public const int AssetModified = 5;
            /// <summary>
            /// The Business Owner created a new asset that has not yet been reviewed
            /// </summary>
            public const int NewAsset = 4;
            /// <summary>
            /// The Business Owner Said that this is not a content of his own
            /// </summary>
            public const int NotABusinessOwner = 3;
            /// <summary>
            /// The Business Owner Reviewed the asset and deemed it was not OK to Extend
            /// </summary>
            public const int DoNotExtend = 2;
            /// <summary>
            /// The Business Owner Reviewed the asset and deemed OK to Extend
            /// </summary>
            public const int Extend = 1;
            /// <summary>
            /// This state Implies that the ConentItem Has not been Reviewd or it is not close to beeing expired therefore is not needed and can't be reviewed by Business Owner to review.
            /// </summary>
            public const int NotAvailable = 0;
            /// <summary>
            /// This is a fake state used to show all previous statuses.
            /// </summary>
            public const int AllAssetsWithAStateAvailable = -1;
        }

        public static class EventLocationManager
        {
            public const string RootNodeTitle = "Global";
        }

        public static class Reports
        {
            public const string UntrackedLabel = "Untracked";
        }

        public static class UploadActivity
        {
            public static class Action
            {
                public const int Upload = 1;
                public const int Delete = 2;
            }

            public static class Status
            {
                public const int Pending = 1;
                public const int Error = 2;
                public const int Saved = 3;
            }
        }

        public static class Salesforce
        {
            public static class ActivityType
            {
                public const int User = 1;
                public const int Download = 2;
                public const int Order = 3;
            }

            public static class TaskStatus
            {
                public const int Pending = 1;
                public const int Ignored = 2;
                public const int Failed = 3;
                public const int Completed = 4;
            }
        }

        public static class OrderStatus
        {
            public const string Fulfillment = "Fulfillment";
            public const string PendingApproval = "Pending Approval";
            public const string PendingMasterCardApproval = "Pending MasterCard Approval";
            public const string PendingCustomization = "Pending Customization";
            public const string Cancelled = "Cancelled";
            public const string OrderCreated = "Order Created";
            public const string PendingCorrection = "Pending Correction";
            public const string Shipped = "Shipped";
        }
    }
}