﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public interface IFileRepository
    {
        void Commit();
        IEnumerable<ViewAllFiles> GetViewAllFiles();
        IEnumerable<ViewCopyFiles> GetViewCopyFiles();
        IQueryable<ReviewedFile> GetReviewedFiles();
        void InsertDeletedOrphanFile(DeletedOrphanFile deletedOrphanFile);
        void InsertFileReviewProcess(FileReviewProcess fileReviewProcess);
        void InsertReviewedFile(ReviewedFile reviewedFile);
        void CleanUpReviewedFiles();
    }
}