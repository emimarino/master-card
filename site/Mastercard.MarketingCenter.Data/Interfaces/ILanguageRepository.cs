﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public interface ILanguageRepository
    {
        IQueryable<Language> GetAll();
        Language GetByTagIdentifier(string tagIdentifier);
    }
}