﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Data
{
    public interface IGdpRequestRepository
    {
        IEnumerable<GdpRequest> GetById(int RequestId);
        void Insert(GdpRequest request);
        GdpCorrelation GetNewCorrelation();
        void UpdateGdpCorrelation(GdpCorrelation correlation);
        IEnumerable<GdpRequest> GetPending();
        void CreateResponse(GdpResponse response);
    }
}