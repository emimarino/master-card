﻿namespace Mastercard.MarketingCenter.Data
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}