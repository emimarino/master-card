﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Data.Interfaces.GDP
{
    public interface IConsentManagementRepository
    {
        void RetrieveExternalFiles();
        ConsentFileData GetLatestConsentLanguageText(string useCode, string locale, string functionCode);
        ConsentFileData GetLatestLegalConsentData(string useCode, string locale, string functionCode = null);
    }
}