﻿using Mastercard.MarketingCenter.DTO.GDP;

namespace Mastercard.MarketingCenter.Data.Interfaces.GDP
{
    public interface IGdpExternalRepository
    {
        bool RetrieveRequests();
        int SubmitReply(GdpResponseDTO serviceResponse);
    }
}