﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Domo.Domain;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Data
{
    public interface ISearchRepository
    {
        void AddNewSearchActivity(string userName, DateTime searchDateTime, string searchBoxEntry, string urlVisited, string Region, int? searchResultsQty);
        IEnumerable<DomoSearchActivity> GetActivity(DateTime from, DateTime to, int? SearchActivityID = null, string UserFullName = null, string UserEmail = null, string UserTitle = null, string UserRegion = null, string UserCountry = null, string SearchDateTime = null, string SearchTerm = null, string UrlVisited = null, string RegionalSite = null, bool includeExcludedResults = false);
        List<FeaturedSearchResult> GetFeaturedSearchResults(string searchQuery, IEnumerable<string> currentSegmentationIds, string regionId);
        List<FeaturedSearchResult> GetFeaturedSearchResults(string searchQuery, IEnumerable<string> currentSegmentationIds, string specialAudienceAccess, string regionId);
        IDictionary<string, List<SearchAutoCompleteItem>> GetSearchAutoCompleteItems();
        List<SearchResult> GetSearchResults(string searchQuery, IEnumerable<string> currentSegmentationIds, string regionId);
        List<SearchResult> GetSearchResults(string searchQuery, IEnumerable<string> currentSegmentationIds, string specialAudienceAccess, string regionId);
    }
}