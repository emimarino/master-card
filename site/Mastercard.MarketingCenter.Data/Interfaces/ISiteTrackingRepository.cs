﻿using System;

namespace Mastercard.MarketingCenter.Data
{
    public interface ISiteTrackingRepository
    {
        void Commit();
        TItem Insert<TItem>(TItem item) where TItem : class, new();
        void ProcessContentItemVisits();
        void ArchiveSiteTracking();
        void DeleteArchivedSiteTracking();
        void UpdateSiteTrackingContext(DateTime startDate, bool resetContext = false);
    }
}