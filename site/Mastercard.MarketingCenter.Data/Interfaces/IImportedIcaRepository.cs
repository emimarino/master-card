﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public interface IImportedIcaRepository
    {
        IQueryable<ImportedIca> GetAll();
        IQueryable<ImportedIca> GetAllActive();
        ImportedIca GetById(int id);
        ImportedIca GetByCid(string cid);
    }
}