﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public interface IUserRepository
    {
        void Commit();
        TItem Insert<TItem>(TItem item) where TItem : class, new();
        TItem Update<TItem>(TItem item) where TItem : class, new();
        IQueryable<User> GetAll();
        User GetUser(int userId);
        User GetUser(string userName);
        User GetByEmail(string email);
        IQueryable<string> GetExpiredUserEmails(int expirationWindowInMonths);
        IQueryable<UserMembership> GetExpiringFollowUpUsers(string regionId, int gdprFollowUpStatusId, int expirationWindowInMonths);
        IQueryable<UserData> GetActiveUsersByIssuerId(string issuerId);
        Aspnet_Membership GetMembershipUserByUserName(string userName);
        Aspnet_Membership GetMembershipUserByEmail(string email);
        IQueryable<UserRegistration> GetUserRegistrations(string individualFirstName = null, string individualLastName = null, string individualEmail = null, string localIssuerName = null, string NoteComment = null, string CID = null, string importedIssuerName = null, string regionId = null, DateTime? lastLoginStart = null, DateTime? lastLoginEnd = null, DateTime? registeredDateStart = null, DateTime? registeredDateEnd = null, bool includeLeftOutResults = false);
        void SetFeedbackRequestDisplayed(string userName);
        void SetContentPreferencesMessageDisplayed(string userName, bool displayContentPreferencesMessage);
    }
}