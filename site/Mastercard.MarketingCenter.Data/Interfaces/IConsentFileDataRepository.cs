﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public interface IConsentFileDataRepository
    {
        void InsertConsentFileData(ConsentFileData consentFile);
        void InsertUserConsent(UserConsent userConsent);
        void UpdateConsentFileData(ConsentFileData consentFile);
        IQueryable<ConsentFileData> GetByIds(IEnumerable<int> ids);
        ConsentFileData GetLatestConsentLanguageText(string locale = null, string functionCode = null);
        ConsentFileData GetLatestLegalConsentData(string locale = null, string useCode = null, string functionCode = null);
        IQueryable<ConsentFileData> GetLatestConsents(string locale = null, string functionCode = null, string useCode = null);
        void Insert(IList<ConsentFileData> consentFile);
    }
}