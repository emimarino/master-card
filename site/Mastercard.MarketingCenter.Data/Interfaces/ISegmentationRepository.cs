﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public interface ISegmentationRepository
    {
        TItem Insert<TItem>(TItem item) where TItem : class, new();
        IQueryable<Segmentation> GetAll();
        IQueryable<Segmentation> GetByRegion(string regionId);
        IQueryable<Segmentation> GetByUserName(string userName);
        IQueryable<Segmentation> GetByEmail(string email);
    }
}