﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public interface ICampaignCategoryRepository
    {
        IQueryable<CampaignCategory> GetAll();
        void DeleteCampaignCategory(CampaignCategory campaignCategory);
        CampaignCategory GetCampaignCategoryById(string campaignCategoryId);
        IQueryable<CampaignCategory> GetCampaignCategoriesByRegionAndLanguage(string regionId, string languageCode);
    }
}