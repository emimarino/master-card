﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;

namespace Mastercard.MarketingCenter.Data
{
    public interface IRepository<TContext> : IDisposable where TContext : DbContext, IObjectContextAdapter, new()
    {
        IQueryable<TItem> Select<TItem>() where TItem : class, new();
        IQueryable<TItem> Select<TItem>(Expression<Func<TItem, bool>> whereClause) where TItem : class, new();
        IOrderedQueryable<TItem> Select<TItem>(Expression<Func<TItem, bool>> whereClause, Expression<Func<TItem, object>> orderBy) where TItem : class, new();
        TItem Insert<TItem>(TItem item) where TItem : class, new();
        TItem Update<TItem>(TItem item) where TItem : class, new();
        void Delete<TItem>(TItem item) where TItem : class, new();
        void Commit();
    }
}