﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public interface IClonesRepository
    {
        void PopulateCloneSourceNotification();
        IQueryable<CloneSourceNotification> GetCloneSourcesNotifications();
        IQueryable<IGrouping<string, CloneSourceNotification>> GetActiveCloneSourcesNotificationGroupedByRegion();
        void UpdateSate(IEnumerable<CloneSourceNotification> elements);
    }
}