﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.DTO.Salesforce;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Interfaces
{
    public interface ISalesforceRepository
    {
        void Commit();
        IEnumerable<SalesforceDownloadActivityDTO> GetDownloadActivities(DateTime startDateTime, DateTime endDateTime);
        IEnumerable<SalesforceOrderActivityDTO> GetOrderActivities(DateTime startDateTime, DateTime endDateTime);
        IEnumerable<SalesforceUserActivityDTO> GetUserActivities(DateTime startDateTime, DateTime endDateTime);
        int CreateSalesforceActivityReport();
        SalesforceActivityReport GetSalesforceActivityReportById(int salesforceActivityReportId);
        int SaveSalesforceToken(SalesforceAccessToken salesforceAccessToken);
        void UpdateSalesforceActivityReportById(int salesforceActivityReportId, DateTime? endDate = null, int? sent = null, int? failed = null, bool isSuccessed = false, int? salesforceTokenId = null, string loggingEventId = null);
        void AddSalesforceTasks(IEnumerable<SalesforceTask> salesforceTasks);
        IQueryable<SalesforceTask> GetSalesforceTasks();
        IQueryable<SalesforceTask> GetSalesforceTasks(DateTime from, DateTime to);
    }
}