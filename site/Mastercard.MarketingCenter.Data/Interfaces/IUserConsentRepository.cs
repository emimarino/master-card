﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public interface IUserConsentRepository
    {
        IEnumerable<ConsentFileData> GetExpiredConsentForUser(string userName, string locale);
        void Insert(UserConsent userConsent);
        void Insert(IEnumerable<UserConsent> userConsents);
        IQueryable<UserConsent> GetByEmail(string email);
        int GetUserIdByEmail(string email);
        IEnumerable<ConsentFileData> GetLatestConsents(string locale, string functionCode);
    }
}