﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public interface IPendingRegistrationRepository
    {
        void Commit();
        TItem Insert<TItem>(TItem item) where TItem : class, new();
        TItem Update<TItem>(TItem item) where TItem : class, new();
        PendingRegistration GetByGuid(string guid);
        IQueryable<PendingRegistration> GetByRegion(string regionId = null);
        IQueryable<PendingRegistration> GetByEmail(string email, string regionId = null);
        IQueryable<PendingRegistration> GetByStatus(string status, string regionId = null);
        IQueryable<PendingRegistration> GetExpiredPendingRegistrations(DateTime expirationDate);
    }
}