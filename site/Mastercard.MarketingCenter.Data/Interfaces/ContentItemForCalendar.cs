﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public abstract class ContentItemForCalendar
    {
        [Key]
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public DateTime? InMarketStartDate { get; set; }
        public DateTime? InMarketEndDate { get; set; }
        public bool ShowInMarketingCalendar { get; set; }
        public int MarketingCalendarPriority { get; set; }

        [ForeignKey("ContentItemId")]
        public ContentItemBase ContentItem { get; set; }
        public virtual ICollection<ContentItemCampaignEvent> ContentItemCampaignEvents { get; set; }

        [NotMapped]
        public IEnumerable<Tag> Markets
        {
            get
            {
                return ContentItem.Tags?.Where(t => t.TagHierarchyPositions.Any(thp => thp.TagCategoryId.Equals(MarketingCenterDbConstants.TagCategories.Markets, StringComparison.OrdinalIgnoreCase)))
                                        .Select(t => t);
            }
        }
    }
}