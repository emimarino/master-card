﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public interface IAuthorizationRepository
    {
        void Commit();
        void InsertUserRoleRegion(UserRoleRegion userRoleRegion);
        void DeleteUserRoleRegion(UserRoleRegion userRoleRegion);
        Role GetRoleByName(string name);
        IQueryable<UserRoleRegion> GetUserRoles(int userId);
        IQueryable<RoleRegion> GetRolesByRegion(string regionId);
        IQueryable<RoleRegion> GetRegionsByRole(int roleId);
        IQueryable<Permission> GetUserPermissionsByRegion(int userId, string regionId);
        IQueryable<Permission> GetRolePermissionsByRegion(int roleId, string regionId);
        IQueryable<UserRoleRegion> GetUsersInRole(string name);
    }
}