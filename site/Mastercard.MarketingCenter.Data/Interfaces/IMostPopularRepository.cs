﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public interface IMostPopularRepository
    {
        IQueryable<MostPopularActivity> GetAllMostPopularActivities();
        IQueryable<MostPopularActivity> GetMostPopularActivitiesByRegion(string regionId);
        IQueryable<MostPopularActivity> GetMostPopularActivitiesByProcessId(long mostPopularProcessId);
        IQueryable<MostPopularProcess> GetAllMostPopularProcesses();
        MostPopularProcess Insert(MostPopularProcess mostPopularProcess);
        void DeleteMostPopularActivity(MostPopularActivity mostPopularActivity);
        void DeleteMostPopularProcess(MostPopularProcess mostPopularProcess);
        void ProcessMostPopularActivity(DateTime startDate, DateTime endDate, string contentTypeIds, long mostPopularProcessId);
    }
}