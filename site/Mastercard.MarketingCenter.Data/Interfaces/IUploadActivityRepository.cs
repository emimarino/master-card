﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Data.Interfaces
{
    public interface IUploadActivityRepository
    {
        void Commit();
        int AddUploadActivity(UploadActivity uploadActivity);
        UploadActivity GetUploadActivityById(int uploadActivityId);
    }
}