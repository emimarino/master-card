﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public interface IOrderRepository
    {
        IQueryable<OrderDownload> GetOrderDownloads(string individualFirstName = null, string individualLastName = null, string individualEmail = null, string localIssuerName = null, string noteComment = null, string CID = null, string importedIssuerName = null, string regionId = null, string relatedProgram = null, string itemName = null, string fulfilmentType = null, int? itemQuantityMin = null, int? itemQuantityMax = null, int? estimatedDistributionMin = null, int? estimatedDistributionMax = null, bool includeLeftOutResults = false, DateTime? startTime = null, DateTime? endTime = null);
    }
}