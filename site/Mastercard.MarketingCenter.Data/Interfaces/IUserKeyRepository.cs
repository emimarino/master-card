﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Data
{
    public interface IUserKeyRepository
    {
        UserKey CreateUserKey(int userId);
        UserKey GetUserKeyByKey(string key);
        UserKey GetUserKeyByUserName(string userName);
        UserKey GetUserKeyByUserNameAndStatus(string userName, int statusID);
        void ChangeUserKeyStatus(string key, int userKeyStatusId);
        void ChangeUserKeyStatusForRelatedKeys(string key, int userKeyStatusId);
    }
}