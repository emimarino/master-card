﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public interface IOfferRepository
    {
        IQueryable<Offer> GetAll();
        IQueryable<CardExclusivity> GetCardExclusivities();
        IQueryable<Category> GetCategories();
        IQueryable<Tag> GetMarketApplicabilities(string regionId);
        IQueryable<Offer> GetByRegion(string regionId);
        Offer GetById(string contentItemId);
        void InsertImportedOffers(IEnumerable<ImportedOffer> importedOffers);
        void ProcessPricelessOffers(string businessOwnerEmail);
        void ArchiveImportedOffers();
        void DeleteArchivedImportedOffers();
        IQueryable<PricelessOffersProcess> GetPricelessOffersProcesses();
    }
}