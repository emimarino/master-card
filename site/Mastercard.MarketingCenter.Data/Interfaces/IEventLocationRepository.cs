﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data.Interfaces
{
    public interface IEventLocationRepository
    {
        IQueryable<EventLocation> GetEventLocations();
        EventLocation AddEventLocation(EventLocation eventLocation);
        int? GetEventLocationHierarchyPositionId(int parentEventLocationId);
        EventLocationPositionHierarchy AddEventLocationToHierarchy(EventLocationPositionHierarchy eventLocationHierarchy);
        void DeleteEventLocationById(int id);
        EventLocationPositionHierarchy GetHierarchyByEventLocationId(int id);
        EventLocation GetEventLocationById(int id);
        EventLocationPositionHierarchy UpdateEventLocationHierarchy(EventLocationPositionHierarchy eventLocationHierarchy);
        IQueryable<EventLocation> GetRelatedEventLocationsByContentItemId(string contentItemId);
    }
}