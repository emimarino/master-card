﻿namespace Mastercard.MarketingCenter.Data
{
    public interface IContentItemWithTitle
    {
        string Title { get; set; }
    }
}