﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public interface ICampaignEventRepository
    {
        IQueryable<CampaignEvent> GetCampaignEvents();
        IQueryable<ContentItemCampaignEvent> GetContentItemCampaignEvents();
        IQueryable<ContentItemCampaignEvent> FilterMarketingCalendarEnabled(IQueryable<ContentItemCampaignEvent> events, bool isEnabled = true);
    }
}