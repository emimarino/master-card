﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public interface ICountryRepository
    {
        IQueryable<Country> GetAll();
        Country GetByTagIdentifier(string tagIdentifier);
    }
}