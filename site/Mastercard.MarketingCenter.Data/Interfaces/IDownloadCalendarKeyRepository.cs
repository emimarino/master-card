﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Data
{
    public interface IDownloadCalendarKeyRepository
    {
        DownloadCalendarKey GetDownloadCalendarKeyByKey(string key);
        DownloadCalendarKey InsertDownloadCalendarKey(DownloadCalendarKey downloadCalendarKey);
    }
}