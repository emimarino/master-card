﻿using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Data
{
    public interface IIssuerRepository
    {
        TItem Insert<TItem>(TItem item) where TItem : class, new();
        IQueryable<Issuer> GetAll();
        IQueryable<Issuer> GetAllByRegionId(string regionId);
        Issuer GetIssuerByDomain(string domain);
        Issuer GetIssuerById(string id);
        Issuer GetIssuerByCid(string cid);
        IQueryable<IssuerSegmentation> GetAllIssuerSegmentations();
        IQueryable<Segmentation> GetAllSegmentations();
        IEnumerable<RetrieveIssuersByProcessorDTO> GetIssuersByProcessor(string processorId, string regionId);
        IQueryable<DomainBlacklist> GetDomainBlackLists();
        void DeleteIssuer(Issuer issuer);
        IQueryable<MasterICA> GetAllMasterICAs();
        MasterICA GetMasterICAByIca(int ica);
        IQueryable<MasterICA> GetMasterICAByMasterIca(int masterIca);
        void DeleteMasterICA(MasterICA masterIca);
    }
}