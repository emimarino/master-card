﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Data
{
    public interface IContentReviewRepository
    {
        IEnumerable<ExpirationReviewState> GetExpirationReviewStates();
    }
}