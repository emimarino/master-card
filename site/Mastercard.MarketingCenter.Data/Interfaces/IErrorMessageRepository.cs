﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Data
{
   public interface IErrorMessageRepository
    {
        ErrorMessage GetById(int listId, string eventName);
    }
}
