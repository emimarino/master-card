﻿CREATE TABLE [dbo].[UserConsent]
(
	UserConsentId INT NOT NULL PRIMARY KEY Identity(1,1),
	 UserId int NOT NULL,
         ConsentType  NVARCHAR(50) NOT NULL,
         ConsentDate DateTime NOT NULL,
         VersionID INT NOT NULL,
		  LocaleCode NVARCHAR(50) NOT NULL,
       ConsentStatus NVARCHAR(50) NOT NULL
        
)
go


CREATE TABLE [dbo].[GdpRequest]
(
	GdpRequestId INT NOT NULL PRIMARY KEY Identity(1,1),
	RequestId INT NOT NULL ,	
	ResponseType NVARCHAR(50),
	RequestType NVARCHAR(50),
	RequestContext NVARCHAR(50),
    RequestDate    DateTime   ,
    SearchKey NVARCHAR(50)
)
go


ALTER TABLE [user] Add  LitigationHold  bit default 0 not null

go