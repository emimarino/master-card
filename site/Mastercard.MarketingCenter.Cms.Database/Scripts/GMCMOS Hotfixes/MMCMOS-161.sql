PRINT N'Altering [dbo].[[CmsSearch]]...';
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[CmsSearch]
(
	@SearchTerm		NVARCHAR(200),
	@RegionId		nchar(6)
)
AS
WITH CTE_SEARCH
AS
(
SELECT
	PR.Title,
	CI.PrimaryContentItemID as Id,
	CI.CreatedDate,
	CI.ModifiedDate,
	'Program' as ItemType,
	null as ListId,
	CI.ContentTypeID as ContentTypeId,
	CI.FrontEndUrl		
FROM
	dbo.ContentItem CI
	JOIN dbo.Program PR ON PR.ContentItemId = CI.PrimaryContentItemID
	JOIN FREETEXTTABLE(dbo.Program, (ShortDescription, LongDescription), @SearchTerm) PRO ON PRO.[Key] = PR.ContentItemId
	WHERE CI.RegionId = @RegionID 
	AND CI.ContentItemID in (select ContentItemId
                             from ContentItem
                             EXCEPT
                             select PrimaryContentItemID
                             from ContentItem
                             where ContentItemId like '%-p')
	AND ci.ContentTypeID in (select Id from CmsRoleAccess where RequiredRolePermissionName = @RegionID)
	AND ci.StatusID <> 8

UNION

SELECT
	PA.Title,
	CI.PrimaryContentItemID as Id,
	CI.CreatedDate,
	CI.ModifiedDate,
	'Page' as ItemType,
	null as ListId,
	CI.ContentTypeID as ContentTypeId,
	CI.FrontEndUrl
FROM
		 dbo.ContentItem CI
	JOIN dbo.Page PA ON PA.ContentItemId = CI.PrimaryContentItemID	
	JOIN FREETEXTTABLE(dbo.Page, (Title,Uri, IntroCopy, Terms, ShortDescription), @SearchTerm) LPO ON LPO.[Key] = PA.ContentItemId
	WHERE CI.RegionId = @RegionID 
	AND CI.ContentItemID in (select ContentItemId
                             from ContentItem
                             EXCEPT
                             select PrimaryContentItemID
                             from ContentItem
                             where ContentItemId like '%-p')
	AND ci.ContentTypeID in (select Id from CmsRoleAccess where RequiredRolePermissionName = @RegionID)
	AND ci.StatusID <> 8

UNION
SELECT
	AT.Title,
	CI.PrimaryContentItemID as Id,
	CI.CreatedDate,
	CI.ModifiedDate,
	'Asset' as ItemType,
	null as ListId,
	CI.ContentTypeID as ContentTypeId,
	CI.FrontEndUrl
FROM
		 dbo.ContentItem CI
	JOIN dbo.Asset AT ON AT.ContentItemId = CI.PrimaryContentItemID
	JOIN FREETEXTTABLE(dbo.Asset, (Title,ShortDescription, LongDescription, BrowseAllMaterialsDisplayText), @SearchTerm) ATO ON ATO.[Key] = AT.ContentItemId
	WHERE CI.RegionId = @RegionID 
	AND CI.ContentItemID in (select ContentItemId
                             from ContentItem
                             EXCEPT
                             select PrimaryContentItemID
                             from ContentItem
                             where ContentItemId like '%-p')
	AND ci.ContentTypeID in (select Id from CmsRoleAccess where RequiredRolePermissionName = @RegionID)
	AND ci.StatusID <> 8

UNION
SELECT
	AW.Title,
	CI.PrimaryContentItemID as Id,
	CI.CreatedDate,
	CI.ModifiedDate,
	'Asset Full Width' as ItemType,
	null as ListId,
	CI.ContentTypeID as ContentTypeId,
	CI.FrontEndUrl
FROM
		 dbo.ContentItem CI
	JOIN dbo.AssetFullWidth AW ON AW.ContentItemId = CI.PrimaryContentItemID	
	JOIN FREETEXTTABLE(dbo.AssetFullWidth, (Title,ShortDescription, LongDescription), @SearchTerm) AWO ON AWO.[Key] = AW.ContentItemId
	WHERE CI.RegionId = @RegionID 
	AND CI.ContentItemID in (select ContentItemId
                             from ContentItem
                             EXCEPT
                             select PrimaryContentItemID
                             from ContentItem
                             where ContentItemId like '%-p')
	AND ci.ContentTypeID in (select Id from CmsRoleAccess where RequiredRolePermissionName = @RegionID)
	AND ci.StatusID <> 8

UNION
SELECT
	DA.Title,
	CI.PrimaryContentItemID as Id,
	CI.CreatedDate,
	CI.ModifiedDate,
	'Downloadable Asset' as ItemType,
	null as ListId,
	CI.ContentTypeID as ContentTypeId,
	CI.FrontEndUrl
FROM
		 dbo.ContentItem CI
	JOIN dbo.DownloadableAsset DA ON DA.ContentItemId = CI.PrimaryContentItemID
	JOIN FREETEXTTABLE(dbo.DownloadableAsset, (Title,ShortDescription, LongDescription), @SearchTerm) DAO ON DAO.[Key] = DA.ContentItemId
	WHERE CI.RegionId = @RegionID 
	AND CI.ContentItemID in (select ContentItemId
                             from ContentItem
                             EXCEPT
                             select PrimaryContentItemID
                             from ContentItem
                             where ContentItemId like '%-p')
	AND ci.ContentTypeID in (select Id from CmsRoleAccess where RequiredRolePermissionName = @RegionID)
	AND ci.StatusID <> 8

UNION
SELECT
	OA.Title,
	CI.PrimaryContentItemID as Id,
	CI.CreatedDate,
	CI.ModifiedDate,
	'Orderable Asset' as ItemType,
	null as ListId,
	CI.ContentTypeID as ContentTypeId,
	CI.FrontEndUrl
FROM
		 dbo.ContentItem CI
	JOIN dbo.OrderableAsset OA ON OA.ContentItemId = CI.PrimaryContentItemID	
	JOIN FREETEXTTABLE(dbo.OrderableAsset, (Title,ShortDescription, LongDescription, Sku), @SearchTerm) OAO ON OAO.[Key] = OA.ContentItemId
	WHERE CI.RegionId = @RegionID 
	AND CI.ContentItemID in (select ContentItemId
                             from ContentItem
                             EXCEPT
                             select PrimaryContentItemID
                             from ContentItem
                             where ContentItemId like '%-p')
	AND ci.ContentTypeID in (select Id from CmsRoleAccess where RequiredRolePermissionName = @RegionID)
	AND ci.StatusID <> 8

UNION
SELECT
	WE.Title,
	CI.PrimaryContentItemID as Id,
	CI.CreatedDate,
	CI.ModifiedDate,
	'Webinar' as ItemType,
	null as ListId,
	CI.ContentTypeID as ContentTypeId,
	CI.FrontEndUrl
FROM dbo.ContentItem CI
	JOIN dbo.Webinar WE ON WE.ContentItemId = CI.PrimaryContentItemID	
	JOIN FREETEXTTABLE(dbo.Webinar, (Title, ShortDescription, LongDescription, Link), @SearchTerm) WEO ON WEO.[Key] = WE.ContentItemId
	WHERE CI.RegionId = @RegionID 
	AND CI.ContentItemID in (select ContentItemId
                             from ContentItem
                             EXCEPT
                             select PrimaryContentItemID
                             from ContentItem
                             where ContentItemId like '%-p')
	AND ci.ContentTypeID in (select Id from CmsRoleAccess where RequiredRolePermissionName = @RegionID)
	AND ci.StatusID <> 8

UNION
SELECT
	MS.Title,
	CI.PrimaryContentItemID as Id,
	CI.CreatedDate,
	CI.ModifiedDate,
	'Marquee Slide' as ItemType,
	null as ListId,
	CI.ContentTypeID as ContentTypeId,
	null as FrontEndUrl
FROM dbo.ContentItem CI
	JOIN dbo.MarqueeSlide MS ON MS.ContentItemId = CI.PrimaryContentItemID	
	JOIN FREETEXTTABLE(dbo.MarqueeSlide, (Title, LongDescription), @SearchTerm) MSO ON MSO.[Key] = MS.ContentItemId
	WHERE CI.RegionId = @RegionID 
	AND CI.ContentItemID in (select ContentItemId
                             from ContentItem
                             EXCEPT
                             select PrimaryContentItemID
                             from ContentItem
                             where ContentItemId like '%-p')
	AND ci.ContentTypeID in (select Id from CmsRoleAccess where RequiredRolePermissionName = @RegionID)
	AND ci.StatusID <> 8

UNION
SELECT
	HD.Title,
	CI.PrimaryContentItemID as Id,
	CI.CreatedDate,
	CI.ModifiedDate,
	'Html Download' as ItemType,
	null as ListId,
	CI.ContentTypeID as ContentTypeId,
	null as FrontEndUrl
FROM dbo.ContentItem CI
	JOIN dbo.HtmlDownload HD ON HD.ContentItemId = CI.PrimaryContentItemID	
	JOIN FREETEXTTABLE(dbo.HtmlDownload, (Title, Url, [Filename]), @SearchTerm) HDO ON HDO.[Key] = HD.ContentItemId
	WHERE CI.RegionId = @RegionID 
	AND CI.ContentItemID in (select ContentItemId
                             from ContentItem
                             EXCEPT
                             select PrimaryContentItemID
                             from ContentItem
                             where ContentItemId like '%-p')
	AND ci.ContentTypeID in (select Id from CmsRoleAccess where RequiredRolePermissionName = @RegionID)
	AND ci.StatusID <> 8

UNION
SELECT
	QL.Title,
	CI.PrimaryContentItemID as Id,
	CI.CreatedDate,
	CI.ModifiedDate,
	'Quick Link' as ItemType,
	null as ListId,
	CI.ContentTypeID as ContentTypeId,
	null as FrontEndUrl
FROM dbo.ContentItem CI
	JOIN dbo.QuickLink QL ON QL.ContentItemId = CI.PrimaryContentItemID	
	JOIN FREETEXTTABLE(dbo.QuickLink, (Title, Url, Paragraph), @SearchTerm) QLO ON QLO.[Key] = QL.ContentItemId
	WHERE CI.RegionId = @RegionID 
	AND CI.ContentItemID in (select ContentItemId
                             from ContentItem
                             EXCEPT
                             select PrimaryContentItemID
                             from ContentItem
                             where ContentItemId like '%-p')
	AND ci.ContentTypeID in (select Id from CmsRoleAccess where RequiredRolePermissionName = @RegionID)
	AND ci.StatusID <> 8

UNION
SELECT
	YV.Title,
	CI.PrimaryContentItemID as Id,
	CI.CreatedDate,
	CI.ModifiedDate,
	'Youtube Video' as ItemType,
	null as ListId,
	CI.ContentTypeID as ContentTypeId,
	null as FrontEndUrl
FROM dbo.ContentItem CI
	JOIN dbo.YoutubeVideo YV ON YV.ContentItemId = CI.PrimaryContentItemID	
	JOIN FREETEXTTABLE(dbo.YoutubeVideo, (Title, Url), @SearchTerm) YVO ON YVO.[Key] = YV.ContentItemId
	WHERE CI.RegionId = @RegionID 
	AND CI.ContentItemID in (select ContentItemId
                             from ContentItem
                             EXCEPT
                             select PrimaryContentItemID
                             from ContentItem
                             where ContentItemId like '%-p')
	AND ci.ContentTypeID in (select Id from CmsRoleAccess where RequiredRolePermissionName = @RegionID)
	AND ci.StatusID <> 8

UNION
SELECT
	S.Title,
	CI.PrimaryContentItemID as Id,
	CI.CreatedDate,
	CI.ModifiedDate,
	'Snippet' as ItemType,
	null as ListId,
	CI.ContentTypeID as ContentTypeId,
	null as FrontEndUrl
FROM dbo.ContentItem CI
	JOIN dbo.Snippet S ON S.ContentItemId = CI.PrimaryContentItemID	
	JOIN FREETEXTTABLE(dbo.Snippet, (Title, Html), @SearchTerm) SO ON SO.[Key] = S.ContentItemId
	WHERE CI.RegionId = @RegionID 
	AND CI.ContentItemID in (select ContentItemId
                             from ContentItem
                             EXCEPT
                             select PrimaryContentItemID
                             from ContentItem
                             where ContentItemId like '%-p')
	AND ci.ContentTypeID in (select Id from CmsRoleAccess where RequiredRolePermissionName = @RegionID)
	AND ci.StatusID <> 8

UNION
SELECT
	IC.Name as Title,
	IC.IconID as Id,
	null as CreatedDate,
	null as ModifiedDate,
	'Icon' as ItemType,
	1 as ListId,
	null as ContentTypeId,
	null as FrontEndUrl				
FROM
	Icon IC	
	JOIN FREETEXTTABLE(dbo.Icon, (Name, Extension, Icon), @SearchTerm) ICO ON ICO.[Key] = IC.IconID
	where LTRIM(RTRIM(@RegionId))='global'
UNION
SELECT
	PC.PromotionCode as Title,
	PC.PromoCodeID as Id,
	null as CreatedDate,
	null as ModifiedDate,
	'Promo Code' as ItemType,
	2 as ListId,
	null as ContentTypeId,
	null as FrontEndUrl			
FROM
	PromoCode PC	
	JOIN FREETEXTTABLE(dbo.PromoCode, (PromotionCode, [Description]), @SearchTerm) PCO ON PCO.[Key] = PC.PromoCodeID
	where LTRIM(RTRIM(@RegionId))='us'
UNION
SELECT
	PS.Title as Title,
	PS.PricingScheduleID as Id,
	null as CreatedDate,
	null as ModifiedDate,
	'Pricing Schedule' as ItemType,
	4 as ListId,
	null as ContentTypeId,
	null as FrontEndUrl			
FROM
	PricingSchedule PS	
	JOIN FREETEXTTABLE(dbo.PricingSchedule, (Title, Details), @SearchTerm) PSO ON PSO.[Key] = PS.PricingScheduleID
	where LTRIM(RTRIM(@RegionId))='us'
UNION
SELECT
	CO.FriendlyName as Title,
	CO.CustomizationOptionID as Id,
	null as CreatedDate,
	null as ModifiedDate,
	'Customization Option' as ItemType,
	5 as ListId,
	null as ContentTypeId,
	null as FrontEndUrl			
FROM
	CustomizationOption CO	
	JOIN FREETEXTTABLE(dbo.CustomizationOption, (FriendlyName, Instructions), @SearchTerm) COO ON COO.[Key] = CO.CustomizationOptionID
	where LTRIM(RTRIM(@RegionId))='us'
UNION
	SELECT
	DB.Domain as Title,
	DB.DomainBlacklistID as Id,
	null as CreatedDate,
	null as ModifiedDate,
	'Domain Blacklist' as ItemType,
	6 as ListId,
	null as ContentTypeId,
	null as FrontEndUrl			
FROM
	DomainBlacklist DB
	JOIN FREETEXTTABLE(dbo.DomainBlacklist, (Domain), @SearchTerm) DBO ON DBO.[Key] = DB.DomainBlacklistID
	where LTRIM(RTRIM(@RegionId))='global'
UNION
SELECT
	PO.Title as Title,
	PO.ProcessorId as Id,
	null as CreatedDate,
	null as ModifiedDate,
	'Processor' as ItemType,
	7 as ListId,
	null as ContentTypeId,
	null as FrontEndUrl			
FROM
	Processor PO
	JOIN FREETEXTTABLE(dbo.Processor, (Title), @SearchTerm) POO ON POO.[Key] = PO.ProcessorID
	where LTRIM(RTRIM(@RegionId))='global'
UNION
SELECT
	FL.Title as Title,
	FL.FeatureLocationID Id,
	null as CreatedDate,
	null as ModifiedDate,
	'Feature Location' as ItemType,
	8 as ListId,
	null as ContentTypeId,
	null as FrontEndUrl			
FROM
	FeatureLocation FL
	JOIN FREETEXTTABLE(dbo.FeatureLocation, (Title), @SearchTerm) FLO ON FLO.[Key] = FL.FeatureLocationID
	where LTRIM(RTRIM(@RegionId))='global'
UNION
SELECT
	VU.Url as Title,
	VU.VanityUrlId Id,
	null as CreatedDate,
	null as ModifiedDate,
	'Vanity URL' as ItemType,
	1008 as ListId,
	null as ContentTypeId,
	null as FrontEndUrl			
FROM
	VanityUrl VU
	JOIN FREETEXTTABLE(dbo.VanityUrl, (Url, VanityUrl), @SearchTerm) VUO ON VUO.[Key] = VU.VanityUrlId
UNION
SELECT
	ST.Title as Title,
	ST.SearchTermID Id,
	null as CreatedDate,
	null as ModifiedDate,
	'Search Term' as ItemType,
	2008 as ListId,
	null as ContentTypeId,
	null as FrontEndUrl			
FROM
	SearchTerm ST
	JOIN FREETEXTTABLE(dbo.SearchTerm, (Title, SearchTerms, [Synonyms], FeaturedSearchResultHtml), @SearchTerm) STO ON STO.[Key] = ST.SearchTermID
Where ST.RegionId=@RegionID
UNION
SELECT
	T.Identifier as Title,
	T.TagID Id,
	T.CreatedDate as CreatedDate,
	T.ModifiedDate as ModifiedDate,
	'Tag' as ItemType,
	2009 as ListId,
	null as ContentTypeId,
	null as FrontEndUrl			
FROM
	Tag T
	JOIN FREETEXTTABLE(dbo.Tag, (Identifier, DisplayName, Description), @SearchTerm) TA ON TA.[Key] = T.TagID
	where LTRIM(RTRIM(@RegionId))='global'
UNION
SELECT
	I.Title as Title,
	I.IssuerID Id,
	I.DateCreated as CreatedDate,
	I.ModifiedDate as ModifiedDate,
	'Issuer' as ItemType,
	3 as ListId,
	null as ContentTypeId,
	null as FrontEndUrl			
FROM
	Issuer I
	JOIN FREETEXTTABLE(dbo.Issuer, (Title, DomainName), @SearchTerm) IA ON IA.[Key] = I.IssuerID
	WHERE I.RegionId=@RegionId
)
SELECT Distinct Title,
	Id,
	CreatedDate,
	ModifiedDate,
	ItemType,
	ListId,
	ContentTypeId,
	CASE WHEN SUBSTRING(FrontEndUrl, 1, 1) = '/' THEN '/portal' + FrontEndUrl ELSE '/portal/' + FrontEndUrl END AS FrontEndUrl	
FROM CTE_SEARCH

GO

PRINT N'Update complete.';
GO