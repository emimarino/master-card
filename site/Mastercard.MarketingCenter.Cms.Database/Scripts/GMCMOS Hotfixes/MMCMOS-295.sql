UPDATE [dbo].[FieldDefinition]
 SET [CustomSettings] = '{ "MustBeAfterNow" : "true", "LowerThanFieldName" : "EndDate" }'
 WHERE [Name] = 'StartDate'