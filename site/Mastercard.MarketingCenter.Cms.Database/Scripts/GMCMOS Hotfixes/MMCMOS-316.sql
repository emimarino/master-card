﻿alter table AssetAttachment alter column FileUrl NVARCHAR(2000)
alter table AssetFullWidthAttachment alter column FileUrl NVARCHAR(2000)
alter table DownloadableAssetAttachment alter column FileUrl NVARCHAR(2000)
alter table OrderableAssetAttachment alter column FileUrl NVARCHAR(2000)
alter table ProgramAttachment alter column FileUrl NVARCHAR(2000)


----this script generates a Pwershell script which creates the duplicate files with the corrected names and gets the update scripts for the respective asset table, if the asset is not a downloadable asset this script must be updated, the powershell sript must be run on the files folder


--begin 
-- DECLARE @ContentItemId varchar(MAX)= 'd4b5%'
--declare @filenames varchar(MAX)=''

--select  @filenames+= (', '''+fileurl+'''') from DownloadableAssetAttachment --//replace for assettypename+attachment
--where ContentItemId like @ContentItemId

--select '@(' + substring(@filenames,2,len(@filenames))+')|%{ @{Name=($_ -split ''/'' -split ''\\'')[-1];FullName=$pwd.Path+''\''+$_}}|ForEach-Object{$foldername=($_.FullName  -split ''/'' -split ''\\'')[-2];$escapedname=([uri]::UnEscapeDataString($_.Name)) ;Copy-Item $_.FullName -Destination ($foldername+"\"+$escapedname);$originalname=($_.FullName -split ''/'' -split ''\\'')[-1];Write-Output @{name="calc"; expression="update DownloadableAssetAttachment set FileUrl=''"+$foldername+"/"+$escapedname+"'' where FileUrl=''"+$foldername+"/"+$originalname+"''" } }|ForEach-Object{ $_.expression+";" }| Format-Table -Wrap'  as filenames

--end


--begin 
-- DECLARE @ContentItemId varchar(MAX)= 'd4d9%'
--declare @filenames varchar(MAX)=''

--select  @filenames+= (', '''+fileurl+'''') from DownloadableAssetAttachment --//replace for assettypename+attachment
--where ContentItemId like @ContentItemId

--select '@(' + substring(@filenames,2,len(@filenames))+')|%{ @{Name=($_ -split ''/'' -split ''\\'')[-1];FullName=$pwd.Path+''\''+$_}}|ForEach-Object{$foldername=($_.FullName  -split ''/'' -split ''\\'')[-2];$escapedname=([uri]::UnEscapeDataString($_.Name)) ;Copy-Item $_.FullName -Destination ($foldername+"\"+$escapedname);$originalname=($_.FullName -split ''/'' -split ''\\'')[-1];Write-Output @{name="calc"; expression="update DownloadableAssetAttachment set FileUrl=''"+$foldername+"/"+$escapedname+"'' where FileUrl=''"+$foldername+"/"+$originalname+"''" } }|ForEach-Object{ $_.expression+";" }| Format-Table -Wrap'  as filenames

--end

