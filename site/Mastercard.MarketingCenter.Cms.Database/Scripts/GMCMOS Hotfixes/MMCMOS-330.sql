UPDATE [dbo].[FieldDefinition]
SET [CustomSettings] = '{ "AcceptFiles" : [ ".mov", ".mp4", ".webm" ], "UploadLocationSetting": "VideosFolder" }'
WHERE [Name] = 'VideoFile'