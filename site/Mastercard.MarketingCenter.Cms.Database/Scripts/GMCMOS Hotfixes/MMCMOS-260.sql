DELETE FROM [dbo].[CmsRoleAccess] WHERE [RoleType] = 'MCAdmin' AND [Id] = 'UserOrderSearch' AND [RequiredRolePermissionName] IS NULL

IF NOT EXISTS (SELECT 1 FROM [dbo].[CmsRoleAccess] WHERE [RoleType] = 'MCAdmin' AND [Id] = 'UserOrderSearch' AND [RequiredRolePermissionName] = 'ap')
BEGIN
	INSERT INTO [dbo].[CmsRoleAccess] ([RoleType], [Area], [Controller], [Action], [Id], [QueryString], [ContentAction], [RequiredRolePermissionName]) 
	VALUES ('MCAdmin', NULL, 'WebForm', 'Index', 'UserOrderSearch', NULL, NULL, 'ap')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[CmsRoleAccess] WHERE [RoleType] = 'MCAdmin' AND [Id] = 'UserOrderSearch' AND [RequiredRolePermissionName] = 'ca')
BEGIN
	INSERT INTO [dbo].[CmsRoleAccess] ([RoleType], [Area], [Controller], [Action], [Id], [QueryString], [ContentAction], [RequiredRolePermissionName]) 
	VALUES ('MCAdmin', NULL, 'WebForm', 'Index', 'UserOrderSearch', NULL, NULL, 'ca')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[CmsRoleAccess] WHERE [RoleType] = 'MCAdmin' AND [Id] = 'UserOrderSearch' AND [RequiredRolePermissionName] = 'eu')
BEGIN
	INSERT INTO [dbo].[CmsRoleAccess] ([RoleType], [Area], [Controller], [Action], [Id], [QueryString], [ContentAction], [RequiredRolePermissionName]) 
	VALUES ('MCAdmin', NULL, 'WebForm', 'Index', 'UserOrderSearch', NULL, NULL, 'eu')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[CmsRoleAccess] WHERE [RoleType] = 'MCAdmin' AND [Id] = 'UserOrderSearch' AND [RequiredRolePermissionName] = 'lac')
BEGIN
	INSERT INTO [dbo].[CmsRoleAccess] ([RoleType], [Area], [Controller], [Action], [Id], [QueryString], [ContentAction], [RequiredRolePermissionName]) 
	VALUES ('MCAdmin', NULL, 'WebForm', 'Index', 'UserOrderSearch', NULL, NULL, 'lac')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[CmsRoleAccess] WHERE [RoleType] = 'MCAdmin' AND [Id] = 'UserOrderSearch' AND [RequiredRolePermissionName] = 'mea')
BEGIN
	INSERT INTO [dbo].[CmsRoleAccess] ([RoleType], [Area], [Controller], [Action], [Id], [QueryString], [ContentAction], [RequiredRolePermissionName]) 
	VALUES ('MCAdmin', NULL, 'WebForm', 'Index', 'UserOrderSearch', NULL, NULL, 'mea')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[CmsRoleAccess] WHERE [RoleType] = 'MCAdmin' AND [Id] = 'UserOrderSearch' AND [RequiredRolePermissionName] = 'us')
BEGIN
	INSERT INTO [dbo].[CmsRoleAccess] ([RoleType], [Area], [Controller], [Action], [Id], [QueryString], [ContentAction], [RequiredRolePermissionName]) 
	VALUES ('MCAdmin', NULL, 'WebForm', 'Index', 'UserOrderSearch', NULL, NULL, 'us')
END