BEGIN TRANSACTION
DECLARE @nextId VARCHAR(10)
DECLARE @nextSeedId INT

IF NOT EXISTS(SELECT 1 FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership')
	BEGIN
		SELECT @nextSeedId = ([IdSeed] + 1 ) FROM [dbo].[IdSeed]
		SELECT @nextId = LOWER(CONVERT(VARCHAR, CONVERT(VARBINARY(2), @nextSeedId), 2))
		UPDATE [dbo].[IdSeed] SET [IdSeed] = @nextSeedId

		INSERT [dbo].[TagCategory] ([TagCategoryID], [ListItemID], [ListID], [ListRelativeID], [Title], [ListLocation], [TagBrowser], [DisplayOrder], [Featurable], [IsRelatable]) 
		VALUES (@nextId, NEWID(), N'821CF247-9763-4452-870B-F55B9F36F764', 0, N'Thought Leadership', NULL, N'Yes', 1, N'Conditional', 1)
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Tag] WHERE [Identifier] = 'business-payments')
	BEGIN
		SELECT @nextSeedId = ([IdSeed] + 1 ) FROM [dbo].[IdSeed]
		SELECT @nextId = LOWER(CONVERT(VARCHAR, CONVERT(VARBINARY(2), @nextSeedId), 2))
		UPDATE [dbo].[IdSeed] SET [IdSeed] = @nextSeedId

		INSERT [dbo].[Tag] ([TagID], [ListItemID], [ListID], [ListRelativeID], [Identifier], [DisplayName], [RestrictAccess], [Featured], [Description], [PageContent], [CreatedDate], [ModifiedDate], [HideFromSearch], [RestrictToSegments], [SpecialAudienceAccess], [RestrictToSpecialAudience]) 
		VALUES (@nextId, NEWID(), N'F6A87193-B682-4B5D-9B2D-2F72B7E3FD5F', 0, N'business-payments', N'Business Payments', 0, 0, N'Business Payments', N'<p>Business Payments</p>', GETDATE(), GETDATE(), 0, N'', NULL, N'')
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Tag] WHERE [Identifier] = 'consumer-insights')
	BEGIN
		SELECT @nextSeedId = ([IdSeed] + 1 ) FROM [dbo].[IdSeed]
		SELECT @nextId = LOWER(CONVERT(VARCHAR, CONVERT(VARBINARY(2), @nextSeedId), 2))
		UPDATE [dbo].[IdSeed] SET [IdSeed] = @nextSeedId

		INSERT [dbo].[Tag] ([TagID], [ListItemID], [ListID], [ListRelativeID], [Identifier], [DisplayName], [RestrictAccess], [Featured], [Description], [PageContent], [CreatedDate], [ModifiedDate], [HideFromSearch], [RestrictToSegments], [SpecialAudienceAccess], [RestrictToSpecialAudience]) 
		VALUES (@nextId, NEWID(), N'F6A87193-B682-4B5D-9B2D-2F72B7E3FD5F', 0, N'consumer-insights', N'Consumer Insights', 0, 0, N'Consumer Insights', N'<p>Consumer Insights</p>', GETDATE(), GETDATE(), 0, N'', NULL, N'')
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Tag] WHERE [Identifier] = 'digital')
	BEGIN
		SELECT @nextSeedId = ([IdSeed] + 1 ) FROM [dbo].[IdSeed]
		SELECT @nextId = LOWER(CONVERT(VARCHAR, CONVERT(VARBINARY(2), @nextSeedId), 2))
		UPDATE [dbo].[IdSeed] SET [IdSeed] = @nextSeedId

		INSERT [dbo].[Tag] ([TagID], [ListItemID], [ListID], [ListRelativeID], [Identifier], [DisplayName], [RestrictAccess], [Featured], [Description], [PageContent], [CreatedDate], [ModifiedDate], [HideFromSearch], [RestrictToSegments], [SpecialAudienceAccess], [RestrictToSpecialAudience]) 
		VALUES (@nextId, NEWID(), N'F6A87193-B682-4B5D-9B2D-2F72B7E3FD5F', 0, N'digital', N'Digital', 0, 0, N'Digital', N'<p>Digital</p>', GETDATE(), GETDATE(), 0, N'', NULL, N'')
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Tag] WHERE [Identifier] = 'security-fraud')
	BEGIN
		SELECT @nextSeedId = ([IdSeed] + 1 ) FROM [dbo].[IdSeed]
		SELECT @nextId = LOWER(CONVERT(VARCHAR, CONVERT(VARBINARY(2), @nextSeedId), 2))
		UPDATE [dbo].[IdSeed] SET [IdSeed] = @nextSeedId

		INSERT [dbo].[Tag] ([TagID], [ListItemID], [ListID], [ListRelativeID], [Identifier], [DisplayName], [RestrictAccess], [Featured], [Description], [PageContent], [CreatedDate], [ModifiedDate], [HideFromSearch], [RestrictToSegments], [SpecialAudienceAccess], [RestrictToSpecialAudience]) 
		VALUES (@nextId, NEWID(), N'F6A87193-B682-4B5D-9B2D-2F72B7E3FD5F', 0, N'security-fraud', N'Security & Fraud', 0, 0, N'Security & Fraud', N'<p>Security & Fraud</p>', GETDATE(), GETDATE(), 0, N'', NULL, N'')
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Tag] WHERE [Identifier] = 'spend-loyalty')
	BEGIN
		SELECT @nextSeedId = ([IdSeed] + 1 ) FROM [dbo].[IdSeed]
		SELECT @nextId = LOWER(CONVERT(VARCHAR, CONVERT(VARBINARY(2), @nextSeedId), 2))
		UPDATE [dbo].[IdSeed] SET [IdSeed] = @nextSeedId

		INSERT [dbo].[Tag] ([TagID], [ListItemID], [ListID], [ListRelativeID], [Identifier], [DisplayName], [RestrictAccess], [Featured], [Description], [PageContent], [CreatedDate], [ModifiedDate], [HideFromSearch], [RestrictToSegments], [SpecialAudienceAccess], [RestrictToSpecialAudience]) 
		VALUES (@nextId, NEWID(), N'F6A87193-B682-4B5D-9B2D-2F72B7E3FD5F', 0, N'spend-loyalty', N'Spend & Loyalty', 0, 0, N'Spend & Loyalty', N'<p>Spend & Loyalty</p>', GETDATE(), GETDATE(), 0, N'', NULL, N'')
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Tag] WHERE [Identifier] = 'whitepapers')
	BEGIN
		SELECT @nextSeedId = ([IdSeed] + 1 ) FROM [dbo].[IdSeed]
		SELECT @nextId = LOWER(CONVERT(VARCHAR, CONVERT(VARBINARY(2), @nextSeedId), 2))
		UPDATE [dbo].[IdSeed] SET [IdSeed] = @nextSeedId

		INSERT [dbo].[Tag] ([TagID], [ListItemID], [ListID], [ListRelativeID], [Identifier], [DisplayName], [RestrictAccess], [Featured], [Description], [PageContent], [CreatedDate], [ModifiedDate], [HideFromSearch], [RestrictToSegments], [SpecialAudienceAccess], [RestrictToSpecialAudience]) 
		VALUES (@nextId, NEWID(), N'F6A87193-B682-4B5D-9B2D-2F72B7E3FD5F', 0, N'whitepapers', N'Whitepapers', 0, 0, N'Whitepapers', N'<p>Whitepapers</p>', GETDATE(), GETDATE(), 0, N'', NULL, N'')
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Tag] WHERE [Identifier] = 'videos')
	BEGIN
		SELECT @nextSeedId = ([IdSeed] + 1 ) FROM [dbo].[IdSeed]
		SELECT @nextId = LOWER(CONVERT(VARCHAR, CONVERT(VARBINARY(2), @nextSeedId), 2))
		UPDATE [dbo].[IdSeed] SET [IdSeed] = @nextSeedId

		INSERT [dbo].[Tag] ([TagID], [ListItemID], [ListID], [ListRelativeID], [Identifier], [DisplayName], [RestrictAccess], [Featured], [Description], [PageContent], [CreatedDate], [ModifiedDate], [HideFromSearch], [RestrictToSegments], [SpecialAudienceAccess], [RestrictToSpecialAudience]) 
		VALUES (@nextId, NEWID(), N'F6A87193-B682-4B5D-9B2D-2F72B7E3FD5F', 0, N'videos', N'Videos', 0, 0, N'Videos', N'<p>Videos</p>', GETDATE(), GETDATE(), 0, N'', NULL, N'')
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Tag] WHERE [Identifier] = 'others')
	BEGIN
		SELECT @nextSeedId = ([IdSeed] + 1 ) FROM [dbo].[IdSeed]
		SELECT @nextId = LOWER(CONVERT(VARCHAR, CONVERT(VARBINARY(2), @nextSeedId), 2))
		UPDATE [dbo].[IdSeed] SET [IdSeed] = @nextSeedId

		INSERT [dbo].[Tag] ([TagID], [ListItemID], [ListID], [ListRelativeID], [Identifier], [DisplayName], [RestrictAccess], [Featured], [Description], [PageContent], [CreatedDate], [ModifiedDate], [HideFromSearch], [RestrictToSegments], [SpecialAudienceAccess], [RestrictToSpecialAudience]) 
		VALUES (@nextId, NEWID(), N'F6A87193-B682-4B5D-9B2D-2F72B7E3FD5F', 0, N'others', N'Others', 0, 0, N'Others', N'<p>Others</p>', GETDATE(), GETDATE(), 0, N'', NULL, N'')
	END

DECLARE @PositionId int

IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [TagCategoryID] = (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership') AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'business-payments'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT NULL, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'business-payments'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END

SELECT	@PositionId = PositionId 
FROM	[dbo].[TagHierarchyPosition]
WHERE	[TagCategoryID] = (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership') AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'business-payments');

IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'whitepapers'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'whitepapers'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'videos'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'videos'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'others'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'others'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [TagCategoryID] = (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership') AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'consumer-insights'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT NULL, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'consumer-insights'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END

SELECT	@PositionId = PositionId 
FROM	[dbo].[TagHierarchyPosition]
WHERE	[TagCategoryID] = (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership') AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'consumer-insights');

IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'whitepapers'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'whitepapers'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'videos'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'videos'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'others'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'others'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [TagCategoryID] = (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership') AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'digital'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT NULL, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'digital'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END

SELECT	@PositionId = PositionId 
FROM	[dbo].[TagHierarchyPosition]
WHERE	[TagCategoryID] = (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership') AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'digital');

IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'whitepapers'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'whitepapers'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'videos'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'videos'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'others'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'others'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [TagCategoryID] = (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership') AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'security-fraud'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT NULL, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'security-fraud'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END

SELECT	@PositionId = PositionId 
FROM	[dbo].[TagHierarchyPosition]
WHERE	[TagCategoryID] = (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership') AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'security-fraud');

IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'whitepapers'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'whitepapers'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'videos'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'videos'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'others'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'others'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [TagCategoryID] = (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership') AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'spend-loyalty'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT NULL, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'spend-loyalty'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END

SELECT	@PositionId = PositionId 
FROM	[dbo].[TagHierarchyPosition]
WHERE	[TagCategoryID] = (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership') AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'spend-loyalty');

IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'whitepapers'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'whitepapers'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'videos'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'videos'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'others'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'others'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Overview')
	BEGIN
		SELECT @nextSeedId = ([IdSeed] + 1 ) FROM [dbo].[IdSeed]
		SELECT @nextId = LOWER(CONVERT(VARCHAR, CONVERT(VARBINARY(2), @nextSeedId), 2))
		UPDATE [dbo].[IdSeed] SET [IdSeed] = @nextSeedId

		INSERT INTO [dbo].[FeatureLocation] VALUES (@nextId,'F6A87193-B682-4B5D-9B2D-2F72B7E3FD5F','Thought Leadership � Overview')
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Business Payments')
	BEGIN
		SELECT @nextSeedId = ([IdSeed] + 1 ) FROM [dbo].[IdSeed]
		SELECT @nextId = LOWER(CONVERT(VARCHAR, CONVERT(VARBINARY(2), @nextSeedId), 2))
		UPDATE [dbo].[IdSeed] SET [IdSeed] = @nextSeedId

		INSERT INTO [dbo].[FeatureLocation] VALUES (@nextId,'F6A87193-B682-4B5D-9B2D-2F72B7E3FD5F','Thought Leadership � Business Payments')
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Consumer Insights')
	BEGIN
		SELECT @nextSeedId = ([IdSeed] + 1 ) FROM [dbo].[IdSeed]
		SELECT @nextId = LOWER(CONVERT(VARCHAR, CONVERT(VARBINARY(2), @nextSeedId), 2))
		UPDATE [dbo].[IdSeed] SET [IdSeed] = @nextSeedId

		INSERT INTO [dbo].[FeatureLocation] VALUES (@nextId,'F6A87193-B682-4B5D-9B2D-2F72B7E3FD5F','Thought Leadership � Consumer Insights')
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Digital')
	BEGIN
		SELECT @nextSeedId = ([IdSeed] + 1 ) FROM [dbo].[IdSeed]
		SELECT @nextId = LOWER(CONVERT(VARCHAR, CONVERT(VARBINARY(2), @nextSeedId), 2))
		UPDATE [dbo].[IdSeed] SET [IdSeed] = @nextSeedId

		INSERT INTO [dbo].[FeatureLocation] VALUES (@nextId,'F6A87193-B682-4B5D-9B2D-2F72B7E3FD5F','Thought Leadership � Digital')
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Security & Fraud')
	BEGIN
		SELECT @nextSeedId = ([IdSeed] + 1 ) FROM [dbo].[IdSeed]
		SELECT @nextId = LOWER(CONVERT(VARCHAR, CONVERT(VARBINARY(2), @nextSeedId), 2))
		UPDATE [dbo].[IdSeed] SET [IdSeed] = @nextSeedId

		INSERT INTO [dbo].[FeatureLocation] VALUES (@nextId,'F6A87193-B682-4B5D-9B2D-2F72B7E3FD5F','Thought Leadership � Security & Fraud')
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Spend & Loyalty')
	BEGIN
		SELECT @nextSeedId = ([IdSeed] + 1 ) FROM [dbo].[IdSeed]
		SELECT @nextId = LOWER(CONVERT(VARCHAR, CONVERT(VARBINARY(2), @nextSeedId), 2))
		UPDATE [dbo].[IdSeed] SET [IdSeed] = @nextSeedId

		INSERT INTO [dbo].[FeatureLocation] VALUES (@nextId,'F6A87193-B682-4B5D-9B2D-2F72B7E3FD5F','Thought Leadership � Spend & Loyalty')
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[FeatureLocationScope] WHERE [FeatureLocationID] = (SELECT TOP 1 [FeatureLocationID] FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Overview') AND [ContentTypeID] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Marquee Slide'))
	BEGIN
		INSERT [dbo].[FeatureLocationScope] ([FeatureLocationID], [ContentTypeID])
		VALUES ((SELECT TOP 1 [FeatureLocationID] FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Overview'), (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Marquee Slide'))
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[FeatureLocationScope] WHERE [FeatureLocationID] = (SELECT TOP 1 [FeatureLocationID] FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Business Payments') AND [ContentTypeID] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Marquee Slide'))
	BEGIN
		INSERT [dbo].[FeatureLocationScope] ([FeatureLocationID], [ContentTypeID])
		VALUES ((SELECT TOP 1 [FeatureLocationID] FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Business Payments'), (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Marquee Slide'))
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[FeatureLocationScope] WHERE [FeatureLocationID] = (SELECT TOP 1 [FeatureLocationID] FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Consumer Insights') AND [ContentTypeID] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Marquee Slide'))
	BEGIN
		INSERT [dbo].[FeatureLocationScope] ([FeatureLocationID], [ContentTypeID])
		VALUES ((SELECT TOP 1 [FeatureLocationID] FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Consumer Insights'), (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Marquee Slide'))
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[FeatureLocationScope] WHERE [FeatureLocationID] = (SELECT TOP 1 [FeatureLocationID] FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Digital') AND [ContentTypeID] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Marquee Slide'))
	BEGIN
		INSERT [dbo].[FeatureLocationScope] ([FeatureLocationID], [ContentTypeID])
		VALUES ((SELECT TOP 1 [FeatureLocationID] FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Digital'), (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Marquee Slide'))
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[FeatureLocationScope] WHERE [FeatureLocationID] = (SELECT TOP 1 [FeatureLocationID] FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Security & Fraud') AND [ContentTypeID] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Marquee Slide'))
	BEGIN
		INSERT [dbo].[FeatureLocationScope] ([FeatureLocationID], [ContentTypeID])
		VALUES ((SELECT TOP 1 [FeatureLocationID] FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Security & Fraud'), (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Marquee Slide'))
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[FeatureLocationScope] WHERE [FeatureLocationID] = (SELECT TOP 1 [FeatureLocationID] FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Spend & Loyalty') AND [ContentTypeID] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Marquee Slide'))
	BEGIN
		INSERT [dbo].[FeatureLocationScope] ([FeatureLocationID], [ContentTypeID])
		VALUES ((SELECT TOP 1 [FeatureLocationID] FROM [dbo].[FeatureLocation] WHERE [Title] = 'Thought Leadership � Spend & Loyalty'), (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Marquee Slide'))
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[FieldDefinition] WHERE [Name] = 'ThoughtLeadership')
	BEGIN
		INSERT INTO [dbo].[FieldDefinition]
		SELECT 'ThoughtLeadership', 'Thought Leadership', 'Tag', 0, 0, NULL, NULL, 1, '{ "TagCategoryId" : "' + (SELECT TOP 1 [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Thought Leadership') + '" }'
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[ContentTypeFields] WHERE [ContentTypeId] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Asset') AND [FieldDefinitionId] = (SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'ThoughtLeadership'))
	BEGIN
		INSERT INTO [dbo].[ContentTypeFields]
		SELECT (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Asset'),
		(SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'ThoughtLeadership'),
		(SELECT TOP 1 [Order] FROM [dbo].[ContentTypeFields] WHERE [ContentTypeId] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Asset') AND [FieldDefinitionId] = (SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'Insigths')),
		0, 0, 0, NULL, NULL, 1
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[ContentTypeFields] WHERE [ContentTypeId] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Asset Full Width') AND [FieldDefinitionId] = (SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'ThoughtLeadership'))
	BEGIN
		INSERT INTO [dbo].[ContentTypeFields]
		SELECT (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Asset Full Width'),
		(SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'ThoughtLeadership'),
		(SELECT TOP 1 [Order] FROM [dbo].[ContentTypeFields] WHERE [ContentTypeId] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Asset Full Width') AND [FieldDefinitionId] = (SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'Insigths')),
		0, 0, 0, NULL, NULL, 1
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[ContentTypeFields] WHERE [ContentTypeId] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Downloadable Asset') AND [FieldDefinitionId] = (SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'ThoughtLeadership'))
	BEGIN
		INSERT INTO [dbo].[ContentTypeFields]
		SELECT (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Downloadable Asset'),
		(SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'ThoughtLeadership'),
		(SELECT TOP 1 [Order] FROM [dbo].[ContentTypeFields] WHERE [ContentTypeId] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Downloadable Asset') AND [FieldDefinitionId] = (SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'Insigths')),
		0, 0, 0, NULL, NULL, 1
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[ContentTypeFields] WHERE [ContentTypeId] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Orderable Asset') AND [FieldDefinitionId] = (SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'ThoughtLeadership'))
	BEGIN
		INSERT INTO [dbo].[ContentTypeFields]
		SELECT (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Orderable Asset'),
		(SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'ThoughtLeadership'),
		(SELECT TOP 1 [Order] FROM [dbo].[ContentTypeFields] WHERE [ContentTypeId] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Orderable Asset') AND [FieldDefinitionId] = (SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'Insigths')),
		0, 0, 0, NULL, NULL, 1
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[ContentTypeFields] WHERE [ContentTypeId] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Program') AND [FieldDefinitionId] = (SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'ThoughtLeadership'))
	BEGIN
		INSERT INTO [dbo].[ContentTypeFields]
		SELECT (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Program'),
		(SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'ThoughtLeadership'),
		(SELECT TOP 1 [Order] FROM [dbo].[ContentTypeFields] WHERE [ContentTypeId] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Program') AND [FieldDefinitionId] = (SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'Insigths')),
		0, 0, 0, NULL, NULL, 1
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[ContentTypeFields] WHERE [ContentTypeId] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Webinar') AND [FieldDefinitionId] = (SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'ThoughtLeadership'))
	BEGIN
		INSERT INTO [dbo].[ContentTypeFields]
		SELECT (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Webinar'),
		(SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'ThoughtLeadership'),
		(SELECT TOP 1 [Order] FROM [dbo].[ContentTypeFields] WHERE [ContentTypeId] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Webinar') AND [FieldDefinitionId] = (SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'Insigths')),
		0, 0, 0, NULL, NULL, 1
	END

IF NOT EXISTS(SELECT 1 FROM [dbo].[ContentTypeFields] WHERE [ContentTypeId] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Page') AND [FieldDefinitionId] = (SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'ThoughtLeadership'))
	BEGIN
		INSERT INTO [dbo].[ContentTypeFields]
		SELECT (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Page'),
		(SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'ThoughtLeadership'),
		(SELECT TOP 1 [Order] FROM [dbo].[ContentTypeFields] WHERE [ContentTypeId] = (SELECT TOP 1 [ContentTypeID] FROM [dbo].[ContentType] WHERE [Title] = 'Page') AND [FieldDefinitionId] = (SELECT TOP 1 [FieldDefinitionId] FROM [dbo].[FieldDefinition] WHERE [Name] = 'Insigths')),
		0, 0, 0, NULL, NULL, 1
	END

COMMIT
GO