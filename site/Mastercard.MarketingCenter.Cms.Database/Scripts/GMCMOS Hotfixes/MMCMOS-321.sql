DECLARE @PositionId int
SELECT	@PositionId = PositionId 
FROM	[dbo].[TagHierarchyPosition]
WHERE	[TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'programs-and-campaigns');

IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'asia-pacific'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'asia-pacific'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Marketing'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'easy-savings'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'easy-savings'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Marketing'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'hong-kong'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'hong-kong'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Marketing'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'india'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'india'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Marketing'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'japan'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'japan'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Marketing'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'korea'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'korea'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Marketing'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'malaysia'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'malaysia'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Marketing'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'MasterCard Business Network'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'MasterCard Business Network'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Marketing'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'outside-asia-pacific'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'outside-asia-pacific'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Marketing'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'Sams Club'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'Sams Club'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Marketing'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'Sams Club-IBCU'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'Sams Club-IBCU'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Marketing'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'singapore'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'singapore'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Marketing'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'taiwan'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'taiwan'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Marketing'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'thailand'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'thailand'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Marketing'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END
IF NOT EXISTS(SELECT 1 FROM [dbo].[TagHierarchyPosition] WHERE [ParentPositionID] = @PositionId AND [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'world-offers-and-experiences'))
	BEGIN
		INSERT [dbo].[TagHierarchyPosition] ([ParentPositionID], [TagID], [TagCategoryID], [SortOrder])
		SELECT @PositionId, (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'world-offers-and-experiences'), (SELECT [TagCategoryID] FROM [dbo].[TagCategory] WHERE [Title] = 'Marketing'), (SELECT MAX([SortOrder]) + 1 FROM [dbo].[TagHierarchyPosition]);
	END

UPDATE [dbo].[Page]
SET [Title] = 'Programs & Campaigns', [Uri] = 'marketing/programs-and-campaigns'
WHERE [ContentItemId] = (SELECT [ContentItemId] FROM [dbo].[ContentItem] WHERE [FrontEndUrl] = 'marketing/merchant-offers' AND [RegionId] = 'ap' AND [StatusID] <> 6)

UPDATE [dbo].[ContentItem]
SET [FrontEndUrl] = 'marketing/programs-and-campaigns'
WHERE [FrontEndUrl] = 'marketing/merchant-offers' AND [RegionId] = 'ap' AND [StatusID] <> 6

UPDATE [dbo].[ContentItemTag]
SET [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'programs-and-campaigns')
WHERE [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'merchant-offers')
AND [ContentItemID] IN (SELECT [ContentItemID] FROM [dbo].[ContentItem] WHERE [Regionid] = 'ap')

UPDATE [dbo].[ContentItemFeatureOnTag]
SET [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'programs-and-campaigns')
WHERE [TagID] = (SELECT [TagID] FROM [dbo].[Tag] WHERE [Identifier] = 'merchant-offers')
AND [ContentItemID] IN (SELECT [ContentItemID] FROM [dbo].[ContentItem] WHERE [Regionid] = 'ap')

UPDATE [dbo].[ContentItemFeatureLocation]
SET [FeatureLocationID] = (SELECT [FeatureLocationID] FROM [dbo].[FeatureLocation] WHERE [Title] = 'Marketing – Programs & Campaigns')
WHERE [FeatureLocationID] = (SELECT [FeatureLocationID] FROM [dbo].[FeatureLocation] WHERE [Title] = 'Marketing  – Merchant Offers')
AND [ContentItemID] IN (SELECT [ContentItemID] FROM [dbo].[ContentItem] WHERE [Regionid] = 'ap')

UPDATE [dbo].[MarqueeSlide]
SET [Title] = 'Marketing - Programs & Campaigns'
WHERE [Title] = 'Marketing - Merchant Offers' AND [ContentItemId] IN (SELECT [ContentItemId] FROM [dbo].[ContentItem] WHERE [RegionId] = 'ap' AND [StatusID] <> 6)