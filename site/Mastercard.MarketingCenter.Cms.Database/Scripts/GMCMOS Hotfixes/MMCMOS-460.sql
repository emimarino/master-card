﻿-- Disable Content Item Favoriting for non-US regions
UPDATE [dbo].[Page] SET [EnableFavoriting] = 0 WHERE [ContentItemId] IN (SELECT [ContentItemId] FROM [ContentItem] WHERE LTRIM(RTRIM([RegionId])) <> 'us');
UPDATE [dbo].[AssetFullWidth] SET [EnableFavoriting] = 0 WHERE [ContentItemId] IN (SELECT [ContentItemId] FROM [ContentItem] WHERE LTRIM(RTRIM([RegionId])) <> 'us');
UPDATE [dbo].[Asset] SET [EnableFavoriting] = 0 WHERE [ContentItemId] IN (SELECT [ContentItemId] FROM [ContentItem] WHERE LTRIM(RTRIM([RegionId])) <> 'us');
UPDATE [dbo].[DownloadableAsset] SET [EnableFavoriting] = 0 WHERE [ContentItemId] IN (SELECT [ContentItemId] FROM [ContentItem] WHERE LTRIM(RTRIM([RegionId])) <> 'us');
UPDATE [dbo].[OrderableAsset] SET [EnableFavoriting] = 0 WHERE [ContentItemId] IN (SELECT [ContentItemId] FROM [ContentItem] WHERE LTRIM(RTRIM([RegionId])) <> 'us');
UPDATE [dbo].[Program] SET [EnableFavoriting] = 0 WHERE [ContentItemId] IN (SELECT [ContentItemId] FROM [ContentItem] WHERE LTRIM(RTRIM([RegionId])) <> 'us');
UPDATE [dbo].[Webinar] SET [EnableFavoriting] = 0 WHERE [ContentItemId] IN (SELECT [ContentItemId] FROM [ContentItem] WHERE LTRIM(RTRIM([RegionId])) <> 'us');


DELETE FROM ContentItemUserSubscription 
WHERE exists(  SELECT 1 FROM  contentitem ci WHERE  ci.ContentITemId=ContentItemUserSubscription.ContentITemId  AND RegionId<>'us')
