IF(NOT EXISTS(SELECT 1 FROM [dbo].[ContentTypeClientEvent] WHERE [ContentTypeID] = 'uv' AND [Name] = 'InvalidateContentQueries' AND [FiredOn] = 'New' AND [ClientEventCode] = '$.get("/portal/Cache/InvalidateContentQueries");' AND [ContentAction] = 'SaveLive' ))
BEGIN
INSERT INTO [dbo].[ContentTypeClientEvent] ([ContentTypeID], [Name], [FiredOn], [ClientEventCode], [ContentAction]) VALUES ( 'uv', 'InvalidateContentQueries', 'New', '$.get("/portal/Cache/InvalidateContentQueries");' , 'SaveLive');
END
IF(NOT EXISTS(SELECT 1 FROM [dbo].[ContentTypeClientEvent] WHERE [ContentTypeID] = 'uv' AND [Name] = 'InvalidateContentQueries' AND [FiredOn] = 'Update' AND [ClientEventCode] = '$.get("/portal/Cache/InvalidateContentQueries");' AND [ContentAction] = 'SaveLive' ))
BEGIN
INSERT INTO [dbo].[ContentTypeClientEvent] ([ContentTypeID], [Name], [FiredOn], [ClientEventCode], [ContentAction]) VALUES ( 'uv', 'InvalidateContentQueries', 'Update', '$.get("/portal/Cache/InvalidateContentQueries");' , 'SaveLive');
END