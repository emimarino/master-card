﻿CREATE TABLE [dbo].[GdpRequest]
(
	GdpRequestId INT NOT NULL PRIMARY KEY Identity(1,1),
	RequestId INT NOT NULL ,	
	ResponseType NVARCHAR(50),
	RequestType NVARCHAR(50),
	RequestContext NVARCHAR(50),
    RequestDate    DateTime   ,
    SearchKey NVARCHAR(50)
)