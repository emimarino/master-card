﻿CREATE TABLE [dbo].[UserConsent]
(
	UserConsentId INT NOT NULL PRIMARY KEY Identity(1,1),
	 UserId int NOT NULL,
         ConsentType  NVARCHAR(50) NOT NULL,
         ConsentDate DateTime NOT NULL,
         VersionID INT NOT NULL,
		  LocaleCode NVARCHAR(50) NOT NULL,
       ConsentStatus NVARCHAR(50) NOT NULL
        
)