﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Domo.DTO;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Domo.Services
{
    public interface IDownloadsService
    {
        /// <summary>
        /// Process and aggregates data to be used in API requests;
        /// </summary>
        /// <returns>The nuber of new Downloads</returns>
        int ProcessData();

        IEnumerable<DomoDownloadItemDto> GetDownloads(DateTime from, DateTime to, string contentRegion = null, string ContentTypeId = null, string userRegion = null, string userFinancialInstitution = null, string userEmail = null, bool includeExcludedResults = false);
        void UpdateDownloadUserData(User user);
        void ExpireDownloadOrphanUserData(int expirationWindowInMonths);
    }
}