﻿using AutoMapper;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Domo.DTO;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Domo.Services
{
    public class SearchActivityService : ISearchActivityService
    {
        private readonly SearchRepository _repository;
        public SearchActivityService(SearchRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<DomoSearchActivityDto> GetActivity(DateTime from, DateTime to, int? SearchActivityID = null, string UserFullName = null, string UserEmail = null, string UserTitle = null, string UserRegion = null, string UserCountry = null, string SearchDateTime = null, string SearchTerm = null, string UrlVisited = null, string RegionalSite = null, bool includeExcludedResults = false)
        {
            return Mapper.Map<IList<DomoSearchActivityDto>>(_repository.GetActivity(from, to, SearchActivityID, UserFullName, UserEmail, UserTitle, UserRegion, UserCountry, SearchDateTime, SearchTerm, UrlVisited, RegionalSite, includeExcludedResults));
        }
    }
}