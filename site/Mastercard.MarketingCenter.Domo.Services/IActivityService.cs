﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Domo.DTO;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Domo.Services
{
    public interface IActivityService
    {
        IEnumerable<DomoActivityItemDto> GetActivity(DateTime from, DateTime to, string contentRegion = null, string userRegion = null, string userMarket = null, string userEmail = null, bool includeExcludedResults = false);
        void UpdateActivityUserData(User user);
        void ExpireActivityOrphanUserData(int expirationWindowInMonths);
    }
}