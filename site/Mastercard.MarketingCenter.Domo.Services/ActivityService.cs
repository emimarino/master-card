﻿using AutoMapper;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Domo.DTO;
using Mastercard.MarketingCenter.Domo.Repository;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Domo.Services
{
    public class ActivityService : IActivityService
    {
        private readonly IDomoActivityRepository _domoActivityRepository;

        public ActivityService(IDomoActivityRepository domoActivityRepository)
        {
            _domoActivityRepository = domoActivityRepository;
        }

        public IEnumerable<DomoActivityItemDto> GetActivity(DateTime from, DateTime to, string contentRegion = null, string userRegion = null, string userMarket = null, string userEmail = null, bool includeExcludedResults = false)
        {
            return Mapper.Map<IList<DomoActivityItemDto>>(_domoActivityRepository.GetActivity(from, to, contentRegion, userRegion, userMarket, userEmail, includeExcludedResults));
        }

        public void UpdateActivityUserData(User user)
        {
            _domoActivityRepository.UpdateActivityUserData(user.UserId, user.Email, user.Profile.FirstName, user.Profile.LastName);
        }

        public void ExpireActivityOrphanUserData(int expirationWindowInMonths)
        {
            _domoActivityRepository.ExpireActivityOrphanUserData(DateTime.Now.AddMonths(-expirationWindowInMonths));
        }
    }
}