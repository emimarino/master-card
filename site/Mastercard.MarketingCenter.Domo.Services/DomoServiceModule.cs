﻿using Autofac;

namespace Mastercard.MarketingCenter.Domo.Services
{
    public class DomoServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DownloadsService>().As<IDownloadsService>();
            builder.RegisterType<ActivityService>().As<IActivityService>();
        }
    }
}