﻿using AutoMapper;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Domo.DTO;
using Mastercard.MarketingCenter.Domo.Repository;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Domo.Services
{
    public class DownloadsService : IDownloadsService
    {
        private readonly IDomoDownloadsRepository _domoDownloadsRepository;

        private static object domoDownloadsUpdateLock = new object();
        public DownloadsService(IDomoDownloadsRepository domoDownloadsRepository)
        {
            _domoDownloadsRepository = domoDownloadsRepository;
        }

        public int ProcessData()
        {
            lock (domoDownloadsUpdateLock)
            {
                _domoDownloadsRepository.CreateAuxTable();
                _domoDownloadsRepository.DecodeFileNames();

                return _domoDownloadsRepository.UpdateData();
            }
        }

        public IEnumerable<DomoDownloadItemDto> GetDownloads(DateTime from, DateTime to, string contentRegion = null, string ContentTypeId = null, string userRegion = null, string userFinancialInstitution = null, string userEmail = null, bool includeExcludedResults = false)
        {
            return Mapper.Map<IList<DomoDownloadItemDto>>(_domoDownloadsRepository.GetDownloads(from, to, contentRegion, ContentTypeId, userRegion, userFinancialInstitution, userEmail, includeExcludedResults));
        }

        public void UpdateDownloadUserData(User user)
        {
            _domoDownloadsRepository.UpdateDownloadUserData(user.UserId, user.Email, user.Profile.FirstName, user.Profile.LastName);
        }

        public void ExpireDownloadOrphanUserData(int expirationWindowInMonths)
        {
            _domoDownloadsRepository.ExpireDownloadOrphanUserData(DateTime.Now.AddMonths(-expirationWindowInMonths));
        }
    }
}