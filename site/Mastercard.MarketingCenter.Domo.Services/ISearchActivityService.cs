﻿using Mastercard.MarketingCenter.Domo.DTO;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Domo.Services
{
    public interface ISearchActivityService
    {
        IEnumerable<DomoSearchActivityDto> GetActivity(DateTime from, DateTime to, int? SearchActivityID = null, string UserFullName = null, string UserEmail = null, string UserTitle = null, string UserRegion = null, string UserCountry = null, string SearchDateTime = null, string SearchTerm = null, string UrlVisited = null, string RegionalSite = null, bool includeExcludedResults = false);
    }
}