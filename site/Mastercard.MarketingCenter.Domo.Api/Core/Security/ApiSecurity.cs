﻿using System.Configuration;

namespace Mastercard.MarketingCenter.Domo.Api.Core.Security
{
    public static class ApiSecurity
    {
        public static bool Validate(string userName, string password)
        {
            string user = ConfigurationManager.AppSettings["DomoUsername"];
            string pwd = ConfigurationManager.AppSettings["DomoPassword"];

            if (userName == user && password == pwd)
            {
                return true;
            }

            return false;
        }
    }
}