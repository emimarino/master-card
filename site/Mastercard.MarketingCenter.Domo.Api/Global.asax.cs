﻿using System.Web;
using System.Web.Http;

namespace Mastercard.MarketingCenter.Domo.Api
{
    public class WebApiApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AutofacConfig.Register();
            AutomapperConfig.Configure();
        }
    }
}