﻿using Mastercard.MarketingCenter.Domo.Api.Attributes;
using Mastercard.MarketingCenter.Domo.Api.Core.Attributes;
using Mastercard.MarketingCenter.Domo.DTO;
using Mastercard.MarketingCenter.Domo.Services;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Mastercard.MarketingCenter.Domo.Api.Controllers
{
    [BasicAuthentication]
    public class SearchActivityController : ApiController
    {
        private readonly ISearchActivityService _searchActivityService;

        public SearchActivityController(ISearchActivityService searchActivityService)
        {
            _searchActivityService = searchActivityService;
        }

        [MaxDate("from", "to")]
        public IEnumerable<DomoSearchActivityDto> Get(DateTime from, DateTime to, int? SearchActivityID = null, string UserFullName = null, string UserEmail = null, string UserTitle = null, string UserRegion = null, string UserCountry = null, string SearchDateTime = null, string SearchTerm = null, string UrlVisited = null, string RegionalSite = null, bool includeExcludedResults = false)
        {
            return _searchActivityService.GetActivity(from, to, SearchActivityID, UserFullName, UserEmail, UserTitle, UserRegion, UserCountry, SearchDateTime, SearchTerm, UrlVisited, RegionalSite, includeExcludedResults);
        }
    }
}