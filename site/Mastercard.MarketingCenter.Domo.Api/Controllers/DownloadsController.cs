﻿using Mastercard.MarketingCenter.Domo.Api.Attributes;
using Mastercard.MarketingCenter.Domo.Api.Core.Attributes;
using Mastercard.MarketingCenter.Domo.DTO;
using Mastercard.MarketingCenter.Domo.Services;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Mastercard.MarketingCenter.Domo.Api.Controllers
{
    [BasicAuthentication]
    public class DownloadsController : ApiController
    {
        private readonly IDownloadsService _downloadsService;
        public DownloadsController(IDownloadsService downloadsService)
        {
            _downloadsService = downloadsService;
        }

        [MaxDate("from", "to")]
        public IEnumerable<DomoDownloadItemDto> Get(DateTime from, DateTime to, string contentItemRegion = null, string contentType = null, string userRegion = null, string userFinancialInstitution = null, string userEmail = null, bool includeExcludedResults = false)
        {
            return _downloadsService.GetDownloads(from, to, contentItemRegion, contentType, userRegion, userFinancialInstitution, userEmail, includeExcludedResults);
        }
    }
}