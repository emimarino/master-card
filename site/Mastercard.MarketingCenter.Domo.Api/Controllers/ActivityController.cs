﻿using Mastercard.MarketingCenter.Domo.Api.Attributes;
using Mastercard.MarketingCenter.Domo.Api.Core.Attributes;
using Mastercard.MarketingCenter.Domo.DTO;
using Mastercard.MarketingCenter.Domo.Services;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Mastercard.MarketingCenter.Domo.Api.Controllers
{
    [BasicAuthentication]
    public class ActivityController : ApiController
    {
        private readonly IActivityService _activityService;

        public ActivityController(IActivityService activityService)
        {
            _activityService = activityService;
        }

        [MaxDate("from", "to")]
        public IEnumerable<DomoActivityItemDto> Get(DateTime from, DateTime to, string contentRegion = null, string userRegion = null, string userMarket = null, string userEmail = null, bool includeExcludedResults = false)
        {
            return _activityService.GetActivity(from, to, contentRegion, userRegion, userMarket, userEmail, includeExcludedResults);
        }
    }
}