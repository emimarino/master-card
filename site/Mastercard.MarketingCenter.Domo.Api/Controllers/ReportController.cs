﻿using AutoMapper;
using Mastercard.MarketingCenter.Domo.Api.Attributes;
using Mastercard.MarketingCenter.Domo.Api.Core.Attributes;
using Mastercard.MarketingCenter.Domo.DTO;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Mastercard.MarketingCenter.Domo.Api.Controllers
{
    [RoutePrefix("reports")]
    [BasicAuthentication]
    public class ReportsController : ApiController
    {
        private readonly IReportService _reportService;

        public ReportsController(IReportService reportService)
        {
            _reportService = reportService;
        }

        [HttpGet]
        [MaxDate("from", "to")]
        [Route("user-registrations")]
        public IEnumerable<UserRegistrationResultItemDto> GetUserRegistrations(DateTime from, DateTime to, string userRegion = null, bool includeExcludedResults = false)
        {
            return Mapper.Map<IEnumerable<UserRegistrationResultItemDto>>(_reportService.GetUserRegistrationReport(from, to, userRegion, !includeExcludedResults, false, false));
        }
    }
}