using Mastercard.MarketingCenter.Domo.Api;
using Mastercard.MarketingCenter.Domo.Api.Core;
using Slam.Cms.Configuration;
using Swashbuckle.Application;
using Swashbuckle.Swagger;
using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Description;
using WebActivatorEx;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]
namespace Mastercard.MarketingCenter.Domo.Api
{
    public static class SwaggerConfig
    {
        public static void Register()
        {
            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1", "Mastercard MarketingCenter Domo API");
                    c.BasicAuth("basic").Description("Basic HTTP Authentication");
                    c.OperationFilter<AddAuthorizationHeaderParameterOperationFilter>();
                    c.OperationFilter<IncludeParameterNamesInOperationIdFilter>();
                    c.RootUrl(ResolveBasePath);
                })
                .EnableSwaggerUi(c =>
                {
                    c.CustomAsset("index", Assembly.GetAssembly(typeof(SwaggerConfig)), "Mastercard.MarketingCenter.Domo.Api.Content.index.html");
                });
        }

        private static string ResolveBasePath(HttpRequestMessage message)
        {
            var apiUrl = new Uri(ConfigurationManager.Environment.ApiUrl);
            var virtualPathRoot = message.GetRequestContext().VirtualPathRoot;
            if (apiUrl.Authority.Equals(message.RequestUri.Authority, StringComparison.InvariantCultureIgnoreCase) &&
                !apiUrl.Scheme.Equals(message.RequestUri.Scheme, StringComparison.InvariantCultureIgnoreCase))
            {
                return new Uri(new Uri($"{apiUrl.Scheme}://{message.RequestUri.Host}", UriKind.Absolute), virtualPathRoot).AbsoluteUri;
            }
            else
            {
                return new Uri(new Uri($"{message.RequestUri.Scheme}://{message.RequestUri.Host}", UriKind.Absolute), virtualPathRoot).AbsoluteUri.TrimEnd('/');
            }
        }
    }

    internal class IncludeParameterNamesInOperationIdFilter : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            if (operation.parameters != null)
            {
                // Select the capitalized parameter names
                var parameters = operation.parameters.Select(
                    p => CultureInfo.InvariantCulture.TextInfo.ToTitleCase(p.name));

                // Set the operation id to match the format "OperationByParam1AndParam2"
                operation.operationId = string.Format(
                    "{0}By{1}",
                    operation.operationId,
                    string.Join("And", parameters));
            }
        }
    }
}