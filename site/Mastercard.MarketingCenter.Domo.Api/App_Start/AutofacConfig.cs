﻿using Autofac;
using Autofac.Integration.WebApi;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Domo.Api.Controllers;
using Mastercard.MarketingCenter.Domo.Repository;
using Mastercard.MarketingCenter.Domo.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Services;
using Slam.Cms;
using Slam.Cms.Common.Interfaces;
using Slam.Cms.Common;
using Slam.Cms.Data;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Http;

namespace Mastercard.MarketingCenter.Domo.Api
{
    public static class AutofacConfig
    {
        public static void Register()
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(typeof(ActivityController).Assembly);

            var connectionString = ConfigurationManager.ConnectionStrings["MasterCardMarketingCenter"].ConnectionString;

            builder.Register(c =>
            {
                var connection = new SqlConnection(connectionString);
                connection.Open();
                return connection;
            }).As<IDbConnection>().InstancePerLifetimeScope();

            builder.Register(c =>
            {
                return new CookieBasedPreviewMode(HttpContext.Current);
            }).As<IPreviewMode>().InstancePerLifetimeScope();
            builder.RegisterType<HttpContextCachingService>().As<ICachingService>().InstancePerLifetimeScope();

            builder.Register(c =>
            {
                return new SlamContext(c.Resolve<IPreviewMode>())
                    .SetKnownContentTypes(typeof(AssetDTO).GetAssembly())
                    .SetCachingService(c.Resolve<ICachingService>())
                    .SetConnectionFactory(c.Resolve<Func<IDbConnection>>());
            }).InstancePerLifetimeScope();

            builder.RegisterType<MarketingCenterDbContext>().InstancePerLifetimeScope();

            builder.RegisterModule<DomoPersistanceModule>();
            
            builder.RegisterType<ReportRepository>();
            builder.RegisterType<SiteTrackingRepository>().As<ISiteTrackingRepository>();
            builder.RegisterType<UserSubscriptionRepository>();
            builder.RegisterType<SearchRepository>();
            builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<IssuerRepository>().As<IIssuerRepository>();
            builder.RegisterType<DownloadsService>().As<IDownloadsService>();
            builder.RegisterType<ActivityService>().As<IActivityService>();
            builder.RegisterType<ReportService>().As<IReportService>();
            builder.RegisterType<SearchActivityService>().As<ISearchActivityService>();

            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(builder.Build()); //Set the WebApi DependencyResolver
        }
    }
}