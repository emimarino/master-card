﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Mastercard.MarketingCenter.Domo.Api.Attributes
{
    public class MaxDateAttribute : ActionFilterAttribute
    {
        private string _fromDateKey { get; set; }
        private string _toDateKey { get; set; }
        public MaxDateAttribute(string fromDateKey = "to", string toDateKey = "from")
        {
            _fromDateKey = fromDateKey;
            _toDateKey = toDateKey;
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var maxAPIResponseVal = ConfigurationManager.AppSettings[$"{actionContext.ControllerContext.ControllerDescriptor.ControllerName}.maxAPIResponse"];
            var maxAPIResponse = maxAPIResponseVal == null ? null : (int?)Convert.ToInt32(maxAPIResponseVal);
            if (maxAPIResponse != null && maxAPIResponse > 0)
            {
                var fromDateValue = Convert.ToDateTime(actionContext.ActionArguments[_fromDateKey]);
                var toDateValue = Convert.ToDateTime(actionContext.ActionArguments[_toDateKey]);
                if ((fromDateValue - toDateValue).TotalDays >= maxAPIResponse)
                {
                    var response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.NotAcceptable, App_GlobalResources.ErrorMessages.MaxDateExceeded);
                    actionContext.Response = response;
                }
            }
        }
    }
}