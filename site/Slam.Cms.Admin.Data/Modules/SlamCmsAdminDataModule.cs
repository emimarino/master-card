﻿using Autofac;

namespace Slam.Cms.Admin.Data
{
    public class SlamCmsAdminDataModule : Module
    {
        public SlamCmsAdminDataModule()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SlamCmsAdminDbContext>();
            builder.RegisterType<BrokenLinkRepository>();
        }
    }
}