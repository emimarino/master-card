﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Slam.Cms.Admin.Data
{
    public class BrokenLinkTrack
    {
        [Key]
        public int BrokenLinkTrackId { get; set; }
        public DateTime Date { get; set; }
        public string UserName { get; set; }
        public BrokenLink BrokenLink { get; set; }
    }
}