﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Slam.Cms.Admin.Data
{
    public class BrokenLink
    {
        [Key]
        public int BrokenLinkId { get; set; }
        public string Url { get; set; }
        public string Application { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsHandled { get; set; }
        public string Message { get; set; }
        public string RedirectUrl { get; set; }
        public string RedirectApplication { get; set; }
        public string Referrer { get; set; }
        public virtual ICollection<BrokenLinkTrack> BrokenLinkTracks { get; set; }
    }
}