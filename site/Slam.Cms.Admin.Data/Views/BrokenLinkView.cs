﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Slam.Cms.Admin.Data
{
	public class BrokenLinkView
	{
		public int BrokenLinkId { get; set; }
		public string Url { get; set; }
		public string Application { get; set; }
		public int Count { get; set; }
		public DateTime? LastAccessedDate { get; set; }
		public DateTime? ModifiedDate { get; set; }
		public string Message { get; set; }
		public string RedirectUrl { get; set; }
		public string RedirectApplication { get; set; }
	}
}
