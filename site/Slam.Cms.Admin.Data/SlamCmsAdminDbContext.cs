﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Slam.Cms.Admin.Data
{
    public class SlamCmsAdminDbContext : DbContext
    {
        public DbSet<BrokenLink> BrokenLinks { get; set; }
        public DbSet<BrokenLinkTrack> BrokenLinkTracks { get; set; }

        public SlamCmsAdminDbContext()
            : base("MasterCardMarketingCenter")
        {
            Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<BrokenLink>().ToTable("BrokenLink", "Admin");
            modelBuilder.Entity<BrokenLinkTrack>().ToTable("BrokenLinkTrack", "Admin");
            modelBuilder.Entity<BrokenLink>().HasMany(l => l.BrokenLinkTracks).WithRequired(t => t.BrokenLink).Map(c => c.MapKey("BrokenLinkId"));
        }
    }
}