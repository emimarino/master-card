﻿using System;
using System.Linq;

namespace Slam.Cms.Admin.Data
{
    /// <summary>
    /// This class represents the unit of work for repository operations
    /// </summary>
    public class SlamCmsAdminStorage
    {
        /// <summary>
        /// Context access for repositories
        /// </summary>
        public SlamCmsAdminDbContext Context { get; private set; }

        /// <summary>
        /// Public parameterless constructor
        /// </summary>
        public SlamCmsAdminStorage()
        {
            this.Context = new SlamCmsAdminDbContext();
        }

        /// <summary>
        /// commits repository operations to database
        /// </summary>
        public void Commit()
        {
            this.Context.SaveChanges();
        }

        #region IDisposable Members

        /// <summary>
        /// Releases resources
        /// </summary>
        public void Dispose()
        {
            this.Context.Dispose();
        }

        #endregion
    }
}