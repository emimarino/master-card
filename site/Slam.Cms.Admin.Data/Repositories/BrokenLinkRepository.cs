﻿using System.Linq;
using System;
using Slam.Cms.Configuration;
using System.IO;
using System.Collections.Generic;
using System.Data.Entity;

namespace Slam.Cms.Admin.Data
{
    public class BrokenLinkRepository : Repository<SlamCmsAdminDbContext>
    {
        public BrokenLinkRepository(SlamCmsAdminDbContext context)
            : base(context)
        { }

        public List<BrokenLinkView> GetAllUnmanagedBrokenLinks()
        {
            return (from brokenLink in Context.BrokenLinks.Where(l => !l.IsHandled)
                    select new BrokenLinkView()
                    {
                        BrokenLinkId = brokenLink.BrokenLinkId,
                        Url = brokenLink.Url,
                        Application = brokenLink.Application,
                        Count = brokenLink.BrokenLinkTracks.Count,
                        LastAccessedDate = brokenLink.BrokenLinkTracks.Select(t => t.Date).Max()
                    })
                    .ToList();
        }

        public List<BrokenLinkView> GetAllManagedBrokenLinks()
        {
            var links = (from brokenLink in Context.BrokenLinks.Where(l => l.IsHandled)
                         select new BrokenLinkView()
                         {
                             BrokenLinkId = brokenLink.BrokenLinkId,
                             Url = brokenLink.Url,
                             Application = brokenLink.Application,
                             ModifiedDate = brokenLink.ModifiedDate,
                             Message = brokenLink.Message,
                             RedirectUrl = brokenLink.RedirectUrl,
                             RedirectApplication = brokenLink.RedirectApplication
                         })
                        .ToList();

            foreach (var link in links)
                if (link.RedirectUrl != null && link.RedirectApplication != null && ConfigurationManager.Solution.Applications.Any(a => a.Key == link.RedirectApplication))
                    link.RedirectUrl = Path.Combine(ConfigurationManager.Solution.Applications[link.RedirectApplication].Url, link.RedirectUrl.TrimStart('/'));

            return links;
        }

        public BrokenLink GetById(int brokenLinkId)
        {
            return Context.BrokenLinks.Single(l => l.BrokenLinkId == brokenLinkId);
        }

        public BrokenLink GetByUrlAndApplication(string url, string application)
        {
            return Context.BrokenLinks.Single(l => l.Url.ToLower() == url.ToLower() && l.Application.ToLower() == application.ToLower());
        }

        public BrokenLink UpdateBrokenLink(BrokenLink brokenLink)
        {
            Context.SaveChanges();
            return brokenLink;
        }

        public bool IsBrokenLinkManaged(string url, string application)
        {
            var brokenLink = Context.BrokenLinks.SingleOrDefault(l => l.Url.ToLower() == url.ToLower() && l.Application.ToLower() == application.ToLower() && l.IsHandled);

            if (brokenLink != null)
            {
                if (brokenLink.Message == "[IgnoreOneTime]")
                {
                    brokenLink.Message = null;
                    brokenLink.IsHandled = false;
                    Context.SaveChanges();
                    return false;
                }
            }

            return (brokenLink != null);
        }

        public void TrackBrokenLink(string url, string application, string referrer, string userName)
        {
            var brokenLink = Context.BrokenLinks.SingleOrDefault(l => l.Url.ToLower() == url.ToLower() && l.Application.ToLower() == application.ToLower());
            if (brokenLink == null)
            {
                brokenLink = new BrokenLink();
                brokenLink.Url = url;
                brokenLink.Application = application;
                brokenLink.IsHandled = false;
                brokenLink.Referrer = referrer;
                Context.BrokenLinks.Add(brokenLink);
            }
            else if (!string.IsNullOrEmpty(referrer) && (string.IsNullOrEmpty(brokenLink.Referrer) || !brokenLink.Referrer.ToLower().Contains(referrer.ToLower())))
            {
                brokenLink.Referrer = string.IsNullOrEmpty(brokenLink.Referrer) ? referrer : (brokenLink.Referrer + "," + referrer);
            }

            var brokenLinkTrack = Context.BrokenLinkTracks.FirstOrDefault(t => t.BrokenLink.BrokenLinkId == brokenLink.BrokenLinkId && t.UserName.ToLower() == userName.ToLower() && DbFunctions.TruncateTime(t.Date) == DbFunctions.TruncateTime(DateTime.Now));
            if (brokenLinkTrack == null)
            {
                brokenLinkTrack = new BrokenLinkTrack();
                brokenLinkTrack.UserName = userName;
                brokenLinkTrack.Date = DateTime.Now;
                brokenLinkTrack.BrokenLink = brokenLink;
                Context.BrokenLinkTracks.Add(brokenLinkTrack);
            }

            Context.SaveChanges();
        }

        public void ClearAllUnmanagedBrokenLinks()
        {
            var command = Context.Database.Connection.CreateCommand();

            command.CommandType = System.Data.CommandType.Text;
            command.CommandText = "UPDATE [Admin].BrokenLink SET Message = '[IgnoreOneTime]', ModifiedDate = GETDATE(), IsHandled = 1 WHERE IsHandled = 0";

            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
            catch
            {
                command.Connection.Close();
                throw;
            }

            command.Connection.Close();
        }
    }
}