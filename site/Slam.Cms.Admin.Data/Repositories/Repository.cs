﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;

namespace Slam.Cms.Admin.Data
{
    /// <summary>
    /// Repository base class used with DbContext
    /// </summary>
    /// <typeparam name="TContext">Type of DdContext that this repository operates on</typeparam>
    public class Repository<TContext> : IRepository<TContext> where TContext : DbContext, IObjectContextAdapter, new()
    {
        protected TContext Context { get; private set; }

        public Repository()
        {
            Context = new TContext();
            Context.Database.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["Database"].ConnectionString;
        }

        /// <summary>
        /// Create new instance of repository
        /// </summary>
        /// <param name="connectionStringName">Connection string name from .config file</param>
        public Repository(string connectionStringName)
        {
            Context = new TContext();
            Context.Database.Connection.ConnectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
        }

        public Repository(TContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            this.Context = context;
        }

        /// <summary>
        /// Dispose repository
        /// </summary>
        public void Dispose()
        {
            if (Context != null)
            {
                // Context is not disposed because the lifecycle is controlled by the outside injection.
                Context = null;
            }
        }

        /// <summary>
        /// Select data from database
        /// </summary>
        /// <typeparam name="TItem">Type of data to select</typeparam>
        /// <returns></returns>
        public IQueryable<TItem> Select<TItem>()
           where TItem : class, new()
        {
            DbSet<TItem> set = Context.Set<TItem>();
            return set;
        }

        /// <summary>
        /// Select data from database using a where clause
        /// </summary>
        /// <typeparam name="TItem">Type of data to select</typeparam>
        /// <param name="whereClause">Where clause / function</param>
        /// <returns></returns>
        public IQueryable<TItem> Select<TItem>(Expression<Func<TItem, bool>> whereClause)
           where TItem : class, new()
        {
            IQueryable<TItem> data = Context.Set<TItem>().Where(whereClause);
            return data;
        }

        /// <summary>
        /// Select data from database using a where clause
        /// </summary>
        /// <typeparam name="TItem">Type of data to select</typeparam>
        /// <param name="whereClause">Where clause / function</param>
        /// <param name="orderBy">Order by clause</param>
        /// <returns></returns>
        public IOrderedQueryable<TItem> Select<TItem>(
          Expression<Func<TItem, bool>> whereClause,
          Expression<Func<TItem, object>> orderBy)
           where TItem : class, new()
        {
            IOrderedQueryable<TItem> data = Context.Set<TItem>().Where(whereClause).OrderBy(orderBy);
            return data;
        }

        /// <summary>
        /// Insert new item into database
        /// </summary>
        /// <typeparam name="TItem">Type of item to insert</typeparam>
        /// <param name="item">Item to insert</param>
        /// <returns>Inserted item</returns>
        public TItem Insert<TItem>(TItem item)
            where TItem : class, new()
        {
            DbSet<TItem> set = Context.Set<TItem>();
            set.Add(item);
            return item;
        }

        /// <summary>
        /// Update an item
        /// </summary>
        /// <typeparam name="TItem">Type of item to update</typeparam>
        /// <param name="item">Item to update</param>
        /// <returns>Updated item</returns>
        public TItem Update<TItem>(TItem item)
            where TItem : class, new()
        {
            DbSet<TItem> set = Context.Set<TItem>();
            set.Attach(item);
            Context.Entry(item).State = EntityState.Modified;
            return item;
        }

        /// <summary>
        /// Delete an item
        /// </summary>
        /// <typeparam name="TItem">Type of item to delete</typeparam>
        /// <param name="item">Item to delete</param>
        public void Delete<TItem>(TItem item)
           where TItem : class, new()
        {
            DbSet<TItem> set = Context.Set<TItem>();
            var entry = Context.Entry(item);
            if (entry != null)
            {
                entry.State = EntityState.Deleted;
            }
            else
            {
                set.Attach(item);
            }
            Context.Entry(item).State = EntityState.Deleted;
        }        
    }
}
