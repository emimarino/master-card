// Custom scripts
$(document).ready(function () {

    if ($('.ie8').length) {
        $('.onoffswitch-label, .branch-trigger').on('click', function (e) {
            e.preventDefault();

            var $lbl = $(this),
                $check = $('#' + $lbl.attr('for'));

            $check.prop('checked', !$check.prop('checked'));
        });
    }

    $(document).on('mouseup', '.paginate_button:not(.active)', function () {
        $('#wrapper').animate({ scrollTop: $('.ibox-content').offset().top });
    });

    $(document).on('mouseup', '[data-event$="Dialog"]', function () {
        var $this = $(this),
            $editor = $this.closest('.note-editor');

        setTimeout(function () {

            var wrapTop = $('.ibox-content').offset().top;

            $('.modal:visible').css({
                'top': $editor.offset().top - wrapTop
            });

            $('#wrapper').scrollTop($editor.offset().top - wrapTop);

        }, 100);
    });

    /* tree checks */
    $('.parent-check-req :checkbox').not('.box-trigger').on('change', function () {
        var $check = $(this),
            $branch = $check.closest('.branch');

        if ($branch.length) {
            var $parentCheck = $branch.siblings('.branch-parent').find(':checkbox');
            if ($(':checked', $branch).length) $parentCheck.prop('checked', true);
        } else if ($check.closest('.branch-parent').length) {
            var $branch = $check.closest('.branch-parent').siblings('.branch');
            if (!$check.is(':checked')) $(':checked', $branch).prop('checked', false);
        }
    });

    // open branch if branch parent is checked
    $('.branch-parent :checkbox').on('change', function () {
        var $checkbox = $(this);

        if ($checkbox.is(':checked')) {
            var branchCheckerId = $checkbox.closest('.branch-parent').siblings('.branch-trigger').attr('for');
            $('#' + branchCheckerId).prop('checked', true);
        }
    });

    /* datepicker */
    $('.input-date').datepicker({
        todayHighlight: true,
        format: "mm/dd/yyyy",
        autoclose: true
    });

    /*expiration datepicker */
    var now = new Date();
    $('.input-date-expiration').datepicker({
        todayHighlight: true,
        format: "mm/dd/yyyy",
        autoclose: true,
        endDate: new Date(now.getFullYear() + 1, now.getMonth(), now.getDate())
    });

    $('.chosen-choices').chosen({
        width: "100%",
        allow_single_deselect: true,
        group_search: false
    });

    /* chained multi-option selects */
    $('.chained-choices, .chosen-choices[multiple]').chosen({
        display_disabled_options: false,
        group_search: false
    });

    $('.chained-choices').on('change', function () {
        var $select = $(this),
            choices = $select.val() || '',
            $next = $('#' + $select.attr('data-chained-to'));

        if (!$next.length) return;

        $('[data-chained-value]', $next).not('[value=""]').prop('disabled', true);

        for (var i = 0; i < choices.length; i++) {
            $('[data-chained-value="' + choices[i] + '"]', $next).prop('disabled', false);
        }

        if ($('[data-chained-value]:enabled', $next).not('[value=""]').length) {
            $next.closest('.chained-to-wrapper').removeClass('hid');
        } else {
            $next.closest('.chained-to-wrapper').addClass('hid');
        }

        $('[data-chained-value]:disabled:selected, [data-chained-value]:disabled :selected', $next).prop('selected', false);

        $('.chained-choices').trigger('chosen:updated');
    });

    // MetsiMenu
    $('#side-menu').metisMenu();

    // Collapse ibox function
    $('.collapse-link:not(.binded)').addClass("binded").click(function () {
        var ibox = $(this).closest('div.ibox');
        var button = $(this).find('i');
        var content = ibox.find('div.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });

    // Close ibox function
    $('.close-link:not(.binded)').addClass("binded").click(function () {
        var content = $(this).closest('div.ibox');
        content.remove();
    });

    // Small todo handler
    $('.check-link:not(.binded)').addClass("binded").click(function () {
        var button = $(this).find('i');
        var label = $(this).next('span');
        button.toggleClass('fa-check-square').toggleClass('fa-square-o');
        label.toggleClass('todo-completed');
        return false;
    });

    // Append config box / Only for demo purpose
    /*
    $.get("skin-config.html", function (data) {
        $('body').append(data);
    });
    */

    // minimalize menu
    $('.navbar-minimalize:not(.binded)').addClass("binded").click(function () {
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();
    })

    // tooltips
    $('.tooltip-demo').tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    })

    // Move modal to body
    // Fix Bootstrap backdrop issu with animation.css
    $('.inmodal').appendTo("body");

    // Full height of sidebar
    function fix_height() {
        var heightWithoutNavbar = $("body > #wrapper").height() - 61;
        $(".sidebard-panel").css("min-height", heightWithoutNavbar + "px");
    }
    fix_height();

    // Fixed Sidebar
    // unComment this only whe you have a fixed-sidebar
    //    $(window).bind("load", function() {
    //        if($("body").hasClass('fixed-sidebar')) {
    //            $('.sidebar-collapse').slimScroll({
    //                height: '100%',
    //                railOpacity: 0.9,
    //            });
    //        }
    //    })

    $(window).bind("load resize click scroll", function () {
        if (!$("body").hasClass('body-small')) {
            fix_height();
        }
    })

    $("[data-toggle=popover]")
        .popover();

    $("[data-toggle=tooltip]").tooltip();
});

// For demo purpose - animation css script
window.animationHover = function (element, animation) {
    element = $(element);
    element.hover(
        function () {
            element.addClass('animated ' + animation);
        },
        function () {
            //wait for animation to finish before removing classes
            window.setTimeout(function () {
                element.removeClass('animated ' + animation);
            }, 2000);
        });
};

// Minimalize menu when screen is less than 768px
$(function () {
    $(window).bind("load resize", function () {
        if ($(this).width() < 769) {
            $('body').addClass('body-small')
        } else {
            $('body').removeClass('body-small')
        }
    })
})

window.SmoothlyMenu = function () {
    if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
        // Hide menu in order to smoothly turn on when maximize menu
        $('#side-menu').hide();
        // For smoothly turn on menu
        setTimeout(
            function () {
                $('#side-menu').fadeIn(500);
            }, 100);
    } else if ($('body').hasClass('fixed-sidebar')) {
        $('#side-menu').hide();
        setTimeout(
            function () {
                $('#side-menu').fadeIn(500);
            }, 300);
    } else {
        // Remove all inline style from jquery fadeIn function to reset menu state
        $('#side-menu').removeAttr('style');
    }
};

// Dragable panels
window.WinMove = function () {
    var element = "[class*=col]";
    var handle = ".ibox-title";
    var connect = "[class*=col]";
    $(element).sortable(
        {
            handle: handle,
            connectWith: connect,
            tolerance: 'pointer',
            forcePlaceholderSize: true,
            opacity: 0.8,
        })
        .disableSelection();
};

function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) != -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}