﻿var Tag = function (getContentTypesUrl, updateUrl) {
    this.getContentTypesUrl = getContentTypesUrl;
    this.updateUrl = updateUrl;
};

Tag.prototype.initialize = function () {

    var that = this;

    var table = $('#tagsGrid').DataTable({
        processing: true,
        serverSide: true,
        searching: true,
        pagingType: "simple_numbers",
        language: { paginate: { next: ">", previous: "&lt;" } },
        ajax:
        {
            "url": this.getContentTypesUrl,
            "type": "POST"
        },
        columns: [{ data: 'Edit', searchable: false, orderable: false }, { data: 'DisplayName', orderable: true }, { data: "TagID", orderable: true }],
        "order": [[1, 'asc']],
        "columnDefs":
            [{
                "targets": [0],
                "render": function (data, type, row) {
                    return "<td class='text-center'>\
                <div class='btn-group'> \
                    <button type='button' class='btn btn-sm dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> \
                    <span class='fa fa-cog'></span> \
                    </button> \
                    <ul class='dropdown-menu'> \
                   <li><a href='" + that.updateUrl + "/" + row["TagID"] + "'>Edit</a></li>" +
                        "<li><a class='delete-tag' href='#' data-id='" + row.TagID + "'>Delete</a></li> \
                </ul></div></td>";
                }
            },
            {
                "targets": [1],
                "render": function (data, type, row) {
                    return "<td class='text-center'><a href='" + that.updateUrl + "/" + row["TagID"] + "'>" + data + "</a></td>";
                }
            }
            ]
    });

    $(window).on('resize', function () {
        table.draw();
    });

    return table;
};
