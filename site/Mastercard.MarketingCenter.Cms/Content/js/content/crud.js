﻿var _getHistoryUrl;

var ContentItemCrud = function (getHistoryUrl) {
    _getHistoryUrl = getHistoryUrl;
};

ContentItemCrud.prototype.initialize = function () {
    $('.html-editor').summernote({
        toolbar: [
            ['textStyle', ['style', 'cleaner']],
            ['textPosition', ['superscript', 'subscript']],
            ['textBlock', ['ul', 'ol', 'paragraph']],
            ['fontStyle', ['bold', 'italic', 'strikethrough']],
            ['fontsize', ['fontsize']],
            ['insertMedia', ['picture', 'video']],
            ['insertBlock', ['table', 'link', 'hr']],
            ['Views', ['codeview', 'fullscreen']]
        ],
        fontSizes: ['8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36'],
        cleaner: {
            action: 'button', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
            newline: '<br>', // Summernote's default is to use '<p><br></p>'
            notStyle: 'position:absolute;top:0;left:0;right:0', // Position of Notification
            icon: '<i class="note-icon-eraser"></i>',
            keepHtml: true, // Remove all Html formats
            keepOnlyTags: ['<p>'], // If keepHtml is true, remove all tags except these
            keepClasses: false, // Remove Classes
            badTags: ['style', 'script', 'applet', 'embed', 'noframes', 'noscript', 'html'], // Remove full tags with contents
            badAttributes: ['style', 'start'], // Remove attributes from remaining tags
            limitChars: false, // 0/false|# 0/false disables option
            limitDisplay: 'both', // text|html|both
            limitStop: false, // true/false
            removeBreakLines: false, // remove new line carry char
            showNotification: false // enable / disable notification popup
        },
        height: 250,
        prettifyHtml: false,
        codemirror: {
            theme: 'default',
            mode: "xml",
            htmlMode: true,
            lineWrapping: true,
            lineNumbers: true,
            tabMode: 'indent'
        },
        callbacks: {
            onChange: function (contents, $editable) {
                var tr = $(this).closest('.form-group');
                if (!tr.length) {
                    return;
                }

                highlightIfChanged(tr);
            }
        }
    });

    $('.html-editor').parent().on('blur', '.CodeMirror', function () {
        var txtArea = $('.CodeMirror').closest(".note-editor").siblings("textarea")
        if (txtArea.summernote('codeview.isActivated')) {
            txtArea.val(txtArea.summernote('code'));
            txtArea.trigger('input');

        }
    });

    $("#histBtn").on("click", function () {
        var htmlEditors = $("textarea.html-editor");
        htmlEditors.each(function (idx, element) {
            var edited = $(element).siblings('.note-editor').find('.note-editable');
            element.innerHTML = edited[0].innerHTML;
        });

        var serialized = $('form').serialize();

        $.ajax({
            type: "POST",
            url: _getHistoryUrl,
            cache: false,
            data: serialized
        })
            .done(function (html) {
                var historyDialog = $("#historyDialog");
                historyDialog.find(".modal-body").empty();
                historyDialog.find(".modal-body").append(html);
                historyDialog.show();
            });
    });

    function disableOriginal(tr) {
        // disabling all original controls
        var parent = tr.children('.original');

        // textarea (summernote)
        var element = parent.find('.html-editor').addBack('.html-editor');
        if (element.length) {
            element.summernote('disable');
        }

        // images, file uploader
        element = parent.find('*[id*="_container"] button.btn, *[id*="_progress_container"]');
        if (element.length) {
            element.hide();
        }

        // tag selectors
        element = parent.find('button[data-toggle=modal]');
        if (element.length) {
            element.hide();
        }

        // markets
        element = parent.find('div[class="market-tag-selected-area"] a[data-toggle=modal]');
        if (element.length) {
            element.hide();
        }
        parent.find('input[type="radio"]').css("pointer-events", "none");
        parent.find('input[type="radio"]').css("cursor", "not-allowed");

        // other items
        element = parent.find('.form-control, .chosen-choices, input[type!="radio"], textarea');
        if (element.length) {
            element.prop('disabled', true);
        }
    }

    var observer = new MutationObserver(function (mutationsList) {

        for (var mutation in mutationsList) {

            var tr = $(mutationsList[mutation].target).closest('.form-group');

            if (!tr.length) {
                return;
            }

            highlightIfChanged(tr);
        }
    });

    // handle
    // 1) HTML elements change (right now is being used for "plupload" aka file upload)

    $('.cloned div[id*="_filelist"]').each(function () {
        // Start observing the target node for configured mutations
        observer.observe(this, { attributes: false, childList: true });
    });
    $('.original:has(div[id*="_filelist"]) input[type="hidden"]').each(function () {
        // Start observing the target node for configured mutations
        observer.observe(this, { attributes: true, childList: false });
    });
    // 2) changes to hidden elements (.change below does not catch them)
    $('.cloned input:hidden').each(function () {
        // Start observing the target node for configured mutations
        observer.observe(this, { attributes: true, childList: false });
    });
    // 3) changes to Feature tag elements
    $('.cloned #selected-featuretags').each(function () {
        // Start observing the target node for configured mutations
        observer.observe(this, { attributes: false, childList: true });
    });

    // comparison of original and cloned Content on the fly
    $(".cloned").not('div[id*="_filelist"]').change(function () {
        var tr = $(this).closest('.form-group');
        if (!tr.length) {
            return;
        }

        highlightIfChanged(tr);
    });

    function compareAll() {
        var trs = $('.form-group');
        $.each(trs, function (index, tr) {
            disableOriginal($(tr));
            highlightIfChanged($(tr));
        });
    }

    // initial comparison of all fields
    compareAll();
};

function getFieldValue(parent) {
    // textarea (summernote)
    var element = parent.find('div.note-editable');
    if (element.length) {
        return element.html().toLowerCase();
    }

    // files
    element = parent.find('div[id*="_filelist"]');
    if (element.length) {
        element = parent.find('input[type="hidden"]');
        if (element.length) {
            return element.val();
        }
        return null;
    }

    // radio
    element = parent.find('input[type=radio]:checked');
    if (element.length) {
        var values = element.map(function () {
            return $(this).val().toLowerCase();
        }).get();
        return values.join(";");
    }

    // multiple checkboxes
    element = parent.find('input:checkbox');
    if (element.length) {
        var values = element.map(function () {
            var checkbox = $(this);
            return checkbox.is(":checked") ? checkbox.val().toLowerCase() : "";
        }).get();
        return values.join(";");
    }

    //select chosen-choices
    element = parent.find('select.chosen-choices');
    if (element.length) {
        var _val = element.val();
        if (Array.isArray(_val)) {
            return element.val().join(';');
        }
        else {
            return element.val();
        }
    }

    // feature on
    element = parent.find('#selected-featuretags');
    if (element.length) {
        var values = element.map(function () {
            return $(this).text().toLowerCase();
        }).get();
        return values.join(";");
    }

    // hidden values
    element = parent.find('input:hidden');
    if (element.length) {
        var val = element.val();
        // Tag elements may store values delimetered by ",".
        // In this case we sort values to get equal result when different order is used
        if (val && val.indexOf(",") != -1) {
            var arr = val.split(",");
            val = arr.sort().join(",");
        }
        return val && val.toLowerCase ? val.toLowerCase() : val;
    }

    // other items
    element = parent.find('.form-control, .chosen-choices');
    if (element.length) {
        var val = element.val();
        return val && val.toLowerCase ? val.toLowerCase() : val;
    }
}

function highlightIfChanged(tr) {

    var originalValue = getFieldValue(tr.children('.original'));
    var currentValue = getFieldValue(tr.children('.cloned'));
    var same = originalValue === currentValue;

    var changeNotification = tr.find('.control-label');
    if (same) {
        changeNotification.removeClass('changed');
    }
    else {
        changeNotification.addClass('changed');
    }
}

var TagField = function () {

};

TagField.prototype.setSelectedItems = function (selectedItems, selectedSelector) {
    var val = null;
    if (selectedItems) {
        val = selectedItems.map(function (el) {
            return el.text;
        }).join(", ");
    }
    $(selectedSelector).text(val);
};

TagField.prototype.onSelectionEnd = function (hiddenSelector, selectedItems, selectedSelector, allFields) {
    var arr = selectedItems.map(function (el) {
        return el.id;
    });
    var oldArr = $(hiddenSelector).val().split(',');

    var changeTagsElemProp = function (itemValue, nextPropValue) {
        allFields.each(function () {
            var _value = JSON.stringify(itemValue.trim());

            // search for same checkbox value
            $(this).find("input[type='checkbox'][value=" + _value + "]").each(function () {
                $(this)[0].checked = nextPropValue;
            });
        })
    }

    var i;
    var j;
    for (i = 0; i < oldArr.length; i++) {
        var item = oldArr[i].trim();
        var deleteItem = true;

        for (j = 0; j < arr.length; j++) {
            if (arr[j].toString().trim() == item) {
                deleteItem = false;
            }
        }

        if (deleteItem && item) {
            changeTagsElemProp(item, false);
        }
    }

    for (i = 0; i < arr.length; i++) {
        var item = arr[i].toString().trim();
        var addItem = true;

        for (j = 0; j < oldArr.length; j++) {
            if (oldArr[j].trim() == item) {
                addItem = false;
            }
        }

        if (addItem && item) {
            changeTagsElemProp(item, true);
        }
    }

    var alltags = [];
    var remove = [];

    allFields.each(function () {
        $(this).trigger('refresh');

        var checked = $("input[type='checkbox'][name='" + "fieldDef.FieldDefinition." + $(this).attr("id").replace("tag-", "") + "']:checked");
        var unchecked = $("[name='" + "fieldDef.FieldDefinition." + $(this).attr("id").replace("tag-", "") + "']input:checkbox:not(:checked)");

        checked.each(function () {
            var identifier = $(this).attr("data-tag-id");
            var category = $(this).attr("data-tag-group");
            var _title = $(this).attr("value-label");
            var _text = $(this).parent().text();

            alltags.push({ tagId: identifier, cat: category, title: _title, text: _text });
        });

        unchecked.each(function () {
            var identifier = $(this).attr("data-tag-id");

            remove.push({ tagId: identifier });
        });
    });

    $(document).trigger("tagsChanged", { items: alltags, remove: remove });

    $(hiddenSelector).val(arr.join());
    this.setSelectedItems(selectedItems, selectedSelector);
};
