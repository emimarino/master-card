﻿function ShowHideRegionIntVisibilityOptions(dropdown) {
    dropdown.parent().find(".chosen-search").hide();
    const options = dropdown.children();
    const pendingVisibilityState = 2;
    var pendingVisibilityOption = $.grep(options, function (i) {
        return i.value == pendingVisibilityState
    });

    if (pendingVisibilityOption != undefined && pendingVisibilityOption.length > 0) {
        $(".chosen-choices option[value='" + pendingVisibilityState + "']").remove();
        $(".chosen-choices").trigger("chosen:updated");
    }
}
