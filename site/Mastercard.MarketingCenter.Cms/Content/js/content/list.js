﻿var countError = false;

var ContentList = function (getContentUrl, listId, contentTypeId, editContentUrl, colsArray, idColumnName, defaultOrderArray, editLinkIndex, canPreview, canCopy, copyUrl, canArchive, canDelete, canDeleteOnlyDraft, canEdit, canRestore) {
    this.getContentUrl = getContentUrl;
    this.listId = listId;
    this.contentTypeId = contentTypeId;
    this.editContentUrl = editContentUrl;
    this.colsArray = colsArray;
    this.idColumnName = idColumnName;
    this.defaultOrderArray = defaultOrderArray;
    this.editLinkIndex = editLinkIndex;
    this.canPreview = canPreview;
    this.canCopy = canCopy;
    this.copyUrl = copyUrl;
    this.canArchive = canArchive;
    this.canDelete = canDelete;
    this.canDeleteOnlyDraft = canDeleteOnlyDraft;
    this.canEdit = canEdit;
    this.canRestore = canRestore;
};

ContentList.prototype.initialize = function (dataCallback) {
    var that = this;
    var table = $('#contentGrid').DataTable({
        processing: true,
        serverSide: true,
        searching: true,
        pagingType: "simple_numbers",
        language: {
            paginate: {
                next: "&gt;",
                previous: "&lt;"
            }
        },
        columns: that.colsArray,
        columnDefs: [{
            "targets": [0],
            "render": function (data, type, row) {
                return "<td class='text-center'>\
                <div class='btn-group'> \
                    <button type='button' class='btn btn-sm dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false' " + ((!(that.canPreview && row.StatusId && row.StatusId != 6) && !(that.canEdit) && !(that.canCopy) && !(that.canArchive && row.StatusId && row.StatusId != 6) && !(that.canDelete && that.listId || that.canDelete && row.StatusId && row.StatusId != 3 && row.StatusId != 4) && !(that.canDeleteOnlyDraft && row.StatusId && row.StatusId == 4) && !(that.canRestore && row.StatusId && row.StatusId == 6)) ? "disabled = 'disabled'" : "") + "> \
                    <span class='fa fa-cog'></span> \
                    </button> \
                    <ul class='dropdown-menu'>"
                    + (that.canPreview && row.StatusId && row.StatusId != 6 ? "<li><a href='/portal" + (row.FrontEndUrl.indexOf("/") == 0 ? "" : "/") + row.FrontEndUrl + "'>Preview</a></li>" : "") +
                    (that.canEdit ? "<li><a href='" + that.editContentUrl + (that.editContentUrl.indexOf("=") > -1 ? "&" : "?") + "id=" + row[that.idColumnName] + "'>Edit</a></li>" : "") +
                    (that.canCopy ? "<li><a href='" + that.copyUrl + (that.copyUrl.indexOf("=") > -1 ? "&" : "?") + "id=" + row[that.idColumnName] + "'>Copy</a></li>" : "") +
                    (that.canArchive ? (row.StatusId && row.StatusId != 6 && row.CanArchive ? "<li><a class='archive-item' href='#' data-id='" + row[that.idColumnName] + "'>Archive</a></li>" : "") : "") +
                    (that.canDelete ? (that.listId ? ("<li><a class='delete-item' href='#' data-id='" + row[that.idColumnName] + "'>Delete</a></li>") : (row.StatusId && row.StatusId != 3 && row.StatusId != 4 && row.CanDelete ? "<li><a class='delete-item' href='#' data-id='" + row[that.idColumnName] + "'>Delete</a></li>" : "")) : "") +
                    (that.canDeleteOnlyDraft ? (row.StatusId && row.StatusId == 4 && row.CanDeleteOnlyDraft ? "<li><a class='delete-only-draft' href='#' data-id='" + row[that.idColumnName] + "'>Remove Draft</a></li>" : "") : "") +
                    (that.canRestore ? (row.StatusId && row.StatusId == 6 && row.CanRestore ? "<li><a class='restore-item' href='#' data-id='" + row[that.idColumnName] + "'>Restore</a></li>" : "") : "") +
                    (row.OriginalContentItemId ? "<li><a href='" + that.editContentUrl + (that.editContentUrl.indexOf("=") > -1 ? "&" : "?") + "id=" + row[that.idColumnName] + "&showComparison=true'>Compare with Source</a></li>" : "") +
                    "</ul></div></td>";
            }
        }, {
            "targets": that.editLinkIndex,
            "render": function (data, type, row) {
                return (that.canEdit ? "<td class='text-center'><a href='" + that.editContentUrl + (that.editContentUrl.indexOf("=") > -1 ? "&" : "?") + "id=" + row[that.idColumnName] + "'>" + data + "</a></td>" : data);
            }
        }],
        ajax: {
            "url": that.getContentUrl,
            "type": "POST",
            "data": function (data) {
                data.contentTypeId = that.contentTypeId;
                data.listId = that.listId;
                if (dataCallback) {
                    dataCallback(data);
                }
                var innerT = $('#contentGrid');
                /* this was added so html fields containing style tags , don't change the style of the table*/
                innerT.on('draw.dt', function () {
                    innerT.find("style").attr("scoped", "scoped");
                    $.getScript("/admin/Scripts/scoper.js", function () {
                        scopeIt();
                    });
                });
            }, "error": handleError,
        },

        order: that.defaultOrderArray
    });

    $(window).on('resize', function () {
        table.draw();
    });

    function handleError(xhr, textStatus, error) { if (!countError) { countError = true; $("#page-wrapper").append("<div class=\"errr\" style=\"color:red;\">" + error + "</div>"); } }

    return table;
};
