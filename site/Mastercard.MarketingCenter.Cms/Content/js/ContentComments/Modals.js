﻿const idSuccessWrapper = "#mc-comments--success--wrapper";
const idErrorWrapper = "#mc-comments--error--wrapper";
const idCommentsBody = ".mc-comments--body";
const idDateChooser = "[name=dateChooser]";
function validateField(selector, validation, message, labelSelector) {
    var messageElem = $(selector);
    var labelElem = $(labelSelector);

    if (!validation(messageElem)) {
        var errorElement = document.createElement("p");
        errorElement.setAttribute("class", "req-mark error");
        errorElement.innerHTML = message;
        errorElement.style = "color:#ff0000;";
        if (labelElem !== undefined && labelElem.length > 0) {
            var errorMark = document.createElement("span");
            errorElement.setAttribute("class", "req-mark error");
            errorMark.innerHTML = "*";
            labelElem.append(errorMark);
            labelElem.css("color", "#ff0000");
        }
        messageElem.css("border-color", "#ff0000");
        messageElem.after(errorElement);
        return false;
    }
    else {
        var errorElementAtt = messageElem.find(".req-mark.error");
        if (errorElementAtt !== undefined && errorElementAtt.length > 0) {
            errorElementAtt.each(function () {
                $(this).remove();
            });
        }
        messageElem.css("border-color", "#000000");
        labelElem.css("color", "#ff0000");
        return true;
    }
}

function isRequiredFieldValid(selector, message, labelSelector) {
    var validation = function (element) {
        return element.val() !== '' && element.val().length > 0;
    };

    return validateField(selector, validation, message, labelSelector);
}

//this closure is intended to encapsulate the modals related to the result of adding comment
function modalPartialTemp() {
    var backLink;
    var parentSelector;
    function hideComments() {
        $(parentSelector + " #mc-comments--body--wrapper").addClass("hide");
        $(".mc-modal--header:not(.comments)").addClass("hide");
    }

    function showComments() {
        $(parentSelector + " #mc-comments--body--wrapper").removeClass("hide");
        $(".mc-modal--header:not(.comments)").removeClass("hide");
        $(idSuccessWrapper).addClass("hide");
        $(idErrorWrapper).addClass("hide");
    }

    return {
        set: function (_parentSelector, _backLink) {
            backLink = _backLink;

            parentSelector = _parentSelector;
            if (backLink === null) {
                $("#comments-partial .mc-modal--back-link").addClass("hide");
            }
            else {
                $("#comments-partial .mc-modal--back-link a").attr("href", backLink);
            }
        },
        loadError: function (reloadPage, defaultErrorMessage, response) {
            hideComments();

            var wrapper = $(idErrorWrapper);
            wrapper.removeClass("hide");

            // try to parse error response, if it fails then showing a default error
            var errorMessage = getErrorText(response);
            if (!errorMessage) {
                errorMessage = defaultErrorMessage;
            }

            wrapper.find(".mc-text-center").text(errorMessage);
            if (reloadPage) {
                $('.reload-page:not(.reload-bound)', idErrorWrapper).click(function () {
                    $('.reload-page', idErrorWrapper).addClass('reload-bound');
                    window.location.reload();
                });
            }
        },
        loadSuccess: function (reloadPage) {
            hideComments();
            $(idSuccessWrapper).removeClass("hide");
            if (reloadPage) {
                $('.reload-page:not(.reload-bound)', idSuccessWrapper).click(function () {
                    $('.reload-page', idSuccessWrapper).addClass('reload-bound');
                    window.location.reload();
                });
            }
        },
        close: function () {
            $('.mc-modal .close-link').click();
            if (closeAction !== undefined && closeAction !== "undefined" && closeAction !== null) {
                closeAction();
            }
        },
        showComments: function () {
            showComments();
        }
    };
}
//this is an instance of the previous clousure
var modalPartial;

function getErrorText(errorResponse) {
    var data = errorResponse.responseJSON;
    if (!data) {
        try {
            data = $.parseJSON(errorResponse.responseText);
        }
        catch (ex) {
            data = null;
        }
    }

    if (!data) {
        return null;
    }

    return data.ErrorText;
}

function getInitials(name) {
    return name.split(' ').map(function (a) {
        return a[0].toUpperCase();
    }).join('');
}

function ToJavaScriptDate(value, _years, _months, _days, _hours, _minutes) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));

    return dateToString(new Date() - new Date(dt.toString()), _years, _months, _days, _hours, _minutes);
}

//this function is to convert the date offset since the comment was created to a string that represents it
function dateToString(diff, _years, _months, _days, _hours, _minutes) {
    var minute = 1000 * 60;
    var hour = 60;
    var day = 24;
    var minutes = Math.floor(diff / minute);
    var hours = Math.floor(minutes / hour);
    var days = Math.floor(hours / day);
    var months = Math.floor(days / 31);
    var years = Math.floor(months / 12);

    var suffixString = " ago";
    if (years > 0) {
        return years + " " + _years.toLowerCase() + suffixString;
    }
    if (months > 0) {
        return months + " " + _months.toLowerCase() + suffixString;
    }
    if (days > 0) {
        return days + " " + _days.toLowerCase() + suffixString;
    }
    if (hours > 0) {
        return hours + " " + _hours.toLowerCase() + suffixString;
    }

    return minutes + " " + _minutes.toLowerCase() + suffixString;
}

function queueComment(local, _errorMessage, _addNewUrl, _years, _months, _days, _hours, _minutes, _meName, exportQueue) {
    var commentData = {
        contentItemId: local.contentItemId,
        commentBody: local.newComment
    };
    local.queue.unshift(commentData);
    exportQueue.unshift(commentData);
    local.comments.push({
        author: _meName,
        avatar: _meName,
        added: "0 " + _minutes + " ago",
        text: local.newComment,
        new: true
    });

    local.newComment = '';
}

function addRealTimeComment(local, _errorMessage, _addNewUrl, _years, _months, _days, _hours, _minutes, _meName, exportQueue, _isAdmin, _defaultErrorMessage) {
    var commentData = {
        contentItemId: local.contentItemId,
        commentBody: local.newComment,
        isAdmin: _isAdmin
    };

    if (local.newComment === '') {
        local.error = _errorMessage;
        return;
    }

    $.ajax({
        type: "POST",
        url: _addNewUrl,
        data: commentData,
        success: function () {
            local.comments.push({
                author: _meName,
                avatar: _meName,
                added: "0 " + _minutes + " ago",
                text: local.newComment,
                new: true
            });

            local.newComment = '';
        },
        error: function ($xhr) {
            modalPartial.loadError(local.reloadPageOnChange, _defaultErrorMessage, $xhr);
        }
    });
}

function contentCommentModals(_getPagedUrl, _addNewUrl, _contentItemId, _backLink, _years, _months, _days, _hours, _minutes, _meName, _errorMessage, _addMessageHandle, _commentSeed, _isNew, _initalTotalCommentsCount, _isAdmin, _defaultErrorMessage) {
    modalPartial = modalPartialTemp();
    var returnObj = {
        messageQueue: []
    };

    var innerAddMessageHandle = null;
    function setInnerAddMessageHandle(func) {
        innerAddMessageHandle = func;
    }

    //this is related to vue.js creates an instance of an object that included the objects scope and compiles based on the element of the selector supplied
    var modalComments = new Vue({
        el: '#modalComments',
        data: {//declaration of variables used in views scope
            comments: [],
            newComment: '',
            bottom: false,
            contentItemId: _contentItemId,
            backLink: _backLink,
            reloadPageOnChange: _backLink !== '',
            pageNumber: 0,
            totalComments: 1,
            error: null,
            queue: [],
            isFirstLoad: true,
            totalCommentsCount: _initalTotalCommentsCount,
            shouldScroll: false,
            firstScrollDone: true
        },
        methods: { //methods that are used within scope
            customAfterAppearHook: function (el) {
                setTimeout(function () {
                    $(el).remove();
                }, 3000);
                this.shouldScroll = true;
            },
            bottomVisible: function () {
                var elem = document.querySelector(idCommentsBody);
                const visible = elem.clientHeight;
                const pageHeight = elem.scrollHeight;
                const bottomOfPage = visible + elem.parentElement.scrollTop >= pageHeight;
                var scrollPercentage = (elem.parentElement.scrollTop + elem.scrollTop) / (elem.scrollHeight - elem.clientHeight);
                return bottomOfPage || scrollPercentage <= 0.05;
            },
            scrollThis: function () {
                $(idCommentsBody).scrollTop($(idCommentsBody)[0].scrollHeight - $(idCommentsBody)[0].clientHeight);
                $(".slimScrollBar").css('top', ($(".slimScrollRail").height() - $(".slimScrollBar").height()).toString() + 'px');
                this.shouldScroll = false;
            },

            loadComment: function () {
                var local = this;
                if (!_isNew || _isNew === null || _isNew === undefined || _isNew === "undefined") {
                    $.post(_getPagedUrl, {
                        "contentItemId": this.contentItemId,
                        "pageNumber": this.pageNumber, "pageSize": 1
                    }, function (data) {
                        if (data.TotalComments !== 0 && data.ContentComments.length > 0) {
                            var item = data.ContentComments[0];
                            local.totalComments = data.TotalComments;
                            local.pageNumber = local.totalComments > local.pageNumber ? local.pageNumber + 1 : local.pageNumber;
                            local.comments.unshift({
                                author: item.SenderUserName,
                                avatar: getInitials(item.SenderUserName),
                                added: ToJavaScriptDate(item.CreationDate, _years, _months, _days, _hours, _minutes).toLocaleString(),
                                text: item.CommentBody
                            });

                            if (local.bottomVisible() && local.totalComments > local.pageNumber) {
                                local.loadComment();
                            }
                        }
                    });

                    if (local.isFirstLoad && (_commentSeed !== null && _commentSeed !== undefined && _commentSeed !== "undefined")) {
                        local.totalComments += _commentSeed.length;
                        for (var seedIndex in _commentSeed) {
                            var seed = _commentSeed[seedIndex];
                            var commentData = {
                                contentItemId: seed.ContentItemID,
                                commentBody: seed.CommentBody
                            };
                            local.queue.unshift(commentData);
                            returnObj.messageQueue.unshift(commentData);
                            local.comments.unshift({
                                author: _meName,
                                avatar: _meName,
                                added: "0 " + _minutes + " ago",
                                text: seed.CommentBody
                            });
                        }
                    }

                    local.isFirstLoad = false;
                }
                else {
                    if (local.isFirstLoad && (_commentSeed !== null && _commentSeed !== undefined && _commentSeed !== "undefined")) {
                        for (var seedIndex in _commentSeed) {
                            var seed = _commentSeed[seedIndex];
                            var commentData = {
                                contentItemId: seed.ContentItemID,
                                commentBody: seed.CommentBody
                            };
                            local.queue.unshift(commentData);
                            returnObj.messageQueue.unshift(commentData);
                            local.comments.unshift({
                                author: _meName,
                                avatar: _meName,
                                added: "0 " + _minutes,
                                text: seed.CommentBody
                            });
                        }
                    }
                }
            },
            inputFocus: function () { this.shouldScroll; },
            addComment: function () {
                if (this.newComment) {
                    this.shouldScroll = true;
                    this.totalCommentsCount++;
                    if (!(_addMessageHandle === undefined || _addMessageHandle == 'undefined' || _addMessageHandle === null)) {
                        _addMessageHandle(this, _errorMessage, _addNewUrl, _years, _months, _days, _hours, _minutes, _meName, returnObj.messageQueue);
                        return;
                    }
                    if (!(innerAddMessageHandle === undefined || innerAddMessageHandle == 'undefined' || innerAddMessageHandle === null)) {
                        innerAddMessageHandle(this, _errorMessage, _addNewUrl, _years, _months, _days, _hours, _minutes, _meName, returnObj.messageQueue);
                        return;
                    }

                    addRealTimeComment(this, _errorMessage, _addNewUrl, _years, _months, _days, _hours, _minutes, _meName, returnObj.messageQueue, _isAdmin, _defaultErrorMessage);
                }
            }
        },
        mounted: function () {
            var local = this;
            $(idCommentsBody).scroll(function () {
                local.bottom = local.bottomVisible();

                if (local.bottom) {
                    local.loadComment();
                }
            });

            this.loadComment();
        },
        updated: function () {
            if (this.firstScrollDone && !this.bottomVisible()) {
                this.firstScrollDone = false;
                this.shouldScroll = true;
            }
            if (this.shouldScroll) {
                this.scrollThis();
            } else {

                var elem = $(idCommentsBody)[0];
                elem.scrollTop = elem.clientHeight;
            }
        }
    });

    returnObj.setInnerAddMessageHandle = setInnerAddMessageHandle;
    return returnObj;
}

function expirationCommentModals(_messageSelector, _confirmBtnSelector, _extensionCommentUrl, _contentItemId, _ShouldExtend, _errorMessage, _labelSelector) {
    $(_confirmBtnSelector).click(function () {
        if (!isRequiredFieldValid(_messageSelector, _errorMessage, _labelSelector)) {
            return;
        }

        var message = $(_messageSelector).val();
        var commentData = {
            extendMessage: message, contentItemID: _contentItemId, shouldExtend: _ShouldExtend,
            daysToExtend: $(idDateChooser).length > 0 ? $(idDateChooser).val() : null
        };

        $.ajax({
            type: "POST",
            url: _extensionCommentUrl,
            data: commentData,
            success: function () {
                $("#modalExtend " + idSuccessWrapper).removeClass("hide");
                $("#modalExtend .main--modal").addClass("hide");
            },
            error: function () { $(_confirmBtnSelector).trigger('expirationError'); }
        });
    });
}

function bulkExpirationCommentModals(_messageSelector, _confirmBtnSelector, _extensionCommentUrl, _contentItemIds, _ShouldExtend, _labelSelector, _errorMessage) {
    $(_confirmBtnSelector).click(function () {
        if (!isRequiredFieldValid(_messageSelector, _errorMessage, _labelSelector)) {
            return;
        }

        var message = $(_messageSelector).val();
        var commentData = {
            extendMessage: message, contentItemIDs: _contentItemIds, ShouldExtend: _ShouldExtend,
            daysToExtend: $(idDateChooser).length > 0 ? $(idDateChooser).val() : null
        };

        $.ajax({
            url: _extensionCommentUrl,
            data: JSON.stringify(commentData),
            type: "POST",
            contentType: 'application/json;',
            dataType: "html",
            success: function () {
                $("#modalExtend " + idSuccessWrapper).removeClass("hide");
                $("#modalExtend .main--modal").addClass("hide");
            },
            error: function () { $(_confirmBtnSelector).trigger('expirationError'); }
        });
    });
}

function deleteConfirmationModals(_messageSelector, _confirmBtnSelector, _url, selectedIds) {
    $(_confirmBtnSelector).click(function () {
        $.ajax({
            type: "POST",
            url: _url,
            data: JSON.stringify({
                SelectedContentItems: selectedIds
            }),
            contentType: "application/json; charset=utf-8",
            success: function () {
                $("#modalConfirm .main--modal").addClass("in");
                window.location.reload(false);
            },
            error: function () { $(_confirmBtnSelector).trigger('expirationError'); }
        });
    });
}
