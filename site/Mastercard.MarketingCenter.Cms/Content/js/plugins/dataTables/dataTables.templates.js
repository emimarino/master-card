﻿var offer_region_visibility_template = function (data, type, full, meta) {
    return data == "0" ? "No" : data == "1" ? "Yes" : "Pending Review";
};

var checkbox_template = function (data, type, full, meta) {
	var is_checked = data == true ? "checked" : "";
    return is_checked ? "Yes" : "No";
};

var id_template = function (data, type, full, meta) {
    return data.replace("-p", "");
};

var image_template = function (data, type, full, meta) {
    return !data ? "" : "<img width='auto' height='32' src='" + data + "'>"
}

var datetime_template = function(data, type, full, meta) {
    var date = toJSDate(data);
    if (date) {
        return date.toLocaleDateString();
    }
    return "";
};

var fileupload_template = function (data, type, full, meta) {
    return data.split('/')[1];
};

function toJSDate(dateTime) {
    if (dateTime) {

        var dateTime = dateTime.split("T"); //dateTime[0] = date, dateTime[1] = time

        var date = dateTime[0].split("-");
        var time = dateTime[1].split(":");

        //(year, month, day, hours, minutes, seconds, milliseconds)
        return new Date(date[0], date[1] - 1, date[2], time[0], time[1], time[2], 0);
    }
    return null;

}