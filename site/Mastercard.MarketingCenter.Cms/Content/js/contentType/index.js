﻿var ContentType = function (getContentTypesUrl, updateUrl) {
    this.getContentTypesUrl = getContentTypesUrl;
    this.updateUrl = updateUrl;
};

ContentType.prototype.initialize = function () {
    var that = this;
    var table = $('#contenTypesGrid').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": this.getContentTypesUrl,
        columns: [{ data: 'Edit' }, { data: 'Title' }],
        "order": [],
        "columnDefs":
            [{
                "targets": [0],
                "render": function (data, type, row) {
                    return "<td class='text-center'><a href='" + that.updateUrl + "/" + row["ContentTypeId"] + "'><i class='glyphicon glyphicon-pencil'></i></a></td>";
                }
            }]
    });

    $(window).on('resize', function () {
        table.draw();
    });

    return table;
};
