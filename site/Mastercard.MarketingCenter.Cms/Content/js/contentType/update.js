﻿var ContentTypeUpdate = function (deleteUrl, indexUrl) {
    this.deleteUrl = deleteUrl;
    this.indexUrl = indexUrl;
};

ContentTypeUpdate.prototype.initialize = function () {
    $("#errors").hide();
    var that = this;
    $("#delete").on("click", function (e) {
        e.preventDefault();

        $.post(that.deleteUrl, function (data) {
            if (!data.Ok) {
                $("#errors").html(data.Message);
                $("#errors").show();
            } else {
                window.location.href = that.indexUrl;
            }
        });
    });
};
