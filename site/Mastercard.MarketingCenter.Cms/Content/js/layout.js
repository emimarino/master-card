﻿var _siteBaseUrl;
var _siteAdminRelativeUrl
var _sitePortalRelativeUrl;

var LayoutScript = function (siteBaseUrl, siteAdminRelativeUrl, sitePortalRelativeUrl) {

    _siteBaseUrl = siteBaseUrl;
    _siteAdminRelativeUrl = siteAdminRelativeUrl;
    _sitePortalRelativeUrl = sitePortalRelativeUrl;
};

LayoutScript.prototype.initialize = function () {
    SetSlamAuthenticationAndPreviewModeCookieIfPublisher();
    SetBreadCrums();
}

function SetSlamAuthenticationAndPreviewModeCookieIfPublisher() {
    var authUrl = siteBaseUrl + _sitePortalRelativeUrl + '/admin/SetSlamAuthenticationAndPreviewModeCookieIfPublisher';

    $.ajax({
        url: authUrl,
        method: 'GET',
        cache: false
    });
}

function SetBreadCrums() {
    $.ajax({
        url: _siteAdminRelativeUrl + '/home/BreadCrums',
        method: 'GET',
        cache: false
    })
        .done(function (html) {

            if (html == null) { return; }

            var count = html.length;
            $.each(html, function (idx, element) {
                var path = _siteAdminRelativeUrl + "/" + element.Controller + "/" + element.Action

                if (element.Id != null || element.Id != undefined)
                    path = path + "/" + element.Id;

                if (element.querystring != null || element.querystring != undefined)
                    path = path + "?" + element.querystring;

                if (idx == count - 1) {
                    var link = $('<li> <strong>' + element.Name + '</strong> </li>');
                    link.addClass("active");
                }
                else {
                    var link = $('<li> <a href="' + path + '" >' + element.Name + '</a></li>');
                }

                $(".breadcrumb").append(link);

            });
        });
}

$(document).ready(function () {
    $('.region-selector select').selectric({
        onChange: function () {
            document.forms["regionSelector"].submit();
            $('.region-selector .selectric-items').remove();
        }
    });
});