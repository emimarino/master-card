﻿function createValidator(formSelector, elementToValidateSelector, validateFunction, message) {
    $(formSelector).submit(function (e) {
        if (!validateFunction($(elementToValidateSelector))) {
            return confirm(message);
        }
        return true;
    });
}

function applyDateIsInPastValidator(formSelector, elementToValidateSelector, message) {
    createValidator(
        formSelector,
        elementToValidateSelector,
        function (sel) {
            return new Date($(sel).val()) == "Invalid Date" || new Date($(sel).val()) >= new Date(Date.now()).setHours(0, 0, 0, 0);
        },
        message
    );
}

function ValidateAuthenticationValidator() {
    $.get('/admin/IsAuthenticated', function (isAuthenticated) {
        if (!isAuthenticated) {
            document.location = '/portal';
            return false;
        }
        return true;
    });
}
