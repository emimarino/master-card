﻿var countError = false;

var IssuersList = function (getContentUrl, editContentUrl, processorId, returnUrl) {
    this.getContentUrl = getContentUrl;
    this.editContentUrl = editContentUrl;
    this.processorId = processorId;
    this.returnUrl = returnUrl;
};

IssuersList.prototype.initialize = function (fieldsToHide) {
    var isHidden = function (field) {
        var hide = false;
        if (fieldsToHide) {
            for (var i = 0; i < fieldsToHide.length; i++) {
                if (fieldsToHide[i] === field) {
                    hide = true;
                    break;
                }
            }
        }
        return hide;
    };

    this._columns = [
        { data: "Actions", title: "Actions", searchable: false, orderable: false },
        { "data": "IssuerId", "title": "Item ID", visible: false },
        { "data": "Title", "title": "Title" },
        { "data": "Processor", "title": "Processor" },
        { "data": "DomainName", "title": "Domain Name" },
        { 'data': 'DateCreated', 'title': 'Created Date', render: datetime_template, 'searchable': 'false', 'orderable': 'true' },
        { 'data': 'ModifiedDate', 'title': 'Modified Date', render: datetime_template, 'searchable': 'false', 'orderable': 'true' }]
    if (checkbox_template && !isHidden("HasOrdered")) {
        this._columns.push({ "data": "HasOrdered", "title": "Has Ordered", render: checkbox_template, visible: !isHidden("HasOrdered") });
    }

    var that = this;
    var table = $('#contentGrid').DataTable({
        processing: true,
        serverSide: true,
        searching: true,
        pagingType: "simple_numbers",
        language: { paginate: { next: ">", previous: "&lt;" } },
        columns: this._columns,
        columnDefs: [
            {
                "targets": [0],
                "render": function (data, type, row) {
                    return "<td class='text-center'>\
                <div class='btn-group'> \
                    <button type='button' class='btn btn-sm dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> \
                    <span class='fa fa-cog'></span> \
                    </button> \
                    <ul class='dropdown-menu'> \
                   <li><a href='" + that.editContentUrl + "&id=" + row["IssuerId"] + "&returnUrl=" + that.returnUrl + "?id=" + that.processorId + "'>Edit</a></li> \
                   <li><a class='delete-issuer' href='#' data-id='" + row.IssuerId + "'>Delete</a></li> \
                </ul></div></td>";
                }
            },
            {
                "targets": [2],
                "render": function (data, type, row) {
                    return "<td class='text-center'><a href='" + that.editContentUrl + "&id=" + row["IssuerId"] + "&returnUrl=" + that.returnUrl + "?id=" + that.processorId + "'>" + row["Title"] + "</a></td>";
                }
            }
        ],
        ajax: {
            "url": that.getContentUrl,
            "type": "POST",
            "data": function (data) {
                data.processorId = that.processorId;
            }, "error": handleError
        },
        order: [[2, "desc"]]
    });

    $(window).on('resize', function () {
        table.draw();
    });
    function handleError(xhr, textStatus, error) { if (!countError) { countError = true; $("#page-wrapper").append("<div class=\"errr\" style=\"color:red;\">" + error + "</div>"); } }

    return table;
};
