﻿$(document).ready(function () {
    var regionCheckboxList = $('#ContentPlaceHolder1_cblReportRegion input');
    isAllRegionNonSelected(null, regionCheckboxList);
    regionCheckboxList.on('change', function (e) {
        var element = this;
        if (isAllRegionNonSelected(element, regionCheckboxList) || !regionCheckboxList[0].checked) {
            regionCheckboxList[0].checked = false;
        }
        else {
            DeselectAll(regionCheckboxList);
            regionCheckboxList[0].checked = true;
        }

        if (IsAllCheckboxesDisabled(regionCheckboxList)) {
            regionCheckboxList[0].checked = true;
        }
    });
});

function isAllRegionNonSelected(element, list) {
    if (element == null) {
        return true;
    }
    else {
        return element.checked && !element.disabled && list.index(element) !== 0;
    }
}

function IsAllCheckboxesDisabled(list) {
    return list.toArray().filter(function (i) {
        return i.checked;
    }).length == 0;
}

function DeselectAll(list) {
    list.each(function (i) {
        var element = this;
        element.checked = false;
    });
}
