﻿function displayLoader(obj, iconPath) {
    $(obj).append('<div id="overlay"></div>');
    $(obj).append('<div id="loader"></div>');
    let overlay = $("#overlay");
    overlay.css({
        "position": "fixed",
        "background-color": "white",
        "opacity": ".5",
        "width": "100%",
        "height": "100%",
        "top": "0",
        "left": "0",
        "z-index": "8000"
    });
    let loader = $("#loader");
    loader.css({
        "position": "fixed",
        "backgound": "white",
        "top": "30%",
        "left": "50%",
        "z-index": "9000"
    });
    loader.prepend('<img src="' + iconPath + '" alt="Loading..." />');
};