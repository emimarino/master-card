﻿var countError = false;

var ContentList = function (getContentUrl, editContentUrl, colsArray, idColumnName, editLinkIndex, divIdentifier, defaultOrderArray) {
    this.getContentUrl = getContentUrl;
    this.editContentUrl = editContentUrl;
    this.colsArray = colsArray;
    this.idColumnName = idColumnName;
    this.editLinkIndex = editLinkIndex;
    this.divIdentifier = divIdentifier;
    this.defaultOrderArray = defaultOrderArray;
};

ContentList.prototype.initialize = function (dataCallback) {

    var that = this;
    var table = $(that.divIdentifier).DataTable({
        processing: true,
        serverSide: true,
        searching: false,
        pagingType: "simple_numbers",
        language: {
            paginate: {
                next: "&gt;",
                previous: "&lt;"
            }
        },
        columnDefs: [{
            "targets": [1],
            "render": function (data, type, row) {
                return ("<td class='text-center'><a href='" + that.editContentUrl + (that.editContentUrl.indexOf("=") > -1 ? "&" : "?") + "id=" + row[that.idColumnName] + "'>" + data + "</a></td>");
            }
        }],
        columns: that.colsArray,
        ajax: {
            "url": that.getContentUrl,
            "type": "POST",
            "data": function (data) {
                if (dataCallback) {
                    dataCallback(data);
                }
            },
            order: that.defaultOrderArray,
            "error": handleError,
        }
    });

    $(window).on('resize', function () {
        table.draw();
    });

    function handleError(xhr, textStatus, error) {
        if (!countError) {
            countError = true;
            $("#page-wrapper").append("<div class=\"errr\" style=\"color:red;\">" + error + "</div>");
        }
    }

    return table;
};

function loadContent(
    getContentUrl,
    editContentUrl,
    defaultOrderArray,
    colsArray,
    gridSelector,
    paramElementSelector,
    onContentInitialize) {

    var contentItemList = new ContentList(
        getContentUrl,
        editContentUrl,
        colsArray,
        "ContentItemId",
        "ContentItemId",
        gridSelector,
        defaultOrderArray);

    var table = contentItemList.initialize(function (data) {
        if (onContentInitialize) {
            onContentInitialize(data);
        }
    });

    if (paramElementSelector) {
        $(paramElementSelector).on('change', function () {
            table.ajax.reload();
        });
    }
}
