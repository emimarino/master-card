const idFilterSelector = ".mcew-assets--filters--selector";
const idResultItem = ".mcew-assets--results-item";
const idCheckboxInput = '.mcew-assets--results .mcew-assets--results-item--checkbox input[type="checkbox"]';
$(function () {
    $('.mcew-assets--filters label').click(function (e) {
        $('.mcew-assets--filters label').not(this).removeClass('active__view');
        $(this).addClass('active__view');
    });
    var filterSelector = $(idFilterSelector);
    $('.current, .options > li', filterSelector).click(function (e) {
        $(this).closest(idFilterSelector).toggleClass('hover');
        if ($(this).is('li')) {
            filterSelector.find('li').not(this).removeClass('selected');
            $(this).addClass('selected');
            filterSelector.find('.current').html($(this).html());
        }
    });
    var selectorOutEvent = null;
    $(idFilterSelector).hover(function () {
        clearTimeout(selectorOutEvent);
    },
        function (e) {
            var $this = $(this);
            clearTimeout(selectorOutEvent);
            selectorOutEvent = setTimeout(function () {
                $this.removeClass('hover');
            }, 500);
        }
    );
    $(idCheckboxInput).click(function (e) {
        var $this = $(this);
        if ($this.prop('checked')) {
            $this.closest(idResultItem).addClass('active__view');
        } else {
            $this.closest(idResultItem).removeClass('active__view');
        }
    });
    $('.mcew-assets--results-selector input[type="checkbox"]').click(function (e) {
        var $this = $(this);
        $(idCheckboxInput).prop('checked', $this.prop('checked'));
        if ($this.prop('checked')) {
            $(idResultItem).addClass('active__view');
        } else {
            $(idResultItem).removeClass('active__view');
        }
    });
    $('.assets-filter-link').click(function (e) {
        e.preventDefault();

        var modalTagId = $(this).attr('href');
        var $modal = $(modalTagId);
        if ($modal.length) {
            $modal.show();
            setTimeout(function () {
                $modal.addClass('show');

                var stopClickInside = function (e) {
                    e.stopPropagation();
                }
                var hideOnOutsideClick = function () {
                    $modal.removeClass('show').hide();
                    setTimeout(function () {
                        $(window).unbind("click", hideOnOutsideClick);
                        $(".mcew-assets--filter-modal--container").unbind("click", stopClickInside);
                    }, 200);
                }
                $(window).click(hideOnOutsideClick);
                $(".mcew-assets--filter-modal--container").click(stopClickInside);
            }, 100);
        }
    });
    $('.mcew-assets--filter-modal .mcew-action-button').click(function (e) {
        e.preventDefault();
        var item = $('input[name="filterOptionModal"]:checked');
        filterOnExpiration(item.get(0));
        $(this).closest('.mcew-assets--filter-modal').removeClass('show').hide();
    })
});
