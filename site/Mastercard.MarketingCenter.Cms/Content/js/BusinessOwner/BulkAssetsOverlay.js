﻿const idSelectAll = "#selectAll";
const idBulkCheckbox = ".mcew-assets--results-item--checkbox input";
const idButtonInput = ".asset-list--item-buttons input";
const idButtonMark = ".asset-list--item-buttons .mark";
const idSelectionOverlay = "#selectionOverlay";

function bulkScope(_extendUrl, _userName, redirectAuthentication) {
    $(idSelectAll).click(function () {
        if ($(this).is(":checked")) {
            $(".mcew-assets--results " + idBulkCheckbox).each(function (a, b) {
                b.checked = true;
                $(b).parent().find(".tick").removeClass("hide");
            });
        }
        else {
            $(".mcew-assets--results " + idBulkCheckbox).each(function (a, b) {
                b.checked = false;
                $(b).parent().find(".tick").addClass("hide");
            });
        }
        $(".mcew-assets--results-item input").first().trigger("change");
    });

    //begin event handling for checkbox in bulk select
    $(idBulkCheckbox).on("change", function () {
        $(".assetsCounter #selectedAssetsCounter").html($(".mcew-assets--results-item input:checked").length);
        if ($(this).is(":checked")) {
            $(this).parent().find(".tick, .line").removeClass("hide");
        }
        else {
            $(this).parent().find(".tick, .line").addClass("hide");
        }
        if ($(".mcew-assets--results-item input:not(:checked)").length > 0) {
            $(idButtonInput)[0].checked = true;
            $(idButtonMark).removeClass("tick");
            $(idButtonMark).addClass("line");
            $(idSelectAll).prop("checked", false);
        } else {
            $(idButtonInput)[0].checked = true;
            $(idButtonMark).removeClass("line");
            $(idButtonMark).addClass("tick");
            $(idSelectAll).prop("checked", true);
        }
        $(idButtonMark).removeClass("hide");
    });

    $(".mcew-assets--results-item input").on("change", function () {
        if ($(".mcew-assets--results-item input:checked").length > 0) {
            $(idSelectionOverlay).removeClass("hide");
        } else {
            $(idSelectionOverlay).addClass("hide");
        }
    })

    $(".selectionOverlay--checkbox " + idBulkCheckbox).on("change", function () {
        if ($(idBulkCheckbox + ":not(:checked)").length > 0) {
            $(idBulkCheckbox).each(function (a, b) {
                b.checked = true;
                $(b).parent().find(".tick").removeClass("hide");
                $(b).closest('.mcew-assets--results-item').addClass('active__view');
            });
            $(idBulkCheckbox).first().trigger("change");
        } else {
            $(idBulkCheckbox).each(function (a, b) {
                b.checked = false;
                $(b).parent().find(".tick").addClass("hide");
                $(b).closest('.mcew-assets--results-item').removeClass('active__view');
            });
            $(idBulkCheckbox).first().trigger("change");
        }
    })
    //end event ahndling for checkbox in bulk select

    $("[data-modal=#extendBulk]").click(function () {
        var modalsParentSelector = "#expireModal";
        var allData = {
            contentItemIDs: $(idBulkCheckbox + "[data-Content-id]:checked").map(function (a) { return this.getAttribute("data-Content-id"); }).get(),
            shouldExtend: $(this).attr("data-Should-Extend"),
            userFullName: _userName
        };
        $('body').append($(modalsParentSelector));
        $.ajax({
            url: _extendUrl,
            data: JSON.stringify(allData),
            type: "POST",
            contentType: 'application/json;',
            success: function (data2, textStatus, request) {
                {
                    redirectAuthentication(request);
                    $(modalsParentSelector).html(data2);
                    $(modalsParentSelector).find(".mc-modal").addClass('in');
                    $(modalsParentSelector).find('.mc-modal .close-link').click(function (e) {
                        e.preventDefault();
                        $(this).closest('.mc-modal').removeClass('in');
                        $(idSelectionOverlay).addClass("hide");
                        $(idBulkCheckbox).each(function (a, b) {
                            b.checked = false;
                            $(b).parent().find(".tick").addClass("hide");
                        });
                        $(idBulkCheckbox).first().trigger("change");
                        window.location.reload();
                    });
                }
            },
            dataType: "html"
        });
    });
}
