﻿var SearchResults = function (editListItemUrl, editContentItemUrl, getResultsUrl, searchTerm) {
    this.editListItemUrl = editListItemUrl;
    this.editContentItemUrl = editContentItemUrl;
    this.getResultsUrl = getResultsUrl;
    this.searchTerm = searchTerm;
};

SearchResults.prototype.initialize = function () {
    var that = this;
    var table = $('#searchResults').DataTable({
        processing: true,
        serverSide: true,
        searching: false,
        pagingType: "simple_numbers",
        language: { paginate: { next: ">", previous: "&lt;" } },
        columns: [
            { "data": "Actions", "title": "Actions", orderable: false },
            { "data": "ItemType", "title": "Item Type" },
            { "data": "Id", "title": "Item ID" },
            { "data": "Title", "title": "Title" },
            { 'data': 'CreatedDate', 'title': 'Created Date', render: datetime_template, 'searchable': 'false', 'orderable': 'true' },
            { 'data': 'ModifiedDate', 'title': 'Modified Date', render: datetime_template, 'searchable': 'false', 'orderable': 'true' },
            { "data": "Rank", "visible": false, orderable: true }],
        columnDefs: [
            {
                "targets": [0],
                "render": function (data, type, row) {
                    var editUrl = that.editContentItemUrl + "?id=" + row["Id"];
                    if (row["ListId"]) {
                        editUrl = that.editListItemUrl + "?id=" + row["Id"] + "&listId=" + row["ListId"];
                    }

                    return "<td class='text-center'>\
                <div class='btn-group'> \
                    <button type='button' class='btn btn-sm dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'> \
                    <span class='fa fa-cog'></span> \
                    </button> \
                    <ul class='dropdown-menu'> \
                   <li> <a href='" + editUrl + "'>Edit</a></li>" +
                        (row["FrontEndUrl"] ? "<li><a href='" + row["FrontEndUrl"] + "' target='_blank'>View in Portal</a></li>" : "") +
                        "</ul></div></td>";
                }
            },
            {
                "targets": [3],
                "render": function (data, type, row) {
                    if (row["ListId"]) {
                        return "<td class='text-center'><a href='" + that.editListItemUrl + "?id=" + row["Id"] + "&listId=" + row["ListId"] + "'>" + row["Title"] + "</a></td>";
                    }

                    return "<td class='text-center'><a href='" + that.editContentItemUrl + "?id=" + row["Id"] + "'>" + row["Title"] + "</a></td>";
                }
            }
        ],
        ajax: {
            "url": that.getResultsUrl,
            "type": "POST",
            "data": function (data) {
                data.searchTerm = that.searchTerm;
            }
        },
        order: [[6, "desc"]]
    });

    $(window).on('resize', function () {
        table.draw();
    });
};
