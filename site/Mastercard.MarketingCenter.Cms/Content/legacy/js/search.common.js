﻿function trim(inputString) {
    if (typeof inputString != "string") {
        return inputString;
    }

    return inputString
        .replace(/^(\s|\n|\r)*((.|\n|\r)*?)(\s|\n|\r)*$/g, "$2")
        .replace(/(\s(?!(\n|\r))(?=\s))+/g, "")
        .replace(/(\n|\r)+/g, "\n\r")
        .replace(/(\n|\r)\s/g, "$1")
        .replace(/\s(\n|\r)/g, "$1");
}

function disable(btnid) {
    var link = document.getElementById(btnid);
    link.disabled = true;
    link.className = "grey_bg_disable none_input";
    link.parentNode.className = "bg_grey_disable bg_btn_nomar";
}

/* Validation Functions */

var dtCh = "/";
var minYear = 1900;
var maxYear = 2100;

function isInteger(s) {
    var i;
    for (i = 0; i < s.length; i++) {
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) {
            return false;
        }
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag) {
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++) {
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) {
            returnString += c;
        }
    }
    return returnString;
}

function daysInFebruary(year) {
    // February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) ? 29 : 28);
}

function DaysArray(n) {
    for (var i = 1; i <= n; i++) {
        this[i] = 31
        if (i == 4 || i == 6 || i == 9 || i == 11) {
            this[i] = 30
        }
        if (i == 2) {
            this[i] = 29
        }
    }
    return this
}

function isDate(dtStr) {
    var daysInMonth = DaysArray(12)
    var pos1 = dtStr.indexOf(dtCh)
    var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
    var strMonth = dtStr.substring(0, pos1)
    var strDay = dtStr.substring(pos1 + 1, pos2)
    var strYear = dtStr.substring(pos2 + 1)
    var strYr = strYear
    if (strDay.charAt(0) == "0" && strDay.length > 1) {
        strDay = strDay.substring(1)
    }
    if (strMonth.charAt(0) == "0" && strMonth.length > 1) {
        strMonth = strMonth.substring(1)
    }
    for (var i = 1; i <= 3; i++) {
        if (strYr.charAt(0) == "0" && strYr.length > 1) {
            strYr = strYr.substring(1)
        }
    }
    var month = parseInt(strMonth)
    var day = parseInt(strDay)
    var year = parseInt(strYr)
    if (pos1 == -1 || pos2 == -1) {
        alert("The date format should be : mm/dd/yyyy")
        return false;
    }
    if (strMonth.length < 1 || month < 1 || month > 12) {
        alert("Please enter a valid month")
        return false;
    }
    if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
        alert("Please enter a valid day")
        return false
    }
    if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
        alert("Please enter a valid 4 digit year between " + minYear + " and " + maxYear)
        return false;
    }
    if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || !isInteger(stripCharsInBag(dtStr, dtCh))) {
        alert("Please enter a valid date")
        return false;
    }
    return true;
}

function ValidateForm(txtFromDate, txtToDate) {
    var _txtFromDate = document.getElementById(txtFromDate);
    var _txtToDate = document.getElementById(txtToDate);

    if (!isDate(_txtFromDate.value)) {
        _txtFromDate.focus()
        return false;
    }
    if (!isDate(_txtToDate.value)) {
        _txtToDate.focus()
        return false;
    }
    var dFrom = new Date(_txtFromDate.value);
    var dTo = new Date(_txtToDate.value);
    if (dFrom > dTo) {
        alert("Start date must be less then End date")
        _txtFromDate.focus()
        return false;
    }
    return true;
}

$(document).ready(function () {
    var hasRegionOptions = function (input) {
        var hasOptions = false;
        var role = $(input).parent().children('label').text();
        if (role === 'Mastercard Admin' || role === 'Content Producer') {
            hasOptions = true;
        }
        return hasOptions;
    };

    var checkDevAdminRegionOptions = function (input) {
        var role = $(input).parent().children('label').text();
        if (role === 'Developer Admin') {
            $('.role_sub_options input[type="checkbox"]').prop('checked', true);
        }
    };

    var resetRegionOptions = function (roleSubOptions) {
        roleSubOptions.find('input[type="checkbox"]').prop('checked', false);
        roleSubOptions.find('input[value="' + $('#current-region-name').val() + ' Region Admin"]').prop('checked', true);
        roleSubOptions.hide();
    };

    if ($('.role_sub_options li').length > 0) {
        var checkedInput = $('input[type="radio"]:checked');
        var roleSubOptions = $('.role_sub_options');
        if (checkedInput.length > 0) {
            if (hasRegionOptions(checkedInput)) {
                $('input[type="radio"]:checked').parent().append(roleSubOptions);
                roleSubOptions.show();
            } else {
                resetRegionOptions(roleSubOptions);
            }
        }
        $('input[type="radio"]').click(function () {
            if (hasRegionOptions(this)) {
                $(this).parent().append(roleSubOptions);
                roleSubOptions.show();
            } else {
                resetRegionOptions(roleSubOptions);
                checkDevAdminRegionOptions(this);
            }
        });
    }
});
