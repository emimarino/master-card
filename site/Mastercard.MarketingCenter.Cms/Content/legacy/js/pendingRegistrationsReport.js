﻿function onRequestStart(sender, args) {
    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
        args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
        args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 ||
        args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
        args.set_enableAjax(false);
    }
}

var column = null;
function MenuShowing(sender, args) {
    if (column == null) {
        return;
    }
    var menu = sender;
    var items = menu.get_items();
    if (column.get_dataType() == "System.String") {
        var i = 0;
        while (i < items.get_count()) {
            var item = items.getItem(i);
            if (!(items.getItem(i).get_value() in { 'NoFilter': '', 'Contains': '', 'NotIsEmpty': '', 'IsEmpty': '', 'NotEqualTo': '', 'EqualTo': '' })) {
                if (item != null) {
                    item.set_visible(false);
                }
            }
            else {
                if (item != null) {
                    item.set_visible(true);
                }
            }
            i++;
        }
    }
    if (column.get_dataType() == "System.Int64") {
        var j = 0; while (j < items.get_count()) {
            var item = items.getItem(j);
            if (!(items.getItem(j).get_value() in { 'NoFilter': '', 'GreaterThan': '', 'LessThan': '', 'NotEqualTo': '', 'EqualTo': '' })) {
                if (item != null) {
                    item.set_visible(false);
                }
            }
            else {
                if (item != null) {
                    item.set_visible(true);
                }
            }
            j++;
        }
    }
    column = null;
}
function filterMenuShowing(sender, eventArgs) {
    column = eventArgs.get_column();
}
