﻿const tagComponent = Vue.component('tag-component', {
    template: '#tag-component',
    props: {
        items: Array,
        node: Object,
        parent: String,
        isroot: {
            type: Boolean,
            default: true
        },
        parentdisabled: {
            type: Boolean,
            default: false
        }
    },
    data: function () {
        return {
            showChildren: [],
            checked: false,
            selectedItems: []
        };
    },
    mounted: function () {
        var self = this;
        self.items.forEach(function (i) {
            if (i.isChecked) {
                self.toggleSelectedItems(i.value);
                self.__updateCheckedFromParentToChildren(i);
            }
        });
    },
    methods: {
        anyChildrenChecked: function (itemId) {
            return this.$refs.tagChildren && (this.$refs.tagChildren.filter(function (child) {
                return child.parent == itemId && (child.selectedItems.length > 0 || child.anyChildrenChecked());
            }).length > 0);
        },
        allChildrenChecked: function (itemId) {
            return this.$refs.tagChildren && (this.$refs.tagChildren.filter(function (child) {
                return child.parent == itemId && child.selectedItems.length == child.items.length;
            }).length > 0);
        },
        toggle: function (item) {
            this.updateCheck(item);
        },
        toggleShowChildren: function (i) {
            const index = this.showChildren.indexOf(i);
            if (index !== -1) {
                this.showChildren.splice(index, 1);
            }
            else {
                this.showChildren.push(i);
            }
        },
        toggleSelectedItems: function (i) {
            const index = this.selectedItems.indexOf(i);
            if (index !== -1) {
                this.selectedItems.splice(index, 1);
            }
            else {
                this.selectedItems.push(i);
            }
        },
        updateCheck: function (item, preventParentUpdate) {
            if (preventParentUpdate === undefined) {
                preventParentUpdate = false;
            }

            this.toggleSelectedItems(item.value);
            if (this.__isSelectedItem(item.value) == this.__isShowChildren(item.value)) {
                this.toggleShowChildren(item.value);
            }

            this.__updateCheckedFromParentToChildren(item);
            if (!preventParentUpdate) {
                this.__notifyFromChildrenToParent();
            }
        },
        notifyUpdateFromChildren: function (item) {
            this.__updateSelectedArrayFromChildrenToParent(item.value);
            this.__notifyFromChildrenToParent();
        },

        /* private methods */
        __updateSelectedArrayFromChildrenToParent: function (itemId) {
            const index = this.selectedItems.indexOf(itemId);
            if (index !== -1) {
                this.selectedItems.splice(index, 1);
            }
            else if (this.allChildrenChecked(itemId)) {
                this.selectedItems.push(itemId);
            }
        },
        __updateCheckedFromParentToChildren: function (item) {
            if (!this.__hasChildren()) {
                return;
            }

            var self = this;
            this.$refs.tagChildren.forEach(function (child) {
                child.items.forEach(function (i) {
                    if (child.parent == item.value && child.__isSelectedItem(i.value) != self.__isSelectedItem(item.value)) {
                        return child.updateCheck(i, true);
                    }
                });
            });
        },
        __notifyFromChildrenToParent: function () {
            this.$emit('update');
        },
        __hasChildren: function () {
            if (!this.$refs.tagChildren) {
                return false;
            }
            if (this.$refs.tagChildren.length === 0) {
                return false;
            }

            return true;
        },
        __isSelectedItem: function (i) {
            return this.selectedItems.indexOf(i) !== -1;
        },
        __isShowChildren: function (i) {
            return this.showChildren.indexOf(i) !== -1;
        }
    },
    computed: {
        getSelectedIds: function () {
            let ids = [];
            ids = ids.concat(this.selectedItems);
            if (this.$refs.tagChildren) {
                this.$refs.tagChildren.forEach(function (child) {
                    if (ids.indexOf(child.parent) === -1) {
                        const _ids = child.getSelectedIds;
                        if (_ids) {
                            ids = ids.concat(_ids);
                        }
                    }
                });
            }

            return ids.join(',');
        }
    }
});

const tag = new Vue({
    el: '#tagComponent',
    components: {
        'tagComponent': tagComponent
    },
    data: function () {
        return {
            toggleButtonName: 'Select All',
            show: true,
            selectedIds: []
        };
    },
    methods: {
        getSelectedIds: function () {
            var ids = [];
            if (this.$refs.tagChildren) {
                ids = ids.concat(this.$refs.tagChildren.getSelectedIds);
            }
            this.selectedIds = ids;
        }
    },
    mounted: function () {
        this.getSelectedIds();
    },
    updated: function () {
        $(document).trigger('updateTagComponentControl');
    }
});
