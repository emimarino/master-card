﻿const locationTagComponent = Vue.component('location-tag', {
    template: '#location-tag',
    props: {
        node: Object,
        index: Number,
        forceshowchildren: {
            type: Boolean,
            default: false
        },
    },
    data: function () {
        return {
            showChildren: true,
            checked: false,
            selected: []
        };
    },
    mounted: function () {
        if (this.node.Status) {
            this.toggle();
        }

        this.updateShowChildren();
    },
    methods: {
        anyChildrenChecked: function () {
            return this.selected.length > 0 || (this.$refs.locationChildren && this.$refs.locationChildren.reduce(function (item) {
                if (!item || typeof item === "undefined") {
                    return 0;
                }
                return item.anyChildrenChecked();
            }) > 0);
        },
        toggle: function () {
            this.updateCheck(!this.checked);
        },
        toggleShowChildren: function () {
            this.showChildren = !this.showChildren;
        },
        updateCheck: function (checked, preventParentUpdate) {
            if (preventParentUpdate === undefined) {
                preventParentUpdate = false;
            }
            this.checked = checked;
            this.updateShowChildren();

            this.__updateCheckedFromParentToChildren(checked);
            this.__updateSelectedArray(checked);

            if (!preventParentUpdate) {
                this.__notifyFromChildrenToParent(this.node.EventLocationId, checked);
            }
        },
        updateShowChildren: function () {
            if (this.forceshowchildren || (this.anyChildrenChecked() && !this.checked)) {
                this.showChildren = true;
            }
            else {
                this.showChildren = false;
            }

            if (this.$refs.locationChildren) {
                this.$refs.locationChildren.forEach(function (x) {
                    x.updateShowChildren();
                });
            }
        },
        notifyUpdateFromChildren: function (locationId, checked) {
            this.__updateSelectedArrayFromChildrenToParent(locationId, checked);
            this.__updateCheckedFromChildrenToParents(checked);
            this.__notifyFromChildrenToParent(this.node.EventLocationId, checked);
        },
        getSelectedIds: function () {
            let ids = [];

            if (this.checked) {
                return [this.node.EventLocationId];
            }
            if (this.node.Children.length === 0) {
                return null;
            }

            this.$refs.locationChildren.forEach(function (x) {
                const _ids = x.getSelectedIds();
                if (_ids) {
                    ids = ids.concat(_ids);
                }
            });

            return ids;
        },

        /* private methods */

        __updateSelectedArray: function (checked) {
            if (checked) {
                this.selected = this.node.Children.map(function (x) {
                    return x.EventLocationId;
                });
            } else {
                this.selected = [];
            }
        },
        __updateSelectedArrayFromChildrenToParent: function (locationId, checked) {
            if (checked) {
                if (this.selected.indexOf(locationId) === -1) {
                    this.selected.push(locationId);
                }
            } else {
                if (this.selected.indexOf(locationId) !== -1) {
                    this.selected = this.selected.filter(function (x) {
                        return x !== locationId;
                    });
                    this.checked = checked;
                }
            }
        },
        __updateCheckedFromParentToChildren: function (checked) {
            if (!this.__hasChildren()) {
                return;
            }

            this.$refs.locationChildren.forEach(function (child) {
                return child.updateCheck(checked, true);
            });
        },
        __updateCheckedFromChildrenToParents: function (checked) {
            if (!this.__hasChildren()) {
                return;
            }
            if (!checked) {
                this.checked = false;
            }
        },
        __notifyFromChildrenToParent: function (locationId, checked) {
            this.$emit('update', locationId, checked);
        },
        __hasChildren: function () {
            if (!this.$refs.locationChildren) {
                return false;
            }
            if (this.$refs.locationChildren.length === 0) {
                return false;
            }

            return true;
        }
    }
});

const locationTags = new Vue({
    el: '#locationTags',
    components: {
        'locationTagComponent': locationTagComponent
    },
    data: function () {
        return {
            items: [],
            toggleButtonName: 'Select All',
            show: true,
            selectedIds: []
        };
    },
    methods: {
        toggle: function () {
            if (this.$refs.locationChildren) {
                this.$refs.locationChildren.forEach(function (x) {
                    return x.updateCheck(true);
                });
            }
        },
        notifyUpdateFromChildren: function (checked) {
            this.selectedIds = this.getSelectedIds();
        },
        getSelectedIds: function () {
            var ids = [];
            if (this.$refs.locationChildren) {
                this.$refs.locationChildren.forEach(function (x) {
                    ids = ids.concat(x.getSelectedIds());
                });
            }
            return ids;
        },
        updateShowChildren: function () {
            if (this.$refs.locationChildren) {
                this.$refs.locationChildren.forEach(function (x) {
                    x.updateShowChildren();
                });
            }
        }
    },
    beforeMount: function () {
        const DATA_JSON = JSON.parse(this.$el.attributes['data-json'].value === '' ? '{}' : this.$el.attributes['data-json'].value);
        this.items = DATA_JSON.Children;
    },
    updated: function () {
        $(document).trigger('updateEventLocationControl');
    }
});
