﻿const marketTags = new Vue({
    el: '#marketTags',
    components: {
        'market-tag': {
            props: {
                cssClass: {
                    type: String,
                    default: '',
                    required: true
                },
                isChecked: {
                    type: String,
                    default: '',
                    required: true
                },
                name: {
                    type: String,
                    default: '',
                    required: true
                },
                tagGroup: {
                    type: String,
                    default: '',
                    required: true
                },
                tagId: {
                    type: String,
                    default: '',
                    required: true
                },
                value: {
                    type: String,
                    default: '',
                    required: true
                },
                valueLabel: {
                    type: String,
                    default: '',
                    required: true
                }
            },
            methods: {
                toggle: function (isChecked) {
                    isChecked != '' ? this.$emit('uncheck', '') : this.$emit('check', 'checked')
                }
            },
            template: '#market-tag'
        }
    },
    data: {
        toggleFlag: false,
        toggleButtonName: 'Select All',
        json: { MarketOptions: [] },
        show: true,
    },
    computed: {
        markets: function () {
            return this.json.MarketOptions
        }
    },
    methods: {
        toggle: function () {
            if (this.markets) {
                if (!this.toggleFlag) {
                    this.markets.forEach(function (x) {
                        x.isChecked = 'checked'
                    })
                    this.toggleFlag = true
                    this.toggleButtonName = 'Unselect All'

                }
                else {
                    this.markets.forEach(function (x) {
                        x.isChecked = ''
                    })
                    this.toggleFlag = false
                    this.toggleButtonName = 'Select All'
                }
            }
        }
    },
    beforeMount: function () {
        this.json = JSON.parse(this.$el.attributes['data-json'].value === '' ? '{}' : this.$el.attributes['data-json'].value)
        this.show = this.json.MarketOptions !== undefined
        var uncheckedOptions = this.json.MarketOptions ? this.json.MarketOptions.filter(function (option) {
            return option.isChecked === ''
        }).length : 0;
        if (uncheckedOptions == 0) {
            this.toggle()
        }
    }
});
