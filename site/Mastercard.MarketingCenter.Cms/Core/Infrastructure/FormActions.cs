﻿namespace Mastercard.MarketingCenter.Cms.Core.Infrastructure
{
    public enum FormActions
    {
        SaveDraft,
        SaveAndPublish
    }
}