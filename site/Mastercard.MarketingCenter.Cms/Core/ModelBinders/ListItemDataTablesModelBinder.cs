﻿using Autofac.Integration.Mvc;
using Mastercard.MarketingCenter.Cms.Core.DataTables;
using Mastercard.MarketingCenter.Cms.Models.List;
using System.Collections.Specialized;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders
{
    [ModelBinderType(typeof(GetListItemsDataTablesRequest))]
    public class ListItemDataTablesModelBinder : DataTablesBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            return Bind(controllerContext, bindingContext, typeof(GetListItemsDataTablesRequest));
        }

        protected override void MapAditionalProperties(IDataTablesRequest requestModel, NameValueCollection requestParameters)
        {
            var model = requestModel as GetListItemsDataTablesRequest;
            if (model != null)
            {
                model.ListId = Get<int>(requestParameters, "listId");
                model.isExpired = Get<bool>(requestParameters, "includeExpired");
            }
        }
    }
}