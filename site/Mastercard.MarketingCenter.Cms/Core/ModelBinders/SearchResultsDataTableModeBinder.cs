﻿using Autofac.Integration.Mvc;
using Mastercard.MarketingCenter.Cms.Core.DataTables;
using Mastercard.MarketingCenter.Cms.Models.Search;
using System.Collections.Specialized;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders
{
    [ModelBinderType(typeof(GetSearchResultsDataTableRequest))]
    public class SearchResultsDataTableModeBinder : DataTablesBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            return Bind(controllerContext, bindingContext, typeof(GetSearchResultsDataTableRequest));
        }

        protected override void MapAditionalProperties(IDataTablesRequest requestModel, NameValueCollection requestParameters)
        {
            var model = requestModel as GetSearchResultsDataTableRequest;
            if (model != null)
            {
                model.SearchTerm = Get<string>(requestParameters, "searchTerm");
            }
        }
    }
}