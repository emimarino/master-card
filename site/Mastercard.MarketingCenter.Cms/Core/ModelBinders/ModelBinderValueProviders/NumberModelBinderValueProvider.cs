﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Web;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders.ModelBinderValueProviders
{
    public class NumberModelBinderValueProvider : ModelBinderValueProviderBase
    {
        public override object GetValue(HttpRequestBase request, string key, FieldDefinition fieldDefinition)
        {
            if (string.IsNullOrEmpty(request.Form[key]))
                return string.Empty;
            return request.Form[key];
        }
    }
}