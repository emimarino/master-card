﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data;
using System;
using System.Web;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders.ModelBinderValueProviders
{
    public class CustomizationModelBinderValueProvider : ModelBinderValueProviderBase
    {
        public override object GetValue(HttpRequestBase request, string key, FieldDefinition fieldDefinition)
        {
            var model = new CustomizationField();
            const string optKey = Constants.FieldPrefix + "Customizable_opt_customizations";
            var optCustomizationOptionField = request.Form[optKey];
            if (!string.IsNullOrEmpty(optCustomizationOptionField))
            {
                model.OptionalCustomizableOptions = optCustomizationOptionField.Split(',');
            }

            const string reqKey = Constants.FieldPrefix + "Customizable_req_customizations";
            var reqCustomizationOptionField = request.Form[reqKey];
            if (!string.IsNullOrEmpty(reqCustomizationOptionField))
            {
                model.RequiredCustomizableOptions = reqCustomizationOptionField.Split(',');
            }
            var booleanModelBinderValueProvider = new BooleanModelBinderValueProvider();
            model.Customizable = Convert.ToBoolean(booleanModelBinderValueProvider.GetValue(request, key, fieldDefinition));

            return model;
        }
    }
}