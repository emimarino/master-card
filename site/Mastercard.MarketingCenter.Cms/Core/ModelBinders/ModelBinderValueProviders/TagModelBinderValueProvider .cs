﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Web;
using System.Web.Helpers;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders.ModelBinderValueProviders
{
    public class TagModelBinderValueProvider : ModelBinderValueProviderBase
    {
        public override object GetValue(HttpRequestBase request, string key, FieldDefinition fieldDefinition)
        {
            return request.Unvalidated(key);
        }
    }
}