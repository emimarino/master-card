﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Web;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders.ModelBinderValueProviders
{
    public class UserLookupValueProvider : ModelBinderValueProviderBase
    {
        public override object GetValue(HttpRequestBase request, string key, FieldDefinition fieldDefinition)
        {
            if (string.IsNullOrEmpty(request.Form[key]))
            {
                return null;
            }
            return request.Form[key];
        }
    }
}