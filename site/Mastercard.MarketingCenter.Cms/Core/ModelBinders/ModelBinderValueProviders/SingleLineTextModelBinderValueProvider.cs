﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Services.Extensions;
using System;
using System.Web;
using System.Web.Helpers;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders.ModelBinderValueProviders
{
    public class SingleLineTextModelBinderValueProvider : ModelBinderValueProviderBase
    {
        public override object GetValue(HttpRequestBase request, string key, FieldDefinition fieldDefinition)
        {
            if (string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                return request.Unvalidated(key);
            }

            var settings = fieldDefinition.GetSettings<SingleLineTextSettings>();
            if (!string.IsNullOrEmpty(settings.GetAllTextFromTheRightOf))
            {
                var value = request[key].ToLowerInvariant();
                var textToGetRid = settings.GetAllTextFromTheRightOf.ToLowerInvariant();

                if (value.Contains(textToGetRid))
                {
                    return value.Substring(value.IndexOf(textToGetRid) + textToGetRid.Length - 1,
                        (value.Length - value.IndexOf(textToGetRid)) - textToGetRid.Length + 1);
                }

                return value;
            }

            if (!string.IsNullOrEmpty(settings.RemoveAllOccurrencesOf))
            {
                var valuesToRemove = settings.RemoveAllOccurrencesOf.Split(',');

                var result = request[key];
                foreach (var valueToRemove in valuesToRemove)
                {
                    result = result.ToLowerInvariant().Replace(valueToRemove.ToLowerInvariant(), string.Empty);
                }

                return result;
            }

            if (settings.UriRemoveDomainSegment)
            {
                var url = request[key];

                Uri uri;
                if (Uri.TryCreate(url, UriKind.Absolute, out uri))
                {
                    return uri.PathAndQuery;
                }
            }

            return request[key];
        }
    }
}