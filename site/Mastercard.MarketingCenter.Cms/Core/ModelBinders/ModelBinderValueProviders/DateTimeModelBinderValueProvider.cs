﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Globalization;
using System.Web;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders.ModelBinderValueProviders
{
    public class DateTimeModelBinderValueProvider : ModelBinderValueProviderBase
    {
        public override object GetValue(HttpRequestBase request, string key, FieldDefinition fieldDefinition)
        {
            object value;
            /*var hour = request.Form[string.Format("{0}_Hour", key)];
            var minute = request.Form[string.Format("{0}_Minute", key)];*/
            var date = request.Form[key];
            if (string.IsNullOrEmpty(date))
                value = null;
            else
            {
                DateTime parsedDate;
                if (DateTime.TryParseExact(string.Format("{0}", date), Constants.DateTimeFormat,
                    CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDate))
                {
                    value = parsedDate;
                }
                else value = null;
            }
            return value;
        }
    }
}