﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;
using System.Web;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders.ModelBinderValueProviders
{
    public class BooleanModelBinderValueProvider : ModelBinderValueProviderBase
    {
        public override object GetValue(HttpRequestBase request, string key, FieldDefinition fieldDefinition)
        {
            var value = request.Form[key];
            return bool.Parse(value.Split(',').FirstOrDefault());
        }
    }
}