﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data.Entities;
using System.Web;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders.ModelBinderValueProviders
{
    public class VanityUrlModelBinderValueProvider : ModelBinderValueProviderBase
    {
        public override object GetValue(HttpRequestBase request, string key, FieldDefinition fieldDefinition)
        {
            var urlPrefix = request[key + "_url"].TrimEnd('/');

            var url = urlPrefix;

            if (string.IsNullOrEmpty(request[key]))
            {
                const string titleKey = Constants.FieldPrefix + "Title";
                url += "/" + request[titleKey].ToURLFriendly();
            }
            else
            {
                url += "/" + request[key].TrimStart('/').stringCleanPartialUrl();
            }

            return url;
        }
    }
}