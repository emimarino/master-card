﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Web;
using System.Web.Helpers;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders.ModelBinderValueProviders
{
    public class SingleLookupModelBinderValueProvider : ModelBinderValueProviderBase
    {
        public override object GetValue(HttpRequestBase request, string key, FieldDefinition fieldDefinition)
        {
            var value = request[key];
            return string.IsNullOrEmpty(value) ? null : request.Unvalidated(key);
        }
    }
}