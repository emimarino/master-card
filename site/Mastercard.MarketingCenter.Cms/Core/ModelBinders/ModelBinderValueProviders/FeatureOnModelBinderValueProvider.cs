﻿using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;
using System.Web;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders.ModelBinderValueProviders
{
    public class FeatureOnModelBinderValueProvider : ModelBinderValueProviderBase
    {
        public override object GetValue(HttpRequestBase request, string key, FieldDefinition fieldDefinition)
        {
            var items = new List<FeatureTagItem>();

            if (string.IsNullOrEmpty(request[key].Trim())) return items;

            var values = request[key].Trim().Split(',');

            foreach (var value in values)
            {
                var fieldValues = value.Split('|');
                var id = fieldValues[0];
                var level = fieldValues[1];
                var type = fieldValues[2];

                items.Add(new FeatureTagItem
                {
                    Id = id,
                    FeatureLevel = Convert.ToInt16(level),
                    Type = (FeatureType)Enum.Parse(typeof(FeatureType), type, true)
                });
            }

            return items;
        }
    }
}