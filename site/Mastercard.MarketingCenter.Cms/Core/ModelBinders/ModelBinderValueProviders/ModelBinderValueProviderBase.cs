﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Web;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders.ModelBinderValueProviders
{
    public abstract class ModelBinderValueProviderBase
    {
        public abstract object GetValue(HttpRequestBase request, string key, FieldDefinition fieldDefinition);
    }
}