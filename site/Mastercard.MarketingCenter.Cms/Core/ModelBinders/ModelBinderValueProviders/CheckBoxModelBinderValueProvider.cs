﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;
using System.Web;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders.ModelBinderValueProviders
{
    public class CheckBoxModelBinderValueProvider : ModelBinderValueProviderBase
    {
        public override object GetValue(HttpRequestBase request, string key, FieldDefinition fieldDefinition)
        {
            var value = request[key];

            if (string.IsNullOrEmpty(value))
                return string.Empty;

            var values = value.Trim().Split(',');

            var returnValues = values.Where(val => val != Constants.CheckboxNullValue).ToList();

            return string.Join(",", returnValues);
        }
    }
}