﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders.ModelBinderValueProviders
{
    public static class ModelBinderValueProviderFactory
    {
        public static ModelBinderValueProviderBase GetModelBinderValueProvider(string fieldTypeCode)
        {
            switch (fieldTypeCode)
            {
                case FieldType.Tag:
                    return new TagModelBinderValueProvider();
                case FieldType.Checkbox:
                    return new CheckBoxModelBinderValueProvider();
                case FieldType.SingleLineText:
                    return new SingleLineTextModelBinderValueProvider();
                case FieldType.SingleLookup:
                    return new SingleLookupModelBinderValueProvider();
                case FieldType.HtmlText:
                case FieldType.MultiLineText:
                    return new TextModelBinderValueProvider();
                case FieldType.DateTime:
                    return new DateTimeModelBinderValueProvider();
                case FieldType.Boolean:
                    return new BooleanModelBinderValueProvider();
                case FieldType.RegionInt:
                case FieldType.Number:
                    return new NumberModelBinderValueProvider();
                case FieldType.Customization:
                    return new CustomizationModelBinderValueProvider();
                case FieldType.FeatureOn:
                    return new FeatureOnModelBinderValueProvider();
                case FieldType.VanityUrl:
                    return new VanityUrlModelBinderValueProvider();
                case FieldType.UserLookup:
                    return new UserLookupValueProvider();
                default:
                    return new DefaultModelBinderValueProvider();
            }
        }
    }
}