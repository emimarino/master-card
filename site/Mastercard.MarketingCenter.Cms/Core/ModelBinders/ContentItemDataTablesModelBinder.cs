﻿using Autofac.Integration.Mvc;
using Mastercard.MarketingCenter.Cms.Core.DataTables;
using Mastercard.MarketingCenter.Cms.Models.Content;
using System.Collections.Specialized;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders
{
    [ModelBinderType(typeof(GetContentItemsDataTablesRequest))]
    public class ContentItemDataTablesModelBinder : DataTablesBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            return Bind(controllerContext, bindingContext, typeof(GetContentItemsDataTablesRequest));
        }

        protected override void MapAditionalProperties(IDataTablesRequest requestModel, NameValueCollection requestParameters)
        {
            var model = requestModel as GetContentItemsDataTablesRequest;
            if (model != null)
            {
                model.ContentTypeId = Get<string>(requestParameters, "contentTypeId");
                model.ShowArchived = Get<bool>(requestParameters, "includeArchive");
                model.isExpired = Get<bool>(requestParameters, "includeExpired");
                model.ShowPendings = Get<bool>(requestParameters, "showPendings");
            }
        }
    }
}