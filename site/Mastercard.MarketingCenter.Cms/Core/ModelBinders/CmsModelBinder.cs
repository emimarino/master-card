﻿using Mastercard.MarketingCenter.Cms.Core.ModelBinders.ModelBinderValueProviders;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders
{
    public abstract class CmsModelBinder : DefaultModelBinder
    {
        protected void AddItemData(IDictionary<string, object> itemData, IEnumerable<FieldDefinition> fieldDefinitions, HttpRequestBase request, bool updateIfExists = false)
        {
            foreach (var key in request.Form.AllKeys)
            {
                var fieldKey = key.ToFieldKey();
                var fieldDefinition = fieldDefinitions.FirstOrDefault(o => o.Name == fieldKey);
                if (fieldDefinition == null)
                {
                    continue;
                }

                var fieldTypeCode = fieldDefinition.FieldTypeCode;
                var value = GetTypeValue(request, key, fieldTypeCode, fieldDefinition);
                if (updateIfExists && itemData.ContainsKey(fieldKey))
                {
                    itemData[fieldKey] = value;
                }
                else
                {
                    itemData.Add(fieldKey, value);
                }
            }
        }

        protected T GetModel<T>(IDictionary<string, object> itemData, IEnumerable<FieldDefinition> fieldDefinitions, HttpRequestBase request)
        {
            T model = Activator.CreateInstance<T>();

            foreach (var key in request.Form.AllKeys)
            {
                var fieldKey = key.Replace(Constants.FieldPrefix, string.Empty);

                var fieldDefinition = fieldDefinitions.FirstOrDefault(o => o.Name == fieldKey);

                if (fieldDefinition == null)
                {
                    continue;
                }

                var fieldTypeCode = fieldDefinition.FieldTypeCode;
                var value = GetTypeValue(request, key, fieldTypeCode, fieldDefinition);

                var property = typeof(T).GetProperty(fieldKey);
                if (property != null)
                {
                    property.SetValue(model, value);
                }
            }

            return model;
        }

        private static object GetTypeValue(HttpRequestBase request, string key, string fieldTypeCode, FieldDefinition fieldDefinition)
        {
            var modelBinderValueProvider = ModelBinderValueProviderFactory.GetModelBinderValueProvider(fieldTypeCode);
            return modelBinderValueProvider.GetValue(request, key, fieldDefinition);
        }

        protected static DateTime? ParseDateTime(string date)
        {
            DateTime parsedDate;
            if (!string.IsNullOrWhiteSpace(date) && DateTime.TryParseExact(date, Constants.DateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDate))
            {
                return parsedDate;
            }

            return null;
        }
    }
}