﻿using Autofac.Integration.Mvc;
using Mastercard.MarketingCenter.Cms.Core.DataTables;
using Mastercard.MarketingCenter.Cms.Models.Issuers;
using System.Collections.Specialized;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders
{
    [ModelBinderType(typeof(GetActiveUsersForIssuerTableRequest))]
    public class ActiveUsersForIssuerDataTableModelBinder : DataTablesBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            return Bind(controllerContext, bindingContext, typeof(GetActiveUsersForIssuerTableRequest));
        }

        protected override void MapAditionalProperties(IDataTablesRequest requestModel, NameValueCollection requestParameters)
        {
            var model = requestModel as GetActiveUsersForIssuerTableRequest;
            if (model != null)
            {
                model.IssuerId = Get<string>(requestParameters, "issuerId");
            }
        }
    }
}