﻿using Autofac.Integration.Mvc;
using Mastercard.MarketingCenter.Cms.Models;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders
{
    [ModelBinderType(typeof(ContentModel))]
    public class ContentItemModelBinder : CmsModelBinder
    {
        private readonly ContentItemDao _contentItemDao;
        private readonly ContentTypeDao _contentTypeDao;
        private readonly UserContext _userContext;

        public ContentItemModelBinder(ContentItemDao contentItemDao, ContentTypeDao contentTypeDao, UserContext userContext)
        {
            _contentItemDao = contentItemDao;
            _contentTypeDao = contentTypeDao;
            _userContext = userContext;
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType == typeof(ContentModel))
            {
                var request = controllerContext.HttpContext.Request;

                var id = request.Form.Get("IdFieldValue");
                var contentTypeId = request.Form.Get("ContentTypeId");
                var canPreview = Convert.ToBoolean(request.Form.Get("CanPreview"));

                var model = new ContentModel
                {
                    IsNewForm = Convert.ToString(controllerContext.RouteData.Values["action"]) == "Create",
                    UserRegion = request.Form.Get("UserRegion"),
                    CanPreview = canPreview,
                    CurrentUserId = _userContext.User.UserId,
                    UploadActivityIds = JsonConvert.DeserializeObject<List<int>>(Convert.ToString(request.Form.Get("UploadActivityIds")))
                };

                model.ExpirationDate = ParseDateTime(request.Form.Get("ExpirationDate")?.ToString());

                if (string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(contentTypeId))
                {
                    // Create content model binding
                    var contentType = _contentTypeDao.GetById(contentTypeId);
                    model.ContentTypeId = contentTypeId;
                    model.ContentTypeFieldDefinitions = contentType.ContentTypeFieldDefinitions.Where(o => !o.HiddenInNew);
                    model.ContentTypeTitle = contentType.Title;
                }
                else
                {
                    // Update content model binding
                    var contentItem = _contentItemDao.GetById(id);
                    model.IdFieldValue = id;
                    model.ContentTypeTitle = contentItem.ContentType.Title;
                    model.ContentTypeId = contentItem.ContentTypeId;
                    model.ContentTypeTitle = contentItem.ContentType.Title;
                    model.ContentTypeFieldDefinitions = contentItem.ContentType.ContentTypeFieldDefinitions;
                    model.LastModifyDate = contentItem.ModifiedDate;
                    model.ModifiedByUserEmail = contentItem.ModifiedByUser.Email;
                    model.ModifiedByUserId = contentItem.ModifiedByUserId;

                    var originalContentData = request.Form.Get("originalContentData");
                    if (originalContentData != null)
                    {
                        model.OriginalContentItemData = JsonConvert.DeserializeObject<Dictionary<string, object>>(originalContentData);
                    }
                }

                AddItemData(model.ContentItemData, model.ContentTypeFieldDefinitions.Select(o => o.FieldDefinition).ToList(), request);

                return model;
            }

            return base.BindModel(controllerContext, bindingContext);
        }
    }
}