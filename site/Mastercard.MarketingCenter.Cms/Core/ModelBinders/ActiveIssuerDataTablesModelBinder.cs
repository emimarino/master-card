﻿using Autofac.Integration.Mvc;
using Mastercard.MarketingCenter.Cms.Core.DataTables;
using Mastercard.MarketingCenter.Cms.Models.Content;
using System.Collections.Specialized;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders
{
    [ModelBinderType(typeof(GetActiveIssuersDataTablesRequest))]
    public class ActiveIssuerDataTablesModelBinder : DataTablesBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            return Bind(controllerContext, bindingContext, typeof(GetActiveIssuersDataTablesRequest));
        }

        protected override void MapAditionalProperties(IDataTablesRequest requestModel, NameValueCollection requestParameters)
        {
            var model = requestModel as GetActiveIssuersDataTablesRequest;
            if (model != null)
            {
                model.Status = Get<string>(requestParameters, "status");
            }
        }
    }
}