﻿using Autofac.Integration.Mvc;
using Mastercard.MarketingCenter.Cms.Models;
using Mastercard.MarketingCenter.Cms.Services.Core;
using Mastercard.MarketingCenter.Data.Daos;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders
{
    [ModelBinderType(typeof(ListItemModel))]
    public class ListItemModelBinder : CmsModelBinder
    {
        private readonly ListDao _listDao;

        public ListItemModelBinder(ListDao listDao)
        {
            _listDao = listDao;
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType == typeof(ListItemModel))
            {
                var request = controllerContext.HttpContext.Request;

                var id = request.Form.Get("IdFieldValue");
                var listId = Convert.ToInt32(request.Form.Get("ListId"));
                var list = _listDao.GetById(listId);
                var returnUrl = request.Form.Get("ReturnUrl");
                var clientSuccesCallback = request.Form.Get("ClientSuccesCallback");
                var clientErrorCallback = request.Form.Get("ClientErrorCallback");
                var clientCompleteCallback = request.Form.Get("ClientCompleteCallback");
                var updateTargetId = request.Form.Get("UpdateTargetId");

                var model = new ListItemModel
                {
                    ListId = listId,
                    IdFieldValue = id,
                    ListTitle = list.Name,
                    ReturnUrl = returnUrl,
                    ClientErrorCallback = clientErrorCallback,
                    ClientSuccesCallback = clientSuccesCallback,
                    ClientCompleteCallback = clientCompleteCallback,
                    UpdateTargetId = updateTargetId
                };

                if (string.IsNullOrEmpty(id))
                {
                    model.ListFieldDefinitions = list.ListFieldDefinitions.Where(o => !o.HiddenInNew);
                }
                else
                {
                    model.ListFieldDefinitions = list.ListFieldDefinitions;
                }

                model.ListFieldDefinitions = model.ListFieldDefinitions.ExcludeTrackingFields().ExcludeRegionField();
                model.HasTrackingFields = model.ListFieldDefinitions.HasTrackingFields();

                AddItemData(model.ListItemData, model.ListFieldDefinitions.Select(o => o.FieldDefinition).ToList(), request);

                return model;
            }

            return base.BindModel(controllerContext, bindingContext);
        }
    }
}