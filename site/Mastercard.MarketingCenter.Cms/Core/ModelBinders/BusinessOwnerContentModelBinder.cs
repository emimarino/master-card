﻿using Autofac.Integration.Mvc;
using Mastercard.MarketingCenter.Cms.Models;
using Mastercard.MarketingCenter.Cms.Services.Core;
using Mastercard.MarketingCenter.Data.Daos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders
{
    [ModelBinderType(typeof(BusinessOwnerContentModel))]
    public class BusinessOwnerContentModelBinder : CmsModelBinder
    {
        private readonly ContentItemDao _contentItemDao;
        private readonly ContentTypeDao _contentTypeDao;

        public BusinessOwnerContentModelBinder(ContentItemDao contentItemDao, ContentTypeDao contentTypeDao)
        {
            _contentItemDao = contentItemDao;
            _contentTypeDao = contentTypeDao;
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType == typeof(BusinessOwnerContentModel))
            {
                var request = controllerContext.HttpContext.Request;

                var id = request.Form.Get("IdFieldValue");
                var contentTypeId = request.Form.Get("ContentTypeId");
                var contentItemTitle = request.Unvalidated.Form.Get("ContentItemTitle");
                int businessOwnerId;
                int.TryParse(request.Form.Get("BusinessOwnerId"), out businessOwnerId);

                var model = new BusinessOwnerContentModel
                {
                    RegionId = request.Form.Get("RegionId"),
                    RegionTitle = request.Form.Get("RegionTitle"),
                    CurrentExpirationDate = ParseDateTime(request.Form.Get("CurrentExpirationDate").ToString()),
                    ExpirationDate = ParseDateTime(request.Form.Get("ExpirationDate").ToString()),
                    BusinessOwnerId = businessOwnerId,
                    UploadActivityIds = JsonConvert.DeserializeObject<List<int>>(Convert.ToString(request.Form.Get("UploadActivityIds")))
                };

                if (string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(contentTypeId))
                {
                    // Create content model binding
                    var contentType = _contentTypeDao.GetById(contentTypeId);
                    model.ContentTypeId = contentTypeId;
                    model.ContentTypeFieldDefinitions = contentType.ContentTypeFieldDefinitions.FilterByBusinessOwner(false);
                }
                else
                {
                    // Update content model binding
                    var contentItem = _contentItemDao.GetById(id);
                    model.IdFieldValue = id;
                    model.ContentItemTitle = contentItemTitle;
                    model.ContentTypeId = contentItem.ContentTypeId;
                    model.ContentTypeFieldDefinitions = contentItem.ContentType.ContentTypeFieldDefinitions.FilterByBusinessOwner(false);

                    var originalContentData = request.Form.Get("originalContentData");
                    if (originalContentData != null)
                    {
                        model.OriginalContentItemData = JsonConvert.DeserializeObject<Dictionary<string, object>>(originalContentData);
                    }
                }

                AddItemData(model.ContentItemData, model.ContentTypeFieldDefinitions.Select(o => o.FieldDefinition).ToList(), request);

                return model;
            }

            return base.BindModel(controllerContext, bindingContext);
        }
    }
}