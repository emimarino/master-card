﻿using Autofac.Integration.Mvc;
using Mastercard.MarketingCenter.Cms.Core.DataTables;
using Mastercard.MarketingCenter.Cms.Models.Issuers;
using System.Collections.Specialized;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Core.ModelBinders
{
    [ModelBinderType(typeof(GetIssuerSearchTableRequest))]
    public class SearchForIssuerDataTableModelBinder : DataTablesBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            return Bind(controllerContext, bindingContext, typeof(GetIssuerSearchTableRequest));
        }

        protected override void MapAditionalProperties(IDataTablesRequest requestModel, NameValueCollection requestParameters)
        {
            var model = requestModel as GetIssuerSearchTableRequest;
            if (model != null)
            {
                model.IssuerToSearch = Get<string>(requestParameters, "issuerToSearch");
                model.IssuerId = Get<string>(requestParameters, "issuerId");
            }
        }
    }
}