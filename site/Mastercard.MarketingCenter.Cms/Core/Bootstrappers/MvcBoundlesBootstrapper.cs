﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using System.Web.Optimization;

namespace Mastercard.MarketingCenter.Cms.Core.Bootstrappers
{
    public class MvcBoundlesBootstrapper : IBootstrapper
    {
        public void Execute()
        {
            RegisterBundles(BundleTable.Bundles);
            BundleTable.EnableOptimizations = true;
        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            RegisterDataTablesBundle(bundles);
            RegisterTagScriptsBundle(bundles);
            RegisterMainBundle(bundles);
            RegisterContentTypeBundle(bundles);
            RegisterContentBundle(bundles);
            RegisterReportBundle(bundles);
            RegisterUserScriptsBundle(bundles);
            RegisterGroupScriptsBundle(bundles);
            RegisterIssuersBundle(bundles);
            RegisterSearchResultsBundle(bundles);
            RegisterBusinessOwnerBundle(bundles);
            RegisterLayoutBundle(bundles);
        }

        private static void RegisterReportBundle(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Report/report")
            .Include("~/Content/css/plugins/datapicker/datepicker3.css")
            .Include("~/Content/css/plugins/codemirror/vibrant.css")
            .Include("~/Content/css/plugins/chosen/chosen.css"));

            bundles.Add(new ScriptBundle("~/bundles/report")
                .Include("~/Content/js/report/report.js"));
        }

        private static void RegisterSearchResultsBundle(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/searchResults")
                .Include("~/Content/js/search/searchResults.js"));
        }

        private static void RegisterMainBundle(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/main")
                .Include("~/Content/js/jquery-1.11.3.min.js")
                .Include("~/Content/js/bootstrap.min.js")
                .Include("~/Content/js/json2.js")
                .Include("~/Content/js/plugins/metisMenu/jquery.metisMenu.js")
                .Include("~/Content/js/plugins/slimscroll/jquery.slimscroll.min.js")
                .Include("~/Content/js/plugins/datapicker/bootstrap-datepicker.js")
                .Include("~/Content/js/plugins/chosen/chosen.jquery.js")
                .Include("~/Content/js/plugins/selectric/jquery.selectric.js")
                .Include("~/scripts/jquery.unobtrusive-ajax.js")
                .Include("~/Content/js/layout.js")
                .Include("~/Content/js/plugins/jeditable/jquery.jeditable.js"));
            
        }
        private static void RegisterDataTablesBundle(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/plugins/datatables")
                .Include("~/Content/css/plugins/dataTables/dataTables.bootstrap.css")
                .Include("~/Content/css/plugins/dataTables/dataTables.responsive.css"));

            bundles.Add(new ScriptBundle("~/bundles/datatables")
                .Include("~/Content/js/plugins/dataTables/jquery.dataTables.js")
                .Include("~/Content/js/plugins/dataTables/dataTables.bootstrap.js")
                .Include("~/Content/js/plugins/dataTables/dataTables.responsive.js")
                .Include("~/Content/js/plugins/dataTables/dataTables.templates.js")
                .Include("~/Content/js/plugins/dataTables/dataTables.tableTools.min.js"));
        }

        private static void RegisterContentTypeBundle(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/contenttype")
                .Include("~/Content/js/contentType/index.js")
                .Include("~/Content/js/contentType/update.js"));
        }

        private static void RegisterIssuersBundle(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/issuers")
                .Include("~/Content/js/issuers/list.js"));
        }

        private static void RegisterContentBundle(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/content")
                .Include("~/Content/css/plugins/datapicker/datepicker3.css")
                .Include("~/Content/css/plugins/codemirror/codemirror.css")
                .Include("~/Content/css/plugins/summernote/summernote-bs3.css")
                .Include("~/Content/css/plugins/summernote/summernote.css")
                .Include("~/Content/css/plugins/chosen/chosen.css")
                .Include("~/Content/css/ContentComments/styles.css")
                .Include("~/Content/css/ContentComments/comments_log_styles.css")
                .Include("~/Content/css/plugins/codemirror/vibrant.css"));

            bundles.Add(new ScriptBundle("~/bundles/content")
                .Include("~/Content/js/content/list.js")
                .Include("~/Content/js/plugins/codemirror/codemirror.js")
                .Include("~/Content/js/plugins/codemirror/mode/xml/xml.js")
                .Include("~/Content/js/plugins/codemirror/formatting.min.js")
                .Include("~/Content/js/plugins/summernote/summernote.min.js")
                .Include("~/Content/js/plugins/plupload/plupload.full.min.js")
                .Include("~/Content/js/plugins/fullcalendar/moment.min.js")
                .Include("~/Content/js/plugins/jquery-ui/jquery-ui-sortable-1.12.1.min.js")
                .Include("~/Content/js/content/crud.js")
                .Include("~/Content/js/plugins/summernote/summernote-cleaner-plugin.js")
                .Include("~/Content/js/validators.js")
                .Include("~/Content/js/content/region-int-visibility.js"));
        }

        private static void RegisterTagScriptsBundle(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/tag")
                .Include("~/Content/js/tag/index.js")
                );
        }

        private static void RegisterLayoutBundle(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/layout")
                .Include("~/Content/css/google-fonts.css")
                .Include("~/Content/font-awesome/css/font-awesome.css")
                .Include("~/Content/css/bootstrap.min.css")                
                .Include("~/Content/css/animate.css")
                .Include("~/Content/css/style.css"));
        }

        private static void RegisterUserScriptsBundle(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/user")
                .Include("~/Content/js/user/index.js")
                );
        }
        private static void RegisterGroupScriptsBundle(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/group")
                .Include("~/Content/js/group/index.js")
                .Include("~/Content/js/group/crud.js")
                .Include("~/Content/js/plugins/summernote/summernote-cleaner-plugin.js"));
        }

        private static void RegisterBusinessOwnerBundle(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/BusinessOwner/content")
                .Include("~/Content/css/BusinessOwner/styles.css"));
        }
    }
}