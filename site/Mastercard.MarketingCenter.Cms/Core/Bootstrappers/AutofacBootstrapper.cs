﻿using Autofac;
using Autofac.Integration.Mvc;
using Mastercard.MarketingCenter.Cms.Core.Modules;
using Mastercard.MarketingCenter.Cms.Services.Modules;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Modules;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Core.Bootstrappers
{
    public class AutofacBootstrapper : IBootstrapper
    {
        public void Execute()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new AutofacWebTypesModule());
            builder.RegisterModule(new CmsModule());
            builder.RegisterModule(new ServicesModule(false, false));
            builder.RegisterModule(new ContextModule(false));
            builder.RegisterModule(new MvcModule());
            builder.RegisterModule(new AutoMapperModule());

            var container = builder.Build();
            var provider = new StandaloneLifetimeScopeProvider(container);
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container, provider));
        }
    }
}