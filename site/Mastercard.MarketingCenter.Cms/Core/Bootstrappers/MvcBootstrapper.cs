﻿using FluentValidation.Mvc;
using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using Mastercard.MarketingCenter.Common.Infrastructure;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Core.Bootstrappers
{
    public class MvcBootstrapper : IBootstrapper
    {
        public void Execute()
        {
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            FluentValidationModelValidatorProvider.Configure();
        }

        private static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new UnhandledExceptionFilterAttribute());
        }
    }
}