﻿using Mastercard.MarketingCenter.Cms.Core.RouteHandlers;
using Mastercard.MarketingCenter.Common.Infrastructure;
using System.Web.Mvc;
using System.Web.Routing;

namespace Mastercard.MarketingCenter.Cms.Core.Bootstrappers
{
    public class RoutingBootstrapper : IBootstrapper
    {
        public void Execute()
        {
            var routes = RouteTable.Routes;

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });

            routes.Add("webforms", new Route("webforms/{page}", new DirectoryRouteHandler(virtualDir: "~/WebForms")));

            routes.Add("legacyCss", new Route("_layouts/1033/styles/{file}", new LegacyFilesRouteHandler()));
            routes.Add("legacyImages", new Route("_layouts/1033/images/{file}", new LegacyFilesRouteHandler()));
            routes.Add("legacyJs", new Route("_layouts/inc/{file}", new LegacyFilesRouteHandler()));

            routes.MapPageRoute("RegistrationPending", "registration/pending/{guid}/{showrejected}", "~/WebForms/PendingRegistration.aspx", false, new RouteValueDictionary { { "showrejected", "false" } });

            routes.MapRoute("PublishedAssets", "businessowner", new { controller = "BusinessOwner", action = "PublishedAssets" });

            routes.MapRoute("Error", "error", new { controller = "Error", action = "Error" });
            routes.MapRoute("NotFound", "notfound", new { controller = "Error", action = "NotFound" });
            routes.MapRoute("Unauthorized", "unauthorized", new { controller = "Error", action = "Unauthorized" });
            routes.MapRoute("IsAuthenticated", "IsAuthenticated", new { controller = "Home", action = "IsAuthenticated" });
            //routes.MapRoute(
            //    "EditListItemRoute", // Route name
            //    "List/Edit/{listId}/{id}/{returnUrl}", // URL with parameters
            //    new { controller = "List", action = "Edit", listId = UrlParameter.Optional, id = UrlParameter.Optional, returnUrl = UrlParameter.Optional });

            //routes.MapRoute(
            //    "CreateListItemRoute", // Route name
            //    "List/Create/{listId}/{id}/{returnUrl}", // URL with parameters
            //    new { controller = "List", action = "Create", listId = UrlParameter.Optional, id = UrlParameter.Optional, returnUrl = UrlParameter.Optional });

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
                );
        }
    }
}