﻿using System.Web;
using System.Web.Routing;

namespace Mastercard.MarketingCenter.Cms.Core.RouteHandlers
{
    public class DirectoryRouteHandler : IRouteHandler
    {
        private readonly string _virtualDir;

        public DirectoryRouteHandler(string virtualDir)
        {
            _virtualDir = virtualDir;
        }

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            var routeValues = requestContext.RouteData.Values;

            if (!routeValues.ContainsKey("page")) return null;

            var page = routeValues["page"].ToString();
            if (!page.EndsWith(".aspx"))
                page += ".aspx";
            var pageVirtualPath = string.Format("{0}/{1}", _virtualDir, page);

            return new PageRouteHandler(pageVirtualPath, true).GetHttpHandler(requestContext);
        }
    }
}