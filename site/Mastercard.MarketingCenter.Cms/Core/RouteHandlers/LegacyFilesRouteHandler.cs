﻿using System.IO;
using System.Web;
using System.Web.Routing;

namespace Mastercard.MarketingCenter.Cms.Core.RouteHandlers
{
    public class LegacyFilesRouteHandler : IRouteHandler
    {
        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            var path = "~/Content/legacy/";
            var routeValues = requestContext.RouteData.Values;

            if (!routeValues.ContainsKey("file")) return null;

            var file = routeValues["file"].ToString();

            switch (Path.GetExtension(file))
            {
                case ".css":
                    path += "css";
                    break;
                case ".js":
                    path += "js";
                    break;
                case ".jpg":
                case ".gif":
                    path += "images";
                    break;
            }
         
            var fileVirtualPath = string.Format("{0}/{1}",path, file);

            return new LegacyFileHttpHandler(fileVirtualPath);
        }

        class LegacyFileHttpHandler : IHttpHandler
        {
            private readonly string _fileVirtualPath;

            public LegacyFileHttpHandler(string fileVirtualPath)
            {
                _fileVirtualPath = fileVirtualPath;
            }

            public void ProcessRequest(HttpContext context)
            {
                var fullPath = context.Server.MapPath(_fileVirtualPath);

                switch (Path.GetExtension(fullPath))
                {
                    case ".css":
                        context.Response.ContentType = "text/css";
                        break;
                    case ".js":
                        context.Response.ContentType = "application/javascript";
                        break;
                    case ".gif":
                        context.Response.ContentType = "image/gif";
                        break;
                    case ".jpg":
                        context.Response.ContentType = "image/jpeg";
                        break;
                }

                context.Response.TransmitFile(fullPath);
            }

            public bool IsReusable
            {
                get { return false; }
            }
        }
    }
}