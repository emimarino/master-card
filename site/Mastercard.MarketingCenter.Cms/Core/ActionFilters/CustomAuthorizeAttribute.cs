﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Mastercard.MarketingCenter.Cms.Core.ActionFilters
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        private string _accessRoleName;
        private bool _allowAllMasterCardUsers;

        // For some reason Autofac is not resolving these dependencies. Probably because its an old version
        public IAuthorizationService AuthorizationService
        {
            get
            {
                return DependencyResolver.Current.GetService<IAuthorizationService>();
            }
        }

        public MarketingCenterDbContext Context
        {
            get { return DependencyResolver.Current.GetService<MarketingCenterDbContext>(); }
        }

        protected UserContext UserContext
        {
            get
            {
                return DependencyResolver.Current.GetService<UserContext>();
            }
        }

        public CustomAuthorizeAttribute() : base()
        {

        }

        public CustomAuthorizeAttribute(string accessRoleName, bool allowAllMasterCardUsers = false) : base()
        {
            _accessRoleName = accessRoleName;
            _allowAllMasterCardUsers = allowAllMasterCardUsers;
        }

        public CustomAuthorizeAttribute(bool allowAllMasterCardUsers) : base()
        {
            _allowAllMasterCardUsers = allowAllMasterCardUsers;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!base.AuthorizeCore(httpContext))
            {
                return false;
            }

            if (_allowAllMasterCardUsers && UserContext.IsMastercardUser)
            {
                return true;
            }

            if (httpContext == null || httpContext.User == null || httpContext.User.Identity == null || httpContext.User.Identity.Name == null ||
                httpContext.Request == null || httpContext.Request.RequestContext == null || httpContext.Request.RequestContext.RouteData == null)
            {
                return false;
            }

            //If ID is set but listId is then authorization should be checked for the list
            //Consider refactoring to include listId in the route -- this used to be the approach but was changed.
            //Because it was refactored for some other reason we use the workaround for now.
            if (string.IsNullOrEmpty(Convert.ToString(httpContext.Request.RequestContext.RouteData.Values["id"])) &&
                string.IsNullOrEmpty(Convert.ToString(httpContext.Request.RequestContext.RouteData.Values["listId"])) &&
                (!string.IsNullOrEmpty(httpContext.Request.QueryString["listId"]) || !string.IsNullOrEmpty(httpContext.Request.Form["listId"])))
            {

                httpContext.Request.RequestContext.RouteData.Values.Add("listId", !string.IsNullOrEmpty(httpContext.Request.QueryString["listId"]) ?
                                                                                  httpContext.Request.QueryString["listId"] :
                                                                                  httpContext.Request.Form["listId"]);
            }
            //If ID is set but contentTypeId is then authorization should be checked for the content type/list
            //Consider refactoring to include contentTypeId in the route.
            else if (string.IsNullOrEmpty(Convert.ToString(httpContext.Request.RequestContext.RouteData.Values["id"])) &&
                     string.IsNullOrEmpty(Convert.ToString(httpContext.Request.RequestContext.RouteData.Values["listId"])) &&
                     (!string.IsNullOrEmpty(httpContext.Request.QueryString["contentTypeId"]) || !string.IsNullOrEmpty(httpContext.Request.Form["contentTypeId"])))
            {

                httpContext.Request.RequestContext.RouteData.Values.Add("listId", !string.IsNullOrEmpty(httpContext.Request.QueryString["contentTypeId"]) ?
                                                                                  httpContext.Request.QueryString["contentTypeId"] :
                                                                                  httpContext.Request.Form["contentTypeId"]);
            }

            var authorized = AuthorizationService.Authorize(httpContext.Request.RequestContext.RouteData.Values, httpContext.Request.Url.Query);

            if (!string.IsNullOrEmpty(_accessRoleName))
            {
                authorized = authorized && httpContext.User.IsInRole(_accessRoleName);
            }

            return authorized;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
            else if (filterContext.HttpContext.Session["ChangedRegion"] != null) //ChangedRegion is only set by the ChangeRegion action meaning the admin's region was just changed
            {
                filterContext.HttpContext.Session.Remove("ChangedRegion");
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    { "action", "Index" },
                    { "controller", "Home" }
                });
            }
            else
            {
                filterContext.Result = new HttpStatusCodeResult(403);
            }
        }
    }
}