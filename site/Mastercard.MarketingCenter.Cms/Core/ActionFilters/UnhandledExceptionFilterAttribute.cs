﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Exceptions;
using Pulsus;
using Resources;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Core.ActionFilters
{
    internal class UnhandledExceptionFilterAttribute : FilterAttribute, IExceptionFilter
    {
        private readonly IStandardResponseService _standardResponseService;
        public UnhandledExceptionFilterAttribute()
        {
            _standardResponseService = DependencyResolver.Current.GetService<IStandardResponseService>();
        }

        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception is NotFoundException)
            {
                LogManager.EventFactory.Create()
                                          .Level(LoggingEventLevel.Information)
                                          .AddTags(Constants.ErrorCodes.NotFound)
                                          .Text("Not Found")
                                          .AddData("Exception Message", filterContext.Exception.Message)
                                          .AddData("Exception InnerException", filterContext.Exception.InnerException)
                                          .AddData("Exception Data", filterContext.Exception.Data)
                                          .AddData("Exception StackTrace", filterContext.Exception.StackTrace)
                                          .Push();

                // 404
                UrlService.RedirectTo(filterContext, "Error", "NotFound");
            }
            else if (filterContext.Exception is AuthorizationException)
            {
                LogManager.EventFactory.Create()
                                       .Level(LoggingEventLevel.Information)
                                       .AddTags(Constants.ErrorCodes.AccessDenied)
                                       .Text("Access Denied")
                                       .AddData("Exception Message", filterContext.Exception.Message)
                                       .AddData("Exception InnerException", filterContext.Exception.InnerException)
                                       .AddData("Exception Data", filterContext.Exception.Data)
                                       .AddData("Exception StackTrace", filterContext.Exception.StackTrace)
                                       .Push();

                // 403
                // When handling 403 we should not redirect so that the URL does not change.  Only set the status code
                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.StatusCode = 403;
            }
            else if (filterContext.Exception is NoBusinessOwnerException)
            {
                LogManager.EventFactory.Create()
                                       .Level(LoggingEventLevel.Information)
                                       .AddTags(Constants.ErrorCodes.AccessDenied)
                                       .Text("Access Denied - No Business Owner")
                                       .AddData("Exception Message", filterContext.Exception.Message)
                                       .AddData("Exception InnerException", filterContext.Exception.InnerException)
                                       .AddData("Exception Data", filterContext.Exception.Data)
                                       .AddData("Exception StackTrace", filterContext.Exception.StackTrace)
                                       .Push();

                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.StatusCode = 400;
                filterContext.Result = _standardResponseService.GetErrorResponse("NoBusinessOwner", ContentItemComments.NoBusinessOwnerErrorMessage);
            }
            else if (filterContext.Exception is BusinessOwnerDisabledRegionException)
            {
                LogManager.EventFactory.Create()
                                       .Level(LoggingEventLevel.Information)
                                       .AddTags(Constants.ErrorCodes.NotFound)
                                       .Text("Not Found - Business Owner Disabled Region")
                                       .AddData("Exception Message", filterContext.Exception.Message)
                                       .AddData("Exception InnerException", filterContext.Exception.InnerException)
                                       .AddData("Exception Data", filterContext.Exception.Data)
                                       .AddData("Exception StackTrace", filterContext.Exception.StackTrace)
                                       .Push();

                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.StatusCode = 404;
                filterContext.Result = new RedirectResult("/portal/error404");
            }
            else
            {
                LogManager.EventFactory.Create()
                                       .Level(LoggingEventLevel.Error)
                                       .AddTags("error unhandled exception")
                                       .Text("Unhandled exception error in Admin")
                                       .AddData("Exception Message", filterContext.Exception.Message)
                                       .AddData("Exception InnerException", filterContext.Exception.InnerException)
                                       .AddData("Exception Data", filterContext.Exception.Data)
                                       .AddData("Exception StackTrace", filterContext.Exception.StackTrace)
                                       .Push();

                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.StatusCode = 500;
                filterContext.Result = new RedirectResult("/admin/error");
            }
        }
    }
}