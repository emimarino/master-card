﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Exceptions;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Web;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Core.ActionFilters
{
    public class BusinessOwnerExpirationManagementEnabledAttribute : AuthorizeAttribute
    {
        protected UserContext UserContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
        protected ISettingsService SettingsService { get { return DependencyResolver.Current.GetService<ISettingsService>(); } }
        private bool isBusinessOwnerEnabled { get; set; }

        public BusinessOwnerExpirationManagementEnabledAttribute() : base()
        {
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            isBusinessOwnerEnabled = SettingsService.GetBusinessOwnerExpirationManagementEnabled(UserContext.SelectedRegion);
            return isBusinessOwnerEnabled;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
            else if (!isBusinessOwnerEnabled)
            {
                throw new BusinessOwnerDisabledRegionException("User authorized but business owner is disabled for the region");
            }
            else
            {
                throw new AuthorizationException("Unauthorized");
            }
        }
    }
}