﻿using System;
using System.Web.Mvc;
using Mastercard.MarketingCenter.Cms.Core.Infrastructure;
using Mastercard.MarketingCenter.Data;

namespace Mastercard.MarketingCenter.Cms.Core.ActionFilters
{
    public class UnitOfWorkAttribute : ActionFilterAttribute
    {
        public IUnitOfWork UnitOfWork { get; set; }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            if (this.UnitOfWork != null && filterContext.Exception == null)
            {
                this.UnitOfWork.Commit();
            }
        }
    }
}