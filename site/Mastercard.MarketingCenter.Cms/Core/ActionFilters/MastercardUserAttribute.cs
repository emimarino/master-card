﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Exceptions;
using System.Web;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Core.ActionFilters
{
    public class MastercardUserAttribute : AuthorizeAttribute
    {
        protected UserContext UserContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }

        public MastercardUserAttribute() : base()
        {
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!base.AuthorizeCore(httpContext))
            {
                return false;
            }

            if (httpContext == null || httpContext.User == null || httpContext.User.Identity == null || httpContext.User.Identity.Name == null ||
                httpContext.Request == null || httpContext.Request.RequestContext == null || httpContext.Request.RequestContext.RouteData == null)
            {
                return false;
            }

            return UserContext.IsMastercardUser;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
            else
            {
                throw new AuthorizationException("Unauthorized");
            }
        }
    }
}