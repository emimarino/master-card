﻿using Autofac;
using Autofac.Integration.Mvc;
using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using System.Reflection;
using Module = Autofac.Module;

namespace Mastercard.MarketingCenter.Cms.Core.Modules
{
    public class MvcModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterControllers(Assembly.GetExecutingAssembly())
                   .PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);

            builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinderProvider();

            builder.RegisterType<CustomAuthorizeAttribute>().PropertiesAutowired();

            builder.RegisterFilterProvider();
        }
    }
}