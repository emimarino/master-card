﻿using Mastercard.MarketingCenter.Cms.Models;
using Mastercard.MarketingCenter.Cms.Services.Core;
using Mastercard.MarketingCenter.Cms.Services.Models;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Common.Models;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Models;
using Newtonsoft.Json;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using MMCServices = Mastercard.MarketingCenter.Services;

namespace Mastercard.MarketingCenter.Cms.Core.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static MvcFormGroupDiv BeginFormGroup<TModel>(this HtmlHelper<TModel> html, string key, string additionalClass = "")
        {
            var tagBuilder = new TagBuilder("div");

            var hasErrors = html.ViewData.ModelState.ContainsKey(key) && html.ViewData.ModelState[key].Errors.Any();

            tagBuilder.Attributes.Add("class", (hasErrors ? "form-group has-error " : "form-group ") + additionalClass);

            html.ViewContext.Writer.Write(tagBuilder.ToString(TagRenderMode.StartTag));

            var mvcFormGroupDiv = new MvcFormGroupDiv(html.ViewContext.Writer);

            return mvcFormGroupDiv;
        }

        public static MvcFormGroupDiv BeginBusinessOwner<TModel>(this HtmlHelper<TModel> html, string key)
        {
            var tagBuilder = new TagBuilder("div");

            var hasErrors = html.ViewData.ModelState.ContainsKey(key) && html.ViewData.ModelState[key].Errors.Any();

            tagBuilder.Attributes.Add("class", hasErrors ? "mc-row has-error" : "mc-row");

            html.ViewContext.Writer.Write(tagBuilder.ToString(TagRenderMode.StartTag));

            var mvcFormGroupDiv = new MvcFormGroupDiv(html.ViewContext.Writer);

            return mvcFormGroupDiv;
        }

        public static MvcHtmlString RenderMaxSize(this HtmlHelper html, FieldDefinition fd)
        {
            var message = string.Empty;
            switch (fd.FieldType.FieldTypeCode)
            {
                case FieldType.HtmlText:
                case FieldType.SingleLineText:

                    message = fd.Length > 0 ?
                    string.Format(Resources.Editors.MaxTextLengthMessage, fd.Length.ToString()) : Resources.Editors.NoLimit;
                    break;

                case FieldType.Image:
                    if (!string.IsNullOrEmpty(fd.CustomSettings))
                    {
                        var settings = fd.GetSettings<FileTypeSettings>();
                        message = fd.CustomSettings.Contains("Recommendations") ? settings.Recommendations : string.Empty;
                    }
                    break;
                case FieldType.SecondaryImage:
                case FieldType.Attachment:
                case FieldType.FileUpload:
                    if (!string.IsNullOrEmpty(fd.CustomSettings))
                    {
                        var settings = fd.GetSettings<FileTypeSettings>();

                        var maxSize = fd.CustomSettings.Contains("MaxSize") ? (float?)settings.MaxSize : null;
                        var unit = string.Empty;
                        if (maxSize != null && maxSize >= 1)
                        {
                            unit = "Gb";
                        }
                        else
                        {
                            unit = "Mb";
                            maxSize = maxSize * 1024;
                        }

                        var maxSizeString = maxSize != null ? string.Format(Resources.Editors.MaxSizeMessage, ((int)maxSize).ToString()) : string.Empty;
                        var AcceptFiles = fd.CustomSettings.Contains("AcceptFiles") ? string.Join(", ", settings.AcceptFiles) : string.Empty;
                        message = string.Join(" ", AcceptFiles, maxSizeString + unit);
                    }
                    break;
            }
            return new MvcHtmlString(string.IsNullOrEmpty(message) ? string.Empty : "(" + message.TrimStart(' ').TrimEnd(' ') + ")");
        }

        public static MvcHtmlString RenderMaxSize(this HtmlHelper html, ContentTypeFieldDefinition ctf)
        {
            return RenderMaxSize(html, ctf.FieldDefinition);
        }

        public static MvcHtmlString RenderClientEvents(this HtmlHelper<ListItemModel> html)
        {
            var list = DependencyResolver.Current.GetService<ListDao>().GetById(html.ViewData.Model.ListId);
            var action = html.ViewData.Model.IdFieldValue == null ? "new" : "update";
            var clientEvents = list.ListClientEvents.Where(o => string.Equals(o.FiredOn, action, StringComparison.InvariantCultureIgnoreCase));

            RenderOfferTypeClientEvents(clientEvents);
            RenderCategoryClientEvents(clientEvents);
            RenderCardExclusivityClientEvents(clientEvents);

            return html.Partial("_ClientEventCode", clientEvents.Select(o => new KeyValuePair<string, string>(o.ContentAction, o.ClientEventCode)));
        }

        public static MvcHtmlString RenderClientEvents(this HtmlHelper<ListModel> html, string action)
        {
            int listId = 0;
            if (!int.TryParse(html.ViewData.Model.Id.ToString(), out listId))
            {
                return null;
            }

            var list = DependencyResolver.Current.GetService<ListDao>().GetById(listId);
            var clientEvents = list.ListClientEvents.Where(o => o.FiredOn.Equals(action, StringComparison.InvariantCultureIgnoreCase));

            RenderOfferTypeClientEvents(clientEvents);
            RenderCategoryClientEvents(clientEvents);
            RenderCardExclusivityClientEvents(clientEvents);

            return new MvcHtmlString(string.Join(";", clientEvents.Select(o => o.ClientEventCode)));
        }

        public static MvcHtmlString RenderClientEvents(this HtmlHelper<ListModel> html, string refreshAction, string contentAction, string contentTypeId)
        {
            var contentType = DependencyResolver.Current.GetService<ContentTypeDao>().GetById(contentTypeId);
            var clientEvents = contentType.ContentTypeClientEvents
                                          .Where(o => o.FiredOn.Equals(refreshAction, StringComparison.InvariantCultureIgnoreCase)
                                                   && (o.ContentAction == null || o.ContentAction.Equals(contentAction, StringComparison.InvariantCultureIgnoreCase)));

            RenderOfferClientEvents(clientEvents);

            return html.Partial("_ClientEventCodeAfterPost", clientEvents.Select(o => new KeyValuePair<string, string>(o.ContentAction, o.ClientEventCode)));
        }

        public static MvcHtmlString RenderClientEvents(this HtmlHelper<ContentModel> html)
        {
            var contentType = DependencyResolver.Current.GetService<ContentTypeDao>().GetById(html.ViewData.Model.ContentTypeId);
            var action = html.ViewData.Model.IdFieldValue == null ? "new" : "update";
            var clientEvents = contentType.ContentTypeClientEvents
                                          .Where(o => o.FiredOn.Equals(action, StringComparison.InvariantCultureIgnoreCase));

            RenderOfferClientEvents(clientEvents);

            return html.Partial("_ClientEventCode", clientEvents.Select(o => new KeyValuePair<string, string>(o.ContentAction, o.ClientEventCode)));
        }

        private static void RenderOfferClientEvents(IEnumerable<ContentTypeClientEvent> clientEvents)
        {
            var urlService = DependencyResolver.Current.GetService<IUrlService>();
            if (clientEvents.Any(ce => ce.ContentTypeId.Equals(MarketingCenterDbConstants.ContentTypeIds.Offer, StringComparison.InvariantCultureIgnoreCase)
                                    && ce.ClientEventCode.Contains(urlService.GetRelativeReloadOfferModelByTokenURL())))
            {
                DependencyResolver.Current.GetService<IJsonWebTokenService>()
                                          .GetAsync(
                                                    new string[] { urlService.GetReloadOfferModelByTokenURL(false), urlService.GetReloadOfferModelByTokenURL(true) },
                                                    new CacheInvalidateOfferModelJsonWebToken()
                                                   );
            }
        }

        private static void RenderOfferTypeClientEvents(IEnumerable<ListClientEvent> clientEvents)
        {
            var urlService = DependencyResolver.Current.GetService<IUrlService>();
            if (clientEvents.Any(ce => ce.ListId.ToString().Equals(MarketingCenterDbConstants.MarketingCalendarListIds.OfferType, StringComparison.InvariantCultureIgnoreCase)
                                    && ce.ClientEventCode.Contains(urlService.GetRelativeReloadOfferModelByTokenURL())))
            {
                DependencyResolver.Current.GetService<IJsonWebTokenService>()
                                          .GetAsync(
                                                    new string[] { urlService.GetReloadOfferModelByTokenURL(false), urlService.GetReloadOfferModelByTokenURL(true) },
                                                    new CacheInvalidateOfferModelJsonWebToken()
                                                   );
            }
        }

        private static void RenderCategoryClientEvents(IEnumerable<ListClientEvent> clientEvents)
        {
            var urlService = DependencyResolver.Current.GetService<IUrlService>();
            if (clientEvents.Any(ce => ce.ListId.ToString().Equals(MarketingCenterDbConstants.MarketingCalendarListIds.Category, StringComparison.InvariantCultureIgnoreCase)
                                    && ce.ClientEventCode.Contains(urlService.GetRelativeReloadCategoryModelByTokenURL())))
            {
                DependencyResolver.Current.GetService<IJsonWebTokenService>()
                                          .GetAsync(
                                                    new string[] { urlService.GetReloadCategoryModelByTokenURL(false), urlService.GetReloadCategoryModelByTokenURL(true) },
                                                    new CacheInvalidateCategoryModelJsonWebToken()
                                                   );
            }
        }

        private static void RenderCardExclusivityClientEvents(IEnumerable<ListClientEvent> clientEvents)
        {
            var urlService = DependencyResolver.Current.GetService<IUrlService>();
            if (clientEvents.Any(ce => ce.ListId.ToString().Equals(MarketingCenterDbConstants.MarketingCalendarListIds.CardExclusivity, StringComparison.InvariantCultureIgnoreCase)
                                    && ce.ClientEventCode.Contains(urlService.GetRelativeReloadCardExclusivityModelByTokenURL())))
            {
                DependencyResolver.Current.GetService<IJsonWebTokenService>()
                                          .GetAsync(
                                                    new string[] { urlService.GetReloadCardExclusivityModelByTokenURL(false), urlService.GetReloadCardExclusivityModelByTokenURL(true) },
                                                    new CacheInvalidateCardExclusivityModelJsonWebToken()
                                                   );
            }
        }

        public static MvcFormGroupDiv BeginFormGroup<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            var tagBuilder = new TagBuilder("div");
            var key = ((MemberExpression)expression.Body).Member.Name;
            var hasErrors = html.ViewData.ModelState.ContainsKey(key) && html.ViewData.ModelState[key].Errors.Any();

            tagBuilder.Attributes.Add("class", hasErrors ? "form-group has-error" : "form-group");

            html.ViewContext.Writer.Write(tagBuilder.ToString(TagRenderMode.StartTag));

            var mvcFormGroupDiv = new MvcFormGroupDiv(html.ViewContext.Writer);

            return mvcFormGroupDiv;
        }

        public static VanityUrlSettings GetVanityUrlSettings(this HtmlHelper helper, FieldDefinition fieldDefinition)
        {
            if (!string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                return fieldDefinition.GetSettings<VanityUrlSettings>();
            }

            throw new InvalidOperationException($"There are no settings defined for the '{fieldDefinition.Name}' Vanity Url Field Type");
        }

        public static string GetNewUniqueId(this HtmlHelper helper)
        {
            var service = DependencyResolver.Current.GetService<IdGeneratorService>();
            return service.GetNewUniqueId();
        }

        public static string GetFieldDefinitionDefaultValue(this HtmlHelper helper, string fieldDefinitionName, string regionId)
        {
            var service = DependencyResolver.Current.GetService<ContentService>();
            return service.GetFieldDefinitionDefaultValue(fieldDefinitionName, regionId);
        }

        public static MvcHtmlString GlyphButton(this HtmlHelper helper, string text, string action, string controller, object linkAttributes, object innerAttributes)
        {
            return GlyphButton(helper, text, action, controller, null, linkAttributes, innerAttributes);
        }

        public static MvcHtmlString GlyphButton(this HtmlHelper helper, string text, string action, string controller,
            object routeData, object linkAttributes, object innerAttributes)
        {
            var url = UrlHelper.GenerateUrl(null, action, controller, new RouteValueDictionary(routeData),
                helper.RouteCollection, helper.ViewContext.RequestContext, true);

            var iTagBuilder = new TagBuilder("i");
            iTagBuilder.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(innerAttributes));

            var innerHtml = $"{iTagBuilder.ToString(TagRenderMode.Normal)}&nbsp;&nbsp;{text}";

            var tagBuilder = new TagBuilder("a")
            {
                InnerHtml = innerHtml
            };

            tagBuilder.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(linkAttributes));
            tagBuilder.MergeAttribute("href", url);

            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString BuildSimpleLookup(this HtmlHelper<FieldDefinition> helper, string fieldname, string data)
        {
            var fieldDef = helper.ViewData.Model;
            if (string.IsNullOrEmpty(fieldDef.CustomSettings))
            {
                throw new InvalidOperationException($"There are no settings defined for the '{fieldname}' Lookup Field Type");
            }

            var userContext = DependencyResolver.Current.GetService<MMCServices.UserContext>();
            var lookupSettings = fieldDef.GetSettings<LookupSettings>();
            lookupSettings.RegionId = userContext.SelectedRegion;

            var lookupServices = DependencyResolver.Current.GetService<LookupServices>();
            var lookupValues = lookupServices.GetLookupValues(lookupSettings);

            var items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "-- Select --", Value = string.Empty });

            items.AddRange(lookupValues.Select(o => new SelectListItem
            {
                Text = o.Value,
                Value = o.Id,
                Selected = o.Id == data
            }));

            return helper.DropDownList(fieldname, items.OrderBy(o => o.Text), new { @class = "chosen-choices" });
        }

        public static MvcHtmlString BuildChoiceDropDown(this HtmlHelper<FieldDefinition> htmlHelper, IDictionary<string, object> contentItemData, FieldDefinition fieldDefinition, ChoiceSettings settings = null)
        {
            IDictionary<string, string> options = new Dictionary<string, string>();
            var fieldName = fieldDefinition.Name;
            var includeEmptyOption = false;
            var emptySetting = string.Empty;
            if (!string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                var choiceSettings = settings ?? fieldDefinition.GetChoiceSettings();
                options = choiceSettings.Options;
                includeEmptyOption = choiceSettings.IncludeEmptyChoice;
                emptySetting = string.IsNullOrEmpty(choiceSettings.EmptyChoiceText) ? "-- Select -- " : choiceSettings.EmptyChoiceText;
            }

            var data = string.Empty;

            if (!string.IsNullOrEmpty(fieldDefinition.DefaultValue))
            {
                data = fieldDefinition.DefaultValue;
            }

            if (contentItemData.ContainsKey(fieldName) && contentItemData[fieldName] != null)
            {
                data = contentItemData[fieldName].ToString();
            }
            var items = options.Select(o => new SelectListItem { Text = o.Key.Trim(), Value = o.Value.Trim(), Selected = (o.Value.Trim() == data.Trim() && !string.IsNullOrEmpty(data)) }).ToList();
            if (includeEmptyOption) { items.Add(new SelectListItem { Text = emptySetting, Value = "", Selected = string.IsNullOrEmpty(data) }); }
            return htmlHelper.DropDownList("_" + fieldName, items.OrderBy(o => o.Text), new { @class = "form-control" });
        }

        public static MvcHtmlString BuildUserLookup(this HtmlHelper<FieldDefinition> helper, string fieldname, string data)
        {
            var fieldDef = helper.ViewData.Model;
            var userService = DependencyResolver.Current.GetService<IUserService>();
            var users = userService.GetAll(false, true).ToList();
            var selectedUser = users.FirstOrDefault(v => v.UserId.ToString() == data);
            MvcHtmlString message = MvcHtmlString.Empty;
            if (selectedUser != null && selectedUser.IsDisabled)
            {
                message = MvcHtmlString.Create($"<label class=\"error\">The current Business Owner, {selectedUser.FullName}, is no longer a valid selection.</label>");
                data = fieldDef.DefaultValue ?? data;
            }

            var lookupValues = users.Where(x => !x.IsDisabled)
                .Select(u => new SelectListItem { Text = $"{u.FullName} ({u.Email})", Value = u.UserId.ToString(), Selected = (u.UserId.ToString() == data) })
                .OrderBy(o => o.Text);

            MvcHtmlString lookup = helper.DropDownList(
                fieldname,
                lookupValues,
                string.Empty,
                new { @class = "chosen-choices" });
            return message == MvcHtmlString.Empty ? lookup : MvcHtmlString.Create(lookup.ToHtmlString() + message.ToHtmlString());
        }

        public static MvcHtmlString BuildTagDropDownList(this HtmlHelper<FieldDefinition> helper, string fieldname, string contentItemId, string SelectedTagIdentifier = null)
        {
            var fieldDef = helper.ViewData.Model;
            if (string.IsNullOrEmpty(fieldDef.CustomSettings))
            {
                throw new InvalidOperationException($"No Custom Settings for field: {fieldDef.Name}");
            }

            var tagSettings = fieldDef.GetSettings<TagSettings>();
            var slamContext = DependencyResolver.Current.GetService<SlamContext>();

            var rootTag = slamContext.GetTagTree().FindNodes(o => o.TagCategory != null && o.TagCategory.TagCategoryId == tagSettings.TagCategoryId && o.Parent == null).FirstOrDefault();

            if (rootTag == null)
            {
                rootTag = new TagTreeNode();
            }

            var tagValues = Enumerable.Empty<KeyValueItem>();

            string _identifier = null;
            if (string.IsNullOrEmpty(SelectedTagIdentifier) ||
                (!string.IsNullOrEmpty(SelectedTagIdentifier) &&
                 !rootTag.Children.Any(st => st.Identifier?.Trim()?.ToLower() == SelectedTagIdentifier?.Trim()?.ToLower())))
            {
                if (!string.IsNullOrEmpty(contentItemId))
                {
                    tagValues = DependencyResolver.Current.GetService<TagServices>().GetSelectedTags(contentItemId);
                }
            }
            else
            {
                _identifier = SelectedTagIdentifier;
            }

            bool isDefaultTag = !tagValues.Any() || !rootTag.Children.Any(t => tagValues.Any(v => v.Id == t.Tag.Identifier));
            IEnumerable<SelectListItem> options = rootTag.Children.Select(t => new SelectListItem { Text = t.Tag.DisplayName, Value = t.Tag.Identifier, Selected = (string.IsNullOrEmpty(_identifier) ? (tagValues.Any(v => v.Id == t.Tag.Identifier) || (t.Tag.Identifier == fieldDef.DefaultValue && isDefaultTag)) : _identifier == t.Tag.Identifier) }).OrderBy(o => o.Text);
            if (!fieldDef.Required)
            {
                options = new SelectListItem[] { new SelectListItem { Text = $"-- Select {fieldDef.Description} --", Value = "", Selected = !options.Any(o => o.Selected) } }.Concat(options);
            }

            return helper.DropDownList(fieldname, options, new { @class = "chosen-choices" });
        }

        public static MvcHtmlString BuildSearchLanguagesTagDropDown(this HtmlHelper<FieldDefinition> helper, string fieldname, string contentItemId, string SelectedTagIdentifier = null)
        {
            var fieldDef = helper.ViewData.Model;
            if (string.IsNullOrEmpty(fieldDef.CustomSettings))
            {
                throw new InvalidOperationException($"No Custom Settings for field: {fieldDef.Name}");
            }

            var tagSettings = fieldDef.GetSettings<TagSettings>();
            var slamContext = DependencyResolver.Current.GetService<SlamContext>();
            var rc = new Slam.Cms.Configuration.RegionConfigManager();
            var uc = DependencyResolver.Current.GetService<MMCServices.UserContext>();
            var languages = rc.GetLanguages(uc.SelectedRegion).Select(l => l.Key.Trim());
            if (languages.Count() < 2)
            {
                return (languages.Count() == 1) ? helper.Hidden(fieldname, languages.FirstOrDefault()) :
                                                  helper.Hidden(fieldname, "en");
            }

            var rootTag = slamContext.GetTagTree().FindNodes(o => o.TagCategory != null && o.TagCategory.TagCategoryId == tagSettings.TagCategoryId && o.Parent == null).FirstOrDefault();

            if (rootTag == null)
            {
                rootTag = new TagTreeNode();
            }

            var filteredChildren = rootTag.Children.Where(o => (!string.IsNullOrWhiteSpace(o.Identifier) ? languages.Contains(o.Identifier?.Trim() ?? "") : false));

            var tagValues = Enumerable.Empty<KeyValueItem>();

            string _identifier = null;
            if (string.IsNullOrEmpty(SelectedTagIdentifier) || (!string.IsNullOrEmpty(SelectedTagIdentifier) && !filteredChildren.Any(st => st.Identifier?.Trim()?.ToLower() == SelectedTagIdentifier?.Trim()?.ToLower())))
            {
                if (!string.IsNullOrEmpty(contentItemId))
                {
                    tagValues = DependencyResolver.Current.GetService<TagServices>().GetSelectedTags(contentItemId);
                }
            }
            else
            {
                _identifier = SelectedTagIdentifier;
            }

            IEnumerable<SelectListItem> options = filteredChildren.Select(t => new SelectListItem { Text = t.Tag.DisplayName, Value = t.Tag.Identifier, Selected = (string.IsNullOrEmpty(_identifier) ? (tagValues.Any(v => v.Id == t.Tag.Identifier) || t.Tag.Identifier == fieldDef.DefaultValue) : _identifier == t.Tag.Identifier) }).OrderBy(o => o.Text);
            if (!fieldDef.Required)
            {
                options = new SelectListItem[] { new SelectListItem { Text = $"-- Select {fieldDef.Description} --", Value = "", Selected = !options.Any(o => o.Selected) } }.Concat(options);
            }
            return helper.DropDownList(fieldname, options, new { @class = "chosen-choices" });
        }

        public static MvcHtmlString BuildContentItemRegionVisibilityDropDown(this HtmlHelper<FieldDefinition> helper, string fieldname, string value)
        {
            SelectListItem selectList1 = new SelectListItem { Text = Constants.OfferState.No, Value = "0", Selected = value == "0" ? true : false };
            SelectListItem selectList2 = new SelectListItem { Text = Constants.OfferState.Yes, Value = "1", Selected = value == "1" ? true : false };
            SelectListItem selectList3 = new SelectListItem { Text = Constants.OfferState.Pending, Value = "2", Selected = value == "2" ? true : false };
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            selectListItems.Add(selectList1);
            selectListItems.Add(selectList2);
            selectListItems.Add(selectList3);
            return helper.DropDownList(fieldname, selectListItems, new { @class = "chosen-choices region-visibility-dropdown" });
        }

        public static MvcHtmlString BuildTagTree(this HtmlHelper helper, FieldDefinition fieldDefinition, string fieldValue, string contentItemId, bool renderBasedOnInterRegionClone = true)
        {
            if (string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                throw new InvalidOperationException($"No Custom Settings for field: {fieldDefinition.Name}");
            }

            var slamContext = DependencyResolver.Current.GetService<SlamContext>();
            var tagSettings = fieldDefinition.GetSettings<TagSettings>();
            var rootTag = slamContext.GetTagTree().FindNodes(o => o.TagCategory != null && o.TagCategory.TagCategoryId == tagSettings.TagCategoryId && o.Parent == null).FirstOrDefault();
            var tagValues = Enumerable.Empty<KeyValueItem>();

            if (renderBasedOnInterRegionClone)
            {
                if (!string.IsNullOrEmpty(contentItemId) && string.IsNullOrEmpty(fieldValue))
                {
                    tagValues = DependencyResolver.Current.GetService<TagServices>().GetSelectedTags(contentItemId);
                }
                else if (!string.IsNullOrEmpty(fieldValue))
                {
                    tagValues = new List<KeyValueItem>(fieldValue.Trim(',').Split(',').Select(v => new KeyValueItem { Id = v }));
                }
            }

            if (rootTag != null)
            {
                var ul = new TagBuilder("ul");
                ul.Attributes.Add("class", "branches-list parent-check-req");

                var sb = new StringBuilder();
                BuildTreeNode(helper, sb, rootTag, true, tagValues, fieldDefinition.Name);
                ul.InnerHtml = sb.ToString();

                return MvcHtmlString.Create(ul.ToString());
            }

            return MvcHtmlString.Empty;
        }

        public static string BuildMarketRegionTagList(this HtmlHelper helper, FieldDefinition fieldDefinition, string fieldValue, string contentItemId, string region)
        {
            if (string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                throw new InvalidOperationException($"No Custom Settings for field: {fieldDefinition.Name}");
            }

            var slamContext = DependencyResolver.Current.GetService<SlamContext>();
            var tagSettings = fieldDefinition.GetSettings<TagSettings>();
            var rootTag = slamContext.GetTagTree()
                                     .FindNodes(o => o.Parent != null
                                                && o.Parent.TagCategory != null
                                                && o.Parent.TagCategory.TagCategoryId == tagSettings.TagCategoryId
                                                && o.Identifier == region)
                                     .FirstOrDefault();

            var tagValues = Enumerable.Empty<KeyValueItem>();
            if (!string.IsNullOrEmpty(fieldValue))
            {
                tagValues = new List<KeyValueItem>(fieldValue.Trim(',').Split(',').Select(v => new KeyValueItem { Id = v }));
            }
            else if (!string.IsNullOrEmpty(contentItemId))
            {
                tagValues = DependencyResolver.Current.GetService<TagServices>().GetSelectedTags(contentItemId);
            }

            if (rootTag?.Children.Any() ?? false)
            {
                dynamic jsonMarkets = new { MarketOptions = new List<Dictionary<string, string>>() };
                foreach (var tag in rootTag.Children.OrderBy(t => t.Text))
                {
                    BuildMarketRegionTagJson(helper, jsonMarkets, tag, tagValues, fieldDefinition.Name);
                }

                return HttpUtility.HtmlDecode(JsonConvert.SerializeObject(jsonMarkets));
            }

            return string.Empty;
        }

        public static string BuildCrossBorderMarketRegionTagList(this HtmlHelper helper, FieldDefinition fieldDefinition, string fieldValue, string contentItemId, string region, bool disableOtherRegions)
        {
            if (string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                throw new InvalidOperationException($"No Custom Settings for field: {fieldDefinition.Name}");
            }

            var slamContext = DependencyResolver.Current.GetService<SlamContext>();
            var tagSettings = fieldDefinition.GetSettings<TagSettings>();
            var rootTag = slamContext.GetTagTree()
                                     .FindNodes(o => o.TagCategory != null
                                                && o.TagCategory.TagCategoryId == tagSettings.TagCategoryId
                                                && o.Parent == null)
                                     .FirstOrDefault();

            var tagValues = Enumerable.Empty<KeyValueItem>();
            if (!string.IsNullOrEmpty(fieldValue))
            {
                tagValues = new List<KeyValueItem>(fieldValue.Trim(',').Split(',').Select(v => new KeyValueItem { Id = v }));
            }
            else if (!string.IsNullOrEmpty(contentItemId))
            {
                tagValues = DependencyResolver.Current.GetService<TagServices>().GetSelectedTags(contentItemId);
            }

            if (rootTag?.Children.Any() ?? false)
            {
                dynamic items = new List<Dictionary<string, object>>();
                foreach (var tag in rootTag.Children.Where(t => tagSettings.IdsToExclude == null || !tagSettings.IdsToExclude.Any() ? true : !tagSettings.IdsToExclude.Contains(t.Identifier.Trim())).OrderBy(t => t.Text))
                {
                    BuildTagComponentJson(helper, items, tag, tagValues, fieldDefinition.Name, region, disableOtherRegions);
                }

                return HttpUtility.HtmlDecode(JsonConvert.SerializeObject(items));
            }

            return string.Empty;
        }

        public static string BuildLocationTagList(this HtmlHelper helper, string contentItemId)
        {
            var eventLocationService = DependencyResolver.Current.GetService<IEventLocationService>();
            var tagtree = eventLocationService.GetEventLocationTreeByContentId(contentItemId);
            return JsonConvert.SerializeObject(tagtree.RootNode);
        }

        public static MvcHtmlString HiddenForTagField(this HtmlHelper helper, FieldDefinition fieldDefinition, string fieldValue, string contentItemId, bool renderBasedOnInterRegionClone = true, object htmlAttributes = null)
        {
            if (string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                throw new InvalidOperationException($"No Custom Settings for field: {fieldDefinition.Name}");
            }

            var tagSettings = fieldDefinition.GetSettings<TagSettings>();
            if (!string.IsNullOrEmpty(contentItemId) && string.IsNullOrEmpty(fieldValue))
            {
                var selectedTags = renderBasedOnInterRegionClone ?
                    DependencyResolver.Current.GetService<TagServices>().GetItemsTagsForCategory(tagSettings.TagCategoryId, contentItemId).Select(o => o.Id)
                    : new List<string>();

                return helper.Hidden(fieldDefinition.Name, string.Join(",", selectedTags), htmlAttributes);
            }

            return helper.Hidden(fieldDefinition.Name, fieldValue, htmlAttributes);
        }

        public static MvcHtmlString HiddenDefaultForMarketTagField(this HtmlHelper helper, FieldDefinition fieldDefinition, string region)
        {
            if (string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                throw new InvalidOperationException($"No Custom Settings for field: {fieldDefinition.Name}");
            }

            string fieldName = $"{fieldDefinition.Name}-default";
            var tagSettings = fieldDefinition.GetSettings<TagSettings>();
            if (!string.IsNullOrEmpty(region))
            {
                var slamContext = DependencyResolver.Current.GetService<SlamContext>();
                var rootTags = slamContext.GetTagTree().FindNodes(o => o.Parent != null && o.Parent.TagCategory != null && o.Parent.TagCategory.TagCategoryId == tagSettings.TagCategoryId && region == "global" ? true : o.Identifier == region);

                if (!rootTags.Any())
                {
                    rootTags = slamContext.GetTagTree().FindNodes(o => o.Identifier == "global");
                }

                return helper.Hidden(fieldName, string.Join(",", rootTags.Select(t => t.Identifier)));
            }

            return helper.Hidden(fieldName);
        }

        public static MvcHtmlString HiddenDefaultForCrossBorderMarketTagField(this HtmlHelper helper, FieldDefinition fieldDefinition, string contentItemId, string region, bool useFieldValueAsDefault)
        {
            if (string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                throw new InvalidOperationException($"No Custom Settings for field: {fieldDefinition.Name}");
            }

            string fieldName = $"{fieldDefinition.Name}-default";
            var tagSettings = fieldDefinition.GetSettings<TagSettings>();
            var slamContext = DependencyResolver.Current.GetService<SlamContext>();
            if (useFieldValueAsDefault && !string.IsNullOrEmpty(region) && !string.IsNullOrEmpty(contentItemId))
            {
                var defaultTags = DependencyResolver.Current.GetService<TagServices>().GetItemsTagsForOtherRegions(region, contentItemId).Select(o => o.Id);
                var rootTags = slamContext.GetTagTree().FindNodes(o => ((o.Parent != null && o.Parent.TagCategory != null && o.Parent.TagCategory.TagCategoryId == tagSettings.TagCategoryId && o.Identifier == region) || defaultTags.Contains(o.Identifier)) && (tagSettings.IdsToExclude == null || !tagSettings.IdsToExclude.Any() ? true : !tagSettings.IdsToExclude.Contains(o.Identifier.Trim())));

                return helper.Hidden(fieldName, string.Join(",", rootTags.Select(t => t.Identifier)));
            }
            else
            {
                var rootTags = slamContext.GetTagTree().FindNodes(o => o.Parent != null && o.Parent.TagCategory != null && o.Parent.TagCategory.TagCategoryId == tagSettings.TagCategoryId && (tagSettings.IdsToExclude == null || !tagSettings.IdsToExclude.Any() ? true : !tagSettings.IdsToExclude.Contains(o.Identifier.Trim())));

                return helper.Hidden(fieldName, string.Join(",", rootTags.Select(t => t.Identifier)));
            }
        }

        public static MvcHtmlString HiddenForLocationField(this HtmlHelper helper, FieldDefinition fieldDefinition, string fieldValue, string contentItemId, object htmlAttributes = null)
        {
            if (string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                throw new InvalidOperationException($"No Custom Settings for field: {fieldDefinition.Name}");
            }

            if (!string.IsNullOrEmpty(contentItemId) && string.IsNullOrEmpty(fieldValue))
            {
                var selectedLocations = DependencyResolver.Current.GetService<IEventLocationService>().GetEventLocationByContentItemId(contentItemId).Select(x => x.EventLocationId);

                return helper.Hidden(fieldDefinition.Name, string.Join(",", selectedLocations), htmlAttributes);
            }

            return helper.Hidden(fieldDefinition.Name, fieldValue, htmlAttributes);
        }

        public static MvcHtmlString BuildTagManager(this HtmlHelper helper, TagTreeNode root)
        {
            var ul = new TagBuilder("ul");
            ul.Attributes.Add("class", "folders-tree");
            var sb = new StringBuilder();

            foreach (var node in root.Children)
            {
                BuildTagManagerCore(sb, node);
            }

            ul.InnerHtml = sb.ToString();

            return MvcHtmlString.Create(ul.ToString());
        }

        public static MvcHtmlString BuildEventLocationManager(this HtmlHelper helper, EventLocationTreeNode root)
        {
            var ul = new TagBuilder("ul");
            ul.Attributes.Add("class", "folders-tree");
            var sb = new StringBuilder();

            foreach (var node in root.Children)
            {
                BuildEventLocationManagerCore(sb, node);
            }

            ul.InnerHtml = sb.ToString();

            return MvcHtmlString.Create(ul.ToString());
        }

        private static void BuildEventLocationManagerCore(StringBuilder stringBuilder, EventLocationTreeNode eventLocationTreeNode)
        {
            if (eventLocationTreeNode != null)
            {
                stringBuilder.Append("<li>");

                if (eventLocationTreeNode.Children.Any())
                    stringBuilder.AppendFormat(@"<input type='checkbox' id='{0}' class='opened-check {1}' /><label class='fa' for='{0}'></label>", Guid.NewGuid().ToString().ToLowerInvariant(), eventLocationTreeNode.IsRoot ? "root-node" : string.Empty);

                var item =
                    string.Format(
                        @"<span class='{0}'><span class='name' data-id='{2}' data-isroot='{4}'><span class='{1}'></span>{3}</span></span>",
                        eventLocationTreeNode.Children.Any() ? "folder" : "lbl",
                        eventLocationTreeNode.Children.Any() ? "fa" : "fa fa-file-o",
                        eventLocationTreeNode.EventLocationId,
                        eventLocationTreeNode.Title,
                        eventLocationTreeNode.IsRoot);

                stringBuilder.Append(item);

                if (eventLocationTreeNode.Children.Any())
                {
                    stringBuilder.Append("<div class='inner'><ul>");

                    foreach (var treeNode in eventLocationTreeNode.Children.OrderBy(o => o.Title))
                    {
                        BuildEventLocationManagerCore(stringBuilder, treeNode);
                    }

                    stringBuilder.Append("</ul></div>");
                }
                stringBuilder.Append("</li>");
            }
        }

        public static MvcHtmlString RenderListTrackingOrderingFields(this HtmlHelper<ListModel> helper)
        {
            var orderableColumns = helper.ViewData.Model.Columns.Where(o => o.HasDefaultOrder).ToList();
            var sb = new StringBuilder();

            if (helper.ViewData.Model.ShowTrackingFields)
            {
                if (!orderableColumns.Any())
                {
                    orderableColumns.Add(new ColumnModel { Name = helper.ViewData.Model.ModifiedDateFieldName, Order = helper.ViewData.Model.Columns.Count() + 1, OrderDirection = "desc" });
                }

                sb.AppendLine(
                    "colsArray.push({ 'data': '{CreatedDateFieldName}', 'title': 'Created Date', render : datetime_template, 'searchable': 'false', 'orderable': 'true' });"
                        .Replace("{CreatedDateFieldName}", helper.ViewData.Model.CreatedDateFieldName));

                sb.AppendLine(
                    "colsArray.push({ 'data': '{ModifiedDateFieldName}', 'title': 'Modified Date', render : datetime_template, 'searchable': 'false', 'orderable': 'true' });"
                        .Replace("{ModifiedDateFieldName}", helper.ViewData.Model.ModifiedDateFieldName));
            }

            foreach (var column in orderableColumns)
            {
                var index = orderableColumns.IndexOf(column) + 1 + column.Order;
                sb.AppendLine($"var arr = [{index}, '{column.OrderDirection}']");
                sb.AppendLine("defaultOrderArray.push(arr)");
            }

            return new MvcHtmlString(sb.ToString());
        }

        public static string GetFileUploadLocationSetting(this HtmlHelper helper, FieldDefinition fieldDefinition)
        {
            var fileUploadSettings = fieldDefinition.GetSettings<FileUploadSettings>();

            return string.IsNullOrEmpty(fieldDefinition.CustomSettings) || string.IsNullOrEmpty(fileUploadSettings.UploadLocationSetting) ? "FilesFolder" : fileUploadSettings.UploadLocationSetting;
        }

        public static bool IsCopyOrClonePostBack(this HtmlHelper helper, string contentItemID, HttpContextBase context)
        {
            var action = context?.Request?.RequestContext?.RouteData?.Values["action"]?.ToString()?.ToLower();
            var posibleStrings = new[] { "copy", "clone" };

            return string.IsNullOrEmpty(contentItemID) && (action == null ? false : posibleStrings.Contains(action));
        }

        private static void BuildTreeNode(HtmlHelper helper, StringBuilder stringBuilder, TagTreeNode tagTreeNode, bool firstPass, IEnumerable<KeyValueItem> tagValues, string fieldName)
        {
            if (tagTreeNode != null)
            {
                if (!firstPass)
                {
                    stringBuilder.Append("<li>");
                }

                if (tagTreeNode.Parent != null)
                {
                    string parentTitle;
                    var parent = tagTreeNode.Parent;
                    while (parent.TagCategory == null && parent.Parent != null)
                    {
                        parent = parent.Parent;
                    }

                    parentTitle = parent?.TagCategory != null ? parent.TagCategory.Title : "Select All";

                    if (tagTreeNode.Children.Any())
                    {
                        stringBuilder.AppendFormat(
                            @"<input type='checkbox' id='{0}' class='box-trigger' />
                              <label class='branch-trigger' for='{0}'>&#x25BA;</label>",
                            Guid.NewGuid().ToString().ToLowerInvariant());
                    }

                    var checkbox =
                        string.Format(
                            @"<div class='{0} checkbox'>
                                <label>
                                    <input type='checkbox' name='{1}' value='{2}' value-label='{4}' data-tag-id='{5}'  data-tag-group='{6}'  {3} />{4}
                                </label>
                              </div>",
                            (tagTreeNode.Children.Any() ? "branch-parent" : string.Empty),
                            helper.ViewData.TemplateInfo.GetFullHtmlFieldName(fieldName),
                            tagTreeNode.Identifier,
                            (tagValues.Any(o => o.Id == tagTreeNode.Identifier) ? "checked" : string.Empty),
                            HttpUtility.HtmlAttributeEncode(string.IsNullOrEmpty(tagTreeNode.Text) ? tagTreeNode.Identifier : tagTreeNode.Text),
                            (tagTreeNode?.Tag?.TagId ?? ""),
                            parent.TagCategory.Title
                        );

                    stringBuilder.Append(checkbox);
                }

                if (tagTreeNode.Children.Any())
                {
                    if (!firstPass)
                    {
                        stringBuilder.Append("<div class='branch'><ul>");
                    }

                    foreach (var treeNode in tagTreeNode.Children.OrderBy(t => t.Text))
                    {
                        BuildTreeNode(helper, stringBuilder, treeNode, false, tagValues, fieldName);
                    }

                    if (!firstPass)
                    {
                        stringBuilder.Append("</ul></div>");
                    }
                }

                if (!firstPass)
                {
                    stringBuilder.Append("</li>");
                }
            }
        }

        private static void BuildMarketRegionTagJson(HtmlHelper helper, dynamic jsonMarkets, TagTreeNode tagTreeNode, IEnumerable<KeyValueItem> tagValues, string fieldName)
        {
            if (tagTreeNode?.Parent != null)
            {
                var parent = tagTreeNode.Parent;
                while (parent.TagCategory == null && parent.Parent != null)
                {
                    parent = parent.Parent;
                }

                var options = jsonMarkets.MarketOptions as List<Dictionary<string, string>>;
                options.Add(new Dictionary<string, string>
                {
                    { "class", tagTreeNode.Children.Any() ? "branch-parent" : string.Empty },
                    { "name",  helper.ViewData.TemplateInfo.GetFullHtmlFieldName(fieldName) },
                    { "value", tagTreeNode.Identifier },
                    { "isChecked", tagValues.Any(o => o.Id == tagTreeNode.Identifier) ? "checked" : string.Empty },
                    { "valueLabel", HttpUtility.HtmlAttributeEncode(string.IsNullOrEmpty(tagTreeNode.Text) ? tagTreeNode.Identifier : tagTreeNode.Text) },
                    { "tagId", tagTreeNode?.Tag?.TagId ?? "" },
                    { "tagGroup", parent.TagCategory.Title },
                });
            }
        }

        private static void BuildTagComponentJson(HtmlHelper helper, dynamic items, TagTreeNode tagTreeNode, IEnumerable<KeyValueItem> tagValues, string fieldName, string region = "", bool disableOtherRegions = false)
        {
            if (tagTreeNode?.Parent != null)
            {
                var parent = tagTreeNode.Parent;
                while (parent.TagCategory == null && parent.Parent != null)
                {
                    parent = parent.Parent;
                }

                dynamic children = new List<Dictionary<string, object>>();
                foreach (var tag in tagTreeNode.Children.OrderBy(t => t.Text))
                {
                    BuildTagComponentJson(helper, children, tag, tagValues, fieldName);
                }

                items.Add(new Dictionary<string, object>
                {
                    { "name",  helper.ViewData.TemplateInfo.GetFullHtmlFieldName(fieldName) },
                    { "value", tagTreeNode.Identifier },
                    { "isChecked", tagValues.Any(o => o.Id == tagTreeNode.Identifier) },
                    { "valueLabel", HttpUtility.HtmlAttributeEncode(string.IsNullOrEmpty(tagTreeNode.Text) ? tagTreeNode.Identifier : tagTreeNode.Text) },
                    { "tagId", tagTreeNode?.Tag?.TagId ?? "" },
                    { "disabled", disableOtherRegions ? !tagTreeNode.Identifier.Equals(region, StringComparison.InvariantCultureIgnoreCase) : false },
                    { "children", children }
                });
            }
        }

        private static void BuildTagManagerCore(StringBuilder stringBuilder, TagTreeNode tagTreeNode)
        {
            if (tagTreeNode != null)
            {
                stringBuilder.Append("<li>");

                if (tagTreeNode.Children.Any())
                    stringBuilder.AppendFormat(@"<input type='checkbox' id='{0}' class='opened-check' /><label class='fa' for='{0}'></label>", Guid.NewGuid().ToString().ToLowerInvariant());

                var item =
                    string.Format(
                        @"<span class='{0}'><span class='name' data-catid='{2}' data-parentid='{3}' data-id='{4}'><span class='{1}'></span>{5}</span></span>",
                        tagTreeNode.Children.Any() ? "folder" : "lbl",
                        tagTreeNode.Children.Any() ? "fa" : "fa fa-file-o",
                        tagTreeNode.TagCategory != null ? tagTreeNode.TagCategory.TagCategoryId : "", tagTreeNode.Id,
                        (tagTreeNode.Tag != null ? tagTreeNode.Tag.TagId : string.Empty), tagTreeNode.Text);

                stringBuilder.Append(item);

                if (tagTreeNode.Children.Any())
                {
                    stringBuilder.Append("<div class='inner'><ul>");

                    foreach (var treeNode in tagTreeNode.Children.OrderBy(o => o.Text))
                    {
                        BuildTagManagerCore(stringBuilder, treeNode);
                    }

                    stringBuilder.Append("</ul></div>");
                }
                stringBuilder.Append("</li>");
            }
        }

        public static MvcHtmlString BuildMultipleLookup(this HtmlHelper<FieldDefinition> helper, string fieldname, string data, object id, bool useOnlyDefaultValues = false)
        {
            var fieldDef = helper.ViewData.Model;
            if (fieldDef == null)
            {
                throw new InvalidOperationException($"No Field Definition for the field {fieldname}");
            }

            if (string.IsNullOrEmpty(fieldDef.CustomSettings))
            {
                throw new InvalidOperationException($"There are no settings defined for the '{fieldname}' Lookup Field Type");
            }

            var userContext = DependencyResolver.Current.GetService<MMCServices.UserContext>();
            var lookupSettings = fieldDef.GetSettings<LookupSettings>();
            lookupSettings.RegionId = userContext.SelectedRegion;
            if (!lookupSettings.AssociationKeysMap.Any() || string.IsNullOrEmpty(lookupSettings.AssociationTableName))
            {
                throw new InvalidOperationException($"No AssociationKeysMap or AssociationTableName defined for field: {fieldname}");
            }

            var htmlAttributes = new Dictionary<string, object> { { "class", "chosen-choices" }, { "tabindex", "-1" } };
            if (!string.IsNullOrEmpty(fieldDef.DefaultValue))
            {
                htmlAttributes.Add("data-placeholder", fieldDef.DefaultValue);
            }
            if (!lookupSettings.ForceDisableMultiSelection)
            {
                htmlAttributes.Add("multiple", "multiple");
            }

            var lookupServices = DependencyResolver.Current.GetService<LookupServices>();
            var lookupValues = lookupServices.GetLookupValues(lookupSettings, helper.ViewData["IsReadOnly"] != null ? (bool)helper.ViewData["IsReadOnly"] : false);
            var selectedValues = Enumerable.Empty<string>();
            if (id != null && !useOnlyDefaultValues)
            {
                selectedValues = lookupServices.GetSelectedLookupValues(id, lookupSettings);
            }
            if (!string.IsNullOrEmpty(data))
            {
                selectedValues = selectedValues.Union(data.Split(','));
            }

            var items = new List<SelectListItem>();
            if (!fieldDef.Required && lookupSettings.ForceDisableMultiSelection)
            {
                // empty value should be added for non-multiple, not required fields
                items.Add(new SelectListItem { Text = "-- Select --", Value = string.Empty });
            }

            if (lookupSettings.IncludeSelectAllChoice)
            {
                items.Add(new SelectListItem
                {
                    Text = lookupSettings.SelectAllChoiceText.IsNullOrWhiteSpace() ? "Select All" : lookupSettings.SelectAllChoiceText,
                    Value = "[select-all]"
                });
            }

            items.AddRange(
                            lookupValues.Select(
                                                o =>
                                                    new SelectListItem
                                                    {
                                                        Text = o.Value,
                                                        Value = o.Id,
                                                        Selected = selectedValues.Any(lkp => lkp != null && lkp.Trim().Equals(o.Id.Trim()))
                                                    }));

            return helper.DropDownList(fieldname, items.OrderBy(o => o.Text), htmlAttributes);
        }

        public static MvcHtmlString BuildMultipleLookupGroup(this HtmlHelper<FieldDefinition> helper, string fieldname, string data, object id, bool useOnlyDefaultValues = false)
        {
            var fieldDef = helper.ViewData.Model;
            if (fieldDef == null)
            {
                throw new InvalidOperationException($"No Field Definition for the field {fieldname}");
            }

            if (string.IsNullOrEmpty(fieldDef.CustomSettings))
            {
                throw new InvalidOperationException($"There are no settings defined for the '{fieldname}' Lookup Field Type");
            }

            var userContext = DependencyResolver.Current.GetService<MMCServices.UserContext>();
            var lookupSettings = fieldDef.GetSettings<LookupGroupSettings>();
            lookupSettings.RegionId = userContext.SelectedRegion;
            if (!lookupSettings.AssociationKeysMap.Any() || string.IsNullOrEmpty(lookupSettings.AssociationTableName))
            {
                throw new InvalidOperationException($"No AssociationKeysMap or AssociationTableName defined for field: {fieldname}");
            }

            var htmlAttributes = new Dictionary<string, object> { { "class", "chosen-choices" }, { "tabindex", "-1" } };
            htmlAttributes.Add("multiple", "multiple");
            if (!string.IsNullOrEmpty(fieldDef.DefaultValue))
            {
                htmlAttributes.Add("data-placeholder", fieldDef.DefaultValue);
            }

            var lookupServices = DependencyResolver.Current.GetService<LookupServices>();
            var lookupValues = lookupServices.GetMultipleLookupGroupValues(lookupSettings);
            var selectedValues = Enumerable.Empty<string>();
            if (id != null && !useOnlyDefaultValues)
            {
                selectedValues = lookupServices.GetSelectedLookupValues(id, lookupSettings);
            }
            if (!string.IsNullOrEmpty(data))
            {
                selectedValues = selectedValues.Union(data.Split(','));
            }

            var groups = new List<SelectListGroup>();
            groups.AddRange(lookupValues.Select(g => new SelectListGroup { Name = g.GroupValue }));
            var items = new List<SelectListItem>();
            lookupValues.ForEach(g => items.AddRange(g.GroupItems.Select(i => new SelectListItem
            {
                Group = groups.FirstOrDefault(gr => gr.Name.Equals(g.GroupValue, StringComparison.InvariantCultureIgnoreCase)),
                Text = i.Value,
                Value = i.Id,
                Selected = selectedValues.Any(lkp => lkp != null && lkp.Trim().Equals(i.Id.Trim()))
            })));

            return helper.DropDownList(fieldname, items.OrderBy(i => i.Group.Name).ThenBy(i => i.Text), htmlAttributes);
        }

        public static MvcHtmlString EditorFor<TModel>(this HtmlHelper<TModel> html, Expression<Func<TModel, FieldDefinition>> expression, Func<TModel, FieldDefinition> templateSelector, object additionalViewData)
        {
            var fieldDefinition = templateSelector(html.ViewData.Model);

            string editor = fieldDefinition.FieldTypeCode;

            if ((fieldDefinition.FieldTypeCode == FieldType.Tag || fieldDefinition.FieldTypeCode == FieldType.SingleLineText || fieldDefinition.FieldTypeCode == FieldType.MultipleLookup || fieldDefinition.FieldTypeCode == FieldType.MultipleLookupGroup) &&
                !string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                var settings = fieldDefinition.GetSettings<TagSettings>();
                editor = string.IsNullOrWhiteSpace(settings.Editor) ? editor : settings.Editor;
            }

            return html.EditorFor(expression, editor, additionalViewData);
        }

        public static MvcHtmlString EditorForTranslatedField<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string templateName, string fieldName, string idFieldName, string itemId, string languageCode, IEnumerable<FieldDefinition> fieldDefinitions, string tableName, string data = null)
        {
            var translationServices = DependencyResolver.Current.GetService<TranslationServices>();
            var parsedData = (string.IsNullOrEmpty(data)) ? null : JsonConvert.DeserializeObject<dynamic>(data)[languageCode];

            Dictionary<string, object> contentItemData = new Dictionary<string, object>();
            if (parsedData == null)
            {
                if (!string.IsNullOrEmpty(idFieldName))
                {
                    var translatedContent = translationServices.GetTranslatedContentForItemIdAndLanguage(idFieldName, itemId, languageCode, tableName);
                    if (translatedContent != null)
                    {
                        contentItemData.Add(fieldName, translatedContent.ContainsKey(fieldName) ? translatedContent[fieldName] : string.Empty);
                    }
                }
            }
            else
            {
                contentItemData.Add(fieldName, parsedData.ContainsKey(fieldName) ? templateName.Equals(FieldType.HtmlText, StringComparison.InvariantCultureIgnoreCase) ? WebUtility.HtmlDecode(parsedData[fieldName].ToString()) : parsedData[fieldName] : string.Empty);
            }

            return html.EditorFor(expression, templateName, new { fieldName = fieldName, contentItemData = contentItemData, fieldDefinitions = fieldDefinitions });
        }

        public static void StripHtmlTextFields(IDictionary<string, object> fields, IEnumerable<ContentTypeFieldDefinition> modelContentTypeFieldDefinitions)
        {
            var contentTypeFieldDefinitions = modelContentTypeFieldDefinitions as ContentTypeFieldDefinition[] ?? modelContentTypeFieldDefinitions.ToArray();
            foreach (var key in fields.Keys.ToList())
            {
                if (!string.IsNullOrEmpty(key) && fields[key] != null && (fields[key]).GetType().Name == "String" &&
                    contentTypeFieldDefinitions.Any(ctf => ctf.FieldDefinition.Name.Equals(key, StringComparison.OrdinalIgnoreCase) &&
                    !ctf.FieldDefinition.FieldTypeCode.Equals("HtmlText", StringComparison.OrdinalIgnoreCase)))
                {
                    fields[key] = ((string)fields[key]).DecodeHtml();
                }
            }
        }
    }

    public class MvcFormGroupDiv : IDisposable
    {
        private readonly TextWriter _writer;

        public MvcFormGroupDiv(TextWriter writer)
        {
            _writer = writer;
        }

        public void Dispose()
        {
            _writer.Write("</div>");
        }

        public void EndFormGroupDiv()
        {
            Dispose();
        }
    }
}