﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using Slam.Cms.Common;
using Slam.Cms.Data;

namespace Mastercard.MarketingCenter.Cms.Core.Extensions
{
    public static class SlamExtensions
    {
        public static TagTree SetRestrictAccessForTags(this TagTree tagTree, string[] tagIdentifiers, bool restrictAccess)
        {
            if (tagIdentifiers.Length > 0)
            {
                ApplyToTagTreeNode(tagTree.Root, n =>
                {
                    if (n != null)
                    {
                        foreach (var childNode in n.Children)
                        {
                            if (childNode.Type == TagTreeNodeType.Tag && childNode.Tag != null && !String.IsNullOrEmpty(childNode.Tag.Identifier) &&
                                (childNode.Tag.Identifier.In(tagIdentifiers) || (n.Tag != null && n.Tag.RestrictAccess == restrictAccess && restrictAccess != childNode.Tag.RestrictAccess)))
                                childNode.Tag.RestrictAccess = restrictAccess;
                        }
                    }
                });
            }

            return tagTree;
        }

        public static void ApplyToTagTreeNode(TagTreeNode node, Action<TagTreeNode> action)
        {
            action(node);

            foreach (var childNode in node.Children)
                ApplyToTagTreeNode(childNode, action);
        }

        public static TagTree FilterTagCategories(this TagTree tagTree, params string[] tagCategoryId)
        {
            var tagCategoriesToRemove = tagTree.Root.Children.Where(n => !n.TagCategory.TagCategoryId.In(tagCategoryId)).ToList();
            foreach (var tagCategory in tagCategoriesToRemove)
                tagTree.Root.Children.Remove(tagCategory);
            return tagTree;
        }

        public static T DeepClone<T>(this T obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }
    }
}
