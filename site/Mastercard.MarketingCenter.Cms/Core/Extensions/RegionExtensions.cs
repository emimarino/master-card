﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Core.Extensions
{
    public static class RegionExtensions
    {
        public static IEnumerable<Region> FilterRegionsWithDisabledBOInterface(this IEnumerable<Region> regionsToFilter)
        {
            var bussinesOwnerServices = DependencyResolver.Current.GetService<IBussinesOwnerService>();
            return bussinesOwnerServices.FilterRegionsWithDisabledBOInterface(regionsToFilter);
        }
    }
}