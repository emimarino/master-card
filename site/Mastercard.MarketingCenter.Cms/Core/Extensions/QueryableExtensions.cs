﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Mastercard.MarketingCenter.Cms.Core.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<T> GetPagedList<T>(this IQueryable<T> queryable,
            int indexStart, int pageSize, string sortProperty, bool @ascending,
            out int totalCount) where T : class
        {
            totalCount = queryable.Count();

            queryable = @ascending
                ? queryable.OrderBy(sortProperty)
                : queryable.OrderByDescending(sortProperty);

            return queryable.Skip(indexStart)
                .Take(pageSize);
        }

        public static IQueryable<T> GetPagedList<T>(this IQueryable<T> queryable,
            int indexStart, int pageSize, IDictionary<string, bool> sortProperties,
            out int totalCount) where T : class
        {
            totalCount = queryable.Count();

            if (sortProperties != null && sortProperties.Any())
            {
                bool isFirst = true;
                IOrderedQueryable<T> ordered = null;
                foreach (var sortProperty in sortProperties)
                {
                    if (isFirst)
                    {
                        ordered = sortProperty.Value
                            ? queryable.OrderBy(sortProperty.Key)
                            : queryable.OrderByDescending(sortProperty.Key);
                        isFirst = false;
                    }
                    else
                    {
                        ordered = sortProperty.Value
                            ? ordered.ThenBy(sortProperty.Key)
                            : ordered.ThenByDescending(sortProperty.Key);
                    }

                }
                queryable = ordered;
            }

            return queryable.Skip(indexStart)
                .Take(pageSize);
        }

        #region Ordering
        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string property)
        {
            return ApplyOrder(source, property, "OrderBy");
        }

        public static IOrderedQueryable<T> OrderByDescending<T>(this IQueryable<T> source, string property)
        {
            return ApplyOrder(source, property, "OrderByDescending");
        }

        public static IOrderedQueryable<T> ThenBy<T>(this IOrderedQueryable<T> source, string property)
        {
            return ApplyOrder(source, property, "ThenBy");
        }

        public static IOrderedQueryable<T> ThenByDescending<T>(this IOrderedQueryable<T> source, string property)
        {
            return ApplyOrder(source, property, "ThenByDescending");
        }

        static IOrderedQueryable<T> ApplyOrder<T>(IQueryable<T> source, string property, string methodName)
        {
            string[] props = property.Split('.');
            Type type = typeof(T);
            ParameterExpression arg = Expression.Parameter(type, "x");
            Expression expr = arg;
            foreach (string prop in props)
            {
                // use reflection (not ComponentModel) to mirror LINQ
                PropertyInfo pi = type.GetProperty(prop);
                expr = Expression.Property(expr, pi);
                type = pi.PropertyType;
            }
            Type delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
            LambdaExpression lambda = Expression.Lambda(delegateType, expr, arg);

            object result = typeof(Queryable).GetMethods().Single(
                    method => method.Name == methodName
                            && method.IsGenericMethodDefinition
                            && method.GetGenericArguments().Length == 2
                            && method.GetParameters().Length == 2)
                    .MakeGenericMethod(typeof(T), type)
                    .Invoke(null, new object[] { source, lambda });
            return (IOrderedQueryable<T>)result;
        }
        #endregion
    }
}