﻿using Mastercard.MarketingCenter.Cms.Models;
using Mastercard.MarketingCenter.Cms.Services.Services;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    public class FeatureOnController : CmsBaseController
    {
        private readonly FeatureOnServices _featureOnServices;

        public FeatureOnController(FeatureOnServices featureOnServices)
        {
            _featureOnServices = featureOnServices;
        }

        public ActionResult Index(string contentItemId, string contentTypeId, bool isInterregionCloning = false, bool interRegionCloningEnabled = true)
        {
            var items = _featureOnServices.GetFeaturableTagCategories(
                contentTypeId,
                UserContext.User.UserName,
                UserContext.User.Groups,
                contentItemId);

            var model = new FeatureOnModel { FeaturableTagCategoryItems = items, ContentItemId = contentItemId, ContentTypeId = contentTypeId, IsInterregionCloning = isInterregionCloning, InterRegionCloningEnabled = interRegionCloningEnabled };

            return PartialView(model);
        }

        public ActionResult FilterTags(List<string> tagsList, string contentTypeId)
        {
            var items = _featureOnServices.FilterTags(contentTypeId, UserContext.User.UserName, tagsList).Distinct();

            return JsonNet(items);
        }

        public ActionResult GetContentItemFeatureOnTag(string id)
        {
            var items = _featureOnServices.GetContentItemFeatureItems(id).ToList();

            return JsonNet(
                            new
                            {
                                locations = items.Where(o => o.Type == FeatureType.FeatureLocation),
                                tags = items.Where(o => o.Type == FeatureType.Tag)
                            });
        }
    }
}