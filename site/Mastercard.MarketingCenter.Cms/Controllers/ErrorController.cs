﻿using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    public class ErrorController : CmsBaseController
    {
        public ActionResult Error()
        {
            Response.StatusCode = 500;
            return View();
        }
        
        public ActionResult NotFound()
        {
            Response.StatusCode = 404;
            return View();
        }

        public ActionResult Unauthorized()
        {
            Response.StatusCode = 403;
            return View();
        }
    }
}