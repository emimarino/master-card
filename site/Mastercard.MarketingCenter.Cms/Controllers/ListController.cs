﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using Mastercard.MarketingCenter.Cms.Core.DataTables;
using Mastercard.MarketingCenter.Cms.Core.Extensions;
using Mastercard.MarketingCenter.Cms.Models;
using Mastercard.MarketingCenter.Cms.Models.List;
using Mastercard.MarketingCenter.Cms.Services.Core;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Exceptions;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    public class ListController : CmsBaseController
    {
        private readonly ListDao _listDao;
        private readonly ContentService _contentService;
        private readonly ListItemTrackingServices _listItemTrackingServices;
        private readonly IIssuerService _issuerService;
        private readonly UserContext _userContext;
        private readonly IAuthorizationService _authorizationService;
        private readonly ICalendarService _calendarService;
        private readonly IErrorMessageService _errorMessageService;
        private readonly IUrlService _urlService;

        public ListController(
            ListDao listDao,
            ContentService contentService,
            ListItemTrackingServices listItemTrackingServices,
            IIssuerService issuerService,
            UserContext userContext,
            IAuthorizationService authorizationService,
            ICalendarService calendarService,
            IErrorMessageService errorMessageService,
            IUrlService urlService)
        {
            _listDao = listDao;
            _contentService = contentService;
            _listItemTrackingServices = listItemTrackingServices;
            _issuerService = issuerService;
            _userContext = userContext;
            _authorizationService = authorizationService;
            _calendarService = calendarService;
            _errorMessageService = errorMessageService;
            _urlService = urlService;
        }

        [CustomAuthorize]
        public ActionResult Index(int id, string refreshAction = "")
        {
            var list = _listDao.GetById(id);
            if (!string.IsNullOrEmpty(refreshAction))
            {
                ViewBag.refreshAction = refreshAction;
            }

            SetPageTitle(list.Name + " List");

            if (!list.ListFieldDefinitions.FilterByPermission().Any())
            {
                ViewBag.Message = $"No Field Definitions found in the database for the List: {list.Name}";
                return View("UnderConstruction");
            }

            var listFieldDefinitions = list.ListFieldDefinitions.FilterByPermission().OrderBy(o => o.Order);
            var idColumn = listFieldDefinitions.FirstOrDefault(o => o.FieldDefinition.FieldType.IsIdentifierField());

            return View(GetListModel(id, list, listFieldDefinitions, idColumn));
        }

        private static ListModel GetListModel(int id, List list, IEnumerable<ListFieldDefinition> listFieldDefinitions, ListFieldDefinition idColumn)
        {
            var listModel = new ListModel
            {
                Title = list.Name,
                ShowTrackingFields = list.ListFieldDefinitions.FilterByPermission().HasTrackingFields(),
                Columns =
                    listFieldDefinitions
                        .ExcludeTrackingFields()
                        .Where(o => o.VisibleInList)
                        .Select(
                            o =>
                                new ColumnModel
                                {
                                    Description = o.FieldDefinition.Description,
                                    Name = o.FieldDefinition.Name,
                                    OrderDirection = o.DefaultOrder,
                                    Order = o.Order,
                                    FieldType = o.FieldDefinition.FieldTypeCode

                                }).OrderBy(c => c.Order).ThenBy(c => c.Name).ToList(),
                IdColumnName = idColumn != null ? idColumn.FieldDefinition.Name : null,
                Id = id,
                CanExpire = list.CanExpire
            };

            if (list.ListFieldDefinitions.FilterByPermission().HasTrackingFields())
            {
                listModel.CreatedDateFieldName =
                    list.ListFieldDefinitions.FilterByPermission().GetFieldNameByTypeCode(FieldType.TrackingCreatedDate);
                listModel.ModifiedDateFieldName =
                    list.ListFieldDefinitions.FilterByPermission().GetFieldNameByTypeCode(FieldType.TrackingUpdatedDate);

                if (string.IsNullOrEmpty(listModel.CreatedDateFieldName) ||
                    string.IsNullOrEmpty(listModel.ModifiedDateFieldName))
                {
                    LogManager.EventFactory.Create()
                        .Level(LoggingEventLevel.Alert)
                        .Text($"No CreatedDate or ModifiedDate tracking field defined for List: {list.Name} ({list.ListId})")
                        .Push();
                }
            }

            ReplaceTranslationsField(ref listModel, list);

            return listModel;
        }

        private static void ReplaceTranslationsField(ref ListModel listModel, List list)
        {
            var translationsField = list.ListFieldDefinitions.FirstOrDefault(d => d.FieldDefinition.FieldTypeCode == FieldType.Translations);
            if (translationsField != null)
            {
                if (!string.IsNullOrEmpty(translationsField.FieldDefinition.CustomSettings))
                {
                    var translationsSettings = translationsField.FieldDefinition.GetSettings<TranslationsSettings>();
                    if (translationsSettings != null && !string.IsNullOrEmpty(translationsSettings.ListViewFieldName))
                    {
                        foreach (var column in listModel.Columns)
                        {
                            if (column.Name == translationsField.FieldDefinition.Name)
                            {
                                column.Description = translationsSettings.ListViewFieldName;
                                break;
                            }
                        }
                    }
                }
            }
        }

        [CustomAuthorize]
        public ActionResult GetContent(GetListItemsDataTablesRequest request)
        {
            var list = _listDao.GetById(request.ListId);
            var result = _contentService.GetContentFor(list, UserContext.SelectedRegion?.Trim(), request.Start, request.Length, request.Search.Value, request.GetSortedColumns(), (list.CanExpire ? (bool?)request.isExpired : null));
            foreach (var row in result.Data)
            {
                var fields = row as IDictionary<string, object>;
                var keys = fields.Keys.ToList();
                foreach (var key in keys)
                {
                    if (!string.IsNullOrEmpty(key) && fields[key] != null && fields[key] is string)
                    {
                        fields[key] = list.ListFieldDefinitions.Any(ctf => ctf.FieldDefinition.Name.Equals(key, StringComparison.OrdinalIgnoreCase)
                                      && ctf.FieldDefinition.FieldTypeCode.Equals(FieldType.HtmlText, StringComparison.OrdinalIgnoreCase)) ?
                                      ((string)fields[key]).SanitizeHtmlString() :
                                      list.ListFieldDefinitions.Any(ctf => ctf.FieldDefinition.Name.Equals(key, StringComparison.OrdinalIgnoreCase)
                                      && ctf.FieldDefinition.FieldType.FieldTypeCode.Equals(FieldType.Image, StringComparison.OrdinalIgnoreCase) && ctf.VisibleInList == true) ?
                                      _urlService.GetStaticImageURL((string)fields[key], 64, 64, false, 95, false) :
                                      ((string)fields[key]).StripHtml();
                    }
                }
            }

            return JsonNet(new DataTablesResponse(request.Draw, result.Data, result.Total, result.Total));
        }

        public ActionResult GetTrackingInfo(string id, int listId)
        {
            if (!_authorizationService.Authorize(
                null,
                Convert.ToString(ControllerContext.RequestContext.RouteData.Values["controller"]),
                Convert.ToString(ControllerContext.RequestContext.RouteData.Values["action"]),
                listId.ToString()))
            {
                throw new AuthorizationException("Unauthorized");
            }

            var trackingInfo = _listItemTrackingServices.GetTrackingFieldsInfo(id, listId);

            return JsonNet(
                            new
                            {
                                trackingInfo.CreatedBy,
                                CreatedDate = trackingInfo.CreatedDate.ToString(CultureInfo.InvariantCulture),
                                trackingInfo.ModifiedBy,
                                ModifiedDate = trackingInfo.ModifiedDate.HasValue ? trackingInfo.ModifiedDate.ToString() : null
                            });
        }

        [CustomAuthorize]
        [HttpPost]
        [UnitOfWork]
        public ActionResult Delete(int listId, string itemId)
        {
            try
            {
                bool deleted = false;
                if (listId.ToString().Equals(MarketingCenterDbConstants.MarketingCalendarListIds.CampaignCategory, StringComparison.InvariantCultureIgnoreCase))
                {
                    deleted = _calendarService.DeleteCampaignCategory(itemId);
                }
                else
                {
                    _contentService.DeleteListItem(listId, itemId);
                    deleted = true;
                }

                if (deleted)
                {
                    return JsonNet(new { ok = true });
                }

                return Json(new { ok = false, errorMessage = GetCustomErrorMessage(listId) });
            }
            catch (Exception)
            {
                return Json(new { ok = false, errorMessage = GetCustomErrorMessage(listId) });
            }
        }

        private string GetCustomErrorMessage(int listId)
        {
            return _errorMessageService.GetErrorMessage(listId, "delete")?.Message;
        }

        public ActionResult Edit(int listId, string id, string returnUrl = null)
        {
            if (!_authorizationService.Authorize(
                null,
                Convert.ToString(ControllerContext.RequestContext.RouteData.Values["controller"]),
                Convert.ToString(ControllerContext.RequestContext.RouteData.Values["action"]),
                listId.ToString()))
            {
                throw new AuthorizationException("Unauthorized");
            }

            var list = _listDao.GetById(listId);

            SetPageTitle("Edit " + list.Name);

            var listItemData = _contentService.GetListItemData(listId, id);
            bool itemFound = listItemData != null;
            if (itemFound)
            {
                var regionField = list.GetRegionField();
                if (regionField != null)
                {
                    var regionFieldValue = listItemData[regionField.Name?.Trim()];
                    itemFound = regionFieldValue == null || Convert.ToString(regionFieldValue).Trim() == _userContext.SelectedRegion?.Trim();
                }
            }

            if (!itemFound)
            {
                LogManager.EventFactory.Create()
                        .Text($"No Item Found in list {listId} with ID: {id}")
                        .Level(LoggingEventLevel.Information)
                        .Push();

                throw new NotFoundException();
            }

            var idField = list.GetIdField();
            var idFieldValue = listItemData[idField.Name];

            AddSpecificListRequiredToFields(ref list);
            var model = new ListItemModel
            {
                ListItemData = listItemData,
                IdFieldValue = idFieldValue,
                ListTitle = list.Name,
                ListTableName = list.TableName,
                HasTrackingFields = list.ListFieldDefinitions.FilterByPermission().HasTrackingFields(),
                ListId = list.ListId,
                ListFieldDefinitions = list.ListFieldDefinitions.FilterByPermission().ExcludeTrackingFields().ExcludeRegionField().FilterBySetting(_userContext.SelectedRegion),
                ReturnUrl = returnUrl
            };

            var fields = model.ListItemData;
            foreach (var key in fields.Keys.ToList())
            {
                if (!string.IsNullOrEmpty(key) && fields[key] != null && (fields[key]).GetType().Name == "String" &&
                    model.ListFieldDefinitions.Any(ctf => ctf.FieldDefinition.Name.Equals(key, StringComparison.OrdinalIgnoreCase)
                       && !ctf.FieldDefinition.FieldTypeCode.Equals("HtmlText", StringComparison.OrdinalIgnoreCase)))
                {
                    fields[key] = ((string)fields[key]).DecodeHtml();
                }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(ListItemModel model)
        {
            ValidateModel(model);
            IDictionary<string, object> fields;
            if (ModelState.IsValid)
            {
                _contentService.UpdateListItem(model.ListId, model.ListItemData, UserContext.User.UserId, UserContext.SelectedRegion);
                if (string.IsNullOrEmpty(model.ReturnUrl))
                {
                    return RedirectToAction("Index", "List", new { id = model.ListId, refreshAction = "Update" });
                }

                return Redirect(model.ReturnUrl);
            }

            fields = model.ListItemData;
            foreach (var key in fields.Keys.ToList())
            {
                if (!string.IsNullOrEmpty(key) && fields[key] != null
                    && fields[key].GetType().Name == "String"
                    && model.ListFieldDefinitions.Any(ctf => ctf.FieldDefinition.Name.Equals(key, StringComparison.OrdinalIgnoreCase)
                    && !ctf.FieldDefinition.FieldTypeCode.Equals("HtmlText", StringComparison.OrdinalIgnoreCase)))
                {
                    fields[key] = ((string)fields[key]).DecodeHtml();
                }
            }

            var list = _listDao.GetById(model.ListId);
            model.ListFieldDefinitions = list.ListFieldDefinitions.FilterByPermission().ExcludeTrackingFields().ExcludeRegionField();
            model.HasTrackingFields = list.ListFieldDefinitions.FilterByPermission().HasTrackingFields();
            model.ListTableName = list.TableName;

            return View("Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ListItemModel model)
        {
            model.ListItemData = _contentService.GetDefaultValueData(model.ListItemData, model.ListFieldDefinitions.Select(o => o.FieldDefinition) as FieldDefinition[] ?? model.ListFieldDefinitions.Select(o => o.FieldDefinition).ToArray(), UserContext.SelectedRegion);

            ValidateModel(model);
            if (ModelState.IsValid)
            {
                _contentService.AddItemContent(model.ListId, model.ListItemData, UserContext.User.UserId, _userContext.SelectedRegion?.Trim());

                if (string.IsNullOrEmpty(model.ReturnUrl))
                {
                    return RedirectToAction("Index", "List", new { id = model.ListId, refreshAction = "Update" });
                }

                return Redirect(model.ReturnUrl);
            }

            var fields = model.ListItemData;

            foreach (var key in fields.Keys.ToList())
            {
                if (!string.IsNullOrEmpty(key) && fields[key] != null && (fields[key]).GetType().Name == "String" &&
                    model.ListFieldDefinitions.Any(ctf => ctf.FieldDefinition.Name.Equals(key, StringComparison.OrdinalIgnoreCase)
                       && !ctf.FieldDefinition.FieldTypeCode.Equals("HtmlText", StringComparison.OrdinalIgnoreCase)))
                {
                    fields[key] = ((string)fields[key]).DecodeHtml();
                }
            }

            var list = _listDao.GetById(model.ListId);
            model.ListFieldDefinitions = list.ListFieldDefinitions.FilterByPermission().ExcludeTrackingFields().ExcludeRegionField().Where(o => !o.HiddenInNew);
            model.HasTrackingFields = list.ListFieldDefinitions.FilterByPermission().HasTrackingFields();
            model.ListTableName = list.TableName;

            return View("Create", model);
        }

        [CustomAuthorize]
        public ActionResult Create(int id, string returnUrl = null)
        {
            return View(SetupCreateModel(id, returnUrl));
        }

        private ListItemModel SetupCreateModel(int id, string returnUrl)
        {
            var list = _listDao.GetById(id);

            SetPageTitle("Create " + list.Name);

            AddSpecificListRequiredToFields(ref list);

            var model = new ListItemModel
            {
                ListId = id,
                ListFieldDefinitions = list.ListFieldDefinitions.FilterByPermission().ExcludeTrackingFields().ExcludeRegionField().FilterBySetting(_userContext.SelectedRegion).Where(o => !o.HiddenInNew),
                ListTitle = list.Name,
                ListTableName = list.TableName,
                HasTrackingFields = list.ListFieldDefinitions.FilterByPermission().HasTrackingFields(),
                ReturnUrl = returnUrl
            };

            model.ListItemData = _contentService.GetDefaultValueData(model.ListItemData, list.ListFieldDefinitions.Select(o => o.FieldDefinition) as FieldDefinition[] ?? list.ListFieldDefinitions.Select(o => o.FieldDefinition).ToArray(), UserContext.SelectedRegion);

            return model;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAsync(ListItemModel model)
        {
            model.ListItemData = _contentService.GetDefaultValueData(model.ListItemData, model.ListFieldDefinitions.Select(o => o.FieldDefinition) as FieldDefinition[] ?? model.ListFieldDefinitions.Select(o => o.FieldDefinition).ToArray(), UserContext.SelectedRegion);

            ValidateModel(model);

            if (ModelState.IsValid)
            {
                var newItemId = _contentService.AddItemContent(model.ListId, model.ListItemData, UserContext.User.UserId, _userContext.SelectedRegion?.Trim());
                return new JsonResult
                {
                    Data = new
                    {
                        ItemId = newItemId
                    }
                };
            }

            var fields = model.ListItemData;

            foreach (var key in fields.Keys.ToList())
            {
                if (!string.IsNullOrEmpty(key) && fields[key] != null && (fields[key]).GetType().Name == "String" &&
                    model.ListFieldDefinitions.Any(ctf => ctf.FieldDefinition.Name.Equals(key, StringComparison.OrdinalIgnoreCase)
                       && !ctf.FieldDefinition.FieldTypeCode.Equals("HtmlText", StringComparison.OrdinalIgnoreCase)))
                {
                    fields[key] = ((string)fields[key]).DecodeHtml();
                }
            }

            var list = _listDao.GetById(model.ListId);
            model.ListFieldDefinitions = list.ListFieldDefinitions.FilterByPermission().ExcludeTrackingFields().ExcludeRegionField().Where(o => !o.HiddenInNew);
            model.HasTrackingFields = list.ListFieldDefinitions.FilterByPermission().HasTrackingFields();
            model.ListTableName = list.TableName;

            return PartialView("CreateAsync", model);
        }

        public ActionResult CreateAsync(int id, string successCallback, string errorCallback, string updateTargetId, string completeCallback)
        {
            var model = SetupCreateModel(id, null);
            model.ClientErrorCallback = errorCallback;
            model.ClientSuccesCallback = successCallback;
            model.ClientCompleteCallback = completeCallback;
            model.UpdateTargetId = updateTargetId;

            return PartialView(model);
        }

        public ActionResult EditAsync(int listId, string id, string successCallback, string errorCallback, string updateTargetId, string completeCallback)
        {
            if (!_authorizationService.Authorize(
                null,
                Convert.ToString(ControllerContext.RequestContext.RouteData.Values["controller"]),
                Convert.ToString(ControllerContext.RequestContext.RouteData.Values["action"]),
                listId.ToString()))
            {
                throw new AuthorizationException("Unauthorized");
            }

            var list = _listDao.GetById(listId);

            SetPageTitle("Edit " + list.Name);

            var listItemData = _contentService.GetListItemData(listId, id);
            bool itemFound = listItemData != null;
            if (itemFound)
            {
                var regionField = list.GetRegionField();
                if (regionField != null)
                {
                    var regionFieldValue = listItemData[regionField.Name?.Trim()];
                    itemFound = regionFieldValue == null || Convert.ToString(regionFieldValue).Trim() == _userContext.SelectedRegion?.Trim();
                }
            }

            if (!itemFound)
            {
                LogManager.EventFactory.Create()
                        .Text($"No Item Found in list {listId} with ID: {id}")
                        .Level(LoggingEventLevel.Information)
                        .Push();

                throw new NotFoundException();
            }

            var idField = list.GetIdField();
            var idFieldValue = listItemData[idField.Name];

            AddSpecificListRequiredToFields(ref list);
            var model = new ListItemModel
            {
                ListItemData = listItemData,
                IdFieldValue = idFieldValue,
                ListTitle = list.Name,
                ListTableName = list.TableName,
                HasTrackingFields = list.ListFieldDefinitions.FilterByPermission().HasTrackingFields(),
                ListId = list.ListId,
                ListFieldDefinitions = list.ListFieldDefinitions.FilterByPermission().ExcludeTrackingFields().ExcludeRegionField(),
                ClientErrorCallback = errorCallback,
                ClientSuccesCallback = successCallback,
                ClientCompleteCallback = completeCallback,
                UpdateTargetId = updateTargetId
            };

            var fields = model.ListItemData;
            foreach (var key in fields.Keys.ToList())
            {
                if (!string.IsNullOrEmpty(key) && fields[key] != null && (fields[key]).GetType().Name == "String" &&
                    model.ListFieldDefinitions.Any(ctf => ctf.FieldDefinition.Name.Equals(key, StringComparison.OrdinalIgnoreCase)
                       && !ctf.FieldDefinition.FieldTypeCode.Equals("HtmlText", StringComparison.OrdinalIgnoreCase)))
                {
                    fields[key] = ((string)fields[key]).DecodeHtml();
                }
            }

            return PartialView(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateAsync(ListItemModel model)
        {
            ValidateModel(model);
            IDictionary<string, object> fields;
            if (ModelState.IsValid)
            {
                _contentService.UpdateListItem(model.ListId, model.ListItemData, UserContext.User.UserId, UserContext.SelectedRegion);
                return new JsonResult
                {
                    Data = new
                    {
                        ItemId = Convert.ToString(model.IdFieldValue)
                    }
                };
            }

            fields = model.ListItemData;
            foreach (var key in fields.Keys.ToList())
            {
                if (!string.IsNullOrEmpty(key) && fields[key] != null
                    && fields[key].GetType().Name == "String"
                    && model.ListFieldDefinitions.Any(ctf => ctf.FieldDefinition.Name.Equals(key, StringComparison.OrdinalIgnoreCase)
                    && !ctf.FieldDefinition.FieldTypeCode.Equals("HtmlText", StringComparison.OrdinalIgnoreCase)))
                {
                    fields[key] = ((string)fields[key]).DecodeHtml();
                }
            }

            var list = _listDao.GetById(model.ListId);
            model.ListFieldDefinitions = list.ListFieldDefinitions.FilterByPermission().ExcludeTrackingFields().ExcludeRegionField();
            model.HasTrackingFields = list.ListFieldDefinitions.FilterByPermission().HasTrackingFields();
            model.ListTableName = list.TableName;

            return PartialView("EditAsync", model);
        }

        private void ValidateModel(ListItemModel model)
        {
            var list = _listDao.GetById(model.ListId);

            AddSpecificListRequiredToFields(ref list);
            var fields = list.ListFieldDefinitions.FilterByPermission().Select(o => o.FieldDefinition);

            var validationListItems = model.ListItemData.DeepClone();
            validationListItems.Add("RegionId", _userContext.SelectedRegion);

            fields = _contentService.SetToggledFieldsToOptional(fields, validationListItems);

            var validationErrors = _contentService.ValidateData(fields, validationListItems);

            var errorItems = validationErrors as ErrorItem[] ?? validationErrors.ToArray();
            errorItems.ForEach(err => ModelState.AddModelError(err.Key, err.ErrorMessage));
        }

        private void AddSpecificListRequiredToFields(ref List list)
        {
            var requiredFieldDefs = list.ListFieldDefinitions
                .Where(fd => !string.IsNullOrEmpty(fd.RequiredInRegions)
                    && fd.RequiredInRegions.Split(';')
                    .Any(r => r.Trim().ToLower() == UserContext.SelectedRegion.Trim().ToLower()));

            if (requiredFieldDefs.Any())
            {
                list.ListFieldDefinitions
                    .FilterByPermission()
                    .Select(o => o.FieldDefinition)
                    .Where(f => requiredFieldDefs.Select(rf => rf.FieldDefinitionId).Contains(f.FieldDefinitionId))
                    .ForEach(reqField => reqField.Required = true);
            }
        }

        [ChildActionOnly]
        public PartialViewResult IssuerListDetails(string issuerId)
        {
            var issuerData = _issuerService.GetIssuerDetail(issuerId);
            return PartialView(issuerData);
        }

        public ActionResult GetContentItemCampaignEvents(string programId, string campaignEventIds)
        {
            var campaignEvents = _calendarService.GetContentItemCampaignEventsFilterByIds(new string[] { programId }).Select(e => e.CampaignEvent)?.ToList() ?? new List<CampaignEvent>();
            var additionalEvents = campaignEventIds?.Split(',');
            if (additionalEvents != null && additionalEvents.Length > 0)
            {
                campaignEvents = campaignEvents.Union(_calendarService.GetCampaignEventsFilterByIds(campaignEventIds.Split(','))).ToList();
            }

            return Json(campaignEvents.OrderByDescending(e => e.StartDate)
                                      .Select(ce => new
                                      {
                                          ce.CampaignEventId,
                                          ce.Title,
                                          CampaignCategory = string.Join(", ", ce.CampaignCategories.Select(ct => ct.CampaignCategoryTranslatedContents.FirstOrDefault()?.Title)),
                                          ce.StartDate,
                                          ce.EndDate,
                                          ce.AlwaysOn
                                      }));
        }

        public ActionResult GetCampaignEventById(string campaignEventId)
        {
            return Json(_calendarService.GetCampaignEventsFilterByIds(new string[] { campaignEventId })
                                        .OrderByDescending(e => e.StartDate)
                                        .Select(ce => new
                                        {
                                            ce.CampaignEventId,
                                            ce.Title,
                                            CampaignCategory = string.Join(", ", ce.CampaignCategories.Select(ct => ct.CampaignCategoryTranslatedContents.FirstOrDefault()?.Title)),
                                            ce.StartDate,
                                            ce.EndDate,
                                            ce.AlwaysOn
                                        }));
        }
    }
}