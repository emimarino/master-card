﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using Mastercard.MarketingCenter.Cms.Models;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    [CustomAuthorize]
    public class ContentTypeController : CmsBaseController
    {
        private readonly ContentTypeDao _contentTypeDao;
        private readonly ContentTypeService _contentTypeService;
        private readonly IdGeneratorService _idGeneratorService;

        public ContentTypeController(ContentTypeDao contentTypeDao, ContentTypeService contentTypeService, IdGeneratorService idGeneratorService)
        {
            _contentTypeDao = contentTypeDao;
            _contentTypeService = contentTypeService;
            _idGeneratorService = idGeneratorService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult GetContentTypes()
        {
            var contentTypes = _contentTypeDao.GetAll().ToList();
            return Json(new { data = contentTypes, recordsTotal = contentTypes.Count(), recordsFiltered = contentTypes.Count }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [UnitOfWork]
        public ActionResult Create(ContentTypeModel model)
        {
            if (ModelState.IsValid)
            {
                var tableName = model.Title.Replace(" ", string.Empty);
                var newContentType = new ContentType
                {
                    ContentTypeId = _idGeneratorService.GetNewUniqueId(),
                    Title = model.Title,
                    DefaultIcon = "blank.jpg",
                    TableName = tableName
                };

                _contentTypeDao.Add(newContentType);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        [HttpPost]
        [UnitOfWork]
        public ActionResult Update(ContentTypeModel model)
        {
            if (ModelState.IsValid)
            {
                var contentType = _contentTypeDao.GetById(model.ContentTypeId);
                contentType.Title = model.Title;

                return RedirectToAction("Index");
            }

            return View(model);
        }

        public ActionResult Update(string id)
        {
            var contentType = _contentTypeDao.GetById(id);
            var model = new ContentTypeModel { ContentTypeId = contentType.ContentTypeId, Title = contentType.Title };

            return View(model);
        }

        [HttpPost]
        [UnitOfWork]
        public ActionResult Delete(string id)
        {
            try
            {
                _contentTypeService.DeleteContentType(id);
                return Json(new { Ok = true }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { Ok = false, ex.Message });
            }
        }
    }
}