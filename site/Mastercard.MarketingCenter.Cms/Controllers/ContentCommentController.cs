﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using Mastercard.MarketingCenter.Cms.Models;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Newtonsoft.Json;
using Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using FilterStatus = Slam.Cms.Data.FilterStatus;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    public class ContentCommentController : Controller
    {
        private readonly IUserService _userService;
        private readonly IContentCommentServices _contentCommentServices;
        private readonly IContentItemService _contentItemService;
        private readonly IEmailService _emailServices;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBussinesOwnerService _bussinesOwnerService;
        private readonly UserContext _userContext;
        private readonly ContentService _contentService;
        private readonly IStandardResponseService _standardResponseService;

        public ContentCommentController(
            IContentCommentServices contentCommentServices,
            IContentItemService contentItemService,
            IEmailService emailServices,
            IUserService userService,
            IUnitOfWork unitOfWork,
            IBussinesOwnerService bussinesOwnerServices,
            UserContext userContext,
            ContentService contentService,
            IStandardResponseService standardResponseService)
        {
            _bussinesOwnerService = bussinesOwnerServices;
            _contentCommentServices = contentCommentServices;
            _emailServices = emailServices;
            _userService = userService;
            _contentItemService = contentItemService;
            _unitOfWork = unitOfWork;
            _userContext = userContext;
            _contentService = contentService;
            _standardResponseService = standardResponseService;
        }

        public ActionResult Widget(
            string contentItemID,
            string backLink = "",
            List<string> commentsSeed = null,
            bool isNew = false,
            bool isAdmin = false)
        {
            var usersAvatar = new string(_userContext.User.FullName.Split(' ').Select(n => n.FirstOrDefault()).ToArray());
            ViewData["commentsSeed"] = commentsSeed == null || commentsSeed.Count() == 0 ? null : JsonConvert.SerializeObject(commentsSeed);
            return PartialView(new ContentCommentViewModel
            {
                ContentItemId = contentItemID,
                BackLink = backLink,
                IsNew = isNew,
                SenderUserAvatar = usersAvatar,
                CommentsTotal = _contentCommentServices.GetCommentsByContent(contentItemID),
                IsAdmin = isAdmin
            });
        }

        [BusinessOwnerExpirationManagementEnabled]
        [HttpPost]
        public ActionResult AddNew(string contentItemID, string commentBody, bool isAdmin)
        {
            if (string.IsNullOrEmpty(contentItemID) || string.IsNullOrEmpty(commentBody))
            {
                return new HttpNotFoundResult();
            }

            User businessOwner = null;
            if (isAdmin)
            {
                // get business owner email
                var contentItemData = _contentService.GetContentItemData(contentItemID);
                var businessOwnerId = _bussinesOwnerService.GetBusinessOwnerId(contentItemData);
                businessOwner = businessOwnerId > 0 ? _userService.GetUser(businessOwnerId) : null;

                if (businessOwner == null)
                {
                    Response.StatusCode = 400;
                    return _standardResponseService.GetErrorResponse("NoBusinessOwner", ContentItemComments.NoBusinessOwnerErrorMessage);
                }
            }

            var senderUser = _userContext.User;
            var contentItem = _contentItemService.GetAssetContentItem(contentItemID, FilterStatus.None);
            var isInsertSuccessful = _contentCommentServices.AddNew(senderUser.UserId, contentItem.ContentTypeId, contentItem.ContentItemId, commentBody);
            if (isInsertSuccessful)
            {
                if (isAdmin)
                {
                    _bussinesOwnerService.SendContentCommentNotificationToBussinessOwner(
                        ((IContentItemWithTitle)contentItem).Title,
                        contentItem.ContentItemId,
                        commentBody,
                        contentItem.RegionId,
                        senderUser.FullName,
                        businessOwner.Email,
                        null);
                }
                else
                {
                    _bussinesOwnerService.SendContentCommentNotificationToAdmin(
                        ((IContentItemWithTitle)contentItem).Title,
                        contentItem.ContentItemId,
                        commentBody,
                        contentItem.RegionId,
                        senderUser.FullName,
                        null);
                }

                _unitOfWork.Commit();

                return new HttpStatusCodeResult(HttpStatusCode.NoContent);
            }
            else
            {
                return new HttpNotFoundResult();
            }
        }

        [HttpPost]
        public JsonResult GetPaged(string contentItemID, int pageNumber, int pageSize = 5)
        {
            var contentItem = _contentItemService.GetContentItem(contentItemID);
            return Json(_contentCommentServices.GetPaged(contentItem.ContentTypeId, contentItem.ContentItemId, pageNumber, pageSize));
        }

        [BusinessOwnerExpirationManagementEnabled]
        [HttpPost]
        public ActionResult BulkExtend(string[] contentItemIDs, bool shouldExtend, string userFullName)
        {
            return PartialView(new BulkExpirationExtensionViewModel
            {
                ContentItemIDs = contentItemIDs,
                ShouldExtend = shouldExtend,
                UserFullName = userFullName
            });
        }

        [BusinessOwnerExpirationManagementEnabled]
        [HttpGet]
        public ActionResult Extend(string contentItemID, DateTime expirationDate, string contentTitle, string userFullName, bool shouldExtend)
        {
            return PartialView(new ExpirationExtensionViewModel
            {
                ContentItemID = contentItemID.GetAsLiveContentItemId(),
                ExpirationDate = expirationDate,
                Title = contentTitle,
                UserFullName = userFullName,
                ShouldExtend = shouldExtend
            });
        }

        [BusinessOwnerExpirationManagementEnabled]
        [HttpPost]
        public ActionResult BulkExtensionComment(string extendMessage, string[] contentItemIDs, bool shouldExtend, int daysToExtend = 0)
        {
            if (contentItemIDs == null || contentItemIDs.Length == 0)
            {
                return HttpNotFound();
            }

            var senderUser = _userContext.User;
            IEnumerable<Slam.Cms.Data.ContentItem> slamContentItems;
            var isInsertSuccessful = _bussinesOwnerService.ExtendOrNotExtend(contentItemIDs, senderUser.UserId, shouldExtend, extendMessage, daysToExtend, out slamContentItems);
            if (!isInsertSuccessful)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Conflict);
            }

            _emailServices.SendBulkExtensionNotification(slamContentItems, shouldExtend, extendMessage, daysToExtend, senderUser.FullName);
            _unitOfWork.Commit();

            return new HttpStatusCodeResult(HttpStatusCode.NoContent);
        }

        [BusinessOwnerExpirationManagementEnabled]
        [HttpPost]
        public ActionResult ExtensionComment(string extendMessage, string contentItemID, bool shouldExtend, int daysToExtend = 0)
        {
            if (string.IsNullOrEmpty(contentItemID) || string.IsNullOrEmpty(extendMessage))
            {
                return HttpNotFound();
            }

            var senderUser = _userContext.User;
            IEnumerable<Slam.Cms.Data.ContentItem> slamContentItems;
            var isInsertSuccessful = _bussinesOwnerService.ExtendOrNotExtend(new List<string> { contentItemID }, senderUser.UserId, shouldExtend, extendMessage, daysToExtend, out slamContentItems);
            if (isInsertSuccessful)
            {
                _emailServices.SendExtensionNotification(
                    slamContentItems.Select(x => (IContentItemWithTitle)x).FirstOrDefault()?.Title,
                    contentItemID, shouldExtend, extendMessage, daysToExtend,
                    _contentItemService.GetContentItem(contentItemID)?.RegionId,
                    senderUser.FullName);

                _unitOfWork.Commit();
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.Conflict);
            }

            return new HttpStatusCodeResult(HttpStatusCode.NoContent);
        }
    }
}