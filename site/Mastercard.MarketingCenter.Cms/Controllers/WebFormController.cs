﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    [CustomAuthorize]
    public class WebFormController : CmsBaseController
    {
        private readonly INavigationService _navigationService;

        public WebFormController(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public ActionResult Index(string id, string header, bool renderPageHeading = false)
        {
            var url = Url.RouteUrl("webforms", new { page = id }).TrimEnd('/');

            ViewBag.WebFormPath = string.Format("{0}{1}", url,
                Request.QueryString.Count > 0 ? "?" + Request.QueryString : string.Empty);

            ViewData["RenderPageHeading"] = renderPageHeading;
            ViewData["Header"] = header ?? GetHeaderFromNav();

            SetPageTitle(header);

            return View();
        }

        private string GetHeaderFromNav()
        {
            string header = null;
            var navModel = _navigationService.GetNavigationModel(
                Request.RequestContext.RouteData.Values,
                Request.QueryString);

            if (navModel != null)
            {
                var currentPath = navModel.GetCurrenPath();
                if (currentPath != null && currentPath.Count > 0)
                {
                    var lastNode = currentPath.Last();
                    if (lastNode != null)
                    {
                        header = lastNode.Name;
                    }
                }
            }

            return header;
        }
    }
}