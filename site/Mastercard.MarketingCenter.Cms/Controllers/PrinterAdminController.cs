﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    [CustomAuthorize]
    public class PrinterAdminController : Controller
    {
        // GET: /PrinterAdmin/
        public ActionResult Index()
        {
            return View();
        }
    }
}