﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using Mastercard.MarketingCenter.Cms.Core.DataTables;
using Mastercard.MarketingCenter.Cms.Core.Extensions;
using Mastercard.MarketingCenter.Cms.Models.Search;
using Mastercard.MarketingCenter.Cms.Services.Services;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    [CustomAuthorize]
    public class SearchController : CmsBaseController
    {
        private readonly SearchServices _searchServices;

        public SearchController(SearchServices searchServices)
        {
            _searchServices = searchServices;
        }

        public ActionResult SearchResults(string q)
        {
            object model = q;
            TempData["q"] = q;

            return View("SearchResults", model);
        }

        public ActionResult GetResults(GetSearchResultsDataTableRequest request)
        {
            int totalCount;
            var searchTerm = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(request.SearchTerm));
            var result = _searchServices.SearchFor(searchTerm, UserContext.SelectedRegion).AsQueryable();

            result = result.GetPagedList(request.Start, request.Length, request.GetSortedColumns(), out totalCount);

            return JsonNet(new DataTablesResponse(request.Draw, result, totalCount, totalCount));
        }
    }
}