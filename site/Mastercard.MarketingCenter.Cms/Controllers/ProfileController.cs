﻿using Mastercard.MarketingCenter.Cms.Models;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Common.Models;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Exceptions;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    public class ProfileController : CmsBaseController
    {
        private readonly UserContext _userContext;
        private readonly IUserService _userService;
        private readonly IAuthorizationService _authorizationService;
        private readonly IUrlService _urlService;
        private readonly IJsonWebTokenService _jsonWebTokenService;

        public ProfileController(UserContext userContext, IUserService userService, IAuthorizationService authorizationService, IUrlService urlService, IJsonWebTokenService jsonWebTokenService)
        {
            _userContext = userContext;
            _userService = userService;
            _authorizationService = authorizationService;
            _urlService = urlService;
            _jsonWebTokenService = jsonWebTokenService;
        }

        public ActionResult Access(string userName, string source)
        {
            if (User.IsInRole(Constants.Roles.DevAdmin) || User.IsInRole(Constants.Roles.McAdmin))
            {
                var user = _userService.GetUserByUserName(userName);
                if (!(_userContext.SelectedRegion?.Trim()?.Equals((user.Region?.Trim() ?? ""), StringComparison.OrdinalIgnoreCase) ?? false))
                {
                    throw new AuthorizationException("Unauthorized");
                }

                var userRole = _authorizationService.GetUserRole(user.UserId);
                var model = new AccessViewModel
                {
                    UserId = user.UserId,
                    RoleId = userRole.RoleId,
                    Regions = userRole.Regions.Select(r => r.RegionId.Trim()),
                    Roles = _authorizationService.GetRegionRolesForCurrentUser(),
                    ReturnUrl = string.IsNullOrEmpty(source) ? _urlService.GetUserOrderSearchURL("mastercard-admin") : source.IndexOf(",") > 0 ? source.Replace(",", "&") : source,
                    CurrentUserRole = _authorizationService.GetCurrentUserRole()
                };

                return View(model);
            }

            throw new AuthorizationException("Unauthorized");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Access(AccessViewModel model)
        {
            var roles = _authorizationService.GetRegionRolesForCurrentUser();
            if ((model.Regions == null || !model.Regions.Any()) && roles.First(r => r.RoleId == model.RoleId).ShowRegions)
            {
                ModelState.AddModelError("", "Select at least one region");
            }

            if (ModelState.IsValid)
            {
                _authorizationService.UpdateUserRole(model.UserId, model.RoleId, model.Regions);

                User.InvalidateUserRole(model.UserId);
                _jsonWebTokenService.GetAsync(
                    new string[] { _urlService.GetInvalidateUserRoleByTokenURL(false), _urlService.GetInvalidateUserRoleByTokenURL(true) },
                    new CacheInvalidateUserRoleJsonWebToken { UserId = model.UserId.ToString() }
                );

                return Redirect(model.ReturnUrl);
            }

            model.Roles = roles;
            model.CurrentUserRole = _authorizationService.GetCurrentUserRole();

            return View(model);
        }
    }
}