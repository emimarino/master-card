﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Models;
using Pulsus;
using Slam.Cms.Common;
using Slam.Cms.Configuration;
using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    public class FileController : CmsBaseController
    {
        private readonly IFileUploadService _fileUploadService;

        public FileController(IFileUploadService fileUploadService)
        {
            _fileUploadService = fileUploadService;
        }

        [CustomAuthorize(allowAllMasterCardUsers: true)]
        public ActionResult Upload(string uploadLocationSetting, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                var fileInfo = GetFileInfo(uploadLocationSetting, SaveFile(uploadLocationSetting, file));

                return JsonNet(new { fileInfo });
            }

            return JsonNet(new { message = "Not a valid file" });
        }

        [HttpPost]
        public JsonResult UploadChunk(string uploadLocationSetting, string key, string identifier, HttpPostedFileBase file, int chunks, int chunk, string name, string contentItemId, string fieldDefinitionName, int actionId)
        {
            string status;
            string message = string.Empty;
            int activityId = -1;
            UploadFileInfo fileInfo = null;
            if (identifier.IsGuid() && key == (identifier + Request.UrlReferrer.PathAndQuery.Replace("//", "/")).GetMD5Hash())
            {
                try
                {
                    var id = Guid.Parse(identifier.ToString());
                    if (file == null || file.ContentLength == 0 || file.InputStream == null)
                    {
                        Response.StatusCode = 500;
                        status = "error";
                        message = "Invalid chunk";
                    }

                    if (!_fileUploadService.ContainsKey(id))
                    {
                        _fileUploadService.Add(id, name);
                    }

                    var thisFile = _fileUploadService.GetFile(id);

                    if (!thisFile.ContainsChunk(chunk))
                    {
                        using (BinaryReader reader = new BinaryReader(file?.InputStream))
                        {
                            var data = reader.ReadBytes((int)file?.InputStream?.Length);
                            thisFile.Add(chunk, data);
                        }
                    }

                    if (thisFile.Count() != chunks)
                    {
                        status = "chunkOk";
                        message = "Chunk uploaded";
                        activityId = 0;
                    }
                    else
                    {
                        var path = GetPath(uploadLocationSetting, name);
                        var fileResult = thisFile.Save(path, name);
                        _fileUploadService.RemoveKey(id);
                        status = "ok";
                        fileInfo = GetFileInfo(uploadLocationSetting, fileResult);
                        activityId = _fileUploadService.CreateUploadActivity(fileInfo, contentItemId, fieldDefinitionName, UserContext.User.UserId, actionId, MarketingCenterDbConstants.UploadActivity.Status.Pending);
                    }
                }
                catch (Exception ex)
                {
                    Response.StatusCode = 500;
                    status = "error";
                    message = "Invalid chunk";
                    activityId = _fileUploadService.CreateUploadActivity(fileInfo, contentItemId, fieldDefinitionName, UserContext.User.UserId, actionId, MarketingCenterDbConstants.UploadActivity.Status.Error);

                    LogManager.EventFactory.Create()
                                           .Text("Upload activity tracking")
                                           .Level(LoggingEventLevel.Warning)
                                           .AddTags("UploadActivity")
                                           .AddData("Status", status)
                                           .AddData("Message", message)
                                           .AddData("UploadActivityId", activityId)
                                           .AddException(ex)
                                           .Push();
                }
            }
            else
            {
                Response.StatusCode = 500;
                status = "error";
                message = "Invalid chunk";
                activityId = _fileUploadService.CreateUploadActivity(fileInfo, contentItemId, fieldDefinitionName, UserContext.User.UserId, actionId, MarketingCenterDbConstants.UploadActivity.Status.Error);

                LogManager.EventFactory.Create()
                                       .Text("Upload activity tracking")
                                       .Level(LoggingEventLevel.Warning)
                                       .AddTags("UploadActivity")
                                       .AddData("Status", status)
                                       .AddData("Message", message)
                                       .AddData("UploadActivityId", activityId)
                                       .Push();
            }

            if (activityId == -1)
            {
                LogManager.EventFactory.Create()
                                       .Text("Upload activity tracking")
                                       .Level(LoggingEventLevel.Warning)
                                       .AddTags("UploadActivity")
                                       .AddData("Status", status)
                                       .AddData("Message", message)
                                       .AddData("UploadLocationSetting", uploadLocationSetting)
                                       .AddData("Key", key)
                                       .AddData("Identifier", identifier)
                                       .AddData("File", file)
                                       .AddData("Chunks", chunks)
                                       .AddData("Chunk", chunk)
                                       .AddData("Name", name)
                                       .AddData("ContentItemId", contentItemId)
                                       .AddData("FieldDefinitionName", fieldDefinitionName)
                                       .AddData("UserId", UserContext.User.UserId)
                                       .AddData("ActionId", actionId)
                                       .AddData("FileInfo", fileInfo)
                                       .AddData("UploadActivityId", activityId)
                                       .Push();
            }

            return Json(new { status, message, fileInfo, activityId });
        }

        [CustomAuthorize(allowAllMasterCardUsers: true)]
        public ActionResult GetFile(string uploadLocationSetting, string file)
        {
            var filePath = GetPath(uploadLocationSetting, file);

            if (!System.IO.File.Exists(filePath))
            {
                LogManager.EventFactory.Create()
                    .Text("File not found:" + filePath)
                    .Level(LoggingEventLevel.Alert)
                    .Push();

                return HttpNotFound();
            }

            return File(filePath, file.GetMimeType(), Path.GetFileName(file));
        }

        [CustomAuthorize(allowAllMasterCardUsers: true)]
        [HttpPost]
        public ActionResult Delete(string filename)
        {
            return JsonNet(new { ok = true, filename = filename.ToRelativeUri() });
        }

        [CustomAuthorize(allowAllMasterCardUsers: true)]
        public ActionResult Info(string uploadLocationSetting, string filename)
        {
            return JsonNet(GetFileInfo(uploadLocationSetting, filename));
        }

        [CustomAuthorize(allowAllMasterCardUsers: true)]
        [HttpPost]
        public ActionResult RegisterUploadActivity(string file, long size, string contentItemId, string fieldDefinitionName, int action)
        {
            try
            {
                var activityId = _fileUploadService.CreateUploadActivity(
                    new UploadFileInfo
                    {
                        name = file,
                        isAbsoluteUrl = false,
                        size = size
                    },
                    contentItemId,
                    fieldDefinitionName,
                    UserContext.User.UserId,
                    action,
                    MarketingCenterDbConstants.UploadActivity.Status.Pending
                );

                return Json(new { success = true, activityId });
            }
            catch (Exception ex)
            {
                var activityId = _fileUploadService.CreateUploadActivity(
                    new UploadFileInfo
                    {
                        name = file,
                        isAbsoluteUrl = false,
                        size = size
                    },
                    contentItemId,
                    fieldDefinitionName,
                    UserContext.User.UserId,
                    action,
                    MarketingCenterDbConstants.UploadActivity.Status.Error
                );

                var loggedEvent = LogManager.EventFactory.Create()
                    .Text("Upload activity tracking")
                    .Level(LoggingEventLevel.Warning)
                    .AddTags("UploadActivity")
                    .AddData("UploadActivityId", activityId)
                    .AddException(ex)
                    .Push();

                return Json(new { eventId = loggedEvent.LoggingEvent.EventId, activityId });
            }
        }

        protected UploadFileInfo GetFileInfo(string uploadLocationSetting, string fileUrl)
        {
            var filePath = GetPath(uploadLocationSetting, fileUrl);
            var size = 0L;

            if (System.IO.File.Exists(filePath))
            {
                var fileInfo = new FileInfo(filePath);
                size = fileInfo.Length;
            }

            return new UploadFileInfo { size = size, name = fileUrl.ToRelativeUri(), isAbsoluteUrl = fileUrl.IsAbsoluteUrl() };
        }

        private string GetPath(string uploadLocationSetting, string fileName)
        {
            return Path.Combine(Server.MapPath(ConfigurationManager.Solution.Settings[uploadLocationSetting]), fileName.TrimStart('/'));
        }

        private string SaveFile(string uploadLocationSetting, HttpPostedFileBase file)
        {
            var guid = Guid.NewGuid().ToString();
            var postedFile = Path.GetFileName(file.FileName);
            var filename = Path.Combine(guid, postedFile);
            var path = GetPath(uploadLocationSetting, filename);

            if (!Directory.Exists(Path.GetDirectoryName(path)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(path));
            }

            file.SaveAs(path);

            return filename;
        }

        private string SaveFile(string uploadLocationSetting, byte[] bytes, string FileName)
        {
            var guid = Guid.NewGuid().ToString();
            var postedFile = Path.GetFileName(FileName);
            var filename = Path.Combine(guid, postedFile);
            var path = GetPath(uploadLocationSetting, filename);

            if (!Directory.Exists(Path.GetDirectoryName(path)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(path));
            }

            using (FileStream fileStream = new FileStream(path, FileMode.Append))
            {
                fileStream.Write(bytes, 0, bytes.Count());
            }

            return filename;
        }

        private void DeleteFile(string uploadLocationSetting, string filename)
        {
            var filePath = GetPath(uploadLocationSetting, filename);

            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);

                if (IsDirectoryEmpty(Path.GetDirectoryName(filePath)))
                {
                    Directory.Delete(Path.GetDirectoryName(filePath), true);
                }
            }
        }

        private bool IsDirectoryEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }
    }
}