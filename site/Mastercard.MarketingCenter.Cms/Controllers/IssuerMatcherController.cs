﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using Mastercard.MarketingCenter.Cms.Core.DataTables;
using Mastercard.MarketingCenter.Cms.Models;
using Mastercard.MarketingCenter.Cms.Models.Content;
using Mastercard.MarketingCenter.Cms.Models.Issuers;
using Mastercard.MarketingCenter.Cms.Services.Services.Issuers;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    [CustomAuthorize("CanManageIssuerMatcher")]
    public class IssuerMatcherController : CmsBaseController
    {
        private readonly MatchingServices _matchingServices;
        private readonly IUserService _userService;
        private readonly IIssuerService _issuerService;
        private readonly IImportedIcaService _importedIcaService;

        public IssuerMatcherController(MatchingServices matchingService, IUserService userService, IIssuerService issuerService, IImportedIcaService importedIcaService)
        {
            _matchingServices = matchingService;
            _userService = userService;
            _issuerService = issuerService;
            _importedIcaService = importedIcaService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Details(string id)
        {
            var issuerData = _issuerService.GetIssuerDetail(id);

            return View(issuerData);
        }

        [HttpPost]
        [UnitOfWork]
        public ActionResult RemoveMatch(string id)
        {
            _matchingServices.ClearMatch(id);

            return new EmptyResult();
        }

        public ActionResult GetActiveUsersForIssuer(GetActiveUsersForIssuerTableRequest request)
        {
            var allActiveUsers = _userService.GetActiveUsersByIssuerId(request.IssuerId).ToList();
            var total = allActiveUsers.Count();

            var pagedUsers = allActiveUsers.OrderByDescending(o => o.LastLoginDate)
                .Skip(request.Start)
                .Take(request.Length);

            return JsonNet(new DataTablesResponse(request.Draw, pagedUsers, total, total));
        }

        public ActionResult GetIssuerSearchResults(GetIssuerSearchTableRequest request)
        {
            var importedIcas = _importedIcaService.GetSearchImportedIcas(request.IssuerToSearch);

            var total = importedIcas.Count();

            var pagedIssuers = importedIcas.OrderBy(o => o.LegalName)
                                           .Skip(request.Start)
                                           .Take(request.Length);

            return JsonNet(new DataTablesResponse(request.Draw, pagedIssuers, total, total));
        }

        public ActionResult GetActiveIssuers(GetActiveIssuersDataTablesRequest request)
        {
            var allActiveIssuers = _matchingServices.GetActiveIssuers();

            if (!string.IsNullOrEmpty(request.Search.Value))
            {
                allActiveIssuers =
                    allActiveIssuers
                    .Where(o => (!string.IsNullOrEmpty(o.Cid) && o.Cid.IndexOf(request.Search.Value, StringComparison.InvariantCultureIgnoreCase) != -1)
                        || o.Title.IndexOf(request.Search.Value, StringComparison.InvariantCultureIgnoreCase) != -1
                        || o.Processor.IndexOf(request.Search.Value, StringComparison.InvariantCultureIgnoreCase) != -1);
            }

            var status = request.Status.ToLowerInvariant();

            switch (status)
            {
                case "set":
                    allActiveIssuers = allActiveIssuers.Where(o => !string.IsNullOrEmpty(o.Cid));
                    break;
                case "missing":
                    allActiveIssuers = allActiveIssuers.Where(o => string.IsNullOrEmpty(o.Cid) && !o.MatchConfirmed.HasValue);
                    break;
                case "unverified":
                    allActiveIssuers = allActiveIssuers.Where(o => o.MatchConfirmed == false);
                    break;
            }

            var orderBy = request.Columns.FirstOrDefault(o => o.IsOrdered);

            allActiveIssuers = allActiveIssuers.ToList();

            if (orderBy == null)
            {
                allActiveIssuers = allActiveIssuers.OrderBy(o => o.Title);
            }
            else
            {
                allActiveIssuers = orderBy.SortDirection == Column.OrderDirection.Ascendant
                    ? allActiveIssuers.OrderBy(o => o.GetType().GetProperty(orderBy.Data).GetValue(o, null))
                    : allActiveIssuers.OrderByDescending(o => o.GetType().GetProperty(orderBy.Data).GetValue(o, null));
            }

            var total = allActiveIssuers.Count();
            var activeIssuers = allActiveIssuers.Skip(request.Start)
                                                .Take(request.Length);

            return JsonNet(new DataTablesResponse(request.Draw, activeIssuers, total, total));
        }

        [HttpPost]
        public ActionResult ExecuteAutomatch()
        {
            _matchingServices.RunAutoMatching();

            return JsonNet(new { ok = true });
        }

        [ChildActionOnly]
        public PartialViewResult MatchedIssuerDetails(string issuerId)
        {
            var issuer = _issuerService.GetIssuerById(issuerId);
            var cid = issuer.Cid ?? issuer.UnconfirmedCid;
            var importedIca = _importedIcaService.GetByCid(cid);

            var model = new MatchedIssuerModel
            {
                Cid = cid,
                ImportedIca = importedIca,
                IssuerId = issuer.IssuerId,
                MatchConfirmed = issuer.MatchConfirmed.GetValueOrDefault()
            };

            return PartialView(model);
        }

        [HttpPost]
        [UnitOfWork]
        public ActionResult MatchIssuer(string issuerToMatchId, string cid)
        {
            _matchingServices.MatchIssuer(issuerToMatchId, cid, UserContext.User);

            return JsonNet(new { ok = true, issuerId = issuerToMatchId });
        }

        [HttpPost]
        [UnitOfWork]
        public ActionResult ClearMatch(string issuerId)
        {
            _matchingServices.ClearMatch(issuerId);

            return JsonNet(new { ok = true, issuerId });
        }
    }
}