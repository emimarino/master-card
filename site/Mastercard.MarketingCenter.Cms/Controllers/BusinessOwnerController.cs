﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using Mastercard.MarketingCenter.Cms.Core.Extensions;
using Mastercard.MarketingCenter.Cms.Models;
using Mastercard.MarketingCenter.Cms.Services.Core;
using Mastercard.MarketingCenter.Cms.Services.Infrastructure;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Extensions;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Entities;
using Mastercard.MarketingCenter.Services.Exceptions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Newtonsoft.Json;
using Pulsus;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    [BusinessOwnerExpirationManagementEnabled]
    [MastercardUser]
    public class BusinessOwnerController : Controller
    {
        private readonly SlamContext _slamContext;
        private readonly UserContext _userContext;
        private readonly ContentTypeDao _contentTypeDao;
        private readonly ContentService _contentService;
        private readonly ContentItemDao _contentItemDao;
        private readonly IContentCommentServices _contentCommentServices;
        private readonly IEmailService _emailServices;
        private readonly IRegionService _regionService;
        private readonly IContentReviewServices _contentReviewServices;
        private readonly IContentItemService _contentItemServices;
        private readonly Common.Services.IdGeneratorService _idGeneratorService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IIconService _iconService;
        private readonly IBussinesOwnerService _bussinesOwnerServices;
        private readonly MastercardAuthenticationService _authenticationService;
        private readonly TagServices _tagServices;
        private readonly IFileUploadService _fileUploadService;
        private readonly ISettingsService _settingsService;

        private int pageSize = 10;

        private const string DefaultContentTypeIdForSuggestion = MarketingCenterDbConstants.ContentTypeIds.DownloadableAsset; // Downloadable Asset is used
        private const string AlternativeContentTypeIdForSuggestion = MarketingCenterDbConstants.ContentTypeIds.Asset; // Asset is used

        public BusinessOwnerController(
            SlamContext slamContext,
            UserContext userContext,
            IContentCommentServices contentCommentServices,
            ContentTypeDao contentTypeDao,
            ContentService contentService,
            ContentItemDao contentItemDao,
            IEmailService emailServices,
            IRegionService regionService,
            IContentReviewServices contentReviewServices,
            IContentItemService contentItemServices,
            Common.Services.IdGeneratorService idGeneratorService,
            IUnitOfWork unitOfWork,
            IIconService iconService,
            IBussinesOwnerService bussinesOwnerServices,
            MastercardAuthenticationService authenticationService,
            TagServices tagServices,
            IFileUploadService fileUploadService,
            ISettingsService settingsService)
        {
            _contentTypeDao = contentTypeDao;
            _contentItemServices = contentItemServices;
            _contentService = contentService;
            _contentItemDao = contentItemDao;
            _slamContext = slamContext;
            _userContext = userContext;
            _contentCommentServices = contentCommentServices;
            _emailServices = emailServices;
            _regionService = regionService;
            _contentReviewServices = contentReviewServices;
            _idGeneratorService = idGeneratorService;
            _unitOfWork = unitOfWork;
            _iconService = iconService;
            _bussinesOwnerServices = bussinesOwnerServices;
            _authenticationService = authenticationService;
            _tagServices = tagServices;
            _fileUploadService = fileUploadService;
            _settingsService = settingsService;
        }

        [ChildActionOnly]
        public ActionResult InitializeTabHeader(string activeTab)
        {
            var model = new BusinessOwnerTabHeaderViewModel
            {
                Controller = "BusinessOwner",
                ActiveTab = activeTab,
                Tabs = new List<BusinessOwnerTabHeaderTabViewModel>
                {
                    new BusinessOwnerTabHeaderTabViewModel {Action = "PublishedAssets", Name = Resources.BusinessOwnerPages.PublishedAssetsTitle},
                    new BusinessOwnerTabHeaderTabViewModel {Action = "PendingApproval", Name = Resources.BusinessOwnerPages.PendingApprovalTitle}
                }
            };

            return PartialView("TabbedHeader", model);
        }

        public ActionResult PublishedAssets(int pageNumber = 1, bool showAllAssets = true, int? expirationWindowInDays = 1, bool openExtendModal = false, int extendModalState = 0, string extendModalContentItemId = "", bool fromDetails = false)
        {
            var items = _contentItemServices.GetUserOwnedAssetContentItems(_userContext.User.UserId,
                                                                           MarketingCenterDbConstants.ExpirationReviewStates.NotAvailable,
                                                                           showAllAssets ? null : expirationWindowInDays,
                                                                           true,
                                                                           extendModalContentItemId);

            int totalPages = (int)Math.Ceiling((double)items.Count() / pageSize);
            var page = pageNumber < 1 ? 1 : (pageNumber > totalPages ? totalPages : pageNumber);
            var model = new BusinessOwnerPublishedAssetsViewModel
            {
                CurrentUserName = _userContext.User.FullName,
                TotalPages = totalPages,
                Page = pageNumber < 1 ? 1 : (pageNumber > totalPages ? totalPages : pageNumber),
                ShowAllAssets = showAllAssets,
                ExpirationDays = new List<int> { 1, 30, 60, 90 },
                ExpirationWindowInDays = expirationWindowInDays.HasValue ? expirationWindowInDays.Value : 1,
                OpenExtendModal = openExtendModal,
                ExtendModalState = extendModalState,
                ExtendModalContentItemId = extendModalContentItemId,
                OpenSuccessModal = fromDetails,
                BusinessOwnerAssets = GetAssets(items, true, page)
            };

            return View(model);
        }

        private IEnumerable<ReviewStateSelectOption> GetReviewstateOptions()
        {
            return _contentReviewServices.GetExpirationReviewStates().Where(rs => rs.ReviewStateId != MarketingCenterDbConstants.ExpirationReviewStates.NotAvailable).Select(rs =>
                        new ReviewStateSelectOption { Name = rs.Name, Color = rs.Color, ReviewStateId = rs.ReviewStateId, CssClassName = rs.Name.Replace(" ", "") }).OrderBy(rs => rs.Name);
        }

        public ActionResult PendingApproval(
            int pageNumber = 1,
            int expirationWindowInDays = -1,
            int reviewState = MarketingCenterDbConstants.ExpirationReviewStates.AllAssetsWithAStateAvailable,
            bool openExtendModal = false,
            int extendModalState = 0)
        {
            var verifiedReviewState = reviewState.Equals(0) ? -1 : reviewState;

            var items = _contentItemServices.GetUserOwnedAssetContentItems(
                _userContext.User.UserId,
                verifiedReviewState,
                (expirationWindowInDays >= 0 ? (int?)expirationWindowInDays : null),
                false);

            int totalPages = (int)Math.Ceiling((double)items.Count() / pageSize);
            var page = pageNumber < 1 ? 1 : (pageNumber > totalPages ? totalPages : pageNumber);

            var businessOwnerAssets = GetAssets(items, false, page, true);
            var reviewStates = GetReviewstateOptions();
            var model = new BusinessOwnerPendingApprovalViewModel
            {
                CurrentUserId = _userContext.User.UserId,
                CurrentUserName = _userContext.User.FullName,
                TotalPages = totalPages,
                Page = page,
                ExpirationWindowInDays = expirationWindowInDays,
                BusinessOwnerAssets = businessOwnerAssets,
                TotalResultsInPage = businessOwnerAssets.Count(),
                ExpirationReviewStates = reviewStates,
                DefaultReviewState = reviewStates.First(rs => rs.ReviewStateId.Equals(verifiedReviewState == -1 ? 1 : verifiedReviewState)),
                ShowAll = verifiedReviewState.Equals(MarketingCenterDbConstants.ExpirationReviewStates.AllAssetsWithAStateAvailable)
            };

            return View(model);
        }

        private IEnumerable<BusinessOwnerAsset> GetAssets(
            IEnumerable<ContentItem> items,
            bool showPublishedOnly,
            int page,
            bool getReviewStates = false)
        {
            var reviewStates = getReviewStates ? GetReviewstateOptions() : null;
            var comments = _contentCommentServices.GetCommentsCount(_userContext.User.UserId).ToList();
            var assets = new List<BusinessOwnerAsset>();
            foreach (var item in items.Skip((page - 1) * pageSize).Take(pageSize))
            {
                var asset = new BusinessOwnerAsset
                {
                    ContentItemId = showPublishedOnly ? item.PrimaryContentItemId : item.ContentItemId,
                    ExpirationDate = item.ExpirationDate.Value,
                    CommentCount = comments.FirstOrDefault(c => c.ContentItemId == item.PrimaryContentItemId)?.CommentsCount ?? 0,
                    NewComments = false,
                    ReviewState = getReviewStates ? reviewStates.First(rs => rs.ReviewStateId.Equals(item.ReviewStateId)) : new ReviewStateSelectOption()
                };

                if (item is IContentItemWithTitle)
                {
                    asset.Title = ((IContentItemWithTitle)item).Title;
                }

                if (item is AssetDTO)
                {
                    var dto = (AssetDTO)item;
                    asset.ShortDescription = dto.ShortDescription.RemoveHtmlTags();
                    asset.ThumbnailUrl = string.IsNullOrEmpty(dto.MainImage) ? dto.ThumbnailImage : dto.MainImage;
                }
                else if (item is DownloadableAssetDTO)
                {
                    var dto = (DownloadableAssetDTO)item;
                    asset.ShortDescription = dto.ShortDescription.RemoveHtmlTags();
                    asset.ThumbnailUrl = string.IsNullOrEmpty(dto.MainImage) ? dto.ThumbnailImage : dto.MainImage;
                }

                assets.Add(asset);
            }

            return assets;
        }

        public ActionResult Details(string id, bool showPublishedOnly = true)
        {
            /*string validId = showPublishedOnly ? id.GetAsLiveContentItemId() : id;
            var model = GetBusinessOwnerContentModel(validId, showPublishedOnly);

            if (!_userContext.User.UserId.Equals(model.BusinessOwnerId))
            {
                // the user is not a Business Owner
                throw new AuthorizationException();
            }*/

            return HttpNotFound();
        }

        public ActionResult ViewInPortal(string id)
        {
            var item = _contentItemServices.GetContentItem(id);
            _userContext.SetUserSelectedRegion(item.RegionId); //Ensure region selection is set for the item's region
            _authenticationService.RemoveSlamPreviewModeCookie(); //Show Live version on FrontEnd
            return Redirect(Slam.Cms.Configuration.ConfigurationManager.Environment.FrontEndUrl.TrimEnd('/') + item.FrontEndUrl);
        }

        public ActionResult GetContentVersionForHistory(BusinessOwnerContentModel model)
        {
            return new JsonResult()
            {
                Data = model.ContentItemData as Dictionary<string, object>
            };
        }

        /// <summary>
        /// Create a suggestion
        /// </summary>
        /// <param name="contentTypeId">By default it is always an Asset</param>
        public ActionResult Create(string contentTypeId = DefaultContentTypeIdForSuggestion, string returnUrl = "")
        {
            //var model = GetBusinessOwnerContentModelForNewAsset(contentTypeId, null, returnUrl);
            //return View("Details", model);
            return HttpNotFound();
        }

        [HttpPost]
        [UnitOfWork]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Details(BusinessOwnerContentModel model, BusinessOwnerActions action, ICollection<ContentCommentDTO> contentComments, bool showPublishedOnly = true)
        {
            ActionResult result = View(model);
            switch (action)
            {
                case BusinessOwnerActions.Create:
                    result = CreateAsset(model, contentComments, showPublishedOnly);
                    break;
                case BusinessOwnerActions.Update:
                    result = UpdateAsset(model, "", contentComments, showPublishedOnly);
                    break;
            }

            return result;
        }

        private ActionResult UpdateAsset(BusinessOwnerContentModel model, string contentItemId = "", ICollection<ContentCommentDTO> contentComments = null, bool showPublishedOnly = true)
        {
            ValidateModel(model, false);
            var returnAction = showPublishedOnly ? "PublishedAssets" : "PendingApproval";
            model.ReturnUrl = Url.Action(returnAction, "BusinessOwner");
            model.ShowPublishedOnly = showPublishedOnly;

            if (ModelState.IsValid)
            {
                var contentItemIdToUse = contentItemId;
                if (string.IsNullOrEmpty(contentItemIdToUse))
                {
                    contentItemIdToUse = model.ContentItemData.GetContentItemId();
                }

                var senderUser = _userContext.User;
                var contentItem = _contentItemServices.TryToGetDraft(contentItemIdToUse);
                contentItemIdToUse = contentItem.ContentItemId;
                var stateIsNA = contentItem.ReviewStateId.Equals(MarketingCenterDbConstants.ExpirationReviewStates.NotAvailable);

                var previousData = _contentService.GetContentItemData(contentItemIdToUse);
                _contentItemServices.PopulateComputedDefaultValues(model.ContentItemData, model.ContentTypeFieldDefinitions, null, previousData);
                if (model.BusinessOwnerId > 0)
                {
                    var key = model.ContentItemData.Keys.FirstOrDefault(k => k.ToLower().Equals(MarketingCenterDbConstants.FieldNames.BusinessOwnerId.ToLower()));
                    key = string.IsNullOrEmpty(key) ? MarketingCenterDbConstants.FieldNames.BusinessOwnerId : key;
                    model.ContentItemData.AddOrUpdate(key, model.BusinessOwnerId);

                }
                _contentService.UpdateContent(model.ContentItemData, ContentActions.SaveAsDraft, senderUser.UserId, model.RegionId, true, stateIsNA ? MarketingCenterDbConstants.ExpirationReviewStates.AssetModified : contentItem.ReviewStateId, model.OriginalContentItemData);

                if (!string.IsNullOrEmpty(contentItemIdToUse))
                {
                    var hasLiveVersion = _contentItemServices.ContentHasLiveVersion(contentItem);
                    if (stateIsNA && hasLiveVersion)
                    {
                        _bussinesOwnerServices.UpdateReviewState(contentItem, MarketingCenterDbConstants.ExpirationReviewStates.AssetModified);
                        contentItemIdToUse = contentItemIdToUse.GetAsDraftContentItemId();
                    }
                    _contentService.UpdateContentExpirationDate(model.ExpirationDate, (hasLiveVersion ? contentItemIdToUse.GetAsDraftContentItemId() : contentItemIdToUse), false);
                    _fileUploadService.SaveUploadActivityByIds(model.UploadActivityIds);

                    if (contentComments != null && _contentCommentServices.AddNewComments(senderUser.UserId, contentComments?.ToList()))
                    {
                        _unitOfWork.Commit();
                    }

                    _emailServices.SendModifiedAssetNotification(model.ContentItemTitle ?? model.ContentItemData.GetData("Title").ToString(), contentItemIdToUse, model.RegionId, _userContext.User.FullName);
                }

                return RedirectToAction(returnAction, "BusinessOwner", new { fromDetails = true });
            }

            ViewData["commentsSeed"] = JsonConvert.SerializeObject(contentComments);

            return StayOnView(model, "Details", model.ShowPublishedOnly);
        }

        private ActionResult CreateAsset(BusinessOwnerContentModel model, ICollection<ContentCommentDTO> contentComments = null, bool showPublishedOnly = true)
        {
            model.ContentItemData[MarketingCenterDbConstants.FieldNames.ContentItemId] = _idGeneratorService.GetNewUniqueId();
            _contentItemServices.PopulateDefaultValuesForSuggestion(model.ContentItemData, _userContext.User.UserId);
            _contentItemServices.PopulateComputedDefaultValues(model.ContentItemData, model.ContentTypeFieldDefinitions);

            // wierd logic: if user selected DownloadableFile then it's a Downloadable Asset, otherwise just Asset
            var contentTypeId = model.ContentItemData.ContainsKey(MarketingCenterDbConstants.FieldNames.Filename) &&
                !string.IsNullOrWhiteSpace(model.ContentItemData[MarketingCenterDbConstants.FieldNames.Filename] as string) ?
                    DefaultContentTypeIdForSuggestion :
                    AlternativeContentTypeIdForSuggestion;

            if (contentTypeId != DefaultContentTypeIdForSuggestion)
            {
                SetContentType(model, contentTypeId);
            }

            ValidateModel(model, false);
            var returnAction = showPublishedOnly ? "PublishedAssets" : "PendingApproval";
            model.ReturnUrl = Url.Action(returnAction, "BusinessOwner");
            model.ShowPublishedOnly = showPublishedOnly;

            if (ModelState.IsValid)
            {
                var senderUser = _userContext.User;
                var newContentItemId = _contentService.AddContent(
                     model.ContentTypeId,
                     model.ContentItemData,
                     senderUser.UserId,
                     model.RegionTitle,
                     ContentActions.SaveAsDraft,
                     MarketingCenterDbConstants.ExpirationReviewStates.NewAsset);

                if (!string.IsNullOrEmpty(newContentItemId))
                {
                    _contentService.UpdateContentExpirationDate(
                        model.ExpirationDate,
                        model.ContentItemData.GetContentItemId(),
                        false);
                    _fileUploadService.SaveUploadActivityByIds(model.UploadActivityIds);
                    _emailServices.SendNewAssetNotification(model.ContentItemTitle ?? model.ContentItemData.GetData("Title").ToString(), newContentItemId, model.RegionId, _userContext.User.FullName);

                    if (contentComments != null && contentComments.Any())
                    {
                        var comments = contentComments.Select(cc => new ContentCommentDTO { CommentBody = cc.CommentBody, ContentItemID = cc.ContentItemID ?? newContentItemId });
                        if (_contentCommentServices.AddNewComments(senderUser.UserId, comments))
                        {
                            _unitOfWork.Commit();
                        }
                    }
                }

                return RedirectToAction(returnAction, "BusinessOwner");
            }

            // reverting Content Type back to default because Validation didn't pass and user needs to enter data again
            SetContentType(model, DefaultContentTypeIdForSuggestion);
            model.IsNewForm = true;
            ViewData["commentsSeed"] = JsonConvert.SerializeObject(contentComments);

            return StayOnView(model, "Details", false);
        }

        private void SetContentType(BusinessOwnerContentModel model, string contentTypeId)
        {
            model.ContentTypeId = contentTypeId;

            if (contentTypeId == AlternativeContentTypeIdForSuggestion)
            {
                // Downloadable Asset and Asset have different Field names for attachments, so we need to swap data between them
                if (model.ContentItemData.ContainsKey(MarketingCenterDbConstants.FieldNames.DownloadableAssetAttachment))
                {
                    model.ContentItemData[MarketingCenterDbConstants.FieldNames.AssetAttachment] = model.ContentItemData[MarketingCenterDbConstants.FieldNames.DownloadableAssetAttachment];
                    model.ContentItemData.Remove(MarketingCenterDbConstants.FieldNames.DownloadableAssetAttachment);
                }

                // Downloadable File is not on Asset
                model.ContentItemData.Remove(MarketingCenterDbConstants.FieldNames.Filename);
            }
            else
            {
                // Downloadable Asset and Asset have different Field names for attachments, so we need to swap data between them
                if (model.ContentItemData.ContainsKey(MarketingCenterDbConstants.FieldNames.AssetAttachment))
                {
                    model.ContentItemData[MarketingCenterDbConstants.FieldNames.DownloadableAssetAttachment] = model.ContentItemData[MarketingCenterDbConstants.FieldNames.AssetAttachment];
                    model.ContentItemData.Remove(MarketingCenterDbConstants.FieldNames.AssetAttachment);
                }

                if (!model.ContentItemData.ContainsKey(MarketingCenterDbConstants.FieldNames.Filename))
                {
                    // Reverting data back because validation didn't pass and we want Downloadable Asset fields back
                    model.ContentItemData[MarketingCenterDbConstants.FieldNames.Filename] = null;
                }
            }
        }

        private ActionResult StayOnView(
            BusinessOwnerContentModel model,
            string viewName,
            bool showPublishedOnly = true)
        {
            if (model.IsNewForm)
            {
                model.AvailableRegions = _regionService.GetAll(false);
            }
            else
            {
                var contentItemId = model.ContentItemData.GetContentItemId();
                var contentItemData = _contentService.GetContentItemData(contentItemId);
                model.ContentItemData = MergeContentItemData(model.ContentItemData, contentItemData);
            }
            var contentType = _contentTypeDao.GetById(model.ContentTypeId);
            model.CanExpire = contentType.CanExpire;
            model.MustExpire = contentType.MustExpire;
            model.ContentTypeFieldDefinitions = contentType.ContentTypeFieldDefinitions.FilterByBusinessOwner(showPublishedOnly);
            model.CurrentUserId = _userContext.User.UserId;

            HtmlHelperExtensions.StripHtmlTextFields(model.ContentItemData, model.ContentTypeFieldDefinitions);

            return View(viewName, model);
        }

        private IDictionary<string, object> MergeContentItemData(IDictionary<string, object> modelData, IDictionary<string, object> originalData)
        {
            var merged = originalData;
            foreach (var pair in modelData)
            {
                // overwriting all original data with data from model
                merged[pair.Key] = pair.Value;
            }

            return merged;
        }

        [HttpPost]
        public ActionResult SubmitNotBusinessOwnerNotification(string contentItemId, string message)
        {
            bool notificationSuccessful = false;

            var contentItem = _contentItemServices.GetContentItem(contentItemId.GetAsLiveContentItemId());
            if (contentItem != null)
            {
                _bussinesOwnerServices.UpdateReviewState(contentItem, MarketingCenterDbConstants.ExpirationReviewStates.NotABusinessOwner);

                if (_contentCommentServices.AddNew(_userContext.User.UserId, contentItem.ContentTypeId, contentItem.ContentItemId, message))
                {
                    var slamContentItem = _contentItemServices.GetAssetContentItem(contentItemId, FilterStatus.None);
                    _emailServices.SendNotAssetBusinessOwnerNotification(contentItemId, ((IContentItemWithTitle)slamContentItem).Title, _userContext.User.FullName, message, contentItem.RegionId);
                    notificationSuccessful = true;
                }

                _unitOfWork.Commit();
            }

            return new HttpStatusCodeResult(notificationSuccessful ? HttpStatusCode.NoContent : HttpStatusCode.NotFound);
        }

        private BusinessOwnerContentModel GetBusinessOwnerContentModel(string contentItemId, bool showPublishedOnly = true)
        {
            var contentItem = _contentItemDao.GetById(contentItemId);
            if (contentItem == null)
            {
                LogManager.EventFactory.Create()
                                        .Text($"No ContentItem Found with ID: {contentItemId}")
                                        .Level(LoggingEventLevel.Information)
                                        .Push();

                throw new NotFoundException();
            }

            var contentItemData = _contentService.GetContentItemData(contentItemId);
            contentItemData.Add(MarketingCenterDbConstants.FieldNames.Language, _tagServices.GetLanguageTagByContentItemId(contentItemId, false)?.Identifier);
            return GetBusinessOwnerContentModel(contentItemData, contentItem.RegionId, contentItem.ContentType, contentItem.ExpirationDate, false, showPublishedOnly);
        }

        private BusinessOwnerContentModel GetBusinessOwnerContentModelForNewAsset(string contentTypeId, string title = null, string returnUrl = "")
        {
            var contentType = _contentTypeDao.GetById(contentTypeId);
            if (contentType == null)
            {
                LogManager.EventFactory.Create()
                                       .Text($"No ContentType Found with ID: {contentTypeId}")
                                       .Level(LoggingEventLevel.Information)
                                       .Push();

                throw new NotFoundException();
            }

            var contentItemData = _contentService.GetDefaultContentItem(contentType, title, Request.UrlReferrer);
            return GetBusinessOwnerContentModel(contentItemData, _userContext.SelectedRegion, contentType, null, true, false, returnUrl);
        }

        private BusinessOwnerContentModel GetBusinessOwnerContentModel(
            IDictionary<string, object> contentItemData,
            string regionId,
            ContentType contentType,
            DateTime? expirationDate,
            bool isNewForm,
            bool showPublishedOnly = true,
            string returnUrl = "")
        {
            var idField = contentType.GetIdField();
            var model = new BusinessOwnerContentModel
            {
                ContentItemData = contentItemData,
                IdFieldValue = contentItemData.ContainsKey(idField.Name) ? contentItemData[idField.Name] : null,
                ContentItemTitle = contentItemData.GetData(MarketingCenterDbConstants.FieldNames.Title)?.ToString(),
                ContentTypeId = contentType.ContentTypeId,
                RegionId = regionId.Trim(),
                RegionTitle = isNewForm ? regionId.Trim() : _regionService.GetById(regionId)?.Name,
                AvailableRegions = _regionService.GetAll(false).FilterRegionsWithDisabledBOInterface(),
                ContentTypeFieldDefinitions = contentType.ContentTypeFieldDefinitions.FilterByBusinessOwner(showPublishedOnly),
                CanExpire = contentType.CanExpire,
                MustExpire = contentType.MustExpire,
                CurrentExpirationDate = expirationDate,
                ExpirationDate = expirationDate,
                BusinessOwnerId = _bussinesOwnerServices.GetBusinessOwnerId(contentItemData),
                CurrentUserId = _userContext.User.UserId,
                IsNewForm = isNewForm,
                ShowPublishedOnly = showPublishedOnly,
                IconUrl = contentType.ContentTypeId == MarketingCenterDbConstants.ContentTypeIds.AssetFullWidth ?
                    _iconService.GetIconUrl((string)contentItemData[MarketingCenterDbConstants.FieldNames.IconId]) :
                    null,
                ReturnUrl = string.IsNullOrWhiteSpace(returnUrl) ? Url.Action((showPublishedOnly ? "PublishedAssets" : "PendingApproval"), "BusinessOwner") : returnUrl
            };

            var fields = model.ContentItemData;
            HtmlHelperExtensions.StripHtmlTextFields(fields, model.ContentTypeFieldDefinitions);

            return model;
        }

        private void ValidateModel(BusinessOwnerContentModel model, bool showPublishedOnly = true)
        {
            var contentType = _contentTypeDao.GetById(model.ContentTypeId);
            var fields = contentType.ContentTypeFieldDefinitions.FilterByBusinessOwner(showPublishedOnly).Select(o => o.FieldDefinition);

            var validationErrors = _contentService.ValidateData(fields, model.ContentItemData);

            var publishedContentItemData = _contentItemServices.GetPublishedContentItem(model.ContentItemData.GetContentItemId().GetAsLiveContentItemId());
            var validationExpirationDateErrors = _contentService.ValidateExpirationDate(contentType.MustExpire, model.ExpirationDate, null, true);

            validationErrors.ForEach(err => ModelState.AddModelError(err.Key, err.ErrorMessage));
            validationExpirationDateErrors.ForEach(err => ModelState.AddModelError(err.Key, err.ErrorMessage));
        }

        [ChildActionOnly]
        public ActionResult _BusinessOwnerLayoutFooter()
        {
            var model = new BusinessOwnerLayoutViewModel();
            var registrationLayoutFooterPage = GetRegistrationLayoutFooterPage();
            model.Footer = registrationLayoutFooterPage?.IntroCopy;

            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult _BusinessOwnerLayoutFooterNav()
        {
            var model = new BusinessOwnerLayoutViewModel();
            var registrationLayoutFooterPage = GetRegistrationLayoutFooterPage();
            if (registrationLayoutFooterPage != null)
            {
                model.Footer = new Regex("<nav class=\"nav-footer\">(.*?)</nav>", RegexOptions.Singleline).Match(registrationLayoutFooterPage.IntroCopy).Value;
            }

            return PartialView(model);
        }

        private PageDTO GetRegistrationLayoutFooterPage()
        {
            return _slamContext.CreateQuery()
                               .FilterRegion(RegionalizeService.UnauthenticatedRegion)
                               .Filter($"Page.Uri LIKE '%{_settingsService.GetSiteApplicationShortName().ToLower()}/registrationlayoutfooter%'")
                               .FilterTaggedWith(_userContext.SelectedLanguage)
                               .Get<PageDTO>()
                               .FirstOrDefault();
        }

        [HttpPost]
        public ActionResult ConfirmDeletion(ContentItemsSelectedModel selected)
        {
            if (selected == null || selected.SelectedContentItems == null || !selected.SelectedContentItems.Any())
            {
                return new HttpStatusCodeResult(HttpStatusCode.NoContent);
            }


            var contentItems = _contentItemServices.GetAssetContentItems(selected.SelectedContentItems, FilterStatus.LatestVersion);
            return PartialView(new ConfirmDeletionViewModel
            {
                AssetTitlesToDelete = contentItems.Cast<IContentItemWithTitle>().Select(x => x.Title),
                AssetIdsToDelete = contentItems.Select(x => x.ContentItemId),
                UserFullName = _userContext.User.FullName
            });
        }

        public ActionResult DeleteAssets(ContentItemsSelectedModel selected)
        {
            if (selected == null || selected.SelectedContentItems == null || !selected.SelectedContentItems.Any())
            {
                return new HttpStatusCodeResult(HttpStatusCode.NoContent);
            }

            foreach (var assetId in selected.SelectedContentItems)
            {
                var contentItem = _contentItemServices.GetContentItem(assetId);

                _bussinesOwnerServices.DeleteByBusinessOwner(contentItem);
            }

            _unitOfWork.Commit();
            return new HttpStatusCodeResult(HttpStatusCode.NoContent);
        }
    }
}