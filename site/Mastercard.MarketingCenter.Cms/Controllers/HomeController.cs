﻿using Mastercard.MarketingCenter.Cms.Models;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Models;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    public class HomeController : CmsBaseController
    {
        private readonly SlamContext _slamContext;
        private readonly IAuthorizationService _authorizationService;
        private readonly UserContext _userContext;
        private readonly IUserService _userService;
        private readonly ISettingsService _settingsService;
        private readonly INavigationService _navigationService;

        public HomeController(
            SlamContext slamContext,
            UserContext userContext,
            IAuthorizationService authorizationService,
            IUserService userService,
            ISettingsService settingsService,
            INavigationService navigationService)
        {
            _slamContext = slamContext;
            _authorizationService = authorizationService;
            _userContext = userContext;
            _userService = userService;
            _settingsService = settingsService;
            _navigationService = navigationService;
        }

        public ActionResult Index()
        {
            var region = UserContext != null ? UserContext.SelectedRegion : ConfigurationManager.AppSettings["DefaultRegion"];
            return View(new IndexModel
            {
                IsGlobalRegion = region.Equals("global", StringComparison.InvariantCultureIgnoreCase),
                BusinessOwnerExpirationManagementEnabled = _settingsService.GetBusinessOwnerExpirationManagementEnabled(region),
                ProgramPrintOrderDateExpirationEnabled = _settingsService.GetProgramPrintOrderDateExpirationEnabled(region)
            });
        }

        [ChildActionOnly]
        public ActionResult _Header(string headerSection)
        {
            var model = new HeaderModel
            {
                Username = UserContext != null ? UserContext.User.FullName : string.Empty,
                SelectedRegion = UserContext != null ? UserContext.SelectedRegion : ConfigurationManager.AppSettings["DefaultRegion"],
                HomeUrl = Request.Url.Scheme + "://" + Request.Url.Authority + ConfigurationManager.AppSettings["PortalUrl"]
            };

            model.LogOutUrl = model.HomeUrl + "/logout";
            model.AvailableRegions = _authorizationService.GetAuthorizedRegions();
            model.HeaderSection = headerSection ?? string.Empty;

            EnsureUserCanAccessSelectedRegion(model.AvailableRegions);

            return PartialView(model);
        }

        private string GetUserDefaultRegion(IEnumerable<Region> availableRegions)
        {
            return (CurrentUser.Region ?? availableRegions?.FirstOrDefault()?.IdTrimmed).Trim();
        }

        private void EnsureUserCanAccessSelectedRegion(IEnumerable<Region> availableRegions)
        {
            if (!availableRegions.Any(r => r.IdTrimmed == _userContext.SelectedRegion))
            {
                var defaultRegion = GetUserDefaultRegion(availableRegions);
                if (string.IsNullOrEmpty(defaultRegion))
                {
                    Response.StatusCode = 403;
                    return;
                }

                _userContext.SetUserSelectedRegion(defaultRegion);
                Response.Redirect(Request.Url.ToString());
            }
        }

        public ActionResult KeepAlive()
        {
            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult ChangeRegion(string selectedRegion)
        {
            _slamContext.InvalidateAllTags();
            _userContext.SetUserSelectedRegion(selectedRegion);
            Session["ChangedRegion"] = "true";
            Session.Remove("NavigationNodeCollection");

            return Redirect(Request.UrlReferrer.ToString());
        }

        [HttpGet]
        public ActionResult ChangeRegion()
        {
            return Redirect(Request.ApplicationPath); //default ChangeRegion GET redirects back to admin home
        }

        [ChildActionOnly]
        public ActionResult _NavBar()
        {
            var model = _navigationService.GetNavigationModel(
                Request.RequestContext.RouteData.Values,
                Request.QueryString);

            Session.Add("BreadCrums", model.GetCurrenPath());

            return PartialView(model);
        }

        public JsonResult BreadCrums()
        {
            var bcs = Session["BreadCrums"] as List<NavigationInfo>;

            return JsonNet(bcs);
        }

        [HttpGet]
        public JsonResult IsAuthenticated()
        {
            if (User.Identity.IsAuthenticated)
            {
                return JsonNet(true);
            }

            return JsonNet(false);
        }
    }
}