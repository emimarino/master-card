﻿using Mastercard.MarketingCenter.Cms.Core.ActionResults;
using Mastercard.MarketingCenter.Cms.Services.Infrastructure;
using Mastercard.MarketingCenter.Common.Filters;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    [NoCache]
    public abstract class CmsBaseController : Controller
    {
        public UserContext UserContext { get; set; }
        public User CurrentUser { get { return UserContext.User; } }

        private IAuthorizationService _authorizationService => DependencyResolver.Current.GetService<IAuthorizationService>();

        protected JsonNetResult JsonNet(object data)
        {
            return new JsonNetResult { Data = data, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        protected void SetPageTitle(string title)
        {
            ViewBag.PageTitle = title + " / ";
        }

        private bool CheckCanAction(string action, string id, string contentAction = null)
        {
            return _authorizationService.Authorize(null, ControllerContext.RouteData.Values["controller"].ToString(), action, id, null, contentAction);
        }

        public bool CheckCanArchive(string id)
        {
            return CheckCanAction("Archive", id);
        }

        public bool CheckCanClone(string id)
        {
            return CheckCanAction("Clone", id);
        }

        public bool CheckCanComment(string id)
        {
            return CheckCanAction("Comment", id);
        }

        public bool CheckCanCopy(string id)
        {
            return CheckCanAction("Copy", id);
        }

        public bool CheckCanCreate(string id)
        {
            return CheckCanAction("Create", id);
        }

        public bool CheckCanDelete(string id)
        {
            return CheckCanAction("Delete", id);
        }

        public bool CheckCanDeleteOnlyDraft(string id)
        {
            return CheckCanAction("DeleteOnlyDraft", id);
        }

        public bool CheckCanDraft(string id)
        {
            return CheckCanAction("Update", id, ContentActions.SaveAsDraft.ToString());
        }

        public bool CheckCanEdit(string id)
        {
            return CheckCanAction("Edit", id);
        }

        public bool CheckCanGetContent(string id)
        {
            return CheckCanAction("GetContent", id);
        }

        public bool CheckCanGetTrackingInfo(string id)
        {
            return CheckCanAction("GetTrackingInfo", id);
        }

        public bool CheckCanHistory(string id)
        {
            return CheckCanAction("History", id);
        }

        public bool CheckCanPreview(string id)
        {
            return CheckCanAction("Update", id, ContentActions.SaveAndPreview.ToString());
        }

        public bool CheckCanPublish(string id)
        {
            return CheckCanAction("Update", id, ContentActions.SaveLive.ToString());
        }

        public bool CheckCanRestore(string id)
        {
            return CheckCanAction("Restore", id) || CheckCanAction("Update", id, ContentActions.Restore.ToString());
        }

        public bool CheckCanUpdate(string id)
        {
            return CheckCanDraft(id) || CheckCanPreview(id) || CheckCanPublish(id);
        }

        public bool CheckCanUpdate(string id, string contentAction)
        {
            return CheckCanAction("Update", id, contentAction);
        }
    }
}