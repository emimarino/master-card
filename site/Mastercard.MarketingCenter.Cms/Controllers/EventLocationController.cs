﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using Mastercard.MarketingCenter.Common.Models;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Models;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using EventLocationModel = Mastercard.MarketingCenter.Cms.Models.EventLocationModel;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    public class EventLocationController : CmsBaseController
    {
        private readonly IEventLocationService _eventLocationService;
        private readonly ICachingService _cachingService;
        private readonly IUrlService _urlService;
        private readonly IJsonWebTokenService _jsonWebTokenService;

        public EventLocationController(ICachingService cachingService, IEventLocationService eventLocationService, IUrlService urlService, IJsonWebTokenService jsonWebTokenService)
        {
            _eventLocationService = eventLocationService;
            _cachingService = cachingService;
            _urlService = urlService;
            _jsonWebTokenService = jsonWebTokenService;
        }

        [CustomAuthorize]
        public ActionResult EventLocationManager(bool invalidateEventLocationTree = false)
        {
            SetPageTitle("Event Location Manager");

            if (invalidateEventLocationTree)
            {
                _cachingService.Delete(_cachingService.GetEventLocationTreeModelCacheKey());
                _jsonWebTokenService.GetAsync(new string[] {_urlService.GetReloadEventLocationModelByTokenURL(false),
                                                            _urlService.GetReloadEventLocationModelByTokenURL(true)},
                                              new CacheInvalidateEventLocationModelJsonWebToken());
                _jsonWebTokenService.GetAsync(new string[] {_urlService.GetReloadOfferModelByTokenURL(false),
                                                            _urlService.GetReloadOfferModelByTokenURL(true)},
                                              new CacheInvalidateOfferModelJsonWebToken());
            }

            return View(_eventLocationService.GetEventLocationTree().RootNode);
        }

        [CustomAuthorize]
        public ActionResult Create(int id)
        {
            SetPageTitle("Create Event Location");
            return View(new EventLocationModel { EventLocationId = id });
        }

        [HttpPost]
        [UnitOfWork]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult Create(EventLocationModel eventLocationModel)
        {
            if (ModelState.IsValid)
            {
                _eventLocationService.AddEventLocationToHierarchy(eventLocationModel.EventLocationId, eventLocationModel.Title, eventLocationModel.PricelessEventCity);
                return RedirectToAction("EventLocationManager", new { invalidateEventLocationTree = true });
            }

            return View(eventLocationModel);
        }

        [CustomAuthorize]
        public ActionResult Update(int id)
        {
            var eventLocationHierarchy = _eventLocationService.GetHierarchyByEventLocationId(id);
            if (eventLocationHierarchy == null)
            {
                return View();
            }

            SetPageTitle("Update Event Location");

            var model = new EventLocationModel
            {
                Title = eventLocationHierarchy.EventLocation.Title,
                PricelessEventCity = eventLocationHierarchy.EventLocation.PricelessEventCity,
                EventLocationId = eventLocationHierarchy.EventLocationId
            };

            return View(model);
        }

        [HttpPost]
        [UnitOfWork]
        [ValidateAntiForgeryToken]
        [CustomAuthorize]
        public ActionResult Update(EventLocationModel eventLocationModel)
        {
            if (ModelState.IsValid)
            {
                var eventLocationHierarchy = _eventLocationService.GetHierarchyByEventLocationId(eventLocationModel.EventLocationId);
                eventLocationHierarchy.EventLocation.Title = eventLocationModel.Title;
                eventLocationHierarchy.EventLocation.PricelessEventCity = eventLocationModel.PricelessEventCity;
                eventLocationHierarchy.EventLocation.ModifiedDate = DateTime.Now;

                _eventLocationService.UpdateEventLocationHierarchy(eventLocationHierarchy);

                return RedirectToAction("EventLocationManager", new { invalidateEventLocationTree = true });
            }

            return View(eventLocationModel);
        }

        [HttpPost]
        [UnitOfWork]
        [CustomAuthorize]
        public ActionResult Delete(int eventLocationId, bool isValidating)
        {
            string locationTitle = null;
            if (isValidating)
            {
                var locations = _eventLocationService.GetEventLocations();
                var locationTree = _eventLocationService.GetEventLocationTree(locations).RootNode;
                var isCurrentOrChildrenInUse = false;
                var locationsInUse = new List<string>();
                IsCurrentOrChildrenInUse(locationTree, locations, eventLocationId, ref isCurrentOrChildrenInUse, locationsInUse);
                if (isCurrentOrChildrenInUse)
                {
                    return JsonNet(new { ok = false, locationsInUse });
                }
                locationTitle = locations.Single(l => l.EventLocationId.Equals(eventLocationId)).Title;
            }

            if (!_eventLocationService.GetHierarchyByEventLocationId(eventLocationId).ParentPositionId.Equals(null) && !isValidating)
            {
                _eventLocationService.DeleteEventLocation(eventLocationId);
                return JsonNet(new { ok = true });
            }
            else
            {
                return JsonNet(new
                {
                    ok = false,
                    eventLocationName = locationTitle
                });
            }
        }

        [HttpGet]
        [CustomAuthorize]
        public ActionResult GetEventLocationTree(string contentItemId)
        {
            var eventLocationTree = _eventLocationService.GetEventLocationTreeByContentId(contentItemId).BuildTree();
            return Json(eventLocationTree.RootNode.Children);
        }

        private void IsCurrentOrChildrenInUse(EventLocationTreeNode node, IEnumerable<EventLocation> eventLocations, int eventLocationId, ref bool offersFound, List<string> locationsInUse)
        {
            foreach (var child in node.Children)
            {
                IsCurrentOrChildrenInUse(child, eventLocations, eventLocationId, ref offersFound, locationsInUse);
                if (eventLocationId.Equals(child.EventLocationId))
                {
                    if (eventLocations.Single(e => e.EventLocationId.Equals(child.EventLocationId)).Offers.Any(x => x.ContentItem.StatusID != Status.Expired && x.ContentItem.StatusID != Status.Archived && x.ContentItem.StatusID != Status.Deleted))
                    {
                        offersFound = true;
                        locationsInUse.Add(child.Title);
                    }
                    else
                    {
                        IsChildrenInUser(child, eventLocations, ref offersFound, locationsInUse);
                    }
                }
            }
        }

        private void IsChildrenInUser(EventLocationTreeNode node, IEnumerable<EventLocation> eventLocations, ref bool offersFound, List<string> locationsInUse)
        {
            foreach (var child in node.Children)
            {
                if (eventLocations.Single(e => e.EventLocationId.Equals(child.EventLocationId)).Offers.Any())
                {
                    offersFound = true;
                    locationsInUse.Add(child.Title);
                }
                IsChildrenInUser(child, eventLocations, ref offersFound, locationsInUse);
            }
        }
    }
}