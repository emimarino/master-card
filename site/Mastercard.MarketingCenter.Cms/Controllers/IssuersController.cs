﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using Mastercard.MarketingCenter.Cms.Core.DataTables;
using Mastercard.MarketingCenter.Cms.Core.Extensions;
using Mastercard.MarketingCenter.Cms.Models;
using Mastercard.MarketingCenter.Cms.Models.Issuers;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    [CustomAuthorize]
    public class IssuersController : CmsBaseController
    {
        private readonly ProcessorService _processorService;
        private readonly ListDao _listDao;
        private readonly UserContext _userContext;
        private readonly IIssuerService _issuerService;
        private const string ShowAllValue = "0";

        public IssuersController(ProcessorService processorService, ListDao listDao, UserContext userContext, IIssuerService issuerService)
        {
            _processorService = processorService;
            _listDao = listDao;
            _userContext = userContext;
            _issuerService = issuerService;
        }

        public ActionResult Manage(string id = null)
        {
            SetPageTitle("Manage Issuers");

            var processors = _processorService.GetProcessors().OrderBy(o => o.Title).ToList();
            var selectedProcessorId = id;
            var issuerList = _listDao.FirstOrDefault(o => o.TableName == MarketingCenterDbConstants.Tables.Issuer);

            if (issuerList == null)
            {
                throw new InvalidOperationException("No Issuer list defined in metadata!");
            }

            if (string.IsNullOrWhiteSpace(selectedProcessorId))
            {
                selectedProcessorId = ShowAllValue;
            }

            processors.Insert(0, new Processor { ProcessorId = ShowAllValue, Title = "Show All" });

            var model = new ManageIssuersModel
            {
                Processors = processors,
                SelectedProcessorId = selectedProcessorId,
                IssuerListId = issuerList.ListId,
                HideHasOrdered = !User.IsInPermission(Common.Infrastructure.Constants.Permissions.CanManageIssuerMatcher)
            };

            return View(model);
        }

        [HttpPost]
        [UnitOfWork]
        public ActionResult Delete(string id)
        {
            _issuerService.DeleteIssuer(_issuerService.GetIssuerById(id));

            return JsonNet(new { ok = true });
        }

        public ActionResult GetIssuers(GetIssuersDataTableRequest request)
        {
            var pagedList = _issuerService.GetIssuersBySearchRequest(_userContext.SelectedRegion, request.ProcessorId, request.Search?.Value, ShowAllValue).Select(i => new
            {
                i.DomainName,
                i.IssuerId,
                i.HasOrdered,
                i.DateCreated,
                i.ModifiedDate,
                Processor = i.Processors.FirstOrDefault() != null ? i.Processors.FirstOrDefault().Title : string.Empty,
                ProcessorId = i.Processors.FirstOrDefault() != null ? i.Processors.FirstOrDefault().ProcessorId : string.Empty,
                i.Title,
                i.RegionId
            })
            .AsQueryable()
            .GetPagedList(request.Start, request.Length, request.GetSortedColumns(), out int totalCount);

            return JsonNet(new DataTablesResponse(request.Draw, pagedList, totalCount, totalCount));
        }
    }
}