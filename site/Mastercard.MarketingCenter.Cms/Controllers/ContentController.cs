﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using Mastercard.MarketingCenter.Cms.Core.DataTables;
using Mastercard.MarketingCenter.Cms.Core.Extensions;
using Mastercard.MarketingCenter.Cms.Models;
using Mastercard.MarketingCenter.Cms.Models.Content;
using Mastercard.MarketingCenter.Cms.Services.Core;
using Mastercard.MarketingCenter.Cms.Services.Infrastructure;
using Mastercard.MarketingCenter.Cms.Services.SerializationWrappers;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Extensions;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Exceptions;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using Slam.Cms.Common;
using Slam.Cms.Common.Interfaces;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    public class ContentController : CmsBaseController
    {
        private readonly ContentTypeDao _contentTypeDao;
        private readonly ContentService _contentService;
        private readonly ICloningService _cloningService;
        private readonly ContentItemDao _contentItemDao;
        private readonly ISettingsService _settingsService;
        private readonly IContentItemService _contentItemService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPreviewMode _previewMode;
        private readonly TagServices _tagServices;
        private readonly AttachmentServices _attachmentServices;
        private readonly FeatureOnServices _featureOnServices;
        private readonly LookupServices _lookupServices;
        private readonly SecondaryImageServices _secondaryImageServices;
        private readonly IFileUploadService _fileUploadService;

        public ContentController(
            ContentTypeDao contentTypeDao,
            ContentService contentService,
            ContentItemDao contentItemDao,
            ICloningService cloningService,
            IContentItemService contentItemService,
            IUnitOfWork unitOfWork,
            ISettingsService settingsService,
            IPreviewMode previewMode,
            TagServices tagServices,
            AttachmentServices attachmentServices,
            SecondaryImageServices secondaryImageServices,
            FeatureOnServices featureOnServices,
            LookupServices lookupServices,
            IFileUploadService fileUploadService
            )
        {
            _contentTypeDao = contentTypeDao;
            _contentService = contentService;
            _contentItemDao = contentItemDao;
            _cloningService = cloningService;
            _settingsService = settingsService;
            _contentItemService = contentItemService;
            _unitOfWork = unitOfWork;
            _previewMode = previewMode;
            _tagServices = tagServices;
            _attachmentServices = attachmentServices;
            _secondaryImageServices = secondaryImageServices;
            _featureOnServices = featureOnServices;
            _lookupServices = lookupServices;
            _fileUploadService = fileUploadService;
        }

        [CustomAuthorize]
        public ActionResult List(string id, ContentActions? contentAction = null, string refreshAction = "")
        {
            var contentType = _contentTypeDao.GetById(id);

            SetPageTitle(contentType.Title + " List");

            if (!contentType.ContentTypeFieldDefinitions.FilterByPermission().Any())
            {
                ViewBag.Message = $"No Field Definitions found in the database for the Content Type: {contentType.Title}";

                return View("UnderConstruction");
            }

            var contentTypeFieldDefinitions = contentType.ContentTypeFieldDefinitions.FilterByPermission().FilterBySetting(UserContext.SelectedRegion).OrderBy(o => o.Order);
            var idColumn = contentTypeFieldDefinitions.FirstOrDefault(o => o.FieldDefinition.FieldType.IsIdentifierField());

            var listModel = new ListModel
            {
                Title = contentType.Title,
                TableName = contentType.TableName,
                Columns = contentTypeFieldDefinitions.Where(o => o.VisibleInList)
                        .Select(
                            o =>
                                new ColumnModel
                                {
                                    Description = o.FieldDefinition.Description,
                                    Name = o.FieldDefinition.Name,
                                    OrderDirection = o.DefaultOrder,
                                    Order = o.Order,
                                    FieldType = o.FieldDefinition.FieldTypeCode
                                })
                        .OrderBy(c => c.FieldTypeOrder)
                        .ThenBy(c => c.Order)
                        .ThenBy(c => c.Name),
                IdColumnName = idColumn?.FieldDefinition.Name,
                Id = id,
                CanPreview = contentType.CanPreview && CheckCanPreview(id),
                CanCopy = contentType.CanCopy && CheckCanCopy(id),
                CanArchive = CheckCanArchive(id),
                CanDelete = CheckCanDelete(id),
                CanDeleteOnlyDraft = CheckCanDeleteOnlyDraft(id),
                CanEdit = CheckCanEdit(id),
                CanUpdate = CheckCanUpdate(id),
                CanPublish = CheckCanPublish(id),
                CanCreate = CheckCanCreate(id),
                CanRestore = CheckCanRestore(id),
                CanHistory = CheckCanHistory(id),
                CanExpire = contentType.CanExpire,
                CanBePending = contentType.CanBePending,
                HasCrossBorderRegions = contentType.HasCrossBorderRegions
            };

            if (!string.IsNullOrWhiteSpace(refreshAction))
            {
                ViewBag.refreshContentTypeId = id;
                ViewBag.refreshAction = refreshAction;
                ViewBag.contentAction = contentAction?.ToString();
            }

            return View(listModel);
        }

        private bool DoesUrlAlreadyExists(ContentModel contentItem)
        {
            if (contentItem.ContentTypeId.Equals(MarketingCenterDbConstants.ContentTypeIds.Page) &&
                _contentService.UrlAlreadyExists(UserContext.SelectedRegion, contentItem.Language, contentItem.Uri, contentItem.ContentItemData.GetContentItemId()))
            {
                return true;
            }

            return false;
        }

        [HttpPost]
        public ActionResult Restore(string id)
        {
            var contentItem = GetContentModel(id);
            if (DoesUrlAlreadyExists(contentItem))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Conflict, Resources.Errors.UriError);
            }

            if (!CheckCanRestore(contentItem.ContentTypeId))
            {
                throw new AuthorizationException("Unauthorized");
            }

            var ci = _contentItemService.GetContentItem(id);
            _contentItemService.UpdateStatus(ci, Status.DraftOnly, UserContext.User.UserId);
            _unitOfWork.Commit();

            return RedirectToAction("List", new { id = contentItem.ContentTypeId });
        }

        [HttpPost]
        public ActionResult Archive(string id)
        {
            var contentItem = _contentItemService.GetContentItem(id);
            if (!CheckCanArchive(contentItem.ContentTypeId))
            {
                throw new AuthorizationException("Unauthorized");
            }

            _contentItemService.UpdateStatus(contentItem, Status.Archived, UserContext.User.UserId);
            _unitOfWork.Commit();

            return RedirectToAction("List", new { id = contentItem.ContentTypeId, refreshAction = "archive" });
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {
            var contentItem = _contentItemService.GetContentItem(id);
            if (!CheckCanDelete(contentItem.ContentTypeId))
            {
                throw new AuthorizationException("Unauthorized");
            }

            _contentItemService.UpdateStatus(contentItem, Status.Deleted, UserContext.User.UserId);
            _unitOfWork.Commit();

            return RedirectToAction("List", new { id = contentItem.ContentTypeId, refreshAction = "delete" });
        }

        [HttpPost]
        public ActionResult DeleteOnlyDraft(string id)
        {
            var contentItem = _contentItemDao.GetById(id);

            if (!CheckCanDeleteOnlyDraft(contentItem.ContentTypeId))
            {
                throw new AuthorizationException("Unauthorized");
            }

            var ci = _contentItemService.GetContentItem(id);
            _contentItemService.DeleteOnlyDraft(ci, UserContext.User.UserId);
            _unitOfWork.Commit();

            return RedirectToAction("List", new { id = contentItem.ContentTypeId });
        }

        [CustomAuthorize]
        public ActionResult GetTrackingInfo(string id)
        {
            string liveId;
            string draftId = string.Empty;
            bool isDraft = id.IsDraftContentItemId();

            if (isDraft)
            {
                liveId = id.GetAsLiveContentItemId();
                draftId = id;
            }
            else
            {
                liveId = id;
            }

            var liveContentItem = _contentItemDao.GetById(liveId);
            var draftContentItem = liveContentItem;

            if (!CheckCanGetTrackingInfo(liveContentItem.ContentTypeId))
            {
                throw new AuthorizationException("Unauthorized");
            }

            if (isDraft)
            {
                draftContentItem = _contentItemDao.GetById(draftId);
            }

            var trackingInfo = JsonNet(
                    new
                    {
                        ModifiedByUsername = liveContentItem.ModifiedByUser.UserName,
                        ModifiedByFullname = liveContentItem.ModifiedByUser.FullName,
                        CreatedByUsername = liveContentItem.CreatedByUser.UserName,
                        CreatedByFullName = liveContentItem.CreatedByUser.FullName,
                        CreatedDate = liveContentItem.CreatedDate.ToString(CultureInfo.InvariantCulture),
                        ModifiedDate = liveContentItem.ModifiedDate.ToString(CultureInfo.InvariantCulture),
                        IsDraft = isDraft,
                        DraftModifiedByUsername = draftContentItem.ModifiedByUser.UserName,
                        DraftModifiedByFullname = draftContentItem.ModifiedByUser.FullName,
                        DraftCreatedByUsername = draftContentItem.CreatedByUser.UserName,
                        DraftCreatedByFullName = draftContentItem.CreatedByUser.FullName,
                        DraftCreatedDate = draftContentItem.CreatedDate.ToString(CultureInfo.InvariantCulture),
                        DraftModifiedDate = draftContentItem.ModifiedDate.ToString(CultureInfo.InvariantCulture)
                    });

            return trackingInfo;
        }

        [CustomAuthorize]
        public ActionResult GetContent(GetContentItemsDataTablesRequest request)
        {
            if (!CheckCanGetContent(request.ContentTypeId))
            {
                throw new AuthorizationException("Unauthorized");
            }

            var contentType = _contentTypeDao.GetById(request.ContentTypeId);
            var statusId = request.ShowArchived ? Status.Archived : (int?)null;

            var result = _contentService.GetContentFor(contentType, UserContext.SelectedRegion?.Trim(), request.Start, request.Length, request.Search.Value, request.GetSortedColumns(), statusId, (contentType.CanExpire && !request.ShowArchived ? (bool?)request.isExpired : null), request.ShowPendings);

            foreach (var row in result.Data)
            {
                var fields = row as IDictionary<string, object>;
                if (fields != null)
                {
                    var keys = fields.Keys.ToList();
                    foreach (var key in keys)
                    {
                        if (!string.IsNullOrEmpty(key) && fields[key] != null && (fields[key]).GetType().Name == "String")
                        {
                            fields[key] = (contentType.ContentTypeFieldDefinitions.Any(ctf => ctf.FieldDefinition.Name.Equals(key, StringComparison.OrdinalIgnoreCase)
                                                                                              && ctf.FieldDefinition.FieldTypeCode.Equals("HtmlText", StringComparison.OrdinalIgnoreCase))) ?
                                ((string)fields[key]).SanitizeHtmlString() :
                                ((string)fields[key]).StripHtml();
                        }
                    }
                }
            }

            return JsonNet(new DataTablesResponse(request.Draw, result.Data, result.Total, result.Total));
        }

        public ActionResult GetContentVersionForHistory(ContentModel model)
        {
            if (!CheckCanEdit(model.ContentTypeId))
            {
                throw new AuthorizationException("Unauthorized");
            }

            return new JsonResult
            {
                Data = _contentService.GetContentVersionForHistory(model.IdFieldValue.ToString(), model.ContentTypeId, model.ContentItemData),
                MaxJsonLength = int.MaxValue
            };
        }

        public ActionResult History(ContentModel model)
        {
            if (!CheckCanHistory(model.ContentTypeId))
            {
                throw new AuthorizationException("Unauthorized");
            }

            var result = new ContentItemVersionModel();
            var serializer = new JsonSerializationWrapper();

            var live = _contentService.GetContentItemData(model.IdFieldValue.ToString());
            var version = _contentService.GetLastContentHistoryFor(model.IdFieldValue.ToString()).FirstOrDefault();

            var fieldsHiddenInHistory = model.ContentTypeFieldDefinitions.Where(x => x.HiddenInHistory).Select(x => x.FieldDefinition.Name);

            if (live != null)
            {
                result.Add(new ContentItemObjectVersion()
                {
                    DateSaved = version?.DateSaved ?? DateTime.Now,
                    SerializedObject = new Dictionary<string, object>(RetrieveContentItemData(live, model)),
                    FieldDefinitions = model.ContentTypeFieldDefinitions,
                    LastModificationByUserId = model.ModifiedByUserId,
                    IsLiveContent = true,
                    LastModifyDate = model.LastModifyDate,
                    UserEmail = model.ModifiedByUserEmail
                });
            }

            if (version != null)
            {
                result.Add(new ContentItemObjectVersion()
                {
                    DateSaved = version.DateSaved,
                    SerializedObject = serializer.Deserialize<Dictionary<string, object>>(version.SerializationData),
                    FieldDefinitions = model.ContentTypeFieldDefinitions,
                    LastModificationByUserId = version.LastModificationByUserId,
                    LastModifyDate = version.LastModifyDate,
                    UserEmail = version.ModifiedByUser?.Email
                });
            }

            foreach (var singleResult in result)
            {
                var fields = singleResult.SerializedObject as IDictionary<string, object>;
                foreach (var key in fields.Keys.ToArray())
                {
                    if (fieldsHiddenInHistory.Contains(key))
                    {
                        fields.Remove(key);
                    }
                    else
                    {
                        var _type = (fields[key])?.GetType()?.Name;

                        if (_type == "String" && !string.IsNullOrEmpty(key))
                        {
                            fields[key] = ((string)fields[key]).SanitizeHtmlString();
                        }

                        if (_type?.Contains("List") ?? false)
                        {
                            fields[key] = (fields[key] != null && fields[key].GetType().GetGenericArguments().Single().Name == "String") ?
                                          string.Join(" , ", ((List<string>)fields[key])) :
                                          null;
                        }
                    }
                }
            }

            return View(result);
        }

        private IDictionary<string, object> RetrieveContentItemData(IDictionary<string, object> liveModel, ContentModel model)
        {
            var item = _contentService.RetrieveDataValues(liveModel, model.ContentTypeFieldDefinitions.Select(ctf => ctf.FieldDefinition));
            return _contentService.GetContentVersionForHistory(item.GetContentItemId(), model.ContentTypeId, item);
        }

        public ActionResult Edit(string id, bool showComparison = false, string cloneRegion = null)
        {
            if (!string.IsNullOrEmpty(cloneRegion) && showComparison)
            {
                UserContext.SetUserSelectedRegion(cloneRegion);
            }
            string validId = _contentService.GetValidContentItemId(id);
            if (validId != id)
            {
                var draftItemIdUrl = new StringBuilder(Url.Action("Edit", "Content"));
                draftItemIdUrl.Append($"?id=" + validId);//we cannot add id QueryString parameter in Url.Action because id exists as route value
                if (showComparison)
                {
                    draftItemIdUrl.Append($"&showComparison=true");
                }
                if (!string.IsNullOrEmpty(cloneRegion))
                {
                    draftItemIdUrl.Append($"&cloneRegion={cloneRegion}");
                }

                return Redirect(draftItemIdUrl.ToString());
            }

            var model = GetContentModel(validId);
            if (model == null)
            {
                LogManager.EventFactory.Create()
                                       .Text($"No ContentItem Found with ID: {validId}")
                                       .Level(LoggingEventLevel.Information)
                                       .Push();

                throw new NotFoundException();
            }

            if (!CheckCanEdit(model.ContentTypeId))
            {
                throw new AuthorizationException("Unauthorized");
            }

            if (showComparison)
            {
                // adding data for comparison
                FillSourceData(model);
            }

            AddFieldsEditedByBusinessOwner(model);

            SetPageTitle($"Edit {model.ContentTypeTitle}");

            var fields = model.ContentItemData;
            foreach (var key in fields.Keys.ToList())
            {
                if (!string.IsNullOrEmpty(key) && fields[key] != null && (fields[key]).GetType().Name == "String" &&
                    model.ContentTypeFieldDefinitions.Any(ctf => ctf.FieldDefinition.Name.Equals(key, StringComparison.OrdinalIgnoreCase) &&
                    !ctf.FieldDefinition.FieldTypeCode.Equals("HtmlText", StringComparison.OrdinalIgnoreCase)))
                {
                    fields[key] = ((string)fields[key]).StripHtml();
                }
            }

            return View(model);
        }

        private bool DraftEditedByBusinessOwner(ContentModel model)
        {
            var contentItemData = model.OriginalContentItemData ?? model.ContentItemData;
            return model.IdFieldValue != null && contentItemData != null &&
                model.ReviewStateId != MarketingCenterDbConstants.ExpirationReviewStates.NotAvailable &&
                (contentItemData[MarketingCenterDbConstants.FieldNames.BusinessOwnerId]?.Equals(model.ModifiedByUserId) ?? false);
        }

        private void AddFieldsEditedByBusinessOwner(ContentModel model)
        {
            var fieldDefs = model.ContentTypeFieldDefinitions.Where(d => d.BusinessOwnerOrder != null).Select(d => d.FieldDefinition);
            List<string> fieldNames = new List<string>();
            if (DraftEditedByBusinessOwner(model))
            {
                IDictionary<string, object> previousContentItemData = new Dictionary<string, object>();
                if (model.StatusId == Status.PublishedAndDraft)
                {
                    previousContentItemData = _contentService.GetContentItemData(model.IdFieldValue.ToString().GetAsLiveContentItemId());
                    var previousContentItem = _contentItemService.GetContentItem(model.IdFieldValue.ToString().GetAsLiveContentItemId());
                    previousContentItemData.Add(MarketingCenterDbConstants.FieldNames.ExpirationDate, previousContentItem.ExpirationDate);

                    _tagServices.PopulateTagsValues(previousContentItemData, fieldDefs, model.IdFieldValue.ToString().GetAsLiveContentItemId());
                    _attachmentServices.PopulateAttachmentValues(previousContentItemData, fieldDefs, model.IdFieldValue.ToString().GetAsLiveContentItemId());
                    _secondaryImageServices.PopulateSecondaryImagesValues(previousContentItemData, fieldDefs, model.IdFieldValue.ToString().GetAsLiveContentItemId());
                }
                else
                {
                    var lastVersion = _contentService.GetLastContentHistoryFor(model.IdFieldValue.ToString().GetAsLiveContentItemId()).FirstOrDefault();
                    if (lastVersion != null)
                    {
                        var serializer = new JsonSerializationWrapper();
                        previousContentItemData = serializer.Deserialize<Dictionary<string, object>>(lastVersion.SerializationData);
                    }
                }

                if (previousContentItemData != null && previousContentItemData.Keys.Count > 0)
                {
                    var currentContentItemData = model.OriginalContentItemData ?? model.ContentItemData.ToDictionary(k => k.Key, k => k.Value);
                    if (currentContentItemData != null)
                    {
                        var tagFieldDefinitions = model.ContentTypeFieldDefinitions.Where(d => d.BusinessOwnerOrder != null && d.FieldDefinition.CustomSettings != null).Select(d => d.FieldDefinition);

                        _tagServices.PopulateTagsValues(currentContentItemData, tagFieldDefinitions, currentContentItemData.GetContentItemId());
                        _attachmentServices.PopulateAttachmentValues(currentContentItemData, tagFieldDefinitions, currentContentItemData.GetContentItemId());
                        _secondaryImageServices.PopulateSecondaryImagesValues(currentContentItemData, tagFieldDefinitions, currentContentItemData.GetContentItemId());
                        currentContentItemData = _contentService.RetrieveDataValues(currentContentItemData, fieldDefs);
                    }
                    foreach (KeyValuePair<string, object> item in previousContentItemData.Where(v => model.ContentTypeFieldDefinitions.Any(d => d.BusinessOwnerOrder != null && d.FieldDefinition.Name == v.Key) || v.Key == MarketingCenterDbConstants.FieldNames.ExpirationDate))
                    {
                        if (currentContentItemData.ContainsKey(item.Key) && item.Key != MarketingCenterDbConstants.FieldNames.ExpirationDate && Convert.ToString(currentContentItemData[item.Key]).Replace("[]", "").Replace("[\"", "").Replace("\"]", "") != Convert.ToString(item.Value).Replace("[]", "") ||
                            (item.Key == MarketingCenterDbConstants.FieldNames.ExpirationDate && model.ExpirationDate.HasValue && model.ExpirationDate.Value != Convert.ToDateTime(item.Value))
                            )
                        {
                            fieldNames.Add(item.Key);
                        }
                    }
                }
            }

            model.FieldsEditedByBusinessOwner = fieldNames;
        }

        private ContentModel GetContentModel(string id, string regionId = null, bool isClone = false)
        {
            var contentItem = _contentItemDao.GetById(id);
            if (contentItem == null || (regionId == null && contentItem.RegionId?.Trim().ToLower() != UserContext.SelectedRegion?.Trim().ToLower() &&
                (!contentItem.ContentType.HasCrossBorderRegions || !_contentItemService.IsApplicableCrossBorderRegion(contentItem.ContentItemId, UserContext.SelectedRegion))))
            {
                return null;
            }

            var contentItemData = _contentService.GetContentItemData(id);

            var idField = contentItem.ContentType.GetIdField();
            var idFieldValue = contentItemData[idField.Name];

            var canViewComments = contentItem.ContentType.CanComment && CheckCanComment(contentItem.ContentTypeId);
            var ContentTypeFieldDefinitionsFiltered = _cloningService.FilterOutFieldsForClones(contentItemData, contentItem.ContentType.ContentTypeFieldDefinitions.FilterByPermission().FilterBySetting(UserContext.SelectedRegion), isClone);
            var model = new ContentModel
            {
                ContentItemData = contentItemData,
                IdFieldValue = idFieldValue,
                ContentTypeTitle = contentItem.ContentType.Title,
                ContentTypeId = contentItem.ContentTypeId,
                ContentTypeFieldDefinitions = ContentTypeFieldDefinitionsFiltered,
                StatusId = contentItem.StatusID,
                CanPreview = contentItem.ContentType.CanPreview && CheckCanPreview(contentItem.ContentTypeId),
                CanCopy = contentItem.ContentType.CanCopy && CheckCanCopy(contentItem.ContentTypeId),
                CanArchive = CheckCanArchive(contentItem.ContentTypeId),
                CanDelete = CheckCanDelete(contentItem.ContentTypeId),
                CanDeleteOnlyDraft = CheckCanDeleteOnlyDraft(contentItem.ContentTypeId),
                CanEdit = CheckCanEdit(contentItem.ContentTypeId),
                CanUpdate = CheckCanUpdate(contentItem.ContentTypeId),
                CanPublish = CheckCanPublish(contentItem.ContentTypeId),
                CanCreate = CheckCanCreate(contentItem.ContentTypeId),
                CanRestore = CheckCanRestore(contentItem.ContentTypeId),
                CanHistory = CheckCanHistory(contentItem.ContentTypeId),
                CanComment = canViewComments && _settingsService.GetBusinessOwnerExpirationManagementEnabled(UserContext.SelectedRegion),
                CanViewComments = canViewComments,
                UserRegion = UserContext.SelectedRegion?.Trim(),
                CanExpire = contentItem.ContentType.CanExpire,
                MustExpire = contentItem.ContentType.MustExpire,
                ExpirationDate = _contentItemDao.GetById(contentItemData.GetContentItemId()).ExpirationDate,
                CurrentUserId = UserContext.User.UserId,
                ModifiedByUserName = contentItem.ModifiedByUser.FullName,
                ModifiedByUserId = contentItem.ModifiedByUserId,
                ModifiedByUserEmail = contentItem.ModifiedByUser.Email,
                LastModifyDate = contentItem.ModifiedDate,
                ReviewStateId = contentItem.ReviewStateId,
                IsCrossBorderRegion = contentItem.ContentType.HasCrossBorderRegions && contentItem.RegionId?.Trim().ToLower() != UserContext.SelectedRegion?.Trim().ToLower()
            };
            return model;
        }

        private void FillSourceData(ContentModel model)
        {
            var originalId = _contentItemService.GetOriginalContentItemId(model.ContentItemData);

            if (!string.IsNullOrEmpty(originalId))
            {
                model.OriginalContentItemDataForComparison = _contentService.GetContentItemData(originalId);
                model.OriginalExpirationDate = _contentItemDao.GetById(originalId).ExpirationDate;
            }
        }

        public ActionResult Clone(string id, string sourceRegion = null)
        {
            var model = GetCopyModel(id, "Clone - {0}", sourceRegion, true, true, isClone: true);
            _tagServices.PopulateTagsValues(model.ContentItemData, model.ContentTypeFieldDefinitions.Select(d => d.FieldDefinition), id.GetAsLiveContentItemId());
            _lookupServices.PopulateMultipleLookupValues(model.ContentItemData, model.ContentTypeFieldDefinitions.Select(d => d.FieldDefinition), id.GetAsLiveContentItemId());
            _featureOnServices.PopulateFeatureOnValues(model.ContentItemData, model.ContentTypeFieldDefinitions.Select(d => d.FieldDefinition), id.GetAsLiveContentItemId());

            if (model == null)
            {
                LogManager.EventFactory.Create()
                  .Text($"No published contentitem to clone found with ID: {id}")
                  .Level(LoggingEventLevel.Information)
                  .Push();

                throw new NotFoundException();
            }

            var contentType = _contentTypeDao.GetById(model.ContentTypeId);
            if (!contentType.Clonable)
            {
                throw new InvalidOperationException($"Cloning for Content Type: {contentType.Title} is not allowed.");
            }

            if (!CheckCanClone(model.ContentTypeId))
            {
                throw new AuthorizationException("Unauthorized");
            }

            SetPageTitle("Clone " + model.ContentTypeTitle);

            return View("Clone", model);
        }

        public ActionResult Copy(string id)
        {
            var model = GetCopyModel(id, "Copy - {0}", null, true, false, true);
            if (model == null)
            {
                LogManager.EventFactory.Create()
                  .Text($"No published contentitem to copy found with ID: {id}")
                  .Level(LoggingEventLevel.Information)
                  .Push();

                throw new NotFoundException();
            }

            var contentType = _contentTypeDao.GetById(model.ContentTypeId);
            if (!contentType.CanCopy)
            {
                throw new InvalidOperationException($"Copying for Content Type: {contentType.Title} is not allowed.");
            }

            if (!CheckCanCopy(model.ContentTypeId))
            {
                throw new AuthorizationException("Unauthorized");
            }

            SetPageTitle("Copy " + model.ContentTypeTitle);

            return View("Copy", model);
        }

        private ContentModel GetCopyModel(
            string id,
            string titleFormat,
            string regionId = null,
            bool setOriginalItemId = false,
            bool isNewItem = false,
            bool isCopy = false,
            bool isClone = false)
        {
            var model = GetContentModel(isCopy ? id : id.GetAsLiveContentItemId(), regionId, true);
            if (!isCopy && (model == null || !(model.StatusId.Equals(Status.Published) || model.StatusId.Equals(Status.PublishedAndDraft))))
            {
                return null;
            }

            var filteredByPermisionFields = model.ContentTypeFieldDefinitions.FilterByPermission().FilterBySetting(UserContext.SelectedRegion);

            var firstTextField = filteredByPermisionFields
                .OrderBy(o => o.Order)
                .FirstOrDefault(o => o.FieldDefinition.FieldTypeCode == FieldType.SingleLineText);

            if (firstTextField == null)
            {
                return model;
            }

            if (setOriginalItemId)
            {
                FillOriginalItemId(model, id);
                model.ContentTypeFieldDefinitions = model.ContentTypeFieldDefinitions.Where(fd => !(fd.FieldDefinition.Name.Equals(MarketingCenterDbConstants.FieldNames.ContentItemId, System.StringComparison.InvariantCultureIgnoreCase)));
            }

            var firstTextFieldData = model.ContentItemData[firstTextField.FieldDefinition.Name];
            model.ContentItemData[firstTextField.FieldDefinition.Name] = string.Format(titleFormat, firstTextFieldData);

            var isInterregionCloning = model.UserRegion != (regionId ?? UserContext.SelectedRegion);
            if (isInterregionCloning)
            {
                filteredByPermisionFields = _cloningService.GetOnlyInterRegionCloning(filteredByPermisionFields);
                model.IsInterRegionCloning = isInterregionCloning;
            }

            var keysToDelete = model.ContentItemData.Select(cit => cit.Key)
                .Where(key => !filteredByPermisionFields.Any(fieldDef => fieldDef.FieldDefinition.Name.Equals(key)));

            keysToDelete.ForEach(key => model.ContentItemData.Remove(key));

            model.ContentItemData = _contentService.GetDefaultValueData(model.ContentItemData, model.ContentTypeFieldDefinitions.Select(o => o.FieldDefinition) as FieldDefinition[] ?? model.ContentTypeFieldDefinitions.Select(o => o.FieldDefinition).ToArray(), UserContext.SelectedRegion, isNewItem, isClone: isClone);

            return model;
        }

        private void FillOriginalItemId(ContentModel model, string id)
        {
            if (model.ContentItemData.ContainsKey(MarketingCenterDbConstants.FieldNames.OriginalContentItemId))
            {
                model.ContentItemData[MarketingCenterDbConstants.FieldNames.OriginalContentItemId] = id.GetAsLiveContentItemId();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Copy(ContentModel model)
        {
            var contentType = _contentTypeDao.GetById(model.ContentTypeId);
            if (!contentType.CanCopy)
            {
                throw new InvalidOperationException($"Copy for Content Type: {contentType.Title} is not allowed.");
            }

            if (!CheckCanCopy(model.ContentTypeId))
            {
                throw new AuthorizationException("Unauthorized");
            }

            model.ContentItemData = _contentService.GetDefaultValueData(
                model.ContentItemData,
                model.ContentTypeFieldDefinitions.Select(o => o.FieldDefinition) as FieldDefinition[] ??
                    model.ContentTypeFieldDefinitions.Select(o => o.FieldDefinition).ToArray(),
                UserContext.SelectedRegion);

            ValidateModel(model);

            if (ModelState.IsValid)
            {
                _cloningService.Clone(model.ContentTypeId, model.ContentItemData, UserContext.User.UserId, model.UserRegion.Trim());
                _contentService.UpdateContentExpirationDate(model.ExpirationDate, model.ContentItemData.GetContentItemId());
                _fileUploadService.SaveUploadActivityByIds(model.UploadActivityIds);
                _unitOfWork.Commit();

                return RedirectToAction("List", "Content", new { id = model.ContentTypeId });
            }

            FillBaseProperties(contentType, model);
            return View("Copy", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Clone(ContentModel model)
        {
            var contentType = _contentTypeDao.GetById(model.ContentTypeId);
            if (!contentType.Clonable)
            {
                throw new InvalidOperationException($"Cloning for Content Type: {contentType.Title} is not allowed.");
            }

            if (!CheckCanClone(model.ContentTypeId))
            {
                throw new AuthorizationException("Unauthorized");
            }

            model.ContentItemData = _contentService.GetDefaultValueData(model.ContentItemData, model.ContentTypeFieldDefinitions.Select(o => o.FieldDefinition) as FieldDefinition[] ?? model.ContentTypeFieldDefinitions.Select(o => o.FieldDefinition).ToArray(), UserContext.SelectedRegion);

            ValidateModel(model);

            if (ModelState.IsValid)
            {
                _cloningService.Clone(model.ContentTypeId, model.ContentItemData, UserContext.User.UserId, model.UserRegion.Trim());
                _contentService.UpdateContentExpirationDate(model.ExpirationDate, model.ContentItemData.GetContentItemId());
                _fileUploadService.SaveUploadActivityByIds(model.UploadActivityIds);
                _unitOfWork.Commit();

                return RedirectToAction("List", "Content", new { id = model.ContentTypeId });
            }

            FillBaseProperties(contentType, model);

            return View("Clone", model);
        }

        private void FillBaseProperties(ContentType contentType, ContentModel model)
        {
            model.CanPreview = contentType.CanPreview;
            model.CanEdit = CheckCanEdit(model.ContentTypeId);
            model.CanUpdate = CheckCanUpdate(model.ContentTypeId);
            model.CanPublish = CheckCanPublish(model.ContentTypeId);
            model.CanExpire = contentType.CanExpire;
            model.MustExpire = contentType.MustExpire;
            model.ContentTypeFieldDefinitions = contentType.ContentTypeFieldDefinitions.FilterByPermission().FilterBySetting(UserContext.SelectedRegion);

            var fields = model.ContentItemData;
            foreach (var key in fields.Keys.ToList())
            {
                var fieldDefinitions = model.ContentTypeFieldDefinitions as ContentTypeFieldDefinition[] ?? model.ContentTypeFieldDefinitions.ToArray();
                var typeFieldDefinitions = model.ContentTypeFieldDefinitions as ContentTypeFieldDefinition[] ?? fieldDefinitions.ToArray();
                var contentTypeFieldDefinitions = model.ContentTypeFieldDefinitions as ContentTypeFieldDefinition[] ?? typeFieldDefinitions.ToArray();
                var modelContentTypeFieldDefinitions = model.ContentTypeFieldDefinitions as ContentTypeFieldDefinition[] ?? contentTypeFieldDefinitions.ToArray();
                if (!string.IsNullOrEmpty(key) && fields[key] != null && (fields[key]).GetType().Name == "String" &&
                    modelContentTypeFieldDefinitions.Any(ctf => ctf.FieldDefinition.Name.Equals(key, StringComparison.OrdinalIgnoreCase) &&
                    !ctf.FieldDefinition.FieldTypeCode.Equals("HtmlText", StringComparison.OrdinalIgnoreCase)))
                {
                    fields[key] = ((string)fields[key]).StripHtml();
                }
            }
        }

        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Update(ContentModel model, ContentActions action)
        {
            if (!CheckCanUpdate(model.ContentTypeId, action.ToString()))
            {
                throw new AuthorizationException("Unauthorized");
            }

            if (model.ContentTypeFieldDefinitions.FilterByPermission().Any(c => c.Permission == Constants.Permissions.HasAlternativeExpiration) &&
                User.IsInPermission(Constants.Permissions.HasAlternativeExpiration))
            {
                var fieldDefinitions = model.ContentTypeFieldDefinitions.FilterByPermission().FilterBySetting(UserContext.SelectedRegion).Where(c => c.FieldDefinition?.CustomSettings != null).Select(f => f.FieldDefinition);
                var alternativeExpirationField = fieldDefinitions.FirstOrDefault(fd => fd.CustomSettings.Contains("isAlternativeExpiration"));

                if (model.ExpirationDate == null && model.ContentItemData.ContainsKey(alternativeExpirationField?.Name ?? ""))
                {
                    var previousAlternativeExpirationDate = (DateTime?)_contentService.GetContentItemData(model.ContentItemData.GetContentItemId())[alternativeExpirationField?.Name];
                    var alternativeExpirationDate = (DateTime?)model.ContentItemData[alternativeExpirationField?.Name];

                    model.ExpirationDate = alternativeExpirationDate != null &&
                                           (previousAlternativeExpirationDate == null || !previousAlternativeExpirationDate.Equals(alternativeExpirationDate)) ?
                                           alternativeExpirationDate :
                                           null;
                }
            }

            ValidateModel(model);

            if (ModelState.IsValid)
            {
                _contentService.UpdateContent(model.ContentItemData, action, UserContext.User.UserId, UserContext.SelectedRegion?.Trim(), false, 0, model.OriginalContentItemData);
                var saveIncludesPublish = action == ContentActions.SaveLive;
                _contentService.UpdateContentExpirationDate(model.ExpirationDate, (action == ContentActions.SaveLive ? model.ContentItemData.GetContentItemId().GetAsLiveContentItemId() : model.ContentItemData.GetContentItemId()), saveIncludesPublish, saveIncludesPublish); /* The last Parameter is repeated because if published the review state must be also updated*/
                _fileUploadService.SaveUploadActivityByIds(model.UploadActivityIds);

                if (model.ContentTypeId.ToLower() == MarketingCenterDbConstants.ContentTypeIds.Webinar)
                {
                    if (model.ContentItemData.ContainsKey("VideoFile") && model.ContentItemData.GetContentItemId() != null)
                    {
                        string contentItemIdFinal = model.ContentItemData.GetContentItemId();
                        if (action == ContentActions.SaveLive)
                        {
                            contentItemIdFinal.GetAsLiveContentItemId();
                        }
                    }
                }
                _unitOfWork.Commit();

                return Redirect(model, action);
            }

            var contentItem = _contentItemDao.GetById(model.ContentItemData.GetContentItemId());
            model.CanPreview = contentItem.ContentType.CanPreview && CheckCanPreview(model.ContentTypeId);
            model.CanEdit = CheckCanEdit(contentItem.ContentTypeId);
            model.CanUpdate = CheckCanUpdate(contentItem.ContentTypeId);
            model.CanPublish = CheckCanPublish(contentItem.ContentTypeId);
            model.CanExpire = contentItem.ContentType.CanExpire;
            model.MustExpire = contentItem.ContentType.MustExpire;
            model.ContentTypeFieldDefinitions = contentItem.ContentType.ContentTypeFieldDefinitions.FilterByPermission().FilterBySetting(UserContext.SelectedRegion);
            model.IsCrossBorderRegion = contentItem.ContentType.HasCrossBorderRegions && contentItem.RegionId?.Trim().ToLower() != UserContext.SelectedRegion?.Trim().ToLower();

            var fields = model.ContentItemData;
            foreach (var key in fields.Keys.ToList())
            {
                var modelContentTypeFieldDefinitions = model.ContentTypeFieldDefinitions as ContentTypeFieldDefinition[] ?? model.ContentTypeFieldDefinitions.ToArray();
                if (!string.IsNullOrEmpty(key) && fields[key] != null && (fields[key]).GetType().Name == "String" &&
                    modelContentTypeFieldDefinitions.Any(ctf => ctf.FieldDefinition.Name.Equals(key, StringComparison.OrdinalIgnoreCase) &&
                    !ctf.FieldDefinition.FieldTypeCode.Equals("HtmlText", StringComparison.OrdinalIgnoreCase)))
                {
                    fields[key] = ((string)fields[key]).StripHtml();
                }
            }
            AddFieldsEditedByBusinessOwner(model);

            return View("Edit", model);
        }

        private ActionResult Redirect(ContentModel model, ContentActions action)
        {
            if (action == ContentActions.SaveAndPreview)
            {
                var updatedContentItem = _contentItemDao.GetById(model.ContentItemData.GetContentItemId());
                new Slam.Cms.Common.Security.AuthenticationService(HttpContext, _previewMode).CreateOrUpdateSlamPreviewModeCookie();
                return Redirect($"/portal/{ updatedContentItem.FrontEndUrl.TrimStart('/')}");
            }

            return RedirectToAction("List", "Content", new
            {
                id = model.ContentTypeId,
                contentAction = action,
                refreshAction = model.IdFieldValue == null ? "new" : "update"
            });
        }

        [CustomAuthorize]
        public ActionResult Create(string id, string title = null)
        {
            var contentType = _contentTypeDao.GetById(id);
            if (contentType == null)
            {
                LogManager.EventFactory.Create()
                                       .Text($"Cannot Found a ContentType with ID: {id}")
                                       .Level(LoggingEventLevel.Information)
                                       .Push();

                throw new InvalidOperationException($"Cannot Found a ContentType with ID: {id}");
            }

            var model = new ContentModel
            {
                ContentItemData = _contentService.GetDefaultContentItem(contentType, title, Request.UrlReferrer, true),
                ContentTypeTitle = contentType.Title,
                ContentTypeId = contentType.ContentTypeId,
                ContentTypeFieldDefinitions = contentType.ContentTypeFieldDefinitions.FilterByPermission().FilterBySetting(UserContext.SelectedRegion).Where(o => !o.HiddenInNew),
                CanPreview = contentType.CanPreview && CheckCanPreview(id),
                CanCopy = contentType.CanCopy && CheckCanCopy(id),
                CanArchive = CheckCanArchive(id),
                CanDelete = CheckCanDelete(id),
                CanDeleteOnlyDraft = CheckCanDeleteOnlyDraft(id),
                CanEdit = CheckCanEdit(id),
                CanUpdate = CheckCanUpdate(id),
                CanPublish = CheckCanPublish(id),
                CanCreate = CheckCanCreate(id),
                CanRestore = CheckCanRestore(id),
                CanHistory = CheckCanHistory(id),
                IsNewForm = true,
                UserRegion = UserContext.SelectedRegion?.Trim(),
                CanExpire = contentType.CanExpire,
                MustExpire = contentType.MustExpire,
                CurrentUserId = UserContext.User.UserId
            };

            AddFieldsEditedByBusinessOwner(model);
            SetPageTitle($"Create {model.ContentTypeTitle}");

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ContentModel model, ContentActions action)
        {
            if (model.ContentTypeFieldDefinitions.FilterByPermission().Any(c => c.Permission == Constants.Permissions.HasAlternativeExpiration) &&
                User.IsInPermission(Constants.Permissions.HasAlternativeExpiration))
            {
                var fieldDefinitions = model.ContentTypeFieldDefinitions.FilterByPermission().FilterBySetting(UserContext.SelectedRegion).Where(c => c.FieldDefinition?.CustomSettings != null)?.Select(f => f.FieldDefinition);
                var alternativeExpirationField = fieldDefinitions.FirstOrDefault(fd => fd.CustomSettings.Contains("isAlternativeExpiration"));
                if (model.ExpirationDate == null && model.ContentItemData.ContainsKey(alternativeExpirationField?.Name ?? ""))
                {
                    var alternativeExpirationDate = (DateTime?)model.ContentItemData[alternativeExpirationField?.Name];
                    model.ExpirationDate = alternativeExpirationDate != null ? alternativeExpirationDate : null;
                }
            }

            model.ContentItemData = _contentService.GetDefaultValueData(model.ContentItemData, model.ContentTypeFieldDefinitions.Select(o => o.FieldDefinition) as FieldDefinition[] ?? model.ContentTypeFieldDefinitions.Select(o => o.FieldDefinition).ToArray(), UserContext.SelectedRegion);

            ValidateModel(model);

            if (ModelState.IsValid)
            {
                _contentService.AddContent(model.ContentTypeId, model.ContentItemData, UserContext.User.UserId, UserContext.SelectedRegion?.Trim(), action);
                var saveIncludesPublish = action == ContentActions.SaveLive;
                _contentService.UpdateContentExpirationDate(model.ExpirationDate, (action == ContentActions.SaveLive ? model.ContentItemData.GetContentItemId().GetAsLiveContentItemId() : model.ContentItemData.GetContentItemId()), saveIncludesPublish, saveIncludesPublish); /* The last Parameter is repeated beacause if published the review state must be also updated*/
                _fileUploadService.SaveUploadActivityByIds(model.UploadActivityIds);

                if (model.ContentTypeId.ToLower() == MarketingCenterDbConstants.ContentTypeIds.Webinar && model.ContentItemData.ContainsKey("VideoFile") && model.ContentItemData.GetContentItemId() != null)
                {
                    string ContentItemIdFinal = model.ContentItemData.GetContentItemId().ToString();
                    if (model.StatusId == Status.Published || model.StatusId == Status.PublishedAndDraft)
                    {
                        ContentItemIdFinal = ContentItemIdFinal.GetAsLiveContentItemId();
                    }
                }

                _unitOfWork.Commit();

                return Redirect(model, action);
            }

            var contentType = _contentTypeDao.GetById(model.ContentTypeId);
            model.CanPreview = contentType.CanPreview;
            model.CanEdit = CheckCanEdit(model.ContentTypeId);
            model.CanUpdate = CheckCanUpdate(model.ContentTypeId);
            model.CanPublish = CheckCanPublish(model.ContentTypeId);
            model.CanExpire = contentType.CanExpire;
            model.MustExpire = contentType.MustExpire;
            model.ContentTypeFieldDefinitions = contentType.ContentTypeFieldDefinitions.FilterByPermission().FilterBySetting(UserContext.SelectedRegion);

            HtmlHelperExtensions.StripHtmlTextFields(model.ContentItemData, model.ContentTypeFieldDefinitions);
            AddFieldsEditedByBusinessOwner(model);

            return View("Create", model);
        }

        private void ValidateModel(ContentModel model)
        {
            var contentType = _contentTypeDao.GetById(model.ContentTypeId);
            var fields = contentType.ContentTypeFieldDefinitions.FilterByPermission().FilterBySetting(UserContext.SelectedRegion).Select(o => o.FieldDefinition);

            fields = _contentService.SetToggledFieldsToOptional(fields, model.ContentItemData);

            //Consider validating expiration date using a supplemental collection of fields passed to the
            //generic ValidateData method and using only ValidateData instead of ValidateExpirationDate
            var additionalContentItemData = new Dictionary<string, object>();
            additionalContentItemData.Add("MustExpire", contentType.MustExpire);
            additionalContentItemData.Add("ExpirationDate", model.ExpirationDate);

            var validationErrors = _contentService.ValidateData(fields, model.ContentItemData, additionalContentItemData);
            var validationExpirationDateErrors = _contentService.ValidateExpirationDate(contentType.MustExpire, model.ExpirationDate, null);

            if (!UserContext.SelectedRegion.Trim().Equals(model.UserRegion?.Trim() ?? ""))
            {
                ModelState.AddModelError("WrongRegion", "The Region Has Changed");
                throw new AuthorizationException("Unauthorized");
            }

            if (DoesUrlAlreadyExists(model))
            {
                ModelState.AddModelError("Uri", Resources.Errors.UriError);
            }

            validationErrors.ForEach(err => ModelState.AddModelError(err.Key, err.ErrorMessage));
            validationExpirationDateErrors.ToList().ForEach(err => ModelState.AddModelError(err.Key, err.ErrorMessage));
        }
    }
}