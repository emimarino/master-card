﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    public class AttachmentController : FileController
    {
        private readonly AttachmentServices _attachmentServices;

        public AttachmentController(AttachmentServices attachmentServices, IFileUploadService fileUploadService) : base(fileUploadService)
        {
            _attachmentServices = attachmentServices;
        }

        [CustomAuthorize(allowAllMasterCardUsers: true)]
        public ActionResult GetAttachments(string contentItemId)
        {
            var attachments = _attachmentServices.GetAttachments(contentItemId).Select(o => GetFileInfo("FilesFolder", o.FileUrl));

            return JsonNet(new { attachments });
        }
    }
}