﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using Mastercard.MarketingCenter.Cms.Models;
using Mastercard.MarketingCenter.Cms.Services.Core;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Slam.Cms.Data;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    [CustomAuthorize]
    public class TagController : CmsBaseController
    {
        private readonly TagCategoryDao _tagCategoryDao;
        private readonly RegionDao _regionDao;
        private readonly SlamContext _slamContext;
        private readonly TagServices _tagServices;
        private readonly IdGeneratorService _idGeneratorService;
        private readonly ListDao _listDao;
        private readonly ContentService _contentService;
        private readonly ISqlCacheRepository _sqlCacheRepository;

        public TagController(TagCategoryDao tagCategoryDao, RegionDao regionDao, SlamContext slamContext, TagServices tagServices, IdGeneratorService idGeneratorService, ListDao listDao, ContentService contentService, ISqlCacheRepository sqlCacheRepository)
        {
            _tagCategoryDao = tagCategoryDao;
            _regionDao = regionDao;
            _slamContext = slamContext;
            _tagServices = tagServices;
            _idGeneratorService = idGeneratorService;
            _listDao = listDao;
            _contentService = contentService;
            _sqlCacheRepository = sqlCacheRepository;
        }

        public ActionResult CreateCategory()
        {
            SetPageTitle("Create Tag Category");

            var tagCategoryModel = new TagCategoryModel { TagBrowser = "Yes" };
            return View(tagCategoryModel);
        }

        public ActionResult Create(string id)
        {
            SetPageTitle("Create Tag");

            var tagList = _listDao.GetById(MarketingCenterDbConstants.ListIds.TagListId);

            var model = new ListItemModel
            {
                ListId = MarketingCenterDbConstants.ListIds.TagListId,
                ListFieldDefinitions = tagList.ListFieldDefinitions.ExcludeTrackingFields().Where(o => !o.HiddenInNew),
                ListTitle = tagList.Name
            };

            ViewBag.ParentId = id;

            return View(model);
        }

        [HttpPost]
        [UnitOfWork]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ListItemModel model)
        {
            ValidateModel(model);

            var parentIdValue = Request.Form["ParentId"];

            if (ModelState.IsValid)
            {
                var tagId = _contentService.AddItemContent(model.ListId, model.ListItemData, UserContext.User.UserId);

                _tagServices.AddNewTagToHierarchy(tagId, parentIdValue);

                return RedirectToAction("TagManager", new { invalidateTagTree = "true" });
            }

            ViewBag.ParentId = parentIdValue;

            return View("Create", model);
        }

        [HttpPost]
        [UnitOfWork]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCategory(TagCategoryModel tagCategoryModel)
        {
            if (ModelState.IsValid)
            {
                var newTagCategory = new Data.Entities.TagCategory
                {
                    DisplayOrder = tagCategoryModel.DisplayOrder,
                    Featurable = tagCategoryModel.Featurable,
                    IsRelatable = tagCategoryModel.IsRelatable,
                    TagBrowser = tagCategoryModel.TagBrowser,
                    Title = tagCategoryModel.Title,
                    TagCategoryId = _idGeneratorService.GetNewUniqueId()
                };
                tagCategoryModel.GetType().GetProperties().ToList().ForEach(
                p => { if (p.GetType().Name.Equals("String", StringComparison.CurrentCulture)) p.GetValue(tagCategoryModel, null)?.ToString().DecodeHtml(); }

                );
                _tagCategoryDao.Add(newTagCategory);

                return RedirectToAction("TagManager", new { invalidateTagTree = "true" });
            }

            return View(tagCategoryModel);
        }

        [HttpPost]
        [UnitOfWork]
        public ActionResult DeleteTagCategory(string id)
        {
            _tagServices.DeleteTagCategory(id);

            return JsonNet(new { ok = true });
        }

        [HttpPost]
        [UnitOfWork]
        public ActionResult DeleteTag(string id)
        {
            _tagServices.DeleteTag(id);

            return JsonNet(new { ok = true });
        }

        [HttpPost]
        [UnitOfWork]
        public ActionResult RemoveFromHierarchy(string id)
        {
            if (id.StartsWith("p"))
            {
                var positionId = Convert.ToInt32(id.Replace("p", string.Empty));
                _tagServices.DeleteTagHierarchyPosition(positionId);

                return JsonNet(new { ok = true });
            }

            return JsonNet(new { ok = false });
        }

        public ActionResult TagManager(string invalidateTagTree = "false")
        {
            SetPageTitle("Tag Manager");

            if (invalidateTagTree == "true")
            {
                _slamContext.InvalidateAllTags();
                _regionDao.GetAll().ForEach(r => _sqlCacheRepository.SaveRegionInvalidator($"RegionTag_{r.IdTrimmed}"));
            }

            var tagTree = _slamContext.GetTagTree();

            return View(tagTree.Root);
        }

        public ActionResult UpdateCategory(string id)
        {
            var tagCategory = _tagCategoryDao.GetById(id);

            SetPageTitle("Update Tag Category");

            var model = new TagCategoryModel
            {
                DisplayOrder = tagCategory.DisplayOrder,
                Featurable = tagCategory.Featurable,
                Title = tagCategory.Title,
                IsRelatable = tagCategory.IsRelatable,
                TagBrowser = tagCategory.TagBrowser,
                TagCategoryId = tagCategory.TagCategoryId
            };

            return View(model);
        }

        [HttpPost]
        [UnitOfWork]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateCategory(TagCategoryModel tagCategoryModel)
        {
            if (ModelState.IsValid)
            {
                var tagCategory = _tagCategoryDao.GetById(tagCategoryModel.TagCategoryId);

                tagCategory.DisplayOrder = tagCategoryModel.DisplayOrder;
                tagCategory.Featurable = tagCategoryModel.Featurable;
                tagCategory.Title = tagCategoryModel.Title;
                tagCategory.TagBrowser = tagCategoryModel.TagBrowser;
                tagCategory.IsRelatable = tagCategoryModel.IsRelatable;

                return RedirectToAction("TagManager", new { invalidateTagTree = "true" });
            }

            return View(tagCategoryModel);
        }

        private void ValidateModel(ListItemModel model)
        {
            var list = _listDao.GetById(MarketingCenterDbConstants.ListIds.TagListId);
            var fields = list.ListFieldDefinitions.Select(o => o.FieldDefinition);

            var validationErros = _contentService.ValidateData(fields, model.ListItemData);

            validationErros.ForEach(err => ModelState.AddModelError(err.Key, err.ErrorMessage));
        }
    }
}