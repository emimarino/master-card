﻿using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    public class SecondaryImageController : FileController
    {
        private readonly SecondaryImageServices _secondaryImageServices;

        public SecondaryImageController(SecondaryImageServices secondaryImageServices, IFileUploadService fileUploadService) : base(fileUploadService)
        {
            _secondaryImageServices = secondaryImageServices;
        }

        public ActionResult GetSecondaryImages(string contentItemId)
        {
            var attachments = _secondaryImageServices.GetSecondaryImages(contentItemId).Select(o => GetFileInfo("ImagesFolder", o.ImageUrl));

            return JsonNet(new { attachments });
        }
    }
}