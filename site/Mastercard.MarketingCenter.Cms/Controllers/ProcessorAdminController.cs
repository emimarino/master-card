﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    [CustomAuthorize]
    public class ProcessorAdminController : CmsBaseController
    {
        // GET: /ProcessorAdmin/
        public ActionResult Index()
        {
            return View();
        }
    }
}