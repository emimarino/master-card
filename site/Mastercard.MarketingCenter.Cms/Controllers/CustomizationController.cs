﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using Mastercard.MarketingCenter.Cms.Models;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    public class CustomizationController : CmsBaseController
    {
        private readonly CustomizationServices _customizationServices;
        private readonly ContentService _contentService;
        private readonly IContentItemService _contentItemServices;

        public CustomizationController(CustomizationServices customizationServices, ContentService contentService, IContentItemService contentItemServices)
        {
            _customizationServices = customizationServices;
            _contentService = contentService;
            _contentItemServices = contentItemServices;
        }

        [ChildActionOnly]
        public ActionResult Index(string id = null)
        {
            var model = new CustomizationModel();
            var itemExists = string.IsNullOrEmpty(id) ? false : _contentItemServices.GetContentItem(id) != null;
            if (itemExists)
            {
                var contentItemData = _contentService.GetContentItemData(id);
                var description = Convert.ToString(contentItemData["ShortDescription"]);
                var title = Convert.ToString(contentItemData["Title"]);

                var customizations = _customizationServices.GetContentItemCustomizations(id).ToList();
                model.Customizations = customizations;
                model.Description = description;
                model.Title = title;
                model.OrderableAssetId = id;
            }
            else
            {
                model.Customizations = _customizationServices.GetAllCustomizations().ToList();
            }

            return PartialView(model);
        }

        [CustomAuthorize]
        public ActionResult GetSelectedCustomizationOptions(string id)
        {
            var customizations = _customizationServices.GetContentItemCustomizations(id).ToList();

            var reqCustomizations = customizations.Where(o => o.State == CustomizationState.Required);
            var optCustomizations = customizations.Where(o => o.State == CustomizationState.Optional);

            var model = new SelectedCustomizationModel
            {
                OptionalCustomizations = optCustomizations,
                RequiredCustomizations = reqCustomizations
            };

            return JsonNet(model);
        }
    }
}