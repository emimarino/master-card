﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using Mastercard.MarketingCenter.Cms.Core.DataTables;
using Mastercard.MarketingCenter.Cms.Core.Extensions;
using Mastercard.MarketingCenter.Cms.Models.Content;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    [CustomAuthorize]
    public class ReportController : CmsBaseController
    {
        protected ReportServices _reportServices;
        protected IReportService _reportService;

        public ReportController(ReportServices reportServices, IReportService reportService)
        {
            _reportServices = reportServices;
            _reportService = reportService;
        }

        public ActionResult FramedReport(string report, string title)
        {
            if (string.IsNullOrEmpty(report))
            {
                return new HttpNotFoundResult();
            }

            ViewBag.ReportName = report;
            ViewData["RenderPageHeading"] = true;
            ViewData["ReportTitle"] = title;

            if (!string.IsNullOrWhiteSpace(title))
            {
                ViewBag.PageTitle = $"{title} / ";
            }

            return View();
        }

        public ActionResult GetBusinessOwnerRequestsReport(DefaultDataTablesRequest request)
        {
            if (request.Length.Equals(0))
            {
                return new HttpNotFoundResult();
            }

            var data = _reportServices.GetHomeReports(
                request.Start,
                request.Length,
                new List<string>() { "Title", "ContentType", "ContentItemId" },
                request.GetSortedColumns(),
                UserContext.SelectedRegion.Trim(),
                null,
                null,
                MarketingCenterDbConstants.ExpirationReviewStates.NotAvailable,
                true);

            return GetTabledResponse(request.Draw, data);
        }

        public ActionResult GetClonedAssetsNewSourceUpdatesReport(DefaultDataTablesRequest request)
        {
            if (request.Length.Equals(0))
            {
                return new HttpNotFoundResult();
            }

            var data = _reportServices.GetClonedAssetsNewSourceUpdates(
                request.Start,
                request.Length,
                new List<string>() { "Title", "ContentType", "ContentItemId", "SourceAssetStatus" },
                request.GetSortedColumns(),
                UserContext.SelectedRegion.Trim());

            return GetTabledResponse(0, data);
        }

        public ActionResult GetContentAboutToExpireReport(GetExpirationContentDataTablesRequest request)
        {
            if (request.Length.Equals(0))
            {
                return new HttpNotFoundResult();
            }

            var data = _reportServices.GetHomeReports(
                request.Start,
                request.Length,
                new List<string>() { "Title", "ContentType", "ContentItemId" },
                request.GetSortedColumns(),
                UserContext.SelectedRegion.Trim(),
                DateTime.Now.Date,
                DateTime.Now.AddMonths(request.Months));

            return GetTabledResponse(request.Draw, data);
        }

        public ActionResult GetProgramPrintOrderDateExpirationReport(GetExpirationContentDataTablesRequest request)
        {
            if (request.Length.Equals(0))
            {
                return new HttpNotFoundResult();
            }

            var data = _reportService.GetProgramPrintOrderDateExpirationReport(
                                                                                UserContext.SelectedRegion.Trim(),
                                                                                DateTime.Now.Date,
                                                                                DateTime.Now.AddMonths(request.Months)
                                                                              )
                                     .AsQueryable()
                                     .GetPagedList(request.Start, request.Length, request.GetSortedColumns(), out int totalCount);

            return GetTabledResponse(request.Draw, data, totalCount);
        }

        public ActionResult GetRecentlyExpiredContentReport(GetExpirationContentDataTablesRequest request)
        {
            if (request.Length.Equals(0))
            {
                return new HttpNotFoundResult();
            }

            var data = _reportServices.GetHomeReports(
                request.Start,
                request.Length,
                new List<string>() { "Title", "ContentType", "ContentItemId" },
                request.GetSortedColumns(),
                UserContext.SelectedRegion.Trim(),
                DateTime.Now.AddMonths(-request.Months),
                DateTime.Now);

            return GetTabledResponse(request.Draw, data);
        }

        private ActionResult GetTabledResponse(int draw, Common.Infrastructure.PagedList<dynamic> data)
        {
            return data != null ? JsonNet(new DataTablesResponse(draw, data.Data, data.Total, data.Total)) as ActionResult : new EmptyResult();
        }

        private ActionResult GetTabledResponse(int draw, IEnumerable<dynamic> data, int totalCount)
        {
            return data != null ? JsonNet(new DataTablesResponse(draw, data, totalCount, totalCount)) as ActionResult : new EmptyResult();
        }
    }
}