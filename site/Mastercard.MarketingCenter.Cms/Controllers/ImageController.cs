﻿using Mastercard.MarketingCenter.Cms.Core.ActionFilters;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Common;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Controllers
{
    [CustomAuthorize(allowAllMasterCardUsers: true)]
    public class ImageController : CmsBaseController
    {
        private readonly IImageService _imageService;
        private readonly IUrlService _urlService;

        public ImageController(IImageService imageService, IUrlService urlService)
        {
            _imageService = imageService;
            _urlService = urlService;
        }

        public ActionResult Upload(HttpPostedFileBase file)
        {
            return file != null && file.ContentLength > 0 ?
                   JsonNet(new { filename = _imageService.SaveImage(file) }) :
                   JsonNet(new { message = "Not a valid file" });
        }

        [HttpPost]
        public ActionResult Delete(string filename)
        {
            return JsonNet(new { ok = true });
        }

        public ActionResult GetFileInfo(string filename)
        {
            var fileInfo = _imageService.GetFileInfo(filename.IsAbsoluteUrl() ? _imageService.GetImageFromUrl(filename) : filename);
            if (fileInfo == null)
            {
                return JsonNet(new
                {
                    name = Resources.Errors.BrokenImage,
                    size = 0,
                    url = _urlService.GetStaticBrokenImageURL(0),
                    height = 0,
                    width = 0
                });
            }

            var imageSize = fileInfo.GetImageSize();
            return JsonNet(new
            {
                name = fileInfo.Name,
                size = fileInfo.Length,
                url = Url.Action("GetImage", "Image", new { filename }),
                height = imageSize.Height,
                width = imageSize.Width
            });
        }

        public ActionResult GetImage(string filename, int? width = null, int? height = null, bool crop = false, int jpegQuality = 95, bool rootPath = false)
        {
            if (filename.IsAbsoluteUrl())
            {
                filename = _imageService.GetImageFromUrl(filename, false);
                rootPath = true;
            }

            var image = _imageService.GetImage(filename, width.GetValueOrDefault(), height.GetValueOrDefault(), crop, jpegQuality, rootPath);
            if (image == null)
            {
                return Redirect(_urlService.GetStaticBrokenImageURL(width.GetValueOrDefault()));
            }

            return File(image, filename.GetMimeType(), Path.GetFileName(filename));
        }
    }
}