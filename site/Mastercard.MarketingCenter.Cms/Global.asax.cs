﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Extensions;
using Pulsus;
using System;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private bool IsInApplicableRegion = false;
        protected void Application_Start()
        {
            MvcHandler.DisableMvcResponseHeader = true;
            Bootstrapper.Execute();
        }

        void Application_Error(object sender, EventArgs e)
        {
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            try
            {
                var app = sender as HttpApplication;
                if (app != null && app.Context != null)
                {
                    app.Context.Response.Headers.Remove("Server");
                }

                const string parametersTosanitize = "ReturnUrl,Source,Referer";
                Request.IsUrlInParametersInternal(parametersTosanitize, false).RejectInvalidCharactersInParameters(parametersTosanitize, false);
            }
            catch (Exception ex)
            {
                LogManager.EventFactory.Create()
                                       .Text("Invalid parameter on Cms")
                                       .Level(LoggingEventLevel.Information)
                                       .AddData("Exception Message", ex.Message)
                                       .AddData("Exception InnerException", ex.InnerException)
                                       .AddData("Exception Data", ex.Data)
                                       .Push();

                string defaultValue = string.IsNullOrEmpty(WebConfigurationManager.AppSettings["AlternativeErrorRedirectionUrl"]) ? null : WebConfigurationManager.AppSettings["AlternativeErrorRedirectionUrl"].ToString();
                Context.RedirectError("/admin/home/error", ex, defaultValue, true);
            }

            SetCurrentCulture();
        }

        protected void Application_PostAuthenticateRequest()
        {
            IsInApplicableRegion = EnsureUserIsInApplicableRegion();
            var principal = DependencyResolver.Current.GetService<IPrincipal>();
            if (principal != null)
            {
                Context.User = principal;
            }
        }

        private bool EnsureUserIsInApplicableRegion()
        {
            var userContext = DependencyResolver.Current.GetService<UserContext>();
            if (userContext != null && Request.IsAuthenticated)
            {
                if (!Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["ApplicableRegions"].Contains(userContext.SelectedRegion))
                {
                    Response.Redirect(new Uri(Slam.Cms.Configuration.ConfigurationManager.Solution.Environments[Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["InApplicableRegionEnvironment"]].AdminUrl).GetLeftPart(UriPartial.Authority).TrimEnd('/') + "/" + Request.Url.PathAndQuery.TrimStart('/'));

                    return false;
                }
                else
                {
                    return true;
                }
            }

            return false;
        }

        private void SetCurrentCulture()
        {
            var userContext = DependencyResolver.Current.GetService<UserContext>();
            if (userContext != null)
            {
                RegionalizeService.SetCurrentCulture(userContext.SelectedRegion, userContext.SelectedLanguage);
            }
        }
    }
}