﻿using System.ComponentModel.DataAnnotations;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class TagCategoryModel
    {
        public string TagCategoryId { get; set; }

        [Required]
        public string Title { get; set; }

        public string TagBrowser { get; set; }

        [Required]
        [Range(0,10000)]
        [Display(Name = "Display Order")]
        public int DisplayOrder { get; set; }
        public string Featurable { get; set; }
        public bool IsRelatable { get; set; }
    }
}