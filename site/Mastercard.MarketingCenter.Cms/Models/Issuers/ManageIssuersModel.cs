﻿using System.Collections.Generic;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data;
using System;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class ManageIssuersModel
    {
        public IEnumerable<Processor> Processors { get; set; }
        public string SelectedProcessorId { get; set; }
        public int IssuerListId { get; set; }
        public bool HideHasOrdered { get; set; }
    }
}