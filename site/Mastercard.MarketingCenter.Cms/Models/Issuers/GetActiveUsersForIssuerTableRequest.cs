﻿using Mastercard.MarketingCenter.Cms.Core.DataTables;

namespace Mastercard.MarketingCenter.Cms.Models.Issuers
{
    public class GetActiveUsersForIssuerTableRequest : DefaultDataTablesRequest
    {
        public string IssuerId { get; set; }
    }
}