﻿using Mastercard.MarketingCenter.Cms.Core.DataTables;

namespace Mastercard.MarketingCenter.Cms.Models.Issuers
{
    public class GetIssuersDataTableRequest : DefaultDataTablesRequest
    {
        public string ProcessorId { get; set; }
    }
}