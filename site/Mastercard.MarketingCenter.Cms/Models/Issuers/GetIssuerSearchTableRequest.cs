﻿using Mastercard.MarketingCenter.Cms.Core.DataTables;

namespace Mastercard.MarketingCenter.Cms.Models.Issuers
{
    public class GetIssuerSearchTableRequest : DefaultDataTablesRequest
    {
        public string IssuerToSearch { get; set; }
        public string IssuerId { get; set; }
    }
}