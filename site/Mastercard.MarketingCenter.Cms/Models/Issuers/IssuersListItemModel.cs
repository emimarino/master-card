﻿using System;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class IssuersListItemModel
    {
        public string IssuerId { get; set; }
        public string Title { get; set; }
        public bool HasOrdered { get; set; }
        public string DomainName { get; set; }
        public string Processor { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
