﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class HeaderModel
    {
        public string Username { get; set; }
        public string LogOutUrl
        {
            get;
            set;
        }

        public string HomeUrl { get; set; }
        public string HeaderSection { get; set; }

        public string SelectedRegion { get; set; }
        public IEnumerable<Region> AvailableRegions { get; set; }
    }
}