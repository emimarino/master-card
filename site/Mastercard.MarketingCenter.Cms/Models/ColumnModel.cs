﻿namespace Mastercard.MarketingCenter.Cms.Models
{
    public class ColumnModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string OrderDirection { get; set; }
        public int Order { get; set; }
        public string FieldType { get; set; }

        public string RenderTemplate
        {
            get
            {
                switch (FieldType)
                {                    
                    case Data.Entities.FieldType.RegionInt:
                        return "render: offer_region_visibility_template";
                    case Data.Entities.FieldType.Boolean:
                    case Data.Entities.FieldType.Customization:
                        return "render: checkbox_template";
                    case Data.Entities.FieldType.GuidPrimaryKey:
                        return "render: id_template";
                    case Data.Entities.FieldType.DateTime:
                    case Data.Entities.FieldType.TrackingCreatedDate:
                    case Data.Entities.FieldType.TrackingUpdatedDate:
                        return "render: datetime_template";
                    case Data.Entities.FieldType.FileUpload:
                        return "render: fileupload_template";
                    case Data.Entities.FieldType.Image:
                        return "render: image_template";
                }

                return string.Empty;
            }
        }

        public bool HasDefaultOrder
        {
            get { return !string.IsNullOrEmpty(OrderDirection); }
        }

        public bool FieldTypeOrder
        {
            get
            {
                switch (FieldType)
                {
                    case Data.Entities.FieldType.RegionInt:
                    case Data.Entities.FieldType.RegionTitle:
                        return true;
                    default:
                        return false;
                }
            }
        }
    }
}