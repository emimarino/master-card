﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class UserCollectionModel : List<UserModel>
    {
        public int GroupId { get; set; }
        public UserCollectionModel(IEnumerable<User> users, int groupId, bool expandGroups = false)
        {
            GroupId = groupId;

            foreach (var user in users)
            {
                Add(new UserModel(user, expandGroups));
            }
        }
    }

    public class UserModel
    {
        public UserModel() { }
        public UserModel(User user, bool expandGroups = false)
        {
            Name = user.FullName;
            UserName = user.UserName;
            UserID = user.UserId;
            IssuerId = user.IssuerId;
            Email = user.Email;
            FeedbackRequestDisplayed = user.Profile.FeedbackRequestDisplayed;
            ContentPreferencesMessageDisplayed = user.Profile.ContentPreferencesMessageDisplayed;
            IsDisabled = user.IsDisabled;
            if (expandGroups)
            {
                Groups = new GroupCollectionModel(user.Groups);
            }
        }

        public string UserName { get; set; }
        public int UserID { get; set; }
        public string Name { get; set; }
        public string IssuerId { get; set; }
        public string Email { get; set; }
        public bool FeedbackRequestDisplayed { get; set; }
        public bool ContentPreferencesMessageDisplayed { get; set; }
        public bool IsDisabled { get; set; }

        public IEnumerable<GroupModel> Groups { get; set; }

        internal User GetUser()
        {
            var result = new User
            {
                UserName = UserName,
                UserId = UserID,
                IssuerId = IssuerId,
                Email = Email,
                IsDisabled = IsDisabled
            };

            return result;
        }
    }
}