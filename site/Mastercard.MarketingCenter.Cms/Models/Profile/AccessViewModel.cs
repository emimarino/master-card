﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Models;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class AccessViewModel
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public IEnumerable<string> Regions { get; set; }
        public IEnumerable<Role> Roles { get; set; }
        public string ReturnUrl { get; set; }
        public UserRoleModel CurrentUserRole { get; set; }
    }
}