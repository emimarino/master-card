﻿using System.Collections.Generic;
using Mastercard.MarketingCenter.Cms.Services.Services;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class FeatureOnModel
    {
        public string ContentTypeId { get; set; }
        public string ContentItemId { get; set; }
        public bool IsInterregionCloning { get; set; } = false;
        public bool InterRegionCloningEnabled { get; set; } = true;
        public List<FeaturableTagCategory> FeaturableTagCategoryItems { get; set; } 
    }
}