﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Models.Content
{
    public class ContentItemObjectVersion : ContentItemVersion
    {
        public Dictionary<string, object> SerializedObject { get; set; }
        public IEnumerable<ContentTypeFieldDefinition> FieldDefinitions { get; set; }
        public bool IsLiveContent { get; set; }
        public string UserEmail { get; set; }
    }

    public class ContentItemVersionModel : List<ContentItemObjectVersion>
    {
    }
}