﻿namespace Mastercard.MarketingCenter.Cms.Models
{
    public class BulkExpirationExtensionViewModel
    {
        public string[] ContentItemIDs { get; set; }
        public string UserFullName { get; set; }
        public bool ShouldExtend { get; set; }
    }
}