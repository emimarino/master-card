﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class ContentModel
    {
        public string ContentTypeTitle { get; set; }
        public object IdFieldValue { get; set; }
        public IDictionary<string, object> ContentItemData { get; set; }
        public IDictionary<string, object> OriginalContentItemData { get; set; }
        public IDictionary<string, object> OriginalContentItemDataForComparison { get; set; }
        public IEnumerable<ContentTypeFieldDefinition> ContentTypeFieldDefinitions { get; set; }
        public string ContentTypeId { get; set; }
        public int StatusId { get; set; }
        public bool CanPreview { get; set; }
        public bool CanExpire { get; set; }
        public bool CanCopy { get; set; }
        public bool CanPublish { get; set; }
        public bool CanCreate { get; set; }
        public bool CanEdit { get; set; }
        public bool CanUpdate { get; set; }
        public bool CanArchive { get; set; }
        public bool CanDelete { get; set; }
        public bool CanDeleteOnlyDraft { get; set; }
        public bool CanRestore { get; set; }
        public bool CanHistory { get; set; }
        public bool CanComment { get; set; }
        public bool CanViewComments { get; set; }
        public bool IsNewForm { get; set; }
        public bool IsInterRegionCloning { get; set; } = false;
        private string _UserRegion;
        public string UserRegion { get { return _UserRegion?.Trim()?.ToLower(); } set { _UserRegion = value?.Trim()?.ToLower(); } }
        public DateTime? ExpirationDate { get; set; }
        public DateTime? OriginalExpirationDate { get; set; }
        private bool _mustExpire = false;
        public bool MustExpire { get { return _mustExpire; } set { _mustExpire = value; } }
        public int CurrentUserId { get; set; }
        public List<string> FieldsEditedByBusinessOwner { get; set; }
        public string ModifiedByUserName { get; set; }
        public int ModifiedByUserId { get; set; }
        public string ModifiedByUserEmail { get; set; }
        public DateTime LastModifyDate { get; set; }
        public int ReviewStateId { get; set; }
        public bool IsCrossBorderRegion { get; set; } = false;
        public IEnumerable<int> UploadActivityIds { get; set; }

        public ContentModel()
        {
            ContentItemData = new Dictionary<string, object>();
            ContentTypeFieldDefinitions = Enumerable.Empty<ContentTypeFieldDefinition>();
            FieldsEditedByBusinessOwner = new List<string>();
            UploadActivityIds = Enumerable.Empty<int>();
        }

        public string Language
        {
            get
            {
                object language;
                ContentItemData.TryGetValue("Language", out language);
                return (language as string)?.Trim();
            }
        }

        public string Uri
        {
            get
            {
                object uri;
                ContentItemData.TryGetValue("Uri", out uri);
                return (uri as string)?.Trim();
            }
        }
    }
}