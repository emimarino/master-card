﻿using Mastercard.MarketingCenter.Cms.Core.DataTables;

namespace Mastercard.MarketingCenter.Cms.Models.Content
{
    public class GetContentItemsDataTablesRequest : DefaultDataTablesRequest
    {
        public string ContentTypeId { get; set; }
        public bool ShowArchived { get; set; }
        public bool isExpired { get; internal set; }
        public bool ShowPendings { get; set; }
    }
}