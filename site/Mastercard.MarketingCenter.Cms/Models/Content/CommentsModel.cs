﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mastercard.MarketingCenter.Cms.Models.Content
{
    public class CommentsModel
    {
        public string ContentItemId { get; set; }

        public int CurrentUserId { get; set; }
    }
}