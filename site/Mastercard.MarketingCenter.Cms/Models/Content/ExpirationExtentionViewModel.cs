﻿using System;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class ExpirationExtensionViewModel
    {
        public string ContentItemID { get; set; }
        public string Title { get; set; }
        public string UserFullName { get; set; }
        public DateTime ExpirationDate { get; set; }
        public bool ShouldExtend { get; set; }
    }
}