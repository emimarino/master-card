﻿using Mastercard.MarketingCenter.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class ContentCommentModalViewModel
    {
        public int CommentsCount { get; set; }
        public string ContentItemId { get; set; }
        public int SenderUserId { get; set; }
        public string BackLink { get; set; } = "";
    }
}