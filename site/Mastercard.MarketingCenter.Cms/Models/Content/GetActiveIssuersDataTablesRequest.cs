﻿using Mastercard.MarketingCenter.Cms.Core.DataTables;

namespace Mastercard.MarketingCenter.Cms.Models.Content
{
    public class GetActiveIssuersDataTablesRequest : DefaultDataTablesRequest
    {
        public string Status { get; set; }
    }
}