﻿namespace Mastercard.MarketingCenter.Cms.Models
{
    public class ContentCommentViewModel
    {
        public string SenderUserAvatar { get; set; }
        public string ContentItemId { get; set; }
        public string BackLink { get; set; } = "";
        public bool IsNew { get; set; }
        public int CommentsTotal { get; set; }
        /// <summary>
        /// If <value>true</value> then BO is notified about added comments, otherwise: admin
        /// </summary>
        public bool IsAdmin { get; set; }
    }
}