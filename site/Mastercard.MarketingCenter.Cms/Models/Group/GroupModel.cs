﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Text;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class GroupCollectionModel : List<GroupModel>
    {
        public GroupCollectionModel(ICollection<Group> groups, bool expandUsers = false)
        {
            foreach (var group in groups)
            {
                this.Add(new GroupModel(group, expandUsers));
            }
        }
    }
    public class GroupModel
    {
        public int GroupId { get; set; }
        public string Name { get; set; }
        public List<UserModel> Users { get; set; }
        public string UsersIds { get; set; }
        public GroupModel()
        {
        }
        public GroupModel(Group group, ICollection<User> users)
        {
            this.GroupId = group.GroupId;
            this.Name = group.Name;

            Users = new List<UserModel>();
            foreach (var user in users)
            {
                this.Users.Add(new UserModel(user));
            }
        }
        public GroupModel(Group group, bool expandUsers = false)
        {
            this.GroupId = group.GroupId;
            this.Name = group.Name;

            Users = new UserCollectionModel(group.Users, group.GroupId, false);

            var bldr = new StringBuilder();

            foreach (var user in group.Users)
            {
                bldr.Append(user.UserId + ";");
            }
        }
        internal Group GetGroup()
        {
            var result = new Group();

            result.Name = this.Name;
            result.GroupId = this.GroupId;

            return result;
        }
    }
}