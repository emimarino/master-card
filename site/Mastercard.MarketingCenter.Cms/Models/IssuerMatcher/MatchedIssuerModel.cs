﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class MatchedIssuerModel
    {
        public string Cid { get; set; }
        public string IssuerId { get; set; }
        public bool MatchConfirmed { get; set; }

        public ImportedIca ImportedIca { get; set; }
    }
}