﻿using System.ComponentModel.DataAnnotations;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class ContentTypeModel
    {
        public string ContentTypeId { get; set; }

        [Required]
        public string Title { get; set; }
    }
}
