﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class ListModel
    {
        public object Id { get; set; }
        public string Title { get; set; }
        public string TableName { get; set; }
        public IEnumerable<ColumnModel> Columns { get; set; }
        public string IdColumnName { get; set; }
        public bool CanPreview { get; set; }
        public bool CanCopy { get; set; }
        public bool CanPublish { get; set; }
        public bool CanCreate { get; set; }
        public bool CanEdit { get; set; }
        public bool CanUpdate { get; set; }
        public bool CanArchive { get; set; }
        public bool CanDelete { get; set; }
        public bool CanDeleteOnlyDraft { get; set; }
        public bool CanRestore { get; set; }
        public bool CanHistory { get; set; }
        public bool CanBePending { get; set; }
        public bool ShowTrackingFields { get; set; }
        public string CreatedDateFieldName { get; set; }
        public string ModifiedDateFieldName { get; set; }
        public bool CanExpire { get; set; }
        public bool HasCrossBorderRegions { get; set; }
    }
}