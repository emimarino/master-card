﻿using Mastercard.MarketingCenter.Cms.Core.DataTables;

namespace Mastercard.MarketingCenter.Cms.Models.Content
{
    public class GetExpirationContentDataTablesRequest : DefaultDataTablesRequest
    {
        public int Months { get; set; }
    }
}