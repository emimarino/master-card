﻿using System.Collections.Generic;
using Mastercard.MarketingCenter.Cms.Services.Services;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class CustomizationModel
    {
        public string OrderableAssetId { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public IList<CustomizationInfo> Customizations { get; set; } 
    }
}
