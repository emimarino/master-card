﻿using System.Collections.Generic;
using Mastercard.MarketingCenter.Cms.Services.Services;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class SelectedCustomizationModel
    {
        public IEnumerable<CustomizationInfo> OptionalCustomizations { get; set; } 
        public IEnumerable<CustomizationInfo> RequiredCustomizations { get; set; } 
    }
}
