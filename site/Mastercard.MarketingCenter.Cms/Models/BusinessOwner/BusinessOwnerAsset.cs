﻿using System;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class BusinessOwnerAsset
    {
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public string ThumbnailUrl { get; set; }
        public bool ThumbnailHasRelativeUrl { get; set; }
        public string ShortDescription { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int CommentCount { get; set; }
        public bool NewComments { get; set; }
        public ReviewStateSelectOption ReviewState { get; set; }

        public double DaysToExpire
        {
            get
            {
                return (ExpirationDate - DateTime.Today).TotalDays;
            }
        }
    }
}