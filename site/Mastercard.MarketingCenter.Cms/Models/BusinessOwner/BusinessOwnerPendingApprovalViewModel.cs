﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class BusinessOwnerPendingApprovalViewModel
    {
        public IEnumerable<BusinessOwnerAsset> BusinessOwnerAssets { get; set; }
        public int ExpirationWindowInDays { get; set; }
        public int Page { get; set; }
        public int TotalPages { get; set; }
        public int TotalResultsInPage { get; set; }
        public int CurrentUserId { get; set; }
        public string CurrentUserName { get; set; }
        public IEnumerable<ReviewStateSelectOption> ExpirationReviewStates { get; set; }
        public ReviewStateSelectOption DefaultReviewState { get; set; }
        public bool ShowAll { get; set; } = false;
    }
}