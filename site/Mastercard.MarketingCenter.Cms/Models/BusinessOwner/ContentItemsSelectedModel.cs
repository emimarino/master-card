﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class ContentItemsSelectedModel
    {
        public IEnumerable<string> SelectedContentItems { get; set; }
    }
}