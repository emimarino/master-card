﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class BusinessOwnerTabHeaderViewModel
    {
        public string Controller { get; set; }
        public string ActiveTab { get; set; }
        public List<BusinessOwnerTabHeaderTabViewModel> Tabs { get; set; }
    }

    public class BusinessOwnerTabHeaderTabViewModel
    {
        public string Name { get; set; }
        public string Action { get; set; }
    }
}