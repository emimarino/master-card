﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class ConfirmDeletionViewModel
    {
        public string UserFullName { get; set; }

        public IEnumerable<string> AssetTitlesToDelete { get; set; }

        public IEnumerable<string> AssetIdsToDelete { get; set; }
    }
}