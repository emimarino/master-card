﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class BusinessOwnerPublishedAssetsViewModel
    {
        public IEnumerable<BusinessOwnerAsset> BusinessOwnerAssets { get; set; }
        public bool ShowAllAssets { get; set; }
        public List<int> ExpirationDays { get; set; }
        public int ExpirationWindowInDays { get; set; }
        public int Page { get; set; }
        public int TotalPages { get; set; }
        public string CurrentUserName { get; set; }
        /// <summary>
        /// This Propery is intended for the Extend Do not Extend link from external
        /// source such as Emails to choose how to open the modal.
        /// </summary>
        public bool OpenExtendModal { get; set; }
        /// <summary>
        /// This Propery is intended for the Extend Do not Extend link from external
        /// source such as Emails to choose how to open the modal.
        /// </summary>
        public int ExtendModalState { get; set; }
        /// <summary>
        /// This Propery is intended for the Extend Do not Extend link from external
        /// source such as Emails to choose how to open the modal.
        /// </summary>
        public string ExtendModalContentItemId { get; set; }
        public bool OpenSuccessModal { get; set; }
    }
}