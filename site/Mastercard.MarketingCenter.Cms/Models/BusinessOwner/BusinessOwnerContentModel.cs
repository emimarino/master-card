﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class BusinessOwnerContentModel
    {
        public object IdFieldValue { get; set; }
        public IDictionary<string, object> ContentItemData { get; set; }
        public IDictionary<string, object> OriginalContentItemData { get; set; }
        public IEnumerable<ContentTypeFieldDefinition> ContentTypeFieldDefinitions { get; set; }
        public string ContentItemTitle { get; set; }
        public string ContentTypeId { get; set; }
        public string RegionId { get; set; }
        public string RegionTitle { get; set; }
        public IEnumerable<Region> AvailableRegions { get; set; }
        public bool CanExpire { get; set; }
        private bool _mustExpire = false;
        public bool MustExpire { get { return _mustExpire; } set { _mustExpire = value; } }
        public DateTime? CurrentExpirationDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int BusinessOwnerId { get; set; }
        public int CurrentUserId { get; set; }
        public bool IsNewForm { get; set; }
        public bool ShowPublishedOnly { get; set; }
        /// <summary>
        /// A specific field for Asset Full Width only to be able to show Icon instead of Thumbnail
        /// </summary>
        public string IconUrl { get; set; }
        public string ReturnUrl { get; set; }
        public IEnumerable<int> UploadActivityIds { get; set; }

        public BusinessOwnerContentModel()
        {
            ContentItemData = new Dictionary<string, object>();
            ContentTypeFieldDefinitions = Enumerable.Empty<ContentTypeFieldDefinition>();
            UploadActivityIds = Enumerable.Empty<int>();
        }

        public string Language
        {
            get
            {
                object language;
                ContentItemData.TryGetValue("Language", out language);
                return (language as string)?.Trim();
            }
        }
    }
}