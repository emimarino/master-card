﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class ReviewStateSelectOption : ExpirationReviewState
    {
        public string CssClassName { get; set; }
    }
}