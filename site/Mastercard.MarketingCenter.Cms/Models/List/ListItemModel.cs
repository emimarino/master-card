﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class ListItemModel
    {
        public string ListTitle { get; set; }
        public string ListTableName { get; set; }
        public object IdFieldValue { get; set; }
        public IDictionary<string, object> ListItemData { get; set; } 
        public IEnumerable<ListFieldDefinition> ListFieldDefinitions { get; set; }
        public int ListId { get; set; }
        public string ReturnUrl { get; set; }
        public bool HasTrackingFields { get; set; }
        public string ClientSuccesCallback { get; set; }
        public string ClientErrorCallback { get; set; }
        public string ClientCompleteCallback { get; set; }
        public string UpdateTargetId { get; set; }

        public ListItemModel()
        {
            ListItemData = new Dictionary<string, object>();
            ListFieldDefinitions = Enumerable.Empty<ListFieldDefinition>();
        }
    }
}