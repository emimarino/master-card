﻿using Mastercard.MarketingCenter.Cms.Core.DataTables;

namespace Mastercard.MarketingCenter.Cms.Models.List
{
    public class GetListItemsDataTablesRequest : DefaultDataTablesRequest
    {
        public int ListId { get; set; }
        public bool isExpired { get; internal set; }
    }
}