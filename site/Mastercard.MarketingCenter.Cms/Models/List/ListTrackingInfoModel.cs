﻿namespace Mastercard.MarketingCenter.Cms.Models
{
    public class ListTrackingInfoModel
    {
        public string Id { get; set; }
        public int ListId { get; set; }
    }
}