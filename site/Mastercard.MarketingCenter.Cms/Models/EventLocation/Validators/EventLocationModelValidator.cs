﻿using FluentValidation;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Models.Validators
{
    public class EventLocationModelValidator : AbstractValidator<EventLocationModel>
    {
        public EventLocationModelValidator()
        {
            RuleFor(x => x.Title).NotEmpty().WithMessage("Title is required.");
            RuleFor(x => x.PricelessEventCity).Must((x, pricelessEventCity) => BeUniquePricelessEventCity(x.EventLocationId, pricelessEventCity)).WithMessage("An Event Location with the same Priceless Event City already exists.");
        }

        private bool BeUniquePricelessEventCity(int eventLocationId, string pricelessEventCity)
        {
            return pricelessEventCity.IsNullOrWhiteSpace() || !DependencyResolver.Current.GetService<IEventLocationService>().ExistPricelessEventCity(eventLocationId, pricelessEventCity);
        }
    }
}