﻿using FluentValidation.Attributes;
using Mastercard.MarketingCenter.Cms.Models.Validators;
using System.ComponentModel.DataAnnotations;

namespace Mastercard.MarketingCenter.Cms.Models
{
    [Validator(typeof(EventLocationModelValidator))]
    public class EventLocationModel
    {
        public int EventLocationId { get; set; }
        public string Title { get; set; }
        public string PricelessEventCity { get; set; }
    }
}