﻿namespace Mastercard.MarketingCenter.Cms.Models
{
    public class IndexModel
    {
        public bool IsGlobalRegion { get; set; }
        public bool BusinessOwnerExpirationManagementEnabled { get; set; }
        public bool ProgramPrintOrderDateExpirationEnabled { get; set; }
    }
}