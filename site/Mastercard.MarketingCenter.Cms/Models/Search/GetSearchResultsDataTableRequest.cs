﻿using Mastercard.MarketingCenter.Cms.Core.DataTables;

namespace Mastercard.MarketingCenter.Cms.Models.Search
{
    public class GetSearchResultsDataTableRequest : DefaultDataTablesRequest
    {
        public string SearchTerm { get; set; }
    }
}