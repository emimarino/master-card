﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mastercard.MarketingCenter.Cms.Models
{
    public class ResourceAccessModel
    {
        public long ResourceAccessId { get; set; }
        public int IdentityId { get; set; }
        public int IdentityType { get; set; }
        public string IdentityName { get; set; }
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Id { get; set; }
        public string QueryString { get; set; }
 

    }
}