﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Data;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Cms.WebForms
{
    public partial class PendingRegistrationsReport : System.Web.UI.Page
    {
        protected string PendingRegistrationUrl { get; set; }
        private SlamContext slamContext;
        private UserContext userContext;
        private bool exporting = false;
        public IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }
        public IPendingRegistrationService _pendingRegistrationService { get { return DependencyResolver.Current.GetService<IPendingRegistrationService>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            RegionalizeService.SetCurrentCulture(RegionalizeService.DefaultRegion, RegionalizeService.DefaultLanguage); //always default to English-US
            if (!IsPostBack)
            {
                if (Request.QueryString["guid"] != null)
                {
                    PendingRegistrationUrl = _urlService.GetPendingRegistrationFrontEndURL(Request.QueryString["guid"]);
                    phParentRedirect.Visible = false;
                    trgReport.Visible = false;
                    phMain.Visible = false;
                    phLoading.Visible = true;
                }
            }

            userContext = DependencyResolver.Current.GetService<UserContext>();
            slamContext = DependencyResolver.Current.GetService<SlamContext>();
        }

        protected void trgReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            PopulateReport();
        }

        private void PopulateReport()
        {
            var results = _pendingRegistrationService.GetPendingRegistrationsByStatus(chkShowRejectedRegistrations.Checked ?
                                                                                      Constants.PendingRegistrationStatus.Rejected :
                                                                                      Constants.PendingRegistrationStatus.Pending,
                                                                                      userContext.SelectedRegion).ToList();
            var regionTag = slamContext.GetTagTree().FindNode(n => n.Identifier == userContext.SelectedRegion);
            var countryField = trgReport.Columns.FindByDataField("Country");
            countryField.Visible = regionTag != null && regionTag.Children != null && regionTag.Children.Count > 0;
            results.ForEach(r =>
            {
                r.DateAdded = r.DateAdded.Date;
                r.DateStatusChanged = r.DateStatusChanged?.Date;
                if (countryField.Visible && !r.Country.IsNullOrEmpty())
                {
                    r.Country = slamContext.GetTagTree().FindNode(n => n.Tag != null && n.Tag.Identifier.Equals(r.Country, StringComparison.OrdinalIgnoreCase))?.Text;
                }
            });

            trgReport.DataSource = results;
            if (results.Any())
            {
                trgReport.Visible = true;
                phNoResults.Visible = false;
                phNoResultsRejected.Visible = false;
                if (chkShowRejectedRegistrations.Checked)
                {
                    trgReport.Columns.FindByUniqueName("RejectedDate").Visible = true;
                    trgReport.Columns.FindByUniqueName("StatusChangedBy").Visible = true;
                }
                else
                {
                    trgReport.Columns.FindByUniqueName("RejectedDate").Visible = false;
                    trgReport.Columns.FindByUniqueName("StatusChangedBy").Visible = false;
                }

            }
            else
            {
                trgReport.Visible = false;
                phNoResults.Visible = !chkShowRejectedRegistrations.Checked;
                phNoResultsRejected.Visible = chkShowRejectedRegistrations.Checked;
            }
        }

        protected void trgReport_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "redirect")
            {
                Response.Redirect(_urlService.GetPendingRegistrationAdminURL(e.CommandArgument.ToString(), chkShowRejectedRegistrations.Checked));
            }

            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName || e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToWordCommandName)
            {
                exporting = true;
                trgReport.HeaderStyle.Width = Unit.Pixel(70);
                trgReport.MasterTableView.GetColumn("Email").Visible = true;
                trgReport.MasterTableView.GetColumn("Email").HeaderStyle.Width = Unit.Pixel(170);
                trgReport.MasterTableView.GetColumn("EmailLink").Visible = false;
                trgReport.MasterTableView.GetColumn("Country").HeaderStyle.Width = Unit.Pixel(60);
            }
        }

        protected void trgReport_PreRender(object sender, EventArgs e)
        {
            trgReport.Culture = new System.Globalization.CultureInfo("en-US");
            if (exporting)
            {
                foreach (var header in trgReport.MasterTableView.GetItems(GridItemType.Header))
                {
                    foreach (TableCell cell in header.Cells)
                    {
                        cell.Style["text-align"] = "left";
                    }
                }
            }
        }

        protected void chkShowRejectedRegistrations_CheckedChanged(object source, EventArgs e)
        {
            PopulateReport();
            trgReport.Rebind();
        }
    }
}