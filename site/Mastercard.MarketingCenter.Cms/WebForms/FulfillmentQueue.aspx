﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FulfillmentQueue.aspx.cs" Inherits="Mastercard.MarketingCenter.Cms.WebForms.FulfillmentQueue" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head runat="server">
    <link href="../Content/legacy/css/MarketingCenter.SharedControls.css" type="text/css" rel="stylesheet" />
    <title></title>
</head>

<body>
    <form id="form1" runat="server">
        <div>

            <div class="center_container">
                <div class="form_container">
                    <div class="grid_container">
                        <asp:Label runat="server" ID="lblNoRecords" CssClass="grey_heading_bld" Text="There are currently no items in the list." Visible="false"></asp:Label>
                        <asp:Repeater ID="rptorder" Visible="true" runat="server">
                            <HeaderTemplate>
                                <div class="grid_top_head">
                                    <span class="grid_heading new_width">Order #</span>
                                    <span class="grid_heading new_width">Date</span>
                                    <span class="grid_heading ">Name</span>
                                    <span class="grid_heading ">Processor</span>
                                    <span class="grid_heading ">Issuer</span>
                                </div>
                                <div class="grid_top_bg">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="white_row">
                                    <span class="data new_width">
                                        <a href='<%# Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["OrderDetailUrl"] + "?orderid=" + ((Eval("OrderNumber")!=null) ?  Eval("OrderNumber").ToString() : "") + "&Source=FulfillmentQueue" %>' class="price">
                                            <%# Eval("OrderNumber")%>
                                        </a>&nbsp;
                                    </span>
                                    <span class="data new_width"><%# (Eval("OrderDate")!= null) ? Convert.ToDateTime(Eval("OrderDate").ToString()).ToString("MM/dd/yyyy") : "" %></span>
                                    <span class="data "><%# Eval("FullName")%> &nbsp;</span>
                                    <span class="data"><%# Eval("ProcessorName")%> &nbsp;</span>
                                    <span class="data "><%# Eval("IssuerName")%>&nbsp; </span>
                                </div>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <div class="white_row gray_backgrnd">
                                    <span class="data new_width">
                                        <a href='<%# Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["OrderDetailUrl"] + "?orderid=" + ((Eval("OrderNumber")!=null) ?  Eval("OrderNumber").ToString() : "") + "&Source=FulfillmentQueue" %>' class="price">
                                            <%# Eval("OrderNumber")%>
                                        </a>&nbsp;
                                    </span>
                                    <span class="data new_width"><%#(Eval("OrderDate")!= null) ? Convert.ToDateTime(Eval("OrderDate").ToString()).ToString("MM/dd/yyyy") : "" %></span>
                                    <span class="data "><%# Eval("FullName")%> &nbsp;</span>
                                    <span class="data"><%# Eval("ProcessorName")%> &nbsp;</span>
                                    <span class="data "><%# Eval("IssuerName")%>&nbsp; </span>
                                </div>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </div>
					<div class="grid_top_head_bot"></div>
                                <div class="pd_bot">&nbsp;</div>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>

        </div>
    </form>
</body>
</html>
