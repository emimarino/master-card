﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Cms.WebForms
{
    public partial class PromotionReport : System.Web.UI.Page
    {
        public string IssuerId { get; set; } = "";
        public double Total { get; } = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["IssuerID"] != null)
                IssuerId = Request.QueryString["IssuerID"];

            if (!IsPostBack)
            {
                GetPromotionsFilter();

                if (ddlPromotions.Items.Count > 0)
                {
                    string promotion = "";
                    if (Request.QueryString["Promotion"] == null)
                        promotion = ddlPromotions.Items[0].Value;
                    else
                        promotion = Request.QueryString["Promotion"];

                    GetPromotionOrders(System.Web.HttpUtility.UrlDecode(promotion), IssuerId);
                }
            }
        }

        public void GetPromotionsFilter()
        {
            ddlPromotions.DataSource = PromotionService.GetPromotionReportFilters();
            ddlPromotions.DataValueField = "Promotion";
            ddlPromotions.DataTextField = "Promotion";
            ddlPromotions.DataBind();
        }

        public void GetPromotionOrders(string promotion, string issuerID)
        {
            if (issuerID.Length == 0)
            {
                List<RetrievePromotionReportResult> orders = PromotionService.GetPromotionOrders(promotion, issuerID);

                var result = from order in orders
                             group order by new { order.DiscountTotal, order.IssuerID, order.Issuer, order.Processor, order.Promotion } into g
                             orderby g.Key.Processor, g.Key.Issuer, g.Count() descending
                             where g.Count() > 0
                             select new { OrderNumber = g.Count(), DiscountTotal = g.Sum(s => s.DiscountTotal), g.Key.IssuerID, g.Key.Issuer, g.Key.Processor, g.Key.Promotion };

                lvPromotions.DataSource = result;
                lvPromotions.DataBind();

                phPromotions.Visible = true;
                phOrders.Visible = false;

                ltTotal.Text = "$" + orders.Sum(o => o.DiscountTotal).ToString();

                decimal subTotal = 0;

                for (int i = 0; i <= lvPromotions.Items.Count - 1; i++)
                {
                    Literal ltProcessor = lvPromotions.Items[i].FindControl("ltProcessor") as Literal;
                    Literal ltDiscountTotal = lvPromotions.Items[i].FindControl("ltDiscountTotal") as Literal;
                    Literal ltProcessorPrev = new Literal();
                    Literal ltProcessorNext = new Literal();

                    subTotal += Convert.ToDecimal(ltDiscountTotal.Text.Replace("$", ""));

                    if (i > 0 && i <= lvPromotions.Items.Count - 1)
                        ltProcessorPrev = lvPromotions.Items[i - 1].FindControl("ltProcessor") as Literal;

                    if ((i + 1) <= lvPromotions.Items.Count - 1)
                        ltProcessorNext = lvPromotions.Items[i + 1].FindControl("ltProcessor") as Literal;

                    if ((ltProcessor.Text == ltProcessorPrev.Text && ltProcessor.Text != ltProcessorNext.Text) || ((i == 0 && lvPromotions.Items.Count > 1) && ltProcessor.Text != ltProcessorNext.Text) || i == lvPromotions.Items.Count - 1)
                    {
                        PlaceHolder phSubTotal = lvPromotions.Items[i].FindControl("phSubTotal") as PlaceHolder;
                        phSubTotal.Visible = true;

                        Literal ltProcessorForSubTotal = lvPromotions.Items[i].FindControl("ltProcessorForSubTotal") as Literal;
                        Literal ltSubTotal = lvPromotions.Items[i].FindControl("ltSubTotal") as Literal;

                        ltProcessorForSubTotal.Text = "Sub-Total for " + ltProcessor.Text;
                        ltSubTotal.Text = "$" + subTotal.ToString();
                        subTotal = 0;
                    }
                }
            }
            else
            {
                List<RetrievePromotionReportResult> orders = PromotionService.GetPromotionOrders(promotion, issuerID);

                gvOrders.DataSource = orders;
                gvOrders.DataBind();
                phPromotions.Visible = false;
                phOrders.Visible = true;

                ddlPromotions.ClearSelection();
                ddlPromotions.Items.FindByText(promotion).Selected = true;
                var getBackUrl = Page.GetRouteUrl("webforms",
                    new { page = "PromotionReport", header = "Promotions Report", renderPageHeading = true });
                litCaption.Text = "<div class=\"main_orng_heading\" style=\"margin:0 0 10px 0\">" + orders.First().Issuer + " - " + orders.First().Processor + string.Format("<span><a href=\"{0}\">Back to Main Report ></a></span></div>", getBackUrl);
            }
        }

        protected void gvOrders_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex % 2 == 0)
                    e.Row.CssClass = "white_row";
                else
                    e.Row.CssClass = "white_row gray_backgrnd";
            }
        }

        protected void ddlPromotions_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetPromotionOrders(ddlPromotions.SelectedValue, "");
        }
    }
}