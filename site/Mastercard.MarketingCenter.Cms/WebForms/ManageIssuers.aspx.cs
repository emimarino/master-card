﻿using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.WebControls;
using IUnitOfWork = Mastercard.MarketingCenter.Data.IUnitOfWork;

namespace Mastercard.MarketingCenter.Cms.WebForms
{
    public partial class ManageIssuers : Page
    {
        private const string ASCENDING = " ASC";
        private const string DESCENDING = " DESC";
        int pageSize = 10;
        private UserContext _userContext;
        private IIssuerService _issuerService;
        private ProcessorService _processorService;
        private IUnitOfWork _unitOfWork;

        protected UserContext UserContext { get { if (_userContext == null) { _userContext = DependencyResolver.Current.GetService<UserContext>(); } return _userContext; } }

        protected IIssuerService IssuerService { get { if (_issuerService == null) { _issuerService = DependencyResolver.Current.GetService<IIssuerService>(); } return _issuerService; } }

        protected ProcessorService ProcessorService { get { if (_processorService == null) { _processorService = DependencyResolver.Current.GetService<ProcessorService>(); } return _processorService; } }

        protected IUnitOfWork UnitOfWork { get { if (_unitOfWork == null) { _unitOfWork = DependencyResolver.Current.GetService<IUnitOfWork>(); } return _unitOfWork; } }

        public string Source
        {
            get
            {

                var source = Page.GetRouteUrl("Default",
                    new RouteValueDictionary
                    {
                        {"action", "Index"},
                        {"controller", "WebForm"},
                        {"id", "ManageIssuers"},
                        {"header", "Manage Issuers"},
                        {"renderPageHeading", "true"}
                    });

                return source;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.UrlReferrer != null && (Request.UrlReferrer.ToString().Contains("EditForm") || Request.UrlReferrer.ToString().Contains("NewForm")))
                {
                    Thread.Sleep(1000);
                }

                Session.Remove("Issuers");

                gvIssuers.PageSize = pageSize;
                GetIssuers();
            }
        }

        protected void gvIssuers_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvIssuers.EditIndex = e.NewEditIndex;
            GetIssuers();
            phError.Visible = false;
            litError.Text = "";
        }

        protected void gvIssuers_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            Clear();
        }

        protected void gvIssuers_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            phError.Visible = false;
            litError.Text = "";
            string msg = "";
            string issuerID = (string)gvIssuers.DataKeys[e.RowIndex].Value;

            GridViewRow row = gvIssuers.Rows[e.RowIndex];
            TextBox txtIssuerName = row.FindControl("txtIssuerName") as TextBox;
            TextBox txtDomainName = row.FindControl("txtDomainName") as TextBox;

            if (string.IsNullOrEmpty(txtIssuerName.Text.Trim()))
            {
                msg += "Please enter a Name.<br/>";
            }

            if (string.IsNullOrEmpty(txtDomainName.Text.Trim()))
            {
                msg += "Please enter a Domain Name. Use comma to put additional domains if needed. (Ex: domain1.com,domain2.com)<br/>";
            }

            if (string.IsNullOrEmpty(msg))
            {
                string[] domains = txtDomainName.Text.Split(',');

                foreach (var domain in domains)
                {
                    var domainIssuer = IssuerService.GetIssuerByDomain(domain, UserContext.SelectedRegion);

                    if (domainIssuer != null && domainIssuer.IssuerId != issuerID)
                    {
                        msg += $"Domain '{domain}' is currently being used by the Financial Institution '{domainIssuer.Title}'.<br/>";
                    }

                    if (!IssuerService.VerifyDomainIsValid(domain.Trim()))
                    {
                        msg += $"Domain is not permitted for use as an Issuer domain: {domain.Trim()}<br/>";
                    }
                }
            }

            if (!string.IsNullOrEmpty(msg))
            {
                phError.Visible = true;
                litError.Text = msg;
                return;
            }

            var issuer = IssuerService.GetIssuerById(issuerID);
            if (issuer != null)
            {
                issuer.DomainName = txtDomainName.Text.Trim();
                issuer.Title = txtIssuerName.Text.Trim();
                issuer.ModifiedDate = DateTime.Now;
                UnitOfWork.Commit();
            }

            gvIssuers.EditIndex = -1;
            Response.Redirect(Request.Url.ToString(), false);
        }

        protected void gvIssuers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvIssuers.PageIndex = e.NewPageIndex;
            GetIssuers();
            phError.Visible = false;
            litError.Text = "";
        }

        protected void gvIssuers_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression;
            ViewState["SortExpression"] = sortExpression;
            IEnumerable<RetrieveIssuersByProcessorDTO> issuers = null;
            if (GridViewSortDirection == SortDirection.Ascending)
            {
                GridViewSortDirection = SortDirection.Descending;
                issuers = SortGridView(GetIssuers(), sortExpression, DESCENDING);
            }
            else
            {
                GridViewSortDirection = SortDirection.Ascending;
                issuers = SortGridView(GetIssuers(), sortExpression, ASCENDING);
            }

            Clear();
            gvIssuers.DataSource = issuers;
            gvIssuers.DataBind();
        }

        private SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                {
                    ViewState["sortDirection"] = SortDirection.Ascending;
                }

                return (SortDirection)ViewState["sortDirection"];
            }
            set
            {
                ViewState["sortDirection"] = value;
            }
        }

        private IEnumerable<RetrieveIssuersByProcessorDTO> SortGridView(
            IEnumerable<RetrieveIssuersByProcessorDTO> issuers,
            string sortExpression,
            string direction)
        {
            switch (sortExpression)
            {
                case "IssuerName":
                    if (direction == " ASC")
                        issuers = issuers.OrderBy(i => i.IssuerName).ToList();
                    else
                        issuers = issuers.OrderBy(i => i.IssuerName).Reverse().ToList();
                    break;
                case "DomainName":
                    if (direction == " ASC")
                        issuers = issuers.OrderBy(i => i.DomainName).ToList();
                    else
                        issuers = issuers.OrderBy(i => i.DomainName).Reverse().ToList();
                    break;
                case "Users":
                    if (direction == " ASC")
                        issuers = issuers.OrderBy(i => i.Users).ToList();
                    else
                        issuers = issuers.OrderBy(i => i.Users).Reverse().ToList();
                    break;
                case "Orders":
                    if (direction == " ASC")
                        issuers = issuers.OrderBy(i => i.Orders).ToList();
                    else
                        issuers = issuers.OrderBy(i => i.Orders).Reverse().ToList();
                    break;
                case "LastOrderDate":
                    if (direction == " ASC")
                        issuers = issuers.OrderBy(i => i.LastOrderDate).ToList();
                    else
                        issuers = issuers.OrderBy(i => i.LastOrderDate).Reverse().ToList();
                    break;
            }

            return issuers;
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            Clear();
            phAddIssuer.Visible = true;
            btnAddNew.Visible = false;
            spanAddNew.Visible = false;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            phError.Visible = false;
            litError.Text = "";
            string msg = "";

            if (string.IsNullOrEmpty(txtIssuerName.Text.Trim()))
            {
                msg += "Please enter a Name.<br/>";
            }

            if (string.IsNullOrEmpty(txtIssuerDomain.Text.Trim()))
            {
                msg += "Please enter a Domain Name. Use comma to put additional domains if needed. (Ex: domain1.com,domain2.com)<br/>";
            }

            if (string.IsNullOrEmpty(msg))
            {
                string[] domains = txtIssuerDomain.Text.Split(',');
                foreach (var domain in domains)
                {
                    var issuer = IssuerService.GetIssuerByDomain(domain, UserContext.SelectedRegion);
                    if (issuer != null)
                    {
                        msg += $"Domain '{domain}' is currently being used by the Financial Institution '{issuer.Title}'.<br/>";
                    }

                    if (!IssuerService.VerifyDomainIsValid(domain.Trim()))
                    {
                        msg += $"Domain is not permitted for use as an Issuer domain: {domain.Trim()}<br/>";
                    }
                }
            }

            if (!string.IsNullOrEmpty(msg))
            {
                phError.Visible = true;
                litError.Text = msg;
                return;
            }

            var processorId = GetProcessor();
            IssuerService.CreateIssuer(
                txtIssuerName.Text.Trim(),
                txtIssuerDomain.Text.Trim(),
                null,
                null,
                processorId,
                UserContext.SelectedRegion,
                UserContext.User.UserId);

            phError.Visible = true;
            litError.Text = "<p style='color:#ff6600;'>New issuer is added successfully.</p>";
            txtIssuerName.Text = "";
            txtIssuerDomain.Text = "";
            txtIssuerName.Focus();
            Session.Remove("Issuers");
            GetIssuers();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.Url.ToString(), false);
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            Session.Remove("Issuers");
            Clear();
        }

        public void Clear()
        {
            gvIssuers.EditIndex = -1;
            GetIssuers();
            phError.Visible = false;
            litError.Text = "";
        }

        protected string GetProcessor()
        {
            if (ViewState["ProcessorFilter"] == null)
            {
                ViewState["ProcessorFilter"] = Request.QueryString["p"] ?? ProcessorService.GetProcessorByIssuerId(UserContext.User.IssuerId)?.ProcessorId;
            }

            return ViewState["ProcessorFilter"]?.ToString();
        }

        private IEnumerable<RetrieveIssuersByProcessorDTO> GetIssuers()
        {
            IEnumerable<RetrieveIssuersByProcessorDTO> issuers = null;
            if (Session["Issuers"] != null)
            {
                issuers = (IEnumerable<RetrieveIssuersByProcessorDTO>)Session["Issuers"];
            }
            else
            {
                issuers = IssuerService.GetIssuersByProcessor(GetProcessor(), UserContext.SelectedRegion);
                Session["Issuers"] = issuers;
            }

            if (!string.IsNullOrEmpty(txtFilter.Text) && txtFilter.Text.Trim() != "-- Type Issuer or Domain Name --")
            {
                string term = txtFilter.Text.Trim().ToLower();
                issuers = issuers.Where(i => i.IssuerName.ToLower().Contains(term) ||
                    i.DomainName.ToLower().Contains(term)).ToList();
            }

            gvIssuers.DataSource = SortGridView(issuers,
                                                ViewState["SortExpression"] != null ? ViewState["SortExpression"].ToString() : "IssuerName",
                                                GridViewSortDirection == SortDirection.Ascending ? ASCENDING : DESCENDING);
            gvIssuers.DataBind();

            return issuers;
        }
    }
}