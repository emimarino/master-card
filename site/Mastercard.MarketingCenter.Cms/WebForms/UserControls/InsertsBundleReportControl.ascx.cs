﻿using Mastercard.MarketingCenter.Services;
using System;
using System.Configuration;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.SharePoint.WebParts
{
    public partial class InsertsBundleReportControl : UserControl
    {
        InsertsBundleService _insertsBundleService { get { return DependencyResolver.Current.GetService<InsertsBundleService>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindLists();
            }
        }

        public void BindLists()
        {
            InsertsBundleListView.DataSource = _insertsBundleService.GetEnrollees();
            InsertsBundleListView.DataBind();
        }

        protected void InsertsBundleListView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var baseUrl = string.Concat(Request.Url.Scheme, System.Uri.SchemeDelimiter, Request.Url.Authority);

            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                Enrollee enrollee = (e.Item as ListViewDataItem).DataItem as Enrollee;
                var urlFormatString = string.Concat(baseUrl, ConfigurationManager.AppSettings["PortalUrl"], "/WebFormsPages/OrderDetailNoChrome.aspx?orderid={0}&Source=BundleReport");

                if (enrollee.Q1PromotionOrderId.HasValue)
                {
                    e.Item.FindControl("Q1PromotionNameLiteral").Visible = false;
                    e.Item.FindControl("Q1PromotionNameHref").Visible = true;
                    (e.Item.FindControl("Q1PromotionNameHref") as HtmlAnchor).HRef = string.Format(urlFormatString, enrollee.Q1PromotionOrderId.Value);
                }
                if (enrollee.Q2PromotionOrderId.HasValue)
                {
                    e.Item.FindControl("Q2PromotionNameLiteral").Visible = false;
                    e.Item.FindControl("Q2PromotionNameHref").Visible = true;
                    (e.Item.FindControl("Q1PromotionNameHref") as HtmlAnchor).HRef = string.Format(urlFormatString, enrollee.Q2PromotionOrderId.Value);
                }
                if (enrollee.Q3PromotionOrderId.HasValue)
                {
                    e.Item.FindControl("Q3PromotionNameLiteral").Visible = false;
                    e.Item.FindControl("Q3PromotionNameHref").Visible = true;
                    (e.Item.FindControl("Q1PromotionNameHref") as HtmlAnchor).HRef = string.Format(urlFormatString, enrollee.Q3PromotionOrderId.Value);
                }
                if (enrollee.Q4PromotionOrderId.HasValue)
                {
                    e.Item.FindControl("Q4PromotionNameLiteral").Visible = false;
                    e.Item.FindControl("Q4PromotionNameHref").Visible = true;
                    (e.Item.FindControl("Q1PromotionNameHref") as HtmlAnchor).HRef = string.Format(urlFormatString, enrollee.Q4PromotionOrderId.Value);
                }
            }
        }
    }
}