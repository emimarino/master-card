﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="UserProfile.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.UserControls.UserProfile" ValidateRequestMode="Enabled" %>

<div class="center_container width958">
    <div class="roundcont_pop" runat="server" id="divAccountInformation" visible="false">
        <div class="roundtop">
            <img src="/portal/inc/IMAGES/tl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
        </div>
        <p>
            <strong>Account Information</strong><br />
            <br />
            Password Has Not Yet been Created.<br />
            <br />
            This account cannot be accessed by the user until a password has been set. Please 
			invite the user to complete the account creation by sending them an email
			with a special link that will allow them to easily set the password. The link can be generated and
			emailed by clicking the button below. 
           
            <div class="bg_btn_left" style="margin-left: 14px; margin-bottom: 5px; background: url('/portal/inc/images/btn_bg_lg.gif') no-repeat left top;">
                <a id="copylink" runat="server" title="Send Link by Email" class="submit_bg_btn">Send Link by Email</a>
            </div>
            <div style="clear: both;"></div>
        </p>
        <div class="roundbottom">
            <img src="/portal/inc/IMAGES/bl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
        </div>
    </div>
    <br />
    <div class="form_container">
        <div class="orange_heading_profile">
            Profile
        </div>
        <p>
            Please complete your profile using the fields below.
        </p>
        <div class="roundcont_error" id="divError" runat="server" visible="false">
            <div class="roundtop">
                <img src="/portal/inc/images/tl.gif" alt="" width="15" height="15" class="corner"
                    style="display: none" />
            </div>
            <div style="float: left; width: 80px; text-align: center;">
                <img src="/portal/inc/images/error_img.gif" border="0px" alt="" width="40" height="40" />
            </div>
            <div style="float: left; width: 800px;">
                <asp:Label ID="lblError" CssClass="span_1" runat="server" Visible="False"></asp:Label>
            </div>
            <div style="clear: both;"></div>
            <div class="roundbottom">
                <img src="/portal/inc/images/bl.gif" alt="" width="15" height="15" class="corner"
                    style="display: none" />
            </div>
        </div>
        <div class="mrt_25">
            <div class="right_form">
                <div class="right_form_container">
                    <span>
                        <label>
                            Address 1<%= (string.IsNullOrEmpty(Region) || Region.Trim().Equals("us", StringComparison.InvariantCultureIgnoreCase) || Region.Trim().Equals("de", StringComparison.InvariantCultureIgnoreCase))?"*":"" %></label>
                        <asp:TextBox ID="txtAddress1" runat="server"></asp:TextBox></span>
                    <span class="label_margin">
                        <label>Address 2</label>
                        <asp:TextBox ID="txtAddress2" runat="server"></asp:TextBox></span>
                    <span class="label_margin">
                        <label>Address 3</label>
                        <asp:TextBox ID="txtAddress3" runat="server"></asp:TextBox></span>
                    <span class="label_margin">
                        <label>
                            City<%= (string.IsNullOrEmpty(Region) || Region.Trim().Equals("us", StringComparison.InvariantCultureIgnoreCase) || Region.Trim().Equals("de", StringComparison.InvariantCultureIgnoreCase))?"*":"" %></label>
                        <asp:TextBox ID="txtCity" runat="server"></asp:TextBox></span>

                    <%if (Region.Trim().Equals("us", StringComparison.InvariantCultureIgnoreCase))
                        { %>
                    <div class="form-group">
                        <label class="form-label">State<span class="req-mark">*</span></label>
                        <asp:DropDownList ID="ddlState" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <%} %>
                    <%else
                        {%>
                    <div class="form-group">
                        <label class="form-label">State / Province <%= (string.IsNullOrEmpty(Region) || Region.Trim().Equals("us", StringComparison.InvariantCultureIgnoreCase))?"*":"" %> </label>
                        <asp:TextBox ID="txtState" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <%} %>

                    <%if (ddlCountry.Items.Count > 2)
                        { %>
                    <div class="form-group">
                        <label class="form-label">Country<span class="req-mark">*</span></label>
                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <%} %>
                    <%else
                        {%>
                    <div class="form-group">
                        <label class="form-label">Country<span class="req-mark">*</span></label>
                        <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                    </div>
                    <%} %>

                    <%if (ddlLanguage.Items.Count > 2)
                        { %>
                    <div class="form-group">
                        <label class="form-label">Language<span class="req-mark">*</span></label>
                        <asp:DropDownList ID="ddlLanguage" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <%} %>

                    <span class="label_margin">
                        <label>
                            Zip Code<%= (string.IsNullOrEmpty(Region)||Region.Trim().Equals("us", StringComparison.InvariantCultureIgnoreCase) || Region.Trim().Equals("de", StringComparison.InvariantCultureIgnoreCase))?"*":"" %></label>
                        <asp:TextBox ID="txtZipCode" runat="server" CssClass="short_width"></asp:TextBox>
                    </span>
                    <div class="label_margin">
                        <label id="lblPassword" runat="server"></label>
                        <asp:Label runat="server" CssClass="email" ID="lblPasswordNA" Text="N/A" Visible="false"></asp:Label>
                        <div runat="server" id="divResetPassword" visible="true" class="div_reset_password">
                            <asp:UpdatePanel ID="panelResetPassword" UpdateMode="Conditional" runat="server">
                                <triggers>
                                    <asp:PostBackTrigger ControlID="btnRefreshSend" />
                                </triggers>
                                <contenttemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:HyperLink runat="server" ID="linkPassword" ClientIDMode="Static" Target="_blank" CssClass="email" Style="float: left; width: 326px; font: bold 11px/20px Verdana, Arial, Helvetica, sans-serif;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label runat="server" CssClass="email" ID="lblExpirationDate" Text="none"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="bg_btn_left" style="margin-bottom: 5px; cursor: pointer;">
                                                    <a class="submit_bg_btn" title="Copy Reset Password URL to Clipboard" onclick="copyToClipboard('#linkPassword')">Copy URL</a>
                                                    <script>
                                                        function copyToClipboard(element) {
                                                            var $temp = $("<input>");
                                                            $("body").append($temp);
                                                            $temp.val($(element).attr('href')).select();
                                                            document.execCommand("copy");
                                                            $temp.remove();
                                                        }
                                                    </script>
                                                </div>
                                                <div class="bg_btn_left" style="margin-left: 15px; margin-bottom: 5px; cursor: pointer;">
                                                    <asp:LinkButton ID="btnRefreshSend" OnClick="btnRefreshSend_Click" CssClass="submit_bg_btn" Text="Refresh & Send" runat="server" title="Refresh Reset Password URL & Send by Email" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </contenttemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="clr"></div>
                    <asp:PlaceHolder runat="server" ID="placeHolderAdmin" Visible="false">
                        <div class="flt_mot label_margin">
                            <label>Admin Access</label>
                            <asp:Label CssClass="email" runat="server" ID="lbladminAccess" Text="none" Style="width: 60%;"></asp:Label>
                            <asp:HyperLink runat="server" CssClass="pad_right price email" ID="linkEdit" Text="Edit" Target="_self" />
                        </div>
                        <div class="clr"></div>
                    </asp:PlaceHolder>
                    <span class="label_margin">
                        <label>Blocked</label>
                        <asp:CheckBox runat="server" ID="chkIsBlocked"></asp:CheckBox>
                    </span>
                </div>
            </div>
            <div class="left_formnew">
                <span>
                    <label>
                        First Name*</label><asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></span>
                <span class="label_margin">
                    <label>
                        Last Name*</label><asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></span>
                <span class="label_margin">
                    <label>
                        Title*</label><asp:TextBox ID="txtTitle" runat="server"></asp:TextBox></span>
                <span class="label_margin">
                    <label>
                        Company&nbsp;Name*</label><asp:TextBox ID="txtIssuerName" runat="server"></asp:TextBox></span>
                <span class="label_margin">
                    <label>
                        Processor</label><asp:TextBox ID="txtProcessor" runat="server"></asp:TextBox>
                </span><span class="label_margin">
                    <label>
                        Phone*</label>
                    <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                </span><span class="label_margin">
                    <label>
                        Mobile</label>
                    <asp:TextBox ID="txtMobile" runat="server"></asp:TextBox></span>
                <% if (this.Region != null ? (this.Region.Trim().Equals("us", StringComparison.InvariantCultureIgnoreCase)) : true)
                    {%>
                <span class="label_margin">
                    <label>
                        Fax</label>
                    <asp:TextBox ID="txtFax" runat="server"></asp:TextBox></span>
                <% } %>
                <span class="label_margin">
                    <label>
                        Email Address*</label>
                    <asp:Label runat="server" CssClass="email" ID="lblEmail"></asp:Label>
                </span>

                <div class="clr"></div>
                <span class="label_margin" runat="server" id="assignIcaField">
                    <label>
                        Financial Inst. ICA #</label>
                    <asp:Label runat="server" CssClass="email" ID="lblIssuerICA" Visible="false"></asp:Label>
                    <asp:HyperLink runat="server" CssClass="price email" ID="lnkIssuerICA"></asp:HyperLink>
                </span>
            </div>
        </div>
        <div class="clr">
        </div>
        <div class="navright_form_container">
            <div style="float: right; margin-top: 5px;">
                <div class="bg_btn_left">
                    <asp:LinkButton ID="btnSubmit" CssClass="submit_bg_btn" OnClick="btnSubmit_Click"
                        Text="Submit" runat="server" />
                </div>
                <div class="bg_btn_left m-left">
                    <asp:LinkButton ID="btnCancel" CssClass="submit_bg_btn" OnClick="btnCancel_Click" Text="Cancel" runat="server" />
                </div>
            </div>
            <span class="label_margin requried fl">* Required field</span>
        </div>
        <div class="clr">
        </div>
    </div>
    <div id="orderdiv" runat="server">
        <div class="main_orng_heading">
            Order History for this user
       
       
        </div>
        <div class="mrt_25 fl" id="divNoOrder" runat="server" visible="false">
            <p>
                <span>User has not placed any orders yet</span>
            </p>
        </div>
        <div class="grid_container" runat="server" id="divOrderHistory">
            <asp:Repeater ID="orderHistory" OnItemDataBound="orderHistory_ItemBound" runat="server">
                <HeaderTemplate>
                    <div class="grid_top_head">
                        <span class="grid_heading">Date</span>
                        <span class="grid_heading">Order #</span>
                        <span class="grid_heading wida80px">Items</span>
                        <span class="grid_heading">Price</span>
                        <span class="grid_heading">Status</span>
                    </div>
                    <div class="grid_top_bg">
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="white_row">
                        <span class="data"><%# Convert.ToDateTime(Eval("OrderDate").ToString()).ToString("MM/dd/yyyy")%></span>
                        <span class="data ">
                            <% if (!string.IsNullOrEmpty((string)ViewState["adminRole"]))
                                { %>
                            <a id="OrderLink" href="#" class="price" target="_top" runat="server">
                                <%# Eval("OrderNumber").ToString()%>
                            </a>
                            <%}
                                else
                                {%>
                            <label id="OrderLinkL" class="price" target="_top" runat="server">
                                <%# Eval("OrderNumber").ToString()%>
                            </label>
                            <%} %>
                        </span>
                        <span class="data wida80px"><%# Eval("Items").ToString()%></span>
                        <span class="data pdleft40px">$<%# Eval("Price").ToString()%></span><span class="data"><%# Eval("OrderStatus").ToString()%></span>
                    </div>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <div class="white_row gray_backgrnd">
                        <span class="data"><%# Convert.ToDateTime(Eval("OrderDate").ToString()).ToString("MM/dd/yyyy")%></span>
                        <span class="data price">
                            <% if (!string.IsNullOrEmpty((string)ViewState["adminRole"]))
                                { %>
                            <a id="OrderLink" href="#" class="price" target="_top" runat="server">
                                <%# Eval("OrderNumber").ToString()%>
                            </a>
                            <%}
                                else
                                {%>
                            <label id="OrderLinkL" class="price" target="_top" runat="server">
                                <%# Eval("OrderNumber").ToString()%>
                            </label>
                            <%} %>
                        </span>
                        <span class="data wida80px"><%# Eval("Items").ToString()%></span>
                        <span class="data pdleft40px">$<%# Eval("Price").ToString()%></span><span class="data"><%# Eval("OrderStatus").ToString()%></span>
                    </div>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </div>
                   
                    <div class="grid_top_head_bot"></div>
                    <div class="pd_bot">&nbsp;</div>
                    </div>               
               
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>
