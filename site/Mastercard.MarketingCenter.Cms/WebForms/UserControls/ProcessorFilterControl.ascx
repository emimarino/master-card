﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProcessorFilterControl.ascx.cs" Inherits="Mastercard.MarketingCenter.Cms.WebForms.UserControls.ProcessorFilterControl" %>

<link href="../Content/legacy/css/MarketingCenter.SharedControls.css" type="text/css" rel="stylesheet" />

<div id="container" runat="server" class="center_container">
    <div class="form_container processorFilter">
        <div class="contact_container mrt">
            <div class="contact_heading">
                <div class="orange_heading_main">Filter by Processor</div>
            </div>
            <div class="contact_heading_bg">
                <asp:DropDownList ID="ddlProcessorAdminOptions" runat="server" AutoPostBack="true" onselectedindexchanged="ddlProcessorAdminOptions_SelectedIndexChanged"></asp:DropDownList>
            </div>
            <div class="contact_heading_bot"></div>
        </div>
    </div>
</div>
