﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services;
using System;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Cms.WebForms.UserControls
{
    public partial class ProcessorFilterControl : UserControl
    {
        private bool? _isUserAdmin;
        private ProcessorService _processorService;

        protected ProcessorService ProcessorService { get { if (_processorService == null) { _processorService = DependencyResolver.Current.GetService<ProcessorService>(); } return _processorService; } }
        public bool IsUserAdmin
        {
            get
            {
                if (_isUserAdmin == null)
                {
                    _isUserAdmin = Context.User.IsInRole(Constants.Roles.McAdmin) || Context.User.IsInRole(Constants.Roles.DevAdmin);
                }

                return _isUserAdmin.Value;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ddlProcessorAdminOptions.SelectedIndexChanged += new EventHandler(ddlProcessorAdminOptions_SelectedIndexChanged);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsUserAdmin)
            {
                if (!this.IsPostBack)
                {
                    ddlProcessorAdminOptions.DataSource = this.ProcessorService.GetProcessors();
                    ddlProcessorAdminOptions.DataTextField = "Title";
                    ddlProcessorAdminOptions.DataValueField = "ProcessorID";
                    ddlProcessorAdminOptions.DataBind();

                    if (Request.QueryString["p"] == null)
                    {
                        string url = Request.RawUrl;
                        if (url.Contains("?"))
                        {
                            url = url + "&p=" + ddlProcessorAdminOptions.SelectedValue;
                        }
                        else
                        {
                            url = url + "?p=" + ddlProcessorAdminOptions.SelectedValue;
                        }
                        Response.Redirect(url, true);
                    }
                    else
                    {
                        ListItem item = ddlProcessorAdminOptions.Items.FindByValue(Request.QueryString["p"]);
                        if (item != null)
                        {
                            item.Selected = true;
                        }
                    }
                }
            }
            else
            {
                container.Visible = false;
            }
        }

        protected void ddlProcessorAdminOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            string url = Request.RawUrl;
            url = url.Replace(url.Substring(url.IndexOf("p=") + 2), ddlProcessorAdminOptions.SelectedValue);
            Response.Redirect(url, true);
        }
    }
}