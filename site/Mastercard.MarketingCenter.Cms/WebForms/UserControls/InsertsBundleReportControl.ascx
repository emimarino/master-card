﻿<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>

<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InsertsBundleReportControl.ascx.cs" Inherits="Mastercard.MarketingCenter.SharePoint.WebParts.InsertsBundleReportControl" %>

<style type="text/css">
	.ms-WPBody {min-height:650px;}
</style>
<div class="orange_heading_profile">Inserts Bundle Report</div>

<br />
<asp:ListView ID="InsertsBundleListView" ItemPlaceholderID="content" OnItemDataBound="InsertsBundleListView_ItemDataBound" runat="server">
	<LayoutTemplate>
		<div class="grid_container">
			<div class="grid_top_head">
				<span class="grid_heading" style="width:225px;">WHO</span>
				<span class="grid_heading" style="width:180px;">WHAT</span>
				<span class="grid_heading" style="width:90px;">WHEN</span>
				<span class="grid_heading" style="width:90px;">QTY</span>
				<span class="grid_heading" style="width:150px;">WHERE</span>
				<span class="grid_heading" style="width:150px;">COST</span>
			</div>
			<div class="grid_top_bg">
				<div id="content" runat="server"></div>
			</div>                
		</div>
		<div class="grid_top_head_bot"/>
		<div class="pd_bot"> </div>
	</LayoutTemplate>
	<ItemTemplate>
		<div class="white_row">
			<span class="data" style="width:225px;">
				<%# Eval("Issuer.Title") %> <br />
				<%# Eval("CustomersName") %> <br />
				<%# Eval("CustomersEmail") %>
			</span>
			<span class="data" style="width:180px;">
				<asp:Literal ID="Q1PromotionNameLiteral" Text='<%# "Q1-" + Eval("Q1PromotionName").ToString() %>' runat="server" />
				<a ID="Q1PromotionNameHref" href="" Visible="false" runat="server"><%# "Q1-" + Eval("Q1PromotionName").ToString() %></a>
				<br />
				<asp:Literal ID="Q2PromotionNameLiteral" Text='<%# "Q2-" + Eval("Q2PromotionName").ToString() %>' runat="server" />
				<a ID="Q2PromotionNameHref" href="/mastercard-admin/Web Part Pages/OrderDetail.aspx?orderid=" Visible="false" runat="server"><%# "Q2-" + Eval("Q2PromotionName").ToString() %></a>
				<br />
				<asp:Literal ID="Q3PromotionNameLiteral" Text='<%# "Q3-" + Eval("Q3PromotionName").ToString() %>' runat="server" />
				<a ID="Q3PromotionNameHref" href="/mastercard-admin/Web Part Pages/OrderDetail.aspx?orderid=" Visible="false" runat="server"><%# "Q3-" + Eval("Q3PromotionName").ToString() %></a>
				<br />
				<asp:Literal ID="Q4PromotionNameLiteral" Text='<%# "Q4-" + Eval("Q4PromotionName").ToString() %>' runat="server" />
				<a ID="Q4PromotionNameHref" href="/mastercard-admin/Web Part Pages/OrderDetail.aspx?orderid=" Visible="false" runat="server"><%# "Q4-" + Eval("Q4PromotionName").ToString() %></a>
				<br />
			</span>
			<span class="data" style="width:90px;"><%# string.Format("{0:d}", Eval("Created")) %></span>
			<span class="data" style="width:90px;">
				Q1-<%# Eval("Q1NumUnits")%> units <br />
				Q2-<%# Eval("Q2NumUnits")%> units <br />
				Q3-<%# Eval("Q3NumUnits")%> units <br />
				Q4-<%# Eval("Q4NumUnits")%> units
			</span>
			<span class="data" style="width:150px;">
				<%# Eval("ShippingAddress") %> <br /><br />
				<%# Eval("ContactName") %> <br />
				<%# Eval("ContactPhoneNumber")%>
			</span>
			<span class="data" style="width:150px;">
				<div id="WithoutAllowanceCostsDiv" runat="server">
					Total order cost <br />
					<%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "{0:c}", Eval("TotalEnrollmentCost"))%>
				</div>
				<div id="WithAllowanceCostsDiv" visible="false" runat="server">
					Allowance <br />
					<%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "{0:c}", 0)%><br />
					Order cost <br />
					<%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "{0:c}", Eval("TotalEnrollmentCost"))%><br />
					Unused <br />
					<%# string.Format(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "{0:c}", Eval("AllowanceUnused")) %>
				</div>
			</span>
		</div>
	</ItemTemplate>    
</asp:ListView>

<div style="clear:both;"></div>
<br /><br />
<div class="orange_heading_profile">Financial Institutions With Funding and Not Enrolled</div>
<br /><br />
<asp:ListView ID="FinancialInstitutionsWithFundingNotEnrolledListView" ItemPlaceholderID="content" runat="server">
	<LayoutTemplate>
		<div class="grid_container">
			<div class="grid_top_head">
				<span class="grid_heading" style="width:250px;">FINANCIAL INSTITUTION</span>
				<span class="grid_heading" style="width:100px;">ALLOWANCE</span>
			</div>
			<div class="grid_top_bg">
				<div id="content" runat="server"></div>
			</div>
		</div>
		<div class="grid_top_head_bot"/>
		<div class="pd_bot"> </div>
	</LayoutTemplate>
	<ItemTemplate>
		<div class="white_row">
			<span class="data" style="width:250px;"><%# Eval("Title") %></span>
			<span class="data" style="width:100px;">0</span>
		</div>
	</ItemTemplate>    
</asp:ListView>

