﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Common;
using Slam.Cms.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.UserControls
{
    public partial class UserProfile : UserControl
    {
        #region Properties

        private string _region;
        public string Region
        {
            get
            {
                if (string.IsNullOrEmpty(_region))
                {
                    _region = User.Region.IsNullOrEmpty() ? _userContext?.SelectedRegion?.Trim() ?? RegionalizeService.DefaultRegion : User.Region;
                }

                return _region.Trim();
            }
        }

        private string _language;
        public string Language
        {
            get
            {
                if (string.IsNullOrEmpty(_language))
                {
                    _language = User.Language.IsNullOrEmpty() ? _userContext?.SelectedLanguage?.Trim() ?? RegionalizeService.DefaultLanguage : User.Language;
                }

                return _language.Trim();
            }
        }

        private string _userName;
        public string UserName
        {
            get
            {
                if (string.IsNullOrEmpty(_userName))
                {
                    if (Request.QueryString["loginid"] != null)
                    {
                        _userName = Request.QueryString["loginid"];
                        if (_userName.IndexOf(',') > 0 && _userName.Split(',').Length > 1)
                        {
                            _userName = _userName.Split(',')[0];
                        }
                    }
                    else
                    {
                        _userName = _userContext.User.UserName;
                    }
                }

                return _userName;
            }
        }

        private string _membershipUserName;
        public string MembershipUserName
        {
            get
            {
                if (string.IsNullOrEmpty(_membershipUserName))
                {
                    _membershipUserName = User.UserName;
                }

                return _membershipUserName;
            }
        }

        private Data.Entities.User _user;
        public Data.Entities.User User
        {
            get
            {
                if (_user == null)
                {
                    _user = _userService.GetUserByUserName(UserName);
                }

                return _user;
            }
        }

        private MembershipUser _membershipUser;
        public MembershipUser MembershipUser
        {
            get
            {
                if (_membershipUser == null)
                {
                    _membershipUser = MembershipService.GetUser(UserName);
                }

                return _membershipUser;
            }
        }

        private CultureInfo _userCultureInfo;
        private CultureInfo UserCultureInfo
        {
            get
            {
                if (_userCultureInfo == null)
                {
                    _userCultureInfo = new CultureInfo(RegionalizeService.GetCulture(User.Region, User.Language));
                }

                return _userCultureInfo;
            }
        }

        private string ResetPasswordSubject
        {
            get
            {
                return Resources.Emails.ResourceManager.GetString("ResetPasswordSubject", UserCultureInfo);
            }
        }

        private string ResetPasswordBody
        {
            get
            {
                return Resources.Emails.ResourceManager.GetString("ResetPasswordBody", UserCultureInfo);
            }
        }

        #endregion

        #region Services

        protected IUnitOfWork _unitOfWork { get { return DependencyResolver.Current.GetService<IUnitOfWork>(); } }
        public UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
        public IUserService _userService { get { return DependencyResolver.Current.GetService<IUserService>(); } }
        public IIssuerService _issuerService { get { return DependencyResolver.Current.GetService<IIssuerService>(); } }
        public IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }
        public ITagService _tagService { get { return DependencyResolver.Current.GetService<ITagService>(); } }
        public IUserKeyService _userKeyService { get { return DependencyResolver.Current.GetService<IUserKeyService>(); } }
        public ISettingsService _settingsService { get { return DependencyResolver.Current.GetService<ISettingsService>(); } }
        public IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }
        public IAuthorizationService _authorizationService { get { return DependencyResolver.Current.GetService<IAuthorizationService>(); } }
        public IRegionService _regionService { get { return DependencyResolver.Current.GetService<IRegionService>(); } }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(_userContext.SelectedRegion?.Trim()?.Equals((User.Region?.Trim() ?? ""), StringComparison.OrdinalIgnoreCase) ?? false))
            {
                Response.ClearContent();
                Response.Write("<h1 style=\"color:#f58025;\">You are not authorized</h1>");
                Response.End();
            }

            if (!IsPostBack)
            {
                HookupCancelButton();
                FillStates();

                if (Request.QueryString["loginid"] != null)
                {
                    if (Context.User.IsInAdminRole())
                    {
                        SetCommonInformation();
                        AdminInformation();
                        GetOrderItemsHistory(MembershipUserName);

                        orderdiv.Visible = true;
                        lblPassword.InnerText = "Reset Password URL";

                        var userKey = _userKeyService.GetByUserNameAndStatus(MembershipUserName, MarketingCenterDbConstants.UserKeyStatus.Available);
                        if (userKey == null)
                        {
                            lblPasswordNA.Text = "N/A";
                            lblPasswordNA.Visible = true;
                            divResetPassword.Visible = false;
                        }
                        else
                        {
                            SetResetPasswordLabels(userKey);
                        }

                        if (Context.User.IsInRole(Constants.Roles.DevAdmin) || Context.User.IsInRole(Constants.Roles.McAdmin))
                        {
                            placeHolderAdmin.Visible = true;
                            SetAdminAccess();
                            ViewState["adminRole"] = Context.User.GetUserRoleName();
                        }
                    }
                    else
                    {
                        Response.Redirect(_urlService.GetInvalidLinkURL());
                    }
                }
                else
                {
                    SetCommonInformation();

                    lblPassword.InnerText = "Password";
                    linkPassword.Text = "Change Password";
                    linkPassword.NavigateUrl = _urlService.GetChangePasswordURL();

                    divAccountInformation.Visible = false;
                    orderdiv.Visible = false;
                }
            }
        }

        private string SetResetPasswordLabels(Data.Entities.UserKey userKey)
        {
            var resetPasswordUrl = _urlService.GetResetPasswordURL(userKey.Key);
            linkPassword.NavigateUrl = resetPasswordUrl;
            linkPassword.ToolTip = resetPasswordUrl;

            var expirationDate = userKey.DateKeyGenerated.AddHours(_settingsService.GetForgotPasswordExpirationTime());
            var expirationDateDifference = DateTime.Now - expirationDate;
            var days = Math.Abs(expirationDateDifference.Days);
            var hours = Math.Abs(expirationDateDifference.Hours);
            var minutes = Math.Abs(expirationDateDifference.Minutes);
            var expirationDateLabel = $@"{(days > 0 ? $"{days} days, " : string.Empty)}
                                         {(hours > 0 ? $"{hours} hours, " : string.Empty)}
                                         {(minutes > 0 ? $"{minutes} minutes" : string.Empty)}
                                         {(days == 0 && hours == 0 && minutes == 0 ? $"a moment" : string.Empty)}"
                                      .Trim()
                                      .Trim(',');

            if (expirationDate < DateTime.Now)
            {
                linkPassword.Text = "This link is expired";
                linkPassword.CssClass = "email error";
                lblExpirationDate.Text = $"Expired {expirationDateLabel} ago";
            }
            else
            {
                linkPassword.Text = resetPasswordUrl;
                linkPassword.CssClass = "email";
                lblExpirationDate.Text = $"Expiring in {expirationDateLabel}";
            }

            return resetPasswordUrl;
        }

        private void SetAdminAccess()
        {
            var userRole = _authorizationService.GetUserRole(User.UserId);
            lbladminAccess.Text = $"{userRole.Description}{(userRole.ShowRegions ? $" ({string.Join(", ", userRole.Regions.Select(r => r.Name))})" : string.Empty)}";

            string referrer = $"&Source={(HttpUtility.UrlEncode(Request.QueryString["TopSource"] != null ? Request.QueryString["TopSource"] : Page.Request.Url.AbsoluteUri))}";
            linkEdit.NavigateUrl = _urlService.GetProfileAccessAdminURL(UserName, referrer);
        }

        protected void GetOrderItemsHistory(string userId)
        {
            List<RetrieveOrderHistoryResult> orderItems = _orderService.GetOrderHistory(userId);
            if (orderItems != null && orderItems.Count > 0)
            {
                orderHistory.DataSource = orderItems;
                orderHistory.DataBind();
                divNoOrder.Visible = false;
                divOrderHistory.Visible = true;
            }
            else
            {
                divNoOrder.Visible = true;
                divOrderHistory.Visible = false;
            }
        }

        protected void FillStates()
        {
            ddlState.DataSource = _regionService.GetStates().ToList();
            ddlState.DataTextField = "StateName";
            ddlState.DataValueField = "StateID";
            ddlState.DataBind();
            ddlState.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        protected void FillCountries()
        {
            ddlCountry.DataSource = _tagService.GetCountries(User.Region, _userContext.SelectedLanguage).OrderBy(c => c.Value);
            ddlCountry.DataTextField = "Value";
            ddlCountry.DataValueField = "Key";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        protected void FillLanguages()
        {
            var categoryChildren = (new RegionConfigManager()).GetLanguages(User.Region);

            ddlLanguage.DataSource = categoryChildren;
            ddlLanguage.DataTextField = "Value";
            ddlLanguage.DataValueField = "Key";
            ddlLanguage.DataBind();
            if (ddlLanguage.Items.Count > 1) //only default value when select is available
            {
                ddlLanguage.Items.Insert(0, new ListItem("--Select--", "0"));
            }
        }

        protected void AdminInformation()
        {
            if (MembershipUser != null)
            {
                if (!MembershipUser.IsApproved)
                {
                    divAccountInformation.Visible = true;
                    copylink.HRef = $@"mailto:{User.Email}?subject={Server.HtmlEncode("Problems getting the verification email")}&body={_urlService.GetCreateAccountURL(MembershipUser.UserName)}";
                }
                else
                {
                    divAccountInformation.Visible = false;
                }
            }
        }

        protected void SetCommonInformation()
        {
            FillCountries();
            FillLanguages();

            if (ddlCountry.Items.Count > 2)
            {
                ddlCountry.SelectedValue = User.Country;
            }
            else
            {
                txtCountry.Text = ddlCountry.Items.FindByValue(User.Country ?? string.Empty)?.ToString() ??
                                  ddlCountry.Items.FindByValue(User.Country?.ToLower() ?? string.Empty)?.ToString() ??
                                  ddlCountry.Items.FindByValue(User.Country?.ToUpper() ?? string.Empty)?.ToString();
            }

            ddlLanguage.SelectedValue = User.Language;
            txtFirstName.Text = User.Profile.FirstName;
            txtLastName.Text = User.Profile.LastName;
            txtIssuerName.Text = User.Profile.IssuerName;
            txtAddress1.Text = User.Profile.Address1;
            txtAddress2.Text = User.Profile.Address2;
            txtAddress3.Text = User.Profile.Address3;

            if (UserBelongsToRegion("us"))
            {
                ddlState.SelectedValue = ddlState.Items.FindByText(User.Profile.State ?? string.Empty)?.Value ??
                                         ddlState.Items.FindByValue(User.Profile.State ?? string.Empty)?.Value ??
                                         string.Empty;
            }
            else
            {
                txtState.Text = User.Profile.State ?? string.Empty;
            }

            txtZipCode.Text = User.Profile.Zip;
            txtPhone.Text = User.Profile.Phone;
            txtFax.Text = User.Profile.Fax;
            txtMobile.Text = User.Profile.Mobile;
            lblEmail.Text = MembershipUser.Email;
            txtCity.Text = User.Profile.City;
            txtTitle.Text = User.Profile.Title;
            txtProcessor.Text = User.Profile.Processor;
            chkIsBlocked.Checked = User.IsBlocked;

            lblIssuerICA.Text = "Unassigned";
            lnkIssuerICA.Text = "Unassigned";
            assignIcaField.Visible = Page.User.IsInPermission(Constants.Permissions.CanManageIssuerMatcher);

            if (User.Issuer != null)
            {
                if (!string.IsNullOrEmpty(User.Issuer.BillingId))
                {
                    lblIssuerICA.Text = User.Issuer.BillingId;
                    lnkIssuerICA.Text = User.Issuer.BillingId;
                }
                else
                {
                    lblIssuerICA.Text = "Unassigned";
                    lnkIssuerICA.Text = "Unassigned";
                }

                string referrer = $"&Source={(HttpUtility.UrlEncode(Request.QueryString["TopSource"] != null ? Request.QueryString["TopSource"] : Page.Request.Url.AbsoluteUri))}";
                lnkIssuerICA.NavigateUrl = _urlService.GetListEditURL("3", User.Issuer.IssuerId, referrer);
                lnkIssuerICA.Target = "_top";
            }

            if (Request.QueryString["loginid"] != null)
            {
                if (Page.User.IsInRole(Constants.Roles.McAdmin) || Page.User.IsInRole(Constants.Roles.DevAdmin))
                {
                    lnkIssuerICA.Visible = true;
                    lblIssuerICA.Visible = false;
                }
                else
                {
                    lnkIssuerICA.Visible = false;
                    lblIssuerICA.Visible = true;
                }
            }
            else
            {
                lnkIssuerICA.Visible = false;
                lblIssuerICA.Visible = true;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {
                User.Country = ddlCountry.SelectedValue.Length > 2 ? ddlCountry.SelectedValue.Trim() : User.Country;
                User.Language = UserBelongsToRegion("us") ? RegionalizeService.DefaultLanguage :
                                ddlLanguage.Items.Count > 1 ? ddlLanguage.SelectedValue.Trim() : User.Language ?? RegionalizeService.DefaultLanguage;

                User.Profile.FirstName = txtFirstName.Text.Trim();
                User.Profile.LastName = txtLastName.Text.Trim();
                User.Profile.IssuerName = txtIssuerName.Text.Trim();

                User.Region = User.Region;
                User.Profile.Address1 = txtAddress1.Text.Trim();
                User.Profile.Address2 = txtAddress2.Text.Trim();
                User.Profile.Address3 = txtAddress3.Text.Trim();
                User.Profile.City = txtCity.Text.Trim();
                User.Profile.State = UserBelongsToRegion("us") ? ddlState.SelectedItem.Text.Trim() : txtState.Text.Trim();
                User.Profile.Zip = txtZipCode.Text.Trim();
                User.Profile.Phone = txtPhone.Text.Trim();
                User.Profile.Mobile = txtMobile.Text.Trim();
                User.Profile.Fax = txtFax.Text.Trim();
                User.Profile.Title = txtTitle.Text.Trim();
                User.Profile.Processor = txtProcessor.Text.Trim();
                User.IsBlocked = chkIsBlocked.Checked;

                _userService.UpdateUser(User);
                _unitOfWork.Commit();

                Response.Clear();
                Response.Write($@"  <html>
                                        <head>
                                            <script type='text/javascript'>
                                                window.top.location.href = '{GetRedirectUrl()}';
                                            </script>
                                        </head>
                                        <body></body>
                                    </html>"
                               );
                Response.End();
            }
            else
            {
                divError.Visible = true;
                lblError.Visible = true;
            }
        }

        public bool UserBelongsToRegion(string region)
        {
            if (string.IsNullOrEmpty(region))
            {
                return false;
            }

            return User.Region.Trim().Equals(region.Trim(), StringComparison.InvariantCultureIgnoreCase);
        }

        private bool ValidateForm()
        {
            lblError.Text = string.Empty;
            var errors = new List<string>();

            if (txtFirstName.Text.Trim().Length == 0)
            {
                errors.Add("Please enter First Name.");
            }

            if (txtLastName.Text.Trim().Length == 0)
            {
                errors.Add("Please enter Last Name.");
            }

            if (txtTitle.Text.Trim().Length == 0)
            {
                errors.Add("Please enter Title.");
            }

            if (txtIssuerName.Text.Trim().Length == 0)
            {
                errors.Add("Please enter Issuer Name.");
            }

            if (txtAddress1.Text.Trim().Length == 0 && (UserBelongsToRegion("us") || UserBelongsToRegion("de")))
            {
                errors.Add("Please enter Address1.");
            }

            if (txtCity.Text.Trim().Length == 0 && (UserBelongsToRegion("us") || UserBelongsToRegion("de")))
            {
                errors.Add("Please enter City.");
            }

            if (ddlState.SelectedIndex == 0 && UserBelongsToRegion("us"))
            {
                errors.Add("Please select State.");
            }

            if (((ddlCountry.SelectedValue.Trim().Length == 0) || (ddlCountry.SelectedValue == "0")) && ddlCountry.Items.Count > 2)
            {
                errors.Add("Please select Country.");
            }

            if (((ddlLanguage.SelectedValue.Trim().Length == 0) || (ddlLanguage.SelectedValue == "0")) && ddlLanguage.Items.Count > 2)
            {
                errors.Add("Please select Language.");
            }

            if (txtZipCode.Text.Trim().Length == 0 && (UserBelongsToRegion("us") || UserBelongsToRegion("de")))
            {
                errors.Add("Please enter Zip Code.");
            }

            if (txtPhone.Text.Trim().Length == 0)
            {
                errors.Add("Please enter Phone No.");
            }

            if (errors.Any())
            {
                errors.ForEach(error => lblError.Text += error + "<br/>");
                return false;
            }

            lblError.Text = string.Empty;
            return true;
        }

        protected void HookupCancelButton()
        {
            btnCancel.OnClientClick = $"window.top.location.href = '{GetRedirectUrl()}'; return false;";
        }

        protected string GetRedirectUrl()
        {
            if (Request.QueryString["Source"] != null)
            {
                return Request.QueryString["Source"];
            }

            if (Request.QueryString["loginid"] != null && Request.QueryString["loginid"].IndexOf(',') > 0 && Request.QueryString["loginid"].Split(',').Length > 1)
            {
                return Request.QueryString["loginid"].Split(',')[1];
            }

            return _urlService.GetFrontEndHomeURL();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(GetRedirectUrl());
        }

        protected void orderHistory_ItemBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var item = e.Item.DataItem as RetrieveOrderHistoryResult;
                (e.Item.FindControl("OrderLink") as HtmlAnchor).HRef = _urlService.GetOrderDetailsURL(item.OrderNumber);
            }
        }

        protected void btnRefreshSend_Click(object sender, EventArgs e)
        {
            var userKey = _userKeyService.CreateUserKey(User.UserId);
            var resetPasswordUrl = SetResetPasswordLabels(userKey);
            panelResetPassword.Update();

            string subject = Server.HtmlEncode(ResetPasswordSubject);
            string body = Server.HtmlEncode(ResetPasswordBody.Replace("[#FullName]", $"{User.FullName}")
                                                             .Replace("[#ResetPasswordUrl]", resetPasswordUrl)
                                                             .Replace("[#ExpirationTime]", _settingsService.GetForgotPasswordExpirationTime().ToString()));
            var mailto = $@"function close(mailto) {"{"}
                                setTimeout(function(){"{"}
                                    mailto.close();
                                {"}"}, 1);
                            {"}"}
                            const mailto = function(){"{"}
                                var url = ""mailto:{MembershipUser.Email}?subject={subject}&body={body}"";
                                var mailto = window.open(url);
                                close(mailto);
                            {"}"};
                            $(document).ready(function(){"{"}
                                mailto();
                            {"}"});";

            Page.ClientScript.RegisterStartupScript(GetType(), Guid.NewGuid().ToString(), mailto, true);
        }
    }
}