﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BillingReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Cms.WebForms.BillingReport" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head runat="server">
    <link href="../Content/legacy/css/MarketingCenter.SharedControls.css" type="text/css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="center_container">
                <div class="form_container">
                    <div class="grid_billing_container">
                        <div class="orange_heading_profile">
                            <asp:Label ID="lblHeading" runat="server"></asp:Label>
                        </div>
                        <asp:Repeater ID="rptOrderResult" runat="server">
                            <HeaderTemplate>
                                <div class="main_heading mrt_25">
                                    <span class="grid_heading" style="width: 65px">Order #</span>
                                    <span class="grid_heading" style="width: 75px">Date</span>
                                    <span class="grid_heading" style="width: 85px">Name</span>
                                    <span class="grid_heading" style="width: 130px">Issuer</span>
                                    <span class="grid_heading" style="width: 175px">Processor</span>
                                    <span class="grid_heading" style="width: 85px">Total</span>
                                </div>
                                <div class="main_heading_bg">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="white_row" id="divrow" runat="server">
                                    <span class="data" style="width: 65px">
                                        <asp:Label ID="lblordno" Text='<%# DataBinder.Eval(Container.DataItem, "Orderno")%>' runat="server" />&nbsp; </span>
                                    <span class="data" style="width: 75px"><%# Eval("OrderDate")%> &nbsp;</span>
                                    <span class="data" style="width: 85px"><%# Eval("Name")%>&nbsp; </span>
                                    <span class="data" style="width: 130px" runat="server" id="spnissuer"><%# Eval("Issuer")%>&nbsp; </span>
                                    <span class="data" style="width: 175px" runat="server" id="spnprocessor"><%# Eval("Processor")%>&nbsp; </span>
                                    <span class="data text_right" style="width: 85px" runat="server" id="spntotal">
                                        <div class="float_left">$</div>
                                        <%# FormatNumber(Convert.ToDecimal(Eval("Total").ToString()))%>
                                    </span>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <div class="white_row" style="width: 90%">
                            <asp:Label ID="lblAllTotal" CssClass="totalbold widthauto fr" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="main_heading_bot">
                    </div>
                    <div class="pd_bot">
                        &nbsp;
                    </div>
                    <div>* Indicates that the order was partially completed during this period.</div>
                    <div>** Indicates that the order was partially completed in a previous month and is completed during this period.</div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
