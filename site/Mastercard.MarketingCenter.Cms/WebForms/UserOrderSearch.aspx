﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserOrderSearch.aspx.cs" Inherits="Mastercard.MarketingCenter.Cms.WebForms.UserOrderSearch" %>

<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>

<link href="../Content/legacy/css/jquery.flexbox.css" type="text/css" rel="stylesheet" />
<link href="../Content/legacy/css/MarketingCenter.SharedControls.css" type="text/css" rel="stylesheet" />


<script type="text/javascript" src="../Content/legacy/js/jquery-1.6.2.min.js"></script>
<script type="text/javascript" src="../Content/legacy/js/jquery.flexbox.min.js"></script>
<script type="text/javascript" src="../Content/legacy/js/search.common.js"></script>

<style>
    .dates {
        width: 88px !important;
    }
</style>

<script type="text/javascript">
    $().ready(function () {
        var filterProcessorQueryValue = '<%= Convert.ToString(GetProcessorFilter())%>';
        var resultsUrl = '<%= GetResultsUrl()%>';

        $('#ffb1').flexbox(resultsUrl + 'FlexResults?' + filterProcessorQueryValue + 'SearchType=username', {
            autoCompleteFirstMatch: true,
            resultTemplate: '<div>{name}</div>',
            showArrow: false,
            paging: false,
            width: 250,
            onSelect: function () {
                $('#hid_nameresults').html(this.value);
            }
        });

        $('#ffb1_input').blur(function () {
            $('#hid_nameresults').html(this.value);
        });

        $('#ffb1_input').keypress(function (e) {
            $('#hid_nameresults').html(this.value);
            if (e.keyCode == 13) { document.getElementById('<%= btnByName.ClientID%>').click(); }
        });

        $('#ffb2').flexbox(resultsUrl + 'FlexResults?' + filterProcessorQueryValue + 'SearchType=useremail', {
            autoCompleteFirstMatch: true,
            resultTemplate: '<div>{name}</div>',
            showArrow: false,
            paging: false,
            width: 250,
            onSelect: function () {
                $('#hid_emailresults').html(this.value);
            }
        });

        $('#ffb2_input').blur(function () {
            $('#hid_emailresults').html(this.value);
        });

        $('#ffb2_input').keypress(function (e) {
            $('#hid_emailresults').html(this.value);
            if (e.keyCode == 13) { document.getElementById('<%= btnByEmail.ClientID%>').click(); }
        });

        $('#ffb3').flexbox(resultsUrl + 'FlexResults?' + filterProcessorQueryValue + 'SearchType=userissuer', {
            autoCompleteFirstMatch: true,
            resultTemplate: '<div>{name}</div>',
            showArrow: false,
            paging: false,
            width: 250,
            onSelect: function () {
                $('#hid_issuerresults').html(this.value);
            }
        });

        $('#ffb3_input').blur(function () {
            $('#hid_issuerresults').html(this.value);
        });

        $('#ffb3_input').keypress(function (e) {
            $('#hid_issuerresults').html(this.value);
            if (e.keyCode == 13) { document.getElementById('<%= btnByIssuer.ClientID%>').click(); }
        });

        $('#ffb4').flexbox(resultsUrl + 'FlexResults?' + filterProcessorQueryValue + 'SearchType=order', {
            autoCompleteFirstMatch: true,
            resultTemplate: '<div>{name}</div>',
            showArrow: false,
            paging: false,
            width: 250,
            minChars: 2,
            onSelect: function () {
                $('#hid_orderresults').html(this.value);
            }
        });

        $('#ffb4_input').blur(function () {
            $('#hid_orderresults').html(this.value);
        });

        $('#ffb4_input').keypress(function (e) {
            $('#hid_orderresults').html(this.value);
            if (e.keyCode == 13) { document.getElementById('<%= btnByOrder.ClientID%>').click(); }
        });

        $('#ffb5').flexbox(resultsUrl + 'FlexResults?' + filterProcessorQueryValue + 'SearchType=ordername', {
            autoCompleteFirstMatch: true,
            resultTemplate: '<div>{name}</div>',
            showArrow: false,
            paging: false,
            width: 250,
            onSelect: function () {
                $('#hid_orderNameresults').html(this.value);
            }
        });

        $('#ffb5_input').blur(function () {
            $('#hid_orderNameresults').html(this.value);
        });

        $('#ffb5_input').keypress(function (e) {
            $('#hid_orderNameresults').html(this.value);
            if (e.keyCode == 13) { document.getElementById('<%= btnByOrderUser.ClientID%>').click(); }
        });

        $('#ffb6').flexbox(resultsUrl + 'FlexResults?' + filterProcessorQueryValue + 'SearchType=orderemail', {
            autoCompleteFirstMatch: true,
            resultTemplate: '<div>{name}</div>',
            showArrow: false,
            paging: false,
            width: 250,
            onSelect: function () {
                $('#hid_orderemailresults').html(this.value);
            }
        });

        $('#ffb6_input').blur(function () {
            $('#hid_orderemailresults').html(this.value);
        });

        $('#ffb6_input').keypress(function (e) {
            $('#hid_orderemailresults').html(this.value);
            if (e.keyCode == 13) { document.getElementById('<%= btnByOrderEmail.ClientID%>').click(); }
        });

        $('#ffb7').flexbox(resultsUrl + 'FlexResults?' + filterProcessorQueryValue + 'SearchType=orderissuer', {
            autoCompleteFirstMatch: true,
            resultTemplate: '<div>{name}</div>',
            showArrow: false,
            paging: false,
            width: 250,
            onSelect: function () {
                $('#hidorderissuer').html(this.value);
            }
        });

        $('#ffb7_input').blur(function () {
            $('#hid_orderissuerresults').html(this.value);
        });

        $('#ffb7_input').keypress(function (e) {
            $('#hid_orderissuerresults').html(this.value);
            if (e.keyCode == 13) { document.getElementById('<%= btnByOrderIssuer.ClientID%>').click(); }
        });

    });

    /* Redirect */
    function redirectToResults(btnName) {

        var a = '';
        if (btnName == 'name') {
            a = document.getElementById("hid_nameresults").innerHTML;
            a = trim(a);
            if (a == '') {
                alert('Please enter User Name first');
                return false;
            } else {
                document.getElementById('<%= hidname.ClientID%>').value = a;
            }
        }
        if (btnName == 'email') {
            a = document.getElementById("hid_emailresults").innerHTML;
            a = trim(a);
            if (a == '') {
                alert('Please enter User Email first');
                return false;
            } else {
                document.getElementById('<%= hidemail.ClientID%>').value = a;
            }
        }
        if (btnName == 'issuer') {
            a = document.getElementById("hid_issuerresults").innerHTML;
            a = trim(a);
            if (a == '') {
                alert('Please enter Issuer first');
                return false;
            } else {
                document.getElementById('<%= hidissuer.ClientID%>').value = a;
            }
        }
        if (btnName == 'order') {
            a = document.getElementById("hid_orderresults").innerHTML;
            a = trim(a);
            if (a == '') {
                alert('Please enter Order No first');
                return false;
            } else {
                document.getElementById('<%= hidorder.ClientID%>').value = a;
            }
        }
        if (btnName == 'ordername') {
            a = document.getElementById("hid_orderNameresults").innerHTML;
            a = trim(a);
            if (a == '') {
                alert('Please enter Name first');
                return false;
            } else {
                document.getElementById('<%= hidordername.ClientID%>').value = a;
            }
        }

        if (btnName == 'orderemail') {
            a = document.getElementById("hid_orderemailresults").innerHTML;
            a = trim(a);
            if (a == '') {
                alert('Please enter Email first');
                return false;
            } else {
                document.getElementById('<%= hidorderemail.ClientID%>').value = a;
            }
        }
        if (btnName == 'orderissuer') {
            a = document.getElementById("hid_orderissuerresults").innerHTML;
            a = trim(a);
            if (a == '') {
                alert('Please enter Issuer first');
                return false;
            } else {
                document.getElementById('<%= hidorderissuer.ClientID%>').value = a;
            }
        }
    }
</script>

<form runat="server">
    <!--[if IE 8 ]><div class="center_container width958 ie8"><![endif]-->
    <!--[if IE 9 ]><div class="center_container width958 ie9"><![endif]-->
    <!---[if !(IE)]><!-->
    <div class="center_container width958">
        <!--<![endif]-->
        <div class="form_container">
            <%--            <div class="orange_heading_profile">
                <asp:label id="lblHeading" runat="server"></asp:label>
                <asp:placeholder runat="server" id="plcDetailMessage">
				<p>Find a specific entry in our system by using the forms below.</p>
			</asp:placeholder>
            </div>--%>
            <asp:placeholder runat="server" id="plcholderuserssearch">
			<div class="faq_blog">
				<div class="contact_container mrt">
					<div class="contact_heading">
						<div class="orange_heading_main">Search Users by</div>
					</div>
					<div class="contact_heading_bg">
						<div class="mr-top15 pad_left15">
							<label>Name</label>
							<input type="hidden" id="hidname" runat="server" />
							<div id="ffb1"></div>
							<div id="hid_nameresults" style="display: none"></div>
							<span class="bg_btn_red bg_btn_nomar">
								<asp:Button runat="server" ID="btnByName" class="submit_bg_btn none_input" Enabled="true" Text="Go" />
							</span>
						</div>
						<div class="clr"></div>
						<div class="mr-top15 pad_left15">
							<label>Email</label>
							<input type="hidden" id="hidemail" runat="server" />
							<div id="ffb2"></div>
							<div id="hid_emailresults" style="display: none"></div>
							<span class="bg_btn_red bg_btn_nomar">
								<asp:Button runat="server" ID="btnByEmail" Enabled="true" class="submit_bg_btn none_input" Text="Go" />
							</span>
						</div>
						<div class="clr"></div>
						<div class="mr-top15 pad_left15">
							<label>Issuer</label>
							<input type="hidden" id="hidissuer" runat="server" />
							<div id="ffb3"></div>
							<div id="hid_issuerresults" style="display: none"></div>
							<span class="bg_btn_red bg_btn_nomar">
								<asp:Button runat="server" ID="btnByIssuer" Enabled="true" class="submit_bg_btn none_input" Text="Go" />
							</span>
						</div>
					</div>
					<div class="contact_heading_bot"></div>
				</div>
			</div>
		</asp:placeholder>
            <asp:placeholder runat="server" id="plcOrderSearch" visible="false">
			<div class="faq_blog">
				<div class="contact_container mrt">
					<div class="contact_heading">
						<div class="orange_heading_main">Search Orders by</div>
					</div>
					<div class="contact_heading_bg">
						<div class="mr-top15 pad_left15">
							<label>Order #</label>
							<input type="hidden" id="hidorder" runat="server" />
							<div id="ffb4"></div>
							<div id="hid_orderresults" style="display: none"></div>
							<span class="bg_btn_red bg_btn_nomar">
								<asp:Button runat="server" ID="btnByOrder" Enabled="true" class="submit_bg_btn none_input" Text="Go"></asp:Button>
							</span>
						</div>
						<div class="clr"></div>
						<div class="mr-top15 pad_left15">
							<label>Name</label>
							<input type="hidden" id="hidordername" runat="server" />
							<div id="ffb5"></div>
							<div id="hid_orderNameresults" style="display: none"></div>
							<span class="bg_btn_red bg_btn_nomar">
								<asp:Button runat="server" ID="btnByOrderUser" Enabled="true" class="submit_bg_btn none_input" Text="Go"></asp:Button>
							</span>
						</div>
						<div class="clr"></div>
						<div class="mr-top15 pad_left15">
							<label>Email</label>
							<input type="hidden" id="hidorderemail" runat="server" />
							<div id="ffb6"></div>
							<div id="hid_orderemailresults" style="display: none"></div>
							<span class="bg_btn_red bg_btn_nomar">
								<asp:Button runat="server" ID="btnByOrderEmail" class="submit_bg_btn none_input" Enabled="true" Text="Go"></asp:Button>
							</span>
						</div>
						<div class="clr"></div>
						<div class="mr-top15 pad_left15">
							<label>Issuer</label>
							<input type="hidden" id="hidorderissuer" runat="server" />
							<div id="ffb7"></div>
							<div id="hid_orderissuerresults" style="display: none"></div>
							<span class="bg_btn_red bg_btn_nomar">
								<asp:Button runat="server" ID="btnByOrderIssuer" Enabled="true" class="submit_bg_btn none_input" Text="Go"></asp:Button>
							</span>
						</div>
						<div class="clr"></div>
						<div class="mr-top15 pad_left15">
							<label>Start Date</label>
							<asp:TextBox runat="server" ID="txtStartDate" class="input_small_1 heigh dates" type="text" size="20"></asp:TextBox>
							<label id="search-orders-by--end-date">End Date</label>
							<asp:TextBox runat="server" class="input_small_1 heigh dates" ID="txtEndDate" type="text" size="20"></asp:TextBox>
							<span class="bg_btn_red bg_btn_nomar">
								<asp:Button runat="server" ID="btnByDate" class="submit_bg_btn none_input" Text="Go"></asp:Button>
							</span>
						</div>
						<div class="clr"></div>
						<div class="mr-top15 pad_left15" id="search-orders-by--state">
							<label>Status</label>
							<asp:DropDownList runat="server" ID="drp" CssClass="fl dd-list"></asp:DropDownList>
							<span class="bg_btn_red bg_btn_nomar">
								<asp:Button runat="server" ID="btnByStatus" class="submit_bg_btn none_input" Text="Go"></asp:Button>
							</span>
						</div>
					</div>
					<div class="contact_heading_bot"></div>
				</div>
			</div>
		</asp:placeholder>
            <div id="divNoOrder" runat="server" visible="false">
                <p>
                    <span class="grey_heading_bld">No Results Found.</span>
                </p>
            </div>
            <asp:placeholder id="plcUserResult" runat="server" visible="false">
			<div class="grid_container">
				<asp:Repeater ID="UserResult" runat="server">
					<HeaderTemplate>
						<div class="grid_top_head">
							<span class="grid_heading">Name</span> 
							<span class="grid_heading" style="width: 250px">Email</span> 
							<span class="grid_heading" style="width: 250px">Issuer</span> 
							<span style="display: <%= !String.IsNullOrEmpty(GetProcessorFilter()) ? "none" : "block" %>" class="grid_heading ">Processor</span>
						</div>
						<div class="grid_top_bg">
					</HeaderTemplate>
					<ItemTemplate>
							<div class="white_row">
								<span class="data">
									<a href="<%# GetUserProfileEditPage() + ((Eval("UserName")!=null) ? ((Eval("UserName").ToString().IndexOf(":") > 0) ? Eval("UserName").ToString().Split(':')[1] : Eval("UserName").ToString()) : "")  + "&Source=" + ConfigurationManager.AppSettings["AdminUrl"] + ConfigurationManager.AppSettings["UserProfileEditorHome"] %>" class="price"><%# Eval("FullName") %></a>
								</span> 
								<span class="data" style="width: 250px">
									<%# Eval("EMail")%>&nbsp;
								</span>
								<span class="data" style="width: 250px">
									<%# Eval("IssuerName")%>
								</span>
								<span style="display: <%= !String.IsNullOrEmpty(GetProcessorFilter()) ? "none" : "block" %>" class="data">
									<%# Eval("ProcessorName")%>
									<span style="display: none"></span>
								</span>
							</div>
					</ItemTemplate>
					<AlternatingItemTemplate>
							<div class="white_row gray_backgrnd">
								<span class="data">
									<a href="<%# GetUserProfileEditPage() + ((Eval("UserName")!=null) ? ((Eval("UserName").ToString().IndexOf(":") > 0) ? Eval("UserName").ToString().Split(':')[1] : Eval("UserName").ToString()) : "")  + "&Source=" + ConfigurationManager.AppSettings["AdminUrl"] + ConfigurationManager.AppSettings["UserProfileEditorHome"] %>" class="price"><%# Eval("FullName") %></a>
								</span>
								<span class="data" style="width: 250px">
									<%# Eval("EMail")%>&nbsp;
								</span>
								<span class="data" style="width: 250px">
									<%# Eval("IssuerName")%>
								</span>
								<span style="display: <%= !String.IsNullOrEmpty(GetProcessorFilter()) ? "none" : "block" %>" class="data">
									<%# Eval("ProcessorName")%>
									<span style="display: none"></span>
								</span>
							</div>
					</AlternatingItemTemplate>
					<FooterTemplate>
						</div>
            </FooterTemplate>
				</asp:Repeater>
        </div>
        <div class="grid_top_head_bot"></div>
        </asp:PlaceHolder>
		<asp:placeholder id="plcholderorderresult" runat="server">
			<div class="grid_container">
				<asp:Repeater ID="rptorder" Visible="true" runat="server">
					<HeaderTemplate>
						<div class="grid_top_head">
							<span class="grid_heading new_width">Order #</span> 
							<span class="grid_heading new_width">Date</span> 
							<span class="grid_heading new_width">Price</span> 
							<span class="grid_heading ">Name</span> 
							<span class="grid_heading ">Issuer</span> 
							<span style="display: <%= !String.IsNullOrEmpty(GetProcessorFilter()) ? "none" : "block" %>" class="grid_heading">Processor</span>
							<span class="grid_heading new_width">Status</span>
						</div>
						<div class="grid_top_bg">
					</HeaderTemplate>
					<ItemTemplate>
							<div class="white_row">
								<span class="data new_width">
									<a href='<%# Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["OrderDetailUrl"] + "?orderid=" + (((Eval("OrderNumber") != null) ? Eval("OrderNumber").ToString() : "") + "&Source=" + Source)%>' class="price"><%# Eval("OrderNumber")%></a>
								</span> 
								<span class="data new_width">
									<%# (Eval("OrderDate") != null) ? Convert.ToDateTime(Eval("OrderDate").ToString()).ToString("MM/dd/yyyy") : ""%>&nbsp;
								</span> 
								<span class="data new_width">
									<%# (Eval("OrderPrice") != null) ? FormatNumber(Convert.ToDecimal(Eval("OrderPrice"))) : ""%>
								</span>
								<asp:PlaceHolder runat="server" ID="placeHolderAdmin" Visible='<%# !Request.Url.ToString().Contains("/printer") && IsUserAdmin%>'>
									<span class="data">
										<a href="<%# GetUserProfileEditPage() + ((Eval("UserName") != null) ? ((Eval("UserName").ToString().IndexOf(":") > 0) ? Eval("UserName").ToString().Split(':')[1] : Eval("UserName").ToString()) : "") + "&Source=" + ConfigurationManager.AppSettings["AdminUrl"] + ConfigurationManager.AppSettings["UserProfileEditorHome"]%>" class="price"><%# Eval("FullName")%></a>&nbsp;
									</span> 
								</asp:PlaceHolder>
								<asp:PlaceHolder runat="server" ID="placeholderprinter" Visible='<%# Request.Url.ToString().Contains("/printer") || !IsUserAdmin%>'>
									<span class="data">
										<%# Eval("FullName")%>&nbsp;
									</span> 
								</asp:PlaceHolder>
								<span class="data ">
									<%# Eval("IssuerName")%>&nbsp; 
								</span>
								<span style="display: <%= !String.IsNullOrEmpty(GetProcessorFilter()) ? "none" : "block" %>" class="data">
									<%# Eval("ProcessorName")%>&nbsp;
								</span> 
								<span class="data new_width">
									<%# Eval("OrderStatus").ToString().Replace("Shipped", "Shipped/Fulfilled")%>&nbsp;
								</span>
							</div>
					</ItemTemplate>
					<AlternatingItemTemplate>
							<div class="white_row gray_backgrnd">
								<span class="data new_width">
									<a href='<%# Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["OrderDetailUrl"] + "?orderid=" + (((Eval("OrderNumber") != null) ? Eval("OrderNumber").ToString() : "") + "&Source=" + Source) %>' class="price"><%# Eval("OrderNumber")%></a>
								</span> 
								<span class="data new_width">
									<%# (Eval("OrderDate") != null) ? Convert.ToDateTime(Eval("OrderDate").ToString()).ToString("MM/dd/yyyy") : ""%>&nbsp;
								</span> 
								<span class="data new_width">
									<%# (Eval("OrderPrice") != null) ? FormatNumber(Convert.ToDecimal(Eval("OrderPrice"))) : ""%>
								</span>
								<asp:PlaceHolder runat="server" ID="placeHolderAdmin" Visible='<%# !Request.Url.ToString().Contains("/printer") && IsUserAdmin%>'>
									<span class="data">
										<a href="<%# GetUserProfileEditPage() + ((Eval("UserName") != null) ? ((Eval("UserName").ToString().IndexOf(":") > 0) ? Eval("UserName").ToString().Split(':')[1] : Eval("UserName").ToString()) : "")+ "&Source=" + ConfigurationManager.AppSettings["AdminUrl"] + ConfigurationManager.AppSettings["UserProfileEditorHome"]%>" class="price"><%# Eval("FullName")%></a>&nbsp;
									</span> 
								</asp:PlaceHolder>
								<asp:PlaceHolder runat="server" ID="placeholderprinter" Visible='<%# Request.Url.ToString().Contains("/printer") || !IsUserAdmin%>'>
									<span class="data">
										<%# Eval("FullName")%>&nbsp;
									</span> 
								</asp:PlaceHolder>
								<span class="data">
									<%# Eval("IssuerName")%>&nbsp; 
								</span>
								<span style="display: <%= !String.IsNullOrEmpty(GetProcessorFilter()) ? "none" : "block" %>" class="data">
									<%# Eval("ProcessorName")%>&nbsp;
								</span> 
								<span class="data new_width">
									<%# Eval("OrderStatus").ToString().Replace("Shipped", "Shipped/Fulfilled")%>&nbsp;
								</span>
							</div>
					</AlternatingItemTemplate>
					<FooterTemplate>
						</div>
						<div class="grid_top_head_bot"></div>
						<div class="pd_bot">&nbsp;</div>
        </FooterTemplate>
				</asp:Repeater>
    </div>
    </asp:PlaceHolder>
	</div>
</div>
</form>
