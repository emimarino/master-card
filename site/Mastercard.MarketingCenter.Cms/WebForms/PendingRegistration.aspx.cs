﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Common;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Cms.WebForms
{
    public partial class PendingRegistration : System.Web.UI.Page
    {
        #region Properties

        public bool CanChangeSegments { get { return User.IsInPermission(Constants.Permissions.CanChangeSegments) && _userContext?.SelectedRegion != null; } }

        public bool CanHaveEmptySegments { get { return User.IsInPermission(Constants.Permissions.CanHaveEmptySegments); } }

        public bool CanHaveEmptyProcessors { get { return User.IsInPermission(Constants.Permissions.CanHaveEmptyProcessors); } }

        public int UserId { get { return _userContext.User.UserId; } }

        #endregion

        #region Services

        private UserContext _userContext;
        private SlamContext _slamContext;

        private IIssuerService _issuerService;
        protected IIssuerService IssuerService
        {
            get
            {
                if (_issuerService == null)
                    _issuerService = DependencyResolver.Current.GetService<IIssuerService>();

                return _issuerService;
            }
        }

        private IImportedIcaService _importedIcaService;
        protected IImportedIcaService ImportedIcaService
        {
            get
            {
                if (_importedIcaService == null)
                    _importedIcaService = DependencyResolver.Current.GetService<IImportedIcaService>();

                return _importedIcaService;
            }
        }

        RegistrationService _registrationService { get { return DependencyResolver.Current.GetService<RegistrationService>(); } }

        IPendingRegistrationService _pendingRegistrationServices { get { return DependencyResolver.Current.GetService<IPendingRegistrationService>(); } }

        ISegmentationService _segmentationService { get { return DependencyResolver.Current.GetService<ISegmentationService>(); } }

        public ProcessorService _processorService { get { return DependencyResolver.Current.GetService<ProcessorService>(); } }

        public IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            _slamContext = DependencyResolver.Current.GetService<SlamContext>();
            _userContext = DependencyResolver.Current.GetService<UserContext>();

            phError.Visible = false;
            phErrorRejected.Visible = false;
            if (!IsPostBack)
            {
                lnkCancel.HRef = _urlService.GetPendingRegistrationsReportURL();
                rbtnExisting.Checked = true;
                rbtnNew.Checked = false;

                LoadFinancialInstitutions();
                LoadPendingRegistration();
                LoadSegmentations();
                LoadProcessors();
            }
        }

        private void LoadSegmentations()
        {
            ddlSegmentations.DataSource = _segmentationService.GetSegmentationsByRegion(_userContext.SelectedRegion).ToList();
            ddlSegmentations.DataTextField = "SegmentationName";
            ddlSegmentations.DataValueField = "SegmentationId";
            ddlSegmentations.AppendDataBoundItems = true;
            ddlSegmentations.DataBind();
        }

        private void LoadProcessors()
        {
            var processors = _processorService.GetProcessors().ToList();
            processors.Insert(0, new Data.Entities.Processor { Title = "Select Processor", ProcessorId = "-1" });

            ddlProcessors.DataSource = processors;
            ddlProcessors.DataTextField = "Title";
            ddlProcessors.DataValueField = "ProcessorID";
            ddlProcessors.AppendDataBoundItems = true;
            ddlProcessors.DataBind();
        }

        private void LoadFinancialInstitutions()
        {
            var ls = IssuerService.GetIssuersByRegion(_userContext.SelectedRegion).OrderBy(i => i.Title).ToList();
            ddlFinancialInstitutions.Items.Add(new ListItem("--- Please choose a value ---"));
            foreach (var issuer in ls)
            {
                ddlFinancialInstitutions.Items.Add(new ListItem($"{issuer.Title} ({issuer.DomainName})", issuer.IssuerId));
            }
        }

        private void LoadPendingRegistration()
        {
            Guid guid = new Guid(Page.RouteData.Values["guid"].ToString());
            bool showRejected = false;
            bool.TryParse(Page.RouteData.Values["showrejected"].ToString(), out showRejected);

            var pr = _pendingRegistrationServices.GetPendingRegistrationsByGuid(guid.ToString());
            if (pr != null)
            {
                var state = (pr.RegionId.IsNullOrEmpty() ? true : (pr.RegionId.Trim().Equals("us", StringComparison.OrdinalIgnoreCase))) ? pr.State.StateName : pr.Province;
                txtRejectionReason.Text = ConfigurationManager.AppSettings[$"PendingRegistration.DefaultRejectReason.{pr.Language}"]?.ToString();
                litFirstName.Text = Server.HtmlEncode(pr.FirstName);
                litLastName.Text = Server.HtmlEncode(pr.LastName);
                litFinancialInstitution.Text = Server.HtmlEncode(pr.FinancialInstitution);
                litPhone.Text = Server.HtmlEncode(pr.Phone);
                litFax.Text = Server.HtmlEncode(pr.Fax);
                litEmail.Text = Server.HtmlEncode(pr.Email);
                litAddress1.Text = Server.HtmlEncode(pr.Address1);
                litCityStateZIP.Text = Server.HtmlEncode($"{pr.City}, {state}, {pr.Zip}");
                litTitle.Text = Server.HtmlEncode(pr.Title);
                litProcessor.Text = Server.HtmlEncode(pr.Processor);
                litCountry.Text = Server.HtmlEncode(pr.Country.IsNullOrEmpty() ? null : _slamContext.GetTagTree().FindNode(n => n.Tag != null && n.Tag.Identifier.Equals(pr.Country, StringComparison.OrdinalIgnoreCase)).Text);
                litRegion.Text = Server.HtmlEncode(pr.RegionId.ToUpper());
                litLanguage.Text = Server.HtmlEncode(pr.Language.IsNullOrEmpty() ? null : _slamContext.GetTagTree().FindNode(n => n.Tag != null && n.Tag.Identifier.Equals(pr.Language, StringComparison.OrdinalIgnoreCase)).Text);

                chkAddDomain.Text = $"Add \"{pr.Email.Split('@')[1]}\" to the Domain Names of the Financial Institution selected above";
                txtFIName.Text = pr.FinancialInstitution;
                txtFIDomain.Text = pr.Email.Split('@')[1];

                rbtnIssuerWithoutCID.Checked = !pr.RegionId.Trim().Equals("us", StringComparison.OrdinalIgnoreCase);

                if (pr.Status.ToLower() != Constants.PendingRegistrationStatus.Pending.ToLower())
                {
                    if (showRejected)
                    {
                        litDescription.Text = "Please review the following registration details and indicate if you want to reinstate and approve it.";
                        txtRejectionReason.Text = pr.RejectionReason;
                        txtRejectionReason.Enabled = false;
                        lnkSaveReject.Visible = false;
                    }
                    else
                    {
                        phRejectOrAdd.Visible = false;
                    }

                    phAlreadyRejectedOrAdded.Visible = true;
                    litStatus.Text = $"The registration has already been {pr.Status.ToUpper()} by {pr.StatusChangedBy}.";
                }
                else
                {
                    litDescription.Text = "Please review the following registration details and either approve or reject it.";
                }
            }
        }

        protected void lnkSaveReject_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtRejectionReason.Text.Trim()))
            {
                phErrorRejected.Visible = true;
                litErrorRejected.Text = "Please enter a reason or message in order to reject the registration.";
            }
            else
            {
                phRejectOrAdd.Visible = false;
                phAlreadyRejectedOrAdded.Visible = true;
                phApprovedRejected.Visible = true;
                litApprovedRejected.Text = "REJECTED";
                _pendingRegistrationServices.RejectPendingRegistration(Page.RouteData.Values["guid"].ToString(), txtRejectionReason.Text.Trim(), _userContext.User.Email, _userContext.SelectedRegion, Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["ApplicationName"]);
            }
        }

        protected void lnkSaveApprove_Click(object sender, EventArgs e)
        {
            litError.Text = string.Empty;
            var errors = new List<string>();
            Guid guid = new Guid(Page.RouteData.Values["guid"].ToString());

            var pr = _pendingRegistrationServices.GetPendingRegistrationsByGuid(guid.ToString());

            if (rbtnExisting.Checked)
            {
                if (ddlFinancialInstitutions.SelectedIndex > 0)
                {
                    if (string.IsNullOrEmpty(_registrationService.GetUserNameForEmail(pr.Email)))
                    {
                        if (chkAddDomain.Checked)
                        {
                            string domain = pr.Email.Split('@')[1].ToLower();
                            var issuer = IssuerService.GetIssuerByDomain(domain, _userContext.SelectedRegion);
                            if (issuer != null)
                            {
                                errors.Add($"Domain '{domain}' is currently being used by the Financial Institution '{issuer.Title}'.");
                            }

                            if (!IssuerService.VerifyDomainIsValid(domain.Trim()))
                            {
                                errors.Add($"Domain is not permitted for use as an Issuer domain: {domain.Trim()}");
                            }
                        }

                        if (!errors.Any())
                        {
                            var user = _pendingRegistrationServices.ApprovePendingRegistration(
                                                                                                        guid.ToString(),
                                                                                                        ddlFinancialInstitutions.SelectedValue,
                                                                                                        _userContext.User.Email,
                                                                                                        (chkAddDomain.Checked) ? pr.Email.Split('@')[1] : null,
                                                                                                        Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["ApplicationName"]
                                                                                                    );
                            if (user != null)
                            {
                                IssuerService.GetIssuerById(ddlFinancialInstitutions.SelectedValue);
                                phRejectOrAdd.Visible = false;
                                phAlreadyRejectedOrAdded.Visible = true;
                                phApprovedRejected.Visible = true;
                                litApprovedRejected.Text = "APPROVED";
                                litStatus.Text = string.Empty;
                            }
                            else
                            {
                                errors.Add("There was a problem when trying to approve the registration.");
                            }
                        }
                    }
                    else
                    {
                        errors.Add("A user with the same email is already registered in the system.");
                    }
                }
                else
                {
                    errors.Add("Please choose a Financial Institution.");
                }
            }
            else
            {
                if (string.IsNullOrEmpty(txtFIName.Text.Trim()))
                {
                    errors.Add("Please enter a Name.");
                }

                if (string.IsNullOrEmpty(txtFIDomain.Text.Trim()))
                {
                    errors.Add("Please enter a Domain Name. Use comma to put additional domains if needed. (Ex: domain1.com,domain2.com))");
                }

                if (!string.IsNullOrEmpty(txtFIICA.Text.Trim()))
                {
                    if (int.TryParse(txtFIICA.Text.Trim(), out int ica))
                    {
                        var masterIca = IssuerService.GetMasterICAByIca(ica);
                        if (masterIca != null)
                        {
                            var issuer = IssuerService.GetIssuersByRegion(_userContext.SelectedRegion).FirstOrDefault(i => i.BillingId == masterIca.MasterIca.ToString());
                            if (issuer != null)
                            {
                                errors.Add($"The ICA value entered is already assigned to {issuer.Title}.");
                            }
                        }
                    }
                    else
                    {
                        errors.Add("The ICA must be a numeric value.");
                    }
                }

                string cidValue = string.Empty;
                HtmlInputRadioButton rbtnIssuerWithoutCID = (HtmlInputRadioButton)pnlIssuerWithoutCID.FindControl("rbtnIssuerWithoutCID");
                if (rbtnIssuerWithoutCID != null && rbtnIssuerWithoutCID.Checked)
                {
                    cidValue = rbtnIssuerWithoutCID.Value;
                }
                else
                {
                    foreach (RepeaterItem item in rptIssuerSearchGrid.Items)
                    {
                        if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                        {
                            HtmlInputRadioButton rbtnMatch = (HtmlInputRadioButton)item.FindControl("rbtnMatch");
                            if (rbtnMatch != null && rbtnMatch.Checked)
                            {
                                cidValue = rbtnMatch.Value;
                            }
                        }
                    }
                }
                if (string.IsNullOrEmpty(cidValue))
                {
                    errors.Add("Please match a CID.");
                }

                // first check the domain(s)
                if (!string.IsNullOrEmpty(txtFIDomain.Text.Trim()))
                {
                    var domains = txtFIDomain.Text.Split(',');
                    foreach (var domain in domains)
                    {
                        var issuer = IssuerService.GetIssuerByDomain(domain, _userContext.SelectedRegion);
                        if (issuer != null)
                        {
                            errors.Add($"Domain '{domain}' is currently being used by the Financial Institution '{issuer.Title}'.");
                        }

                        if (!IssuerService.VerifyDomainIsValid(domain.Trim()))
                        {
                            errors.Add($"Domain is not permitted for use as an issuer domain: {domain.Trim()}");
                        }
                    }
                }

                var segments = (from ListItem item in ddlSegmentations.Items where item.Selected && !string.IsNullOrWhiteSpace(item.Value) select item.Value).ToArray();
                if (!CanHaveEmptySegments && (segments == null || segments.Length < 1))
                {
                    errors.Add("Please, choose at least one issuer segment.");
                }

                if (!CanHaveEmptyProcessors && (ddlProcessors.SelectedIndex < 1))
                {
                    errors.Add("Please, choose at least one issuer processor.");
                }

                if (!errors.Any())
                {
                    if (string.IsNullOrEmpty(_registrationService.GetUserNameForEmail(pr.Email)))
                    {
                        var user = _pendingRegistrationServices.ApprovePendingRegistration(
                                Page.RouteData.Values["guid"].ToString(),
                                txtFIName.Text.Trim(),
                                txtFIDomain.Text.Trim(),
                                txtFIICA.Text.Trim(),
                                User.IsInPermission(Constants.Permissions.CanChangeSegments) ? segments : null,
                                _userContext.User.Email,
                                ddlProcessors.SelectedValue,
                                (_userContext?.SelectedRegion ?? ""),
                                UserId,
                                cidValue.ToUpperInvariant() != "NOCID" ? cidValue : null,
                                Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["ApplicationName"]
                            );

                        if (user != null)
                        {
                            phRejectOrAdd.Visible = false;
                            phAlreadyRejectedOrAdded.Visible = true;
                            phApprovedRejected.Visible = true;
                            litApprovedRejected.Text = "APPROVED";
                            litStatus.Text = string.Empty;
                        }
                        else
                        {
                            errors.Add("There was a problem when trying to approve the registration.");
                        }
                    }
                    else
                    {
                        errors.Add("A user with the same email is already registered in the system.");
                    }
                }
            }

            if (errors.Any())
            {
                errors.ForEach(error => litError.Text += error + "<br/>");
                phError.Visible = true;
            }
        }

        protected void rptIssuerSearchGrid_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
        }

        protected void lnkSearchMatch_Click(object sender, EventArgs e)
        {
            LoadIssuerSearchGrid();
        }

        private void LoadIssuerSearchGrid()
        {
            rptIssuerSearchGrid.ItemDataBound += new RepeaterItemEventHandler(rptIssuerSearchGrid_ItemDataBound);
            rptIssuerSearchGrid.DataSource = ImportedIcaService.GetSearchImportedIcas(txtSearchMatch.Text).ToList();
            rptIssuerSearchGrid.DataBind();
        }
    }
}