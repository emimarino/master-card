﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomizationQueue.aspx.cs" Inherits="Mastercard.MarketingCenter.Cms.WebForms.CustomizationQueue" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head runat="server">
    <title></title>
    <link href="../Content/legacy/css/MarketingCenter.SharedControls.css" type="text/css" rel="stylesheet" />

    <style>
        a:link {
            color: #0072bc;
            text-decoration: none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="center_container">
                <div class="form_container">
                    <div class="grid_container">
                        <asp:Label runat="server" CssClass="grey_heading_bld" ID="lblNoRecords" Text="There are currently no items in the list." Visible="false"></asp:Label>
                        <asp:Repeater ID="rptorder" Visible="true" runat="server">
                            <HeaderTemplate>
                                <div class="grid_top_head">
                                    <span class="grid_heading new_width">
                                        <asp:LinkButton ID="lnkHeaderOrderNumber" runat="server" Text="Order #" CommandName="Sort" CommandArgument="OrderNumber" OnCommand="lnkHeader_Command"></asp:LinkButton></span>
                                    <span class="grid_heading new_width">
                                        <asp:LinkButton ID="lnkHeaderDate" runat="server" Text="Date" CommandName="Sort" CommandArgument="Date" OnCommand="lnkHeader_Command"></asp:LinkButton></span>
                                    <span class="grid_heading">
                                        <asp:LinkButton ID="lnkHeaderName" runat="server" Text="Name" CommandName="Sort" CommandArgument="Name" OnCommand="lnkHeader_Command"></asp:LinkButton></span>
                                    <span class="grid_heading ">
                                        <asp:LinkButton ID="lnkHeaderProcessor" runat="server" Text="Processor" CommandName="Sort" CommandArgument="Processor" OnCommand="lnkHeader_Command"></asp:LinkButton></span>
                                    <span class="grid_heading ">
                                        <asp:LinkButton ID="lnkHeaderIssuer" runat="server" Text="Issuer" CommandName="Sort" CommandArgument="Issuer" OnCommand="lnkHeader_Command"></asp:LinkButton></span>
                                    <span class="grid_heading ">
                                        <asp:LinkButton ID="lnkHeaderStatus" runat="server" Text="Status" CommandName="Sort" CommandArgument="Status" OnCommand="lnkHeader_Command"></asp:LinkButton></span>
                                </div>
                                <div class="grid_top_bg">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="white_row">
                                    <span class="data new_width">
                                        <a href='<%# Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["OrderDetailUrl"] + "?orderid=" + ((Eval("OrderNumber")!=null) ?  Eval("OrderNumber").ToString() : "") + "&Source=CustomizationQueue" %>' class="price">
                                            <%#  Eval("OrderNumber")%>
                                        </a>&nbsp;
                                    </span>
                                    <span class="data new_width"><%# (Eval("OrderDate")!=null) ? Convert.ToDateTime(Eval("OrderDate").ToString()).ToString("MM/dd/yyyy") : "" %></span>
                                    <span class="data "><%# Eval("FullName")%> &nbsp;</span>
                                    <span class="data"><%# Eval("ProcessorName")%> &nbsp;</span>
                                    <span class="data "><%# Eval("IssuerName")%>&nbsp; </span>
                                    <span class="data "><%# Eval("OrderStatus")%>&nbsp; </span>
                                </div>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <div class="white_row gray_backgrnd">
                                    <span class="data new_width">
                                        <a href='<%# Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["OrderDetailUrl"] + "?orderid=" + ((Eval("OrderNumber")!=null) ?  Eval("OrderNumber").ToString() : "") + "&Source=CustomizationQueue" %>' class="price">
                                            <%#  Eval("OrderNumber")%>
                                        </a>&nbsp;
                                    </span>
                                    <span class="data new_width"><%# (Eval("OrderDate")!=null) ? Convert.ToDateTime(Eval("OrderDate").ToString()).ToString("MM/dd/yyyy") : "" %></span>
                                    <span class="data "><%# Eval("FullName")%> &nbsp;</span>
                                    <span class="data"><%# Eval("ProcessorName")%> &nbsp;</span>
                                    <span class="data "><%# Eval("IssuerName")%>&nbsp; </span>
                                    <span class="data "><%# Eval("OrderStatus")%>&nbsp; </span>
                                </div>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </div>
					<div class="grid_top_head_bot"></div>
                                <div class="pd_bot">&nbsp;</div>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
