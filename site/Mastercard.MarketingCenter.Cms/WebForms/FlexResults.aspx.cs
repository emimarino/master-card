﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.WebForms
{
    public partial class FlexResults : System.Web.UI.Page
    {
        private UserContext _userContext;
        private LookupService _lookupService { get { return DependencyResolver.Current.GetService<LookupService>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            _userContext = DependencyResolver.Current.GetService<UserContext>();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.Clear();

            // Do not cache response

            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            // Set the content type and encoding for JSON

            Response.ContentType = "application/json";
            Response.ContentEncoding = Encoding.UTF8;

            string query = Request["q"].ToString().ToLower();

            int page = 0;
            int.TryParse(Request["p"], out page);
            int size = 0;
            int.TryParse(Request["s"], out size);

            string[] words = null;

            if (Request["SearchType"].ToString() == "username")
            {
                words = getUserNameList(query);
            }
            else if (Request["SearchType"].ToString() == "useremail")
            {
                words = getUserEmailList(query);
            }
            else if (Request["SearchType"].ToString() == "userissuer")
            {
                words = getUserIssuerList(query);
            }
            else if (Request["SearchType"].ToString() == "ordername")
            {
                words = getOrderNameList(query);
            }
            else if (Request["SearchType"].ToString() == "orderemail")
            {
                words = getOrderEmailList(query);
            }
            else if (Request["SearchType"].ToString() == "orderissuer")
            {
                words = getOrderIssuerList(query);
            }
            else if (Request["SearchType"].ToString() == "order")
            {
                words = getOrderNoList(query);
            }

            int total = 0;

            StringBuilder sb = new StringBuilder();
            sb.Append("{\"results\":[");

            List<string> results = new List<string>();

            for (int i = 0; i < words?.Length; i++)
            {
                results.Add("{\"id\":" + i + ",\"name\":\"" + words[i] + "\"}");
                total++;
            }

            string[] resultsArray = results.ToArray();
            List<string> pagedResults = new List<string>();

            if (size > 0)
            {
                int start = (page - 1) * size + 1;
                int end = (start > (total - size)) ? total : start + size - 1;
                for (int i = start - 1; i < end; i++)
                {
                    pagedResults.Add(resultsArray[i]);
                }
            }

            if (pagedResults.Count > 0)
            {
                sb.Append(string.Join(",", pagedResults.ToArray()));
            }
            else if (resultsArray.Length > 0)
            {
                sb.Append(string.Join(",", resultsArray));
            }
            sb.Append("],\"total\":\"" + total + "\"}");

            Response.Write(sb.ToString());

            // Flush the response buffer
            Response.Flush();

            // Complete the request.  NOTE: Do not use Response.End() here, because it throws a ThreadAbortException, which cannot be caught!
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        protected string[] getUserNameList(string prefixText)
        {
            string processorId = "";
            if (Request.QueryString["Processor"] != null)
            {
                processorId = Request.QueryString["Processor"];
            }

            List<string> lsreturn = new List<string>();
            List<RetrieveUserByTextResult> lp = _lookupService.GetUserNames(prefixText, processorId, _userContext.SelectedRegion);
            foreach (RetrieveUserByTextResult item in lp)
            {
                lsreturn.Add(item.UserName);
            }

            return lsreturn.ToArray();
        }

        protected string[] getOrderNameList(string prefixText)
        {
            string processorId = "";
            if (Request.QueryString["Processor"] != null)
            {
                processorId = Request.QueryString["Processor"];
            }

            List<string> lsreturn = new List<string>();
            List<RetrieveUsersFromOrdersResult> lp = _lookupService.GetOrderNames(prefixText, processorId);
            foreach (RetrieveUsersFromOrdersResult item in lp)
            {
                lsreturn.Add(item.UserName);
            }

            return lsreturn.ToArray();
        }

        protected string[] getUserEmailList(string prefixText)
        {
            string processorId = "";
            if (Request.QueryString["Processor"] != null)
            {
                processorId = Request.QueryString["Processor"];
            }

            List<string> lsreturn = new List<string>();
            List<RetrieveUserEmailByTextResult> lp = _lookupService.GetUserEmails(prefixText, processorId, _userContext.SelectedRegion);
            foreach (RetrieveUserEmailByTextResult item in lp)
            {
                lsreturn.Add(item.Email);
            }

            return lsreturn.ToArray();
        }

        protected string[] getOrderEmailList(string prefixText)
        {
            string processorId = "";
            if (Request.QueryString["Processor"] != null)
            {
                processorId = Request.QueryString["Processor"];
            }

            List<string> lsreturn = new List<string>();
            List<RetrieveUserEmailsFromOrdersResult> lp = _lookupService.GetOrderEmails(prefixText, processorId);
            foreach (RetrieveUserEmailsFromOrdersResult item in lp)
            {
                lsreturn.Add(item.Email);
            }

            return lsreturn.ToArray();
        }

        protected string[] getUserIssuerList(string prefixText)
        {
            string processorId = "";
            if (Request.QueryString["Processor"] != null)
            {
                processorId = Request.QueryString["Processor"];
            }

            List<string> lsreturn = new List<string>();
            List<RetrieveIssuerByTextResult> lp = _lookupService.GetUserIssuers(prefixText, processorId, _userContext.SelectedRegion);

            foreach (RetrieveIssuerByTextResult item in lp)
            {
                lsreturn.Add(item.IssuerName);
            }

            return lsreturn.ToArray();
        }

        protected string[] getOrderIssuerList(string prefixText)
        {
            string processorId = "";
            if (Request.QueryString["Processor"] != null)
            {
                processorId = Request.QueryString["Processor"];
            }
            List<string> lsreturn = new List<string>();
            List<RetrieveIssuersFromOrdersResult> lp = _lookupService.GetOrderIssuers(prefixText, processorId);

            foreach (RetrieveIssuersFromOrdersResult item in lp)
            {
                lsreturn.Add(item.IssuerName);
            }

            return lsreturn.ToArray();
        }

        protected string[] getOrderNoList(string prefixText)
        {
            string processorId = "";
            if (Request.QueryString["Processor"] != null)
            {
                processorId = Request.QueryString["Processor"];
            }
            List<string> lsreturn = new List<string>();
            List<RetrieveOrderNoByTextResult> lp = _lookupService.GetOrders(prefixText, processorId);

            foreach (RetrieveOrderNoByTextResult item in lp)
            {
                lsreturn.Add(item.OrderNumber);
            }

            return lsreturn.ToArray();
        }
    }
}