﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BillingReportListing.aspx.cs" Inherits="Mastercard.MarketingCenter.Cms.WebForms.BillingReportListing" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head runat="server">
    <link href="../Content/legacy/css/MarketingCenter.SharedControls.css" type="text/css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="lblErrors" runat="server" Visible="false" ForeColor="Red"></asp:Label>
            <div class="contact_heading_bg" style="background: none !important;">
                <div class="mr-top15">
                    <label>Start Date</label>
                    <asp:TextBox runat="server" ID="txtStartDate" class="input_small_1 heigh" type="text" size="20"></asp:TextBox>
                    <label style="width: auto !important; padding: 0px 5px 0px 20px">End Date</label>
                    <asp:TextBox runat="server" class="input_small_1 heigh" ID="txtEndDate" type="text" size="20"></asp:TextBox>
                    <span class="bg_btn_red bg_btn_nomar">
                        <asp:Button runat="server" ID="btnExport" class="submit_bg_btn none_input" Text="Export to Excel" OnClick="btnExport_Click" />
                    </span>
                </div>
            </div>
            <div class="clr"></div>
            <asp:Repeater ID="rptMonthResult" runat="server">
                <ItemTemplate>
                    <span class="data"><a href='BillingReport.aspx?odate=<%#Eval("Odate")%>&ordermonth=<%#Eval("OrderDate")%>'>
                        <%#Eval("OrderDate")%></a></span><br />
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </form>
</body>
</html>
