﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExportBillingReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Cms.WebForms.ExportBillingReport" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
            <asp:GridView ID="gvExportData" AutoGenerateColumns="false" runat="server">
                <Columns>
                    <asp:BoundField DataField="IssuerName" HeaderText="Issuer Name" />
                    <asp:BoundField DataField="BillingID" HeaderText="ICA" />
                    <asp:BoundField DataField="OrderNumber" HeaderText="Order #" />
                    <asp:BoundField DataField="PromotionAmount" HeaderText="Promotion Amount" DataFormatString="${0:#,0}" />
                    <asp:TemplateField HeaderText="Total">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# String.Format(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "{0:c}", ((Decimal)Eval("Total"))-((decimal)Eval("PromotionAmount"))) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ReportData" HeaderText="Report Data" />
                </Columns>
            </asp:GridView>
    </form>
</body>
</html>
