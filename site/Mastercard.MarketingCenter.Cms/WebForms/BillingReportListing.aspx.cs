﻿using Mastercard.MarketingCenter.Services;
using System;
using System.Web;
using System.Web.UI;

namespace Mastercard.MarketingCenter.Cms.WebForms
{
    public partial class BillingReportListing : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptMonthResult.DataSource = BillingReportService.GetOrderMonths();
                rptMonthResult.DataBind();
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            lblErrors.Visible = false;
            lblErrors.Text = string.Empty;

            if (txtStartDate.Text.Trim().Length == 0)
            {
                lblErrors.Text = "Please enter Start Date<br />";
                lblErrors.Visible = true;
            }
            if (txtEndDate.Text.Trim().Length == 0)
            {
                lblErrors.Text += "Please enter End Date<br />";
                lblErrors.Visible = true;
            }

            if (txtStartDate.Text.Trim().Length > 0 && txtEndDate.Text.Trim().Length > 0)
            {
                DateTime startDate = DateTime.MinValue;
                DateTime endDate = DateTime.MinValue;

                if (!DateTime.TryParse(txtStartDate.Text, out startDate))
                {
                    lblErrors.Text = "Start Date Invalid<br />";
                    lblErrors.Visible = true;
                }

                if (!DateTime.TryParse(txtEndDate.Text, out endDate))
                {
                    lblErrors.Text += "End Date Invalid<br />";
                    lblErrors.Visible = true;
                }

                if (startDate != DateTime.MinValue && endDate != DateTime.MinValue)
                {
                    if (startDate.CompareTo(endDate) < 0)
                    {
                        var redirectUrl = Page.GetRouteUrl("webforms",
                            new
                            {
                                page = "ExportBillingReport",
                                StartDate = HttpUtility.UrlEncode(startDate.ToString("d")),
                                EndDate = HttpUtility.UrlEncode(endDate.ToString("d"))
                            });
                        Response.Redirect(redirectUrl);
                    }
                    else
                    {
                        lblErrors.Text += "End Date must be later than Start Date<br />";
                        lblErrors.Visible = true;
                    }
                }
            }
        }
    }
}