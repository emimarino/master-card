﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.WebForms
{
    public partial class UserOrderSearch : System.Web.UI.Page
    {
        private bool? _isUserAdmin;
        private bool? _isUserProcessor;
        private UserContext _userContext;
        private ProcessorService _processorService;

        protected ProcessorService ProcessorService
        {
            get
            {
                if (_processorService == null)
                {
                    _processorService = DependencyResolver.Current.GetService<ProcessorService>();
                }

                return _processorService;
            }
        }

        protected IReportService ReportService
        {
            get
            {
                return DependencyResolver.Current.GetService<IReportService>();
            }
        }

        public bool IsUserAdmin
        {
            get
            {
                if (_isUserAdmin == null)
                {
                    _isUserAdmin = Context.User.IsInRole(Constants.Roles.McAdmin) || Context.User.IsInRole(Constants.Roles.DevAdmin);
                }

                return _isUserAdmin.Value;
            }
        }

        public bool IsUserProcessor
        {
            get
            {
                if (_isUserProcessor == null)
                {
                    _isUserProcessor = Context.User.IsInRole(Constants.Roles.Processor);
                }

                return _isUserProcessor.Value;
            }
        }

        private bool? _isUserPrinter;
        public bool IsUserPrinter
        {
            get
            {
                if (_isUserPrinter == null)
                {
                    _isUserPrinter = Context.User.IsInRole(Constants.Roles.Printer);
                }

                return _isUserPrinter.Value;
            }
        }

        public string ComesFrom
        {
            get
            {
                if (string.IsNullOrEmpty(Request.QueryString["comesFrom"]))
                {
                    return "mastercard-admin";
                }
                return Request.QueryString["comesFrom"];
            }
        }

        public string Source
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["Source"]))
                {
                    return Request.QueryString["Source"];
                }
                return ComesFrom;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            btnByEmail.Click += new EventHandler(btnByEmail_Click);
            btnByIssuer.Click += new EventHandler(btnByIssuer_Click);
            btnByName.Click += new EventHandler(btnByName_Click);
            btnByOrder.Click += new EventHandler(btnByOrder_Click);
            btnByOrderUser.Click += new EventHandler(btnByOrderUser_Click);
            btnByOrderEmail.Click += new EventHandler(btnByOrderEmail_Click);
            btnByOrderIssuer.Click += new EventHandler(btnByOrderIssuer_Click);
            btnByDate.Click += new EventHandler(btnByDate_Click);
            btnByStatus.Click += new EventHandler(btnByStatus_Click);

            btnByEmail.OnClientClick = "return redirectToResults('email');";
            btnByName.OnClientClick = "return redirectToResults('name');";
            btnByIssuer.OnClientClick = "return redirectToResults('issuer');";
            btnByOrder.OnClientClick = "return redirectToResults('order');";
            btnByOrderUser.OnClientClick = "return redirectToResults('ordername');";
            btnByOrderEmail.OnClientClick = "return redirectToResults('orderemail');";
            btnByOrderIssuer.OnClientClick = "return redirectToResults('orderissuer');";

            btnByDate.OnClientClick = "return ValidateForm('" + txtStartDate.ClientID + "','" + txtEndDate.ClientID + "');";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _userContext = DependencyResolver.Current.GetService<UserContext>();
            if (!IsPostBack)
            {
                GetProcessorFilter();
                drp.DataSource = ReportService.GetOrderStatuses(ViewState["Processor"]?.ToString()).ToList();
                drp.DataTextField = "OrderStatus";
                drp.DataValueField = "OrderStatus";
                drp.DataBind();

                if (ComesFrom.ToLower() == "processor" || IsUserProcessor)
                {
                    plcholderuserssearch.Visible = true;
                    plcOrderSearch.Visible = true;
                    plcUserResult.Visible = false;
                    plcholderorderresult.Visible = false;
                }
                else if (IsUserAdmin)
                {
                    if (ComesFrom.ToLower() == "mastercard-admin")
                    {
                        plcholderuserssearch.Visible = true;
                        plcOrderSearch.Visible = true;
                        plcUserResult.Visible = false;
                        plcholderorderresult.Visible = false;
                    }
                    else
                    {
                        plcholderuserssearch.Visible = false;
                        plcOrderSearch.Visible = true;
                        plcUserResult.Visible = false;
                        plcholderorderresult.Visible = false;
                    }
                }
                else if (IsUserPrinter)
                {
                    plcholderuserssearch.Visible = false;
                    plcOrderSearch.Visible = true;
                    plcUserResult.Visible = false;
                    plcholderorderresult.Visible = false;
                }

                plcOrderSearch.Visible = plcOrderSearch.Visible && User.IsInPermission(Constants.Permissions.CanManageOrders);

                if (!string.IsNullOrEmpty(GetOrderIssuerQuery()))
                {
                    hidorderissuer.Value = GetOrderIssuerQuery();

                    btnByOrderIssuer_Click(this, e);
                }

                if (!string.IsNullOrEmpty(GetUserIssuerQuery()))
                {
                    hidissuer.Value = GetUserIssuerQuery();

                    btnByIssuer_Click(this, e);
                }

                if (!string.IsNullOrEmpty(GetOrderDateQueryStart()) && !string.IsNullOrEmpty(GetOrderDateQueryEnd()))
                {
                    txtStartDate.Text = GetOrderDateQueryStart();
                    txtEndDate.Text = GetOrderDateQueryEnd();

                    btnByDate_Click(this, e);
                }
            }
        }

        protected string FormatNumber(decimal num)
        {
            NumberFormatInfo nf = new System.Globalization.NumberFormatInfo();
            nf.CurrencySymbol = "$";
            nf.NumberDecimalDigits = 2;
            return num.ToString("C", nf);
        }

        protected string GetUserProfileEditPage()
        {
            return ConfigurationManager.AppSettings["UserProfileEditorUrl"].TrimStart('/');
        }

        protected string GetUserIssuerQuery()
        {
            string query = "";
            if (!IsPostBack)
            {
                query = Request.QueryString["UserIssuer"] ?? "";
            }
            return query;
        }

        protected string GetOrderIssuerQuery()
        {
            string query = "";
            if (!IsPostBack)
            {
                query = Request.QueryString["OrderIssuer"] ?? "";
            }
            return query;
        }

        protected string GetOrderDateQueryStart()
        {
            string date = "";
            if (!IsPostBack)
            {
                if (Request.QueryString["OrderDate"] != null)
                {
                    date = DateTime.Parse(Request.QueryString["OrderDate"], CultureInfo.InvariantCulture).Date.ToString();
                }
            }
            return date;
        }

        protected string GetOrderDateQueryEnd()
        {
            string date = "";
            if (!IsPostBack)
            {
                if (Request.QueryString["OrderDate"] != null)
                {
                    date = DateTime.Parse(Request.QueryString["OrderDate"], CultureInfo.InvariantCulture).AddDays(1).AddMinutes(-2).ToString();
                }
            }
            return date;
        }

        public string GetResultsUrl()
        {
            return HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + ResolveUrl("~/").TrimEnd('/') + "/webforms/";
        }

        public string GetProcessorFilter()
        {
            if (ViewState["ProcessorFilter"] == null && Context.User.IsInRole(Constants.Roles.Processor))
            {
                ViewState["Processor"] = Request.QueryString["p"] ?? ProcessorService.GetProcessorByIssuerId(_userContext.User.IssuerId)?.ProcessorId;
                ViewState["ProcessorFilter"] = $"Processor={ViewState["Processor"]}&";
            }

            return ViewState["ProcessorFilter"]?.ToString();
        }

        void btnByName_Click(object sender, EventArgs e)
        {
            if (hidname.Value.Trim().Length > 0)
            {
                string processor = null;
                if (ViewState["Processor"] != null)
                {
                    processor = ViewState["Processor"].ToString();
                }

                var lstresult = ReportService.GetFilterUsers(hidname.Value, null, null, processor, _userContext.SelectedRegion).ToList();
                if (lstresult.Any())
                {
                    UserResult.DataSource = lstresult;
                    UserResult.DataBind();
                    SetVisibility(false, true, false, false, false);
                }
                else
                {
                    SetVisibility(false, false, false, false, true);
                }
            }
        }

        void btnByIssuer_Click(object sender, EventArgs e)
        {
            if (hidissuer.Value.Trim().Length > 0)
            {
                string processor = null;
                if (ViewState["Processor"] != null)
                {
                    processor = ViewState["Processor"].ToString();
                }

                var lstresult = ReportService.GetFilterUsers(null, null, hidissuer.Value.ToString(), processor, _userContext.SelectedRegion).ToList();
                if (lstresult.Any())
                {
                    UserResult.DataSource = lstresult;
                    UserResult.DataBind();
                    SetVisibility(false, true, false, false, false);
                }
                else
                {
                    SetVisibility(false, false, false, false, true);
                }
            }
        }

        void btnByEmail_Click(object sender, EventArgs e)
        {
            if (hidemail.Value.Trim().Length > 0)
            {
                string processor = null;
                if (ViewState["Processor"] != null)
                {
                    processor = ViewState["Processor"].ToString();
                }

                var lstresult = ReportService.GetFilterUsers(null, hidemail.Value.ToString(), null, processor, _userContext.SelectedRegion).ToList();

                if (lstresult.Any())
                {
                    SetVisibility(false, true, false, false, false);
                    UserResult.DataSource = lstresult;
                    UserResult.DataBind();
                }
                else
                {
                    SetVisibility(false, false, false, false, true);
                }
            }
        }

        protected void btnByOrder_Click(object sender, EventArgs e)
        {
            if (hidorder.Value.Trim().Length > 0)
            {
                string processor = null;
                if (ViewState["Processor"] != null)
                {
                    processor = ViewState["Processor"].ToString();
                }

                var lstresult = ReportService.GetFilterOrders(hidorder.Value, null, null, null, null, null, null, processor).ToList();

                if (lstresult.Any())
                {
                    SetVisibility(false, false, false, true, false);
                    rptorder.DataSource = lstresult;
                    rptorder.DataBind();
                }
                else
                {
                    SetVisibility(false, false, false, false, true);
                }
            }
        }

        protected void btnByOrderUser_Click(object sender, EventArgs e)
        {
            if (hidordername.Value.Trim().Length > 0)
            {
                string processor = null;
                if (ViewState["Processor"] != null)
                {
                    processor = ViewState["Processor"].ToString();
                }

                var lstresult = ReportService.GetFilterOrders(null, hidordername.Value, null, null, null, null, null, processor).ToList();

                if (lstresult.Any())
                {
                    SetVisibility(false, false, false, true, false);
                    rptorder.DataSource = lstresult;
                    rptorder.DataBind();
                }
                else
                {
                    SetVisibility(false, false, false, false, true);
                }
            }
        }

        protected void btnByOrderEmail_Click(object sender, EventArgs e)
        {
            if (hidorderemail.Value.Trim().Length > 0)
            {
                string processor = null;
                if (ViewState["Processor"] != null)
                {
                    processor = ViewState["Processor"].ToString();
                }

                var lstresult = ReportService.GetFilterOrders(null, null, hidorderemail.Value, null, null, null, null, processor).ToList();

                if (lstresult.Any())
                {
                    SetVisibility(false, false, false, true, false);
                    rptorder.DataSource = lstresult;
                    rptorder.DataBind();
                }
                else
                {
                    SetVisibility(false, false, false, false, true);
                }
            }
        }

        protected void btnByOrderIssuer_Click(object sender, EventArgs e)
        {
            if (hidorderissuer.Value.Trim().Length > 0)
            {
                string processor = null;
                if (ViewState["Processor"] != null)
                {
                    processor = ViewState["Processor"].ToString();
                }

                var lstresult = ReportService.GetFilterOrders(null, null, null, hidorderissuer.Value, null, null, null, processor).ToList();

                if (lstresult.Any())
                {
                    SetVisibility(false, false, false, true, false);
                    rptorder.DataSource = lstresult;
                    rptorder.DataBind();
                }
                else
                {
                    SetVisibility(false, false, false, false, true);
                }
            }
        }

        protected void btnByDate_Click(object sender, EventArgs e)
        {
            if (txtStartDate.Text.Trim().Length > 0 && txtEndDate.Text.Trim().Length > 0)
            {
                try
                {
                    DateTime? stdate = null;
                    DateTime? enddate = null;

                    stdate = Convert.ToDateTime(txtStartDate.Text);
                    enddate = Convert.ToDateTime(txtEndDate.Text);
                    if (stdate.Value < enddate.Value)
                    {
                        SetVisibility(false, false, false, true, false);

                        string processor = null;
                        if (ViewState["Processor"] != null)
                        {
                            processor = ViewState["Processor"].ToString();
                        }

                        rptorder.DataSource = ReportService.GetFilterOrders(null, null, null, null, stdate, enddate, null, processor).ToList();
                        rptorder.DataBind();
                    }
                }
                catch
                {
                }
            }
        }

        protected void btnByStatus_Click(object sender, EventArgs e)
        {
            string processor = null;
            if (ViewState["Processor"] != null)
            {
                processor = ViewState["Processor"].ToString();
            }

            if (drp.Items.Count > 0 && drp.SelectedItem != null)
            {
                if (drp.SelectedItem.Text.Trim().Length > 0)
                {
                    SetVisibility(false, false, false, true, false);

                    var lstresult = ReportService.GetFilterOrders(null, null, null, null, null, null, drp.SelectedItem.Text.Replace("Shipped/Fulfilled", "Shipped"), processor).ToList();

                    if (lstresult.Any())
                    {
                        SetVisibility(false, false, false, true, false);
                        rptorder.DataSource = lstresult;
                        rptorder.DataBind();
                    }
                    else
                    {
                        SetVisibility(false, false, false, false, true);
                    }
                }
            }
            else
            {
                SetVisibility(false, false, false, true, false);

                var lstresult = ReportService.GetFilterOrders(null, null, null, null, null, null, "asdfasdf", processor).ToList();

                if (lstresult.Any())
                {
                    SetVisibility(false, false, false, true, false);
                    rptorder.DataSource = lstresult;
                    rptorder.DataBind();
                }
                else
                {
                    SetVisibility(false, false, false, false, true);
                }
            }
        }

        protected void SetVisibility(bool plcuser, bool plcuserresult, bool OrderSearchPanel, bool OrderResult, bool NoOrder)
        {
            plcOrderSearch.Visible = OrderSearchPanel;
            plcholderorderresult.Visible = OrderResult;
            plcUserResult.Visible = plcuserresult;
            plcholderuserssearch.Visible = plcuser;
            divNoOrder.Visible = NoOrder;
        }
    }
}