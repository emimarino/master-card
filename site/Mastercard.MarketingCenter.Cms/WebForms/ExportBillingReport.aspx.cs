﻿using Mastercard.MarketingCenter.Services;
using System;
using System.Web;
using System.Web.UI;

namespace Mastercard.MarketingCenter.Cms.WebForms
{
    public partial class ExportBillingReport : Page
    {
        private bool _exportCall = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["StartDate"] != null && Request.QueryString["EndDate"] != null)
            {
                _exportCall = true;
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment;filename=BillingReportOutput.xls");

                Response.Charset = "";
                Response.ContentType = "application/vnd.xls";

                gvExportData.DataSource = BillingReportService.GetOrderDetailRangeReport(DateTime.Parse(HttpUtility.UrlDecode(Request.QueryString["StartDate"])), DateTime.Parse(HttpUtility.UrlDecode(Request.QueryString["EndDate"])));
                gvExportData.DataBind();

                using (System.IO.StringWriter stringWrite = new System.IO.StringWriter())
                {
                    using (HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite))
                    {
                        gvExportData.RenderControl(htmlWrite);
                        Response.Write(stringWrite.ToString());
                        Response.End();
                    }
                }
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            if (!_exportCall)
            {
                base.VerifyRenderingInServerForm(control);
            }
        }
    }
}