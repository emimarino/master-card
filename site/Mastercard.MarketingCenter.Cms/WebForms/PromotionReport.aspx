﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PromotionReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Cms.WebForms.PromotionReport" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head runat="server">
    <link href="../Content/legacy/css/MarketingCenter.SharedControls.css" type="text/css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <style type="text/css">
                .ms-WPBody {
                    min-height: 650px;
                }
            </style>
         
            <asp:Literal ID="litCaption" runat="server" />

            Select Promotion:
            <asp:DropDownList ID="ddlPromotions" AutoPostBack="true" OnSelectedIndexChanged="ddlPromotions_SelectedIndexChanged" runat="server" />
            <br />
            <br />
            <asp:PlaceHolder ID="phPromotions" Visible="false" runat="server">
                <asp:ListView ID="lvPromotions" ItemPlaceholderID="content" runat="server">
                    <LayoutTemplate>
                        <div class="grid_container">
                            <div class="grid_top_head">
                                <span class="grid_heading"># of Orders</span>
                                <span class="grid_heading">Discount Total</span>
                                <span class="grid_heading" style="width: 210px;">Issuer</span>
                                <span class="grid_heading" style="width: 210px;">Processor</span>
                                <span class="grid_heading">Total</span>
                            </div>
                            <div class="grid_top_bg">
                                <div id="content" runat="server"></div>
                            </div>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class="white_row">
                            <span class="data">
                                <asp:HyperLink ID="ltOrderNumber" Text='<%# Eval("OrderNumber") %>' NavigateUrl='<%# Page.GetRouteUrl("webforms", new { page = "PromotionReport", IssuerID = HttpUtility.UrlEncode(Eval("IssuerID").ToString()), Promotion = HttpUtility.UrlEncode(Eval("Promotion").ToString()) }) %>' runat="server" />
                            </span>
                            <span class="data">
                                <asp:Literal ID="ltDiscountTotal" Text='<%# Eval("DiscountTotal", "${0:F2}")%>' runat="server" /></span>
                            <span class="data" style="width: 210px;">
                                <asp:Literal ID="ltIssuer" Text='<%# Eval("Issuer")%>' runat="server" /></span>
                            <span class="data" style="width: 210px;">
                                <asp:Literal ID="ltProcessor" Text='<%# Eval("Processor")%>' runat="server" /></span>
                            <span class="data"></span>
                        </div>
                        <asp:PlaceHolder ID="phSubTotal" Visible="false" runat="server">
                            <div class="white_row">
                                <span class="data">&nbsp;</span>
                                <span class="data">&nbsp;</span>
                                <span class="data" style="width: 210px;">&nbsp;</span>
                                <span class="data" style="width: 210px;">
                                    <asp:Literal ID="ltProcessorForSubTotal" runat="server" /></span>
                                <span class="data">
                                    <asp:Literal ID="ltSubTotal" runat="server" /></span>
                            </div>
                        </asp:PlaceHolder>
                    </ItemTemplate>
                </asp:ListView>
                <div class="grid_top_bg" style="padding: 10px 5px 0 5px; border-top: solid 1px #D4D6D6; height: 35px; font-weight: bold;">
                    <span style="margin-left: 517px;">Total for promotion</span>
                    <span style="margin-left: 78px;">
                        <asp:Literal ID="ltTotal" runat="server" /></span>
                </div>
                <div class="grid_top_head_bot" />
                <div class="pd_bot"></div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phOrders" Visible="false" runat="server">
                <asp:GridView ID="gvOrders" GridLines="None" OnRowCreated="gvOrders_RowCreated" AutoGenerateColumns="false" CssClass="grid_container"
                    runat="server">
                    <Columns>
                        <asp:TemplateField HeaderText="Order #" HeaderStyle-CssClass="grid_heading" ItemStyle-CssClass="data white_row_new">
                            <ItemTemplate>
                                <a href='<%# Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["OrderDetailUrl"] + "?orderid=" + Eval("OrderNumber") + "&Source=PromotionReport" %>' style="cursor: pointer;"><%# Eval("OrderNumber") %></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="OrderDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText="Date" HeaderStyle-CssClass="grid_heading" ItemStyle-CssClass="data" />
                        <asp:BoundField DataField="Total" DataFormatString="${0:F2}" HeaderText="Order Total" HeaderStyle-CssClass="grid_heading" ItemStyle-CssClass="data" />
                        <asp:BoundField DataField="DiscountTotal" DataFormatString="${0:F2}" HeaderText="Discount Total" HeaderStyle-CssClass="grid_heading" ItemStyle-CssClass="data" />
                        <asp:BoundField DataField="FullName" HeaderText="Name" HeaderStyle-CssClass="grid_heading" ItemStyle-CssClass="data white_row_new_right" />
                    </Columns>
                    <HeaderStyle CssClass="grid_top_head grid_top_head_new" />
                </asp:GridView>
                <div class="grid_top_head_bot"></div>
            </asp:PlaceHolder>
        </div>
    </form>
</body>
</html>
