﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageIssuers.aspx.cs" Inherits="Mastercard.MarketingCenter.Cms.WebForms.ManageIssuers" %>

<%@ Register Src="~/WebForms/UserControls/ProcessorFilterControl.ascx" TagPrefix="uc1" TagName="ProcessorFilterControl" %>

<link href="../Content/legacy/css/MarketingCenter.SharedControls.css" type="text/css" rel="stylesheet" />

<script type="text/javascript">
    function clearFilterMessage(elem) {
        if (elem.value == "-- Type Issuer or Domain Name --") {
            elem.value = "";
        }
    }

    function showFilterMessage(elem) {
        if (elem.value == "") {
            elem.value = "-- Type Issuer or Domain Name --";
        }
    }
</script>
<style type="text/css">
    .grid_container .grid_heading {
        font: bold 12px/25px Verdana,Arial,Helvetica,sans-serif;
        padding-left: 10px;
        float: none !important;
    }

    .grid_container .data {
        width: 130px;
        font: 11px/14px Verdana,Arial,Helvetica,sans-serif;
        padding: 5px 0 8px 10px;
    }

    a:link {
        color: #0072bc;
        text-decoration: none;
    }

    tbody td {
        font-size: 8pt;
        font-family: verdana,arial,helvetica,sans-serif;
    }
</style>

<form id="form1" runat="server">
    <uc1:ProcessorFilterControl runat="server" ID="ProcessorFilterControl" />
    <div class="center_container">
        <div class="form_container">
            <div class="grid_container">
                <asp:panel id="pnlFilter" runat="server" defaultbutton="btnFilter" cssclass="buttonsContainer">
                    <div class="left">
                        <span id="spanAddNew" runat="server" class="bg_btn_red bg_btn_nomar">
                            <asp:Button ID="btnAddNew" runat="server" Text="Add New" style="cursor: pointer" CssClass="submit_bg_btn none_input" OnClick="btnAddNew_Click" />
                        </span>
                    </div>
                    <div class="right">
                        <asp:TextBox ID="txtFilter" runat="server" Width="204px" Text="-- Type Issuer or Domain Name --" onfocus="clearFilterMessage(this);" onblur="showFilterMessage(this);"></asp:TextBox>
                        <span class="bg_btn_red bg_btn_nomar">
                            <asp:Button ID="btnFilter" runat="server" Text="Filter" style="cursor: pointer" CssClass="submit_bg_btn none_input" OnClick="btnFilter_Click" />
                        </span>
                    </div>
                    <div class="clr"></div>
                </asp:panel>

                <asp:PlaceHolder ID="phError" runat="server" Visible="false">
                    <div style="color: #f00;">
                        <asp:Literal ID="litError" runat="server" />
                        <br />
                    </div>
                </asp:PlaceHolder>

                <asp:PlaceHolder ID="phAddIssuer" Visible="false" runat="server">
                    <table style="margin-bottom: 15px;">
                        <tbody>
                            <tr>
                                <th scope="col" style="width: 180px;">Name<span style="color: #f00;">*</span></th>
                                <th scope="col" style="width: 180px;">Domain Name(s)<span style="color: #f00;">*</span></th>
                                <th scope="col"></th>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtIssuerName" runat="server" Width="170"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtIssuerDomain" runat="server" Width="170"></asp:TextBox></td>
                                <td>
                                    <span class="bg_btn_red bg_btn_nomar">
                                        <asp:Button ID="btnAdd" Text="Add" style="cursor: pointer" CssClass="submit_bg_btn none_input" OnClick="btnAdd_Click" runat="server" />
                                    </span>
                                    <span class="bg_btn_red bg_btn_nomar">
                                        <asp:Button ID="btnCancel" Text="Cancel" style="cursor: pointer" CssClass="submit_bg_btn none_input" OnClick="btnCancel_Click" runat="server" />
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </asp:PlaceHolder>
                <asp:gridview id="gvIssuers" datakeynames="IssuerID" cssclass="grid_container"
                    onrowediting="gvIssuers_RowEditing"
                    onrowupdating="gvIssuers_RowUpdating" onrowcancelingedit="gvIssuers_RowCancelingEdit"
                    autogeneratecolumns="false" gridlines="None" width="100%"
                    allowpaging="true" onpageindexchanging="gvIssuers_PageIndexChanging"
                    allowsorting="true" onsorting="gvIssuers_Sorting" pagesize="20" runat="server">
                    <columns>
                        <asp:TemplateField HeaderText="Issuer Name" SortExpression="IssuerName" HeaderStyle-CssClass="grid_heading" ItemStyle-CssClass="data">
                            <ItemTemplate><asp:Literal ID="ltIssuerName" Text='<%# Eval("IssuerName") %>' runat="server" /></ItemTemplate>                
                            <EditItemTemplate>
                                <asp:TextBox Text='<%# Eval("IssuerName") %>' ID="txtIssuerName" runat="server" />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Domain Name" SortExpression="DomainName" HeaderStyle-CssClass="grid_heading" ItemStyle-CssClass="data">
                            <ItemTemplate><asp:Literal ID="ltDomainName" Text='<%# Eval("DomainName") %>' runat="server" /></ItemTemplate>
                            <EditItemTemplate><asp:TextBox ID="txtDomainName" Text='<%# Eval("DomainName") %>' runat="server" /></EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="# of Users" SortExpression="Users" HeaderStyle-CssClass="grid_heading" ItemStyle-CssClass="data">
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkUsers" Text='<%# Eval("Users") %>' NavigateUrl='<%# Page.GetRouteUrl("webforms", new { page = "UserOrderSearch", comesFrom = "processor-admin", UserIssuer = Eval("IssuerName"), p = string.IsNullOrEmpty(GetProcessor()) ? string.Empty : GetProcessor() }) %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="# of Orders" SortExpression="Orders" HeaderStyle-CssClass="grid_heading" ItemStyle-CssClass="data">
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkOrders" Text='<%# Eval("Orders") %>' NavigateUrl='<%# Page.GetRouteUrl("webforms", new { page = "UserOrderSearch", comesFrom = "processor-admin", OrderIssuer = Eval("IssuerName"), source = "ProcAdminManageIssuers", p = string.IsNullOrEmpty(GetProcessor()) ? string.Empty : GetProcessor() }) %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date of Last Order" SortExpression="LastOrderDate" HeaderStyle-CssClass="grid_heading" ItemStyle-CssClass="data">
                            <ItemTemplate>
                                <asp:HyperLink ID="lnkLastOrderDate" Text='<%# DataBinder.Eval(Container.DataItem, "LastOrderDate", "{0:MM/dd/yyyy}") %>' NavigateUrl='<%# Page.GetRouteUrl("webforms", new { page = "UserOrderSearch", comesFrom = "processor", OrderDate = Eval("LastOrderDate"), Source = "ProcAdminManageIssuers", p = string.IsNullOrEmpty(GetProcessor()) ? string.Empty : GetProcessor() }) %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEdit" Text="[Edit]" CssClass="action-edit" CommandName="Edit" runat="server" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <div class="bg_btn_left" style="margin-right: 8px;"><asp:LinkButton ID="btnUpdate" Text="Update" CssClass="submit_bg_btn" CommandName="Update" runat="server" /></div>
                                <div class="bg_btn_left"><asp:LinkButton ID="LinkButton1" Text="Cancel" CssClass="submit_bg_btn" CommandName="Cancel" runat="server" /></div>
                            </EditItemTemplate>
                        </asp:TemplateField>
                    </columns>
                    <pagersettings mode="NumericFirstLast" pagebuttoncount="5" position="Bottom" nextpagetext="Next>" lastpagetext="Last>>" />
                    <headerstyle cssclass="grid_top_head grid_top_head_new" forecolor="#002672" font-size="13px" horizontalalign="Left" />
                    <alternatingrowstyle backcolor="#EFEFEF" />
                    <emptydatatemplate>
                        <asp:Label runat="server" CssClass="grey_heading_bld" ID="lblNoRecords" Text="There are currently no items in the list." />
                    </emptydatatemplate>
                </asp:gridview>
                <div class="grid_top_head_bot"></div>
                <div style="clear: both"></div>
                <br />
                <br />
            </div>
        </div>
    </div>
</form>
