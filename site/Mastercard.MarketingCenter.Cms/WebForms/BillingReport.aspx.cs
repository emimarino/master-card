﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Cms.WebForms
{
    public partial class BillingReport : System.Web.UI.Page
    {
        DataTable dtOrder;
        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime OrderDate;
            if (!IsPostBack)
            {
                if (Request.QueryString["odate"] != null)
                {
                    OrderDate = Convert.ToDateTime(Request.QueryString["odate"].ToString());
                    CreateTable();
                    LoadReport(OrderDate);
                    if (Request.QueryString["ordermonth"] != null)
                        lblHeading.Text = Request.QueryString["ordermonth"].ToString();
                }
            }

        }
        protected void CreateTable()
        {
            dtOrder = new DataTable();
            DataColumn dcol = new DataColumn("Orderno", Type.GetType("System.String"));
            dtOrder.Columns.Add(dcol);
            dcol = new DataColumn("OrderDate", Type.GetType("System.String"));
            dtOrder.Columns.Add(dcol);
            dcol = new DataColumn("Name", Type.GetType("System.String"));
            dtOrder.Columns.Add(dcol);
            dcol = new DataColumn("Issuer", Type.GetType("System.String"));
            dtOrder.Columns.Add(dcol);
            dcol = new DataColumn("Processor", Type.GetType("System.String"));
            dtOrder.Columns.Add(dcol);
            dcol = new DataColumn("Total", Type.GetType("System.String"));
            dtOrder.Columns.Add(dcol);
            dcol = new DataColumn("Date", Type.GetType("System.DateTime"));
            dtOrder.Columns.Add(dcol);
            dcol = new DataColumn("PTotal", Type.GetType("System.String"));
            dtOrder.Columns.Add(dcol);

        }
        protected void LoadReport(DateTime OrderDate)
        {
            decimal issuerTotal = 0;
            decimal processorTotal = 0;
            decimal userTotal = 0;
            decimal overAllTotal = 0;
            string issuerIca = string.Empty;
            string issuerUrl = string.Empty;

            if (System.Web.Configuration.WebConfigurationManager.AppSettings["IssuerUrl"] != null)
            {
                issuerUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["IssuerUrl"].ToString();
            }

            List<RetrieveOrderDetailReportResult> reportOrders = BillingReportService.GetOrderDetailReport(OrderDate);

            var processors = (from p in reportOrders
                              select p.processor).Distinct();

            foreach (string processor in processors)
            {
                processorTotal = 0;
                var Issuers = (from i in reportOrders  // get all the Issuer of selected processor
                               where i.processor == processor
                               select i.IssuerName).Distinct();

                foreach (string Issuer in Issuers)
                {
                    var Users_List = (from u in reportOrders
                                      where u.IssuerName == Issuer && u.processor == processor
                                      select u.FullName).Distinct();

                    issuerTotal = 0;

                    foreach (string User in Users_List)
                    {

                        var userReportOrders = (from u in reportOrders
                                                where u.IssuerName == Issuer && u.processor == processor && u.FullName == User
                                                select u);

                        userTotal = 0;
                        foreach (RetrieveOrderDetailReportResult reportOrder in userReportOrders)
                        {
                            if (!String.IsNullOrEmpty(reportOrder.BillingID) && reportOrder.BillingID != "0")
                                issuerIca = "ICA# " + reportOrder.BillingID.ToString();
                            else
                            {
                                if (!String.IsNullOrEmpty(issuerUrl))
                                    issuerIca = "<a class='price' href = '" + issuerUrl + "'>ICA# UNASSIGNED </a>";
                            }
                            userTotal = userTotal + Convert.ToDecimal(reportOrder.Total) - reportOrder.PromotionAmount;

                            string orderNumber = reportOrder.OrderNumber;
                            if (reportOrder.NumberSubOrders != reportOrder.CompleteSubOrders)
                            {
                                if (reportOrder.LastSubOrderDateCompleted.HasValue && reportOrder.LastSubOrderDateCompleted.Value.Month == OrderDate.Month && reportOrder.LastSubOrderDateCompleted.Value.Year == OrderDate.Year)
                                {
                                    orderNumber += " **";
                                }
                                else
                                {
                                    orderNumber += " *";
                                }
                            }
                            AddNewRow(orderNumber, reportOrder.OrderDate.ToString(), reportOrder.FullName, reportOrder.IssuerName, reportOrder.processor, Convert.ToDecimal(reportOrder.Total) - reportOrder.PromotionAmount, 0);
                        }

                        AddNewRow(string.Empty, string.Empty, string.Empty, string.Empty, "Total for " + User, userTotal, 0);
                        issuerTotal = issuerTotal + userTotal;


                    }

                    processorTotal = processorTotal + issuerTotal;
                    AddNewRow(string.Empty, string.Empty, string.Empty, issuerIca, "Total for " + Issuer, issuerTotal, 0);

                }
                overAllTotal = overAllTotal + processorTotal;
                AddNewRow(string.Empty, string.Empty, string.Empty, string.Empty, "Total for " + processor, processorTotal, processorTotal);

            }
            if (!String.IsNullOrEmpty(overAllTotal.ToString()))
                AddNewRow(string.Empty, string.Empty, string.Empty, string.Empty, "Total for " + Request.QueryString["ordermonth"].ToString(), overAllTotal, 0);


            rptOrderResult.ItemDataBound += new RepeaterItemEventHandler(rptOrderResult_ItemDataBound);
            rptOrderResult.DataSource = dtOrder;
            rptOrderResult.DataBind();
            //GetTotal(OverAll_Total);
        }


        void rptOrderResult_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;
                string orderno = drv["Orderno"].ToString();
                string pTotal = drv["PTotal"].ToString();
                if (orderno == string.Empty)
                {
                    HtmlGenericControl ht = (HtmlGenericControl)e.Item.FindControl("spnissuer");
                    ht.Attributes.Add("class", "databold width180px");
                    ht = (HtmlGenericControl)e.Item.FindControl("spnprocessor");
                    ht.Attributes.Add("class", "databold wida80px");
                    ht = (HtmlGenericControl)e.Item.FindControl("spntotal");
                    ht.Attributes.Add("class", "databold text_right");
                    if (pTotal != "0")
                    {
                        ht = (HtmlGenericControl)e.Item.FindControl("divrow");
                        ht.Attributes.Add("class", "white_row dotted_border");
                    }

                }
            }
        }

        protected string FormatNumber(decimal num)
        {
            System.Globalization.NumberFormatInfo nf = new System.Globalization.NumberFormatInfo();
            nf.NumberDecimalDigits = 2;
            nf.CurrencySymbol = "";
            return num.ToString("C", nf);
        }

        protected void AddNewRow(string orderno, string OrderDate, string Name, string Issuer, string processor, decimal Total, decimal PTotal)
        {
            DataRow dr = dtOrder.NewRow();
            dr["Orderno"] = orderno;
            if (OrderDate != string.Empty)
                dr["OrderDate"] = (Convert.ToDateTime(OrderDate)).Date.ToString("MM/dd/yyyy");
            else
                dr["OrderDate"] = string.Empty;
            dr["Name"] = Name;
            dr["Issuer"] = Issuer;
            dr["Processor"] = processor;
            dr["Total"] = Total;
            dr["PTotal"] = PTotal;

            if (OrderDate != string.Empty)
                dr["Date"] = Convert.ToDateTime(OrderDate);

            dtOrder.Rows.Add(dr);
        }
    }
}