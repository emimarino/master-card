﻿using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.WebForms
{
    public partial class FulfillmentQueue : System.Web.UI.Page
    {
        IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetOrderOnFulfillmentQueue();
            }
        }

        protected void GetOrderOnFulfillmentQueue()
        {
            List<RetrieveOrdersByStatusResult> ls = _orderService.GetOrderByStatus("Fulfillment");
            if (ls.Count > 0)
            {
                lblNoRecords.Visible = false;
                rptorder.Visible = true;
                rptorder.DataSource = _orderService.GetOrderByStatus("Fulfillment");
                rptorder.DataBind();
            }
            else
            {
                rptorder.Visible = false;
                lblNoRecords.Visible = true;
            }
        }
    }
}