﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebForms/WebForms.Master" AutoEventWireup="true" Inherits="Mastercard.MarketingCenter.Cms.WebForms.PendingRegistration" CodeBehind="PendingRegistration.aspx.cs" %>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="/admin/Content/css/plugins/chosen/chosen.css" rel="stylesheet" />
    <style>
        /* CHOSEN PLUGIN */
        .chosen-container-single .chosen-single {
            background: #ffffff;
            box-shadow: none;
            -moz-box-sizing: border-box;
            background-color: #FFFFFF;
            border: 1px solid darkgray;
            border-radius: 2px;
            cursor: text;
            height: auto !important;
            margin: 0;
            overflow: hidden;
            font-size: 11px;
            position: relative;
            width: 592px;
        }

        .chosen-container-multi .chosen-choices {
            min-height: unset !important;
            padding-left: 8px;
            width: 590px;
            border-color: darkgray;
        }

            .chosen-container-multi .chosen-choices li.search-choice {
                height: auto !important;
                min-height: unset !important;
                font-size: 11px;
                margin: 1px 5px 1px 0;
                padding: 1px 20px 1px 5px;
            }

                .chosen-container-multi .chosen-choices li.search-choice .search-choice-close {
                    top: 3px;
                }

            .chosen-container-multi .chosen-choices li.search-field input[type="text"] {
                margin: 0;
                padding: 0;
                height: auto !important;
                color: #444;
                font-size: 11px;
                font-family: Verdana, Arial, Helvetica, sans-serif;
                line-height: 20px;
            }

        .inner {
            padding: 10px;
        }
    </style>
    <script type="text/javascript" src="/admin/Content/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="/admin/Content/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/admin/Content/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script type="text/javascript" src="/admin/Content/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="/admin/Content/js/plugins/chosen/chosen.jquery.js"></script>

    <div style="overflow: hidden;">
        <div style="margin-bottom: 20px;">
            <p>
                <asp:Literal ID="litDescription" runat="server"></asp:Literal>
            </p>
            <br />
            <table>
                <tbody>
                    <tr>
                        <td style="width: 150px;">First Name:</td>
                        <td>
                            <asp:Literal ID="litFirstName" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Last Name:</td>
                        <td>
                            <asp:Literal ID="litLastName" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Title:</td>
                        <td>
                            <asp:Literal ID="litTitle" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Company Name:</td>
                        <td>
                            <asp:Literal ID="litFinancialInstitution" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Processor:</td>
                        <td>
                            <asp:Literal ID="litProcessor" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Phone:</td>
                        <td>
                            <asp:Literal ID="litPhone" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Fax:</td>
                        <td>
                            <asp:Literal ID="litFax" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Email Address:</td>
                        <td>
                            <asp:Literal ID="litEmail" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Address:</td>
                        <td>
                            <asp:Literal ID="litAddress1" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Literal ID="litAddress2" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Literal ID="litAddress3" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>City, State, ZIP:</td>
                        <td>
                            <asp:Literal ID="litCityStateZIP" runat="server"></asp:Literal></td>
                    </tr>
                    <% if (!string.IsNullOrEmpty(litCountry.Text))
                        { %>
                    <tr>
                        <td>Country:</td>
                        <td style="color: #f58025">
                            <asp:Literal ID="litCountry" runat="server"></asp:Literal></td>
                    </tr>
                    <% } %>
                    <% if (!string.IsNullOrEmpty(litRegion.Text))
                        { %>
                    <tr>
                        <td>Region:</td>
                        <td style="color: #f58025">
                            <asp:Literal ID="litRegion" runat="server"></asp:Literal></td>
                    </tr>
                    <% } %>
                    <% if (!string.IsNullOrEmpty(litLanguage.Text))
                        { %>
                    <tr>
                        <td>Language:</td>
                        <td style="color: #f58025">
                            <asp:Literal ID="litLanguage" runat="server"></asp:Literal></td>
                    </tr>
                    <% } %>
                </tbody>
            </table>
        </div>
        <asp:PlaceHolder ID="phAlreadyRejectedOrAdded" runat="server" Visible="false">
            <p style="color: #ff6600;">
                <asp:Literal ID="litStatus" runat="server"></asp:Literal>
            </p>
            <asp:PlaceHolder ID="phApprovedRejected" runat="server" Visible="false">
                <p style="color: #ff6600;">
                    The registration has been succesfully
                    <asp:Literal ID="litApprovedRejected" runat="server"></asp:Literal>. A notification email has been sent to the registrant.
                </p>
            </asp:PlaceHolder>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phRejectOrAdd" runat="server">
            <div class="clr"></div>
            <div style="width: 300px; border-right: 1px solid #aaa; float: left;">
                <p class="grey_head">REJECT</p>

                <p>REJECTION REASON/MESSAGE:</p>
                <asp:TextBox ID="txtRejectionReason" runat="server" TextMode="MultiLine" Width="250" Height="140"></asp:TextBox>
                <asp:PlaceHolder ID="phErrorRejected" runat="server" Visible="false">
                    <div style="color: #f00; width: 250px">
                        <asp:Literal ID="litErrorRejected" runat="server"></asp:Literal>
                    </div>
                </asp:PlaceHolder>
                <div class="bg_btn mt9">
                    <asp:LinkButton runat="server" ID="lnkSaveReject" CssClass="submit_bg_btn" Style="padding-left: 10px; margin-top: 10px; background-position: center; border-radius: 5px;" Text="Reject" OnClick="lnkSaveReject_Click">Reject</asp:LinkButton>
                </div>
            </div>
            <div class="inner" style="float: left;">
                <p class="grey_head">APPROVE</p>
                <div>
                    <asp:RadioButton ID="rbtnExisting" GroupName="group1" runat="server" /><span>ASSIGN NEW USER TO THIS EXISTING FINANCIAL INSTITUTION</span>
                    <div class="inner">
                        <div>
                            <asp:DropDownList ID="ddlFinancialInstitutions" runat="server" CssClass="chosen-choices" TabIndex="-1" Width="600"></asp:DropDownList>
                        </div>
                        <div class="inner">
                            <asp:CheckBox ID="chkAddDomain" runat="server" Checked="true" />
                        </div>
                    </div>
                </div>
                <div>
                    <asp:RadioButton ID="rbtnNew" GroupName="group1" runat="server" /><span>CREATE NEW FINANCIAL INSTITUTION</span><br />
                    <table class="inner">
                        <tbody>
                            <tr>
                                <td style="width: 250px;">Name<span style="color: #f00;">*</span></td>
                                <td style="width: 250px;">Domain Name(s)<span style="color: #f00;">*</span></td>
                                <td style="width: 100px;">ICA</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtFIName" runat="server" Width="245"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtFIDomain" runat="server" Width="245"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtFIICA" runat="server" Width="90"></asp:TextBox></td>
                            </tr>
                            <% if (CanChangeSegments)
                                { %>
                            <tr>
                                <td style="width: 180px;" colspan="4">Segmentation<% if (!CanHaveEmptySegments)
                                                                                      { %><span style="color: #f00;">*</span> <% } %></td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:ListBox runat="server" ID="ddlSegmentations" SelectionMode="Multiple" Width="440" CssClass="chosen-choices" TabIndex="-1" multiple="multiple" data-placeholder="Select Segmentation..." />
                                </td>
                            </tr>
                            <% } %>
                            <tr>
                                <td style="width: 180px;" colspan="4">Processor<% if (!CanHaveEmptyProcessors)
                                                                                   { %><span style="color: #f00;">*</span> <% } %></td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:DropDownList runat="server" ID="ddlProcessors" Width="440" CssClass="chosen-choices" TabIndex="-1" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <style>
                                        .issuers-search-box {
                                            background: #fbfbfb;
                                            border: 1px solid #e2e2e2;
                                            padding: 15px;
                                            margin: 15px 0;
                                            border-radius: 5px;
                                        }

                                        .issuers-search-header {
                                            margin: -15px -15px 15px -15px;
                                            padding: 10px 15px;
                                            border-bottom: 1px solid #e2e2e2;
                                            background-color: rgba(255,255,255,0.5);
                                        }

                                        .issuers-tbl-wrap {
                                            max-height: 200px;
                                            overflow: auto;
                                            margin: -17px;
                                            padding: 17px;
                                        }

                                        .issuers-search-box h4 {
                                            margin: 0;
                                            font-size: 12px;
                                        }

                                        .issuers-search-box .remove {
                                            display: inline-block;
                                            width: 18px;
                                            height: 18px;
                                            background: #ccc;
                                            text-decoration: none;
                                            line-height: 18px;
                                            text-align: center;
                                            color: #000;
                                            font-size: 14px;
                                        }

                                        .issuers-tbl {
                                            width: 100%;
                                            margin-top: 15px;
                                        }

                                            .issuers-tbl th {
                                                text-align: left;
                                            }

                                            .issuers-tbl th,
                                            .issuers-tbl td {
                                                padding: 5px;
                                            }

                                            .issuers-tbl td {
                                                border-top: 1px solid #e2e2e2;
                                                vertical-align: top;
                                            }

                                            .issuers-tbl label {
                                                white-space: nowrap;
                                            }

                                        .cf:before, .cf:after {
                                            content: " ";
                                            display: table;
                                        }

                                        .cf:after {
                                            clear: both;
                                        }
                                    </style>
                                    <div style="position: relative">
                                        <div class="issuers-search-box" style="position: relative; top: 0%">
                                            <h3 class="issuers-search-header">Match CID<span style="color: #f00;">*</span></h3>
                                            <div class="issuers-tbl-wrap">
                                                <h4>Issuer Search</h4>
                                                <div class="cf">
                                                    <asp:TextBox ID="txtSearchMatch" runat="server" Width="436" Style="float: left; margin-right: 15px;"></asp:TextBox>
                                                    <asp:LinkButton runat="server" ID="lnkSearchMatch" CssClass="submit_bg_btn" Style="padding-left: 10px; background-position: center; border-radius: 5px;" Text="Search" OnClick="lnkSearchMatch_Click">Search</asp:LinkButton>
                                                </div>
                                                <asp:Repeater ID="rptIssuerSearchGrid" runat="server" OnItemDataBound="rptIssuerSearchGrid_ItemDataBound">
                                                    <HeaderTemplate>
                                                        <table class="issuers-tbl">
                                                            <tr>
                                                                <th scope="col">CID</th>
                                                                <th scope="col">Issuer</th>
                                                                <th scope="col">Address</th>
                                                                <th scope="col">City</th>
                                                                <th scope="col">State</th>
                                                                <th scope="col">Country</th>
                                                                <th scope="col">Heatmap data</th>
                                                                <th scope="col"></th>
                                                            </tr>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%# Eval("Cid")%></td>
                                                            <td><%# Eval("LegalName")%></td>
                                                            <td><%# Eval("HqAddress")%></td>
                                                            <td><%# Eval("HqCityName")%></td>
                                                            <td><%# Eval("HqProvinceName")%></td>
                                                            <td><%# Eval("HqCountryName")%></td>
                                                            <td><%# Eval("HeatmapData")%></td>
                                                            <td>
                                                                <label>
                                                                    <input id="rbtnMatch" runat="server" type="radio" name="searchBy" class="searchBy" value='<%#Eval("Cid") %>' style="position: relative; top: 3px;" />Match
                                                                </label>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                <asp:Panel ID="pnlIssuerWithoutCID" runat="server">
                                                    <label>
                                                        <input id="rbtnIssuerWithoutCID" runat="server" type="radio" name="searchBy" class="searchBy" value="NoCid" style="position: relative; top: 3px; left: -5px;" />create issuer without CID
                                                    </label>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="inner" style="height: 50px;">
                    <div class="bg_btn mt9">
                        <asp:LinkButton runat="server" ID="lnkSaveApprove" CssClass="submit_bg_btn" Style="padding-left: 10px; margin-top: 10px; background-position: center; border-radius: 5px;" Text="Approve" OnClick="lnkSaveApprove_Click">Approve</asp:LinkButton>
                    </div>
                    <div class="bg_btn mt9">
                        <a href="~/" runat="server" id="lnkCancel" class="submit_bg_btn" style="padding-left: 10px; margin-top: 10px; background-position: center; border-radius: 5px; float: right;" target="_parent">Cancel</a>
                    </div>
                </div>
                <asp:PlaceHolder ID="phError" runat="server" Visible="false">
                    <div class="inner" style="color: #f00;">
                        <asp:Literal ID="litError" runat="server"></asp:Literal>
                    </div>
                </asp:PlaceHolder>
            </div>
        </asp:PlaceHolder>
    </div>
    <script type="text/javascript" src="/admin/Content/js/inspinia.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('input[type=radio]').click(function () {
                var cname = $(this).attr('class');

                $('.' + cname).each(function () {
                    $(this).prop('checked', false);
                });

                $(this).prop('checked', true);
            });
        });
    </script>
</asp:Content>
