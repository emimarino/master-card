﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebForms/WebForms.Master" AutoEventWireup="true" CodeBehind="PendingRegistrationsReport.aspx.cs"
    Inherits="Mastercard.MarketingCenter.Cms.WebForms.PendingRegistrationsReport" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../Content/legacy/js/pendingRegistrationsReport.js" type="text/javascript"></script>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
        <div style="background-color: #ffffff; width: 100%; height: 100%"></div>
    </telerik:RadAjaxLoadingPanel>
    <asp:PlaceHolder ID="phParentRedirect" runat="server" Visible="false">
        <script type="text/javascript">
            window.parent.location = '<%= PendingRegistrationUrl %>';
        </script>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phMain" runat="server">
        <div class="clr" style="margin-bottom: 10px;">
            <asp:CheckBox ID="chkShowRejectedRegistrations" runat="server" Text="Show Rejected Registrations" AutoPostBack="true" OnCheckedChanged="chkShowRejectedRegistrations_CheckedChanged"></asp:CheckBox>
        </div>
        <telerik:RadGrid ID="trgReport" Width="800" AllowSorting="True" PageSize="15" AllowPaging="True" runat="server" GridLines="None" AutoGenerateColumns="False" OnNeedDataSource="trgReport_NeedDataSource" OnPreRender="trgReport_PreRender" AllowFilteringByColumn="true" ShowGroupPanel="true" OnItemCommand="trgReport_ItemCommand">
            <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
                <PagerStyle Mode="NextPrevNumericAndAdvanced" />
                <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false" />
                <Columns>
                    <telerik:GridDateTimeColumn HeaderText="Date Registered" DataField="DateAdded" DataFormatString="{0:MM/dd/yyyy}" DataType="System.DateTime" HtmlEncode="false" FilterControlWidth="68"></telerik:GridDateTimeColumn>
                    <telerik:GridBoundColumn HtmlEncode="true" HeaderText="First Name" DataField="FirstName" FilterControlWidth="65"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HtmlEncode="true" HeaderText="Last Name" DataField="LastName" FilterControlWidth="65"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HtmlEncode="true" HeaderText="Email" DataField="Email" Visible="false" FilterControlWidth="178"></telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Email" HeaderStyle-Width="200" SortExpression="Email" UniqueName="EmailLink" DataField="Email">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"GUID") %>' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"Email") %>' runat="server" />
                        </ItemTemplate>
                        <ItemStyle CssClass="rad_grid_hyperlink" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn HtmlEncode="true" HeaderText="Country" Visible="false" DataField="Country" FilterControlWidth="65"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HtmlEncode="true" HeaderText="FI Name" DataField="FinancialInstitution" FilterControlWidth="65"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HtmlEncode="true" HeaderText="Processor" DataField="Processor" FilterControlWidth="65"></telerik:GridBoundColumn>
                    <telerik:GridDateTimeColumn HeaderText="Rejected Date" DataField="DateStatusChanged" DataFormatString="{0:MM/dd/yyyy}" DataType="System.DateTime" HtmlEncode="false" FilterControlWidth="68" UniqueName="RejectedDate"></telerik:GridDateTimeColumn>
                    <telerik:GridTemplateColumn HeaderText="Rejected By" HeaderStyle-Width="200" SortExpression="StatusChangedBy" UniqueName="StatusChangedBy" DataField="StatusChangedBy">
                        <ItemTemplate>
                            <asp:Label ID="StatusChangeByLabel" Text='<%# DataBinder.Eval(Container.DataItem,"StatusChangedBy") %>' runat="server" />
                        </ItemTemplate>
                        <ItemStyle CssClass="rad_grid_hyperlink" />
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
                <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
            </ClientSettings>
            <PagerStyle Mode="NextPrevAndNumeric" />
            <HeaderStyle Width="100px" />
            <ExportSettings ExportOnlyData="true" IgnorePaging="true">
                <Pdf PageLeftMargin="0.1in" />
            </ExportSettings>
            <GroupingSettings ShowUnGroupButton="true" CaseSensitive="false" />
        </telerik:RadGrid>
        <asp:PlaceHolder ID="phNoResults" runat="server" Visible="false">
            <p><em>There are no pending registrations.</em></p>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phNoResultsRejected" runat="server" Visible="false">
            <p><em>There are no rejected registrations.</em></p>
        </asp:PlaceHolder>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phLoading" runat="server" Visible="false">
        <p>Loading...</p>
    </asp:PlaceHolder>
</asp:Content>
