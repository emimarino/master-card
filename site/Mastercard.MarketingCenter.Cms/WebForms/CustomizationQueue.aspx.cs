﻿using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Cms.WebForms
{
    public partial class CustomizationQueue : System.Web.UI.Page
    {
        IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                Session.Remove("OrderOnCustomizationQueue");
                GetOrderOnCustomisationQueue();
            }
        }

        protected void GetOrderOnCustomisationQueue()
        {
            List<RetrieveOrdersByStatusResult> ls = _orderService.GetOrderByStatus("UnApproved");
            if (ls.Count > 0)
            {
                lblNoRecords.Visible = false;
                rptorder.Visible = true;
                rptorder.DataSource = ls;
                rptorder.DataBind();
            }
            else
            {
                rptorder.Visible = false;
                lblNoRecords.Visible = true;
            }
        }

        protected void lnkHeader_Command(object sender, CommandEventArgs e)
        {
            List<RetrieveOrdersByStatusResult> orders = null;
            if (Session["OrderOnCustomizationQueue"] != null)
            {
                orders = (List<RetrieveOrdersByStatusResult>)Session["OrderOnCustomizationQueue"];
            }
            else
            {
                orders = _orderService.GetOrderByStatus("UnApproved");
            }

            if (orders.Count > 0 && e.CommandName == "Sort")
            {
                string sortExpression = String.Empty;
                if (ViewState["SortExpression"] != null)
                {
                    sortExpression = Convert.ToString(ViewState["SortExpression"]);
                }

                switch (e.CommandArgument.ToString())
                {
                    case "OrderNumber":
                        if (sortExpression == "OrderNumber ASC")
                        {
                            orders = orders.OrderByDescending(i => i.OrderNumber).ToList();
                            ViewState["SortExpression"] = "OrderNumber DESC";
                        }
                        else
                        {
                            orders = orders.OrderBy(i => i.OrderNumber).ToList();
                            ViewState["SortExpression"] = "OrderNumber ASC";
                        }
                        break;
                    case "Date":
                        if (sortExpression == "OrderDate ASC")
                        {
                            orders = orders.OrderByDescending(i => i.OrderDate).ToList();
                            ViewState["SortExpression"] = "OrderDate DESC";
                        }
                        else
                        {
                            orders = orders.OrderBy(i => i.OrderDate).ToList();
                            ViewState["SortExpression"] = "OrderDate ASC";
                        }
                        break;
                    case "Name":
                        if (sortExpression == "Name ASC")
                        {
                            orders = orders.OrderByDescending(i => i.FullName).ToList();
                            ViewState["SortExpression"] = "Name DESC";
                        }
                        else
                        {
                            orders = orders.OrderBy(i => i.FullName).ToList();
                            ViewState["SortExpression"] = "Name ASC";
                        }
                        break;
                    case "Processor":
                        if (sortExpression == "ProcessorName ASC")
                        {
                            orders = orders.OrderByDescending(i => i.ProcessorName).ToList();
                            ViewState["SortExpression"] = "ProcessorName DESC";
                        }
                        else
                        {
                            orders = orders.OrderBy(i => i.ProcessorName).ToList();
                            ViewState["SortExpression"] = "ProcessorName ASC";
                        }
                        break;
                    case "Issuer":
                        if (sortExpression == "IssuerName ASC")
                        {
                            orders = orders.OrderByDescending(i => i.IssuerName).ToList();
                            ViewState["SortExpression"] = "IssuerName DESC";
                        }
                        else
                        {
                            orders = orders.OrderBy(i => i.IssuerName).ToList();
                            ViewState["SortExpression"] = "IssuerName ASC";
                        }
                        break;
                    case "Status":
                        if (sortExpression == "OrderStatus ASC")
                        {
                            orders = orders.OrderByDescending(i => i.OrderStatus).ToList();
                            ViewState["SortExpression"] = "OrderStatus DESC";
                        }
                        else
                        {
                            orders = orders.OrderBy(i => i.OrderStatus).ToList();
                            ViewState["SortExpression"] = "OrderStatus ASC";
                        }
                        break;
                }

                rptorder.DataSource = orders;
                rptorder.DataBind();
            }
        }
    }
}