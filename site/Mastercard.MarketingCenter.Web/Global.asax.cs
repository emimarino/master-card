﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Web.Core.Services;
using Pulsus;
using Slam.Cms.Configuration;
using StackExchange.Profiling;
using System;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Routing;

namespace Mastercard.MarketingCenter.DefaultMvc3
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private readonly bool _profilerEnabled = bool.Parse(WebConfigurationManager.AppSettings["ProfilerEnabled"]);
        private bool _isInApplicableRegion;

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapPageRoute("Home", "", "~/Home.aspx");

            routes.MapRoute("Default", // Route name
                            "{controller}/{action}/{id}", // URL with parameters
                            new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        protected void Application_Start()
        {
            MvcHandler.DisableMvcResponseHeader = true;
            Bootstrapper.Execute();
        }

        void Application_Error(object sender, EventArgs e)
        {
            var ex = Server.GetLastError();
            LogManager.EventFactory.Create()
                        .Text($"Error executing in application level...")
                        .Level(LoggingEventLevel.Error)
                        .AddData("Exception Message", ex.Message)
                        .AddData("Exception InnerException", ex.InnerException)
                        .AddData("Exception Data", ex.Data)
                        .AddData("Exception StackTrace", ex.StackTrace)
                        .AddData("Path", Request.Path)
                        .Push();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            try
            {
                var app = sender as HttpApplication;
                if (app != null && app.Context != null)
                {
                    app.Context.Response.Headers.Remove("Server");
                }

                const string parametersTosanitize = "ReturnUrl,Source,TrackingType,Referer";
                Request.IsUrlInParametersInternal(parametersTosanitize).RejectInvalidCharactersInParameters(parametersTosanitize);
            }
            catch (Exception ex)
            {
                string defaultValue = string.IsNullOrEmpty(WebConfigurationManager.AppSettings["AlternativeErrorRedirectionUrl"]) ? null : WebConfigurationManager.AppSettings["AlternativeErrorRedirectionUrl"].ToString();
                Context.RedirectError("/portal/error", ex, defaultValue, true);
            }

            ManageSsl();

            if (_profilerEnabled)
            {
                MiniProfiler.Start();
            }

            SetCurrentCulture();
        }

        protected void Application_PostAuthenticateRequest()
        {
            using (MiniProfiler.StepStatic("Application_PostAuthenticateRequest"))
            {
                _isInApplicableRegion = EnsureUserIsInApplicableRegion();
                var principal = DependencyResolver.Current.GetService<IPrincipal>();
                if (principal != null)
                {
                    Context.User = principal;
                }
            }
        }

        private bool EnsureUserIsInApplicableRegion()
        {
            var userContext = DependencyResolver.Current.GetService<UserContext>();
            if (userContext != null && Request.IsAuthenticated)
            {
                if (!ConfigurationManager.Environment.Settings["ApplicableRegions"].Contains(userContext.SelectedRegion))
                {
                    var uri = new Uri(Context.Items.Contains("OriginalRequestUrl") ? Context.Items["OriginalRequestUrl"].ToString() : Request.Url.ToString());
                    Response.Redirect(new Uri(ConfigurationManager.Solution.Environments[ConfigurationManager.Environment.Settings["InApplicableRegionEnvironment"]].FrontEndUrl).GetLeftPart(UriPartial.Authority).TrimEnd('/') + "/" + uri.PathAndQuery.TrimStart('/'));

                    return false;
                }

                return true;
            }

            return false;
        }

        private void SetCurrentCulture()
        {
            var userContext = DependencyResolver.Current.GetService<UserContext>();
            if (userContext != null)
            {
                RegionalizeService.SetCurrentCulture(userContext.SelectedRegion, userContext.SelectedLanguage);
            }
        }

        protected void Application_EndRequest()
        {
            if (_profilerEnabled)
            {
                MiniProfiler.Stop();
            }
        }

        protected void ManageSsl()
        {
            try
            {
                var frontEndUrl = new Uri(ConfigurationManager.Environment.FrontEndUrl);
                if (frontEndUrl.Authority.Equals(Request.Url.Authority, StringComparison.InvariantCultureIgnoreCase) &&
                    frontEndUrl.Scheme.Equals("https", StringComparison.InvariantCultureIgnoreCase))
                {
                    DependencyResolver.Current.GetService<RedirectionService>().RequireSsl();
                }
            }
            catch
            {
                throw new Exception("Could not verify or redirect url or for ssl verification.");
            }
        }
    }
}