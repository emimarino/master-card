﻿<%@ Page Language="C#" MasterPageFile="~/WebFormsPages/MMPShoppingCart.Master" AutoEventWireup="true" Inherits="ProofReview" Title="Order Detail" CodeBehind="ProofReview.aspx.cs" %>

<%@ Register TagPrefix="uc" TagName="CustomerOrderDetail" Src="~/UserControls/CustomerOrderDetail.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <uc:CustomerOrderDetail runat="server" />
</asp:Content>