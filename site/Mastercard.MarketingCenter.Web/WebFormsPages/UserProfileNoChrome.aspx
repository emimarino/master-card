﻿<%@ Page Language="C#" MasterPageFile="~/WebFormsPages/MMP.minimal.master" ErrorPage="~/WebFormsPages/ErrorNoChrome.aspx" AutoEventWireup="true" CodeBehind="UserProfileNoChrome.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.WebFormsPages.UserProfileNoChrome" %>

<%@ Register Src="~/UserControls/UserProfile.ascx" TagName="UserProfile" TagPrefix="uc1" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <uc1:UserProfile runat="server" />
</asp:Content>