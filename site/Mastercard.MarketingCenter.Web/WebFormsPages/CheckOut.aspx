﻿<%@ Page Language="C#" MasterPageFile="~/WebFormsPages/MMPShoppingCart.Master" AutoEventWireup="true"
    Inherits="CheckOut" Title="Customize and Check Out" CodeBehind="CheckOut.aspx.cs" %>

<%@ Register Src="~/UserControls/ImageUpload.ascx" TagName="ImageUpload" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="center_container">
        <div class="form_container">
            <div class="orange_heading_profile">
                <%=Resources.ShoppingCart.CheckOut_Title%>
            </div>
            <p class="none_padding">
                <%=Resources.ShoppingCart.CheckOut_Description%>
            </p>
            <asp:PlaceHolder ID="placeHolderMessage" runat="server" Visible="false">
                <div class="roundcont_error mrt_25">
                    <div class="roundtop">
                        <img src="/portal/inc/images/tl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
                    </div>
                    <div style="float: left; width: 80px; text-align: center;">
                        <img src="/portal/inc/images/error_img.gif" border="0px" alt="" width="40" height="40" />
                    </div>
                    <div style="float: left; width: 800px;">
                        <asp:Label ID="lblError" runat="server" CssClass="span_1" Visible="true"></asp:Label>
                    </div>
                    <div style="clear: both;"></div>
                    <div class="roundbottom">
                        <img src="/portal/inc/images/bl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
                    </div>
                </div>
            </asp:PlaceHolder>
            <div class="faq_right_container mrt_25">
                <div class="right_tab_top">
                    <div class="orange_heading_main">
                        <%=Resources.ShoppingCart.CustomizationGuide%>
                    </div>
                </div>
                <div class="right_tab_bg">
                    <p class="none_padding">
                        <%=Resources.ShoppingCart.CustomizationGuideInstructions%>
                    </p>
                    <asp:Repeater runat="server" ID="rptItems">
                        <ItemTemplate>
                            <div class="link_img">
                                <div class="content_span">
                                    <a target="new" href='<%# Url.Action("LibraryDownload", "File", new { url = DataBinder.Eval(Container.DataItem, "PDFPreview") })%>'>
                                        <%# Eval("Title")%></a>
                                </div>
                                <div class="image_span">
                                    <a target="new" href='<%# Url.Action("LibraryDownload", "File", new { url = DataBinder.Eval(Container.DataItem, "PDFPreview") })%>'>
                                        <img src="/portal/inc/images/pdf_icon.gif" border="0" alt="" width="25" height="27" /></a>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="right_tab_bot">
                </div>

                <p>&nbsp;</p>
                <div class="right_tab_top">
                    <div class="orange_heading_main">
                        <%=Resources.ShoppingCart.ArtworkRequirements%>
                    </div>
                </div>
                <div class="right_tab_bg">
                    <p class="none_padding">
                        <%=Resources.ShoppingCart.ArtworkRequirementsInstructions%>
                    </p>
                </div>
                <div class="right_tab_bot">
                </div>
            </div>
            <div class="faq_blog mrt_25">
                <div class="main_heading">
                    <div class="orange_heading_main">
                        <%=Resources.ShoppingCart.CheckOut_Subtitle%>
                    </div>
                </div>
                <div class="main_heading_bg">
                    <asp:Repeater runat="server" ID="rptCustomizations">
                        <ItemTemplate>
                            <div class="chk_outgrid">
                                <div class="right_grid">
                                    <div>
                                        <asp:PlaceHolder ID="PlaceHolderimg" runat="server" Visible='<%#(Eval("FieldType").ToString().ToLower() == "image") ? true : false %>'>
                                            <uc1:ImageUpload ID="imgUpload" runat="server" Value='<%#(Eval("FieldType").ToString().ToLower() == "image") ? Eval("DefaultCustomizationData") : string.Empty %>' />
                                        </asp:PlaceHolder>
                                        <asp:PlaceHolder ID="PlaceHoldertxt" runat="server" Visible='<%#(Eval("FieldType").ToString().ToLower() == "image")?false : true %>'>
                                            <asp:TextBox runat="server" ID="txtOptionsValue" MaxLength='<%#  ((Eval("MaxLength")!=null && Eval("MaxLength").ToString().Trim().Length>0) ? Convert.ToInt32(Eval("MaxLength").ToString()) : 0) %>' Height='<%# (Eval("FieldType").ToString().ToLower() == "multi line text")?75 : 12 %>' TextMode='<%#GetTextBoxMode(Eval("FieldType").ToString()) %>' onkeydown='<%# (Eval("FieldType").ToString().ToLower()=="integer") ? "return ForceNumericInput(event,false,false);" : "" %>' CssClass="input_area text_nav" Text='<%#Eval("DefaultCustomizationData") %>'></asp:TextBox>
                                        </asp:PlaceHolder>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <p>
                                        <%# Eval("Instructions") == null ? "" : Eval("Instructions").ToString() + " " %>(Used for: <%# Eval("ApplicableItems").ToString() %>)
                                    </p>
                                </div>
                                <asp:Label runat="server" ID="FieldType" Text='<%# Eval("FieldType") %>' Visible="false"></asp:Label>
                                <asp:Label runat="server" ID="CustomizationOptionItemID" Text='<%# Eval("CustomizationOptionID") %>' Visible="false"></asp:Label>
                                <asp:Label runat="server" ID="FriendlyName" Text='<%# Eval("FriendlyName") %>' Visible="false"></asp:Label>
                                <asp:Label runat="server" ID="Required" Text='<%# Eval("Required") %>' Visible="false"></asp:Label>
                                <div class="heading_span">
                                    <%# Eval("FriendlyName") %><%#  ((Eval("Required").ToString().ToLower()=="true") ? "*" : "") %>
                                </div>
                            </div>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <div class="chk_outgrid gray_backgrnd">
                                <div class="right_grid">
                                    <div>
                                        <asp:PlaceHolder ID="PlaceHolderimg" runat="server" Visible='<%#(Eval("FieldType").ToString().ToLower() == "image") ? true : false %>'>
                                            <uc1:ImageUpload ID="imgUpload" runat="server" Value='<%#(Eval("FieldType").ToString().ToLower() == "image") ? Eval("DefaultCustomizationData") : string.Empty %>' />
                                        </asp:PlaceHolder>
                                        <asp:PlaceHolder ID="PlaceHoldertxt" runat="server" Visible='<%#(Eval("FieldType").ToString().ToLower() == "image")?false : true %>'>
                                            <asp:TextBox runat="server" ID="txtOptionsValue" MaxLength='<%#  ((Eval("MaxLength")!=null && Eval("MaxLength").ToString().Trim().Length>0) ? Convert.ToInt32(Eval("MaxLength").ToString()) : 0) %>' Height='<%# (Eval("FieldType").ToString().ToLower() == "multi line text")?75 : 12 %>' TextMode='<%#GetTextBoxMode(Eval("FieldType").ToString()) %>' onkeydown='<%# (Eval("FieldType").ToString().ToLower()=="integer") ? "return ForceNumericInput(event,false,false);" : "" %>' CssClass="input_area text_nav" Text='<%#Eval("DefaultCustomizationData") %>'></asp:TextBox>
                                        </asp:PlaceHolder>
                                    </div>
                                    <div class="clr">
                                    </div>
                                    <p>
                                        <%# Eval("Instructions") == null ? "" : Eval("Instructions").ToString() + " " %>(Used for: <%# Eval("ApplicableItems").ToString() %>)
                                    </p>
                                </div>
                                <asp:Label runat="server" ID="FieldType" Text='<%# Eval("FieldType") %>' Visible="false"></asp:Label>
                                <asp:Label runat="server" ID="CustomizationOptionItemID" Text='<%# Eval("CustomizationOptionID") %>' Visible="false"></asp:Label>
                                <asp:Label runat="server" ID="FriendlyName" Text='<%# Eval("FriendlyName") %>' Visible="false"></asp:Label>
                                <asp:Label runat="server" ID="Required" Text='<%# Eval("Required") %>' Visible="false"></asp:Label>
                                <div class="heading_span">
                                    <%# Eval("FriendlyName") %><%#  ((Eval("Required").ToString().ToLower()=="true") ? "*" : "") %>
                                </div>
                            </div>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="main_heading_bot">
                </div>
                <div class="cart_requried">
                    * Required field
                </div>
                <div class="bg_btn fr mt9">
                    <asp:LinkButton runat="server" ID="lnkubmit" CssClass="submit_bg_btn" Text="<%$Resources:Shared,Continue%>" OnClick="lnkubmit_Click"><%=Resources.Shared.Continue%> &gt;</asp:LinkButton>
                </div>
                <div class="bg_btn_left fr mr11">
                    <asp:LinkButton ID="btnCancel" runat="server" Text="<%$Resources:Shared,Back%>" CssClass="submit_bg_btn" OnClick="btnCancel_Click"> &lt; <%=Resources.Shared.Back%></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>