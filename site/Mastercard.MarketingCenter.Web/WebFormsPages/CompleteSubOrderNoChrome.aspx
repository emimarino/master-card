﻿<%@ Page Language="C#" MasterPageFile="~/WebFormsPages/MMP.minimal.master" AutoEventWireup="true" CodeBehind="CompleteSubOrderNoChrome.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.WebFormsPages.CompleteSubOrderNoChrome" %>

<%@ Register TagPrefix="mc" TagName="CompleteSubOrder" Src="~/UserControls/CompleteSubOrder.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <mc:CompleteSubOrder ID="completeSubOrder" runat="server"></mc:CompleteSubOrder>
</asp:Content>