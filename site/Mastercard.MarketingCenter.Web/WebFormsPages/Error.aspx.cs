﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using System;
using System.Web.UI;

public partial class Error : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.Page.Title = Resources.Errors.PageError;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.RouteData.Values["code"] != null)
        {
            switch (Page.RouteData.Values["code"].ToString())
            {
                case Constants.ErrorCodes.InvalidOrder:
                    phInvalidOrderError.Visible = true;
                    break;
                case Constants.ErrorCodes.AccessDenied:
                    phAccessDeniedError.Visible = true;
                    break;
                default:
                    phGeneralError.Visible = true;
                    break;
            }
        }
        else
        {
            phGeneralError.Visible = true;
        }
    }
}