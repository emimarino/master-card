<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head id="Head1">
    <link href="/portal/inc/styles/style.css" rel="stylesheet" type="text/css" />
    <script type='text/javascript' src='/portal/inc/scripts/videoplayer/swfobject.js?v=2'></script>
    <title></title>
</head>
<body>
    <div id="Video"></div>
    <script type="text/javascript">
        var flashvars = {
            smoothing: "false",
            file: "CustomersPlusCarolyn6.mov",
            provider: "rtmp",
            streamer: "rtmp://s24w6z9cizy0mf.cloudfront.net/cfx/st"
        };
        var params = {
            allowfullscreen: "true",
            allowscriptaccess: "always",
            wmode: "opaque"
        };
        var attributes = {
            id: "Video",
            name: "Video"
        };

        swfobject.embedSWF("/portal/inc/scripts/videoplayer/player-licensed.swf", "Video", "720", "500", "9", false, flashvars, params, attributes);
    </script>
</body>
</html>