﻿<%@ Page Language="C#" MasterPageFile="~/WebFormsPages/MMPShoppingCart.Master" AutoEventWireup="True" Inherits="OrderConfirmation" Title="Untitled Page" CodeBehind="OrderConfirmation.aspx.cs" %>

<%@ Register Assembly="AWS.Web.UI.WebControls" Namespace="AWS.Web.UI.WebControls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="center_container">
        <div class="form_container">
            <div class="orange_heading_profile">
                <%=Resources.ShoppingCart.OrderConfirmation_Title%>
            </div>
            <div class="order_conformation">
                <div class="order_detail">
                    <span>
                        <asp:Label ID="lblOrderNo" runat="server"></asp:Label></span>
                    <span>
                        <asp:Label ID="lblOrderDate" runat="server"></asp:Label></span>
                </div>
            </div>
            <div class="clr">
            </div>
            <div class="faq_right_container mrt_166">
                <div class="right_tab_top">
                    <div class="orange_heading_main">
                        <%=Resources.ShoppingCart.ItemTypes%>
                    </div>
                </div>
                <div class="right_tab_bg">
                    <p class="none_padding">
                        <%=Resources.ShoppingCart.ItemTypesInstructions%>
                    </p>
                    <div class="link_img">
                        <div class="content_span"><%=Resources.ShoppingCart.Customizable%></div>
                        <div class="image_span">
                            <img src="/portal/inc/images/document_edit.png" border="0" alt="" width="25" height="27" />
                        </div>
                    </div>
                    <div class="link_img">
                        <div class="content_span"><%=Resources.ShoppingCart.MastercardBrandedCollateral%></div>
                        <div class="image_span">
                            <img src="/portal/inc/images/document_lock.png" border="0" alt="" width="25" height="27" />
                        </div>
                    </div>
                    <div class="link_img">
                        <div class="content_span"><%=Resources.ShoppingCart.VariableDataPrinting%></div>
                        <div class="image_span">
                            <img src="/portal/inc/images/vdp.png" border="0" alt="" width="25" height="27" />
                        </div>
                    </div>
                    <div class="link_img">
                        <div class="content_span"><%=Resources.ShoppingCart.ElectronicDelivery%></div>
                        <div class="image_span">
                            <img src="/portal/inc/images/document_down.png" border="0" alt="" width="25" height="27" />
                        </div>
                    </div>
                </div>
                <div class="right_tab_bot">
                </div>
            </div>
            <div class="faq_blog">
                <div class="roundcont_order mrt20">
                    <div class="roundtop">
                        <img src="/portal/inc/images/tl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
                    </div>
                    <p>
                        <strong><%=Resources.ShoppingCart.ImportantNote%></strong><br />
                        <%=Resources.ShoppingCart.OrderConfirmation_ImportantNote%>
                        <br />
                        <br />
                        <span id="noteForCustomizations" runat="server">
                            <%=Resources.ShoppingCart.OrderConfirmation_NoteForCustomizations%>
                        </span>
                        <span id="noteForShipping" runat="server">
                            <%=Resources.ShoppingCart.OrderConfirmation_NoteForShipping%>
                        </span>
                        <span id="noteForApprovalPOD" runat="server">
                            <%=Resources.ShoppingCart.OrderConfirmation_NoteForApprovalPOD%>
                        </span>
                    </p>
                    <div class="roundbottom">
                        <img src="/portal/inc/images/bl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
                    </div>
                </div>
                <div class="clr">
                </div>
                <div class="faq_blog">
                    <!-- grid_container-->
                    <div class="fr mrt_25"></div>
                    <asp:GridView ID="grdShoppingCart" CssClass="main_heading_bg mrt_25" runat="server" AutoGenerateColumns="false" BorderStyle="None" GridLines="None" OnRowDataBound="grdShoppingCart_ItemDataBound">
                        <Columns>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    <div class="main_heading">
                                        <span class="order_heading width110 pdl_15px pdr_15px"><%=Resources.ShoppingCart.Sku.ToUpperInvariant()%></span>
                                        <span class="order_heading width150"><%=Resources.ShoppingCart.Title%></span>
                                        <span class="order_heading" style="width: 50px;">&nbsp;</span>
                                        <span class="order_heading text_right pdr_25px"><%=Resources.ShoppingCart.Quantity%></span>
                                        <span class="order_heading"><%=Resources.ShoppingCart.Type%></span>
                                        <span class="order_heading width100 text_right"><%=Resources.ShoppingCart.Price%></span>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="chk_outgrid ">
                                        <span class="order_heading_data width110 pdl_15px pdr_15px word_wrap">
                                            <%# Eval("SKU").ToString()%></span>
                                        <span class="order_heading_data width150">
                                            <%# Eval("ItemName").ToString()%></span>
                                        <span class="order_heading_data" style="width: 50px;">&nbsp;</span>
                                        <span class="order_heading_data text_right pdr_25px">
                                            <asp:Label runat="server" ID="txtQuantity" Text='<%# Eval("ItemQuantity").ToString()%>' readonly="true" size="20"></asp:Label></span>
                                        <span class="order_heading_data">
                                            <asp:Image ID="imgIcon" runat="server" /></span>
                                        <span class="order_heading_data width100 text_right">
                                            <%# FormatNumber(Convert.ToDecimal(GetPrice(Eval("ItemPrice").ToString())))%></span>
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:Panel ID="pnlPromotion" class="main_heading_bg promotion" runat="server" Visible="false">
                        <asp:Panel ID="pnlPromotionApplied" runat="server" CssClass="chk_outgrid">
                            <div class="promo-description">
                                <asp:Literal ID="litPromotionDescription" runat="server"></asp:Literal>
                            </div>
                            <div class="promo-amount">
                                <asp:Literal ID="litPromotionAmount" runat="server"></asp:Literal>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                    <div class="main_heading_bg">
                        <div class="chk_outgrid" style="border-top: 1px solid #aaa; margin-left: 15px; margin-right: 15px; width: 648px;">
                            <asp:PlaceHolder ID="phBillingICA" runat="server">
                                <div class="billing_ica">
                                    <%=Resources.ShoppingCart.BillingIca%>:
                                    <asp:Literal ID="litBillingICA" runat="server"></asp:Literal>
                                </div>
                            </asp:PlaceHolder>
                            <div class="total_span fr">
                                <%=Resources.ShoppingCart.Total%>:
                        <asp:Label ID="lblTotal" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="meaning_bot">
                    </div>
                    <div id="pnlGuideToBenefitsNote" runat="server" class="requried">
                        * <%=Resources.ShoppingCart.GuideToBenefitsDetails%>
                    </div>
                    <div id="pnlShippingNote" runat="server" class="requried">
                        * <%=Resources.ShoppingCart.ShippingHandlingDetails%>
                    </div>
                    <!-- grid_container-->
                    <asp:PlaceHolder ID="phEstimatedDistribution" runat="server">
                        <div class="fr mrt_25" style="padding-bottom: 3px;">&nbsp;</div>
                        <asp:GridView ID="grdEstimatedDistribution" CssClass="main_heading_bg" runat="server" AutoGenerateColumns="false" BorderStyle="None" GridLines="None">
                            <Columns>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                                    <HeaderTemplate>
                                        <div class="main_heading">
                                            <span class="order_heading width110 pdl_15px pdr_15px"><%=Resources.ShoppingCart.Sku.ToUpperInvariant()%></span>
                                            <span class="order_heading width150"><%=Resources.ShoppingCart.Title%></span>
                                            <span class="order_heading width30">&nbsp;</span>
                                            <span class="order_heading width205"><%=Resources.ShoppingCart.EstimatedDistribution%></span>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="chk_outgrid ">
                                            <span class="order_heading_data width110 pdl_15px pdr_15px word_wrap"><%# Eval("SKU").ToString()%></span>
                                            <span class="order_heading_data width150"><%# Eval("ItemName").ToString()%></span>
                                            <span class="order_heading_data width30">&nbsp;</span>
                                            <span class="order_heading_data text_right width56" style="padding-right: 25px;">
                                                <asp:Label runat="server" ID="txtEstimatedDistribution" Text='<%# Eval("EstimatedDistribution")%>' readonly="true" size="20"></asp:Label></span>
                                        </div>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <div class="chk_outgrid gray_backgrnd">
                                            <span class="order_heading_data width110 pdl_15px pdr_15px word_wrap"><%# Eval("SKU").ToString()%></span>
                                            <span class="order_heading_data width150"><%# Eval("ItemName").ToString()%></span>
                                            <span class="order_heading_data width30">&nbsp;</span>
                                            <span class="order_heading_data text_right width56" style="padding-right: 25px;">
                                                <asp:Label runat="server" ID="txtEstimatedDistribution" Text='<%# Eval("EstimatedDistribution")%>' readonly="true" size="20"></asp:Label></span>
                                        </div>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                    </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <div class="meaning_bot">
                        </div>
                    </asp:PlaceHolder>
                    <!-- grid_container-->
                    <asp:PlaceHolder ID="placeCustomizationOptions" runat="server">
                        <div class="fr mrt_25"></div>
                        <div class="main_heading mrt_25">
                            <div class="orange_heading_main">
                                <%=Resources.ShoppingCart.CustomizationDetails%>
                            </div>
                        </div>
                        <div class="main_heading_bg">
                            <asp:Repeater runat="server" ID="rptCustomizations">
                                <ItemTemplate>
                                    <div class="chk_outgrid">
                                        <div class="custmize_span">
                                            <%# Eval("FriendlyName") %><%#  ((Eval("Required")!=null &&  Eval("Required").ToString().ToLower()=="true") ? "*" : "") %>
                                        </div>
                                        <div class="right_grid_custmize">
                                            <div>
                                                <asp:PlaceHolder ID="PlaceHolderimg" runat="server" Visible='<%#(Eval("FieldType").ToString().ToLower() == "image")?true : false %>'>
                                                    <img id="image_cust" runat="server" height="131" width="128" src='<%# ((Eval("DefaultCustomizationData")!=null)) ? _urlService.GetStaticImageURL(Eval("DefaultCustomizationData").ToString(),200,200,false,85,true) : "" %>' alt="" />
                                                </asp:PlaceHolder>
                                                <asp:PlaceHolder ID="PlaceHoldertxt" runat="server" Visible='<%#(Eval("FieldType").ToString().ToLower() == "image")?false : true %>'>
                                                    <asp:Label runat="server" ID="txtOptionsValue" CssClass="order_heading_data width_auto text_align_justify"
                                                        Text='<%# (Eval("DefaultCustomizationData")!=null) ? Eval("DefaultCustomizationData").ToString().Replace("\r\n","<br/>") : "" %>'></asp:Label>
                                                </asp:PlaceHolder>
                                            </div>
                                            <div class="clr">
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <div class="chk_outgrid gray_backgrnd">
                                        <div class="custmize_span">
                                            <%# Eval("FriendlyName") %><%#  (( Eval("Required") !=null && Eval("Required").ToString().ToLower()=="true") ? "*" : "") %>
                                        </div>
                                        <div class="right_grid_custmize">
                                            <div>
                                                <asp:PlaceHolder ID="PlaceHolderimg" runat="server" Visible='<%#(Eval("FieldType").ToString().ToLower() == "image")?true : false %>'>
                                                    <img id="image_cust" runat="server" height="131" width="128" src='<%# ((Eval("DefaultCustomizationData")!=null)) ? _urlService.GetStaticImageURL(Eval("DefaultCustomizationData").ToString(),200,200,false,85,true) : "" %>' alt="" />
                                                </asp:PlaceHolder>
                                                <asp:PlaceHolder ID="PlaceHoldertxt" runat="server" Visible='<%#(Eval("FieldType").ToString().ToLower() == "image")?false : true %>'>
                                                    <asp:Label runat="server" ID="txtOptionsValue" CssClass="order_heading_data width_auto text_align_justify"
                                                        Text='<%# (Eval("DefaultCustomizationData")!=null) ? Eval("DefaultCustomizationData").ToString().Replace("\r\n","<br/>") : "" %>'></asp:Label>
                                                </asp:PlaceHolder>
                                            </div>
                                            <div class="clr">
                                            </div>
                                        </div>
                                    </div>
                                </AlternatingItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="main_heading_bot">
                        </div>
                    </asp:PlaceHolder>
                    <!--end grid container-->
                    <!-- grid_container-->
                    <div class="fr mrt_25" style="padding-bottom: 3px;">
                        &nbsp;
                    </div>
                    <div class="main_heading ">
                        <span class="order_heading width175 pdl_15px"><%=Resources.ShoppingCart.FulfillmentInformation%></span>
                        <span class="order_heading width95"><%=Resources.ShoppingCart.Sku.ToUpperInvariant()%></span>
                        <span class="order_heading width205"><%=Resources.ShoppingCart.Title%></span>
                        <span class="order_heading width95"><%=Resources.ShoppingCart.Quantity%></span>
                        <span class="order_heading width80 text_right"><%=Resources.ShoppingCart.Price%></span>
                    </div>
                    <div class="main_heading_bg">
                        <mc:ElectronicFullfilment ID="electronicFullfilment" runat="Server"></mc:ElectronicFullfilment>
                        <mc:VdpFullfilment ID="vdpFullfilment" runat="Server"></mc:VdpFullfilment>
                        <mc:PrintFullfilment ID="printFullfilment" runat="Server"></mc:PrintFullfilment>
                    </div>
                    <div class="main_heading_bot"></div>
                    <!--end grid container-->
                    <div class="btn_left fr mr11">
                        <asp:LinkButton runat="server" ID="ConfirmOrder" class="btn_right" Text="<%$Resources:Shared,BackToHome%>" OnClick="ConfirmOrder_Click"></asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <asp:PlaceHolder ID="phGiveConsent" runat="server" Visible="false">
            <style type="text/css">
                .ui-dialog {
                    color: #666 !important;
                }

                .ui-dialog-title {
                    font-weight: bold;
                }

                .ui-dialog-titlebar-close {
                    display: none;
                }

                .ui-dialog-content {
                    padding: 0 45px 0px 45px !important;
                }

                .ui-widget-content a {
                    color: #fff;
                    text-decoration: none !important;
                }

                .submit_bg_btn {
                    padding-left: 25px !important;
                }
            </style>
            <div id="consent-modal" title="<%=Resources.Shared.PleaseConfirm%>:">
                <p><%=Resources.ShoppingCart.ProgramImplementationHelp%></p>
                <div style="margin: 0 auto; width: 100%;">
                    <div style="padding-left: 100px;">
                        <div class="bg_btn">
                            <asp:LinkButton runat="server" ID="LinkButton1" class="submit_bg_btn" Text="<%$Resources:Shared,Yes%>" OnClick="btnGiveConsentYes_Click"></asp:LinkButton>
                        </div>
                        <div class="bg_btn mr11">
                            <asp:LinkButton runat="server" ID="LinkButton2" class="submit_bg_btn" Text="<%$Resources:Shared,No%>" OnClick="btnGiveConsentNo_Click"></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(function () {
                    $("#consent-modal").dialog({
                        height: 140,
                        modal: true,
                        autoOpen: false,
                        draggable: false,
                        height: 'auto',
                        width: 'auto',
                        resizable: false,
                        width: 475,
                        open: function () {
                            //$('.ui-widget-overlay').css('z-index', '1001');
                        }
                    });
                    $("#consent-modal").dialog('open');
                });
            </script>
        </asp:PlaceHolder>
</asp:Content>
