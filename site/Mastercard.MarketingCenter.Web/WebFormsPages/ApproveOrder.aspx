﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebFormsPages/MMPShoppingCart.Master" AutoEventWireup="true" Inherits="ApproveOrder" CodeBehind="ApproveOrder.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="center_container">
        <div class="grey_head mrtb">
            <span><%=Resources.ShoppingCart.ApproveOrder_Title%></span>
        </div>
        <div class="landing_page_body_html_container">
            <%=Resources.ShoppingCart.ApproveOrder_Description%>
        </div>
    </div>
</asp:Content>