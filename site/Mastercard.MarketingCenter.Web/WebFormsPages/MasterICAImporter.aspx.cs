﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Web.Mvc;

public partial class MasterICAImporter : System.Web.UI.Page
{
    private const string TempMasterIcaListFilename = "MasterICAsList.xls";

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.Page.Title = Resources.ShoppingCart.MasterIcaImporter_Title;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (fileUpload.HasFile)
        {
            if (fileUpload.PostedFile.FileName.EndsWith(".xls"))
            {
                ProcessFile();
            }
            else
            {
                phError.Visible = true;
                litError.Text = Resources.Errors.FileInvalidXls;
            }
        }
    }

    private void ProcessFile()
    {
        string filePath = Server.MapPath("/portal/TempUpload/" + TempMasterIcaListFilename);

        // first we save the file
        SavePostedFile(filePath);

        string connectionString = $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={filePath};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";";
        using (OleDbConnection connection = new OleDbConnection(connectionString))
        {
            connection.Open();
            using (OleDbCommand command = new OleDbCommand("SELECT [Primary ICA ], [ICA Num] FROM [Active ICAs$]", connection))
            {
                using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
                {
                    using (DataTable table = new DataTable())
                    {
                        adapter.Fill(table);
                        if (table.Rows.Count > 0)
                        {
                            var _issuerService = DependencyResolver.Current.GetService<IIssuerService>();
                            _issuerService.DeleteAllMasterICAs();

                            List<MasterICA> icas = new List<MasterICA>();
                            for (int rowIdx = 0; rowIdx < table.Rows.Count; rowIdx++)
                            {
                                if (int.TryParse(table.Rows[rowIdx][0].ToString(), out int masterIca) &&
                                    int.TryParse(table.Rows[rowIdx][1].ToString(), out int secondaryIca))
                                {
                                    icas.Add(new MasterICA
                                    {
                                        MasterIca = masterIca,
                                        Ica = secondaryIca
                                    });
                                }
                            }

                            _issuerService.InsertMasterICAs(icas);
                        }
                    }
                }
            }
        }
    }

    private void SavePostedFile(string filePath)
    {
        fileUpload.SaveAs(filePath);
    }
}