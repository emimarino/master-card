﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Error404 : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.Page.Title = Resources.Errors.PageNotFound;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}