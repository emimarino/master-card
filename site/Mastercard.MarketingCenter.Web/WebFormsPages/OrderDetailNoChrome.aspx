﻿<%@ Page Language="C#" MasterPageFile="~/WebFormsPages/MMP.minimal.master" AutoEventWireup="true" CodeBehind="OrderDetailNoChrome.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.WebFormsPages.OrderDetailNoChrome" %>

<%@ Register TagPrefix="mc" TagName="OrderDetail" Src="~/UserControls/OrderDetail.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <mc:OrderDetail ID="orderDetail" runat="server"></mc:OrderDetail>
</asp:Content>
