﻿<%@ Page Title="Your Estimated Distribution" Language="C#" MasterPageFile="~/WebFormsPages/MMPShoppingCart.Master" AutoEventWireup="true" CodeBehind="EstimatedDistribution.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.WebFormsPages.EstimatedDistribution" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadJavascriptPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="center_container">
        <div class="form_container">
            <asp:HiddenField ID="hdShoppingCartId" runat="server" />
            <div class="orange_heading_profile"><%=Resources.ShoppingCart.EstimatedDistribution_Title%></div>
            <div class="clr"></div>
            <div class="faq_blog" style="height: 15px;">
                <p id="defaultHelpMessage" runat="server"></p>
            </div>
            <div class="faq_blog">
                <asp:PlaceHolder runat="server" ID="plcError" Visible="false">
                    <span style="font-size: 12px; font-weight: bold;">
                        <asp:Label runat="server" ID="lblError" CssClass="red_error no_padding"></asp:Label></span><br />
                    <br />
                </asp:PlaceHolder>
                <asp:GridView ID="grdEstimatedDistribution" CssClass="main_heading_bg" runat="server" AutoGenerateColumns="false" BorderStyle="None" GridLines="None" OnRowDataBound="grdEstimatedDistribution_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                            <HeaderTemplate>
                                <div class="main_heading">
                                    <span class="order_heading width110 pdl_15px"><%=Resources.ShoppingCart.Sku.ToUpperInvariant()%></span>
                                    <span class="order_heading width150"><%=Resources.ShoppingCart.Title%></span>
                                    <span class="order_heading">&nbsp;</span>
                                    <span class="order_heading" style="width: 310px !important;"><%=Resources.ShoppingCart.EstimatedDistribution%>*</span>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="chk_outgrid ">
                                    <span class="order_heading_data width110 pdl_15px word_wrap"><%# Eval("SKU").ToString()%></span>
                                    <span class="order_heading_data width150"><%# Eval("Title").ToString()%></span>
                                    <span class="order_heading_data">&nbsp;</span>
                                    <span class="order_heading_data" style="width: 310px !important;">
                                        <asp:TextBox MaxLength="8" runat="server" ID="txtEstimatedDistribution" Text='<%# Eval("EstimatedDistribution")%>' onkeydown="return ForceNumericInput(event,false,false);" size="20"></asp:TextBox>
                                        <br />
                                        <em><%=Resources.ShoppingCart.EstimatedDistribution_Indication%></em>
                                    </span>
                                    <asp:HiddenField ID="hdnContentItemId" runat="server" />
                                </div>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <div class="chk_outgrid gray_backgrnd">
                                    <span class="order_heading_data width110 pdl_15px word_wrap"><%# Eval("SKU").ToString()%></span>
                                    <span class="order_heading_data width150"><%# Eval("Title").ToString()%></span>
                                    <span class="order_heading_data">&nbsp;</span>
                                    <em class="order_heading_data" style="width: 310px !important;">
                                        <asp:TextBox MaxLength="8" runat="server" ID="txtEstimatedDistribution" Text='<%# Eval("EstimatedDistribution")%>' onkeydown="return ForceNumericInput(event,false,false);" size="20"></asp:TextBox>
                                        <br />
                                        <em><%=Resources.ShoppingCart.EstimatedDistribution_Indication%></em>
                                        </span>
                                    <asp:HiddenField ID="hdnContentItemId" runat="server" />
                                </div>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <div class="meaning_bot"></div>
                <div class="cart_requried">* <em><%=Resources.Forms.RequiredField%></em>.</div>
                <div class="bg_btn fr mt9">
                    <asp:LinkButton runat="server" ID="lnkSubmit" CssClass="submit_bg_btn" Text="<%$Resources:Shared,Continue%>" OnClick="lnkSubmit_Click"><%=Resources.Shared.Continue%> &gt;</asp:LinkButton>
                </div>
                <div class="bg_btn_left fr mr11">
                    <asp:LinkButton ID="btnCancel" runat="server" Text="<%$Resources:Shared,Back%>" CssClass="submit_bg_btn" OnClick="lnkCancel_Click"> &lt; <%=Resources.Shared.Back%></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>