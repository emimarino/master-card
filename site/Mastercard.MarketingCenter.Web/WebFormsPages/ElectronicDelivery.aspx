﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebFormsPages/MMPShoppingCart.Master" AutoEventWireup="true" Inherits="ElectronicDelivery" CodeBehind="ElectronicDelivery.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="center_container">
        <div class="form_container">
            <div class="orange_heading_profile"><%=Resources.ShoppingCart.ElectronicDelivery%></div>
            <div class="order_detail">
                <span>
                    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                </span>
            </div>
            <div class="clr"></div>
            <div class="faq_blog">
                <p>
                    <asp:Literal ID="litElectronicDeliveryContact" runat="server"></asp:Literal>
                </p>
            </div>
        </div>
    </div>
</asp:Content>