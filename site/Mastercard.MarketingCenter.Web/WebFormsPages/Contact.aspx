﻿<%@ Page Language="C#" MasterPageFile="~/WebFormsPages/MMP.master" AutoEventWireup="true" Inherits="Contact" Title="Contact Us" CodeBehind="Contact.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%= AntiForgery.GetHtml() %>
    <header class="page-section carousel-section slides-section">
        <div class="carousel pausing-carousel illustration">
            <section class="carousel-slide covered">
                <img src="/portal/content/images/ills/contacts/w1400.jpg" srcset="/portal/content/images/ills/contacts/w1400.jpg 1400w, /portal/content/images/ills/contacts/w992.jpg 992w, /portal/content/images/ills/contacts/w768.jpg 768w, /portal/content/images/ills/contacts/w480.jpg 480w" sizes="(max-width: 480px) 480px, (max-width: 768px) 768px, (max-width: 1024px) 992px, (min-width: 1025px) 1400px, 100vw" width="1400" height="468" class="cover-image" alt="" />
                <div class="slick-inner content-spaces-in">
                    <h1><%=Resources.Forms.ContactUs_Title%></h1>
                    <p>
                        <%=Resources.Forms.ContactUs_Description%>
                    </p>
                </div>
            </section>
        </div>
    </header>
    <div class="content-container content-spaces-in">
        <article class="content-text">
            <div class="form form-contact-us">
                <asp:PlaceHolder ID="placeHolderMessage" runat="server" Visible="false">
                    <p class="alert alert-warning">
                        <asp:Label ID="lblerror" runat="server" CssClass="span_1" Visible="False"></asp:Label>
                    </p>
                </asp:PlaceHolder>
                <h2><%=Resources.Forms.ContactUs_Subtitle%></h2>
                <%if(region.Value.ToLower()=="us"){ %>
                <asp:RadioButtonList ID="radioContactOptions" runat="server" CellPadding="0" CellSpacing="0" CssClass="form-group" RepeatLayout="Flow" RepeatDirection="Vertical">
                    <asp:ListItem Class="radio" Text="<%$Resources:Forms,ContactUs_CheckBoxQuestionsLabel%>"></asp:ListItem>
                    <asp:ListItem Class="radio" Text="<%$Resources:Forms,ContactUs_CheckBoxMaterialOrdersLabel%>"></asp:ListItem>
                    <asp:ListItem Class="radio" Text="<%$Resources:Forms,ContactUs_CheckBoxGuideToBenefitsLabel%>"></asp:ListItem>
                </asp:RadioButtonList>
                <% }  %>
                <%else { %>
                <label class="form-group" > <%=Resources.Forms.ContactUs_CheckBoxQuestionsLabel %></label>
                  <% }  %>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label"><%=Resources.Forms.FirstName%> <span class="req-mark">*</span></label>
                            <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><%=Resources.Forms.LastName%> <span class="req-mark">*</span></label>
                            <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><%=Resources.Forms.FinancialInstitution%> <span class="req-mark">*</span></label>
                            <asp:TextBox ID="txtIssuerName" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><%=Resources.Forms.Email%> <span class="req-mark">*</span></label>
                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><%=Resources.Forms.Phone%> <span class="req-mark">*</span></label>
                            <asp:TextBox ID="TxtPhone" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-sm-7 col-sm-offset-1">
                        <div class="form-group">
                            <label class="control-label"><%=Resources.Forms.Subject%> <span class="req-mark">*</span></label>
                            <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label class="control-label"><%=Resources.Forms.Message %><span class="req-mark">*</span></label>
                            <asp:TextBox ID="txtMessage" TextMode="MultiLine" runat="server" CssClass="form-control message-area"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="form-group btn-group tar">
                    <span class="btn-label req-mark">* <%=Resources.Forms.RequiredField%></span>
                    <asp:LinkButton ID="btnCancel" runat="server" Text="<%$Resources:Shared,Cancel%>" CssClass="btn" OnClick="btnCancel_Click"></asp:LinkButton>
                    <asp:LinkButton ID="bnSubmit" runat="server" Text="<%$Resources:Shared,Send%>" CssClass="btn" OnClick="bnSubmit_Click"></asp:LinkButton>
                </div>
                <asp:HiddenField ID="region" runat="server" />
                <asp:HiddenField ID="language"  runat="server" />
                <asp:HiddenField ID="country" runat="server" />
                <asp:Literal ID="ltContactInformation" runat="server"></asp:Literal>
                <asp:HiddenField ID="hdnUserProcessor" runat="server" />
            </div>
        </article>
    </div>
</asp:Content>