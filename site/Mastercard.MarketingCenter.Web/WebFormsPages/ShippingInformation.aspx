﻿<%@ Page Language="C#" MasterPageFile="~/WebFormsPages/MMPShoppingCart.Master" AutoEventWireup="true"
    Inherits="ShippingInformation_Page" Title="Untitled Page" CodeBehind="ShippingInformation.aspx.cs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadJavascriptPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="center_container">
        <div class="form_container ">
            <div class="orange_heading_profile"><%=Resources.ShoppingCart.ShippingInformation_Title%></div>
            <asp:PlaceHolder runat="server" ID="panelError" Visible="false">
                <div class="roundcont_error mrt_25">
                    <div class="roundtop">
                        <img src="/portal/inc/images/tl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
                    </div>
                    <div style="float: left; width: 80px; text-align: center;">
                        <img src="/portal/inc/images/error_img.gif" border="0px" alt="" width="40" height="40" />
                    </div>
                    <div style="float: left; width: 300px;">
                        <asp:Label ID="lblerror" runat="server" CssClass="span_1" Visible="False"></asp:Label>
                    </div>
                    <div style="clear: both;"></div>
                    <div class="roundbottom">
                        <img src="/portal/inc/images/bl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
                    </div>
                </div>
            </asp:PlaceHolder>
            <div class="grid_container mrt_25">
                <div class="grid_top_head">
                    <div class="orange_heading_main"><%=Resources.ShoppingCart.ShippingInformation %></div>
                </div>
                <div class="grid_top_bg pd_bot10">
                    <div class="right_form">
                        <span>
                            <label>
                                <%=Resources.ShoppingCart.SpecialInstructionsDetails %>
                            </label>
                            <asp:TextBox ID="txtSpecialInstructions" runat="server" TextMode="MultiLine" MaxLength="950" onkeyDown="checkTextAreaMaxLength(this,event,'950');"></asp:TextBox>
                        </span>
                        <div class="orange_box">
                            <div class="roundtop">
                                <img src="/portal/inc/images/tl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
                            </div>
                            <div class="orange_middlebox">
                                <span class="bold_title">
                                    <label>
                                        <%=Resources.ShoppingCart.RushOrderRequested %>
                                    </label>
                                    <div>
                                        <asp:CheckBox ID="chkRushOrder" runat="server" Text="<%$Resources:ShoppingCart,ShippingInformation_ChackBoxRushOrderLabel%>" />
                                    </div>
                                </span>


                                <span class="bold_title">
                                    <label>
                                        <%=Resources.ShoppingCart.MultipleAddresses%>
                                    </label>
                                    <div>
                                        <asp:CheckBox ID="chkMultipleAddresses" runat="server" Text="<%$Resources:ShoppingCart,ShippingInformation_ChackBoxMultipleAddressesLabel%>" AutoPostBack="true" OnCheckedChanged="chkMultipleAddresses_CheckedChanged" />
                                    </div>
                                </span>
                            </div>
                            <div class="roundbottom">
                                <img src="/portal/inc/images/bl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
                            </div>
                        </div>
                    </div>
                    <div class="left_form width493px mrt_10">
                        <span>
                            <label class="label_margin pdl_15px">Address*<span style="margin-top: 0px;"><%=Resources.ShoppingCart.ShippingInformation_AddressShippingLabel%></span></label>
                            <asp:TextBox ID="txtAddress" Rows="6" TextMode="MultiLine" runat="server"></asp:TextBox>
                        </span>
                        <span class="label_margin pdl_15px" style="margin-top: 20px;">
                            <label><%=Resources.Forms.ContactName%>*</label>
                            <asp:TextBox ID="txtContactName" runat="server"></asp:TextBox>
                        </span>
                        <span class="label_margin pdl_15px">
                            <label><%=Resources.Forms.Phone%>*</label>
                            <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                        </span>
                        <asp:HiddenField ID="hdnDefaultShippingId" runat="server" />

                        <asp:Repeater ID="rptMoreAddresses" runat="server" OnItemDataBound="rptMoreAddresses_ItemDataBound" OnItemCommand="rptMoreAddresses_ItemCommand">
                            <ItemTemplate>
                                <div style="background-color: #EFEFEF; margin: 0 8px;">
                                    <span class="label_margin pdl_top">
                                        <label>&nbsp;</label>
                                        <asp:LinkButton ID="lnkDeleteAddress" Style="padding-left: 281px;" runat="server" CommandName="DELETE">[<%=Resources.Shared.Delete%>]</asp:LinkButton>
                                    </span>
                                    <span class="label_margin_less pdl_15px">
                                        <label><%=Resources.Forms.Address%>*</label>
                                        <asp:TextBox ID="txtAddress" Rows="6" TextMode="MultiLine" runat="server"></asp:TextBox>
                                    </span>
                                    <span class="label_margin pdl_15px">
                                        <label><%=Resources.Forms.ContactName%>*</label>
                                        <asp:TextBox ID="txtContactName" runat="server"></asp:TextBox>
                                    </span>
                                    <span class="label_margin pdl_btm">
                                        <label><%=Resources.Forms.Phone%>*</label>
                                        <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                                    </span>
                                </div>
                                <asp:HiddenField ID="hdnShippingInformationId" runat="server" />
                            </ItemTemplate>
                            <FooterTemplate>
                                <span class="label_margin pdl_15px">
                                    <label>&nbsp;</label>
                                    <div class="bg_btn_left">
                                        <asp:LinkButton ID="btnAddAddress" runat="server" CssClass="submit_bg_btn" CommandName="ADD"><%=Resources.Forms.AddAnotherAddress%></asp:LinkButton>
                                    </div>
                                </span>
                            </FooterTemplate>
                        </asp:Repeater>

                    </div>
                </div>
                <div class="grid_top_bot"></div>
            </div>
            <div class="cart_requried">* Required field</div>
            <div class="bg_btn fr mt9">
                <asp:LinkButton ID="bnSubmit" runat="server" Text="<%$Resources:Shared,Continue%>" CssClass="submit_bg_btn" OnClick="bnSubmit_Click"><%=Resources.Shared.Continue%> &gt;</asp:LinkButton>
            </div>
            <div class="bg_btn_left fr mr11">
                <asp:LinkButton ID="btnCancel" runat="server" Text="<%$Resources:Shared,Back%>" CssClass="submit_bg_btn" OnClick="btnCancel_Click"> &lt; <%=Resources.Shared.Back%></asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>