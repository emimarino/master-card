﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.WebFormsPages;
using Pulsus;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.UI.WebControls;

public partial class ReviewOrder : System.Web.UI.Page
{
    decimal total = 0;
    Dictionary<string, AssetPrice> assetPrices = new Dictionary<string, AssetPrice>();
    string Special_Instructions = string.Empty;

    UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
    public IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }
    public IEmailService _emailService { get { return DependencyResolver.Current.GetService<IEmailService>(); } }
    public IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.Page.Title = Resources.ShoppingCart.ReviewOrder_Title;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int CartID = GetCartId();
        if (!this.IsPostBack)
        {
            List<RetrieveShoppingCartByUserIdResult> lsCartItems = ShoppingCartService.GetShoppingCartByUserId(_userContext.User.UserName).Where(x => x.Quantity.Value > 0).ToList();
            if (lsCartItems.Count > 0)
            {
                var draftCartItems = lsCartItems.Where(ci => ci.ContentItemID.EndsWith("-p"));
                if (draftCartItems.Count() > 0)
                {
                    btnConfirmOrder.Enabled = false;
                    plcError.Visible = true;
                    lblError.Text = Resources.Errors.OrderDraftItems;
                    foreach (var draftItem in draftCartItems)
                    {
                        lblError.Text += $"<br />{draftItem.SKU} | {draftItem.Title}";
                    }
                }

                grdShoppingCart.DataSource = lsCartItems;
                grdShoppingCart.DataBind();

                if (!string.IsNullOrEmpty(lsCartItems[0].PromotionCode))
                {
                    PromotionCodeResponse promotionResponse = PromotionService.ApplyPromotionCode(lsCartItems[0].PromotionCode, CartID, _userContext.User.IssuerId);
                    if (string.IsNullOrEmpty(promotionResponse.ErrorMessage))
                    {
                        litPromotionDescription.Text = promotionResponse.Description;
                        litPromotionAmount.Text = $"$-{(promotionResponse.DiscountAmount < total ? promotionResponse.DiscountAmount.ToString("N2") : total.ToString("N2"))}";
                        pnlPromotion.Visible = true;

                        lblTotal.Text = FormatNumber(promotionResponse.DiscountAmount < total ? total - promotionResponse.DiscountAmount : 0);
                    }
                    else
                    {
                        PromotionService.RemovePromotionCode(CartID);
                        lblPromotionError.Text = Resources.Errors.PromotionCodeRemoved;
                        lblPromotionError.Visible = true;
                    }
                }

                if (lsCartItems.Any(i => !i.ElectronicDelivery))
                {
                    if (lsCartItems.Any(i => !i.ElectronicDelivery && i.VDP != null && !i.VDP.Value))
                    {
                        linkShipping.HRef = "/portal/shippinginformation";

                        using (MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext())
                        {
                            var ShippingDetail = dataContext.RetrieveShippingInformation(CartID, null).ToList();
                            if (ShippingDetail.Count > 1)
                            {
                                linkShippingAllocation.HRef = "/portal/shippingallocation";
                            }
                            else
                            {
                                linkShippingAllocation.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        linkShipping.Visible = false;
                        linkShippingAllocation.Visible = false;
                    }

                    pnlShippingNote.Visible = true;
                    lblTotal.Text += "*";
                }
                else
                {
                    linkShipping.Visible = false;
                    linkShippingAllocation.Visible = false;
                }

                var sCartItems = ShoppingCartService.GetShoppingCartByUserId(_userContext.User.UserName).Where(c => c.ElectronicDelivery);
                var cartItems = ShoppingCartService.GetShoppingCartItems(CartID).Where(ci => ci.ElectronicDelivery);
                if (cartItems.Count() > 0)
                {
                    var edCartItems = new List<EstimatedDistributionCartItem>();
                    foreach (var sCartItem in sCartItems)
                    {
                        edCartItems.Add(new EstimatedDistributionCartItem()
                        {
                            ContentItemId = sCartItem.ContentItemID,
                            SKU = sCartItem.SKU,
                            Title = sCartItem.Title,
                            EstimatedDistribution = cartItems.Where(ci => ci.ContentItemID == sCartItem.ContentItemID).FirstOrDefault().EstimatedDistribution
                        });

                        grdEstimatedDistribution.DataSource = edCartItems;
                        grdEstimatedDistribution.DataBind();
                    }
                }
                else
                {
                    phEstimatedDistribution.Visible = false;
                    linkEstimatedDistribution.Visible = false;
                }
            }
            else
            {
                Response.Redirect("/portal");
            }

            BindCustomizationGrid(CartID);
            linkEstimatedDistribution.HRef = "/portal/estimateddistribution";
            linkCustomization.HRef = "/portal/checkout";
            linkOrder.HRef = "/portal/shoppingcart";
            linkOrderICA.HRef = "/portal/shoppingcart";

            electronicFullfilment.CartId = CartID;
            vdpFullfilment.CartId = CartID;
            printFullfilment.CartId = CartID;

            var cart = ShoppingCartService.GetShoppingCartById(CartID);
            if (cart.BillingID.HasValue)
            {
                litBillingICA.Text = cart.BillingID.Value.ToString();
            }
            else
            {
                phBillingICA.Visible = false;
            }
        }
    }

    protected void grdShoppingCart_ItemDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            RetrieveShoppingCartByUserIdResult item = (RetrieveShoppingCartByUserIdResult)e.Row.DataItem;
            Image imgIcon = (Image)e.Row.FindControl("imgIcon");
            if (item.ElectronicDelivery)
            {
                imgIcon.ImageUrl = "~/inc/images/document_down.png";
            }
            else
            {
                if (item.VDP != null && item.VDP.Value)
                {
                    imgIcon.ImageUrl = "~/inc/images/vdp.png";
                }
                else
                {
                    if (item.Customizable != null && item.Customizable.Value)
                    {
                        imgIcon.ImageUrl = "~/inc/images/document_edit.png";
                    }
                    else
                    {
                        imgIcon.ImageUrl = "~/inc/images/document_lock.png";
                    }
                }
            }
        }
    }

    protected int GetCartId()
    {
        int cartId = 0;
        if (ViewState["CartId"] != null)
        {
            cartId = Int32.Parse(ViewState["CartId"].ToString());
        }
        else
        {
            int? id = ShoppingCartService.GetActiveShoppingCart(_userContext.User.UserName);
            if (id != null)
            {
                cartId = id.Value;
            }
        }

        return cartId;
    }

    private void BindCustomizationGrid(int CartID)
    {
        List<RetrieveCustomizationsResult> ls = ShoppingCartService.GetCustomizationOptions(_userContext.User.UserName, CartID, false);
        rptCustomizations.DataSource = ls;
        rptCustomizations.DataBind();

        placeCustomizations.Visible = ShoppingCartService.GetCustomizationOptions(_userContext.User.UserName, CartID, true).Count > 0;
    }

    protected string FormatNumber(decimal num)
    {
        System.Globalization.NumberFormatInfo nf = new System.Globalization.NumberFormatInfo();
        nf.CurrencySymbol = "$";
        nf.NumberDecimalDigits = 2;
        return num.ToString("C", nf);
    }

    protected string GetPrice(string GlobalID, string Quantity, bool electronicDelivery)
    {
        string returnPrice = string.Empty;
        if (electronicDelivery)
        {
            AssetPrice downloadPrice = ShoppingCartService.GetAssetPrice(GlobalID, Convert.ToInt32(Quantity), true);
            returnPrice = FormatNumber(downloadPrice.TotalPrice);
            total = total + downloadPrice.TotalPrice;
            lblTotal.Text = FormatNumber(total);
        }
        else
        {
            if (Quantity.Length > 0)
            {
                if (Quantity != "0")
                {
                    decimal intQuantity = decimal.Parse(Quantity);
                    if (!assetPrices.ContainsKey(GlobalID))
                    {
                        assetPrices.Add(GlobalID, ShoppingCartService.GetAssetPrice(GlobalID, decimal.ToInt32(intQuantity), false));
                    }

                    decimal price = assetPrices[GlobalID].TotalPrice;
                    total = total + price;
                    returnPrice = FormatNumber(price);
                    lblTotal.Text = FormatNumber(total);
                }
                else
                {
                    returnPrice = FormatNumber(0);
                }
            }
        }
        return returnPrice;
    }

    protected string GetItemPrice(string GlobalID, string Quantity, bool electronicDelivery)
    {
        string returnPrice = string.Empty;
        if (electronicDelivery)
        {
            returnPrice = FormatNumber(ShoppingCartService.GetAssetPrice(GlobalID, Convert.ToInt32(Quantity), true).TotalPrice);
        }
        else
        {
            if (Quantity.Length > 0)
            {
                if (Quantity != "0")
                {
                    decimal intQuantity = decimal.Parse(Quantity);
                    if (!assetPrices.ContainsKey(GlobalID))
                    {
                        assetPrices.Add(GlobalID, ShoppingCartService.GetAssetPrice(GlobalID, decimal.ToInt32(intQuantity), false));
                    }

                    if (assetPrices[GlobalID].ItemPrice > 0)
                    {
                        decimal price = assetPrices[GlobalID].ItemPrice;
                        returnPrice = FormatNumber(price);
                    }
                    else
                    {
                        returnPrice = Resources.Forms.NotApply;
                    }
                }
                else
                {
                    returnPrice = FormatNumber(0);
                }
            }
        }

        return returnPrice;
    }

    protected void btnConfirmOrder_Click(object sender, EventArgs e)
    {
        int ShoppingCartId = GetCartId();

        List<RetrieveShoppingCartByUserIdResult> lsCartItems = ShoppingCartService.GetShoppingCartByUserId(_userContext.User.UserName).Where(x => x.Quantity.Value > 0).ToList();
        if (lsCartItems.Count > 0)
        {
            if (!string.IsNullOrEmpty(lsCartItems[0].PromotionCode))
            {
                PromotionCodeResponse promotionResponse = PromotionService.ApplyPromotionCode(lsCartItems[0].PromotionCode, ShoppingCartId, _userContext.User.IssuerId);
                if (!string.IsNullOrEmpty(promotionResponse.ErrorMessage))
                {
                    PromotionService.RemovePromotionCode(ShoppingCartId);
                    lblPromotionError.Text = $"{Resources.Errors.PromotionEligibilityChanged} {Resources.Errors.PromotionCodeRemoved}";
                    lblPromotionError.Visible = true;
                }
            }
        }

        if (ShoppingCartId > 0)
        {
            ShoppingCartService.CleanShoppingCartItems(ShoppingCartId);

            int? OrderID = _orderService.CreateOrder(ShoppingCartId);
            if (OrderID != null)
            {
                ProcessOrderItemsAdvisorsTag(OrderID.Value);
                RetrieveOrderDetailResult OrderItems1 = _orderService.GetOrderDetails(Convert.ToInt32(OrderID));

                SetOrderStatus(Convert.ToInt32(OrderID), OrderItems1.OrderStatus);
                SendConfirmationMail(Convert.ToInt32(OrderID));

                Response.Redirect($"/portal/orderconfirmation/{OrderItems1.OrderNumber.ToString()}");
            }
        }
    }

    protected void ProcessOrderItemsAdvisorsTag(int orderId)
    {
        var slamContext = DependencyResolver.Current.GetService<SlamContext>();
        var orderItems = _orderService.GetOrderItemsForOrder(orderId);
        var advisorsTags = Slam.Cms.Configuration.ConfigurationManager.Solution.Settings["RealTimeNotifications.AdvisorsTags"].Split(',');
        foreach (var orderItem in orderItems)
        {
            var contentItem = slamContext.CreateQuery().FilterContentItemId(orderItem.ItemID).IncludeTags().Cache(SlamQueryCacheBehavior.NoCache).Get().FirstOrDefault();
            if (contentItem != null)
            {
                if (contentItem.Tags.Select(t => t.Identifier).Intersect(advisorsTags).Any())
                {
                    _orderService.UpdateOrderItemHasAdvisorsTag(orderItem.OrderItemID, true);
                }
            }
            else
            {
                LogManager.EventFactory.Create()
                          .Text("Error processing order items advisors tags...")
                          .Level(LoggingEventLevel.Error)
                          .AddData("OrderItem.OrderItemID", orderItem.OrderItemID)
                          .AddData("OrderItem.OrderID", orderItem.OrderID)
                          .AddData("OrderItem.ItemID", orderItem.ItemID)
                          .AddData("OrderItem.SubOrderID", orderItem.SubOrderID)
                          .AddData("Message", $"Content Item Id '{orderItem.ItemID}' not found")
                          .Push();
            }
        }
    }

    protected void SetOrderStatus(int Orderid, string currentStatus)
    {
        string strHostName = System.Net.Dns.GetHostName();
        string clientIPAddress = System.Net.Dns.GetHostAddresses(strHostName).GetValue(0).ToString();

        if (currentStatus == "Order Created")
        {
            _orderService.SetOrderStatus(Orderid, "Pending Customization", "Order Placed", clientIPAddress, _userContext.User.UserName);
        }
        else if (currentStatus == "Shipped")
        {
            _orderService.SetOrderStatus(Orderid, "Shipped", "Order Placed and Fulfilled", clientIPAddress, _userContext.User.UserName);
        }
        else
        {
            _orderService.SetOrderStatus(Orderid, "Fulfillment", "Order Placed", clientIPAddress, _userContext.User.UserName);
        }
    }

    protected string RetrieveShippingInformation(int Orderid)
    {
        string shipping_Detail = string.Empty;
        Special_Instructions = string.Empty;
        string Address = string.Empty;
        List<RetrieveShippingInformationResult> ShippingDetail = _orderService.GetShippingInformation(Orderid);
        RetrieveOrderDetailResult orderDetail = _orderService.GetOrderDetails(Orderid);
        if (ShippingDetail != null)
        {
            foreach (RetrieveShippingInformationResult s in ShippingDetail)
            {
                Address = s.Address + Environment.NewLine;
                Address += Environment.NewLine;
                Address += Resources.ShoppingCart.ItemsOrdered + ":" + Environment.NewLine;

                using (MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext())
                {
                    var orderItems = dataContext.RetrieveOrderItemsForShippingInformation(s.ShippingInformationID).ToList();
                    if (orderItems.Count > 0)
                    {
                        foreach (RetrieveOrderItemsForShippingInformationResult orderItem in orderItems)
                        {
                            Address += $"{orderItem.SKU}, {orderItem.ItemName}, {orderItem.ItemQuantity}{Environment.NewLine}";
                        }
                    }
                }

                shipping_Detail += Environment.NewLine + Address;

                if (!string.IsNullOrEmpty(orderDetail.SpecialInstructions))
                {
                    Special_Instructions = orderDetail.SpecialInstructions;
                }
                else
                {
                    Special_Instructions = " ";
                }

                if (orderDetail.RushOrder)
                {
                    Special_Instructions += Environment.NewLine + Environment.NewLine + Resources.ShoppingCart.RushOrderRequested.ToUpperInvariant();
                }
            }
        }

        if (string.IsNullOrEmpty(shipping_Detail))
        {
            shipping_Detail = "     ";
        }

        return shipping_Detail;
    }

    protected string RetrieveShippingInformationForApproval(int Orderid)
    {
        string Address = string.Empty;
        using (MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext())
        {
            var shippingLocations = dataContext.RetrieveShippingInformationForOrderApproval(Orderid).ToList();
            _orderService.GetOrderDetails(Orderid);
            if (shippingLocations != null && shippingLocations.Count > 0)
            {
                foreach (RetrieveShippingInformationForOrderApprovalResult s in shippingLocations)
                {
                    Address = s.Address + Environment.NewLine;
                    Address += Environment.NewLine;
                }
            }

            if (string.IsNullOrEmpty(Address))
            {
                Address = "     ";
            }
        }
        return Address;
    }

    protected void SendConfirmationMail(int OrderID)
    {
        List<TableItem> listitems;

        decimal totals = 0;
        decimal discountTotals = 0;
        decimal netTotal = 0;

        StringBuilder sb = new StringBuilder();
        StringBuilder sbEd = new StringBuilder();
        string result = string.Empty;
        StringBuilder Special_Text = new StringBuilder();
        String Shipping_Text = string.Empty;

        List<RetrieveOrderItemsResult> OrderItems = _orderService.GetOrderItems(Convert.ToInt32(OrderID));
        Shipping_Text = RetrieveShippingInformation(Convert.ToInt32(OrderID));
        EmailText et;
        listitems = new List<TableItem>();

        if (OrderItems != null)
        {
            sb.Append(".................................................................");
            sb.Append(Environment.NewLine);

            TableItem t = new TableItem();

            t.Head = Resources.ShoppingCart.Sku.ToUpperInvariant();
            t.Width = 12;
            t.Align = "Left";
            listitems.Add(t);

            t = new TableItem();
            t.Head = Resources.ShoppingCart.Title;
            t.Width = 22;
            t.Align = "Left";
            listitems.Add(t);

            t = new TableItem();
            t.Head = Resources.ShoppingCart.Quantity;
            t.Width = 17;
            t.Align = "Left";
            listitems.Add(t);

            t = new TableItem();
            t.Head = Resources.ShoppingCart.Price;
            t.Width = 17;
            t.Align = "Left";
            listitems.Add(t);

            et = new EmailText();
            result = et.GenerateText(listitems);

            sb.Append(result);
            sb.Append(".................................................................");
            sb.Append(Environment.NewLine);

            foreach (RetrieveOrderItemsResult r in OrderItems)
            {
                listitems.Clear();

                t = new TableItem();

                t.Head = r.SKU.ToString() + " ";
                t.Width = 12;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = r.ItemName.ToString() + " ";
                t.Width = 22;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = r.ItemQuantity.ToString() + " ";
                t.Width = 14;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = FormatNumber(Convert.ToDecimal(r.ItemPrice.ToString()));
                totals = totals + Convert.ToDecimal(r.ItemPrice.ToString());
                t.Width = 12;
                t.Align = "Right";
                listitems.Add(t);

                et = new EmailText();
                result = et.GenerateText(listitems);
                sb.Append(result);
            }

            netTotal = totals;

            RetrievePromotionForOrderResult promotion = _orderService.GetPromotion(OrderID);
            if (promotion != null)
            {
                discountTotals = Convert.ToDecimal(promotion.PromotionAmount);
                netTotal -= discountTotals;

                t = new TableItem();
                listitems.Clear();

                t.Head = promotion.PromotionDescription;
                t.Width = 48;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = $"$-{discountTotals.ToString("N2")}";
                t.Width = 14;
                t.Align = "Right";
                listitems.Add(t);

                et = new EmailText();
                result = et.GenerateText(listitems);
                sb.Append(result);
            }

            sb.Append(".................................................................");
            sb.Append(Environment.NewLine);

            t = new TableItem();
            listitems.Clear();

            t.Head = string.Empty;
            t.Width = 12;
            t.Align = "Left";
            listitems.Add(t);

            t = new TableItem();
            t.Head = string.Empty;
            t.Width = 22;
            t.Align = "Left";
            listitems.Add(t);

            t = new TableItem();
            t.Head = "Total: ";
            t.Width = 14;
            t.Align = "Right";
            listitems.Add(t);

            t = new TableItem();
            t.Head = FormatNumber(Convert.ToDecimal(netTotal)) + "*";
            t.Width = 13;
            t.Align = "Right";
            listitems.Add(t);

            et = new EmailText();
            result = et.GenerateText(listitems);
            sb.Append(result);
            sb.Append(".................................................................");
        }

        // Estimated Distribution Details
        if (OrderItems.Any(o => o.ElectronicDelivery))
        {
            listitems.Clear();
            sbEd.Append(Environment.NewLine);
            sbEd.Append(Environment.NewLine);
            sbEd.Append($"--- {Resources.ShoppingCart.EstimatedDistribution.ToUpperInvariant()} ---");
            sbEd.Append(Environment.NewLine);
            sbEd.Append(Environment.NewLine);
            sbEd.Append(".................................................................");
            sbEd.Append(Environment.NewLine);

            TableItem t = new TableItem();

            t.Head = Resources.ShoppingCart.Sku.ToUpperInvariant();
            t.Width = 12;
            t.Align = "Left";
            listitems.Add(t);

            t = new TableItem();
            t.Head = Resources.ShoppingCart.Title;
            t.Width = 22;
            t.Align = "Left";
            listitems.Add(t);

            t = new TableItem();
            t.Head = Resources.ShoppingCart.EstimatedDistribution;
            t.Width = 17;
            t.Align = "Left";
            listitems.Add(t);

            et = new EmailText();
            result = et.GenerateText(listitems);

            sbEd.Append(result);
            sbEd.Append(".................................................................");
            sbEd.Append(Environment.NewLine);

            var edOrderItems = _orderService.GetOrderItemsForOrder(Convert.ToInt32(OrderID)).Where(oi => oi.ElectronicDelivery);
            foreach (var edOrderItem in edOrderItems)
            {
                listitems.Clear();

                t = new TableItem();

                t.Head = edOrderItem.SKU.ToString() + " ";
                t.Width = 12;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = edOrderItem.ItemName.ToString() + " ";
                t.Width = 22;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = edOrderItem.EstimatedDistribution.ToString() + " ";
                t.Width = 14;
                t.Align = "Left";
                listitems.Add(t);

                et = new EmailText();
                result = et.GenerateText(listitems);
                sbEd.Append(result);
            }
        }

        // Shipping Details
        TableItem TI = new TableItem();
        listitems.Clear();

        TI.Head = Special_Instructions;
        TI.Width = 65;
        TI.Align = "Left";
        listitems.Add(TI);

        et = new EmailText();
        result = et.GenerateText(listitems);
        Special_Text.Append(result);

        bool requiresApproval = false;
        IEnumerable<RetrieveOrderItemsResult> approvableItems = OrderItems.Where(i => !string.IsNullOrEmpty(i.ApproverEmailAddress));
        requiresApproval = approvableItems != null && approvableItems.Count() > 0;

        string shippingNote = string.Empty;
        if (OrderItems.Any(i => !i.ElectronicDelivery))
        {
            StreamReader reader = new StreamReader(Server.MapPath(WebConfigurationManager.AppSettings["ShippingNote.TemplateFile"]));
            shippingNote = reader.ReadToEnd();
            reader.Close();
        }

        string shippingTemplate = string.Empty;
        if (!string.IsNullOrEmpty(Shipping_Text))
        {
            StreamReader reader = new StreamReader(Server.MapPath(WebConfigurationManager.AppSettings["Shipping.TemplateFile"]));
            shippingTemplate = reader.ReadToEnd();
            reader.Close();

            shippingTemplate = shippingTemplate.Replace("[#SHIPPINGDETAILS]", Shipping_Text);
            shippingTemplate = shippingTemplate.Replace("[#SPECIALINSTRUCTIONS]", Special_Text.ToString());
        }

        string electronicDeliveryTemplate = string.Empty;
        List<RetrieveOrderItemsResult> downloadItems = OrderItems.Where(i => i.ElectronicDelivery).ToList();
        if (downloadItems.Count > 0)
        {
            string electronicDeliveryTemplateKey = string.Empty;
            if (requiresApproval)
            {
                electronicDeliveryTemplateKey = "ElectronicDelivery.TemplateFileNoLinks";
            }
            else
            {
                electronicDeliveryTemplateKey = "ElectronicDelivery.TemplateFile";
            }

            StreamReader reader = new StreamReader(Server.MapPath(WebConfigurationManager.AppSettings[electronicDeliveryTemplateKey]));
            electronicDeliveryTemplate = reader.ReadToEnd();
            reader.Close();

            if (!requiresApproval)
            {
                StringBuilder sbDownloadLinks = new StringBuilder();
                string downloadUrlFormat = $"{RegionalizeService.GetBaseUrl(DependencyResolver.Current.GetService<UserContext>().SelectedRegion).TrimEnd('/')}/{WebConfigurationManager.AppSettings["ElectronicDelivery.DownloadUrlFormat"].TrimStart('/')}";
                foreach (RetrieveOrderItemsResult item in downloadItems)
                {
                    sbDownloadLinks.Append(downloadUrlFormat.Replace("{key}", item.ElectronicDeliveryID.ToString()).Replace("{sku}", Regex.Replace(item.SKU.Replace(" ", ""), @"[^\w\-\+\~]", "")));
                    sbDownloadLinks.Append(Environment.NewLine);
                }

                electronicDeliveryTemplate = electronicDeliveryTemplate.Replace("[#EXPIRATION_DATE]", downloadItems[0].ExpirationDate.Value.ToString("MM/dd/yy"));
                electronicDeliveryTemplate = electronicDeliveryTemplate.Replace("[#MAX_DOWNLOAD]", WebConfigurationManager.AppSettings["ElectronicDelivery.MaxDownload"]);
                electronicDeliveryTemplate = electronicDeliveryTemplate.Replace("[#DOWNLOAD_LINKS]", sbDownloadLinks.ToString());
            }
        }

        string vdpTemplate = string.Empty;
        List<RetrieveOrderItemsResult> vdpItems = OrderItems.Where(i => i.VDP).ToList();
        if (vdpItems.Count > 0)
        {
            StreamReader reader = new StreamReader(Server.MapPath(WebConfigurationManager.AppSettings["Vdp.TemplateFile"]));
            vdpTemplate = reader.ReadToEnd();
            reader.Close();
        }

        RetrieveOrderDetailResult OrderItems1 = _orderService.GetOrderDetails(OrderID);
        if (OrderItems != null)
        {
            var userId = DependencyResolver.Current.GetService<UserContext>().User?.UserId ?? 0;
            _emailService.SendOrderConfirmationEmail(OrderItems1.OrderNumber.ToString(), OrderItems1.OrderDate.Value.ToString("MM/dd/yyyy"), sb.ToString(), _userContext.User.Email, _userContext.User.Profile.FirstName.ToString(), _userContext.User.Profile.LastName.ToString(), shippingNote, shippingTemplate, OrderItems1.OrderStatus == "Pending Customization", OrderItems.Any(i => !i.Customizations.Value && !i.ElectronicDelivery), vdpTemplate, electronicDeliveryTemplate, sbEd.ToString(), requiresApproval && OrderItems.Any(i => !i.ElectronicDelivery), userId);
            if (!requiresApproval && OrderItems.Any(i => !i.ElectronicDelivery))
            {
                _emailService.SendNewOrderEmail(OrderItems1.OrderNumber.ToString(), OrderItems1.OrderStatus == "Pending Customization", OrderItems.Any(i => i.VDP), userId, OrderItems1.RushOrder);
            }

            if (requiresApproval)
            {
                _orderService.SetOrderStatus(OrderID, "Pending MasterCard Approval");
                string domain = _userContext.User.Email.Substring(_userContext.User.Email.IndexOf("@") + 1, (_userContext.User.Email.Length - _userContext.User.Email.IndexOf("@") - 1));
                _emailService.SendNewOrderForApprovalEmail(OrderItems1.OrderNumber.ToString(), $"{_userContext.User.Profile.FirstName.ToString()} {_userContext.User.Profile.LastName.ToString()}", _userContext.User.Profile.IssuerName, domain, _userContext.User.Profile.Phone, _userContext.User.Email, _userContext.User.Profile.Address1, _userContext.User.Profile.City, _userContext.User.Profile.State, _userContext.User.Profile.Zip, approvableItems.ToList(), RetrieveShippingInformationForApproval(Convert.ToInt32(OrderID)), userId);
            }
        }
    }

    private struct CartItemPrice
    {
        public decimal Price { get; set; }
        public int Quantity { get; set; }
    }
}