﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ApproveOrder : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.Page.Title = Resources.ShoppingCart.ApproveOrder_Title;
    }

    public IUserService _userServices { get { return DependencyResolver.Current.GetService<IUserService>(); } }
    IIssuerService _issuerService { get { return DependencyResolver.Current.GetService<IIssuerService>(); } }
    IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }
    IEmailService _emailService { get { return DependencyResolver.Current.GetService<IEmailService>(); } }
    UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string orderString = string.Empty;
            if (Page.RouteData.Values["orderid"] != null)
            {
                orderString = _orderService.GetOrderIDByOrderNo(Page.RouteData.Values["orderid"].ToString()).ToString();
            }

            if (!string.IsNullOrEmpty(orderString))
            {
                int orderId = Convert.ToInt32(orderString);
                RetrieveOrderDetailResult orderDetail = _orderService.GetOrderDetails(orderId);
                string nextStatus = orderDetail.OrderStatus;
                List<RetrieveOrderItemsResult> orderItems = _orderService.GetOrderItems(orderId);

                if (nextStatus == "Pending MasterCard Approval")
                {
                    bool canShip = true;
                    bool canFulfill = true;
                    foreach (RetrieveOrderItemsResult item in orderItems)
                    {
                        if (canShip && item.ElectronicDelivery)
                        {
                            nextStatus = "Shipped";
                        }
                        else if (canFulfill && item.Customizations.HasValue && !item.Customizations.Value)
                        {
                            nextStatus = "Fulfillment";
                            canShip = false;
                        }
                        else
                        {
                            nextStatus = "Pending Customization";
                            canShip = false;
                            canFulfill = false;
                        }
                    }

                    if (nextStatus == "Pending Customization")
                    {
                        _orderService.SetOrderStatus(orderId, "Pending Customization");
                        _emailService.SendNewOrderEmail(orderDetail.OrderNumber.ToString(), true, orderItems.Any(i => i.VDP), _userContext.User.UserId, orderDetail.RushOrder);
                    }
                    else if (nextStatus == "Shipped")
                    {
                        _orderService.SetOrderStatus(orderId, "Shipped");
                        SendElectronicDeliveryOrderApprovedEmail(orderDetail, orderItems);
                    }
                    else
                    {
                        _orderService.SetOrderStatus(orderId, "Fulfillment");
                        _emailService.SendNewOrderEmail(orderDetail.OrderNumber.ToString(), false, orderItems.Any(i => i.VDP), _userContext.User.UserId, orderDetail.RushOrder);
                    }
                }
            }
        }
    }

    protected void SendElectronicDeliveryOrderApprovedEmail(RetrieveOrderDetailResult orderDetail, List<RetrieveOrderItemsResult> orderItems)
    {
        if (orderItems != null)
        {
            List<TableItem> listitems;

            NumberFormatInfo formatInfo = new NumberFormatInfo();
            formatInfo.CurrencySymbol = "$";
            formatInfo.NumberDecimalDigits = 2;

            decimal totals = 0;
            StringBuilder sb = new StringBuilder();
            string result = string.Empty;

            EmailText et;
            listitems = new List<TableItem>();

            sb.Append(".................................................................");
            sb.Append(Environment.NewLine);

            TableItem t = new TableItem();

            t.Head = Resources.ShoppingCart.Sku.ToUpperInvariant();
            t.Width = 12;
            t.Align = "Left";
            listitems.Add(t);

            t = new TableItem();
            t.Head = Resources.ShoppingCart.Title;
            t.Width = 22;
            t.Align = "Left";
            listitems.Add(t);

            t = new TableItem();
            t.Head = Resources.ShoppingCart.Quantity;
            t.Width = 17;
            t.Align = "Left";
            listitems.Add(t);

            t = new TableItem();
            t.Head = Resources.ShoppingCart.Price;
            t.Width = 17;
            t.Align = "Left";
            listitems.Add(t);

            et = new EmailText();
            result = et.GenerateText(listitems);

            sb.Append(result);
            sb.Append(".................................................................");
            sb.Append(Environment.NewLine);

            foreach (RetrieveOrderItemsResult r in orderItems)
            {
                listitems.Clear();

                t = new TableItem();

                t.Head = r.SKU.ToString() + " ";
                t.Width = 12;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = r.ItemName.ToString() + " ";
                t.Width = 22;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = r.ItemQuantity.ToString() + " ";
                t.Width = 14;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = r.ItemPrice.HasValue ? r.ItemPrice.Value.ToString("C", formatInfo) : "";
                totals = totals + Convert.ToDecimal(r.ItemPrice.ToString());
                t.Width = 12;
                t.Align = "Right";
                listitems.Add(t);

                et = new EmailText();
                result = et.GenerateText(listitems);
                sb.Append(result);
            }

            RetrievePromotionForOrderResult promotion = _orderService.GetPromotion(orderDetail.OrderID);
            if (promotion != null)
            {
                totals -= Convert.ToDecimal(promotion.PromotionAmount);

                t = new TableItem();
                listitems.Clear();

                t.Head = promotion.PromotionDescription;
                t.Width = 48;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = $"$-{Convert.ToDecimal(promotion.PromotionAmount).ToString("N2")}";
                t.Width = 14;
                t.Align = "Right";
                listitems.Add(t);

                et = new EmailText();
                result = et.GenerateText(listitems);
                sb.Append(result);
            }

            sb.Append(".................................................................");
            sb.Append(Environment.NewLine);

            t = new TableItem();
            listitems.Clear();

            t.Head = string.Empty;
            t.Width = 12;
            t.Align = "Left";
            listitems.Add(t);

            t = new TableItem();
            t.Head = string.Empty;
            t.Width = 22;
            t.Align = "Left";
            listitems.Add(t);

            t = new TableItem();
            t.Head = "Total: ";
            t.Width = 14;
            t.Align = "Right";
            listitems.Add(t);

            t = new TableItem();
            t.Head = totals.ToString("C", formatInfo) + "*";
            t.Width = 13;
            t.Align = "Right";
            listitems.Add(t);

            et = new EmailText();
            result = et.GenerateText(listitems);
            sb.Append(result);
            sb.Append(".................................................................");

            string electronicDeliveryTemplate = string.Empty;
            List<RetrieveOrderItemsResult> downloadItems = orderItems.Where(i => i.ElectronicDelivery).ToList();
            if (downloadItems.Count > 0)
            {
                StreamReader reader = new StreamReader(Server.MapPath(WebConfigurationManager.AppSettings["ElectronicDelivery.TemplateFile"]));
                electronicDeliveryTemplate = reader.ReadToEnd();
                reader.Close();

                StringBuilder sbDownloadLinks = new StringBuilder();
                string downloadUrlFormat = $"{RegionalizeService.GetBaseUrl(_userContext.SelectedRegion).TrimEnd('/')}/{WebConfigurationManager.AppSettings["ElectronicDelivery.DownloadUrlFormat"].TrimStart('/')}";
                foreach (RetrieveOrderItemsResult item in downloadItems)
                {
                    sbDownloadLinks.Append(downloadUrlFormat.Replace("{key}", item.ElectronicDeliveryID.ToString()).Replace("{sku}", Regex.Replace(item.SKU.Replace(" ", ""), @"[^\w\-\+\~]", "")));
                    sbDownloadLinks.Append(Environment.NewLine);
                }

                electronicDeliveryTemplate = electronicDeliveryTemplate.Replace("[#EXPIRATION_DATE]", downloadItems[0].ExpirationDate.Value.ToString("MM/dd/yy"));
                electronicDeliveryTemplate = electronicDeliveryTemplate.Replace("[#MAX_DOWNLOAD]", WebConfigurationManager.AppSettings["ElectronicDelivery.MaxDownload"]);
                electronicDeliveryTemplate = electronicDeliveryTemplate.Replace("[#DOWNLOAD_LINKS]", sbDownloadLinks.ToString());
            }

            _emailService.SendElectronicDeliveryOrderApprovedEmail(orderDetail.OrderNumber, orderDetail.OrderDate.Value.ToString("MM/dd/yyyy"), sb.ToString(), _userContext.User.Email, _userContext.User.Profile.FirstName.ToString(), _userContext.User.Profile.LastName.ToString(), electronicDeliveryTemplate, _userContext.User.UserId);
        }
    }
}