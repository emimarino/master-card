﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebFormsPages/MMP.master" AutoEventWireup="true" Inherits="MasterICAImporter" CodeBehind="MasterICAImporter.aspx.cs" %>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="clr">
        <p class="grey_head"><%=Resources.ShoppingCart.MasterIcaImporter_Title%></p>
    </div>
    <div>
        <asp:FileUpload ID="fileUpload" runat="server" Width="400" />
        <asp:Button ID="btnUpload" Text="Upload" runat="server" OnClick="btnUpload_Click" />
    </div>
    <asp:PlaceHolder ID="phError" runat="server" Visible="false">
        <p style="color: #ff0000;">
            <asp:Literal ID="litError" runat="server"></asp:Literal>
        </p>
    </asp:PlaceHolder>
</asp:Content>