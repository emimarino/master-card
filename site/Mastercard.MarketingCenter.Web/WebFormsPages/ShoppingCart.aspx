﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/WebFormsPages/MMPShoppingCart.Master" Inherits="ShoppingCart_Page" Title="Shopping Cart" CodeBehind="ShoppingCart.aspx.cs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadJavascriptPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="center_container">
        <div class="form_container">
            <asp:HiddenField ID="hdShoppingCartId" runat="server" />
            <div class="orange_heading_profile"><%=Resources.ShoppingCart.ShoppingCart_Title%></div>
            <div class="clr"></div>

            <div class="faq_blog">
                <p id="defaultHelpMessage" runat="server"><%=Resources.ShoppingCart.ShoppingCart_Description%></p>

                <div id="multiAddressHelpMessage" runat="server" class="roundcont_error" style="margin: 25px 0;">
                    <div class="roundtop">
                        <img src="/portal/inc/images/tl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
                    </div>
                    <p>
                        <%=Resources.ShoppingCart.ShoppingCart_MultipleAddressHelpMessage%>
                    </p>
                    <div class="roundbottom">
                        <img src="/portal/inc/images/bl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
                    </div>
                </div>

                <asp:PlaceHolder runat="server" ID="plcError" Visible="false">
                    <asp:Label runat="server" ID="lblError" CssClass="red_error"></asp:Label>
                    <br />
                    <br />
                </asp:PlaceHolder>
                <div id="divNoOrder" runat="server" visible="false">
                    <p>
                        <span class="grey_heading_bld"><%=Resources.ShoppingCart.ShoppingCart_CartNoItems%></span>
                    </p>
                </div>
            </div>
            <div class="form_container">
                <div class="faq_right_container">
                    <div class="right_tab_top">
                        <div class="orange_heading_main">
                            <%=Resources.ShoppingCart.ItemTypes%>
                        </div>
                    </div>
                    <div class="right_tab_bg">
                        <p class="none_padding">
                            <%=Resources.ShoppingCart.ItemTypesInstructions%>
                        </p>
                        <div class="link_img">
                            <div class="content_span"><%=Resources.ShoppingCart.Customizable%></div>
                            <div class="image_span">
                                <img src="/portal/inc/images/document_edit.png" border="0" alt="" width="25" height="27" />
                            </div>
                        </div>
                        <div class="link_img">
                            <div class="content_span"><%=Resources.ShoppingCart.MastercardBrandedCollateral%></div>
                            <div class="image_span">
                                <img src="/portal/inc/images/document_lock.png" border="0" alt="" width="25" height="27" />
                            </div>
                        </div>
                        <div class="link_img">
                            <div class="content_span"><%=Resources.ShoppingCart.VariableDataPrinting%></div>
                            <div class="image_span">
                                <img src="/portal/inc/images/vdp.png" border="0" alt="" width="25" height="27" />
                            </div>
                        </div>
                        <div class="link_img">
                            <div class="content_span"><%=Resources.ShoppingCart.ElectronicDelivery%></div>
                            <div class="image_span">
                                <img src="/portal/inc/images/document_down.png" border="0" alt="" width="25" height="27" />
                            </div>
                        </div>
                    </div>
                    <div class="right_tab_bot">
                    </div>
                </div>
                <div class="faq_blog">
                    <asp:GridView ID="grdShoppingCart" CssClass="main_heading_bg" runat="server" AutoGenerateColumns="false" BorderStyle="None" GridLines="None" OnRowDataBound="grdShoppingCart_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    <div class="main_heading">
                                        <span class="order_heading width110 pdl_15px"><%=Resources.ShoppingCart.Sku.ToUpperInvariant()%></span>
                                        <span class="order_heading width150"><%=Resources.ShoppingCart.Title%></span>
                                        <span class="order_heading">&nbsp;</span>
                                        <span class="order_heading"><%=Resources.ShoppingCart.Quantity%></span>
                                        <span class="order_heading"><%=Resources.ShoppingCart.Type%></span>
                                        <span class="order_heading" style="text-align: right;"><%=Resources.ShoppingCart.Price%></span>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="chk_outgrid ">
                                        <span class="order_heading_data width110 pdl_15px word_wrap"><%# Eval("SKU").ToString()%></span>
                                        <span class="order_heading_data width150"><%# Eval("Title").ToString()%></span>
                                        <span class="order_heading_data">&nbsp;</span>
                                        <span class="order_heading_data">
                                            <asp:TextBox CssClass="text_right" MaxLength="6" runat="server" ID="txtQuantity" Visible='<%# !multipleShippingAddresses && !Convert.ToBoolean(Eval("ElectronicDelivery")) %>' Text='<%# Eval("Quantity").ToString()%>' onkeydown="return ForceNumericInput(event,false,false);" size="20"></asp:TextBox>
                                            <asp:Label ID="lblQuantity" Visible='<%# multipleShippingAddresses || Convert.ToBoolean(Eval("ElectronicDelivery"))%>' runat="server" Text='<%# Eval("Quantity").ToString()%>'></asp:Label>
                                        </span>
                                        <span class="order_heading_data">
                                            <asp:Image ID="imgIcon" runat="server" />
                                        </span>
                                        <asp:Label runat="server" Visible="false" ID="lblShoppingCartId" Text='<%# Eval("CartID").ToString()%>'></asp:Label>
                                        <asp:Label runat="server" Visible="false" ID="lblGlobalID" Text='<%# Eval("ContentItemID").ToString()%>'></asp:Label>
                                        <span class="order_heading_data width150 text_right">
                                            <asp:Label runat="server" Visible="true" ID="lblPrice">  <%# GetPrice(Eval("ContentItemID").ToString(), Eval("Quantity").ToString(), Convert.ToBoolean(Eval("ElectronicDelivery")))%>  </asp:Label>
                                            <asp:LinkButton ID="linkRemove" runat="server" CommandArgument='<%# Eval("CartID").ToString() + "," + Eval("ContentItemID").ToString() + "," + Eval("ElectronicDelivery").ToString()%>' CommandName="Remove" Text="<%$Resources:ShoppingCart,ButtonRemoveLabel%>"></asp:LinkButton>
                                        </span>
                                        <asp:HiddenField ID="hdItemType" runat="server" />
                                    </div>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <div class="chk_outgrid gray_backgrnd">
                                        <span class="order_heading_data width110 pdl_15px word_wrap"><%# Eval("SKU").ToString()%></span>
                                        <span class="order_heading_data width150"><%# Eval("Title").ToString()%></span>
                                        <span class="order_heading_data">&nbsp;</span>
                                        <span class="order_heading_data">
                                            <asp:TextBox CssClass="text_right" MaxLength="6" runat="server" ID="txtQuantity" Visible='<%# !multipleShippingAddresses && !Convert.ToBoolean(Eval("ElectronicDelivery")) %>' Text='<%# Eval("Quantity").ToString()%>' onkeydown="return ForceNumericInput(event,false,false);" size="20"></asp:TextBox><asp:Label ID="lblQuantity" Visible='<%# multipleShippingAddresses || Convert.ToBoolean(Eval("ElectronicDelivery")) %>' runat="server" Text='<%# Eval("Quantity").ToString()%>'></asp:Label></span>
                                        <span class="order_heading_data">
                                            <asp:Image ID="imgIcon" runat="server" /></span>
                                        <asp:Label runat="server" Visible="false" ID="lblShoppingCartId" Text='<%# Eval("CartID").ToString()%>'></asp:Label>
                                        <asp:Label runat="server" Visible="false" ID="lblGlobalID" Text='<%# Eval("ContentItemID").ToString()%>'></asp:Label>
                                        <span class="order_heading_data width150 text_right">
                                            <asp:Label runat="server" Visible="true" ID="lblPrice">  <%# GetPrice(Eval("ContentItemID").ToString(), Eval("Quantity").ToString(), Convert.ToBoolean(Eval("ElectronicDelivery")))%>  </asp:Label>
                                            <asp:LinkButton ID="linkRemove" runat="server" CommandArgument='<%# Eval("CartID").ToString() + "," + Eval("ContentItemID").ToString() + "," + Eval("ElectronicDelivery").ToString()%>' CommandName="Remove" Text="<%$Resources:ShoppingCart,ButtonRemoveLabel%>"></asp:LinkButton></span>
                                        <asp:HiddenField ID="hdItemType" runat="server" />
                                    </div>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <asp:Panel ID="pnlPromotion" class="main_heading_bg promotion" runat="server">
                        <asp:Panel ID="pnlPromotionForm" runat="server" CssClass="chk_outgrid">
                            <asp:Label ID="lblPromotionError" runat="server" EnableViewState="false" Visible="false" CssClass="error"></asp:Label>
                            <%=Resources.ShoppingCart.EnterPromotionalCode%>
                            <div class="promotion-box">
                                <asp:TextBox ID="txtPromotionCode" runat="server"></asp:TextBox>
                                <div class="bg_btn" style="margin-left: 10px;">
                                    <asp:LinkButton ID="btnValidateCode" runat="server" Text="<%$Resources:Forms,Validate%>" CssClass="submit_bg_btn" OnClick="btnValidateCode_Click"></asp:LinkButton>
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlPromotionApplied" runat="server" Visible="false" CssClass="chk_outgrid">
                            <div class="promo-description">
                                <asp:Literal ID="litPromotionDescription" runat="server"></asp:Literal>
                            </div>
                            <div class="promo-amount">
                                <asp:Literal ID="litPromotionAmount" runat="server"></asp:Literal>
                            </div>
                        </asp:Panel>
                    </asp:Panel>

                    <asp:PlaceHolder runat="server" ID="shoppingCartFooter">

                        <div class="main_heading_bg">
                            <div class="chk_outgrid" style="border-top: 1px solid #aaa; margin-left: 15px; margin-right: 15px; width: 648px;">
                                <asp:PlaceHolder ID="phBillingICA" runat="server">
                                    <div style="width: 100%; color: red; padding-bottom: 5px;" class="error">
                                        <asp:Literal ID="litBillingICAError" runat="server"></asp:Literal>
                                    </div>
                                    <div class="billing_ica">
                                        <%=Resources.ShoppingCart.BillingIca%>:
                                        <asp:TextBox ID="txtBillingICA" runat="server"></asp:TextBox>
                                    </div>
                                </asp:PlaceHolder>
                                <div class="total_span fr">
                                    <%=Resources.ShoppingCart.Total%>:
                                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="meaning_bot"></div>
                        <div id="pnlGuideToBenefitsNote" runat="server" class="cart_requried" style="width: 100% !important;">
                            * <%=Resources.ShoppingCart.GuideToBenefitsDetails%>
                        </div>
                        <div id="pnlShippingNote" runat="server" class="cart_requried" visible="false">
                            * <%=Resources.ShoppingCart.ShippingHandlingDetails%>
                        </div>
                    </asp:PlaceHolder>

                    <div runat="server" id="divCheckOut" class="btn_left fr">
                        <asp:LinkButton runat="server" ID="linkCheckOut" CssClass="btn_right" title="<%$Resources:ShoppingCart,CheckOut_Title%>" Text="<%$Resources:Shared,Continue%> &gt;"><%=Resources.Shared.Continue%> &gt;</asp:LinkButton>
                    </div>
                    <div runat="server" id="divUpdateCart" class="btn_left fr mr11">
                        <asp:LinkButton runat="server" ID="btnUpdateCart" CssClass="btn_right" title="<%$Resources:ShoppingCart,UpdateCart%>" Text="<%$Resources:ShoppingCart,UpdateCart%>"></asp:LinkButton>
                    </div>
                    <div class="btn_left fr mr11">
                        <a href="/portal" class="btn_right" title="<%=Resources.ShoppingCart.FindMoreAssets%>">&lt; <%=Resources.ShoppingCart.FindMoreAssets%></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
