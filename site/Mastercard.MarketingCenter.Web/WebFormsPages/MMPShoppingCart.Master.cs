﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Extensions;
using Mastercard.MarketingCenter.Web.Core.Services;
using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Xml.Linq;

namespace Mastercard.MarketingCenter.Web.WebFormsPages
{
    public partial class MMPShoppingCart : System.Web.UI.MasterPage
    {
        protected string SiteName
        {
            get
            {
                return Page.GetSiteName();
            }
        }

        public UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
        public IUserService _userService { get { return DependencyResolver.Current.GetService<IUserService>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.Url.AbsolutePath.Contains("electronicdownload") &&
                !DependencyResolver.Current.GetService<IPrincipal>().IsInPermission(Constants.Permissions.CanUseShoppingCart))
            {
                Response.StatusCode = 404;
                Response.End();
            }

            Response.Cache.DisableCache();

            ManageCookieHandOffs();
            if (!IsPostBack)
            {
                var service = DependencyResolver.Current.GetService<IPageTrackingService>();
                service.TrackPageAccess();
            }
            else
            {
                System.Web.Helpers.AntiForgery.Validate();
            }

            if (WebConfigurationManager.AppSettings["EnableSSL"] != null && Request.Url.Scheme != Uri.UriSchemeHttps && WebConfigurationManager.AppSettings["EnableSSL"].ToLower() == "true")
            {
                Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"] + Request.RawUrl);
            }

            FillLogoImage();
        }

        protected string getUserName()
        {
            return _userContext.User.FullName;
        }

        private void FillLogoImage()
        {
        }

        protected string convertostring(ArrayList a)
        {
            string items = string.Empty;
            for (int i = 0; i <= a.Count - 1; i++)
            {
                if (!string.IsNullOrEmpty(items))
                    items = items + ",";
                items = items + a[i].ToString();
            }
            return items;
        }

        public static string EncodeField(string Title)
        {
            string enTitle = Title.Trim().Replace(" ", "_").Replace("&", "+").Replace("/", "~").Replace("'", "^");
            return enTitle;
        }

        private string GetPageTitle()
        {
            string sreturn = string.Empty;
            try
            {
                if (this.Page.ToString().Replace("ASP", "").Replace(".", "").Replace("_", ".") != "home.aspx")
                {
                    XDocument xd = XDocument.Load(Server.MapPath("~") + "/Web.sitemap");

                    var page = from item in xd.Root.Descendants()
                               where item.Attribute(XName.Get("template")).Value.ToLower().Contains(this.Page.ToString().Replace("ASP", "").Replace(".", "").Replace("_", "."))
                               select item;
                    if (page != null)
                        sreturn = page.FirstOrDefault().Attribute("title").Value.ToString();
                }
            }
            finally { }

            return sreturn;
        }

        private string GetPageUrl(string title)
        {
            string sreturn = string.Empty;
            try
            {
                XDocument xd = XDocument.Load(Server.MapPath("~") + "/Web.sitemap");

                var page = from item in xd.Root.Descendants()
                           where (item.Attribute(XName.Get("title")).Value.Equals(title))
                           select item;

                if (page.FirstOrDefault() != null)
                    if (page.FirstOrDefault().Attribute("url").Value != null)
                        sreturn = page.FirstOrDefault().Attribute("url").Value.ToString().Replace(" ", "-").Replace("&", "+").Replace("/", "~").Replace("'", "^");
            }
            catch (Exception e) { throw e; }
            finally { }

            return sreturn;
        }

        public string GetResource(string fileUrl)
        {
            string newFileUrl = fileUrl;
            string filePath = HttpContext.Current.Server.MapPath(fileUrl);
            if (File.Exists(filePath))
            {
                newFileUrl += "?t=" + File.GetLastWriteTime(filePath).ToFileTime();
            }

            return newFileUrl;
        }

        protected string GetSPUserName()
        {
            return _userContext.User.UserName;
        }

        protected void ManageCookieHandOffs() //todo: this is duplicated remove and consolidate
        {
            if (Session["SlamAuthenticationAndPreviewModeCookieFlag"] != null && (bool)Session["SlamAuthenticationAndPreviewModeCookieFlag"])
            {
                var authenticationService = DependencyResolver.Current.GetService<MastercardAuthenticationService>();
                authenticationService.CreateOrUpdateSlamAuthenticationCookie(_userContext.User.UserName);
                authenticationService.CreateOrUpdateSlamPreviewModeCookie();
                Session["SlamAuthenticationAndPreviewModeCookieFlag"] = null;
            }
        }
    }
}