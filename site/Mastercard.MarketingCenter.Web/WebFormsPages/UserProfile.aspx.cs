﻿using System;
using Mastercard.MarketingCenter.Web.Core.Extensions;

public partial class UserProfile : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.Page.Title = Resources.Forms.UserProfile_Title;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.DisableCache();
    }
}