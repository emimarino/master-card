﻿<%@ Page Language="C#" MasterPageFile="~/WebFormsPages/MMPShoppingCart.Master" AutoEventWireup="true"
    Inherits="ShippingAllocation" Title="Shipping Allocation" CodeBehind="ShippingAllocation.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="center_container">
        <div class="form_container">
            <div class="order_conformation">
                <div class="faq_blog">
                    <div class="fl">
                        <div class="order_detail fl">
                            <span><%=Resources.ShoppingCart.ShoppingCart_Title%></span>
                            <div style="padding: 3px 0 8px 0;"><%=Resources.ShoppingCart.ShippingAllocation_CurrentOrderItems%></div>
                        </div>
                    </div>
                    <!-- grid_container-->
                    <asp:GridView ID="grdShoppingCart" CssClass="main_heading_bg" runat="server" AutoGenerateColumns="false" BorderStyle="None" GridLines="None">
                        <Columns>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    <div class="main_heading">
                                        <span class="order_heading width110 pdl_15px"><%=Resources.ShoppingCart.Sku.ToUpperInvariant()%></span>
                                        <span class="order_heading width150 pdl_right"><%=Resources.ShoppingCart.Title%></span>
                                        <span class="order_heading width50">&nbsp;</span>
                                        <span class="order_heading"><%=Resources.ShoppingCart.Quantity%></span>
                                        <span class="order_heading width110"><%=Resources.ShoppingCart.PricePerItem%></span>
                                        <span class="order_heading"><%=Resources.ShoppingCart.Price%></span>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="chk_outgrid ">
                                        <span class="order_heading_data width110 pdl_15px word_wrap">
                                            <%# Eval("SKU").ToString()%>
                                        </span>
                                        <span class="order_heading_data width150 pdl_right">
                                            <%# Eval("Title").ToString()%>
                                        </span>
                                        <span class="order_heading_data width50">&nbsp;</span>
                                        <span class="order_heading_data">
                                            <asp:Label runat="server" ID="txtQuantity" Text='<%# Eval("Quantity").ToString()%>' readonly="true" size="20"></asp:Label>
                                        </span>
                                        <span class="order_heading_data width110">
                                            <%# GetItemPrice(Eval("ContentItemID").ToString(), Eval("Quantity").ToString(), Convert.ToBoolean(Eval("ElectronicDelivery")))%>
                                        </span>
                                        <span class="order_heading_data width150">
                                            <%# GetPrice(Eval("ContentItemID").ToString(), Eval("Quantity").ToString(), Convert.ToBoolean(Eval("ElectronicDelivery")))%>
                                        </span>
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <asp:Panel ID="pnlPromotion" class="main_heading_bg promotion" runat="server" Visible="false">
                        <asp:Panel ID="pnlPromotionApplied" runat="server" CssClass="chk_outgrid">
                            <div class="promo-description">
                                <asp:Literal ID="litPromotionDescription" runat="server"></asp:Literal>
                            </div>
                            <div class="promo-amount">
                                <asp:Literal ID="litPromotionAmount" runat="server"></asp:Literal>
                            </div>
                        </asp:Panel>
                    </asp:Panel>

                    <div class="main_heading_bg">
                        <div class="chk_outgrid">
                            <div class="total_span fr">
                                <%=Resources.ShoppingCart.Total%>:
                                <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                *
                            </div>
                        </div>
                    </div>
                    <div class="meaning_bot">
                    </div>
                    <div class="cart_requried" style="float: left; padding-bottom: 0px !important; width: 300px !important;">
                        * <%=Resources.ShoppingCart.GuideToBenefitsDetails%>
                    </div>
                    <div class="cart_requried" style="float: left; padding-bottom: 0px !important; width: 300px !important;">
                        * <%=Resources.ShoppingCart.ShippingHandlingDetails%>
                    </div>
                    <div class="bg_btn_left fr">
                        <asp:LinkButton ID="lnkUpdateCart" runat="server" Text="<%$Resources:ShoppingCart,UpdateCart%>" CssClass="submit_bg_btn"><%=Resources.ShoppingCart.UpdateCart%></asp:LinkButton>
                    </div>
                    <div style="clear: both;"></div>
                    <!-- grid_container-->

                    <div class="fl">
                        <div class="order_detail fl">
                            <span><%=Resources.ShoppingCart.ShippingAllocation%></span>
                            <div style="padding: 3px 0 8px 0;"><%=Resources.ShoppingCart.ShippingAllocationDescription%></div>
                        </div>
                    </div>
                    <asp:Repeater ID="rptShippingAddresses" runat="server" OnItemDataBound="rptShippingAddresses_ItemDataBound">
                        <HeaderTemplate>
                            <div class="main_heading ">
                                <span class="order_heading width295 pdl_15px"><%=Resources.ShoppingCart.ShippingInformation%></span>
                                <span class="order_heading width95"><%=Resources.ShoppingCart.Sku.ToUpperInvariant()%></span>
                                <span class="order_heading width150 pdl_right"><%=Resources.ShoppingCart.Title%></span>
                                <span class="order_heading width100"><%=Resources.ShoppingCart.Quantity%></span>
                            </div>
                            <div class="main_heading_bg">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="chk_outgrid">
                                <div class="right_block">
                                    <asp:Repeater ID="rptInnerCartItems" runat="server">
                                        <ItemTemplate>
                                            <div style="margin-bottom: 4px">
                                                <span class="order_heading_data width95 word_wrap">
                                                    <%# Eval("SKU").ToString()%>
                                                </span>
                                                <span class="order_heading_data width150 pdl_right">
                                                    <%# Eval("Title").ToString()%>
                                                </span>
                                                <span class="order_heading_data width100">
                                                    <asp:TextBox runat="server" ID="txtQuantity" AutoPostBack="true" OnTextChanged="txtQuantity_TextChanged" onkeydown="return ForceNumericInput(event,false,false);" Text='<%# Eval("Quantity").ToString()%>'></asp:TextBox>
                                                </span>
                                                <asp:HiddenField ID="hdnContentItemID" runat="server" />
                                                <div class="clr"></div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                                <div class="left_block">
                                    <span class="order_heading_data width175 pdl_15px"><%# Eval("Address").ToString().Replace(Environment.NewLine, "<br />")%></span>
                                    <span class="order_heading_data"><%# Eval("ContactName").ToString()%><br />
                                        <%# Eval("Phone").ToString()%></span>
                                </div>
                            </div>
                            <asp:HiddenField ID="hdnShippingInformationId" runat="server" />
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <div class="chk_outgrid gray_backgrnd">
                                <div class="right_block">
                                    <asp:Repeater ID="rptInnerCartItems" runat="server">
                                        <ItemTemplate>
                                            <div style="margin-bottom: 4px">
                                                <span class="order_heading_data width95 word_wrap">
                                                    <%# Eval("SKU").ToString()%>
                                                </span>
                                                <span class="order_heading_data width150 pdl_right">
                                                    <%# Eval("Title").ToString()%>
                                                </span>
                                                <span class="order_heading_data width100">
                                                    <asp:TextBox runat="server" ID="txtQuantity" AutoPostBack="true" OnTextChanged="txtQuantity_TextChanged" onkeydown="return ForceNumericInput(event,false,false);" Text='<%# Eval("Quantity").ToString()%>'></asp:TextBox>
                                                </span>
                                                <asp:HiddenField ID="hdnContentItemID" runat="server" />
                                                <div class="clr"></div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                                <div class="left_block">
                                    <span class="order_heading_data width175 pdl_15px"><%# Eval("Address").ToString().Replace(Environment.NewLine, "<br />")%></span>
                                    <span class="order_heading_data"><%# Eval("ContactName").ToString()%><br />
                                        <%# Eval("Phone").ToString()%></span>
                                </div>
                            </div>
                            <asp:HiddenField ID="hdnShippingInformationId" runat="server" />
                        </AlternatingItemTemplate>
                        <FooterTemplate>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="main_heading_bot"></div>
                <!--end grid container-->
                <div class="bg_btn fr mt9">
                    <asp:LinkButton ID="bnSubmit" runat="server" Text="<%$Resources:Shared,Continue%>" CssClass="submit_bg_btn" OnClick="bnSubmit_Click"><%=Resources.Shared.Continue%> &gt;</asp:LinkButton>
                </div>
                <div class="bg_btn_left fr mr11">
                    <asp:LinkButton ID="btnCancel" runat="server" Text="<%$Resources:Shared,Back%>" CssClass="submit_bg_btn" OnClick="btnCancel_Click"> &lt; <%=Resources.Shared.Back%></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>