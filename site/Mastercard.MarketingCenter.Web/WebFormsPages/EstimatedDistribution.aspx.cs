﻿using Mastercard.MarketingCenter.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.WebFormsPages
{
    public partial class EstimatedDistribution : System.Web.UI.Page
    {
        UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Page.Title = Resources.ShoppingCart.EstimatedDistribution_Title;

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        private void LoadData()
        {
            var cartId = ShoppingCartService.GetActiveShoppingCart(_userContext.User.UserName);
            if (cartId.HasValue)
            {
                var lsCartItems = ShoppingCartService.GetShoppingCartByUserId(_userContext.User.UserName).Where(c => c.ElectronicDelivery);
                var cartItems = ShoppingCartService.GetShoppingCartItems(cartId.Value).Where(ci => ci.ElectronicDelivery);
                var edCartItems = new List<EstimatedDistributionCartItem>();
                foreach (var lsCartItem in lsCartItems)
                {
                    edCartItems.Add(new EstimatedDistributionCartItem()
                    {
                        ContentItemId = lsCartItem.ContentItemID,
                        SKU = lsCartItem.SKU,
                        Title = lsCartItem.Title,
                        EstimatedDistribution = cartItems.Where(ci => ci.ContentItemID == lsCartItem.ContentItemID).FirstOrDefault().EstimatedDistribution
                    });
                }

                grdEstimatedDistribution.DataSource = edCartItems;
                grdEstimatedDistribution.DataBind();
                hdShoppingCartId.Value = cartId.ToString();
            }
        }

        protected void grdEstimatedDistribution_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var item = (EstimatedDistributionCartItem)e.Row.DataItem;
                HiddenField hdnContentItemId = (HiddenField)e.Row.FindControl("hdnContentItemId");
                hdnContentItemId.Value = item.ContentItemId;
            }
        }

        protected void lnkSubmit_Click(object sender, EventArgs e)
        {
            if (Validate())
            {
                var cartId = int.Parse(hdShoppingCartId.Value);
                var userId = _userContext.User.UserName;
                foreach (GridViewRow r in grdEstimatedDistribution.Rows)
                {
                    var txtEstimatedDistribution = (TextBox)r.FindControl("txtEstimatedDistribution");
                    var hdnContentItemId = (HiddenField)r.FindControl("hdnContentItemId");

                    ShoppingCartService.UpdateShoppingCartItemEstimatedDistribution(cartId, hdnContentItemId.Value, int.Parse(txtEstimatedDistribution.Text.Trim()));
                }

                var lsCartItems = ShoppingCartService.GetShoppingCartByUserId(userId);
                var ls = ShoppingCartService.GetCustomizationOptions(userId, cartId, true);
                if (ls.Any())
                {
                    Response.Redirect("/portal/checkout");
                }
                else
                {
                    if (lsCartItems.Any(i => (i.VDP == null || !i.VDP.Value) && !i.ElectronicDelivery))
                    {
                        Response.Redirect("/portal/shippinginformation");
                    }
                    else
                    {
                        Response.Redirect("/portal/revieworder");
                    }
                }
            }
        }

        protected void lnkCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("/portal/shoppingcart");
        }

        private new bool Validate()
        {
            plcError.Visible = false;
            foreach (GridViewRow r in grdEstimatedDistribution.Rows)
            {
                int iQuantity = 0;
                var txtEstimatedDistribution = (TextBox)r.FindControl("txtEstimatedDistribution");
                if (txtEstimatedDistribution.Text.Trim().Length == 0)
                {
                    plcError.Visible = true;
                    lblError.Text = Resources.Errors.EstimatedDistributionEmpty;
                    return false;
                }
                else if (!int.TryParse(txtEstimatedDistribution.Text.Trim(), out iQuantity))
                {
                    plcError.Visible = true;
                    lblError.Text = Resources.Errors.EstimatedDistributionInvalid;
                    return false;
                }
                else if (int.Parse(txtEstimatedDistribution.Text.Trim()) <= 1)
                {
                    plcError.Visible = true;
                    lblError.Text = string.Format(Resources.Errors.EstimatedDistributionGreater, 1);
                    return false;
                }
            }
            return true;
        }
    }

    public class EstimatedDistributionCartItem
    {
        public string ContentItemId { get; set; }
        public string SKU { get; set; }
        public string Title { get; set; }
        public int? EstimatedDistribution { get; set; }
    }
}