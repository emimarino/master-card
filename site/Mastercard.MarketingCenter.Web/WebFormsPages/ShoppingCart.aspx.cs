﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

public partial class ShoppingCart_Page : System.Web.UI.Page
{
    decimal total = 0;
    protected bool multipleShippingAddresses = false;

    UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
    IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }
    IEmailService _emailService { get { return DependencyResolver.Current.GetService<IEmailService>(); } }
    IIssuerService _issuerService { get { return DependencyResolver.Current.GetService<IIssuerService>(); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            BindGrid();
            LoadBillingICA();
            if (Session["CartClicked"] != null)
            {
                Session["CartClicked"] = "ViewCart";
            }
            else
            {
                Session["CartClicked"] = "OpenCart";
            }
        }
    }

    private void LoadBillingICA()
    {
        var icas = _issuerService.GetMasterICAsByBillingId(_userContext.User.Issuer.BillingId);
        if (icas != null && icas.Count() > 0)
        {
            int? shoppingCartId = ShoppingCartService.GetActiveShoppingCart(_userContext.User.UserName);
            if (shoppingCartId.HasValue)
            {
                var cart = ShoppingCartService.GetShoppingCartById(shoppingCartId.Value);
                if (cart.BillingID.HasValue)
                {
                    txtBillingICA.Text = cart.BillingID.Value.ToString();
                }
                else
                {
                    // first we check the latest order ICA
                    var latestOrder = _orderService.GetLatestOrderForUser(_userContext.User.UserName);
                    if (latestOrder != null && latestOrder.BillingID.HasValue)
                    {
                        txtBillingICA.Text = latestOrder.BillingID.Value.ToString();
                    }
                }
            }
        }
        else
        {
            phBillingICA.Visible = false;
        }
    }

    private void BindGrid()
    {
        total = 0;
        List<RetrieveShoppingCartByUserIdResult> lsCartItems = ShoppingCartService.GetShoppingCartByUserId(_userContext.User.UserName);
        if (lsCartItems.Count > 0)
        {
            grdShoppingCart.DataSource = lsCartItems;
            grdShoppingCart.DataBind();

            hdShoppingCartId.Value = lsCartItems[0].CartID.ToString();
            if (!string.IsNullOrEmpty(lsCartItems[0].PromotionCode))
            {
                PromotionCodeResponse promotionResponse = PromotionService.ApplyPromotionCode(lsCartItems[0].PromotionCode, lsCartItems[0].CartID, _userContext.User.IssuerId);
                if (string.IsNullOrEmpty(promotionResponse.ErrorMessage))
                {
                    litPromotionDescription.Text = promotionResponse.Description;
                    litPromotionAmount.Text = $"$-{(promotionResponse.DiscountAmount < total ? promotionResponse.DiscountAmount.ToString("N2") : total.ToString("N2"))}";
                    pnlPromotionApplied.Visible = true;
                    pnlPromotionForm.Visible = false;

                    lblTotal.Text = FormatNumber(promotionResponse.DiscountAmount < total ? total - promotionResponse.DiscountAmount : 0);
                }
                else
                {
                    PromotionService.RemovePromotionCode(lsCartItems[0].CartID);
                    lblPromotionError.Text = Resources.Errors.PromotionCodeRemoved;
                    lblPromotionError.Visible = true;
                }
            }

            if (lsCartItems.Any(i => !i.ElectronicDelivery))
            {
                if (lsCartItems.Any(i => i.VDP == null || !i.VDP.Value))
                {
                    multipleShippingAddresses = ShoppingCartService.CartHasMultipleShippingAddresses(lsCartItems[0].CartID);

                    defaultHelpMessage.Visible = !multipleShippingAddresses;
                    multiAddressHelpMessage.Visible = multipleShippingAddresses;

                    if (!lsCartItems.Any(i => i.VDP != null && i.VDP.Value))
                    {
                        divUpdateCart.Visible = !multipleShippingAddresses;
                    }
                }
                else
                {
                    multiAddressHelpMessage.Visible = false;
                }

                pnlShippingNote.Visible = true;
                lblTotal.Text += "*";
            }
            else
            {
                multiAddressHelpMessage.Visible = false;
                divUpdateCart.Visible = false;
            }

            linkCheckOut.Enabled = true;
            btnUpdateCart.Enabled = true;
            divNoOrder.Visible = false;
            divUpdateCart.Attributes.Remove("class");
            divUpdateCart.Attributes.Add("class", "btn_left fr mr11");

            divCheckOut.Attributes.Remove("class");
            divCheckOut.Attributes.Add("class", "btn_left fr mr11");

            btnUpdateCart.CssClass = "btn_right ";
            linkCheckOut.CssClass = "btn_right";
            shoppingCartFooter.Visible = true;
            grdShoppingCart.Visible = true;
        }
        else
        {
            multiAddressHelpMessage.Visible = false;
            grdShoppingCart.Visible = false;
            divUpdateCart.Attributes.Remove("class");
            divUpdateCart.Attributes.Add("class", "bg_grey_left fr mr11");

            divCheckOut.Attributes.Remove("class");
            divCheckOut.Attributes.Add("class", "bg_grey_left fr mr11");

            divNoOrder.Visible = true;
            linkCheckOut.Enabled = false;
            btnUpdateCart.Enabled = false;
            btnUpdateCart.CssClass = "grey_bg_btn";
            linkCheckOut.CssClass = "grey_bg_btn";
            shoppingCartFooter.Visible = false;

            pnlPromotion.Visible = false;
        }
    }

    private new bool Validate()
    {
        plcError.Visible = false;
        foreach (GridViewRow r in grdShoppingCart.Rows)
        {
            int iQuantity = 0;
            TextBox txtQuantity = (TextBox)r.FindControl("txtQuantity");
            if (txtQuantity.Text.Trim().Length == 0)
            {
                plcError.Visible = true;
                lblError.Text = Resources.Errors.QuantityEmpty;
                return false;
            }
            else if (!int.TryParse(txtQuantity.Text.Trim(), out iQuantity))
            {
                plcError.Visible = true;
                lblError.Text = Resources.Errors.QuantityInvalid;
                return false;
            }
        }

        if (phBillingICA.Visible)
        {
            if (string.IsNullOrEmpty(txtBillingICA.Text.Trim()))
            {
                litBillingICAError.Text = Resources.Errors.IcaEmpty;
                return false;
            }

            int ica;
            if (int.TryParse(txtBillingICA.Text.Trim(), out ica))
            {
                if (!_issuerService.GetMasterICAsByBillingId(_userContext.User.Issuer.BillingId).Where(i => i.Ica == ica).Any())
                {
                    _emailService.WrongICAEmail(_userContext.User.Email, ica.ToString(), _userContext.User.UserId);
                    litBillingICAError.Text = Resources.Errors.IcaInvalid;
                    return false;
                }
            }
            else
            {
                litBillingICAError.Text = Resources.Errors.IcaInvalid;
                return false;
            }
        }

        return true;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.Page.Title = Resources.ShoppingCart.ShoppingCart_Title;
        grdShoppingCart.RowCommand += new GridViewCommandEventHandler(grdShoppingCart_RowCommand);
        linkCheckOut.Click += new EventHandler(linkCheckOut_Click);
        btnUpdateCart.Click += new EventHandler(btnUpdateCart_Click);
    }

    protected void grdShoppingCart_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            RetrieveShoppingCartByUserIdResult item = (RetrieveShoppingCartByUserIdResult)e.Row.DataItem;

            HiddenField itemType = (HiddenField)e.Row.FindControl("hdItemType");
            Image imgIcon = (Image)e.Row.FindControl("imgIcon");

            if (item.ElectronicDelivery)
            {
                itemType.Value = "ElectronicDelivery";
                imgIcon.ImageUrl = "~/inc/images/document_down.png";
            }
            else
            {
                if (item.VDP != null && item.VDP.Value)
                {
                    itemType.Value = "VDP";
                    imgIcon.ImageUrl = "~/inc/images/vdp.png";
                }
                else
                {
                    itemType.Value = "Print";
                    if (item.Customizable != null && item.Customizable.Value)
                    {
                        imgIcon.ImageUrl = "~/inc/images/document_edit.png";
                    }
                    else
                    {
                        imgIcon.ImageUrl = "~/inc/images/document_lock.png";
                    }
                }
            }
        }
    }

    void btnUpdateCart_Click(object sender, EventArgs e)
    {
        if (grdShoppingCart.Rows.Count > 0)
        {
            if (Validate())
            {
                List<RetrieveShoppingCartByUserIdResult> lsCartItems = ShoppingCartService.GetShoppingCartByUserId(_userContext.User.UserName);
                bool multipleShipping = ShoppingCartService.CartHasMultipleShippingAddresses(lsCartItems[0].CartID);
                int count = 0;
                int ShoppingCartCount = 0;
                ShoppingCartCount = grdShoppingCart.Rows.Count;
                foreach (GridViewRow r in grdShoppingCart.Rows)
                {
                    Label lblShoppingCartId = (Label)r.FindControl("lblShoppingCartId");

                    Label lblGlobalId = (Label)r.FindControl("lblGlobalId");
                    Label lblPrice = (Label)r.FindControl("lblPrice");

                    HiddenField hdItemType = (HiddenField)r.FindControl("hdItemType");

                    if (hdItemType.Value != "ElectronicDelivery")
                    {
                        TextBox txtQuantity = (TextBox)r.FindControl("txtQuantity");
                        if (txtQuantity.Text.Trim().Length > 0 && lblGlobalId.Text.Trim().Length > 0)
                        {
                            if (Convert.ToInt32(txtQuantity.Text.Trim()) > 0)
                            {
                                lblPrice.Text = GetPrice(lblGlobalId.Text, txtQuantity.Text, false);
                                if (hdItemType.Value == "VDP")
                                {
                                    ShoppingCartService.SaveShoppingCartItem(int.Parse(lblShoppingCartId.Text), lblGlobalId.Text, 0, decimal.ToInt32(decimal.Parse(txtQuantity.Text.Trim())), false);
                                }
                                else if (!multipleShipping)
                                {
                                    ShoppingCartService.SaveShoppingCartItem(int.Parse(lblShoppingCartId.Text), lblGlobalId.Text, ShippingServices.GetDefaultShippingInformationIdForCart(int.Parse(lblShoppingCartId.Text)), decimal.ToInt32(decimal.Parse(txtQuantity.Text.Trim())), false);
                                }
                            }
                            else
                            {
                                ShoppingCartService.RemoveShoppingCartItem(int.Parse(lblShoppingCartId.Text), lblGlobalId.Text, false);
                                ShoppingCartService.ResetCustomizationData(int.Parse(lblShoppingCartId.Text));
                                count++;
                            }
                        }
                    }
                }

                BindGrid();
            }
        }
    }

    void linkCheckOut_Click(object sender, EventArgs e)
    {
        string ShoppingCartId = string.Empty;
        bool containsPrint = false;
        if (grdShoppingCart.Rows.Count > 0)
        {
            if (Validate())
            {
                if (phBillingICA.Visible)
                {
                    ShoppingCartService.UpdateShoppingCartBillingICA(int.Parse(hdShoppingCartId.Value), int.Parse(txtBillingICA.Text));
                }

                if (divUpdateCart.Visible)
                {
                    btnUpdateCart_Click(null, null);
                }

                foreach (GridViewRow r in grdShoppingCart.Rows)
                {
                    Label lblShoppingCartId = (Label)r.FindControl("lblShoppingCartId");
                    ShoppingCartId = lblShoppingCartId.Text;

                    HiddenField hdItemType = (HiddenField)r.FindControl("hdItemType");
                    if (hdItemType.Value == "Print")
                    {
                        containsPrint = true;
                    }
                }

                var lsCartItems = ShoppingCartService.GetShoppingCartByUserId(_userContext.User.UserName);
                if (lsCartItems.Any(i => i.ElectronicDelivery))
                {
                    Response.Redirect("/portal/estimateddistribution");
                }
                else
                {
                    var ls = ShoppingCartService.GetCustomizationOptions(_userContext.User.UserName, int.Parse(ShoppingCartId), true);
                    if (ls.Count > 0)
                    {
                        Response.Redirect("/portal/checkout");
                    }
                    else
                    {
                        if (containsPrint)
                        {
                            Response.Redirect("/portal/shippinginformation");
                        }
                        else
                        {
                            Response.Redirect("/portal/revieworder");
                        }
                    }
                }
            }
        }
    }

    void grdShoppingCart_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName != null && e.CommandArgument != null)
        {
            string[] parameters = e.CommandArgument.ToString().Split(',');
            if (parameters.Length > 0)
            {
                ShoppingCartService.RemoveShoppingCartItem(int.Parse(parameters[0]), parameters[1], Convert.ToBoolean(parameters[2]));
                ShoppingCartService.ResetCustomizationData(int.Parse(parameters[0]));
                lblTotal.Text = "";
                Response.Redirect(this.Page.Request.Url.AbsoluteUri);
            }
        }
    }

    protected string FormatNumber(decimal num)
    {
        System.Globalization.NumberFormatInfo nf = new System.Globalization.NumberFormatInfo();
        nf.CurrencySymbol = "$";
        nf.NumberDecimalDigits = 2;
        return num.ToString("C", nf);
    }

    protected string GetPrice(string GlobalID, string Quantity, bool electronicDelivery)
    {
        string sretun = string.Empty;
        if (Quantity.Length > 0)
        {
            decimal intQuantity = decimal.Parse(Quantity);
            decimal price = ShoppingCartService.GetAssetPrice(GlobalID, decimal.ToInt32(intQuantity), electronicDelivery).TotalPrice;
            total = total + price;
            sretun = FormatNumber(price);
            lblTotal.Text = FormatNumber(total);

        }

        return sretun;
    }

    protected void btnValidateCode_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtPromotionCode.Text))
        {
            PromotionCodeResponse promotionResponse = PromotionService.ApplyPromotionCode(txtPromotionCode.Text, Convert.ToInt32(hdShoppingCartId.Value), _userContext.User.IssuerId);
            if (string.IsNullOrEmpty(promotionResponse.ErrorMessage))
            {
                Response.Redirect(Request.Url.AbsoluteUri);
            }
            else
            {
                lblPromotionError.Visible = true;
                lblPromotionError.Text = promotionResponse.ErrorMessage;
            }
        }
        else
        {
            lblPromotionError.Visible = true;
            lblPromotionError.Text = Resources.Errors.PromotionCodeEmpty;
        }
    }
}