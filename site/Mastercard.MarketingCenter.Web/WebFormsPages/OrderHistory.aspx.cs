﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

public partial class OrderHistory : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Page.Title = Resources.ShoppingCart.OrderHistory_Title;
    }

    UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
    IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetOrderItemsHistory();
        }
    }

    protected string FormatNumber(decimal num)
    {
        System.Globalization.NumberFormatInfo nf = new System.Globalization.NumberFormatInfo();
        nf.CurrencySymbol = "$";
        nf.NumberDecimalDigits = 2;
        return num.ToString("C", nf);
    }

    protected void GetOrderItemsHistory()
    {
        List<RetrieveOrderHistoryResult> OrderItems = _orderService.GetOrderHistory(_userContext.User.UserName);
        if (OrderItems != null)
        {
            if (OrderItems.Count != 0)
            {
                historygrid.Visible = true;
                divNoOrder.Visible = false;
                orderHistory.DataSource = OrderItems;
                orderHistory.DataBind();
                placeHolderMessage.Visible = true;
            }
            else
            {
                divNoOrder.Visible = true;
                historygrid.Visible = false;
                placeHolderMessage.Visible = false;
            }
        }
        else
        {
            historygrid.Visible = false;
            placeHolderMessage.Visible = false;
        }
    }
}