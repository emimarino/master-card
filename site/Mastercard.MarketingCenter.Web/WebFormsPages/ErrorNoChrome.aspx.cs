﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using System;
using System.Web.UI;

namespace Mastercard.MarketingCenter.Web.WebFormsPages
{
    public partial class ErrorNoChrome : Page
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Page.Title = Resources.Errors.PageError;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.RouteData.Values["code"] != null)
            {
                switch (Page.RouteData.Values["code"].ToString())
                {
                    case Constants.ErrorCodes.InvalidOrder:
                        phInvalidOrderError.Visible = true;
                        break;
                    case Constants.ErrorCodes.AccessDenied:
                        phAccessDeniedError.Visible = true;
                        break;
                    default:
                        phGeneralError.Visible = true;
                        break;
                }
            }
            else if (Request.QueryString["code"] != null)
            {
                switch (Request.QueryString["code"].ToString())
                {
                    case Constants.ErrorCodes.InvalidOrder:
                        phInvalidOrderError.Visible = true;
                        break;
                    case Constants.ErrorCodes.AccessDenied:
                        phAccessDeniedError.Visible = true;
                        break;
                    default:
                        phGeneralError.Visible = true;
                        break;
                }
            }
            else
            {
                phGeneralError.Visible = true;
            }
        }
    }
}