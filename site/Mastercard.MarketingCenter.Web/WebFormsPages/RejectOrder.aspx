﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebFormsPages/MMPShoppingCart.Master" AutoEventWireup="true" Inherits="RejectOrder" CodeBehind="RejectOrder.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="center_container">
        <div class="form_container">
            <div class="grey_head">
                <span><%=Resources.ShoppingCart.RejectOrder_Title%></span>
            </div>
            <asp:PlaceHolder ID="rejectionForm" runat="server">
                <div class="order_conformation">
                    <div class="landing_page_body_html_container">
                        <%=Resources.ShoppingCart.RejectOrder_Description%>
                    </div>
                    <div class="order_detail">
                        <span>
                            <asp:Label ID="lblOrderNo" runat="server"></asp:Label>
                        </span>
                    </div>
                </div>
                <div class="clr"></div>
                <div class="faq_blog">
                    <%=Resources.ShoppingCart.RejectionReasonMessage%><br />
                    <div style="width: 400px; float: left;">
                        <asp:TextBox ID="txtRejectionReason" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </div>
                    <div>
                        <span class="submit_bg_btn_left mb11 fl" style="margin-top: 0;">
                            <asp:Button ID="btnSave" Text="<%$Resources:Shared,Save%>" OnClick="btnSave_Click" CssClass="submit_bg_btn" Style="border: none;" runat="server" />
                        </span>
                    </div>
                    <div class="clr"></div>
                </div>
                <div class="clr"></div>
                <br />
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="submissionMessage" runat="server">
                <div class="order_conformation">
                    <div class="landing_page_body_html_container">
                        <%=Resources.ShoppingCart.OrderNumberCancelled.Split(new string[] { "{0}" }, StringSplitOptions.None)[0]%><asp:Label ID="lblOrderNo2" runat="server"></asp:Label><%=Resources.ShoppingCart.OrderNumberCancelled.Split(new string[] { "{0}" }, StringSplitOptions.None)[1]%>
                    </div>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="invalidMessage" runat="server">
                <div class="order_conformation">
                    <div class="landing_page_body_html_container">
                        <%=Resources.ShoppingCart.OrderNumberNotCancelled.Split(new string[] { "{0}" }, StringSplitOptions.None)[0]%><asp:Label ID="lblOrderNo3" runat="server"></asp:Label><%=Resources.ShoppingCart.OrderNumberNotCancelled.Split(new string[] { "{0}" }, StringSplitOptions.None)[1]%>
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>