﻿<%@ Page Title="Error Page" Language="C#" MasterPageFile="~/WebFormsPages/MMP.minimal.master" AutoEventWireup="true" CodeBehind="ErrorNoChrome.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.WebFormsPages.ErrorNoChrome" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div class="center_container">
        <div class="form_container">
            <div class="errorpage">
                <asp:PlaceHolder ID="phInvalidOrderError" runat="server" Visible="false">
                    <div class="orange_heading_profile"><%=Resources.Errors.OrderNumberInvalid%></div>
                    <div class="info mrt_25"><%=Resources.Errors.EmailSentToWebsiteAdministrators%></div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="phAccessDeniedError" runat="server" Visible="false">
                    <div class="orange_heading_profile"><%=Resources.Errors.PageUnavailable%></div>
                    <div class="info mrt_25"><%=Resources.Errors.ContactMastercardAccountRepresentative%></div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="phGeneralError" runat="server" Visible="false">
                    <div class="orange_heading_profile"><%=Resources.Errors.ProcessingRequest%></div>
                    <div class="info mrt_25"><%=Resources.Errors.EmailSentToWebsiteAdministrators%></div>
                </asp:PlaceHolder>
            </div>
        </div>
    </div>
</asp:Content>