﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Configuration;
using System.Security;
using System.Web.Mvc;
using Mastercard.MarketingCenter.Services;

public partial class logout : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.Page.Title = Resources.Shared.Logout;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var authenticationService = DependencyResolver.Current.GetService<MastercardAuthenticationService>();
        authenticationService.RemoveSlamAuthenticationCookie();
        authenticationService.RemoveSlamPreviewModeCookie();
        HttpContext.Current.Session.Clear();
        FormsAuthentication.SignOut();
        authenticationService.RemoveSessionIdCookie();
        Response.Redirect(FormsAuthentication.LoginUrl);
    }
}