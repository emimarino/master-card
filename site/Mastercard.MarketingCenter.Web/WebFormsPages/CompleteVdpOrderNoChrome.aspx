﻿<%@ Page Language="C#" MasterPageFile="~/WebFormsPages/MMP.minimal.master" AutoEventWireup="true" CodeBehind="CompleteVdpOrderNoChrome.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.WebFormsPages.CompleteVdpOrderNoChrome" %>

<%@ Register TagPrefix="mc" TagName="CompleteVdpOrder" Src="~/UserControls/CompleteVdpOrder.ascx" %>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <mc:CompleteVdpOrder ID="completeVdpOrder" runat="server"></mc:CompleteVdpOrder>
</asp:Content>