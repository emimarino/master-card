﻿<%@ Page Language="C#" MasterPageFile="~/WebFormsPages/MMPShoppingCart.Master" AutoEventWireup="true" Inherits="OrderHistory" Title="Untitled Page" CodeBehind="OrderHistory.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="page-wrapper cf order-history">
        <!-- Order History -->
        <header class="header-section header-first-section cold-colors content-spaces-in tac">
            <div class="inner">
                <h1><%=Resources.ShoppingCart.OrderHistory_Title%></h1>
                <asp:PlaceHolder runat="server" ID="placeHolderMessage" Visible="false">
                    <p><%=Resources.ShoppingCart.OrderHistory_Description%></p>
                </asp:PlaceHolder>
            </div>
        </header>
        <div class="content-container grid-spaces-in">
            <div class="content-text">
                <div id="divNoOrder" runat="server" visible="false">
                    <p><%=Resources.ShoppingCart.OrderNotPlaced%></p>
                </div>
                <div class="grid_container" runat="server" id="historygrid">
                    <asp:Repeater ID="orderHistory" runat="server">
                        <HeaderTemplate>
                            <table>
                                <thead>
                                    <tr>
                                        <th scope="col"><%=Resources.Shared.Date%></th>
                                        <th scope="col"><%=Resources.ShoppingCart.OrderNumber%></th>
                                        <th scope="col"><%=Resources.ShoppingCart.Items%></th>
                                        <th scope="col"><%=Resources.ShoppingCart.Price%></th>
                                        <th scope="col" style="width: 25%;"><%=Resources.ShoppingCart.Status%></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# Convert.ToDateTime(Eval("OrderDate").ToString()).ToString("MM/dd/yyyy")%></td>
                                <td><a href='orderdetails/<%# Eval("OrderNumber").ToString()%>' class='data'><%# Eval("OrderNumber").ToString()%></a></td>
                                <td><%# Eval("Items").ToString()%></td>
                                <td><%#FormatNumber(Convert.ToDecimal(Eval("Price").ToString()))%></td>
                                <td><%# Eval("OrderStatus").ToString()%></td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr>
                                <td><%# Convert.ToDateTime(Eval("OrderDate").ToString()).ToString("MM/dd/yyyy")%></td>
                                <td><a href='orderdetails/<%# Eval("OrderNumber").ToString()%>' class='data'><%# Eval("OrderNumber").ToString()%></a></td>
                                <td><%# Eval("Items").ToString()%></td>
                                <td><%#FormatNumber(Convert.ToDecimal(Eval("Price").ToString()))%></td>
                                <td><%# Eval("OrderStatus").ToString()%></td>
                            </tr>
                        </AlternatingItemTemplate>
                        <FooterTemplate>
                            </tbody>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
    </div>
</asp:Content>