﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Web.Mvc;
using System.Web.UI;

public partial class SendDataFile : System.Web.UI.Page
{
    UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
    IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }
    IEmailService _emailService { get { return DependencyResolver.Current.GetService<IEmailService>(); } }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.Page.Title = Resources.ShoppingCart.SendDataFile_Title;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.RouteData.Values["orderid"] != null)
            {
                int orderId = Convert.ToInt32(Page.RouteData.Values["orderid"]);

                FtpAccount ftp = FtpAccountServices.GetFtpAccountInformation(orderId, _userContext.User.UserName);
                if (ftp != null)
                {
                    pnlFtpPending.Visible = false;

                    if (!string.IsNullOrEmpty(ftp.Server))
                    {
                        pnlFtpAvailable.Visible = true;
                    }
                    else
                    {
                        pnlFtpRetrieved.Visible = true;
                    }
                }
            }
        }
    }

    protected void btnRetrieveFtpInfo_Click(object sender, EventArgs e)
    {
        if (Page.RouteData.Values["orderid"] != null)
        {
            int orderId = Convert.ToInt32(Page.RouteData.Values["orderid"]);
            FtpAccount ftp = FtpAccountServices.GetFtpAccountInformation(orderId, _userContext.User.UserName);
            if (ftp != null)
            {
                litServer.Text = ftp.Server;
                litUserName.Text = ftp.UserName;
                litPassword.Text = ftp.Password;

                pnlFtpInfo.Visible = true;
                pnlFtpAvailable.Visible = false;

                FtpAccountServices.ClearFtpAccountInformation(ftp.FtpAccountID);
                _emailService.SendVdpFtpAccountInformationReceivedEmail(_orderService.GetOrderNumberByOrderId(orderId), _userContext.User.UserId);
            }
        }
    }
}