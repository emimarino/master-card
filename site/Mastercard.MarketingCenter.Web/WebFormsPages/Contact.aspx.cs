﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core;
using Mastercard.MarketingCenter.Web.Core.Extensions;
using Slam.Cms.Data;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

public partial class Contact : System.Web.UI.Page
{
    string _region = string.Empty;
    string _language = string.Empty;
    string _country = string.Empty;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Page.Title = Resources.Forms.ContactUs_Title;
    }

    UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
    public IUserService _userServices { get { return DependencyResolver.Current.GetService<IUserService>(); } }
    IEmailService _emailService { get { return DependencyResolver.Current.GetService<IEmailService>(); } }

    private void GetCountriesAndLanguages()
    {
        var config = (MastercardSitemap)DependencyResolver.Current.GetService<Slam.Cms.Sitemap>();

        ConcurrentDictionary<string, IDictionary<string, string>> tempConcurrentDictionary = new ConcurrentDictionary<string, IDictionary<string, string>>();
        foreach (var reg in MastercardSitemap.AvailableRegionSitemaps.Keys)
        {
            tempConcurrentDictionary.TryAdd(reg, config.GetLanguages(reg));
        }

        RegionalizeService.Languages = new ConcurrentDictionary<string, string>(tempConcurrentDictionary.SelectMany(c => c.Value).Distinct());

        SlamContext slamContext = DependencyResolver.Current.GetService<SlamContext>();
        var categoryChildren = slamContext.GetTagTree()?.FindNode(tt => (tt.TagCategory != null ? tt.TagCategory.TagCategoryId.Equals(MarketingCenterDbConstants.TagCategories.Markets, StringComparison.OrdinalIgnoreCase) : false))?.Children;

        List<TagTreeNode> tags = new List<TagTreeNode>();
        if (categoryChildren != null)
        {
            foreach (var n in categoryChildren)
            {
                tags.AddRange(TagTree.GetLeast(n));
            }
        }

        RegionalizeService.Countries = new ConcurrentDictionary<string, string>(tags.ToDictionary(c => c.Identifier, c => c.Text));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.DisableCache();
        if (RegionalizeService.Countries == null || RegionalizeService.Languages == null)
        {
            GetCountriesAndLanguages();
        }

        if (!IsPostBack)
        {
            if (Request.UrlReferrer != null && Request.UrlReferrer.AbsolutePath != null)
            {
                if (ViewState["UrlReferrer"] != null)
                {
                    ViewState["UrlReferrer"] = Request.UrlReferrer.AbsolutePath;
                }
                else
                {
                    ViewState.Add("UrlReferrer", Request.UrlReferrer.AbsolutePath);
                }
            }

            txtFirstName.Text = _userContext.User.Profile.FirstName;
            txtLastName.Text = _userContext.User.Profile.LastName;
            txtIssuerName.Text = _userContext.User.Profile.IssuerName;
            TxtPhone.Text = _userContext.User.Profile.Phone;
            region.Value = _userContext.SelectedRegion;
            language.Value = _userContext.SelectedLanguage;
            country.Value = _userContext.User.Country;
            txtEmail.Text = _userContext.User.Email;

            MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();

            RetrieveUserInformationResult u = dataContext.RetrieveUserInformation(_userContext.User.UserName).FirstOrDefault();

            hdnUserProcessor.Value = u != null ? u.processor : string.Empty;

            radioContactOptions.SelectedIndex = 0;

            if (Request.QueryString["q"] != null)
            {
                radioContactOptions.SelectedIndex = int.Parse(Request.QueryString["q"]);
            }
        }

        _region = _userContext.SelectedRegion ?? region.Value?.Trim();
        _language = _userContext.SelectedLanguage ?? language.Value?.Trim();
        _country = country.Value?.Trim();
    }

    protected string getUrlReferrer()
    {
        string sReturn = string.Empty;
        if (ViewState["UrlReferrer"] != null)
        {
            sReturn = ViewState["UrlReferrer"].ToString();
        }

        return sReturn;
    }

    protected void SendEmail()
    {
        string toEmail;
        if (radioContactOptions.SelectedIndex > 0 && _region?.Trim()?.ToLower() == "us")
        {
            if (radioContactOptions.SelectedIndex == 1)
            {
                toEmail = RegionalizeService.RegionalizeSetting("ContactUs.PrinterEmail", "us");
            }
            else
            {
                toEmail = RegionalizeService.RegionalizeSetting("ContactUs.GuideToBenefitsEmail", "us");
            }
        }
        else
        {
            toEmail = RegionalizeService.RegionalizeSetting("ContactForm.ToEmail", _region?.Trim()?.ToLower());
        }

        _emailService.SendContactUsEmail(txtFirstName.Text, txtLastName.Text, txtEmail.Text, hdnUserProcessor.Value, txtIssuerName.Text, TxtPhone.Text, txtMessage.Text, txtSubject.Text, toEmail, _country, this.GetSiteName(), _userContext.User.UserId, _region, _language);
    }

    protected bool ValidateForm()
    {
        var errors = new List<string>();

        if (txtSubject.Text.Length == 0)
        {
            errors.Add(Resources.Errors.SubjectEmpty);
        }

        if (txtFirstName.Text.Trim().Length == 0)
        {
            errors.Add(Resources.Errors.FirstNameEmpty);
        }

        if (txtLastName.Text.Trim().Length == 0)
        {
            errors.Add(Resources.Errors.LastNameEmpty);
        }

        if (txtIssuerName.Text.Trim().Length == 0)
        {
            errors.Add(Resources.Errors.IssuerNameEmpty);
        }

        if (txtEmail.Text.Trim().Length == 0)
        {
            errors.Add(Resources.Errors.ContactUs_EmailEmpty);
        }
        else
        {
            string strRegex = @"^([a-zA-Z0-9_\'\-\.]+)@((\[[0-9]{1,3}" +
            @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
            @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex regex = new Regex(strRegex);

            if (!regex.IsMatch(txtEmail.Text.Trim()))
            {
                errors.Add(Resources.Errors.ContactUs_EmailInvalid);
            }
        }

        if (TxtPhone.Text.Trim().Length == 0)
        {
            errors.Add(Resources.Errors.PhoneEmpty);
        }

        if (txtMessage.Text.Trim().Length == 0)
        {
            errors.Add(Resources.Errors.MessageEmpty);
        }

        if (errors.Any())
        {
            errors.ForEach(error => lblerror.Text += error + "<br/>");
            return false;
        }
        else
        {
            return true;
        }
    }

    protected void bnSubmit_Click(object sender, EventArgs e)
    {
        System.Web.Helpers.AntiForgery.Validate();
        lblerror.Text = String.Empty;
        lblerror.Visible = false;
        placeHolderMessage.Visible = false;

        if (ValidateForm())
        {
            SendEmail();
            Response.Redirect("/portal/thankyou");
        }
        else
        {
            placeHolderMessage.Visible = true;
            lblerror.Visible = true;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("/portal/");
    }
}