﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Web.UserControls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

public partial class CheckOut : System.Web.UI.Page
{
    protected UrlHelper Url;
    UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Page.Title = Resources.ShoppingCart.CheckOut_Title;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Url = new UrlHelper(Request.RequestContext);
        if (!IsPostBack)
        {
            BindGrid();
        }
    }

    protected int GetCartId()
    {
        int cartId = 0;
        if (ViewState["CartId"] != null)
        {
            cartId = int.Parse(ViewState["CartId"].ToString());
        }

        return cartId;
    }

    private void BindGrid()
    {
        List<RetrieveShoppingCartByUserIdResult> lsItems = ShoppingCartService.GetShoppingCartByUserId(_userContext.User.UserName).Where(i => (i.Customizable != null && i.Customizable.Value) && !i.ElectronicDelivery).ToList();
        rptItems.DataSource = lsItems;
        rptItems.DataBind();

        if (lsItems.Count > 0)
        {
            int cartId = lsItems[0].CartID;
            List<RetrieveCustomizationsResult> ls = ShoppingCartService.GetCustomizationOptions(_userContext.User.UserName, cartId, true);

            rptCustomizations.DataSource = ls;
            rptCustomizations.DataBind();

            ViewState["CartId"] = cartId;
        }
    }

    protected TextBoxMode GetTextBoxMode(string FiledType)
    {
        TextBoxMode treturn = TextBoxMode.SingleLine;
        switch (FiledType.ToLower())
        {
            case "multi line text":
                treturn = TextBoxMode.MultiLine;
                break;
            default:
                break;
        }

        return treturn;
    }

    protected void lnkubmit_Click(object sender, EventArgs e)
    {
        int ShoppingCartId = GetCartId();

        if (ValidateForm())
        {
            ShoppingCartService.ResetCustomizationData(ShoppingCartId);
            if (rptCustomizations.Items.Count > 0)
            {
                foreach (RepeaterItem r in rptCustomizations.Items)
                {
                    Label lblCustomizationOptionItemID = (Label)r.FindControl("CustomizationOptionItemID");

                    string value;
                    TextBox txtOptionsValue = (TextBox)r.FindControl("txtOptionsValue");
                    if (txtOptionsValue.Visible != false)
                    {
                        value = txtOptionsValue.Text.Trim();
                    }
                    else
                    {
                        ImageUpload img = (ImageUpload)r.FindControl("imgUpload");
                        value = img.Value.ToString();
                    }

                    ShoppingCartService.SaveCustomizationData(ShoppingCartId, lblCustomizationOptionItemID.Text, value);
                }
            }

            List<RetrieveShoppingCartByUserIdResult> lsCartItems = ShoppingCartService.GetShoppingCartByUserId(_userContext.User.UserName).Where(x => x.Quantity.Value > 0).ToList();
            if (lsCartItems.Any(i => !i.ElectronicDelivery && i.VDP != null && !i.VDP.Value))
            {
                Response.Redirect("/portal/shippinginformation");
            }
            else
            {
                Response.Redirect("/portal/revieworder");
            }
        }
    }

    private bool ValidateForm()
    {
        string Error = string.Empty; ;
        if (rptCustomizations.Items.Count > 0)
        {
            foreach (RepeaterItem r in rptCustomizations.Items)
            {
                string FriendlyName = string.Empty;
                bool Required = false;
                Label lblFieldType = (Label)r.FindControl("FieldType");
                Label lblFriendlyName = (Label)r.FindControl("FriendlyName");
                Label lblRequired = (Label)r.FindControl("Required");
                TextBox txtQuantity = (TextBox)r.FindControl("txtOptionsValue");
                ImageUpload imgUpload = (ImageUpload)r.FindControl("imgUpload");
                FriendlyName = lblFriendlyName.Text;
                Required = (lblRequired.Text.ToLower() == "true" ? true : false);
                if (Required)
                {
                    switch (lblFieldType.Text.ToLower())
                    {
                        case "date":
                            if (txtQuantity.Text.Trim().Length == 0)
                            {
                                if (!string.IsNullOrEmpty(Error)) Error = Error + "<br/>";
                                Error = Error + string.Format(Resources.Errors.FriendlyNameEmpty, FriendlyName);
                            }
                            else
                            {
                                try
                                {
                                    Convert.ToDateTime(txtQuantity.Text.Trim());
                                }
                                catch
                                {
                                    if (!string.IsNullOrEmpty(Error)) Error = Error + "<br/>";
                                    Error = Error + string.Format(Resources.Errors.FriendlyNameIncorrectFormat, FriendlyName);
                                }
                            }
                            break;

                        case "image":
                            if (imgUpload != null && string.IsNullOrEmpty(imgUpload.Value))
                            {
                                if (!string.IsNullOrEmpty(Error)) Error = Error + "<br/>";
                                Error = Error + string.Format(Resources.Errors.FriendlyNameSelectEmpty, FriendlyName);
                            }
                            break;

                        default:
                            if (txtQuantity.Text.Trim().Length == 0)
                            {
                                if (!string.IsNullOrEmpty(Error)) Error = Error + "<br/>";
                                Error = Error + string.Format(Resources.Errors.FriendlyNameEmpty, FriendlyName);
                            }
                            break;
                    }
                }
            }

            if (Error.Trim().Length > 0)
            {
                lblError.Text = Error;
                placeHolderMessage.Visible = true;
                return false;
            }
        }

        placeHolderMessage.Visible = false;
        return true;
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("/portal/shoppingcart");
    }
}