﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Data;
using System;
using System.IO;
using System.Linq;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.UI;

public partial class ElectronicDelivery : System.Web.UI.Page
{
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        if (_userContext.User == null)
        {
            Page.MasterPageFile = "~/WebFormsPages/MMP.minimal.master";
        }
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Page.Title = Resources.ShoppingCart.ElectronicDelivery;
    }

    UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
    SlamContext _slamContext { get { return DependencyResolver.Current.GetService<SlamContext>(); } }
    AssetRepository _assetRepository { get { return DependencyResolver.Current.GetService<AssetRepository>(); } }
    ISettingsService _settingsService { get { return DependencyResolver.Current.GetService<ISettingsService>(); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.RouteData.Values["key"] != null)
        {
            var download = _assetRepository.GetElectronicDelivery(Page.RouteData.Values["key"].ToString());
            if (download != null)
            {
                bool getDownloadFromPath = false;
                string filePath = Path.GetFullPath(Path.Combine(Server.MapPath(WebConfigurationManager.AppSettings["ElectronicDelivery.FileDirectory"]), download.FileLocation.TrimStart('/').TrimStart('\\')));

                if (!File.Exists(filePath))
                {
                    filePath = Path.GetFullPath(Path.Combine(Server.MapPath(_settingsService.GetFilesFolder()), download.FileLocation.TrimStart('/').TrimStart('\\')));
                    getDownloadFromPath = true;
                }

                if (File.Exists(filePath))
                {
                    if (download.ExpirationDate > DateTime.Now)
                    {
                        if (download.DownloadCount > 0)
                        {
                            FileInfo file = new FileInfo(filePath);
                            string filename = download.OrderItem == null ? file.Name : download.OrderItem.SKU + file.Extension;
                            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", filename));
                            Response.TransmitFile(filePath);

                            if (getDownloadFromPath)
                            {
                                var downloadItem = _slamContext.CreateQuery()
                                                               .FilterContentTypes<Mastercard.MarketingCenter.Data.DTOs.DownloadableAssetDTO>()
                                                               .Filter(string.Format("DownloadableAsset.Filename like '%{0}%'", download.FileLocation))
                                                               .Get<Mastercard.MarketingCenter.Data.DTOs.DownloadableAssetDTO>()
                                                               .Take(1)
                                                               .FirstOrDefault();
                                if (downloadItem != null)
                                {
                                    Request.RequestContext.RouteData.Values.Add("contentItemId", downloadItem.PrimaryContentItemId);
                                }
                            }
                        }
                        else
                        {
                            litMessage.Text = Resources.Errors.DownloadLimitExceeded;
                        }

                        download.DownloadCount -= 1;
                        _assetRepository.Commit();
                    }
                    else
                    {
                        litMessage.Text = Resources.Errors.DownloadExpired;
                    }
                }
                else
                {
                    litMessage.Text = Resources.Errors.DownloadNotFound;
                    Response.StatusCode = 404;
                }
            }
            else
            {
                litMessage.Text = Resources.Errors.DownloadNotFound;
                Response.StatusCode = 404;
            }

            if (_userContext.SelectedRegion.Equals("de", StringComparison.InvariantCultureIgnoreCase) || _userContext.User == null)
            {
                litElectronicDeliveryContact.Visible = false;
            }
            else
            {
                litElectronicDeliveryContact.Visible = true;
                litElectronicDeliveryContact.Text = string.Format(Resources.ShoppingCart.ElectronicDeliveryContact, RegionalizeService.RegionalizeSetting("ElectronicDeliveryContact.ContactUsAddress", _userContext.User?.Region?.Trim()?.ToLower() ?? RegionalizeService.DefaultRegion?.Trim()?.ToLower()));
            }
        }
    }
}