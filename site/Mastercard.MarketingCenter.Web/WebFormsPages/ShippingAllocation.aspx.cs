﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

public partial class ShippingAllocation : System.Web.UI.Page
{
    decimal total = 0;
    Dictionary<string, AssetPrice> assetPrices = new Dictionary<string, AssetPrice>();

    UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Page.Title = Resources.ShoppingCart.ShoppingCart_Title;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindOrderGrid();
            LoadShippingDetailsFromShoppingCart(GetCartId());
        }
    }

    protected int GetCartId()
    {
        int cartId = 0;
        if (ViewState["CartId"] != null)
        {
            cartId = int.Parse(ViewState["CartId"].ToString());
        }
        else
        {
            cartId = ShoppingCartService.GetActiveShoppingCart(_userContext.User.UserName).Value;
        }

        return cartId;
    }

    private void BindOrderGrid()
    {
        total = 0;
        List<RetrieveShoppingCartByUserIdResult> lsCartItems = ShoppingCartService.GetShoppingCartByUserId(_userContext.User.UserName);
        if (lsCartItems.Count > 0)
        {
            grdShoppingCart.DataSource = lsCartItems;
            grdShoppingCart.DataBind();

            if (!string.IsNullOrEmpty(lsCartItems[0].PromotionCode))
            {
                PromotionCodeResponse promotionResponse = PromotionService.ApplyPromotionCode(lsCartItems[0].PromotionCode, lsCartItems[0].CartID, _userContext.User.IssuerId);
                if (string.IsNullOrEmpty(promotionResponse.ErrorMessage))
                {
                    litPromotionDescription.Text = promotionResponse.Description;
                    litPromotionAmount.Text = string.Format("$-{0}", promotionResponse.DiscountAmount < total ? promotionResponse.DiscountAmount.ToString("N2") : total.ToString("N2"));
                    pnlPromotion.Visible = true;

                    lblTotal.Text = FormatNumber(promotionResponse.DiscountAmount < total ? total - promotionResponse.DiscountAmount : 0);
                }
            }
        }
        else
            Response.Redirect("/portal");
    }

    protected void LoadShippingDetailsFromShoppingCart(int CartID)
    {
        using (MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext())
        {
            var ShippingDetail = dataContext.RetrieveShippingInformation(CartID, null).ToList();

            if (ShippingDetail.Count > 0)
            {
                rptShippingAddresses.DataSource = ShippingDetail;
                rptShippingAddresses.DataBind();
            }
        }
    }

    protected void rptShippingAddresses_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item != null && (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            using (MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext())
            {
                RetrieveShippingInformationResult shippingInfo = e.Item.DataItem as RetrieveShippingInformationResult;

                HiddenField hdnShippingInformationId = e.Item.FindControl("hdnShippingInformationId") as HiddenField;
                hdnShippingInformationId.Value = shippingInfo.ShippingInformationID.ToString();

                Repeater rptInnerCartItems = e.Item.FindControl("rptInnerCartItems") as Repeater;
                rptInnerCartItems.ItemDataBound += new RepeaterItemEventHandler(rptInnerCartItems_ItemDataBound);

                var cartItems = dataContext.RetrieveShoppingCartItemsForShippingInformation(shippingInfo.ShippingInformationID).ToList();
                if (cartItems.Count > 0)
                {
                    rptInnerCartItems.DataSource = cartItems;
                    rptInnerCartItems.DataBind();
                }
            }
        }
    }

    protected void rptInnerCartItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item != null && (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            RetrieveShoppingCartItemsForShippingInformationResult cartItem = e.Item.DataItem as RetrieveShoppingCartItemsForShippingInformationResult;
            HiddenField hdnContentItemID = e.Item.FindControl("hdnContentItemID") as HiddenField;

            hdnContentItemID.Value = cartItem.ContentItemID;
        }
    }

    protected void txtQuantity_TextChanged(object sender, EventArgs e)
    {
        TextBox txtQuantity = (TextBox)sender;

        int ShoppingCartId = GetCartId();
        int quantity = 0;
        if (ShoppingCartId > 0 && Int32.TryParse(txtQuantity.Text.Trim(), out quantity))
        {
            RepeaterItem currentItem = (RepeaterItem)(txtQuantity.NamingContainer);
            HiddenField hdnContentItemID = currentItem.FindControl("hdnContentItemID") as HiddenField;
            HiddenField hdnShippingInformationId = currentItem.NamingContainer.NamingContainer.FindControl("hdnShippingInformationId") as HiddenField;
            ShoppingCartService.SaveShoppingCartItem(ShoppingCartId, hdnContentItemID.Value, Int32.Parse(hdnShippingInformationId.Value), quantity, false);
            BindOrderGrid();
        }
    }

    protected string FormatNumber(decimal num)
    {
        System.Globalization.NumberFormatInfo nf = new System.Globalization.NumberFormatInfo();
        nf.CurrencySymbol = "$";
        nf.NumberDecimalDigits = 2;
        return num.ToString("C", nf);
    }

    protected void bnSubmit_Click(object sender, EventArgs e)
    {
        Response.Redirect("/portal/revieworder");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("/portal/shippinginformation");
    }

    protected string GetPrice(string GlobalID, string Quantity, bool electronicDelivery)
    {
        string returnPrice = string.Empty;
        if (electronicDelivery)
        {
            AssetPrice downloadPrice = ShoppingCartService.GetAssetPrice(GlobalID, Convert.ToInt32(Quantity), true);
            returnPrice = FormatNumber(downloadPrice.TotalPrice);
            total = total + downloadPrice.TotalPrice;
            lblTotal.Text = FormatNumber(total);
        }
        else
        {
            if (Quantity.Length > 0)
            {
                decimal intQuantity = decimal.Parse(Quantity);
                if (!assetPrices.ContainsKey(GlobalID))
                    assetPrices.Add(GlobalID, ShoppingCartService.GetAssetPrice(GlobalID, decimal.ToInt32(intQuantity), false));

                decimal price = assetPrices[GlobalID].TotalPrice;
                total = total + price;
                returnPrice = FormatNumber(price);
                lblTotal.Text = FormatNumber(total);
            }
        }
        return returnPrice;
    }

    protected string GetItemPrice(string GlobalID, string Quantity, bool electronicDelivery)
    {
        string returnPrice = string.Empty;
        if (electronicDelivery)
            returnPrice = FormatNumber(ShoppingCartService.GetAssetPrice(GlobalID, Convert.ToInt32(Quantity), true).TotalPrice);
        else
        {
            if (Quantity.Length > 0)
            {
                decimal intQuantity = decimal.Parse(Quantity);
                if (!assetPrices.ContainsKey(GlobalID))
                    assetPrices.Add(GlobalID, ShoppingCartService.GetAssetPrice(GlobalID, decimal.ToInt32(intQuantity), false));

                if (assetPrices[GlobalID].ItemPrice > 0)
                {
                    decimal price = assetPrices[GlobalID].ItemPrice;
                    returnPrice = FormatNumber(price);
                }
                else
                    returnPrice = Resources.Forms.NotApply;
            }
        }
        return returnPrice;
    }
}