﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.WebControls;

public partial class ShippingInformation_Page : System.Web.UI.Page
{
    protected int extendedAddressCount = 0;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Page.Title = Resources.ShoppingCart.ShippingInformation_Title;
    }

    UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
    public IUserService _userService { get { return DependencyResolver.Current.GetService<IUserService>(); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["TimeStamp"] = DateTime.Now.Ticks.ToString();
            LoadShippingDetails();

            rptMoreAddresses.Visible = chkMultipleAddresses.Checked;
        }
    }

    protected void rptMoreAddresses_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item != null && (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
        {
            RetrieveShippingInformationResult currentItem = e.Item.DataItem as RetrieveShippingInformationResult;
            LinkButton lnkDeleteAddress = e.Item.FindControl("lnkDeleteAddress") as LinkButton;
            TextBox txtRepeaterAddress = e.Item.FindControl("txtAddress") as TextBox;
            TextBox txtRepeaterContactName = e.Item.FindControl("txtContactName") as TextBox;
            TextBox txtRepeaterPhone = e.Item.FindControl("txtPhone") as TextBox;
            HiddenField hdnShippingInformationId = e.Item.FindControl("hdnShippingInformationId") as HiddenField;

            lnkDeleteAddress.CommandArgument = extendedAddressCount.ToString();
            extendedAddressCount++;

            txtRepeaterAddress.Text = currentItem.Address;
            txtRepeaterContactName.Text = currentItem.ContactName;
            txtRepeaterPhone.Text = currentItem.Phone;
            hdnShippingInformationId.Value = currentItem.ShippingInformationID.ToString();
        }
    }

    protected void rptMoreAddresses_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        List<RetrieveShippingInformationResult> addresses = new List<RetrieveShippingInformationResult>();
        if (Session["ExtraAddresses" + ViewState["TimeStamp"].ToString()] != null)
        {
            addresses = (List<RetrieveShippingInformationResult>)Session["ExtraAddresses" + ViewState["TimeStamp"].ToString()];
        }

        //Order here should match that in addresses since addresses is bound to the
        //Repeater
        int currentItem = 0;
        foreach (RepeaterItem item in rptMoreAddresses.Items)
        {
            TextBox txtRepeaterAddress = item.FindControl("txtAddress") as TextBox;
            TextBox txtRepeaterContactName = item.FindControl("txtContactName") as TextBox;
            TextBox txtRepeaterPhone = item.FindControl("txtPhone") as TextBox;
            HiddenField hdnShippingInformationId = item.FindControl("hdnShippingInformationId") as HiddenField;

            if (hdnShippingInformationId.Value == "0")
            {
                addresses[currentItem].Address = txtRepeaterAddress.Text;
                addresses[currentItem].ContactName = txtRepeaterContactName.Text;
                addresses[currentItem].Phone = txtRepeaterPhone.Text;
            }

            currentItem++;
        }

        if (e.CommandName.ToUpper() == "DELETE")
        {
            if (e.CommandArgument != null)
            {
                int index = int.Parse(e.CommandArgument.ToString());

                if (Session["DeletedAddress" + ViewState["TimeStamp"].ToString()] == null)
                {
                    Session["DeletedAddress" + ViewState["TimeStamp"].ToString()] = addresses[index].ShippingInformationID;
                }
                else
                {
                    Session["DeletedAddress" + ViewState["TimeStamp"].ToString()] += "," + addresses[index].ShippingInformationID;
                }

                addresses.RemoveAt(index);
                Session["ExtraAddresses" + ViewState["TimeStamp"].ToString()] = addresses;

                rptMoreAddresses.DataSource = addresses;
                rptMoreAddresses.DataBind();
            }
        }
        else if (e.CommandName.ToUpper() == "ADD")
        {
            RetrieveShippingInformationResult newShippingAddress = new RetrieveShippingInformationResult();
            newShippingAddress.ShippingInformationID = 0;

            addresses.Add(newShippingAddress);
            Session["ExtraAddresses" + ViewState["TimeStamp"].ToString()] = addresses;

            rptMoreAddresses.DataSource = addresses;
            rptMoreAddresses.DataBind();
        }
    }

    protected int GetCartId()
    {
        int cartId = 0;
        if (ViewState["CartId"] != null)
        {
            cartId = Int32.Parse(ViewState["CartId"].ToString());
        }
        else
        {
            cartId = ShoppingCartService.GetActiveShoppingCart(_userContext.User.UserName).Value;
        }

        return cartId;
    }

    protected void LoadShippingDetails()
    {
        if (!LoadShippingDetailsFromShoppingCart())
        {
            if (!LoadShippingDetailsFromOrders())
            {
                LoadShippingDetailsFromProfile();
            }
        }

        int ShoppingCartId = GetCartId();
        if (ShoppingCartId > 0)
        {
            ShoppingCart cart = ShippingServices.GetShoppingCartOptions(ShoppingCartId);
            txtSpecialInstructions.Text = cart.ShippingInstructions;
            chkRushOrder.Checked = cart.RushOrder.HasValue ? cart.RushOrder.Value : false;
        }
    }
    protected bool LoadShippingDetailsFromShoppingCart()
    {
        bool shippingLoaded = false;
        using (MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext())
        {
            int ShoppingCartId = GetCartId();
            if (ShoppingCartId > 0)
            {
                List<RetrieveShippingInformationResult> ShippingDetail = dataContext.RetrieveShippingInformation(ShoppingCartId, null).ToList();

                if (ShippingDetail.Count > 0)
                {
                    RetrieveShippingInformationResult defaultShipping = ShippingDetail.First();
                    txtContactName.Text = defaultShipping.ContactName;
                    txtAddress.Text = defaultShipping.Address;
                    txtPhone.Text = defaultShipping.Phone;
                    hdnDefaultShippingId.Value = defaultShipping.ShippingInformationID.ToString();

                    if (ShippingDetail.Count > 1)
                    {
                        Session["ExtraAddresses" + ViewState["TimeStamp"].ToString()] = ShippingDetail.Skip(1).ToList();

                        rptMoreAddresses.DataSource = Session["ExtraAddresses" + ViewState["TimeStamp"].ToString()];
                        rptMoreAddresses.DataBind();

                        chkMultipleAddresses.Checked = true;
                    }

                    shippingLoaded = true;
                }
            }
        }

        return shippingLoaded;
    }

    protected bool LoadShippingDetailsFromOrders()
    {
        using (MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext())
        {
            RetrieveUserLatestShippingInformationResult shippingDetail = dataContext.RetrieveUserLatestShippingInformation(_userContext.User.UserName).FirstOrDefault();
            if (shippingDetail != null)
            {
                txtContactName.Text = shippingDetail.ContactName;
                txtAddress.Text = shippingDetail.Address;
                txtPhone.Text = shippingDetail.Phone;
                return true;
            }
        }

        return false;
    }

    protected void LoadShippingDetailsFromProfile()
    {
        txtContactName.Text = _userContext.User.FullName;
        txtAddress.Text = string.Format("{1}{0}{2}{0}{3}{0}{4}, {5} {6}", Environment.NewLine, _userContext.User.Profile.Address1, _userContext.User.Profile.Address2, _userContext.User.Profile.Address3, _userContext.User.Profile.City, _userContext.User.Profile.State, _userContext.User.Profile.Zip);
        txtPhone.Text = _userContext.User.Profile.Phone;
    }

    private bool ValidateForm()
    {
        var errors = new List<string>();

        if (txtContactName.Text.Trim().Length == 0)
        {
            errors.Add(Resources.Errors.ContactNameEmpty);
        }

        if (txtAddress.Text.Trim().Length == 0)
        {
            errors.Add(Resources.Errors.AddressEmpty);
        }

        if (txtPhone.Text.Trim().Length == 0)
        {
            errors.Add(Resources.Errors.PhoneEmpty);
        }

        if (txtSpecialInstructions.Text.Trim().Length > 950)
        {
            errors.Add(Resources.Errors.SpecialInstructionsLenght);
        }

        if (rptMoreAddresses.Visible)
        {
            foreach (RepeaterItem item in rptMoreAddresses.Items)
            {
                TextBox txtRepeaterAddress = item.FindControl("txtAddress") as TextBox;
                TextBox txtRepeaterContactName = item.FindControl("txtContactName") as TextBox;
                TextBox txtRepeaterPhone = item.FindControl("txtPhone") as TextBox;

                if (txtRepeaterContactName.Text.Trim().Length == 0)
                {
                    errors.Add(Resources.Errors.ContactNameEmpty);
                }

                if (txtRepeaterAddress.Text.Trim().Length == 0)
                {
                    errors.Add(Resources.Errors.AddressEmpty);
                }

                if (txtRepeaterPhone.Text.Trim().Length == 0)
                {
                    errors.Add(Resources.Errors.PhoneEmpty);
                }
            }
        }

        if (errors.Any())
        {
            errors.ForEach(error => lblerror.Text += error + "<br/>");
            return false;
        }
        else
        {
            return true;
        }
    }

    protected void bnSubmit_Click(object sender, EventArgs e)
    {
        int ShoppingCartId = GetCartId();
        if (ShoppingCartId > 0)
        {
            if (ValidateForm())
            {
                int defaultShippingId = 0;
                Int32.TryParse(hdnDefaultShippingId.Value, out defaultShippingId);

                bool setDefaults = defaultShippingId == 0;

                defaultShippingId = ShippingServices.SetShippingDetails(ShoppingCartId, defaultShippingId, txtContactName.Text.Trim(), txtPhone.Text.Trim(), txtAddress.Text.Trim());
                List<RetrieveShoppingCartByUserIdResult> lsCartItems = ShoppingCartService.GetShoppingCartByUserId(_userContext.User.UserName);
                if (setDefaults)
                {
                    foreach (RetrieveShoppingCartByUserIdResult cartItem in lsCartItems)
                    {
                        if (!cartItem.VDP.Value && !cartItem.ElectronicDelivery)
                        {
                            ShoppingCartService.SaveShoppingCartItem(ShoppingCartId, cartItem.ContentItemID, defaultShippingId, cartItem.Quantity.Value, false);
                        }
                    }
                }

                if (rptMoreAddresses.Visible)
                {
                    foreach (RepeaterItem item in rptMoreAddresses.Items)
                    {
                        TextBox txtRepeaterAddress = item.FindControl("txtAddress") as TextBox;
                        TextBox txtRepeaterContactName = item.FindControl("txtContactName") as TextBox;
                        TextBox txtRepeaterPhone = item.FindControl("txtPhone") as TextBox;
                        HiddenField hdnShippingInformationId = item.FindControl("hdnShippingInformationId") as HiddenField;

                        int shippingId = 0;
                        Int32.TryParse(hdnShippingInformationId.Value, out shippingId);

                        bool addNew = shippingId == 0;
                        shippingId = ShippingServices.SetShippingDetails(ShoppingCartId, shippingId, txtRepeaterContactName.Text.Trim(), txtRepeaterPhone.Text.Trim(), txtRepeaterAddress.Text.Trim());
                        if (addNew)
                        {
                            foreach (RetrieveShoppingCartByUserIdResult cartItem in lsCartItems)
                            {
                                if (!cartItem.VDP.Value && !cartItem.ElectronicDelivery)
                                {
                                    ShoppingCartService.SaveShoppingCartItem(ShoppingCartId, cartItem.ContentItemID, shippingId, 0, true);
                                }
                            }
                        }
                    }

                    if (Session["DeletedAddress" + ViewState["TimeStamp"].ToString()] != null)
                    {
                        string[] deleteIds = Session["DeletedAddress" + ViewState["TimeStamp"].ToString()].ToString().Split(',');
                        foreach (string deleteId in deleteIds)
                        {
                            if (deleteId != "0")
                            {
                                ShippingServices.DeleteShippingInformation(ShoppingCartId, Int32.Parse(deleteId));
                            }
                        }
                    }
                }
                else
                {
                    List<RetrieveShippingInformationResult> addresses = new List<RetrieveShippingInformationResult>();
                    if (Session["ExtraAddresses" + ViewState["TimeStamp"].ToString()] != null)
                    {
                        addresses = (List<RetrieveShippingInformationResult>)Session["ExtraAddresses" + ViewState["TimeStamp"].ToString()];
                    }

                    foreach (RetrieveShippingInformationResult address in addresses)
                    {
                        if (address.ShippingInformationID > 0)
                        {
                            ShippingServices.DeleteShippingInformation(ShoppingCartId, address.ShippingInformationID);
                        }
                    }
                }

                ShippingServices.UpdateShoppingCart(ShoppingCartId, txtSpecialInstructions.Text.Trim(), chkRushOrder.Checked);
                if (chkMultipleAddresses.Checked && rptMoreAddresses.Items.Count > 0)
                {
                    Response.Redirect("/portal/shippingallocation");
                }
                else
                {
                    Response.Redirect("/portal/revieworder");
                }
            }
            else
            {
                lblerror.Visible = true;
                panelError.Visible = true;
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        int ShoppingCartId = GetCartId();
        if (ShoppingCartId > 0)
        {
            List<RetrieveCustomizationsResult> ls = ShoppingCartService.GetCustomizationOptions(_userContext.User.UserName, ShoppingCartId, false);
            if (ls.Count > 0)
            {
                Response.Redirect("/portal/checkout");
            }
            else
            {
                Response.Redirect("/portal/shoppingcart");
            }
        }
    }

    protected void chkMultipleAddresses_CheckedChanged(object sender, EventArgs e)
    {
        if (chkMultipleAddresses.Checked && Session["ExtraAddresses" + ViewState["TimeStamp"].ToString()] == null)
        {
            RetrieveShippingInformationResult newShippingAddress = new RetrieveShippingInformationResult();
            newShippingAddress.ShippingInformationID = 0;
            List<RetrieveShippingInformationResult> addresses = new List<RetrieveShippingInformationResult>();
            addresses.Add(newShippingAddress);
            Session["ExtraAddresses" + ViewState["TimeStamp"].ToString()] = addresses;

            rptMoreAddresses.Visible = true;
            rptMoreAddresses.DataSource = addresses;
            rptMoreAddresses.DataBind();
        }
        else
        {
            rptMoreAddresses.Visible = chkMultipleAddresses.Checked;
        }
    }
}