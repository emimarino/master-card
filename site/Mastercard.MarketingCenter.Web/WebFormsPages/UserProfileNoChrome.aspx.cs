﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Web.Core.Extensions;
using System;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.WebFormsPages
{
    public partial class UserProfileNoChrome : System.Web.UI.Page
    {
        UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            RegionalizeService.SetCurrentCulture(_userContext.SelectedRegion, RegionalizeService.DefaultLanguage);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            Page.Title = Resources.Forms.UserProfile_Title;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.DisableCache();
        }
    }
}