﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Web.Mvc;
using System.Web.UI;

public partial class RejectOrder : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Page.Title = Resources.ShoppingCart.RejectOrder_Title;
    }

    UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
    public IUserService _userService { get { return DependencyResolver.Current.GetService<IUserService>(); } }
    IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }
    IEmailService _emailService { get { return DependencyResolver.Current.GetService<IEmailService>(); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            rejectionForm.Visible = false;
            submissionMessage.Visible = false;
            invalidMessage.Visible = true;

            if (Page.RouteData.Values["orderid"] != null)
            {
                string orderString = string.Empty;
                if (Page.RouteData.Values["orderid"] != null)
                {
                    orderString = _orderService.GetOrderIDByOrderNo(Page.RouteData.Values["orderid"].ToString()).ToString();
                }

                if (!string.IsNullOrEmpty(orderString))
                {
                    int orderId = Convert.ToInt32(orderString);
                    string nextStatus = _orderService.GetOrderDetails(orderId).OrderStatus;
                    if (nextStatus == "Pending MasterCard Approval")
                    {
                        rejectionForm.Visible = true;
                        submissionMessage.Visible = false;
                        invalidMessage.Visible = false;
                    }
                }

                lblOrderNo.Text = Resources.ShoppingCart.Order + " # " + Page.RouteData.Values["orderid"].ToString();
                lblOrderNo2.Text = Page.RouteData.Values["orderid"].ToString();
                lblOrderNo3.Text = Page.RouteData.Values["orderid"].ToString();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string orderString = string.Empty;
        if (Page.RouteData.Values["orderid"] != null)
        {
            orderString = _orderService.GetOrderIDByOrderNo(Page.RouteData.Values["orderid"].ToString()).ToString();
        }

        if (!string.IsNullOrEmpty(orderString))
        {
            int orderId = Convert.ToInt32(orderString);
            RetrieveOrderDetailResult orderDetail = _orderService.GetOrderDetails(orderId);
            if (orderDetail.OrderStatus == "Pending MasterCard Approval")
            {
                _orderService.SetOrderStatus(orderId, "Cancelled", $"Order Cancelled: {txtRejectionReason.Text}", System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName()).GetValue(0).ToString(), _userContext.User.UserName);

                var user = _userService.GetUserByUserName(orderDetail.UserId);
                var userId = DependencyResolver.Current.GetService<UserContext>().User?.UserId ?? 0;
                _emailService.SendOrderRejectedEmail(Page.RouteData.Values["orderid"].ToString(), txtRejectionReason.Text, user.Email, user.Profile.FirstName, user.Profile.LastName, userId);
                rejectionForm.Visible = false;
                submissionMessage.Visible = true;
                invalidMessage.Visible = false;
            }
            else
            {
                rejectionForm.Visible = false;
                submissionMessage.Visible = false;
                invalidMessage.Visible = true;
            }
        }
        else
        {
            rejectionForm.Visible = false;
            submissionMessage.Visible = false;
            invalidMessage.Visible = true;
        }
    }
}