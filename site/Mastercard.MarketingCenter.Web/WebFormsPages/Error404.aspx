﻿<%@ Page Title="Error Page" Language="C#" MasterPageFile="~/WebFormsPages/MMP.master" AutoEventWireup="true" Inherits="Error404" CodeBehind="Error404.aspx.cs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="center_container">
        <div class="errorpage">
            <h1>
                <%=Resources.Errors.PageNotFound%></h1>
            <div class="info">
                <%=Resources.Errors.PageNotLocated%>
            </div>
        </div>
    </div>
</asp:Content>