﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebFormsPages/MMP.master" AutoEventWireup="true" Inherits="SendDataFile" CodeBehind="SendDataFile.aspx.cs" %>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="center_container">
        <div class="form_container_sendfile" style="float: none;">
            <div class="orange_head mrt"><%=Resources.ShoppingCart.SendDataFile_Title%></div>
            <div>
                <p class="under_construction none_left">
                    <%=Resources.ShoppingCart.SendDataFile_Description%>
                </p>
            </div>
        </div>

        <div class="faq_right_container_wide" style="padding-top: 20px; float: none;">
            <div class="right_tab_top_wide">
                <div class="orange_heading_main"><%=Resources.ShoppingCart.SendDataFile_FtpAccountInformation%></div>
            </div>
            <div class="right_tab_bg_wide">
                <asp:Panel ID="pnlFtpPending" runat="server">
                    <%=Resources.ShoppingCart.SendDataFile_FtpAccountInformationSetUp%>
                </asp:Panel>
                <asp:Panel ID="pnlFtpAvailable" runat="server" Visible="false">
                    <%=Resources.ShoppingCart.SendDataFile_FtpAccountInformationAccess%>
                    <br />
                    <br />
                    <br />
                    <asp:Button ID="btnRetrieveFtpInfo" class="RetFtpInfo" runat="server" Text="<%$Resources:ShoppingCart,SendDataFile_RetrieveFtpLoginInformation%>" OnClick="btnRetrieveFtpInfo_Click" />
                </asp:Panel>
                <asp:Panel ID="pnlFtpInfo" runat="server" Visible="false">
                    <table width="290" class="ftp_info_table">
                        <tr>
                            <td width="75">
                                <div class="content_span"><strong><%=Resources.Forms.Server%>:</strong></div>
                            </td>
                            <td>
                                <div class="content_span">
                                    <asp:Literal ID="litServer" runat="server"></asp:Literal>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td width="75">
                                <div class="content_span"><strong><%=Resources.Forms.UserName%>:</strong></div>
                            </td>
                            <td>
                                <div class="content_span">
                                    <asp:Literal ID="litUserName" runat="server"></asp:Literal>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td width="75">
                                <div class="content_span"><strong><%=Resources.Forms.Password%>:</strong></div>
                            </td>
                            <td>
                                <div class="content_span">
                                    <asp:Literal ID="litPassword" runat="server"></asp:Literal>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <%=Resources.ShoppingCart.SendDataFile_Note%>
                </asp:Panel>
                <asp:Panel ID="pnlFtpRetrieved" runat="server" Visible="false">
                    <%=Resources.ShoppingCart.SendDataFile_SecurityNote%>
                </asp:Panel>
            </div>
            <div class="right_tab_bot_wide"></div>
        </div>
    </div>
</asp:Content>
