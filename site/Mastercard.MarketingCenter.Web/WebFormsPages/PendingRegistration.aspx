﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebFormsPages/MMP.master" AutoEventWireup="true" Inherits="PendingRegistration" CodeBehind="PendingRegistration.aspx.cs" %>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="/admin/Content/legacy/css/MarketingCenter.SharedControls.css" type="text/css" rel="stylesheet" />
    <link href="/admin/Content/css/plugins/chosen/chosen.css" type="text/css" rel="stylesheet" />
    <style>
        .submit_bg_btn {
            border-radius: 5px;
            float: left;
            margin: 20px 0 0 0;
            padding: 0 10px 10px 10px;
            width: auto;
            height: 20px !important;
            min-width: 60px;
            text-decoration: none;
            font: bold 11px/20px Arial, Helvetica, sans-serif !important;
            background: url("/portal/inc/images/btn_bg_right.gif") no-repeat scroll right top transparent !important;
            background-position: center;
            cursor: pointer;
        }

        .item label {
            font-family: "Accord Alternate", Arial, Helvetica, sans-serif;
            font-size: 18px;
            font-weight: inherit;
            margin-top: 0;
            margin-left: 0;
        }

        /* CHOSEN PLUGIN */
        .chosen-container-single .chosen-single {
            background: #ffffff;
            box-shadow: none;
            -moz-box-sizing: border-box;
            background-color: #FFFFFF;
            border: 1px solid darkgray;
            border-radius: 2px;
            cursor: text;
            height: auto !important;
            margin: 0;
            overflow: hidden;
            font-size: 11px;
            position: relative;
            width: 592px;
        }

        .chosen-container-multi .chosen-choices {
            min-height: unset !important;
            padding-left: 8px;
            width: 590px;
            border-color: darkgray;
        }

            .chosen-container-multi .chosen-choices li.search-choice {
                height: auto !important;
                min-height: unset !important;
                font-size: 11px;
                margin: 1px 5px 1px 0;
                padding: 1px 20px 1px 5px;
            }

                .chosen-container-multi .chosen-choices li.search-choice .search-choice-close {
                    top: 3px;
                }

            .chosen-container-multi .chosen-choices li.search-field input[type="text"] {
                margin: 0;
                padding: 0;
                height: auto !important;
                color: #444;
                font-size: 11px;
                font-family: Verdana, Arial, Helvetica, sans-serif;
                line-height: 20px;
            }
    </style>
    <script type="text/javascript" src="/admin/Content/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/admin/Content/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script type="text/javascript" src="/admin/Content/js/plugins/chosen/chosen.jquery.js"></script>

    <asp:HiddenField runat="server" ID="PreviousRegion" />
    <div class="clr" style="margin-top: 20px;"></div>
    <div style="overflow: hidden;">
        <div>
            <a href="~/" runat="server" id="lnkBackPendingRegistrationsReport">< Back to Pending Registrations</a>
            <p class="grey_head">PENDING REGISTRATION REVIEW</p>
        </div>
        <div style="margin-bottom: 20px;">
            <p>Please review the following registration details and either approve or reject it.</p>
            <br />
            <table>
                <tbody>
                    <tr>
                        <td style="width: 150px;">First Name:</td>
                        <td>
                            <asp:Literal ID="litFirstName" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Last Name:</td>
                        <td>
                            <asp:Literal ID="litLastName" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Title:</td>
                        <td>
                            <asp:Literal ID="litTitle" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Company Name:</td>
                        <td>
                            <asp:Literal ID="litFinancialInstitution" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Processor:</td>
                        <td>
                            <asp:Literal ID="litProcessor" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Phone:</td>
                        <td>
                            <asp:Literal ID="litPhone" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Fax:</td>
                        <td>
                            <asp:Literal ID="litFax" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Email Address:</td>
                        <td>
                            <asp:Literal ID="litEmail" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>Address:</td>
                        <td>
                            <asp:Literal ID="litAddress1" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Literal ID="litAddress2" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Literal ID="litAddress3" runat="server"></asp:Literal></td>
                    </tr>
                    <tr>
                        <td>City, State, ZIP:</td>
                        <td>
                            <asp:Literal ID="litCityStateZIP" runat="server"></asp:Literal></td>
                    </tr>
                    <% if (!string.IsNullOrEmpty(litCountry.Text))
                        { %>
                    <tr>
                        <td>Country:</td>
                        <td>
                            <asp:Literal ID="litCountry" runat="server"></asp:Literal></td>
                    </tr>
                    <% } %>
                    <% if (!string.IsNullOrEmpty(litRegion.Text))
                        { %>
                    <tr>
                        <td>Region:</td>
                        <td>
                            <asp:Literal ID="litRegion" runat="server"></asp:Literal></td>
                    </tr>
                    <% } %>
                    <% if (!string.IsNullOrEmpty(litLanguage.Text))
                        { %>
                    <tr>
                        <td>Language:</td>
                        <td>
                            <asp:Literal ID="litLanguage" runat="server"></asp:Literal></td>
                    </tr>
                    <% } %>
                    <% if (!string.IsNullOrEmpty(litSegmentation.Text))
                        { %>
                    <tr>
                        <td>Segmentation:</td>
                        <td>
                            <asp:Literal ID="litSegmentation" runat="server"></asp:Literal></td>
                    </tr>
                    <% } %>
                </tbody>
            </table>
        </div>
        <asp:PlaceHolder ID="phAlreadyRejectedOrAdded" runat="server" Visible="false">
            <p style="color: #ff6600;">
                <asp:Literal ID="litStatus" runat="server"></asp:Literal>
            </p>
            <asp:PlaceHolder ID="phApprovedRejected" runat="server" Visible="false">
                <p style="color: #ff6600;">
                    The registration has been succesfully
                    <asp:Literal ID="litApprovedRejected" runat="server"></asp:Literal>. A notification email has been sent to the registrant. To view the current list of Pending Registrations, please <a href="~/" runat="server" id="lnkPendingRegistrationsReport">click here</a>.
                </p>
            </asp:PlaceHolder>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phRejectOrAdd" runat="server">
            <div style="width: 300px; border-right: 1px solid #aaa; float: left;" class="clr">
                <p class="grey_head">REJECT</p>

                <p>REJECTION REASON/MESSAGE:</p>
                <asp:TextBox ID="txtRejectionReason" runat="server" TextMode="MultiLine" Width="250" Height="140" Font-Size="Small"></asp:TextBox>
                <asp:PlaceHolder ID="phErrorRejected" runat="server" Visible="false">
                    <div style="color: #f00; font-size: 14px;">
                        <asp:Literal ID="litErrorRejected" runat="server"></asp:Literal>
                    </div>
                </asp:PlaceHolder>
                <asp:LinkButton runat="server" ID="lnkSaveReject" CssClass="submit_bg_btn" Text="Reject" OnClick="lnkSaveReject_Click">Reject</asp:LinkButton>
            </div>
            <div style="float: left; margin-left: 20px; width: 600px; height: 600px;">
                <p class="grey_head">APPROVE</p>

                <div style="height: 90px;">
                    <asp:RadioButton ID="rbtnExisting" GroupName="group1" runat="server" /><span>ASSIGN NEW USER TO THIS EXISTING FINANCIAL INSTITUTION</span><br />
                    <div style="margin-left: 20px; margin-top: 5px; margin-bottom: 10px;">
                        <asp:DropDownList ID="ddlFinancialInstitutions" runat="server" CssClass="chosen-choices" TabIndex="-1" Width="600"></asp:DropDownList>
                    </div>
                    <asp:CheckBox ID="chkAddDomain" runat="server" Style="font-weight: normal; margin-left: 20px;" CssClass="not_bold" Font-Bold="false" Checked="true" />
                </div>
                <div>
                    <asp:RadioButton ID="rbtnNew" GroupName="group1" runat="server" /><span>CREATE NEW FINANCIAL INSTITUTION</span><br />
                    <table style="width: 100%; height: 50px; margin-left: 20px; margin-top: 5px;">
                        <tbody>
                            <tr>
                                <td style="width: 250px;">Name<span style="color: #f00;">*</span></td>
                                <td style="width: 250px;">Domain Name(s)<span style="color: #f00;">*</span></td>
                                <td style="width: 100px;">ICA</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtFIName" runat="server" Width="245"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtFIDomain" runat="server" Width="245"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="txtFIICA" runat="server" Width="90"></asp:TextBox></td>
                            </tr>
                            <% if (CanChangeSegments)
                                { %>
                            <tr>
                                <td style="width: 180px;">Segmentation<% if (!CanHaveEmptySegments) { %><span style="color: #f00;">*</span> <% } %></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:ListBox runat="server" ID="ddlSegmentations" SelectionMode="Multiple" Width="440" CssClass="chosen-choices" TabIndex="-1" multiple="multiple" data-placeholder="Select Segmentation..." />
                                </td>
                            </tr>
                            <% } %>
                            <tr>
                                <td style="width: 180px;">Processor<% if (!CanHaveEmptyProcessors) { %><span style="color: #f00;">*</span> <% } %></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:DropDownList runat="server" ID="ddlProcessors" Width="440" CssClass="chosen-choices" TabIndex="-1" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <asp:PlaceHolder ID="phError" runat="server" Visible="false">
                    <div style="color: #f00;">
                        <asp:Literal ID="litError" runat="server"></asp:Literal>
                    </div>
                </asp:PlaceHolder>
                <asp:LinkButton runat="server" ID="lnkSaveApprove" CssClass="submit_bg_btn" Text="Approve" OnClick="lnkSaveApprove_Click">Approve</asp:LinkButton>
            </div>
        </asp:PlaceHolder>
    </div>
    <div class="clr" style="margin-top: 20px;"></div>
    <script type="text/javascript" src="/admin/Content/js/inspinia.js"></script>
</asp:Content>
