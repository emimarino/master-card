﻿using AWS.Email;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.UI.WebControls;

public partial class OrderConfirmation : System.Web.UI.Page
{
    string _specialInstructions = string.Empty;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Page.Title = Resources.ShoppingCart.OrderConfirmation_Title;
    }

    public UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
    public IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }
    public ProcessorService _processorService { get { return DependencyResolver.Current.GetService<ProcessorService>(); } }
    public IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }

    private decimal _total;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.RouteData.Values["orderid"] != null)
            {
                int orderid = _orderService.GetOrderIDByOrderNo(Page.RouteData.Values["orderid"].ToString());

                if (orderid != 0)
                {
                    var order = _orderService.GetOrderById(orderid);
                    if (order.BillingID.HasValue)
                    {
                        litBillingICA.Text = order.BillingID.Value.ToString();
                    }
                    else
                    {
                        phBillingICA.Visible = false;
                    }

                    RetrieveOrderDetails(orderid);
                    RetrieveCustomisationDetails(orderid);
                    RetrieveOrderItems(orderid);
                    RetrieveEstimatedDistribution(orderid);

                    electronicFullfilment.OrderId = orderid;
                    vdpFullfilment.OrderId = orderid;
                    printFullfilment.OrderId = orderid;
                }
                else
                {
                    Response.Redirect("/portal/error/invalidorder");
                }

                CheckOrderItemsAdvisorsTag(orderid);
            }
        }
    }

    protected void CheckOrderItemsAdvisorsTag(int orderId)
    {
        var order = _orderService.GetOrderById(orderId);
        if (order.ConsentGiven == null)
        {
            var orderItems = _orderService.GetOrderItemsForOrder(orderId);
            if (orderItems.Any(oi => oi.HasAdvisorsTag))
            {
                phGiveConsent.Visible = true;
            }
        }
    }

    protected void grdShoppingCart_ItemDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            RetrieveOrderItemsResult orderItem = (RetrieveOrderItemsResult)e.Row.DataItem;
            Image imgIcon = (Image)e.Row.FindControl("imgIcon");
            if (orderItem.ElectronicDelivery)
            {
                imgIcon.ImageUrl = "~/inc/images/document_down.png";
            }
            else
            {
                if (orderItem.VDP)
                {
                    imgIcon.ImageUrl = "~/inc/images/vdp.png";
                }
                else
                {
                    imgIcon.ImageUrl = Convert.ToBoolean(orderItem.Customizations) ? "~/inc/images/document_edit.png" : "~/inc/images/document_lock.png";
                }
            }
        }
    }

    private bool ValidUser(string userId)
    {
        return !userId.IsNullOrWhiteSpace() &&
                userId.Equals(_userContext.User.UserName, StringComparison.InvariantCultureIgnoreCase) ||
                Page.User.IsInRole(Constants.Roles.McAdmin) ||
                Page.User.IsInRole(Constants.Roles.DevAdmin);
    }

    protected void RetrieveOrderItems(int orderId)
    {
        _total = 0;
        List<RetrieveOrderItemsResult> orderItems = _orderService.GetOrderItems(orderId);
        if (orderItems != null)
        {
            grdShoppingCart.DataSource = orderItems;
            grdShoppingCart.DataBind();

            RetrievePromotionForOrderResult promotion = _orderService.GetPromotion(orderId);
            if (promotion != null)
            {
                litPromotionDescription.Text = promotion.PromotionDescription;
                litPromotionAmount.Text = $"$-{Convert.ToDecimal(promotion.PromotionAmount).ToString("N2")}";
                pnlPromotion.Visible = true;

                lblTotal.Text = FormatNumber(_total - Convert.ToDecimal(promotion.PromotionAmount));
            }

            noteForShipping.Visible = orderItems.Any(i => !i.ElectronicDelivery && !i.Customizations.Value);
            pnlShippingNote.Visible = orderItems.Any(i => !i.ElectronicDelivery);
            noteForApprovalPOD.Visible = false;
            RetrieveOrderDetailResult orderDetail = _orderService.GetOrderDetails(orderId);
            if (orderDetail.OrderStatus.ToLower() == "pending mastercard approval" && orderItems.Any(i => !string.IsNullOrEmpty(i.ApproverEmailAddress)))
            {
                if (!orderItems.Any(i => i.ElectronicDelivery))
                {
                    noteForApprovalPOD.Visible = true;
                    noteForShipping.Visible = false;
                }
            }

            if (pnlShippingNote.Visible)
            {
                lblTotal.Text += "*";
            }
        }
    }

    protected void RetrieveEstimatedDistribution(int orderId)
    {
        var results = _orderService.GetOrderItemsForOrder(orderId).Where(oi => oi.ElectronicDelivery);
        if (results.Any())
        {
            grdEstimatedDistribution.DataSource = results;
            grdEstimatedDistribution.DataBind();
        }
        else
        {
            phEstimatedDistribution.Visible = false;
        }
    }

    protected void RetrieveCustomisationDetails(int orderid)
    {
        List<RetrieveCustomizationsResult> ls = _orderService.GetCustomizationOptions(orderid);
        rptCustomizations.DataSource = ls;
        rptCustomizations.DataBind();

        placeCustomizationOptions.Visible = ls.Count > 0;
        noteForCustomizations.Visible = placeCustomizationOptions.Visible;
    }

    protected void RetrieveOrderDetails(int orderId)
    {
        RetrieveOrderDetailResult orderItems = _orderService.GetOrderDetails(orderId);
        if (orderItems != null)
        {
            lblOrderNo.Text = Resources.ShoppingCart.Order + " #         " + orderItems.OrderNumber.ToString();
            lblOrderDate.Text = Resources.Shared.Date + "           " + orderItems.OrderDate.Value.ToString("MM/dd/yyyy");
            if (!ValidUser(orderItems.UserId.ToString()))
            {
                Response.Redirect("/portal/error/accesdenied");
            }
        }
    }

    protected string FormatNumber(decimal num)
    {
        System.Globalization.NumberFormatInfo nf = new System.Globalization.NumberFormatInfo
        {
            CurrencySymbol = "$",
            NumberDecimalDigits = 2
        };

        return num.ToString("C", nf);
    }

    protected string GetPrice(string price)
    {
        if (price.Length > 0)
        {
            decimal dprice = decimal.Parse(price);
            _total = _total + dprice;

            lblTotal.Text = FormatNumber(_total);
        }

        return price;
    }

    protected void ConfirmOrder_Click(object sender, EventArgs e)
    {
        Response.Redirect("/portal");
    }

    protected void btnGiveConsentYes_Click(object sender, EventArgs e)
    {
        UpdateConsentGiven(true);
    }

    protected void btnGiveConsentNo_Click(object sender, EventArgs e)
    {
        UpdateConsentGiven(false);
    }

    private void UpdateConsentGiven(bool consentGiven)
    {
        var orderId = _orderService.GetOrderIDByOrderNo(Page.RouteData.Values["orderid"].ToString());
        _orderService.UpdateOrderConsentGiven(orderId, consentGiven);
        if (consentGiven)
        {
            SendConfirmationMailCopyToAdvisorsTeam(orderId);
        }

        Response.Redirect(Request.RawUrl);
    }

    protected string RetrieveShippingInformation(int orderid)
    {
        string shippingInformation = string.Empty;
        _specialInstructions = string.Empty;
        var shippingDetail = _orderService.GetShippingInformation(orderid);
        var orderDetail = _orderService.GetOrderDetails(orderid);
        if (shippingDetail != null)
        {
            foreach (var s in shippingDetail)
            {
                var address = s.Address + Environment.NewLine;
                address += Environment.NewLine;
                address += $"{Resources.ShoppingCart.ItemsOrdered}:{Environment.NewLine}";

                var orderItems = _orderService.GetOrderItemsForShippingInformation(s.ShippingInformationID).ToList();
                if (orderItems.Any())
                {
                    foreach (var orderItem in orderItems)
                    {
                        address += $"{orderItem.SKU}, {orderItem.ItemName}, {orderItem.ItemQuantity}{Environment.NewLine}";
                    }
                }

                shippingInformation += Environment.NewLine + address;

                if (!string.IsNullOrEmpty(orderDetail.SpecialInstructions))
                {
                    _specialInstructions = orderDetail.SpecialInstructions;
                }
                else
                {
                    _specialInstructions = " ";
                }

                if (orderDetail.RushOrder)
                {
                    _specialInstructions += Environment.NewLine + Environment.NewLine + Resources.ShoppingCart.RushOrderRequested.ToUpperInvariant();
                }
            }
        }

        if (string.IsNullOrEmpty(shippingInformation))
        {
            shippingInformation = "     ";
        }

        return shippingInformation;
    }

    protected void SendConfirmationMailCopyToAdvisorsTeam(int orderId)
    {
        decimal totals = 0;
        StringBuilder sb = new StringBuilder();
        StringBuilder sbEd = new StringBuilder();
        string result;
        StringBuilder specialText = new StringBuilder();

        List<RetrieveOrderItemsResult> orderItems = _orderService.GetOrderItems(Convert.ToInt32(orderId));

        var shippingText = RetrieveShippingInformation(Convert.ToInt32(orderId));
        EmailText et;
        var listitems = new List<TableItem>();

        if (orderItems != null)
        {
            sb.Append(".................................................................");
            sb.Append(Environment.NewLine);

            TableItem t = new TableItem
            {
                Head = Resources.ShoppingCart.Sku.ToUpperInvariant(),
                Width = 12,
                Align = "Left"
            };

            listitems.Add(t);

            t = new TableItem
            {
                Head = Resources.ShoppingCart.Title,
                Width = 22,
                Align = "Left"
            };
            listitems.Add(t);

            t = new TableItem
            {
                Head = Resources.ShoppingCart.Quantity,
                Width = 17,
                Align = "Left"
            };
            listitems.Add(t);

            t = new TableItem
            {
                Head = Resources.ShoppingCart.Price,
                Width = 17,
                Align = "Left"
            };
            listitems.Add(t);

            et = new EmailText();
            result = et.GenerateText(listitems);

            sb.Append(result);
            sb.Append(".................................................................");
            sb.Append(Environment.NewLine);

            foreach (RetrieveOrderItemsResult r in orderItems)
            {
                listitems.Clear();

                t = new TableItem
                {
                    Head = r.SKU + " ",
                    Width = 12,
                    Align = "Left"
                };

                listitems.Add(t);

                t = new TableItem
                {
                    Head = r.ItemName + " ",
                    Width = 22,
                    Align = "Left"
                };
                listitems.Add(t);

                t = new TableItem
                {
                    Head = r.ItemQuantity + " ",
                    Width = 14,
                    Align = "Left"
                };
                listitems.Add(t);

                t = new TableItem { Head = FormatNumber(Convert.ToDecimal(r.ItemPrice.ToString())) };
                totals = totals + Convert.ToDecimal(r.ItemPrice.ToString());
                t.Width = 12;
                t.Align = "Right";
                listitems.Add(t);

                et = new EmailText();
                result = et.GenerateText(listitems);
                sb.Append(result);
            }

            RetrievePromotionForOrderResult promotion = _orderService.GetPromotion(orderId);
            if (promotion != null)
            {
                totals -= Convert.ToDecimal(promotion.PromotionAmount);

                t = new TableItem();
                listitems.Clear();

                t.Head = promotion.PromotionDescription;
                t.Width = 48;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem
                {
                    Head = $"$-{Convert.ToDecimal(promotion.PromotionAmount).ToString("N2")}",
                    Width = 14,
                    Align = "Right"
                };
                listitems.Add(t);

                et = new EmailText();
                result = et.GenerateText(listitems);
                sb.Append(result);
            }

            sb.Append(".................................................................");
            sb.Append(Environment.NewLine);

            t = new TableItem();
            listitems.Clear();

            t.Head = string.Empty;
            t.Width = 12;
            t.Align = "Left";
            listitems.Add(t);

            t = new TableItem
            {
                Head = string.Empty,
                Width = 22,
                Align = "Left"
            };
            listitems.Add(t);

            t = new TableItem
            {
                Head = "Total: ",
                Width = 14,
                Align = "Right"
            };
            listitems.Add(t);

            t = new TableItem();
            t.Head = FormatNumber(Convert.ToDecimal(totals)) + "*";
            t.Width = 13;
            t.Align = "Right";
            listitems.Add(t);

            et = new EmailText();
            result = et.GenerateText(listitems);
            sb.Append(result);
            sb.Append(".................................................................");
        }

        // Estimated Distribution Details
        if (orderItems != null)
        {
            foreach (var orderItem in orderItems)
            {
                if (orderItem.ElectronicDelivery)
                {
                    listitems.Clear();
                    sbEd.Append(Environment.NewLine);
                    sbEd.Append(Environment.NewLine);
                    sbEd.Append($"--- {Resources.ShoppingCart.EstimatedDistribution.ToUpperInvariant()} ---");
                    sbEd.Append(Environment.NewLine);
                    sbEd.Append(Environment.NewLine);
                    sbEd.Append(".................................................................");
                    sbEd.Append(Environment.NewLine);

                    TableItem t = new TableItem
                    {
                        Head = Resources.ShoppingCart.Sku.ToUpperInvariant(),
                        Width = 12,
                        Align = "Left"
                    };

                    listitems.Add(t);

                    t = new TableItem
                    {
                        Head = Resources.ShoppingCart.Title,
                        Width = 22,
                        Align = "Left"
                    };
                    listitems.Add(t);

                    t = new TableItem
                    {
                        Head = Resources.ShoppingCart.EstimatedDistribution,
                        Width = 17,
                        Align = "Left"
                    };
                    listitems.Add(t);

                    et = new EmailText();
                    result = et.GenerateText(listitems);

                    sbEd.Append(result);
                    sbEd.Append(".................................................................");
                    sbEd.Append(Environment.NewLine);

                    var edOrderItems = _orderService.GetOrderItemsForOrder(Convert.ToInt32(orderId)).Where(oi => oi.ElectronicDelivery);
                    foreach (var edOrderItem in edOrderItems)
                    {
                        listitems.Clear();

                        t = new TableItem
                        {
                            Head = edOrderItem.SKU + " ",
                            Width = 12,
                            Align = "Left"
                        };
                        listitems.Add(t);

                        t = new TableItem
                        {
                            Head = edOrderItem.ItemName + " ",
                            Width = 22,
                            Align = "Left"
                        };
                        listitems.Add(t);

                        t = new TableItem
                        {
                            Head = edOrderItem.EstimatedDistribution.ToString() + " ",
                            Width = 14,
                            Align = "Left"
                        };
                        listitems.Add(t);

                        et = new EmailText();
                        result = et.GenerateText(listitems);
                        sbEd.Append(result);
                    }
                    break;
                }
            }

            // Shipping Details
            TableItem tableItem = new TableItem();
            listitems.Clear();

            tableItem.Head = _specialInstructions;
            tableItem.Width = 65;
            tableItem.Align = "Left";
            listitems.Add(tableItem);

            et = new EmailText();
            result = et.GenerateText(listitems);
            specialText.Append(result);

            IEnumerable<RetrieveOrderItemsResult> approvableItems = orderItems.Where(i => !string.IsNullOrEmpty(i.ApproverEmailAddress));
            var requiresApproval = approvableItems.Any();

            string shippingNote = string.Empty;
            if (orderItems.Any(i => !i.ElectronicDelivery))
            {
                StreamReader reader = new StreamReader(Server.MapPath(WebConfigurationManager.AppSettings["ShippingNote.TemplateFile"]));
                shippingNote = reader.ReadToEnd();
                reader.Close();
            }

            string shippingTemplate = string.Empty;
            if (!string.IsNullOrEmpty(shippingText))
            {
                StreamReader reader = new StreamReader(Server.MapPath(WebConfigurationManager.AppSettings["Shipping.TemplateFile"]));
                shippingTemplate = reader.ReadToEnd();
                reader.Close();

                shippingTemplate = shippingTemplate.Replace("[#SHIPPINGDETAILS]", shippingText);
                shippingTemplate = shippingTemplate.Replace("[#SPECIALINSTRUCTIONS]", specialText.ToString());
            }

            string electronicDeliveryTemplate = string.Empty;
            List<RetrieveOrderItemsResult> downloadItems = orderItems.Where(i => i.ElectronicDelivery).ToList();
            if (downloadItems.Count > 0)
            {
                string electronicDeliveryTemplateKey;
                if (requiresApproval)
                {
                    electronicDeliveryTemplateKey = "ElectronicDelivery.TemplateFileNoLinks";
                }
                else
                {
                    electronicDeliveryTemplateKey = "ElectronicDelivery.TemplateFile";
                }
                StreamReader reader = new StreamReader(Server.MapPath(WebConfigurationManager.AppSettings[electronicDeliveryTemplateKey]));
                electronicDeliveryTemplate = reader.ReadToEnd();
                reader.Close();

                if (!requiresApproval)
                {
                    StringBuilder sbDownloadLinks = new StringBuilder();
                    string downloadUrlFormat = $"{RegionalizeService.GetBaseUrl(_userContext.SelectedRegion).TrimEnd('/')}/{WebConfigurationManager.AppSettings["ElectronicDelivery.DownloadUrlFormat"].TrimStart('/')}";
                    foreach (RetrieveOrderItemsResult item in downloadItems)
                    {
                        sbDownloadLinks.Append(downloadUrlFormat.Replace("{key}", item.ElectronicDeliveryID.ToString())
                            .Replace("{sku}", Regex.Replace(item.SKU.Replace(" ", ""), @"[^\w\-\+\~]", "")));
                        sbDownloadLinks.Append(Environment.NewLine);
                    }

                    electronicDeliveryTemplate = electronicDeliveryTemplate.Replace("[#EXPIRATION_DATE]",
                        downloadItems[0].ExpirationDate.Value.ToString("MM/dd/yy"));
                    electronicDeliveryTemplate = electronicDeliveryTemplate.Replace("[#MAX_DOWNLOAD]",
                        WebConfigurationManager.AppSettings["ElectronicDelivery.MaxDownload"]);
                    electronicDeliveryTemplate = electronicDeliveryTemplate.Replace("[#DOWNLOAD_LINKS]", sbDownloadLinks.ToString());
                }
            }

            string vdpTemplate = string.Empty;
            List<RetrieveOrderItemsResult> vdpItems = orderItems.Where(i => i.VDP).ToList();
            if (vdpItems.Count > 0)
            {
                StreamReader reader = new StreamReader(Server.MapPath(WebConfigurationManager.AppSettings["Vdp.TemplateFile"]));
                vdpTemplate = reader.ReadToEnd();
                reader.Close();
            }

            RetrieveOrderDetailResult orderItems1 = _orderService.GetOrderDetails(orderId);
            if (orderItems != null)
            {
                var confirmationEmailBody = GetOrderConfirmationEmailBody(orderItems1.OrderNumber,
                    orderItems1.OrderDate.Value.ToString("MM/dd/yyyy"), sb.ToString(), _userContext.User.Email,
                    _userContext.User.Profile.FirstName.ToString(), _userContext.User.Profile.LastName.ToString(), shippingNote, shippingTemplate,
                    orderItems1.OrderStatus == "Pending Customization",
                    orderItems.Any(i => !i.Customizations.Value && !i.ElectronicDelivery), vdpTemplate,
                    electronicDeliveryTemplate, sbEd.ToString());
                EmailRequest Mail_Request = new EmailRequest();
                Mail_Request.Template.TemplateType = EmailTemplateType.Text;
                Mail_Request.Subject = "Marketing Center Advisors Customer Requested Referral";
                Mail_Request.TemplateFile = Slam.Cms.Configuration.ConfigurationManager.Solution.Settings["RealTimeNotifications.ConsentGivenConfirmationCopy.TemplateFile"];
                Mail_Request.FromAddress = Slam.Cms.Configuration.ConfigurationManager.Solution.Settings["RealTimeNotifications.ConsentGivenConfirmationCopy.FromAddress"];
                Mail_Request.FromName = Slam.Cms.Configuration.ConfigurationManager.Solution.Settings["RealTimeNotifications.ConsentGivenConfirmationCopy.FromName"];
                Mastercard.MarketingCenter.Data.Entities.Processor processor = _processorService.GetProcessorByIssuerId(_userContext.User.IssuerId);

                string[] recipients = Slam.Cms.Configuration.ConfigurationManager.Solution.Settings["RealTimeNotifications.ConsentGivenConfirmationCopy.ToAddress"].Split(';');
                foreach (var recipientEmail in recipients)
                {
                    EmailRecipient recipient = new EmailRecipient(recipientEmail);

                    recipient.Tokens.Add("CUSTOMERNAME", _userContext.User.FullName);
                    recipient.Tokens.Add("CUSTOMEREMAIL", _userContext.User.Email);
                    recipient.Tokens.Add("CUSTOMERPHONE", string.IsNullOrEmpty(_userContext.User.Profile.Phone) ? " " : _userContext.User.Profile.Phone);
                    recipient.Tokens.Add("CUSTOMERTITLE", string.IsNullOrEmpty(_userContext.User.Profile.Title) ? " " : _userContext.User.Profile.Title);
                    recipient.Tokens.Add("FI", (_userContext.User.Issuer == null) ? " " : _userContext.User.Issuer.Title);
                    recipient.Tokens.Add("PROCESSOR", (processor == null) ? " " : processor.Title);

                    recipient.Tokens.Add("CONFIRMATIONCOPY", confirmationEmailBody);

                    Mail_Request.Recipients.Add(recipient);
                }
                Mail_Request.SubmitRequest();
            }
        }
    }

    private string GetOrderConfirmationEmailBody(string orderno, string date, string ItemDetails, string email, string firstName, string lastName, string shippingNote, string shippingTemplate, bool customizedOrder, bool readyToPrint, string vdpTemplate, string electronicDeliveryTemplate, string estimatedDistributionTemplate)
    {
        string confirmationEmail;
        using (var reader = new StreamReader(customizedOrder ? Server.MapPath($"~/EmailTemplate/{ConfigurationManager.AppSettings["OrderConfirmation.TemplateFile"]}") :
                                                               Server.MapPath($"~/EmailTemplate/{ConfigurationManager.AppSettings["OrderConfirmationNoCustomization.TemplateFile"]}")))
        {
            confirmationEmail = reader.ReadToEnd();
            confirmationEmail = confirmationEmail.Replace("[#ORDERNO]", orderno);
            confirmationEmail = confirmationEmail.Replace("[#DATE]", date);
            confirmationEmail = confirmationEmail.Replace("[#DETAILS]", ItemDetails);
            confirmationEmail = confirmationEmail.Replace("[#READYTOPRINTMESSAGE]", readyToPrint ? Resources.ShoppingCart.ReadyToPrintMessage : "  ");
            confirmationEmail = confirmationEmail.Replace("[#SHIPPINGNOTE]", string.IsNullOrEmpty(shippingNote) ? " " : shippingNote);
            confirmationEmail = confirmationEmail.Replace("[#ESTIMATEDDISTRIBUTION]", string.IsNullOrEmpty(estimatedDistributionTemplate) ? " " : estimatedDistributionTemplate);
            confirmationEmail = confirmationEmail.Replace("[#SHIPPING]", string.IsNullOrEmpty(shippingTemplate) ? " " : shippingTemplate);
            confirmationEmail = confirmationEmail.Replace("[#TARGETURL]", _urlService.GetOrderDetailsURL(orderno));
            confirmationEmail = confirmationEmail.Replace("[#VDP]", string.IsNullOrEmpty(vdpTemplate) ? " " : vdpTemplate);
            confirmationEmail = confirmationEmail.Replace("[#ELECTRONICDELIVERY]", string.IsNullOrEmpty(electronicDeliveryTemplate) ? " " : electronicDeliveryTemplate);
        }

        return confirmationEmail;
    }
}

public class EstimatedDistributionOrderItem
{
    public string Sku { get; set; }
    public string Title { get; set; }
    public int? EstimatedDistribution { get; set; }
}