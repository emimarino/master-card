﻿using System;

public partial class Thankyou : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Page.Title = Resources.Forms.ThankYou_Title;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }
}