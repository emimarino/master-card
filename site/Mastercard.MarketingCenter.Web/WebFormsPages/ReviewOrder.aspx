﻿<%@ Page Language="C#" MasterPageFile="~/WebFormsPages/MMPShoppingCart.Master" AutoEventWireup="True" Inherits="ReviewOrder" Title="Untitled Page" CodeBehind="ReviewOrder.aspx.cs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadJavascriptPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="center_container">
        <div class="form_container">
            <div class="orange_heading_profile">
                <%=Resources.ShoppingCart.ReviewOrder_Title%>
            </div>
            <div class="faq_right_container mrt_47">
                <div class="right_tab_top">
                    <div class="orange_heading_main">
                        <%=Resources.ShoppingCart.ItemTypes%>
                    </div>
                </div>
                <div class="right_tab_bg">
                    <p class="none_padding">
                        <%=Resources.ShoppingCart.ItemTypesInstructions%>
                    </p>
                    <div class="link_img">
                        <div class="content_span"><%=Resources.ShoppingCart.Customizable%></div>
                        <div class="image_span">
                            <img src="/portal/inc/images/document_edit.png" border="0" alt="" width="25" height="27" />
                        </div>
                    </div>
                    <div class="link_img">
                        <div class="content_span"><%=Resources.ShoppingCart.MastercardBrandedCollateral%></div>
                        <div class="image_span">
                            <img src="/portal/inc/images/document_lock.png" border="0" alt="" width="25" height="27" />
                        </div>
                    </div>
                    <div class="link_img">
                        <div class="content_span"><%=Resources.ShoppingCart.VariableDataPrinting%></div>
                        <div class="image_span">
                            <img src="/portal/inc/images/vdp.png" border="0" alt="" width="25" height="27" />
                        </div>
                    </div>
                    <div class="link_img">
                        <div class="content_span"><%=Resources.ShoppingCart.ElectronicDelivery%></div>
                        <div class="image_span">
                            <img src="/portal/inc/images/document_down.png" border="0" alt="" width="25" height="27" />
                        </div>
                    </div>
                </div>
                <div class="right_tab_bot">
                </div>
            </div>
            <div class="order_conformation">
                <div class="faq_blog">
                    <!-- grid_container-->
                    <div class="fl">
                        <div class="order_detail fl">
                            <span><%=Resources.ShoppingCart.OrderDetails%></span>
                        </div>
                    </div>
                    <div class="fr mrt_30">
                        <a runat="server" id="linkOrder" href="#" class="price fr"><%=Resources.ShoppingCart.OrderDetailsEdit%></a>
                    </div>
                    <asp:GridView ID="grdShoppingCart" CssClass="main_heading_bg" runat="server" AutoGenerateColumns="false" BorderStyle="None" GridLines="None" OnRowDataBound="grdShoppingCart_ItemDataBound">
                        <Columns>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    <div class="main_heading">
                                        <span class="order_heading width110 pdl_15px pdr_15px"><%=Resources.ShoppingCart.Sku.ToUpperInvariant()%></span>
                                        <span class="order_heading width150"><%=Resources.ShoppingCart.Title%></span>
                                        <span class="order_heading width30">&nbsp;</span>
                                        <span class="order_heading"><%=Resources.ShoppingCart.Quantity%></span>
                                        <span class="order_heading width60"><%=Resources.ShoppingCart.Type%></span>
                                        <span class="order_heading width80 text_right"><%=Resources.ShoppingCart.UnitPrice%></span>
                                        <span class="order_heading width90 text_right"><%=Resources.ShoppingCart.Price%></span>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="chk_outgrid ">
                                        <span class="order_heading_data width110 pdl_15px pdr_15px word_wrap">
                                            <%# Eval("SKU").ToString()%>
                                        </span>
                                        <span class="order_heading_data width150">
                                            <%# Eval("Title").ToString()%>
                                        </span>
                                        <span class="order_heading_data width30">&nbsp;</span>
                                        <span class="order_heading_data text_right width56" style="padding-right: 25px;">
                                            <asp:Label runat="server" ID="txtQuantity" Text='<%# Eval("Quantity").ToString()%>' readonly="true" size="20"></asp:Label>
                                        </span>
                                        <span class="order_heading_data width60">
                                            <asp:Image ID="imgIcon" runat="server" /></span>
                                        <span class="order_heading_data width80 text_right">
                                            <%# GetItemPrice(Eval("ContentItemID").ToString(), Eval("Quantity").ToString(), Convert.ToBoolean(Eval("ElectronicDelivery")))%>
                                        </span>
                                        <span class="order_heading_data width90 text_right">
                                            <%# GetPrice(Eval("ContentItemID").ToString(), Eval("Quantity").ToString(), Convert.ToBoolean(Eval("ElectronicDelivery")))%>
                                        </span>
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <asp:Label ID="lblPromotionError" runat="server" EnableViewState="false" Visible="false" CssClass="main_heading_bg promotion error"></asp:Label>
                    <asp:Panel ID="pnlPromotion" class="main_heading_bg promotion" runat="server" Visible="false">
                        <asp:Panel ID="pnlPromotionApplied" runat="server" CssClass="chk_outgrid">
                            <div class="promo-description">
                                <asp:Literal ID="litPromotionDescription" runat="server"></asp:Literal>
                            </div>
                            <div class="promo-amount">
                                <asp:Literal ID="litPromotionAmount" runat="server"></asp:Literal>
                            </div>
                        </asp:Panel>
                    </asp:Panel>

                    <div class="main_heading_bg">
                        <div class="chk_outgrid" style="border-top: 1px solid #aaa; margin-left: 15px; margin-right: 15px; width: 648px;">
                            <asp:PlaceHolder ID="phBillingICA" runat="server">
                                <div class="billing_ica">
                                    <span style="float: left; margin-right: 20px;">
                                        <%=Resources.ShoppingCart.BillingIca%>:
                                    <asp:Literal ID="litBillingICA" runat="server"></asp:Literal>
                                    </span>
                                    <a runat="server" id="linkOrderICA" href="#" class="price"><%=Resources.ShoppingCart.BillingIcaEdit%></a>
                                </div>
                            </asp:PlaceHolder>
                            <div class="total_span fr">
                                <%=Resources.ShoppingCart.Total%>:
                                <asp:Label ID="lblTotal" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="meaning_bot">
                    </div>
                    <div id="pnlGuideToBenefitsNote" runat="server" class="requried">
                        * <%=Resources.ShoppingCart.GuideToBenefitsDetails%>
                    </div>
                    <div id="pnlShippingNote" runat="server" class="requried" visible="false">
                        * <%=Resources.ShoppingCart.ShippingHandlingDetails%>
                        <br />
                        <br />
                        <%=Resources.ShoppingCart.SubmitOrderDescription%>
                    </div>
                    <!-- grid_container-->
                    <asp:PlaceHolder ID="phEstimatedDistribution" runat="server">
                        <div class="clr" style="margin-bottom: 15px;">
                        </div>
                        <div class="fr mrt_30">
                            <a runat="server" id="linkEstimatedDistribution" href="#" class="price fr"><%=Resources.ShoppingCart.EstimatedDistributionEdit%></a>
                        </div>
                        <asp:GridView ID="grdEstimatedDistribution" CssClass="main_heading_bg" runat="server" AutoGenerateColumns="false" BorderStyle="None" GridLines="None">
                            <Columns>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                                    <HeaderTemplate>
                                        <div class="main_heading">
                                            <span class="order_heading width110 pdl_15px pdr_15px"><%=Resources.ShoppingCart.Sku.ToUpperInvariant()%></span>
                                            <span class="order_heading width150"><%=Resources.ShoppingCart.Title%></span>
                                            <span class="order_heading width30">&nbsp;</span>
                                            <span class="order_heading width205"><%=Resources.ShoppingCart.EstimatedDistribution%></span>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="chk_outgrid ">
                                            <span class="order_heading_data width110 pdl_15px pdr_15px word_wrap"><%# Eval("SKU").ToString()%></span>
                                            <span class="order_heading_data width150"><%# Eval("Title").ToString()%></span>
                                            <span class="order_heading_data width30">&nbsp;</span>
                                            <span class="order_heading_data text_right width56" style="padding-right: 25px;">
                                                <asp:Label runat="server" ID="txtEstimatedDistribution" Text='<%# Eval("EstimatedDistribution").ToString()%>' readonly="true" size="20"></asp:Label></span>
                                        </div>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                        <div class="chk_outgrid gray_backgrnd">
                                            <span class="order_heading_data width110 pdl_15px pdr_15px word_wrap"><%# Eval("SKU").ToString()%></span>
                                            <span class="order_heading_data width150"><%# Eval("Title").ToString()%></span>
                                            <span class="order_heading_data width30">&nbsp;</span>
                                            <span class="order_heading_data text_right width56" style="padding-right: 25px;">
                                                <asp:Label runat="server" ID="txtEstimatedDistribution" Text='<%# Eval("EstimatedDistribution").ToString()%>' readonly="true" size="20"></asp:Label></span>
                                        </div>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                    </FooterTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <div class="main_heading_bot" style="margin-bottom: 15px">
                        </div>
                    </asp:PlaceHolder>
                    <!-- grid_container-->
                    <div class="clr" style="margin-bottom: 15px;">
                    </div>
                    <asp:PlaceHolder ID="placeCustomizations" runat="server">
                        <div class="fr" style="margin-top: 10px;">
                            <a runat="server" id="linkCustomization" class="price"><%=Resources.ShoppingCart.CustomizationsDetailsEdit%></a>
                        </div>
                        <div class="main_heading ">
                            <div class="orange_heading_main">
                                <%=Resources.ShoppingCart.CustomizationDetails%>
                            </div>
                        </div>
                        <div class="main_heading_bg">
                            <asp:Repeater runat="server" ID="rptCustomizations">
                                <ItemTemplate>
                                    <div class="chk_outgrid">
                                        <div class="custmize_span">
                                            <%# Eval("FriendlyName") %><%#  ((Eval("Required") != null && Eval("Required").ToString().ToLower() == "true") ? "*" : "")%>
                                        </div>
                                        <div class="right_grid_custmize">
                                            <div>
                                                <asp:PlaceHolder ID="PlaceHolderimg" runat="server" Visible='<%#(Eval("FieldType").ToString().ToLower() == "image") ? true : false %>'>
                                                    <img id="image_cust" runat="server" height="131" width="128" src='<%# ((Eval("DefaultCustomizationData")!=null)) ? _urlService.GetStaticImageURL(Eval("DefaultCustomizationData").ToString(),200,200,false,85,true) : "" %>' alt="" />
                                                </asp:PlaceHolder>
                                                <asp:PlaceHolder ID="PlaceHoldertxt" runat="server" Visible='<%#(Eval("FieldType").ToString().ToLower() == "image") ? false : true %>'>
                                                    <asp:Label runat="server" ID="txtOptionsValue" CssClass="order_heading_data width_auto text_align_justify" Text='<%# (Eval("DefaultCustomizationData")!=null) ? Eval("DefaultCustomizationData").ToString().Replace("\r\n","<br/>") : ""%>'></asp:Label>
                                                </asp:PlaceHolder>
                                            </div>
                                            <div class="clr">
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <div class="chk_outgrid gray_backgrnd">
                                        <div class="custmize_span">
                                            <%# Eval("FriendlyName") %><%#   ((Eval("Required") != null && Eval("Required").ToString().ToLower() == "true") ? "*" : "")%>
                                        </div>
                                        <div class="right_grid_custmize">
                                            <div>
                                                <asp:PlaceHolder ID="PlaceHolderimg" runat="server" Visible='<%#(Eval("FieldType").ToString().ToLower() == "image") ? true : false %>'>
                                                    <img id="image_cust" runat="server" height="131" width="128" src='<%# ((Eval("DefaultCustomizationData")!=null)) ? _urlService.GetStaticImageURL(Eval("DefaultCustomizationData").ToString(),200,200,false,85,true) : "" %>' alt="" />
                                                </asp:PlaceHolder>
                                                <asp:PlaceHolder ID="PlaceHoldertxt" runat="server" Visible='<%#(Eval("FieldType").ToString().ToLower() == "image") ? false : true %>'>
                                                    <asp:Label runat="server" ID="txtOptionsValue" CssClass="order_heading_data width_auto text_align_justify" Text='<%# (Eval("DefaultCustomizationData")!=null) ? Eval("DefaultCustomizationData").ToString().Replace("\r\n","<br/>") : "" %>'></asp:Label>
                                                </asp:PlaceHolder>
                                            </div>
                                            <div class="clr">
                                            </div>
                                        </div>
                                    </div>
                                </AlternatingItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="main_heading_bot" style="margin-bottom: 15px">
                        </div>
                    </asp:PlaceHolder>
                    <!--end grid container-->
                    <!-- grid_container-->
                    <div class="fr" style="padding-bottom: 3px; margin-top: 10px;">
                        <a runat="server" id="linkShipping" class="price"><%=Resources.ShoppingCart.ShippingInformationEdit%></a><a
                            runat="server" id="linkShippingAllocation" style="margin-left: 10px;" class="price"><%=Resources.ShoppingCart.ShippingAllocationEdit%></a>
                    </div>
                    <div class="main_heading">
                        <span class="order_heading width178 ml14"><%=Resources.ShoppingCart.FulfillmentInformation%></span>
                        <span class="order_heading width95"><%=Resources.ShoppingCart.Sku.ToUpperInvariant()%></span>
                        <span class="order_heading width205"><%=Resources.ShoppingCart.Title%></span>
                        <span class="order_heading width95"><%=Resources.ShoppingCart.Quantity%></span>
                        <span class="order_heading width80 text_right"><%=Resources.ShoppingCart.Price%></span>
                    </div>
                    <div class="main_heading_bg">
                        <mc:ElectronicFullfilment ID="electronicFullfilment" runat="Server"></mc:ElectronicFullfilment>
                        <mc:VdpFullfilment ID="vdpFullfilment" runat="Server"></mc:VdpFullfilment>
                        <mc:PrintFullfilment ID="printFullfilment" runat="Server"></mc:PrintFullfilment>
                    </div>
                </div>
                <div class="main_heading_bot">
                </div>
                <!--end grid container-->
                <div class="bg_btn_left fr pd_bot">
                    <asp:Button runat="server" ID="btnConfirmOrder" CssClass="submit_bg_btn" BorderWidth="0px" Text="<%$Resources:ShoppingCart,SubmitOrder%>" OnClick="btnConfirmOrder_Click"></asp:Button>
                </div>
                <asp:PlaceHolder runat="server" ID="plcError" Visible="false">
                    <span style="font-size: 12px; font-weight: bold;">
                        <asp:Label runat="server" ID="lblError" CssClass="red_error no_padding"></asp:Label>
                    </span>
                </asp:PlaceHolder>
            </div>
        </div>
    </div>
</asp:Content>
