﻿<%@ Page Title="Error Page" Language="C#" MasterPageFile="~/WebFormsPages/MMP.master" AutoEventWireup="true" Inherits="Error" CodeBehind="Error.aspx.cs" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="center_container">
        <div class="errorpage">
            <asp:PlaceHolder ID="phInvalidOrderError" runat="server" Visible="false">
                <div class="orange_heading_profile"><%=Resources.Errors.OrderNumberInvalid%></div>
                <div class="info mrt_25"><%=Resources.Errors.EmailSentToWebsiteAdministrators%></div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phAccessDeniedError" runat="server" Visible="false">
                <div class="orange_heading_profile"><%=Resources.Errors.PageUnavailable%></div>
                <div class="info mrt_25"><%=Resources.Errors.ContactMastercardAccountRepresentative%></div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phGeneralError" runat="server" Visible="false">
                <div class="orange_heading_profile"><%=Resources.Errors.ProcessingRequest%></div>
                <div class="info mrt_25"><%=Resources.Errors.EmailSentToWebsiteAdministrators%></div>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>