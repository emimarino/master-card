﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="BreadCrumb.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.UserControls.BreadCrumb" %>

<asp:Repeater ID="rptBreadCrumbItems" runat="server" OnItemDataBound="rptBreadCrumbItems_ItemDataBound">
    <ItemTemplate>
        <asp:PlaceHolder runat="server" ID="lnkBreadCrumbItem">
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("Url") %>'><%# Eval("Title") %></asp:HyperLink>
        </asp:PlaceHolder>

        <asp:PlaceHolder runat="server" Visible="false" ID="ltBreadCrumbItem">
            <span>
                <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("Title") %>'></asp:Literal>
            </span>
        </asp:PlaceHolder>
    </ItemTemplate>
</asp:Repeater>