﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NoTagBrowserFooter.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.UserControls.NoTagBrowserFooter" %>

<footer class="page-footer no-tagbrowser">
    <div class="clearfix nav-copy grid-spaces-in">
        <div class="copyrights"><%=string.Format(Resources.Shared.MastercardCopyrights, DateTime.Now.Year)%></div>
        <nav class="nav-footer">
            <%=Resources.Shared.Mastercard%>
            <%= string.Format(Resources.Shared.MastercardTermsOfUse, sitemap.GetSetting("TermsOfUse", UserContext.SelectedRegion))%>
            <%=string.Format(Resources.Shared.MastercardPrivacyPolicy, sitemap.GetSetting("PrivacyNotice", UserContext.SelectedRegion))%>
        </nav>
    </div>
</footer>

<script language="javascript">
    var visitorType = "<%=VisitorType%>";
</script>