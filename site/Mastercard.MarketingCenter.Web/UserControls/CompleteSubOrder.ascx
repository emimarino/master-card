﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="CompleteSubOrder.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.UserControls.CompleteSubOrder" %>

<script src="<%= DependencyResolver.Current.GetService<Mastercard.MarketingCenter.Services.Interfaces.IUrlService>().GetStaticCommonFunctionJs() %>"></script>
<div class="center_container">
    <div class="form_container">
        <div class="orange_heading_profile">
            <%=Resources.ShoppingCart.CompletePartialShipment%>
        </div>
        <div class="clr">
        </div>
        <div class="faq_blog">
            <div class="roundcont_error mrb_25" id="divError" runat="server" visible="False">
                <div class="roundtop">
                    <img src="/portal/inc/images/tl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
                </div>
                <div style="float: left; width: 80px; text-align: center;">
                    <img height="40" border="0px" width="40" alt="" src="/portal/inc/images/error_img.gif" />
                </div>
                <div style="float: left; width: 500px;">
                    <asp:Label ID="lblError" CssClass="span_1" runat="server" Visible="False"></asp:Label>
                </div>
                <div style="clear: both;"></div>
                <div class="roundbottom">
                    <img src="/portal/inc/images/bl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
                </div>
            </div>
            <!--User and Shipping Information-->
            <div class="main_heading ">
                <span class="order_heading width300 pdl_15px"><%=Resources.ShoppingCart.ShippingInformation%></span>
                <span class="order_heading width95"><%=Resources.ShoppingCart.Sku.ToUpperInvariant()%></span>
                <span class="order_heading width90"><%=Resources.ShoppingCart.Title%></span>
                <span class="order_heading width90"><%=Resources.ShoppingCart.Quantity%></span>
                <span class="order_heading width90"><%=Resources.ShoppingCart.Price%></span>
            </div>
            <div class="main_heading_bg">
                <div class="chk_outgrid">
                    <div class="right_block">
                        <asp:Repeater ID="rptSubOrderItems" runat="server">
                            <ItemTemplate>
                                <div style="margin-bottom: 3px;">
                                    <span class="order_heading_data width95 word_wrap">
                                        <%# Eval("SKU").ToString()%>
                                    </span>
                                    <span class="order_heading_data width90">
                                        <%# Eval("ItemName").ToString()%>
                                    </span>
                                    <span class="order_heading_data width90">
                                        <asp:Label runat="server" ID="txtQuantity" Text='<%# Eval("ItemQuantity").ToString()%>' readonly="true" size="20"></asp:Label>
                                    </span>
                                    <span class="order_heading_data width90">
                                        <%# GetPrice(Eval("ItemPrice").ToString())%>
                                    </span>
                                    <div class="clr"></div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <div class="left_block">
                        <span class="order_heading_data width175 pdl_15px">
                            <asp:Literal ID="ltAddress" runat="server"></asp:Literal></span>
                        <span class="order_heading_data">
                            <asp:Literal ID="ltContactName" runat="server"></asp:Literal><br />
                            <asp:Literal ID="ltPhone" runat="server"></asp:Literal>
                        </span>
                    </div>
                </div>
                <div id="divcostadjustment" runat="server">
                    <div class="heading_span_new mrt_10">
                        <%=Resources.ShoppingCart.PriceAdjustment%>
                    </div>
                    <div class="chk_outgrid ">
                        <div class="data_lost_right">
                            <span class="fr mr11">$<asp:Label ID="lblPriceAdjustment" runat="server" CssClass="pricedetails" Visible="False"></asp:Label>
                                <asp:TextBox ID="txtPriceAdjustment" MaxLength="10" runat="server"></asp:TextBox>
                            </span>
                        </div>
                        <div class="data_rtf_left">
                            <asp:Label ID="lblDescription" runat="server" CssClass="pricedetails" Visible="False"></asp:Label>
                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" Rows="3" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="chk_outgrid gray_backgrnd">
                        <div class="data_lost_right">
                            <span class="fr mr11">$<asp:Label ID="lblShippingCost" runat="server" CssClass="pricedetails" Visible="False"></asp:Label>
                                <asp:TextBox ID="txtShippingCost" MaxLength="10" runat="server"></asp:TextBox>
                            </span>
                        </div>
                        <div class="data_rtf_left">
                            <label><%=Resources.ShoppingCart.ShippingCost%> *</label>
                        </div>
                    </div>
                    <div class="chk_outgrid border_rotbot">
                        <div class="data_lost_right">
                            <span class="fr mr11">$<asp:Label ID="lblPostageCost" runat="server" CssClass="pricedetails" Visible="False"></asp:Label>
                                <asp:TextBox ID="txtPostageCost" MaxLength="10" runat="server"></asp:TextBox>
                            </span>
                        </div>
                        <div class="data_rtf_left">
                            <label><%=Resources.ShoppingCart.PostageCost%></label>
                        </div>
                    </div>
                    <div class="chk_outgrid ">
                        <div class="heading_span_new mrt_10">
                            <%=Resources.ShoppingCart.TrackingNumber%>*
                        </div>
                        <div class="data_rtf_left">
                            <asp:TextBox CssClass="text_rate" ID="txtTracking" MaxLength="255" runat="server"></asp:TextBox>
                            <asp:HiddenField ID="hdnShippingInformationId" runat="server" />
                        </div>
                    </div>
                    <div class="fl" style="width: 660px; float: right;">
                        <span runat="server" id="spnComplete" class="bg_btn_left mr11 fr">
                            <asp:Button ID="btnComplete" BorderWidth="0px" CssClass="submit_bg_btn" runat="server" Text="<%$Resources:Shared,Complete%>" />
                        </span>
                        <span class="bg_btn_left mr11 fr">
                            <asp:Button ID="btnCancel" BorderWidth="0px" CssClass="submit_bg_btn " runat="server" Text="<%$Resources:Shared,Cancel%>" />
                        </span>
                        <span class="label_margin requried fl">* <%=Resources.Shared.Required%></span>
                    </div>
                </div>
                <div class="clr"></div>
                <div class="fr" style="margin-bottom: 4px; padding-bottom: 3px;">&nbsp;</div>
                <div class="main_heading_bot"></div>
            </div>
            <div class="clr"></div>
            <!--end grid container-->
            <div class="pd_bot fl">
                &nbsp;
            </div>
        </div>
    </div>