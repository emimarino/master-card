﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.UserControls.Footer" %>
<div id="footer">
    <footer class="page-footer">
        <nav class="nav-footer-map grid-spaces-in">
            <div class="inner footer-grid">
                <asp:Repeater ID="browseAllGroups" runat="server" OnItemDataBound="browseAllGroups_ItemDataBound">
                    <ItemTemplate>
                        <div class="grid-item--wrapper">
                            <asp:Repeater runat="server" ID="browseAllTagCategory" OnItemDataBound="browseAllTagCategory_ItemDataBound">
                                <ItemTemplate>
                                    <dl class="grid-item">
                                        <dt><%# Mastercard.MarketingCenter.Common.Extensions.StringExtension.RawWithSpecialCharacters(Eval("Text").ToString())%></dt>
                                        <asp:Repeater runat="server" ID="browseAllTagCategoryChildren">
                                            <ItemTemplate>
                                                <dd <%# Container.ItemIndex > 4 ? "class='hide-more'" : "" %>>
                                                    <a tracking-type="browse-all" href="<%= BaseUrl %>/tagbrowser/<%# Eval("Identifier")%>"><%# Mastercard.MarketingCenter.Common.Extensions.StringExtension.RawWithSpecialCharacters(Eval("Text").ToString())%></a> (<%# Eval("ContentItemCount")%>)
                                                </dd>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <dd id="ddShowMore" runat="server" class="more-link"><a onclick="show_more(this);">show more <em class="fa fa-angle-down"></em></a></dd>
                                        <dd id="ddShowLess" runat="server" class="less-link"><a onclick="show_less(this);">show less <em class="fa fa-angle-up"></em></a></dd>
                                    </dl>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </nav>
        <div class="clearfix nav-copy grid-spaces-in">
            <div class="copyrights"><%=string.Format(Resources.Shared.MastercardCopyrights, DateTime.Now.Year)%></div>
            <nav class="nav-footer">
                <%=Resources.Shared.Mastercard%>
                <%=string.Format(Resources.Shared.MastercardTermsOfUse, regionConfig.GetSetting("TermsOfUse", UserContext.SelectedRegion))%>
                <%=string.Format(Resources.Shared.MastercardPrivacyPolicy, regionConfig.GetSetting("PrivacyNotice", UserContext.SelectedRegion))%>
                <%
                    if(!string.IsNullOrEmpty(regionConfig.GetSetting("Imprint", UserContext.SelectedRegion)))
                    {
                %>
                        <%=string.Format(UserContext.SelectedRegion.Equals("de") ? "<a class=\"item\" href=\"{0}\" title=\"impressum\" target=\"_blank\" rel=\"noopener noreferrer\">Impressum</a>" : Resources.Shared.Imprint, regionConfig.GetSetting("Imprint", UserContext.SelectedRegion))%>
                <%
                    }
                %>

            </nav>
        </div>
    </footer>
    <script language="javascript">
        var visitorType = "<%=VisitorType%>";
    </script>
</div>
