﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.UserControls
{
    public partial class VdpFullfilmentItem : System.Web.UI.UserControl
    {
        public int CartId
        {
            get { return Convert.ToInt32(ViewState["CartId"]); }
            set { ViewState["CartId"] = value; }
        }

        public int OrderId
        {
            get { return Convert.ToInt32(ViewState["OrderId"]); }
            set { ViewState["OrderId"] = value; }
        }

        public bool AllowToComplete
        {
            get { return Convert.ToBoolean(ViewState["AllowToComplete"]); }
            set { ViewState["AllowToComplete"] = value; }
        }

        IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }
        IEmailService _emailService { get { return DependencyResolver.Current.GetService<IEmailService>(); } }
        IUserService _userService { get { return DependencyResolver.Current.GetService<IUserService>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                pnlComplete.Visible = AllowToComplete;
                lnkSendDataFile.NavigateUrl = "~/senddatafile/" + OrderId;

                MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();
                if (OrderId == 0)
                {
                    List<RetrieveVdpCartItemsByCartIdResult> vdpItems = dataContext.RetrieveVdpCartItemsByCartId(CartId).ToList();
                    if (vdpItems.Count > 0)
                    {
                        rptVdpItems.DataSource = vdpItems;
                        rptVdpItems.DataBind();
                    }
                }
                else
                {
                    List<RetrieveVdpOrderItemsByOrderIdResult> vdpItems = dataContext.RetrieveVdpOrderItemsByOrderId(OrderId).ToList();
                    if (vdpItems.Count > 0)
                    {
                        rptVdpOrderItems.DataSource = vdpItems;
                        rptVdpOrderItems.DataBind();

                        pnlVdpOrder.Visible = true;

                        if ((Page.User.IsInRole(Constants.Roles.Printer) || Page.User.IsInRole(Constants.Roles.DevAdmin) || Page.User.IsInRole(Constants.Roles.McAdmin)) &&
                            Request.Url.ToString().ToLower().Contains(Slam.Cms.Configuration.ConfigurationManager.Environment.AdminUrl.ToLower()))
                        {
                            vdpInstruction.Visible = false;
                            ftpStatus.Attributes["style"] = "";

                            FtpAccount ftp = dataContext.FtpAccounts.SingleOrDefault(f => f.OrderID == OrderId);
                            if (ftp == null)
                            {
                                litFtpStatus.Text = Resources.ShoppingCart.PendingAccountNotification;
                                lnkSendFtp.Attributes["style"] = "";
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(ftp.Server))
                                {
                                    litFtpStatus.Text = Resources.ShoppingCart.PendingRetrievalAccountInformation;
                                    lnkSendFtp.Attributes["style"] = "";
                                    txtFtpServer.Text = ftp.Server;
                                    txtFtpUserName.Text = ftp.UserName;
                                    txtFtpPassword.Text = ftp.Password;
                                }
                                else
                                {
                                    if (!ftp.FileAccepted)
                                    {
                                        litFtpStatus.Text = Resources.ShoppingCart.PendingVerificationReviewDataFile;
                                        btnAcceptDataFile.Visible = true;
                                    }
                                    else
                                    {
                                        litFtpStatus.Text = Resources.ShoppingCart.DataFileAccepted;
                                        VdpOrderInformation vdpOrderInformation = _orderService.GetVdpOrderInformation(OrderId);
                                        if (vdpOrderInformation == null || !vdpOrderInformation.Completed)
                                        {
                                            string orderStatus = _orderService.GetOrderStatus(OrderId);
                                            btnComplete.Enabled = orderStatus.Equals("Fulfillment");
                                            if (btnComplete.Enabled)
                                            {
                                                ((HtmlGenericControl)btnComplete.Parent).Attributes.Remove("class");
                                                ((HtmlGenericControl)btnComplete.Parent).Attributes.Add("class", "bg_btn_left ml11 mb11 fl");
                                                btnComplete.CssClass = "submit_bg_btn";
                                            }
                                        }
                                        else
                                        {
                                            lblCompleted.Visible = true;
                                            btnComplete.Visible = false;

                                            ((HtmlGenericControl)btnComplete.Parent).Visible = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFtpServer.Text) && !string.IsNullOrEmpty(txtFtpUserName.Text) && !string.IsNullOrEmpty(txtFtpPassword.Text))
            {
                MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();
                FtpAccount ftp = dataContext.FtpAccounts.SingleOrDefault(f => f.OrderID == OrderId);
                if (ftp == null)
                {
                    ftp = new FtpAccount();
                    ftp.DateCreated = DateTime.Now;
                    dataContext.FtpAccounts.InsertOnSubmit(ftp);
                }

                ftp.Server = txtFtpServer.Text;
                ftp.UserName = txtFtpUserName.Text;
                ftp.Password = txtFtpPassword.Text;
                ftp.OrderID = OrderId;
                ftp.FileAccepted = false;

                dataContext.SubmitChanges();

                var order = _orderService.GetOrderById(OrderId);
                if (order != null)
                {
                    var user = _userService.GetUserByUserName(order.UserID);
                    if (user != null)
                    {
                        _emailService.SendFtpAccountInformationEmail(user.Email, order.OrderNumber, user.UserId);
                    }
                }

                Response.Redirect(Request.Url.AbsoluteUri);
            }
            else
            {
                pnlError.Visible = true;
                mdlFtp.Show();
            }
        }

        protected void btnAcceptDataFile_Click(object sender, EventArgs e)
        {
            MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();
            FtpAccount ftp = dataContext.FtpAccounts.SingleOrDefault(f => f.OrderID == OrderId);
            if (ftp != null)
            {
                ftp.FileAccepted = true;
                dataContext.SubmitChanges();

                Response.Redirect(Request.Url.AbsoluteUri);
            }
        }

        protected string GetPrice(string globalId, string quantity)
        {
            AssetPrice assetPrice = ShoppingCartService.GetAssetPrice(globalId, Convert.ToInt32(quantity), false);
            if (assetPrice.ItemPrice > 0)
            {
                return string.Format(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "{0:C}", assetPrice.ItemPrice * Convert.ToInt32(quantity));
            }

            return string.Format(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "{0:C}", assetPrice.TotalPrice);
        }

        protected void btnComplete_Click(object sender, EventArgs e)
        {
            string referrer = Request.QueryString["Source"] != null ? "&Source=" + Request.QueryString["Source"] : "";
            Response.Redirect("CompleteVdpOrderNoChrome.aspx?OrderId=" + OrderId + referrer);
        }
    }
}