﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.UserControls
{
    public partial class PrintFullfilmentItem : System.Web.UI.UserControl
    {
        public int CartId
        {
            get { return Convert.ToInt32(ViewState["CartId"]); }
            set { ViewState["CartId"] = value; }
        }

        public int OrderId
        {
            get { return Convert.ToInt32(ViewState["OrderId"]); }
            set { ViewState["OrderId"] = value; }
        }

        private Dictionary<string, AssetPrice> fullfilmentInfo = new Dictionary<string, AssetPrice>();
        List<string> assetsWithMinimums = new List<string>();

        UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
        IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (OrderId == 0)
                {
                    List<RetrieveShoppingCartByUserIdResult> lsCartItems = ShoppingCartService.GetShoppingCartByUserId(_userContext.User.UserName).Where(x => x.Quantity.Value > 0).ToList();
                    if (lsCartItems.Any(i => !i.ElectronicDelivery && i.VDP != null && !i.VDP.Value))
                    {
                        using (MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext())
                        {
                            var ShippingDetail = dataContext.RetrieveShippingInformation(CartId, null).ToList();

                            if (ShippingDetail.Count > 0)
                            {
                                rptShippingAddresses.DataSource = ShippingDetail;
                                rptShippingAddresses.DataBind();

                                ShoppingCart cart = ShippingServices.GetShoppingCartOptions(CartId);
                                lblSpecialInstructions.Text = cart.ShippingInstructions;
                                if (cart.RushOrder.HasValue ? cart.RushOrder.Value : false)
                                    ltRushOrder.Text = "<br />" + Resources.ShoppingCart.RushOrderRequested.ToUpperInvariant();

                                divspecialinstructions.Visible = true;
                            }
                            else
                                divspecialinstructions.Visible = false;
                        }
                    }
                    else
                        divspecialinstructions.Visible = false;
                }
                else
                {
                    List<RetrieveShippingInformationResult> ShippingDetail = _orderService.GetShippingInformation(OrderId);
                    if (ShippingDetail != null)
                    {
                        rptOrderShippingAddresses.DataSource = ShippingDetail;
                        rptOrderShippingAddresses.DataBind();
                    }

                    RetrieveOrderDetailResult orderDetail = _orderService.GetOrderDetails(OrderId);

                    if (!String.IsNullOrEmpty(orderDetail.SpecialInstructions))
                    {
                        divspecialinstructions.Visible = true;
                        lblSpecialInstructions.Text = orderDetail.SpecialInstructions.Replace("\r\n", "<br/>");
                    }
                    else
                        divspecialinstructions.Visible = false;

                    if (orderDetail.RushOrder)
                        ltRushOrder.Text = "<br /><br />" + Resources.ShoppingCart.RushOrderRequested.ToUpperInvariant();
                }
            }
        }

        protected void rptShippingAddresses_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null && (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                RetrieveShippingInformationResult shippingInfo = e.Item.DataItem as RetrieveShippingInformationResult;

                Repeater rptInnerCartItems = e.Item.FindControl("rptInnerCartItems") as Repeater;

                using (MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext())
                {
                    var cartItems = dataContext.RetrieveShoppingCartItemsForShippingInformation(shippingInfo.ShippingInformationID).Where(x => x.Quantity.Value > 0).ToList();
                    if (cartItems.Count > 0)
                    {
                        rptInnerCartItems.DataSource = cartItems;
                        rptInnerCartItems.DataBind();
                    }
                    else
                        e.Item.Visible = false;
                }
            }
        }

        protected void rptOrderShippingAddresses_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null && (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                RetrieveShippingInformationResult shippingInfo = e.Item.DataItem as RetrieveShippingInformationResult;

                Repeater rptInnerOrderItems = e.Item.FindControl("rptInnerOrderItems") as Repeater;

                using (MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext())
                {
                    var orderItems = dataContext.RetrieveOrderItemsForShippingInformation(shippingInfo.ShippingInformationID).ToList();
                    if (orderItems.Count > 0)
                    {
                        rptInnerOrderItems.DataSource = orderItems;
                        rptInnerOrderItems.DataBind();
                    }
                }
            }
        }

        protected string GetPartialPrice(string GlobalID, string Quantity)
        {
            string returnPrice = string.Empty;
            if (Quantity.Length > 0)
            {
                if (Quantity != "0")
                {
                    decimal intQuantity = decimal.Parse(Quantity);
                    if (!fullfilmentInfo.ContainsKey(GlobalID))
                        fullfilmentInfo.Add(GlobalID, ShoppingCartService.GetAssetPrice(GlobalID, ShoppingCartService.GetPODContentItemTotalQuantityForShoppingCart(CartId, GlobalID), false));

                    if (fullfilmentInfo[GlobalID].ItemPrice > 0)
                        returnPrice = FormatNumber(intQuantity * fullfilmentInfo[GlobalID].ItemPrice);
                    else
                    {
                        if (!assetsWithMinimums.Contains(GlobalID))
                        {
                            returnPrice = FormatNumber(fullfilmentInfo[GlobalID].TotalPrice);
                            assetsWithMinimums.Add(GlobalID);
                        }
                        else
                            returnPrice = FormatNumber(0);
                    }
                }
                else
                    returnPrice = FormatNumber(0);
            }
            return returnPrice;
        }

        protected string FormatNumber(decimal num)
        {
            System.Globalization.NumberFormatInfo nf = new System.Globalization.NumberFormatInfo();
            nf.CurrencySymbol = "$";
            nf.NumberDecimalDigits = 2;
            return num.ToString("C", nf);
        }
    }
}