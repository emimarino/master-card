﻿using AWS.Web.Data;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.UserControls
{
    public partial class FooterShoppingCart : System.Web.UI.UserControl
    {
        protected UrlHelper Url;

        public string VisitorType
        {
            get;
            set;
        }

        public Core.MastercardSitemap sitemap
        {
            get
            {
                return (Core.MastercardSitemap)DependencyResolver.Current.GetService<Slam.Cms.Sitemap>();
            }
        }

        public UserContext UserContext
        {
            get
            {
                return DependencyResolver.Current.GetService<UserContext>();
            }
        }

        protected ISegmentationService SegmentationServices
        {
            get
            {
                return DependencyResolver.Current.GetService<ISegmentationService>();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Url = new UrlHelper(Request.RequestContext);

            if (!IsPostBack)
            {
                var users = DataServices.ExecuteForList<RetrieveUserInformationResult>("RetrieveUserInformation", UserContext.User?.UserName ?? string.Empty);
                if (users != null && users.Any())
                {
                    var user = users[0];
                    switch (user.processor)
                    {
                        case "MasterCard":
                            VisitorType = "MasterCard";
                            break;
                        case "A Plus":
                            VisitorType = "Vendor";
                            break;
                        default:
                            VisitorType = string.Join(";", SegmentationServices.GetSegmentationsByUserName(UserContext.User.UserName)?.Select(s => $"{s.SegmentationName}"));
                            break;
                    }
                }
            }
        }
    }
}