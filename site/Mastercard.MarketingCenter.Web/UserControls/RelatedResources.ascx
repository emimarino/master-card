﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_RelatedResources" CodeBehind="RelatedResources.ascx.cs" %>

<div id="divRelatedResources" runat="server" class="level_right_bar">
    <div class="inner_short_top">
        <%=Resources.Shared.RelatedResources%>
    </div>
    <div class="inner_short_center related_resources">
        <asp:Repeater ID="rptRelatedResources" runat="server">
            <ItemTemplate>
                <p>
                    <a href="<%# DataBinder.Eval(Container.DataItem, "HRef") %>"><%# DataBinder.Eval(Container.DataItem, "Title")%></a>
                </p>
            </ItemTemplate>
        </asp:Repeater>
        <div class="clear"></div>
    </div>
    <div class="inner_short_bottom">
    </div>
</div>