﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.UserControls
{
    public partial class ElectronicFullfilmentItem : System.Web.UI.UserControl
    {
        public int CartId
        {
            get { return Convert.ToInt32(ViewState["CartId"]); }
            set { ViewState["CartId"] = value; }
        }

        public int OrderId
        {
            get { return Convert.ToInt32(ViewState["OrderId"]); }
            set { ViewState["OrderId"] = value; }
        }

        IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();
                if (OrderId == 0)
                {
                    List<RetrieveElectronicDeliveryCartItemsByCartIdResult> edItems = dataContext.RetrieveElectronicDeliveryCartItemsByCartId(CartId).ToList();
                    if (edItems.Count > 0)
                    {
                        rptElectronicDownloadCartItems.DataSource = edItems;
                        rptElectronicDownloadCartItems.DataBind();
                    }
                }
                else
                {
                    List<RetrieveElectronicDeliveryOrderItemsByOrderIdResult> orderItems = dataContext.RetrieveElectronicDeliveryOrderItemsByOrderId(OrderId).ToList();
                    if (orderItems.Count > 0)
                    {
                        RetrieveOrderDetailResult order = _orderService.GetOrderDetails(OrderId);

                        bool requiresApproval = false;
                        foreach (RetrieveElectronicDeliveryOrderItemsByOrderIdResult item in orderItems)
                        {
                            requiresApproval = (item.NeedsApproval.HasValue && item.NeedsApproval.Value) || requiresApproval;
                        }

                        litExpirationDate.Text = orderItems[0].ExpirationDate.ToString("MM/dd/yy");
                        litMaxDownload.Text = WebConfigurationManager.AppSettings["ElectronicDelivery.MaxDownload"];

                        rptDownloadLinks.DataSource = orderItems;
                        rptDownloadLinks.DataBind();

                        rptElectronicDownloadOrderItems.DataSource = orderItems;
                        rptElectronicDownloadOrderItems.DataBind();

                        pnlOrderItems.Visible = true;
                        plElectronicDeliveryApprovalNote.Visible = requiresApproval && order.OrderStatus == "Pending MasterCard Approval";
                        plElectronicDeliveryInfo.Visible = !plElectronicDeliveryApprovalNote.Visible;
                    }
                }
            }
        }

        protected void rptDownloadLinks_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                RetrieveElectronicDeliveryOrderItemsByOrderIdResult order = (RetrieveElectronicDeliveryOrderItemsByOrderIdResult)e.Item.DataItem;
                HyperLink lnkDownload = (HyperLink)e.Item.FindControl("lnkDownload");
                Literal litRemainingDownload = (Literal)e.Item.FindControl("litRemainingDownload");

                lnkDownload.Text = lnkDownload.NavigateUrl = $"{RegionalizeService.GetBaseUrl(DependencyResolver.Current.GetService<UserContext>().SelectedRegion).TrimEnd('/')}/{WebConfigurationManager.AppSettings["ElectronicDelivery.DownloadUrlFormat"].TrimStart('/').Replace("{key}", order.ElectronicDeliveryID.ToString()).Replace("{sku}", Regex.Replace(order.SKU.Replace(" ", ""), @"[^\w\-\+\~]", ""))}";
                litRemainingDownload.Text = order.DownloadCount.ToString();

                if (HttpContext.Current.User.IsInRole(Constants.Roles.McAdmin) || HttpContext.Current.User.IsInRole(Constants.Roles.DevAdmin))
                {
                    HtmlGenericControl spanResetDownload = (HtmlGenericControl)e.Item.FindControl("spanResetDownload");
                    Button btnResetDownload = (Button)e.Item.FindControl("btnResetDownload");
                    btnResetDownload.CommandArgument = order.ElectronicDeliveryID.ToString();
                    spanResetDownload.Visible = true;
                }
            }
        }

        protected void btnResetDownload_Click(object sender, EventArgs e)
        {
            int maxDownload = Convert.ToInt32(WebConfigurationManager.AppSettings["ElectronicDelivery.MaxDownload"]);
            if (maxDownload > 0)
            {
                Button btnResetDownload = (Button)sender;
                MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();
                var ed = dataContext.ElectronicDeliveries.SingleOrDefault(i => i.ElectronicDeliveryID.ToString() == btnResetDownload.CommandArgument);
                ed.DownloadCount = maxDownload;
                ed.ExpirationDate = DateTime.Now.AddDays(Convert.ToInt32(WebConfigurationManager.AppSettings["ElectronicDelivery.ExpirationDays"]));
                dataContext.SubmitChanges();

                litExpirationDate.Text = ed.ExpirationDate.ToString("MM/dd/yy");

                List<RetrieveElectronicDeliveryOrderItemsByOrderIdResult> orderItems = dataContext.RetrieveElectronicDeliveryOrderItemsByOrderId(OrderId).ToList();
                if (orderItems.Count > 0)
                {
                    rptDownloadLinks.DataSource = orderItems;
                    rptDownloadLinks.DataBind();
                }
            }
        }
    }
}