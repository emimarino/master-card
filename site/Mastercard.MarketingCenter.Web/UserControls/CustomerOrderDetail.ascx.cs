﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.UserControls
{
    public partial class CustomerOrderDetail : System.Web.UI.UserControl
    {
        decimal total = 0;
        decimal _totalCost = 0;
        string orderStatus = string.Empty;

        public IUserService _userService { get { return DependencyResolver.Current.GetService<IUserService>(); } }
        public IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }
        public IEmailService _emailService { get { return DependencyResolver.Current.GetService<IEmailService>(); } }
        public UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
        public IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var orderID = string.Empty;
                if (Page.RouteData.Values["orderid"] != null)
                {
                    orderID = _orderService.GetOrderIDByOrderNo(Page.RouteData.Values["orderid"].ToString()).ToString();
                }
                else if (HttpContext.Current.Items["orderid"] != null)
                {
                    orderID = _orderService.GetOrderIDByOrderNo(HttpContext.Current.Items["orderid"].ToString()).ToString();
                }
                else if (Request.QueryString["orderID"] != null)
                {
                    orderID = _orderService.GetOrderIDByOrderNo(Request.QueryString["orderID"].ToString()).ToString();
                }

                if (Request.QueryString["Source"] != null)
                {
                    backToQueue.Visible = true;
                    backToQueue.HRef = Request.QueryString["Source"].ToString();
                }
                else
                {
                    backToQueue.Visible = false;
                }

                if (!string.IsNullOrEmpty(orderID) && orderID != "0")
                {
                    ViewState["OrderId"] = orderID;

                    divcostadjustment.Visible = false;
                    lblpriceadjustment.Visible = false;
                    lblDescription.Visible = false;
                    plcUserOptions.Visible = false;
                    Approvaldiv.Visible = false;
                    plcPrinter.Visible = false;
                    plcUser.Visible = false;
                    int Orderid = int.Parse(orderID);
                    RetrieveOrderDetails(Orderid);
                    RetrieveShippingInformation(Orderid);
                    RetrieveCustomisationDetails(Orderid);
                    RetrieveOrderLog(Orderid);
                    RetrieveOrderItems(Orderid);

                    electronicFullfilment.OrderId = Convert.ToInt32(orderID);
                    vdpFullfilment.OrderId = Convert.ToInt32(orderID);

                    var order = _orderService.GetOrderById(Orderid);
                    if (order.BillingID.HasValue)
                    {
                        litBillingICA.Text = order.BillingID.Value.ToString();
                    }
                    else
                    {
                        phBillingICA.Visible = false;
                    }
                }
                else
                {
                    Response.Redirect("/portal/error/invalidorder");
                }
            }
        }

        protected string FormatNumber(decimal num)
        {
            System.Globalization.NumberFormatInfo nf = new System.Globalization.NumberFormatInfo();
            nf.CurrencySymbol = "$";
            nf.NumberDecimalDigits = 2;
            return num.ToString("C", nf);
        }

        protected bool IsFileUploaded(object o)
        {
            bool breturn = false;
            if (o != null && !string.IsNullOrEmpty(o.ToString()))
            {
                breturn = true;
            }
            else if (Request.QueryString["orderID"] == null)
            {
                breturn = true;
            }

            return breturn;
        }

        protected string TruncateFile(object o)
        {
            string sreturn = string.Empty;
            if (o != null && !string.IsNullOrEmpty(o.ToString()) && o.ToString().Length > 0)
            {
                string[] filevalues = o.ToString().Split(new string[] { @"/" }, StringSplitOptions.None);
                if (filevalues.Length > 0)
                {
                    sreturn = @"../" + filevalues[filevalues.Length - 1];
                    if (sreturn.Length > 21)
                    {
                        sreturn = sreturn.Insert(20, " ");
                    }
                }
            }

            return sreturn;
        }

        protected bool IsProofPdfExits(object o)
        {
            bool breturn = false;
            if (o != null && !string.IsNullOrEmpty(o.ToString()) && o.ToString().Length > 0)
            {
                breturn = true;
            }

            return breturn;
        }

        private bool _isPrinter = false;
        private bool _isPrinterSet = false;
        protected bool IsPrinter()
        {
            bool isPrinter = _isPrinter;
            if (!_isPrinterSet)
            {
                _isPrinter = Page.User.IsInRole(Constants.Roles.Printer) || Page.User.IsInRole(Constants.Roles.DevAdmin) || Page.User.IsInRole(Constants.Roles.McAdmin);
                isPrinter = _isPrinter;
                _isPrinterSet = true;
            }

            return isPrinter;
        }

        protected void RetrieveShippingInformation(int Orderid)
        {
            List<RetrieveShippingInformationResult> ShippingDetail = _orderService.GetShippingInformation(Orderid);
            if (ShippingDetail != null)
            {
                rptShippingAddresses.DataSource = ShippingDetail;
                rptShippingAddresses.DataBind();
            }

            RetrieveOrderDetailResult orderDetail = _orderService.GetOrderDetails(Orderid);
            orderStatus = orderDetail.OrderStatus.ToLower();

            if (!string.IsNullOrEmpty(orderDetail.SpecialInstructions))
            {
                divspecialinstructions.Visible = true;
                lblSpecialInstructions.Text = orderDetail.SpecialInstructions.Replace("\r\n", "<br/>");
            }
            else
            {
                divspecialinstructions.Visible = false;
            }

            if (orderDetail.RushOrder)
            {
                ltRushOrder.Text = "<br /><br />" + Resources.ShoppingCart.RushOrderRequested.ToUpperInvariant();
            }
        }

        protected void rptShippingAddresses_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null && (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                RetrieveShippingInformationResult shippingInfo = e.Item.DataItem as RetrieveShippingInformationResult;

                Repeater rptInnerSubOrders = e.Item.FindControl("rptInnerSubOrders") as Repeater;

                int orderId = Int32.Parse(ViewState["OrderId"].ToString());

                List<RetrieveOrderSubOrdersResult> subOrderItems = _orderService.GetSubOrders(orderId, shippingInfo.ShippingInformationID);
                if (subOrderItems.Count > 0)
                {
                    rptInnerSubOrders.DataSource = subOrderItems;
                    rptInnerSubOrders.DataBind();
                }
            }
        }

        protected void rptInnerSubOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null && (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                RetrieveOrderSubOrdersResult subOrder = e.Item.DataItem as RetrieveOrderSubOrdersResult;

                Repeater rptInnerOrderItems = e.Item.FindControl("rptInnerOrderItems") as Repeater;

                List<RetrieveSubOrderItemsResult> orderItems = _orderService.GetSubOrderItems(subOrder.SubOrderID);
                if (orderItems.Count > 0)
                {
                    rptInnerOrderItems.DataSource = orderItems;
                    rptInnerOrderItems.DataBind();
                }
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            HtmlLink css = new HtmlLink();
            if (Request.QueryString["orderID"] != null)
            {
                css.Href = "/_layouts/1033/STYLES/MMP.PortalStyle.css";
                css.Attributes["rel"] = "stylesheet";
                css.Attributes["type"] = "text/css";
                css.Attributes["media"] = "all";
                Page.Header.Controls.Add(css);
            }

            chkApprove.Attributes.Add("onclick", "validateCorrection('" + chkFeedback.ClientID + "','" + chkApprove.ClientID + "','" + txtFeedback.ClientID + "','" + btnSubmit.ClientID + "');");
            chkFeedback.Attributes.Add("onclick", "validateCorrection('" + chkFeedback.ClientID + "','" + chkApprove.ClientID + "','" + txtFeedback.ClientID + "','" + btnSubmit.ClientID + "');");

            rptItems.ItemCommand += new RepeaterCommandEventHandler(rptItems_ItemCommand);
            rptItems.ItemDataBound += new RepeaterItemEventHandler(rptItems_ItemDataBound);

            btnPrinterComplete.Click += new EventHandler(bnSubmit_Click);
            btnPrinterCancel.Click += new EventHandler(btnCancel_Click);
        }

        protected void rptItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null && (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                RetrieveOrderItemsResult orderItem = e.Item.DataItem as RetrieveOrderItemsResult;
                HyperLink fileLink = e.Item.FindControl("lnkProofFile") as HyperLink;
                if (orderItem.ProofPDF != null)
                {
                    fileLink.Visible = true;
                    fileLink.Text = orderItem.ProofPDF.Substring(orderItem.ProofPDF.LastIndexOf("/") + 1);
                    fileLink.NavigateUrl = _urlService.GetProofFileURL(orderItem.ProofPDF);
                }

                Image imgIcon = (Image)e.Item.FindControl("imgIcon");
                Literal litIcon = (Literal)e.Item.FindControl("litIcon");

                if (orderItem.ElectronicDelivery)
                {
                    imgIcon.ImageUrl = "/portal/inc/images/document_down.png";
                    litIcon.Text = Resources.ShoppingCart.ElectronicDelivery;
                }
                else
                {
                    if (orderItem.VDP)
                    {
                        imgIcon.ImageUrl = "/portal/inc/images/vdp.png";
                        litIcon.Text = Resources.ShoppingCart.VariableDataPrinting;
                    }
                    else
                    {
                        if (Convert.ToBoolean(orderItem.Customizations))
                        {
                            imgIcon.ImageUrl = "/portal/inc/images/document_edit.png";
                            litIcon.Text = Resources.ShoppingCart.Customizable;
                        }
                        else
                        {
                            imgIcon.ImageUrl = "/portal/inc/images/document_lock.png";
                            litIcon.Text = Resources.ShoppingCart.MastercardBrandedCollateral;
                        }
                    }
                }
            }
        }

        void btnCancel_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["Source"] != null)
            {
                Response.Redirect(Request.QueryString["Source"]);
            }
        }

        protected string[] FormatString(string ItemName)
        {
            int NoOFWords = 0;
            NoOFWords = WordCount(ItemName.ToString());
            string[] Items = new string[NoOFWords / 4 + 1];
            string New_Item = string.Empty;

            if (NoOFWords > 4)
            {
                string[] words = ItemName.ToString().Split(' ');

                for (int i = 0; i <= NoOFWords - 1; i++)
                {
                    if (i % 4 == 0 && i >= 4)
                    {
                        New_Item += System.Environment.NewLine;
                    }
                    else
                    {
                        if (i == 0)
                        {
                            New_Item += words[i].ToString();
                        }
                        else
                        {
                            New_Item += ' ' + words[i].ToString();
                        }
                    }
                }
                Items = New_Item.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            }
            else
            {
                Items.SetValue(ItemName, 0);
            }

            return Items;
        }

        public static int WordCount(string Text)
        {
            string tmpStr = Text.Replace("\t", " ").Trim();
            tmpStr = tmpStr.Replace("\n", " ");
            tmpStr = tmpStr.Replace("\r", " ");

            while (tmpStr.IndexOf("  ") != -1)
            {
                tmpStr = tmpStr.Replace("  ", " ");
            }

            return tmpStr.Split(' ').Length;
        }

        void rptItems_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName != null)
            {
                if (e.CommandName.ToLower() == "replace")
                {
                    PlaceHolder placeHolderEdit = (PlaceHolder)e.Item.FindControl("placeHolderEdit");
                    PlaceHolder placeholderFiledExits = (PlaceHolder)e.Item.FindControl("placeholderFiledExits");
                    placeholderFiledExits.Visible = false;
                    placeHolderEdit.Visible = true;
                }
            }
        }

        protected void RetrieveCustomisationDetails(int Orderid)
        {
            List<RetrieveCustomizationsResult> ls = _orderService.GetCustomizationOptions(Orderid);
            rptCustomizations.DataSource = ls;
            rptCustomizations.DataBind();

            placeCustomizationDetails.Visible = ls.Count > 0;
        }

        protected void RetrieveOrderLog(int OrderId)
        {
            List<RetrieveOrderLogResult> OrderItems = _orderService.GetOrderLog(OrderId);
            if (OrderItems != null)
            {
                rptOrderHistory.DataSource = OrderItems;
                rptOrderHistory.DataBind();
            }
        }

        protected void RetrieveOrderItems(int OrderId)
        {
            List<RetrieveOrderItemsResult> OrderItems = _orderService.GetOrderItems(OrderId);
            if (OrderItems != null)
            {
                rptItems.DataSource = OrderItems;
                rptItems.DataBind();

                var p = (from o in OrderItems select o.ItemPrice).Sum();

                _totalCost = _totalCost + Convert.ToDecimal(p);

                RetrievePromotionForOrderResult promotion = _orderService.GetPromotion(OrderId);
                if (promotion != null)
                {
                    litPromotionDescription.Text = promotion.PromotionDescription;
                    litPromotionAmount.Text = string.Format("$-{0}", Convert.ToDecimal(promotion.PromotionAmount).ToString("N2"));
                    pnlPromotion.Visible = true;

                    _totalCost -= Convert.ToDecimal(promotion.PromotionAmount);
                }

                litTotal.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "{0:C}", _totalCost);
            }
        }

        protected void RetrieveOrderDetails(int OrderId)
        {
            RetrieveOrderDetailResult OrderItems = _orderService.GetOrderDetails(OrderId);

            if (OrderItems != null)
            {
                if (Request.QueryString["orderID"] == null && !ValidUser(OrderItems.UserId.ToString()) && !IsUserIsPrinter())
                {
                    Response.Redirect("/portal/error/accesdenied");
                }

                ViewState["OrderNumber"] = OrderItems.OrderNumber.ToString();
                lblOrderNo.Text = Resources.ShoppingCart.Order + " #         " + OrderItems.OrderNumber.ToString();
                lblOrderDate.Text = Resources.Shared.Date + "          " + OrderItems.OrderDate.Value.ToString("MM/dd/yyyy");
                lblStatustitle.Text = OrderItems.OrderStatus.ToString().Replace("Shipped", "Shipped/Fulfilled");

                if (OrderItems.PriceAdjustment != null)
                {
                    lblpriceadjustment.Text = FormatNumber(Convert.ToDecimal(OrderItems.PriceAdjustment.Value.ToString()));
                    _totalCost = _totalCost + Convert.ToDecimal(OrderItems.PriceAdjustment.Value.ToString());
                }
                if (OrderItems.Description != null)
                {
                    lblDescription.Text = OrderItems.Description;
                }

                plcUserOptions.Visible = false;
                orderStatus = OrderItems.OrderStatus.ToLower();
                switch (orderStatus)
                {
                    case "pending customization":
                    case "pending correction":
                        if (Request.QueryString["orderID"] != null)
                        {
                            divcostadjustment.Visible = false;
                            plcUserOptions.Visible = false;
                            Approvaldiv.Visible = true;
                            plcPrinter.Visible = true;
                            plcUser.Visible = false;
                        }
                        else
                        {
                            Approvaldiv.Visible = false;
                            plcPrinter.Visible = false;
                            plcUser.Visible = false;
                        }
                        break;
                    case "shipped":
                        divcostadjustment.Visible = !String.IsNullOrEmpty(lblpriceadjustment.Text);
                        plcUserOptions.Visible = false;
                        Approvaldiv.Visible = false;
                        lblpriceadjustment.Visible = true;
                        lblDescription.Visible = true;
                        plcPrinter.Visible = false;
                        plcUser.Visible = false;
                        break;
                    case "fulfillment":
                        if (Request.QueryString["orderID"] != null)
                        {
                            divcostadjustment.Visible = true;
                            lblpriceadjustment.Visible = false;
                            lblDescription.Visible = false;
                            plcUserOptions.Visible = false;
                            Approvaldiv.Visible = false;
                            plcPrinter.Visible = false;
                            plcUser.Visible = false;
                        }
                        else
                        {
                            divcostadjustment.Visible = false;
                            plcUserOptions.Visible = false;
                            Approvaldiv.Visible = false;
                            lblpriceadjustment.Visible = true;
                            lblDescription.Visible = true;
                            Approvaldiv.Visible = false;
                            plcPrinter.Visible = false;
                            plcUser.Visible = false;
                        }
                        break;
                    case "pending approval":
                        if (Request.QueryString["orderID"] == null)
                        {
                            if (ValidUser(OrderItems.UserId.ToLower()))
                            {
                                divcostadjustment.Visible = false;
                                lblpriceadjustment.Visible = false;
                                lblDescription.Visible = false;
                                Approvaldiv.Visible = true;
                                plcPrinter.Visible = false;
                                plcUser.Visible = true;
                                plcUserOptions.Visible = true;
                            }
                            else if (IsUserIsPrinter())
                            {
                                divcostadjustment.Visible = false;
                                lblpriceadjustment.Visible = false;
                                lblDescription.Visible = false;
                                Approvaldiv.Visible = false;
                                plcPrinter.Visible = false;
                                plcUser.Visible = false;
                                plcUserOptions.Visible = false;
                            }
                        }
                        else
                        {
                            divcostadjustment.Visible = false;
                            lblpriceadjustment.Visible = false;
                            lblDescription.Visible = false;
                            Approvaldiv.Visible = false;
                            plcPrinter.Visible = false;
                            plcUser.Visible = false;
                            plcUserOptions.Visible = false;
                        }
                        break;
                }

                btnPrinterComplete.Enabled = false;
            }
        }

        private bool ValidUser(string userId)
        {
            bool breturn = false;
            if (!userId.IsNullOrWhiteSpace() &&
                userId.Equals(_userContext.User.UserName, StringComparison.InvariantCultureIgnoreCase) ||
                Page.User.IsInRole(Constants.Roles.McAdmin) ||
                Page.User.IsInRole(Constants.Roles.DevAdmin))
            {
                breturn = true;
            }

            return breturn;
        }

        private bool IsUserIsPrinter()
        {
            bool breturn = false;
            if (Page.User.IsInRole(Constants.Roles.Printer))
            {
                breturn = true;
            }

            return breturn;
        }

        private bool UploadFile(int OrderID, string ItemID, FileUpload fupload)
        {
            bool breturn = false;
            if (fupload.Value != null && fupload.Value.Trim().Length > 0)
            {
                _orderService.UpdateItem(OrderID, ItemID, fupload.Value);
                breturn = true;
            }

            return breturn;
        }

        protected string ParseLogUser(string UserId)
        {
            string sreturn = UserId;
            try
            {
                var user = _userService.GetUserByUserName(UserId);
                if (user != null)
                {
                    sreturn = user.FullName;
                }
            }
            catch { }

            return sreturn;
        }

        ///Fires when both types (printer,normal user,mcadmin) user click on submit button   
        protected void bnSubmit_Click(object sender, EventArgs e)
        {
            bool bretun = false;
            bool isPrinter = false;
            string OrderNO = string.Empty;

            //get order Number if the user is from the portal           
            if (Page.RouteData.Values["orderid"] != null)
                OrderNO = Page.RouteData.Values["orderid"].ToString().ToString();

            //get order Number if the user is from the printer side
            if (Request.QueryString["orderID"] != null)
            {
                isPrinter = true;
                OrderNO = Request.QueryString["orderID"];
            }

            if (string.IsNullOrEmpty(OrderNO))
                return;

            //get orderid  from orderNo.
            int Order = _orderService.GetOrderIDByOrderNo(OrderNO);

            //save the orderItem proof Pdf's 

            //if user is printer and has items to upload
            if (rptItems.Items.Count > 0 && isPrinter)
            {
                foreach (RepeaterItem item in rptItems.Items)
                {
                    var fupload = item.FindControl("UploadProofFile") as Mastercard.MarketingCenter.Web.UserControls.FileUpload;
                    Label lblItemId = (Label)item.FindControl("lblItemID");
                    if (fupload != null && lblItemId.Text.Length > 0)
                    {
                        bretun = UploadFile(Order, lblItemId.Text, fupload);
                    }
                    else
                    {
                        bretun = false;
                    }
                }
            }

            string strHostName = System.Net.Dns.GetHostName();
            string clientIPAddress = System.Net.Dns.GetHostAddresses(strHostName).GetValue(0).ToString();

            RetrieveOrderDetailResult orderDetail = _orderService.GetOrderDetails(Order);
            var user = _userService.GetUserByUserName(orderDetail.UserId);

            // enter data into orderProcessNote and the update order 
            switch (lblStatustitle.Text.ToLower())
            {
                case "pending customization":
                    if (orderDetail.OrderStatus.ToLower() != "pending approval")
                    {
                        if (Request.QueryString["orderID"] != null)
                        {
                            _orderService.SetOrderStatus(Order, "Pending Approval", "Proof Ready<br/>" + txtFeedback.Text.Trim(), clientIPAddress, _userContext.User.UserName);

                            _emailService.SendProofEmail(OrderNO, user.Email, user.Profile.FirstName, user.Profile.LastName, user.UserId);

                            Response.Redirect(Request.Url.AbsoluteUri);
                            return;
                        }
                    }
                    break;
                case "pending correction":
                    if (_orderService.GetOrderStatus(OrderNO).ToLower() != "pending approval")
                    {
                        if (Request.QueryString["orderID"] != null)
                        {
                            _orderService.SetOrderStatus(Order, "Pending Approval", "Proof Ready Again<br/>" + txtFeedback.Text.Trim(), clientIPAddress, _userContext.User.UserName);

                            _emailService.SendProofEmail(OrderNO, user.Email, user.Profile.FirstName, user.Profile.LastName, user.UserId);
                            Response.Redirect(Request.Url.AbsoluteUri);
                            return;
                        }
                    }
                    break;
                case "fulfillment":
                    break;
                case "pending approval":
                    if (chkApprove.Checked)
                    {
                        if (_orderService.GetOrderStatus(OrderNO).ToLower() != "fulfillment")
                        {
                            _emailService.SendProofApprovedEmail(OrderNO, user.UserId);

                            _orderService.SetOrderStatus(Order, "Fulfillment", "Proof Approved<br/>" + txtFeedback.Text.Trim(), clientIPAddress, _userContext.User.UserName);
                        }
                    }
                    else
                    {
                        if (_orderService.GetOrderStatus(OrderNO).ToLower() != "pending correction")
                        {
                            _emailService.SendPendingCorrectionEmail(OrderNO, user.UserId);

                            _orderService.SetOrderStatus(Order, "Pending Correction", txtFeedback.Text.Trim(), clientIPAddress, _userContext.User.UserName);
                        }
                    }
                    break;
            }

            Response.Redirect(Request.Url.AbsoluteUri);
        }

        protected string GetPrice(string price)
        {
            string sretun = string.Empty;
            if (price.Length > 0)
            {
                decimal dprice = decimal.Parse(price);
                total = total + dprice;
                sretun = FormatNumber(dprice);
            }

            return sretun;
        }

        protected TextBoxMode GetTextBoxMode(string FieldType)
        {
            TextBoxMode treturn = TextBoxMode.SingleLine;
            switch (FieldType.ToLower())
            {
                case "multi line text":
                    treturn = TextBoxMode.MultiLine;
                    break;
                default:
                    break;
            }

            return treturn;
        }
    }
}