﻿using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Web.Mvc;
using System.Web.UI.HtmlControls;

namespace Mastercard.MarketingCenter.Web.UserControls
{
    public partial class CompleteVdpOrder : System.Web.UI.UserControl
    {
        IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }
        IEmailService _emailService { get { return DependencyResolver.Current.GetService<IEmailService>(); } }
        IUserService _userService { get { return DependencyResolver.Current.GetService<IUserService>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            HtmlLink css = new HtmlLink();
            css.Href = Slam.Cms.Configuration.ConfigurationManager.Environment.AdminUrl.TrimEnd('/') + "/_layouts/1033/STYLES/MarketingCenter.SharedControls.css";
            css.Attributes["rel"] = "stylesheet";
            css.Attributes["type"] = "text/css";
            css.Attributes["media"] = "all";
            Page.Header.Controls.Add(css);

            if (!string.IsNullOrEmpty(Request.QueryString["OrderId"]))
            {
                txtPriceAdjustment.Attributes.Add("onkeydown", "return ForceNumericInput(event,true,true);");
                txtPostageCost.Attributes.Add("onkeydown", "return ForceNumericInput(event,true,true);");

                int orderId = Convert.ToInt32(Request.QueryString["OrderId"]);
                VdpOrderInformation vdpInfo = _orderService.GetVdpOrderInformation(orderId);

                if (vdpInfo != null)
                {
                    txtDescription.Text = vdpInfo.Description;
                    txtPriceAdjustment.Text = vdpInfo.PriceAdjustment.ToString();
                    txtPostageCost.Text = vdpInfo.PostageCost.ToString();
                }

                vdpFullfilmentItem.OrderId = orderId;
            }
        }

        protected void btnComplete_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["OrderId"]))
            {
                if (!string.IsNullOrEmpty(txtPostageCost.Text))
                {
                    if (decimal.TryParse(txtPostageCost.Text, out decimal postageCost))
                    {
                        decimal priceAdjustment = 0;
                        if (string.IsNullOrEmpty(txtPriceAdjustment.Text) || decimal.TryParse(txtPriceAdjustment.Text, out priceAdjustment))
                        {
                            int orderId = Convert.ToInt32(Request.QueryString["OrderId"]);

                            var order = _orderService.GetOrderById(orderId);
                            if (order != null)
                            {
                                _orderService.SaveVdpOrderInformation(orderId, txtDescription.Text, priceAdjustment, postageCost);

                                var user = _userService.GetUserByUserName(order.UserID);
                                if (user != null)
                                {
                                    _emailService.SendVdpOrderCompletedEmail(user.Email, order.OrderNumber, user.UserId);
                                }

                                string referrer = Request.QueryString["Source"] != null ? "&Source=" + Request.QueryString["Source"] : "";
                                Response.Redirect("OrderDetailNoChrome.aspx?orderid=" + order.OrderNumber + referrer);
                            }
                        }
                        else
                        {
                            litError.Text = Resources.Errors.PriceAdjustmentAmountInvalid;
                            litError.Visible = true;
                        }
                    }
                    else
                    {
                        litError.Text = Resources.Errors.PostageCostAmountInvalid;
                        litError.Visible = true;
                    }
                }
                else
                {
                    litError.Text = Resources.Errors.PostageCostRequired;
                    litError.Visible = true;
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["OrderId"]))
            {
                int orderId = Convert.ToInt32(Request.QueryString["OrderId"]);
                string orderNumber = _orderService.GetOrderNumberByOrderId(orderId);

                string referrer = Request.QueryString["Source"] != null ? "&Source=" + Request.QueryString["Source"] : "";
                Response.Redirect("OrderDetailNoChrome.aspx?orderid=" + orderNumber + referrer);
            }
        }
    }
}