﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_Paginator" CodeBehind="Paginator.ascx.cs" %>

<asp:Panel runat="server" ID="AllPaginatorPanel" CssClass="pagination">
    <asp:LinkButton ID="Previous" runat="server" Text="<%$Resources:Shared,Previous%>" OnClick="Previous_Click" CssClass="arrow arrow-prev" />
    <asp:Panel ID="PagesPanel" runat="server" CssClass="page-num"></asp:Panel>
    <asp:LinkButton ID="Next" runat="server" Text="<%$Resources:Shared,Next%>" OnClick="Next_Click" CssClass="arrow arrow-next" />
</asp:Panel>