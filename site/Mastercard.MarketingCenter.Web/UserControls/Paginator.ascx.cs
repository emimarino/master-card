﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_Paginator : System.Web.UI.UserControl
{
    public event EventHandler CurrentPageUpdated;
    private const int INITIAL_PAGES = 5;
    private const int PAGES_TO_CENTER = 2;
    private const int LAST_PAGES = 2;

    public int Pages
    {
        get
        {
            if (ViewState["Pages"] == null)
                return 0;

            return int.Parse(ViewState["Pages"].ToString());
        }
        set
        {
            ViewState["Pages"] = value;
        }
    }

    public int CurrentPage
    {
        get
        {
            if (ViewState["CurrentPage"] == null)
                return 1;

            return int.Parse(ViewState["CurrentPage"].ToString());
        }
        set
        {
            ViewState["CurrentPage"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            CurrentPage = 1;
    }

    protected void Previous_Click(object sender, EventArgs e)
    {
        CurrentPage--;

        if (CurrentPageUpdated != null)
            CurrentPageUpdated(this, EventArgs.Empty);
    }

    protected void Next_Click(object sender, EventArgs e)
    {
        CurrentPage++;

        if (CurrentPageUpdated != null)
            CurrentPageUpdated(this, EventArgs.Empty);
    }

    protected void PageLink_Click(object sender, EventArgs e)
    {
        var link = (LinkButton)sender;

        CurrentPage = int.Parse(link.Text);

        if (CurrentPageUpdated != null)
            CurrentPageUpdated(this, EventArgs.Empty);
    }

    public void DefineRenderingConditions()
    {
        if (Pages > 1)
        {
            AllPaginatorPanel.Visible = true;

            Previous.CssClass = "arrow arrow-prev";
            Next.CssClass = "arrow arrow-next";

            Previous.Enabled = true;
            Next.Enabled = true;

            if (CurrentPage == 1)
            {
                Previous.Enabled = false;
                Previous.CssClass = "arrow arrow-prev disabled";
            }

            if (CurrentPage == Pages)
            {
                Next.Enabled = false;
                Next.CssClass = "arrow arrow-next disabled";
            }

            PagesPanel.Controls.Clear();

            RenderInitialPages();
            RenderLastPages();

            PagesPanel.Visible = Pages != 1;
            Next.Visible = Pages != 1;
            Previous.Visible = Pages != 1;
        }
        else
            AllPaginatorPanel.Visible = false;
    }

    private void RenderLastPages()
    {
        if (Pages > INITIAL_PAGES)
        {
            //if ((INITIAL_PAGES <= (Pages + LAST_PAGES) && 
            //    CurrentPage + PAGES_TO_CENTER < Pages - LAST_PAGES + 1
            //    && !(CurrentPage + PAGES_TO_CENTER + LAST_PAGES == Pages))
            //    && (Pages > INITIAL_PAGES + LAST_PAGES))
            if ((CurrentPage + PAGES_TO_CENTER < Pages - LAST_PAGES + 1)
                && !(CurrentPage + PAGES_TO_CENTER + LAST_PAGES == Pages)
                && (Pages > INITIAL_PAGES + LAST_PAGES))
            {
                Label separator = new Label();
                separator.ID = "Separator";
                separator.Text = "...";
                separator.CssClass = "pages-sep";

                PagesPanel.Controls.Add(separator);
            }

            if (CurrentPage + PAGES_TO_CENTER <= Pages - LAST_PAGES + 1)
            {
                int initialPage = Math.Max(Math.Max(Pages - LAST_PAGES + 1, CurrentPage + PAGES_TO_CENTER + LAST_PAGES - 1), 1 + (PAGES_TO_CENTER * 2) + 1);

                int lastPage = Pages;

                DrawPageButtons(initialPage, lastPage);
            }
        }
    }

    private void RenderInitialPages()
    {
        int initialPage = Math.Max(CurrentPage - PAGES_TO_CENTER, 1);
        int lastPage = 0;

        if (INITIAL_PAGES > Pages)
            lastPage = Pages;
        else
        {
            if (CurrentPage <= PAGES_TO_CENTER)
                lastPage = INITIAL_PAGES;
            else
                lastPage = CurrentPage + PAGES_TO_CENTER;
        }

        lastPage = Math.Min(lastPage, Pages);

        if (CurrentPage + PAGES_TO_CENTER > Pages)
            initialPage = CurrentPage - INITIAL_PAGES + 1 - (CurrentPage - lastPage);

        initialPage = Math.Max(initialPage, 1);

        DrawPageButtons(initialPage, lastPage);
    }

    private void DrawPageButtons(int initialPage, int lastPage)
    {
        while (initialPage <= lastPage)
        {
            if (initialPage != CurrentPage)
            {
                LinkButton link = new LinkButton();
                link.ID = "Page_" + initialPage.ToString();
                link.Text = initialPage.ToString();
                link.Click += new EventHandler(PageLink_Click);
                link.CssClass = "page-num";

                PagesPanel.Controls.Add(link);
            }
            else
            {
                Label link = new Label();
                link.ID = "Page_" + initialPage.ToString();
                link.Text = initialPage.ToString();
                link.CssClass = "page-num current";

                PagesPanel.Controls.Add(link);
            }

            initialPage++;
        }
    }
}