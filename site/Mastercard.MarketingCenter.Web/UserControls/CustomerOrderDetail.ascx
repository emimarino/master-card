<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="CustomerOrderDetail.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.UserControls.CustomerOrderDetail" %>
<%@ Register Src="FileUpload.ascx" TagName="FileUpload" TagPrefix="uc1" %>
<%@ Register TagPrefix="mc" TagName="VdpFullfilment" Src="VdpFullfilmentItem.ascx" %>
<%@ Register TagPrefix="mc" TagName="ElectronicFullfilment" Src="ElectronicFullfilmentItem.ascx" %>

<script src="<%= DependencyResolver.Current.GetService<Mastercard.MarketingCenter.Services.Interfaces.IUrlService>().GetStaticCommonFunctionJs() %>"></script>
<div class="center_container">
    <div class="form_container">
        <div class="orange_heading_profile">
            <%=Resources.ShoppingCart.OrderDetails%>
        </div>
        <div class="order_conformation">
            <div class="order_detail_right">
                <a href="#" runat="server" id="backToQueue" class="blue_linked" title="<%=Resources.ShoppingCart.GoToQueue%>">&lt; <%=Resources.ShoppingCart.GoToQueue%></a>
                <span class="red_btn">
                    <asp:Label ID="lblStatustitle" runat="server"></asp:Label></span>
            </div>
            <div class="order_detail">
                <span>
                    <asp:Label ID="lblOrderNo" runat="server"></asp:Label></span>
                <span>
                    <asp:Label ID="lblOrderDate" runat="server"></asp:Label></span>
            </div>
        </div>
        <div class="clr">
        </div>
        <div class="faq_blog">
            <div class="roundcont_error mrb_25" id="divError" runat="server" visible="False">
                <div class="roundtop">
                    <img src="/portal/inc/images/tl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
                </div>
                <div style="float: left; width: 80px; text-align: center;">
                    <img src="/portal/inc/images/error_img.gif" border="0px" alt="" width="40" height="40" />
                </div>
                <div style="float: left; width: 800px; padding-top: 10px;">
                    <asp:Label ID="lblError" CssClass="span_1" runat="server" Visible="False"></asp:Label>
                </div>
                <div style="clear: both;"></div>
                <div class="roundbottom">
                    <img src="/portal/inc/images/bl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
                </div>
            </div>
            <div class="main_heading">
                <span class="order_heading width110 pdl_15px pdr_15px"><%=Resources.ShoppingCart.Sku.ToUpperInvariant()%></span>
                <span class="order_heading width150"><%=Resources.ShoppingCart.Title%></span>
                <span class="order_heading padding-grid text_right"><%=Resources.ShoppingCart.Quantity%></span>
                <span class="order_heading text_right"><%=Resources.ShoppingCart.Price%></span>
                <span class="order_heading pd_left20 width150"><%=Resources.ShoppingCart.ProofPdf%></span>
            </div>
            <div class="main_heading_bg" id="divPooofGrid">
                <div id="divItemgrid" runat="server">
                    <asp:Repeater runat="server" ID="rptItems" OnItemDataBound="rptItems_ItemDataBound">
                        <ItemTemplate>
                            <div class="chk_outgrid">
                                <span class="order_heading_data width110 pdl_15px pdr_15px word_wrap"><%#Eval("SKU")%></span>
                                <span class="order_heading_data width150"><%#Eval("ItemName")%></span>
                                <span class="order_heading_data padding-grid text_right"><%#Eval("ItemQuantity")%></span>
                                <span class="order_heading_data pdleft5 rightwidth"><%# GetPrice(Eval("ItemPrice").ToString())%>&nbsp;</span>
                                <asp:Label runat="server" ID="lblItemID" Visible="false" Text='<%# Eval("ItemID").ToString()%>'></asp:Label>
                                <span class="order_heading_data_upload pd_left20 width150">
                                    <asp:PlaceHolder ID="placeImageNoCustomization" runat="server" Visible='<%# Eval("Customizations").ToString() == "False" %>'>
                                        <asp:Image ID="imgIcon" runat="server" />
                                        <asp:Literal ID="litIcon" runat="server"></asp:Literal>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="placeImageUpload" runat="server" Visible='<%# Eval("Customizations").ToString() == "True" %>'>
                                        <asp:Literal ID="ltUploadMessage" Visible='<%# Eval("ProofPDF") == null && !IsPrinter() %>' runat="server" Text="<%$Resources:ShoppingCart,CustomizedProofReviewDescription%>"></asp:Literal>
                                        <uc1:FileUpload ID="UploadProofFile" Visible="false" runat="server" Value='<%# ((Eval("ProofPDF")!=null) ?  Eval("ProofPDF").ToString() : "") %>' />
                                        <asp:HyperLink ID="lnkProofFile" runat="server"></asp:HyperLink>
                                    </asp:PlaceHolder>
                                </span>
                            </div>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <div class="chk_outgrid gray_backgrnd">
                                <span class="order_heading_data width110 pdl_15px pdr_15px word_wrap"><%#Eval("SKU")%></span>
                                <span class="order_heading_data width150"><%#Eval("ItemName")%></span>
                                <span class="order_heading_data padding-grid text_right"><%#Eval("ItemQuantity")%></span>
                                <span class="order_heading_data pdleft5 rightwidth"><%# GetPrice(Eval("ItemPrice").ToString())%>&nbsp;</span>
                                <asp:Label runat="server" ID="lblItemID" Visible="false" Text='<%# Eval("ItemID").ToString()%>'></asp:Label>
                                <span class="order_heading_data_upload pd_left20 width150">
                                    <asp:PlaceHolder ID="placeImageNoCustomization" runat="server" Visible='<%# Eval("Customizations").ToString() == "False" %>'>
                                        <asp:Image ID="imgIcon" runat="server" />
                                        <asp:Literal ID="litIcon" runat="server"></asp:Literal>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="placeImageUpload" runat="server" Visible='<%# Eval("Customizations").ToString() == "True" %>'>
                                        <asp:Literal ID="ltUploadMessage" Visible='<%# Eval("ProofPDF") == null && !IsPrinter() %>' runat="server" Text="<%$Resources:ShoppingCart,CustomizedProofReviewDescription%>"></asp:Literal>
                                        <uc1:FileUpload ID="UploadProofFile" Visible="false" runat="server" Value='<%# ((Eval("ProofPDF")!=null) ?  Eval("ProofPDF").ToString() : "") %>' />
                                        <asp:HyperLink ID="lnkProofFile" runat="server" Visible="false"></asp:HyperLink>
                                    </asp:PlaceHolder>
                                </span>
                            </div>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </div>
                <asp:Panel ID="pnlPromotion" class="main_heading_bg promotion" runat="server" Visible="false">
                    <asp:Panel ID="pnlPromotionApplied" runat="server" CssClass="chk_outgrid">
                        <div class="promo-description">
                            <asp:Literal ID="litPromotionDescription" runat="server"></asp:Literal>
                        </div>
                        <div class="promo-amount">
                            <asp:Literal ID="litPromotionAmount" runat="server"></asp:Literal>
                        </div>
                    </asp:Panel>
                </asp:Panel>
                <div class="clear"></div>
                <div style="border-top: 1px solid #aaa; margin-left: 15px; margin-right: 15px; width: 648px; margin-top: 20px; padding-top: 10px;">
                    <asp:PlaceHolder ID="phBillingICA" runat="server">
                        <div class="billing_ica">
                            <%=Resources.ShoppingCart.BillingIca%>:&nbsp;<asp:Literal ID="litBillingICA" runat="server"></asp:Literal>
                        </div>
                    </asp:PlaceHolder>
                    <div style="float: right; width: 155px; margin-bottom: 20px; font-weight: bold; font-size: 12px;">
                        <%=Resources.ShoppingCart.Total%>:&nbsp;<asp:Literal ID="litTotal" runat="server"></asp:Literal>
                    </div>
                </div>
                <div class="clear"></div>
                <div id="Approvaldiv" runat="server">
                    <asp:PlaceHolder runat="server" ID="plcPrinter">
                        <div class="fl" style="width: 400px; float: right;" align="right">
                            <span class="bg_btn_left mr11 fr">
                                <asp:Button ID="btnPrinterCancel" BorderWidth="0px" CssClass="submit_bg_btn " runat="server" Text="<%$Resources:Shared,Cancel%>" /></span>
                            <span class="bg_grey_left fr mr11" runat="server" id="spnUpdateButton">
                                <asp:Button ID="btnPrinterComplete" BorderWidth="0px" CssClass="grey_bg_btn" runat="server" Enabled="False" Text="<%$Resources:Shared,Complete%>" />
                            </span>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="plcUser">
                        <div style="width: 75px;" class="bg_grey_left fr">
                            <asp:Button ID="btnSubmit" BorderWidth="0px" CssClass="grey_bg_btn" Enabled="False" runat="server" Text="<%$Resources:Shared,Submit%>" OnClick="bnSubmit_Click" />
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="plcUserOptions" runat="server" Visible="False">
                        <div class="fl order_radio_textaria" style="width: 550px; margin-left: 15px;">
                            <asp:RadioButton ID="chkApprove" GroupName="UserOption" runat="server" Text="<%$Resources:ShoppingCart,ApproveAllItems%>" />
                            <span class="fl">
                                <asp:RadioButton ID="chkFeedback" GroupName="UserOption" runat="server" Text="<%$Resources:ShoppingCart,ProvideFeedbackCorrections%>" />
                            </span>
                        </div>
                    </asp:PlaceHolder>
                    <div class="heading_span_new mrt_10">
                        <%=Resources.ShoppingCart.Notes%>:
                    </div>
                    <div>
                        <asp:TextBox ID="txtFeedback" TextMode="MultiLine" CssClass="nav_textarea_nav mr-top15" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div id="divcostadjustment" runat="server" visible="False">
                    <div class="heading_span_new mrt_10">
                        <%=Resources.ShoppingCart.OrderLevelPriceAdjustment%>
                    </div>
                    <div class="chk_outgrid ">
                        <div class="data_lost_right">
                            <span class="fr mr11">
                                <asp:Label ID="lblpriceadjustment" Visible="False" runat="server" CssClass="label pricedetails"></asp:Label>
                            </span>
                        </div>
                        <div class="data_rtf_left">
                            <asp:Label ID="lblDescription" CssClass="email" Visible="False" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="chk_outgrid" id="divorderhistory" runat="server">
                    <div class="heading_span_new mrt_10">
                        <%=Resources.ShoppingCart.OrderProgressHistory%>
                    </div>
                    <div class="grid_rows_order">
                        <asp:Repeater runat="server" ID="rptOrderHistory">
                            <ItemTemplate>
                                <div class="grid_detail">
                                    <span class="order_heading_data width150 "><%# (Eval("Date") != null) ? Convert.ToDateTime(Eval("Date").ToString()).ToString("MM/dd/yyyy") : ""%>&nbsp;</span>
                                    <span class="order_heading_data" style="width: 200px !important;"><%# ((Eval("Approval")!=null ? ParseLogUser(Eval("Approval").ToString()) : "")) %>&nbsp;</span>
                                    <span class="order_heading_data width150" style="width: 300px !important;"><%#Eval("Note")%>&nbsp;</span>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
            <div class="meaning_bot"></div>
        </div>
        <div class="clr"></div>
        <br />
        <div class="faq_blog">
            <!--Customisations-->
            <asp:PlaceHolder ID="placeCustomizationDetails" runat="server">
                <div class="main_heading">
                    <div class="orange_heading_main">
                        <%=Resources.ShoppingCart.CustomizationDetails%>
                    </div>
                </div>
                <div class="main_heading_bg">
                    <asp:Repeater runat="server" ID="rptCustomizations">
                        <ItemTemplate>
                            <div class="chk_outgrid">
                                <div class="custmize_span">
                                    <%# Eval("FriendlyName")%><%# ((Eval("Required") != null && Eval("Required").ToString().ToLower() == "true") ? "*" : "")%>
                                </div>
                                <div class="right_grid_custmize">
                                    <asp:PlaceHolder ID="PlaceHolderimg" runat="server" Visible='<%#(Eval("FieldType").ToString().ToLower() == "image")?true : false %>'>
                                        <img id="image_cust" runat="server" height="128" width="131" src='<%# ((Eval("DefaultCustomizationData")!=null)) ? _urlService.GetStaticImageURL(Eval("DefaultCustomizationData").ToString(),200,200,false,85,true) : "" %>' alt="" />
                                        <a runat="server" href='<%# ((Eval("DefaultCustomizationData")!=null)) ? _urlService.GetStaticImageURL(Eval("DefaultCustomizationData").ToString(),rootPath:true) : "" %>' visible="<%# IsPrinter() %>" class="price" target="new"><%=Resources.Shared.Download%></a>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="PlaceHoldertxt" runat="server" Visible='<%#(Eval("FieldType").ToString().ToLower() == "image")?false : true %>'>
                                        <asp:Label runat="server" ID="txtOptionsValue" CssClass="order_heading_data width_auto text_align_justify"
                                            Text='<%# (Eval("DefaultCustomizationData")!=null) ? Eval("DefaultCustomizationData").ToString().Replace("\r\n","<br/>") : "" %>'></asp:Label>
                                    </asp:PlaceHolder>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <div class="chk_outgrid gray_backgrnd">
                                <div class="custmize_span">
                                    <%# Eval("FriendlyName")%><%#   ((Eval("Required") != null && Eval("Required").ToString().ToLower() == "true") ? "*" : "")%>
                                </div>
                                <div class="right_grid_custmize">
                                    <asp:PlaceHolder ID="PlaceHolderimg" runat="server" Visible='<%#(Eval("FieldType").ToString().ToLower() == "image")?true : false %>'>
                                        <img id="image_cust" runat="server" height="128" width="131" src='<%# ((Eval("DefaultCustomizationData")!=null)) ? _urlService.GetStaticImageURL(Eval("DefaultCustomizationData").ToString(),200,200,false,85,true) : "" %>' alt="" />
                                        <a runat="server" href='<%# ((Eval("DefaultCustomizationData")!=null)) ? _urlService.GetStaticImageURL(Eval("DefaultCustomizationData").ToString(),rootPath:true) : "" %>' visible="<%# IsPrinter() %>" class="price" target="new"><%=Resources.Shared.Download%></a>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="PlaceHoldertxt" runat="server" Visible='<%#(Eval("FieldType").ToString().ToLower() == "image")?false : true %>'>
                                        <asp:Label runat="server" ID="txtOptionsValue" CssClass="order_heading_data width_auto text_align_justify" Text='<%# (Eval("DefaultCustomizationData")!=null) ? Eval("DefaultCustomizationData").ToString().Replace("\r\n","<br/>") : "" %>'></asp:Label>
                                    </asp:PlaceHolder>
                                    <div class="clr">
                                    </div>
                                </div>
                            </div>
                        </AlternatingItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="main_heading_bot">
                </div>
            </asp:PlaceHolder>
        </div>
        <div class="clr"></div>
        <br />
        <div class="faq_blog">
            <!--User and Shipping Information-->
            <div class="main_heading">
                <span class="order_heading width178 ml14"><%=Resources.ShoppingCart.FulfillmentInformation%></span>
                <span class="order_heading width95"><%=Resources.ShoppingCart.Sku.ToUpperInvariant()%></span>
                <span class="order_heading width205"><%=Resources.ShoppingCart.Title%></span>
                <span class="order_heading width95"><%=Resources.ShoppingCart.Quantity%></span>
                <span class="order_heading width80 text_right"><%=Resources.ShoppingCart.Price%></span>
            </div>
            <div class="main_heading_bg">
                <mc:ElectronicFullfilment ID="electronicFullfilment" runat="Server"></mc:ElectronicFullfilment>
                <mc:VdpFullfilment ID="vdpFullfilment" runat="Server"></mc:VdpFullfilment>
                <asp:Repeater ID="rptShippingAddresses" runat="server" OnItemDataBound="rptShippingAddresses_ItemDataBound">
                    <ItemTemplate>
                        <div class="chk_outgrid fulfillment">
                            <div class="right">
                                <asp:Repeater ID="rptInnerSubOrders" runat="server" OnItemDataBound="rptInnerSubOrders_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:Repeater ID="rptInnerOrderItems" runat="server">
                                            <ItemTemplate>
                                                <div style="margin-bottom: 3px;">
                                                    <span class="fulfillment-item-col word_wrap">
                                                        <%# Eval("SKU").ToString()%>
                                                    </span>
                                                    <span class="fulfillment-item-col-wide">
                                                        <%# Eval("ItemName").ToString()%>
                                                    </span>
                                                    <span class="fulfillment-item-col right-align">
                                                        <asp:Label runat="server" ID="txtQuantity" Text='<%# Eval("ItemQuantity").ToString()%>' readonly="true" size="20"></asp:Label>
                                                    </span>
                                                    <span class="fulfillment-item-col width80 text_right">
                                                        <%# GetPrice(Eval("ItemPrice").ToString())%>
                                                    </span>
                                                    <div class="clr"></div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <div id="Div1" runat="server" visible='<%# Eval("PriceAdjustment") != null || Eval("ShippingCost") != null %>' class="grid_separator">&nbsp;</div>
                                        <div id="Div2" style="margin-bottom: 3px;" runat="server" visible='<%# Eval("PriceAdjustment") != null && Decimal.Parse(Eval("PriceAdjustment").ToString()) > 0 %>'>
                                            <span class="order_heading_data width175"><%=Resources.ShoppingCart.PriceAdjustment%>:
                                            </span>
                                            <span class="order_heading_data width100"><%# Eval("Description") %></span>
                                            <span class="order_heading_data width90">$<%# Eval("PriceAdjustment") %>
                                            </span>
                                            <div class="clr"></div>
                                        </div>
                                        <div id="Div3" style="margin-bottom: 3px;" runat="server" visible='<%# Eval("ShippingCost") != null %>'>
                                            <span class="order_heading_data width175"><%=Resources.ShoppingCart.ShippingCost%>:
                                            </span>
                                            <span class="order_heading_data width100">&nbsp;</span>
                                            <span class="order_heading_data width90">$<%# Eval("ShippingCost")%>
                                            </span>
                                            <div class="clr"></div>
                                        </div>
                                        <div id="Div4" runat="server" visible='<%# Eval("TrackingNumber") != null %>' class="grid_separator">&nbsp;</div>
                                        <div id="Div5" style="margin-bottom: 3px;" runat="server" visible='<%# Eval("TrackingNumber") != null %>'>
                                            <span class="order_heading_data width175"><%=Resources.ShoppingCart.TrackingNumber%>:
                                            </span>
                                            <span class="order_heading_data width100"><%# Eval("TrackingNumber")%></span>
                                            <span class="order_heading_data width90">&nbsp;</span>
                                            <div class="clr"></div>
                                        </div>
                                        <div class="fr" style="margin-bottom: 4px; padding-bottom: 3px;">&nbsp;</div>
                                        <div class="clr"></div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                            <div class="left">
                                <span class="fulfillment-item-col"><%=Resources.ShoppingCart.ShippingInformation.ToUpperInvariant()%></span>
                                <span class="fulfillment-item-col"><%# Eval("Address").ToString().Replace(Environment.NewLine, "<br />")%></span>
                                <span class="fulfillment-item-col"><%# Eval("ContactName").ToString()%><br />
                                    <%# Eval("Phone").ToString()%></span>
                                <div class="clr"></div>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="main_heading_bg" id="divspecialinstructions" runat="server">
                <div class="chk_outgrid gray_backgrnd">
                    <div class="heading_span_new">
                        <%=Resources.ShoppingCart.SpecialInstructionsDetails%>
                    </div>
                    <div class="fl joti">
                        <asp:Label ID="lblSpecialInstructions" CssClass="instruction" runat="server"></asp:Label><br />
                        <br />
                        <asp:Literal ID="ltRushOrder" runat="server"></asp:Literal>
                    </div>
                </div>
            </div>
            <div class="main_heading_bot"></div>
        </div>
        <div class="clr"></div>
        <!--end grid container-->
        <div class="pd_bot fl">
            &nbsp;
        </div>
    </div>
</div>
