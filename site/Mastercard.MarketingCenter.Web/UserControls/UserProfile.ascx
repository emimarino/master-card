﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="UserProfile.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.UserControls.UserProfile" %>

<div runat="server" id="divAccountInformation" visible="false">
    <section class="content-spaces-in spaces-vertical-in hot-colors">
        <h1 class="h3"><strong><%=Resources.Forms.AccountInformation%></strong></h1>
        <p class="h5">
            <strong><%=Resources.Forms.UserProfile_PasswordNotCreated%></strong><br />
            <%=Resources.Forms.UserProfile_PasswordNotCreatedDescription%>
        </p>
        <p><a id="A1" runat="server" title="<%=Resources.Forms.UserProfile_SendLinkEmail%>" class="btn"><%=Resources.Forms.UserProfile_SendLinkEmail%></a></p>
    </section>
</div>
<div class="content-spaces-in spaces-vertical-in">
    <header class="content-header intro">
        <h1 style="margin-bottom: 0;"><%=Resources.Forms.UserProfile_Title%></h1>
        <p><%=Resources.Forms.UserProfile_Description%></p>
    </header>
    <div class="form">
        <div id="divError" runat="server" visible="false">
            <p class="alert alert-warning">
                <asp:Label ID="lblError" runat="server" Visible="False"></asp:Label>
            </p>
        </div>
        <div class="row columns">
            <div class="column col-sm-5">
                <div class="form-group">
                    <label class="form-label"><%=Resources.Forms.FirstName%><span class="req-mark">*</span></label>
                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-label"><%=Resources.Forms.LastName%><span class="req-mark">*</span></label>
                    <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-label"><%=Resources.Forms.Title%><span class="req-mark">*</span></label>
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-label"><%=Resources.Forms.FinancialInstitution%><span class="req-mark">*</span></label>
                    <asp:TextBox ID="txtIssuerName" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-label"><%=Resources.Forms.Processor%></label>
                    <asp:TextBox ID="txtProcessor" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-label"><%=Resources.Forms.Phone%><span class="req-mark">*</span></label>
                    <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-label"><%=Resources.Forms.Mobile%></label>
                    <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <% if (this.Region != null ? (this.Region.ToLower() == "us") : true)
                    {%>
                <div class="form-group">
                    <label class="form-label"><%=Resources.Forms.Fax%></label>
                    <asp:TextBox ID="txtFax" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <% } %>
                <div class="form-group">
                    <label class="form-label"><%=Resources.Forms.BusinessEmailAddress%></label>
                    <asp:Label runat="server" ID="lblEmail" CssClass="form-control"></asp:Label>
                </div>
                <div class="form-group">
                    <label class="form-label"><%=Resources.Forms.FinancialInstitutionIcaNumber%></label>
                    <asp:Label runat="server" ID="lblIssuerICA" Visible="false" CssClass="form-control"></asp:Label>
                    <asp:HyperLink runat="server" ID="lnkIssuerICA" Target="_top" CssClass="form-control"></asp:HyperLink>
                </div>
            </div>
            <div class="cols-separator col-sm-2"></div>
            <div class="column col-sm-5">
                <div class="form-group">
                    <label class="form-label">
                        <%=Resources.Forms.Address1%> <% if (Region != null && (Region.ToLower() == "us"))
                                                          {%><span class="req-mark">*</span><%} %></label>
                    <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-label"><%=Resources.Forms.Address2%></label>
                    <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-label"><%=Resources.Forms.Address3%></label>
                    <asp:TextBox ID="txtAddress3" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="form-label">
                        <%=Resources.Forms.City%> <% if (Region != null && (Region.ToLower() == "us"))
                                                      {%><span class="req-mark">*</span><%} %></label>
                    <asp:TextBox ID="txtCity" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <%if (Region.ToLower().Equals("US".ToLower()))
                    { %>
                <div class="form-group">
                    <label class="form-label"><%=Res.State%><span class="req-mark">*</span></label>
                    <asp:DropDownList ID="ddlState" runat="server" CssClass="form-control"></asp:DropDownList>
                </div>
                <%} %>
                <%else
                {%>
                <div class="form-group">
                    <label class="form-label"><%=Res.State%></label>
                    <asp:TextBox ID="txtState" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <%} %>
                <%if (ddlCountry.Items.Count > 2)
                    { %>
                <div class="form-group">
                    <label class="form-label"><%=Resources.Forms.Country%><span class="req-mark">*</span></label>
                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control"></asp:DropDownList>
                </div>
                <%} %>
                <%else
                {%>
                <div class="form-group">
                    <label class="form-label"><%=Resources.Forms.Country%><span class="req-mark">*</span></label>
                    <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                </div>

                <%} %>
                <%if (ddlLanguage.Items.Count > 2)
                    { %>
                <div class="form-group">
                    <label class="form-label"><%=Resources.Forms.Language%><span class="req-mark">*</span></label>
                    <asp:DropDownList ID="ddlLanguage" runat="server" CssClass="form-control"></asp:DropDownList>
                </div>
                <%} %>
                <div class="form-group">
                    <label class="form-label">
                        <%=Res.ZipCode%> <% if (Region != null && (Region.ToLower() == "us"))
                                             {%><span class="req-mark">*</span><%} %></label>
                    <asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label id="lblPassword" runat="server" class="form-label"></label>
                    <asp:HyperLink runat="server" ID="linkPassword" Target="_top" CssClass="form-control" />
                    <asp:Label runat="server" CssClass="form-control" ID="lblPasswordNA" Text="<%$Resources:Forms,NotApply%>" Visible="false"></asp:Label>
                </div>
                <asp:PlaceHolder runat="server" ID="placeHolderAdmin" Visible="false">
                    <div class="form-group">
                        <label class="form-label"><%=Resources.Forms.AdminAccess%></label>
                        <asp:Label CssClass="form-control" runat="server" ID="lbladminAccess" Text="<%$Resources:Shared,None%>"> </asp:Label>
                        <asp:HyperLink runat="server" CssClass="form-control" ID="linkEdit" Text="<%$Resources:Shared,Edit%>" Target="_top" />
                    </div>
                </asp:PlaceHolder>
            </div>
        </div>
        <div class="form-group btn-group">
            <div class="control-label"><span class="req-mark">* <%=Resources.Forms.RequiredField%></span></div>
            <asp:LinkButton ID="btnSubmit" CssClass="btn" OnClick="btnSubmit_Click" Text="<%$Resources:Shared,Submit%>" runat="server" />
            <asp:LinkButton ID="btnCancel" CssClass="btn" OnClick="btnCancel_Click" Text="<%$Resources:Shared,Cancel%>" runat="server" />
        </div>
    </div>
</div>

<%-- Order history for current profile --%>
<%-- information is shown on page on request with loginid like: portal/profile?loginid=2987c91c-1cfb-4b37-b591-1ef338e1b8c2 --%>
<div id="orderdiv" runat="server">
    <header class="header-section header-first-section cold-colors content-spaces-in tac">
        <div class="inner">
            <h1><%=Resources.ShoppingCart.UserOrderHistory%></h1>
        </div>
    </header>
    <div class="content-container grid-spaces-in">
        <div class="content-text">
            <div class="mrt_25 fl" id="divNoOrder" runat="server" visible="false">
                <p><span><%=Resources.ShoppingCart.UserWithNoOrders%></span></p>
            </div>
            <div class="grid_container" runat="server" id="divOrderHistory">
                <asp:Repeater ID="orderHistory" OnItemDataBound="orderHistory_ItemBound" runat="server">
                    <HeaderTemplate>
                        <table>
                            <thead>
                                <tr>
                                    <th scope="col"><%=Resources.Shared.Date%></th>
                                    <th scope="col"><%=Resources.ShoppingCart.OrderNumber%></th>
                                    <th scope="col"><%=Resources.ShoppingCart.Items%></th>
                                    <th scope="col"><%=Resources.ShoppingCart.Price%></th>
                                    <th scope="col"><%=Resources.ShoppingCart.Status%></th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><%# Convert.ToDateTime(Eval("OrderDate").ToString()).ToString("MM/dd/yyyy")%></td>
                            <td><a id="OrderLink" href="#" class="price" target="_top" runat="server"><%# Eval("OrderNumber").ToString()%></a>
                            </span>
						    <td><%# Eval("Items").ToString()%></td>
                            <td>$<%# Eval("Price").ToString()%></td>
                            <td><%# Eval("OrderStatus").ToString()%></td>
                        </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <tr>
                            <td><%# Convert.ToDateTime(Eval("OrderDate").ToString()).ToString("MM/dd/yyyy")%></td>
                            <td><a id="OrderLink" href="#" class="price" target="_top" runat="server"><%# Eval("OrderNumber").ToString()%></a></td>
                            <td><%# Eval("Items").ToString()%></td>
                            <td>$<%# Eval("Price").ToString()%></td>
                            <td><%# Eval("OrderStatus").ToString()%></td>
                        </tr>
                    </AlternatingItemTemplate>
                    <FooterTemplate>
                        </tbody>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</div>
