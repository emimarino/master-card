﻿using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Common;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.UI;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.UserControls
{
    public partial class ImageUpload : UserControl
    {
        public IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }

        public bool Editable
        {
            get
            {
                bool editable = true;
                if (ViewState["Editable"] != null)
                {
                    editable = (bool)ViewState["Editable"];
                }

                return editable;
            }
            set
            {
                ViewState["Editable"] = value;
                lnkReplaceFile.Visible = value;
            }
        }

        public string Value
        {
            get
            {
                return hdnCurrentFileUrl.Value.ToString().Replace("/portal", "~");
            }
            set
            {
                hdnCurrentFileUrl.Value = value;
                divNewFile.Attributes["style"] = "display: none;";
                divEditFile.Attributes["style"] = "display: block;";
                lnkCancelUpload.Visible = true;

                if (hdnCurrentFileUrl.Value.ToString().EndsWith(".eps"))
                {
                    lnkViewFull.Text = Resources.Shared.Download;
                    imgThumbnail.Src = _urlService.GetStaticImageURL(hdnCurrentFileUrl.Value.ToString().Replace(".eps", ".jpg"), 100, 100, false, 85, true);
                }
                else
                {
                    lnkViewFull.Text = Resources.Shared.ViewFull;
                    imgThumbnail.Src = _urlService.GetStaticImageURL(hdnCurrentFileUrl.Value.ToString(), 100, 100, false, 85, true);
                }

                lnkViewFull.NavigateUrl = _urlService.GetStaticImageURL(hdnCurrentFileUrl.Value.ToString(), rootPath: true);
                lnkViewFull.Target = "_blank";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnHiddenUploadButton.Attributes.Add("style", "display: none;");
                lnkReplaceFile.Visible = Editable;
            }

            radImageUpload.AllowedFileExtensions = new string[] { ".jpe", ".eps", ".jpg", ".jpeg" };

            radImageUpload.OnClientFileSelected = "Do" + radImageUpload.UniqueID + "Click";

            Page.ClientScript.RegisterStartupScript(GetType(), "Do" + radImageUpload.UniqueID + "Click", " function Do" + radImageUpload.UniqueID + "Click() { " + Page.ClientScript.GetPostBackEventReference(btnHiddenUploadButton, "") + " } ", true);

            if (!IsPostBack && string.IsNullOrEmpty(hdnCurrentFileUrl.Value.ToString()))
            {
                divNewFile.Attributes["style"] = "display: block;";
                divEditFile.Attributes["style"] = "display: none;";
                lnkCancelUpload.Visible = false;
            }

            divErrorMessage.Visible = false;

            if (Request.UserAgent.Contains("MSIE 6"))
            {
                radImageUpload.EnableFileInputSkinning = false;
            }
        }

        protected void lnkCancelUpload_Click(object sender, EventArgs e)
        {
            divNewFile.Attributes["style"] = "display: none;";
            divEditFile.Attributes["style"] = "display: block;";
            updateImageUpload.Update();
        }

        protected void lnkReplaceFile_Click(object sender, EventArgs e)
        {
            divNewFile.Attributes["style"] = "display: block;";
            divEditFile.Attributes["style"] = "display: none;";
            updateImageUpload.Update();
        }

        protected void btnHiddenUploadButton_Click(object sender, EventArgs e)
        {
            if (radImageUpload.UploadedFiles.Count > 0)
            {
                foreach (UploadedFile file in radImageUpload.UploadedFiles)
                {
                    string guid = Guid.NewGuid().ToString();
                    string saveUrl = string.Format(@"{0}/{1}/{2}", WebConfigurationManager.AppSettings["CustomizationFiles.ImageUploadPath"].TrimEnd('/'), guid, Regex.Replace(file.GetName(), "[^\\w\\s\\.]", ""));

                    string imageUploadPath = Server.MapPath(WebConfigurationManager.AppSettings["CustomizationFiles.ImageUploadPath"].TrimEnd('/'));
                    string saveLocation = string.Format(@"{0}\{1}\{2}", imageUploadPath, guid, Regex.Replace(file.GetName(), "[^\\w\\s\\.]", ""));
                    string finalSaveLocation = string.Format(@"{0}\{1}\{2}", imageUploadPath, guid, Regex.Replace(file.GetName(), "[^\\w\\s\\.]", ""));
                    Directory.CreateDirectory(string.Format(@"{0}\{1}", imageUploadPath, guid));

                    file.SaveAs(saveLocation);

                    int.TryParse(WebConfigurationManager.AppSettings["CustomizationFiles.MaxImageHeight"], out int maxImageHeight);
                    int.TryParse(WebConfigurationManager.AppSettings["CustomizationFiles.MaxImageWidth"], out int maxImageWidth);

                    //Check to resize
                    if (finalSaveLocation.ToLower().EndsWith(".eps"))
                    {
                        finalSaveLocation = finalSaveLocation.Substring(0, saveLocation.Length - 3) + "jpg";
                        ImageResizer.ResizeAndSaveImage(saveLocation, finalSaveLocation, maxImageWidth, maxImageHeight, true);
                    }
                    else
                    {
                        bool imageNeedsResized = false;
                        using (System.Drawing.Image image = System.Drawing.Image.FromFile(saveLocation))
                        {
                            imageNeedsResized = maxImageWidth > 0 && maxImageHeight > 0 && (maxImageWidth < image.Width || maxImageHeight < image.Height);
                        }

                        if (imageNeedsResized)
                        {
                            ImageResizer.ResizeAndSaveImage(saveLocation, finalSaveLocation, maxImageWidth, maxImageHeight, true);
                        }
                    }

                    Value = saveUrl;
                }
            }
            else
            {
                divErrorMessage.Visible = true;
            }
        }
    }
}