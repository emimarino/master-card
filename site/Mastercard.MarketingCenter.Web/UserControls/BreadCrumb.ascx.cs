﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.UserControls
{
    public class BreadCrumbItem
    {
        public string Title { get; set; }
        public string Url { get; set; }
    }

    public partial class BreadCrumb : System.Web.UI.UserControl
    {
        private List<BreadCrumbItem> breadCrumbList = new List<BreadCrumbItem>();
        private List<BreadCrumbItem> loadedBreadCrumbList = new List<BreadCrumbItem>();

        private string pageTitle = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (SiteMap.CurrentNode != null)
                {
                    AddBreadCrumbItemsFromSiteMapNode(SiteMap.CurrentNode);
                    if (SiteMap.CurrentNode["holdInBreadCrumb"] != null && SiteMap.CurrentNode["holdInBreadCrumb"].ToUpper() == "TRUE")
                        Session["BreadCrumbItemList"] = loadedBreadCrumbList;
                    else
                        Session["BreadCrumbItemList"] = null;
                    breadCrumbList.AddRange(loadedBreadCrumbList);
                }
                else if ((Session["BreadCrumbItemList"] == null && SiteMap.CurrentNode == null) || (HttpContext.Current.Items["globalid"] != null))
                {
                    AddBreadCrumbItemsFromSiteMapNode(SiteMap.RootNode);
                    Session["BreadCrumbItemList"] = loadedBreadCrumbList;
                    int insertIndex = 0;
                    foreach (BreadCrumbItem item in loadedBreadCrumbList)
                    {
                        breadCrumbList.Insert(insertIndex, item);
                        insertIndex++;
                    }
                }
                else if (Session["BreadCrumbItemList"] != null)
                {
                    List<BreadCrumbItem> sessionBreadCrumbList = (List<BreadCrumbItem>)Session["BreadCrumbItemList"];
                    loadedBreadCrumbList.Clear();
                    loadedBreadCrumbList.AddRange(sessionBreadCrumbList);

                    loadedBreadCrumbList.AddRange(breadCrumbList);
                    breadCrumbList = loadedBreadCrumbList;
                }

                rptBreadCrumbItems.DataSource = breadCrumbList;
                rptBreadCrumbItems.DataBind();

                Page.Title = pageTitle.Trim();

                if (HttpContext.Current.Items["RouteUrl"] != null)
                {
                    SiteMapNode currentNode = SiteMap.Provider.FindSiteMapNode("~/" + HttpContext.Current.Items["RouteUrl"].ToString().TrimStart('/'));
                    if (currentNode != null && currentNode["pageTitle"] != null)
                        Page.Title = currentNode["pageTitle"];
                }
            }
        }

        protected void rptBreadCrumbItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                BreadCrumbItem currentItem = e.Item.DataItem as BreadCrumbItem;
                Control ltBreadCrumbItem = e.Item.FindControl("ltBreadCrumbItem");
                Control lnkBreadCrumbItem = e.Item.FindControl("lnkBreadCrumbItem");

                if (currentItem.Url == Request.RawUrl)
                {
                    lnkBreadCrumbItem.Visible = false;
                    ltBreadCrumbItem.Visible = true;
                }
                else
                {
                    lnkBreadCrumbItem.Visible = true;
                    ltBreadCrumbItem.Visible = false;
                }

                if (e.Item.ItemIndex == 0)
                    pageTitle += "";
                else
                    pageTitle += currentItem.Title + " ";
            }
        }

        public void AddItemToBreadCrumb(string title)
        {
            BreadCrumbItem item = new BreadCrumbItem();
            item.Title = title;
            item.Url = Request.RawUrl.TrimEnd('/');

            BreadCrumbItem existingItem = breadCrumbList.Find(i => i.Url.ToUpper() == item.Url.ToUpper());
            if (existingItem == null)
            {
                breadCrumbList.Add(item);

                if (rptBreadCrumbItems.HasControls())
                {
                    rptBreadCrumbItems.DataSource = breadCrumbList;
                    rptBreadCrumbItems.DataBind();
                }
            }
            else
            {
                List<BreadCrumbItem> sessionBreadCrumbList = (List<BreadCrumbItem>)Session["BreadCrumbItemList"];
                sessionBreadCrumbList.Remove(existingItem);
                Session["BreadCrumbItemList"] = sessionBreadCrumbList;
            }
        }

        public void AddItemToBreadCrumb(string title, string url)
        {
            BreadCrumbItem item = new BreadCrumbItem();
            item.Title = title;
            item.Url = url.TrimEnd('/');

            BreadCrumbItem existingItem = breadCrumbList.Find(i => i.Title.ToUpper() == item.Title.ToUpper());
            if (existingItem == null)
            {
                breadCrumbList.Add(item);

                if (rptBreadCrumbItems.HasControls())
                {
                    rptBreadCrumbItems.DataSource = breadCrumbList;
                    rptBreadCrumbItems.DataBind();
                }
            }
            else
            {
                List<BreadCrumbItem> sessionBreadCrumbList = (List<BreadCrumbItem>)Session["BreadCrumbItemList"];
                sessionBreadCrumbList.Remove(existingItem);
                Session["BreadCrumbItemList"] = sessionBreadCrumbList;
            }
        }

        private void AddBreadCrumbItemsFromSiteMapNode(SiteMapNode node)
        {
            BreadCrumbItem item = new BreadCrumbItem();
            item.Title = node.Title;
            item.Url = Page.ResolveUrl(node.Url).TrimEnd('/');

            loadedBreadCrumbList.Insert(0, item);

            if (node != SiteMap.RootNode)
                AddBreadCrumbItemsFromSiteMapNode(node.ParentNode);
        }
    }
}