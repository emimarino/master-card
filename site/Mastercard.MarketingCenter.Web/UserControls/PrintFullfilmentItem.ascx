﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="PrintFullfilmentItem.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.UserControls.PrintFullfilmentItem" %>

<asp:Repeater ID="rptShippingAddresses" runat="server" OnItemDataBound="rptShippingAddresses_ItemDataBound">
    <ItemTemplate>
        <div class="chk_outgrid fulfillment">
            <div class="right">
                <asp:Repeater ID="rptInnerCartItems" runat="server">
                    <ItemTemplate>
                        <div style="margin-bottom: 3px;">
                            <span class="fulfillment-item-col word_wrap">
                                <%# Eval("SKU").ToString()%>
                            </span>
                            <span class="fulfillment-item-col-wide">
                                <%# Eval("Title").ToString()%>
                            </span>
                            <span class="fulfillment-item-col right-align">
                                <asp:Label runat="server" ID="txtQuantity" Text='<%# Eval("Quantity").ToString()%>' readonly="true" size="20"></asp:Label>
                            </span>
                            <span class="fulfillment-item-col" style="width: 80px; text-align: right;">
                                <%# GetPartialPrice(Eval("ContentItemID").ToString(), Eval("Quantity").ToString())%>
                            </span>
                            <div class="clr"></div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="left">
                <span class="fulfillment-item-col"><%=Resources.ShoppingCart.ShippingInformation.ToUpperInvariant()%></span>
                <span class="fulfillment-item-col"><%# Eval("Address").ToString().Replace(Environment.NewLine, "<br />")%></span>
                <span class="fulfillment-item-col"><%# Eval("ContactName").ToString()%><br />
                    <%# Eval("Phone").ToString()%></span>
            </div>
        </div>
    </ItemTemplate>
</asp:Repeater>

<asp:Repeater ID="rptOrderShippingAddresses" runat="server" OnItemDataBound="rptOrderShippingAddresses_ItemDataBound">
    <ItemTemplate>
        <div class="chk_outgrid fulfillment">
            <div class="right">
                <asp:Repeater ID="rptInnerOrderItems" runat="server">
                    <ItemTemplate>
                        <div style="margin-bottom: 3px;">
                            <span class="fulfillment-item-col word_wrap">
                                <%# Eval("SKU").ToString()%>
                            </span>
                            <span class="fulfillment-item-col-wide">
                                <%# Eval("ItemName").ToString()%>
                            </span>
                            <span class="fulfillment-item-col right-align">
                                <asp:Label runat="server" ID="txtQuantity" Text='<%# Eval("ItemQuantity").ToString()%>' readonly="true" size="20"></asp:Label>
                            </span>
                            <span class="fulfillment-item-col" style="width: 80px; text-align: right;">
                                <%# FormatNumber(Convert.ToDecimal(Eval("ItemPrice")))%>
                            </span>
                            <div class="clr"></div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="left">
                <span class="fulfillment-item-col"><%=Resources.ShoppingCart.ShippingInformation.ToUpperInvariant()%></span>
                <span class="fulfillment-item-col"><%# Eval("Address").ToString().Replace(Environment.NewLine, "<br />")%></span>
                <span class="fulfillment-item-col"><%# Eval("ContactName").ToString()%><br />
                    <%# Eval("Phone").ToString()%></span>
            </div>
        </div>
    </ItemTemplate>
</asp:Repeater>

<div class="main_heading_bg" id="divspecialinstructions" runat="server">
    <div class="chk_outgrid gray_backgrnd">
        <div class="heading_span_new">
            <%=Resources.ShoppingCart.SpecialInstructionsDetails%>
        </div>
        <div class="fl joti">
            <asp:Label ID="lblSpecialInstructions" CssClass="instruction" runat="server"></asp:Label><br />
            <br />
            <asp:Literal ID="ltRushOrder" runat="server"></asp:Literal>
        </div>
    </div>
</div>