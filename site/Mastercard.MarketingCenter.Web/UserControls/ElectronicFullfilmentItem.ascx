﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ElectronicFullfilmentItem.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.UserControls.ElectronicFullfilmentItem" %>

<asp:Repeater ID="rptElectronicDownloadCartItems" runat="server">
    <HeaderTemplate>
        <div class="chk_outgrid fulfillment">
            <div class="right">
    </HeaderTemplate>
    <ItemTemplate>
        <div style="margin-bottom: 3px;">
            <span class="fulfillment-item-col word_wrap">
                <%# Eval("SKU").ToString()%>
            </span>
            <span class="fulfillment-item-col-wide">
                <%# Eval("Title").ToString()%>
            </span>
            <span class="fulfillment-item-col right-align">1
            </span>
            <span class="fulfillment-item-col" style="width: 80px; text-align: right;">
                <%# String.Format(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "{0:C}", Eval("DownloadPrice")) %>
            </span>
            <div class="clr"></div>
        </div>
    </ItemTemplate>
    <FooterTemplate>
        </div>
			<div class="left">
                <span class="fulfillment-item-col"><%=Resources.ShoppingCart.ElectronicDelivery%></span>
                <span class="fulfillment-item-col"><%=Resources.ShoppingCart.ElectronicDeliveryInstructions%></span>
            </div>
        </div>
		<div class="clr"></div>
        <div class="separator"></div>
    </FooterTemplate>
</asp:Repeater>

<asp:Panel ID="pnlOrderItems" runat="server" Visible="false">
    <asp:Repeater ID="rptElectronicDownloadOrderItems" runat="server">
        <HeaderTemplate>
            <div class="chk_outgrid fulfillment">
                <div class="right">
        </HeaderTemplate>
        <ItemTemplate>
            <div style="margin-bottom: 3px;">
                <span class="fulfillment-item-col word_wrap">
                    <%# Eval("SKU").ToString()%>
                </span>
                <span class="fulfillment-item-col-wide">
                    <%# Eval("ItemName").ToString()%>
                </span>
                <span class="fulfillment-item-col right-align">1
                </span>
                <span class="fulfillment-item-col" style="width: 80px; text-align: right;">
                    <%# String.Format(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "{0:C}", Eval("ItemPrice")) %>
                </span>
                <div class="clr"></div>
            </div>
        </ItemTemplate>
        <FooterTemplate>
            </div>
				<div class="left">
                    <span class="fulfillment-item-col"><%=Resources.ShoppingCart.ElectronicDelivery%></span>
                    <span class="fulfillment-item-col"></span>
                </div>
            </div>
        </FooterTemplate>
    </asp:Repeater>
    <asp:PlaceHolder ID="plElectronicDeliveryInfo" runat="server">
        <div class="ed-info">
            <div style="margin-bottom: 10px; font-weight: bold;">
                <%=Resources.ShoppingCart.ElectronicDeliveryInformation.ToUpperInvariant()%>
            </div>
            <div>
                <%=Resources.ShoppingCart.ElectronicDeliveryInformationDescription.Split(new string[] { "{0}" }, StringSplitOptions.None)[0]%><asp:Literal ID="litExpirationDate" runat="server"></asp:Literal><%=Resources.ShoppingCart.ElectronicDeliveryInformationDescription.Split(new string[] { "{0}" }, StringSplitOptions.None)[1].Split(new string[] { "{1}" }, StringSplitOptions.None)[0]%><asp:Literal ID="litMaxDownload" runat="server"></asp:Literal><%=Resources.ShoppingCart.ElectronicDeliveryInformationDescription.Split(new string[] { "{1}" }, StringSplitOptions.None)[1]%>
            </div>
            <asp:Repeater ID="rptDownloadLinks" runat="server" OnItemDataBound="rptDownloadLinks_ItemDataBound">
                <HeaderTemplate>
                    <table class="tbl-links" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <th scope="col" class="link"><%=Resources.ShoppingCart.ElectronicDeliveryDownloadingLink%>:</th>
                            <th scope="col"><%=Resources.ShoppingCart.ElectronicDeliveryRemainingDownloads%>:</th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:HyperLink ID="lnkDownload" runat="server"></asp:HyperLink></td>
                        <td>
                            <span style="float: left;">
                                <asp:Literal ID="litRemainingDownload" runat="server"></asp:Literal>&nbsp;</span>
                            <span id="spanResetDownload" runat="server" class="bg_btn_left mb11 fl" style="margin-top: 0;" visible="false">
                                <asp:Button ID="btnResetDownload" runat="server" Text="<%$Resources:Shared,Reset%>" CssClass="submit_bg_btn" Style="border: 0;" OnClick="btnResetDownload_Click" />
                            </span>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <div>
                <%=Resources.ShoppingCart.ElectronicDeliveryEmailContact%>
            </div>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plElectronicDeliveryApprovalNote" runat="server">
        <div class="ed-info">
            <div style="margin-bottom: 10px; font-weight: bold;">
                <%=Resources.ShoppingCart.ImportantNote.ToUpperInvariant()%>:
            </div>
            <div style="width: 32px; float: left">
                <img src="/portal/inc/images/alert_symbol.gif" style="border: none" alt="" />
            </div>
            <div style="width: 600px; float: left">
                <%=Resources.ShoppingCart.OrderBeingReviewed%>
            </div>
            <div class="clr"></div>
        </div>
    </asp:PlaceHolder>
    <div class="clr"></div>
    <div class="separator"></div>
</asp:Panel>