<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ImageUpload.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.UserControls.ImageUpload" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="rad" %>
<asp:UpdatePanel ID="updateImageUpload" UpdateMode="Conditional" runat="server">
    <triggers>
        <asp:PostBackTrigger ControlID="btnHiddenUploadButton" />
        <asp:PostBackTrigger ControlID="lnkReplaceFile" />
    </triggers>
    <contenttemplate>
        <div id="divEditFile" runat="server" class="uploadedFile" style="display: block;">
            <div id="divUploadFilePreview" class="fl">
                <img id="imgThumbnail" runat="server" max-height="100" max-width="100" alt=""/>
            </div>
            <div class="fl">
            <ul id="uploadOptions">
                <li><asp:HyperLink ID="lnkViewFull" runat="server"><%=Resources.Shared.ViewFull%></asp:HyperLink></li>
                <li><asp:LinkButton ID="lnkReplaceFile" OnClick="lnkReplaceFile_Click" runat="server" Text="<%$Resources:Shared,Replace%>" /></li>
            </ul></div>
            <div class="clr"></div>
        </div>
        <div id="divNewFile" runat="server" style="display: block">
            <div id="divUploadFile">
                <div id="divErrorMessage" runat="server" style="color: Red;"><%=Resources.Errors.FileInvalidImage%></div>
                <rad:RadUpload id="radImageUpload" RegisterWithScriptManager="false" Width="195px" runat="server" ReadOnlyFileInputs="true" ControlObjectsVisibility="None" AllowedFileExtensions=".jpe,.eps,.jpg,.jpeg" data-clientFilter=".jpe,.eps,.jpg,.jpeg" OnClientAdded="OnClientAdded"></rad:RadUpload>
                <asp:Button ID="btnHiddenUploadButton" runat="server" onclick="btnHiddenUploadButton_Click" />
                <script>
                    function OnClientAdded(sender, args) {
                        var allowedMimeTypes = $telerik.$(sender.get_element()).attr("data-clientFilter");
                        $telerik.$(args.get_row()).find(".ruFileInput").attr("accept", allowedMimeTypes);
                    }
                </script>
            </div>
            <div id="divUploadFileCancel">
                <asp:LinkButton ID="lnkCancelUpload" runat="server" OnClick="lnkCancelUpload_Click" Text="<%$Resources:Shared,Cancel%>" />               
            </div>
            <div class="clr"></div>
        </div>
        <asp:HiddenField ID="hdnCurrentFileUrl" runat="server" />
    </contenttemplate>
</asp:UpdatePanel>