﻿using AWS.Web.Data;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core;
using Mastercard.MarketingCenter.Web.Core.Controls;
using Mastercard.MarketingCenter.Web.Core.Services;
using Slam.Cms.Common;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.UserControls
{
    public partial class Footer : RegionalUserControl
    {
        protected string OrderChildren { get; set; }
        protected ISegmentationService SegmentationServices { get { return DependencyResolver.Current.GetService<ISegmentationService>(); } }
        public MastercardSitemap regionConfig { get { return (MastercardSitemap)DependencyResolver.Current.GetService<Slam.Cms.Sitemap>(); ; } }

        protected UrlHelper Url;
        protected int rptrItemIdx = 0;
        protected SlamContext slamContext;

        public string VisitorType
        {
            get;
            set;
        }

        public static string BaseUrl
        {
            get
            {
                return string.Concat(HttpContext.Current.Request.Url.Scheme, "://", HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Url = new UrlHelper(Request.RequestContext);
            if (UserContext == null)
            {
                UserContext = DependencyResolver.Current.GetService<UserContext>();
            }

            if (!IsPostBack)
            {
                var users = DataServices.ExecuteForList<RetrieveUserInformationResult>("RetrieveUserInformation", UserContext?.User?.UserName ?? string.Empty);
                if (users != null && users.Any())
                {
                    var user = users[0];
                    switch (user.processor)
                    {
                        case "MasterCard":
                            VisitorType = "MasterCard";
                            break;
                        case "A Plus":
                            VisitorType = "Vendor";
                            break;
                        default:
                            VisitorType = string.Join(";", SegmentationServices.GetSegmentationsByUserName(UserContext?.User?.UserName)?.Select(s => $"{s.SegmentationName}"));
                            break;
                    }
                }
            }

            ManageBrowseAll();
        }

        protected void ManageBrowseAll()
        {
            var slamContextService = DependencyResolver.Current.GetService<SlamContextService>();
            IList<TagTreeNode> items;

            items = slamContextService.GetTreeNode(SlamQueryCacheBehavior.Default, UserContext.SelectedRegion, UserContext.SelectedLanguage);
            browseAllGroups.DataSource = items.ToList();
            browseAllGroups.DataBind();
        }

        protected void browseAllGroups_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var browseAllTagCategory = e.Item.FindControl("browseAllTagCategory") as Repeater;
            browseAllTagCategory.DataSource = ((TagTreeNode)(e.Item.DataItem)).Children.ToList();
            browseAllTagCategory.DataBind();
        }

        protected void browseAllTagCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var browseAllTagCategoryChildren = e.Item.FindControl("browseAllTagCategoryChildren") as Repeater;
            var node = ((TagTreeNode)(e.Item.DataItem));
            var children = new List<TagTreeNode>();
            if (node.TagCategory != null && node.TagCategory.TagCategoryId.Equals(MarketingCenterDbConstants.TagCategories.Markets, StringComparison.OrdinalIgnoreCase))
            {
                foreach (var child in node.Children)
                {
                    if (child.Children.Any())
                    {
                        children.AddRange(child.Children);
                    }
                    else
                    {
                        children.Add(child);
                    }
                }
            }
            else
            {
                children = (List<TagTreeNode>)node.Children;
            }

            browseAllTagCategoryChildren.DataSource = children.OrderBy(x => x.Text);
            browseAllTagCategoryChildren.DataBind();

            var showMore = e.Item.FindControl("ddShowMore");
            var showLess = e.Item.FindControl("ddShowLess");
            showMore.Visible = showLess.Visible = children.Count() > 5;
        }
    }
}