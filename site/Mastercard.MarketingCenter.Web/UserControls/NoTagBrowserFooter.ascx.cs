﻿using AWS.Web.Data;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Data;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.UserControls
{
    public partial class NoTagBrowserFooter : System.Web.UI.UserControl
    {
        protected UrlHelper Url;
        protected int rptrItemIdx = 0;
        protected SlamContext slamContext;
        public Core.MastercardSitemap sitemap
        {
            get
            {
                return (Core.MastercardSitemap)DependencyResolver.Current.GetService<Slam.Cms.Sitemap>();
            }
        }
        public UserContext UserContext
        {
            get
            {
                return DependencyResolver.Current.GetService<UserContext>();
            }
        }

        protected ISegmentationService SegmentationServices
        {
            get
            {
                return DependencyResolver.Current.GetService<ISegmentationService>();
            }
        }

        public string VisitorType
        {
            get;
            set;
        }

        public static string BaseUrl
        {
            get
            {
                return string.Concat(HttpContext.Current.Request.Url.Scheme, "://", HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Url = new UrlHelper(Request.RequestContext);

            if (!IsPostBack && UserContext.User != null)
            {
                var users = DataServices.ExecuteForList<RetrieveUserInformationResult>("RetrieveUserInformation", UserContext.User?.UserName ?? string.Empty);
                if (users != null && users.Any())
                {
                    var user = users[0];
                    switch (user.processor)
                    {
                        case "MasterCard":
                            VisitorType = "MasterCard";
                            break;
                        case "A Plus":
                            VisitorType = "Vendor";
                            break;
                        default:
                            VisitorType = string.Join(";", SegmentationServices.GetSegmentationsByUserName(UserContext.User.UserName)?.Select(s => $"{s.SegmentationName}"));
                            break;
                    }
                }
            }
        }
    }
}