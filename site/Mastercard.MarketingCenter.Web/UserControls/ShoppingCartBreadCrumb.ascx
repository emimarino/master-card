﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCartBreadCrumb.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.UserControls.ShoppingCartBreadCrumb" %>

<asp:Repeater ID="rptBreadCrumbItems" runat="server" OnItemDataBound="rptBreadCrumbItems_ItemDataBound">
    <ItemTemplate>
        <asp:Literal ID="ltSeparator" runat="server" Text=' : '></asp:Literal>
        <asp:HyperLink ID="lnkBreadCrumbItem" runat="server" NavigateUrl='<%# Eval("Url") %>'><%# Eval("Title") %></asp:HyperLink>
        <asp:Literal ID="ltBreadCrumbItem" runat="server" Text='<%# Eval("Title") %>'></asp:Literal>
    </ItemTemplate>
</asp:Repeater>