﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="UserControls_InsertsBundleProgramLandingPagePanel" CodeBehind="InsertsBundleProgramLandingPagePanel.ascx.cs" %>

<div id="divFAQs" runat="server" class="level_right_bar">
    <div class="inner_short_top"><%=Resources.Shared.BundlesProgram%></div>
    <div class="inner_short_center related_resources">
        <a href='<%= ToolkitServices.GetInsertsBundleToolkitPageUrl() %>'>
            <img src="/portal/inc/images/IBCU_Bundles_SideBox.gif" style="width: 152px; height: 112px; margin: 10px 0px 5px 10px; border: 0px;" alt="" /></a>
        <p><%=Resources.Shared.BundlesProgramMarketingPlan%></p>
        <div style="margin-bottom: 5px; margin-left: 12px; margin-right: 60px" class="btn_left"><a class="btn_right" title="<%=Resources.Shared.GetStarted%>" href='<%= ToolkitServices.GetInsertsBundleToolkitPageUrl() %>'><%=Resources.Shared.LearnMore.ToUpperInvariant()%></a> </div>
        <div class="clear"></div>
    </div>
    <div class="inner_short_bottom"></div>
</div>