﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="VdpFullfilmentItem.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.UserControls.VdpFullfilmentItem" %>
<%@ Register TagPrefix="ajax2" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Repeater ID="rptVdpItems" runat="server">
    <HeaderTemplate>
        <div class="chk_outgrid fulfillment">
            <div class="right">
    </HeaderTemplate>
    <ItemTemplate>
        <div style="margin-bottom: 3px;">
            <span class="fulfillment-item-col word_wrap">
                <%# Eval("SKU").ToString()%>
            </span>
            <span class="fulfillment-item-col-wide">
                <%# Eval("Title").ToString()%>
            </span>
            <span class="fulfillment-item-col right-align">
                <asp:Label runat="server" ID="txtQuantity" Text='<%# Eval("Quantity").ToString()%>' readonly="true" size="20"></asp:Label>
            </span>
            <span class="fulfillment-item-col" style="width: 80px; text-align: right;">
                <%# GetPrice(Eval("ContentItemID").ToString(), Eval("Quantity").ToString())%>
            </span>
            <div class="clr"></div>
        </div>
    </ItemTemplate>
    <FooterTemplate>
        </div>
			<div class="left">
                <span class="fulfillment-item-col"><%=Resources.ShoppingCart.VariableDataPrinting%></span>
                <span class="fulfillment-item-col"><%=Resources.ShoppingCart.VariableDataPrintingInstructionsToProvide%></span>
            </div>
        </div>
		<div class="clr"></div>
        <div class="separator"></div>
    </FooterTemplate>
</asp:Repeater>

<asp:Panel ID="pnlVdpOrder" runat="server" CssClass="chk_outgrid fulfillment" Visible="false">
    <div class="right">
        <asp:Repeater ID="rptVdpOrderItems" runat="server">
            <ItemTemplate>
                <div style="margin-bottom: 3px;">
                    <span class="fulfillment-item-col word_wrap">
                        <%# Eval("SKU").ToString()%>
                    </span>
                    <span class="fulfillment-item-col-wide">
                        <%# Eval("ItemName").ToString()%>
                    </span>
                    <span class="fulfillment-item-col right-align">
                        <asp:Label runat="server" ID="txtQuantity" Text='<%# Eval("ItemQuantity").ToString()%>' readonly="true" size="20"></asp:Label>
                    </span>
                    <span class="fulfillment-item-col" style="width: 80px; text-align: right;">
                        <%# String.Format(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "{0:C}", Eval("ItemPrice")) %>
                    </span>
                    <div class="clr"></div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <div class="left">
        <div id="pnlComplete" runat="server" visible="false">
            <span id="spnBtnComplete" runat="server" class="bg_grey_left mb11 fl">
                <asp:Button ID="btnComplete" runat="server" BorderWidth="0px" Text="<%$Resources:Shared,Complete%>" Enabled="false" CssClass="grey_bg_btn" OnClick="btnComplete_Click" />
            </span>
            <asp:Label ID="lblCompleted" runat="server" Text="<%$Resources:Shared,Completed%>" CssClass="completed-sub-order" Visible="false"></asp:Label>
        </div>
        <div id="vdpInstruction" runat="server">
            <span class="fulfillment-item-col"><%=Resources.ShoppingCart.VariableDataPrinting%></span>
            <span class="fulfillment-item-col">
                <asp:HyperLink ID="lnkSendDataFile" runat="server"><%=Resources.ShoppingCart.VariableDataPrintingClickForInstructions%></asp:HyperLink>
            </span>
        </div>
        <div id="ftpStatus" runat="server" style="display: none;">
            <span class="fulfillment-item-col"><%=Resources.ShoppingCart.VdpFtpStatus%>:</span>
            <span class="fulfillment-item-col">
                <asp:Literal ID="litFtpStatus" runat="server"></asp:Literal><br />
                <a id="lnkSendFtp" href="#" runat="server" style="display: none;"><%=Resources.ShoppingCart.SendResendAccountInformation%></a>
                <asp:Button ID="btnAcceptDataFile" runat="server" Text="<%$Resources:ShoppingCart,AcceptDataFile%>" OnClick="btnAcceptDataFile_Click" Visible="false" />
            </span>
        </div>
    </div>
    <div class="clr"></div>
    <div class="separator"></div>
    <ajax2:ModalPopupExtender ID="mdlFtp" runat="server" CancelControlID="btnCancel" TargetControlID="lnkSendFtp" PopupControlID="pnlFtp"></ajax2:ModalPopupExtender>
    <asp:Panel ID="pnlFtp" runat="server" CssClass="modalPopup" Style="width: 500px;">
        <span style="font-size: 15px;"><strong><%=Resources.ShoppingCart.VariableDataPrintingFtpAccountInformation%></strong><br />
            <br />
            <asp:Panel ID="pnlError" runat="server" EnableViewState="false" Visible="false" CssClass="red_error">
                <%=Resources.Errors.AllFieldsRequired%>.<br />
                <br />
            </asp:Panel>
        </span>
        <br />
        <br />
        <table>
            <tr>
                <td><%=Resources.Forms.Server%>:</td>
                <td>
                    <asp:TextBox ID="txtFtpServer" runat="server" Style="width: 350px;"></asp:TextBox></td>
            </tr>
            <tr>
                <td><%=Resources.Forms.UserName%>:</td>
                <td>
                    <asp:TextBox ID="txtFtpUserName" runat="server" Style="width: 350px;"></asp:TextBox></td>
            </tr>
            <tr>
                <td><%=Resources.Forms.Password%>:</td>
                <td>
                    <asp:TextBox ID="txtFtpPassword" runat="server" Style="width: 350px;"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <asp:Button ID="btnCancel" runat="server" Text="<%$Resources:Shared,Cancel%>" />
                    <asp:Button ID="btnSave" runat="server" Text="<%$Resources:Shared,Save%>" OnClick="btnSave_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>