<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="FileUpload.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.UserControls.FileUpload" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="rad" %>
<asp:UpdatePanel ID="updateFileUpload" UpdateMode="Conditional" runat="server">
    <triggers>
        <asp:PostBackTrigger ControlID="btnHiddenUploadButton" /> 
    </triggers>
    <contenttemplate>
        <div id="divEditFile" runat="server" class="uploadedFile" style="display: block">
            <div id="divUploadFileLink">
                <asp:HyperLink ID="lnkViewFile" CssClass="price" runat="server"><%=Resources.Shared.ViewFile%></asp:HyperLink>
            </div>
            <div id="divUploadFileReplace"><asp:LinkButton ID="lnkReplaceFile" OnClick="lnkReplaceFile_Click" runat="server" Text="<%$Resources:Shared,Replace%>" /></div>
        </div>
        <div id="divNewFile" runat="server" style="display: block">
            <div id="divUploadFile">
                <rad:RadUpload id="radFileUpload" Skin="Telerik" InputSize="18" Width="150px" runat="server" ReadOnlyFileInputs="true" ControlObjectsVisibility="None" AllowedFileExtensions=".pdf" data-clientFilter=".pdf" OnClientAdded="OnClientAdded"></rad:RadUpload>
                <asp:Button ID="btnHiddenUploadButton" runat="server" onclick="btnHiddenUploadButton_Click" />
                <script>
                    function OnClientAdded(sender, args) {
                        var allowedMimeTypes = $telerik.$(sender.get_element()).attr("data-clientFilter");
                        $telerik.$(args.get_row()).find(".ruFileInput").attr("accept", allowedMimeTypes);
                    }
                </script>
                <div id="divErrorMessage" runat="server" style="color: Red;"><%=Resources.Errors.FileInvalid%></div>
            </div>
            <div id="divUploadFileCancel">
                <asp:LinkButton ID="lnkCancelUpload" runat="server" OnClick="lnkCancelUpload_Click" Text="<%$Resources:Shared,Cancel%>" />
            </div>
        </div>
        <asp:HiddenField ID="hdnCurrentFileUrl" runat="server" />
    </contenttemplate>
</asp:UpdatePanel>