﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FooterShoppingCart.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.UserControls.FooterShoppingCart" %>
<div id="footer">
    <footer class="page-footer">
        <div class="clearfix nav-copy grid-spaces-in">
            <div class="copyrights"><%=string.Format(Resources.Shared.MastercardCopyrights, DateTime.Now.Year)%></div>
            <nav class="nav-footer">
                <%=Resources.Shared.Mastercard%>
                <%= string.Format(Resources.Shared.MastercardTermsOfUse, sitemap.GetSetting("TermsOfUse", UserContext.SelectedRegion))%>
                <%= string.Format(Resources.Shared.MastercardPrivacyPolicy, sitemap.GetSetting("PrivacyNotice", UserContext.SelectedRegion))%>
            </nav>
        </div>
    </footer>
    <script language="javascript">
        var visitorType = "<%=VisitorType%>";
    </script>
</div>
