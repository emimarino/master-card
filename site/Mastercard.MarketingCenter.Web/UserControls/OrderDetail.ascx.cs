﻿using AjaxControlToolkit;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.UserControls
{
    public partial class OrderDetail : UserControl
    {
        decimal total = 0;
        decimal _totalCost = 0;
        string orderStatus = string.Empty;
        private string mode = string.Empty;

        public UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
        public IUserService _userService { get { return DependencyResolver.Current.GetService<IUserService>(); } }
        public IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }
        public IEmailService _emailService { get { return DependencyResolver.Current.GetService<IEmailService>(); } }
        public IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["mode"]))
            {
                mode = Request.QueryString["mode"];
            }

            if (mode == "edit")
            {
                lnkBtnEdit.Visible = false;
                tabs.ActiveTabIndex = 1;
            }

            if (!IsPostBack)
            {
                lnkOverrideStatus.Visible = HttpContext.Current.User.IsInRole(Constants.Roles.McAdmin) || HttpContext.Current.User.IsInRole(Constants.Roles.DevAdmin);

                SetBackUrl();

                string orderID = string.Empty;
                if (Page.RouteData.Values["orderid"] != null)
                {
                    orderID = _orderService.GetOrderIDByOrderNo(Page.RouteData.Values["orderid"].ToString()).ToString();
                }
                else if (Request.QueryString["orderID"] != null)
                {
                    orderID = _orderService.GetOrderIDByOrderNo(Request.QueryString["orderID"].ToString()).ToString();
                }
                else if (Request.QueryString["oid"] != null)
                {
                    orderID = Request.QueryString["oid"];
                }

                if (!string.IsNullOrEmpty(orderID) && orderID != "0")
                {
                    ViewState["OrderId"] = orderID;

                    divcostadjustment.Visible = false;
                    lblpriceadjustment.Visible = false;
                    txtpriceadjustment.Visible = false;
                    lblDescription.Visible = false;
                    txtDescription.Visible = false;
                    plcUserOptions.Visible = false;
                    Approvaldiv.Visible = false;
                    plcPrinter.Visible = false;
                    plcUser.Visible = false;
                    int Orderid = int.Parse(orderID);
                    RetrieveOrderDetails(Orderid);
                    SetStatusInDropdown();
                    RetrieveShippingInformation(Orderid);
                    RetrieveCustomisationDetails(Orderid);
                    RetrieveOrderLog(Orderid);
                    RetrieveOrderItems(Orderid);

                    electronicFullfilment.OrderId = Convert.ToInt32(orderID);
                    vdpFullfilment.OrderId = Convert.ToInt32(orderID);
                    vdpFullfilment.AllowToComplete = true;
                }
                else
                {
                    Response.Redirect($"{Page.ErrorPage}?code=invalidorder");
                }

                if (Request.UrlReferrer != null && Request.UrlReferrer.ToString().ToLower().Contains("completesuborder"))
                {
                    foreach (TabPanel panel in tabs.Tabs)
                    {
                        if (panel.HeaderText == "Fulfillment Information")
                        {
                            tabs.ActiveTab = panel;
                            break;
                        }
                    }
                }
            }
        }

        private void SetBackUrl()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Source"]))
            {
                var source = HttpUtility.UrlDecode(Request.QueryString["Source"]).ToLowerInvariant();
                switch (source)
                {
                    case "mastercard-admin":
                        backToQueue.NavigateUrl = btnPrinterCancel.HRef = _urlService.GetUserOrderSearchURL("mastercard-admin", "header=User%2FOrder%20Search");
                        HookupBackToQueueLink();
                        break;
                    case "userprofileeditorhome":
                        backToQueue.NavigateUrl = btnPrinterCancel.HRef = _urlService.GetUserProfileEditorHomeURL();
                        break;
                    case "bundlereport":
                        backToQueue.NavigateUrl = btnPrinterCancel.HRef = _urlService.GetInsertsBundleReportURL();
                        break;
                    case "customizationqueue":
                        backToQueue.NavigateUrl = btnPrinterCancel.HRef = _urlService.GetCustomizationQueueURL();
                        break;
                    case "fulfillmentqueue":
                        backToQueue.NavigateUrl = btnPrinterCancel.HRef = _urlService.GetFulfillmentQueueURL();
                        break;
                    case "procadminmanageissuers":
                        backToQueue.NavigateUrl = btnPrinterCancel.HRef = _urlService.GetManageIssuersURL();
                        break;
                    case "procadminuserordersearch":
                        backToQueue.NavigateUrl = btnPrinterCancel.HRef = _urlService.GetUserOrderSearchURL("processor", "header=User%2FOrder%20Search&source=ProcAdminUserOrderSearch");
                        HookupBackToQueueLink();
                        break;
                    case "promotionreport":
                        backToQueue.NavigateUrl = btnPrinterCancel.HRef = _urlService.GetPromotionReportURL();
                        break;
                    default:
                        backToQueue.Visible = btnPrinterCancel.Visible = false;
                        break;
                }
            }
            else if (!string.IsNullOrEmpty(Request.QueryString["ReturnUrl"]))
            {
                backToQueue.NavigateUrl = btnPrinterCancel.HRef = Request.QueryString["ReturnUrl"].ToString().Trim();
            }
            else
            {
                backToQueue.NavigateUrl = btnPrinterCancel.HRef = Slam.Cms.Configuration.ConfigurationManager.Environment.FrontEndUrl.TrimEnd('/');
            }
        }

        protected void HookupBackToQueueLink()
        {
            backToQueue.Attributes.Add("onclick", $"window.top.location.href = '{backToQueue.NavigateUrl}'; return false;");
        }

        protected string FormatNumber(decimal num)
        {
            System.Globalization.NumberFormatInfo nf = new System.Globalization.NumberFormatInfo();
            nf.CurrencySymbol = "$";
            nf.NumberDecimalDigits = 2;
            return num.ToString("C", nf);
        }

        protected bool IsFileUploaded(object o)
        {
            bool breturn = false;
            if (o != null && !string.IsNullOrEmpty(o.ToString()))
                breturn = true;
            else if (Request.QueryString["orderID"] == null)
                breturn = true;
            return breturn;
        }

        protected string TruncateFile(object o)
        {
            string sreturn = string.Empty;
            if (o != null && !string.IsNullOrEmpty(o.ToString()) && o.ToString().Length > 0)
            {
                string[] filevalues = o.ToString().Split(new string[] { @"/" }, StringSplitOptions.None);
                if (filevalues.Length > 0)
                {
                    sreturn = @"../" + filevalues[filevalues.Length - 1];
                    if (sreturn.Length > 21)
                        sreturn = sreturn.Insert(20, " ");
                }
            }
            return sreturn;
        }

        protected bool IsProofPdfExits(object o)
        {
            bool breturn = false;
            if (o != null && !string.IsNullOrEmpty(o.ToString()) && o.ToString().Length > 0)
                breturn = true;

            return breturn;

        }

        private bool _isPrinter = false;
        private bool _isPrinterSet = false;
        protected bool IsPrinter()
        {
            bool isPrinter = _isPrinter;
            if (!_isPrinterSet)
            {
                _isPrinter = Page.User.IsInRole(Constants.Roles.Printer) || Page.User.IsInRole(Constants.Roles.DevAdmin) || Page.User.IsInRole(Constants.Roles.McAdmin);
                isPrinter = _isPrinter;
                _isPrinterSet = true;
            }

            return isPrinter;
        }

        protected void RetrieveShippingInformation(int Orderid)
        {
            List<RetrieveShippingInformationResult> ShippingDetail = _orderService.GetShippingInformation(Orderid);
            if (ShippingDetail != null)
            {
                rptShippingAddresses.DataSource = ShippingDetail;
                rptShippingAddresses.DataBind();
            }

            RetrieveOrderDetailResult orderDetail = _orderService.GetOrderDetails(Orderid);
            orderStatus = orderDetail.OrderStatus.ToLower();

            if (!string.IsNullOrEmpty(orderDetail.SpecialInstructions))
            {
                divspecialinstructions.Visible = true;
                lblSpecialInstructions.Text = orderDetail.SpecialInstructions.Replace("\r\n", "<br/>");
            }
            else
            {
                divspecialinstructions.Visible = false;
            }

            if (orderDetail.RushOrder)
            {
                ltRushOrder.Text = "<br /><br />" + Resources.ShoppingCart.RushOrderRequested.ToUpperInvariant();
            }
        }

        protected void rptShippingAddresses_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null && (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                RetrieveShippingInformationResult shippingInfo = e.Item.DataItem as RetrieveShippingInformationResult;
                Repeater rptInnerSubOrders = e.Item.FindControl("rptInnerSubOrders") as Repeater;
                LinkButton lnkJoinShipments = e.Item.FindControl("lnkJoinShipments") as LinkButton;
                int orderId = int.Parse(ViewState["OrderId"].ToString());

                List<RetrieveOrderSubOrdersResult> subOrderItems = _orderService.GetSubOrders(orderId, shippingInfo.ShippingInformationID);
                if (subOrderItems.Count > 0)
                {
                    rptInnerSubOrders.DataSource = subOrderItems;
                    rptInnerSubOrders.DataBind();

                    bool subOrderIsCompleted = false;
                    foreach (RetrieveOrderSubOrdersResult item in subOrderItems)
                    {
                        subOrderIsCompleted = item.Completed || subOrderIsCompleted;
                    }

                    lnkJoinShipments.Visible = subOrderItems.Count > 1 && !subOrderIsCompleted;
                    lnkJoinShipments.CommandArgument = orderId.ToString() + "," + shippingInfo.ShippingInformationID.ToString();
                }
            }
        }

        protected void rptShippingAddresses_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "JOIN")
            {
                int orderId = 0;
                int shippingInformationId = 0;

                string[] arguments = e.CommandArgument.ToString().Split(',');
                if (arguments.Length > 1)
                {
                    Int32.TryParse(arguments[0], out orderId);
                    Int32.TryParse(arguments[1], out shippingInformationId);

                    _orderService.JoinShipmentSubOrdersInOrder(orderId, shippingInformationId);
                }
                RetrieveShippingInformation(orderId);
            }
        }

        protected void rptInnerSubOrders_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "COMPLETE")
            {
                int subOrderId = 0;
                Int32.TryParse(e.CommandArgument.ToString(), out subOrderId);

                if (Page.RouteData.Values["orderid"] != null)
                {
                    Response.Redirect("~/completesuborder/" + subOrderId.ToString());
                }
                if (Request.QueryString["orderID"] != null)
                {
                    string referrer = Request.QueryString["Source"] != null ? "&Source=" + Request.QueryString["Source"] : "";
                    Response.Redirect("CompleteSubOrderNoChrome.aspx?subOrderId=" + subOrderId.ToString() + referrer);
                }

            }
            else if (e.CommandName == "OK")
            {
                int subOrderId = 0;
                Int32.TryParse(e.CommandArgument.ToString(), out subOrderId);

                Repeater rptModalOrderItems = e.Item.FindControl("rptModalOrderItems") as Repeater;

                string splitOrderItemIds = "";
                int selectedItems = 0;
                foreach (RepeaterItem item in rptModalOrderItems.Items)
                {
                    CheckBox chkSplitItem = item.FindControl("chkSplitItem") as CheckBox;
                    HiddenField hdnOrderItemId = item.FindControl("hdnOrderItemId") as HiddenField;

                    if (chkSplitItem.Checked)
                    {
                        splitOrderItemIds = hdnOrderItemId.Value + ",";
                        selectedItems++;
                    }
                }

                if (selectedItems > 0 && rptModalOrderItems.Items.Count > selectedItems)
                    _orderService.SplitSubOrder(subOrderId, splitOrderItemIds.TrimEnd(','));

                string orderID = string.Empty;

                if (ViewState["OrderId"] == null)
                {
                    if (Page.RouteData.Values["orderid"] != null)
                        orderID = _orderService.GetOrderIDByOrderNo(Page.RouteData.Values["orderid"].ToString()).ToString();

                    if (Request.QueryString["orderID"] != null)
                        orderID = _orderService.GetOrderIDByOrderNo(Request.QueryString["orderID"].ToString()).ToString();
                }
                else
                    orderID = ViewState["OrderId"].ToString();

                RetrieveShippingInformation(Int32.Parse(orderID));
            }
        }

        protected void rptInnerSubOrders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null && (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                RetrieveOrderSubOrdersResult subOrder = e.Item.DataItem as RetrieveOrderSubOrdersResult;

                Repeater rptInnerOrderItems = e.Item.FindControl("rptInnerOrderItems") as Repeater;
                Repeater rptModalOrderItems = e.Item.FindControl("rptModalOrderItems") as Repeater;

                HyperLink lnkSplitSubOrder = e.Item.FindControl("lnkSplitSubOrder") as HyperLink;

                Button btnCompleteSubOrder = e.Item.FindControl("btnCompleteSubOrder") as Button;
                HtmlGenericControl spanCompleteSubOrder = e.Item.FindControl("spanCompleteSubOrder") as HtmlGenericControl;
                HtmlGenericControl divFulfillmentOptions = e.Item.FindControl("divFulfillmentOptions") as HtmlGenericControl;

                Literal ltModalAddress = e.Item.FindControl("ltModalAddress") as Literal;
                Literal ltModalContactName = e.Item.FindControl("ltModalContactName") as Literal;
                Literal ltModalPhone = e.Item.FindControl("ltModalPhone") as Literal;

                Button btnModalOk = e.Item.FindControl("btnModalOk") as Button;
                Button btnModalCancel = e.Item.FindControl("btnModalCancel") as Button;

                btnModalOk.CommandArgument = subOrder.SubOrderID.ToString();
                btnCompleteSubOrder.CommandArgument = subOrder.SubOrderID.ToString();

                HtmlGenericControl modalItems = e.Item.FindControl("modalItems") as HtmlGenericControl;
                HtmlGenericControl splitModal = e.Item.FindControl("splitModal") as HtmlGenericControl;

                if (orderStatus == "fulfillment" || (orderStatus == "" && IsPostBack))
                {
                    lnkSplitSubOrder.Visible = !subOrder.Completed;
                    spanCompleteSubOrder.Visible = !subOrder.Completed;
                }
                else
                {
                    lnkSplitSubOrder.Visible = false;
                    spanCompleteSubOrder.Visible = false;
                    divFulfillmentOptions.Visible = subOrder.Completed;
                }

                btnModalOk.OnClientClick = @"
								var checkBoxes = $('#" + modalItems.ClientID + @" input:even'); 
								var checks = new Array(checkBoxes.length); 
								for(var i = 0; i < checkBoxes.length; i++) { checks[i] = checkBoxes[i].checked; } 
								$.unblockUI({ onUnblock:
									function()
									{   
										modalSelections = '';
									}
								}); 
								checkBoxes = $('#" + modalItems.ClientID + @" input:even');  
								for(var i = 0; i < checkBoxes.length; i++) { checkBoxes[i].checked = checks[i]; } ";

                btnModalOk.UseSubmitBehavior = false;

                btnModalCancel.OnClientClick = @"$.unblockUI({ onUnblock:
											function()
											{
												var checkBoxes = $('#" + modalItems.ClientID + @" input:even');
												var selections = modalSelections.split(',');
												for(var c = 0; c < checkBoxes.length; c++)
												{
													checkBoxes[c].checked = (selections[c] == 'true');
												}
												modalSelections = '';
											}
										}); return false";

                btnModalCancel.UseSubmitBehavior = false;
                btnModalCancel.CausesValidation = false;

                List<RetrieveSubOrderItemsResult> orderItems = _orderService.GetSubOrderItems(subOrder.SubOrderID);
                if (orderItems.Count > 0)
                {
                    rptInnerOrderItems.DataSource = orderItems;
                    rptInnerOrderItems.DataBind();

                    rptModalOrderItems.DataSource = orderItems;
                    rptModalOrderItems.DataBind();

                    if (lnkSplitSubOrder.Visible && orderItems.Count == 1)
                    {
                        lnkSplitSubOrder.Visible = false;
                    }
                }

                if (orderItems.Count == 2)
                    lnkSplitSubOrder.Attributes.Add("onclick", "var checkBoxes = $('#" + modalItems.ClientID + @" input:even'); checkBoxes[1].checked = true; " + Page.ClientScript.GetPostBackEventReference(btnModalOk, "") + "; return false;");
                else
                    lnkSplitSubOrder.Attributes.Add("onclick", "$.blockUI({ message: $('#" + splitModal.ClientID + @"') }); return false;");

                RepeaterItem shippingRepeaterItem = (RepeaterItem)e.Item.Parent.Parent;
                if (shippingRepeaterItem.DataItem != null)
                {
                    RetrieveShippingInformationResult shippingItem = (RetrieveShippingInformationResult)shippingRepeaterItem.DataItem;
                    ltModalAddress.Text = shippingItem.Address;
                    ltModalContactName.Text = shippingItem.ContactName;
                    ltModalPhone.Text = shippingItem.Phone;
                }
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            HtmlLink css = new HtmlLink();
            if (Request.QueryString["orderID"] != null)
            {
                css.Href = Slam.Cms.Configuration.ConfigurationManager.Environment.AdminUrl.TrimEnd('/') + "/_layouts/1033/STYLES/MarketingCenter.SharedControls.css";
                css.Attributes["rel"] = "stylesheet";
                css.Attributes["type"] = "text/css";
                css.Attributes["media"] = "all";
                this.Page.Header.Controls.Add(css);
            }

            chkApprove.Attributes.Add("onclick", "validateCorrection('" + chkFeedback.ClientID + "','" + chkApprove.ClientID + "','" + txtFeedback.ClientID + "','" + btnSubmit.ClientID + "');");
            chkFeedback.Attributes.Add("onclick", "validateCorrection('" + chkFeedback.ClientID + "','" + chkApprove.ClientID + "','" + txtFeedback.ClientID + "','" + btnSubmit.ClientID + "');");
            txtpriceadjustment.Attributes.Add("onkeydown", "return ForceNumericInput(event,true,true);");

            btnSaveAdjustment.Click += new EventHandler(btnSaveAdjustment_Click);
            rptItems.ItemCommand += new RepeaterCommandEventHandler(rptItems_ItemCommand);
            rptItems.ItemDataBound += new RepeaterItemEventHandler(rptItems_ItemDataBound);
            rptItems.ItemCreated += new RepeaterItemEventHandler(rptItems_ItemCreated);
            btnPrinterComplete.Click += new EventHandler(bnSubmit_Click);

            bool addJqueryReference = true;
            string jqueryInclude = string.Format("<script type=\"text/javascript\" language=\"javascript\" src=\"{0}\"></script>", Slam.Cms.Configuration.ConfigurationManager.Environment.AdminUrl.TrimEnd('/') + "/_layouts/inc/jquery-1.2.6.min.js");
            foreach (Control control in Page.Header.Controls)
            {
                if (control is LiteralControl && ((LiteralControl)control).Text == jqueryInclude)
                {
                    addJqueryReference = false;
                }
            }

            if (addJqueryReference)
            {
                Page.Header.Controls.Add(new LiteralControl(jqueryInclude));
            }

            bool addBlockUiReference = true;
            string blockUiInclude = string.Format("<script type=\"text/javascript\" language=\"javascript\" src=\"{0}\"></script>", Slam.Cms.Configuration.ConfigurationManager.Environment.AdminUrl.TrimEnd('/') + "/_layouts/inc/jquery.blockUI.js");
            foreach (Control control in Page.Header.Controls)
            {
                if (control is LiteralControl && ((LiteralControl)control).Text == blockUiInclude)
                {
                    addBlockUiReference = false;
                }
            }

            if (addBlockUiReference)
            {
                Page.Header.Controls.Add(new LiteralControl(blockUiInclude));
            }
        }

        protected void btnSaveAdjustment_Click(object sender, EventArgs e)
        {
            if (ValidateCost())
            {
                decimal? PriceAdjustment = null;
                decimal Adjustment = 0;
                if (txtpriceadjustment.Text.Trim().Length > 0 && !decimal.TryParse(txtpriceadjustment.Text.Trim(), out Adjustment))
                {
                    divError.Visible = true;
                    lblError.Text = Resources.Errors.PriceAjustmentInvalid;
                    return;
                }
                else
                    PriceAdjustment = Adjustment;

                string orderID = string.Empty;

                if (ViewState["OrderId"] == null)
                {
                    if (Page.RouteData.Values["orderid"] != null)
                        orderID = _orderService.GetOrderIDByOrderNo(Page.RouteData.Values["orderid"].ToString()).ToString();

                    if (Request.QueryString["orderID"] != null)
                        orderID = _orderService.GetOrderIDByOrderNo(Request.QueryString["orderID"].ToString()).ToString();
                }
                else
                    orderID = ViewState["OrderId"].ToString();

                divError.Visible = false;
                if (!string.IsNullOrEmpty(orderID) && orderID != "0")
                {
                    int Order = int.Parse(orderID);
                    _orderService.UpdatePriceAdjustment(Order, PriceAdjustment, 0, 0, txtDescription.Text.Trim());

                    Response.Redirect(Request.Url.AbsoluteUri);
                }
            }
        }

        protected void rptItems_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null && (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                if (ScriptManager.GetCurrent(this.Page) == null)
                {
                    FileUpload upload = e.Item.FindControl("UploadProofFile") as FileUpload;
                    e.Item.Controls.Remove(upload);
                }
            }
        }

        protected void rptItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item != null && (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                RetrieveOrderItemsResult orderItem = e.Item.DataItem as RetrieveOrderItemsResult;
                HyperLink fileLink = e.Item.FindControl("lnkProofFile") as HyperLink;
                if (ScriptManager.GetCurrent(this.Page) != null)
                {
                    FileUpload upload = e.Item.FindControl("UploadProofFile") as FileUpload;
                    upload.Visible = true;
                    fileLink.Visible = false;
                }
                else
                {
                    if (orderItem.ProofPDF != null)
                    {
                        fileLink.Visible = true;
                        fileLink.Text = orderItem.ProofPDF.Substring(orderItem.ProofPDF.LastIndexOf("/") + 1);
                        fileLink.NavigateUrl = _urlService.GetProofFileURL(orderItem.ProofPDF);
                    }
                }

                if (fileLink != null)
                {
                    if (fileLink.Visible && !string.IsNullOrEmpty(fileLink.Text) && mode == "edit")
                    {
                        HyperLink lnkReplaceProofFile = e.Item.FindControl("lnkReplaceProofFile") as HyperLink;
                        FileUpload UploadProofFile = e.Item.FindControl("UploadProofFile") as FileUpload;

                        if (lnkReplaceProofFile != null && UploadProofFile != null)
                        {
                            lnkReplaceProofFile.Visible = true;
                            lnkReplaceProofFile.Attributes.Add("onclick", "ShowProofUpload('" + lnkReplaceProofFile.ClientID + "','" + fileLink.ClientID + "','" + UploadProofFile.ClientID + "')");
                        }
                    }
                }

                HtmlGenericControl divRowItemTemplate = e.Item.FindControl("divRowItemTemplate") as HtmlGenericControl;
                if (divRowItemTemplate != null)
                {
                    if (e.Item.ItemIndex % 2 == 0)
                    {
                        divRowItemTemplate.Attributes.Add("class", "chk_outgrid");
                    }
                    else
                    {
                        divRowItemTemplate.Attributes.Add("class", "chk_outgrid gray_backgrnd");
                    }
                }

                Image imgIcon = (Image)e.Item.FindControl("imgIcon");
                Literal litIcon = (Literal)e.Item.FindControl("litIcon");

                if (orderItem.ElectronicDelivery)
                {
                    imgIcon.ImageUrl = "/portal/inc/images/document_down.png";
                    litIcon.Text = Resources.ShoppingCart.ElectronicDelivery;
                }
                else
                {
                    if (orderItem.VDP)
                    {
                        imgIcon.ImageUrl = "/portal/inc/images/vdp.png";
                        litIcon.Text = Resources.ShoppingCart.VariableDataPrinting;
                    }
                    else
                    {
                        if (Convert.ToBoolean(orderItem.Customizations))
                        {
                            imgIcon.ImageUrl = "/portal/inc/images/document_edit.png";
                            litIcon.Text = Resources.ShoppingCart.Customizable;
                        }
                        else
                        {
                            imgIcon.ImageUrl = "/portal/inc/images/document_lock.png";
                            litIcon.Text = Resources.ShoppingCart.MastercardBrandedCollateral;
                        }
                    }
                }
            }
        }

        protected string[] FormatString(string ItemName)
        {
            int NoOFWords = 0;
            NoOFWords = WordCount(ItemName.ToString());
            string[] Items = new string[NoOFWords / 4 + 1];
            string New_Item = string.Empty;

            if (NoOFWords > 4)
            {
                string[] words = ItemName.ToString().Split(' ');

                for (int i = 0; i <= NoOFWords - 1; i++)
                {
                    if (i % 4 == 0 && i >= 4)
                        New_Item += System.Environment.NewLine;
                    else
                    {
                        if (i == 0)
                            New_Item += words[i].ToString();
                        else
                            New_Item += ' ' + words[i].ToString();
                    }
                }
                Items = New_Item.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            }
            else
                Items.SetValue(ItemName, 0);

            return Items;
        }

        public static int WordCount(string Text)
        {
            string tmpStr;

            tmpStr = Text.Replace("\t", " ").Trim();
            tmpStr = tmpStr.Replace("\n", " ");
            tmpStr = tmpStr.Replace("\r", " ");

            while (tmpStr.IndexOf("  ") != -1)
                tmpStr = tmpStr.Replace("  ", " ");

            return tmpStr.Split(' ').Length;
        }

        void rptItems_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName != null)
            {
                if (e.CommandName.ToLower() == "replace")
                {
                    PlaceHolder placeHolderEdit = (PlaceHolder)e.Item.FindControl("placeHolderEdit");
                    PlaceHolder placeholderFiledExits = (PlaceHolder)e.Item.FindControl("placeholderFiledExits");
                    placeholderFiledExits.Visible = false;
                    placeHolderEdit.Visible = true;
                }
            }
        }

        protected void RetrieveCustomisationDetails(int Orderid)
        {
            List<RetrieveCustomizationsResult> ls = _orderService.GetCustomizationOptions(Orderid);
            if (ls.Count > 0)
            {
                rptCustomizations.DataSource = ls;
                rptCustomizations.DataBind();
            }
            placeCustomizationDetails.Visible = ls.Count > 0;
            tabCustomizationDetails.Visible = placeCustomizationDetails.Visible;
        }

        protected void RetrieveOrderLog(int OrderId)
        {
            List<RetrieveOrderLogResult> OrderItems = _orderService.GetOrderLog(OrderId);
            if (OrderItems != null)
            {
                rptOrderHistory.DataSource = OrderItems;
                rptOrderHistory.DataBind();
            }
        }

        protected void RetrieveOrderItems(int OrderId)
        {
            List<RetrieveOrderItemsResult> OrderItems = _orderService.GetOrderItems(OrderId);

            if (OrderItems != null)
            {
                rptItems.DataSource = OrderItems;
                rptItems.DataBind();

                var p = (from o in OrderItems
                         select o.ItemPrice).Sum();

                _totalCost = _totalCost + Convert.ToDecimal(p);

                RetrievePromotionForOrderResult promotion = _orderService.GetPromotion(OrderId);
                if (promotion != null)
                {
                    litPromotionDescription.Text = promotion.PromotionDescription;
                    litPromotionAmount.Text = String.Format("$-{0}", Convert.ToDecimal(promotion.PromotionAmount).ToString("N2"));
                    pnlPromotion.Visible = true;

                    _totalCost -= Convert.ToDecimal(promotion.PromotionAmount);
                }

                litTotal.Text = string.Format(System.Globalization.CultureInfo.GetCultureInfo("en-US"), "{0:C}", _totalCost);
            }
        }

        protected void RetrieveOrderDetails(int OrderId)
        {
            RetrieveOrderDetailResult OrderItems = _orderService.GetOrderDetails(OrderId);

            if (OrderItems != null)
            {
                if (Request.QueryString["orderID"] == null && !ValidUser(OrderItems.UserId.ToString()) && !IsUserIsPrinter())
                {
                    Response.Redirect("/portal/error/accesdenied");
                }

                ViewState["OrderNumber"] = OrderItems.OrderNumber.ToString();
                lblOrderNo.Text = Resources.ShoppingCart.Order + " #         " + OrderItems.OrderNumber.ToString();
                lblOrderDate.Text = Resources.Shared.Date + "          " + OrderItems.OrderDate.Value.ToString("MM/dd/yyyy");
                lblStatustitle.Text = OrderItems.OrderStatus.ToString().Replace("Shipped", "Shipped/Fulfilled");

                if (OrderItems.PriceAdjustment != null)
                {
                    lblpriceadjustment.Text = FormatNumber(Convert.ToDecimal(OrderItems.PriceAdjustment.Value.ToString()));
                    txtpriceadjustment.Text = OrderItems.PriceAdjustment.Value.ToString();
                    _totalCost = _totalCost + Convert.ToDecimal(OrderItems.PriceAdjustment.Value.ToString());
                }
                if (OrderItems.Description != null)
                {
                    lblDescription.Text = OrderItems.Description;
                    txtDescription.Text = OrderItems.Description;
                }

                plcUserOptions.Visible = false;
                orderStatus = OrderItems.OrderStatus.ToLower();
                switch (orderStatus)
                {
                    case "pending customization":
                    case "pending correction":
                        if (Request.QueryString["orderID"] != null)
                        {
                            divcostadjustment.Visible = false;
                            plcUserOptions.Visible = false;
                            Approvaldiv.Visible = true;
                            plcPrinter.Visible = true;
                            plcUser.Visible = false;
                        }
                        else
                        {
                            Approvaldiv.Visible = false;
                            plcPrinter.Visible = false;
                            plcUser.Visible = false;
                        }
                        break;
                    case "shipped":
                        divcostadjustment.Visible = !String.IsNullOrEmpty(lblpriceadjustment.Text);
                        plcUserOptions.Visible = false;
                        Approvaldiv.Visible = false;
                        lblpriceadjustment.Visible = true;
                        spanSaveAdjustment.Visible = false;
                        txtpriceadjustment.Visible = false;
                        lblDescription.Visible = true;
                        txtDescription.Visible = false;
                        plcPrinter.Visible = false;
                        plcUser.Visible = false;
                        break;
                    case "fulfillment":
                        if (Request.QueryString["orderID"] != null)
                        {
                            divcostadjustment.Visible = true;
                            lblpriceadjustment.Visible = false;
                            txtpriceadjustment.Visible = true;
                            lblDescription.Visible = false;
                            txtDescription.Visible = true;
                            plcUserOptions.Visible = false;
                            Approvaldiv.Visible = false;
                            plcPrinter.Visible = false;
                            plcUser.Visible = false;
                        }
                        else
                        {
                            divcostadjustment.Visible = false;
                            plcUserOptions.Visible = false;
                            Approvaldiv.Visible = false;
                            lblpriceadjustment.Visible = true;
                            spanSaveAdjustment.Visible = false;
                            txtpriceadjustment.Visible = false;
                            lblDescription.Visible = true;
                            txtDescription.Visible = false;
                            Approvaldiv.Visible = false;
                            plcPrinter.Visible = false;
                            plcUser.Visible = false;
                        }
                        break;
                    case "pending approval":
                        if (Request.QueryString["orderID"] == null)
                        {
                            if (ValidUser(OrderItems.UserId.ToLower()))
                            {
                                divcostadjustment.Visible = false;
                                plcUserOptions.Visible = true;
                                Approvaldiv.Visible = true;
                                plcPrinter.Visible = false;
                                plcUser.Visible = true;
                            }
                            else if (IsUserIsPrinter())
                            {
                                divcostadjustment.Visible = false;
                                plcUserOptions.Visible = false;
                                Approvaldiv.Visible = false;
                                lblpriceadjustment.Visible = true;
                                txtpriceadjustment.Visible = false;
                                lblDescription.Visible = true;
                                txtDescription.Visible = false;
                                plcPrinter.Visible = false;
                                plcUser.Visible = false;
                            }
                        }
                        else
                        {
                            divcostadjustment.Visible = false;
                            plcUserOptions.Visible = false;
                            Approvaldiv.Visible = false;
                            lblpriceadjustment.Visible = true;
                            spanSaveAdjustment.Visible = false;
                            txtpriceadjustment.Visible = false;
                            lblDescription.Visible = true;
                            txtDescription.Visible = false;
                            plcPrinter.Visible = false;
                            plcUser.Visible = false;
                        }
                        break;
                }
                btnPrinterComplete.Enabled = false;
            }
        }

        private bool ValidUser(string userId)
        {
            bool breturn = false;
            if (!userId.IsNullOrWhiteSpace() &&
                userId.Equals(_userContext.User.UserName, StringComparison.InvariantCultureIgnoreCase) ||
                Page.User.IsInRole(Constants.Roles.McAdmin) ||
                Page.User.IsInRole(Constants.Roles.DevAdmin))
            {
                breturn = true;
            }

            return breturn;
        }

        private bool IsUserIsPrinter()
        {
            bool breturn = false;
            if (Page.User.IsInRole(Constants.Roles.Printer))
            {
                breturn = true;
            }

            return breturn;
        }

        private bool UploadFile(int OrderID, string ItemID, FileUpload fupload)
        {
            bool breturn = false;

            if (fupload.Value != null && fupload.Value.Trim().Length > 0)
            {
                _orderService.UpdateItem(OrderID, ItemID, fupload.Value);
                breturn = true;
            }
            return breturn;
        }

        protected string ParseLogUser(string UserId)
        {
            string sreturn = UserId;
            try
            {
                var user = _userService.GetUserByUserName(UserId);
                if (user != null)
                {
                    sreturn = user.FullName;
                }
            }
            catch { }

            return sreturn;
        }

        ///Fires when both types (printer,normal user,mcadmin) user click on submit button   
        protected void bnSubmit_Click(object sender, EventArgs e)
        {
            bool isPrinter = false;
            string OrderNO = string.Empty;

            //get order Number if the user is from the portal           
            if (Page.RouteData.Values["orderid"] != null)
            {
                OrderNO = Page.RouteData.Values["orderid"].ToString().ToString();
            }

            //get order Number if the user is from the printer side
            if (Request.QueryString["orderID"] != null)
            {
                isPrinter = true;
                OrderNO = Request.QueryString["orderID"];
            }
            if (string.IsNullOrEmpty(OrderNO))
            {
                return;
            }

            //get orderid  from orderNo.
            int Order = _orderService.GetOrderIDByOrderNo(OrderNO);

            //save the orderItem proof Pdf's 

            //if user is printer and has items to upload
            if (rptItems.Items.Count > 0 && isPrinter)
            {
                foreach (RepeaterItem item in rptItems.Items)
                {
                    FileUpload fupload = (FileUpload)item.FindControl("UploadProofFile");
                    Label lblItemId = (Label)item.FindControl("lblItemID");
                    if (fupload != null && lblItemId.Text.Length > 0)
                    {
                        UploadFile(Order, lblItemId.Text, fupload);
                    }
                }
            }

            string strHostName = System.Net.Dns.GetHostName();
            string clientIPAddress = System.Net.Dns.GetHostAddresses(strHostName).GetValue(0).ToString();

            RetrieveOrderDetailResult orderDetail = _orderService.GetOrderDetails(Order);
            var user = _userService.GetUserByUserName(orderDetail.UserId);
            var userId = _userContext.User?.UserId ?? 0;
            // enter data into orderProcessNote and the update order 
            switch (lblStatustitle.Text.ToLower())
            {
                case "pending customization":
                    if (orderDetail.OrderStatus.ToLower() != "pending approval")
                    {
                        if (Request.QueryString["orderID"] != null)
                        {
                            _orderService.SetOrderStatus(Order, "Pending Approval", "Proof Ready<br/>" + txtFeedback.Text.Trim(), clientIPAddress, _userContext.User.UserName);

                            _emailService.SendProofEmail(OrderNO, user.Email, user.Profile.FirstName, user.Profile.LastName, userId);

                            Response.Redirect(this.Request.Url.AbsoluteUri);

                            return;
                        }
                    }
                    break;
                case "pending correction":
                    if (_orderService.GetOrderStatus(OrderNO).ToLower() != "pending approval")
                    {
                        if (Request.QueryString["orderID"] != null)
                        {
                            _orderService.SetOrderStatus(Order, "Pending Approval", "Proof Ready Again<br/>" + txtFeedback.Text.Trim(), clientIPAddress, _userContext.User.UserName);

                            _emailService.SendProofEmail(OrderNO, user.Email, user.Profile.FirstName, user.Profile.LastName, userId);
                            Response.Redirect(Request.Url.AbsoluteUri);
                            return;
                        }
                    }
                    break;
                case "fulfillment":
                    break;
                case "pending approval":
                    if (chkApprove.Checked)
                    {
                        if (_orderService.GetOrderStatus(OrderNO).ToLower() != "fulfillment")
                        {
                            _emailService.SendProofApprovedEmail(OrderNO, userId);

                            _orderService.SetOrderStatus(Order, "Fulfillment", "Proof Approved<br/>" + txtFeedback.Text.Trim(), clientIPAddress, _userContext.User.UserName);
                        }
                    }
                    else
                    {
                        if (_orderService.GetOrderStatus(OrderNO).ToLower() != "pending correction")
                        {
                            _emailService.SendPendingCorrectionEmail(OrderNO, userId);

                            _orderService.SetOrderStatus(Order, "Pending Correction", txtFeedback.Text.Trim(), clientIPAddress, _userContext.User.UserName);
                        }
                    }
                    break;
            }
            Response.Redirect(this.Request.Url.AbsoluteUri);
            return;
        }

        protected bool ValidateCost()
        {
            divError.Visible = true;
            string strError = string.Empty;

            if (txtpriceadjustment.Text.Trim().Length > 0)
            {
                if (txtDescription.Text.Trim().Length <= 0)
                    strError += Resources.Errors.DescriptionEmpty;
            }
            else
            {
                strError += Resources.Errors.DescriptionEmpty;
                strError += Resources.Errors.AdjustmentEmpty;
            }

            if (strError != string.Empty)
            {
                divError.Visible = true;
                lblError.Text = strError;
                lblError.Visible = true;
                return false;
            }
            else
            {
                divError.Visible = false;
                lblError.Visible = false;
                return true;
            }
        }

        protected string GetPrice(string price)
        {
            string sretun = string.Empty;
            if (price.Length > 0)
            {
                decimal dprice = decimal.Parse(price);
                total = total + dprice;

                sretun = FormatNumber(dprice);
            }
            return sretun;
        }

        protected TextBoxMode GetTextBoxMode(string FieldType)
        {
            TextBoxMode treturn = TextBoxMode.SingleLine;
            switch (FieldType.ToLower())
            {
                case "multi line text":
                    treturn = TextBoxMode.MultiLine;
                    break;
                default:
                    break;
            }
            return treturn;
        }

        /*Enhancement Starts*/

        private void SetStatusInDropdown()
        {
            ddlStatus.Items.Clear();
            if (lblStatustitle.Text.ToLower() != "pending approval")
                ddlStatus.Items.Add("Pending Approval");
            if (lblStatustitle.Text.ToLower() != "pending customization")
                ddlStatus.Items.Add("Pending Customization");
            if (lblStatustitle.Text.ToLower() != "pending correction")
                ddlStatus.Items.Add("Pending Correction");
            if (lblStatustitle.Text.ToLower() != "fulfillment")
                ddlStatus.Items.Add("Fulfillment");
            if (lblStatustitle.Text.ToLower() != "shipped" && lblStatustitle.Text.ToLower() != "shipped/fulfilled")
                ddlStatus.Items.Add(new ListItem("Shipped/Fulfilled", "Shipped"));
            if (lblStatustitle.Text.ToLower() != "cancelled")
                ddlStatus.Items.Add("Cancelled");
        }

        protected void OverrideStatusOK_Click(object sender, EventArgs e)
        {
            string orderID = string.Empty;
            if (Page.RouteData.Values["orderid"] != null)
            {
                orderID = _orderService.GetOrderIDByOrderNo(Page.RouteData.Values["orderid"].ToString()).ToString();
            }

            if (Request.QueryString["orderID"] != null)
            {
                orderID = _orderService.GetOrderIDByOrderNo(Request.QueryString["orderID"].ToString()).ToString();
            }

            int Orderid = int.Parse(orderID);

            //Updation Code for Customization
            string strHostName = System.Net.Dns.GetHostName();
            string clientIPAddress = System.Net.Dns.GetHostAddresses(strHostName).GetValue(0).ToString();
            string feedBack = Resources.ShoppingCart.StatusChangedTo + " " + ddlStatus.SelectedItem.Text;
            if (!string.IsNullOrEmpty(txtNote.Text))
            {
                feedBack += string.Format("\n{0}: {1}", Resources.ShoppingCart.Note.ToUpperInvariant(), txtNote.Text);
            }

            _orderService.SetOrderStatus(Orderid, ddlStatus.SelectedItem.Value, feedBack, clientIPAddress, _userContext.User.UserName);

            string orderNo = "";
            RetrieveOrderDetailResult orderInfo = _orderService.GetOrderDetails(Orderid);
            if (orderInfo != null)
            {
                orderNo = orderInfo.OrderNumber;
            }

            RetrieveOrderDetailResult orderDetail = _orderService.GetOrderDetails(Orderid);
            string username = orderDetail.UserId.Substring(orderDetail.UserId.IndexOf(":") + 1);
            MembershipUser user = MembershipService.GetUser(username);
            var userId = _userContext.User?.UserId ?? 0;
            if (chkNotifyCustomer.Checked)
            {
                _emailService.SendOrderStatusEmail(user.Email, orderNo, lblStatustitle.Text, ddlStatus.SelectedItem.Text, txtNote.Text, userId);
            }

            if (chkNotifyPrinterAdmin.Checked)
            {
                _emailService.SendOrderStatusEmailForPrinters(orderNo, lblStatustitle.Text, ddlStatus.SelectedItem.Text, txtNote.Text, userId);
            }

            ReloadPage();
        }

        protected bool ReturnVisibilityForCustomizationsImage(string FieldType, string VisibilityMode)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["mode"]))
            {
                mode = Request.QueryString["mode"];
            }

            if (FieldType.ToLower() == "image")
            {
                if (mode == "edit" && VisibilityMode == "edit")
                    return true;
                else if (mode != "edit" && VisibilityMode == "readonly")
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        protected bool ReturnVisibilityForCustomizationsOptionValue(string FieldType, string VisibilityMode)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["mode"]))
                mode = Request.QueryString["mode"];

            if (FieldType.ToLower() == "image")
                return false;
            else
            {
                if (mode == "edit" && VisibilityMode == "edit")
                    return true;
                else if (mode != "edit" && VisibilityMode == "readonly")
                    return true;
                else
                    return false;
            }
        }

        protected void CustomizationEditLink_Click(object sender, EventArgs e)
        {
            string redirectUrl = Request.RawUrl;
            if (redirectUrl.Contains("?"))
                redirectUrl += "&mode=edit";
            else
                redirectUrl += "?mode=edit";

            Response.Redirect(redirectUrl);
        }

        protected void CustomizationCancel_Click(object sender, EventArgs e)
        {
            ReloadPage();
        }

        private void ReloadPage()
        {
            string redirectUrl = Request.RawUrl;
            redirectUrl = redirectUrl.Replace("&mode=edit", "");
            redirectUrl = redirectUrl.Replace("?mode=edit", "");
            Response.Redirect(redirectUrl);
        }

        protected int GetCartId()
        {
            int cartId = 0;
            if (ViewState["CartId"] != null)
            {
                cartId = int.Parse(ViewState["CartId"].ToString());
            }

            return cartId;
        }

        protected void CustomizationSave_Click(object sender, EventArgs e)
        {
            string orderID = string.Empty;

            if (Page.RouteData.Values["orderid"] != null)
                orderID = _orderService.GetOrderIDByOrderNo(Page.RouteData.Values["orderid"].ToString()).ToString();

            if (Request.QueryString["orderID"] != null)
                orderID = _orderService.GetOrderIDByOrderNo(Request.QueryString["orderID"].ToString()).ToString();

            int Orderid = int.Parse(orderID);

            //Updation Code for Customization
            string strHostName = System.Net.Dns.GetHostName();
            string clientIPAddress = System.Net.Dns.GetHostAddresses(strHostName).GetValue(0).ToString();

            foreach (RepeaterItem item in rptCustomizations.Items)
            {
                HiddenField hdnCustomizationOptionID = item.FindControl("hdnCustomizationOptionID") as HiddenField;
                ImageUpload uploadImage = item.FindControl("uploadImage") as ImageUpload;
                TextBox txtOptionsValueEdit = item.FindControl("txtOptionsValueEdit") as TextBox;
                HiddenField hdnIsRequired = item.FindControl("hdnIsRequired") as HiddenField;

                //Validationg Required field
                if (hdnIsRequired != null && txtOptionsValueEdit != null)
                {
                    if (hdnIsRequired.Value.ToLower() == "true" && string.IsNullOrEmpty(txtOptionsValueEdit.Text))
                    {
                        Literal litErrMsg = item.FindControl("litErrMsg") as Literal;
                        HiddenField hdnFriendlyName = item.FindControl("hdnFriendlyName") as HiddenField;
                        if (litErrMsg != null && hdnFriendlyName != null)
                        {
                            litErrMsg.Text = "<br/>" + hdnFriendlyName.Value + " is required.";
                        }

                        return;
                    }
                }

                if (uploadImage != null && uploadImage.Visible && !string.IsNullOrEmpty(uploadImage.Value))
                {
                    _orderService.UpdateCustomizationDetails(Orderid, GetCartId(), hdnCustomizationOptionID.Value, uploadImage.Value, clientIPAddress, _userContext.User.UserName);
                }
                else
                {
                    _orderService.UpdateCustomizationDetails(Orderid, GetCartId(), hdnCustomizationOptionID.Value, txtOptionsValueEdit?.Text, clientIPAddress, _userContext.User.UserName);
                }
            }

            _orderService.SaveOrderProcessNotesCustomizationDetails(Orderid, clientIPAddress, _userContext.User.UserName);

            //Reload the Page
            ReloadPage();
        }

        protected void rptCustomizations_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string FieldType = DataBinder.Eval(e.Item.DataItem, "FieldType", "");

                PlaceHolder PlaceHolderimg = e.Item.FindControl("PlaceHolderimg") as PlaceHolder;
                if (PlaceHolderimg != null)
                    PlaceHolderimg.Visible = ReturnVisibilityForCustomizationsImage(FieldType, "readonly");

                PlaceHolder phImgEdit = e.Item.FindControl("phImgEdit") as PlaceHolder;
                if (phImgEdit != null)
                    phImgEdit.Visible = ReturnVisibilityForCustomizationsImage(FieldType, "edit");

                PlaceHolder PlaceHoldertxt = e.Item.FindControl("PlaceHoldertxt") as PlaceHolder;
                if (PlaceHoldertxt != null)
                    PlaceHoldertxt.Visible = ReturnVisibilityForCustomizationsOptionValue(FieldType, "readonly");

                PlaceHolder phTextEdit = e.Item.FindControl("phTextEdit") as PlaceHolder;
                if (phTextEdit != null)
                {
                    phTextEdit.Visible = ReturnVisibilityForCustomizationsOptionValue(FieldType, "edit");

                    TextBox txtOptionsValueEdit = phTextEdit.FindControl("txtOptionsValueEdit") as TextBox;
                    if (txtOptionsValueEdit != null)
                    {
                        string MaxLength = DataBinder.Eval(e.Item.DataItem, "MaxLength", "");
                        if (!string.IsNullOrEmpty(MaxLength))
                            txtOptionsValueEdit.MaxLength = Convert.ToInt32(MaxLength);

                        if (FieldType.ToLower() == "multi line text")
                        {
                            txtOptionsValueEdit.TextMode = TextBoxMode.MultiLine;
                            txtOptionsValueEdit.Rows = 5;
                        }
                    }
                }

                if (phImgEdit != null)
                {
                    ImageUpload uploadImage = phImgEdit.FindControl("uploadImage") as ImageUpload;
                    if (uploadImage != null)
                        uploadImage.Editable = true;
                }

                HtmlGenericControl divItemTemplate = e.Item.FindControl("divItemTemplate") as HtmlGenericControl;
                if (divItemTemplate != null)
                {
                    if (e.Item.ItemIndex % 2 == 0)
                        divItemTemplate.Attributes.Add("class", "chk_outgrid");
                    else
                        divItemTemplate.Attributes.Add("class", "chk_outgrid gray_backgrnd");
                }
            }
            else if (e.Item.ItemType == ListItemType.Footer)
            {
                HtmlGenericControl divEditButtonContainer = e.Item.FindControl("divEditButtonContainer") as HtmlGenericControl;
                if (divEditButtonContainer != null)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["mode"]))
                        mode = Request.QueryString["mode"];
                    divEditButtonContainer.Visible = mode == "edit" ? true : false;
                }
            }
        }
    }
}