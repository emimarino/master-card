﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Common;
using Slam.Cms.Configuration;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.UserControls
{
    public partial class UserProfile : System.Web.UI.UserControl
    {
        public string Region { get; set; }
        private string _userName;
        public string UserName
        {
            get
            {
                if (string.IsNullOrEmpty(_userName))
                {
                    if (Request.QueryString["loginid"] != null)
                    {
                        _userName = Request.QueryString["loginid"];
                        if (_userName.IndexOf(',') > 0 && _userName.Split(',').Length > 1)
                        {
                            _userName = _userName.Split(',')[0];
                        }
                    }
                    else
                    {
                        _userName = _userContext.User.UserName;
                    }
                }

                return _userName;
            }
        }
        private Data.Entities.User _user;
        public Data.Entities.User User
        {
            get
            {
                if (_user == null)
                {
                    _user = _userService.GetUserByUserName(UserName);
                }

                return _user;
            }
        }
        public UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
        public LocalRes Res { get { return new LocalRes(this); } }
        public IUserService _userService { get { return DependencyResolver.Current.GetService<IUserService>(); } }
        public ITagService _tagService { get { return DependencyResolver.Current.GetService<ITagService>(); } }
        public IIssuerService _issuerService { get { return DependencyResolver.Current.GetService<IIssuerService>(); } }
        public IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }
        public IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }
        protected IUnitOfWork _unitOfWork { get { return DependencyResolver.Current.GetService<IUnitOfWork>(); } }
        public IAuthorizationService _authorizationService { get { return DependencyResolver.Current.GetService<IAuthorizationService>(); } }
        IRegionService _regionService { get { return DependencyResolver.Current.GetService<IRegionService>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Region) && (!string.IsNullOrEmpty(Request.QueryString["loginid"]) || _userContext.User != null))
            {
                var user = _userContext.User != null ? _userContext.User : _userService.GetUserByUserName(Request.QueryString["loginid"]?.ToString());
                Region = user.Region.IsNullOrEmpty() ? RegionalizeService.DefaultRegion : user.Region.ToString();
            }

            if (!IsPostBack)
            {
                HookupCancelButton();
                FillStates();
                if (Request.QueryString["loginid"] != null)
                {
                    if (Context.User.IsInAdminRole())
                    {
                        string username = Request.QueryString["loginid"];
                        if (username.IndexOf(",") > 0 && username.Split(',').Length > 1)
                        {
                            username = username.Split(',')[0];
                        }

                        MembershipUser user = MembershipService.GetUser(username);
                        if (user != null)
                        {
                            username = user.UserName;
                            AdminInformation(username);
                            setCommonInformation(username);
                            GetOrderItemsHistory(_userService.GetUserByUserName(username).UserName);
                            orderdiv.Visible = true;

                            var userKeyService = DependencyResolver.Current.GetService<IUserKeyService>();
                            var userName = $"{Membership.Provider.Name}:{user.UserName}";
                            var userKey = userKeyService.GetByUserNameAndStatus(userName, MarketingCenterDbConstants.UserKeyStatus.Available);

                            if (userKey == null)
                            {
                                lblPassword.InnerText = Resources.Forms.ResetPasswordUrl;
                                linkPassword.Text = Resources.Forms.NotApply;
                                linkPassword.NavigateUrl = string.Empty;
                                linkPassword.Visible = false;
                                lblPasswordNA.Text = Resources.Forms.NotApply;
                                lblPasswordNA.Visible = true;
                            }
                            else
                            {
                                lblPassword.InnerText = Resources.Forms.ResetPasswordUrl;
                                linkPassword.Text = Path.Combine(ConfigurationManager.Environment.FrontEndUrl, $"resetpassword/{userKey.ToString()}");
                                linkPassword.NavigateUrl = Path.Combine(ConfigurationManager.Environment.FrontEndUrl, $"resetpassword/{userKey.ToString()}");
                            }

                            if (Context.User.IsInRole(Constants.Roles.DevAdmin) || Context.User.IsInRole(Constants.Roles.McAdmin))
                            {
                                placeHolderAdmin.Visible = true;
                                SetAdminAccess(username);
                            }
                        }
                    }
                    else
                    {
                        Response.Redirect(_urlService.GetInvalidLinkURL());
                    }
                }
                else
                {
                    setCommonInformation(_userContext.User.UserName);

                    lblPassword.InnerText = Resources.Forms.Password;
                    linkPassword.Text = Resources.Forms.ChangePassword;
                    linkPassword.NavigateUrl = Path.Combine(ConfigurationManager.Environment.FrontEndUrl, "changepassword");

                    divAccountInformation.Visible = false;
                    orderdiv.Visible = false;
                }
            }
        }

        private void SetAdminAccess(string LoginID)
        {
            var userRole = _authorizationService.GetUserRole(User.UserId);
            lbladminAccess.Text = $"{userRole.Description}{(userRole.ShowRegions ? $" ({string.Join(", ", userRole.Regions.Select(r => r.Name))})" : string.Empty)}";

            string referrer = $"&Source={(HttpUtility.UrlEncode(Request.QueryString["TopSource"] != null ? Request.QueryString["TopSource"] : Page.Request.Url.AbsoluteUri))}";
            linkEdit.NavigateUrl = _urlService.GetProfileAccessAdminURL(LoginID, referrer);
        }

        protected void GetOrderItemsHistory(string UserID)
        {
            List<RetrieveOrderHistoryResult> OrderItems = _orderService.GetOrderHistory(UserID);
            if (OrderItems != null)
            {
                if (OrderItems.Count > 0)
                {
                    orderHistory.DataSource = OrderItems;
                    orderHistory.DataBind();
                    divNoOrder.Visible = false;
                    divOrderHistory.Visible = true;
                }
                else
                {
                    divNoOrder.Visible = true;
                    divOrderHistory.Visible = false;
                }
            }
            else
            {
                divNoOrder.Visible = true;
                divOrderHistory.Visible = false;
            }
        }

        protected void FillStates()
        {
            ddlState.DataSource = _regionService.GetStates().ToList();
            ddlState.DataTextField = "StateName";
            ddlState.DataValueField = "StateID";
            ddlState.DataBind();
            ddlState.Items.Insert(0, new ListItem(Resources.Forms.DropdownSelect, "0"));
        }

        protected void FillCountries(string _region)
        {
            ddlCountry.DataSource = _tagService.GetCountries(_region, _userContext.SelectedLanguage).OrderBy(c => c.Value);
            ddlCountry.DataTextField = "Value";
            ddlCountry.DataValueField = "Key";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select--", "0"));
        }

        protected void FillLanguages(string region, string language)
        {
            IDictionary<string, string> ls = (new Core.MastercardSitemap()).GetLanguages(region, (Page.User.IsInRole(Constants.Roles.McAdmin) || Page.User.IsInRole(Constants.Roles.DevAdmin)) ? string.Empty : language);
            ddlLanguage.DataSource = ls;
            ddlLanguage.DataTextField = "Value";
            ddlLanguage.DataValueField = "Key";
            ddlLanguage.DataBind();
            if (ddlLanguage.Items.Count > 1)
            {
                ddlLanguage.Items.Insert(0, new ListItem("--Select--", "0"));
            }
        }

        protected void AdminInformation(string username)
        {
            MembershipUser user = MembershipService.GetUser(username);
            if (user != null)
            {
                if (!user.IsApproved)
                {
                    divAccountInformation.Visible = true;
                }
                else
                {
                    divAccountInformation.Visible = false;
                }
            }
        }

        protected void setCommonInformation(string username)
        {
            var user = _userService.GetUserByUserName(username);
            Region = user.Region.IsNullOrEmpty() ? RegionalizeService.DefaultRegion : user.Region.ToString().Trim();
            FillCountries(Region);
            FillLanguages(Region, user.Language);

            if (ddlCountry.Items.Count > 2)
            {
                ddlCountry.SelectedValue = user.Country.ToString();
            }
            else
            {
                txtCountry.Text = ddlCountry.Items.FindByValue(user.Country ?? "")?.ToString() ?? ddlCountry.Items.FindByValue(user.Country?.ToLower() ?? "")?.ToString() ?? ddlCountry.Items.FindByValue(user.Country.ToUpper() ?? "")?.ToString();
            }

            ddlLanguage.SelectedValue = user.Language.ToString();
            txtFirstName.Text = user.Profile.FirstName.ToString();
            txtLastName.Text = user.Profile.LastName.ToString();
            txtIssuerName.Text = user.Profile.IssuerName.ToString();
            txtAddress1.Text = user.Profile.Address1.ToString();
            txtAddress2.Text = user.Profile.Address2.ToString();
            txtAddress3.Text = user.Profile.Address3.ToString();

            if (Region.ToLower().Equals("US".ToLower()))
            {
                ddlState.SelectedItem.Text = user.Profile.State.ToString();
            }
            else
            {
                txtState.Text = user.Profile.State?.ToString();
            }

            txtZipCode.Text = user.Profile.Zip.ToString();
            txtPhone.Text = user.Profile.Phone.ToString();
            txtMobile.Text = user.Profile.Mobile.ToString();
            txtFax.Text = user.Profile.Fax.ToString();
            lblEmail.Text = user.Email.ToString();
            txtCity.Text = user.Profile.City.ToString();
            txtTitle.Text = user.Profile.Title.ToString();
            txtProcessor.Text = user.Profile.Processor?.ToString();

            lblIssuerICA.Text = Resources.Forms.Unassigned;
            lnkIssuerICA.Text = Resources.Forms.Unassigned;

            if (user.Issuer != null)
            {
                if (!string.IsNullOrEmpty(user.Issuer.BillingId))
                {
                    lblIssuerICA.Text = user.Issuer.BillingId;
                    lnkIssuerICA.Text = user.Issuer.BillingId;
                }
                else
                {
                    lblIssuerICA.Text = Resources.Forms.Unassigned;
                    lnkIssuerICA.Text = Resources.Forms.Unassigned;
                }

                string referrer = $"&Source={(HttpUtility.UrlEncode(Request.QueryString["TopSource"] != null ? Request.QueryString["TopSource"] : Page.Request.Url.AbsoluteUri))}";
                lnkIssuerICA.NavigateUrl = Path.Combine(ConfigurationManager.Environment.AdminUrl, $"List/Edit?listid=3&ID={user.Issuer.IssuerId}{referrer}");
                lnkIssuerICA.Target = "_top";
            }

            if (Request.QueryString["loginid"] != null)
            {
                if (Page.User.IsInRole(Constants.Roles.McAdmin) || Page.User.IsInRole(Constants.Roles.DevAdmin))
                {
                    lnkIssuerICA.Visible = true;
                    lblIssuerICA.Visible = false;
                }
                else
                {
                    lnkIssuerICA.Visible = false;
                    lblIssuerICA.Visible = true;
                }
            }
            else
            {
                lnkIssuerICA.Visible = false;
                lblIssuerICA.Visible = true;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (ValidateForm())
            {
                string username = string.Empty;
                if (Request.QueryString["loginid"] != null)
                {
                    username = Request.QueryString["loginid"].ToString();
                    if (username.IndexOf(',') > 0 && username.Split(',').Length > 1)
                    {
                        username = username.Split(',')[0];
                    }
                }
                else
                {
                    username = _userContext.User.UserName;
                }

                var user = _userService.GetUserByUserName(username);
                user.Profile.FirstName = txtFirstName.Text.Trim();
                user.Profile.LastName = txtLastName.Text.Trim();
                user.Profile.IssuerName = txtIssuerName.Text.Trim();
                user.Country = ddlCountry.SelectedValue.Count() > 2 ? ddlCountry.SelectedValue.Trim() : user.Country;
                user.Language = !UserBelongsToRegion("us") ? (ddlLanguage.Items.Count > 1 ? ddlLanguage.SelectedValue.Trim() : user.Language ?? RegionalizeService.DefaultLanguage) : RegionalizeService.DefaultLanguage;
                user.Profile.Address1 = txtAddress1.Text.Trim();
                user.Profile.Address2 = txtAddress2?.Text?.Trim();
                user.Profile.Address3 = txtAddress3?.Text?.Trim();
                user.Profile.City = txtCity.Text.Trim();
                user.Profile.State = !UserBelongsToRegion("us") ? txtState.Text.Trim() : ddlState.SelectedItem.Text.Trim();
                user.Profile.Zip = txtZipCode?.Text?.Trim();
                user.Profile.Phone = txtPhone?.Text?.Trim();
                user.Profile.Mobile = txtMobile?.Text?.Trim();
                user.Profile.Fax = txtFax?.Text?.Trim();
                user.Profile.Title = txtTitle?.Text?.Trim();
                user.Profile.Processor = txtProcessor.Text.Trim();

                _userService.UpdateUser(user);
                _unitOfWork.Commit();

                Response.Clear();
                Response.Write($@"<html><head><script type='text/javascript'>
					              window.top.location.href = '{GetRedirectUrl()}';
				                  </script></head><body></body></html>");
                Response.End();
            }
            else
            {
                divError.Visible = true;
                lblError.Visible = true;
            }
        }

        public bool UserBelongsToRegion(string ExpectedRegion)
        {
            if (string.IsNullOrEmpty(ExpectedRegion))
            {
                return false;
            }

            return Region.Equals(ExpectedRegion.Trim(), StringComparison.OrdinalIgnoreCase);
        }

        private bool ValidateForm()
        {
            lblError.Text = string.Empty;
            var errors = new List<string>();

            if (txtFirstName.Text.Trim().Length == 0)
            {
                errors.Add(Resources.Errors.FirstNameEmpty);
            }

            if (txtLastName.Text.Trim().Length == 0)
            {
                errors.Add(Resources.Errors.LastNameEmpty);
            }

            if (txtTitle.Text.Trim().Length == 0)
            {
                errors.Add(Resources.Errors.TitleEmpty);
            }

            if (txtIssuerName.Text.Trim().Length == 0)
            {
                errors.Add(Resources.Errors.IssuerNameEmpty);
            }

            if (txtAddress1.Text.Trim().Length == 0 && UserBelongsToRegion("us"))
            {
                errors.Add(Resources.Errors.Address1Empty);
            }

            if (txtCity.Text.Trim().Length == 0 && UserBelongsToRegion("us"))
            {
                errors.Add(Resources.Errors.CityEmpty);
            }

            if (txtZipCode.Text.Trim().Length == 0 && UserBelongsToRegion("us"))
            {
                errors.Add(Res.ZipCodeEmpty);
            }

            if (txtPhone.Text.Trim().Length == 0)
            {
                errors.Add(Resources.Errors.PhoneEmpty);
            }

            if (((ddlCountry.SelectedValue.Trim().Length == 0) || (ddlCountry.SelectedValue == "0")) && ddlCountry.Items.Count > 2)
            {
                errors.Add(Resources.Errors.CountryEmpty);
            }

            if (((ddlLanguage.SelectedValue.Trim().Length == 0) || (ddlLanguage.SelectedValue == "0")) && ddlLanguage.Items.Count > 2)
            {
                errors.Add(Resources.Errors.LanguageEmpty);
            }

            if (ddlState.SelectedIndex == -1 && UserBelongsToRegion("us"))
            {
                errors.Add(Res.StateEmpty);
            }

            if (errors.Any())
            {
                errors.ForEach(error => lblError.Text += error + "<br/>");
                return false;
            }

            lblError.Text = string.Empty;
            return true;
        }

        protected void HookupCancelButton()
        {
            btnCancel.OnClientClick = $"window.top.location.href = '{GetRedirectUrl()}'; return false;";
        }

        protected string GetRedirectUrl()
        {
            if (Request.QueryString["Source"] != null)
            {
                return Request.QueryString["Source"];
            }

            if (Request.QueryString["loginid"] != null && Request.QueryString["loginid"].IndexOf(',') > 0 && Request.QueryString["loginid"].Split(',').Length > 1)
            {
                return Request.QueryString["loginid"].Split(',')[1];
            }

            return "/portal";
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(GetRedirectUrl());
        }

        protected void orderHistory_ItemBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var item = e.Item.DataItem as RetrieveOrderHistoryResult;
                (e.Item.FindControl("OrderLink") as HtmlAnchor).HRef = $"/portal/orderdetails/{item.OrderNumber}";
            }
        }

        public class LocalRes
        {
            private UserProfile parent;

            public LocalRes(UserProfile parent)
            {
                this.parent = parent;
            }

            private string GetProperty(string prop, string _Class = "Forms")
            {
                try
                {
                    string final = parent.GetGlobalResourceObject(_Class, (prop + (parent.Region.ToLower() == "us" ? "US" : ""))).ToString();
                    if (final == null)
                    {
                        final = HttpContext.GetGlobalResourceObject(_Class, prop).ToString();
                    }

                    return final;
                }
                catch
                {
                    return HttpContext.GetGlobalResourceObject(_Class, prop).ToString();
                }
            }

            public string ZipCode
            {
                get
                {
                    return GetProperty("ZipCode");
                }
            }

            public string State
            {
                get
                {
                    return GetProperty("State");
                }
            }

            public string StateEmpty
            {
                get
                {
                    return GetProperty("StateEmpty", "Errors");
                }
            }

            public string ZipCodeEmpty
            {
                get
                {
                    return GetProperty("ZipCodeEmpty", "Errors");
                }
            }
        }
    }
}