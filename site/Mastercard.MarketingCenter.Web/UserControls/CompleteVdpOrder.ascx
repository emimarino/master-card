﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="CompleteVdpOrder.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.UserControls.CompleteVdpOrder" %>

<%@ Register TagPrefix="mc" TagName="VdpFullfilmentItem" Src="VdpFullfilmentItem.ascx" %>

<script src="<%= DependencyResolver.Current.GetService<Mastercard.MarketingCenter.Services.Interfaces.IUrlService>().GetStaticCommonFunctionJs() %>"></script>
<div class="center_container">
    <div class="form_container">
        <div class="orange_heading_profile">
            <%=Resources.ShoppingCart.CompleteVdpOrder%>
        </div>
        <div class="clr"></div>
        <div class="faq_blog">
            <div class="main_heading ">
                <span class="order_heading width300 pdl_15px"><%=Resources.ShoppingCart.FullfilmentInformation%></span>
                <span class="order_heading width95"><%=Resources.ShoppingCart.Sku%></span>
                <span class="order_heading width90"><%=Resources.ShoppingCart.Title%></span>
                <span class="order_heading width90"><%=Resources.ShoppingCart.Quantity%></span>
                <span class="order_heading width90"><%=Resources.ShoppingCart.Price%></span>
            </div>
            <div class="main_heading_bg" style="padding-bottom: 10px;">
                <mc:VdpFullfilmentItem ID="vdpFullfilmentItem" runat="server" />
                <div style="color: red; padding: 15px; font-weight: bold; clear: both;">
                    <asp:Literal ID="litError" runat="server" EnableViewState="false" Visible="false"></asp:Literal>
                </div>
                <div id="divcostadjustment" runat="server">
                    <div class="heading_span_new mrt_10">
                        <%=Resources.ShoppingCart.PriceAdjustment%>
                    </div>
                    <div class="chk_outgrid ">
                        <div class="data_lost_right">
                            <asp:Label ID="lblPriceAdjustment" runat="server" CssClass="pricedetails" Visible="False"></asp:Label>
                            <asp:TextBox ID="txtPriceAdjustment" MaxLength="10" runat="server"></asp:TextBox>
                        </div>
                        <div class="data_rtf_left">
                            <asp:Label ID="lblDescription" runat="server" CssClass="pricedetails" Visible="False"></asp:Label>
                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="chk_outgrid gray_backgrnd">
                        <div class="data_lost_right">
                            <asp:Label ID="lblPostageCost" runat="server" CssClass="pricedetails" Visible="False"></asp:Label>
                            <asp:TextBox ID="txtPostageCost" MaxLength="10" runat="server"></asp:TextBox>
                        </div>
                        <div class="data_rtf_left">
                            <label>
                                <%=Resources.ShoppingCart.PostageCost%> *</label>
                        </div>
                    </div>
                    <div class="fl" style="width: 660px; float: right;">
                        <span runat="server" id="spnComplete" class="bg_btn_left mr11 fr">
                            <asp:Button ID="btnComplete" BorderWidth="0px" CssClass="submit_bg_btn" runat="server" Text="<%$Resources:Shared,Complete%>" OnClick="btnComplete_Click" />
                        </span><span class="bg_btn_left mr11 fr">
                            <asp:Button ID="btnCancel" BorderWidth="0px" CssClass="submit_bg_btn " runat="server" Text="<%$Resources:Shared,Cancel%>" />
                        </span><span class="label_margin requried fl">* <%=Resources.Shared.Required%></span>
                    </div>
                </div>
            </div>
            <div class="main_heading_bot"></div>
        </div>
    </div>
</div>
