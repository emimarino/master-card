﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_RelatedResources : System.Web.UI.UserControl
{
    public object DataSource { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        LoadRelatedResources();
    }

    private void LoadRelatedResources()
    {
        rptRelatedResources.DataSource = DataSource;
        rptRelatedResources.DataBind();
    }

    public class RelatedResourcesControlViewData
    {
        public string HRef { get; set; }
        public string Title { get; set; }
    }
}