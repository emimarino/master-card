﻿using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.UserControls
{
    public partial class FileUpload : UserControl
    {
        public IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }
        public ISettingsService _settingsService { get { return DependencyResolver.Current.GetService<ISettingsService>(); } }
        public IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }

        public string Value
        {
            get
            {
                return hdnCurrentFileUrl.Value;
            }
            set
            {
                hdnCurrentFileUrl.Value = value;

                divNewFile.Attributes["style"] = "display: none;";
                divEditFile.Attributes["style"] = "display: block;";
                lnkCancelUpload.Visible = true;
                lnkViewFile.Text = hdnCurrentFileUrl.Value.Substring(hdnCurrentFileUrl.Value.LastIndexOf("/", StringComparison.Ordinal) + 1);
                lnkViewFile.NavigateUrl = _urlService.GetProofFileURL(hdnCurrentFileUrl.Value.ToString());
                lnkViewFile.Target = "_blank";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                btnHiddenUploadButton.Attributes.Add("style", "display: none;");
            }

            radFileUpload.OnClientFileSelected = "Do" + radFileUpload.UniqueID + "Click";

            radFileUpload.AllowedFileExtensions = new[] { ".pdf" };

            Page.ClientScript.RegisterStartupScript(GetType(), "Do" + radFileUpload.UniqueID + "Click", " function Do" + radFileUpload.UniqueID + "Click() { " + Page.ClientScript.GetPostBackEventReference(btnHiddenUploadButton, "") + " } ", true);

            if (!IsPostBack && string.IsNullOrEmpty(hdnCurrentFileUrl.Value))
            {
                divNewFile.Attributes["style"] = "display: block;";
                divEditFile.Attributes["style"] = "display: none;";
                lnkCancelUpload.Visible = false;
            }

            divErrorMessage.Visible = false;

            if (Request.UserAgent.Contains("MSIE 6"))
            {
                radFileUpload.EnableFileInputSkinning = false;
            }

            if (!IsPrinterCompleted())
            {
                lnkReplaceFile.Visible = true;
            }
            else
            {
                lnkReplaceFile.Visible = false;
            }
        }

        protected bool IsPrinterCompleted()
        {
            int orderId;

            if (Request.QueryString["orderID"] != null)
            {
                orderId = _orderService.GetOrderIDByOrderNo(Request.QueryString["orderID"]);
                if (orderId != 0)
                {
                    RetrieveOrderDetailResult orderItems = _orderService.GetOrderDetails(orderId);
                    switch (orderItems.OrderStatus.ToLower())
                    {
                        case "shipped":
                        case "fulfillment":
                        case "pending approval":
                            return true;

                        default:
                            return false;
                    }
                }
            }
            else
            {
                if (Page.RouteData.Values["orderid"] != null)
                {
                    orderId = _orderService.GetOrderIDByOrderNo(Page.RouteData.Values["orderid"].ToString());
                    if (orderId != 0)
                    {
                        RetrieveOrderDetailResult orderItems = _orderService.GetOrderDetails(orderId);

                        switch (orderItems.OrderStatus.ToLower())
                        {
                            case "pending customization":
                                Visible = false;
                                break;
                        }

                        return true;
                    }
                }
            }

            return true;
        }

        protected void lnkCancelUpload_Click(object sender, EventArgs e)
        {
            divNewFile.Attributes["style"] = "display: none;";
            divEditFile.Attributes["style"] = "display: block;";
            radFileUpload.Visible = false;
            updateFileUpload.Update();
        }

        protected void lnkReplaceFile_Click(object sender, EventArgs e)
        {
            divNewFile.Attributes["style"] = "display: block;";
            divEditFile.Attributes["style"] = "display: none;";
            radFileUpload.Visible = true;
            updateFileUpload.Update();
        }

        protected void btnHiddenUploadButton_Click(object sender, EventArgs e)
        {
            if (radFileUpload.UploadedFiles.Count > 0)
            {
                foreach (UploadedFile file in radFileUpload.UploadedFiles)
                {
                    string guid = Guid.NewGuid().ToString();
                    string customizationFilesUploadPath = _settingsService.GetCustomizationFilesUploadPath().TrimEnd('/');
                    string filename = Regex.Replace(file.GetName(), "[^\\w\\s\\.]", string.Empty);
                    string saveLocation = $@"{Server.MapPath(customizationFilesUploadPath)}\{guid}\{filename}";
                    if (!Directory.Exists(Path.GetDirectoryName(saveLocation)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(saveLocation));
                    }

                    file.SaveAs(saveLocation);

                    Value = $"{customizationFilesUploadPath}/{guid}/{filename}";
                    radFileUpload.Visible = false;
                    Repeater rptItems;
                    if (Parent.Parent is Repeater repeater)
                    {
                        rptItems = repeater;
                    }
                    else
                    {
                        if (Parent.Parent.Parent is Repeater innerRepeater)
                        {
                            rptItems = innerRepeater;
                        }
                        else
                        {
                            rptItems = (Repeater)Parent.Parent.Parent.Parent;
                        }
                    }

                    Button btnPrinterComplete = (Button)rptItems.Parent.Parent.FindControl("btnPrinterComplete");
                    UpdatePanel updatePanelPrinterButton = (UpdatePanel)rptItems.Parent.Parent.FindControl("UpdatePanelPrinterButton");
                    if (rptItems != null && rptItems.Items.Count > 0)
                    {
                        btnPrinterComplete.Enabled = true;
                        btnPrinterComplete.CssClass = "submit_bg_btn";
                        ((HtmlGenericControl)btnPrinterComplete.Parent).Attributes.Remove("class");
                        ((HtmlGenericControl)btnPrinterComplete.Parent).Attributes.Add("class", "bg_btn_left mr11 fr");

                        foreach (RepeaterItem item in rptItems.Items)
                        {
                            FileUpload fupload = (FileUpload)item.FindControl("UploadProofFile");
                            RadUpload radu = (RadUpload)fupload.FindControl("radFileUpload");
                            HyperLink lviewfile = (HyperLink)fupload.FindControl("lnkViewFile");

                            if (radu.Visible && radu.UploadedFiles.Count == 0 && lviewfile.Text.Trim().Length == 0)
                            {
                                btnPrinterComplete.CssClass = "grey_bg_btn";
                                ((HtmlGenericControl)btnPrinterComplete.Parent).Attributes.Remove("class");
                                ((HtmlGenericControl)btnPrinterComplete.Parent).Attributes.Add("class", "bg_grey_left mr11 fr");
                                btnPrinterComplete.Enabled = false;
                            }
                        }
                    }

                    if (updatePanelPrinterButton != null)
                    {
                        updatePanelPrinterButton.Update();
                    }
                }
            }
            else
            {
                divErrorMessage.Visible = true;
            }
        }
    }
}