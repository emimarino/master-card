﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace Mastercard.MarketingCenter.Web.UserControls
{
    public partial class CompleteSubOrder : System.Web.UI.UserControl
    {
        decimal total = 0;

        public IUserService _userServices { get { return DependencyResolver.Current.GetService<IUserService>(); } }
        IOrderService _orderService { get { return DependencyResolver.Current.GetService<IOrderService>(); } }
        IEmailService _emailService { get { return DependencyResolver.Current.GetService<IEmailService>(); } }
        UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.UrlReferrer != null)
                    ViewState["Referrer"] = Request.UrlReferrer.ToString();

                int subOrderId = 0;
                if (Page.RouteData.Values["suborderid"] != null)
                    Int32.TryParse(Page.RouteData.Values["suborderid"].ToString(), out subOrderId);
                if (Request.QueryString["subOrderId"] != null)
                    Int32.TryParse(Request.QueryString["subOrderId"], out subOrderId);

                RetrieveSubOrderDetailResult subOrder = _orderService.GetSubOrderDetail(subOrderId);

                ltAddress.Text = subOrder.Address.Replace(Environment.NewLine, "<br />");
                ltContactName.Text = subOrder.ContactName;
                ltPhone.Text = subOrder.Phone;

                txtDescription.Text = subOrder.Description;
                txtPostageCost.Text = subOrder.PostageCost.ToString();
                txtPriceAdjustment.Text = subOrder.PriceAdjustment.ToString();
                txtShippingCost.Text = subOrder.ShippingCost.ToString();
                txtTracking.Text = subOrder.TrackingNumber;
                hdnShippingInformationId.Value = subOrder.ShippingInformationID.ToString();

                rptSubOrderItems.DataSource = _orderService.GetSubOrderItems(subOrderId);
                rptSubOrderItems.DataBind();
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            HtmlLink css = new HtmlLink();
            if (Request.QueryString["subOrderId"] != null)
            {
                css.Href = Slam.Cms.Configuration.ConfigurationManager.Environment.AdminUrl.TrimEnd('/') + "/_layouts/1033/STYLES/MarketingCenter.SharedControls.css";
                css.Attributes["rel"] = "stylesheet";
                css.Attributes["type"] = "text/css";
                css.Attributes["media"] = "all";
                this.Page.Header.Controls.Add(css);
            }

            btnComplete.Click += new EventHandler(btnComplete_Click);
            btnCancel.Click += new EventHandler(btnCancel_Click);

            txtPriceAdjustment.Attributes.Add("onkeydown", "return ForceNumericInput(event,true,true);");
            txtPostageCost.Attributes.Add("onkeydown", "return ForceNumericInput(event,true,true);");
            txtShippingCost.Attributes.Add("onkeydown", "return ForceNumericInput(event,true,true);");
        }

        protected string GetPrice(string price)
        {
            string sretun = string.Empty;
            if (price.Length > 0)
            {
                decimal dprice = decimal.Parse(price);
                total = total + dprice;

                sretun = FormatNumber(dprice);
            }
            return sretun;
        }

        protected string FormatNumber(decimal num)
        {
            System.Globalization.NumberFormatInfo nf = new System.Globalization.NumberFormatInfo();
            nf.CurrencySymbol = "$";
            nf.NumberDecimalDigits = 2;
            return num.ToString("C", nf);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (ViewState["Referrer"] != null)
                Response.Redirect(ViewState["Referrer"].ToString());
            else
            {
                string returnUrl = string.Empty;
                int subOrderId = 0;

                if (Page.RouteData.Values["suborderid"] != null)
                {
                    Int32.TryParse(Page.RouteData.Values["suborderid"].ToString(), out subOrderId);
                    returnUrl = "~/orderdetail/";
                }
                if (Request.QueryString["subOrderId"] != null)
                {
                    Int32.TryParse(Request.QueryString["subOrderId"], out subOrderId);
                    returnUrl = "OrderDetailNoChrome.aspx?orderid=";
                }
                RetrieveSubOrderDetailResult subOrder = _orderService.GetSubOrderDetail(subOrderId);

                string referrer = Request.QueryString["Source"] != null ? "&Source=" + Request.QueryString["Source"] : string.Empty;
                Response.Redirect(returnUrl + _orderService.GetOrderDetails(subOrder.OrderID).OrderNumber + referrer);
            }
        }

        protected void btnComplete_Click(object sender, EventArgs e)
        {
            if (ValidateCost())
            {
                decimal? PriceAdjustment = null;
                decimal Adjustment = 0;
                if (txtPriceAdjustment.Text.Trim().Length > 0 && !decimal.TryParse(txtPriceAdjustment.Text.Trim(), out Adjustment))
                {
                    divError.Visible = true;
                    lblError.Text = Resources.Errors.PriceAjustmentInvalid;
                    btnComplete.Enabled = true;
                    btnComplete.CssClass = "submit_bg_btn";
                    ((HtmlGenericControl)btnComplete.Parent).Attributes.Remove("class");
                    ((HtmlGenericControl)btnComplete.Parent).Attributes.Add("class", "bg_btn_left mr11 fr");
                    return;
                }
                else
                    PriceAdjustment = Adjustment;

                decimal? PostageCost = null;
                decimal PCost = 0;
                if (txtPostageCost.Text.Trim().Length > 0 && !decimal.TryParse(txtPostageCost.Text.Trim(), out PCost))
                {
                    divError.Visible = true;
                    lblError.Text = Resources.Errors.PostageCostInvalid;
                    btnComplete.Enabled = true;
                    btnComplete.CssClass = "submit_bg_btn";
                    ((HtmlGenericControl)btnComplete.Parent).Attributes.Remove("class");
                    ((HtmlGenericControl)btnComplete.Parent).Attributes.Add("class", "bg_btn_left mr11 fr");

                    return;
                }
                else
                    PostageCost = PCost;

                decimal? ShippingCost = null;
                decimal SCost = 0;
                if (txtShippingCost.Text.Trim().Length > 0 && !decimal.TryParse(txtShippingCost.Text.Trim(), out SCost))
                {
                    divError.Visible = true;
                    lblError.Text = Resources.Errors.ShippingCostInvalid;
                    btnComplete.Enabled = true;
                    btnComplete.CssClass = "submit_bg_btn";
                    ((HtmlGenericControl)btnComplete.Parent).Attributes.Remove("class");
                    ((HtmlGenericControl)btnComplete.Parent).Attributes.Add("class", "bg_btn_left mr11 fr");
                    return;
                }
                else
                {
                    ShippingCost = SCost;
                }

                int subOrderId = 0;
                if (Page.RouteData.Values["suborderid"] != null)
                {
                    int.TryParse(Page.RouteData.Values["suborderid"].ToString(), out subOrderId);
                }
                if (Request.QueryString["subOrderId"] != null)
                {
                    int.TryParse(Request.QueryString["subOrderId"], out subOrderId);
                }

                divError.Visible = false;
                if (subOrderId != 0)
                {
                    RetrieveSubOrderDetailResult subOrderDetail = _orderService.GetSubOrderDetail(subOrderId);
                    string strHostName = System.Net.Dns.GetHostName();
                    string clientIPAddress = System.Net.Dns.GetHostAddresses(strHostName).GetValue(0).ToString();
                    RetrieveSubOrderCompletionDetailResult completionDetail = _orderService.GetSubOrderCompletionDetail(subOrderId);
                    if (completionDetail.CompletedSiblingSubOrders == completionDetail.SiblingSubOrders)
                    {
                        _orderService.SetOrderStatus(subOrderDetail.OrderID, "Shipped", "Order Shipped/Fulfilled", clientIPAddress, _userContext.User.UserName);
                        _orderService.UpdateSubOrderPriceAdjustment(subOrderId, PriceAdjustment, PostageCost, ShippingCost, txtDescription.Text.Trim(), true);
                    }
                    else
                    {
                        _orderService.UpdateSubOrderPriceAdjustment(subOrderId, PriceAdjustment, PostageCost, ShippingCost, txtDescription.Text.Trim(), false);
                    }

                    _orderService.UpdateSubOrderTrackingNumber(subOrderId, txtTracking.Text.Trim());

                    SendConfirmationMail(subOrderId, _orderService.GetSubOrderDetail(subOrderId), PriceAdjustment, ShippingCost, PostageCost, txtDescription.Text.Trim(), completionDetail.SiblingSubOrders > 0);

                    if (ViewState["Referrer"] != null)
                    {
                        Response.Redirect(ViewState["Referrer"].ToString());
                    }
                    else
                    {
                        string returnUrl = string.Empty;
                        if (Page.RouteData.Values["suborderid"] != null)
                        {
                            returnUrl = "~/orderdetail/";
                        }
                        if (Request.QueryString["subOrderId"] != null)
                        {
                            returnUrl = "OrderDetailNoChrome.aspx?orderid=";
                        }

                        string referrer = Request.QueryString["Source"] != null ? "&Source=" + Request.QueryString["Source"] : "";
                        Response.Redirect(returnUrl + _orderService.GetOrderDetails(subOrderDetail.OrderID).OrderNumber + referrer);
                    }
                }
            }
        }

        protected bool ValidateCost()
        {
            divError.Visible = true;
            string strError = string.Empty;

            if (txtShippingCost.Text.Trim().Length <= 0)
            {
                strError = Resources.Errors.ShippingCostEmpty;
            }

            if (txtTracking.Text.Trim().Length <= 0)
            {
                strError += Resources.Errors.TrackingNumberEmpty;
            }

            if (txtPriceAdjustment.Text.Trim().Length > 0 && txtDescription.Text.Trim().Length <= 0)
            {
                strError += Resources.Errors.DescriptionEmpty;
            }

            if (strError != string.Empty)
            {
                divError.Visible = true;
                lblError.Text = strError;
                lblError.Visible = true;
                return false;
            }
            else
            {
                divError.Visible = false;
                lblError.Visible = false;
                return true;
            }
        }

        string _specialInstructions = string.Empty;

        protected void SendConfirmationMail(int subOrderId, RetrieveSubOrderDetailResult subOrderDetail, decimal? PriceAdjustment, decimal? ShippingCost, decimal? PostageCost, string desc, bool sendPartialShippingEmail)
        {
            List<TableItem> listitems;
            decimal totals = 0;
            StringBuilder sb = new StringBuilder();
            StringBuilder specialText = new StringBuilder();

            List<RetrieveSubOrderItemsResult> OrderItems = _orderService.GetSubOrderItems(subOrderId);
            RetrieveOrderDetailResult order = _orderService.GetOrderDetails(subOrderDetail.OrderID);

            var shippingText = Environment.NewLine + subOrderDetail.Address + Environment.NewLine;
            shippingText += Environment.NewLine;
            shippingText += Resources.ShoppingCart.ItemsShipped + ":" + Environment.NewLine;

            if (OrderItems.Count > 0)
            {
                foreach (RetrieveSubOrderItemsResult orderItem in OrderItems)
                {
                    shippingText += $"{orderItem.SKU}, {orderItem.ItemName}, {orderItem.ItemQuantity}{Environment.NewLine}";
                }
            }

            shippingText += Environment.NewLine + Resources.ShoppingCart.TrackingCode.ToUpperInvariant() + ": " + subOrderDetail.TrackingNumber + Environment.NewLine;

            if (!string.IsNullOrEmpty(order.SpecialInstructions))
            {
                _specialInstructions = order.SpecialInstructions;
            }
            else
            {
                _specialInstructions = " ";
            }

            if (order.RushOrder)
            {
                _specialInstructions += Environment.NewLine + Environment.NewLine + Resources.ShoppingCart.RushOrderRequested;
            }

            var user = _userServices.GetUserByUserName(order.UserId);
            string result;
            EmailText et;
            string ItemNames = string.Empty;
            listitems = new List<TableItem>();
            if (OrderItems != null)
            {
                sb.Append(".................................................................");
                sb.Append(System.Environment.NewLine);

                TableItem t = new TableItem();

                t.Head = Resources.ShoppingCart.Sku.ToUpperInvariant();
                t.Width = 12;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = Resources.ShoppingCart.Title;
                t.Width = 22;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = Resources.ShoppingCart.Quantity;
                t.Width = 17;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = Resources.ShoppingCart.Price;
                t.Width = 10;
                t.Align = "Right";
                listitems.Add(t);

                et = new EmailText();
                result = et.GenerateText(listitems);

                sb.Append(result);
                sb.Append(".................................................................");
                sb.Append(System.Environment.NewLine);
                foreach (RetrieveSubOrderItemsResult r in OrderItems)
                {
                    listitems.Clear();

                    t = new TableItem();

                    t.Head = r.SKU.ToString();
                    t.Width = 12;
                    t.Align = "Left";
                    listitems.Add(t);

                    t = new TableItem();
                    t.Head = r.ItemName.ToString() + " ";
                    t.Width = 22;
                    t.Align = "Left";
                    listitems.Add(t);

                    t = new TableItem();
                    t.Head = r.ItemQuantity.ToString();
                    t.Width = 17;
                    t.Align = "Left";
                    listitems.Add(t);

                    t = new TableItem();
                    t.Head = FormatNumber(r.ItemPrice).ToString();
                    totals = totals + Convert.ToDecimal(r.ItemPrice.ToString());
                    t.Width = 10;
                    t.Align = "Right";
                    listitems.Add(t);

                    et = new EmailText();
                    result = et.GenerateText(listitems);
                    sb.Append(result);
                    sb.Append(System.Environment.NewLine);

                    if (!string.IsNullOrEmpty(ItemNames)) ItemNames = ItemNames + ",";
                    ItemNames = ItemNames + r.ItemName.ToString();
                }

                if (PriceAdjustment != null && PriceAdjustment.HasValue)
                {
                    listitems.Clear();
                    t = new TableItem();

                    t.Head = desc + " ";
                    t.Width = 49;
                    t.Align = "Left";
                    listitems.Add(t);

                    t = new TableItem();
                    t.Head = string.Empty;
                    t.Width = 1;
                    t.Align = "Left";
                    listitems.Add(t);

                    t = new TableItem();
                    t.Head = string.Empty;
                    t.Width = 1;
                    t.Align = "Left";
                    listitems.Add(t);

                    t = new TableItem();
                    t.Head = FormatNumber(PriceAdjustment.Value);
                    t.Width = 10;
                    t.Align = "Right";
                    listitems.Add(t);
                    et = new EmailText();
                    result = et.GenerateText(listitems);
                    sb.Append(result);
                    sb.Append(System.Environment.NewLine);

                    totals = totals + PriceAdjustment.Value;
                }

                if (ShippingCost != null && ShippingCost.HasValue)
                {
                    listitems.Clear();
                    t = new TableItem();

                    t.Head = Resources.ShoppingCart.ShippingCost + " ";
                    t.Width = 49;
                    t.Align = "Left";
                    listitems.Add(t);

                    t = new TableItem();
                    t.Head = string.Empty;
                    t.Width = 1;
                    t.Align = "Left";
                    listitems.Add(t);

                    t = new TableItem();
                    t.Head = string.Empty;
                    t.Width = 1;
                    t.Align = "Left";
                    listitems.Add(t);

                    t = new TableItem();
                    t.Head = FormatNumber(ShippingCost.Value);
                    t.Width = 10;
                    t.Align = "Right";
                    listitems.Add(t);
                    et = new EmailText();
                    result = et.GenerateText(listitems);
                    sb.Append(result);
                    sb.Append(System.Environment.NewLine);

                    totals = totals + ShippingCost.Value;
                }

                if (PostageCost != null && PostageCost.HasValue)
                {
                    listitems.Clear();
                    t = new TableItem();

                    t.Head = Resources.ShoppingCart.PostageCost + " ";
                    t.Width = 49;
                    t.Align = "Left";
                    listitems.Add(t);

                    t = new TableItem();
                    t.Head = string.Empty;
                    t.Width = 1;
                    t.Align = "Left";
                    listitems.Add(t);

                    t = new TableItem();
                    t.Head = string.Empty;
                    t.Width = 1;
                    t.Align = "Left";
                    listitems.Add(t);

                    t = new TableItem();
                    t.Head = FormatNumber(PostageCost.Value);
                    t.Width = 10;
                    t.Align = "Right";
                    listitems.Add(t);
                    et = new EmailText();
                    result = et.GenerateText(listitems);
                    sb.Append(result);
                    sb.Append(System.Environment.NewLine);

                    totals = totals + PostageCost.Value;
                }

                sb.Append(".................................................................");
                sb.Append(Environment.NewLine);

                t = new TableItem();
                listitems.Clear();

                t.Head = string.Empty;
                t.Width = 12;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = string.Empty;
                t.Width = 22;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = "Total: ";
                t.Width = 17;
                t.Align = "Right";
                listitems.Add(t);

                t = new TableItem();
                t.Head = FormatNumber(totals);
                t.Width = 10;
                t.Align = "Right";
                listitems.Add(t);

                et = new EmailText();
                result = et.GenerateText(listitems);
                sb.Append(result);
                sb.Append(".................................................................");
            }

            // Shipping Details

            TableItem TI = new TableItem();
            listitems.Clear();

            TI.Head = _specialInstructions;
            TI.Width = 65;
            TI.Align = "Left";
            listitems.Add(TI);

            et = new EmailText();
            result = et.GenerateText(listitems);
            specialText.Append(result);

            if (OrderItems != null)
            {
                var userId = DependencyResolver.Current.GetService<UserContext>().User?.UserId ?? 0;
                _emailService.SendOrderShippedEmail(order.OrderNumber.ToString(), order.OrderDate.Value.ToString("MM/dd/yyyy"), sb.ToString(), FormatNumber(totals), user.Email, user.Profile.FirstName, user.Profile.LastName, shippingText, _specialInstructions, sendPartialShippingEmail, userId);
            }
        }

        protected string RetrieveShippingInformation(int Orderid, string specialInstructions, bool rushOrder)
        {
            string shipping_Detail = string.Empty;
            List<RetrieveShippingInformationResult> ShippingDetail = _orderService.GetShippingInformation(Orderid);
            if (ShippingDetail != null)
            {
                foreach (RetrieveShippingInformationResult s in ShippingDetail)
                {
                    string address = s.Address + Environment.NewLine;
                    address += Environment.NewLine;
                    address += Resources.ShoppingCart.ItemsShipped + ":" + Environment.NewLine;

                    List<RetrieveOrderItemsForShippingInformationResult> orderItems = _orderService.GetOrderItemsForShippingInformation(s.ShippingInformationID);
                    if (orderItems.Count > 0)
                    {
                        foreach (RetrieveOrderItemsForShippingInformationResult orderItem in orderItems)
                        {
                            address += string.Format("{0}, {1}, {2}{3}", orderItem.SKU, orderItem.ItemName, orderItem.ItemQuantity, Environment.NewLine);
                        }
                    }

                    address += Environment.NewLine + Resources.ShoppingCart.TrackingCode.ToUpperInvariant() + ": " + s.OrderTrackingNumber + Environment.NewLine;

                    shipping_Detail += Environment.NewLine + address;

                    if (!string.IsNullOrEmpty(specialInstructions))
                    {
                        _specialInstructions = specialInstructions;
                    }
                    else
                    {
                        _specialInstructions = " ";
                    }

                    if (rushOrder)
                    {
                        _specialInstructions += Environment.NewLine + Environment.NewLine + Resources.ShoppingCart.RushOrderRequested.ToUpperInvariant();
                    }
                }
            }

            return shipping_Detail;
        }
    }
}