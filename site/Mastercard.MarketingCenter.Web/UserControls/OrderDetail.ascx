﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="OrderDetail.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.UserControls.OrderDetail" %>
<%@ Register Src="FileUpload.ascx" TagName="FileUpload" TagPrefix="uc1" %>
<%@ Register TagPrefix="ajax2" Namespace="AjaxControlToolkit" Assembly="AJAXControlToolkit" %>
<%@ Register TagPrefix="mc" TagName="VdpFullfilment" Src="VdpFullfilmentItem.ascx" %>
<%@ Register TagPrefix="mc" TagName="ElectronicFullfilment" Src="ElectronicFullfilmentItem.ascx" %>
<%@ Register TagPrefix="mc" TagName="ImageUpload" Src="ImageUpload.ascx" %>

<script type="text/javascript">
    var modalSelections = '';
    $(document).ready(function () {
        $.blockUI.defaults.css = {};
        $.blockUI.defaults.fadeIn = 0;
        $.blockUI.defaults.fadeOut = 0;
    });
</script>
<script language="javascript" type="text/javascript">
    function ShowImageUpload(objThis, ImgControl, FileUploadControl) {
        var imgCtrl = document.getElementById(ImgControl);
        if (imgCtrl)
            imgCtrl.style.display = 'none';

        var uploadCtrl = document.getElementById(FileUploadControl);
        if (uploadCtrl)
            uploadCtrl.style.display = 'block';

        var objThisCtrl = document.getElementById(objThis);
        if (objThisCtrl)
            objThisCtrl.style.display = 'none';
    }
    function ShowProofUpload(objThis, ImgControl, FileUploadControl) {
        var imgCtrl = document.getElementById(ImgControl);
        if (imgCtrl)
            imgCtrl.style.display = 'none';

        var uploadCtrl = document.getElementById(FileUploadControl);
        if (uploadCtrl)
            uploadCtrl.style.display = 'block';

        var objThisCtrl = document.getElementById(objThis);
        if (objThisCtrl)
            objThisCtrl.style.display = 'none';
    }
    function checkFileExtension(elem) {
        var filePath = elem.value;

        if (filePath.indexOf('.') == -1)
            return false;

        var validExtensions = new Array();
        var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();

        //Add valid extentions in this array
        validExtensions[0] = 'jpg';
        validExtensions[1] = 'eps';
        //validExtensions[1] = 'jpeg';

        for (var i = 0; i < validExtensions.length; i++) {
            if (ext == validExtensions[i])
                return true;
        }
        elem.value = '';
        alert('<%=Resources.Errors.FileExtensionNotAllowed.Split(new string[] { "{0}" }, StringSplitOptions.None)[0]%>' + ext.toUpperCase() + '<%=Resources.Errors.FileExtensionNotAllowed.Split(new string[] { "{0}" }, StringSplitOptions.None)[1]%>');
        return false;
    }
</script>

<script src="<%= DependencyResolver.Current.GetService<Mastercard.MarketingCenter.Services.Interfaces.IUrlService>().GetStaticCommonFunctionJs() %>"></script>

<style type="text/css">
    .modalBackground {
        background-color: Gray;
        filter: alpha(opacity=70);
        opacity: 0.7;
    }

    .modalPopup {
        background-color: #f5f5f5;
        border-width: 2px;
        border-style: solid;
        border-color: #FB5E06;
        width: 350px;
        padding: 20px;
        position: absolute !important;
    }

        .modalPopup div {
            position: relative;
            overflow: hidden;
            margin-bottom: 10px;
        }

            .modalPopup div textarea {
                height: 75px;
                width: 270px;
            }

            .modalPopup div label {
                width: 250px;
            }

            .modalPopup div.buttonContainer {
                text-align: center;
                margin: 0;
                padding: 0;
            }

    .orange_heading_main {
        float: left;
        width: 500px;
    }

    .orange_heading_main_edit {
        float: right;
        width: 100px;
        text-align: right;
        padding-right: 10px;
        font-family: Verdana,Arial,Helvetica,sans-serif;
        -x-system-font: none;
        font-size: 12px;
        font-size-adjust: none;
        line-height: 24px;
    }

        .orange_heading_main_edit a,
        .orange_heading_main_edit a:hover,
        .orange_heading_main_edit a:visited {
            text-decoration: none;
            color: #FF6600;
            font-weight: bold;
        }

    .order_detail_right ul {
        float: none;
        list-style: none;
        list-style-image: none;
        list-style-position: outside;
        list-style-type: none;
    }

        .order_detail_right ul li {
            float: none;
            text-align: right;
            vertical-align: middle;
            overflow: hidden;
        }
</style>

<div class="center_container">
    <div class="form_container">
        <div class="orange_heading_profile">
            <%=Resources.ShoppingCart.OrderDetails%>
        </div>
        <div class="order_conformation">
            <div class="order_detail_right">
                <ul>
                    <li>
                        <asp:HyperLink runat="server" ID="backToQueue" Target="_self" class="blue_linked" title="<%$Resources:ShoppingCart,GoToQueue%>">&lt; <%=Resources.Shared.Back%></asp:HyperLink>
                    </li>
                    <li style="margin: 5px 0;">
                        <span class="red_btn" style="margin-top: 0px;">
                            <asp:Label ID="lblStatustitle" runat="server"></asp:Label></span>
                    </li>
                    <li style="clear: both;">
                        <asp:HyperLink ID="lnkOverrideStatus" Text="<%$Resources:ShoppingCart,OverrideStatus%>" Style="cursor: pointer; margin-top: 5px;" runat="server" />
                    </li>
                </ul>
                <ajax2:ModalPopupExtender ID="mpeOverrideStatus" CacheDynamicResults="true" Drag="false" BackgroundCssClass="modalBackground" CancelControlID="btnOverrideStatusCancel" PopupControlID="pnlOverrideStatus" TargetControlID="lnkOverrideStatus" RepositionMode="RepositionOnWindowScroll" runat="server" />

                <asp:Panel ID="pnlOverrideStatus" Style="display: none;" CssClass="modalPopup" runat="server">
                    <div><%=Resources.ShoppingCart.ChangeStatusFillOutForm%>:</div>
                    <div>
                        <%=Resources.ShoppingCart.ChangeStatusTo%> : 
								<asp:DropDownList ID="ddlStatus" runat="server" />
                    </div>
                    <div>
                        <asp:CheckBox ID="chkNotifyCustomer" runat="server" /><label for='<%# chkNotifyCustomer.ClientID %>'><%=Resources.ShoppingCart.NotifyCustomer%></label>
                    </div>
                    <div>
                        <asp:CheckBox ID="chkNotifyPrinterAdmin" runat="server" /><label for='<%# chkNotifyPrinterAdmin.ClientID %>'><%=Resources.ShoppingCart.NotifyPrinterAdmin%></label>
                    </div>
                    <div>
                        <%=Resources.ShoppingCart.ReasonNoteInNotificationEmail%>
                    </div>
                    <div>
                        <asp:TextBox ID="txtNote" TextMode="MultiLine" runat="server" />
                    </div>
                    <div class="buttonContainer">
                        <span class="bg_btn_left mr11 fr">
                            <asp:Button ID="btnOverrideStatusCancel" Text="<%$Resources:Shared,Cancel%>" CssClass="submit_bg_btn" Style="border: none;" runat="server" />
                        </span>
                        <span class="bg_btn_left mr11 fr">
                            <asp:Button ID="btnOverrideStatusOK" OnClick="OverrideStatusOK_Click" Text="<%$Resources:Shared,Ok%>" CssClass="submit_bg_btn" Style="border: none;" runat="server" />
                        </span>
                    </div>
                </asp:Panel>
                </ul>                                    
            </div>
            <div class="order_detail">
                <span>
                    <asp:Label ID="lblOrderNo" runat="server"></asp:Label></span> <span>
                        <asp:Label ID="lblOrderDate" runat="server"></asp:Label></span>
            </div>
        </div>
        <div class="clr">
        </div>
        <ajax2:TabContainer ID="tabs" runat="server" ActiveTabIndex="0">
            <ajax2:TabPanel runat="server" ID="tabOrderDetails" HeaderText="<%$Resources:ShoppingCart,OrderDetails%>">
                <ContentTemplate>
                    <div class="faq_blog">
                        <div class="roundcont_error mrb_25" id="divError" runat="server" visible="False">
                            <div class="roundtop">
                                <img src="/portal/inc/images/tl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
                            </div>
                            <div style="float: left; width: 80px; text-align: center;">
                                <img src="/portal/inc/images/error_img.gif" border="0px" alt="" width="40" height="40" />
                            </div>
                            <div style="float: left; width: 800px; padding-top: 10px;">
                                <asp:Label ID="lblError" CssClass="span_1" runat="server" Visible="False"></asp:Label>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="roundbottom">
                                <img src="/portal/inc/images/bl.gif" alt="" width="15" height="15" class="corner" style="display: none" />
                            </div>
                        </div>
                        <div class="main_heading">
                            <span class="order_heading width110 pdl_15px pdr_15px"><%=Resources.ShoppingCart.Sku.ToUpperInvariant()%></span>
                            <span class="order_heading width165"><%=Resources.ShoppingCart.Title%></span>
                            <span class="order_heading  width75 text_right"><%=Resources.ShoppingCart.Quantity%></span>
                            <span class="order_heading text_right"><%=Resources.ShoppingCart.Price%></span>
                            <span class="order_heading width150" style="padding-left: 20px;"><%=Resources.ShoppingCart.ProofPdf%></span>
                        </div>
                        <div class="main_heading_bg" id="divPooofGrid">
                            <div id="divItemgrid" runat="server">
                                <asp:Repeater runat="server" ID="rptItems" OnItemDataBound="rptItems_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="chk_outgrid" id="divRowItemTemplate" runat="server">
                                            <span class="order_heading_data width110 pdl_15px pdr_15px word_wrap"><%#Eval("SKU")%></span>
                                            <span class="order_heading_data width165"><%#Eval("ItemName")%></span>
                                            <span class="order_heading_data width75 text_right"><%#Eval("ItemQuantity")%></span>
                                            <span class="order_heading_data text_right"><%# GetPrice(Eval("ItemPrice").ToString())%>&nbsp;</span>
                                            <asp:Label runat="server" ID="lblItemID" Visible="false" Text='<%# Eval("ItemID").ToString()%>'></asp:Label>
                                            <span class="order_heading_data_upload pd_left20 width150">
                                                <asp:PlaceHolder ID="placeImageNoCustomization" runat="server" Visible='<%# Eval("Customizations").ToString() == "False" %>'>
                                                    <asp:Image ID="imgIcon" runat="server" />
                                                    <asp:Literal ID="litIcon" runat="server"></asp:Literal>
                                                </asp:PlaceHolder>
                                                <asp:PlaceHolder ID="placeImageUpload" runat="server" Visible='<%# Eval("Customizations").ToString() == "True" %>'>
                                                    <asp:Literal ID="ltUploadMessage" Visible='<%# Eval("ProofPDF") == null && !IsPrinter() %>' runat="server" Text="<%$Resources:ShoppingCart,CustomizedProofReviewDescription%>"></asp:Literal>
                                                    <uc1:FileUpload ID="UploadProofFile" Visible="false" runat="server" Value='<%# ((Eval("ProofPDF")!=null) ?  Eval("ProofPDF").ToString() : "") %>' />
                                                    <asp:HyperLink ID="lnkProofFile" runat="server"></asp:HyperLink>
                                                    <asp:HyperLink ID="lnkReplaceProofFile" Visible="false" Text="<%$Resources:Shared,Replace%>" runat="server" />
                                                </asp:PlaceHolder>
                                            </span>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                            <asp:Panel ID="pnlPromotion" class="main_heading_bg promotion" runat="server" Visible="false">
                                <asp:Panel ID="pnlPromotionApplied" runat="server" CssClass="chk_outgrid">
                                    <div class="promo-description">
                                        <asp:Literal ID="litPromotionDescription" runat="server"></asp:Literal>
                                    </div>
                                    <div class="promo-amount">
                                        <asp:Literal ID="litPromotionAmount" runat="server"></asp:Literal>
                                    </div>
                                </asp:Panel>
                            </asp:Panel>
                            <div style="float: right; width: 155px; margin-bottom: 20px; font-weight: bold; font-size: 11px;">
                                <%=Resources.ShoppingCart.Total%>:&nbsp;<asp:Literal ID="litTotal" runat="server"></asp:Literal>
                            </div>
                            <div class="clear"></div>
                            <div id="Approvaldiv" runat="server">
                                <asp:PlaceHolder runat="server" ID="plcPrinter">
                                    <div class="fl" style="width: 400px; float: right;" align="right">
                                        <span class="bg_btn_left mr11 fr">
                                            <a id="btnPrinterCancel" runat="server" class="submit_bg_btn" target="_top"><%=Resources.Shared.Cancel%></a>
                                        </span><span class="bg_grey_left fr mr11" runat="server" id="spnUpdateButton">
                                            <asp:Button ID="btnPrinterComplete" BorderWidth="0px" CssClass="grey_bg_btn" runat="server" Enabled="False" Text="<%$Resources:Shared,Save%>" />
                                        </span>
                                    </div>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder runat="server" ID="plcUser">
                                    <div style="width: 75px;" class="bg_grey_left fr">
                                        <asp:Button ID="btnSubmit" BorderWidth="0px" CssClass="grey_bg_btn" Enabled="False" runat="server" Text="<%$Resources:Shared,Submit%>" OnClick="bnSubmit_Click" />
                                    </div>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="plcUserOptions" runat="server" Visible="False">
                                    <div class="fl order_radio_textaria" style="width: 550px; margin-left: 15px;">
                                        <asp:RadioButton ID="chkApprove" GroupName="UserOption" runat="server" Text="<%$Resources:ShoppingCart,ApproveAllItems%>" />
                                        <span class="fl">
                                            <asp:RadioButton ID="chkFeedback" GroupName="UserOption" runat="server" Text="<%$Resources:ShoppingCart,ProvideFeedbackCorrections%>" /></span>
                                    </div>
                                </asp:PlaceHolder>
                                <div class="heading_span_new mrt_10">
                                    <%=Resources.ShoppingCart.Notes%>:
                                </div>
                                <div>
                                    <asp:TextBox ID="txtFeedback" TextMode="MultiLine" CssClass="nav_textarea_nav mr-top15" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div id="divcostadjustment" runat="server" visible="False">
                                <div class="heading_span_new mrt_10">
                                    <%=Resources.ShoppingCart.OrderLevelPriceAdjustment%>
                                </div>
                                <div class="chk_outgrid ">
                                    <div class="data_lost_right">
                                        <span id="spanSaveAdjustment" runat="server" class="bg_btn_left mr11 mar_none fr">
                                            <asp:Button ID="btnSaveAdjustment" BorderWidth="0px" CssClass="submit_bg_btn " runat="server" Text="<%$Resources:Shared,Save%>" /></span>
                                        <span class="fr mr11">
                                            <%= txtpriceadjustment.Visible ? "$" : "" %><asp:Label ID="lblpriceadjustment" Visible="False" runat="server" CssClass="label pricedetails"></asp:Label>
                                            <asp:TextBox ID="txtpriceadjustment" MaxLength="10" Visible="False" runat="server"></asp:TextBox>
                                        </span>
                                    </div>
                                    <div class="data_rtf_left">
                                        <asp:Label ID="lblDescription" CssClass="email" Visible="False" runat="server"></asp:Label>
                                        <asp:TextBox ID="txtDescription" TextMode="MultiLine" Visible="False" Rows="3" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="chk_outgrid" id="divorderhistory" runat="server">
                                <div class="heading_span_new mrt_10">
                                    <%=Resources.ShoppingCart.OrderProgressHistory%>
                                </div>
                                <div class="grid_rows_order">
                                    <asp:Repeater runat="server" ID="rptOrderHistory">
                                        <ItemTemplate>
                                            <div class="grid_detail">
                                                <span class="order_heading_data width150 ">
                                                    <%# (Eval("Date") != null) ? Convert.ToDateTime(Eval("Date").ToString()).ToString("MM/dd/yyyy") : ""%>&nbsp;</span>
                                                <span class="order_heading_data" style="width: 200px !important;"><%# ((Eval("Approval")!=null ? ParseLogUser(Eval("Approval").ToString()) : "")) %>&nbsp;</span>
                                                <span class="order_heading_data width150" style="width: 300px !important;"><%#Eval("Note")%>&nbsp;</span>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="meaning_bot"></div>
                    </div>
                    <div class="clr"></div>
                </ContentTemplate>
            </ajax2:TabPanel>

            <ajax2:TabPanel runat="server" ID="tabCustomizationDetails" HeaderText="Customization Details">
                <ContentTemplate>
                    <div class="faq_blog">
                        <!--Customisations-->
                        <asp:PlaceHolder ID="placeCustomizationDetails" runat="server">
                            <div class="main_heading">
                                <div class="orange_heading_main"><%=Resources.ShoppingCart.CustomizationDetails%></div>
                                <div class="orange_heading_main_edit">
                                    <asp:LinkButton ID="lnkBtnEdit" Text="<%$Resources:Shared,Edit%>" Visible='<%# HttpContext.Current.User.IsInRole(Mastercard.MarketingCenter.Common.Infrastructure.Constants.Roles.McAdmin) %>' OnClick="CustomizationEditLink_Click" runat="server" />
                                </div>
                            </div>
                            <div class="main_heading_bg">
                                <asp:Repeater runat="server" OnItemDataBound="rptCustomizations_ItemDataBound" ID="rptCustomizations">
                                    <ItemTemplate>
                                        <div id="divItemTemplate" runat="server">
                                            <div class="custmize_span">
                                                <%# Eval("FriendlyName")%><%#   ((Eval("Required") != null && Eval("Required").ToString().ToLower() == "true") ? "*" : "")%>
                                            </div>
                                            <div class="right_grid_custmize">
                                                <asp:PlaceHolder ID="PlaceHolderimg" runat="server">
                                                    <img id="image_cust" runat="server" height="128" width="131" src='<%# ((Eval("DefaultCustomizationData")!=null)) ? _urlService.GetStaticImageURL(Eval("DefaultCustomizationData").ToString(),200,200,false,85,true) : "" %>' alt="" />
                                                    <a runat="server" href='<%# ((Eval("DefaultCustomizationData")!=null)) ? _urlService.GetStaticImageURL(Eval("DefaultCustomizationData").ToString(),rootPath:true) : "" %>' visible="<%# IsPrinter() %>" class="price" target="new">Download</a>
                                                </asp:PlaceHolder>
                                                <asp:PlaceHolder ID="phImgEdit" runat="server">
                                                    <mc:ImageUpload ID="uploadImage" runat="server" Value='<%# (Eval("DefaultCustomizationData")!=null && Eval("FieldType").ToString()=="Image") ? Eval("DefaultCustomizationData").ToString():""%>' />
                                                </asp:PlaceHolder>
                                                <asp:HiddenField ID="hdnCustomizationOptionID" Value='<%# Eval("CustomizationOptionID").ToString() %>' runat="server" />
                                                <asp:PlaceHolder ID="PlaceHoldertxt" runat="server">
                                                    <asp:Label runat="server" ID="txtOptionsValue" CssClass="order_heading_data width_auto text_align_justify" Text='<%# (Eval("DefaultCustomizationData")!=null) ? Eval("DefaultCustomizationData").ToString().Replace("\r\n","<br/>") : "" %>' />
                                                </asp:PlaceHolder>
                                                <asp:PlaceHolder ID="phTextEdit" runat="server">
                                                    <asp:TextBox ID="txtOptionsValueEdit" Text='<%# (Eval("DefaultCustomizationData")!=null) ? Eval("DefaultCustomizationData").ToString().Replace("\r\n","<br/>") : "" %>' Width="390px" runat="server" />
                                                    <asp:HiddenField ID="hdnIsRequired" Value='<%# Eval("Required").ToString() %>' runat="server" />
                                                    <asp:HiddenField ID="hdnFriendlyName" Value='<%# Eval("FriendlyName").ToString() %>' runat="server" />
                                                    <span style="color: #ff6600; font-weight: bold;">
                                                        <asp:Literal ID="litErrMsg" runat="server" /></span>
                                                </asp:PlaceHolder>
                                                <div class="clr"></div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <div id="divEditButtonContainer" runat="server">
                                            <span class="bg_btn_left mr11 fr">
                                                <asp:Button ID="btnCancel" Text="<%$Resources:Shared,Cancel%>" OnClick="CustomizationCancel_Click" CssClass="submit_bg_btn" Style="border: none;" runat="server" />
                                            </span>
                                            <span class="bg_btn_left mr11 fr">
                                                <asp:Button ID="btnSave" Text="<%$Resources:Shared,Save%>" OnClick="CustomizationSave_Click" CssClass="submit_bg_btn" Style="border: none;" runat="server" />
                                            </span>
                                        </div>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                            <div class="main_heading_bot">
                            </div>
                        </asp:PlaceHolder>
                    </div>
                    <div class="clr"></div>
                </ContentTemplate>
            </ajax2:TabPanel>
            <ajax2:TabPanel runat="server" ID="tabShippingDetails" HeaderText="Fulfillment Information">
                <ContentTemplate>
                    <div class="faq_blog">
                        <!--User and Shipping Information-->
                        <div class="main_heading" style="padding-left: 13px;">
                            <span class="order_heading width178 ml14"><%=Resources.ShoppingCart.FulfillmentInformation%></span>
                            <span class="order_heading width95" style="padding-left: 10px;"><%=Resources.ShoppingCart.Sku.ToUpperInvariant()%></span>
                            <span class="order_heading width205"><%=Resources.ShoppingCart.Title%></span>
                            <span class="order_heading width80 text_right"><%=Resources.ShoppingCart.Quantity%></span>
                            <span class="order_heading text_right"><%=Resources.ShoppingCart.Price%></span>
                        </div>
                        <div class="main_heading_bg">
                            <mc:ElectronicFullfilment ID="electronicFullfilment" runat="Server"></mc:ElectronicFullfilment>
                            <mc:VdpFullfilment ID="vdpFullfilment" runat="Server"></mc:VdpFullfilment>
                            <asp:Repeater ID="rptShippingAddresses" runat="server" OnItemDataBound="rptShippingAddresses_ItemDataBound" OnItemCommand="rptShippingAddresses_ItemCommand">
                                <ItemTemplate>
                                    <div class="chk_outgrid fulfillment">
                                        <div class="right">
                                            <asp:Repeater ID="rptInnerSubOrders" runat="server" OnItemDataBound="rptInnerSubOrders_ItemDataBound" OnItemCommand="rptInnerSubOrders_ItemCommand">
                                                <ItemTemplate>
                                                    <div id="divFulfillmentOptions" runat="server" visible='<%# IsPrinter() %>' style="margin-bottom: 3px;">
                                                        <span class="order_heading_data_alt width95">
                                                            <asp:Label ID="lblCompleteSubOrder" runat="server" Visible='<%# Eval("Completed").ToString() == "True" %>' CssClass="completed-sub-order"><%=Resources.ShoppingCart.Completed%></asp:Label>
                                                            <asp:HyperLink ID="lnkSplitSubOrder" runat="server" NavigateUrl="javascript:void(0)"><%=Resources.ShoppingCart.Split%></asp:HyperLink>&nbsp;
                                                        </span>
                                                        <span class="order_heading_data width90">&nbsp;</span>
                                                        <span class="order_heading_data width90">&nbsp;</span>
                                                        <span class="order_heading_data_alt width90">&nbsp;<span id="spanCompleteSubOrder" runat="server" class="bg_btn_left complete_suborder mar_none"><asp:Button ID="btnCompleteSubOrder" runat="server" CommandName="COMPLETE" Text="<%$Resources:Shared,Complete%>" BorderWidth="0px" CssClass="submit_bg_btn" /></span>
                                                        </span>
                                                        <div class="clr"></div>
                                                    </div>
                                                    <asp:Repeater ID="rptInnerOrderItems" runat="server">
                                                        <ItemTemplate>
                                                            <div style="margin-bottom: 3px;">
                                                                <span class="fulfillment-item-col word_wrap">
                                                                    <%# Eval("SKU").ToString()%>
                                                                </span>
                                                                <span class="fulfillment-item-col-wide">
                                                                    <%# Eval("ItemName").ToString()%>
														
                                                                </span>
                                                                <span class="fulfillment-item-col right-align">
                                                                    <asp:Label runat="server" ID="txtQuantity" Text='<%# Eval("ItemQuantity").ToString()%>' readonly="true" size="20"></asp:Label>
                                                                </span>
                                                                <span class="fulfillment-item-col" style="width: 80px; text-align: right;">
                                                                    <%# GetPrice(Eval("ItemPrice").ToString())%>
                                                                </span>
                                                                <div class="clr"></div>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <div id="Div1" runat="server" visible='<%# Eval("PriceAdjustment") != null || Eval("ShippingCost") != null %>' class="grid_separator">&nbsp;</div>
                                                    <div id="Div2" style="margin-bottom: 3px;" runat="server" visible='<%# Eval("PriceAdjustment") != null && Decimal.Parse(Eval("PriceAdjustment").ToString()) > 0 %>'>
                                                        <span class="order_heading_data width175"><%=Resources.ShoppingCart.PriceAdjustment%>:
                                                        </span>
                                                        <span class="order_heading_data width100"><%# Eval("Description") %></span>
                                                        <span class="order_heading_data width90">$<%# Eval("PriceAdjustment") %>
                                                        </span>
                                                        <div class="clr"></div>
                                                    </div>
                                                    <div id="Div3" style="margin-bottom: 3px;" runat="server" visible='<%# Eval("ShippingCost") != null %>'>
                                                        <span class="order_heading_data width175"><%=Resources.ShoppingCart.ShippingCost%>:
                                                        </span>
                                                        <span class="order_heading_data width100">&nbsp;</span>
                                                        <span class="order_heading_data width90">$<%# Eval("ShippingCost")%>
                                                        </span>
                                                        <div class="clr"></div>
                                                    </div>
                                                    <div id="Div4" runat="server" visible='<%# Eval("TrackingNumber") != null %>' class="grid_separator">&nbsp;</div>
                                                    <div id="Div5" style="margin-bottom: 3px;" runat="server" visible='<%# Eval("TrackingNumber") != null %>'>
                                                        <span class="order_heading_data width175"><%=Resources.ShoppingCart.TrackingNumber%>:
                                                        </span>
                                                        <span class="order_heading_data width100"><%# Eval("TrackingNumber")%></span>
                                                        <span class="order_heading_data width90">&nbsp;</span>
                                                        <div class="clr"></div>
                                                    </div>

                                                    <div id="splitModal" runat="server" style="display: none; cursor: default">
                                                        <div class="modal_main_heading">
                                                            <span class="modal_heading pdl_15px"><%=Resources.ShoppingCart.SelectItemsForPartialShipment%></span>
                                                        </div>
                                                        <div class="modal_main_heading_bg">
                                                            <div class="modal_section border_rotbot">
                                                                <span class="order_heading_data width175 pdl_15px">
                                                                    <asp:Literal ID="ltModalAddress" runat="server"></asp:Literal>
                                                                </span>
                                                                <span class="order_heading_data">
                                                                    <asp:Literal ID="ltModalContactName" runat="server"></asp:Literal><br />
                                                                    <asp:Literal ID="ltModalPhone" runat="server"></asp:Literal>
                                                                </span>
                                                            </div>
                                                            <div id="modalItems" runat="server" class="modal_section border_rotbot">
                                                                <asp:Repeater ID="rptModalOrderItems" runat="server">
                                                                    <ItemTemplate>
                                                                        <div style="margin-bottom: 3px;">
                                                                            <span class="order_heading_data_alt width175 pdl_15px splitcheck word_wrap">
                                                                                <asp:CheckBox ID="chkSplitItem" runat="server" Text='<%# Eval("SKU").ToString()%>' />
                                                                            </span>
                                                                            <span class="order_heading_data_alt width175">
                                                                                <%# Eval("ItemName").ToString()%>
                                                                            </span>
                                                                            <asp:HiddenField ID="hdnOrderItemId" runat="server" Value='<%# Eval("OrderItemID").ToString()%>' />
                                                                            <div class="clr"></div>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </div>
                                                            <div class="modal_section">
                                                                <span class="bg_btn_left mr11 fr">
                                                                    <asp:Button ID="btnModalCancel" CommandName="CANCEL" BorderWidth="0px" CssClass="submit_bg_btn " runat="server" Text="<%$Resources:Shared,Cancel%>" /></span>
                                                                <span runat="server" id="spnComplete" class="bg_btn_left mr11 fr">
                                                                    <asp:Button ID="btnModalOk" CommandName="OK" BorderWidth="0px" CssClass="submit_bg_btn" runat="server" Text="<%$Resources:Shared,Ok%>" /></span>
                                                            </div>
                                                        </div>
                                                        <div class="modal_main_heading_bot"></div>
                                                    </div>
                                                    <div class="fr" style="margin-bottom: 4px; padding-bottom: 3px;">&nbsp;</div>
                                                    <div class="clr"></div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <div class="left">
                                            <span class="fulfillment-item-col"><%=Resources.ShoppingCart.ShippingInformation.ToUpperInvariant()%></span>
                                            <span class="fulfillment-item-col"><%# Eval("Address").ToString().Replace(Environment.NewLine, "<br />")%></span>
                                            <span class="fulfillment-item-col"><%# Eval("ContactName").ToString()%><br />
                                                <%# Eval("Phone").ToString()%></span>
                                            <div class="clr"></div>
                                            <span class="fulfillment-item-col" style="margin-top: 5px">
                                                <asp:LinkButton ID="lnkJoinShipments" runat="server" CommandName="JOIN"><%=Resources.ShoppingCart.JoinShipments%></asp:LinkButton></span>
                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="main_heading_bg" id="divspecialinstructions" runat="server">
                            <div class="chk_outgrid gray_backgrnd">
                                <div class="heading_span_new">
                                    <%=Resources.ShoppingCart.SpecialInstructionsDetails%></span>
                                </div>
                                <div class="fl joti">
                                    <asp:Label ID="lblSpecialInstructions" CssClass="instruction" runat="server"></asp:Label><br />
                                    <br />
                                    <asp:Literal ID="ltRushOrder" runat="server"></asp:Literal>
                                </div>
                            </div>
                        </div>
                        <div class="main_heading_bot"></div>
                    </div>
                    <div class="clr"></div>
                </ContentTemplate>
            </ajax2:TabPanel>
        </ajax2:TabContainer>
        <!--end grid container-->
        <div class="pd_bot fl">&nbsp;</div>
    </div>
</div>
