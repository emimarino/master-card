﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Pulsus;
using Slam.Cms.Admin.Core.Services;
using System.Web.Mvc;
using System.Web.Security;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class ErrorController : BaseController
    {
        public ActionResult Error404()
        {
            var serviceResponse = BrokenLinkService.GetNotFoundAction(Request);

            if (serviceResponse.Redirect)
            {
                return RedirectPermanent(serviceResponse.RedirectUrl);
            }

            if (Request.UrlReferrer != null && Request.UrlReferrer.Authority == Request.Url.Authority && !Request.UrlReferrer.ToString().Contains(FormsAuthentication.LoginUrl.TrimStart('~')))
            {
                LogManager.EventFactory.Create()
                                       .AddHttpContext()
                                       .Text("InternallyReferredBrokenLink: [{0}] to [{1}]", Request.UrlReferrer.ToString(), Request.Url.ToString())
                                       .Push();
            }

            Response.StatusCode = 404;

            if (serviceResponse.ShowCustomErrorMessage)
            {
                ViewBag.CustomErrorMessage = serviceResponse.CustomErrorMessage;
            }

            return View();
        }

        public ActionResult Error(string code)
        {
            ViewBag.Code = code;

            if (code != Constants.ErrorCodes.AccessDenied && code != Constants.ErrorCodes.ProgramExpired)
            {
                Response.StatusCode = 500;
            }

            return View();
        }
    }
}