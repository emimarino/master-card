﻿using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class SessionController : Controller
    {
        [HttpGet]
        public ActionResult IsLoggedIn()
        {
            var loggedIn = Session.Count > 0;

            return Json(new { loggedIn }, JsonRequestBehavior.AllowGet);
        }
    }
}