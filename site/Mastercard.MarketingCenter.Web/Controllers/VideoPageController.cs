﻿using Mastercard.MarketingCenter.Web.Core.Models;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class VideoPageController : BaseController
    {
        // GET: /VideoPage/
        public ActionResult Index()
        {
            return View("VideoPage", new VideoPageViewModel
            {
                SiteName = SiteName
            });
        }
    }
}