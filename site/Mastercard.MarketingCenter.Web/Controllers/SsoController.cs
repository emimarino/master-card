﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Models;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class SsoController : BaseController
    {
        private IIssuerService _issuerService;
        private ISettingsService _settingsService;

        public SsoController(IIssuerService issuerService, ISettingsService settingsService)
        {
            _issuerService = issuerService;
            _settingsService = settingsService;
        }

        public ActionResult Index(string region)
        {
            var targetUrl = _settingsService.GetEMarketingLoginHandshakeTargetUrl(region);
            if (targetUrl != null)
            {
                ViewData.Model = new SsoIndexViewModel
                {
                    TargetUrl = targetUrl,
                    Token = GetToken(targetUrl),
                    RedirectTo = GetRedirectTo()
                };
            }

            return View();
        }

        private string GetToken(string targetUrl)
        {
            var issuer = _issuerService.GetIssuerById(CurrentUser.IssuerId);
            var header = "{\"typ\":\"JWT\",\"alg\":\"HS256\"}";
            var claims = "{{\"email\":\"{0}\",\"name\":\"{1}\",\"issuer\":\"{2}\",\"iss\":\"{3}\",\"exp\":{4}}}".F(CurrentUser.Email, CurrentUser.FullName, issuer == null ? "" : issuer.Title, targetUrl ?? Request.Url.GetLeftPart(UriPartial.Authority), (int)(DateTime.UtcNow.AddHours(1) - new DateTime(1970, 1, 1)).TotalSeconds);
            var b64header = Convert.ToBase64String(Encoding.UTF8.GetBytes(header)).Replace('+', '-').Replace('/', '_').Replace("=", "");
            var b64claims = Convert.ToBase64String(Encoding.UTF8.GetBytes(claims)).Replace('+', '-').Replace('/', '_').Replace("=", "");

            var payload = "{0}.{1}".F(b64header, b64claims);
            byte[] message = Encoding.UTF8.GetBytes(payload);

            string signature = Convert.ToBase64String(new HMACSHA256(Encoding.UTF8.GetBytes(Constants.ApiVerificationHash)).ComputeHash(message))
                                      .Replace('+', '-')
                                      .Replace('/', '_')
                                      .Replace("=", "");

            return "{0}.{1}".F(payload, signature);
        }

        private string GetRedirectTo()
        {
            return Request.QueryString["RedirectTo"] != null ? Request.QueryString["RedirectTo"].ToString().Trim() : string.Empty;
        }
    }
}