﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core;
using Mastercard.MarketingCenter.Web.Core.Extensions;
using Mastercard.MarketingCenter.Web.Core.Filters;
using Mastercard.MarketingCenter.Web.Core.Models;
using Mastercard.MarketingCenter.Web.Core.Services;
using Slam.Cms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;
using CommonConstants = Mastercard.MarketingCenter.Common.Infrastructure.Constants;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class DynamicController : BaseController
    {
        private readonly SlamContextService _slamContextService;
        private readonly MastercardSitemap _sitemap;
        private readonly IRegionService _regionService;
        private readonly MastercardRolePrincipal _mastercardRolePrincipal;

        public DynamicController(SlamContextService slamContextService, Sitemap sitemap, IRegionService regionService, IPrincipal principal)
        {
            _slamContextService = slamContextService;
            _sitemap = sitemap as MastercardSitemap;
            _regionService = regionService;
            _mastercardRolePrincipal = principal as MastercardRolePrincipal;
        }

        [ChildActionOnly]
        [IgnorePageTracking]
        public ActionResult InitializeTabNavigator(string activeTab)
        {
            SitemapNode sitemapNode;
            if (Request.Params.AllKeys.Contains("nodeKey"))
            {
                sitemapNode = _sitemap.FindNode(_sitemap.RootNode, n => n.Key.ToLower() == Request.Params.Get("nodeKey"));
            }
            else
            {
                sitemapNode = _sitemap.FindNode(_sitemap.RootNode, n => n.Url.ToLower() == new Uri(HttpContext.Request.Url.ToString()).LocalPath.ToLower());
            }

            if (sitemapNode.Parent != null)
            {
                while (sitemapNode.Parent.Key != "home")
                {
                    sitemapNode = sitemapNode.Parent;
                }
            }

            sitemapNode = _sitemap.FilterSitemapByPermissions(_mastercardRolePrincipal.IsInFullAdminRole(), sitemapNode);

            var model = new TabNavigatorViewModel
            {
                Controller = (string)RouteData.Values["controller"],
                ActiveTab = activeTab,
                Tabs = new List<TabNavigatorTabViewModel>
                {
                    new TabNavigatorTabViewModel
                    {
                        Url = sitemapNode.Url,
                        Name = Resources.Shared.Overview
                    }
                }
            };

            var subMenus = new Dictionary<string, string>().SubMenu(sitemapNode.Key);
            foreach (var subMenu in subMenus)
            {
                model.Tabs.Add(new TabNavigatorTabViewModel
                {
                    Url = subMenu.Value,
                    Name = subMenu.Key
                });
            }

            return PartialView("TabbedPanel", model);
        }

        [IgnorePageTracking]
        public ActionResult Overview(string nodeKey)
        {
            if (nodeKey.IsNullOrWhiteSpace())
            {
                return RedirectToHome();
            }

            var rootNode = _sitemap.FilterSitemapByPermissions(_mastercardRolePrincipal.IsInFullAdminRole(), _sitemap.RootNode);
            var currentNode = _sitemap.FindNode(rootNode, n => n.Key.ToLower() == nodeKey.ToLower());
            if (currentNode == null)
            {
                return RedirectToHome();
            }

            new Dictionary<string, string>().SubMenu(currentNode.Key);

            ViewBag.title = currentNode.Title;

            var model = new DynamicOverviewViewModel
            {
                Page = _slamContextService.GetPageByNodeUrl(currentNode.Url, UserContext.SelectedRegion, true, true),

                CarouselSlides = currentNode.Location.IsNullOrWhiteSpace() ? new CarouselSlideViewModel() : new CarouselSlideViewModel
                {
                    Slides = _slamContextService.GetDynamicCarouselSlides(currentNode.Location)
                },

                sectionList = new List<TagBrowserItemThreeAcrossList>()
            };

            foreach (var subMenu in currentNode.Children)
            {
                var menu = new TagBrowserItemThreeAcrossList
                {
                    TitleFooterUrl = subMenu.Url,
                    Title = subMenu.Title,
                    TagBrowserItems = new List<ContentItemListItem>()
                };

                foreach (var children in subMenu.Children)
                {
                    string subMenuKey = subMenu.Key.Substring(0, subMenu.Key.LastIndexOf("#", StringComparison.Ordinal) > 0 ? subMenu.Key.LastIndexOf("#", StringComparison.Ordinal) : subMenu.Key.Length);
                    string childrenKey = children.Key.Substring(0, children.Key.LastIndexOf("-section", StringComparison.Ordinal));

                    menu.TagBrowserItems = menu.TagBrowserItems.Union(_slamContextService.GetDynamicOverviewItems(subMenuKey, childrenKey, true, children.SingleTagNode)
                                                                                         .Select(a => a.AsContentItemListItem()))
                                                               .Take(3);
                }

                model.sectionList.Add(menu);
            }

            model.Title = ViewBag.Title;
            model.SetupSectionsCrossSellBoxes();

            if (model.Page != null && model.Page.ContentTypeId != null && model.Page.ContentItemId != null)
            {
                TempData["ContentTypeId"] = model.Page.ContentTypeId;
                TempData["ContentItemId"] = model.Page.ContentItemId;
            }

            return View(model);
        }

        [IgnorePageTracking]
        public ActionResult DynamicPage(string nodeKey, string parentKey)
        {
            if (nodeKey.IsNullOrWhiteSpace() || parentKey.IsNullOrWhiteSpace())
            {
                return RedirectToHome();
            }

            var rootNode = _sitemap.FilterSitemapByPermissions(_mastercardRolePrincipal.IsInFullAdminRole(), _sitemap.RootNode);
            var currentNode = _sitemap.FindNode(rootNode, n => n.Key.ToLower() == nodeKey.ToLower());
            if (currentNode == null)
            {
                return RedirectToHome();
            }

            new Dictionary<string, string>().Sections(parentKey, nodeKey);
            var currentRegion = _regionService.GetById(UserContext.SelectedRegion);

            ViewBag.title = currentNode.Title;

            var model = new DynamicSectionViewModel
            {
                Page = _slamContextService.GetPageByNodeUrl(currentNode.Url, UserContext.SelectedRegion, true, true),

                CarouselSlides = currentNode.Location.IsNullOrWhiteSpace() ? new CarouselSlideViewModel() : new CarouselSlideViewModel
                {
                    Slides = _slamContextService.GetDynamicCarouselSlides(currentNode.Location)
                },

                Lists = new List<TagBrowserItemTwoAcrossListMarketingTab>(),
                TabTitle = currentNode.Title
            };

            foreach (var section in currentNode.Children)
            {
                string currentNodeKey = currentNode.Key.Substring(0, currentNode.Key.LastIndexOf("#", StringComparison.Ordinal) > 0 ? currentNode.Key.LastIndexOf("#", StringComparison.Ordinal) : currentNode.Key.Length);
                string sectionKey = section.Key.Substring(0, section.Key.LastIndexOf("-section", StringComparison.Ordinal));

                var contentItemListItems = _slamContextService.GetDynamicPageItems(currentNodeKey, sectionKey, true, section.SingleTagNode);

                model.Lists.Add(new TagBrowserItemTwoAcrossListMarketingTab()
                {
                    Title = section.Title,
                    ContentItemListItems = contentItemListItems.Select(a => a.AsContentItemListItem()),
                    ShowIfContentItemListsItemsIsEmpty = currentRegion != null && (User.IsInRole(CommonConstants.Roles.DevAdmin) || User.IsInRole(CommonConstants.Roles.McAdmin))
                });
            }

            TempData["ContentTypeId"] = model.Page.ContentTypeId;
            TempData["ContentItemId"] = model.Page.ContentItemId;

            model.SetupSectionsCrossSellBoxes();

            return View(model);
        }
    }
}