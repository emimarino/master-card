﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services.Exceptions;
using Mastercard.MarketingCenter.Web.Core;
using Mastercard.MarketingCenter.Web.Core.Models;
using Slam.Cms;
using Slam.Cms.Common;
using Slam.Cms.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class TagBrowserController : BaseController
    {
        private readonly SlamContext slamContext;

        public TagBrowserController(SlamContext slamContext)
        {
            this.slamContext = slamContext;
        }

        [HttpGet]
        public ActionResult Index(TagBrowserParameters parameters)
        {
            var filterTags = parameters.FilteredTags;
            var regionFilterTags = new string[0];
            if (parameters.FilteredTags.Contains(UserContext.SelectedRegion))
            {
                regionFilterTags = parameters.FilteredTags.Where(t => t == UserContext.SelectedRegion).ToArray();
                filterTags = parameters.FilteredTags.Where(t => t != UserContext.SelectedRegion).ToArray();

                if (filterTags.Length == 0) //If only the region tag is applied then this is equivalent to clearing the tag browser
                {
                    return Redirect(HttpContext.Request.CurrentExecutionFilePath.Replace(UserContext.SelectedRegion, string.Empty).TrimEnd('/'));
                }
            }

            var contentItems = slamContext.CreateQuery()
                                          .FilterTagBrowserTypes()
                                          .FilterSegmentations(CurrentSegmentationIds)
                                          .FilterTagBrowserTypesForSpecialAudience()
                                          .FilterTagBrowserTypesShowInTagBrowser()
                                          .FilterPseudoTaggedByParentTags(regionFilterTags)
                                          .FilterTaggedWith(filterTags)
                                          .FilterExpiredAssets()
                                          .OrderByFeaturedOnTag(parameters.LastFilteredTag)
                                          .IncludeTags()
                                          .Get();

            if (contentItems.Count() == 0)
            {
                throw new NotFoundException();
            }

            var tagTree = slamContext.GetTagTree()
                                     .Translate(UserContext.SelectedLanguage)
                                     .TranslateTagCategories(UserContext.SelectedLanguage)
                                     .UpdateContentItemCount(contentItems)
                                     .ExpandRegionMarketTags(contentItems)
                                     .FlattenMarketTags()
                                     .ForTagBrowser(this, parameters)
                                     .RemoveTagsWithNoItems()
                                     .RemoveTagCategoriesWithNoTags()
                                     .RemoveTagCategoriesWithSingleTag(new string[] { MarketingCenterDbConstants.TagCategories.Markets, MarketingCenterDbConstants.TagCategories.Languages });

            // List of tags
            var breadcrumbNodes = new List<SitemapNode>();
            var selectedTags = new List<string>();
            var tags = slamContext.GetTags();
            SitemapNode lastNode = null;
            List<string> accumulatedTags = new List<string>();
            foreach (var identifier in filterTags)
            {
                var tag = tags.FirstOrDefault(x => x.Identifier == identifier);
                if (tag == null)
                    continue;

                var tagTreeNode = tagTree.FindNode(n => n.Tag != null && n.Tag.TagId == tag.TagId);
                if (tagTreeNode != null)
                {
                    selectedTags.Add(tagTreeNode.Text);

                    if (tagTreeNode.Identifier.Contains('/'))
                    {
                        accumulatedTags.AddRange(tagTreeNode.Identifier.Split('/'));
                    }
                    else
                    {
                        accumulatedTags.Add(tagTreeNode.Identifier);
                    }

                    breadcrumbNodes.Add(new SitemapNode()
                    {
                        Key = tagTreeNode.Identifier,
                        Title = tagTreeNode.Text,
                        Url = Url.TagBrowser("tagbrowser").AddTags(accumulatedTags.ToArray()).Render()
                    });

                    lastNode = breadcrumbNodes[breadcrumbNodes.Count - 1];
                }
            }

            // Content Page
            var contentItemsPage = contentItems.GetPage(parameters.PageNumber, 10, x => x.AsContentItemListItem().Title);
            var pager = contentItemsPage.ToPager(i => Url.TagBrowser(parameters)
                                                         .Page(i)
                                                         .Render());

            // ViewModel
            var model = new TagBrowserIndexViewModel();
            model.TagTree = tagTree;
            model.BreadcrumbNodes = breadcrumbNodes;
            model.LastNode = lastNode;
            if (selectedTags.Count > 0)
            {
                model.LastTag = selectedTags.Last();
                var lastTag = tags.FirstOrDefault(x => x.Identifier == parameters.FilteredTags.Last());
                lastTag.Translate(UserContext.SelectedLanguage);
                model.LastTagPageContent = lastTag.PageContent;
                selectedTags.RemoveAt(selectedTags.Count - 1);
                model.SelectedTags = selectedTags;
            }

            model.ResultsCount = contentItemsPage.TotalCount;
            model.Results = new ContentItemList()
            {
                ContentItems = contentItemsPage.Select(ci => ci.AsContentItemListItem(x => x.FeatureLevel >= -1))
            };
            model.Pager = pager;
            model.FeaturedItems = new NarrowPanelList
            {
                Title = Resources.Shared.FeaturedItems,
                RenderThumbnail = true,
                ContentItemList = slamContext.CreateQuery().FilterTagBrowserTypes()
                                                           .FilterSegmentations(CurrentSegmentationIds)
                                                           .FilterTagBrowserTypesForSpecialAudience()
                                                           .FilterFeaturedOnLocation(MarketingCenterDbConstants.FeatureLocations.TagBrowser, FilterOperator.GreaterOrEqual, 0)
                                                           .Get()
                                                           .Select(a => a.AsContentItemListItem())
            };

            ViewData.Model = model;

            if (parameters.FilteredTags.Length > 0)
            {
                var service = DependencyResolver.Current.GetService<Mastercard.MarketingCenter.Web.Core.Services.IPageTrackingService>();
                service.TrackTagHit(parameters.FilteredTags);
            }

            return View();
        }

        [ChildActionOnly]
        public ActionResult BrowseAll()
        {
            var contentItems = slamContext.CreateQuery()
                                          .FilterTagBrowserTypes()
                                          .FilterSegmentations(CurrentSegmentationIds)
                                          .FilterTagBrowserTypesForSpecialAudience()
                                          .FilterExpiredAssets()
                                          .FilterTagBrowserTypesShowInTagBrowser()
                                          .IncludeTags()
                                          .Get();

            var columns = slamContext.GetTagTree()
                                     .Translate(UserContext.SelectedLanguage)
                                     .TranslateTagCategories(UserContext.SelectedLanguage)
                                     .ForTagBrowser(this, "tagbrowser")
                                     .UpdateContentItemCount(contentItems)
                                     .ExpandRegionMarketTags(contentItems)
                                     .FlattenMarketTags()
                                     .RemoveTagsWithNoItems()
                                     .RemoveTagCategoriesWithNoTags()
                                     .RemoveTagCategoriesWithSingleTag(new string[] { MarketingCenterDbConstants.TagCategories.Markets, MarketingCenterDbConstants.TagCategories.Languages })
                                     .ForMastercardFlyout();

            var model = new TagBrowserBrowseAllViewModel();
            model.Columns = columns;
            ViewData.Model = model;

            return PartialView();
        }
    }
}