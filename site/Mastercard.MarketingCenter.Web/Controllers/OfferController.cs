﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Filters;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Exceptions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Models;
using Mastercard.MarketingCenter.Web.Core;
using Mastercard.MarketingCenter.Web.Core.Models;
using Mastercard.MarketingCenter.Web.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using CommonConstants = Mastercard.MarketingCenter.Common.Infrastructure.Constants;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class OfferController : BaseController
    {
        private readonly IOfferService _offerService;
        private readonly IEventLocationService _eventLocationService;
        private readonly ISettingsService _settingsService;
        private readonly ICsvFileService _csvFileService;
        private readonly SlamContextService _slamContextService;
        private readonly EventLocationTree _eventLocationTree;

        public OfferController(IOfferService offerService, IEventLocationService eventLocationService, ISettingsService settingsService, ICsvFileService csvFileService, SlamContextService slamContextService)
        {
            _offerService = offerService;
            _eventLocationService = eventLocationService;
            _settingsService = settingsService;
            _csvFileService = csvFileService;
            _slamContextService = slamContextService;
            _eventLocationTree = _eventLocationService.GetEventLocationTree();
        }

        public ActionResult Index()
        {
            if (HasUserAccess())
            {
                return View();
            }

            throw new NotFoundException();
        }

        [NoCache]
        [HttpPost]
        public ActionResult List(OfferListViewModel model)
        {
            if (_settingsService.GetOffersEnabled(UserContext.SelectedRegion))
            {
                if (model.MarketApplicabilityIds != null)
                {
                    var markets = model.MarketApplicabilityIds.ToList();
                    markets.Add(UserContext.SelectedRegion);
                    model.MarketApplicabilityIds = markets;
                }

                var offers = _offerService.GetOffersByRegion(UserContext.SelectedRegion);

                if (model.CardExclusivityIds != null && model.CardExclusivityIds.Any())
                {
                    offers = offers.Where(o => o.CardExclusivityIds == null || !o.CardExclusivityIds.Any() || o.CardExclusivityIds.Intersect(model.CardExclusivityIds).Any());
                }

                if (model.CategoryIds != null && model.CategoryIds.Any())
                {
                    offers = offers.Where(o => o.CategoryIds.Intersect(model.CategoryIds).Any());
                }

                if (model.MarketApplicabilityIds != null && model.MarketApplicabilityIds.Any())
                {
                    offers = offers.Where(o => o.MarketApplicabilityIds.Intersect(model.MarketApplicabilityIds).Any());
                }

                if (model.StartDate > DateTime.MinValue && model.EndDate > DateTime.MinValue)
                {
                    offers = offers.Where(o => (!o.ValidityPeriodFromDate.HasValue && !o.ValidityPeriodToDate.HasValue)
                                            || (model.StartDate <= o.ValidityPeriodFromDate.Value && o.ValidityPeriodFromDate.Value <= model.EndDate)
                                            || (model.StartDate <= o.ValidityPeriodToDate.Value && o.ValidityPeriodToDate.Value <= model.EndDate)
                                            || (model.StartDate > o.ValidityPeriodFromDate.Value && o.ValidityPeriodToDate.Value > model.EndDate));
                }

                if (!model.Text.IsNullOrWhiteSpace())
                {
                    offers = offers.Where(o => o.Title.ToLower().Contains(model.Text.Trim().ToLower()) || o.ShortDescription.StripHtml().ToLower().Contains(model.Text.Trim().ToLower()) || o.LongDescription.StripHtml().ToLower().Contains(model.Text.Trim().ToLower()));
                }

                if (model.EventLocationIds != null && model.EventLocationIds.Any(el => el > -1))
                {
                    offers = offers.Where(o => o.EventLocations.Select(oel => oel.EventLocationId).Intersect(model.EventLocationIds).Any()
                                            // Find offers where the selected event location is its parent.
                                            || _eventLocationTree.FindNodes(n => !n.IsRoot && model.EventLocationIds.Contains(n.EventLocationId) && n.HasAnyChildren(ch => o.EventLocations.Select(oel => oel.EventLocationId).Contains(ch.EventLocationId))).Any()
                                            // Find offers where the selected event location is its child.
                                            || _eventLocationTree.FindNodes(n => !n.IsRoot && model.EventLocationIds.Contains(n.EventLocationId) && n.HasAnyParent(p => o.EventLocations.Select(oel => oel.EventLocationId).Contains(p.EventLocationId))).Any());
                }

                if (model.OfferQuantityOnly)
                {
                    return new JsonResult
                    {
                        Data = offers.Count(),
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }

                var offerList = offers.OrderBy($"{model.SortBy} {model.SortDirection}").ToList();
                model.Offers = offerList.GetRange(model.Start, (model.Length == 0 || offerList.Count < model.Length + model.Start) ? offerList.Count - model.Start : model.Length);

                return new JsonResult
                {
                    Data = model,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }

            throw new NotFoundException();
        }

        [NoCache]
        public ActionResult Filters()
        {
            if (_settingsService.GetOffersEnabled(UserContext.SelectedRegion))
            {
                var offers = _offerService.GetOffersByRegion(UserContext.SelectedRegion);
                var marketApplicabilities = _offerService.GetMarketApplicabilities(UserContext.SelectedRegion);
                var jsonResponse = new JsonResult
                {
                    Data = new OfferFiltersViewModel
                    {
                        Categories = GetCategoriesByOffers(offers),
                        CardExclusivities = GetCardExclusivitiesByOffers(offers),
                        MarketApplicabilities = GetMarketApplicabilitiesByOffers(offers, marketApplicabilities),
                        ShowMarketApplicabilityFilter = marketApplicabilities.Any()
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
                return jsonResponse;
            }

            throw new NotFoundException();
        }

        [NoCache]
        public ActionResult EventLocationFilter()
        {
            if (_settingsService.GetOffersEnabled(UserContext.SelectedRegion))
            {
                var offers = _offerService.GetOffersByRegion(UserContext.SelectedRegion);
                var jsonResponse = new JsonResult
                {
                    Data = new OfferEventLocationFilterViewModel
                    {
                        EventLocations = new List<EventLocationModel> {
                                             new EventLocationModel {
                                                 EventLocationId = -1,
                                                 Title = Resources.Offer.AllLocations,
                                                 Offers = -1
                                             }
                                         }
                                         .Concat(GetEventLocationsByOffers(offers))
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
                return jsonResponse;
            }

            throw new NotFoundException();
        }

        public ActionResult Details(string contentItemId)
        {
            if (HasUserAccess())
            {
                var offer = _offerService.GetOffer(contentItemId, UserContext.SelectedRegion);
                if (offer != null)
                {
                    TempData["ContentItemId"] = offer.ContentItemId;
                    TempData["ContentTypeId"] = MarketingCenterDbConstants.ContentTypeIds.Offer;

                    return View(new OfferDetailsViewModel
                    {
                        Offer = offer,
                        CarouselSlides = new CarouselSlideViewModel
                        {
                            Slides = new List<CarouselSlideItem> { offer.MainImage.AsCarouselSlideItem() }.Concat(offer.SecondaryImages.Select(x => x.ImageUrl.AsCarouselSlideItem()))
                        },
                        DownloadImages = new List<DownloadImage> {
                                             new DownloadImage { ImageTitle = Resources.Offer.MainImage, ImageUrl = offer.MainImage },
                                             new DownloadImage { ImageTitle = Resources.Offer.LogoImage, ImageUrl = offer.LogoImage }
                                         }
                                         .Concat(offer.SecondaryImages.Select((image, index) => new DownloadImage
                                         {
                                             ImageTitle = $"{Resources.Offer.SecondaryImage} {index + 1}",
                                             ImageUrl = image.ImageUrl
                                         }))
                    });
                }
            }

            throw new NotFoundException();
        }

        public ActionResult RelatedOffers(string relatedItems)
        {
            return PartialView(new RelatedContentItemsList
            {
                RelatedContentItems = _slamContextService.GetRelatedOffers(relatedItems.Split(','))
            });
        }

        private bool HasUserAccess()
        {
            return User.IsInFullAdminRole() ? _settingsService.GetAdminFrontEndOffersEnabled(UserContext.SelectedRegion)
                                            : _settingsService.GetEndUserFrontEndOffersEnabled(UserContext.SelectedRegion);
        }

        private IEnumerable<CardExclusivityModel> GetCardExclusivitiesByOffers(IEnumerable<OfferListModel> offers)
        {
            return _offerService.GetCardExclusivities()
                                .Where(ce => ce.Offers > 0 && offers.Any(o => o.CardExclusivityIds.Contains(ce.CardExclusivityId)))
                                .OrderBy(ce => ce.Title);
        }

        private IEnumerable<CategoryModel> GetCategoriesByOffers(IEnumerable<OfferListModel> offers)
        {
            return _offerService.GetCategories()
                                .Where(c => c.Offers > 0 && offers.Any(o => o.CategoryIds.Contains(c.CategoryId)))
                                .OrderBy(c => c.Title);
        }

        private IEnumerable<MarketApplicabilityModel> GetMarketApplicabilitiesByOffers(IEnumerable<OfferListModel> offers, IEnumerable<MarketApplicabilityModel> marketApplicabilities)
        {
            return marketApplicabilities.Where(ma => ma.Offers > 0 || offers.Any(o => o.MarketApplicabilityIds.Contains(UserContext.SelectedRegion)))
                                        .OrderBy(ma => ma.Title);
        }

        private IEnumerable<EventLocationModel> GetEventLocationsByOffers(IEnumerable<OfferListModel> offers)
        {
            return _eventLocationService.GetEventLocationModel()
                                        .Where(el => offers.Any(o => o.EventLocations.Any(oel => oel.EventLocationId.Equals(el.EventLocationId)
                                        // Find all parent event locations.
                                        || _eventLocationTree.FindNodes(n => !n.IsRoot && n.EventLocationId.Equals(el.EventLocationId) && n.HasAnyChildren(ch => ch.EventLocationId.Equals(oel.EventLocationId))).Any())))
                                        .OrderBy(c => c.Title);
        }

        [Authorize(Roles = CommonConstants.Roles.DevAdmin)]
        public ActionResult RefreshPricelessOffers()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = CommonConstants.Roles.DevAdmin)]
        public ActionResult RefreshPricelessOffers(HttpPostedFileBase csvFile)
        {
            if (csvFile == null || csvFile.ContentLength == 0 || !csvFile.FileName.EndsWith(".csv"))
            {
                ModelState.AddModelError("", "Please upload a valid csv file");
            }

            if (ModelState.IsValid)
            {
                var offers = _csvFileService.ReadCsv<ImportedOfferModel>(csvFile.InputStream);
                _offerService.InsertImportedOffers(offers);

                _offerService.ProcessPricelessOffers();

                _offerService.ProcessUnmatchedPricelessOffers();

                ModelState.AddModelError("", $"Successfully Refreshed Priceless Offers");
            }

            return View();
        }
    }
}