﻿using Mastercard.MarketingCenter.Web.Core.Results;
using Mastercard.MarketingCenter.Web.Core.Services;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class TrackingController : Controller
    {
        public IPageTrackingService _pageTrackingService { get; set; }

        [HttpPost]
        public ActionResult TrackLink(string href, string how)
        {
            _pageTrackingService.TrackClientClick(href, how);
            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult TrackPage()
        {
            _pageTrackingService.TrackPageAccess();
            return new EmptyResult();
        }

        public ActionResult TrackMailDispatcher(string mailDispatcherId)
        {
            if (int.TryParse(mailDispatcherId, out int id) && id > 0)
            {
                _pageTrackingService.TrackMailDispatcher(id);
                return new ImagePixelResult();
            }
            else
            {
                return HttpNotFound();
            }
        }
    }
}