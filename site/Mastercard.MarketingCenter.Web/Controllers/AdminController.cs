﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Filters;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.HttpModules;
using Mastercard.MarketingCenter.Web.Core.Models;
using Mastercard.MarketingCenter.Web.Core.Services;
using Slam.Cms;
using Slam.Cms.Common.Interfaces;
using Slam.Cms.Configuration;
using StackExchange.Profiling;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class AdminController : BaseController
    {
        private readonly IPreviewMode _previewMode;
        private readonly MastercardAuthenticationService _authenticationService;
        private readonly RedirectionService _redirectionService;
        private readonly CmsRoleAccessRepository _cmsRoleAccessRepository;
        private readonly IRegionService _regionService;

        public AdminController(IPreviewMode previewMode, MastercardAuthenticationService authenticationService, RedirectionService redirectionService, CmsRoleAccessRepository cmsRoleAccessRepository, IRegionService regionService)
        {
            _previewMode = previewMode;
            _authenticationService = authenticationService;
            _redirectionService = redirectionService;
            _cmsRoleAccessRepository = cmsRoleAccessRepository;
            _regionService = regionService;
        }

        [HttpGet]
        [NoCache]
        public ActionResult TogglePreviewMode(string returnUrl)
        {
            if (_previewMode.Enabled)
            {
                _authenticationService.RemoveSlamPreviewModeCookie();
            }
            else
            {
                _authenticationService.CreateOrUpdateSlamPreviewModeCookie();
            }

            if (returnUrl.IsNullOrWhiteSpace())
            {
                return RedirectToHome();
            }
            else
            {
                return Redirect(returnUrl);
            }
        }

        [HttpPost]
        public ActionResult RefreshVanityUrlCache()
        {
            VanityUrlCache.Invalidate();

            return Json(new { ok = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [NoCache]
        public ActionResult Close(string returnUrl)
        {
            _authenticationService.RemoveSlamPreviewModeCookie();
            _authenticationService.RemoveSlamAuthenticationCookie();

            if (returnUrl.IsNullOrWhiteSpace())
            {
                return RedirectToHome();
            }
            else
            {
                return Redirect(returnUrl);
            }
        }

        [HttpGet]
        [NoCache]
        public ActionResult ForceUser(string userName)
        {
            _authenticationService.CreateOrUpdateSlamAuthenticationCookie(userName);
            return Content("OK", "text/plain");
        }

        [HttpGet]
        public RedirectResult LoginHandshake()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return new RedirectResult("{0}?Admin=true&{1}".F(FormsAuthentication.LoginUrl, Request.Url.Query.TrimStart('?')));
            }

            var loginToken = _authenticationService.GetNewToken(UserContext.User.UserName, MembershipService.GetUser(UserContext.User.UserName).GetPassword());
            var redirectUrl = ConfigurationManager.Environment.AdminUrl.TrimEnd('/') + "/";
            if (Request.QueryString["ReturnUrl"] != null)
            {
                redirectUrl += Request.QueryString["ReturnUrl"].TrimStart('/');
            }

            redirectUrl += redirectUrl.Contains("?") ? "&t=" + loginToken : "?t=" + loginToken;

            return new RedirectResult(redirectUrl);
        }

        [HttpGet]
        public JsonpResult IsAuthenticated()
        {
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole(Constants.Roles.McAdmin))
                {
                    Session["SlamAuthenticationAndPreviewModeCookieFlag"] = true;
                }

                return new JsonpResult(true);
            }

            return new JsonpResult(false);
        }

        [HttpGet]
        public JsonpResult AuthenticateWithToken(string token)
        {
            if (token != null)
            {
                var usernameAndPassword = _authenticationService.GetUsernameAndPasswordFromLoginToken(new Guid(token));
                if (usernameAndPassword != null && Membership.Provider.ValidateUser(usernameAndPassword[0], usernameAndPassword[1]))
                {
                    FormsAuthentication.SetAuthCookie(usernameAndPassword[0], false);
                    return new JsonpResult(true);
                }
            }
            return new JsonpResult(false);
        }

        [HttpGet]
        public JsonpResult SetSlamAuthenticationAndPreviewModeCookieIfPublisher()
        {
            using (MiniProfiler.Current.Step("SetSlamAuthenticationAndPreviewModeCookieIfPublisher"))
            {
                return new JsonpResult(_authenticationService.SetSlamAdminCookies());
            }
        }

        [HttpPost]
        public ActionResult ClearAutocompleteCache()
        {
            SearchAutoCompleteService.ClearAutocompleteCache();

            return Json(new { ok = true }, JsonRequestBehavior.AllowGet);
        }

        [ChildActionOnly]
        public ActionResult Bar()
        {
            _authenticationService.SetSlamAdminCookies();
            var userName = _authenticationService.GetSlamUserName();

            if (userName.IsNullOrWhiteSpace())
            {
                return new EmptyResult();
            }

            var model = new AdminBarViewModel
            {
                ShowUserName = false,
                UserName = userName,
                AdminUrl = ConfigurationManager.Environment.AdminUrl,
                PreviewMode = _previewMode.Enabled,
                UserIsAdminInCurrentRegion = User.IsInAdminRole()
            };

            model.PageHasContentItemId = TempData["ContentItemId"] != null && TempData["ContentTypeId"] != null;
            if (model.PageHasContentItemId)
            {
                model.ContentItemId = TempData["contentItemId"].ToString();
                model.EditContentItemUrl = CheckCanAction(UserContext.SelectedRegion, "Content", "Edit", TempData["ContentTypeId"].ToString()) ?
                                                   string.Concat(ConfigurationManager.Environment.AdminUrl.TrimEnd('/'), "/Content/Edit?id={0}".F(model.ContentItemId)) :
                                                   string.Empty;
                model.CloneContentItemUrl = !_previewMode.Enabled && TempData["Clonable"] != null && TempData["Clonable"].ToString() == true.ToString() ?
                                            string.Concat(ConfigurationManager.Environment.AdminUrl, "/Content/Clone?id={0}&sourceRegion={1}".F(model.ContentItemId, UserContext.SelectedRegion)) : string.Empty;
            }

            model.UserAdminRegions = new List<Region>();
            if (!model.CloneContentItemUrl.IsNullOrEmpty())
            {
                foreach (var region in _regionService.GetAll())
                {
                    if (CheckCanAction(region.IdTrimmed, "Content", "Clone", TempData["ContentTypeId"].ToString()) && User.IsInFullAdminRole(region.IdTrimmed))
                    {
                        model.UserAdminRegions.Add(region);

                        if (region.IdTrimmed.Equals(UserContext?.SelectedRegion?.Trim() ?? string.Empty, StringComparison.OrdinalIgnoreCase))
                        {
                            model.UserIsAdminInCurrentRegion = true;
                        }
                    }
                }
            }

            ViewData.Model = model;
            return PartialView();
        }

        public void RedirectToAdmin()
        {
            _redirectionService.RedirectToAdmin();
        }

        private bool CheckCanAction(string region, string controller, string action, string id)
        {
            var accessibleRoutes = GetAuthorizedResourcesByUser().Where(a =>
                                string.Equals(a.Controller, controller, StringComparison.InvariantCultureIgnoreCase) &&
                                (string.IsNullOrEmpty(a.Action) || string.Equals(a.Action, action, StringComparison.InvariantCultureIgnoreCase)) &&
                                (string.IsNullOrEmpty(a.Id) || string.Equals(a.Id, id, StringComparison.InvariantCultureIgnoreCase)));

            //if the accessible routes have permission requirements one of them must be matched
            if (accessibleRoutes.Any(a => !string.IsNullOrEmpty(a.RequiredRolePermissionName)))
            {
                accessibleRoutes = accessibleRoutes.Where(a => !string.IsNullOrEmpty(a.RequiredRolePermissionName));
            }

            return accessibleRoutes.Any(a => a.RequiredRolePermissionName == null || User.IsInPermission(a.RequiredRolePermissionName, region));
        }

        private List<CmsRoleAccess> GetAuthorizedResourcesByUser()
        {
            return _cmsRoleAccessRepository.GetCmsRoleAccessByRoleType(User.GetUserRoleName(false)).ToList();
        }
    }
}