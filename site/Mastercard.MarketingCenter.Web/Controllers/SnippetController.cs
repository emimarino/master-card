﻿using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Web.Core.Models;
using Slam.Cms.Data;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class SnippetController : BaseController
    {
        private readonly SlamContext slamContext;

        public SnippetController(SlamContext slamContext)
        {
            this.slamContext = slamContext;
        }

        [ChildActionOnly]
        public ActionResult Snippet(string handle)
        {
            var model = new SnippetViewModel()
            {
                Snippet = slamContext.CreateQuery().FilterSegmentations(CurrentSegmentationIds).Filter($"Snippet.Title = '{handle}'").Get<SnippetDTO>().FirstOrDefault()
            };

            return PartialView(model);
        }
    }
}