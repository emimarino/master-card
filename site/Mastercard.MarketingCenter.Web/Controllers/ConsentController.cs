﻿using Mastercard.MarketingCenter.Data.Metadata.GDP;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Models;
using Mastercard.MarketingCenter.Web.Core.Models.Consent;
using Slam.Cms;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class ConsentController : BaseController
    {
        private readonly IConsentManagementService _consentManagementService;
        private readonly Sitemap _sitemap;
        private readonly IUserService _userServices;
        private readonly IIssuerService _issuerService;

        public ConsentController(IConsentManagementService consentManagementService, Sitemap sitemap, IUserService userServices, IIssuerService issuerService)
        {
            _sitemap = sitemap;
            _consentManagementService = consentManagementService;
            _userServices = userServices;
            _issuerService = issuerService;
        }

        private ActionResult ConsentResult(string locale, string key, string defaultValue)
        {
            if (string.IsNullOrEmpty(locale))
            {
                return HttpNotFound("Locale is missing");
            }
            var UseCode = ConfigurationManager.AppSettings[key] ?? defaultValue;
            var file = _consentManagementService.GetLatestLegalConsentData(UseCode, locale);
            if (file == null)
            {
                return HttpNotFound("Consent is missing.");
            }
            if (file.DocumentType.Equals(ConsentDocumentType.Document, StringComparison.OrdinalIgnoreCase))
            {
                return File(file.ConsentData, "application/pdf");
            }
            else
            {
                var model = new ConsentDataViewModel() { Body = file.ConsentData };
                return View("Consent", model);
            }
        }

        [AllowAnonymous]
        [ActionName("terms-of-use")]
        public ActionResult TermsOfUse(string locale)
        {
            return ConsentResult(locale, "TermsOfUse.UseCode", "tu");
        }

        [ActionName("privacy-notice")]
        public ActionResult PrivacyNotice(string locale)
        {
            return ConsentResult(locale, "PrivacyNotice.UseCode", "pn");
        }

        public ActionResult ConsentRenew()
        {
            return View("ConsentExpiredView", GetContentExpiredViewModel(UserContext.User.UserName));
        }

        private ConsentListViewModel GetContentExpiredViewModel(string username)
        {
            var user = _userServices.GetUserByUserName(username);
            var expiredContent = _consentManagementService.GetExpiredConsentForUser(username, user.Locale);

            return new ConsentListViewModel { consentFileData = expiredContent };
        }

        [HttpPost]
        public JsonResult AcceptedConsents(string acceptedConsentList)
        {
            var user = Membership.GetUser();
            if (user == null)
            {
                return Json(new
                {
                    status = "error",
                    message = "Could not find user"
                });
            }

            var acceptedConsentListIds = acceptedConsentList.Split(',').Select(x => int.Parse(x));
            _consentManagementService.InsertUserConsentByEmail(user.Email, acceptedConsentListIds);
            return Json(new
            {
                status = "ok",
                message = ""
            });
        }
    }
}