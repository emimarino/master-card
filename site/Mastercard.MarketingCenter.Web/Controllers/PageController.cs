﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services.Exceptions;
using Mastercard.MarketingCenter.Web.Core.Models;
using Mastercard.MarketingCenter.Web.Core.Services;
using Slam.Cms.Common;
using Slam.Cms.Data;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class PageController : BaseController
    {
        private readonly SlamContext _slamContext;
        private readonly SlamContextService _slamContextService;
        private readonly MigratedContentRepository _migratedContentRepository;

        public PageController(SlamContext slamContext, SlamContextService slamContextService, MigratedContentRepository migratedContentRepository)
        {
            this._slamContext = slamContext;
            this._slamContextService = slamContextService;
            this._migratedContentRepository = migratedContentRepository;
        }

        private ContentItem CheckMigratedContentItem(string id)
        {
            var migratedContent = _migratedContentRepository.GetMigratedContent(id);
            if (migratedContent == null)
            {
                return null;
            }

            return _slamContext.CreateQuery().FilterContentItemId(migratedContent.Id).Get().FirstOrDefault();
        }

        public ActionResult Page(string pageurl)
        {
            var page = _slamContextService.GetPageByUri("page/{0}".F(pageurl), true);
            if (page == null)
            {
                // we treat the pageurl as if it is an old guid, cause the old site used the route /page/{guid}
                var contentItem = CheckMigratedContentItem(pageurl);
                if (contentItem != null)
                {
                    return Redirect("~/" + contentItem.FrontEndUrl.TrimStart('/'));
                }

                throw new NotFoundException();
            }

            var model = new PageViewModel { Page = page };

            RouteData.Values.Add("contentItemId", model.Page.ContentItemId);

            if (!string.IsNullOrWhiteSpace(model.Page.RestrictToSpecialAudience))
            {
                if (!string.IsNullOrWhiteSpace(CurrentSpecialAudience))
                {
                    string[] stringSeparators = new string[] { ";#" };

                    var pageSpecialAudiences = model.Page.RestrictToSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    var currentSpecialAudiences = CurrentSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                    bool match = false;
                    foreach (var specAud in currentSpecialAudiences)
                    {
                        if (pageSpecialAudiences.Contains(specAud))
                        {
                            match = true;
                            break;
                        }
                    }

                    if (!match)
                    {
                        throw new AuthorizationException();
                    }
                }
                else
                {
                    throw new AuthorizationException();
                }
            }

            TempData["ContentItemId"] = page.ContentItemId;
            TempData["ContentTypeId"] = page.ContentTypeId;

            return View(model);
        }

        public ActionResult MasterCardResources()
        {
            var page = _slamContextService.GetPageByUri("mc-resources", true);
            if (page == null)
            {
                throw new NotFoundException();
            }

            var model = new MasterCardResourcesViewModel { Page = page };

            if (!string.IsNullOrWhiteSpace(model.Page.RestrictToSpecialAudience))
            {
                if (!string.IsNullOrWhiteSpace(CurrentSpecialAudience))
                {
                    string[] stringSeparators = new string[] { ";#" };

                    var pageSpecialAudiences = model.Page.RestrictToSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    var currentSpecialAudiences = CurrentSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                    bool match = false;
                    foreach (var specAud in currentSpecialAudiences)
                    {
                        if (pageSpecialAudiences.Contains(specAud))
                        {
                            match = true;
                            break;
                        }
                    }

                    if (!match)
                    {
                        throw new AuthorizationException();
                    }
                }
                else
                {
                    throw new AuthorizationException();
                }
            }

            TempData["ContentItemId"] = model.Page.ContentItemId;
            TempData["ContentTypeId"] = model.Page.ContentTypeId;

            return View(model);
        }
    }
}