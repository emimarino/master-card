﻿using AWS.Web.Data;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Models;
using Mastercard.MarketingCenter.Web.Core;
using Mastercard.MarketingCenter.Web.Core.Extensions;
using Mastercard.MarketingCenter.Web.Core.Models;
using Mastercard.MarketingCenter.Web.Core.Models.Shared;
using Mastercard.MarketingCenter.Web.Core.Services;
using Slam.Cms;
using Slam.Cms.Configuration;
using StackExchange.Profiling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using CommonConstants = Mastercard.MarketingCenter.Common.Infrastructure.Constants;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class HeaderController : BaseController
    {
        private readonly MastercardRolePrincipal _mastercardRolePrincipal;
        private readonly IUserRepository _userRepository;
        private readonly MastercardAuthenticationService _authenticationService;
        private readonly IRegionService _regionService;
        private readonly SlamContextService _slamContextService;
        private readonly IAuthorizationService _authorizationService;
        private readonly MastercardSitemap _sitemap;
        private readonly ISegmentationService _segmentationService;
        private readonly ISettingsService _settingsService;
        private readonly IMarketingCenterWebApplicationService _marketingCenterWebApplicationService;

        public HeaderController(string region = null, string language = null)
        {
            if (!region.IsNullOrEmpty() && !language.IsNullOrEmpty())
            {
                UserContext = new UserContext(region, language);
                _sitemap = new MastercardSitemap(UserContext);
            }
            else
            {
                UserContext = DependencyResolver.Current.GetService<UserContext>();
                _sitemap = (MastercardSitemap)DependencyResolver.Current.GetService<Sitemap>();
            }
            _mastercardRolePrincipal = DependencyResolver.Current.GetService<IPrincipal>() as MastercardRolePrincipal;
            _userRepository = DependencyResolver.Current.GetService<IUserRepository>();
            _authenticationService = DependencyResolver.Current.GetService<MastercardAuthenticationService>();
            _regionService = DependencyResolver.Current.GetService<IRegionService>();
            _slamContextService = DependencyResolver.Current.GetService<SlamContextService>();
            _authorizationService = DependencyResolver.Current.GetService<IAuthorizationService>();
            _segmentationService = DependencyResolver.Current.GetService<ISegmentationService>();
            _settingsService = DependencyResolver.Current.GetService<ISettingsService>();
            _marketingCenterWebApplicationService = DependencyResolver.Current.GetService<IMarketingCenterWebApplicationService>();
        }

        public HeaderController(IPrincipal principal, IUserRepository userRepository, MastercardAuthenticationService authenticationService, IRegionService regionService,
            SlamContextService slamContextService, IAuthorizationService authorizationService, ISegmentationService segmentationService,
            ISettingsService settingsService, IMarketingCenterWebApplicationService marketingCenterWebApplicationService, Sitemap sitemap,
            UserContext userContext)
        {
            _mastercardRolePrincipal = principal as MastercardRolePrincipal;
            _userRepository = userRepository;
            _authenticationService = authenticationService;
            _regionService = regionService;
            _slamContextService = slamContextService;
            _authorizationService = authorizationService;
            _segmentationService = segmentationService;
            _settingsService = settingsService;
            _marketingCenterWebApplicationService = marketingCenterWebApplicationService;
            _sitemap = (MastercardSitemap)sitemap;
            UserContext = userContext;
        }

        public MiniProfiler Profiler
        {
            get
            {
                return MiniProfiler.Current;
            }
        }

        [HttpPost]
        public ActionResult LanguageChanged(string selectedLanguage)
        {
            UserContext.SetUserSelectedLanguage(selectedLanguage);

            return new EmptyResult();
        }

        public ActionResult ChangeLanguage(string selectedLanguage)
        {
            UserContext.SetUserSelectedLanguage(selectedLanguage);
            return Redirect(Request.UrlReferrer.ToString());
        }

        [HttpPost]
        public ActionResult RegionChanged(string selectedRegion)
        {
            ChangeRegion(selectedRegion);

            return new EmptyResult();
        }

        private void ChangeRegion(string selectedRegion)
        {
            UserContext.SetUserSelectedRegion(selectedRegion);
            VerifyRegionAdminAccess(selectedRegion);
            EnsureUserCanAccessSelectedLanguage(false);
            if (Session["CurrentSegmentation"] != null)
            {
                Session["CurrentSegmentation"] = new string[] { RegionConfigManager.DefaultSegmentation };
            }
        }

        public ActionResult ChangeRegion(string selectedRegion, string redirectUrl = null)
        {
            ChangeRegion(selectedRegion);

            return Redirect(redirectUrl ?? Request.UrlReferrer.ToString());
        }

        private void VerifyRegionAdminAccess(string selectedRegion)
        {
            var region = _regionService.GetById(selectedRegion);
            if (region != null && !_mastercardRolePrincipal.IsInFullAdminRole())
            {
                _authenticationService.RemoveSlamAuthenticationCookie();
                _authenticationService.RemoveSlamPreviewModeCookie();
            }
        }

        [HttpPost]
        public ActionResult SegmentChanged(string[] selectedSegment)
        {
            if (_mastercardRolePrincipal.IsInPermission(CommonConstants.Permissions.CanChangeSegments))
            {
                _slamContextService.InvalidateAllTags();
                Session["CurrentSegmentation"] = selectedSegment;
            }

            return new EmptyResult();
        }

        public ActionResult ChangeSegment(string[] selectedSegment)
        {
            if (_mastercardRolePrincipal.IsInPermission(CommonConstants.Permissions.CanChangeSegments))
            {
                _slamContextService.InvalidateAllTags();
                Session["CurrentSegmentation"] = selectedSegment;
            }

            return Redirect(Request.UrlReferrer.ToString());
        }

        [HttpPost]
        public ActionResult CurateSearchText(string searchText)
        {
            var txt = searchText == null ? string.Empty : searchText.Trim();
            if (!string.IsNullOrEmpty(txt))
            {
                txt = txt.Replace("\"", "quotenc")
                         .Replace("<", "")
                         .Replace(">", "")
                         .Replace("*", "")
                         .Replace("%", "")
                         .Replace("&", "")
                         .Replace(":", "")
                         .Replace("\\", "")
                         .Replace("?", "");
            }

            return Json(new { curatedText = Server.UrlEncode(txt) });
        }

        [HttpPost]
        public ActionResult Feedback(bool giveFeedback)
        {
            _userRepository.SetFeedbackRequestDisplayed(UserContext.User.UserName);

            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult UpdateContentPreferencesMessage(bool displayContentPreferencesMessage)
        {
            _userRepository.SetContentPreferencesMessageDisplayed(UserContext.User.UserName, displayContentPreferencesMessage);

            return new EmptyResult();
        }

        public ActionResult Header()
        {
            return View(GetHeaderViewModel());
        }

        public HeaderViewModel GetHeaderViewModel(bool canChangeRegions = true, bool canSearchContent = true)
        {
            EnsureUserCanAccessSelectedLanguage();
            EnsureUserCanAccessSelectedRegion();
            var currentSegmentationIds = _marketingCenterWebApplicationService.GetCurrentSegmentationIds();
            var segmentations = _segmentationService.GetTranslatedSegmentations(UserContext.SelectedRegion, UserContext.SelectedLanguage);

            return new HeaderViewModel
            {
                CurrentSitemapNode = _sitemap.FilterSitemapByPermissions(_mastercardRolePrincipal.IsInFullAdminRole(), GetSitemapNode((HttpContext?.Items["CurrentSitemapNodeKey"]?.ToString() ?? Constants.DefaultSitemapNodeKey).ToLower())),
                Languages = LoadLanguageSelector(),
                Regions = LoadRegionSelector(),
                CanChangeRegions = canChangeRegions,
                CanSearchContent = canSearchContent,
                CanSeeNoneSegmentationButton = _mastercardRolePrincipal.IsInPermission(CommonConstants.Permissions.CanSeeNoneSegmentationButton),
                SitemapByPermissions = _sitemap.FilterSitemapByPermissions(_mastercardRolePrincipal.IsInFullAdminRole(), _sitemap.RootNode).Children,
                BusinessOwnerExpirationManagementEnabled = _settingsService.GetBusinessOwnerExpirationManagementEnabled(UserContext.SelectedRegion),
                CanUseShoppingCart = _mastercardRolePrincipal.IsInPermission(CommonConstants.Permissions.CanUseShoppingCart),
                ProcessorLogo = GetProcessorLogo(),
                SegmentationLabel = GetSegmentationLabel(segmentations, currentSegmentationIds),
                CanChangeSegments = _mastercardRolePrincipal.IsInPermission(CommonConstants.Permissions.CanChangeSegments),
                Segmentations = segmentations,
                CurrentSegmentationIds = currentSegmentationIds,
                SegmentationIds = new JavaScriptSerializer().Serialize(segmentations.Select(x => x.SegmentationId)),
                IsInAdminRole = _mastercardRolePrincipal.IsInAdminRole()
            };
        }

        public ActionResult GetSearchAutoComplete(SearchFilterModel searchFilters)
        {
            return new JsonResult
            {
                Data = SearchAutoCompleteService.GetSearchAutoCompleteResults(searchFilters.SearchTerm,
                                                                              searchFilters.CurrentSegmentationIds,
                                                                              searchFilters.CurrentSpecialAudience,
                                                                              searchFilters.RegionId,
                                                                              searchFilters.Language)
                                                .Take(5),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        private void EnsureUserCanAccessSelectedLanguage(bool redirect = true)
        {
            var languages = LoadLanguageSelector().ToList();
            TempData["languages"] = languages;

            if (UserContext?.User != null && !languages.Any(l => l.Code == UserContext.SelectedLanguage))
            {
                UserContext.SetUserSelectedLanguage(languages.Count > 0 ? languages.First().Code : RegionalizeService.DefaultLanguage);
                if (redirect)
                {
                    Response.Redirect(HttpContext.Items.Contains("OriginalRequestUrl") ? HttpContext.Items["OriginalRequestUrl"].ToString() : Request.Url.ToString()); // Entire page needs rebuilt after setting the cookie value
                }
            }
        }

        private void EnsureUserCanAccessSelectedRegion()
        {
            if (!MastercardSitemap.AvailableRegionSitemaps.ContainsKey(UserContext.SelectedRegion))
            {
                ChangeRegion(UserContext.User.Region);

                Response.Redirect(Request.Url.ToString()); // Entire page needs rebuilt after change region
            }
        }

        private SitemapNode GetSitemapNode(string sitemapNodeKey)
        {
            return _sitemap.FindNode(_sitemap.RootNode, n => n.Key == sitemapNodeKey);
        }

        private IEnumerable<RegionLanguage> LoadLanguageSelector()
        {
            var languages = TempData["languages"] as IEnumerable<RegionLanguage>;

            return languages != null ? languages : _sitemap.GetLanguages(UserContext.SelectedRegion,
                                         _mastercardRolePrincipal.IsInRole(CommonConstants.Roles.McAdmin) || _mastercardRolePrincipal.IsInRole(CommonConstants.Roles.DevAdmin) ?
                                         string.Empty : UserContext.SelectedLanguage)
                           .Select(l => new RegionLanguage { Name = l.Value, Code = l.Key });
        }

        private IEnumerable<Region> LoadRegionSelector()
        {
            if (_mastercardRolePrincipal.IsInFullAdminRole(false))
            {
                return _regionService.GetAll().Where(r => MastercardSitemap.AvailableRegionSitemaps.ContainsKey(r.IdTrimmed));
            }
            else if (UserContext.User != null)
            {
                var userRegions = _authorizationService.GetUserRole(UserContext.User.UserId).Regions.Select(r => r.RegionId.Trim());
                return _regionService.GetAll()
                                     .Where(r => MastercardSitemap.AvailableRegionSitemaps.ContainsKey(r.IdTrimmed)
                                              && (UserContext.User.Region.Trim().Equals(r.IdTrimmed, StringComparison.InvariantCultureIgnoreCase)
                                              || userRegions.Contains(r.IdTrimmed)));
            }

            return null;
        }

        private string GetProcessorLogo()
        {
            var user = DataServices.ExecuteForList<Services.Data.RetrieveUserInformationResult>("RetrieveUserInformation", UserContext?.User?.UserName ?? string.Empty)
                                   .FirstOrDefault();
            if (user != null && !string.IsNullOrEmpty(user.CobrandLogo))
            {
                UrlHelper Url = new UrlHelper(HttpContext.Request.RequestContext);
                return Url.ImageUrl(user.CobrandLogo, 1000, 1000);
            }

            return string.Empty;
        }

        protected string GetSegmentationLabel(IEnumerable<SegmentationModel> segmentations, IEnumerable<string> currentSegmentationIds)
        {
            return !currentSegmentationIds.Contains(RegionConfigManager.DefaultSegmentation) && segmentations.Any(s => currentSegmentationIds.Contains(s.SegmentationId)) ?
                    string.Join(" + ", segmentations.Where(s => currentSegmentationIds.Contains(s.SegmentationId)).Select(s => s.SegmentationName)) :
                    currentSegmentationIds.Contains(RegionConfigManager.NoneSegmentation) ? Resources.Shared.None : Resources.Shared.All;
        }
    }
}