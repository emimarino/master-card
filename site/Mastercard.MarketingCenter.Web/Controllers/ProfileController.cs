﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Filters;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core;
using Mastercard.MarketingCenter.Web.Core.Models;
using Mastercard.MarketingCenter.Web.Core.Models.Profile;
using Mastercard.MarketingCenter.Web.Core.Services;
using Slam.Cms;
using Slam.Cms.Common;
using Slam.Cms.Common.Interfaces;
using Slam.Cms.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class ProfileController : BaseController
    {
        private readonly IPreviewMode _previewMode;
        private readonly UserSubscriptionService CurrentUserSubscriptionService;
        private readonly IUserService CurrentUserServices;
        private readonly IIssuerService _issuerService;
        private readonly RegionRepository _regionRepository;
        private readonly ITagService _tagService;
        private readonly IContentItemService _contentItemServices;
        private readonly IUnitOfWork _unitOfWork;
        private readonly RegionConfigManager _regionConfigManager;
        private readonly IAnonymousTokenService _anonymousTokenService;
        private readonly ISettingsService _settingsService;
        private readonly IEmailService _emailService;
        private readonly MastercardSitemap _sitemap;
        private readonly IRegionService _regionService;

        public ProfileController(
            IPreviewMode previewMode,
            UserSubscriptionService userSubscriptionService,
            IUserService userServices,
            IIssuerService issuerService,
            RegionRepository regionRepository,
            ITagService tagService,
            IContentItemService contentItemServices,
            RegionConfigManager regionConfigManager,
            IUnitOfWork unitOfWork,
            IAnonymousTokenService anonymousTokenService,
            ISettingsService settingsService,
            IEmailService emailService,
            IRegionService regionService,
            Sitemap sitemap)
        {
            _previewMode = previewMode;
            CurrentUserSubscriptionService = userSubscriptionService;
            CurrentUserServices = userServices;
            _issuerService = issuerService;
            _regionRepository = regionRepository;
            _tagService = tagService;
            _contentItemServices = contentItemServices;
            _regionConfigManager = regionConfigManager;
            _unitOfWork = unitOfWork;
            _anonymousTokenService = anonymousTokenService;
            _settingsService = settingsService;
            _emailService = emailService;
            _regionService = regionService;
            _sitemap = (MastercardSitemap)sitemap;
        }

        [ChildActionOnly]
        public ActionResult InitializeTabHeader(string activeTab, string returnUrl)
        {
            returnUrl = returnUrl.IsNullOrWhiteSpace() && Request.UrlReferrer != null && Request.UrlReferrer.Host == Request.Url.Host && IsValidReturnUrl(Request.UrlReferrer.PathAndQuery) ?
                        Request.UrlReferrer.PathAndQuery :
                        !returnUrl.IsNullOrWhiteSpace() && IsValidReturnUrl(returnUrl) ? returnUrl : "/portal";

            var model = new TabHeaderViewModel
            {
                Controller = "Profile",
                ActiveTab = activeTab,
                Tabs = (new List<TabHeaderTabViewModel>
                {
                    new TabHeaderTabViewModel {
                        Action = "Index",
                        Name = Resources.Shared.Profile,
                        ReturnUrl = returnUrl
                    }
                })
                .IncludeByFeatureKey(
                    new TabHeaderTabViewModel
                    {
                        Action = "MyFavorites",
                        Name = Resources.Forms.MyFavorites,
                        ReturnUrl = returnUrl
                    },
                    "ShowTab.MyFavorites")
                .IncludeByFeatureKey(
                    new TabHeaderTabViewModel
                    {
                        Action = "EmailNotifications",
                        Name = Resources.Forms.EmailNotifications,
                        ReturnUrl = returnUrl
                    },
                    "ShowTab.EmailNotifications")
                .ToList()
            };

            return PartialView("TabbedHeader", model);
        }

        public ActionResult Index(string returnUrl)
        {
            var model = GetProfileIndexViewModel(!returnUrl.IsNullOrWhiteSpace() && IsValidReturnUrl(returnUrl) ? returnUrl : "/portal");

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ProfileIndexViewModel model)
        {
            ValidateModel(model);

            var states = _regionService.GetStates();
            var countries = _tagService.GetCountries(CurrentUser.Region, UserContext.SelectedLanguage);
            var languages = _tagService.GetLanguages(CurrentUser.Region, _sitemap.GetLanguages(CurrentUser.Region));

            if (ModelState.IsValid)
            {
                CurrentUser.Profile.FirstName = model.FirstName ?? string.Empty;
                CurrentUser.Profile.LastName = model.LastName ?? string.Empty;
                CurrentUser.Profile.IssuerName = model.IssuerName ?? string.Empty;
                CurrentUser.Profile.Address1 = model.Address1 ?? string.Empty;
                CurrentUser.Profile.Address2 = model.Address2 ?? string.Empty;
                CurrentUser.Profile.Address3 = model.Address3 ?? string.Empty;
                CurrentUser.Profile.City = model.City ?? string.Empty;
                CurrentUser.Profile.State = model.State ?? string.Empty;
                CurrentUser.Profile.Zip = model.ZipCode ?? string.Empty;
                CurrentUser.Profile.Phone = model.Phone ?? string.Empty;
                CurrentUser.Profile.Fax = model.Fax ?? string.Empty;
                CurrentUser.Profile.Title = model.Title ?? string.Empty;
                CurrentUser.Profile.Processor = model.Processor ?? string.Empty;
                CurrentUser.Profile.Mobile = model.Mobile ?? string.Empty;
                CurrentUser.Country = (string.IsNullOrEmpty(model.Country) || !countries.ContainsKey(model.Country)) ? countries.FirstOrDefault().Key : model.Country;
                CurrentUser.Language = (string.IsNullOrEmpty(model.Language) || !languages.ContainsKey(model.Language)) ? languages.FirstOrDefault().Key : model.Language;

                CurrentUserServices.UpdateUser(CurrentUser);
                _unitOfWork.Commit();

                ViewBag.ProfileSuccess = true;
            }
            else
            {
                ViewBag.ProfileSuccess = false;
            }

            model.States = states;
            model.Countries = countries;
            model.Languages = languages;
            PopulateOptOutdata(ref model);

            return View(model);
        }

        [FeatureAction("ShowTab.MyFavorites")]
        public ActionResult MyFavorites(string returnUrl)
        {
            var model = new ProfileMyFavoritesViewModel
            {
                ContentItemUserSubscriptions = CurrentUserSubscriptionService.GetSubscribedContentItemUserSubscriptions(CurrentUser.UserId, UserContext.SelectedRegion),
                ReturnUrl = !returnUrl.IsNullOrWhiteSpace() && IsValidReturnUrl(returnUrl) ? returnUrl : "/portal"
            };

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Unsubscribe(Guid id)
        {
            var userData = _anonymousTokenService.GetAnonymousTokenData(id);
            if (userData?.UserId != null)
            {
                CurrentUserSubscriptionService.SaveUserSubscription(userData.UserId, MarketingCenterDbConstants.UserSubscriptionFrequency.Never);
                _unitOfWork.Commit();
                RegionalizeService.SetCurrentCulture(userData.RegionId, userData.OptionalParameter);

                return View(new UnsubscribeViewModel { ImageUrl = _emailService.GetEmailTemplateHeaderImageUrl(userData.RegionId) });
            }

            return new HttpUnauthorizedResult();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [FeatureAction("ShowTab.MyFavorites")]
        public ActionResult MyFavorites(ProfileMyFavoritesViewModel model)
        {
            try
            {
                if (model.AssignedContentItemIds != null)
                {
                    foreach (var contentItemId in model.AssignedContentItemIds)
                    {
                        if (model.SelectedContentItemIds == null || !model.SelectedContentItemIds.Contains(contentItemId))
                        {
                            CurrentUserSubscriptionService.SaveContentItemUserSubscription(contentItemId, CurrentUser.UserId, false);
                        }
                    }
                }

                ViewBag.ProfileSuccess = true;
                _unitOfWork.Commit();
            }
            catch
            {
                ViewBag.ProfileSuccess = false;
            }

            model.ContentItemUserSubscriptions = CurrentUserSubscriptionService.GetSubscribedContentItemUserSubscriptions(CurrentUser.UserId, UserContext.SelectedRegion);

            return View(model);
        }

        [FeatureAction("ShowTab.EmailNotifications")]
        public ActionResult EmailNotifications(string returnUrl)
        {
            var userSubscription = CurrentUserSubscriptionService.GetUserSubscription(CurrentUser.UserId);
            var model = new ProfileEmailNotificationsViewModel
            {
                Frequency = userSubscription == null ? MarketingCenterDbConstants.UserSubscriptionFrequency.Weekly : userSubscription.UserSubscriptionFrequencyId,
                ReturnUrl = !returnUrl.IsNullOrWhiteSpace() && IsValidReturnUrl(returnUrl) ? returnUrl : "/portal"
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [FeatureAction("ShowTab.EmailNotifications")]
        public ActionResult EmailNotifications(ProfileEmailNotificationsViewModel model)
        {
            try
            {
                CurrentUserSubscriptionService.SaveUserSubscription(CurrentUser.UserId, model.Frequency);
                ViewBag.ProfileSuccess = true;
                _unitOfWork.Commit();
            }
            catch
            {
                ViewBag.ProfileSuccess = false;
            }

            return View(model);
        }

        [ChildActionOnly]
        public ActionResult Subscription(string contentItemId)
        {
            var userId = CurrentUser.UserId;
            var userSubscription = CurrentUserSubscriptionService.GetContentItemUserSubscription(contentItemId, userId);

            var model = new ProfileSubscriptionViewModel
            {
                ContentItemId = userSubscription == null ? contentItemId : userSubscription.ContentItemId,
                IsUserSubscribed = userSubscription == null ? false : userSubscription.Subscribed,
                HideSubscriptions = _previewMode.Enabled,
                FrecuencyText = CurrentUserSubscriptionService.GetUserSubscriptionFrecuencyMessage(userId),
                showFirstTimeSubscribe = CurrentUserSubscriptionService.ShowFirstTimeSubscribe(userId),
                showNeverFrecuencyMessage = CurrentUserSubscriptionService.ShowNeverFrecuencyMessage(CurrentUser)
            };

            return PartialView(model);
        }

        [HttpGet]
        [NoCache]
        public JsonResult ToggleSubscription(string contentItemId)
        {
            var contentItem = _contentItemServices.GetContentItem(contentItemId);
            if (contentItem == null)
            {
                return Json(new { error = true }, JsonRequestBehavior.AllowGet);
            }

            var userId = CurrentUser.UserId;
            var model = new ToggleSubscriptionViewModel();

            if (CurrentUserSubscriptionService.ShowFirstTimeSubscribe(userId))
            {
                model.showFirstTimeSubscribe = true;
            }

            model.subscribed = CurrentUserSubscriptionService.ToggleContentItemUserSubscription(contentItemId, userId);

            if (model.subscribed && CurrentUserSubscriptionService.ShowNeverFrecuencyMessage(CurrentUser))
            {
                CurrentUserSubscriptionService.SetNeverFrecuencyMessageAsShown(userId);
                model.showNeverFrecuencyMessage = true;
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public bool UserBelongsToRegion(string region, Data.Entities.User _u = null)
        {
            if (string.IsNullOrEmpty(region))
            {
                return false;
            }

            Data.Entities.User u = _u == null ? CurrentUser : _u;
            string userRegion = string.IsNullOrEmpty(u.Region) ? RegionalizeService.DefaultRegion : u.Region;

            return userRegion.Trim().Equals(region.Trim(), StringComparison.InvariantCultureIgnoreCase);
        }

        private static bool IsValidReturnUrl(string returnUrl)
        {
            returnUrl = returnUrl.ToLowerInvariant();

            return !(returnUrl.Contains("login") || returnUrl.Contains("profile") || returnUrl.Contains("registration"));
        }

        private void PopulateOptOutdata(ref ProfileIndexViewModel model)
        {

            var currentLanguage = UserContext.SelectedLanguage != null && UserContext.SelectedLanguage.IndexOf("-") > 0 ?
                                                   UserContext.SelectedLanguage.Split('-')[1].ToLower() : UserContext.SelectedLanguage;
            model.OptOutText = _regionConfigManager.GetSetting($"OptOut.Text.{currentLanguage}", UserContext.SelectedRegion);
            model.OptOutText = string.IsNullOrEmpty(model.OptOutText) ? _regionConfigManager.GetSetting("OptOut.Text", UserContext.SelectedRegion) : model.OptOutText;
            model.OptOutLink = _regionConfigManager.GetSetting($"OptOut.Link.{currentLanguage}", UserContext.SelectedRegion);
            model.OptOutLink = string.IsNullOrEmpty(model.OptOutLink) ? _regionConfigManager.GetSetting("OptOut.Link", UserContext.SelectedRegion) : model.OptOutLink;
        }

        private ProfileIndexViewModel GetProfileIndexViewModel(string returnUrl)
        {
            var states = _regionService.GetStates();
            var countries = _tagService.GetCountries(CurrentUser.Region, UserContext.SelectedLanguage);
            var languages = _tagService.GetLanguages(CurrentUser.Region, _sitemap.GetLanguages(CurrentUser.Region));

            var model = new ProfileIndexViewModel
            {
                FirstName = CurrentUser.Profile.FirstName,
                LastName = CurrentUser.Profile.LastName,
                IssuerName = CurrentUser.Profile.IssuerName,
                Address1 = CurrentUser.Profile.Address1,
                Address2 = CurrentUser.Profile.Address2,
                Address3 = CurrentUser.Profile.Address3,
                State = CurrentUser.Profile.State,
                States = states,
                ZipCode = CurrentUser.Profile.Zip,
                Phone = CurrentUser.Profile.Phone,
                Fax = CurrentUser.Profile.Fax,
                Email = CurrentUser.Email,
                City = CurrentUser.Profile.City,
                Title = CurrentUser.Profile.Title,
                Mobile = CurrentUser.Profile.Mobile,
                Processor = CurrentUser.Profile.Processor,
                IssuerIca = CurrentUser.Issuer?.BillingId ?? "Unassigned",
                ReturnUrl = returnUrl,
                Countries = countries,
                Languages = languages,
                Country = CurrentUser.Country,
                Language = CurrentUser.Language
            };

            PopulateOptOutdata(ref model);

            return model;
        }

        private void ValidateModel(ProfileIndexViewModel model)
        {
            if (model.FirstName.IsNullOrWhiteSpace())
            {
                ModelState.AddModelError("", Resources.Errors.FirstNameEmpty);
            }

            if (model.LastName.IsNullOrWhiteSpace())
            {
                ModelState.AddModelError("", Resources.Errors.LastNameEmpty);
            }

            if (model.Title.IsNullOrWhiteSpace())
            {
                ModelState.AddModelError("", Resources.Errors.TitleEmpty);
            }

            if (model.IssuerName.IsNullOrWhiteSpace())
            {
                ModelState.AddModelError("", Resources.Errors.IssuerNameEmpty);
            }

            if (model.Address1.IsNullOrWhiteSpace() && (UserBelongsToRegion("us", CurrentUser) || UserBelongsToRegion("de", CurrentUser)))
            {
                ModelState.AddModelError("", Resources.Errors.Address1Empty);
            }

            if (model.City.IsNullOrWhiteSpace() && (UserBelongsToRegion("us", CurrentUser) || UserBelongsToRegion("de", CurrentUser)))
            {
                ModelState.AddModelError("", Resources.Errors.CityEmpty);
            }

            if (model.ZipCode.IsNullOrWhiteSpace() && (UserBelongsToRegion("us", CurrentUser) || UserBelongsToRegion("de", CurrentUser)))
            {
                ModelState.AddModelError("", (UserBelongsToRegion("us") ? Resources.Errors.ZipCodeEmptyUS : Resources.Errors.ZipCodeEmpty));
            }

            if (model.State.IsNullOrWhiteSpace() && UserBelongsToRegion("us", CurrentUser))
            {
                ModelState.AddModelError("", Resources.Errors.StateEmptyUS);
            }

            if (model.Phone.IsNullOrWhiteSpace())
            {
                ModelState.AddModelError("", Resources.Errors.PhoneEmpty);
            }

            if (model.Country.IsNullOrWhiteSpace())
            {
                ModelState.AddModelError("", Resources.Errors.CountryEmpty);
            }

            if (model.Language.IsNullOrWhiteSpace())
            {
                ModelState.AddModelError("", Resources.Errors.LanguageEmpty);
            }
        }
    }
}