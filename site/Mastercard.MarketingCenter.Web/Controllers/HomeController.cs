﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Filters;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core;
using Mastercard.MarketingCenter.Web.Core.Models;
using Mastercard.MarketingCenter.Web.Core.Services;
using Slam.Cms;
using Slam.Cms.Data;
using StackExchange.Profiling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class HomeController : BaseController
    {
        private readonly SlamContext _slamContext;
        private readonly SlamContextService _slamContextService;
        private readonly UserSubscriptionService _userSubscriptionService;
        private readonly Sitemap _sitemap;
        private readonly IMostPopularService _mostPopularService;
        private SlamQueryResult<MarqueeSlideDTO> _homeMarqueeSlides;
        private SlamQueryResult<YoutubeVideoDTO> _homeYoutubeVideos;
        private readonly ISettingsService _settingsService;

        public HomeController(SlamContext slamContext, SlamContextService slamContextService, UserSubscriptionService userSubscriptionService,
            Sitemap sitemap, IMostPopularService mostPopularService, ISettingsService settingsService)
        {
            _slamContext = slamContext;
            _slamContextService = slamContextService;
            _userSubscriptionService = userSubscriptionService;
            _sitemap = sitemap;
            _mostPopularService = mostPopularService;
            _settingsService = settingsService;
        }

        public SlamQueryResult<MarqueeSlideDTO> HomeMarqueeSlides
        {
            get
            {
                if (_homeMarqueeSlides == null)
                {
                    _homeMarqueeSlides = _slamContextService.GetHomeMarqueeSlides();
                }

                return _homeMarqueeSlides;
            }
        }

        public SlamQueryResult<YoutubeVideoDTO> HomeYoutubeVideos
        {
            get
            {
                if (_homeYoutubeVideos == null)
                {
                    _homeYoutubeVideos = _slamContextService.GetHomeYoutubeVideos();
                }

                return _homeYoutubeVideos;
            }
        }

        public MiniProfiler Profiler
        {
            get
            {
                return MiniProfiler.Current;
            }
        }

        public ActionResult Index()
        {
            HomeIndexViewModel model;
            var triggerMarketingCustomizedHomepageEnabled = false;
            bool.TryParse(RegionalizeService.RegionalizeSetting("TriggerMarketingCustomizedHomepage.Enabled", UserContext.SelectedRegion.Trim(), false), out triggerMarketingCustomizedHomepageEnabled);
            using (Profiler.Step("Load Model"))
            {
                var videos = HomeYoutubeVideos
                                    .FilterFeaturedOnLocation(MarketingCenterDbConstants.FeatureLocations.Home, FilterOperator.GreaterOrEqual, 0)
                                    .OrderBy(o => o.OrderNumber)
                                    .ThenByFeaturedOnLocation(MarketingCenterDbConstants.FeatureLocations.Home)
                                    .ThenByDescending(o => o.ModifiedDate)
                                    .Select(o => o.AsCarouselSlideItem());

                var marquees = HomeMarqueeSlides
                                .FilterFeaturedOnLocation(MarketingCenterDbConstants.FeatureLocations.Home, FilterOperator.GreaterOrEqual, 0)
                                .OrderBy(o => o.OrderNumber)
                                .ThenByFeaturedOnLocation(MarketingCenterDbConstants.FeatureLocations.Home)
                                .ThenByDescending(o => o.ModifiedDate)
                                .Select(o => o.AsCarouselSlideItem());

                model = new HomeIndexViewModel
                {
                    TriggerMarketingCustomizedHomepageEnabled = triggerMarketingCustomizedHomepageEnabled,
                    CarouselSlides = new CarouselSlideViewModel
                    {
                        Slides = videos.Union(marquees).OrderBy(o => o.Order)
                    },
                    QuickLinks = _slamContextService.GetHomeQuickLinks().OrderBy(o => o.OrderNumber),
                    FindAProductViewModel = GetFindProductViewModel(),
                    ShowOptimizationDashboard = VerifySitemapIncludesOptimizationDashboard(),
                    SiteName = SiteName,
                    PartialSiteName = SiteName.ToLower().Replace("mastercard", string.Empty),
                    ContentPreferencesViewModel = GetContentPreferencesViewModel(),
                    HomeBanner = (_slamContextService.GetHomeHeaderSnippet()?.Html ?? string.Empty),
                    HomeSubMarqueeText = (_slamContextService.GetHomeSubMarqueeSnippet()?.Html ?? string.Empty)
                };
            }

            return View(model);
        }

        public ActionResult AutomaticEmailNotifications()
        {
            var page = _slamContextService.GetPageByUri("Home/AutomaticEmailNotifications");
            var model = new AutomaticEmailNotificationsViewModel()
            {
                Title = (page?.Title ?? "Automatic Email Notifications"),
                AutomaticEmailNotificationsBody = (page?.IntroCopy ?? string.Empty)
            };
            var service = DependencyResolver.Current.GetService<IPageTrackingService>();
            if (page != null && page.ContentTypeId != null && page.ContentItemId != null)
            {
                TempData["ContentTypeId"] = page.ContentTypeId;
                TempData["ContentItemId"] = page.ContentItemId;
            }

            return View(model);
        }

        private bool VerifySitemapIncludesOptimizationDashboard()
        {
            using (Profiler.Step("VerifySitemapIncludesOptimizationDashboard"))
            {
                return _sitemap.FindNode("optimization") != null && User.IsInPermission(Common.Infrastructure.Constants.Permissions.CanViewOptimizationDashboard);
            }
        }

        private FindAProductViewModel GetFindProductViewModel()
        {
            using (Profiler.Step("GetFindProductViewModel"))
            {
                if (User.IsInPermission(Common.Infrastructure.Constants.Permissions.CanViewFindAProduct))
                {
                    var videos = HomeYoutubeVideos
                                    .FilterFeaturedOnLocation(MarketingCenterDbConstants.FeatureLocations.HomeCarousel, FilterOperator.GreaterOrEqual, 0)
                                    .OrderBy(o => o.OrderNumber)
                                    .ThenByFeaturedOnLocation(MarketingCenterDbConstants.FeatureLocations.HomeCarousel)
                                    .ThenByDescending(o => o.ModifiedDate)
                                    .Select(o => o.AsContentItemListItem());

                    var marquees = HomeMarqueeSlides
                                    .FilterFeaturedOnLocation(MarketingCenterDbConstants.FeatureLocations.HomeCarousel, FilterOperator.GreaterOrEqual, 0)
                                    .OrderBy(o => o.OrderNumber)
                                    .ThenByFeaturedOnLocation(MarketingCenterDbConstants.FeatureLocations.HomeCarousel)
                                    .ThenByDescending(o => o.ModifiedDate)
                                    .Select(o => o.AsContentItemListItem());

                    var model = new FindAProductViewModel
                    {
                        ContentItemListItems = videos.Union(marquees).OrderBy(o => o.Order)
                    };

                    var snippet = _slamContextService.GetProductBoxSnippet();
                    if (snippet != null && !snippet.Html.IsNullOrWhiteSpace())
                    {
                        model.FindAProductBoxHtml = snippet.Html;
                    }
                    else
                    {
                        var tagTree = _slamContext.GetTagTree();
                        tagTree.Translate(UserContext.SelectedLanguage);
                        model.ProductBoxItems = new Dictionary<TagTreeNode, IDictionary<TagTreeNode, IList<ContentItemListItem>>>();

                        var productsNode = _sitemap.FindNode("products");
                        if (productsNode != null)
                        {
                            model.ProductsSiteMapNode = productsNode;

                            foreach (var node in productsNode.Children)
                            {
                                var tagNode = tagTree.FindNode(n => n.Identifier == node.Key && n.Tag != null);
                                if (tagNode != null)
                                {
                                    model.ProductBoxItems.Add(tagNode, new Dictionary<TagTreeNode, IList<ContentItemListItem>>());
                                }

                                foreach (var subNode in node.Children)
                                {
                                    subNode.Key = subNode.Key.Replace("-section", string.Empty);
                                    if (subNode.Key.Contains("#"))
                                        subNode.Key = subNode.Key.Substring(0, subNode.Key.IndexOf("#"));
                                    var subTagNode = tagTree.FindNode(n => n.Identifier == subNode.Key && n.Tag != null);
                                    if (subTagNode != null)
                                    {
                                        model.ProductBoxItems[tagNode].Add(subTagNode, new List<ContentItemListItem>());
                                    }
                                }
                            }

                            var products = _slamContextService.GetHomeProducts();
                            foreach (var product in products)
                            {
                                var matchingProductNodes = model.ProductBoxItems.Keys.Where(k => product.Tags.Any(t => t.Identifier == k.Identifier));
                                if (matchingProductNodes.Any())
                                {
                                    matchingProductNodes.ForEach(matchingProductNode =>
                                       {
                                           var matchingCardTypeNode = model.ProductBoxItems[matchingProductNode].Keys.FirstOrDefault(k => product.Tags.Any(t => t.Identifier == k.Identifier));
                                           if (matchingCardTypeNode != null)
                                           {
                                               model.ProductBoxItems[matchingProductNode][matchingCardTypeNode].Add(product.AsContentItemListItem());
                                           }
                                       });

                                }
                            }

                            var itemsToRemove = new List<TagTreeNode>();
                            model.ProductBoxItems.ForEach(p =>
                            {
                                if (!p.Value.Any(t => t.Value.Any()))
                                {
                                    itemsToRemove.Add(p.Key);
                                }
                                else
                                {
                                    var subItemsToRemove = new List<TagTreeNode>();
                                    p.Value.ForEach(c =>
                                    {
                                        if (!c.Value.Any())
                                        {
                                            subItemsToRemove.Add(c.Key);
                                        }
                                    });

                                    foreach (var subItemToRemove in subItemsToRemove)
                                    {
                                        p.Value.Remove(subItemToRemove);
                                    }
                                }
                            });

                            foreach (var item in itemsToRemove)
                            {
                                model.ProductBoxItems.Remove(item);
                            }
                        }
                    }

                    return model;
                }

                return null;
            }
        }

        private ContentPreferencesViewModel GetContentPreferencesViewModel()
        {
            using (MiniProfiler.Current.Step("GetContentPreferencesViewModel"))
            {
                var snippets = _slamContextService.GetContentPreferencesButtonSnippets();

                var contentPreferencesButton = snippets.FirstOrDefault(p => p.Tags.Any(t => t.Identifier.Equals(UserContext.SelectedLanguage, StringComparison.OrdinalIgnoreCase)));
                if (contentPreferencesButton == null)
                {
                    contentPreferencesButton = snippets.FirstOrDefault(p => p.Tags.Any(t => t.Identifier.Equals(RegionalizeService.DefaultLanguage, StringComparison.OrdinalIgnoreCase)));
                }

                return new ContentPreferencesViewModel
                {
                    UserSubscriptionFrequency = _userSubscriptionService.GetUserSubscriptionFrequency(CurrentUser.UserId),
                    IsUserSubscribedToContent = _userSubscriptionService.IsUserSubscribedToContent(CurrentUser.UserId, UserContext.SelectedRegion),
                    ContentPreferencesMessageDisplayed = CurrentUser.Profile.ContentPreferencesMessageDisplayed,
                    ContentPreferencesButtonHtml = contentPreferencesButton != null && !contentPreferencesButton.Html.IsNullOrWhiteSpace() ? contentPreferencesButton.Html : string.Empty
                };
            }
        }

        [NoCache]
        public ActionResult GetMyFavoritesContent()
        {
            var userSubscriptions = _userSubscriptionService.GetSubscribedContentItemUserSubscriptions(CurrentUser.UserId, UserContext.SelectedRegion);
            var myFavoritesContentItems = _slamContextService.GetContentItemsFilterByIds(userSubscriptions.Select(us => us.ContentItemId).ToArray()).Select(ci => ci.AsContentItemListItem());

            return PartialView("_MyFavoritesContent", new MyFavoritesViewModel
            {
                MyFavoritesItems = myFavoritesContentItems.Join(userSubscriptions,
                                                                ci => ci.ContentItem.ContentItemId,
                                                                us => us.ContentItemId,
                                                                (ci, us) => new MyFavoritesItem
                                                                {
                                                                    ContentItemId = ci.ContentItem.ContentItemId,
                                                                    Title = ci.Title,
                                                                    Summary = ci.Summary,
                                                                    MainImageUrl = ci.MainImageUrl,
                                                                    ResourceUrl = ci.ResourceUrl,
                                                                    SavedDate = us.SavedDate
                                                                })
                                                          .OrderByDescending(mfi => mfi.SavedDate)
                                                          .Take(_settingsService.GetDefaultMyFavoritesTotalItems())
                                                          .OrderBy(mfi => mfi.Title)
                                                          .ThenByDescending(mfi => mfi.SavedDate)
            });
        }

        public ActionResult GetMostPopularContent()
        {
            var mostPopularActivities = _mostPopularService.GetMostPopularActivitiesByRegion(UserContext.SelectedRegion);
            var mostPopularContentItems = _slamContextService.GetContentItemsFilterByIds(mostPopularActivities.Select(mpa => mpa.ContentItemId).ToArray()).Select(ci => ci.AsContentItemListItem());

            return PartialView("_MostPopularContent", new MostPopularViewModel
            {
                MostPopularItems = mostPopularContentItems.Join(mostPopularActivities,
                                                                ci => ci.ContentItem.ContentItemId,
                                                                a => a.ContentItemId,
                                                                (ci, a) => new MostPopularItem
                                                                {
                                                                    ContentItemId = ci.ContentItem.ContentItemId,
                                                                    Title = ci.Title,
                                                                    MainImageUrl = ci.MainImageUrl,
                                                                    ResourceUrl = ci.ResourceUrl,
                                                                    LastVisitDate = a.LastVisitDate,
                                                                    VisitCount = a.VisitCount
                                                                })
                                                          .OrderByDescending(mpi => mpi.VisitCount)
                                                          .ThenByDescending(mpi => mpi.LastVisitDate)
                                                          .Take(_settingsService.GetDefaultMostPopularTotalItems()),
                LessItemsQuantity = _settingsService.GetDefaultMostPopularLessItems()
            });
        }
    }
}