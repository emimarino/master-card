﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Entities;
using Mastercard.MarketingCenter.Services.Exceptions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core;
using Mastercard.MarketingCenter.Web.Core.Extensions;
using Mastercard.MarketingCenter.Web.Core.Models;
using Mastercard.MarketingCenter.Web.Core.Models.ContentItems;
using Mastercard.MarketingCenter.Web.Core.Services;
using Slam.Cms;
using Slam.Cms.Common;
using Slam.Cms.Configuration;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Status = Slam.Cms.Data.Status;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    [Core.Filters.CustomAuthorize]
    public class ContentDetailController : BaseController
    {
        public const string CommandPrintOnDemand = "printondemmand";
        public const string CommandElectronicDelivery = "electronicdelivery";
        public const string CommandBatchPrint = "batchprint";
        private static readonly string OrderableAssetValidationMessage = System.Configuration.ConfigurationManager.AppSettings["ValidationMessages.OrderableAsset.Approver"];
        private readonly SlamContext _slamContext;
        private readonly SlamContextService _slamContextService;
        private readonly AssetRepository _assetRepository;
        private readonly IRegionService _regionService;
        private readonly MigratedContentRepository _migratedContentRepository;
        private readonly IContentItemService _contentItemService;
        private readonly IIssuerService _issuerService;
        private readonly IUrlService _urlService;
        private readonly IEmailService _emailService;
        private readonly ISettingsService _settingsService;

        private bool NotAllowedWithCurrentSegment(IEnumerable<ContentItemSegmentation> segments)
        {
            var isContentRestrictedToAnySegment = segments.Count() > 0;
            var isContentRestrictedToCurrenteSegment = segments.Any(s => CurrentSegmentationIds.Contains(s.SegmentationId));
            var isSegmentFilterOff = CurrentSegmentationIds.Any(s => s.Equals(RegionConfigManager.DefaultSegmentation, StringComparison.InvariantCultureIgnoreCase));
            return isContentRestrictedToAnySegment &&
                 !isContentRestrictedToCurrenteSegment &&
                 !isSegmentFilterOff;
        }

        public ContentDetailController(SlamContext slamContext, SlamContextService slamContextService, AssetRepository assetRepository, MigratedContentRepository migratedContentRepository, IContentItemService contentItemService, IUrlService urlService, IIssuerService issuerService, IRegionService regionService, IEmailService emailService, ISettingsService settingsService)
        {
            _slamContext = slamContext;
            _slamContextService = slamContextService;
            _assetRepository = assetRepository;
            _migratedContentRepository = migratedContentRepository;
            _contentItemService = contentItemService;
            _urlService = urlService;
            _issuerService = issuerService;
            _regionService = regionService;
            _emailService = emailService;
            _settingsService = settingsService;
        }

        private ContentItem CheckMigratedContentItem(string id)
        {
            var migratedContent = _migratedContentRepository.GetMigratedContent(id);
            if (migratedContent == null)
            {
                return null;
            }

            var contentItem = _slamContext.CreateQuery().FilterContentItemId(migratedContent.Id).Get().FirstOrDefault();
            return contentItem;
        }

        public ActionResult MigratedContent(string globalid)
        {
            var contentItem = CheckMigratedContentItem(globalid);
            if (contentItem == null)
            {
                throw new NotFoundException();
            }

            return Redirect($"~/{contentItem.FrontEndUrl.TrimStart('/')}");
        }

        [HttpGet]
        public ActionResult Asset(string contentItemId, string title, string trackingType = null)
        {
            return ProcessContentItem(contentItemId, trackingType);
        }

        [HttpPost]
        public ActionResult Asset(string contentItemId, string command, int? quantity)
        {
            var contentItem = _slamContext.CreateQuery().FilterContentItemId(contentItemId).IncludeSegmentations().Get().FirstOrDefault();
            if (contentItem == null)
            {
                throw new NotFoundException();
            }

            var segments = _contentItemService.GetSegmentation(contentItem.ContentItemId);

            if (NotAllowedWithCurrentSegment(segments))
            {
                throw new AuthorizationException();
            }

            var orderableAsset = _contentItemService.GetOrderableAsset(contentItem.ContentItemId);
            if (!string.IsNullOrWhiteSpace(orderableAsset.RestrictToSpecialAudience))
            {
                if (!string.IsNullOrWhiteSpace(CurrentSpecialAudience))
                {
                    string[] stringSeparators = new string[] { ";#" };
                    var orderableAssetSpecialAudiences = orderableAsset.RestrictToSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    var currentSpecialAudiences = CurrentSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                    bool match = false;
                    foreach (var specAud in currentSpecialAudiences)
                    {
                        if (orderableAssetSpecialAudiences.Contains(specAud))
                        {
                            match = true;
                            break;
                        }
                    }

                    if (!match)
                    {
                        throw new AuthorizationException();
                    }
                }
                else
                {
                    throw new AuthorizationException();
                }
            }

            var model = ProcessOrderableAsset(orderableAsset, contentItem);
            model.Quantity = quantity;
            var addedToCart = false;
            switch (command)
            {
                case CommandElectronicDelivery:
                    addedToCart = ProcessElectronicDelivery(model);
                    break;
                case CommandPrintOnDemand:
                    addedToCart = ProcessPrintOnDemand(model, quantity);
                    break;
                case CommandBatchPrint:
                    addedToCart = ProcessBatchPrint(model, quantity);
                    break;
            }

            if (addedToCart)
                return Redirect("/portal/shoppingcart");

            return View("OrderableAsset", model);
        }

        public ActionResult ApplicableMarkets(string contentItemId)
        {
            var dbContext = DependencyResolver.Current.GetService<MarketingCenterDbContext>();
            var contentItemTags = dbContext.Tags.Where(t => t.ContentItems.Any(ci => ci.ContentItemId.Equals(contentItemId, StringComparison.OrdinalIgnoreCase))).Select(t => t.TagID);
            var regionId = UserContext.SelectedRegion;
            if (string.IsNullOrEmpty(regionId))
            {
                return null;
            }

            var region = _regionService.GetById(regionId);
            var node = _slamContext.GetTagTree()
                                   .FindNode(t => (t?.Tag?.TagId != null && (t?.Tag?.TagId?.Equals(region.TagId, StringComparison.OrdinalIgnoreCase) ?? false)) &&
                                                  (t.Parent?.TagCategory != null &&
                                                  (t.Parent?.TagCategory?.TagCategoryId?.Equals(MarketingCenterDbConstants.TagCategories.Markets, StringComparison.OrdinalIgnoreCase) ?? false)));

            var allregionMarkets = node.Children;
            if (allregionMarkets.Count < 2)
            {
                return new EmptyResult();
            }

            var regionMarkets = allregionMarkets.Where(cit => contentItemTags.Contains(cit.Parent.Tag.TagId) ||
                                                              contentItemTags.Contains(cit.Tag.TagId))
                                                .ForEach(rm => rm.Tag.Translate(UserContext.SelectedLanguage));

            var excludedregionMarkets = allregionMarkets.Where(cit => !(contentItemTags.Contains(cit.Parent.Tag.TagId)) &&
                                                                      (!contentItemTags.Contains(cit.Tag.TagId)))
                                                        .ForEach(rm => rm.Tag.Translate(UserContext.SelectedLanguage));

            var applicable = new List<string>();
            regionMarkets.ForEach((rm) =>
            {
                applicable.Add("\"" + rm.Tag.DisplayName + "\"");
            });
            var excluded = new List<string>();
            excludedregionMarkets.ForEach((rm) =>
            {
                excluded.Add("\"" + rm.Tag.DisplayName + "\"");
            });

            ViewData["CountriesTotal"] = allregionMarkets.Count;
            ViewData["AvailableInCountry"] = (regionMarkets.Any(rm => rm.Identifier.Equals(UserContext.User.Country.Trim(), StringComparison.Ordinal)) ?
                                              Resources.Forms.AvailableInCountry :
                                              Resources.Forms.NotAvailableInCountry);
            ViewData["AvailableInCountryImage"] = (regionMarkets.Any(rm => rm.Identifier.Equals(UserContext.User.Country.Trim(), StringComparison.Ordinal)) ?
                                                   Url.Content("~/Content/images/icons/Applicable.PNG") :
                                                   Url.Content("~/Content/images/icons/nonApplicable.PNG"));

            return PartialView(new ApplicableMarketsViewModel
            {
                Applicable = applicable.OrderBy(a => a),
                Excluded = excluded.OrderBy(e => e)
            });
        }

        public ActionResult Program(string contentItemId, string title)
        {
            return ProcessContentItem(contentItemId);
        }

        public ActionResult Webinar(string contentItemId, string title)
        {
            return ProcessContentItem(contentItemId);
        }

        private ActionResult ProcessContentItem(string contentItemId, string trackingType = null)
        {
            var contentItem = _slamContext.CreateQuery().FilterContentItemId(contentItemId).IncludeTags().IncludeSegmentations().AllowExpired().Get().FirstOrDefault();
            if (contentItem == null)
            {
                var ci = CheckMigratedContentItem(contentItemId);
                if (ci == null)
                {
                    throw new NotFoundException();
                }

                return Redirect($"~/{ci.FrontEndUrl.TrimStart('/')}");
            }

            TempData["ContentItemId"] = contentItem.ContentItemId;
            TempData["ContentTypeId"] = contentItem.ContentTypeId;
            TempData["Clonable"] = false;

            var segments = _contentItemService.GetSegmentation(contentItem.ContentItemId);
            if (NotAllowedWithCurrentSegment(segments))
            {
                throw new AuthorizationException();
            }

            if (contentItem.ExpirationDate.HasValue && contentItem.ExpirationDate < DateTime.Now)
            {
                throw new ProgramExpiredException();
            }

            if (contentItem.ContentTypeId == MarketingCenterDbConstants.ContentTypeIds.Asset)
            {
                var asset = _contentItemService.GetAsset(contentItem.ContentItemId);
                return ProcessAsset(asset, contentItem);
            }

            if (contentItem.ContentTypeId == MarketingCenterDbConstants.ContentTypeIds.AssetFullWidth)
            {
                var assetFullWidth = _contentItemService.GetAssetFullWidth(contentItem.ContentItemId);
                var routedata = RouteData;
                routedata.Values["Action"] = "AssetFullWidth";
                if (!DependencyResolver.Current.GetService<AuthorizationService>().Authorize(routedata))
                {
                    throw new AuthorizationException();
                }

                return ProcessAssetFullWidth(assetFullWidth, contentItem);
            }

            if (contentItem.ContentTypeId == MarketingCenterDbConstants.ContentTypeIds.DownloadableAsset)
            {
                var downloadableAsset = _contentItemService.GetDownloadableAsset(contentItem.ContentItemId);
                var routedata = RouteData;
                routedata.Values["Action"] = "DownloadableAsset";
                if (!DependencyResolver.Current.GetService<Core.Services.AuthorizationService>().Authorize(routedata))
                {
                    throw new AuthorizationException();
                }

                return ProcessDownloadableAsset(downloadableAsset, trackingType, contentItem);
            }

            if (contentItem.ContentTypeId == MarketingCenterDbConstants.ContentTypeIds.OrderableAsset)
            {
                var orderableAsset = _contentItemService.GetOrderableAsset(contentItem.ContentItemId);
                var routedata = RouteData;
                routedata.Values["Action"] = "OrderableAsset";
                if (!DependencyResolver.Current.GetService<AuthorizationService>().Authorize(routedata))
                {
                    throw new AuthorizationException();
                }

                if (!string.IsNullOrWhiteSpace(orderableAsset.RestrictToSpecialAudience))
                {
                    if (!string.IsNullOrWhiteSpace(CurrentSpecialAudience))
                    {
                        string[] stringSeparators = new string[] { ";#" };
                        var orderableAssetSpecialAudiences = orderableAsset.RestrictToSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                        var currentSpecialAudiences = CurrentSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                        bool match = false;
                        foreach (var specAud in currentSpecialAudiences)
                        {
                            if (orderableAssetSpecialAudiences.Contains(specAud))
                            {
                                match = true;
                                break;
                            }
                        }

                        if (!match)
                        {
                            throw new AuthorizationException();
                        }
                    }
                    else
                    {
                        throw new AuthorizationException();
                    }
                }

                return View("OrderableAsset", ProcessOrderableAsset(orderableAsset, contentItem));
            }

            if (contentItem.ContentTypeId == MarketingCenterDbConstants.ContentTypeIds.Program)
            {
                var program = _contentItemService.GetProgram(contentItem.ContentItemId);
                if (!string.IsNullOrEmpty(program.RedirectURL))
                {
                    return Redirect("~/" + program.RedirectURL.TrimStart('/'));
                }

                var routedata = RouteData;
                routedata.Values["Action"] = "Program";
                if (!DependencyResolver.Current.GetService<AuthorizationService>().Authorize(routedata))
                {
                    throw new AuthorizationException();
                }

                return ProcessProgram(program, contentItem);
            }

            if (contentItem.ContentTypeId == MarketingCenterDbConstants.ContentTypeIds.Webinar)
            {
                var webinar = _contentItemService.GetWebinar(contentItem.ContentItemId);
                return ProcessWebinar(webinar, contentItem);
            }

            return new EmptyResult();
        }

        private ActionResult ProcessAsset(Asset asset, ContentItem contentItem)
        {
            TempData["Clonable"] = asset.AllowCloning && (contentItem.StatusID.Equals(Status.Published) || contentItem.StatusID.Equals(Status.PublishedAndDraft));

            if (!string.IsNullOrWhiteSpace(asset.RestrictToSpecialAudience))
            {
                if (!string.IsNullOrWhiteSpace(CurrentSpecialAudience))
                {
                    string[] stringSeparators = new string[] { ";#" };
                    var assetSpecialAudiences = asset.RestrictToSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    var currentSpecialAudiences = CurrentSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                    bool match = false;
                    foreach (var specAud in currentSpecialAudiences)
                    {
                        if (assetSpecialAudiences.Contains(specAud))
                        {
                            match = true;
                            break;
                        }
                    }

                    if (!match)
                    {
                        throw new AuthorizationException();
                    }
                }
                else
                {
                    throw new AuthorizationException();
                }
            }

            var browseAllMaterialTags = _assetRepository.GetAssetBrowseAllMaterialTags(asset.ContentItemId).ToList();
            AddExpandedMarketTags(browseAllMaterialTags);

            var model = new AssetViewModel
            {
                ContentItemId = asset.ContentItemId,
                PrimaryContentItemId = contentItem.PrimaryContentItemId,
                FrontEndUrl = contentItem.FrontEndUrl,
                AssetType = asset.AssetType,
                CrossSellBox = asset.ShowInCrossSellBox,
                Title = asset.Title,
                EnableFavoriting = asset.EnableFavoriting,
                ShortDescription = asset.ShortDescription,
                LongDescription = asset.LongDescription,
                CallToAction = asset.CallToAction,
                MainImage = asset.MainImage,
                PreviewFiles = asset.PreviewFiles && _settingsService.GetPreviewFilesEnabled(UserContext.SelectedRegion),
                Attachments = _assetRepository.GetAssetAttachments(asset.ContentItemId),
                BrowseAllMaterialTags = browseAllMaterialTags,
                RegionsApplicableToClone = new ContentItemCloneToRegionViewModel
                {
                    RegionNames = ConvertToCommaSeparated(_contentItemService.GetApplicableCloneToRegionsNames(asset.ContentItemId))
                },
                RelatedContentItemsList = new RelatedContentItemsList
                {
                    RelatedContentItems = _slamContextService.GetRelatedItems(_contentItemService.GetContentItemRelatedItems(asset.ContentItemId))
                }
            };

            model.SetupSectionsCrossSellBoxes();
            model.Issuer = GetIssuerForCurrentUser();

            if (_settingsService.GetShowRelatedProgramEnabled(UserContext.SelectedRegion))
            {
                var relatedProgram = _assetRepository.GetAssetRelatedProgram(asset.ContentItemId);
                if (relatedProgram != null)
                {
                    model.RelatedProgram = GetRelatedProgram(relatedProgram.ProgramContentItemId);
                }
            }

            return View("Asset", model);
        }

        private ActionResult ProcessAssetFullWidth(AssetFullWidth assetFullWidth, ContentItem contentItem)
        {
            TempData["Clonable"] = assetFullWidth.AllowCloning && (contentItem.StatusID.Equals(Status.Published) || contentItem.StatusID.Equals(Status.PublishedAndDraft));
            if (!string.IsNullOrWhiteSpace(assetFullWidth.RestrictToSpecialAudience))
            {
                if (!string.IsNullOrWhiteSpace(CurrentSpecialAudience))
                {
                    string[] stringSeparators = new string[] { ";#" };
                    var assetFullWidthSpecialAudiences = assetFullWidth.RestrictToSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    var currentSpecialAudiences = CurrentSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                    bool match = false;
                    foreach (var specAud in currentSpecialAudiences)
                    {
                        if (assetFullWidthSpecialAudiences.Contains(specAud))
                        {
                            match = true;
                            break;
                        }
                    }

                    if (!match)
                    {
                        throw new AuthorizationException();
                    }
                }
                else
                {
                    throw new AuthorizationException();
                }
            }

            var model = new AssetFullWidthViewModel
            {
                Title = assetFullWidth.Title,
                EnableFavoriting = assetFullWidth.EnableFavoriting,
                BottomAreaDescription = assetFullWidth.BottomAreaDescription,
                ContentItemId = assetFullWidth.ContentItemId,
                PrimaryContentItemId = contentItem.PrimaryContentItemId,
                FrontEndUrl = contentItem.FrontEndUrl,
                AssetType = assetFullWidth.AssetType,
                CrossSellBox = assetFullWidth.ShowInCrossSellBox,
                LongDescription = assetFullWidth.LongDescription,
                ShortDescription = assetFullWidth.ShortDescription,
                BrowseAllMaterialTags = _assetRepository.GetAssetFullWidthBrowseAllMaterialTags(assetFullWidth.ContentItemId),
                RelatedContentItemsList = new RelatedContentItemsList
                {
                    RelatedContentItems = _slamContextService.GetRelatedItems(_contentItemService.GetContentItemRelatedItems(assetFullWidth.ContentItemId))
                }
            };

            model.RegionsApplicableToClone = model.RegionsApplicableToClone = new ContentItemCloneToRegionViewModel
            {
                RegionNames = ConvertToCommaSeparated(_contentItemService.GetApplicableCloneToRegionsNames(assetFullWidth.ContentItemId))
            };

            model.SetupSectionsCrossSellBoxes();
            model.Issuer = GetIssuerForCurrentUser();

            if (_settingsService.GetShowRelatedProgramEnabled(UserContext.SelectedRegion))
            {
                var relatedProgram = _assetRepository.GetAssetFullWidthRelatedProgram(assetFullWidth.ContentItemId);
                if (relatedProgram != null)
                {
                    model.RelatedProgram = GetRelatedProgram(relatedProgram.ProgramContentItemId);
                }
            }

            return View("AssetFullWidth", model);
        }

        private ActionResult ProcessDownloadableAsset(DownloadableAsset downloadableAsset, string trackingType, ContentItem contentItem)
        {
            TempData["Clonable"] = downloadableAsset.AllowCloning && (contentItem.StatusID.Equals(Status.Published) || contentItem.StatusID.Equals(Status.PublishedAndDraft));
            if (!string.IsNullOrWhiteSpace(downloadableAsset.RestrictToSpecialAudience))
            {
                if (!string.IsNullOrWhiteSpace(CurrentSpecialAudience))
                {
                    string[] stringSeparators = new string[] { ";#" };
                    var downloadableAssetSpecialAudiences = downloadableAsset.RestrictToSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    var currentSpecialAudiences = CurrentSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                    bool match = false;
                    foreach (var specAud in currentSpecialAudiences)
                    {
                        if (downloadableAssetSpecialAudiences.Contains(specAud))
                        {
                            match = true;
                            break;
                        }
                    }

                    if (!match)
                    {
                        throw new AuthorizationException();
                    }
                }
                else
                {
                    throw new AuthorizationException();
                }
            }

            var browseAllMaterialTags = _assetRepository.GetDownloadableAssetBrowseAllMaterialTags(downloadableAsset.ContentItemId).ToList();
            AddExpandedMarketTags(browseAllMaterialTags);
            var tags = _contentItemService.GetTags(contentItem.ContentItemId);

            var model = new DownloadableAssetViewModel
            {
                Title = downloadableAsset.Title,
                EnableFavoriting = downloadableAsset.EnableFavoriting,
                ShortDescription = downloadableAsset.ShortDescription,
                LongDescription = downloadableAsset.LongDescription,
                MainImage = downloadableAsset.MainImage,
                CallToAction = downloadableAsset.CallToAction,
                Tags = tags,
                ContentItemId = downloadableAsset.ContentItemId,
                PrimaryContentItemId = contentItem.PrimaryContentItemId,
                FrontEndUrl = contentItem.FrontEndUrl,
                AssetType = downloadableAsset.AssetType,
                CrossSellBox = downloadableAsset.ShowInCrossSellBox,
                PreviewFiles = downloadableAsset.PreviewFiles && _settingsService.GetPreviewFilesEnabled(UserContext.SelectedRegion),
                Attachments = _assetRepository.GetDownloadableAssetAttachments(downloadableAsset.ContentItemId),
                BrowseAllMaterialTags = browseAllMaterialTags,
                RegionsApplicableToClone = new ContentItemCloneToRegionViewModel
                {
                    RegionNames = ConvertToCommaSeparated(_contentItemService.GetApplicableCloneToRegionsNames(downloadableAsset.ContentItemId))
                },
                RelatedContentItemsList = new RelatedContentItemsList
                {
                    RelatedContentItems = _slamContextService.GetRelatedItems(_contentItemService.GetContentItemRelatedItems(downloadableAsset.ContentItemId))
                },
                DownloadFile = new DownloadFile
                {
                    ContentItemId = downloadableAsset.ContentItemId,
                    PrimaryContentItemId = contentItem.PrimaryContentItemId,
                    Title = downloadableAsset.Title,
                    Filename = downloadableAsset.Filename,
                    IsInUserMarket = tags.Any(t => t.Identifier.Equals(UserContext.User.Country.Trim(), StringComparison.InvariantCultureIgnoreCase) || t.Identifier.Equals(UserContext.User.Region.Trim(), StringComparison.InvariantCultureIgnoreCase)),
                    RequestEstimatedDistribution = downloadableAsset.RequestEstimatedDistribution,
                    SetForElectronicDelivery = downloadableAsset.SetForElectronicDelivery,
                    TrackingType = string.IsNullOrEmpty(trackingType) ? null : $"{trackingType}-downloadasset-{downloadableAsset.ContentItemId}",
                }
            };

            if (_settingsService.GetShowRelatedProgramEnabled(UserContext.SelectedRegion))
            {
                var relatedProgram = _assetRepository.GetDownloadableAssetRelatedProgram(downloadableAsset.ContentItemId);
                if (relatedProgram != null)
                {
                    model.RelatedProgram = GetRelatedProgram(relatedProgram.ProgramContentItemId);
                }
            }

            model.SetupSectionsCrossSellBoxes();
            model.Issuer = GetIssuerForCurrentUser();

            var fullDownloadPath = System.IO.Path.GetFullPath(System.IO.Path.Combine(Server.MapPath(_settingsService.GetFilesFolder()), downloadableAsset.Filename.TrimStart('/').TrimStart('\\')));
            model.DownloadFile.DownloadableFileExists = System.IO.File.Exists(fullDownloadPath);
            VerifyUnicodeFormDFullPath(model.DownloadFile, fullDownloadPath);

            return View("DownloadableAsset", model);
        }

        private void VerifyUnicodeFormDFullPath(DownloadFile file, string fullDownloadPath)
        {
            if (!file.DownloadableFileExists)
            {
                var normalizedStringFormD = fullDownloadPath.Normalize(System.Text.NormalizationForm.FormD);
                file.DownloadableFileExists = System.IO.File.Exists(normalizedStringFormD);
                if (file.DownloadableFileExists)
                {
                    System.IO.File.Move(normalizedStringFormD, fullDownloadPath);
                }
            }
        }

        private OrderableAssetViewModel ProcessOrderableAsset(OrderableAsset orderableAsset, ContentItem contentItem)
        {
            var model = new OrderableAssetViewModel
            {
                ContentItemId = orderableAsset.ContentItemId,
                PrimaryContentItemId = contentItem.PrimaryContentItemId,
                FrontEndUrl = contentItem.FrontEndUrl,
                AssetType = orderableAsset.AssetType,
                CrossSellBox = orderableAsset.ShowInCrossSellBox,
                Title = orderableAsset.Title,
                EnableFavoriting = orderableAsset.EnableFavoriting,
                Sku = orderableAsset.Sku,
                LongDescription = orderableAsset.LongDescription,
                ShortDescription = orderableAsset.ShortDescription,
                Vdp = orderableAsset.Vdp,
                CallToAction = orderableAsset.CallToAction,
                MainImage = orderableAsset.MainImage,
                PDFPreview = orderableAsset.PDFPreview,
                Archived = contentItem.StatusID == Status.Archived,
                ElectronicDeliveryFile = orderableAsset.ElectronicDeliveryFile,
                Attachments = _assetRepository.GetOrderableAssetAttachments(orderableAsset.ContentItemId),
                BrowseAllMaterialTags = _assetRepository.GetOrderableAssetBrowseAllMaterialTags(orderableAsset.ContentItemId),
                PricingSchedule = orderableAsset.PricingSchedule,
                RelatedContentItemsList = new RelatedContentItemsList
                {
                    RelatedContentItems = _slamContextService.GetRelatedItems(_contentItemService.GetContentItemRelatedItems(orderableAsset.ContentItemId))
                }
            };

            if (_settingsService.GetShowRelatedProgramEnabled(UserContext.SelectedRegion))
            {
                var relatedProgram = _assetRepository.GetOrderableAssetRelatedProgram(orderableAsset.ContentItemId);
                if (relatedProgram != null)
                {
                    model.RelatedProgram = GetRelatedProgram(relatedProgram.ProgramContentItemId);
                }
            }

            model.SetupSectionsCrossSellBoxes();
            model.Issuer = GetIssuerForCurrentUser();

            return model;
        }

        private ActionResult ProcessProgram(Program program, ContentItem contentItem)
        {
            if (!string.IsNullOrWhiteSpace(program.RestrictToSpecialAudience))
            {
                if (!string.IsNullOrWhiteSpace(CurrentSpecialAudience))
                {
                    string[] stringSeparators = new string[] { ";#" };
                    var programSpecialAudiences = program.RestrictToSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    var currentSpecialAudiences = CurrentSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                    bool match = false;
                    foreach (var specAud in currentSpecialAudiences)
                    {
                        if (programSpecialAudiences.Contains(specAud))
                        {
                            match = true;
                            break;
                        }
                    }

                    if (!match)
                    {
                        throw new AuthorizationException();
                    }
                }
                else
                {
                    throw new AuthorizationException();
                }
            }

            var browseAllMaterialTags = _assetRepository.GetProgramBrowseAllMaterialTags(program.ContentItemId).ToList();
            AddExpandedMarketTags(browseAllMaterialTags);

            var model = new ProgramViewModel
            {
                Title = program.Title,
                EnableFavoriting = program.EnableFavoriting,
                ShortDescription = program.ShortDescription,
                LongDescription = program.LongDescription,
                MainImage = program.MainImage,
                ShowOrderPeriods = program.ShowOrderPeriods,
                PrintedOrderPeriodStartDate = program.PrintedOrderPeriodStartDate,
                PrintedOrderPeriodAlternateDisplayText = program.PrintedOrderPeriodAlternateDisplayText,
                PrintedOrderPeriodAlternateDateText = program.PrintedOrderPeriodAlternateDateText,
                PrintedOrderPeriodEndDate = program.PrintedOrderPeriodEndDate,
                OnlineOrderPeriodStartDate = program.OnlineOrderPeriodStartDate,
                OnlineOrderPeriodAlternateDisplayText = program.OnlineOrderPeriodAlternateDisplayText,
                OnlineOrderPeriodAlternateDateText = program.OnlineOrderPeriodAlternateDateText,
                OnlineOrderPeriodEndDate = program.OnlineOrderPeriodEndDate,
                InMarketStartDate = program.InMarketStartDate,
                InMarketEndDate = program.InMarketEndDate,
                InMarketAlternateDisplayText = program.InMarketAlternateDisplayText,
                InMarketAlternateDateText = program.InMarketAlternateDateText,
                CallToAction = program.CallToAction,
                ContentItemId = program.ContentItemId,
                PrimaryContentItemId = contentItem.PrimaryContentItemId,
                FrontEndUrl = contentItem.FrontEndUrl,
                AssetType = program.AssetType,
                CrossSellBox = program.ShowInCrossSellBox,
                PreviewFiles = program.PreviewFiles && _settingsService.GetPreviewFilesEnabled(UserContext.SelectedRegion),
                Attachments = _assetRepository.GetProgramAttachments(program.ContentItemId),
                BrowseAllMaterialTags = browseAllMaterialTags,
                RelatedContentItemsList = new RelatedContentItemsList
                {
                    RelatedContentItems = _slamContextService.GetRelatedItems(_contentItemService.GetContentItemRelatedItems(program.ContentItemId))
                }
            };

            var relatedContentItemIds = _assetRepository.GetProgramRelatedAssetContentItemIds(program.ContentItemId).ToArray();
            if (relatedContentItemIds.Length > 0)
            {
                model.RelatedAssets = _slamContext.CreateQuery()
                                                  .FilterTagBrowserTypes()
                                                  .FilterSegmentations(CurrentSegmentationIds)
                                                  .FilterTagBrowserTypesForSpecialAudience()
                                                  .FilterContentItemId(relatedContentItemIds)
                                                  .FilterExpiredAssets()
                                                  .Get()
                                                  .Select(a => a.AsContentItemListItem())
                                                  .OrderBy(a => a.Title);
            }

            model.SetupSectionsCrossSellBoxes();

            return View("Program", model);
        }

        private ActionResult ProcessWebinar(Webinar webinar, ContentItem contentItem)
        {
            if (!string.IsNullOrWhiteSpace(webinar.RestrictToSpecialAudience))
            {
                if (!string.IsNullOrWhiteSpace(CurrentSpecialAudience))
                {
                    string[] stringSeparators = new string[] { ";#" };
                    var webinarSpecialAudiences = webinar.RestrictToSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    var currentSpecialAudiences = CurrentSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                    bool match = false;
                    foreach (var specAud in currentSpecialAudiences)
                    {
                        if (webinarSpecialAudiences.Contains(specAud))
                        {
                            match = true;
                            break;
                        }
                    }

                    if (!match)
                    {
                        throw new AuthorizationException();
                    }
                }
                else
                {
                    throw new AuthorizationException();
                }
            }

            var model = new WebinarViewModel
            {
                Title = webinar.Title,
                EnableFavoriting = webinar.EnableFavoriting,
                Date = webinar.Date,
                ShortDescription = webinar.ShortDescription,
                LongDescription = webinar.LongDescription,
                VideoFile = webinar.VideoFile,
                StaticVideoImage = webinar.StaticVideoImage,
                ShowImageInsteadVideo = webinar.ShowImageInsteadVideo,
                Link = webinar.Link,
                LinkText = webinar.LinkText,
                ContentItemId = webinar.ContentItemId,
                PrimaryContentItemId = contentItem.PrimaryContentItemId,
                FrontEndUrl = contentItem.FrontEndUrl,
                AssetType = webinar.AssetType,
                CrossSellBox = webinar.ShowInCrossSellBox,
                RelatedContentItemsList = new RelatedContentItemsList
                {
                    RelatedContentItems = _slamContextService.GetRelatedItems(_contentItemService.GetContentItemRelatedItems(webinar.ContentItemId))
                }
            };

            model.SetupSectionsCrossSellBoxes();

            return View("Webinar", model);
        }

        [ChildActionOnly]
        public ActionResult AttachmentsAndCallToAction(IEnumerable<Attachment> attachments, string callToAction, string mainImage, bool previewFiles)
        {
            var model = new AttachmentsAndCallToActionViewModel
            {
                Attachments = attachments,
                CallToAction = callToAction,
                MainImage = mainImage,
                PreviewFiles = previewFiles
            };

            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult BrowseAllMaterialTag(IEnumerable<Data.Entities.Tag> tags)
        {
            var model = new BrowseAllMaterialViewModel();
            if (tags.Count() > 0)
            {
                List<string> filterTags = tags.Select(t => t.Identifier).ToList();
                List<string> regionFilterTags = new List<string>();
                if (filterTags.Contains(UserContext.SelectedRegion))
                {
                    regionFilterTags = filterTags.Where(t => t == UserContext.SelectedRegion).ToList();
                    filterTags = filterTags.Where(t => t != UserContext.SelectedRegion).ToList();
                }

                model.Count = _slamContext.CreateQuery()
                    .FilterTagBrowserTypes()
                    .FilterTagBrowserTypesForSpecialAudience()
                    .FilterTaggedWith(filterTags)
                    .FilterPseudoTaggedByParentTags(regionFilterTags)
                    .FilterExpiredAssets()
                    .Get()
                    .Count();

                model.Render = (model.Count > 0);
                regionFilterTags.AddRange(filterTags);
                model.Url = Url.TagBrowser("tagbrowser").AddTags(regionFilterTags).Render();
            }

            return PartialView(model);
        }

        [ChildActionOnly]
        public ActionResult PricingTable(PricingSchedule pricingSchedule)
        {
            var model = new PricingTableViewModel
            {
                Title = pricingSchedule.Title,
                TableHeader = pricingSchedule.TableHeader,
                PricingTable = pricingSchedule.GetProcessedPricingTable(),
                ContactPrinter = pricingSchedule.ContactPrinterForMore.HasValue && pricingSchedule.ContactPrinterForMore.Value,
                MinimumPrice = pricingSchedule.MinimumPrice,
                PricingNotes = pricingSchedule.Details
            };

            return PartialView(model);
        }

        private bool ProcessElectronicDelivery(OrderableAssetViewModel model)
        {
            var cartId = GetShoppingCartId();
            if (!ValidateOrderableAsset(model.ContentItemId))
            {
                model.ElectronicDeliveryError = OrderableAssetValidationMessage;
                return false;
            }

            ShoppingCartService.SaveElectronicDeliveryCartItem(cartId, model.ContentItemId);
            return true;
        }

        private bool ProcessPrintOnDemand(OrderableAssetViewModel model, int? quantity)
        {
            var error = ValidatekQuantity(quantity);
            if (!string.IsNullOrEmpty(error))
            {
                model.PrintOnDemandError = error;
                return false;
            }

            if (!ValidateOrderableAsset(model.ContentItemId))
            {
                model.PrintOnDemandError = OrderableAssetValidationMessage;
                return false;
            }

            var pricingTable = model.PricingSchedule.GetProcessedPricingTable();
            if (pricingTable != null && model.PricingSchedule.ContactPrinterForMore.HasValue && model.PricingSchedule.ContactPrinterForMore.Value)
            {
                if (quantity >= pricingTable.Items.Last().Quantity)
                {
                    model.PrintOnDemandError = Resources.Errors.QuantityLower.F(pricingTable.Items.Last().Quantity);
                    return false;
                }
            }

            AddToCart(model, quantity.Value);
            return true;
        }

        private bool ProcessBatchPrint(OrderableAssetViewModel model, int? quantity)
        {
            var error = ValidatekQuantity(quantity);
            if (!string.IsNullOrEmpty(error))
            {
                model.BatchPrintError = error;
                return false;
            }

            if (!ValidateOrderableAsset(model.ContentItemId))
            {
                model.BatchPrintError = OrderableAssetValidationMessage;
                return false;
            }

            AddToCart(model, quantity.Value);
            return true;
        }

        private bool ValidateOrderableAsset(string globalId)
        {
            var orderableAsset = _contentItemService.GetOrderableAsset(globalId);
            if (orderableAsset == null)
            {
                return true;
            }

            return !(!string.IsNullOrEmpty(orderableAsset.ApproverEmailAddress) && ShoppingCartService.ShoppingCartContainsAssetsWithApproval(GetShoppingCartId()));
        }

        private void AddToCart(OrderableAssetViewModel model, int quantity)
        {
            var cartId = GetShoppingCartId();
            bool forceNew = false;
            int defaultShippingInfoId = ShippingServices.GetDefaultShippingInformationIdForCart(cartId);

            if (!model.Vdp.HasValue || !model.Vdp.Value)
            {
                foreach (var shippingInfo in ShippingServices.GetShippingInformationForCart(cartId))
                {
                    if (shippingInfo.ShippingInformationID != defaultShippingInfoId)
                    {
                        ShoppingCartService.SaveShoppingCartItem(cartId, model.ContentItemId, shippingInfo.ShippingInformationID, 0, true);
                        forceNew = true;
                    }
                }

                ShoppingCartService.SaveShoppingCartItem(cartId, model.ContentItemId, defaultShippingInfoId, quantity, forceNew);
            }
            else
            {
                ShoppingCartService.SaveShoppingCartItem(cartId, model.ContentItemId, 0, quantity, false);
            }
        }

        private string ValidatekQuantity(int? quantity)
        {
            if (!quantity.HasValue)
            {
                return Resources.Errors.QuantityEmpty;
            }

            return quantity <= 0 ? Resources.Errors.QuantityGreater : string.Empty;
        }

        private int GetShoppingCartId()
        {
            int? cartID = null;
            cartID = ShoppingCartService.GetActiveShoppingCart(CurrentUser.UserName) ??
                     ShoppingCartService.CreateShoppingCart(CurrentUser.UserName);

            return cartID.Value;
        }

        private Issuer GetIssuerForCurrentUser()
        {
            return _issuerService.GetIssuerById(CurrentUser.IssuerId);
        }

        private void AddExpandedMarketTags(IList<Data.Entities.Tag> tags)
        {
            var browseAllMaterialTagNodes = _slamContext.GetTagTree().FindNodes(n => n.Tag != null && tags.Any(t => t.TagID == n.Tag.TagId));
            var marketTagNodes = browseAllMaterialTagNodes.Where(t => t.IsMarketTag());
            foreach (var marketTagNode in marketTagNodes)
            {
                if (marketTagNode.Parent != null && marketTagNode.Parent.Tag != null && !tags.Any(t => t.Identifier == marketTagNode.Parent.Tag.Identifier))
                {
                    tags.Add(new Data.Entities.Tag
                    {
                        TagID = marketTagNode.Parent.Tag.TagId,
                        DisplayName = marketTagNode.Parent.Text,
                        Identifier = marketTagNode.Parent.Tag.Identifier,
                        HideFromSearch = true,
                        RestrictToSpecialAudience = string.Empty
                    });
                }
            }
        }

        public ActionResult SetEstimatedDistribution(string contentItemId, string estimatedDistribution)
        {
            int.TryParse(estimatedDistribution, out int estimatedDistributionCount);
            var user = UserContext.User;
            var downloadableAssetEstimatedDistribution = _assetRepository.SaveDownloadableAssetEstimatedDistribution(contentItemId, estimatedDistributionCount, user.UserId, null);

            return Json(new
            {
                status = "OK",
                downloadableAssetEstimatedDistributionId = downloadableAssetEstimatedDistribution.Id
            });
        }

        public ActionResult GetElectronicDelivery(string filename, string contentItemId = null, int downloadableAssetEstimatedDistributionId = 0)
        {
            var electronicDelivery = _assetRepository.CreateElectronicDelivery(Server.UrlDecode(filename), contentItemId, CurrentUser.UserName, UserContext.SelectedRegion);
            if (downloadableAssetEstimatedDistributionId > 0)
            {
                var downloadableAssetEstimatedDistribution = _assetRepository.GetDownloadableAssetEstimatedDistributionById(downloadableAssetEstimatedDistributionId);
                downloadableAssetEstimatedDistribution.ElectronicDeliveryId = electronicDelivery.ElectronicDeliveryID;
                _assetRepository.UpdateDownloadableAssetEstimatedDistribution(downloadableAssetEstimatedDistribution);
                _assetRepository.Commit();
            }

            return Json(
                new
                {
                    status = "OK",
                    downloadCount = electronicDelivery.DownloadCount,
                    electronicDeliveryId = electronicDelivery.ElectronicDeliveryID,
                    expirationDate = electronicDelivery.ExpirationDate.ToString("d"),
                    link = _urlService.GetElectronicDeliveryDownloadUrl(electronicDelivery.ElectronicDeliveryID.ToString(), UserContext.SelectedRegion)
                });
        }

        public ActionResult SendElectronicDeliveryEmail(string electronicDeliveryId, string itemName)
        {
            var electronicDelivery = _assetRepository.GetElectronicDelivery(electronicDeliveryId);
            _emailService.SendElectronicDeliveryDownloadableAssetEmail(
                          itemName,
                          _urlService.GetElectronicDeliveryDownloadUrl(electronicDelivery.ElectronicDeliveryID.ToString(), UserContext.SelectedRegion),
                          electronicDelivery.ExpirationDate,
                          electronicDelivery.DownloadCount,
                          UserContext.User.Email,
                          UserContext.User.FullName,
                          UserContext.User.UserId,
                          UserContext.SelectedRegion,
                          UserContext.SelectedLanguage);

            return new EmptyResult();
        }

        private string ConvertToCommaSeparated(IEnumerable<string> collection, string separator = ", ")
        {
            return string.Join(separator, collection);
        }

        private ProgramDTO GetRelatedProgram(string programContentItemId)
        {
            return _slamContext.CreateQuery()
                               .FilterSegmentations(CurrentSegmentationIds)
                               .FilterTagBrowserTypesForSpecialAudience()
                               .FilterContentItemId(programContentItemId)
                               .Get<ProgramDTO>()
                               .FirstOrDefault();
        }
    }
}