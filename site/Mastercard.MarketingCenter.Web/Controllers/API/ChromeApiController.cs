﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Web.Core.Controls;
using Mastercard.MarketingCenter.Web.Core.Models.Api;
using Mastercard.MarketingCenter.Web.Core.Services;
using Newtonsoft.Json;
using Pulsus;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.UI;

namespace Mastercard.MarketingCenter.Web.Controllers.Api
{
    public class ChromeController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage Get(string region, string language)
        {
            if (Request.Headers.Any(h => h.Value.Any(v => v == Constants.ApiVerificationHash)))
            {
                var content = new ChromeApiResponse
                {
                    Head = RenderUserControl(Configuration.VirtualPathRoot.TrimEnd('/') + "/UserControls/ChromeHead.ascx", region, language),
                    Header = HeaderService.Header(HttpContext.Current, false, false, region, language),
                    Footer = RenderUserControl(Configuration.VirtualPathRoot.TrimEnd('/') + "/UserControls/Footer.ascx", region, language)
                };

                try
                {
                    return new HttpResponseMessage
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(content), System.Text.Encoding.UTF8, "application/json")
                    };
                }
                catch (Exception ex)
                {
                    LogManager.EventFactory.Create()
                      .Text("Error Serializing Content for Chrome API Response...")
                      .AddTags("Chrome API Controller")
                      .Level(LoggingEventLevel.Alert)
                      .AddData("Content", content)
                      .AddData("Region", region)
                      .AddData("Language", language)
                      .AddData("Exception Message", ex.Message)
                      .AddData("Exception InnerException", ex.InnerException)
                      .AddData("Exception Data", ex.Data)
                      .Push();

                    return new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.InternalServerError,
                        Content = new StringContent("Internal Server Error.")
                    };
                }
            }

            return new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.Forbidden,
                Content = new StringContent("Request unauthorized.")
            };
        }

        private string RenderUserControl(string userControl, string region, string language)
        {
            try
            {
                string output = string.Empty;
                using (var page = new Page())
                {
                    var control = page.LoadControl(userControl);
                    if (control != null && control is RegionalUserControl)
                    {
                        ((RegionalUserControl)control).UserContext = new UserContext(region, language);
                        ((RegionalUserControl)control).Sitemap = new Core.MastercardSitemap(((RegionalUserControl)control).UserContext);
                    }

                    if (control != null)
                    {
                        page.Controls.Add(control);
                    }

                    using (var writer = new StringWriter())
                    {
                        HttpContext.Current.Server.Execute(page, writer, false);
                        output = writer.ToString();
                    }
                }

                return ExpandRelativeUrls(output);
            }
            catch (Exception ex)
            {
                LogManager.EventFactory.Create()
                  .Text("Error Rendering User Control for Chrome API Response...")
                  .AddTags("Chrome API Controller")
                  .Level(LoggingEventLevel.Alert)
                  .AddData("User Control", userControl)
                  .AddData("Region", region)
                  .AddData("Language", language)
                  .AddData("Exception Message", ex.Message)
                  .AddData("Exception InnerException", ex.InnerException)
                  .AddData("Exception Data", ex.Data)
                  .Push();

                return string.Empty;
            }
        }

        private string ExpandRelativeUrls(string content)
        {
            if (content == null)
            {
                return null;
            }

            var baseUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority).TrimEnd('/');

            return content.Replace("\"/portal", string.Format("\"{0}/portal", baseUrl)).Replace("'/portal", string.Format("'{0}/portal", baseUrl));
        }
    }
}