﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Filters;
using Slam.Cms.Data;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class FileController : AsyncController
    {
        private readonly SlamContext _slamContext;
        private readonly ISettingsService _settingsService;
        private readonly IImageService _imageService;

        public FileController(SlamContext slamContext, ISettingsService settingsService, IImageService imageService)
        {
            _slamContext = slamContext;
            _settingsService = settingsService;
            _imageService = imageService;
        }

        [PageTracking]
        public ActionResult HtmlDownload(string id, string title)
        {
            var htmlDownload = _slamContext.CreateQuery().FilterContentItemId(id).Get<HtmlDownloadDTO>().FirstOrDefault();
            if (htmlDownload == null)
            {
                Pulsus.LogManager.EventFactory.Create()
                                              .Text($"The HtmlDownload with ID: {id} and Title: {title} does not exist")
                                              .Push();

                return HttpNotFound();
            }

            var filename = htmlDownload.Filename;
            var filesPath = _settingsService.GetFilesFolder();
            var fullFilePath = Path.Combine(Server.MapPath(filesPath), filename.TrimStart('/').TrimStart('\\'));
            if (!System.IO.File.Exists(fullFilePath))
            {
                Pulsus.LogManager.EventFactory.Create()
                                              .Text($"The HtmlDownload File: {fullFilePath} is not valid!")
                                              .Push();

                return HttpNotFound();
            }

            Response.AppendHeader("Content-Disposition", $"inline; filename={Path.GetFileName(filename)}");
            return File(fullFilePath, filename.GetMimeType());
        }

        public ActionResult ProofFile(string file)
        {
            if (string.IsNullOrEmpty(file))
            {
                return HttpNotFound();
            }

            var fullPath = Server.MapPath(file);
            if (System.IO.File.Exists(fullPath) && fullPath.StartsWith(Server.MapPath(_settingsService.GetCustomizationFilesUploadPath())))
            {
                return File(fullPath, "application/octet-stream", Path.GetFileName(fullPath));
            }

            return HttpNotFound($"{Resources.Errors.FileNotFound}: {fullPath}");
        }

        [PageTracking]
        public ActionResult Download(string url)
        {
            return InternalDownload(url);
        }

        [PageTracking]
        public ActionResult DownloadImage(string url, string title)
        {
            if (string.IsNullOrEmpty(url) || !url.IsValidImage())
            {
                return HttpNotFound();
            }

            if (url.IsAbsoluteUrl())
            {
                return File(_imageService.GetImageFromUrl(url), url.GetMimeType(), title.IsNullOrWhiteSpace() ? Path.GetFileName(url) : $"{title}{Path.GetExtension(url)}");
            }

            return File(_imageService.GetMappedImage(url, true), url.GetMimeType(), title.IsNullOrWhiteSpace() ? Path.GetFileName(url) : $"{title}{Path.GetExtension(url)}");
        }

        [PageTracking]
        public ActionResult LibraryDownload(string url)
        {
            return InternalDownload(url);
        }

        private ActionResult InternalDownload(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return HttpNotFound();
            }

            if (url.IsAbsoluteUrl() && url.IsValidImage())
            {
                string imagePath = _imageService.GetImageFromUrl(url);
                return File(imagePath, imagePath.GetMimeType(), Path.GetFileName(imagePath));
            }

            var filesPath = Server.MapPath(_settingsService.GetFilesFolder());
            var fullPath = Path.GetFullPath(Path.Combine(filesPath, url.TrimStart('/').TrimStart('\\')));
            if (System.IO.File.Exists(fullPath) && fullPath.StartsWith(filesPath))
            {
                return File(fullPath, "application/octet-stream", Path.GetFileName(url));
            }

            return HttpNotFound($"{Resources.Errors.FileNotFound}: {fullPath}");
        }
    }
}