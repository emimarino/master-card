﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Filters;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Common.Models;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Services;
using Slam.Cms.Data;
using System;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    [SessionState(System.Web.SessionState.SessionStateBehavior.ReadOnly)]
    public class CacheController : BaseController
    {
        private readonly SlamContext _slamContext;
        private readonly SlamContextService _slamContextService;
        private readonly MastercardAuthenticationService _authenticationService;
        private readonly IJsonWebTokenService _jsonWebTokenService;
        private readonly IOfferService _offerService;
        private readonly IEventLocationService _eventLocationService;

        public CacheController(SlamContext slamContext, SlamContextService slamContextService, MastercardAuthenticationService authenticationService, IJsonWebTokenService jsonWebTokenService, IOfferService offerService, IEventLocationService eventLocationService)
        {
            _slamContext = slamContext;
            _slamContextService = slamContextService;
            _authenticationService = authenticationService;
            _jsonWebTokenService = jsonWebTokenService;
            _offerService = offerService;
            _eventLocationService = eventLocationService;
        }

        [HttpGet]
        [NoCache]
        public ActionResult Invalidate(string id)
        {
            switch (id.ToLowerInvariant())
            {
                case "tags":
                    _slamContext.InvalidateTags();
                    break;
                case "segmentations":
                    _slamContext.InvalidateSegmentations();
                    break;
                case "users":
                    _slamContext.InvalidateUsers();
                    break;
                case "status":
                    _slamContext.InvalidateStatus();
                    break;
                case "queries":
                    _slamContext.InvalidateQueries();
                    break;
                case "content":
                    // assume it's a ContentItemId -> invalidate all for now
                    _slamContext.InvalidateQueries();
                    break;
            }

            return Content("OK", "text/plain");
        }

        [HttpGet]
        [NoCache]
        public ActionResult InvalidateTagTree()
        {
            _slamContextService.InvalidateAllTags();

            return Content("OK", "text/plain");
        }

        [HttpGet]
        [NoCache]
        public ActionResult InvalidateContentQueries()
        {
            _slamContext.InvalidateQueries();
            _slamContextService.ReloadCacheQueries();

            return Content("OK", "text/plain");
        }

        [HttpGet]
        [NoCache]
        public ActionResult InvalidateUserRole(string id)
        {
            if (User.IsInRole(Constants.Roles.McAdmin) || User.IsInRole(Constants.Roles.DevAdmin))
            {
                if (int.TryParse(id, out int userId))
                {
                    User.InvalidateUserRole(userId);

                    return Content("OK", "text/plain");
                }

                return Content("Error: Verify UserId", "text/plain");
            }

            return Content("Error: Request unauthorized", "text/plain");
        }

        [HttpGet]
        [NoCache]
        public ActionResult InvalidateUserRoleByToken()
        {
            if (Request.Headers.HasKeys())
            {
                string authorizationHeader = Request.Headers["Authorization"];
                if (_jsonWebTokenService.IsValidToken(authorizationHeader))
                {
                    var jwtModel = _jsonWebTokenService.GetFromToken<CacheInvalidateUserRoleJsonWebToken>(authorizationHeader);
                    if (int.TryParse(jwtModel.ExpirationDate, out int expirationDate) && expirationDate > DateTime.UtcNow.ToUnixTimestamp() &&
                        int.TryParse(jwtModel.UserId, out int userId))
                    {
                        User.InvalidateUserRole(userId);
                        _authenticationService.RemoveSlamAuthenticationCookie();
                        _authenticationService.RemoveSlamPreviewModeCookie();

                        return Content("OK", "text/plain");
                    }

                    return Content("Error: Verify ExpirationDate and UserId", "text/plain");
                }
            }

            return Content("Error: Request unauthorized", "text/plain");
        }

        [HttpGet]
        [NoCache]
        public ActionResult InvalidateRolePermissions(string id, string region)
        {
            if (User.IsInRole(Constants.Roles.McAdmin) || User.IsInRole(Constants.Roles.DevAdmin))
            {
                if (int.TryParse(id, out int roleId) && !string.IsNullOrWhiteSpace(region))
                {
                    User.InvalidateRolePermissions(roleId, region);
                    return Content("OK", "text/plain");
                }

                return Content("Error: Verify RoleId and Region", "text/plain");
            }

            return Content("Error: Request unauthorized", "text/plain");
        }

        [HttpGet]
        [NoCache]
        public ActionResult ReloadOfferModelByToken()
        {
            if (Request.Headers.HasKeys())
            {
                string authorizationHeader = Request.Headers["Authorization"];
                if (_jsonWebTokenService.IsValidToken(authorizationHeader))
                {
                    var jwtModel = _jsonWebTokenService.GetFromToken<CacheInvalidateOfferModelJsonWebToken>(authorizationHeader);
                    if (int.TryParse(jwtModel.ExpirationDate, out int expirationDate) && expirationDate > DateTime.UtcNow.ToUnixTimestamp())
                    {
                        _offerService.ReloadOfferModel();
                        _offerService.ReloadCategoryModel();
                        _offerService.ReloadCardExclusivityModel();
                        _offerService.ReloadMarketApplicabilityModel();
                        return Content("OK", "text/plain");
                    }

                    return Content("Error: Verify ExpirationDate", "text/plain");
                }
            }

            return Content("Error: Request unauthorized", "text/plain");
        }

        [HttpGet]
        [NoCache]
        public ActionResult ReloadCategoryModelByToken()
        {
            if (Request.Headers.HasKeys())
            {
                string authorizationHeader = Request.Headers["Authorization"];
                if (_jsonWebTokenService.IsValidToken(authorizationHeader))
                {
                    var jwtModel = _jsonWebTokenService.GetFromToken<CacheInvalidateCategoryModelJsonWebToken>(authorizationHeader);
                    if (int.TryParse(jwtModel.ExpirationDate, out int expirationDate) && expirationDate > DateTime.UtcNow.ToUnixTimestamp())
                    {
                        _offerService.ReloadCategoryModel();
                        return Content("OK", "text/plain");
                    }

                    return Content("Error: Verify ExpirationDate", "text/plain");
                }
            }

            return Content("Error: Request unauthorized", "text/plain");
        }

        [HttpGet]
        [NoCache]
        public ActionResult ReloadCardExclusivityModelByToken()
        {
            if (Request.Headers.HasKeys())
            {
                string authorizationHeader = Request.Headers["Authorization"];
                if (_jsonWebTokenService.IsValidToken(authorizationHeader))
                {
                    var jwtModel = _jsonWebTokenService.GetFromToken<CacheInvalidateCardExclusivityModelJsonWebToken>(authorizationHeader);
                    if (int.TryParse(jwtModel.ExpirationDate, out int expirationDate) && expirationDate > DateTime.UtcNow.ToUnixTimestamp())
                    {
                        _offerService.ReloadCardExclusivityModel();
                        return Content("OK", "text/plain");
                    }

                    return Content("Error: Verify ExpirationDate", "text/plain");
                }
            }

            return Content("Error: Request unauthorized", "text/plain");
        }

        [HttpGet]
        [NoCache]
        public ActionResult ReloadEventLocationModelByToken()
        {
            if (Request.Headers.HasKeys())
            {
                string authorizationHeader = Request.Headers["Authorization"];
                if (_jsonWebTokenService.IsValidToken(authorizationHeader))
                {
                    var jwtModel = _jsonWebTokenService.GetFromToken<CacheInvalidateEventLocationModelJsonWebToken>(authorizationHeader);
                    if (int.TryParse(jwtModel.ExpirationDate, out int expirationDate) && expirationDate > DateTime.UtcNow.ToUnixTimestamp())
                    {
                        _eventLocationService.ReloadEventLocationModel();
                        return Content("OK", "text/plain");
                    }

                    return Content("Error: Verify ExpirationDate", "text/plain");
                }
            }

            return Content("Error: Request unauthorized", "text/plain");
        }
    }
}