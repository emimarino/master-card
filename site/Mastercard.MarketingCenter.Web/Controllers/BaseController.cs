﻿using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Web.Core.Filters;
using StackExchange.Profiling;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Resources;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    [CacheInvalidatorFilter]
    [PageTracking]
    public abstract class BaseController : Controller
    {
        private readonly bool _profilerEnabled = bool.Parse(WebConfigurationManager.AppSettings["ProfilerEnabled"]);
        private IDisposable _childActionTracer;
        private IDisposable _actionExecutingToExecuted;
        private IDisposable _resultExecutingToExecuted;

        public UserContext UserContext { get; set; }
        public User CurrentUser { get { return UserContext.User; } }

        protected string SiteName
        {
            get
            {
                var resources = new ResourceManager("Resources.Shared", Assembly.Load("App_GlobalResources"));
                return resources.GetString(Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["ApplicationNameResource"]) ?? Resources.Shared.MosSiteName;
            }
        }

        public IMarketingCenterWebApplicationService MarketingCenterWebApplicationService { get; set; }

        public IEnumerable<string> CurrentSegmentationIds
        {
            get { return MarketingCenterWebApplicationService.GetCurrentSegmentationIds(); }
        }

        public string CurrentSpecialAudience
        {
            get { return MarketingCenterWebApplicationService.GetCurrentSpecialAudience(); }
        }

        protected ICookieService CookieService
        {
            get
            {
                return DependencyResolver.Current.GetService<ICookieService>();
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            _actionExecutingToExecuted = MiniProfiler.Current.Step(String.Format("OnActionExecuting: {0}/{1}", filterContext.ActionDescriptor.ControllerDescriptor.ControllerName, filterContext.ActionDescriptor.ActionName));

            using (MiniProfiler.Current.Step("Manage Cookies"))
            {
                ManageCookieHandOffs();
            }

            using (MiniProfiler.Current.Step("ChildAction Route handling"))
            {
                if (filterContext.IsChildAction)
                {
                    var routeName = GetRouteName(filterContext.ActionDescriptor);
                    _childActionTracer = MiniProfiler.Current.Step(routeName);
                }
            }

            using (MiniProfiler.Current.Step("Session Management"))
            {
                MarketingCenterWebApplicationService.GetCurrentSegmentationIds();
            }

            using (MiniProfiler.Current.Step("Base OnActionExecution"))
            {
                ViewBag.SelectedRegion = UserContext.SelectedRegion;
                base.OnActionExecuting(filterContext);
            }
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            using (MiniProfiler.Current.Step("OnActionExecuted"))
            {
                if (_actionExecutingToExecuted != null)
                {
                    _actionExecutingToExecuted.Dispose();
                }

                base.OnActionExecuted(filterContext);
            }
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            _resultExecutingToExecuted = MiniProfiler.Current.Step("OnResultExecuting");

            filterContext.Controller.ViewData["ProfilerEnabled"] = _profilerEnabled;

            base.OnResultExecuting(filterContext);
        }

        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            using (MiniProfiler.Current.Step("OnResultExecuted"))
            {
                if (_resultExecutingToExecuted != null)
                {
                    _resultExecutingToExecuted.Dispose();
                }

                if (_childActionTracer != null)
                {
                    _childActionTracer.Dispose();
                }

                base.OnResultExecuted(filterContext);
            }
        }

        protected virtual ActionResult RedirectToHome()
        {
            return RedirectToAction("Index", "Home");
        }

        private string GetRouteName(ActionDescriptor actionDescriptor)
        {
            return actionDescriptor.ControllerDescriptor.ControllerName + "/" + actionDescriptor.ActionName;
        }

        protected void ManageCookieHandOffs()
        {
            if (Session["SlamAuthenticationAndPreviewModeCookieFlag"] != null && (bool)Session["SlamAuthenticationAndPreviewModeCookieFlag"])
            {
                var authenticationService = DependencyResolver.Current.GetService<MastercardAuthenticationService>();
                authenticationService.CreateOrUpdateSlamAuthenticationCookie(UserContext.User.UserName);
                authenticationService.CreateOrUpdateSlamPreviewModeCookie();
                Session["SlamAuthenticationAndPreviewModeCookieFlag"] = null;
            }
        }
    }
}