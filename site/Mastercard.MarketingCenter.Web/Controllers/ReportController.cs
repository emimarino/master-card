﻿using EvoPdf;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Exceptions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core;
using Mastercard.MarketingCenter.Web.Core.Models;
using Mastercard.MarketingCenter.Web.Core.Services;
using Pulsus;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using CommonConstants = Mastercard.MarketingCenter.Common.Infrastructure.Constants;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class ReportController : BaseController
    {
        private readonly SlamContext _slamContext;
        private readonly SlamContextService _slamContextService;
        private readonly IssuerAssessmentService _issuerAssessmentService;
        private readonly IImportedIcaRepository _importedIcaRepository;
        private readonly IssuerUnmatchedRepository _issuerUnmatchedRepository;
        private readonly IUserService _userService;
        private readonly IIssuerService _issuerService;
        private readonly IEmailService _emailService;
        private readonly IUrlService _urlService;
        private readonly ISettingsService _settingsService;

        public ReportController(SlamContext slamContext, SlamContextService slamContextService, IssuerAssessmentService issuerAssessmentService, IImportedIcaRepository importedIcaRepository, IssuerUnmatchedRepository issuerUnmatchedRepository, IUserService userServices, IIssuerService issuerService, IEmailService emailService, IUrlService urlService, ISettingsService settingsService)
        {
            _slamContext = slamContext;
            _slamContextService = slamContextService;
            _issuerAssessmentService = issuerAssessmentService;
            _importedIcaRepository = importedIcaRepository;
            _issuerUnmatchedRepository = issuerUnmatchedRepository;
            _userService = userServices;
            _issuerService = issuerService;
            _emailService = emailService;
            _urlService = urlService;
            _settingsService = settingsService;
        }

        public ActionResult Index(bool showReportCover = true, string trackingType = null)
        {
            var issuer = GetIssuerForCurrentUser();
            var model = new ReportSummaryModel
            {
                TrackingType = string.IsNullOrEmpty(trackingType) ? "report" : $"{trackingType}-report",
                Issuer = issuer
            };

            if (string.IsNullOrEmpty(issuer.Cid))
            {
                model.Error = true;
                string message = $"Issuer {issuer.Title} doesn't have a CID associated";
                LogManager.EventFactory.Create()
                                       .Text(message)
                                       .Level(LoggingEventLevel.Warning)
                                       .Push();

                _issuerUnmatchedRepository.Add(new IssuerUnmatched
                {
                    Error = "NOCID",
                    Message = message,
                    IssuerID = issuer.IssuerId,
                    ImportedIcaId = null,
                    UserId = CurrentUser.UserId,
                    Date = DateTime.Now
                });

                _emailService.SendNotificationEmail(string.Empty, issuer.Title, issuer.DomainName, string.Empty, CurrentUser.FullName, CurrentUser.Email, CurrentUser.UserId);
            }
            else
            {
                ProcessReportSummaryModel(showReportCover, issuer, model, true);
            }

            return View(model);
        }

        public ActionResult Optimize(string trackingType = null)
        {
            return View(new OptimizeModel
            {
                Recommendations = _slamContextService.GetRecommendations(),
                TrackingType = string.IsNullOrEmpty(trackingType) ? "optimize" : $"{trackingType}-optimize"
            });
        }

        public ActionResult DashboardLanding(string trackingType = null)
        {
            return View(new DashboardLandingModel
            {
                TrackingType = string.IsNullOrEmpty(trackingType) ? "dashboard" : $"{trackingType}-dashboard"
            });
        }

        public ActionResult Goal(string contentItemId, bool returnToOptimize = false, string trackingType = null)
        {
            var recommendation = _slamContext.CreateQuery()
                                             .FilterContentTypes<RecommendationDTO>()
                                             .IncludeTags()
                                             .FilterContentItemId(contentItemId)
                                             .GetFirstOrDefault<RecommendationDTO>();
            if (recommendation == null)
            {
                throw new InvalidOperationException($"Recommendation with ID: {contentItemId} not found!");
            }

            var tag = recommendation.Tags.FirstOrDefault();
            if (tag == null)
            {
                throw new InvalidOperationException($"No tag defined: {contentItemId}");
            }

            string optimizationTagIdentifier = _settingsService.GetIssuerOptimizationOptimizationSolutionsTagIdentifier();
            var optimizationTags = _slamContext.GetTagTree()
                                                   .FindNodes(o => o.Parent != null && o.Parent.Identifier == optimizationTagIdentifier)
                                                   .ToList();
            if (!optimizationTags.Any())
            {
                throw new InvalidOperationException($"Couldn't find any tag for the: {optimizationTagIdentifier} tag category.");
            }

            var optimizationCategories = optimizationTags.Select(o => new OptimizationCategory
            {
                CategoryTitle = o.Text,
                Assets = _slamContext.CreateQuery()
                                     .FilterContentTypes<DownloadableAssetDTO>()
                                     .FilterTaggedWith(tag.Identifier, o.Identifier)
                                     .Get<DownloadableAssetDTO>()
            });

            var issuer = GetIssuerForCurrentUser();
            var model = new GoalModel
            {
                Title = recommendation.DisplayName,
                OptimizationCategories = optimizationCategories,
                ShortDescription = recommendation.ShortDescription,
                TrackingType = string.IsNullOrEmpty(trackingType) ? $"goal-{contentItemId}" : $"{trackingType}-goal-{contentItemId}",
                Issuer = issuer,
                ReportHeaderModel = GetReportHeaderModel(issuer.Cid),
                ReturnUrl = returnToOptimize ? Url.RouteUrl("Optimize") : Url.RouteUrl("Report", new { showReportCover = false })
            };

            return View(model);
        }

        public ActionResult Admin(string cid, bool showReportCover = true, string trackingType = null)
        {
            var issuer = _issuerService.GetIssuerByCid(cid);
            if (issuer != null)
            {
                var userIssuer = GetIssuerForCurrentUser();
                if (Roles.IsUserInRole(CommonConstants.Roles.ReportAdmin) || Roles.IsUserInRole(CommonConstants.Roles.McAdmin) || Roles.IsUserInRole(CommonConstants.Roles.DevAdmin) || (!string.IsNullOrEmpty(userIssuer.Cid) && userIssuer.Cid == cid))
                {
                    var model = new ReportSummaryModel
                    {
                        TrackingType = string.IsNullOrEmpty(trackingType) ? "adminreport" : $"{trackingType}-adminreport",
                        Issuer = issuer
                    };

                    ProcessReportSummaryModel(showReportCover, issuer, model);

                    return View("Index", model);
                }

                throw new AuthorizationException();
            }

            throw new NotFoundException();
        }

        public ActionResult Share(string key)
        {
            var shareKey = _issuerAssessmentService.GetShareKeyByKey(key);
            if (shareKey != null)
            {
                var issuer = _issuerService.GetIssuerByCid(shareKey.Cid);
                if (issuer != null)
                {
                    var model = new ReportSummaryModel
                    {
                        TrackingType = "sharereport",
                        Issuer = issuer
                    };

                    ProcessReportSummaryModel(false, issuer, model);

                    model.ReportHeader.Name = _userService.GetUser(shareKey.UserID).FullName;

                    return View("Index", model);
                }

                throw new NotFoundException();
            }

            throw new NotFoundException();
        }

        public PartialViewResult ContactForm(Issuer issuer)
        {
            return PartialView("_ContactForm", new ContactFormModel
            {
                User = CurrentUser,
                Issuer = issuer
            });
        }

        public PartialViewResult ShareForm(string cid)
        {
            return PartialView("_ShareForm", new ShareFormModel
            {
                Cid = cid
            });
        }

        [ChildActionOnly]
        public ActionResult FeaturableCarouselSlide(string featureLocationId, string title = null, string longDescription = null)
        {
            var featuredMarquee = _slamContext.CreateQuery()
                                              .FilterContentTypes<MarqueeSlideDTO, YoutubeVideoDTO>()
                                              .FilterFeaturedOnLocation(featureLocationId, FilterOperator.GreaterOrEqual, 0)
                                              .Get()
                                              .Select(ci => ci.AsCarouselSlideItem())
                                              .OrderBy(ci => ci.Order)
                                              .FirstOrDefault();

            var model = new CarouselSlideViewModel
            {
                Slides = Enumerable.Empty<CarouselSlideItem>()
            };

            if (featuredMarquee != null)
            {
                if (!string.IsNullOrEmpty(title))
                {
                    featuredMarquee.Title = title;
                }

                if (!string.IsNullOrEmpty(longDescription))
                {
                    featuredMarquee.Description = string.Empty;
                }

                model.Slides = new List<CarouselSlideItem> { featuredMarquee };
            }

            return PartialView("DisplayTemplates/CarouselSlide", model);
        }

        [HttpPost]
        public ActionResult SendContactForm(string email, string issuer, string name, string message, string cid)
        {
            try
            {
                _emailService.SendLeadGenEmail(email, issuer, name, message, Url.Action("Admin", "Report", new { cid = cid }), CurrentUser.UserId);
                return Json(new { ok = true }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { ok = false }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SendShareForm(string cid)
        {
            if (_settingsService.GetShareReportEnabled())
            {
                var issuer = _issuerService.GetIssuerByCid(cid);
                if (issuer != null)
                {
                    SendShareFormEmail(issuer, ConvertUrltoPdf(_urlService.GetShareReportUrl(_issuerAssessmentService.CreateShareKey(CurrentUser.UserId, issuer.Cid)), issuer));
                }
            }

            return Json(new { ok = true }, JsonRequestBehavior.AllowGet);
        }

        private void ProcessReportSummaryModel(bool showReportCover, Issuer issuer, ReportSummaryModel model, bool shareReport = false)
        {
            var reportGoalsModel = _issuerAssessmentService.GetReportAssessment(issuer.Cid);
            if (reportGoalsModel == null)
            {
                model.Error = true;
                showReportCover = false;
                string message = $"Cannot find CID {issuer.Cid} for Assessment Report";
                LogManager.EventFactory.Create()
                                       .Text(message)
                                       .Level(LoggingEventLevel.Warning)
                                       .Push();

                var importedIca = _importedIcaRepository.GetByCid(issuer.Cid);

                _issuerUnmatchedRepository.Add(new IssuerUnmatched
                {
                    Error = "NOHEATMAP",
                    Message = message,
                    IssuerID = issuer.IssuerId,
                    ImportedIcaId = importedIca.ImportedIcaId,
                    UserId = CurrentUser.UserId,
                    Date = DateTime.Now
                });

                _emailService.SendNotificationEmail(issuer.Cid, issuer.Title, issuer.DomainName, importedIca.LegalName, CurrentUser.FullName, CurrentUser.Email, CurrentUser.UserId);
            }

            model.ReportHeader = GetReportHeaderModel(issuer.Cid);
            model.ReportAssessment = reportGoalsModel;
            model.ShowReportCover = showReportCover;
            model.ShareReport = shareReport;
        }

        private Issuer GetIssuerForCurrentUser()
        {
            return _issuerService.GetIssuerById(CurrentUser.IssuerId);
        }

        private ReportHeaderModel GetReportHeaderModel(string cid)
        {
            return new ReportHeaderModel
            {
                Name = UserContext.User?.FullName,
                ImportedIca = _importedIcaRepository.GetByCid(cid)
            };
        }

        private void SendShareFormEmail(Issuer issuer, string pdfFileName)
        {
            var importedIca = _importedIcaRepository.GetByCid(issuer.Cid);
            if (importedIca != null)
            {
                _emailService.SendShareReportEmail(CurrentUser.Email, importedIca.LegalName, CurrentUser.FullName, UserContext.User.Profile.Phone, pdfFileName, CurrentUser.UserId);
            }
        }

        private string ConvertUrltoPdf(string urlToConvert, Issuer issuer)
        {
            // Create the PDF converter. Optionally the HTML viewer width can be specified as parameter
            // The default HTML viewer width is 1024 pixels.
            PdfConverter pdfConverter = new PdfConverter
            {
                LicenseKey = _settingsService.GetEvoPdfLicenseKey()
            };

            // set the license key - required
            // set the converter options - optional
            pdfConverter.PdfDocumentOptions.AutoSizePdfPage = true;

            // Select the HTML elements for which to retrieve location and other information from HTML document
            pdfConverter.HiddenHtmlElementsSelectors = new[] { "nav" };

            // Performs the conversion and get the pdf document bytes that can be saved to a file or sent as a browser response
            byte[] pdfBytes = pdfConverter.GetPdfBytesFromUrl(urlToConvert);

            var dir = Server.MapPath("~/App_Data/EvoPdfs");
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            var pdfFileName = $"{dir}\\{DateTime.Now.ToString("yyyy-MM-dd")}_{issuer.Title.Replace(" ", "_")}_Report.pdf";

            // write content to the pdf
            using (var fs = new FileStream(pdfFileName, FileMode.Create))
            {
                using (var writer = new BinaryWriter(fs))
                {
                    writer.Write(pdfBytes, 0, pdfBytes.Length);
                    writer.Close();
                }
            }

            return pdfFileName;
        }
    }
}