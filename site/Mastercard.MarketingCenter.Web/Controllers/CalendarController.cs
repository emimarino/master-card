﻿using EvoPdf;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Filters;
using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Exceptions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Models;
using Mastercard.MarketingCenter.Web.Core.Services;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class CalendarController : BaseController
    {
        private readonly SlamContextService _slamContextService;
        private readonly IMarketingCenterWebApplicationService _marketingCenterWebApplicationService;
        private readonly ICalendarService _calendarService;
        private readonly ITagService _tagService;
        private readonly IRegionService _regionService;
        private readonly IUserRepository _userRepository;
        private readonly IRegionalizeService _regionalizeService;
        private readonly IUrlService _urlService;
        private readonly ISettingsService _settingsService;
        private readonly ICachingService _cachingService;

        public CalendarController(SlamContextService slamContextService, IMarketingCenterWebApplicationService marketingCenterWebApplicationService, ICalendarService calendarService, ITagService tagService, IRegionService regionService, IUserRepository userRepository, IRegionalizeService regionalizeService, IUrlService urlService, ISettingsService settingsService, ICachingService cachingService)
        {
            _slamContextService = slamContextService;
            _marketingCenterWebApplicationService = marketingCenterWebApplicationService;
            _calendarService = calendarService;
            _tagService = tagService;
            _regionService = regionService;
            _userRepository = userRepository;
            _regionalizeService = regionalizeService;
            _urlService = urlService;
            _settingsService = settingsService;
            _cachingService = cachingService;
        }

        public ActionResult Index()
        {
            if (HasUserAccess())
            {
                var page = _slamContextService.GetPageByUri("calendar", true);
                if (page == null)
                {
                    throw new NotFoundException();
                }

                var model = new CalendarViewModel { Page = page };

                if (!string.IsNullOrWhiteSpace(model.Page.RestrictToSpecialAudience))
                {
                    if (!string.IsNullOrWhiteSpace(CurrentSpecialAudience))
                    {
                        string[] stringSeparators = new string[] { ";#" };

                        var pageSpecialAudiences = model.Page.RestrictToSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                        var currentSpecialAudiences = CurrentSpecialAudience.ToLowerInvariant().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                        bool match = false;
                        foreach (var specAud in currentSpecialAudiences)
                        {
                            if (pageSpecialAudiences.Contains(specAud))
                            {
                                match = true;
                                break;
                            }
                        }

                        if (!match)
                        {
                            throw new AuthorizationException();
                        }
                    }
                    else
                    {
                        throw new AuthorizationException();
                    }
                }

                TempData["ContentItemId"] = model.Page.ContentItemId;
                TempData["ContentTypeId"] = model.Page.ContentTypeId;
                ViewBag.ShowEndYear = _settingsService.GetMarketingCalendarShowEndYear(UserContext.SelectedRegion);

                return View(model);
            }

            throw new NotFoundException();
        }

        public ActionResult DownloadCalendar(string key)
        {
            var downloadCalendarKey = _calendarService.GetDownloadCalendarKey(key);
            if (downloadCalendarKey != null)
            {
                RegionalizeService.SetCurrentCulture(downloadCalendarKey.RegionId, downloadCalendarKey.Language);
                ViewBag.DownloadCalendarKey = downloadCalendarKey;
                return View();
            }

            throw new NotFoundException();
        }

        public ActionResult GetDownloadCalendar(int downloadYear, string campaignCategoryIds = "", string marketIdentifiers = "")
        {
            if (HasUserAccess())
            {
                var downloadCalendar = _calendarService.CreateDownloadCalendarKey(CurrentUser.UserId,
                                                                              UserContext.SelectedRegion,
                                                                              UserContext.SelectedLanguage,
                                                                              _marketingCenterWebApplicationService.GetCurrentSpecialAudience(),
                                                                              string.Join(",", _marketingCenterWebApplicationService.GetCurrentSegmentationIds()),
                                                                              campaignCategoryIds,
                                                                              marketIdentifiers,
                                                                              downloadYear);
                SaveDownloadCalendarCache(downloadCalendar);

                string downloadCalendarTitle = string.Format(Resources.Calendar.DownloadCalendarHeader, downloadYear);
                PdfConverter pdfConverter = new PdfConverter { LicenseKey = ConfigurationManager.AppSettings["EvoPdfLicenseKey"] };
                pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Landscape;
                pdfConverter.PdfDocumentOptions.FitWidth = true;
                pdfConverter.PdfDocumentOptions.LeftMargin = 10;
                pdfConverter.PdfDocumentOptions.RightMargin = 10;
                pdfConverter.PdfHeaderOptions.AddElement(new HtmlToPdfVariableElement(GetDownloadCalendarHeader(downloadYear), null));
                pdfConverter.PdfDocumentOptions.ShowHeader = true;
                pdfConverter.PdfFooterOptions.AddElement(new HtmlToPdfVariableElement(GetDownloadCalendarFooter(), null));
                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentInfo.Title = downloadCalendarTitle;

                string downloadCalendarUrl = _urlService.GetFullUrlOnFrontEnd(Url.Action("DownloadCalendar", "Calendar", new { key = downloadCalendar.Key }));
                Response.AddHeader("Content-Disposition", $"inline;filename=\"{downloadCalendarTitle}.pdf\"");

                return File(pdfConverter.GetPdfBytesFromUrl(downloadCalendarUrl), "application/pdf");
            }

            throw new NotFoundException();
        }

        private string GetDownloadCalendarHeader(int calendarYear)
        {
            string templateBody = GetCalendarTemplate("Header.htm");
            templateBody = templateBody.Replace("[#FrontEndUrl]", _urlService.GetFrontEndHomeURL().TrimEnd('/'));
            templateBody = templateBody.Replace("[#CalendarYear]", calendarYear.ToString());

            return templateBody;
        }

        private string GetDownloadCalendarFooter()
        {
            string templateBody = GetCalendarTemplate("Footer.htm");
            return templateBody.Replace("[#FrontEndUrl]", _urlService.GetFrontEndHomeURL().TrimEnd('/')); ;
        }

        private string GetCalendarTemplate(string filePath)
        {
            string templateFolder = "Views/Calendar/DownloadTemplates/";
            string templateFile = RegionalizeService.RegionalizeTemplate(filePath, UserContext.SelectedRegion, UserContext.SelectedLanguage, templateFolder);
            string templateBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, templateFolder, templateFile)))
            {
                templateBody = reader.ReadToEnd();
            }

            return templateBody;
        }

        [NoCache]
        public ActionResult ContentItemCampaignEvents()
        {
            if (HasUserAccess())
            {
                int currentYear = DateTime.Now.Year;
                DateTime calendarStartDate = new DateTime(currentYear, 1, 1);
                DateTime calendarEndDate = new DateTime(currentYear + 1, 12, 31);

                return new JsonResult
                {
                    Data = new CalendarContentItemCampaignEventsModel
                    {
                        ContentItems = GetCalendarContentItemModel(_calendarService.GetContentItemCampaignEventsFilterByIds(
                                                               _slamContextService.GetCalendarContentItems(null, null, null, null, null, true, true)
                                                                                  .Select(p => p.ContentItemId).ToArray(),
                                                               calendarStartDate,
                                                               calendarEndDate),
                                                           UserContext.SelectedRegion,
                                                           UserContext.SelectedLanguage),
                        CalendarStartDate = calendarStartDate,
                        CalendarEndDate = calendarEndDate,
                        RegionIdentifier = UserContext.SelectedRegion,
                        CampaignCategories = _calendarService.GetCampaignCategoriesByRegionAndLanguage(UserContext.SelectedRegion, UserContext.SelectedLanguage)
                                                        .Select(c => GetCalendarCampaignCategoryModel(c))
                                                        .OrderBy(c => c.Order)
                                                        .ThenBy(c => c.Title),
                        Markets = _tagService.GetCountries(UserContext.SelectedRegion, UserContext.SelectedLanguage)
                                             .Select(c => GetCalendarMarketModel(c.Key, c.Value))
                                             .OrderBy(m => m.Title)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }

            throw new NotFoundException();
        }

        [NoCache]
        public ActionResult ContentItemCampaignEventsByDownloadCalendarKey(string key)
        {
            var downloadCalendarKey = _calendarService.GetDownloadCalendarKey(key);
            if (downloadCalendarKey != null)
            {
                return new JsonResult
                {
                    Data = GetCalendarContentItemCampaignEventsModelByDownloadCalendarKey(downloadCalendarKey),
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }

            throw new NotFoundException();
        }

        private IEnumerable<CalendarContentItemModel> GetCalendarContentItemModel(IEnumerable<ContentItemCampaignEvent> contentItems, string regionIdentifier, string language = null)
        {
            if (language == null)
            {
                language = UserContext.SelectedLanguage;
            }

            var calendarContentItems = new List<CalendarContentItemModel>();

            GetPreGroupedEvents(contentItems).GroupBy(pce => new
            {
                pce.ContentItem,
                pce.ContentItemForCalendar,
                pce.CampaignEvent.CampaignCategories,
                pce.Row
            })
            .ForEach(pce => calendarContentItems.AddRange(pce.Key.CampaignCategories.Select(ct =>
            {
                return new CalendarContentItemModel
                {
                    ContentItemId = pce.Key.ContentItem.ContentItemId,
                    Title = pce.Key.ContentItemForCalendar.Title,
                    FrontEndUrl = GetContentItemFrontEndUrl(pce.Key.ContentItemForCalendar),
                    MarketIdentifiers = pce.Key.ContentItemForCalendar.Markets.Any(m => !m.Identifier.Equals(regionIdentifier, StringComparison.InvariantCultureIgnoreCase)) ?
                                        pce.Key.ContentItemForCalendar.Markets.Where(m => !m.Identifier.Equals(regionIdentifier, StringComparison.InvariantCultureIgnoreCase)).Select(m => m.Identifier) :
                                        new string[] { regionIdentifier },
                    CampaignCategoryId = ct.CampaignCategoryId,
                    CampaignCategoryTitle = ct.CampaignCategoryTranslatedContents?.FirstOrDefault(c => c.LanguageCode == language)?.Title,
                    ShortDescription = pce.Key.ContentItemForCalendar.ShortDescription?.StripHtml(),
                    CampaignEvents = pce.OrderBy(g => g.CampaignEvent.StartDate)
                                        .Select(g => GetCalendarCampaignEventModel(pce.Key.ContentItemForCalendar.InMarketStartDate, pce.Key.ContentItemForCalendar.InMarketEndDate, g.CampaignEvent)),
                    Priority = pce.Key.ContentItemForCalendar.MarketingCalendarPriority,
                    ExpirationDate = pce.Key.ContentItemForCalendar.ContentItem.ExpirationDate
                };
            })));

            return calendarContentItems.OrderBy(p => p.Priority)
                                       .ThenBy(p => p.Title);
        }

        private IEnumerable<ContentItemCampaignEventModel> GetPreGroupedEvents(IEnumerable<ContentItemCampaignEvent> contentItem)
        {
            var allEvents = contentItem.Select(p => new ContentItemCampaignEventModel
            {
                ContentItemForCalendar = p.ContentItemForCalendar,
                ContentItem = p.ContentItemForCalendar.ContentItem,
                CampaignEvent = p.CampaignEvent,
                Row = 1
            })
            .OrderBy(e => e.ContentItem.ContentItemId)
            .ThenBy(e => e.CampaignEvent.StartDate)
            .ToList();

            Dictionary<int, ContentItemCampaignEventModel> previousEvents = new Dictionary<int, ContentItemCampaignEventModel>();
            foreach (var currentEvent in allEvents)
            {
                foreach (var k in previousEvents.Keys.OrderBy(p => p))
                {
                    var startDate = currentEvent.CampaignEvent.AlwaysOn ? (currentEvent?.ContentItemForCalendar?.InMarketStartDate).Value : currentEvent.CampaignEvent.StartDate.Value;
                    var endDate = previousEvents[k].CampaignEvent.AlwaysOn ? (previousEvents[k]?.ContentItemForCalendar?.InMarketEndDate).Value : previousEvents[k].CampaignEvent.EndDate.Value;
                    if (previousEvents[k] != null
                        && currentEvent.ContentItem.ContentItemId.Equals(previousEvents[k].ContentItem.ContentItemId)
                        && currentEvent.CampaignEvent.CampaignCategories.Any(ct => previousEvents[k].CampaignEvent.CampaignCategories.Select(act => act.CampaignCategoryId).Contains(ct.CampaignCategoryId))
                        && startDate.AddDays(-1 * startDate.Day + 1) <= endDate.AddMonths(1).AddDays(-1 * endDate.Day))
                    {
                        currentEvent.Row = previousEvents[k].Row + 1;
                    }
                    else //if no date conflict, break
                    {
                        break;
                    }
                }
                previousEvents[currentEvent.Row - 1] = currentEvent;
            }

            return allEvents;
        }

        private string GetContentItemFrontEndUrl(ContentItemForCalendar contentItem)
        {
            return !contentItem.ContentItem.ExpirationDate.HasValue || contentItem.ContentItem.ExpirationDate >= DateTime.Today ?
                    _urlService.SetFrontEndUrl(
                       new Dictionary<string, object>() { { MarketingCenterDbConstants.FieldNames.Title, contentItem.Title }, { MarketingCenterDbConstants.FieldNames.Uri, contentItem.ContentItem.FrontEndUrl } },
                       contentItem.ContentItem.ContentType.FrontEndUrl,
                       contentItem.ContentItemId) :
                    string.Empty;
        }

        private CalendarCampaignEventModel GetCalendarCampaignEventModel(DateTime? inMarketStartDate, DateTime? inMarketEndDate, CampaignEvent campaignEvent)
        {
            return new CalendarCampaignEventModel
            {
                ItemId = campaignEvent.CampaignEventId,
                Title = campaignEvent.Title,
                StartDate = campaignEvent.AlwaysOn ? inMarketStartDate.Value : campaignEvent.StartDate.Value,
                EndDate = campaignEvent.AlwaysOn ? inMarketEndDate.Value : campaignEvent.EndDate.Value,
                AlwaysOn = campaignEvent.AlwaysOn
            };
        }

        private CalendarCampaignCategoryModel GetCalendarCampaignCategoryModel(CampaignCategory campaignCategory, string language = null)
        {
            var campaignCategoryTranslatedContent = campaignCategory?.CampaignCategoryTranslatedContents?.FirstOrDefault(c => c.LanguageCode.Equals(language ?? UserContext.SelectedLanguage, StringComparison.InvariantCultureIgnoreCase));
            return new CalendarCampaignCategoryModel
            {
                ItemId = campaignCategory.CampaignCategoryId,
                Title = campaignCategoryTranslatedContent?.Title,
                Order = campaignCategory.OrderNumber,
                MarketingCalendarUrl = campaignCategory.MarketingCalendarUrl
            };
        }

        private CalendarMarketModel GetCalendarMarketModel(string identifier, string title)
        {
            return new CalendarMarketModel
            {
                Identifier = identifier,
                Title = title
            };
        }

        private void SaveDownloadCalendarCache(DownloadCalendarKey downloadCalendarKey)
        {
            _cachingService.Save(
                GetCalendarContentItemCampaignEventsModelCacheKey(downloadCalendarKey.Key),
                GetCalendarContentItemCampaignEventsModelByDownloadCalendarKey(downloadCalendarKey),
                new TimeSpan(0, 10, 0));
        }

        private CalendarContentItemCampaignEventsModel GetCalendarContentItemCampaignEventsModelByDownloadCalendarKey(DownloadCalendarKey downloadCalendarKey)
        {
            string calendarContentItemCampaignEventsModelCacheKey = GetCalendarContentItemCampaignEventsModelCacheKey(downloadCalendarKey.Key);
            var calendarContentItemCampaignEventsModel = _cachingService.Get<CalendarContentItemCampaignEventsModel>(calendarContentItemCampaignEventsModelCacheKey);
            if (calendarContentItemCampaignEventsModel == null)
            {
                DateTime calendarStartDate = new DateTime(downloadCalendarKey.DownloadYear, 1, 1);
                DateTime calendarEndDate = new DateTime(downloadCalendarKey.DownloadYear, 12, 31);

                return new CalendarContentItemCampaignEventsModel
                {
                    ContentItems = GetCalendarContentItemModel(_calendarService.GetContentItemCampaignEventsFilterByIds(
                                                                    _slamContextService.GetCalendarContentItems(
                                                                        downloadCalendarKey.RegionId,
                                                                        downloadCalendarKey.Language,
                                                                        downloadCalendarKey.SpecialAudience,
                                                                        downloadCalendarKey.SegmentationIds.Split(','),
                                                                        null,
                                                                        true,
                                                                        true
                                                                    ).Select(p => p.ContentItemId).ToArray(),
                                                                    calendarStartDate,
                                                                    calendarEndDate),
                                                               downloadCalendarKey.RegionId,
                                                               downloadCalendarKey.Language),
                    CalendarStartDate = calendarStartDate,
                    CalendarEndDate = calendarEndDate,
                    RegionIdentifier = downloadCalendarKey.RegionId,
                    CampaignCategories = _calendarService.GetCampaignCategoriesByIds(downloadCalendarKey.CampaignCategoryIds.Split(','))
                                                         .Select(c => GetCalendarCampaignCategoryModel(c, downloadCalendarKey.Language))
                                                         .OrderBy(c => c.Order)
                                                         .ThenBy(c => c.Title),
                    Markets = _tagService.GetTagsByIdentifiersAndLanguage(downloadCalendarKey.MarketIdentifiers.Split(','), downloadCalendarKey.Language)
                                         .Select(m => GetCalendarMarketModel(m.Key, m.Value))
                                         .OrderBy(m => m.Title)
                };
            }

            return calendarContentItemCampaignEventsModel;
        }

        private string GetCalendarContentItemCampaignEventsModelCacheKey(string key)
        {
            return $"CalendarContentItemCampaignEventsModel_{key}";
        }

        private bool HasUserAccess()
        {
            return User.IsInFullAdminRole() ? _settingsService.GetAdminFrontEndMarketingCalendarEnabled(UserContext.SelectedRegion)
                                            : _settingsService.GetEndUserFrontEndMarketingCalendarEnabled(UserContext.SelectedRegion);
        }
    }
}