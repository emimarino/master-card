﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Models;
using Mastercard.MarketingCenter.Web.Core.Models.Search;
using Mastercard.MarketingCenter.Web.UserControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Controllers
{
    public class SearchController : BaseController
    {
        private readonly IUrlService _urlService;
        private readonly ISearchService _searchService;

        public static string DefaultRegion { get { return System.Configuration.ConfigurationManager.AppSettings["DefaultRegion"]; } }
        public static string DefaultLanguage { get { return System.Configuration.ConfigurationManager.AppSettings["DefaultLanguage"]; } }

        public SearchController(IUrlService urlService, ISearchService searchService)
        {
            _urlService = urlService;
            _searchService = searchService;
        }

        public ActionResult Index(string searchQuery, PostbackVisitLinkModel postbackModel)
        {
            if (!string.IsNullOrEmpty(postbackModel.UrlVisited))
            {
                AddSearchActivity(postbackModel.UrlVisited, searchQuery);
                return Redirect(postbackModel.UrlVisited);
            }

            SearchContentResultModel AllResults = _searchService.GetSearchResults(searchQuery, UserContext.SelectedRegion, CurrentUser.Issuer.SpecialAudienceAccess);
            var searchResultsQty = AllResults.FeaturedSearchResults.Count() + AllResults.SearchResults.Count();
            AddSearchActivity(postbackModel.UrlVisited, searchQuery, searchResultsQty);
            SearchModel searchModel = new SearchModel()
            {
                SearchQuery = searchQuery.GetQueryClearOfSynonyms(),
                ResultsCount = searchResultsQty
            };

            return View(searchModel);
        }


        public ActionResult GetSearchResults(SearchListViewModel model)
        {
            if (model.SearchQuery.IsNullOrEmpty())
            {
                return null;
            }

            SearchContentResultModel AllResults = _searchService.GetSearchResults(model.SearchQuery, UserContext.SelectedRegion, CurrentUser.Issuer.SpecialAudienceAccess);
            model.SearchQuery = model.SearchQuery.GetQueryClearOfSynonyms();
            AddBreadCrumbInfo(model.SearchQuery);

            if (!model.SortBy.Equals(Constants.SearchOrderOption.Relevance))
            {
                AllResults.SearchResults = AllResults.SearchResults.OrderBy($"{model.SortBy} {model.SortDirection}").ToList();
            }

            var searchResultsCount = AllResults.SearchResults.Count();
            AllResults.SearchResults = AllResults.SearchResults.ToList().GetRange(model.Start, (model.Length == 0 || searchResultsCount < model.Length + model.Start) ? searchResultsCount - model.Start : model.Length);
            AllResults.FeaturedSearchResults.AsParallel().Where(l => l.SearchLanguage.IsNullOrEmpty() || l.SearchLanguage == UserContext.SelectedLanguage)
                                                         .ForEach(x => x.Image = _urlService.GetStaticImageURL(x.Image, 256, 256, false, 85, false))
                                                         .ToList();
            return new JsonResult
            {
                Data = AllResults,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        private void AddSearchActivity(string urlVisited, string queryPath, int? searchResultsQty = null)
        {
            string searchQuery = queryPath == null ? string.Empty : queryPath.Replace("quotenc", "\"\"");
            searchQuery = Server.UrlDecode(searchQuery);
            _searchService.AddNewSearchActivity(UserContext.User.UserName, DateTime.Now, searchQuery, urlVisited, UserContext.SelectedRegion, searchResultsQty);
        }

        private void AddBreadCrumbInfo(string searchQuery)
        {
            var baseUrl = _urlService.GetFrontEndHomeURL();

            List<BreadCrumbItem> breadCrumbs = new List<BreadCrumbItem>() { new BreadCrumbItem() { Title = Resources.Shared.Home, Url = baseUrl }, new BreadCrumbItem() { Title = Resources.Forms.Search_Title, Url = _urlService.GetSearchURL(searchQuery) } };

            Session["BreadCrumbItemList"] = breadCrumbs;
        }
    }
}