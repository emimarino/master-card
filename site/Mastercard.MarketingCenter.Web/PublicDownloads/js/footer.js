_.templateSettings = {
    evaluate: /\{\{#([\s\S]+?)\}\}/g,            // {{# console.log("blah") }}
    interpolate: /\{\{[^#\{]([\s\S]+?)[^\}]\}\}/g,  // {{ title }}
    escape: /\{\{\{([\s\S]+?)\}\}\}/g         // {{{ title }}}
};

(function () {
    var Sizer = {
        setHeights: function ($elements, options) {
            // options { lineHeight, reset, resize, selector }
            if (!$elements) {
                return;
            }
            options = options || {};

            // if $selector was declared, ensure it's a jQuery obj, otherwise assume it's $elements
            var $selector = $elements;
            if (options['selector']) {
                $selector = $elements.find(options['selector']);
            }
            // reset element heights
            if (options['reset'] !== false) {
                Sizer.adjustHeights($elements, options, 'auto');
                Sizer.adjustHeights($selector, options, 'auto');
            }

            // determine height of tallest selector
            var maxHeight = _.max($selector.map(function () {
                return $(this).outerHeight(true);
            }));
            // if maxHeight wasn't returned correctly, return 0
            if (!_.isNumber(maxHeight) || !_.isFinite(maxHeight)) {
                return 0;
            }

            // resize elements to maxHeight
            if (options['resize'] !== false) {
                Sizer.adjustHeights($elements, options, maxHeight);
            }

            return maxHeight;
        },
        adjustHeights: function ($elements, options, size) {
            if (options['lineHeight'] !== true) {
                $elements.css({ height: size });
            } else {
                $elements.css({ height: size, lineHeight: size + 'px' });
            }
        },
        setBackgroundImage: function ($el, $img) {
            var elHeight = $el.outerHeight();
            var imgHeight = $img.outerHeight();
            if (elHeight > imgHeight) {
                $img.outerHeight(elHeight);
            }

            var elWidth = $el.outerWidth();
            var imgWidth = $img.width();
            if (elWidth > imgWidth) {
                $img.width(elWidth);
            }
            if (elWidth < imgWidth) {
                var marginLeft = (imgWidth - elWidth) / -2;
                $img.css({ marginLeft: marginLeft });
            }
        }
    };
    MCWCM.Sizer = Sizer;
})();
// Poor man's media query matching library.  Since IE 8-9 require advanced mediaquery shims to use mediaMatch for more than screen and print ex. https://github.com/weblinc/media-match.  If the project only requires simple width mediaqueries this is a better solution.

(function () {
    var eventMatrix = {},
        windowWidth = window.innerWidth,
        handlerName = function (inside) { return (inside) ? 'inside' : 'outside'; };

    window.breakerbox = {
        on: function (key, breakpoint, enterFn, exitFn) {
            var inside = windowWidth < breakpoint;

            if (!eventMatrix[breakpoint]) {
                eventMatrix[breakpoint] = {
                    watchers: {},
                    inside: inside
                }
            }

            // if no exitFn function was defined use enterFn for it
            if (arguments.length === 3) {
                exitFn = enterFn;
            }

            var watcherMethods = {
                inside: enterFn,
                outside: exitFn
            };

            var fnName = handlerName(inside);
            watcherMethods[fnName]();

            eventMatrix[breakpoint].watchers[key] = watcherMethods;
        },

        off: function (key, breakpoint) {
            delete eventMatrix[breakpoint].watchers[key];
        },

        // handler for resize you need to attach the listener yourself this allows it to be throttled and bound using what ever library you prefer.
        resize: function () {
            windowWidth = window.innerWidth;
            for (var breakpoint in eventMatrix) {
                var config = eventMatrix[breakpoint];
                var inside = windowWidth < parseInt(breakpoint);
                if (inside !== config.inside) {
                    var fnName = handlerName(inside);
                    config.inside = inside;
                    for (var key in config.watchers) {
                        config.watchers[key][fnName]();
                    }
                }
            }
        },
        activeListeners: function () {
            return eventMatrix;
        }
    };
})();
/* globals jQuery, _ */
(function ($, _, MCWCM) {
    // Generates a standard init function for a component
    var ComponentWrapperFactory = function (selector, defaultOptions, instanceObject) {
        var parseInstanceOptions = function ($root) {
            var $config = $root.find('> .component-config');
            var instanceOptions = {};

            _.each(defaultOptions, function (optionValue, optionKey) {
                var dataValue = $config.data(optionKey);

                if (!_.isUndefined(dataValue)) {
                    instanceOptions[optionKey] = dataValue;
                }
            });
            return instanceOptions;
        };

        return {
            init: function (siteOptions) {
                siteOptions = _.extend({}, defaultOptions, siteOptions);

                $(selector).each(function () {
                    var $this = $(this);
                    var instanceOptions = _.extend({}, siteOptions, parseInstanceOptions($this));
                    new instanceObject($this, instanceOptions);
                });
            }
        }
    };

    MCWCM.ComponentWrapperFactory = ComponentWrapperFactory;
})(jQuery, _, MCWCM);

; (function (window, undefined) {
    function Initializr(namespace, name) {
        if (_.isObject(namespace)) {
            this.namespace = namespace;
        } else {
            throw new Error('no namespace specified');
        }

        this.name = name;
        this._errors = [];
        return this;
    };

    $.extend(Initializr.prototype, {
        component: function (name, args) {
            var initializr = this;
            var namespace = initializr.namespace;
            var component;

            if (!_.isUndefined(args)) {
                if (!_.isArray(args)) {
                    args = [args];
                }
            } else {
                args = [];
            }

            if (namespace) {
                component = namespace[name];
            }
            if (component && _.isFunction(component.init)) {
                component.init.apply(component, args);
            } else {
                initializr._errors.push(name);
            }
        },
        logErrors: function () {
            var initializr = this;
            var errors = initializr._errors;
            if (errors.length) {
                _.each(errors, function (err) {
                    console.error('%s Component %s failed to load.', initializr.name, err);
                });
            } else {
                console.log('%s Components successfully loaded.', initializr.name);
            }

            errors = [];
        }
    });

    window.Initializr = Initializr;

})(this);
/* globals jQuery */

/*
	Window Resize
	Custom jquery event to get around IE8 issue where resize event on window is fired anytime the body height changes.
	http://snook.ca/archives/javascript/ie6_fires_onresize
*/

(function ($) {
    var $event = $.event,
        $special,
        windowHeight,
        windowWidth;

    $special = $event.special.windowresize = {
        setup: function () {
            $(this).on("resize", $special.handler);
        },
        teardown: function () {
            $(this).off('resize', $special.handler);
        },
        handler: function (event, execAsap) {
            var context = this,
                args = arguments,
                dispatch = function () {
                    event.type = 'windowresize';
                    $event.dispatch.apply(context, args);
                };

            if (windowHeight !== document.documentElement.clientHeight || windowWidth !== document.documentElement.clientWidth) {
                dispatch();
            }
            windowHeight = document.documentElement.clientHeight;
            windowWidth = document.documentElement.clientWidth;
        }
    };
})(jQuery);
/* globals jQuery, MCWCM */
(function ($, MCWCM) {

    var JumpFocusInstance = function ($root) {
        this.$focusTarget = $($root.data('target'));
        $root.on('click', function () {
            this.$focusTarget.focus();
        }.bind(this));
    };

    MCWCM.JumpFocus = MCWCM.ComponentWrapperFactory('.jump-to', {}, JumpFocusInstance);
})(jQuery, MCWCM);
/* globals jQuery, _, MCWCM */

(function ($, _, MCWCM) {
    // swap call to action button text which is long form with the data-real-copy attribute's value.
    var CallToActionButtonInstance = function ($root, options) {
        this.$root = $root;
        this.setCopy();
        this.setClear();
        $(window).on('resize', _.debounce($.proxy(this.setClear, this), 200));
    };

    $.extend(CallToActionButtonInstance.prototype, {
        setCopy: function () {
            this.$root.find('a.cta').each(function () {
                var $copy = $(this).find('.btn-copy');
                var realCopy = $(this).data('real-copy');
                $copy.html(realCopy);
            });
        },
        setClear: function () {
            if ($('body').width() < 480) {
                this.addClear();
            } else {
                this.$root.find('.clearfix').remove();
            }
        },
        addClear: function () {
            this.$root.find('a.cta:first').after('<div class="clearfix"></div>');
        }
    });

    MCWCM.CallToActionButton = MCWCM.ComponentWrapperFactory('.cta-group', {}, CallToActionButtonInstance);
})($, _, MCWCM);
(function () {
    var FeatureCarouselInstance = function ($root) {
        this.$root = $root;

        this.$root.imagesLoaded($.proxy(this.init, this));
        $(window).on("windowresize", _.debounce($.proxy(this.onResize, this), 40));
    };

    $.extend(FeatureCarouselInstance.prototype, {
        init: function () {
            this.bindObjects();

            this.setDescription();
            this.setPositions();
            this.setHeights();

            this.bindEvents();
            this.$root.css({ visibility: 'visible' });
        },
        reinit: function () {
            this.setDescription();
            this.setPositions();
        },

        resetClasses: function (currentState) {
            this.$items.removeClass('active');
            if (currentState === 'default') {
                this.$root.removeClass('active').addClass('default');
            } else {
                this.$root.removeClass('default').addClass('active');
                this.setClasses();
            }
        },
        setClasses: function () {
            this.$items.eq(this.activeIndex).addClass('active');
            this.reinit();
        },
        setDescription: function () {
            this.$itemTitles.show();
            if (this.$root.hasClass('default')) {
                this.$activeData.html(this.$featureData.html());
            } else {
                var $activeItemTitle = this.$items.eq(this.activeIndex).find(this.activeItemTitle);
                $activeItemTitle.hide();
                var activeHTML = this.$items.eq(this.activeIndex).find(this.activeItemData).html();
                this.$activeData.html(activeHTML);
            }
        },
        setHeights: function () {
            this.setActiveDataHeight();
            this.setFrameHeight();
            this.backgroundHeight = this.$root.outerHeight();
            this.setBackground();
        },
        setPositions: function () {
            var conveyerItemsLength = this.itemsLength;
            var indexWidth = (this.$frame.width() / this.itemsLength);
            var activeIndex = this.activeIndex;

            this.$items.css('width', indexWidth).each(function () {
				/**Start with: CLICKED INDEX, CURRENT INDEX, CENTER POSITION
				 * 1. INDEX DISTANCE: depending on the larger value, determine difference between
				 *     CLICKED INDEX - CURRENT INDEX ||
				 *     CURRENT INDEX - CLICKED INDEX
				 * 2. ADJUSTED INDEX: depending on the larger value, determine
				 *     CENTER POSITION + INDEX DISTANCE
				 *     CENTER POSITION - INDEX DISTANCE
				** End with: ADJUSTED INDEX (will be used as multiplier for left-position) */

                var indexDistance = 0; // this default is how far from the clicked index the current index is
                var adjustedIndex = (conveyerItemsLength - 1) / 2; // this starting point is the CENTER POSITION

                if ($(this).index() > activeIndex) {
                    indexDistance = $(this).index() - activeIndex;
                    // change the adjustedIndex to be the distance from the center position
                    adjustedIndex = adjustedIndex + indexDistance;
                    if (adjustedIndex >= conveyerItemsLength) { adjustedIndex = adjustedIndex - conveyerItemsLength; }
                } else if ($(this).index() < activeIndex) {
                    indexDistance = activeIndex - $(this).index();
                    // change the adjustedIndex to be the distance from the center position
                    adjustedIndex = adjustedIndex - indexDistance;
                    if (adjustedIndex < 0) { adjustedIndex = adjustedIndex + conveyerItemsLength; }
                }

                var left = adjustedIndex * indexWidth;

                $(this).animate({
                    left: left
                }, 100, function () { });
            });

            return indexWidth;
        },

        setActiveDataHeight: function () {
            this.$itemData.width(this.$root.width());

            var activeDataHeight = this.$featureData.outerHeight();

            var featureDataHeight = 0;
            this.$itemData.each(function () {
                if ($(this).outerHeight() > featureDataHeight) {
                    featureDataHeight = $(this).outerHeight();
                }
            });

            this.$featureData.css({ display: 'none' });
            this.$itemData.css({ display: 'none', width: 0 });
            activeDataHeight = activeDataHeight > featureDataHeight ? activeDataHeight : featureDataHeight;

            this.$activeData.outerHeight(activeDataHeight);
            return activeDataHeight;
        },

        setBackground: function () {
            this.$backgroundImg.css({
                height: 'auto',
                marginLeft: 0,
                width: 'auto'
            });

            this.$backgroundImg.outerHeight(this.backgroundHeight);

            if (this.$backgroundImg.outerWidth() > this.$root.outerWidth()) {

                var marginLeft = (this.$backgroundImg.outerWidth() - this.$root.outerWidth()) / 2;
                this.$backgroundImg.css({ marginLeft: marginLeft * -1 });

            } else {

                this.$backgroundImg.outerWidth(this.$root.outerWidth());
                this.$backgroundImg.outerHeight('auto');

            }
        },

        setFrameHeight: function () {
            var frameHeight = MCWCM.Sizer.setHeights(this.$itemImages, { lineHeight: true, selector: 'img' }) +
                MCWCM.Sizer.setHeights(this.$itemTitles, { resize: false });
            this.$frame.outerHeight(frameHeight);
            return frameHeight;
        },

        bindObjects: function () {
            this.selectorBase = '.feature-carousel__';

            this.$navPrev = this.$root.find(this.selectorBase + 'prev');
            this.$navNext = this.$root.find(this.selectorBase + 'next');

            this.$activeData = this.$root.find(this.selectorBase + 'active-data');
            this.$background = this.$root.find(this.selectorBase + 'background');
            this.$backgroundImg = this.$background.find('img');
            this.$featureData = this.$root.find(this.selectorBase + 'toggle-data');
            this.$frame = this.$root.find(this.selectorBase + 'frame');
            this.$header = this.$root.find(this.selectorBase + 'header');
            this.$items = this.$root.find(this.selectorBase + 'item');
            this.$itemImages = this.$root.find(this.selectorBase + 'item-image');
            this.$itemTitles = this.$root.find(this.selectorBase + 'item-title');
            this.$itemData = this.$root.find(this.selectorBase + 'item-toggle-data');

            this.activeItemTitle = this.selectorBase + 'item-title';
            this.activeItemData = this.selectorBase + 'item-toggle-data';
            this.activeIndex = 0;
            this.itemsLength = this.$items.length;
            this.originalBackgroundHeight = this.$root.find('.feature-carousel__background').outerHeight();
        },
        bindEvents: function () {
            this.$navPrev.on('click', $.proxy(this.onPrevPressed, this)).keyup(clickOnEnter);
            this.$navNext.on('click', $.proxy(this.onNextPressed, this)).keyup(clickOnEnter);

            // Add gesture support using hammerjs
            this.$root.hammer().on({
                'swipeleft': $.proxy(this.onPrevPressed, this),
                'swiperight': $.proxy(this.onNextPressed, this)
            });

            this.$items.on('click', $.proxy(this.onItemPressed, this));
        },

        onItemPressed: function (event) {
            this.activeIndex = $(event.currentTarget).index();
            this.resetClasses('active');
        },
        onNextPressed: function () {
            this.activeIndex = this.activeIndex === 0 ? this.itemsLength - 1 : this.activeIndex - 1;
            this.resetClasses('active');
        },
        onPrevPressed: function () {
            this.activeIndex = this.activeIndex == this.itemsLength - 1 ? 0 : this.activeIndex + 1;
            this.resetClasses('active');
        },
        onResize: function () {
            this.activeIndex = Math.floor(this.itemsLength / 2);
            this.setHeights();
            this.resetClasses('default');
            this.setClasses();
        }
    });

    var clickOnEnter = function (e) {
        if (e.which !== 13) {
            return false;
        }
        $(e.target).click();
    };

    MCWCM.FeatureCarousel = MCWCM.ComponentWrapperFactory('.feature-carousel', {}, FeatureCarouselInstance);
})();
(function () {
    var FAQsInstance = function ($root, options) {
        this.$root = $root;
        $root.data('FAQs', this);
        this.options = options;
        this.$controls = $root.find('.panel-heading');
        this.$collapsibles = $root.find('.collapse');
        this.$collapsibles.collapse({ toggle: false });
        this.$form = this.$root.find('form.faqs__form');

        this.bindToggle();
        this.bindFilter();

        this.handleEmpty();

        this.setDeepLink();
    };

    $.extend(FAQsInstance.prototype, {
        bindFilter: function () {
            var self = this;
            var $form = this.$form;

            $form.on('submit', function (e) {
                e.preventDefault();

                var data = $form.serializeArray();
                data = _.reduce(data, function (memo, obj, i, arr) {
                    memo[obj.name] = obj.value;
                    return memo;
                }, {});

                var filter = data['faqs__filter'];
                var $faqs = self.$root.find('.faq');
                var $filteredFaqs = self.filterQuestions(filter);

                $faqs.not($filteredFaqs).hide();
                $filteredFaqs.show();

                self.handleEmpty(filter);
            });

        },
        faqAllowed: function ($faq, filter) {
            if (!filter) {
                return true;
            }

            var allowed = $faq.find('.filterby').data('filter');
            allowed = allowed.split(',');

            return _.contains(allowed, filter);
        },
        filterQuestions: function (filter) {
            var self = this;
            var $faqs = this.$root.find('.faq');

            if (!filter) {
                return $faqs;
            }

            return $faqs.filter(function () {
                return self.faqAllowed($(this), filter);
            });
        },

        handleEmpty: function (filter) {
            var self = this;
            var $root = this.$root;

            /* loop over each category */
            this.$collapsibles.each(function () {
                var $collapse = $(this);
                var $panel = $collapse.closest('.panel');
                var $msg = $collapse.find('.nofaqs');
                var $faqs = $collapse.find('.faq').filter(function () {
                    return self.faqAllowed($(this), filter);
                });

                if (!$faqs.length) {
                    $msg.show();
                    $panel.hide();
                } else {
                    $msg.hide();
                    $panel.show();
                }
            });

            var $faqs = $root.find('.faq').filter(function () {
                return self.faqAllowed($(this), filter);
            });
            if (!$faqs.length) {
                $root.find('.nofaqsframe').show();
            } else {
                $root.find('.nofaqsframe').hide();
            }
        },

        setDeepLink: function () {
            // Rewards & Cash Back
            // #rewards-and-cash-back
            var deepLink = MCWCM.locationHref.split("#")[1];
            var $cats = this.$root.find('.panel-title').find('> a');
            var panelHeight = this.$controls.outerHeight();

            $cats.each(function (index) {
                var catTitle = steralizeTitle($(this).html());
                if (catTitle === deepLink) {
                    $(this).trigger('click');

                    var marginTop = $('#sticky-header-nav').outerHeight() * 2;
                    marginTop = marginTop + panelHeight;
                    var offsetTop = $(this).offset().top;
                    $(window).scrollTop(offsetTop - marginTop);

                    return;
                }
            });

            function steralizeTitle(title) {
                if (!title) {
                    return title;
                }

                // lowercase
                title = title.toLowerCase();
                // change &amp; or & to 'and'
                title = title.replace(/[&]amp[;]/g, 'and');
                // remove entities
                title = title.replace(/[&][a-zA-Z0-9]+[;]/g, '');
                //remove special characters
                title = title.replace(/[!$%^&*()_+|~=`{}[]:\/;<>?,.@#]/g, '');
                //change spaces to dashes
                title = title.replace(/[ ]/g, '-');

                return title;
            }
        },

        bindToggle: function () {
            var $controls = this.$controls;
            var $collapsibles = this.$collapsibles;

            $controls.on('click', function (e) {
                e.preventDefault();

                var $target = $(e.currentTarget);
                var $collapse = $target.closest('.panel').find('.collapse');
                var willShow = true;

                if ($collapse.hasClass('in')) {
                    willShow = false;
                }
                $collapse.collapse('toggle');

                if (willShow) {
                    $target.addClass('expanded');
                } else {
                    $target.removeClass('expanded');
                }
            });
        }
    });

    MCWCM.FAQs = MCWCM.ComponentWrapperFactory('.faqframe', {}, FAQsInstance);
})();
(function () {
    var HeroBannerCarouselInstance = function ($root, options) {
        this.$root = $root;
        this.options = options;
        this.$slickFrame = this.$root.find('.herobannerpar');
        this.$navPrev = this.$root.find('.prev');
        this.$navNext = this.$root.find('.next');
        this.$data = this.$root.find('.hero-banner-carousel__data');
        this.autoplay = this.$data.data('autoplay');
        this.fade = this.$data.data('fade');
        this.pauseSpeed = this.$data.data('pauseSpeed');
        this.rotationSpeed = this.$data.data('rotationSpeed');

        if (isNaN(this.pauseSpeed)) {
            this.pauseSpeed = 2000;
        } else {
            this.pauseSpeed = this.pauseSpeed * 1000;
        }

        if (isNaN(this.rotationSpeed)) {
            this.rotationSpeed = 600;
        } else {
            this.rotationSpeed = this.rotationSpeed * 1000;
        }

        if ($('body').hasClass('wcmmode-preview')) {
            this.$root.find('.new.section').remove();
        }

        this.$root.imagesLoaded($.proxy(this.slickInit, this));

        $(window).on("windowresize", _.debounce($.proxy(this.onResize, this), 200));
    };

    $.extend(HeroBannerCarouselInstance.prototype, {
        slickInit: function () {
            this.$slickFrame.on('init', $.proxy(this.slickPostInit, this));
            this.$slickFrame.slick(this.getSlickOptions());
        },
        slickPostInit: function () {
            this.bindObjects();
            this.bindEvents();
            this.slideDimensions();
        },

        onResize: function () {
            this.slideDimensions();
        },

        bindEvents: function () {
            this.$navPrev.on('click', $.proxy(this.onSlickPrev, this));
            this.$navNext.on('click', $.proxy(this.onSlickNext, this));

            $slick = this.$slickFrame;

            this.$navDots = this.$root.find('.slick-dots').find('button');
            this.$navDots.on('click', $.proxy(this.onDotClick, this));

            // Add gesture support using hammerjs
            this.$root.hammer().on({
                'swiperight': $.proxy(this.onSlickPrev, this),
                'swipeleft': $.proxy(this.onSlickNext, this)
            });
        },
        bindObjects: function () {
            this.$slickSlides = this.$slickFrame.find('.hero');
            this.$slickImages = this.$slickSlides.find('.image');
            // hide navs when less than max slides
            if (this.$slickFrame.find(this.$slickSlides).length == 1) {
                this.$navPrev.hide();
                this.$navNext.hide();
            }
        },

        getSlickOptions: function () {
            var slideWidth = this.$root.width() / 3;
            return {
                arrows: false,
                autoplay: this.autoplay,
                autoplaySpeed: this.pauseSpeed,
                cssEase: 'linear',
                dots: true,
                draggable: false,
                fade: this.fade,
                infinite: true,
                pauseOnHover: false,
                pauseOnDotsHover: false,
                rtl: this.options.rtl,
                slidesToShow: 1,
                speed: this.rotationSpeed,
                zIndex: 3
            };
        },

        onDotClick: function () {
            this.$slickFrame.slick('slickPause');
            this.$slickFrame.slick('setOption', 'autoplay', false, false);
        },

        onSlickPrev: function () {
            this.slickNav('prev');
        },
        onSlickNext: function () {
            this.slickNav('next');
        },
        slickNav: function (direction) {
            if (direction === 'prev') {
                this.$slickFrame.slick('slickPrev');
                this.$navPrev.blur();
            } else {
                this.$slickFrame.slick('slickNext');
                this.$navNext.blur();
            }
        },

        slideDimensions: function () {
            var frameWidth = this.$slickFrame.outerWidth();

            this.$slickSlides.each(function () {
                var $wrapper = $(this).find('.hero-banner-wrapper');
                var $img = $(this).find('img');
                var isBgImg = $wrapper.hasClass('full');

                if (isBgImg) {
                    $img.height('auto').width('auto');
                    $img.width(frameWidth);
                }
            });

            var sizerSelector = '.hero-banner-wrapper, .text, .full img';
            if ($('body').width() <= MCWCM.breakpoints.xs) {
                sizerSelector = '.text';
            }
            var frameHeight = MCWCM.Sizer.setHeights(this.$slickFrame, {
                reset: true,
                selector: sizerSelector
            });

            this.$slickSlides.height(frameHeight);

            this.$slickSlides.each(function () {
                var $wrapper = $(this).find('.hero-banner-wrapper');
                var imgIsFull = $wrapper.hasClass('full');
                var $img = $(this).find('img');

                if (!$(this).find('img').length && $(this).find('.text.bottom')) {
                    $(this).find('.hero-banner__text--no-image').addClass('hero-banner__text--no-image');
                }

                $(this).removeClass('sizeContent');

                var h = $img.height();
                if (h < frameHeight && imgIsFull) {
                    $img.width('auto');
                    $img.height(frameHeight);
                    centerImage($img, frameWidth);
                }
                function centerImage($img, frameWidth) {
                    var marginLeft = ($img.width() - frameWidth) / -2;
                    $img.css({
                        marginLeft: marginLeft
                    });
                }
            });
        }
    });

    MCWCM.HeroBannerCarousel = MCWCM.ComponentWrapperFactory('.hero-banner-carousel', {
        rtl: false
    }, HeroBannerCarouselInstance);

    /* **********************************************************
     * HERO BANNER START ****************************************
     * **********************************************************/
    var HeroBannerInstance = function ($root, options) {
        this.$root = $root;
        $root.data('HeroBanner', this);
        this.options = options;

        this.$wrapper = this.$root.find('.hero-banner-wrapper');
        this.$image = this.$root.find('.image');
        this.$text = this.$root.find('.text')

        if ($('body').hasClass('wcmmode-edit') &&
            this.$wrapper.hasClass('full') &&
            !this.$image.length) {
            this.$text.css({
                position: 'relative'
            });
        }

        this.$root.imagesLoaded($.proxy(this.init, this));

        $(window).on("windowresize", _.debounce($.proxy(this.onResize, this), 200));
    };

    $.extend(HeroBannerInstance.prototype, {
        init: function () {
            if (this.$root.closest('.hero-banner-carousel').length) {
                // carousel banners are sized to match their siblings
                // these banners are sized in the HeroBannerCarouselInstance
                return;
            }
            this.bannerDimensions();
        },
        onResize: function () {
            this.init();
        },

        bannerDimensions: function () {
            var frameWidth = this.$root.outerWidth();
            var $img = this.$root.find('img');
            var isBgImg = this.$wrapper.hasClass('full');

            if (isBgImg) {
                $img.height('auto').width('auto');
                $img.width(frameWidth);
            }

            var sizerSelector = this.$wrapper;
            if (this.$wrapper.hasClass('full')) {
                if ($('body').width() <= MCWCM.breakpoints.xs) {
                    sizerSelector = '.text';
                } else {
                    sizerSelector = '.text, img';
                }
            }

            var frameHeight = MCWCM.Sizer.setHeights(this.$root, {
                selector: sizerSelector
            });

            this.$wrapper.height(frameHeight);

            if (!isBgImg) {
                return;
            }

            this.$wrapper.removeClass('sizeContent');

            var h = $img.height();

            if (h < frameHeight) {
                this.$wrapper.addClass('sizeContent');
                $img.width('auto');
                $img.height(frameHeight);
            }
        }
    });

    MCWCM.HeroBanner = MCWCM.ComponentWrapperFactory('.hero', {
        'wrapper-modifier': 'sizeContent',
        'carousel-modifier': 'sizeCarousel',
        'content-selector': '>.text',
        'image-selector': '.image img'
    }, HeroBannerInstance);
})();
(function () {
    var HeroBannerLightCarouselInstance = function ($root, options) {
        this.$root = $root;
        this.$slickFrame = this.$root.find('.herobannerlightpar');
        this.options = options;
        this.$navPrev = this.$root.find('.prev');
        this.$navNext = this.$root.find('.next');

        if ($('body').find('.wcmmode-preview')) {
            this.$root.find('.new.section').remove();
        }

        this.$root.imagesLoaded($.proxy(this.slickInit, this));
    };

    $.extend(HeroBannerLightCarouselInstance.prototype, {
        slickInit: function () {
            this.$slickFrame.on('init', $.proxy(this.slickPostInit, this));
            this.$slickFrame.slick(this.getSlickOptions());
        },
        slickPostInit: function () {
            this.bindObjects();
            this.bindEvents();
        },
        bindEvents: function () {
            this.$navPrev.on('click', $.proxy(this.onSlickPrev, this));
            this.$navNext.on('click', $.proxy(this.onSlickNext, this));

            // Add gesture support using hammerjs
            this.$root.hammer().on({
                'swiperight': $.proxy(this.onSlickPrev, this),
                'swipeleft': $.proxy(this.onSlickNext, this)
            });
        },
        bindObjects: function () {
            this.$slickSlides = this.$slickFrame.find(this.classBase + 'slide');
            this.$slickImages = this.$slickSlides.find(this.classBase + 'slide-image');
            // hide navs when less than max slides
            if (this.$slickFrame.find(this.$slickSlides).length == 1) {
                this.$prev.hide();
                this.$next.hide();
            }
        },

        getSlickOptions: function () {
            var slideWidth = this.$root.width() / 3;
            return {
                arrows: false,
                draggable: false,
                infinite: true,
                rtl: this.options.rtl,
                slidesToShow: 1,
                speed: 600
            };
        },
        onSlickPrev: function () {
            this.slickNav('prev');
        },
        onSlickNext: function () {
            this.slickNav('next');
        },
        slickNav: function (direction) {
            if (direction === 'prev') {
                this.$slickFrame.slick('slickPrev');
                this.$navPrev.blur();
            } else {
                this.$slickFrame.slick('slickNext');
                this.$navNext.blur();
            }
        }
    });

    MCWCM.HeroBannerLightCarousel = MCWCM.ComponentWrapperFactory('.hero-banner-light-carousel', {}, HeroBannerLightCarouselInstance);
})();
(function () {
    var ParallaxInstance = function ($root, options) {
        this.$root = $root;
        this.options = options;
        this.curpos = 0;

        this.initParallax();
        this.createNav();
        this.onResize();

        $(window).resize(this.onResize);
        //$(window).on('scroll', $.proxy(this.onScroll, this));
    };

    $.extend(ParallaxInstance.prototype, {
        initParallax: function () {
            setTimeout(this.onResize, 500);
        },

        createNav: function () {
            var navContent = '';
            for (var i = 0; i < $('.parallaxitem').length; i++) {
                navContent += '<div class="parallax-radial"></div>';
            }

            $('.parallaxitem').append('<div class="parallax-nav">' + navContent + '</div>');
            $('.parallax-nav').css('top', $('.parallaxitem').eq(0).find('.parallax-background').height() / 2);
            $('.parallax-nav').each(function () {
                $(this).css('margin-top', 0 - $(this).height() / 2);
            });

            $('.parallax-radial').click(function () {
                this.headSize = $('#sticky-header-nav').height();
                $("body, html").animate({ scrollTop: $('.parallaxitem').eq($(this).index()).offset().top - this.headSize }, 1000);
            });

            $('.parallax-circle-nav').click(function () {
                this.headSize = $('#sticky-header-nav').height();
                var index = $(this).parent().parent().index() + 1;
                $("body, html").animate({ scrollTop: $('.parallaxitem').eq(index).offset().top - this.headSize }, 1000);
            });

            for (var i = 0; i < $('.parallaxitem').length; i++) {
                $('.parallaxitem').eq(i).find('.parallax-radial').eq(i).addClass('selected');
            }
        },

        onResize: function () {
            $('.parallaxitem').each(function () {
                var pCT = $(this).find('.parallax-center');
                var pBG = $(this).find('.parallax-background');
                var offset = ($(window).width() - 1024) / 2;
                if ($(window).width() < 1024) {
                    $('.parallax-scroll').css('margin-left', offset);
                    if (!pBG.hasClass('foreground')) {
                        pBG.css('margin-left', offset);
                    }
                    if (pBG.hasClass('foreground')) {
                        //pCT.find('.h2').css('margin-top', pBG.find('img').height() + 20);
                    }
                }
            });
        },

        onScroll: function () {
			/*var pItems = $('.parallaxitem');
			this.headSize = $('#sticky-header-nav').height();
			if($(window).scrollTop() >= pItems.eq(0).offset().top){
				var navTop = $(window).scrollTop() - pItems.eq(0).offset().top;
				$('.parallax-nav').css('top', navTop + pItems.eq(0).find('.parallax-background').height()/2);

				var endPoint = pItems.eq(pItems.length - 1).offset().top;
				endPoint 	+= pItems.eq(pItems.length - 1).find('.parallax-background').height() /2;
				if($('.parallax-nav').offset().top > endPoint){
					endPoint -= pItems.eq(0).offset().top;
					$('.parallax-nav').css('top', endPoint);
				}
			}else{
				$('.parallax-nav').css('top', $('.parallaxitem').eq(0).find('.parallax-background').height()/2);
			}

			if(this.curpos < $('.parallaxitem').length - 1){
				if($(window).scrollTop() >= $('.parallaxitem').eq(this.curpos+1).offset().top - this.headSize){
					this.curpos++;
					$('.parallax-radial').removeClass('selected');
					$('.parallax-radial').eq(this.curpos).addClass('selected');
				}
			}

			if($(window).scrollTop() < $('.parallaxitem').eq(this.curpos).offset().top - this.headSize){
				this.curpos--;
      			this.curpos = (this.curpos < 0)? 0: this.curpos;
				$('.parallax-radial').removeClass('selected');
				$('.parallax-radial').eq(this.curpos).addClass('selected');
			}*/
        }
    });

    MCWCM.ParallaxScroller = MCWCM.ComponentWrapperFactory('.parallaxframe', {}, ParallaxInstance);
})();
(function () {
    var ProductCarouselInstance = function ($root) {
        this.$root = $root;
        this.classBase = '.product-carousel__';
        this.$slickFrame = this.$root.find(this.classBase + 'frame');
        this.$navPrev = this.$root.find(this.classBase + 'prev');
        this.$navNext = this.$root.find(this.classBase + 'next');
        this.metaWrapper = this.classBase + 'slide-meta-wrapper';

        this.$root.imagesLoaded($.proxy(this.slickInit, this));
        $(window).on("windowresize", _.debounce($.proxy(this.slickResize, this), 200));
        this.bindEvents();
    };

    $.extend(ProductCarouselInstance.prototype, {
        slickInit: function () {
            this.$slickFrame.on('init', $.proxy(this.slickPostInit, this));
            this.$slickFrame.slick(this.getSlickOptions());
        },
        slickPostInit: function () {
            this.bindObjects();
            this.setClasses();
            this.setImageWrapperHeights();
            this.setDescription();
        },
        slickRotate: function () {
            this.bindObjects();
            this.setClasses();
            this.setSlickTitles(); //called from setImageWrapperHeights at init
            this.setDescription();
        },
        slickResize: function () {
            this.resetClasses();
            this.$slickFrame.on('init', $.proxy(this.slickPostInit, this));
            this.$slickFrame.slick('unslick');
            this.$root.imagesLoaded($.proxy(this.slickInit, this));
        },

        bindEvents: function () {
            this.$navPrev.on('click', $.proxy(this.onSlickPrev, this));
            this.$navNext.on('click', $.proxy(this.onSlickNext, this));

            // Add gesture support using hammerjs
            this.$root.hammer().on({
                'swiperight': $.proxy(this.onSlickPrev, this),
                'swipeleft': $.proxy(this.onSlickNext, this)
            });
        },
        bindObjects: function () {
            this.$slickSlides = this.$slickFrame.find(this.classBase + 'slide');
            this.$slickCenter = this.$slickFrame.find('.slick-center');
            this.$slickPrev = this.$slickCenter.prev();
            this.$slickNext = this.$slickCenter.next();
            this.$slickImages = this.$slickSlides.find(this.classBase + 'slide-image');
        },

        getSlickOptions: function () {
            var slideWidth = this.$root.width() / 3;
            var centerPadding = this.isMobile() ? (slideWidth / 4) * 3 : slideWidth;
            return {
                arrows: false,
                centerMode: true,
                centerPadding: centerPadding + 'px',
                draggable: false,
                infinite: true,
                slidesToShow: 1,
                speed: 0
            };
        },
        resetClasses: function () {
            this.$slickSlides.removeClass('active--left active--right');
        },
        setClasses: function () {
            this.$slickPrev.addClass('active--left');
            this.$slickNext.addClass('active--right');
        },
        setDescription: function () {
            // display current item's title, subtitle, and cta-group
            var description = this.$slickCenter.find(this.metaWrapper).html();
            this.$root.find(this.classBase + 'active-data').html(description);
        },
        setImageWrapperHeights: function () {
            this.imageWrapperHeight = MCWCM.Sizer.setHeights(this.$slickImages, { lineHeight: true, selector: 'img' });
            this.setSlickTitles();
        },
        setSlickTitles: function () {
            // reset top margins
            this.$slickSlides.find(this.metaWrapper).css({ marginTop: 0 });

            // set active slide title margins
            var titleMarginTop = this.calculateTitleMargin();

            this.$slickPrev.find(this.metaWrapper).css({ marginTop: titleMarginTop });
            this.$slickNext.find(this.metaWrapper).css({ marginTop: titleMarginTop });
        },

        calculateTitleMargin: function () {
            var prevHeight = this.$slickPrev.find('img').outerHeight();
            var nextHeight = this.$slickNext.find('img').outerHeight();
            var tallestImage = prevHeight >= nextHeight ? prevHeight : nextHeight;

            // difference of tallest image from image container, divided by two, made negative, with 20 extra
            return ((this.$slickImages.outerHeight() - tallestImage) / 2 * -1) + 10;
        },
        isMobile: function () {
            return this.$root.width() < 768 || $('html').hasClass('.touch')
        },

        onSlickPrev: function () {
            this.slickNav('prev');
        },
        onSlickNext: function () {
            this.slickNav('next');
        },
        slickNav: function (direction) {
            this.resetClasses();
            if (direction === 'prev') {
                this.$slickFrame.slick('slickPrev');
                this.$navPrev.blur();
            } else {
                this.$slickFrame.slick('slickNext');
                this.$navNext.blur();
            }
            this.slickRotate();
        }
    });

    var clickOnEnter = function (e) {
        if (e.which !== 13) {
            return false;
        }
        var $target = $(e.target);
        $target.click();
        $target.focus();
    };

    MCWCM.ProductCarousel = MCWCM.ComponentWrapperFactory('.product-carousel', {}, ProductCarouselInstance);
})();
(function () {
    var PromotionColumnsInstance = function ($root, options) {
        this.$root = $root;
        this.$promoSections = this.$root.find('.promotionrotating, .promopod');

        var $body = $('body');
        var modeClassNamePrefix = 'wcmmode-';
        var isEditMode = $body.hasClass(modeClassNamePrefix + 'edit');
        var isDesignMode = $body.hasClass(modeClassNamePrefix + 'design');
        var isPreviewMode = $body.hasClass(modeClassNamePrefix + 'preview');

        if (isEditMode || isDesignMode) {
            return;
        }

        if (this.$root.width() >= 768) {
            this.$root.imagesLoaded(_.debounce($.proxy(this.alignHeights, this), 200));
            $(window).on("windowresize", _.debounce($.proxy(this.alignHeights, this), 200));
        }

        $root.on('adjustHeight', $.proxy(this.alignHeights, this));
    };

    $.extend(PromotionColumnsInstance.prototype, {
        alignHeights: function () {
            var targetHeight = MCWCM.Sizer.setHeights(this.$promoSections, { reset: false, resize: false });
            this.$promoSections.each(function () {
                var promoHeight = $(this).outerHeight();
                if (targetHeight > promoHeight) {
                    var padding = (targetHeight - promoHeight) / 2;
                    if ($(this).attr('class').match('promopod')) {
                        var textHeight = targetHeight - $(this).find('.image').height();
                        $(this).find('.text').outerHeight(textHeight);
                    }
                    else if ($(this).attr('class').match('promotionrotating')) {
                        $(this).find('.promotion-rotating-inner')
                            .css({ paddingTop: padding, paddingBottom: padding + 80 });
                    }
                }
            });
        }
    });

    MCWCM.PromotionColumns = MCWCM.ComponentWrapperFactory('.promo-row', {}, PromotionColumnsInstance);
})();
(function () {
    $.extend($.roundaboutShapes, {
        promoPod: function (r, a, t) {
            var x = Math.sin(r + a),
                y = (Math.sin(r + 3 * Math.PI / 2 + a) / 8) * t * .6,
                z = (Math.cos(r + a) + 1) / 2,
                scale = (Math.sin(r + Math.PI / 2 + a) / 2) + 0.5;

            return {
                x: x,
                y: y,
                z: z,
                scale: scale
            };
        }
    })

    var PromotionRotatingInstance = function ($root, options) {
        var $roundaboutList = $root.find('.items');

        $roundaboutList.each(function () {
            $(this).find('img').css('margin-top', 0 - $(this).find('img').height() / 2);
        });

        var displayDescription = function () {
            var focusIndex = $roundaboutList.roundabout('getChildInFocus');
            var $elemInFocus = $roundaboutList.find('li').eq(focusIndex);
            var $description = $elemInFocus.find('.title').clone();
            $root.find('.active-description-container').html($description);
        }
        if (!$('.wcmmode-edit').length) {
            $root.imagesLoaded(function () {
                $roundaboutList.roundabout({
                    minOpacity: 1, //prevent opacity
                    minScale: 0.25,
                    tilt: -8, // make elements in background appear further up the page
                    responsive: true, // reinitialize roundabout when screen is resized
                    shape: 'promoPod'
                }, displayDescription);
            });
        }

        // Attach events for next and previous arrows
        $root.find('.prev').on('click', function () {
            $roundaboutList.roundabout('animateToPreviousChild');
            $root.find('.prev').blur();
        }).keyup(clickOnEnter);

        $root.find('.next').on('click', function () {
            $roundaboutList.roundabout('animateToNextChild');
            $root.find('.next').blur();
        }).keyup(clickOnEnter);

        // Add gesture support using hammerjs
        $root.hammer().on({
            'swipeleft': function (ev) {
                $roundaboutList.roundabout('animateToNextChild');
            },
            'swiperight': function (ev) {
                $roundaboutList.roundabout('animateToPreviousChild');
            }
        });

        $roundaboutList.on('animationEnd', function () {
            displayDescription();
            $root.trigger('adjustHeight');
        });
    };

    var clickOnEnter = function (e) {
        if (e.which !== 13) {
            return false;
        }
        var $target = $(e.target);
        $target.click();
    };

    MCWCM.PromotionRotating = MCWCM.ComponentWrapperFactory('.promotion-rotating-inner', {}, PromotionRotatingInstance);
})();
(function () {
    var RelatedContentScrollerInstance = function ($root, options) {
        this.$root = $root;
        this.$slickFrame = this.$root.find('.related-content-items');
        this.slideCount = this.$root.find('.related-content-item').length;
        this.$navPrev = this.$root.find('.prev');
        this.$navNext = this.$root.find('.next');
        this.$navs = this.$root.find('.prev, .next');
        this.options = options;

        this.isInitialized = false;

        if ($('body').find('.wcmmode-preview')) {
            // is there a back-end way to do this?
            this.$root.find('.new.section').remove();
        }

        this.slickInit();

        $(window).on("windowresize", _.debounce($.proxy(this.slickResize, this), 100));

        this.bindEvents();
    };

    $.extend(RelatedContentScrollerInstance.prototype, {
        slickInit: function () {
            this.isInitialized = true;
            this.$slickFrame.on('init', $.proxy(this.slickPostInit, this));
            this.$slickFrame.slick(this.getSlickOptions());
        },
        getSlickOptions: function () {
            var slideWidth = this.$slickFrame.width() / 3;
            var centerPadding = slideWidth;
            var initialSlide = 1;

            if (this.isMobile()) {
                centerPadding = (slideWidth / 4) * 3;
                initialSlide = 0;
            }
            if (this.isMobileMobile()) {
                this.$root.addClass('active');
            } else {
                this.$root.removeClass('active');
            }

            return {
                arrows: false,
                centerMode: true,
                centerPadding: centerPadding + 'px',
                draggable: false,
                infinite: true,
                initialSlide: initialSlide,
                rtl: this.options.rtl,
                slidesToShow: 1,
                speed: 600
            };
        },
        slickPostInit: function () {
            this.bindObjects();
            this.setHeights();
            this.toggleNavs();
            this.$slickFrame.css({ visibility: 'visible' });
            this.isInitialized = false;
        },
        slickResize: function () {
            if (this.$slickFrame.hasClass('slick-initialized') && !this.isInitialized) {
                this.$slickFrame.slick('setOption', 'centerPadding', this.getSlickOptions().centerPadding, true);
                this.setHeights();
                this.toggleNavs();
            } else {
                this.slickInit();
            }
        },

        setHeights: function () {
            this.titleHeight = MCWCM.Sizer.setHeights(this.$slickTitles);
            this.descHeight = MCWCM.Sizer.setHeights(this.$slickDescs);
        },
        toggleNavs: function () {
            if (this.slideCount <= 3 && !this.isMobile() || this.isMobileMobile()) {
                this.$navs.hide();
            } else {
                this.$navs.show();
            }
        },
        isMobile: function () {
            return this.$root.width() < 768 || $('html').hasClass('.touch');
        },
        isMobileMobile: function () {
            return this.$root.width() < 480;
        },

        bindEvents: function () {
            this.$navPrev.click($.proxy(this.onSlickPrev, this));
            this.$navNext.click($.proxy(this.onSlickNext, this));
        },
        bindObjects: function () {
            this.$slickSlides = this.$slickFrame.find('.related-content-item');
            this.$slickCenter = this.$slickFrame.find('.slick-center');
            this.$slickPrev = this.$slickCenter.prev();
            this.$slickNext = this.$slickCenter.next();
            this.$slickImages = this.$slickSlides.find('.imageWrapper');
            this.$slickTitles = this.$slickSlides.find('.title');
            this.$slickDescs = this.$slickSlides.find('.description');
        },

        onSlickPrev: function () {
            this.slickNav('prev');
        },
        onSlickNext: function () {
            this.slickNav('next');
        },
        slickNav: function (direction) {
            if (direction === 'prev') {
                this.$slickFrame.slick('slickPrev');
                this.$navPrev.blur();
            } else {
                this.$slickFrame.slick('slickNext');
                this.$navNext.blur();
            }
        }
    });

    MCWCM.RelatedContentScroller = MCWCM.ComponentWrapperFactory('.related-content-scroller', {}, RelatedContentScrollerInstance);
})();
(function ($, _, MCWCM) {
    var MCFormInstance = function ($root, options) {
        this.init($root, options);
    };

    $.extend(MCFormInstance.prototype, {
        init: function ($root, options) {
            this.$root = $root;
            $root.data('MCForm', this);
            this.$form = this.$root.find('form');
            this.$modal = this.$root.find('.modal');

            this.initRequiredSelects();
            this.validatr();
            this.bindForm();
        },
        initRequiredSelects: function () {
            var $selects = this.$form.find('select[required]');
            $selects.each(function () {
                $(this).find('option:first').attr({
                    selected: true,
                    disabled: true
                }).css('display', 'none');
            });
        },
        validatr: function () {
            var $form = this.$form;
            var $msg = this.$root.find('.mcform__validation');

            $form.validatr({
                theme: 'bootstrap',
                showall: true,
                position: function ($error, $input) {
                    var $wrapper = $input.parent();
                    var $field = $wrapper.closest('.mcform__field');
                    $wrapper.find('.validatr-message').remove();
                    $wrapper.prepend($error);
                    $field.addClass('validatr-error');
                }
            });

            var $fieldsToValidate = $form.validatr('getElements');
            $fieldsToValidate.wrap('<div class="mcform__input"></div>');

            $fieldsToValidate.on('valid', function (e) {
                var $wrapper = $(e.target).parent();
                var $field = $wrapper.closest('.mcform__field');
                $field.removeClass('validatr-error');
                $wrapper.find('.validatr-message').animate({
                    height: 0
                }, 200, function () {
                    $(this).remove();
                });
            });

            $fieldsToValidate.on('invalid', function (e) {
                $msg.show();
            });

            // wrap in debounce to avoid recursive callstack exceeded err
            $fieldsToValidate.on('valid', _.debounce(function (e) {
                if ($form.validatr('validateForm')) {
                    $msg.hide();
                }
            }, 200));
        },
        bindForm: function () {
            this.$form.on('submit', _.bind(this.onSubmit, this));
        },
        onSubmit: function (e) {
            var success = _.bind(this.success, this);
            var failure = _.bind(this.failure, this);
            e.preventDefault();
            if (this.validate()) {
                this.submit().done(success).fail(failure);
            }
        },
        validate: function () {
            var $invalid;
            var scrollDuration = 0;
            var isValid = this.$form.validatr('validateForm');
            if (!isValid) {
                $invalid = this.$form.find('.validatr-message').first();

                if (verge.viewportW() >= 768) {
                    scrollDuration = 250;
                }
                $.scrollTo($invalid, scrollDuration, {
                    offset: -1 * $('#sticky-header-nav').height() - 10,
                    onAfter: function () {
                        $invalid.closest('.mcform__field').find(':input').focus();
                    }
                });
            }
            return isValid;
        },
        submit: function () {
            var $form = this.$form;
            var endpoint = $form.attr('action');
            var method = $form.attr('method') || 'POST';

            return $form.ajaxSubmit({
                url: endpoint
            }).data('jqxhr');
        },
        success: function (result) {
            var $responseCode = $(result).text();
            if ($responseCode && $responseCode != 'success') {
                this.failure('', '', 'returned 200 with failure code');
                return;
            }

            var $modal = this.$modal;
            $modal.modal({
                backdrop: 'static'
            });

            this.$root.find('.error-banner').hide();
        },
        failure: function (xhr, textStatus, errorThrown) {
            // console.log('Submit failed', errorThrown);
            this.$root.find('.error-banner').show();
        }
    });

    MCWCM.MCForm = MCFormInstance;

})($, _, MCWCM);

(function ($, _, MCWCM) {

    var EmailFormInstance = function ($root, options) {
        this.init($root, options);
    }

    $.extend(EmailFormInstance.prototype, MCWCM.MCForm.prototype, {});

    MCWCM.EmailForm = MCWCM.ComponentWrapperFactory('.emailform', {}, EmailFormInstance);
})(jQuery, _, MCWCM);
(function ($, _, MCWCM) {

    var SalesForceInstance = function ($root, options) {
        this.init($root, options);
        if (location.hash == '#ThankYou') {
            this.success();
        }
    }

    $.extend(SalesForceInstance.prototype, MCWCM.MCForm.prototype, {
        onSubmit: function (e) {
            if (!this.validate()) {
                e.preventDefault();
            }
        },
        submit: function () {
        }
    });

    MCWCM.SalesForce = MCWCM.ComponentWrapperFactory('.salesforceform', {}, SalesForceInstance);
})(jQuery, _, MCWCM);
/* globals jQuery, _, MCWCM */

(function ($, _, MCWCM) {
    var TwitterBannerInstance = function ($root, options) {
        this.$root = $root;
        this.$slickFrame = this.$root.find('.tweets');
        this.$prev = $root.find('.prev');
        this.$next = $root.find('.next');
        this.$tweetContainer = $root.find('.tweets');

        // If banner was never configured all the way exit.
        if (this.$tweetContainer.length === 0) {
            return;
        }

        this.options = options;
        this.slider = null;
        this.tweetTemplate = _.template($root.find('.tweet-template').html());

        this.fetchTweets();
    };

    $.extend(TwitterBannerInstance.prototype, {
        slickInit: function () {
            this.$slickFrame.on('init', $.proxy(this.slickPostInit, this));
            this.$slickFrame.slick(this.getSlickOptions());
        },
        slickPostInit: function () {
            this.bindEvents();
            this.bindObjects();
        },
        bindEvents: function () {
            this.$prev.on('click', $.proxy(this.onSlickPrev, this));
            this.$next.on('click', $.proxy(this.onSlickNext, this));

            //Add gesture support using hammerjs
            this.$root.hammer().on({
                'swiperight': $.proxy(this.onSlickPrev, this),
                'swipeleft': $.proxy(this.onSlickNext, this)
            });
        },
        bindObjects: function () {
            this.$slickSlides = this.$slickFrame.find('.tweet');
            this.$slickImages = this.$slickFrame.find('.tweet');
            //Hide nav when less than max slides...
            if (this.$slickFrame.find(this.$slickSlides).length == 1) {
                this.$prev.hide();
                this.$next.hide();
            }
        },
        getSlickOptions: function () {
            var slideWidth = this.$root.width() / 3;
            return {
                arrows: false,
                draggable: false,
                infinite: true,
                rtl: this.options.rtl,
                slidesToShow: 1,
                speed: 600
            };
        },
        onSlickPrev: function () {
            this.slickNav('prev');
        },
        onSlickNext: function () {
            this.slickNav('next');
        },
        slickNav: function (direction) {
            if (direction == 'prev') {
                this.$slickFrame.slick('slickPrev');
                this.$prev.blur();
            }
            else {
                this.$slickFrame.slick('slickNext');
                this.$next.blur();
            }
        },

        fetchTweets: function () {
            var path = this.options['json-path'];
            if (path) {
                $.getJSON(path).done($.proxy(this.onFetchTweetsSuccess, this));
            }
        },

        onFetchTweetsSuccess: function (tweets) {
            var tweetTemplate = this.tweetTemplate;
            var tweetsHtml = _.map(tweets, function (tweet) {
                return tweetTemplate(tweet);
            });

            this.$tweetContainer.html(tweetsHtml.join(''));
            this.slickInit();
        }
    });

    MCWCM.TwitterBanner = MCWCM.ComponentWrapperFactory(
        '.twitter-banner',
        {
            'json-path': ''
        },
        TwitterBannerInstance
    );
})($, _, MCWCM);
/* globals jQuery, _, MCWCM */

(function ($, _, MCWCM) {
    var isTouchDevice = function () {
        return 'ontouchstart' in window
            || window.DocumentTouch
            && document instanceof DocumentTouch
    };

    var TabsInstance = function ($root, options) {
        this.$root = $root;
        this.$tabs = $root.find('.nav-tabs li');
        this.options = options;

        this.autoRotateId = null;

        this.$tabs.on('click', _.bind(this.onTabPressed, this));

        if (this.options.speed) {
            var speed = options.speed * 1000;
            this.autoRotateId = setInterval(_.bind(this.next, this), speed);
        }

        this.$tabs.toggleClass('allow-hover',
            !(isTouchDevice() &&
                ($(window).width() < 768)
                || (navigator.userAgent.match(/iPad/i) != null)));
    };

    $.extend(TabsInstance.prototype, {
        next: function () {
            var $current = this.$tabs.filter('.active');
            var $next = $current.next();
            if ($next.length === 0) {
                $next = this.$tabs.eq(0);
            }

            $next.find('> a').trigger('click', [true]);
        },
        onTabPressed: function (event, continueAutoRotate) {
            if (continueAutoRotate !== true) {
                clearInterval(this.autoRotateId);
            }

            var $target = $(event.target);
            var $currentTarget = $(event.currentTarget);
            // if only the li was clicked on and not the child anchor, make sure to transition to the tab.
            if ($target.is($currentTarget)) {
                $currentTarget.find(' > a').trigger('click', [continueAutoRotate]);
            }
        }
    });

    MCWCM.Tabs = MCWCM.ComponentWrapperFactory(
        '.tabs',
        {
            'speed': 0
        },
        TabsInstance
    );
})($, _, MCWCM);
/* globals jQuery, _, MCWCM */

(function ($, _, MCWCM) {
    var TabScrollerInstance = function ($root, options) {
        this.$root = $root;
        this.options = options;
        this.start = 0;

        this.$root.data('TabScroller', this);
        this.$root.hammer({
            drag: true,
            dragBlockHorizontal: true,
            dragLockMinDistance: 8
        });
        // prevent default webkit behaviour
        this.$root.find('a').css('-webkit-user-drag', 'none');
        this.onResize();
        $(window).resize('resize', _.throttle(_.bind(this.onResize, this)));

        //this.bindEvents();

        this.setDeepLink();
    };

    _.extend(TabScrollerInstance.prototype, {
        onDragStart: function (event) {
            this.start = this.$root.offset().left;
        },
        onDrag: function (event) {
            var gesture = event.gesture;
            var newPosition = gesture.deltaX + this.start
            this.$root.css(this.getPositionConfig(newPosition));
        },
        onDragEnd: function (event) {
            var gesture = event.gesture;
            var velocity = gesture.velocityX;
            var direction = (gesture.direction == 'left') ? -1 : 1;
            var newPosition = this.$root.offset().left + (velocity * this.options['kinetic-multiplier'] * direction);

            this.$root.animate(this.getPositionConfig(newPosition), this.options['kinetic-duration']);
            this.start = 0;
        },
        onResize: function () {
            var windowWidth = $(window).width();
            this.min = Math.min(-this.$root.width() + $(window).width(), 0);
            if (windowWidth < this.options['breakpoint']) {
                this.unbindEvents();
                this.bindEvents();
                this.$root.css(this.getPositionConfig(this.$root.offset().left));
            } else {
                this.$root.css(this.getPositionConfig(0));
                this.unbindEvents();
            }
        },
        bindEvents: function () {
            this.$root.on({
                dragstart: _.bind(this.onDragStart, this),
                dragleft: _.bind(this.onDrag, this),
                dragright: _.bind(this.onDrag, this),
                dragend: _.bind(this.onDragEnd, this)
            });
            this.$root.find('a[data-toggle="tab"]').on('shown.bs.tab', _.bind(this.onTabShown, this));
        },
        unbindEvents: function () {
            this.$root.off('dragstart dragleft dragright dragend');
            this.$root.find('a[data-toggle="tab"]').off('shown.bs.tab');
        },
        getPositionConfig: function (newPosition) {
            if (newPosition < this.min) {
                newPosition = this.min;
            } else if (newPosition > 0) {
                newPosition = 0;
            }
            return {
                left: newPosition
            }
        },
        onTabShown: function (event) {
            var tabScroller = this;
            var $target = $(event.target); // activated tab
            //event.relatedTarget // previous tab
            if (!$target.is('a')) {
                return;
            }

            var $tab = $target.closest('li');
            tabScroller.scrollToTab($tab);
        },
        scrollToTab: function ($tab, duration) {
            var tabWidth = $tab.width();
            var offset = $tab.offset();
            var windowWidth = $(window).width();

            if (!_.isNumber(duration)) {
                duration = this.options['kinetic-duration'];
            }

            //			if(offset.left < 0 || (offset.left + tabWidth > windowWidth)){
            var position = $tab.position();
            var newPosition = -(position.left - ((windowWidth - tabWidth) / 2));
            this.$root.animate(this.getPositionConfig(newPosition), duration, 'swing');
            //			}
        },

        setDeepLink: function () {
            // Rewards & Cash Back
            // #rewards-and-cash-back

            var deepLink = MCWCM.locationHref.split("#")[1];
            var $tabs = this.$root.find('.nav-tabs').find('> li');
            var tabsLength = $tabs.length - 1;
            $tabs.each(function (index) {
                var tabTitle = steralizeTitle($(this).find('a').html());
                if (tabTitle === deepLink) {
                    $(this).find('a').trigger('click');
                    return;
                }
            });

            function steralizeTitle(title) {
                // lowercase
                title = title.toLowerCase();
                // change &amp; or & to 'and'
                title = title.replace(/[&]amp[;]/g, 'and');
                // remove entities
                title = title.replace(/[&][a-zA-Z0-9]+[;]/g, '');
                //remove special characters
                title = title.replace(/[!$%^&*()_+|~=`{}[]:\/;<>?,.@#]/g, '');
                //change spaces to dashes
                title = title.replace(/[ ]/g, '-');

                return title;
            }
        }
    });

    MCWCM.TabScroller = MCWCM.ComponentWrapperFactory(
        '.nav-tab-wrapper',
        {
            'kinetic-multiplier': 50,
            'kinetic-duration': 400,
            'breakpoint': 768
        },
        TabScrollerInstance
    );
})($, _, MCWCM);
/* globals jQuery, MCWCM */

(function ($, MCWCM) {
    MCWCM.TitleTextLink = {
        init: function () {
            $(window).on('resize', _.debounce(MCWCM.TitleTextLink.resize, 100));
            setTimeout(MCWCM.TitleTextLink.resize, 10);	// wait a moment for fonts to redraw.
        },
        resize: function () {
            // clear out all set heights
            $('.title-text-link .text').css('height', 'auto');

            // for each row set equal height title text links
            $('.title-text-link').closest('.row').each(function () {
                var groups = [[]];
                var groupIndex = 0;
                var $row = $(this);
                $row.children().each(function () {
                    var $child = $(this);
                    if ($child.hasClass('clearfix')) {
                        //When a clearfix is encountered that is active create a new group
                        if ($child.css('display') !== 'none') {
                            groups.push([]);
                            groupIndex++;
                        }
                    } else if ($child.width() !== $row.width()) {
                        // column isnt full width so its a part of a group
                        groups[groupIndex].push($child.get(0));
                    }
                });
                // for each group find the tallest component and make all other components that height
                $.each(groups, function (index, group) {
                    var topHeight = 0;
                    var $group = $(group).find('.text');
                    //$group.css('height', 'auto');
                    $group.each(function () {
                        topHeight = Math.max(topHeight, $(this).height());
                    });
                    $group.height(topHeight);
                });
            });
        }
    };
})(jQuery, MCWCM);
(function () {

    var VideoMultipleInstance = function ($root, options) {
        this.$root = $root;
        this.options = options;

        this.$modal = this.$root.find('.modal');

        this.$video = this.$root.find('.modal .embedded-video');
        if (Modernizr.touch === true) {
            this.$video = this.$root.find('.fallback-embedded-video');
        }

        this.videoId = this.$root.find('.component-config').data('videoId');
        this.$videoPlaceholder = this.$root.find('.video-image');
        this.$videoFallbackContainer = this.$root.find('.fallback-embedded-video-container');
        this.videoActive = false;
        this.youTube1 = this.$root.find('.component-config').data('youTube1');
        this.vimeo1 = this.$root.find('.component-config').data('vimeo1');
        this.youku1 = this.$root.find('.component-config').data('youku1');
        this.youTube2 = this.$root.find('.component-config').data('youTube2');
        this.vimeo2 = this.$root.find('.component-config').data('vimeo2');
        this.youku2 = this.$root.find('.component-config').data('youku2');
        this.youTube3 = this.$root.find('.component-config').data('youTube3');
        this.vimeo3 = this.$root.find('.component-config').data('vimeo3');
        this.youku3 = this.$root.find('.component-config').data('youku3');
        this.vimeoVideoThumbNailId1 = this.$root.find('.component-config').data('vimeovideothumbnailid1');
        this.vimeoVideoThumbNailId2 = this.$root.find('.component-config').data('vimeovideothumbnailid2');
        this.vimeoVideoThumbNailId3 = this.$root.find('.component-config').data('vimeovideothumbnailid3');
        this.youkuVideoId = this.$root.find('.component-config').data('youkuId');

        if (!Modernizr.touch && swfobject.hasFlashPlayerVersion("1") && (this.youku1 === true || this.youku2 === true || this.youku3 === true)) {
            this.$modal.find('.modal-body').append("<iframe class='youkuIframe1' src='https://statics.youku.com/v1.0.0625/v/swf/loader.swf?VideoIDS=" + this.youkuVideoId + "==&isShowRelatedVideo=false' frameborder=0 allowfullscreen></iframe>");
        }
        this.$youkuIframe = this.$root.find('.youkuIframe1');

        if (this.vimeo1 === true || this.vimeo2 === true || this.vimeo3 === true) {
            if (this.vimeo1 === true && this.vimeoVideoThumbNailId1) {
                var imageDiv1 = $('.vimeo_thumb_wrapper1');
                fetchVimeoimage(this.vimeoVideoThumbNailId1, imageDiv1)
            }
            if (this.vimeo2 === true && this.vimeoVideoThumbNailId2) {
                var imageDiv2 = $('.vimeo_thumb_wrapper2');
                fetchVimeoimage(this.vimeoVideoThumbNailId2, imageDiv2)
            }
            if (this.vimeo3 === true && this.vimeoVideoThumbNailId3) {
                var imageDiv3 = $('.vimeo_thumb_wrapper3');
                fetchVimeoimage(this.vimeoVideoThumbNailId3, imageDiv3)
            }
            var vimeoIframeMultiGen = this.$root.find('.vimeoPlayerMulti');
            var vimeoIframe1 = this.$root.find('#vimeo1');
            var vimeoIframeMobile1 = this.$root.find('#vimeoMobile1');
            var vimeomModal = this.$root.find('.modal');
            //var vimeoVideoContent = this.$root.find('.video-single-content');
            var vimeovVideoPlaceholder = this.$root.find('.video-image');
            var vimeoVideoFallbackContainer = this.$root.find('.fallback-embedded-video-container');
            var vimeoClose = this.$root.find('.close-button-link');
            var vimeoPlay = this.$root.find('.video-button');
            var playerOrigin = '*';
            if (!Modernizr.touch) {
                vimeomModal.on('hide.bs.modal', _.bind(onPause, this));
            }
            // Listen for messages from the player
            if (window.addEventListener) {
                window.addEventListener('message', onMessageReceived, false);
            }
            else {
                window.attachEvent('onmessage', onMessageReceived, false);
            }

            //fetch vimeo thumbnail
            function fetchVimeoimage(vimeoId, imageDiv) {
                $.ajax({
                    type: 'GET',
                    url: 'https://vimeo.com/api/v2/video/' + vimeoId + '.json',
                    jsonp: 'callback',
                    dataType: 'jsonp',
                    success: function (data) {
                        var thumbnail_src = data[0].thumbnail_large;
                        appendVimeoImage(imageDiv, thumbnail_src)
                    }
                });
            }

            function appendVimeoImage(imageDiv, thumbnail_src) {
                imageDiv.each(function (index, item) {
                    if (item.innerHTML === '') {
                        imageDiv.append('<img src="' + thumbnail_src + '"/>');
                    }
                    return false;
                });
            }

            // Handle messages received from the player
            function onMessageReceived(event) {
                // Handle messages from the vimeo player only
                if (!(/^https?:\/\/player.vimeo.com/).test(event.origin)) {
                    return false;
                }

                if (playerOrigin === '*') {
                    playerOrigin = event.origin;
                }

                var data = JSON.parse(event.data);

                switch (data.event) {
                    case 'ready':
                        onReady();
                        break;

                    case 'finish':
                        onFinish();
                        break;
                }
            }

            // Helper function for sending a message to the player
            function post(action, value) {
                var data = {
                    method: action
                };

                if (value) {
                    data.value = value;
                }

                var message = JSON.stringify(data);
                if (!Modernizr.touch) {
                    vimeoIframe1[0].contentWindow.postMessage(message, playerOrigin);
                } else if (Modernizr.touch) {
                    vimeoIframeMobile1[0].contentWindow.postMessage(message, playerOrigin);
                }
            }

            function onReady() {
                post('addEventListener', 'finish');
            }

            function onFinish() {
                if (!Modernizr.touch) {
                    vimeomModal.modal('hide').removeClass('active');
                    // $('.video-single-content').css('display', 'none');
                }
                if (Modernizr.touch) {
                    //  vimeoVideoContent.hide();
                    vimeovVideoPlaceholder.addClass('active');
                    //  vimeovVideoPlaceholder.addClass('active');
                    vimeoVideoFallbackContainer.removeClass('active');
                }
                vimeoIframeMultiGen[0].src = vimeoIframeMultiGen[0].src;
            }

            function onPause() {
                post('unload');
            }

            if (!Modernizr.touch) {
                vimeoClose.on('click', function (e) {
                    post('unload');
                });
            }

            vimeoPlay.on('click', function (e) {
                post('play');
            });

        }

        if (this.youTube1 === true || this.youTube2 === true || this.youTube3 === true) {
            var youtubeProperties = {
                'videoId': this.videoId,
                'playerVars': {
                    'origin': window.location.origin,
                    'rel': 0,
                    'enablejsapi': 1,
                    'controls': 2,
                    'html5': 1,
                    'autoplay': 0
                },
                'events': {
                    'onStateChange': _.bind(this.onPlayerStateChange, this),
                }
            };

            this.videoPlayer = new YT.Player(this.$video.get(0), youtubeProperties);
        }
        this.bindEvents();
    };

    $.extend(VideoMultipleInstance.prototype, {
        bindEvents: function () {
            $(MCWCM.VideoMultiple).bind('video-multiple-pause', _.bind(this.pauseVideo, this));
            if (this.youTube1 === true || this.youTube2 === true || this.youTube3 === true || this.youku1 === true || this.youku2 === true || this.youku3 === true) {
                this.$modal.on('hide.bs.modal', _.bind(this.pauseVideo, this));
            }
            this.$root.find('.video-button').click(_.bind(function () {
                if (this.youku1 === true || this.youku2 === true || this.youku3 === true) {
                    if (Modernizr.touch || (!Modernizr.touch && !swfobject.hasFlashPlayerVersion("1"))) {
                        window.open("http://player.youku.com/embed/" + this.youkuVideoId);
                    } else {
                        this.playVideo();
                    }
                } else {
                    this.playVideo();
                }
            }, this)).keyup(clickOnEnter);

            $(Window).on('resize', _.debounce(this.resize, 200));
            var view = this;
            this.$root.find('.close-button-link').on('click', function (e) {
                e.preventDefault();
                view.endVideo();
            })
        },
        playVideo: function (event) {

            if (!Modernizr.touch) {
                $(MCWCM.VideoMultiple).trigger('video-multiple-pause');
                this.$modal.modal('show').addClass('active');
                if (this.youTube1 === true || this.youTube2 === true || this.youTube3 === true) {
                    if (this.videoPlayer.seekTo) {
                        this.videoPlayer.seekTo(0);
                    }

                    if (this.videoPlayer.playVideo) {
                        this.videoPlayer.playVideo();
                    }
                }
            } else {
                //this.$modal.modal('show').addClass('active');
                this.$videoPlaceholder.removeClass('active');
                this.$videoFallbackContainer.addClass('active');
                if (this.youTube1 === true || this.youTube2 === true || this.youTube3 === true) {
                    if (this.videoPlayer.playVideo) {
                        setTimeout(function () {
                            this.videoPlayer.playVideo();
                        }.bind(this), 500)
                    }
                }
            }
            this.resize();
        },

        pauseVideo: function (event, $component) {
            if (this.youTube1 === true || this.youTube2 === true || this.youTube3 === true) {
                if (this.videoPlayer.pauseVideo) {
                    this.videoPlayer.pauseVideo();
                }
            } else if (!Modernizr.touch && (this.youku1 === true || this.youku2 === true || this.youku3 === true) && event.type === 'hide') {
                this.$youkuIframe[0].src = this.$youkuIframe[0].src;
            }
        },

        onPlayerStateChange: function (event) {
            if (event.data == YT.PlayerState.ENDED) {
                this.endVideo();
            }
        },

        endVideo: function () {
            this.videoActive = false;
            if (!Modernizr.touch) {
                this.$modal.modal('hide').removeClass('active');
                //$('.video-single-content').css('display', 'none');
            }
            else {
                //this.$videoContent.hide();
                this.$videoPlaceholder.show();
                this.resize();
            }
        },
        resize: function () {
            var dialog = $('.videos .video-item .modal.in > .modal-dialog');
            var content = dialog.find('.modal-content');
            var margin = ((verge.viewportH() - content.outerHeight()) / 2.0) + 'px';

            dialog.css('margin-top', margin)
            dialog.css('margin-bottom', margin)
        }
    });

    var clickOnEnter = function (e) {
        if (e.which !== 13) {
            return false;
        }
        var $target = $(e.target);
        $target.click();
    };

    MCWCM.VideoMultiple = MCWCM.ComponentWrapperFactory('.videomultiple .video-item', {}, VideoMultipleInstance);
})();

(function () {
    'use strict';

    var VideoSingleInstance = function ($root, options) {
        this.$root = $root;
        $root.data('VideoSingle', this);
        this.options = options;

        this.$modal = this.$root.find('.modal');
        this.$videoPlaceholder = this.$root.find('.resp-video');
        this.$videoContent = this.$root.find('.video-single-content');
        this.$textWrapper = this.$root.find('.text-wrapper');
        this.videoId = this.$root.find('.component-config').data('videoId');
        this.$videoIframe = this.$videoContent.find('.embedded-video');
        this.youTube = this.$root.find('.component-config').data('youTube');
        this.vimeo = this.$root.find('.component-config').data('vimeo');
        this.youku = this.$root.find('.component-config').data('youku');
        this.$playerOrigin = '*';

        if (this.youku === true && !Modernizr.touch && swfobject.hasFlashPlayerVersion("1")) {
            this.$modal.find('.modal-body').append("<iframe class='youkuIframe' src='https://statics.youku.com/v1.0.0625/v/swf/loader.swf?VideoIDS=" + this.videoId + "==&isShowRelatedVideo=false' frameborder=0 allowfullscreen></iframe>");
        }
        this.$youkuIframe = this.$videoContent.find('.youkuIframe');

        if (Modernizr.touch === true) {
            this.$videoIframe = this.$videoContent.find('.fallback-embedded-video');
        }
        this.$playButton = this.$videoPlaceholder.find('.video-button-container');
        this.$videoFallbackContainer = this.$root.find('.fallback-embedded-video-container');
        this.videoActive = false;

        if (this.vimeo === true) {
            var vimeoIframeGen = this.$videoContent.find('.vimeoPlayer');
            var vimeoIframe = this.$videoContent.find('#vimeo');
            var vimeoIframeMobile = this.$videoContent.find('#vimeoMobile');
            var vimeomModal = this.$root.find('.modal');
            var vimeoVideoContent = this.$root.find('.video-single-content');
            var vimeovVideoPlaceholder = this.$root.find('.resp-video');
            var vimeoVideoFallbackContainer = this.$root.find('.fallback-embedded-video-container');
            var vimeoClose = this.$root.find('.close-button-link');
            var vimeoPlay = this.$root.find('.video-button');
            var playerOrigin = '*';
            if (!Modernizr.touch) {
                vimeomModal.on('hide.bs.modal', _.bind(onPause, this));
            }
            // Listen for messages from the player
            if (window.addEventListener) {
                window.addEventListener('message', onMessageReceived, false);
            } else {
                window.attachEvent('onmessage', onMessageReceived, false);
            }

            // Handle messages received from the player
            function onMessageReceived(event) {
                // Handle messages from the vimeo player only
                if (!(/^https?:\/\/player.vimeo.com/).test(event.origin)) {
                    return false;
                }

                if (playerOrigin === '*') {
                    playerOrigin = event.origin;
                }

                var data = JSON.parse(event.data);

                switch (data.event) {
                    case 'ready':
                        onReady();
                        break;

                    case 'finish':
                        onFinish();
                        break;
                }
            }

            // Helper function for sending a message to the player
            function post(action, value) {
                var data = {
                    method: action
                };

                if (value) {
                    data.value = value;
                }

                var message = JSON.stringify(data);
                if (!Modernizr.touch) {
                    vimeoIframe[0].contentWindow.postMessage(message, playerOrigin);
                } else if (Modernizr.touch) {
                    vimeoIframeMobile[0].contentWindow.postMessage(message, playerOrigin);
                }
            }

            function onReady() {
                post('addEventListener', 'finish');
            }

            function onFinish() {
                if (!Modernizr.touch) {
                    vimeomModal.modal('hide').removeClass('active');
                    //$('.video-single-content').css('display', 'none');
                    vimeoVideoContent.hide();

                } else {
                    vimeoVideoContent.hide();
                    vimeovVideoPlaceholder.show();
                }
                vimeoIframeGen[0].src = vimeoIframeGen[0].src;
            }

            function onPause() {
                //$('.video-single-content').css('display', 'none');
                vimeoVideoContent.hide();
                post('unload');
            }

            if (!Modernizr.touch) {
                vimeoClose.on('click', function (e) {
                    post('unload');
                });
            }

            vimeoPlay.on('click', function (e) {
                post('play');
            });
        }

        if (this.youTube === true) {
            var youtubeProperties = {
                'videoId': this.videoId,
                'playerVars': {
                    'autohide': 1,
                    'autoplay': 0,
                    'enablejsapi': 1,
                    'controls': 2,
                    'html5': 1,
                    'origin': window.location.origin,
                    'rel': 0,
                    'showInfo': 0
                },
                'events': {
                    'onStateChange': $.proxy(this.onPlayerStateChange, this)
                }
            };

            this.videoPlayer = new YT.Player(this.$videoIframe.get(0), youtubeProperties);
            this.$root.imagesLoaded($.proxy(this.bindEvents, this));
            $(window).on("windowresize", _.debounce($.proxy(this.onResize, this), 200));
        }
        this.bindEvents();
        this.$root.imagesLoaded($.proxy(this.bindEvents, this));
    };

    $.extend(VideoSingleInstance.prototype, {

        onResize: function () {
            this.setHeight();
        },

        bindEvents: function () {
            // SET HEIGHT
            this.setHeight();

            $(MCWCM.VideoSingle).bind('video-single-pause', _.bind(this.pauseVideo, this));
            if (this.youTube === true || this.youku === true) {
                this.$modal.on('hide.bs.modal', _.bind(this.pauseVideo, this));
            }
            var bindVideoButton = this.$root.find('.video-button');
            if (this.youku === true) {
                bindVideoButton = this.$root.find('.video-button').unbind('click');
            }
            bindVideoButton.click(_.bind(function (e) {
                e.stopPropagation();
                this.videoActive = true;
                if (!Modernizr.touch) {
                    if (this.youku === true && !swfobject.hasFlashPlayerVersion("1")) {
                        window.open("http://player.youku.com/embed/" + this.videoId);
                    } else {
                        this.playVideo();
                    }
                } else {
                    if (this.youku === true) {
                        //$('.video-single-content').css('display', 'none');
                        this.$videoContent.hide();
                        window.open("http://player.youku.com/embed/" + this.videoId);
                    } else {
                        this.$root.find('.resp-video').hide();
                        this.playVideo();
                    }
                }
            }, this)).keyup(clickOnEnter);

            var view = this;
            this.$root.find('.close-button-link').on('click', function (e) {
                e.preventDefault();
                view.onVideoEnd();
                //$('.video-single-content').css('display', 'none');
                //this.$videoContent.hide();
            })
        },

        playVideo: function () {
            //this.$videoContent.show();

            if (!Modernizr.touch) {
                this.$videoContent.css('display', 'inline');
                $(MCWCM.VideoSingle).trigger('video-single-pause');
                this.$modal.modal('show').addClass('active');
                if (this.youTube === true) {
                    if (this.videoPlayer.seekTo) {
                        this.videoPlayer.seekTo(0);
                    }
                    if (this.videoPlayer.playVideo) {
                        this.videoPlayer.playVideo();
                    }
                }
            } else {
                this.$videoContent.show();
                this.$videoPlaceholder.removeClass('active');
                this.$videoFallbackContainer.addClass('active');
            }
            this.resize();
        },

        pauseVideo: function (event, $component) {
            if (event.type === 'hide') {
                this.$videoContent.hide();
            }
            var timer = this.timer;
            clearTimeout(timer);
            if (!this.$root.is($component)) {
                if (this.youTube === true) {
                    this.videoPlayer.pauseVideo();
                } else if (!Modernizr.touch && this.youku === true && event.type === 'hide') {
                    this.$youkuIframe[0].src = this.$youkuIframe[0].src;
                }
            }
        },

        onPlayerStateChange: function (event) {
            if (event.data == YT.PlayerState.ENDED) {
                this.onVideoEnd();
            }
        },

        onVideoEnd: function () {
            this.videoActive = false;
            if (!Modernizr.touch) {
                this.$modal.modal('hide').removeClass('active');
                this.$videoContent.hide();
            } else {
                this.$videoContent.hide();
                this.$videoPlaceholder.show();
                this.resize();
            }
        },

        resize: function () {
            // SET HEIGHT
            this.setHeight();
            var dialog = $('.js-singleVideo-container .modal.in > .modal-dialog'),
                content = dialog.find('.modal-content'),
                margin = ((verge.viewportH() - content.outerHeight()) / 2.0) + 'px';
            dialog.css('margin-top', margin)
            dialog.css('margin-bottom', margin)
        },

        fitWrapper: function (e) {
            this.setHeight();

            var modifier = this.options['wrapper-modifier'],
                contentSelector = this.options['content-selector'],
                imageSelector = this.options['image-selector'];

            this.$root.each(function () {
                var $component = $(this),
                    $text = $component.find(contentSelector),
                    $img = $component.find(imageSelector);
                if ($component.hasClass(modifier)) {
                    if ($img.width() < $component.width()) {
                        $component.removeClass(modifier);
                    }
                } else {
                    if ($img.outerHeight() < $component.outerHeight()) {
                        $component.addClass(modifier);
                    }
                }
            });

            if (!e || !e.silent) {
                this.$root.trigger('fitWrapper');
            }
        },

        setHeight: function () {
            var height = 0;

            if (this.videoActive) {
                this.$videoPlaceholder.removeClass('sizeContent');

                var viewport_height = verge.viewportH() - $('#sticky-header-nav').outerHeight(),
                    video_height = $('.video-single-content').outerHeight();

                height = video_height;
                height = 'auto';
                this.$root.outerHeight(height);
            }
            else {
                var frameWidth = this.$root.outerWidth(),
                    $img = this.$videoPlaceholder.find('img');

                $img.outerHeight('auto').width('auto');
                $img.width(frameWidth);

                var sizerSelector = this.$videoPlaceholder;

                sizerSelector = $('.text-wrapper, .js-singleVideo img');

                var frameHeight = MCWCM.Sizer.setHeights(this.$videoPlaceholder, {
                    selector: sizerSelector
                });

                this.$root.outerHeight(frameHeight);

                this.$videoPlaceholder.removeClass('sizeContent');

                var h = $img.outerHeight();
                if (h < frameHeight) {
                    this.$videoPlaceholder.addClass('sizeContent');
                    $img.width('auto');
                    $img.outerHeight(frameHeight);
                }

                this.$textWrapper.css({
                    marginTop: "70px"
                });

                var $wrapper = $(this).find('.resp-video > .full');

                function centerImage($img, frameWidth) {
                    // Manage image to the center of canvas
                    var marginLeft = ($img.width() - frameWidth) / -2;
                    $img.css({
                        marginLeft: marginLeft
                    });
                }

                //Manage the image height
                $img.width('auto');
                $img.height(frameHeight);
                centerImage($img, frameWidth);
            }
        }
    });

    var clickOnEnter = function (e) {
        if (e.which !== 13) {
            return false;
        }
        var $target = $(e.target);
        $target.click();
    };

    MCWCM.VideoSingle = MCWCM.ComponentWrapperFactory('.js-singleVideo-container', {
        "wrapper-modifier": "sizeContent",
        "content-selector": ".js-singleVideo",
        "image-selector": ".image img"
    }, VideoSingleInstance);
})();
// Grid options
function gridOptions(options) {
    var default_config, settings;

    default_config = {
        viewcount: 0,
        startIndex: 0,
        handleSorting: false, // Flags to handle sorting in front end
        sortType: ''
    };

    settings = $.extend(default_config, options);

    this.container = settings.container;
    this.itemscontainer = settings.itemholder;
    this.template = settings.template;
    this.jsonData = settings.dataObject;

    // View more inits
    this.moreCount = settings.viewcount;
    this.moreBtn = '#' + this.container + ' .viewmore';

    this.sortBtn = '#' + this.container + ' .gridsorting';
    this.defaultSort = settings.defaultSort;

    // Filter inits
    this.filtercontainer = settings.filterContainer;
    this.applyBtn = '.' + this.filtercontainer + ' .applyFilter';

    this.startIndex = settings.startIndex;
    this.itemsinpage = settings.itemsingrid;

    // Flags to handle sorting in front end
    this.frontendSort = settings.handleSorting;
    this.sortType = settings.sortType;
    this.fullResponse;
    this.sortingTypes = [];
}

(function ($) {

    var listingDefault = {
        pageType: $('#pageType').val(),
        filterApplied: false,
        sortingApplied: false,
        replacehtml: false,
        scrollpos: 0,
        filterQuery: '',
        productType: ''
    }

    $.fn.renderListing = function (options) {
        return this.each(function () {
            var instance = new gridOptions(options);
            $(this).data("gridreference", instance);
            instance.init();
        });
    };

    gridOptions.prototype = {

        // Make ajax call
        makeEventsAPI: function (url) {
            var _this = this, jsonObj;
            // Flag frontend sort - if true and has response, don't make new ajax call
            if (_this.frontendSort) {
                if (typeof _this.fullResponse != 'undefined' && _this.fullResponse != '') {
                    _this.sortingHandler();
                    return;
                }
            }
            $.ajax({
                url: url
            }).done(function (response) {
                jsonObj = $.parseJSON(response);
                if (jsonObj != null && !($.isEmptyObject(jsonObj))) {
                    _this.noresults(false);
                    _this.jsonData = jsonObj;
                    // Flag front end - if true sort before pasting data 
                    if (_this.frontendSort) {
                        _this.fullResponse = jsonObj;
                        _this.sortingHandler();
                    } else {
                        _this.gridRendition(_this.jsonData.data);
                    }
                } else {
                    _this.noresults(true);
                }

            }).error(function () {
                console.log('Error while ajax: GSA request failed')
            });
        },

        // Append html to respective container
        applyHtml: function (html) {
            // Updating title in product listing page
            if ($('#productType').val() == 'productlisting') {
                if (listingDefault.productType != '') {
                    if (listingDefault.productType.toLowerCase() == 'all') {
                        $('.prod_title').empty().text($('.prod_title').attr('data-default'));
                    } else {
                        $('.prod_title').empty().text(listingDefault.productType);
                    }
                    $('.prod_title').removeClass('hide');
                }
            }

            var filterApplied = listingDefault.filterApplied;
            if (filterApplied || listingDefault.replacehtml) {
                $('#' + this.container + ' .' + this.itemscontainer).empty().html(html);
                listingDefault.replacehtml = listingDefault.sortingApplied = listingDefault.filterApplied = false;
            } else {
                $('#' + this.container + ' .' + this.itemscontainer).append(html);
            }
            if (listingDefault.scrollpos != 0) {
                this.bodyscroll(listingDefault.scrollpos);
            }
            this.updateresult();
            $('.' + this.itemscontainer + ' .animate').animate({ opacity: 1 }, 300);
        },

        // Preparing sorting query
        sortingQuery: function (defaultSort) {
            var sortApplied = $(this.sortBtn + '.applied'),
                joinArray,
                i = 0,
                tempArray = [],
                orderby;

            if (sortApplied.length) {
                $.each(sortApplied, function () {
                    if ($(this).hasClass('asc')) {
                        orderby = 'A';
                        if ($(this).attr('data-sort') == 'dt') {
                            orderby = 'D';
                        }
                    } else {
                        orderby = 'D';
                        if ($(this).attr('data-sort') == 'dt') {
                            orderby = 'A';
                        }
                    }
                    // For date GSA format "date:D:R:d1", rest "loc:A"
                    if ($(this).attr('data-sort') == 'dt') {
                        tempArray[i] = 'date' + ':' + orderby + ':R:d1';
                    } else {
                        tempArray[i] = 'meta:' + $(this).attr('data-sort') + ':' + orderby;
                    }
                    //tempArray[i] = $(this).attr('data-sort')+':'+orderby;
                    i++;
                });
                /* Commenting multiple sorting option, using the recent sorted value */
                //joinArray = "meta:"+tempArray.join('.');
                joinArray = tempArray[0];
            } else if (defaultSort) { // Default sorting
                switch (defaultSort) {
                    case 'date':
                        joinArray = "date:D:R:d1";
                        break;
                    case 'title':
                        joinArray = "meta:title:A";
                        break;
                }

            }
            return joinArray;
        },

        // Preparing URI for GSA api
        prepareQuery: function (pageType) {
            var clientid = $('#gsa_client').val(),
                siteid = $('#gsa_site').val(),
                basegsaurl = $('#gsaurl').val() || '/search',
                currentyear = $('#defaultyear').val() || (new Date().getFullYear()),
                defaultdtrange = $('#defaultdtrange ').val() || undefined,
                selectInputs = $('.' + this.filtercontainer).find('select');

            var sortQuery = '',
                daterange = false,
                selectedvalues = selectid = hasValue = dateQuery = filterQuries = '',
                nextdataset = '', queryStr, startIndex, requiredFields = numofitems = '', jsonfields;

            var gsa_basics = 'client=' + clientid + '&proxystylesheet=' + clientid + '&site=' + siteid;

            function daysInMonth(month, year) {
                return new Date(year, month, 0).getDate();
            }

            if ($('.' + this.filtercontainer + ' #year').length || $('.' + this.filtercontainer + ' #month').length) {
                daterange = true;
            }

            // Fields required from JSON
            switch (pageType) {
                case 'events':
                    jsonfields = '&getfields=title.type.img.imgalt.etype.roles.itype.dt.disdt.loc.exturl';
                    break;
                case 'products':
                    jsonfields = '&getfields=title.type.img.imgalt.ssnm.desc.roles';
                    break;
                case 'articles':
                case 'blog':
                    jsonfields = '&getfields=title.roles.img.imgalt.disdt.analystDisplay.exturl';
                    break;
            }

            // Filter quries
            if (listingDefault.filterQuery == '') {
                if (listingDefault.filterApplied == true) {
                    $.each(selectInputs, function () {
                        hasValue = $(this).val();
                        selectid = $(this).attr('id');
                        if (hasValue && hasValue.toLowerCase() != 'all' && selectid != 'year' && selectid != 'month') {
                            filterQuries += ('.' + selectid + ':' + encodeURIComponent($(this).val().replace('.', '%2E')));
                        }
                    });
                }
                // Event type and filter queries if any
                listingDefault.filterQuery = '&requiredfields=type:' + pageType + filterQuries;
            }

            var requiredFields = listingDefault.filterQuery;

            // Date selections
            if (daterange) {
                var selectedyear = ($('#year').val() || '').toLowerCase(),
                    selectedMonth = ($('#month').val() || '').toLowerCase();

                if (selectedyear != '' && selectedMonth != '') {
                    if (selectedMonth == 'all') {
                        dateQuery = (selectedyear + '-01-01') + '..' + (selectedyear + '-12-31');
                    } else {
                        dateQuery = (selectedyear + '-' + selectedMonth + '-01') + '..' + (selectedyear + '-' + selectedMonth + '-' + daysInMonth(selectedMonth, selectedyear));
                    }
                } else if (selectedyear != '' && selectedMonth == '') {
                    dateQuery = (selectedyear + '-01-01') + '..' + (selectedyear + '-12-31');;
                } else if (selectedyear == '' && selectedMonth != '') {
                    if (selectedMonth == 'all') {
                        dateQuery = (currentyear + '-01-01') + '..' + (currentyear + '-12-31');
                    } else {
                        dateQuery = (currentyear + '-' + selectedMonth + '-01') + '..' + (currentyear + '-' + selectedMonth + '-' + daysInMonth(selectedMonth, currentyear));
                    }
                    //var lastValue = $('#year option:last-child').val() || (new Date().getFullYear() + 3);
                    //var firstValue = $('#year option:first-child').val() || (new Date().getFullYear() - 3);
                    //dateQuery = (firstValue + '-' + selectedMonth + '01') + '..' + (lastValue + '-' + selectedMonth + '31');
                    $('#year').val(currentyear).trigger('change');
                } else {
                    //if(!listingDefault.filterApplied) {
                    dateQuery = defaultdtrange;
                    //}
                }

                if (dateQuery != '' && typeof dateQuery != "undefined") {
                    dateQuery = '&q=inmeta:dt:daterange:' + dateQuery;
                } else {
                    dateQuery = '';
                }
            }

            // starting index and no of items to request
            if (typeof this.itemsinpage != "undefined") {
                numofitems = this.itemsinpage;
                startIndex = (this.itemsinpage * this.moreCount);
                if (listingDefault.sortingApplied) {
                    numofitems = (this.itemsinpage * (this.moreCount + 1));
                    startIndex = 0;
                }
            } else {
                startIndex = 0;
            }

            // Handling sorting
            if (!this.frontendSort) {
                if ($(this.sortBtn + '.applied').length > 0) {
                    sortQuery = '&sort=' + this.sortingQuery();
                } else if (this.defaultSort) {
                    sortQuery = '&sort=' + this.sortingQuery(this.defaultSort);
                }
            }

            // Flag for frontend sort - if true we will be requesting for the entire data set
            if (!this.frontendSort) {
                nextdataset = '&start=' + startIndex + ((numofitems != '') ? "&num=" + numofitems : "");
            } else {
                nextdataset = ('&start=' + 0 + '&num=' + listcount);
            }

            queryStr = basegsaurl + '?' + gsa_basics + '&output=xml_no_dtd&oe=UTF-8&ie=UTF-8&filter=0' + jsonfields + requiredFields + nextdataset + sortQuery + dateQuery;

            return queryStr;
        },

        // Handling user actions, view more, applyfilter, etc.
        handleActions: function () {
            var _this = this, query;
            // view more click
            $(_this.moreBtn).on('click', function () {
                $(this).addClass('hide');
                $('#' + _this.container + ' .proIndicator').removeClass('hide');
                listingDefault.scrollpos = $('#' + _this.container).offset().top + ($('#' + _this.container + ' .gridcontent').outerHeight() - 100); // 100 for adjusting the pos, since menu is positioned absolute.
                _this.moreCount++;

                // Checks for frontend sorting
                _this.clientsideSort(_this.sortType);

                query = _this.prepareQuery(listingDefault.pageType);
                _this.makeEventsAPI(query);
            });

            // Handling filter apply
            $(_this.applyBtn).on('click', function () {
                // Handling product landing page, if user clicks apply filter redirect to product listing
                var prodLanding = productlistpage = false, prodListQuery = '', listURL, dataVal;
                if ($('#productType').length) {
                    if ($('#productType').val() == 'productlanding') {
                        prodLanding = true;
                        listUrl = $('#prodListPath').val() || '/content/advisors/en-us/products-group-listing/products';
                    } else if ($('#productType').val() == 'productlisting') {
                        productlistpage = true;
                        if ($('#roles').val()) {
                            listingDefault.productType = $('#roles').val();
                        } else {
                            listingDefault.productType = $('.prod_title').attr('data-default');
                        }
                    }
                }

                // Refresh/reload page if no selection made
                var selectmade = 0, selectInputs = $('.' + _this.filtercontainer).find('select'), tempArray = [];
                $.each(selectInputs, function () {
                    hasValue = $(this).val();
                    if (hasValue) {
                        selectmade++;
                    }
                    if (prodLanding) {
                        dataVal = ($('option:selected', this).attr('data-value') || '').toLowerCase();
                        if (dataVal != '' && dataVal != 'all') {
                            tempArray[selectmade] = dataVal;
                        }
                    }
                });

                prodListQuery = tempArray.join('.');

                // Product landing - when apply filter redirect to product listing page with selected values as selectors
                if (prodLanding) {
                    var redirectUrl = listUrl + prodListQuery + '.html';
                    window.top.location.href = redirectUrl;
                } else {
                    if (selectmade < 1) {
                        if (productlistpage) {
                            var producturl = $('#prodListPath').val();
                            window.top.location.href = producturl + '.html';
                        } else {
                            location.reload();
                        }
                    }
                }

                listingDefault.filterQuery = '';
                listingDefault.scrollpos = $('#' + _this.container).offset().top;
                //$(this).attr('disabled','disabled');
                listingDefault.filterApplied = true, _this.moreCount = 0;
                $('#' + _this.container + ' .nomore').addClass('hide');

                //reset sorting
                var appliedsort = $('#' + _this.container + ' .applied');
                $.each($(_this.sortBtn), function () {
                    $(this).removeClass('applied').removeClass('asc').removeClass('dsc');
                    if ($(this).hasClass('default')) {
                        $(this).addClass('applied').addClass('asc');
                    }
                });
                listingDefault.sortingApplied = false;

                // Enable/disable front end sort and clearing the previous responses if any
                _this.clientsideSort();
                _this.fullResponse = '';

                query = _this.prepareQuery(listingDefault.pageType);
                _this.makeEventsAPI(query);
            });

            // Handling sorting
            $(_this.sortBtn).on('click', function () {
                // Removing previous sorted values
                $(_this.sortBtn).not(this).removeClass('applied').removeClass('asc').removeClass('dsc');

                listingDefault.scrollpos = $('#' + _this.container).offset().top;
                $(this).addClass('applied');
                //var sortingType = $(this).attr('data-sort');

                // Checks for frontend sorting
                _this.sortType = $(this).attr('data-sort');
                _this.clientsideSort(_this.sortType);

                if ($(this).hasClass('asc')) {
                    $(this).removeClass('asc').addClass('dsc');
                } else {
                    $(this).removeClass('dsc').addClass('asc');
                }
                listingDefault.sortingApplied = listingDefault.replacehtml = true;
                query = _this.prepareQuery(listingDefault.pageType);
                _this.makeEventsAPI(query);
            });

            // Reset filter
            $('.' + _this.filtercontainer + ' .resetFilter').on('click', function (event) {
                event.preventDefault();
                var selvalue;
                selectInputs = $('.' + _this.filtercontainer).find('select');
                $(selectInputs).each(function () {
                    $(this).val([]);
                    selvalue = $(this).attr('data-place');
                    $(this).parents('.customdropdown').find('.labelValue').html(selvalue);
                });
                listingDefault.filterApplied = false;
            });

            $('.' + _this.filtercontainer + ' select').on('change', function () {
                if ($(this).attr('id') == 'month') {
                    $(this).parents('.select-wrap').siblings('.labelValue').text($(this).find('option:selected').text());
                } else {
                    $(this).parents('.select-wrap').siblings('.labelValue').text($(this).val());
                }
            });

            // Product listing - When there is a query
            var gsaquery = keyvaluepair = '', getPair, selectVal, selectId, i = 0;
            if ($('#tagTitles').length) {
                gsaquery = $('#tagTitles').val();
                if (gsaquery != '') {
                    gsaquery = gsaquery.replace(/[\[\]']+/g, '').split(',');
                    $.each(gsaquery, function () {
                        getPair = gsaquery[i].split(':');
                        if (getPair.length > 0) {
                            selectVal = encodeURIComponent(getPair[1]);
                            selectId = $.trim(getPair[0]);
                            if (selectVal != 'all') {
                                keyvaluepair += ('.' + selectId + ':' + selectVal);
                            }
                            if (selectId == 'roles') {
                                listingDefault.productType = decodeURIComponent(selectVal);
                            }
                            $('#' + selectId).val(decodeURIComponent(selectVal)).trigger('change');
                        }
                        i++;
                    });
                    if (keyvaluepair != '') {
                        listingDefault.filterQuery = '&requiredfields=type:' + listingDefault.pageType + keyvaluepair;
                        listingDefault.filterApplied = true;
                        _this.clientsideSort();
                        query = _this.prepareQuery(listingDefault.pageType);
                        _this.makeEventsAPI(query);
                    }
                }
            }
        },

        // Show hide view more
        handleviewmore: function () {
            var itemsLength = $('.' + this.itemscontainer + ' .item').length;
            var dataCount = this.jsonData.count;
            if (itemsLength < dataCount) {
                $(this.moreBtn).removeClass('hide');
                $('#' + this.container + ' .proIndicator').addClass('hide');
                if (!$('#' + this.container + ' .nomore').hasClass('hide')) {
                    $('#' + this.container + ' .nomore').addClass('hide');
                }
            } else {
                $(this.moreBtn).addClass('hide');
                $('#' + this.container + ' .proIndicator').addClass('hide');
                $('#' + this.container + ' .nomore').removeClass('hide');
            }
        },

        // Render grids with acutal object
        gridRendition: function (jsonData) {
            if (jsonData && !$.isEmptyObject(jsonData)) {
                var obj = jsonData, html, tmpObj, preparedHtml = '', pageType = listingDefault.pageType;
                var currentHtml = '', newpreparedHtml = '';

                switch (pageType) {
                    case 'events':
                        html = $("#" + this.template).html();
                        for (key in obj) {
                            if (obj.hasOwnProperty(key)) {
                                tmpObj = obj[key];
                                preparedHtml += html.replace('{img}', tmpObj.img)
                                    .replace('{imgalt}', tmpObj.imgalt)
                                    .replace('{title}', tmpObj.title)
                                    .replace('{url}', tmpObj.url)
                                    .replace('{titleURL}', tmpObj.url)
                                    .replace('{etype}', tmpObj.etype)
                                    .replace('{atype}', tmpObj.roles)
                                    .replace('{disdt}', tmpObj.disdt)
                                    .replace('{loc}', tmpObj.loc);
                            }
                        }
                        break;
                    case 'products':
                        html = $("#" + this.template).html();
                        for (key in obj) {
                            if (obj.hasOwnProperty(key)) {
                                tmpObj = obj[key];
                                preparedHtml += html.replace('{imgsrc}', tmpObj.img)
                                    .replace('{imgalt}', tmpObj.imgalt)
                                    .replace('{title}', tmpObj.title)
                                    .replace('{desc}', tmpObj.desc)
                                    .replace('{url}', tmpObj.url)
                                    .replace('{titleURL}', tmpObj.url);
                            }
                        }
                        break;
                    case 'articles':
                    case 'blog':
                        html = $("#" + this.template).html();
                        for (key in obj) {
                            if (obj.hasOwnProperty(key)) {
                                tmpObj = obj[key];
                                currentHtml = html.replace('{imgsrc}', tmpObj.img)
                                    .replace('{imgalt}', tmpObj.imgalt)
                                    .replace('{title}', tmpObj.title)
                                    .replace('{anm}', (typeof tmpObj.analystDisplay != 'undefined' && tmpObj.analystDisplay) ? tmpObj.analystDisplay : '')
                                    .replace('{disdt}', tmpObj.disdt)
                                    .replace('{url}', (typeof tmpObj.exturl != 'undefined' && tmpObj.exturl) ? tmpObj.exturl : tmpObj.url)
                                    .replace('{titleURL}', (typeof tmpObj.exturl != 'undefined' && tmpObj.exturl) ? tmpObj.exturl : tmpObj.url);

                                newpreparedHtml = $(currentHtml);
                                newpreparedHtml.find('span').filter(function () { return $.trim(this.innerHTML) == "" }).remove();

                                if (typeof tmpObj.exturl != 'undefined' && tmpObj.exturl) {
                                    newpreparedHtml.find(".contentUrl").attr("target", "_blank");
                                    newpreparedHtml.find(".contentImage").attr("target", "_blank");
                                }
                                preparedHtml += $('<div>').append(newpreparedHtml).html();
                            }
                        }
                        break;
                }
                this.applyHtml(preparedHtml);
                this.handleviewmore();
            }
        },

        bodyscroll: function (value) {
            $('html, body').animate({
                scrollTop: value
            }, 600);
        },

        noresults: function (iftrue) {
            if (iftrue) {
                if (listingDefault.filterApplied == true || listingDefault.replacehtml == true) {
                    $('#' + this.container).addClass('hide');
                    $('.resultcount').addClass('hide');
                    $('.noresults').removeClass('hide');
                } else {
                    this.handleviewmore();
                }

            } else {
                $('#' + this.container).removeClass('hide');
                $('.resultcount').removeClass('hide');
                $('.noresults').addClass('hide');
                $('#' + this.container + ' .proIndicator').addClass('hide');
                this.handleviewmore();
            }
        },

        updateresult: function () {
            var total, results;
            total = this.jsonData.count;
            results = $('#' + this.container + ' .item').length;
            if ($('.resultcount').length) {
                $('.resultcount .res-shown').empty().text(results);
                $('.resultcount .res-avail').empty().text(total);
            }
        },

        // Reset select value index to -1
        resetSelect: function () {
            $('.' + this.filtercontainer + ' select').each(function () {
                $(this).prop("selectedIndex", -1);
            })
        },

        // Remove duplicate array - * Not been used
        removeDuplicates: function (arr) {
            var tempObj = {};
            var tempArray = [];
            for (var i = 0; i < arr.length; i++) {
                if (!(arr[i] in tempObj)) {
                    tempArray.push(arr[i]);
                    tempObj[arr[i]] = true;
                }
            }
            return tempArray;
        },

        // Slice array based on items per page
        sliceArray: function (response) {
            var tmpArray = [], startIndex = 0, numofitems;
            if (typeof this.itemsinpage != "undefined") {
                numofitems = this.itemsinpage;
                startIndex = (this.itemsinpage * this.moreCount);
                if (listingDefault.sortingApplied) {
                    numofitems = (this.itemsinpage * (this.moreCount + 1));
                    startIndex = 0;
                } else {
                    numofitems = (parseInt(startIndex) + parseInt(this.itemsinpage))
                }
            } else {
                startIndex = 0;
            }
            if (response) {
                if (typeof this.itemsinpage != "undefined") {
                    tmpArray = response.slice(startIndex, numofitems);
                } else {
                    tmpArray = response;
                }
            }
            return tmpArray;
        },

        // Enable or disabling front end sorting flag and preparing required sort types based on user clicks and default sortings 
        clientsideSort: function (sorttype) {
            var _this = this,
                sortarray = ['loc', 'title'],
                arrayApplied = [],
                sortApplied = $(this.sortBtn + '.applied');
            _this.frontendSort = false;
            if (sortApplied.length) {
                $.each(sortApplied, function () {
                    arrayApplied.push($(this).attr('data-sort'))
                });
            }
            if (typeof sorttype != "undefined" && sorttype != "") {
                if ($.inArray(sorttype, arrayApplied) < 0) {
                    arrayApplied.push(sorttype);
                }
            }
            if (_this.defaultSort && typeof _this.defaultSort != 'undefined') {
                arrayApplied.push(this.defaultSort)
            }
            $.each(arrayApplied, function (index) {
                if ($.inArray(arrayApplied[index], sortarray) > -1) {
                    if ($.inArray(arrayApplied[index], _this.sortingTypes) < 0) {
                        _this.sortingTypes.push(arrayApplied[index]);
                    }
                    _this.frontendSort = true;
                }
            });
        },

        sortAlphabets: function (response, prop) {
            var tempArray = [], objSorted = {}, key, sortby = prop;
            objSorted = response.sort(function (a, b) {
                return (a[sortby] > b[sortby]) ? 1 : ((a[sortby] < b[sortby]) ? -1 : 0);
            });
            return objSorted;
        },

        // Front end sorting
        sortbykey: function (sortType, orderBy) {
            var tempObject = {}, _this = this;
            switch (sortType) {
                case 'loc':
                case 'title':
                    tempObject = _this.sortAlphabets(_this.fullResponse.data, sortType);
                    if (orderBy == 'dsc') {
                        tempObject.reverse();
                    }
                    break;
            }
            tempObject = _this.sliceArray(tempObject);
            _this.gridRendition(tempObject);
        },

        sortingHandler: function () {
            var _this = this, tempObject = {}, sortOrder = 'asc';
            if (_this.sortingTypes.length > 0) {
                $.each(_this.sortingTypes, function (index) {
                    if ($('span[data-sort=' + _this.sortingTypes + ']').hasClass('dsc')) {
                        sortOrder = 'dsc';
                    }
                    _this.sortbykey(_this.sortingTypes[index], sortOrder)
                });
            }
        },

        // Initiating main
        init: function () {
            this.gridRendition(this.jsonData);
            this.resetSelect();
            this.handleActions();
        }
    };

    var listingMain = {

        itemsperpage: $('#resultsperpage').val() || undefined,

        init: function () {
            var jsonData = {}, itemscount, itemsingrid = $('#resultsperpage').val() || '';

            //  Show view more on initial page load
            /*  Hardcoding 1000 if total count not available, this will look for 0 - 1000 records
                This is when user using tag builder for navigation */
            listcount = parseInt($('#listCount').val() || 1000);

            if (itemsingrid == '0' || itemsingrid == '') {
                itemscount = $('#gridContainer .item').length;
            } else {
                itemscount = listingMain.itemsperpage;
            }

            if ((listcount > itemscount) && listcount != 0) {
                $('#gridContainer .viewmore').removeClass('hide');
            } else {
                if ($('.noresults').hasClass('hide')) {
                    $('#gridContainer .nomore').removeClass('hide');
                }
            }

            listingMain.initateListing(jsonData);
        },

        initateListing: function (response) {
            switch (listingDefault.pageType) {
                case 'events':
                    $("#content").renderListing({
                        itemsingrid: listingMain.itemsperpage, // Do not use if no limit
                        dataObject: response,
                        template: "eventlistTemplate",
                        itemholder: "itemsholder",
                        container: "gridContainer",
                        filterContainer: "filter-container"
                    });
                    break;
                case 'products':
                    $("#content").renderListing({
                        itemsingrid: listingMain.itemsperpage,
                        dataObject: response,
                        template: "productListingTemplate",
                        itemholder: "content-wrapper",
                        container: "gridContainer",
                        filterContainer: "filter-container",
                        defaultSort: "title"
                    });
                    break;
                case 'articles':
                case 'blog':
                    $("#content").renderListing({
                        itemsingrid: listingMain.itemsperpage,
                        dataObject: response,
                        template: "articleTemplate",
                        itemholder: "content-wrapper",
                        container: "gridContainer",
                        filterContainer: "filter-container",
                        defaultSort: "date"
                    });
                    break;
            }
        },
    };

    // Initial call to render listing
    listingMain.init();

}(jQuery));
jQuery(document).ready(function ($) {
    var DisplayPieces = "",
        Steps = "",
        ChanceToShowonImage = "",
        ChanceToShowonThumb = "",
        parkingPos = 253,
        jssorObject;

    setProperties = function () {
        // body...
        if ($(window).width() < 768) {
            DisplayPieces = 4;
            Steps = 3;
            ChanceToShowonImage = 2;
            ChanceToShowonThumb = 0;
            parkingPos = 0;
        } else {
            DisplayPieces = 4;
            Steps = 3;
            ChanceToShowonImage = 1;
            ChanceToShowonThumb = 2;
        }
    };

    setProperties();

    var options = {
        $AutoPlay: true, //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
        $AutoPlayInterval: 6000, //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
        $SlideDuration: 500, //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
        $DragOrientation: 1, //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
        $UISearchMode: 0, //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).

        $ArrowNavigatorOptions: { //[Optional] Options to specify and enable arrow navigator or not
            $Class: $JssorArrowNavigator$, //[Requried] Class to create arrow navigator instance
            $ChanceToShow: ChanceToShowonImage, //[Required] 0 Never, 1 Mouse Over, 2 Always
            $AutoCenter: 2 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
        },

        $ThumbnailNavigatorOptions: {
            $Class: $JssorThumbnailNavigator$, //[Required] Class to create thumbnail navigator instance
            $ChanceToShow: 2, //[Required] 0 Never, 1 Mouse Over, 2 Always
            $Loop: 1, //[Optional] Enable loop(circular) of carousel or not, 0: stop, 1: loop, 2 rewind, default value is 1
            $SpacingX: 1, //[Optional] Horizontal space between each thumbnail in pixel, default value is 0
            $SpacingY: 1, //[Optional] Vertical space between each thumbnail in pixel, default value is 0
            $DisplayPieces: DisplayPieces, //[Optional] Number of pieces to display, default value is 1
            $ParkingPosition: parkingPos,

            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$, //[Requried] Class to create arrow navigator instance
                $ChanceToShow: ChanceToShowonThumb, //[Required] 0 Never, 1 Mouse Over, 2 Always
                $AutoCenter: 2, //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                $Steps: Steps //[Optional] Steps to go for each navigation request, default value is 1
            }
        }
    };

    if ($('#slider1_container').length) {

        if ((navigator.appName == 'Microsoft Internet Explorer') || (!!navigator.userAgent.match(/Trident.*rv[ :]*11\./))) {
            $("body").addClass('ie');
            $(".slide-holder").eq(0).css('z-index', '5');
        }

        if ($('#env').val() == "publish") {
            $('.jssor-container_author').removeClass('jssor-container_author').addClass('jssor-container');
            var items = $('.jssort-slides').find('.slide-holder');
            if ($(items).length) {
                cloneItems = $(items).clone();
                $('.jssort-slides').empty().append(cloneItems);
            }

            var cloneGallery = $('#slider1_container').html(), windowSize = $(window).width(),
                currOrientation = deviceOrientation();

            // Initiate plugin while resizing
            $(window).resize(function () {
                windowSize = $(window).width();
                changedOrientation = deviceOrientation();

                if (windowSize < 768 && (currOrientation != changedOrientation)) {
                    currOrientation = deviceOrientation();
                    $('#slider1_container').empty().removeAttr('jssor-slider').removeAttr('style').append(cloneGallery);
                    jssorObject = new $JssorSlider$("slider1_container", options);
                    $(".jssor-container").data('controller', jssorObject);
                    // Setting width and height for video
                    setvideodim(windowSize)
                    $JssorPlayer$.$FetchPlayers(document.body);
                    akamaiLoadHandler("mediaGallery");
                }
            });

            // Setting width and height for video
            if ($('.videoholder').length) {
                setvideodim(windowSize)
            }

            jssorObject = new $JssorSlider$("slider1_container", options);
            $(".jssor-container").data('controller', jssorObject);

            //fetch and initialize youtube players
            $JssorPlayer$.$FetchPlayers(document.body);
        }
    }

    function deviceOrientation() {
        var currOrientation = '';
        if (window.innerHeight > window.innerWidth) {
            currOrientation = 'portrait';
        } else if (window.orientation === 90 || window.orientation === -90) {
            currOrientation = 'landscape';
        }
        return currOrientation;

    }

    function setvideodim(windowwidth) {
        var vidwidth, vidheight;
        if (windowSize > 767) {
            vidwidth = '660px', vidheight = '380px';
        } else if (windowSize > 479 && windowSize < 768) {
            vidwidth = $('.slide-content').width(), vidheight = '210px';
        } else {
            vidwidth = $('.slide-content').width(), vidheight = '141px';
        }
        $('.videoholder .slide-content').css({ 'width': vidwidth, 'height': vidheight })
    }
});
var GLOBAL = {};

var allFields, salesFormPresent, formValidateId;
var requiredSymbol, statePlaceholder, countryPlaceHolder;
GLOBAL.errorObject = {};

GLOBAL.createCookie = function (name, value, days, secureFlag) {
    var expires;
    if (parseInt(days, 10)) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }

    // Set secure attribute for HTTPS protocol
    if (secureFlag) {
        if (document.location.protocol == "https:") {
            document.cookie = name + "=" + escape(value) + expires + "; secure";
        }
        else {
            document.cookie = name + "=" + escape(value) + expires + ";";
        }
    } else {
        document.cookie = name + "=" + escape(value) + expires + ";";
    }
};

GLOBAL.checkCookie = function (name) {
    var x, key, cookArr, nameM = false, arr;
    try {
        cookArr = document.cookie.split(";");
        arr = cookArr.map(function (s) {
            return s.trim();
        });
        for (x = 0; x < arr.length; x++) {
            key = arr[x].split("=")[0];
            if (key === name) {
                nameM = true;
            }
        }
    }
    catch (e) {
        console.log("Cookie check failed");
    }
    return nameM;
};

GLOBAL.getCookieValue = function (name) {
    var x, key, value, cookArr;
    cookArr = document.cookie.split(";").map(function (s) { return s.trim(); });
    for (x in cookArr) {
        key = cookArr[x].split("=")[0];
        value = cookArr[x].split("=")[1];
        if (key == name) { return value; }
    }
    return undefined;
};

GLOBAL.showGlobalError = function (msg) {
    var t = $(".global-error");
    t.html(msg);
    t.removeClass("hide");
};

GLOBAL.findInvalidChar = function (testString, invalidCharArray) {
    var test = true;
    $.each(invalidCharArray, function (i, item) {
        if (testString.indexOf(item) != -1) {
            test = false;
            return false;
        }
    });
    return test;

};

GLOBAL.validFailColor = function (idSel, errorMsg) {
    var selID = "#" + idSel, errorID = "error-" + idSel;
    var findParent = $(selID).parents('.inpField');
    var errorElement = jQuery('<div>', {
        "class": "errorMsg",
        text: errorMsg,
        id: errorID
    });
    $(selID).css('border', '1px solid red');
    if ($("#" + errorID).length === 0) {
        $(findParent).before(errorElement);
    }
    else {
        $("#" + errorID).replaceWith(errorElement);
    }
};

GLOBAL.eraseFailColor = function (idSel) {
    $("#" + idSel).css('border', 'none');
    $("#error-" + idSel).remove();
};

GLOBAL.paintError = function () {
    var i;
    for (i in GLOBAL.errorObject) {
        GLOBAL.validFailColor(i, GLOBAL.errorObject[i]);
    }
};

GLOBAL.eraseError = function () {
    var i, idSel;
    for (i = 0; i < allFields.length; i++) {
        idSel = $(allFields[i]).attr('id');
        if (!(idSel in GLOBAL.errorObject)) {
            GLOBAL.eraseFailColor(idSel);
        }
    }
};

GLOBAL.bindKeyValidation = function () {
    allFields.on('keypress', function () {
        GLOBAL.validate();
    });
};

GLOBAL.bindBlurValidation = function () {
    allFields.on('blur', function () {
        GLOBAL.validate();
    });
};

GLOBAL.validate = function () {
    var validAll = true;
    var loopVar, prop, ele, plc, eleType;
    var fnameForm = salesFormPresent ? "first_name" : "fName";
    var lnameForm = salesFormPresent ? "last_name" : "lName";
    var zipCodeForm = salesFormPresent ? "zip_code" : "zipcode";
    var validateBlockDomain = salesFormPresent ? false : true;
    var testResult;
    var emailVal = $("#email").val() || "";
    var phoneVal = $("#phone").val() || "";
    var fnameVal = $("#" + fnameForm).val() || "";
    var lnameVal = $("#" + lnameForm).val() || "";
    var zipCodeVal = $("#" + zipCodeForm).val() || "";

    var i, x = $(".blockedDomain"), domainArr = [];
    var invalidCharEmail = ["#", "$", "%", "&", "*", "+", "/", "=", "^", "`", "{", "|", "}", "~", "<", ">", ";", "(", ")", "!", '"', "?", "'"];
    var invalidCharZipCode = ["^", "{", "}", "#", "=", "@", "(", ")", "/", "\\", "<", ">"];

    for (i = 0; i < x.length; i++) {
        domainArr.push($(x[i]).val());
    }

    allFields = $("#" + formValidateId + " #fields").find(':input');
    for (loopVar = 0; loopVar < allFields.length; loopVar++) {
        ele = $(allFields[loopVar]);
        prop = ele.attr('required');
        if (prop == 'required') {
            if (ele.val() === '') {
                eleType = ele.prop("tagName");
                if (eleType == "INPUT") {
                    plc = (ele.siblings('.placeholder').text().split(requiredSymbol)[0]).trim();
                }
                else if (eleType == "SELECT") {
                    plc = (ele.parents('.select-wrap').siblings('.labelValue').text().split(requiredSymbol)[0]).trim();
                }
                GLOBAL.errorObject[ele.attr('id')] = plc + " is blank. Please enter a valid value";
                validAll = false;
            }
            else {

                delete GLOBAL.errorObject[ele.attr('id')];
            }
        }
    }
    var re = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    var rePhone = /^[0-9()-]+$/;
    var reFName = /^[^{}#=+$\[\]@()^<>\/\\]+$/g;
    var reLName = /^[^{}#=+$\[\]@()^<>\/\\]+$/g;
    if (fnameVal) {
        testResult = reFName.test(fnameVal);
        if (!testResult) {
            GLOBAL.errorObject[fnameForm] = "Enter a valid first name";
            validAll = false;
        }
        else {

            delete GLOBAL.errorObject[fnameForm];
        }
    }

    if (lnameVal) {
        testResult = reLName.test(lnameVal);
        if (!testResult) {
            GLOBAL.errorObject[lnameForm] = "Enter a valid last name";
            validAll = false;
        }
        else {
            delete GLOBAL.errorObject[lnameForm];
        }
    }
    if (zipCodeVal) {
        if (zipCodeVal.length > 30) {
            GLOBAL.errorObject[zipCodeForm] = "Invalid Postal code. Please re-enter";
            validAll = false;
        }
        else if (!GLOBAL.findInvalidChar(zipCodeVal, invalidCharZipCode)) {
            GLOBAL.errorObject[zipCodeForm] = "Invalid characters found in zipcode, please re-check";
            validAll = false;
        }
        else {
            delete GLOBAL.errorObject[zipCodeForm];
        }

    }

    if (emailVal) {
        testResult = re.test(emailVal);
        if (!GLOBAL.findInvalidChar(emailVal, invalidCharEmail)) {
            GLOBAL.errorObject["email"] = "Invalid characters found in email, please re-check";
            validAll = false;
        }
        else if (emailVal.length <= 5 || emailVal.length >= 100) {
            GLOBAL.errorObject["email"] = "Email length is invalid, please re-check";
            validAll = false;
        }
        else if (!(testResult)) {
            GLOBAL.errorObject["email"] = "Enter valid email";
            validAll = false;
        }
        else if (validateBlockDomain) {
            for (i = 0; i < domainArr.length; i++) {
                if (emailVal.indexOf(domainArr[i]) != -1) {
                    GLOBAL.errorObject["email"] = "Email contains blocked domain " + domainArr[i];
                    validAll = false;
                }
            }
        }
        else {
            delete GLOBAL.errorObject["email"];
        }
    }

    if (phoneVal) {
        testResult = rePhone.test(phoneVal);
        if (!testResult) {
            GLOBAL.errorObject["phone"] = "Enter valid phone number";
            validAll = false;
        }
        else {
            delete GLOBAL.errorObject["phone"];
        }
    }
    GLOBAL.eraseError();
    GLOBAL.paintError();
    return (validAll);
};

GLOBAL.populateCountry = function (resultState) {
    var stateRequired = $("#state").attr("required") ? requiredSymbol : "";
    var requiredTag = $("#state").attr("required") ? "required" : "";

    if (resultState.length === 0) {
        $("#state").parents(".inpField").replaceWith('<div class="inpField"><label class="placeholder" for="state">' + statePlaceholder + '</label><input id="state" class="inputfield" type="text" autocomplete="off" ' + requiredTag + ' /></div>');

        $("input#state").on('focus', function () {
            $(this).siblings('label').hide();
        }).on('blur', function () {
            if (!$(this).val()) {
                $(this).siblings('label').show();
            }
        });

        $("input#state").on("blur keypress", function () {
            GLOBAL.validate();
        });
    }
    else {

        $("#state").parents('.inpField').replaceWith('<div class="select-wrapper inpField customdropdown"><label for="state"><span class="placeholderSel labelValue">' + statePlaceholder + '</span><div class="select-wrap"><select id="state" class="dropdownclass "' + requiredTag + '></select></div></label></div>');

        $("#state").append($('<option>', {
            value: "",
            "data-default": "true",
            text: statePlaceholder
        }));
        $.each(resultState, function (i, item) {
            $("#state").append($('<option>', { 'data-value': item, text: item }));
        });

        $('#state').on('change', function () {
            $(this).parents('.select-wrap').siblings('.labelValue').text($(this).val());
        });

        $("#state").on("blur keypress", function () {
            GLOBAL.validate();
        });


    }
};

$(document).ready(function () {
    var evId, reUrl = $("#talkpointEventurl").val() + "?ei=", emailID;
    var thankMessage = $('#thankYouMessage').val();
    var alreadyMessage = $('#alreadyRegisteredMessage').val();
    var ctaLinkText = $('#CTAlinkMessage').val();
    requiredSymbol = $("#reqd").children(".red").text();
    var stateEle = $("#state").parents(".select-wrapper");
    var countryEle = $("#country").parents(".select-wrapper");
    statePlaceholder = (stateEle.length == 0) ? "" : (stateEle.prev().text().split(requiredSymbol)[0].trim());
    countryPlaceHolder = (countryEle.length == 0) ? "" : (countryEle.prev().text().split(requiredSymbol)[0].trim());
    salesFormPresent = ($("#salesforceform").length !== 0);
    formValidateId = salesFormPresent ? "salesforceform" : "form";
    allFields = $("#" + formValidateId + " #fields").find(':input');

    evId = $("#ei").val();

    //event listeners to show / hide placeholder text
    $('.inpField input').on('focus', function () {
        $(this).siblings('label').hide();
    }).on('blur', function () {
        if (!$(this).val()) {
            $(this).siblings('label').show();
        }
    });

    $('.customdropdown select').bind('change', function () {
        if ($(this).attr('id') == 'month') {
            $(this).parents('.select-wrap').siblings('.labelValue').text($(this).find('option:selected').text());
        } else {
            if ($(this).val().toLowerCase() == 'all') {
                $(this).parents('.select-wrap').siblings('.labelValue').text($(this).find('option:selected').text());
            } else {
                $(this).parents('.select-wrap').siblings('.labelValue').text($(this).val());
            }
        }
    });

    var uri = window.location.href;
    var confName;
    // Conference: cookie creation while register
    if (($('#confPageName').length) && ($('#salesforceform').length)) {
        if (uri.indexOf('#ThankYou') > -1) {
            confName = $('#confPageName').val().replace(/ /g, '');
            if (!(GLOBAL.checkCookie(confName))) {
                GLOBAL.createCookie(confName, '1', 365, true);
            }
        }
    }

    // Conference: Handling display of form & messages
    if ($('#confPageName').length) {
        confName = $('#confPageName').val().replace(/ /g, '');
        const confStatus = $('#eventStatus').val().toLowerCase();
        if (confStatus) {
            if (confStatus == 'past') {
                $('.salesforceformcontainer').addClass('hide');
                $('.conf_occured').removeClass('hide');
            } else if (confStatus == 'future') {
                var getConfCookie = GLOBAL.getCookieValue(confName);
                if (getConfCookie && (uri.indexOf('#ThankYou') > -1)) {
                    $('.salesforceformcontainer').addClass('hide');
                    $('.conf_reg').removeClass('hide');
                } else if (getConfCookie) {
                    $('.salesforceformcontainer').addClass('hide');
                    $('.conf_exist').removeClass('hide');
                }
            }
        }
    }

    //check existing cookie
    if (GLOBAL.checkCookie("event_id_" + evId)) {
        emailID = GLOBAL.getCookieValue("event_id_" + evId);
        reUrl = reUrl + evId + "&email=" + emailID;
        $(".center").html('<div class="thanks">' + alreadyMessage + '</div><br/><a href="' + reUrl + '" target="_blank" class="thanks-btn">' + ctaLinkText + '</a>');
    }


    $("#salesforceform").submit(function (e) {
        if (GLOBAL.validate()) {
            $("#retURL").val(window.location.href + '#ThankYou');
        }
        else {
            GLOBAL.bindKeyValidation();
            GLOBAL.bindBlurValidation();
            $("body").animate({ scrollTop: $('#' + Object.keys(GLOBAL.errorObject)[0]).offset().top - 30 }, 800);
            e.preventDefault();
        }
    });

    $("#form").submit(function (ev) {
        if (GLOBAL.validate()) {
            var ctxPath = $('#ctx').text();
            var postData = $(this).serializeArray(),
                passId = $('#talkpointClientId').val() || '',
                formURL = $('#talkpointServiceurl').val() || '',
                dateState = $("#eventStatus").val(),
                thanks;

            postData.push({ 'name': 'pass', 'value': passId });

            if (dateState === "") {
                alert("Event date is still not set for this event");
                return false;
            }
            else {

                $.ajax({
                    url: formURL,
                    type: "POST",
                    data: postData,
                    success: function (response, textStatus, jqXHR) {
                        switch ($.trim(response)) {
                            case "Event not found.":
                                GLOBAL.showGlobalError("Invalid event ID");
                                break;
                            case "Unauthorized.":
                                GLOBAL.showGlobalError("The client pass ID does not have access to the Event ID ");
                                break;
                            case "Registration for this event is prohibited.":
                                GLOBAL.showGlobalError("The event is configured for anonymous registration");
                                break;
                            case "Email not found.":
                                GLOBAL.showGlobalError("Valid email address is required for registration. This message indicates an email was not passed");
                                break;
                            case "Email format not valid.":
                                GLOBAL.showGlobalError("Invalid email format.");
                                break;
                            case "User already registered.":
                            case "SUCCESS!":
                                evId = $("#ei").val();
                                emailID = $('#email').val();
                                reUrl = $("#talkpointEventurl").val() + "?ei=" + evId + "&email=" + emailID;
                                if (dateState === "future") {
                                    GLOBAL.createCookie("event_id_" + evId, emailID, 365, true);
                                    if (!Boolean($(".thanks").length)) {
                                        $(".center").html('<div class="thanks">' + thankMessage + '</div><br/><a href="' + reUrl + '" target="_blank" class="thanks-btn">' + ctaLinkText + '</a>');
                                    }
                                }
                                else {
                                    GLOBAL.createCookie("event_id_" + evId, emailID, 365, true);
                                    if (!Boolean($(".thanks").length)) {
                                        $(".center").html('<div class="thanks">' + thankMessage + '</div><br/><a href="' + reUrl + '" target="_blank" class="thanks-btn">' + ctaLinkText + '</a>');
                                        window.open(reUrl, '_blank');
                                    }
                                }
                                break;
                            case "No reg data sent.":
                                GLOBAL.showGlobalError("General error. Please contact technical support.");
                                break;
                            default:
                                GLOBAL.showGlobalError('Registration Failed. Please contact technical support.');
                                break;
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        GLOBAL.showGlobalError('Error: Please contact technical support.');
                    }
                });
            }
            ev.preventDefault();
            ev.stopImmediatePropagation();
        }
        else {
            GLOBAL.bindKeyValidation();
            GLOBAL.bindBlurValidation();
            $("body").animate({ scrollTop: $('#' + Object.keys(GLOBAL.errorObject)[0]).offset().top - 30 }, 800);
            ev.preventDefault();
        }
    });

    $("#country").change(function (e) {
        var cVal = $('#country').find(":selected").attr('value');
        var dVal = $('#country').find(":selected").attr('data-value');
        //if default placeholder option is selected 
        if (cVal === "" && dVal === undefined) {
            var stateRequired = $("#state").attr("required") ? requiredSymbol : "";
            var requiredTag = $("#state").attr("required") ? "required" : "";

            $("#state").parents('.inpField').replaceWith('<div class="select-wrapper inpField customdropdown"><label for="state"><span class="placeholderSel labelValue">' + statePlaceholder + '</span><div class="select-wrap"><select id="state" class="dropdownclass" ' + requiredTag + '></select></div></label></div>');
            $("#country").parents('.select-wrap').siblings('.labelValue').text(countryPlaceHolder);
        }
        else {
            var ctxPath = $('#ctx').text();
            $.ajax({
                url: ctxPath + "/bin/enterprise/registrationFormServlet",
                type: "GET",
                data: { "countryTagId": dVal },
                success: function (data, textStatus, jqXHR) {
                    var arr = [];
                    data.forEach(function (e) {
                        arr.push(e.text);
                    });
                    GLOBAL.populateCountry(arr);
                },
                error: function (xhr, status, error) {
                    console.log('Error: Please contact technical support.');
                }
            });
            e.stopImmediatePropagation();
        }
    });
});
function akamaiLoadHandler(initState) {
    initState = initState || "all";
    if ($('.AkamaiPlayer').length) {
        var amp, playerDivs, ampPath, ampResources, contextroot, tempConfig = {};
        if (initState == "mediaGallery") {
            playerDivs = $(".jssor-container .AkamaiPlayer");
        } else {
            playerDivs = $(".AkamaiPlayer");
        }

        contextroot = $(playerDivs[0]).data('contextroot') || '';

        if ($(playerDivs[0]).data('amppath').length) {
            ampPath = $(playerDivs[0]).data('amppath');
        }

        if ($(playerDivs[0]).data('ampresources').length) {
            ampResources = $(playerDivs[0]).data('ampresources');
        }

        // Config object
        var akamaiPlayerConfig = {
            paths: {
                player: ampPath,
                resources: ampResources
            },
            autoplay: true,
            domain: "",
            volumepanel: {
                direction: "vertical"
            },
            media: {
                poster: "",
                source: [{}, {}],
                track: [{
                    kind: "captions",
                    type: "application/ttml+xml",
                    src: ""
                }]
            },
            captioning: {
                enabled: false
            },
            branding: {
                logo: ""
            },
            flash: {
                params: {
                    wmode: "opaque"
                }, view: {
                    elements: {
                        endSlate: {
                            attributes: {
                                hideElements: "brandingLogo|viewAll"
                            }
                        }
                    }
                },
                xml: "<?xml version=\"1.0\" encoding=\"UTF-8\"?><application><player branding_preload=\"none\" auto_replay=\"true\" caption_language=\"en\" dvr_enabled=\"1\" enable_alternate_server_mapping=\"true\" enable_end_user_mapping=\"true\" eventmanagementstates_enabled=\"false\" eventmanagementstates_status_interval=\"5\" eventmanagementstates_status_url=\"../resources/eventmanagement/playerstate.txt\" font_size=\"18\" hds_live_low_latency=\"false\" is_token_required=\"false\" locale_setting=\"en\" media_analytics_logging_enabled=\"false\" rewind_interval=\"15\" set_resume_dvr_at_live=\"true\" start_bitrate_index=\"-1\" title=\"Video Title\" use_netsession_client=\"true\" video_url=\"http://multiplatform-f.akamaihd.net/z/multi/april11/hdworld/hdworld_,512x288_450_b,640x360_700_b,768x432_1000_b,1024x576_1400_m,1280x720_1900_m,1280x720_2500_m,1280x720_3500_m,.mp4.csmil/manifest.f4m\" volume=\"50\"></player></application>"
            },
            dash: {
                resources: [{
                    src: "//cdn.dashjs.org/v1.5.1/dash.all.js"
                }]
            }

        };

        // Method to calculate width and height
        function getvideosize(_self) {
            var vidres = _self.attr('data-res'), vidwidth, vidheight;
            if (vidres) {
                vidres = vidres.split('x');
                vidwidth = vidres[0];
                vidheight = vidres[1];
            }

            var parentwidth = _self.parent().innerWidth(),
                basewidth = vidwidth || '606',
                baseheight = vidheight || '340';

            var ratioHeight = (baseheight * parentwidth) / basewidth;

            return {
                vidwidth: Math.floor(parentwidth),
                vidheight: Math.floor(ratioHeight)
            }
        }

        // Setting video height based on its width
        function setvideoheight() {
            if (playerDivs.length) {
                playerDivs.each(function () {
                    var _this = $(this);
                    var vdim = getvideosize(_this);
                    _this.width(vdim.vidwidth);
                    _this.height(vdim.vidheight);
                });
            }
        }

        // Initializing players
        if (typeof ampPath != 'undefined' && typeof ampResources != 'undefined') {
            playerDivs.each(function () {
                var _this = $(this);
                _this.mainHolder = $('#' + _this.attr('id')).parents('.akamaiPlayerContainer');

                // Set height to each videos
                var vdim = getvideosize(_this);
                _this.width(vdim.vidwidth);
                _this.height(vdim.vidheight);
                //_this.mainHolder.css('height',vdim.vidheight);

                // Options
                tempConfig = akamaiPlayerConfig;
                tempConfig.autoplay = ((_this.attr('data-autoplay') == 'disable') ? false : false);
                tempConfig.media.title = _this.attr('data-title');
                //tempConfig.media.poster = contextroot + _this.data('poster');

                if (typeof _this.attr('data-src') === 'undefined') {
                    tempConfig.media.source[0].src = "";
                }
                else {
                    tempConfig.media.source[0].src = _this.attr('data-src');
                }

                if (typeof _this.attr('data-type') === 'undefined') {
                    tempConfig.media.source[0].type = "";
                }
                else {
                    tempConfig.media.source[0].type = _this.attr('data-type');
                }

                if (typeof _this.attr('data-fallback-src') === 'undefined') {
                    tempConfig.media.source[1].src = "";
                }
                else {
                    tempConfig.media.source[1].src = _this.attr('data-fallback-src');
                }

                if (typeof _this.attr('data-fallback-type') === 'undefined') {
                    tempConfig.media.source[1].type = "";
                }
                else {
                    tempConfig.media.source[1].type = _this.attr('data-fallback-type');
                }


                if (_this.attr('data-caption')) {
                    tempConfig.captioning.enabled = true;
                    tempConfig.media.track[0].src = _this.attr('data-captionSrc');
                }

                amp = akamai.amp.AMP.create(_this.attr('id'), tempConfig);

                _this.data('controller', amp);
                _this.closeBtn = $(_this.mainHolder).find('.amp-close');
                _this.playBtn = $(_this.mainHolder).find('.amp-video-btn');
                _this.poster = $(_this.mainHolder).find('.amp-poster');

                $(_this.closeBtn).on('click', function () {
                    _this.closeBtn.hide();
                    _this.mainHolder.find('.akamai-video').css('height', '0');
                    _this.poster.show();
                    _this.data('controller').pause();
                    _this.mainHolder.attr('style', '');
                    clearTimeout(_this.timer);
                });

                $(_this.playBtn).on('click', function () {
                    _this.poster.hide();
                    _this.mainHolder.css('height', 'auto');
                    _this.mainHolder.find('.akamai-video').css('height', '100%');
                    if ($('.jssor-container').length) {
                        $('.jssor-container').data('controller').$Pause();
                    }
                    _this.data('controller').play();
                });

                $(_this).hover(function () {
                    _this.closeBtn.show();
                    _this.timer = setTimeout(function () {
                        _this.closeBtn.hide();
                    }, 2400);
                });
            });
        }

        if ($('.jssor-container').length) {
            akamaiListeners();
        }

        $(window).resize(function () {
            setvideoheight();
        });
    }
}

// Akamai player listener to play/stop gallery auto slides
function akamaiListeners() {
    var jssorObject = $('.jssor-container').data('controller');
    $('.jssor-container .AkamaiPlayer').each(function () {
        var playerController = $(this).data('controller');
        playerController.addEventListener('play', function () {
            jssorObject.$Pause();
        });
        playerController.addEventListener('pause', function () {
            jssorObject.$Play();
        });
    });

    jssorObject.$On($JssorSlider$.$EVT_PARK, function (slideIndex, fromIndex) {
        if ($('.slide-holder').eq(fromIndex).find('.AkamaiPlayer').length > 0) {
            $('.slide-holder').eq(fromIndex).find('.akamaiPlayerContainer .amp-close').trigger('click');
        }
    });
}

$(window).load(akamaiLoadHandler);
// Initial call to initiate amp

$(document).ready(function () {
    $("#akamaiMetaDataForm").submit(function (e) {
        e.preventDefault();
        $('#akamaiSuccessMessage').hide();
        var titleErrorFlag = false,
            urlErrorFlag = false,
            fallbackErrorFlag = false,
            typeErrorFlag = false,
            fallbackTypeErrorFlag = false
        if (!$('#videoTitle').val() || /^\s*$/.test($('#videoTitle').val())) {
            $('#akamaiTitleError').html("Please enter Video Title");
            $('#akamaiTitleError').show();
            titleErrorFlag = false;
        }
        else if (/[~`!#$%\^&*+=\-\[\]\\';,\/{}|\\":<>\?]/g.test($('#videoTitle').val())) {
            $('#akamaiTitleError').html("Special characters are not allowed");
            $('#akamaiTitleError').show();
            titleErrorFlag = false;
        }
        else {
            titleErrorFlag = true;
            $('#akamaiTitleError').hide();
        }
        if (!$('#videoURL').val() || /^\s*$/.test($('#videoURL').val())) {
            $('#akamaiURLError').html("Please enter valid Video URL");
            $('#akamaiURLError').show();
            urlErrorFlag = false;
        }
        else {
            urlErrorFlag = true;
            $('#akamaiURLError').hide();
        }
        if (!$('#fallbackURL').val() || /^\s*$/.test($('#fallbackURL').val())) {
            $('#akamaiFallbackError').html("Please enter valid Video Fallback URL");
            $('#akamaiFallbackError').show();
            fallbackErrorFlag = false;
        }
        else {
            fallbackErrorFlag = true;
            $('#akamaiFallbackError').hide();
        }
        if (!$('#videoType').val() || /^\s*$/.test($('#videoType').val())) {
            $('#akamaiTypeError').html("Please enter Video Type");
            $('#akamaiTypeError').show();
            typeErrorFlag = false;
        }
        else if (/[~`!#$%\^&*+=\[\]\\';,{}|\\":<>\?]/g.test($('#videoType').val())) {
            $('#akamaiTypeError').html("Only / is allowed");
            $('#akamaiTypeError').show();
            typeErrorFlag = false;
        }
        else {
            typeErrorFlag = true;
            $('#akamaiTypeError').hide();
        }
        if (!$('#fallbackVideoType').val() || /^\s*$/.test($('#fallbackVideoType').val())) {
            $('#akamaiFallbackTypeError').html("Please enter Fallback Video Type");
            $('#akamaiFallbackTypeError').show();
            fallbackTypeErrorFlag = false;
        }
        else if (/[~`!#$%\^&*+=\[\]\\';,{}|\\":<>\?]/g.test($('#fallbackVideoType').val())) {
            $('#akamaiFallbackTypeError').html("Only / and - are allowed");
            $('#akamaiFallbackTypeError').show();
            fallbackTypeErrorFlag = false;
        }
        else {
            fallbackTypeErrorFlag = true;
            $('#akamaiFallbackTypeError').hide();
        }
        if (titleErrorFlag == false || urlErrorFlag == false || fallbackErrorFlag == false || typeErrorFlag == false || fallbackTypeErrorFlag == false) {
            return;
        }
        var contextRoot = $('.akamaivideometadata #contextpath').val() || "";

        $.ajax({
            type: "POST",
            url: contextRoot + "/apps/enterprise/videoMetadata",
            data: $("form").serialize(),
            dataType: 'json',
            encode: true,
            success: function (data) {
                $('#akamaiSuccessMessage').html(data.response);
                $('#akamaiSuccessMessage').show();
                $('#akamaiMetaDataForm')[0].reset();
            },
            error: function () {
                $('#akamaiSuccessMessage').html("Error, The server cant be reached right now. Please try Again.");
                $('#akamaiSuccessMessage').show();
            }
        })
            .done();
        return false;
    });
});
(function () {
    var ArticleModalInstance = function (url) {
        this.url = url;
        this.$root = $('body').find('#general-content-modal');

        this.getArticleModal();
    };

    $.extend(ArticleModalInstance.prototype, {
        getArticleModal: function () {
            $.ajax({
                url: this.url
            }).done(_.bind(this.populateModal, this));
        },

        populateModal: function (articleContent) {
            // parse ajax return, remove leading white-space so jquery-migrate doesn't throw an error
            var $article = $($.parseHTML(articleContent)).filter('*').find('.article');

            // reset modal
            this.$root.find('.article').remove();

            // insert new article
            this.$root.append($article);

            this.$root.modal({
                backdrop: 'static' // don't close modal unless the user uses an explicit CTA
            });

            // activate social links
            this.setIcons();
        },
        setIcons: function () {
            janrain.social.addWidgetTo(this.$root.find('.janrainSocialPlaceholder').get(-1));
            this.$root.imagesLoaded($.proxy(this.scrubIcons, this));
        },
        scrubIcons: function () {
            this.$root.find('.janrainProvider').html('<div></div>');
            this.setHeights();
        },
        setHeights: function () {
            var sidebarHeight = 0;
            this.$root.find('.article-sidebar .janrainProvider').each(function () {
                sidebarHeight = sidebarHeight + $(this).outerHeight();
            });
            this.$root.find('.article-sidebar').css({ height: sidebarHeight });

            var tallest = MCWCM.Sizer.setHeights(this.$root.find('.article-sidebar, .article-wrapper'), { resize: false });
        }
    });

    MCWCM.ArticleModal = ArticleModalInstance;
})();

(function () {
    var ContentFeedInstance = function ($root, options) {
        this.$root = $root;
        this.$feedFrame = this.$root.find('.content-feed__scroll-frame');
        this.$feedContainer = this.$root.find('.content-feed__scroll-container');
        this.$frames = this.$feedContainer.find('.content-feed__frame');
        this.$frameBlocks = this.$feedContainer.find('.content-feed__frame-block');
        this.$items = this.$frames.find('.content-feed-item');
        this.options = options;
        this.increment = 0;
        this.navTrigger = false;

        this.scrollSpeed = this.$root.find('.content-feed-data').data('scrollSpeed');
        if (isNaN(this.scrollSpeed) || this.scrollSpeed < 1 || this.scrollSpeed > 5) {
            this.scrollSpeed = 30;
        } else {
            this.scrollSpeed = this.scrollSpeed * 10;
        }

        if (!this.$frames.length) {
            return;
        }

        this.$prev = this.$root.find('.content-feed__prev.prev');
        this.$next = this.$root.find('.content-feed__next.next');

        // hammer nav
        this.start = 0;
        this.$feedContainer.data('ContentFeedInstance', this);

        // prevent default webkit behaviour
        this.$feedContainer.css('-webkit-user-drag', 'none');

        if (this.$root.hasClass('content-feed--admin')) {
            this.contentOffsetLeft = 0;
        } else {
            this.contentOffsetLeft = $('#content').offset().left;
        }

        this.initFeed();
        if (!$('html').hasClass('touch')) {
            this.bindEvents();
        }

        $(window).on('windowresize', _.debounce(_.bind(this.onResize, this), 200));
    };

    $.extend(ContentFeedInstance.prototype, {
        initFeed: function () {
            this.setItemHrefs();
            this.verticalResponsive();
            this.showContentFeed();

            this.frameWidth = this.$feedFrame.width();
            this.containerWidth = this.getWrapperWidth();
            this.$feedContainer.width(this.containerWidth);
            this.min = Math.min(-this.$feedContainer.width() + this.frameWidth, 0);
            this.$feedContainer.css(this.getPositionConfig(this.$feedContainer.offset().left));
            this.$feedFrame.css({ marginTop: 0 });
        },
        onResize: function () {
            if (this.$root.hasClass('content-feed--admin')) {
                this.contentOffsetLeft = 0;
            } else {
                this.contentOffsetLeft = $('#content').offset().left;
            }

            this.min = Math.min(-this.containerWidth + this.frameWidth, 0);
            this.initFeed();
        },

        bindEvents: function () {
            this.$root.hammer({
                drag: true,
                dragBlockHorizontal: true,
                dragLockMinDistance: 8
            });
            this.$root.on({
                dragstart: _.bind(this.onDragStart, this),
                drag: _.bind(this.onDrag, this),
                dragend: _.bind(this.onDragEnd, this)
            });

            if (!$('html').hasClass('touch')) {
                this.$prev.on('mouseover', $.proxy(this.onPrev, this));
                this.$next.on('mouseover', $.proxy(this.onNext, this));

                this.$prev.on('mouseout', $.proxy(this.offNav, this));
                this.$next.on('mouseout', $.proxy(this.offNav, this));

                this.toggleNavs();
            } else {
                this.$prev.parent().hide();
                this.$next.parent().hide();
            }

            // keyboard navigation
            this.$frames.on('focus', function (e) {
                $(e.target).addClass('focus');
            });

            // keyboard navigation
            this.$frames.find('a.btn').on('blur', function (e) {
                $(e.target).closest('.content-feed__frame').removeClass('focus');
            });

            this.$root.on('click', '.article-modal', $.proxy(this.getArticleModal, this));
        },

        setItemHrefs: function () {
            this.$items.each(this.setHref);
        },
        setHref: function (index, item) {
            var $item = $(item);
            var href = $(this).find(".cta").attr("href");
            var target = $(this).find(".cta").attr("target");
            $(this).find('.cta').on('click', function (e) {
                e.preventDefault();
            });
            if (!href) {
                return;
            }
            $item.off().on('click', function () {
                if (target) {
                    window.open(href);
                } else {
                    window.location = href;
                }
            });
        },

        onPrev: function () {
            this.scrollDirection = -1;
            if (this.navTrigger === false) {
                var _this = this;
                this.navDelayTO = setTimeout($.proxy(_this.onNav, _this), 400);
            } else {
                this.onNav();
            }
        },
        onNext: function () {
            this.scrollDirection = 1;
            if (this.navTrigger === false) {
                var _this = this;
                this.navDelayTO = setTimeout($.proxy(_this.onNav, _this), 400);
            } else {
                this.onNav();
            }
        },
        onNav: function () {
            this.navTrigger = true;
            var scrollSpeed = this.scrollSpeed;
            this.setNewPositionTO = setTimeout($.proxy(this.setNewPosition, this), scrollSpeed);
            this.toggleNavs();
        },
        setNewPosition: function () {

            this.increment = this.increment - this.scrollDirection;
            var newPosition = (-this.contentOffsetLeft + this.$feedContainer.offset().left + this.increment);
            this.$feedContainer.css(this.getPositionConfig(newPosition));
            this.onNav();
        },
        offNav: function (event) {
            this.increment = 0;
            if ($(event.currentTarget).attr('class') != $(event.toElement).parent().attr('class') &&
                $(event.currentTarget).attr('class') != $(event.toElement).attr('class')) {
                this.navTrigger = false;
            }
            clearTimeout(this.navDelayTO);
            clearTimeout(this.setNewPositionTO);
        },

        onDragStart: function (event) {
            this.start = this.$feedContainer.offset().left;
        },
        onDrag: function (event) {
            var gesture = event.gesture;
            var newPosition = gesture.deltaX + this.start;
            this.$feedContainer.css(this.getPositionConfig(newPosition));
        },
        onDragEnd: function (event) {
            var gesture = event.gesture;
            var velocity = gesture.velocityX;
            var direction = (gesture.direction == 'left') ? -1 : 1;
            var newPosition = this.$feedContainer.offset().left +
                (velocity * this.options['kinetic-multiplier'] * direction);

            this.$feedContainer
                .animate(this.getPositionConfig(newPosition), this.options['kinetic-duration']);
            this.$items.blur();
            this.start = 0;
        },

        getPositionConfig: function (newPosition) {
            if (newPosition < this.min) {
                newPosition = this.min;
            } else if (newPosition > 0) {
                newPosition = 0;
            }
            return { left: newPosition }
        },
        getWrapperWidth: function () {
            var width = 0;
            if (this.isResponsive()) {
                this.$frameBlocks.each(function () {
                    width = width + $(this).width();
                });
            } else {
                this.$frames.each(function () {
                    width = width + $(this).width();
                });
            }
            return width;
        },

        showContentFeed: function () {
            this.$root.css({ visibility: 'visible' });
        },

        toggleNavs: function () {
            this.leftPosition = this.getLeftPosition();

            if (this.leftPosition == 0) {
                this.$prev.hide();
                this.$prev.parent().hide();
            } else {
                this.$prev.show();
                this.$prev.parent().show();
            }

            this.rightPosition = this.containerWidth + this.leftPosition;

            if (this.rightPosition == this.frameWidth) {
                this.$next.hide();
                this.$next.parent().hide();
            } else {
                this.$next.show();
                this.$next.parent().show();
            }
        },

        getLeftPosition: function () {
            var offsetLeft = this.$feedContainer.offset().left;

            return offsetLeft - this.contentOffsetLeft;
        },

        verticalResponsive: function () {
            if (this.isResponsive()) {
                this.$root.addClass('content-feed--vertical-responsive');
            }
        },
        isResponsive: function () {
            // FOR VERTICAL RESPONSIVENESS UNCOMMENT NEXT LINE
            // return $(window).height() < 700 || MCWCM.breakpoints.isMobile;
            return MCWCM.breakpoints.isMobile;
        },

        getArticleModal: function (event) {
            event.preventDefault();
            new MCWCM.ArticleModal($(event.currentTarget).attr('href'));
            this.$prev.hide();
            this.$next.hide();
            $('#general-content-modal').on('hidden.bs.modal', $.proxy(this.toggleNavs, this));
        },
        isMobile: function () {
            return this.$root.width() < 768 || $('html').hasClass('.touch')
        },
    });

    MCWCM.ContentFeed = MCWCM.ComponentWrapperFactory('.content-feed',
        {
            'kinetic-multiplier': 100,
            'kinetic-duration': 20,
            'breakpoint': 480
        }, ContentFeedInstance);
})();;
; (function (window, undefined) {
    MCWCM.locationHref = window.location.href;
    $(document).ready(function () {
        var $body = $('body');
        var modeClassNamePrefix = 'wcmmode-';
        var isEditMode = $body.hasClass(modeClassNamePrefix + 'edit');
        var isDesignMode = $body.hasClass(modeClassNamePrefix + 'design');
        var isPreviewMode = $body.hasClass(modeClassNamePrefix + 'preview');

        MCWCM.mode = {
            isEditMode: $body.hasClass(modeClassNamePrefix + 'edit'),
            isDesignMode: $body.hasClass(modeClassNamePrefix + 'design'),
            isPreviewMode: $body.hasClass(modeClassNamePrefix + 'preview')
        };
        MCWCM.breakpoints = {
            xs: 480,
            sm: 768,
            md: 1024,
            lg: 1401,
        };
        _.extend(MCWCM.breakpoints, {
            isLarge: $body.width() >= MCWCM.breakpoints.lg,
            isMedium: $body.width() >= MCWCM.breakpoints.lg,
            isSmall: $body.width() >= MCWCM.breakpoints.sm,
            isXSmall: $body.width() >= MCWCM.breakpoints.xs,
            isTouch: $('html').hasClass('touch'),
            isTablet: $body.width() < MCWCM.breakpoints.sm || $('html').hasClass('touch'),
            isMobile: $body.width() < MCWCM.breakpoints.xs || $('html').hasClass('touch')
        });
    });
})(this);
