$(".goto").click(function (e) {
    e.preventDefault();
    var id = $(this).attr("href"),
        topSpace = 140;
    $('html, body').animate({
        scrollTop: $(id).offset().top - topSpace
    }, 800);
});