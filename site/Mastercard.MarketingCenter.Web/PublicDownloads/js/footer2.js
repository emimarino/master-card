(function () {
    var Grouper = {
        byColumn: function ($row, getSibling, ignoreSibling) {
            var groups = [[]];
            var groupIndex = 0;
            if (!$row.length || !getSibling.length || !ignoreSibling.length) {
                return groups;
            }
            // grab the first getSibling, as well as all getSiblings
            // that follow an ignoreSibling element
            var selector = getSibling + ':first,' +
                ignoreSibling + ':visible + ' + getSibling + ',' +
                ignoreSibling + ':visible + ' + ignoreSibling + ':hidden + ' + getSibling;
            groups = $row.children(selector)
                .map(function () {
                    // map each of those items into an array that contains
                    // itself, plus all getSiblings up until the next ignoreSibling
                    return $(this).add($(this).nextUntil(ignoreSibling + ':visible'));
                });
            return groups;
        }
    };

    MCCOM.Grouper = Grouper;
})();


// When on screens with width < 768, size the modal to the screen size.  Then manually listen for
// touch events on the modal content to force scrolling.  This is done to work around scrolling
// bugs with stock Android 2.x browsers

// https://code.google.com/p/android/issues/detail?id=6864
// http://stackoverflow.com/questions/17965599/bootstrap-3-long-modal-scrolling-background-on-android
// http://chris-barr.com/2010/05/scrolling_a_overflowauto_element_on_a_touch_screen_device/

var isTouchDevice = function () {
    try {
        document.createEvent("TouchEvent");
        return true;
    } catch (e) {
        return false;
    }
};

var touchScroll = function (selector) {
    if (isTouchDevice()) {
        var scrollStartPosY = 0;
        var scrollStartPosX = 0;
        $('body').delegate(selector, 'touchstart', function (e) {
            scrollStartPosY = this.scrollTop + e.originalEvent.touches[0].pageY;
            scrollStartPosX = this.scrollLeft + e.originalEvent.touches[0].pageX;
        });
        $('body').delegate(selector, 'touchmove', function (e) {
            if ((this.scrollTop < this.scrollHeight - this.offsetHeight &&
                this.scrollTop + e.originalEvent.touches[0].pageY < scrollStartPosY - 5) ||
                (this.scrollTop != 0 && this.scrollTop + e.originalEvent.touches[0].pageY > scrollStartPosY + 5)) {
                e.preventDefault();
            }
            if ((this.scrollLeft < this.scrollWidth - this.offsetWidth &&
                this.scrollLeft + e.originalEvent.touches[0].pageX < scrollStartPosX - 5) ||
                (this.scrollLeft != 0 && this.scrollLeft + e.originalEvent.touches[0].pageX > scrollStartPosX + 5)) {
                e.preventDefault();
            }
            this.scrollTop = scrollStartPosY - e.originalEvent.touches[0].pageY;
            this.scrollLeft = scrollStartPosX - e.originalEvent.touches[0].pageX;
        });
    }
};

var removeTouchScroll = function (selector) {
    $(selector).off('touchstart touchmove');
};

jRespond([{
    label: 'handheld',
    enter: 0,
    exit: 767
}]).addFunc({
    breakpoint: 'handheld',
    enter: function () {
        $('.modal-content').css('max-height', $(window).height() * 0.9);
        $(window).on('orientationchange', function () {
            $('.modal-content').css('max-height', $(window).height() * 0.9);
        });
        touchScroll('.modal-content');
    },
    exit: function () {
        $('.modal-content').css('max-height', "");
        $(window).off('orientationchange');
        removeTouchScroll('.modal-content');
    }
});

/* globals jQuery, MCCOM */
(function ($, MCCOM) {
    var AccessAccountInstance = function ($root, options) {
        this.options = options;
        this.$root = $root;
        this.$dropdown = this.$root.find('select');
        this.$goBtn = this.$root.find('.access-account__go-button');
        this.$goBtn.click($.proxy(this.onGoPressed, this));
        this.$modal = $(this.options['modal-selector']);
        if (this.options['account-list-path']) {
            this.fetchBanks();
        }
    };

    $.extend(AccessAccountInstance.prototype, {
        fetchBanks: function () {
            $.ajax({
                url: this.options['account-list-path']
            }).done(_.bind(this.populateDropdown, this));
        },
        populateDropdown: function (banks) {
            var html = [];

            _.each(banks, function (bank) {
                html.push('<option value="', bank.value, '">', bank.name, '</option>');
            }, this);

            this.$dropdown.append(html.join(''));

            this.$dropdown.selectpicker('refresh');
        },
        onGoPressed: function () {
            var bankName = this.$dropdown.find('option:selected').text();
            var bankURL = this.$dropdown.find('option:selected').attr('value');
            var originalMessage = this.$modal.find('.original-msg').html();
            var nothingSelected = this.$dropdown[0].selectedIndex === 0;

            // Only open modal if a bank was selected.
            if (nothingSelected) {
                return false;
            }

            // replace
            var newMessage = originalMessage.replace('PLACEHOLDER', '<span class="bank-name">' + bankName + '</span>');

            // hide modal when navigating to bank site.
            this.$modal.find('.access-account__go').attr('href', bankURL);
            this.$modal.find('.access-account__go').click($.proxy(function () {
                this.$modal.modal('hide');
            }, this));

            this.$modal.find('#access-account-disclaimer-header').html(newMessage);

            this.$modal.modal({
                // don't close modal unless the user uses an explicit CTA
                backdrop: 'static'
            });
        }
    });

    MCCOM.AccessAccount = MCWCM.ComponentWrapperFactory('.access-account', {
        'account-list-path': '',
        'modal-selector': '#access-account-disclaimer-modal'
    }, AccessAccountInstance);
})(jQuery, MCCOM);
(function () {
    var ArticleInstance = function ($root) {
        this.$root = $root;
        this.$sidebar = this.$root.find('.article-sidebar');
        this.$wrapper = this.$root.find('.article-wrapper');
        this.$elements = this.$root.find('.article-sidebar, .article-wrapper');
        this.$root.imagesLoaded($.proxy(this.scrubIcons, this));
        $(window).resize('resize', _.debounce(_.bind(this.onResize, this), 100));
    };
    $.extend(ArticleInstance.prototype, {
        onResize: function () {
            this.$elements.css({ height: 'auto' });
            this.setHeights();
        },
        scrubIcons: function () {
            this.$root.find('.janrainProvider').html('<div></div>');
            this.setHeights();
        },
        setHeights: function () {
            var sidebarHeight = 0;
            this.$sidebar.find('.janrainProvider').each(function () {
                sidebarHeight = sidebarHeight + $(this).outerHeight();
            });
            this.$sidebar.outerHeight(sidebarHeight);
            var tallest = MCWCM.Sizer.setHeights(this.$elements, { reset: false, resize: true });
        }
    });

    MCCOM.Article = MCWCM.ComponentWrapperFactory('.article', {}, ArticleInstance);
})();










(function (MCCOM) {
    var JanrainInstance = function ($root) {
        this.$root = $root;
        this.$root.imagesLoaded($.proxy(this.scrubIcons, this));
    };
    $.extend(JanrainInstance.prototype, {
        scrubIcons: function () {

            //this.$root.find('.janrainProvider').html('<div></div>');
            setTimeout(function () {

                console.log("social2");
                $(".janrainProviderList").find(".janrainProvider").html("<div></div>");
            }, 500);
        }
    });



    MCCOM.Janrain = MCWCM.ComponentWrapperFactory('.janrain-social-sharing', {}, JanrainInstance);
})(MCCOM);





/* globals jQuery, MCCOM */
(function ($, MCCOM) {
    var MegaMenuHomeInstance = function ($root, options) {
        this.$root = $root;

        $root.imagesLoaded(_.bind(this.resize, this));
        this.bindMouseOver();
        this.bindOnClick();
        $('.menu-list-wrapper').accessibleMegaMenu();
        // 8/11/14 Noah: this was causing IE to stack height values so the dropdowns became increasingly tall
        // Bind throttle resize to the window event
        // _.bindAll(this, 'resize');
        // var throttledResize = _.throttle(this.resize, 100);
        // $(window).resize(throttledResize);

    };

    MegaMenuHomeInstance.prototype = {
		/**
		 * Resize all menu items to match each other vertically
		 */
        resize: function () {

            var $root = this.$root;
            var defaultHeight = $('.dropdown-default').outerHeight();

            $root.find('.dropdown-active').each(function () {
                $(this).addClass('active-view');

                // set image-wrapper heights to tallest image
                var tallestImage = 0;
                $(this).find('.image').each(function () {
                    if ($(this).find('img').outerHeight() > tallestImage) {
                        tallestImage = $(this).outerHeight();
                    }
                });
                $(this).find('.image')
                    .outerHeight(tallestImage)
                    .css({
                        'line-height': tallestImage + 'px'
                    });

                // set text-wrapper heights to tallest span
                var tallestLink = 0;
                $(this).find('.link span').each(function () {
                    if ($(this).outerHeight() > tallestLink) {
                        tallestLink = $(this).outerHeight();
                    }
                });
                $(this).find('.link').outerHeight(tallestLink);

                var activeHeight = $(this).outerHeight();
                var activeHeightA = $(this).height();

                // determine top-bottom padding based on half of the deficit between default and active
                var deficit = defaultHeight - activeHeight;
                if (deficit <= 0) {
                    deficit = 40;
                }

                $(this).find('.promo-text').css({ 'height': activeHeightA + deficit + 'px' });
                $(this).find('.subpage').css({ 'height': activeHeightA + deficit + 'px' });

                $(this).removeClass('active-view');
            });

        },

        bindMouseOver: function () {
            var $root = this.$root;
            $root.find('> ul > li').on('mouseover', function () {
                $(this).find('.dropdown-active').addClass('active-view');

                $(this).on('mouseout', function () {
                    $(this).find('.dropdown-active').removeClass('active-view');
                });
            });
        },

        bindOnClick: function () {
            $('span.mainNav').on('click', function (event) {
                var link = $(this).data('href');
                window.location.href(link);
            });
        }
    };

    MCCOM.MegaMenuHome = MCWCM.ComponentWrapperFactory('.menu-list-wrapper', {}, MegaMenuHomeInstance);
})(jQuery, MCCOM);
/* globals jQuery, MCCOM */
(function ($, MCCOM) {
    var IdentityTheftInstance = function ($root, options) {
        this.$root = $root;
        this.options = options;

        this.$text = this.$root.find('.firstsix');
        this.$err = this.$root.find('.err');
        this.$goBtn = this.$root.find('.btn');

        this.$goBtn.click($.proxy(this.onGoPressed, this));
    };

    $.extend(IdentityTheftInstance.prototype, {
        onGoPressed: function (e) {
            var mcregex = /^[25]\d{5}$/;

            if (mcregex.test(this.$text.val())) {
                this.$err.hide();
            } else {
                e.preventDefault();
                this.$err.show();
            }
        }
    });

    MCCOM.IdentityTheft = MCWCM.ComponentWrapperFactory('.identity-theft', {}, IdentityTheftInstance);
})(jQuery, MCCOM);
/* globals jQuery, MCCOM */
(function (MCCOM) {
    var StickyNavInstance = function ($root, options) {
        this.$root = $root;
        this.targetHeight = this.$root.offset().top;

        if (this.$root.width() > 480) {
            $('header').imagesLoaded($.proxy(this.onImagesLoaded, this));
        }
    };

    $.extend(StickyNavInstance.prototype, {
        onImagesLoaded: function () {
            if ($('.content-feed__frame').length) {
                this.targetHeight = $('.content-feed__scroll-frame').outerHeight();
            }

            this.$root.affix({
                offset: {
                    top: 0
                }
            });

            // Uncomment if Content Feed in Header is re-activated
            // this.$root.affix({
            // 	offset: {
            // 		top: this.targetHeight
            // 	}
            // });

            // hide secondary and subNav before sticky becomes affixed
            this.$root.on('affix.bs.affix', $.proxy(this.onAffixed, this));

            // show everything when sticky becomes un-affixed
            this.$root.on('affix-top.bs.affix', $.proxy(this.onAffixedTop, this));
        },

        onAffixed: function (event) {
            // arbitrary assignment based on position-fixed sticky-nav height (95px)
            // and an additional amount of aesthetic awesomeness
            var margin = '95px';
            var scroll = 0;
            if (this.targetHeight > 0) {
                scroll = this.targetHeight + 1;
            }

            // Uncomment if Content Feed in Header is re-activated
            // $('header').next('div').css({ 'margin-top': margin });
        },

        onAffixedTop: function (event) {
            // Uncomment if Content Feed in Header is re-activated
            // $('header').next('div').css({ 'margin-top': 0 });
        }
    });

    MCCOM.StickyNav = MCWCM.ComponentWrapperFactory('#sticky-header-nav', {}, StickyNavInstance);
})(MCCOM);

/* globals jQuery, MCCOM */
(function (MCCOM) {
    var WelcomeBannerInstance = function ($root, options) {
        if ($('body').hasClass('wcmmode-edit')
            || $('body').hasClass('wcmmode-design')) {
            $root.css('display', 'none');
            return {};
        }

        this.$root = $root;
        this.hide = false;
        this.bindEvents();
    };

    $.extend(WelcomeBannerInstance.prototype, {
        bindEvents: function () {
            // On some pages the 'affixed-top.bs.affix' event does not fire when
            // reloading the page and not scrolled to the top.  So check the
            // scroll position after a short time to see if we need to slide the
            // banner up.
            setTimeout(_.bind(function (e) {
                if ($(window).scrollTop() > 0) {
                    this.onAffixed();
                }
                else {
                    this.onAffixedTop();
                }
            }, this), 1000);

            $('#sticky-header-nav').on('affixed.bs.affix', $.proxy(this.onAffixed, this));
            $('#sticky-header-nav').on('affixed-top.bs.affix', $.proxy(this.onAffixedTop, this));
        },

        onAffixed: function (event) {
            this.hide = true;
            this.$root.slideUp();
        },

        onAffixedTop: function (event) {
            if (this.hide) {
                return;
            }
            this.$root.slideDown();
        }
    });

    MCCOM.WelcomeBanner = MCWCM.ComponentWrapperFactory('#welcome-banner', {}, WelcomeBannerInstance);
    $(document).ready(function () {

        var locVal = document.documentElement.lang;
        if (locVal.indexOf("ar_") > -1) {
            $('body > div ').removeAttr("style");
            $('body > div ').css("direction", "rtl");

        }
    });
})(MCCOM);

/* globals jQuery, MCCOM */
(function ($, MCCOM) {

    var template = [
        '<div class="loading-indicator">',
        '<div class="loading-mask">',
        '</div>',
        '<div class="loading-spinner">',
        '</div>',
        '</div>'
    ];

    var LoadingIndicator = function ($root) {
        this.$root = $root;
    };

    $.extend(LoadingIndicator.prototype, {
        init: function () {
            this.$el = this.createSpinnerEl();
            if (!this.$root.find('.loading-indicator').length) {
                this.$root.append(this.$el);
            }
        },
        createSpinnerEl: function () {
            return $(template.join(''));
        },
        show: function () {
            this.$el.addClass('is-showing');
        },
        hide: function () {
            this.$el.removeClass('is-showing');
        }
    });

    MCCOM.LoadingIndicator = new LoadingIndicator($('body'));

})(jQuery, MCCOM);
(function () {
    'use strict';

    var CountryLanguageSelectorInstance = function ($root, options) {
        this.$root = $root;
        this.options = options;
        this.isSplash = this.$root.hasClass('splash');
        this.baseOffset = 30;

        this.$selector = this.$root.find('.country-language-selector');
        this.$mobileSelector = $('.mobile-country-language-selector');
        this.$backgroundImage = this.$selector.find('img');

        this.$modal = this.$root.find('.modal--country-language');
        this.$modalContent = this.$root.find('.modal-content');
        this.$regionDropDown = this.$modal.find('[name="region"]');
        this.$languageDropDown = this.$modal.find('[name="country-language"]');
        this.defaultRegion = this.$regionDropDown.html();
        this.defaultLanguage = this.$languageDropDown.html();

        this.$confirmationButton = this.$modal.find('#countryLanguageConfirmationButton');

        this.configURL = this.options['country-language-path'];

        if (this.isSplash && (MCWCM.mode.isEditMode || MCWCM.mode.isDesignMode)) {
            return;
        }

        if (this.configURL) {
            this.getCountryLanguageData()
                .done(_.bind(function (countryLanguageData) {
                    this.onGetCountryLanguageData(countryLanguageData);
                    if (this.isSplash) {
                        this.openSelectionModal();
                        this.$root.imagesLoaded($.proxy(this.resizeBackground, this));
                    }
                }, this));
            this.moveModal();
        }

        if (this.isSplash) {
            $(window).on("windowresize", _.debounce($.proxy(this.onResize, this), 40));
        }
    };

    $.extend(CountryLanguageSelectorInstance.prototype, {
        onResize: function () {
            this.resizeBackground();
            this.positionModal();
        },
        resizeBackground: function () {
            MCWCM.Sizer.setBackgroundImage($(window), this.$backgroundImage);
        },
        positionModal: function () {
            var offset = Math.floor((window.innerHeight - this.$modalContent.height()) / 2);
            if (offset > this.baseOffset) {
                this.$modalContent.css('margin', offset + 'px auto');
            }
        },
        getCountryLanguageData: function () {
            return $.ajax(this.configURL);
        },
        onGetCountryLanguageData: function (regionLanguageData) {
            this.populateModal(regionLanguageData);
            this.$selector.on('click', _.bind(this.openSelectionModal, this));
            this.$mobileSelector.on('click', _.bind(this.openSelectionModal, this));
            this.$regionDropDown.on('change', _.bind(this.onRegionChange, this));
            this.$confirmationButton.on('click', _.bind(this.onConfirmationClick, this));
        },
        openSelectionModal: function (event) {
            if (event) {
                event.preventDefault();
            }

            this.setDefaultRegion();
            this.$regionDropDown.trigger('change');
            this.setDefaultLanguage();
            this.$languageDropDown.selectpicker('refresh');

            this.$modal.on('hidden.bs.modal', function () {
                $('.top-bar').addClass('hidden-xs')
                    .addClass('hidden-sm');
            });

            this.$modal.on('show.bs.modal', function () {
                $('.top-bar').removeClass('hidden-xs')
                    .removeClass('hidden-sm');
            });
            if (this.isSplash) {
                this.$modal.modal({
                    backdrop: 'static',
                    keyboard: false,
                    show: true
                }).addClass('active');
            } else {
                this.$modal.modal('show').addClass('active');
            }

            this.positionModal();
        },
        populateModal: function (regionLanguageData) {
            this.configData = regionLanguageData || this.configData;

            this.buildRegionDropDown();
        },
        // Move modal to same level as modal backdrop to fix index issue
        moveModal: function () {
            this.$modal.detach().appendTo('body');
        },
        setDefaultRegion: function () {
            this.$regionDropDown.val(this.options['default-region']);
        },
        setDefaultLanguage: function () {
            this.$languageDropDown.val(this.options['default-country-language']);
        },
        buildRegionDropDown: function () {
            var regionDropDownHTML = '';
            _.each(this.configData, function (region, regionKey) {
                var regionOption = $('<option></option>').val(regionKey).text(region.name);
                regionDropDownHTML += regionOption.get(0).outerHTML;

                region.languageDropDownHTML = this.buildLanguageDropDownHTML(regionKey);
            }, this);
            if (this.isSplash) {
                //this.$regionDropDown.prepend(this.defaultRegion);
                regionDropDownHTML = this.defaultRegion + regionDropDownHTML;
            }
            this.$regionDropDown.html(regionDropDownHTML).selectpicker('refresh');
        },

        // Build and store the html for a region language drop down
        buildLanguageDropDownHTML: function (regionKey) {
            var html = '';

            _.each(
                _.sortBy(this.configData[regionKey].sites, function (language) {
                    return language.title;
                }),
                function (language) {
                    var countryLanguage = language.title + ' - ' + language.languageinlanguage;
                    var languageOption = $('<option></option>').val(countryLanguage).text(countryLanguage);
                    languageOption.attr('data-url', language.url);
                    html += languageOption.get(0).outerHTML;
                },
                this);
            if (this.isSplash) {
                html = this.defaultLanguage + html;
            }

            if (!html.length) {
                html = html = this.defaultLanguage;
            }

            return html;
        },

        onRegionChange: function (event) {
            if (this.$regionDropDown.val() === null || this.$regionDropDown.val() === undefined || !this.$regionDropDown.val()) {
                this.$languageDropDown.html(this.defaultLanguage).selectpicker('refresh');
                return;
            }

            var html = this.configData[this.$regionDropDown.val()].languageDropDownHTML;

            this.$languageDropDown.html(html).selectpicker('refresh');
        },

        onConfirmationClick: function (event) {
            if (this.$languageDropDown.val().length) {
                var countURL = this.$languageDropDown.find(':selected').data('url');

                if ($('#rememberme').is(':checked')) {
                    var newDate = new Date();
                    var annualMS = 365 * 24 * 60 * 60 * 1000;

                    newDate.setTime(newDate.getTime() + annualMS);
                    var expires = "expires=" + newDate.toUTCString();

                    document.cookie = "preferredCountryURL=" +
                        countURL +
                        "; " + expires;
                }

                window.location = countURL;
            }
        },
    });

    MCCOM.CountryLanguageSelector = MCWCM.ComponentWrapperFactory('.country-language', {
        'country-language-path': '',
        'default-region': '',
        'default-country': '',
        'default-country-language': ''
    }, CountryLanguageSelectorInstance);
})();

/* globals jQuery, _, MCCOM */


//Requires Logo component to be on the page and either Mega Menu or Homepage Menu
(function ($, _, MCCOM) {
    var MobileMenuInstance = function ($root, options) {
    };

    $.extend(MobileMenuInstance.prototype, {
        init: function ($root, options) {
            this.$root = $root;
            this.options = options;

            this.footerComponentsSelector = this.options['footer-components-selectors'].join(', ');

            this.$menuContainer = $(options['menu-container-selector']);
            this.$mainMenu = this.$menuContainer.find('.mobile-main-menu');
            this.$mobileMainMenuCountryLanguage = this.$menuContainer.find('.mobile-country-language-selector');
            this.$desktopMainMenuCountryLanguage = $('.country-language-selector');
            this.$menuHeader = this.$menuContainer.find('.mobile-menu-header');
            this.$mobileMenuFooter = this.$menuContainer.find('.mobile-footer');

            var homeURL = $('.logo a').attr('href');
            this.$menuHeader.find('.home').attr('href', homeURL || "");

            this.currentPosition = [];
            this.structure = this.buildFromDesktopMenu();
            this.renderMenuCountryLanguageSelector();
            this.renderMenu();
            this.renderFooter();

            this.bindEvents();
        },
        bindEvents: function () {
            $('.mobile-menu-btn').on('click', _.bind(this.onMobileMenuPressed, this));
            this.$menuContainer.find('.back').on('click', _.bind(this.onBackPressed, this));
            this.$mainMenu.on('click', '.mobile-menu-down-level', _.bind(this.onDeepenTreePressed, this));
            this.$mobileMenuFooter.find('.expandable .title').on('click', _.bind(this.onExpandableMenuPressed, this));
        },
        buildFromDesktopMenu: function () {
            throw new Error("Abstract method");
        },
        onMobileMenuPressed: function (event) {
            $('html').toggleClass('show-mobile-menu');
            $('.mobile-menu-btn').toggleClass('active');
        },
        buildHtml: function (currentPage) {
            var templateString = this.$menuContainer.find('.mobile-main-menu-template').html();
            var template = _.template(templateString);

            return template({ currentPage: currentPage });
        },
        currentStructure: function () {
            if (this.currentPosition.length === 0) { return this.structure; }
            var positions = this.currentPosition.slice(0);
            var structure = this.structure.subpages[positions.shift()];
            _.each(positions, function (position) {
                structure = structure.subpages[position];
            });

            return structure;
        },
        onDeepenTreePressed: function (event) {
            var $target = $(event.currentTarget).closest('li');
            this.currentPosition.push($target.index());
            this.renderMenu();
        },
        renderMenu: function () {
            this.$menuHeader.toggleClass('show-back-button', this.currentPosition.length !== 0);
            var html = this.buildHtml(this.currentStructure());
            this.$mainMenu.html(html);
        },
        renderMenuCountryLanguageSelector: function () {
            this.$mobileMainMenuCountryLanguage.append(this.$desktopMainMenuCountryLanguage.find('.language').clone(),
                this.$desktopMainMenuCountryLanguage.find('.country-flag').clone());
        },
        renderFooter: function () {
            var $components = $('footer').find(this.footerComponentsSelector);
            _.each($components.toArray(), function (elem, index) {
                if (this.isExpandableMenuItem($(elem))) {
                    $(elem).addClass('expandable');
                    $(elem).find('.title').append('<div class="toggle-icon"></div>');
                }

                $(elem).clone(true, true).appendTo(this.$mobileMenuFooter);
            }, this);

            this.hackMobileMenuAccessAccount();
        },

        hackMobileMenuAccessAccount: function () {
            $('.mobile-footer .dropdown-menu').css({ maxHeight: '203px', overflowY: 'scroll', top: '-203px' });
            $('footer .access-account select.selectpicker').change(function () {
                var accountSelected = $('.mobile-footer .access-account select.selectpicker option[value="' + $(this).val() + '"]')[0];
                if (!accountSelected) {
                    return;
                }
                $('.mobile-footer .access-account .bootstrap-select .filter-option').text(accountSelected.text);
            });
        },
        isExpandableMenuItem: function ($element) {
            return $element.find('.title').length > 0;
        },
        onExpandableMenuPressed: function (event) {
            $(event.target).closest('.expandable').toggleClass('open');
        },
        onBackPressed: function () {
            this.currentPosition.pop();
            this.renderMenu();
        }
    });

    // Mobile Mega Menu
    var MobileMenuMegaInstance = function ($root, options) {
        var init = _.bind(this.init, this);
        _.delay(init, 1000, $root, options);
    };

    $.extend(MobileMenuMegaInstance.prototype, MobileMenuInstance.prototype, {
        buildFromDesktopMenu: function () {
            var buildSubpages = function ($sublist) {
                var subpages = [];

                $sublist.find('> li').each(function () {
                    subpages.push({
                        link: $(this).find('> a').get(0),
                        subpages: buildSubpages($(this).find(' > ul'))
                    });
                });
                return subpages;
            };

            var currentPage = {
                link: '',
                subpages: []
            };
            this.$root.find('.menu-list-wrapper > ul > li').each(function () {
                var $link = $(this).find('> a');
                if ($link.length === 0) { return; }
                var $sublist = $(this).find('.mega-menu__dropdown > ul');
                var subpages = [];

                currentPage.subpages.push({
                    link: $link.get(0),
                    subpages: buildSubpages($sublist)
                });
            });

            return currentPage;
        }
    });

    // Mobile Homepage Menu
    var MobileMenuHomeInstance = function ($root, options) {
        var init = _.bind(this.init, this);
        _.delay(init, 1000, $root, options);
    };

    $.extend(MobileMenuHomeInstance.prototype, MobileMenuInstance.prototype, {
        buildFromDesktopMenu: function () {
            var currentPage = {
                link: '',
                subpages: []
            };
            this.$root.find('.menu-list-wrapper > ul > li').each(function () {
                var $link = $(this).find('> a');
                var $sublinks = $(this).find('.dropdown .subpage');
                var subpages = [];

                $sublinks.each(function () {
                    var $this = $(this);
                    var $link = $('<a>').attr('href', $this.attr('href')).text($this.text());

                    subpages.push({
                        link: $link.get(0),
                        subpages: []
                    });
                });

                currentPage.subpages.push({
                    link: $link.get(0),
                    subpages: subpages
                });
            });

            return currentPage;
        }
    });

    var defaultOptions = {
        'menu-container-selector': '.mobile-menu',
        'footer-components-selectors': ['.sociallinks', '.linklist', '.access-account']
    };

    MCCOM.MobileMenuMega = MCWCM.ComponentWrapperFactory('.main-nav .menu-mega', defaultOptions, MobileMenuMegaInstance);
    MCCOM.MobileMenuHome = MCWCM.ComponentWrapperFactory('.main-nav .menu-homepage', defaultOptions, MobileMenuHomeInstance);
})(jQuery, _, MCCOM);

var deviceDetector = function () {
    var b = navigator.userAgent.toLowerCase(),
        a = function (a) {
            void 0 !== a && (b = a.toLowerCase());
            return /(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/.test(b) ? "tablet" : /(mobi|ipod|phone|blackberry|opera mini|fennec|minimo|symbian|psp|nintendo ds|archos|skyfire|puffin|blazer|bolt|gobrowser|iris|maemo|semc|teashark|uzard)/.test(b) ? "phone" : "desktop"
        };
    return {
        device: a(),
        detect: a,
        isMobile: "desktop" != a() ? !0 : !1,
        userAgent: b
    }
}();


$(document).ready(function () {
    if (deviceDetector.device == 'desktop') {
        $("html").removeClass("touch");
    }

});
(function () {
    angular.module('MCCOM.megaMenu', []);

    var MegaMenuInstance = function ($root, options) {
        this.$root = $root;
        angular.bootstrap($root, ['MCCOM.megaMenu']);
    };
    MCCOM.MegaMenu = MCWCM.ComponentWrapperFactory('.mega-menu-content-module', {}, MegaMenuInstance);
})();
//TODO: make element selectors configurable or at least be made into variables.
//TODO: Use either active or open on elements instead of arbitrarily using them for different ones.

(function () {
    var app = angular.module('MCCOM.megaMenu');

    app.controller('menu', ['$scope', '$element', '$timeout', '$http',
        function ($scope, $element, $timeout, $http) {
            var subMenuClass = '.mega-menu__sub-menu';

            function resetDropdown($dropdown) {
                // turn off all sub menus when mouse is outside of dropdown.
                var $reverseSubMenus = $($dropdown.find('.mega-menu__sub-menu').get().reverse());
                $reverseSubMenus.each(function (index, elem) {
                    $(elem).data('aim').activate(null);
                });

                // reset promo to not being compact
                $dropdown.find('.mega-menu__promo-container').removeClass('mega-menu__promo-container--compact');
            }

            // makes menu row's children appear
            function activate($row) {
                $row.addClass('active');
                $element.find(subMenuClass).removeClass('open');
                $row.parents(subMenuClass).addClass('open');
                $row.find(subMenuClass).eq(0).addClass('open');
                activatePromo($row);
            }

            // makes menu row's children disappear
            function deactivate($row) {
                activatePromo($row.closest('li.active'));
                $row.removeClass('active');
                $row.find('.active').removeClass('active');
                $row.find(subMenuClass).removeClass('open');
                activate($row.closest('.active'));
            }

            // prevent menu row from deactivating when hovering over the promo
            function exitMenu($row, event) {
                var $related = $(event.relatedTarget);
                var relatedIsPromo = $related.hasClass('mega-menu__promo-container');
                var relatedIsInPromo = $related.closest('.mega-menu__promo-container').length;

                if (relatedIsPromo || relatedIsInPromo) {
                    return false;
                }

                return true;
            }

            // given a menu row determines how to display the promo
            function activatePromo($row) {
                var $promo = $row.closest(".mega-menu__dropdown").find(".mega-menu__promo-container");
                var $parentSubMenus = $row.parents(subMenuClass);
                if ($promo.length !== 0 && $parentSubMenus.length !== 0) {
                    switch ($parentSubMenus.length) {
                        // first column
                        case 1:
                            $promo.show();
                            $promo.toggleClass('mega-menu__promo-container--compact', $row.hasClass('has-child-menu'));
                            break;
                        // second column
                        case 2:
                            $promo.toggle(!$row.hasClass('has-child-menu'));
                            $promo.addClass('mega-menu__promo-container--compact');
                            break;
                    }
                } else {
                    $promo.hide();
                }
            }

            // Makes the menu keyboard navigable.
            function configureAccessibleMegaMenu() {
                // on focus shows children of a menu row so that the accessible plugin can now allow access via keyboard.
                $element.on('focus', subMenuClass + ' a', function (event) {
                    activate($(event.currentTarget).closest('li'));
                });

                // on blur hides children of a menu row also resents dropdown if element now in focus is outside of the megamenu
                $element.on('blur', subMenuClass + ' a', function (event) {
                    deactivate($(event.currentTarget).closest('li'));
                    if ($(event.relatedTarget).closest('.mega-menu-content-module').length === 0) {
                        resetDropdown($(event.currentTarget).closest('.mega-menu__dropdown'));
                    }
                });

                $element.accessibleMegaMenu({
                    menuClass: "menu",
                    topNavItemClass: "main-menu-item",
                    panelClass: "mega-menu__dropdown",
                    panelGroupClass: "sub-menu__item",
                    hoverClass: "hover",
                    focusClass: "focus",
                    openClass: "open"
                });
            }

            // configure jquery.menu-aim plugin which handles triangular navigation
            function configureMenuAim() {
                $element.find('.mega-menu__sub-menu').each(function (index, elem) {
                    $(elem).menuAim({
                        activate: function (row) {
                            $row = $(row);
                            activate($row);
                        },
                        deactivate: function (row) {
                            $row = $(row);
                            deactivate($row);
                        },
                        exitMenu: function (menu, event) {
                            var $menu = $(menu);
                            return exitMenu($menu, event);
                        }
                    });
                });
            }

            function getMegaMenuJSON() {
                var path = $element.data('menu-path');

                $http.get(path).success(function (data) {
                    $scope.pages = data;
                    $timeout(setup);
                });
            }

            function setup() {
                $element.on('mouseleave', ".mega-menu__dropdown", function (event) {
                    resetDropdown($(event.currentTarget));
                });

                configureAccessibleMegaMenu();
                configureMenuAim();
            }

            var activePaths = location.pathname.substring(location.pathname.indexOf('/') + 1).split('/');

            $scope.isInPath = function (itemUrl) {
                if (angular.isUndefined(itemUrl)) {
                    return;
                }

                var pathItem = false;

                var itemPaths = itemUrl.substring(itemUrl.indexOf('/') + 1).split('/');

                if (itemPaths.length <= activePaths.length) {
                    var ticker = 0;

                    for (var x = 0; x < itemPaths.length; x++) {
                        if (itemPaths[x].toLowerCase().replace(/[.]html/, '') === activePaths[x].toLowerCase().replace(/[.]html/, '')) {
                            ticker = ticker + 1;
                        }
                    }

                    if (ticker === itemPaths.length) {
                        pathItem = true;
                    }
                }

                return pathItem;
            }

            getMegaMenuJSON();

            // TODO: rename and make selectors have context of element
            $scope.toggleNADropdown = function (display) {
                if ($('.dropdown-menu.nonAudience').length) {
                    $('.nonAudience').css('display', display);
                }
            }
        }
    ]);
})();
(function () {
    var app = angular.module('MCCOM.megaMenu');
    app.directive('dropdown', function () {
        return {
            restrict: 'A',
            transclude: true,
            replace: true,
            scope: {
                promoPath: '=',
                page: '='
            },
            template: $('#mega-menu-dropdown-template').html(),
            controller: ['$scope', '$element', '$http', '$timeout',
                function ($scope, $element, $http, $timeout) {
                    function setHeights() {
                        var $candidates = $element.find('.mega-menu__sub-menu, .mega-menu__promo-container');
                        var $resetables = $element.add($candidates);

                        // Expose all elements to get a good height
                        $resetables.css({
                            'position': 'absolute',
                            'visibility': 'hidden',
                            'display': 'block',
                            'height': 'auto'
                        });

                        var maxHeight = 0;
                        // Get the max height
                        $candidates.each(function (index, element) {
                            var $candidate = $(element);
                            if ($candidate.outerHeight() > maxHeight) {
                                maxHeight = $candidate.outerHeight();
                            }
                        });

                        var $promoContainer = $element.find('.mega-menu__promo-container').addClass('mega-menu__promo-container--compact');

                        if ($promoContainer.outerHeight() > maxHeight) {
                            maxHeight = $promoContainer.outerHeight();
                        }

                        $promoContainer.removeClass('mega-menu__promo-container--compact');

                        // Revert elements back to original state
                        $resetables.css({
                            'position': '',
                            'visibility': '',
                            'display': '',
                            'height': maxHeight
                        });
                    }

                    // fetch promotion container;
                    if ($scope.promoPath) {
                        $http.get($scope.promoPath).success(function (data) {
                            var html = $.trim(data);
                            $(html).appendTo($element.find('.mega-menu__promo-container'));
                            picturefill();

                            // if promo has images set heights after they have loaded otherwise perform immediately
                            var $images = $element.find('img');
                            if ($images.length) {
                                $images.imagesLoaded(setHeights);
                            } else {
                                setHeights();
                            }
                        });
                    } else {
                        $scope.$watch('page', function () {
                            $timeout(setHeights);
                        });
                    }

                    $(window).resize(_.throttle(setHeights, 100));
                }
            ]
        };
    });
})();
(function () {
    var app = angular.module('MCCOM.megaMenu');
    app.directive('subMenu', ['$compile',
        function ($compile) {
            return {
                restrict: 'A',
                transclude: false,
                replace: true,
                scope: {
                    page: '='
                },
                template: $('#mega-menu-sub-menu-template').html(),
                // allows recursive compilation of directive
                compile: function (tElement, tAttr) {
                    var contents = tElement.contents().remove();
                    var compiledContents;
                    return function (scope, iElement, iAttr) {
                        if (!compiledContents) {
                            compiledContents = $compile(contents);
                        }
                        compiledContents(scope, function (clone, scope) {
                            iElement.append(clone);
                        });
                    };
                }
            };
        }
    ]);
})();
/* globals jQuery, verge, MCCOM */
(function (MCCOM) {
    var MENU_BAR_HEIGHT = 45;

    var isAndroid = navigator.userAgent.toLowerCase().indexOf("android") > -1;

    var SearchToggleInstance = function ($root, options) {
        this.$root = $root;
        this.$stickyHeaderNav = $('#sticky-header-nav');
        this.$searchContainer = $('#search-container');
        this.options = options;

        this.searchOpen = false;
        this.userScrollTop = 0;

        this.getSearchComponent();
        $('#search-open').on('click', _.bind($.proxy(this.onTogglePressed, this)));
        if (isAndroid) {
            $(window).resize('resize', _.debounce(_.bind(this.onResize, this), 100));
        }
    };

    $.extend(SearchToggleInstance.prototype, {
        getSearchComponent: function () {
            var searchPath = this.options['search-path'];
            // If the search path has not been configured in CQ then bypass
            if (!searchPath) {
                return;
            }
            // AJAX the search page content.
            this.$searchContainer.load(
                searchPath + '.html',
                _.bind(this.onSearchComponentLoaded, this)
            );
        },
        heightWithMenuBar: function () {
            return verge.viewportH() - ($('.top-bar').outerHeight() + MENU_BAR_HEIGHT);
        },
        hideMenuBar: function () {
            this.$searchContainer.animate({
                height: verge.viewportH()
            });
        },
        showMenuBar: function () {
            this.$searchContainer.animate({
                height: this.heightWithMenuBar()
            });
        },
        onSearchComponentLoaded: function () {
            angular.bootstrap(this.$searchContainer.get(0), ['MCCOM.search']);

            var view = this;
            $('input.query').on('focus', function (e) {
                view.input_focused = true;

                if (!MCWCM.breakpoints.isSmall && MCWCM.breakpoints.isTouch) {
                    view.hideMenuBar();
                }


            });

            $('input.query').on('blur', function (e) {
                view.input_focused = false;
                view.showMenuBar();


            });
        },
        ifSearchOpen: function () {
            var windowHeight = window.innerHeight;
            $('.page-wrap').css({ height: windowHeight, overflow: 'hidden' });

            _.defer(_.bind(function () {
                // calling this twice fixes an issue with the
                // content feed not scrolling out of view
                $(window).scrollTop(this.$stickyHeaderNav.offset().top);
                $(window).scrollTop(this.$stickyHeaderNav.offset().top);
            }, this));
        },

        onTogglePressed: function () {
            this.searchOpen = !this.searchOpen;
            if (this.searchOpen) {
                this.userScrollTop = $(window).scrollTop();
                $('.content-feed').hide();
                this.ifSearchOpen();
            }
            else {
                $('.page-wrap').css({ height: 'auto', overflow: 'auto' });
                $('.content-feed').show(_.bind(function () {
                    $(document).scrollTop(this.userScrollTop, 0);
                }, this));
            }

            this.$stickyHeaderNav.toggleClass('fixed');
            this.$root.toggleClass('close-search');

            this.$searchContainer.height(verge.viewportH() - ($('.top-bar').outerHeight() + MENU_BAR_HEIGHT));
            this.$searchContainer.slideToggle('fast');
        },
        onResize: function () {
            if (!this.searchOpen) {
                return;
            }
            this.ifSearchOpen();

            if (this.input_focused &&
                !MCWCM.breakpoints.isSmall &&
                MCWCM.breakpoints.isTouch) {
                this.hideMenuBar();
            }
            else {
                this.showMenuBar();
            }
        }
    });

    MCCOM.SearchToggle = MCWCM.ComponentWrapperFactory('.search-header', {
        'search-path': ''
    }, SearchToggleInstance);
})(MCCOM);

(function () {
    var ArticleModalInstance = function (url) {
        this.url = url;
        this.$root = $('body').find('#general-content-modal');

        this.getArticleModal();
    };

    $.extend(ArticleModalInstance.prototype, {
        getArticleModal: function () {
            $.ajax({
                url: this.url
            }).done(_.bind(this.populateModal, this));
        },

        populateModal: function (articleContent) {
            // parse ajax return, remove leading white-space so jquery-migrate doesn't throw an error
            var $article = $($.parseHTML(articleContent)).filter('*').find('.article');

            // reset modal
            this.$root.find('.article').remove();

            // insert new article
            this.$root.append($article);

            this.$root.modal({
                backdrop: 'static' // don't close modal unless the user uses an explicit CTA
            });

            // activate social links
            this.setIcons();
        },
        setIcons: function () {
            janrain.social.addWidgetTo(this.$root.find('.janrainSocialPlaceholder').get(-1));
            this.$root.imagesLoaded($.proxy(this.scrubIcons, this));
        },
        scrubIcons: function () {
            this.$root.find('.janrainProvider').html('<div></div>');
            this.setHeights();
        },
        setHeights: function () {
            var sidebarHeight = 0;
            this.$root.find('.article-sidebar .janrainProvider').each(function () {
                sidebarHeight = sidebarHeight + $(this).outerHeight();
            });
            this.$root.find('.article-sidebar').css({ height: sidebarHeight });

            var tallest = MCWCM.Sizer.setHeights(this.$root.find('.article-sidebar, .article-wrapper'), { resize: false });
        }
    });

    MCCOM.ArticleModal = ArticleModalInstance;
})();










(function () {
    var ContentFeedInstance = function ($root, options) {
        this.$root = $root;
        this.$feedFrame = this.$root.find('.content-feed__scroll-frame');
        this.$feedContainer = this.$root.find('.content-feed__scroll-container');
        this.$frames = this.$feedContainer.find('.content-feed__frame');
        this.$frameBlocks = this.$feedContainer.find('.content-feed__frame-block');
        this.$items = this.$frames.find('.content-feed-item');
        this.options = options;
        this.increment = 0;
        this.navTrigger = false;

        this.scrollSpeed = this.$root.find('.content-feed-data').data('scrollSpeed');
        if (isNaN(this.scrollSpeed) || this.scrollSpeed < 1 || this.scrollSpeed > 5) {
            this.scrollSpeed = 30;
        } else {
            this.scrollSpeed = this.scrollSpeed * 10;
        }

        if (!this.$frames.length) {
            return;
        }

        this.$prev = this.$root.find('.content-feed__prev.prev');
        this.$next = this.$root.find('.content-feed__next.next');

        // hammer nav
        this.start = 0;
        this.$feedContainer.data('ContentFeedInstance', this);

        // prevent default webkit behaviour
        this.$feedContainer.css('-webkit-user-drag', 'none');

        if (this.$root.hasClass('content-feed--admin')) {
            this.contentOffsetLeft = 0;
        } else {
            this.contentOffsetLeft = $('#content').offset().left;
        }

        this.initFeed();
        if (!$('html').hasClass('touch')) {
            this.bindEvents();
        }

        $(window).on('windowresize', _.debounce(_.bind(this.onResize, this), 200));
    };

    $.extend(ContentFeedInstance.prototype, {
        initFeed: function () {
            this.setItemHrefs();
            this.verticalResponsive();
            this.showContentFeed();

            this.frameWidth = this.$feedFrame.width();
            this.containerWidth = this.getWrapperWidth();
            this.$feedContainer.width(this.containerWidth);
            this.min = Math.min(-this.$feedContainer.width() + this.frameWidth, 0);
            this.$feedContainer.css(this.getPositionConfig(this.$feedContainer.offset().left));
            this.$feedFrame.css({ marginTop: 0 });
        },
        onResize: function () {
            if (this.$root.hasClass('content-feed--admin')) {
                this.contentOffsetLeft = 0;
            } else {
                this.contentOffsetLeft = $('#content').offset().left;
            }

            this.min = Math.min(-this.containerWidth + this.frameWidth, 0);
            this.initFeed();
        },

        bindEvents: function () {
            this.$root.hammer({
                drag: true,
                dragBlockHorizontal: true,
                dragLockMinDistance: 8
            });
            this.$root.on({
                dragstart: _.bind(this.onDragStart, this),
                drag: _.bind(this.onDrag, this),
                dragend: _.bind(this.onDragEnd, this)
            });

            if (!$('html').hasClass('touch')) {
                this.$prev.on('mouseover', $.proxy(this.onPrev, this));
                this.$next.on('mouseover', $.proxy(this.onNext, this));

                this.$prev.on('mouseout', $.proxy(this.offNav, this));
                this.$next.on('mouseout', $.proxy(this.offNav, this));

                this.toggleNavs();
            } else {
                this.$prev.parent().hide();
                this.$next.parent().hide();
            }

            // keyboard navigation
            this.$frames.on('focus', function (e) {
                $(e.target).addClass('focus');
            });

            // keyboard navigation
            this.$frames.find('a.btn').on('blur', function (e) {
                $(e.target).closest('.content-feed__frame').removeClass('focus');
            });

            this.$root.on('click', '.article-modal', $.proxy(this.getArticleModal, this));
        },

        setItemHrefs: function () {
            this.$items.each(this.setHref);
        },
        setHref: function (index, item) {
            var $item = $(item);
            var href = $(this).find(".cta").attr("href");
            var target = $(this).find(".cta").attr("target");
            $(this).find('.cta').on('click', function (e) {
                e.preventDefault();
            });
            if (!href) {
                return;
            }
            $item.off().on('click', function () {
                if (target) {
                    window.open(href);
                } else {
                    window.location = href;
                }
            });
        },

        onPrev: function () {
            this.scrollDirection = -1;
            if (this.navTrigger === false) {
                var _this = this;
                this.navDelayTO = setTimeout($.proxy(_this.onNav, _this), 400);
            } else {
                this.onNav();
            }
        },
        onNext: function () {
            this.scrollDirection = 1;
            if (this.navTrigger === false) {
                var _this = this;
                this.navDelayTO = setTimeout($.proxy(_this.onNav, _this), 400);
            } else {
                this.onNav();
            }
        },
        onNav: function () {
            this.navTrigger = true;
            var scrollSpeed = this.scrollSpeed;
            this.setNewPositionTO = setTimeout($.proxy(this.setNewPosition, this), scrollSpeed);
            this.toggleNavs();
        },
        setNewPosition: function () {

            this.increment = this.increment - this.scrollDirection;
            var newPosition = (-this.contentOffsetLeft + this.$feedContainer.offset().left + this.increment);
            this.$feedContainer.css(this.getPositionConfig(newPosition));
            this.onNav();
        },
        offNav: function (event) {
            this.increment = 0;
            if ($(event.currentTarget).attr('class') != $(event.toElement).parent().attr('class') &&
                $(event.currentTarget).attr('class') != $(event.toElement).attr('class')) {
                this.navTrigger = false;
            }
            clearTimeout(this.navDelayTO);
            clearTimeout(this.setNewPositionTO);
        },

        onDragStart: function (event) {
            this.start = this.$feedContainer.offset().left;
        },
        onDrag: function (event) {
            var gesture = event.gesture;
            var newPosition = gesture.deltaX + this.start;
            this.$feedContainer.css(this.getPositionConfig(newPosition));
        },
        onDragEnd: function (event) {
            var gesture = event.gesture;
            var velocity = gesture.velocityX;
            var direction = (gesture.direction == 'left') ? -1 : 1;
            var newPosition = this.$feedContainer.offset().left +
                (velocity * this.options['kinetic-multiplier'] * direction);

            this.$feedContainer
                .animate(this.getPositionConfig(newPosition), this.options['kinetic-duration']);
            this.$items.blur();
            this.start = 0;
        },

        getPositionConfig: function (newPosition) {
            if (newPosition < this.min) {
                newPosition = this.min;
            } else if (newPosition > 0) {
                newPosition = 0;
            }
            return { left: newPosition }
        },
        getWrapperWidth: function () {
            var width = 0;
            if (this.isResponsive()) {
                this.$frameBlocks.each(function () {
                    width = width + $(this).width();
                });
            } else {
                this.$frames.each(function () {
                    width = width + $(this).width();
                });
            }
            return width;
        },

        showContentFeed: function () {
            this.$root.css({ visibility: 'visible' });
        },

        toggleNavs: function () {
            this.leftPosition = this.getLeftPosition();

            if (this.leftPosition == 0) {
                this.$prev.hide();
                this.$prev.parent().hide();
            } else {
                this.$prev.show();
                this.$prev.parent().show();
            }

            this.rightPosition = this.containerWidth + this.leftPosition;

            if (this.rightPosition == this.frameWidth) {
                this.$next.hide();
                this.$next.parent().hide();
            } else {
                this.$next.show();
                this.$next.parent().show();
            }
        },

        getLeftPosition: function () {
            var offsetLeft = this.$feedContainer.offset().left;

            return offsetLeft - this.contentOffsetLeft;
        },

        verticalResponsive: function () {
            if (this.isResponsive()) {
                this.$root.addClass('content-feed--vertical-responsive');
            }
        },
        isResponsive: function () {
            // FOR VERTICAL RESPONSIVENESS UNCOMMENT NEXT LINE
            // return $(window).height() < 700 || MCWCM.breakpoints.isMobile;
            return MCWCM.breakpoints.isMobile;
        },

        getArticleModal: function (event) {
            event.preventDefault();
            new MCCOM.ArticleModal($(event.currentTarget).attr('href'));
            this.$prev.hide();
            this.$next.hide();
            $('#general-content-modal').on('hidden.bs.modal', $.proxy(this.toggleNavs, this));
        },
        isMobile: function () {
            return this.$root.width() < 768 || $('html').hasClass('.touch')
        },
    });

    MCCOM.ContentFeed = MCWCM.ComponentWrapperFactory('.content-feed',
        {
            'kinetic-multiplier': 100,
            'kinetic-duration': 20,
            'breakpoint': 480
        }, ContentFeedInstance);
})();;
/* globals jQuery, MCCOM */
(function () {

    angular.module('MCCOM.findACard', [
        'ui.router',
        'ngSanitize'
    ]);

    var FindACardInstance = function ($root, options) {
        this.$root = $root;
        angular.bootstrap($root, ["MCCOM.findACard"]);
    };

    MCCOM.FindACard = MCWCM.ComponentWrapperFactory('.find-a-card-module', {}, FindACardInstance);

})();
/* globals jQuery, MCCOM */
(function () {

    angular.module('MCCOM.findACard')
        .config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
            function ($stateProvider, $urlRouterProvider, $httpProvider) {

                // Start at carousel
                $urlRouterProvider.otherwise('/');

                $stateProvider
                    .state('carousel', {
                        // The only state with a url, so we can set it as the default route
                        url: '/',
                        views: {
                            'step': {
                                templateProvider: function () {
                                    return $('#choose-mode-template').html();
                                },
                                controller: [
                                    '$state',
                                    '$rootScope',
                                    'findACardModel',
                                    function ($state, $rootScope, findACardModel) {
                                        findACardModel.modes = [];
                                        findACardModel.resetAnswers();
                                    }
                                ]
                            }
                        }
                    })
                    .state('questions', {
                        params: ['questionsEndpoint'],
                        views: {
                            'step': {
                                templateProvider: function () {
                                    return $('#questions-template').html();
                                },
                                resolve: {
                                    questions: [
                                        '$stateParams',
                                        '$state',
                                        '$rootScope',
                                        'questionsModel',
                                        'findACardModel',
                                        function ($stateParams, $state, $rootScope, questionsModel, findACardModel) {
                                            var questionsEndpoint = $stateParams.questionsEndpoint || $rootScope.questionsEndpoint;
                                            return questionsModel.get(questionsEndpoint).success(function () {
                                                findACardModel.resetAnswers();
                                                if (!questionsModel.anyQuestionsInCurrentMode()) {
                                                    $state.go('results');
                                                }
                                            });
                                        }
                                    ]
                                }
                            }
                        }
                    })
                    .state('results', {
                        params: ['resultsEndpoint'],
                        views: {
                            'step': {
                                templateProvider: function () {
                                    return $('#results-template').html();
                                },
                                resolve: {
                                    results: ['$stateParams', '$rootScope', 'resultsModel',
                                        function ($stateParams, $rootScope, resultsModel) {
                                            var resultsEndpoint = $stateParams.resultsEndpoint || $rootScope.resultsEndpoint;
                                            return resultsModel.get(resultsEndpoint);
                                        }
                                    ]
                                }
                            }
                        }
                    });

                $httpProvider.interceptors.push(['$rootScope', '$q', 'errors', function ($rootScope, $q, errors) {
                    return {
                        'request': function (config) {
                            $rootScope.errors = [];
                            return config;
                        },
                        'responseError': function (rejection) {
                            errors.catchError(rejection.status + ' - ' + rejection.statusText);
                            return $q.reject(rejection);
                        }
                    };
                }]);
            }
        ]);

    angular.module('MCCOM.findACard').run([
        '$rootScope',
        'FacAnalyticsService',
        function ($rootScope, FacAnalyticsService) {

            // Immediately fire off the product type to analytics
            FacAnalyticsService.businessConsumer();

            $rootScope.$on('$stateChangeStart', function () {
                MCCOM.LoadingIndicator.show();
            });
            $rootScope.$on('$stateChangeSuccess', function () {
                MCCOM.LoadingIndicator.hide();
                $(function () {
                    setTimeout(function () {
                        MCWCM.TabScroller.init();
                        var $tabScroller = $('.find-a-card .step .nav-tab-wrapper');
                        var tabScroller = $tabScroller.data('TabScroller');
                        var $tab = $tabScroller.find('.nav-tabs>li.active');
                        if (tabScroller && $tab.length) {
                            tabScroller.scrollToTab($tab, 0);
                        }
                    },
                        0);
                });
            });
            $rootScope.$on('$stateChangeError', function () {
                MCCOM.LoadingIndicator.hide();
            });
        }
    ]);

})();
(function () {


	/**
	 * Find A Card Model - holds user choices on mode and answers to questions.
	 */
    angular.module('MCCOM.findACard').service('findACardModel', ['$state',
        function ($state) {
            if ($('html').hasClass('ie8') || $('html').hasClass('ie9')) {
                setInterval(function () { $(document).trigger('scroll'); }, 250);
            }

            this.modeType = "";
            this.category = "";
            this.type = "";
            this.ids = [];

            this.userAnswers = {};

			/**
			 * Set properites on the model
			 * @param {Object} prop - properties to change
			 */
            this.set = function (prop) {
                $.extend(this, prop);
            };

            this.resetAnswers = function () {
                this.userAnswers = {};
            };

            this.areAllQuestionsAnswered = function () {
                return _.keys(this.userAnswers).length >= this.ids.length;
            };

            this.isActiveMode = function (mode) {
                return mode.category === this.category &&
                    mode.type === this.type
                this.arrayOfModeIds(mode) === this.ids;
            };

            this.isModeType = function (modeType) {
                return this.modeType === modeType;
            };

			/**
			 * update mode (category/type)
			 * @param  {object} mode { category, mode }
			 */
            this.updateMode = function (mode) {
                this.modeType = mode.modeType;
                this.category = mode.category;
                this.type = mode.type;
                this.ids = this.arrayOfModeIds(mode);
            };

			/**
			 * returns an array of mode question ids. sometimes the ids are stored in a comma
			 * separated list, and sometimes they are already an array
			 * @param  {Object} mode
			 * @return {Array}
			 */
            this.arrayOfModeIds = function (mode) {
                if (_.isArray(mode.ids)) {
                    return mode.ids;
                } else {
                    return mode.ids.split(',');
                }
            }

			/**
			 * update mode match non-carousel link, and then immediately go
			 * to questions
			 * @param  {Object} mode
			 */
            this.updateLinkMode = function (mode) {
                // set mode
                this.updateMode(mode);
                // immediately go to questions state
                $state.go('questions');
            };

			/**
			 * should the filter dropdown be shown for active mode?
			 * @return {Boolean}
			 */
            this.isFilterDropdownShown = function () {
                var mode = _.find(this.modes, this.isActiveMode, this);
                if (!mode) {
                    return false;
                }
                return mode.includeFilterDropdown == 'true' || mode.includeFilterDropdown == true;
            };

        }
    ]);

	/**
	 * Questions Model
	 */
    angular.module('MCCOM.findACard').service('questionsModel', [
        '$http',
        '$state',
        'findACardModel',
        function ($http, $state, findACardModel) {

            this.questions = [];


            this.arrayAccessFormPaths = [
                'questions.question',
                'questions.question.answers.answer'
            ];

            this.get = function (questionsEndpoint) {
                var _this = this;

                return $http.get(questionsEndpoint, {
                    transformResponse: function (xml) {
                        var x2js = new X2JS({
                            arrayAccessFormPaths: _this.arrayAccessFormPaths
                        });
                        var questions = x2js.xml_str2json(xml);
                        if (questions && questions.questions && questions.questions.question) {
                            return questions.questions.question;
                        }
                        else {
                            return [];
                        }
                    }
                })
                    .success(function (questions) {
                        _this.questions = questions;
                    });
            };

            this.update = function (mode) {
                findACardModel.updateMode(mode);
                // Reload the state to get new questions for new category
                $state.go('questions', {}, {
                    reload: true
                });
            };

			/**
			 * Are any of the questions in the currently selected mode's list of questions?
			 * @return {Boolean}
			 */
            this.anyQuestionsInCurrentMode = function () {
                var questionIds = _.pluck(this.questions, 'id');
                return _.intersection(questionIds, findACardModel.ids).length;
            };



        }
    ]);

	/**
	 * Results Model
	 */
    angular.module('MCCOM.findACard').service('resultsModel', [
        '$http',
        '$rootScope',
        '$state',
        'findACardModel',
        function ($http, $rootScope, $state, findACardModel) {

			/**
			 * Cards that match the user's answers
			 * @type {Array}
			 */
            this.cards = [];

            this.arrayAccessFormPaths = [
                'cards.card',
                'cards.card.rewards.reward'
            ];

			/**
			 * Get cards
			 */
            this.get = function (resultspath) {
                var _this = this;

                var params = _.extend({
                    CATEGORY: findACardModel.category,
                    TYPE: findACardModel.type,
                }, findACardModel.userAnswers);

                return $http.get(resultspath, {
                    params: params,
                    responseType: 'text',
                    transformResponse: function (xml) {
                        var x2js = new X2JS({
                            arrayAccessFormPaths: _this.arrayAccessFormPaths
                        });
                        var results = x2js.xml_str2json(xml);
                        if (!results) {
                            return [];
                        }
                        if (results.cards && results.cards.card) {
                            return results.cards.card;
                        } else if (results.cards) {
                            return results.cards;
                        } else {
                            return [];
                        }

                    }
                })
                    .success(function (cards) {
                        cards.forEach(function (card, i) {
                            card.sortindex = Math.floor((Math.random() * 10000) + 1);
                        });
                        _this.cards = cards;
                    });
            };

            this.update = function (mode) {
                findACardModel.updateMode(mode);
                // Reload the state to get new results
                $state.go('results', {}, {
                    reload: true
                });
            };

			/**
			 * Does the specified card have the reward?
			 * @param  {object}  card
			 * @param  {string}  reward
			 * @return {Boolean}
			 */
            this.hasReward = function (card, reward) {
                if (!card) {
                    return false;
                }
                return _.any(card.rewards.reward, function (r) {
                    return r.toString() === reward;
                });
            };

            this.getTitle = function (card) {
                if (!card) {
                    return '';
                }
                var name = card.name.toString();
                var parsePosition = this.titleParsePosition(card);
                var title;
                if (parsePosition > -1) {
                    title = name.substring(0, parsePosition);
                } else {
                    title = name;
                }
                // Title is full of copyright symbols
                title = title.replace(/&#174;/g, '<sup>&#174;</sup>');
                return title;
            };

            this.getSubtitle = function (card) {
                if (!card) {
                    return '';
                }
                var name = card.name.toString();
                var parsePosition = this.titleParsePosition(card);
                if (parsePosition > -1) {
                    // jump past the parser
                    return name.substring(parsePosition + 2);
                } else {
                    return '';
                }
            };

            this.titleParsePosition = function (card) {
                var name = card.name.toString();
                var parsePosition = name.indexOf('-');
                if (parsePosition < 0) {
                    parsePosition = name.indexOf('|');
                }
                return parsePosition;
            };

            this.introRateString = function (card) {
                if (!card) {
                    return '';
                }
                if (card.introRatePurchaseTxt.length) {
                    return card.introRatePurchaseTxt;
                }
                else if (card.introRateBalanceTxt.length) {
                    return card.introRateBalanceTxt;
                }

                else if (card.introApr.length) {
                    return card.introApr;
                }
                else {
                    return '-';
                }
            }

            this.getBy = function (prop, value) {
                return _.find(this.cards, function (card) {
                    return card[prop] == value;
                });
            };

            this.isFeatured = function (card) {
                return (card.featured === true) || (card.featured === 'true');
            };

        }
    ]);


	/**
	 * Compare Cards Model
	 */
    angular.module('MCCOM.findACard').service('CompareCardsModel', [
        'resultsModel',
        '$rootScope',
        function (resultsModel, $rootScope) {
            // Maximum number of cards allowed
            this.MAX_COMPARISON_SIZE = 4;

            // Comparison selections i.e. { <cardProgramId>: true, <cardProgramId>: true }
            // Staging ground for determining cards in the comparison collection
            this.comparisonSelections = {};
            // Record of previous selections.
            this.previousSelections = {};

            // Collection of card objects for comparison
            this.cards = [];

            // Initialize collection
            _(this.MAX_COMPARISON_SIZE).times(function (index) {
                this.cards[index] = {};
            }, this);


			/**
			 * Update the selections and card collection
			 */
            this.update = function () {
                var uniqueIds;

                // clear out all false selections first.
                // Then comparisonSelections will be in a stable, clean state.
                this._removeUncheckedSelections();

                // Too many items chosen
                if (this._exceedsMaxSize()) {
                    // remove new selection from comparisonSelections list, so it doesn't end up in the cards array
                    uniqueIds = _.difference(_.keys(this.comparisonSelections), _.keys(this.previousSelections));
                    _.each(uniqueIds, function (cardProgramId) {
                        delete this.comparisonSelections[cardProgramId];
                        this._remove(cardProgramId);
                    }, this);
                }
                // Extra items selected that aren't in cards array
                else if (_.size(this.comparisonSelections) > this.size()) {
                    // find new item and add it to the cards
                    uniqueIds = _.difference(_.keys(this.comparisonSelections), _.keys(this.previousSelections));
                    _.each(uniqueIds, function (cardProgramId) {
                        this._add(cardProgramId);
                    }, this);
                }
                // Extra cards in cards array that need to be removed
                else if (_.size(this.comparisonSelections) < this.size()) {
                    uniqueIds = _.difference(_.pluck(this.cards, 'cardProgramId'), _.keys(this.comparisonSelections));
                    _.each(uniqueIds, function (cardProgramId) {
                        this._remove(cardProgramId);
                    }, this);

                }

                // remember selections for comparing the next time the list changes
                this.previousSelections = _.clone(this.comparisonSelections);

            };

			/**
			 * Number of filled slots
			 * @return {Number}
			 */
            this.size = function () {
                var filledSlots = _.filter(this.cards, function (card) {
                    return card.cardProgramId;
                });
                return filledSlots.length;
            };


            this.isFull = function () {
                return this.size() >= this.MAX_COMPARISON_SIZE;
            };

            this.isInCards = function (card) {
                return _.any(this.cards, function (c) {
                    return c === card;
                });
            };

			/**
			 * Remove cardProgramId from the selection object
			 * @param  {Number} cardProgramId
			 */
            this.removeFromSelection = function (cardProgramId) {
                delete this.comparisonSelections[cardProgramId];
                this.update();
            };

			/**
			 * Returns whether the number of selections exceeds the maximum number allowed
			 * @return {Boolean}
			 */
            this._exceedsMaxSize = function () {
                return _.size(this.comparisonSelections) > this.MAX_COMPARISON_SIZE;
            };

			/**
			 * Remove all the selections that have been unchecked. When they are unchecked, their state
			 * changes to false.
			 */
            this._removeUncheckedSelections = function () {
                this.comparisonSelections = this._objFilter(this.comparisonSelections, function (selection) {
                    return selection === true;
                });
            };

			/**
			 * Add card with supplied cardProgramId to the comparison card collection
			 * @param {Number} cardProgramId
			 */
            this._add = function (cardProgramId) {
                if (this.size() >= this.MAX_COMPARISON_SIZE) {
                    return false;
                }
                var card = resultsModel.getBy('cardProgramId', cardProgramId);
                if (card) {
                    var availableSlot = this._firstAvailableSlot();
                    this.cards[availableSlot] = card;

                    $rootScope.$broadcast('comparecards:add', card);
                }
            };

			/**
			 * Remove card with supplied cardProgramId from the comparison card collection
			 * @param  {Number} cardProgramId
			 */
            this._remove = function (cardProgramId) {
                var card = _.find(this.cards, function (c) {
                    return c.cardProgramId === cardProgramId;
                });
                if (card) {
                    // new array without the card
                    this.cards = _.filter(this.cards, function (c) {
                        return c != card;
                    });
                    this._fillEmptySlots();
                    $rootScope.$broadcast('comparecards:remove', card);
                }
            };



			/**
			 * Returns index of the first slot that is not filled
			 * @return {Number}
			 */
            this._firstAvailableSlot = function () {
                var programIds = _.pluck(this.cards, 'cardProgramId');
                return _.indexOf(programIds, undefined);
            };

			/**
			 * Fill any remaining slots with empty objects
			 */
            this._fillEmptySlots = function () {
                _(this.MAX_COMPARISON_SIZE).times(function (index) {
                    if (!this.cards[index]) {
                        this.cards[index] = {};
                    }
                }, this);
            };

            this._objFilter = function (input, test, context) {
                return _.reduce(input, function (obj, v, k) {
                    if (test.call(context, v, k, input)) {
                        obj[k] = v;
                    }
                    return obj;
                }, {}, context);

            };
        }
    ]);


    angular.module('MCCOM.findACard').service('errors', ['$rootScope',
        function ($rootScope) {
            this.clear = function () {
                $rootScope.$broadcast('error:clear');
            };
            this.catchError = function (message) {
                $rootScope.$broadcast('error', message);
            };
        }
    ]);

})();
(function () {

	/**
	 * Choose Mode Controller - Step 1 of Find A Card
	 */
    angular.module('MCCOM.findACard').controller('chooseModeCtrl', [
        '$scope',
        '$timeout',
        'findACardModel',
        function ($scope, $timeout, findACardModel) {
            $scope.findACardModel = findACardModel;

            $scope.isActiveMode = function (mode) {
                return findACardModel.isActiveMode(mode);
            };

            $timeout(function () {
                findACardModel.productType = $scope.productType;
            })
        }
    ]);

	/**
	 * Questions Controller - Step 2 of Find A Card
	 */
    angular.module('MCCOM.findACard').controller('questionsCtrl', [
        '$scope',
        'findACardModel',
        'questionsModel',
        function ($scope, findACardModel, questionsModel) {
            $scope.findACardModel = findACardModel;
            $scope.questionsModel = questionsModel;
            $scope.isQuestionInActiveMode = function (question, ids) {
                return $scope.findACardModel.ids.indexOf(question.id) > -1;
            };

        }
    ]);


	/**
	 * Results Controller - Step 3 of Find A Card
	 */
    angular.module('MCCOM.findACard').controller('resultsCtrl', [
        '$scope',
        '$timeout',
        'findACardModel',
        'resultsModel',
        'CompareCardsModel',
        function ($scope, $timeout, findACardModel, resultsModel, CompareCardsModel) {
            $scope.findACardModel = findACardModel;
            $scope.resultsModel = resultsModel;
            $scope.CompareCardsModel = CompareCardsModel;

            $scope.sort = {
                attribute: ''
            };
            $scope.filter = {
                reward: ''
            };

            $scope.cardOrderer = function (card) {
                var defaultSort = 'sortindex';
                // Sponsored Cards always show at the top
                if (resultsModel.isFeatured(card)) {
                    return -100000000;
                }
                else if ($scope.sort.attribute === 'name') {
                    return card.name.toString();
                }
                else if ($scope.sort.attribute === 'bank') {
                    return card[$scope.sort.attribute];
                }
                else if ($scope.sort.attribute === 'introRateSortBy') {
                    return $scope.introRateSortValue(card);
                }
                else if ($scope.sort.attribute) {
                    return +card[$scope.sort.attribute];
                }
                else {
                    return card[defaultSort];
                }
            };
            $scope.filterByReward = function (card) {
                if (!$scope.filter.reward.length) {
                    return true;
                }
                else {
                    return resultsModel.hasReward(card, $scope.filter.reward);
                }
            };

			/**
			 * introRateSortValue. Returns the first it finds of:
			 * 1. Intro rate on purchases
			 * 2. Intro rate on balances
			 * 3. Intro rate
			 * @param  {[type]} card
			 */
            $scope.introRateSortValue = function (card) {
                if (card.introRatePurchaseTxt.length) {
                    return +card.introPurchaseSortBy;
                }
                else if (card.introRateBalanceTxt.length) {
                    return +card.introBalanceSortBy;
                }
                else if (card.introApr.length) {
                    return +card.introApr;
                } else {
                    return +10000;
                }

            };

        }
    ]);

	/**
	 * CompareCardsCtrl
	 */
    angular.module('MCCOM.findACard').controller('CompareCardsCtrl', [
        '$scope',
        'CompareCardsModel',
        '$timeout',
        function ($scope, CompareCardsModel, $timeout) {
            $scope.CompareCardsModel = CompareCardsModel;
            $scope._ = _;
        }
    ]);

})();
(function () {

	/**
	 * Error Banner
	 */
    angular.module('MCCOM.findACard').directive('errorBanner', [
        '$rootScope',
        '$timeout',
        function ($rootScope, $timeout) {

            return {
                restrict: 'A',
                scope: {},
                replace: true,
                template: $('#error-banner-template').html(),
                link: function (scope, el, attr) {
                    scope.errorMsg = '';
                    $rootScope.$on('error', showError);
                    // $rootScope.$on('error:clear', showError);

                    function showError(name, errorMsg) {

                        scope.errorMsg = errorMsg;
                        $timeout(function () {
                            fadeOut();
                        }, 4000, false);

                    }
                    function clear() {
                        scope.errorMsg = '';
                    }
                    function fadeOut() {
                        el.fadeOut(1000, function () {
                            scope.$apply(clear);
                        });
                    }
                }
            };
        }]);
	/**
	 * Mode Carousel Directive
	 */
    angular.module('MCCOM.findACard').directive('modeCarousel', ['findACardModel', '$timeout',
        function (findACardModel, $timeout) {

            return {
                restrict: 'A',
                scope: {
                    // Content author' carousel options
                    config: '=',
                    pauseRotation: '='
                },
                link: function (scope, el, attrs, carouselCtrl) {

                    var defaultCarouselOptions = {
                        auto: true,
                        controls: false,
                        pause: 4000,
                        pager: false,
                        onSliderLoad: setInitialMode,
                        onSlideBefore: updateMode
                    };

                    function init() {
                        scope.pauseRotation = pauseRotation;
                        scope.findACardModel = findACardModel;
                        scope.$watch('findACardModel', handleModeChange, true);

                        el.bxSlider(createOptions());
                        bindPauseEvents();

                        var $bxNavPrev = el.closest('.mode-carousel').find('.prev');
                        var $bxNavNext = el.closest('.mode-carousel').find('.next');

                        $bxNavPrev.on('click', onPrev);
                        $bxNavNext.on('click', onNext);

                        scope.$on('destroy', destroy);
                    }

                    function createOptions() {
                        // config values from AEM
                        config = scope.config || {};
                        // AEM stores speed in seconds. convert to ms
                        if (config.pause) {
                            var MS_IN_SEC = 1000;
                            config.pause *= MS_IN_SEC;
                            defaultCarouselOptions.pause = config.pause;
                        }
                        return defaultCarouselOptions;
                    }

                    function setInitialMode() {
                        var currentSlideEl = slideElementOfIndex(el.getCurrentSlide());
                        updateMode(currentSlideEl);
                    }

                    function slideElementOfIndex(index) {
                        return el.find('.mode-carousel__slide:not(.bx-clone):eq(' + index + ')');
                    }

                    function updateMode($slideElement) {
                        var category = $slideElement.data('category'),
                            type = $slideElement.data('type'),
                            ids = $slideElement.data('ids');

                        findACardModel.updateMode({
                            modeType: 'tab',
                            category: category,
                            type: type,
                            ids: (ids + '').split(',')
                        });
                        // Angular isn't aware of bxSlider, so it needs to be told
                        // to look for changes
                        $timeout(function () {
                            scope.$apply();
                            slideTabScroller(indexOfSlide($slideElement));
                        });
                    }

                    function handleModeChange(newMode) {
                        if (!newMode) {
                            return false;
                        }
                        else if (newMode.isModeType('link')) {
                            pauseRotation();
                            return false;
                        }
                        else if (newMode.isModeType('tab')) {
                            goToSlideOfCurrentMode();
                        }
                    }

                    function goToSlideOfCurrentMode() {
                        // clear call stack
                        $timeout(function () {
                            var slideEl = slideOfCurrentMode();
                            var index = indexOfSlide(slideEl);
                            // Go to appropriate slide if not already there
                            if (index !== el.getCurrentSlide()) {
                                pauseRotation();
                                el.goToSlide(index);
                            }
                        });
                    }

                    function slideOfCurrentMode() {
                        var category = findACardModel.category || "",
                            type = findACardModel.type || "";

                        return el.find('.mode-carousel__slide:not(.bx-clone)[data-category="' +
                            category + '"][data-type="' + type + '"]');
                    }

                    function indexOfSlide(slideEl) {
                        var index;
                        el.find('.mode-carousel__slide:not(.bx-clone)').each(function (idx, element) {
                            if (element == slideEl.get(0)) {
                                index = idx;
                            }
                        });
                        return index;
                    }

					/**
					 * Events that should cause the carousel to stop auto-rotation
					 */
                    function bindPauseEvents() {
                        var $bxNav = el.closest('.mode-carousel').find('.next, .prev');
                        $bxNav.on('click', pauseRotation);
                        //console.info($bxNav);

                        var $navTabs = el.closest('.find-a-card-module .step').find('.nav-tabs > li > a');
                        $navTabs.on('click', pauseRotation);
                        //console.info($navTabs);
                        if (Modernizr.touch) {
                            el.hammer().on('swipe', pauseRotation);
                        }
                    }

                    function pauseRotation() {
                        el.stopAuto();
                    }

                    function onPrev() {
                        el.goToPrevSlide();
                    }
                    function onNext() {
                        el.goToNextSlide();
                    }

                    function destroy() {
                        el.destroySlider();
                    }

                    function slideTabScroller(index) {
                        var $tabScroller = $('.find-a-card .step .nav-tab-wrapper');
                        var tabScroller = $tabScroller.data('TabScroller');
                        var $tab = $tabScroller.find('.nav-tabs>li').eq(index);
                        if (tabScroller && $tab.length) {
                            tabScroller.scrollToTab($tab);
                        }
                    }

                    init();

                }
            };
        }
    ]);

    angular.module('MCCOM.findACard').directive('modeAdvertisements', [
        'findACardModel',
        'FacAnalyticsService',
        '$timeout',
        function (findACardModel, FacAnalyticsService, $timeout) {
            return {
                restrict: 'A',
                scope: {
                    'modeAdvertisements': '='
                },
                link: function (scope, el, attrs) {
                    scope.findACardModel = findACardModel;
                    function toggleAds(model) {
                        var topPadding = 20;
                        var bottomPadding = 20;
                        var headerHeight = 75;
                        var navTabsHeight = 46;
                        var footerHeight = $('footer').outerHeight(true);
                        var adHeight = 216;
                        var topPos = headerHeight + navTabsHeight + topPadding + 'px';
                        var bottomPos = footerHeight + bottomPadding + 'px';

                        if (findACardModel.isActiveMode(scope.modeAdvertisements)) {
                            el.show();
                            var adName = el.find('a').attr('name');
                            if (adName !== undefined) {
                                // FacAnalyticsService.advertisementImpression(adName);
                                FacAnalyticsService.providerWatch(findACardModel.category, FacAnalyticsService.advertisementReferral, adName);
                            }
                        } else {
                            el.hide();
                        }

                        $timeout(function () {
                            scope.$apply();
                        });

                        if (!$('html').hasClass('ie8')) {
                            $(window).on('scroll', function (event) {
                                var bodyHeight = $('body').outerHeight();
                                var position = event.currentTarget.pageYOffset + (footerHeight * 2);

                                if (position > bodyHeight) {
                                    bottomPos = footerHeight + bottomPadding + 'px';
                                } else {
                                    bottomPos = $(window).outerHeight() -
                                        (headerHeight + navTabsHeight + topPadding + adHeight) +
                                        'px';
                                }

                                el.css({
                                    'bottom': bottomPos
                                });
                            });
                        }
                    }

                    scope.$watch('findACardModel.category', toggleAds);
                    scope.$watch('findACardModel.type', toggleAds);
                }
            };
        }
    ]);

    angular.module('MCCOM.findACard').directive('stickyBottomBar', [
        '$timeout',
        function ($timeout) {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    function affix() {
                        var bottomOffset = $('footer').outerHeight(true);
                        el.affix({
                            offset: {
                                bottom: bottomOffset
                            }
                        });
                        // el.attr('data-offset-bottom', bottomOffset);
                    }

                    function destroy() {
                        $(window).off('windowresize', affix);
                    }

                    affix();
                    $(window).on('windowresize', affix);
                    scope.$on('destroy', destroy);
                }
            };
        }
    ]);

	/**
	 * Selectpicker
	 */
    angular.module('MCCOM.findACard').directive('selectpicker', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',

            link: function (scope, element, attrs) {
                $timeout(function () {
                    scope.$apply(function () {
                        element.selectpicker();
                    });
                });
            }
        };
    }]);

	/**
	 * Set height of container to take all of the viewport not being used by the header
	 */
    angular.module('MCCOM.findACard').directive('fillViewport', [
        function () {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    function setHeight() {
                        // reset height
                        el.css('min-height', '');
                        // Use all viewport height not taken by the header
                        var viewportRemainingHeight = $(window).outerHeight(true) - $('header').outerHeight(true);
                        el.css('min-height', viewportRemainingHeight + 'px');
                    }
                    function destroy() {
                        $(window).off('resize', setHeight);
                    }
                    $(window).on('resize', setHeight);
                    setHeight();
                    scope.$on('destroy', destroy);
                }
            };
        }
    ]);

    angular.module('MCCOM.findACard').directive('respond', [
        '$timeout',
        function ($timeout) {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    function respond() {
                        var newTest = verge.viewportW() < 768;

                        if (newTest !== scope.isSmall) {
                            scope.$broadcast('MCCOM_RESPOND', newTest);
                        }
                    }

                    scope.$on("MCCOM_RESPOND", function (event, isSmall) {
                        scope.$apply(function () {
                            scope.isSmall = isSmall;
                        });
                    });
                    $timeout(respond, 0);
                    $(window).on('resize', _.debounce(respond, 200));
                }
            };
        }
    ]);

	/**
	 * Set height of container to take all of the viewport not being used by the header
	 */
    angular.module('MCCOM.findACard').directive('moreInfoModal', [
        '$timeout',
        'resultsModel',
        'CompareCardsModel',
        function ($timeout, resultsModel, CompareCardsModel) {
            return {
                restrict: 'A',
                replace: true,
                template: $('#more-info-modal-template').html(),
                scope: {
                    card: '=card',
                    show: '=show'
                },
                link: function (scope, el, attrs) {
                    scope.resultsModel = resultsModel;
                    scope.CompareCardsModel = CompareCardsModel;
                    scope.show = function (card) {
                        scope.moreInfoCard = card;
                        $timeout(function () {

                            el.modal({
                                backdrop: true
                            });
                            scope.$apply();
                        });
                    };
                    function close() {
                        var newTest = verge.viewportW() < 768;
                        if (newTest) {
                            el.find('.close').click();
                            $(window).off('resize', throttledClose);
                        }
                    }

                    var throttledClose = _.debounce(_.bind(close), 200);
                    $(window).on('resize', throttledClose);

                }
            };
        }
    ]);

	/**
	 * Compare Cards Modal
	 */
    angular.module('MCCOM.findACard').directive('compareCardsModal', [
        '$timeout',
        'resultsModel',
        'CompareCardsModel',
        function ($timeout, resultsModel, CompareCardsModel) {
            return {
                restrict: 'A',
                replace: true,
                scope: {
                    show: '=show'
                },
                template: $('#compare-cards-modal-template').html(),
                link: function (scope, el, attrs) {
                    scope.resultsModel = resultsModel;
                    scope.CompareCardsModel = CompareCardsModel;
                    scope.show = function () {
                        $timeout(function () {
                            el.modal({
                                backdrop: true
                            });
                            equalizeCardColumns();
                            stretchModalToFillViewport();
                            $(window).off('resize', equalizeCardColumns);
                            $(window).on('resize', equalizeCardColumns);
                        });
                    };
                    scope.$on('destroy', destroy);

                    function equalizeCardColumns() {
                        var cardColumns = el.find('.card-column__content');
                        var tallestColumn = _.max(cardColumns, function (cardColumn) {
                            return $(cardColumn).height();
                        });
                        cardColumns.css({
                            'min-height': $(tallestColumn).height() + 'px'
                        });
                    }

                    function stretchModalToFillViewport() {
                        var windowHeight = $(window).height();
                        var modalMargins = 60;
                        var cardSummariesHeight = $(el).find('.card-summaries').outerHeight();
                        var modalHeaderHeight = $(el).find('.modal-header').outerHeight();
                        var sum = modalMargins + cardSummariesHeight + modalHeaderHeight;
                        if (windowHeight > sum) {
                            $(el).find('.card-details').outerHeight(windowHeight - sum);
                        }
                    }

                    function destroy() {
                        $(window).off('resize', equalizeCardColumns);
                    }
                }
            };
        }
    ]);

	/**
	 * Angular wrapper around the bootstrap-checkbox plugin
	 */
    angular.module('MCCOM.findACard').directive('bootstrapCheckbox', [
        '$timeout',
        function ($timeout) {
            return {
                restrict: 'A',
                require: '?ngModel',
                link: function (scope, el, attrs, ngModel) {
                    $timeout(function () {

                        el.checkbox({
                            buttonStyle: 'btn-link',
                            buttonStyleChecked: 'btn-active',
                            checkedClass: 'glyphicon-ok'
                        });
                        scope.$apply();
                    });

                    scope.$watch(function () {
                        return ngModel.$modelValue;
                    }, _.debounce(updateCheckboxIcons, 50));

                    function updateCheckboxIcons(newValue) {
                        if (newValue) {
                            showCheckedIcon();
                        }
                        else {
                            showUncheckedIcon();
                        }
                    }

                    function showCheckedIcon() {
                        el.siblings('.button-checkbox').find('.btn')
                            .addClass('btn-active')
                            .removeClass('btn-link');
                        el.siblings('.button-checkbox').find('.glyphicon-ok').css('display', 'inline-block');
                        el.siblings('.button-checkbox').find('.cb-icon-check-indeterminate').css('display', 'none');
                        el.siblings('.button-checkbox').find('.cb-icon-check-empty').css('display', 'none');
                    }
                    function showUncheckedIcon() {
                        el.siblings('.button-checkbox').find('.btn')
                            .removeClass('btn-active')
                            .addClass('btn-link');
                        el.siblings('.button-checkbox').find('.glyphicon-ok').css('display', 'none');
                        el.siblings('.button-checkbox').find('.cb-icon-check-indeterminate').css('display', 'none');
                        el.siblings('.button-checkbox').find('.cb-icon-check-empty').css('display', 'inline-block');
                    }
                }
            };
        }]);

    angular.module('MCCOM.findACard').directive('clickonenter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    $(event.target).click();
                    event.preventDefault();
                }
            });
        };
    });

})();

(function () {
	/**
	 * Find A Card Analytics
	 * Click Events defined below in Directive
	 * State Change Events defined in top of Service
	 * Event Functions defined in Service
	 */
    angular.module('MCCOM.findACard').service('FacAnalyticsService', [
        '$rootScope',
        'findACardModel',
        'questionsModel',
        'resultsModel',
        function ($rootScope, findACardModel, questionsModel, resultsModel) {
            var _this = this;

            // Noop to avoid errors
            if (!window.s) {
                window.s = {
                    tl: function () { }
                };
            }

            // STATE CHANGES
            $rootScope.$on('$stateChangeSuccess', function (event, toState) {
                if (toState.name === 'carousel') {
                    // _this.businessConsumer(); //findACardModel.productType service used
                    _this.providerWatch(findACardModel.productType, _this.businessConsumer);
                }
                if (toState.name === 'results') {
                    // console.log('stateChangeSuccess results', toState, resultsModel.cards.length);
                    if (!resultsModel.cards.length) {
                        _this.noCardsDisplayed(); //no service used
                    }
                    else {
                        // _this.searchResultsDisplay(); //findACardModel.category service used
                        _this.providerWatch(findACardModel.category, _this.searchResultsDisplay);
                    }
                }
            });
            // COMPARE CARDS ADDED
            $rootScope.$on('comparecards:add', function (event, card) {
                // _this.compareCardSelection(card); //findACardModel.category
                _this.providerWatch(findACardModel.category, _this.compareCardSelection, card);
            });

            // allow eventFunctions to not fire until model services return values
            this.providerWatch = function (provider, eventFunction, arg) {
                // console.log('providerWatch', provider, eventFunction, arg);
                // services take a micro-second to populate, wait for it
                var stopProviderWatcher = $rootScope.$watch(function () {
                    return findACardModel.productType;
                }, function (provision) {
                    if (provision === undefined) {
                        return;
                    }
                    eventFunction(arg);
                    stopProviderWatcher();
                });
            };

            this.userAnswersString = function () {
                // console.log('userAnswersString', findACardModel.userAnswers);
                return _.reduce(findACardModel.userAnswers, function (memo, answer) {
                    return memo + '&' + answer;
                }, 'cf');
            };

            // Search Results Display on tab/rewards dropdown change
            this.searchResultsDisplay = function () {
                var cardChunks = _this.groupCardsIntoChunks(resultsModel.cards, 5);
                // console.log('searchResultsDisplay cards chunked', cardChunks);
                var arrayOfProductStrings = _.map(cardChunks, _this.createProductChunkString, this);
                // console.log('searchResultsDisplay products arrayed', arrayOfProductStrings);
                _.each(arrayOfProductStrings, _this.sendProducts, this);
            };

            this.groupCardsIntoChunks = function (cards, size) {
                var chunkedCards = _.groupBy(cards, function (card, index) {
                    return Math.floor(index / size);
                });
                // console.log('groupCardsIntoChunks', chunkedCards);
                return _.toArray(chunkedCards);
            };
            this.createProductChunkString = function (cards) {
                return _.reduce(cards, function (str, card, index) {
                    if (index < cards.length) {
                        return str + _this.productValueString(card) + ',';
                    }
                    else {
                        return str + _this.productValueString(card);
                    }
                }, '', this);
            };
            this.productValueString = function (card) {
                var type = resultsModel.isFeatured(card) ? 'featured' : 'stan';
                return findACardModel.category + ';cf%' + card.cardProgramId + ';;;event15=1|event4=1;evar16=cf%' + type;
            };
            this.sendProducts = function (stringOfProductValues) {
                s.linkTrackVars = "prop3,prop4,eVar3,eVar4,eVar14,events,products";
                s.linkTrackEvents = "event4,event15,event17,event18";
                s.eVar14 = _this.userAnswersString();
                s.prop3 = "find a card";
                s.prop4 = "cf%dia";
                s.eVar3 = "find a card";
                s.eVar4 = "cf%dia";
                s.events = "event15,event18,event4,event17";
                s.products = stringOfProductValues;
                s.tl(true, 'o', 'Card Viewed');
                // console.log('sendProducts', s, stringOfProductValues);
            };

            // No Cards Displayed on tab/rewards dropdown change
            this.noCardsDisplayed = function () {
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,events';
                s.linkTrackEvents = 'event4';
                s.prop3 = 'find a card';
                s.prop4 = 'nocardsDisplayed';
                s.eVar3 = 'find a card';
                s.eVar4 = 'nocardsDisplayed';
                s.events = 'event4';
                s.tl(true, 'o', 'No Cards Displayed');
                // console.log('noCardsDisplayed', s);
            };

            // Apply link
            this.referral = function (card) {
                // console.log('referral', resultsModel.isFeatured(card));
                if (resultsModel.isFeatured(card)) {
                    _this.featuredReferral(card);
                }
                else {
                    _this.standardReferral(card);
                }
            };

            // Standard card "Apply" link
            this.standardReferral = function (card) {
                if (findACardModel.productType === 'afac') {
                    findACardModel.productType = 'Consumer';
                } else if (findACardModel.productType === 'bfac') {
                    findACardModel.productType = 'Business';
                }
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,eVar14,eVar16,eVar21,eVar23,eVar26,products,events';
                s.linkTrackEvents = 'event14,event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%an';
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%an';
                s.eVar14 = _this.userAnswersString();
                s.eVar16 = 'cf%stan';
                s.eVar21 = 'cf%an';
                s.eVar23 = 'cf%std';
                s.eVar26 = findACardModel.category;
                s.events = 'event14,event4';
                s.products = findACardModel.productType + ';cf%' + card.cardProgramId + ';;;event14=1;evar16=cf%stan';
                s.tl(true, 'o', 'Standard Referral');
                // console.log('standardReferral', s, card);
            };

            // Sponsored card "Apply" link
            this.featuredReferral = function (card) {
                if (findACardModel.productType === 'afac') {
                    findACardModel.productType = 'Consumer';
                } else if (findACardModel.productType === 'bfac') {
                    findACardModel.productType = 'Business';
                }
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,eVar14,eVar16,eVar21,eVar23,eVar26,products,events';
                s.linkTrackEvents = 'event14,event19,event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%an';
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%an';
                s.eVar14 = _this.userAnswersString();
                s.eVar16 = 'cf%feat';
                s.eVar21 = 'cf%an';
                s.eVar23 = 'cf%ftd';
                s.eVar26 = findACardModel.category;
                s.events = 'event14,event19,event4';
                s.products = findACardModel.productType + ';cf%' + card.cardProgramId + ';;;event14=1|event19=1;evar16=cf%feat';
                s.tl(true, 'o', 'Featured Referral');
                // console.log('featuredReferral', s, card);
            };

            // moreInfo links
            this.moreInfo = function (card) {
                if (resultsModel.isFeatured(card)) {
                    _this.featuredMoreInfoDisplay(card);
                }
                else {
                    _this.standardMoreInfoDisplay(card);
                }
            };

            // Standard card "More info" link
            this.standardMoreInfoDisplay = function (card) {
                if (findACardModel.productType === 'afac') {
                    findACardModel.productType = 'Consumer';
                } else if (findACardModel.productType === 'bfac') {
                    findACardModel.productType = 'Business';
                }
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,eVar14,eVar15,eVar23,eVar26,events,products';
                s.linkTrackEvents = 'event15,event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%dcm';
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%dcm';
                s.eVar14 = _this.userAnswersString();
                s.eVar15 = 'cf%dcm';
                s.eVar23 = 'cf%dcm';
                s.eVar26 = findACardModel.category;
                s.events = 'event15,event4';
                s.products = findACardModel.productType + ';cf%' + card.cardProgramId + ';;;event15=1;evar16=cf%stan';
                s.tl(true, 'o', 'Standard More Info Display');
                // console.log('standardMoreInfoDisplay', s, card);
            };

            // Sponsored card "More info" link
            this.featuredMoreInfoDisplay = function (card) {
                if (findACardModel.productType === 'afac') {
                    findACardModel.productType = 'Consumer';
                } else if (findACardModel.productType === 'bfac') {
                    findACardModel.productType = 'Business';
                }
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,eVar14,eVar15,eVar23,eVar26,events,products';
                s.linkTrackEvents = 'event15,event18,event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%dcm';
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%dcm';
                s.eVar14 = _this.userAnswersString();
                s.eVar15 = 'cf%dcm';
                s.eVar23 = 'cf%dcm';
                s.eVar26 = findACardModel.category;
                s.events = 'event15,event18,event4';
                s.products = findACardModel.productType + ';cf%' + card.cardProgramId + ';;;event15|event18=1;evar16=cf%feat';
                s.tl(true, 'o', 'Featured More Info Display');
                // console.log('featuredMoreInfoDisplay', s, card);
            };

            // moreInfoReferrals
            this.moreInfoReferral = function (card) {
                if (resultsModel.isFeatured(card)) {
                    _this.featuredMoreInfoReferral(card);
                }
                else {
                    _this.standardMoreInfoReferral(card);
                }
            };

            // Standard card More Info "Apply" link
            this.standardMoreInfoReferral = function (card) {
                if (findACardModel.productType === 'afac') {
                    findACardModel.productType = 'Consumer';
                } else if (findACardModel.productType === 'bfac') {
                    findACardModel.productType = 'Business';
                }
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,eVar14,eVar16,eVar21,eVar23,eVar26,products,events';
                s.linkTrackEvents = 'event14,event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%can';
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%can';
                s.eVar14 = _this.userAnswersString();
                s.eVar16 = 'cf%stan';
                s.eVar21 = 'cf%can';
                s.eVar23 = 'cf%dcm';
                s.eVar26 = findACardModel.category;
                s.events = 'event14,event4';
                s.products = findACardModel.productType + ';cf%' + card.cardProgramId + ';;;event14=1;evar16=cf%stan';
                s.tl(true, 'o', 'Standard More Info Referral');
                // console.log('standardMoreInfoReferral', s, card);
            };

            // Sponsored card More Info "Apply" link
            this.featuredMoreInfoReferral = function (card) {
                if (findACardModel.productType === 'afac') {
                    findACardModel.productType = 'Consumer';
                } else if (findACardModel.productType === 'bfac') {
                    findACardModel.productType = 'Business';
                }
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,eVar14,eVar16,eVar21,eVar23,eVar26,products,events';
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,eVar14,eVar16,eVar21,eVar23,eVar26,products,events';
                s.linkTrackEvents = 'event14,event19,event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%can';
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%can';
                s.eVar14 = _this.userAnswersString();
                s.eVar16 = 'cf%feat';
                s.eVar21 = 'cf%can';
                s.eVar23 = 'cf%dcm';
                s.eVar26 = findACardModel.category;
                s.events = 'event14,event19,event4';
                s.products = findACardModel.productType + ';cf%' + card.cardProgramId + ';;;event14=1|event19=1;evar16=cf%feat';
                s.tl(true, 'o', 'Featured More Info Referral');
                // console.log('featuredMoreInfoReferral', s, card);
            };

            this.compareCardSelection = function (card) {
                if (resultsModel.isFeatured(card)) {
                    _this.featuredCompareCardSelection(card);
                }
                else {
                    _this.standardCompareCardSelection(card);
                }
            };
            // Sponsored card add to Compare tray
            this.featuredCompareCardSelection = function (card) {
                if (findACardModel.productType === 'afac') {
                    findACardModel.productType = 'Consumer';
                } else if (findACardModel.productType === 'bfac') {
                    findACardModel.productType = 'Business';
                }
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,eVar26,events,products';
                s.linkTrackEvents = 'event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%cc';
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%cc';
                s.eVar26 = findACardModel.category;
                s.events = 'event4';
                s.products = findACardModel.productType + ';cf%' + card.cardProgramId + ';;;event4=1;evar16=cf%feat';
                s.tl(true, 'o', 'Featured Compare Card Selection');
                // console.log('featuredCompareCardSelection', s, card);
            };
            // Standard card add to Compare tray
            this.standardCompareCardSelection = function (card) {
                if (findACardModel.productType === 'afac') {
                    findACardModel.productType = 'Consumer';
                } else if (findACardModel.productType === 'bfac') {
                    findACardModel.productType = 'Business';
                }
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,eVar26,events,products';
                s.linkTrackEvents = 'event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%cc';
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%cc';
                s.eVar26 = findACardModel.category;
                s.events = 'event4';
                s.products = findACardModel.productType + ';cf%' + card.cardProgramId + ';;;event4=1;evar16=cf%stan';
                s.tl(true, 'o', 'Standard Compare Card Selection');
                // console.log('standardCompareCardSelection', s, card);
            };

            // Compare Overlay display
            this.compareDisplay = function () {
                if (findACardModel.productType === 'afac') {
                    findACardModel.productType = 'Consumer';
                } else if (findACardModel.productType === 'bfac') {
                    findACardModel.productType = 'Business';
                }
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,eVar14,eVar15,eVar23,eVar26,events,products';
                s.linkTrackEvents = 'event15,event18,event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%dic';
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%dic';
                s.eVar14 = _this.userAnswersString();
                s.eVar15 = 'cf%dic';
                s.eVar23 = 'cf%cmp';
                s.eVar26 = findACardModel.category;
                s.events = 'event15,event18,event4';
                var cardids = '';
                $(".card_box").each(function () {
                    cardids += findACardModel.productType + ';cf%' + $(this).attr("id") + ';;;';
                    if ($(this).children("h5").text() == 'true') {
                        cardids += 'event15|event18=1;evar16=cf%feat,';
                    } else {
                        cardids += 'event15;evar16=cf%stan,';
                    }
                    // console.log('compareDisplay productType, cardids', findACardModel.productType, cardids);
                });
                cardids = cardids.slice(0, -1);
                s.products = cardids;
                s.tl(true, 'o', ' Compare Card Display');
                // console.log('compareDisplay', s);
            };

            // moreInfoReferrals
            this.compareCardsReferral = function (card) {
                if (resultsModel.isFeatured(card)) {
                    _this.featuredCompareCardsReferral(card);
                }
                else {
                    _this.standardCompareCardsReferral(card);
                }
            };

            // Standard card Compare Overlay "Apply" link
            this.standardCompareCardsReferral = function (card) {
                if (findACardModel.productType === 'afac') {
                    findACardModel.productType = 'Consumer';
                } else if (findACardModel.productType === 'bfac') {
                    findACardModel.productType = 'Business';
                }
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,eVar14,eVar16,eVar21,eVar23,eVar26,products,events';
                s.linkTrackEvents = 'event14,event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%ca';
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%ca';
                s.eVar14 = _this.userAnswersString();
                s.eVar16 = 'cf%stan';
                s.eVar21 = 'cf%ca';
                s.eVar23 = 'cf%dmp';
                s.eVar26 = findACardModel.category;
                s.events = 'event14,event4';
                s.products = findACardModel.productType + ';cf%' + card.cardProgramId + ';;;event14=1;evar16=cf%stan';
                s.tl(true, 'o', 'Standard Compare Referral');
                // console.log('standardCompareCardsReferral', s, card);
            };

            // Sponsored card Compare Overlay "Apply" link
            this.featuredCompareCardsReferral = function (card) {
                if (findACardModel.productType === 'afac') {
                    findACardModel.productType = 'Consumer';
                } else if (findACardModel.productType === 'bfac') {
                    findACardModel.productType = 'Business';
                }
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,eVar14,eVar16,eVar21,eVar23,eVar26,products,events';
                s.linkTrackEvents = 'event14,event19,event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%ca';
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%ca';
                s.eVar14 = _this.userAnswersString();
                s.eVar16 = 'cf%feat';
                s.eVar21 = 'cf%ca';
                s.eVar23 = 'cf%cmp';
                s.eVar26 = findACardModel.category;
                s.events = 'event14,event19,event4';
                s.products = findACardModel.productType + ';cf%' + card.cardProgramId + ';;;event14=1|event19=1;evar16=cf%feat';
                s.tl(true, 'o', 'Featured Compare Referral');
                // console.log('featuredCompareCardsReferral', s, card);
            };

            // Screener Question Answers
            this.screenerAnswers = function () {
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,eVar14,events';
                s.linkTrackEvents = 'event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%screenerquestions';
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%screenerquestions';
                s.eVar14 = _this.userAnswersString();
                s.events = 'event4';
                s.tl(true, 'o', 'Screener Answers');
                // console.log('screenerAnswers', s);
            };

            // Product select page Business or Consumer
            // @TODO: This will probably need to be moved outside of the Angular app, since it applies
            // to the parent page
            this.businessConsumer = function () {
                if (findACardModel.productType === 'afac') {
                    findACardModel.productType = 'Consumer';
                } else if (findACardModel.productType === 'bfac') {
                    findACardModel.productType = 'Business';
                }
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,events';
                s.linkTrackEvents = 'event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%Business or Consumer links%' + findACardModel.productType;
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%Business or Consumer links%' + findACardModel.productType;
                s.events = 'event4';
                s.tl(true, 'o', 'Business or Consumer Links');
                // console.log('businessConsumer 2', s);
            };

            // Select page bottom links
            this.bottomLinks = function (linkname) {
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,events';
                s.linkTrackEvents = 'event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%bottom links%' + linkname;
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%bottom links%' + linkname;
                s.events = 'event4';
                s.tl(true, 'o', 'Bottom Links');
                // console.log('bottomLinks', s, linkname);
            };

            // Results page tabs
            this.headerNavLinks = function (tabname) {
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,events';
                s.linkTrackEvents = 'event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%header links%' + tabname;
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%header links%' + tabname;
                s.events = 'event4';
                s.tl(true, 'o', 'Header Links');
                // console.log('headerNavLinks', s, tabname);
            };

            this.startOver = function () {
                if (resultsModel.cards.length) {
                    _this.hasCardsStartOver();
                }
                else {
                    _this.noCardsStartOver();
                }
            };

            this.hasCardsStartOver = function () {
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,eVar14,events';
                s.linkTrackEvents = 'event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%header links%start over';
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%header links%start over';
                s.eVar14 = _this.userAnswersString();
                s.events = 'event4';
                s.tl(true, 'o', 'Header Links');
                // console.log('hasCardsStartOver', s);
            };

            // "Search again" link with 0 cards
            this.noCardsStartOver = function () {
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,eVar14,events';
                s.linkTrackEvents = 'event4';
                s.prop3 = 'find a card';
                s.prop4 = 'nocardsDisplayed-StartOver';
                s.eVar3 = 'find a card';
                s.eVar4 = 'nocardsDisplayed-StartOver';
                s.eVar14 = _this.userAnswersString();
                s.events = 'event4';
                s.tl(true, 'o', 'No Cards Startover');
                // console.log('noCardsStartOver', s);
            };

            // Advertisement appears on page
            this.advertisementImpression = function (adName) {
                if (findACardModel.productType === 'afac') {
                    findACardModel.productType = 'Consumer';
                } else if (findACardModel.productType === 'bfac') {
                    findACardModel.productType = 'Business';
                }
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,eVar14,eVar15,eVar23,eVar26,eVar41,products,events';
                s.linkTrackEvents = 'event7,event15,event17,event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%skyscraperimpression';
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%skyscraperimpression';
                s.eVar14 = _this.userAnswersString();
                s.eVar15 = 'cf%skyscraperimpression';
                s.eVar23 = 'cf%skyscraper';
                s.eVar26 = findACardModel.category;
                s.eVar41 = 'APPLICATION VISITOR NUMBER';
                s.events = 'event7,event15,event17,event4';
                s.products = findACardModel.productType + ';cf%' + adName;
                s.tl(true, 'o', 'Advertisement Impression');
                // console.log('advertisementImpression', s);
            };
            // Advertisement is clicked
            this.advertisementReferral = function (adName) {
                if (findACardModel.productType === 'afac') {
                    findACardModel.productType = 'Consumer';
                } else if (findACardModel.productType === 'bfac') {
                    findACardModel.productType = 'Business';
                }
                s.linkTrackVars = 'prop3,prop4,eVar3,eVar4,eVar14,eVar15,eVar23,eVar26,eVar41,products,events';
                s.linkTrackEvents = 'event7,event15,event17,event4';
                s.prop3 = 'find a card';
                s.prop4 = 'cf%skyscraperimpression';
                s.eVar3 = 'find a card';
                s.eVar4 = 'cf%skyscraperimpression';
                s.eVar14 = _this.userAnswersString();
                s.eVar15 = 'cf%skyscraperimpression';
                s.eVar23 = 'cf%skyscraper';
                s.eVar26 = findACardModel.category;
                s.eVar41 = 'APPLICATION VISITOR NUMBER';
                s.events = 'event7,event15,event17,event4';
                s.products = findACardModel.productType + ';cf%' + adName;
                s.tl(true, 'o', 'Advertisement Referral');
                // console.log('advertisementReferral', s);
            };
        }
    ]);


    angular.module('MCCOM.findACard').directive('facAnalytics', [
        'FacAnalyticsService',
        function (FacAnalyticsService) {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    el.on('click', 'a.submit-questions-btn', function () {
                        FacAnalyticsService.screenerAnswers();
                    });
                    el.on('click', '.card-container a.card__apply-btn', function () {
                        var card = $(this).scope().card;
                        FacAnalyticsService.referral(card);
                    });
                    el.on('click', '[ng-click^="moreInfo"]', function () {
                        // console.log('fired');
                        var card = $(this).scope().card;
                        FacAnalyticsService.moreInfo(card);
                    });
                    el.on('click', '.more-info-modal a.card__apply-btn', function () {
                        var card = $(this).scope().moreInfoCard;
                        FacAnalyticsService.moreInfoReferral(card);
                    });
                    el.on('click', '.compare-btn', function () {
                        FacAnalyticsService.compareDisplay();
                    });
                    el.on('click', '.compare-cards-modal a.card__apply-btn', function () {
                        var card = $(this).scope().card;
                        FacAnalyticsService.compareCardsReferral(card);
                    });
                    el.on('click', '.mode-link-bar .mode-link a', function (event) {
                        FacAnalyticsService.bottomLinks($(this).text());
                    });
                    el.on('click', '.results-category', function (event) {
                        FacAnalyticsService.headerNavLinks($(this).text());
                    });
                    el.on('click', 'a.search-again', function () {
                        if (el.is('.fac-results')) {
                            FacAnalyticsService.startOver();
                        }
                    });
                    el.on('click', '.mode-advertisement a', function () {
                        var adName = $(this).attr('name');
                        // FacAnalyticsService.advertisementReferral(adName);
                        FacAnalyticsService.providerWatch(findACardModel.category, FacAnalyticsService.advertisementReferral, adName);
                    });

                    // Model change
                    // FacAnalytics.standardCompareCardSelection
                    // FacAnalytics.featuredCompareCardSelection
                }
            };

        }
    ]);
})();
/* globals jQuery, MCCOM */
(function ($, MCCOM) {
    var LocatorInstance = function ($root, options) {
        this.init($root, options);
    }

    $.extend(LocatorInstance.prototype, {
        init: function ($root, options) {
            this.$root = $root;
            this.options = options;

            this.$form = $root.find('form');
            this.$resultsContainer = $root.find('.locator-results-container');

            var validatrOptions = {
                location: 'top',
                valid: $.proxy(this.onSearch, this)
            };
            this.$form.validatr(validatrOptions);

            // airports modal
            this.$airportsModalLink = this.$root.find('.airports-modal-link');
            this.$airportsModalLink.on('click', _.bind(this.showAirportsModal, this));
            this.$airportsModal = this.$root.find('.locator-airports-modal');
            this.$airportsModal.on('loaded.bs.modal', _.bind(this.onAirportsModalLoaded, this));

            this.infoWindowTemplate = _.template('<div class="info-window-heading">{{ heading }}</div><div class="info-window-content">{{ content }}</div>');
            this.map = null;
            this.infoWindow = null;
            this.markers = null;

            this.getCountriesDropdown();

			/*
			 * 5/19/15 ncr: dear future, I'm really really sorry about this.
			 * For three days I have committed myself to epic battle against
			 * rePower in IE8 and IE9.
			 * The solution I've settled on, the final solution,
			 * is below. What you're about to see will disturb you.
			 * I awkwardly present ...
			 * The Shake It Up
			 * i'msosorry
			 */

            if ($('html').hasClass('ie8') || $('html').hasClass('ie9')) {
                _this = this;
                _this.$root.css({
                    'min-height': 0
                });

                setTimeout(function () {
                    _this.$root.css({
                        'height': '1600px'
                    });
                }, 400);
                setTimeout(function () {
                    _this.$root.css({
                        'height': '0px'
                    });
                }, 400);
                setTimeout(function () {
                    _this.$root.css({
                        'height': '1600px'
                    });
                }, 400);
                setTimeout(function () {
                    _this.$root.css({
                        'height': 'auto'
                    });
                }, 400);
                setTimeout(function () {
                    _this.$root.find('> div').css({
                        'visibility': 'visible'
                    });
                }, 1600);
            }
        },

        getCountriesDropdown: function () {
            this.$root.find('.countries-field-wrapper').load(this.options['countries-path']);
        },
        getMobileOperatingSystem: function () {
            var userAgent = navigator.userAgent || navigator.vendor || window.opera;
            if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i)) {
                return 'iOS';
            }
            else if (userAgent.match(/Android/i)) {
                return 'Android';
            }
            else {
                return 'unknown';
            }
        },
        showAirportsModal: function (event) {
            event.preventDefault();
            this.$airportsModal.modal({
                remote: this.options['airport-form-path']
            });
        },
        onAirportsModalLoaded: function (event) {
            this.$airportsModal.find('[name=country]').on('change', _.bind(this.onAirportCountryChange, this));
            this.$airportsModal.find('form').on('submit', _.bind(this.onAirportsFormSubmit, this));
        },
        onAirportCountryChange: function (event) {
            var $citiesFieldWrapper = this.$airportsModal.find('.airport-city-field-wrapper');
            var url = this.options['airport-cities-path'] + '?country=' + this.$airportsModal.find('[name=country]').val()
            $citiesFieldWrapper.load(url, _.bind(this.onGetAirportCities, this));
        },
        onGetAirportCities: function (data) {

        },
        onAirportsFormSubmit: function (event) {
            event.preventDefault();
            var $results = this.$airportsModal.find('.airport-results');
            var params = this.$airportsModal.find('form').serialize();
            $results.load(
                this.options['airport-results-path'] + '?' + params,
                _.bind(this.onAirportResultsLoaded, this)
            );
        },
        onAirportResultsLoaded: function () {
            this.$airportsModal.find('form').hide();
            this.$airportsModal.find('.search-again').on('click', _.bind(this.onSearchAgainPressed, this));
        },
        onSearchAgainPressed: function (event) {
            event.preventDefault();
            this.$airportsModal.find('form').show();
            this.$airportsModal.find('.airport-results').empty();
        },
        getResults: function () {
            var params = this.$form.serialize();
            this.$resultsContainer.load(this.options['results-path'], params, _.bind(this.onGetResultsSuccess, this));
        },
        collapseForm: function () {
            this.$form.hide();
        },
        expandForm: function () {
            this.$form.show();
        },
        onSearch: function () {
            this.getResults();
            return false;
        },
        onGetResultsSuccess: function (data) {
            this.refreshMap();
            this.collapseForm();
        },
        refreshMap: function () {
            var $map = this.$root.find('.map');
            this.center = new google.maps.LatLng($map.data('lat'), $map.data('lng'));
            var centerName = $map.data('center-name');
            var mapOptions = {
                center: this.center,
                zoom: $map.data('zoom'),
                mapTypeControl: false,
                streetViewControl: false
            };

            this.map = new google.maps.Map($map.get(0), mapOptions);

            google.maps.event.addListener(this.map, 'idle', _.bind(this.onMapIdle, this));
            this.configureMarkerGrouping();

            this.markers = this.addMarkers();
            this.markers.push(this.addCenterMarker(this.center, centerName));

            this.adjustMapToFitMarkers();
        },
        adjustMapToFitMarkers: function () {
            // adjust the bounds to fit all markers
            var bounds = new google.maps.LatLngBounds();
            bounds.extend(this.center);

            // reverse sort markers by longitude to prevent wrapping around the world
            var sortedMarkers = _.sortBy(this.markers, function (marker) {
                return marker.getPosition().lng();
            }).reverse();

            _.each(sortedMarkers, function (marker) {
                bounds.extend(marker.getPosition());
            });

            this.map.fitBounds(bounds);
        },
        addCenterMarker: function (center, centerName) {
            var map = this.map;
            var marker = new google.maps.Marker({
                position: center,
                map: map,
                title: centerName,
                id: 'center',
                infoWindowContent: this.infoWindowTemplate({ heading: centerName, content: '' })
            });

            this.markerGrouper.addMarker(marker);
            return marker;
        },
        addMarkers: function () {
            return this.$resultsContainer.find('.locations > li').map(_.bind(this.addMarker, this));
        },
        addMarker: function (index, elem) {
            var map = this.map;
            var $result = $(elem);
            var latLng = new google.maps.LatLng($result.data('lat'), $result.data('lng'));

            var marker = new google.maps.Marker({
                position: latLng,
                map: map,
                id: index + 1,
                infoWindowContent: this.createResultInfoWindow($result)
            });
            this.markerGrouper.addMarker(marker);
            return marker;
        },
        createResultInfoWindow: function ($result) {
            var heading = $result.find('.info-window-header').text();
            var content = $result.find('.info-window-content').map(function () {
                return $(this).clone().removeClass('info-window-content').get(0).outerHTML;
            }).toArray().join('');
            return this.infoWindowTemplate({ heading: heading, content: content })
        },
        onMarkerClick: function (marker, event) {
            if (this.infoWindow !== null) {
                this.infoWindow.close();
            }
            this.infoWindow = new google.maps.InfoWindow({
                content: marker.infoWindowContent
            });
            this.infoWindow.open(this.map, marker);
        },
        configureMarkerGrouping: function () {
            this.markerGrouper = new OverlappingMarkerSpiderfier(this.map,
                {
                    markersWontMove: true,
                    markersWontHide: true,
                    keepSpiderfied: true,
                    nearbyDistance: 10,
                    circleSpiralSwitchover: Infinity,
                    legWeight: 0 // no spider lines
                });

            // add spiderfy listener 
            this.markerGrouper.addListener('click', _.bind(this.onMarkerClick, this));
            this.markerGrouper.addListener('spiderfy', _.bind(function (markers) {
                this.markAsNotGrouped(markers);
            }, this));
            this.markerGrouper.addListener('unspiderfy', _.bind(function (markers) {
                this.markAsGrouped(markers);
            }, this));
        },
        onMapIdle: function () {
            var nearbyMarkers = this.markerGrouper.markersNearAnyOtherMarker();
            var allMarkers = this.markerGrouper.getMarkers();

            this.markAsNotGrouped(allMarkers, true);
            this.markAsGrouped(nearbyMarkers, true);
        },
        markAsNotGrouped: function (markers, idled) {
            var markersBasePath = this.options['markers-base-path'];
            _.each(markers, function (marker) {
                if (marker.id == 'center') {
                    marker.setIcon(markersBasePath + 'center.png');
                } else {
                    marker.setIcon(markersBasePath + 'result-' + marker.id + '.png');
                }

                if (!idled) {
                    marker.spiderfied = true;
                }
            });
        },
        markAsGrouped: function (markers, idled) {
            var markersBasePath = this.options['markers-base-path'];
            _.each(markers, function (marker) {
                // do not set maker icon as grouped while being spiderfied.
                if (idled && marker.spiderfied) {
                    return;
                }

                if (marker.id === 'center') {
                    marker.setIcon(markersBasePath + 'center-grouped.png');
                } else {
                    marker.setIcon(markersBasePath + 'result-grouped.png');
                }

                if (!idled) {
                    marker.spiderfied = false;
                }
            });
        }
    });

    MCCOM.Locator = MCWCM.ComponentWrapperFactory('.locator', {
        'results-path': '',
        'airport-form-path': '',
        'airport-results-path': '',
        'airport-cities-path': '',
        'countries-path': '',
        'markers-base-path': '/etc/clientlibs/mastercard_com/images/locators/markers/'
    }, LocatorInstance);

})(jQuery, MCCOM);
(function () {
    angular.module('MCCOM.search', ['ngSanitize']);

    var SearchInstance = function ($root, options) {
        this.$root = $root;
        angular.bootstrap($root, ['MCCOM.search']);
    };

    MCCOM.Search = MCWCM.ComponentWrapperFactory('.search-module', {}, SearchInstance);
    MCCOM.ExecQueue.add(MCCOM.Search.init);
})();
angular.module('MCCOM.search').service('GoogleSearchApplianceService', ['$http', '$q',
    function ($http, $q) {
        this.resultsEndpoint = '';
        this.suggestEndpoint = '';
        this.perPage = 8;
        this.site = '';
        this.client = '';

        this.getResults = function (query, page) {
            var defered = $q.defer();

            var onSuccess = function (data) {
                var $xml = $($.parseXML(data));
                var $responseTag = $xml.find('RES');
                if ($responseTag.length === 0) {
                    defered.resolve({
                        total: 0,
                        results: []
                    })
                } else {
                    var total = parseInt($responseTag.find('M').text());
                    if (_.isNaN(total)) {
                        total = 0;
                    }
                    defered.resolve({
                        total: total,
                        results: $responseTag.find('R').map(Result)
                    });
                }
            };

            var onFailure = function () {
                defered.reject(arguments);
            }

            var params = {
                q: query,
                start: (page - 1) * this.perPage,
                num: this.perPage,
                output: 'xml_no_dtd',
                site: this.site,
                client: this.client,
                getfields: 'mastercard_search_result_image'
            };

            var httpPromise = $http.get(this.resultsEndpoint, {
                params: params
            });

            httpPromise.success(onSuccess)
            httpPromise.error(onFailure);

            return defered.promise;
        };

        this.getSuggestions = function (query) {
            var timeoutDefered = $q.defer();
            var defered = $q.defer();

            var onSuccess = function (data) {
                defered.resolve(data);
            }

            var onFailure = function () {
                defered.reject(arguments);
            }

            var params = {
                token: query,
                output: 'legacy',
                site: this.site,
                client: this.client
            };

            var httpPromise = $http.get(this.suggestEndpoint, {
                timeout: timeoutDefered.promise,
                params: params
            });

            httpPromise.success(onSuccess)
            httpPromise.error(onFailure);

            return {
                promise: defered.promise,
                cancel: function (reason) {
                    timeoutDefered.resolve(reason);
                }
            }
        };

        var Result = function (index, xml) {
            var $xml = $(xml);
            return {
                url: $xml.find('U').text(),
                title: $xml.find('T').text(),
                snippit: $xml.find('S').text(),
                image: $xml.find('MT[N="mastercard_search_result_image"]').attr('V')
            }
        }
    }
]);

angular.module('MCCOM.search').controller('SearchController', ['$scope', '$rootScope', 'GoogleSearchApplianceService',
    function ($scope, $rootScope, GoogleSearchApplianceService) {
        $scope.query = '';
        $scope.isLoading = false;
        $scope.perPage = 8;
        $scope.maxPagesDisplayed = 5;
        $scope.page = 1;
        $scope.total = null;
        $scope.hasErrors = false;
        $scope.results = [];

        $scope.getSuggestions = _.bind(GoogleSearchApplianceService.getSuggestions, GoogleSearchApplianceService);

        $scope.getResults = function () {
            if ($scope.query === '') {
                return;
            }

            $scope.hasErrors = false;
            var promise = GoogleSearchApplianceService.getResults($scope.query, $scope.page);
            promise.then($scope.onGetResultsSuccess, $scope.onGetResultsFailure)['finally']($scope.onGetResultsFinished);
        };

        $scope.onGetResultsSuccess = function (data) {
            $scope.isLoading = true;
            $scope.results = data.results;
            $scope.total = data.total;
        };

        $scope.onGetResultsFailure = function () {
            $scope.hasErrors = true;
        };

        $scope.onGetResultsFinished = function () {
            $scope.isLoading = false;

            $('html.touch .search-controller .query').blur();
        };

        $scope.newSearch = function () {
            $scope.firstSearchPerformed = true;
            $scope.page = 1;
            $scope.results = [];
            $scope.total = null;
            $scope.getResults();
        };

        $scope.$watch('page', function (newValue) {
            $scope.getResults();
        });

        $rootScope.$on('SearchOccurring', function () {
            $scope.firstSearchPerformed = true;
            $scope.page = 1;
            $scope.results = [];
            $scope.total = null;
        });
    }
]);
angular.module('MCCOM.search').directive('pagination', [
    function () {
        return {
            restrict: 'A',
            scope: {
                current: '=',
                total: '=',
                max: '=',
                perPage: '='
            },
            template: function (element, attrs) {
                return element.html();
            },
            controller: ['$scope', function ($scope) {
                $scope.pages = [];

                $scope.prev = function () {
                    if ($scope.current <= 1) { return; }
                    $scope.current--;
                };

                $scope.next = function () {
                    if ($scope.current == $scope.total) { return; }
                    $scope.current++;
                };

                $scope.changePage = function (page) {
                    $scope.current = page;
                };

                $scope.$watch('current', function () {
                    calculate();
                });

                $scope.$watch('total', function () {
                    calculate();
                });

                var calculate = function () {
                    $scope.pageCount = Math.ceil($scope.total / $scope.perPage);
                    $scope.pages = calculatePages();
                    var maxPages = $scope.pages[$scope.pages.length - 1];
                    if ($scope.current > maxPages) {
                        $scope.current = maxPages
                    }
                };


                var calculatePages = function () {
                    var pageCount = $scope.pageCount;
                    var breakpoint = Math.ceil($scope.max / 2);
                    var start = 0;
                    var end = 0;

                    if ($scope.total !== null && $scope.total !== 0) {
                        if (pageCount < $scope.max || $scope.current <= breakpoint) {
                            start = 1;
                        } else {
                            start = $scope.current - (breakpoint - 1);
                        }
                        end = Math.min(pageCount + 1, start + $scope.max);

                        // end boundry check to ensure max pages are still shown at the end.
                        if (end - start < $scope.max && pageCount >= $scope.max) {
                            start = end - $scope.max;
                        }
                    }

                    return _.range(start, end);
                };
            }]
        };
    }
]);
angular.module('MCCOM.search').directive('autocomplete', [
    function () {
        var AutocompleteController = ['$scope', '$element', '$rootScope',
            function ($scope, $element, $rootScope) {
                $scope.topSuggestion = '';
                $scope.tempValue = $scope.value;
                $scope.requests = [];
                $scope.suggestionsEnabled = true;

                // -1 suggestion index = user typed value shows and first suggestion shows next to it hilighted
                $scope.suggestionIndex = -1;
                $scope.suggestions = [];

                var minCharacters = 3;

                $scope.onKeyDown = function ($event) {
                    $rootScope.$broadcast('SearchOccurring');
                    $scope.suggestionsEnabled = true;
                    switch ($event.keyCode) {
                        case 9:
                            $scope.onTab($event);
                            break;
                        case 13:
                            $scope.onReturn($event);
                            break;
                        case 39:
                            $scope.onRight($event);
                            break;
                    }
                };

                $scope.onKeyUp = function ($event) {
                    switch ($event.keyCode) {
                        case 38:
                            $scope.onUp($event);
                            break;
                        case 40:
                            $scope.onDown($event);
                            break;
                        default:
                            $scope.value = $scope.tempValue;
                    }
                };

                $scope.onUp = function ($event) {
                    var newIndex = $scope.suggestionIndex - 1;

                    if (newIndex < -1) {
                        newIndex = $scope.suggestions.length - 1;
                    }

                    $scope.suggestionIndex = newIndex;
                    $scope.tempValue = $scope.suggestions[newIndex] || $scope.value;
                };

                $scope.onDown = function ($event) {
                    var newIndex = $scope.suggestionIndex + 1;
                    if (newIndex >= $scope.suggestions.length) {
                        newIndex = -1;
                    }
                    $scope.suggestionIndex = newIndex;
                    $scope.tempValue = $scope.suggestions[newIndex] || $scope.value;
                };

                $scope.onRight = function ($event) {
                    var cursorPosition = $event.currentTarget.selectionStart;
                    if (cursorPosition === $scope.value.length) {
                        $scope.value = $scope.tempValue = $scope.topSuggestion;
                    }
                };

                $scope.onTab = function ($event) {
                    $scope.select();
                };

                $scope.onReturn = function ($event) {
                    $scope.select();
                };

                $scope.clickSuggestion = function (suggestion) {
                    $rootScope.$broadcast('SearchOccurring');
                    $scope.select(suggestion);
                    $scope.submit()();
                };

                $scope.clickSearch = function () {
                    $rootScope.$broadcast('SearchOccurring');
                    $scope.select($scope.suggestions[0] || $scope.value);
                    $scope.submit()();
                };

                $scope.select = function (selection) {
                    $scope.suggestions = [];
                    $scope.tempValue = selection || $scope.tempValue;
                    $scope.value = $scope.tempValue;
                    $scope.suggestionsEnabled = false;
                    $scope.cancelRequests();

                };

                $scope.setTopSuggestion = function (index) {
                    var topSuggestion = '';
                    if ($scope.suggestions.length > 0) {
                        topSuggestion = $scope.value + $scope.suggestions[0].substr($scope.value.length);
                    }
                    $scope.topSuggestion = topSuggestion;
                };

                $scope.$watch('suggestions', function () {
                    $scope.suggestionIndex = -1;
                    $scope.setTopSuggestion($scope.suggestionIndex);
                });

                $scope.getSuggestions = function () {
                    $scope.cancelRequests();

                    var request = $scope.fetch()($scope.value);

                    request.promise.then(function (suggestions) {
                        if ($scope.suggestionsEnabled) {
                            $scope.suggestionIndex = -1;
                            $scope.suggestions = suggestions;
                        }
                    })['finally'](function () {
                        $scope.requests.splice($scope.requests.indexOf(request), 1);
                    });

                    $scope.requests.push(request);
                };

                $scope.cancelRequests = function () {
                    _.each($scope.requests, function (request) {
                        request.cancel('aborted');
                    });
                };
                // TODO: Move into its own directive in the future
                $scope.$watch('value', function (newValue) {
                    if (!Modernizr.input.placeholder) {
                        if (!newValue) {
                            $element.find('.fake-placeholder').val(
                                $element.find('.query').attr('placeholder'));
                        }
                        else {
                            $element.find('.fake-placeholder').val('');
                        }
                    }

                    if (newValue.length < minCharacters) {
                        $scope.suggestions = [];
                        $scope.cancelRequests();
                        return;
                    }

                    $scope.getSuggestions();
                });
            }
        ];

        return {
            restrict: 'A',
            scope: {
                fetch: '&',
                value: '=',
                submit: '&'
            },
            template: function (element, attrs) {
                return element.html();
            },
            controller: AutocompleteController
        };
    }
]);
(function () {
    var searchModule = angular.module('MCCOM.search');

    var mapOptions = function (object, options, mapping) {
        _.each(mapping, function (optionName, property) {
            if (angular.isDefined(options[optionName])) {
                object[property] = options[optionName];
            }
        });
    };

    searchModule.service('AppConfig', ['$rootElement',
        function ($rootElement) {
            return $rootElement.find('.component-config').data();
        }
    ]);

    searchModule.config(['$provide',
        function ($provide) {
            $provide.decorator('GoogleSearchApplianceService', ['$delegate', 'AppConfig',
                function ($delegate, AppConfig) {
                    mapOptions($delegate, AppConfig, {
                        resultsEndpoint: 'resultsEndpoint',
                        suggestEndpoint: 'suggestionsEndpoint',
                        client: 'gsaClientId',
                        site: 'gsaSiteId'
                    });
                    return $delegate;
                }
            ]);
        }
    ]);
})();
; (function (window, undefined) {

    var initMCWCM = new Initializr(MCWCM, 'MCWCM');
    var WCMComponent = _.bind(initMCWCM.component, initMCWCM);
    var initMCCOM = new Initializr(MCCOM, 'MCCOM');
    var COMComponent = _.bind(initMCCOM.component, initMCCOM);

    MCWCM.locationHref = window.location.href;

    $(document).ready(function () {
        $(window).on('windowresize', _.debounce(breakerbox.resize, 150));

        //RTL detection, option for Slick Slider...
        MCCOM.rightToLeft = false;
        if ($('html').attr('dir') === 'RTL') {
            MCCOM.rightToLeft = true;
        }

        // wcmmode detection
        var $body = $('body');
        var modeClassNamePrefix = 'wcmmode-';
        var isEditMode = $body.hasClass(modeClassNamePrefix + 'edit');
        var isDesignMode = $body.hasClass(modeClassNamePrefix + 'design');
        var isPreviewMode = $body.hasClass(modeClassNamePrefix + 'preview');

        MCWCM.mode = {
            isEditMode: $body.hasClass(modeClassNamePrefix + 'edit'),
            isDesignMode: $body.hasClass(modeClassNamePrefix + 'design'),
            isPreviewMode: $body.hasClass(modeClassNamePrefix + 'preview')
        };

        MCWCM.breakpoints = {
            xs: 480,
            sm: 768,
            md: 1024,
            lg: 1401,
        };
        _.extend(MCWCM.breakpoints, {
            isLarge: $body.width() >= MCWCM.breakpoints.lg,
            isMedium: $body.width() >= MCWCM.breakpoints.lg,
            isSmall: $body.width() >= MCWCM.breakpoints.sm,
            isXSmall: $body.width() >= MCWCM.breakpoints.xs,
            isTouch: $('html').hasClass('touch'),
            isTablet: $body.width() < MCWCM.breakpoints.sm || $('html').hasClass('touch'),
            isMobile: $body.width() < MCWCM.breakpoints.xs || $('html').hasClass('touch')
        });

        // needs to run first because it may impact dimension calculations of other components
        WCMComponent('CallToActionButton');
        $('a.cta').each(function () {
            var $copy = $(this).find('.btn-copy');
            var realCopy = $(this).data('real-copy');
            $copy.html(realCopy);
        });

        if (!isEditMode && !isDesignMode) {
            COMComponent('StickyNav');
        }

        if (!isEditMode) {
            WCMComponent('HeroBannerCarousel', { rtl: MCCOM.rightToLeft });
            WCMComponent('HeroBannerLightCarousel', { rtl: MCCOM.rightToLeft });
            WCMComponent('ParallaxScroller');
            WCMComponent('ProductCarousel', { rtl: MCCOM.rightToLeft });
            WCMComponent('RelatedContentScroller', { rtl: MCCOM.rightToLeft });
            WCMComponent('Tabs');
            WCMComponent('TabScroller');
        }

        // Always run inalphabeticalorderwhynot
        COMComponent('AccessAccount');
        COMComponent('Article');
        COMComponent('ContentFeed');
        COMComponent('CountryLanguageSelector');
        WCMComponent('FeatureCarousel');
        COMComponent('LoadingIndicator');
        WCMComponent('FAQs');
        COMComponent('FindACard', { rtl: MCCOM.rightToLeft });
        WCMComponent('HeroBanner');
        COMComponent('Janrain');
        WCMComponent('JumpFocus');
        COMComponent('Locator');
        WCMComponent('EmailForm');
        WCMComponent('SalesForce');
        COMComponent('MegaMenuHome');
        COMComponent('MegaMenu');
        COMComponent('MobileMenuMega');
        COMComponent('MobileMenuHome');
        WCMComponent('PromotionColumns');
        WCMComponent('PromotionRotating');
        COMComponent('SearchToggle');
        WCMComponent('TitleTextLink');
        WCMComponent('TwitterBanner', { rtl: MCCOM.rightToLeft });
        COMComponent('WelcomeBanner');
        COMComponent('IdentityTheft');

        initMCWCM.logErrors();
        initMCCOM.logErrors();

        $('input[type="checkbox"]').checkbox({
            buttonStyle: 'btn-link',
            buttonStyleChecked: 'btn-active',
            checkedClass: 'glyphicon-ok'
        });
        $('select.selectpicker').selectpicker({
            size: 6.69
        });

        $('.prev, .next').css({
            zIndex: 9999
        });

        $('.mega-menu__audience > a > span').on('click', function () {
            var url = $(this).parent().attr('href');
            window.location = url;
        });

        /* Because Tables don't overflow properly, wrap em */
        $('.rich-text table').wrap('<div class="table-wrap"></div>');
    });

    function onYouTubeIframeAPIReady() {
        $(function () {
            WCMComponent('VideoMultiple');
            WCMComponent('VideoSingle');
        });
    }

    if (window.YT) {
        YT.ready(onYouTubeIframeAPIReady);
    } else {
        window.onYouTubeIframeAPIReady = onYouTubeIframeAPIReady;
    }

	/*
	 * Language Demo
	 */
    var $languageSnippets = $('.language-snippet');
    $languageSnippets.each(function () {
        var $snippet = $(this);
        var fontFamily = '<p>Font: ' + $snippet.find('p').css('font-family') + '</p>';
        $snippet.append(fontFamily);
    });

})(this);
