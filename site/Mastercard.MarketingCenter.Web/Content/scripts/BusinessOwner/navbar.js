﻿/**
 * Created by rbratchenko on 21.07.2015.
 */
if (typeof document.mainJS == "undefined") {
    document.mainJS = true;

    // http://spin.js.org/#v2.3.2
    !function (a, b) { "object" == typeof module && module.exports ? module.exports = b() : "function" == typeof define && define.amd ? define(b) : a.Spinner = b() }(this, function () { "use strict"; function a(a, b) { var c, d = document.createElement(a || "div"); for (c in b) d[c] = b[c]; return d } function b(a) { for (var b = 1, c = arguments.length; c > b; b++) a.appendChild(arguments[b]); return a } function c(a, b, c, d) { var e = ["opacity", b, ~~(100 * a), c, d].join("-"), f = .01 + c / d * 100, g = Math.max(1 - (1 - a) / b * (100 - f), a), h = j.substring(0, j.indexOf("Animation")).toLowerCase(), i = h && "-" + h + "-" || ""; return m[e] || (k.insertRule("@" + i + "keyframes " + e + "{0%{opacity:" + g + "}" + f + "%{opacity:" + a + "}" + (f + .01) + "%{opacity:1}" + (f + b) % 100 + "%{opacity:" + a + "}100%{opacity:" + g + "}}", k.cssRules.length), m[e] = 1), e } function d(a, b) { var c, d, e = a.style; if (b = b.charAt(0).toUpperCase() + b.slice(1), void 0 !== e[b]) return b; for (d = 0; d < l.length; d++) if (c = l[d] + b, void 0 !== e[c]) return c } function e(a, b) { for (var c in b) a.style[d(a, c) || c] = b[c]; return a } function f(a) { for (var b = 1; b < arguments.length; b++) { var c = arguments[b]; for (var d in c) void 0 === a[d] && (a[d] = c[d]) } return a } function g(a, b) { return "string" == typeof a ? a : a[b % a.length] } function h(a) { this.opts = f(a || {}, h.defaults, n) } function i() { function c(b, c) { return a("<" + b + ' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">', c) } k.addRule(".spin-vml", "behavior:url(#default#VML)"), h.prototype.lines = function (a, d) { function f() { return e(c("group", { coordsize: k + " " + k, coordorigin: -j + " " + -j }), { width: k, height: k }) } function h(a, h, i) { b(m, b(e(f(), { rotation: 360 / d.lines * a + "deg", left: ~~h }), b(e(c("roundrect", { arcsize: d.corners }), { width: j, height: d.scale * d.width, left: d.scale * d.radius, top: -d.scale * d.width >> 1, filter: i }), c("fill", { color: g(d.color, a), opacity: d.opacity }), c("stroke", { opacity: 0 })))) } var i, j = d.scale * (d.length + d.width), k = 2 * d.scale * j, l = -(d.width + d.length) * d.scale * 2 + "px", m = e(f(), { position: "absolute", top: l, left: l }); if (d.shadow) for (i = 1; i <= d.lines; i++) h(i, -2, "progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)"); for (i = 1; i <= d.lines; i++) h(i); return b(a, m) }, h.prototype.opacity = function (a, b, c, d) { var e = a.firstChild; d = d.shadow && d.lines || 0, e && b + d < e.childNodes.length && (e = e.childNodes[b + d], e = e && e.firstChild, e = e && e.firstChild, e && (e.opacity = c)) } } var j, k, l = ["webkit", "Moz", "ms", "O"], m = {}, n = { lines: 12, length: 7, width: 5, radius: 10, scale: 1, corners: 1, color: "#000", opacity: .25, rotate: 0, direction: 1, speed: 1, trail: 100, fps: 20, zIndex: 2e9, className: "spinner", top: "50%", left: "50%", shadow: !1, hwaccel: !1, position: "absolute" }; if (h.defaults = {}, f(h.prototype, { spin: function (b) { this.stop(); var c = this, d = c.opts, f = c.el = a(null, { className: d.className }); if (e(f, { position: d.position, width: 0, zIndex: d.zIndex, left: d.left, top: d.top }), b && b.insertBefore(f, b.firstChild || null), f.setAttribute("role", "progressbar"), c.lines(f, c.opts), !j) { var g, h = 0, i = (d.lines - 1) * (1 - d.direction) / 2, k = d.fps, l = k / d.speed, m = (1 - d.opacity) / (l * d.trail / 100), n = l / d.lines; !function o() { h++; for (var a = 0; a < d.lines; a++) g = Math.max(1 - (h + (d.lines - a) * n) % l * m, d.opacity), c.opacity(f, a * d.direction + i, g, d); c.timeout = c.el && setTimeout(o, ~~(1e3 / k)) }() } return c }, stop: function () { var a = this.el; return a && (clearTimeout(this.timeout), a.parentNode && a.parentNode.removeChild(a), this.el = void 0), this }, lines: function (d, f) { function h(b, c) { return e(a(), { position: "absolute", width: f.scale * (f.length + f.width) + "px", height: f.scale * f.width + "px", background: b, boxShadow: c, transformOrigin: "left", transform: "rotate(" + ~~(360 / f.lines * k + f.rotate) + "deg) translate(" + f.scale * f.radius + "px,0)", borderRadius: (f.corners * f.scale * f.width >> 1) + "px" }) } for (var i, k = 0, l = (f.lines - 1) * (1 - f.direction) / 2; k < f.lines; k++) i = e(a(), { position: "absolute", top: 1 + ~(f.scale * f.width / 2) + "px", transform: f.hwaccel ? "translate3d(0,0,0)" : "", opacity: f.opacity, animation: j && c(f.opacity, f.trail, l + k * f.direction, f.lines) + " " + 1 / f.speed + "s linear infinite" }), f.shadow && b(i, e(h("#000", "0 0 4px #000"), { top: "2px" })), b(d, b(i, h(g(f.color, k), "0 0 1px rgba(0,0,0,.1)"))); return d }, opacity: function (a, b, c) { b < a.childNodes.length && (a.childNodes[b].style.opacity = c) } }), "undefined" != typeof document) { k = function () { var c = a("style", { type: "text/css" }); return b(document.getElementsByTagName("head")[0], c), c.sheet || c.styleSheet }(); var o = e(a("group"), { behavior: "url(#default#VML)" }); !d(o, "transform") && o.adj ? i() : j = d(o, "animation") } return h });


    function is_win_prop(prop) {
        return !!('' + prop in window);
    }

    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i);
        }
    };


    $(document).ready(function () {
        var baseUrl = '';
        if (typeof (baseSiteUrl) !== 'undefined' && baseSiteUrl) {
            baseUrl = baseSiteUrl;
        }

        if ($.fn.selectric) {
            $('.language-selector select').selectric({
                onChange: function () {
                    $.ajax({
                        type: 'POST',
                        url: baseUrl + '/header/languagechanged',
                        data: { selectedLanguage: $(this).val() },
                        beforeSend: function () {
                            $('.language-selector .selectric-items').remove();
                        },
                        success: function () {
                            window.location.reload();
                        }
                    });
                }
            });

            $('.region-selector select').selectric({
                onChange: function () {
                    $.ajax({
                        type: 'POST',
                        url: baseUrl + '/header/regionchanged',
                        data: { selectedRegion: $(this).val() },
                        beforeSend: function () {
                            $('.region-selector .selectric-items').remove();
                        },
                        success: function () {
                            window.location.reload();
                        }
                    });
                }
            });
        }

        if ($('.icon-tiles').length) {
            var $it = $('.icon-tiles');
            $('.flyout-nav', $it).width($('.flyout-nav:first', $it).closest('.cell').width()).width('');
        }

        $(document).on('click', '.video-preview', function () {
            var $wrap = $(this),
                $video = $wrap.find('video').get(0);

            if (!$wrap.find('video').length) {
                return false;
            }

            if ($video.paused) {
                $video.play();
                $wrap.addClass('playing');
            } else {
                $video.pause();
                $wrap.removeClass('playing');
            }
        });

        if (isMobile.any()) {
            $('body').addClass('is-mobile');
            $('.print-link').remove();
        }

        var $window = $(window),
            boxSlider = $('.boxes-slider:has(.box:nth-of-type(2))'),
            pausingCarousel = $('.pausing-carousel:has(.carousel-slide)'),
            $dataSlick = $('[data-slick]'),
            $searchSegment = $('#segment-field');

        function checkNavTogglers() {
            if ($window.width() > 768) {
                $('.main-nav-bar .check-toggler').on('change', function () {
                    $('.main-nav-bar .check-toggler').not($(this)).prop('checked', false);
                });
            }
        }
        checkNavTogglers();
        $window.on('resize', checkNavTogglers);

        // open nav flyouts on hover
        var clickTargetTimer;

        function setClickTimer(target, time) {
            clickTargetTimer = setTimeout(function () {
                target.prop('checked', !target.prop('checked')).change();
            }, time);
        }

        function clearClickTimer() {
            try {
                clearTimeout(clickTargetTimer);
            }
            catch (e) {
            }
        }
        if (!$('.is-mobile').length) {
            $('.main-nav-bar label').on('mouseenter mouseleave', function (e) {
                var $check = $('#' + $(this).attr('for'));
                // if there is timer --> clear it
                clearClickTimer();
                // if panel is not opened and event is enter --> set timer to open it
                if (e.type === 'mouseenter' && !$check.is(':checked')) {
                    setClickTimer($check, 150);
                }
                // if panel is opened and event is leave --> set timer to close it
                if (e.type === 'mouseleave' && $check.is(':checked')) {
                    setClickTimer($check, 150);
                }
            });

            $('.sub-nav-panel').on('mouseleave', function () {
                var $check = $(this).siblings('.check-toggler:checked');
                // if there is timer --> clear it
                clearClickTimer();
                // set new timer on leaving panel
                setClickTimer($check, 150);
            }).on('mouseenter', function () {
                // if there is timer --> clear it
                clearClickTimer();
            });
        }

        // if there is a click
        $(document).on('mouseup', function (e) {
            // and there is an opened panel —> handle it
            if ($('.main-nav-bar .check-toggler:checked').length) {
                panelHandler(e);
            }
        });

        function panelHandler(e) {
            // if clicked is NOT inside panel
            if (!$(e.target).closest('.sub-nav-panel').length) {
                // if clicked is label
                if ($(e.target).is('.main-nav-bar label')) {
                    // if there is timer --> clear it
                    if (!$('.is-mobile').length) {
                        clearClickTimer();
                    }
                } else {
                    // else —> hide all panels
                    $('.main-nav-bar .check-toggler').prop('checked', false).change();
                }
            }
        }

        $('.main-nav .item > label').on('click', function (e) {
            var $label = $(this),
                $check = $('#' + $label.attr('for'));

            if ($label.data('href')) {
                if ($('.is-mobile').length) {
                    if ($check.is(':checked')) {
                        window.location.href = $label.data('href');
                    }
                }
                else {
                    window.location.href = $label.data('href');
                }
            }
        });

        // wrap yt videos in text in responsive div shell
        var ytVideoFrame = $('iframe[src*="www.youtube.com/embed/"]');
        if (ytVideoFrame.length) {
            ytVideoFrame.each(function (i, el) {
                var $el = $(el);

                if (!$el.closest('.embed-responsive').length) {
                    $el.addClass('embed-responsive-item').wrap('<div class="embed-responsive embed-responsive-16by9"></div>');
                }
            })
        }

        if (typeof ($.fn.lazyYT) !== 'undefined') {
            $('.lazyYT').lazyYT(); //load any lazy loading Youtube videos
        }

        $searchSegment.on('change', function () {
            $.ajax({
                type: 'POST',
                async: true,
                url: baseUrl + '/header/segmentchanged',
                data: { selectedSegment: $searchSegment.val() },
                beforeSend: function () {
                    $('.m-switch-check').prop('disabled', true);
                    $searchSegment.prop('disabled', true);
                },
                success: function () {
                    window.location.reload();
                }
            });
        });

        // show prompt if adding to cart on mobile device
        if (isMobile.any()) {
            if ($('.btn-prompt').length) {
                var $btnPrompt = $('.btn-prompt');

                $btnPrompt.data('action', $btnPrompt.prop('onclick')).prop('onclick', null).off('click');

                for (var i = 0; i < $btnPrompt.length; i++) {
                    var $el = $($btnPrompt[i]);

                    $el.attr('data-prompt-id', i);
                    createPromptModal(i, $el.attr('data-prompt'), $el.data('action'));
                }

                function createPromptModal(i, text, action) {
                    $('body').append('<div id="prompt-modal' + i + '" class="prompt-modal hid"><p class="h3">' + text + '</p><p><span class="btn confirm-btn">Ok</span></p></div>');
                    $("#prompt-modal" + i).dialog({
                        modal: true,
                        resizable: false,
                        autoOpen: false,
                        height: 'auto',
                        width: 'auto',
                        close: action
                    });
                }

                $btnPrompt.on('mouseup', function (e) {
                    var $this = $(this);
                    $('#prompt-modal' + $this.attr('data-prompt-id')).dialog('open');
                    $('#prompt-modal' + $this.attr('data-prompt-id')).closest('.ui-dialog').addClass('prompt');
                    return false;
                });

                $(document).on('click', '.prompt-modal .confirm-btn', function () {
                    $(this).closest('.prompt-modal').dialog('close');
                });
            }
        }

        $(document).on('click', '.ui-widget-overlay', function () {
            $('.prompt-modal').dialog('close');
        });

        // content tabs with labels (with "+" sign aside)
        if ($('.tabs-panel-nav').length) {
            $('.tab, .label-tab').on('click', function (e) {
                e.preventDefault();

                var $this = $(this),
                    id = $this.attr('href') || $this.attr('data-href'),
                    hrefIdEl = $('[href="' + id + '"], [data-href="' + id + '"]');

                if ($this.hasClass('tab')) {
                    hrefIdEl.addClass('active').siblings().removeClass('active');
                    $(id).show().siblings('.tabs-panel').hide();
                } else {
                    $(id).toggle();
                    hrefIdEl.toggleClass('active', $(id).is(':visible'));
                }

                $this.blur();
            });

            $(window).on('resize', handleTabsResize);

            function handleTabsResize(e) {
                $('.tabs-panel-nav:visible').each(function (i, el) {
                    var $panel = $(el);
                    if (!$('.active', $panel).length) {
                        $('.tab', $panel).eq(0).trigger('click');
                    }
                });
            }
        }

        if ($dataSlick.length) {
            $dataSlick.not('.slick-initialized').slick({
                lazyLoad: 'progressive',
                arrows: true,
                dots: false
            });
        }

        // remove box slider if no slides in it
        checkRelated('#related-content-boxes');
        checkRelated('#faq-boxes');

        function checkRelated(parent) {
            parent = $(parent);
            if (!parent.length) {
                return;
            }
            if ($('.box', parent).length) {
                parent.removeClass('invisible');
            } else {
                parent.remove();
            }
        }

        // boxes slider like 'Related Content'
        if (boxSlider.length) {
            boxSlider.each(function (i, el) {
                var $el = $(el),
                    boxSliderSize = $('.box', $el).length;

                $el.attr('data-slider-size', boxSliderSize);

                $el.toggleClass('less', boxSliderSize < 4);

                $el.not('.slick-initialized').slick({
                    slidesToShow: (boxSliderSize < 4 ? boxSliderSize : 4),
                    slidesToScroll: 1,
                    centerMode: (boxSliderSize < 4),
                    arrows: true,
                    responsive: [
                        {
                            breakpoint: 992,
                            settings: {
                                centerMode: true,
                                centerPadding: '6%',
                                slidesToShow: (boxSliderSize < 3 ? boxSliderSize : 3)
                            }
                        }, {
                            breakpoint: 768,
                            settings: {
                                centerMode: true,
                                centerPadding: '12%',
                                slidesToShow: (boxSliderSize < 2 ? boxSliderSize : 2)
                            }
                        }, {
                            breakpoint: 480,
                            settings: 'unslick'
                        }
                    ]
                });

            });


            function boxSliderResizeHandler() {
                var winWidth = $window.width();

                boxSlider.each(function (i, el) {
                    var $el = $(el),
                        boxSliderSize = $el.attr('data-slider-size');

                    $el.not('.slick-initialized').slick('slickGoTo', 0);

                    var lessFlag = false;

                    if (winWidth > 992) {
                        lessFlag = boxSliderSize < 4;
                    } else if (winWidth > 768) {
                        lessFlag = boxSliderSize < 3;
                    } else {
                        lessFlag = boxSliderSize < 2;
                    }

                    boxSlider.toggleClass('less', lessFlag);
                });

                if (winWidth > 480) {
                    boxSlider.not('.slick-initialized').slick('init');
                } else {
                    boxSlider.not('.slick-initialized').slick('unslick');
                }
            }

            boxSliderResizeHandler();

            $window.on('resize', boxSliderResizeHandler);
        }

        $('.tabs-nav-list .tab.active').on('click', function (e) {
            e.preventDefault();
            var $tab = $(this);
            if ($window.width() < 767) {
                $tab.closest('.tabs-nav-list').toggleClass('opened');
            }
        });

        // carousel with pause
        if (pausingCarousel.length) {

            pausingCarousel.on('init', function (event, slick) {
                var $carousel = $(this),
                    $dots = $('.slick-dots', $carousel);

                if ($dots.length) {
                    $dots.append('<li class="slick-pause"><i class="fa"></i></li>');
                }
            }).on('beforeChange', function (event, slick) {
                var $carousel = $(this);
                var $pausingCarousel = $('.pausing-carousel');
                $pausingCarousel.css('z-index', '-1');
                if ($carousel.hasClass('paused')) {
                    $carousel.slick('slickPause');
                    $pausingCarousel.css('z-index', '1');
                } else {
                    $carousel.slick('slickPlay');
                }
            }).on('afterChange', function (event, slick) {
                $pausingCarousel.css('z-index', '1');
            }).on('click', '.slick-pause', function (e) {
                e.preventDefault();
                var $carousel = $(this).closest('.pausing-carousel');
                $carousel.toggleClass('paused').trigger('beforeChange');
            });

            pausingCarousel.not('.slick-initialized').slick({
                fade: true,
                dots: true,
                arrows: false,
                autoplaySpeed: 6000,
                adaptiveHeight: true,
                pauseOnHover: true
            });

            pausingCarousel.slick('slickPlay');
        }

        // search screen handlers
        var searchPanel = (function () {
            var $bar = $('.main-nav-bar'),
                $scr = $('.search-screen'),
                $body = $('body');

            $('.search-item').on('click', handlePanelTrigger);

            function handlePanelTrigger(e) {
                e.preventDefault();
                var $this = $(e.target).hasClass('search-item') ? $(e.target) : $(e.target).closest('.search-item');

                // if browse all panel is opened -- close it
                $('#browse-all-check:checked').length && $('#browse-all-check').prop('checked', false);

                function searchFieldFocusHandler() {
                    if (!$('#search-field').is(':focus')) {
                        $('#search-field').focus();
                    }
                }

                if ($this.hasClass('active')) {
                    $scr.removeClass('opened').css({ 'height': 0 });
                    $this.removeClass('active');
                    $body.removeClass('cover-screen');
                    $window.off('resize', setHeight);
                    $window.off('keydown', searchFieldFocusHandler);
                } else {
                    $scr.addClass('opened').css({ 'height': calcHeight() });
                    $this.addClass('active');
                    $body.addClass('cover-screen');
                    $window.on('resize', setHeight);
                    $window.on('keydown', searchFieldFocusHandler);
                }
            }

            function setHeight() {
                if ($scr.hasClass('opened')) {
                    $scr.height(calcHeight());
                }
            }

            function calcHeight() {
                return $window.height() - ($bar.height() + $scr.offset().top);
            }
        })();

        // search input handlers
        $(function () {
            var searchForm = $('#search-form'),
                searchInput = $('#search-field'),
                searchButton = $('#search-button-link');

            if (!searchInput.length) {
                return;
            }

            searchInput.autocomplete({
                source: baseUrl + '/webformspages/SearchAutoComplete.aspx?',
                varname: 'term',
                json: true,
                shownoresults: true,
                maxresults: 25
            });

            searchInput.on('keyup keypress', function (e) {

                if (e.keyCode == 13 || e.which == 13) {
                    e.preventDefault();
                    doSearch();
                }
            });

            searchButton.on('click', doSearch);

            function doSearch() {
                var query = searchInput.val();

                if (!query || searchForm.hasClass('loading-spinner')) {
                    return;
                }

                searchForm.addClass('loading-spinner');
                searchInput.blur();

                $.ajax({
                    type: 'POST',
                    async: true,
                    url: baseUrl + '/header/curatesearchtext',
                    data: { searchText: query },
                    success: function (data) {
                        if (data.curatedText.length > 0 && data.curatedText != 'Search') {
                            location.href = baseUrl + '/search/' + data.curatedText;
                        }
                    }
                });
            }

            var opts = {
                lines: 11 // The number of lines to draw
                , length: 0 // The length of each line
                , width: 8 // The line thickness
                , radius: 24 // The radius of the inner circle
                , scale: 0.5 // Scales overall size of the spinner
                , corners: 1 // Corner roundness (0..1)
                , color: '#fff' // #rgb or #rrggbb or array of colors
                , opacity: 0.2 // Opacity of the lines
                , rotate: 0 // The rotation offset
                , direction: 1 // 1: clockwise, -1: counterclockwise
                , speed: 1.5 // Rounds per second
                , trail: 60 // Afterglow percentage
                , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
                , zIndex: 2e9 // The z-index (defaults to 2000000000)
                , className: 'spinner' // The CSS class to assign to the spinner
                , top: '50%' // Top position relative to parent
                , left: 'auto' // Left position relative to parent
                , shadow: false // Whether to render a shadow
                , hwaccel: false // Whether to use hardware acceleration
                , position: 'absolute' // Element positioning
            }

            // handler for search selector in nav bar
            $('#search-segment-selector-list .item').on('click', function (e) {
                e.preventDefault();
                var item = $(this),
                    itemLabel = item.text(),
                    itemValue = item.attr('data-value');

                item.addClass('active').siblings().removeClass('active');
                $searchSegment.val(itemValue).trigger('change');
                $('[for="search-segment-selector"]').attr('data-title', itemLabel).text(itemLabel);
                $('#search-segment-selector').prop('checked', false);
                checkMobileSearchSegment();
            });

            //handler for search selector mobile
            function checkMobileSearchSegment() {
                var searchSegmentValue = $searchSegment.val();

                $('.m-switch-check').prop('checked', false);
                if ($('.m-switch-check[value="' + searchSegmentValue + '"]').length) {
                    $('.m-switch-check[value="' + searchSegmentValue + '"]').prop('checked', true);
                } else if (searchSegmentValue) {
                    $('.m-switch-check').prop('checked', true);
                }
            }

            checkMobileSearchSegment();
            if (isMobile.any()) {
                $window.on('resize', checkMobileSearchSegment);
            }

            $('.m-switch-check').on('change', function () {
                var valueALL = ';#IBCU;#LARGE/MID TIER;#',
                    $switch = $(this),
                    state = $switch.prop('checked'),
                    value;

                if (!state) {
                    $('.m-switch-check').not($switch).prop('checked', true);
                }

                value = state ? $switch.val() : $('.m-switch-check:checked').val();

                var segment = ($('.m-switch-check:not(:checked)').length) ? value : valueALL;
                $searchSegment.val(segment).trigger('change');

            });
        });

        $('.nav-footer-map dt').on('click', function () {
            var $this = $(this);

            if (window.innerWidth < 481) {
                $this.closest('dl').toggleClass('opened');
            }
        });

        if ($('.ie8').length) {
            $('.cover-image, .icon-tiles .cell > .label, .cell .inner-text').each(function (i, el) {
                var $el = $(el);
                $el.css({
                    marginTop: 0 - $el.height() / 2,
                    marginLeft: 0 - $el.width() / 2
                });
            });

            $('.thumb img').each(function (i, el) {
                var $el = $(el),
                    src = $el.attr('src');

                $el.closest('.thumb').css({
                    'background': 'no-repeat url(' + src + ') center'
                });

                $el.css('visibility', 'hidden');
            });

            $('.text-slide .more a').each(function (i, el) {
                var $el = $(el);
                $el.css({
                    marginLeft: 0 - $el.width() / 2
                });
            });
        }
    });
}
