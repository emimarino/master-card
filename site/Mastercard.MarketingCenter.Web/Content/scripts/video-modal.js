﻿/*================== amazon urls =============================*/
var videoBaseUrl = '';
var bucket = '';
var s3Bucket = '';
if (typeof (bucket) !== 'undefined' && bucket !== '') {
    s3Bucket = bucket;
}

var s3 = 'https://s3.amazonaws.com/' + s3Bucket + '/';
var amazon = {
    's3': s3,
    'rtmp': 'rtmp://s24w6z9cizy0mf.cloudfront.net/cfx/st/'
};
if (s3Bucket === '') {
    videoBaseUrl = window.location.origin + '/portal/content/files/videos/';
}
else {
    videoBaseUrl = amazon["s3"];
}
/*============================================================*/

var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i);
    }
};

var videoInfo = '';
var vidName = '';
var vidHeight = '';
var vidWidth = '';
var vidSrc = false;
var jwp = [];

// internal video links processing
var getVideoInfo = function (e) {
    var $el = $(e),
        videoInfo = $el.attr('data-video') || $el.attr('name') || false;

    videoInfo = videoInfo ? videoInfo.split(':') : false;

    vidName = $el.attr('data-video-name') || videoInfo[0] || '';
    vidWidth = $el.attr('data-video-width') || parseInt(videoInfo[1], 10) || 'auto';
    vidHeight = $el.attr('data-video-height') || parseInt(videoInfo[2], 10) || 'auto';

    vidSrc = $el.attr('data-video-src') || false;
}

var buildModal = function (i) {
    $('body').append('<div id="video-modal' + i + '" class="video-modal hid" data-h="' + vidHeight + '" data-w="' + vidWidth + '"><div id="player' + i + '" style="max-width:' + vidWidth + ';max-height:' + vidHeight + ';"></div></div>');
    $("#video-modal" + i).dialog({
        modal: true,
        autoOpen: false,
        resizable: false,
        maxWidth: vidWidth,
        maxHeight: vidHeight,
        position: {
            my: "center",
            at: "center",
            of: window
        },
        create: function () {
            $(document).on('click', '.ui-widget-overlay', function () {
                $("#video-modal" + i).dialog('close');
            });
        },
        open: function () {
            var $dialog = $("#video-modal" + i).closest('.ui-dialog');
            $dialog.addClass('opened');
        },
        close: function () {
            jwp[i].stop();
            $(document).off('click', '.ui-widget-overlay');
            $('.ui-dialog.opened').removeClass('opened');
        }
    });
}

// change video size proportionally when resizing page
function fixPlayerSize() {

    var playerSize = calcPlayerSize();

    var w = playerSize[0],
        h = playerSize[1];

    var $box = $('[id^="player"]:visible'),
        $dialog = $box.closest('.ui-dialog');

    var left = '50%',
        mleft = 0 - (w / 2);

    if (window.innerWidth < 768) {
        left = mleft = 0;
        w = '100%';
    }

    $dialog.width(w).height('auto');

    if ($box.is('iframe')) {
        $box.width(w).height(h);
    }

    $dialog.css({
        'position': 'fixed',
        'left': left,
        'marginLeft': mleft,
        'top': '50%',
        'marginTop': 0 - (h / 2)
    });
}

function calcPlayerSize($player) {
    var $window = $(window),
        offsetW = ($window.width() < 768) ? 0 : 120,
        offsetH = 120,
        ww = $window.width() - offsetW,
        wh = $window.height() - offsetH,
        $player = $player || $('[id^="video-modal"]:visible'),
        pw = parseInt($player.attr('data-w'), 10),
        ph = parseInt($player.attr('data-h'), 10);

    var wr = ww / wh,
        pr = pw / ph;

    var w, h;

    if (wr > pr) {
        w = pw * wh / ph;
        h = wh;
    } else {
        w = ww;
        h = ph * ww / pw;
    }

    w = w > pw ? pw : w;
    h = h > ph ? ph : h;

    return [parseInt(w, 10), parseInt(h, 10)];
}

$(window).on('resize', function () {
    if ($('[id^="video"]:visible').length) {
        fixPlayerSize();
    }
});

var setupPlayer = function (i, width, height, name, url) {
    var defaultSrc = videoBaseUrl + name,
        src = url ? url : defaultSrc,
        playerOptions = {
            'file': src,
            'width': '100%',
            'aspectratio': width + ':' + height,
            'controlbar': 'over',
            'autostart': 'true',
            'stretching': 'fill',
            'skin': {
                'name': 'glow'
            }
        };

    // if host is amazon --> include streamer
    if (bucket && typeof (bucket) !== 'undefined' && src === defaultSrc && $('.ie10').length) {
        playerOptions['primary'] = 'flash';
        playerOptions['sources'] = [{
            'file': amazon['rtmp'] + name
        }, {
            'file': src
        }];
    }

    jwp[i] = jwplayer('player' + i);

    jwp[i].setup(playerOptions).on('ready', fixPlayerSize);
}

// you tube video links processing
var yt = (function () {
    var players = {};
    var frameApiScript;

    // collect info from "data-yt" attribute,
    // data-yt="youtubeid::width::height"
    function getVideoInfo(el) {
        var $el = $(el);
        var data = $el.data('yt').split('::');

        return {
            id: data[0],
            w: data[1],
            h: data[2]
        }
    }

    var $dialog = {};
    // set up modal for iframe
    function createModal(data) {

        $('body').append('<div id="video-modal' + data.id + '" class="video-modal hid" data-h="' + data.h + '" data-w="' + data.w + '"><div id="player' + data.id + '" style="display: block;"></div></div>');

        $dialog[data.id] = $("#video-modal" + data.id).dialog({
            modal: true,
            autoOpen: false,
            height: 'auto',
            width: 'auto',
            resizable: false,
            create: function () {
                $(".slick-slider").ready(function () {
                    $('[data-video-id="' + data.id + '"]').addClass('ready');
                });
                initVideo(data);
            },
            open: function () {
                var videoLoaded = function () {
                    if (typeof (YT) == 'undefined' || typeof (YT.Player) == 'undefined') {
                        setTimeout(function () {
                            videoLoaded();
                        }, 50);
                    } else {
                        $("#video-modal" + data.id).addClass('opened');
                        fixPlayerSize();
                        if (typeof players[data.id].playVideo == 'function') {
                            players[data.id].playVideo();
                        }
                        $(document).on('click', '.ui-widget-overlay', function () {
                            $dialog[data.id].dialog('close');
                        });
                    }
                };
                videoLoaded();
            },
            close: function () {
                players[data.id].pauseVideo();
                $(document).off('click', '.ui-widget-overlay');
                $('.video-modal.opened').removeClass('opened');
            }
        });
    }

    // set up iframe though youtube api
    function initVideo(data) {

        function addPlayer() {
            players[data.id] = new YT.Player('player' + data.id, {
                height: data.h,
                width: data.w,
                videoId: data.id,
                playerVars: {
                    rel: 0
                },
                events: {
                    'onReady': onPlayerReady
                }
            });
        }

        if (typeof (YT) == 'undefined' || typeof (YT.Player) == 'undefined') {
            if (typeof (frameApiScript) === 'undefined') {
                frameApiScript = document.createElement('script');

                frameApiScript.src = "https://www.youtube.com/iframe_api";
                var firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(frameApiScript, firstScriptTag);
            }

            var afterYTLoads = function (callback) {
                if (typeof (YT) == 'undefined' || typeof (YT.Player) == 'undefined') {
                    setTimeout(function () {
                        afterYTLoads(callback);
                    }, 50);
                } else {
                    callback();
                }
            };
            afterYTLoads(addPlayer);
        }
        else {
            addPlayer();
        }

        function onPlayerReady(event) {
            if ($('#video-modal' + data.id).closest('ui-dialog').hasClass('opened')) {
                event.target.playVideo();
            }
        }
    }

    function placeVideo(data) {
        if ($('[data-video-id="' + data.id + '"]').hasClass('embed')) {
            return;
        }

        $('[data-video-id="' + data.id + '"]').addClass('embed').append('<div class="embed-responsive embed-responsive-16by9"><div class="embed-responsive-item" id="player' + data.id + '"></div></div>');

        initVideo(data);
    }

    return {
        getVideoInfo: getVideoInfo,
        initVideo: initVideo,
        createModal: createModal,
        placeVideo: placeVideo,
        players: players
    }
})();

$('document').ready(function () {
    var vEmbed = {
        build: function (el, i) {
            /* collect info */
            var $el = $(el),
                path = videoBaseUrl, //amazon['s3'],
                shortInfo = $el.attr('data-video') || $el.attr('name') || false;

            shortInfo = shortInfo ? shortInfo.split(':') : false;

            var fileName = $el.attr('data-video-name') || shortInfo[0] || '',
                width = $el.attr('data-video-width') || parseInt(shortInfo[1], 10) || 'auto',
                height = $el.attr('data-video-height') || parseInt(shortInfo[2], 10) || 'auto',
                preview = $el.attr('data-video-preview') || '';

            fileName = (fileName.split('.mp4').length > 1) ? fileName.split('.mp4').shift() : fileName;

            /* setup player */
            $el.before('<div id="' + fileName + '-' + i + '"></div>');

            var playerInstance = jwplayer(fileName + '-' + i);

            playerInstance.setup({
                file: path + fileName + ".mp4",
                image: path + fileName + ".jpg",
                width: '100%',
                aspectratio: width + ':' + height,
                mediaid: fileName,
                skin: { name: 'glow' }
            });

            $el.remove();
        }
    }

    if (isMobile.any()) {
        $('body').addClass('is-mobile');
    }

    var videoEmbed = $('.video-player-embed');
    if (videoEmbed.length) {
        for (var i = 0; i < videoEmbed.length; i++) {
            vEmbed.build(videoEmbed[i], i);
        }
    }

    var videoLinks = $('.video-link:not(.yt)');
    if (videoLinks.length) {

        for (var i = 0; i < videoLinks.length; i++) {
            getVideoInfo(videoLinks[i]);

            buildModal(i);

            var $vlink = $(videoLinks[i]);

            if ($vlink.is('a')) {
                $vlink.attr('href', '#');
            }

            if ($vlink.hasClass('video-preview') && !$vlink.children('img').length && $vlink.attr('data-video-src')) {
                $vlink.append('<img src="' + $vlink.attr('data-video-src').replace('.', '-') + '.jpg" class="img-responsive" alt="" />');
            }

            $vlink.attr('videoId', i);

            $vlink.on('click', function (e) {
                e.preventDefault();

                var videoId = $(this).attr('videoId');

                $('#video-modal' + videoId).dialog('open');

                getVideoInfo(this);

                setupPlayer(videoId, vidWidth, vidHeight, vidName, vidSrc);
            });
        }
    }

    var videoLinksYT = $('.video-link.yt');
    if (videoLinksYT.length) {

        for (var i = 0; i < videoLinksYT.length; i++) {

            var el = videoLinksYT[i],
                data = yt.getVideoInfo(el);

            if (data.id) {
                $(el).attr('data-video-id', data.id);

                if ($('.is-mobile').length) {
                    yt.placeVideo(data);
                } else {
                    yt.createModal(data);
                }

                $('[data-video-id]').on('click', function (e) {
                    e.preventDefault();
                    var $wrap = $(this),
                        id = $wrap.attr('data-video-id');

                    if (!$('.is-mobile').length) {
                        $('#video-modal' + id).dialog('open');
                    }
                });
            }
        }
    }
});
