﻿$('document').ready(function () {
    var s3 = 'https://s3.amazonaws.com/' + bucket + '/';
    var id = window.location.search.substring(1).split('=')[1];
    var videoList = [
	/*0*/{ name: 'Kathy_1.mp4', width: '720', height: '540' },
	/*1*/{ name: 'Christina_2.mp4', width: '720', height: '540' },
	/*2*/{ name: 'Westley_3.mp4', width: '720', height: '540' }
    ]
    jwplayer('player-div').setup({
        'controlbar': 'over',
        'file': videoList[id].name,
        'width': videoList[id].width,
        'autostart': 'true',
        'stretching': 'exactfit',
        'height': videoList[id].height,
        modes: [
            {
                type: 'flash',
                src: "/portal/content/jwplayer/player.swf"
            },
            {
                type: 'html5',
                config: {
                    file: s3 + videoList[id].name,
                    provider: "video"
                }
            }
        ],
        provider: 'rtmp',
        streamer: 'rtmp://s24w6z9cizy0mf.cloudfront.net/cfx/st/'
    });
});
