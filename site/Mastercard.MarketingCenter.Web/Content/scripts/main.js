﻿$(document).ready(function () {
    var baseUrl = '';
    if (typeof (baseSiteUrl) !== 'undefined' && baseSiteUrl) {
        baseUrl = baseSiteUrl;
    }

    $('#segment-field').change(function () {
        $.ajax({
            type: 'POST',
            async: true,
            url: baseUrl + '/portal/header/segmentchanged',
            data: { selectedSegment: $('#segment-field').val() },
            success: function () {
                window.location.reload();
            }
        });
    });


    $('#search-field').autocomplete({
        script: baseUrl + '/portal/webformspages/SearchAutoComplete.aspx?',
        varname: 'input',
        json: true,
        shownoresults: true,
        maxresults: 25
    });


    var searchForm = $('#search-form'),
        searchInput = $('#search-field'),
        searchButton = $('#search-button-link');

    if (!searchInput.length) {
        return;
    }

    searchInput.autocomplete({
        source: baseUrl + '/portal/webformspages/SearchAutoComplete.aspx?',
        varname: 'input',
        json: true,
        shownoresults: true,
        maxresults: 25
    });

    searchInput.on('keyup keypress', function (e) {

        if (e.keyCode == 13 || e.which == 13) {
            e.preventDefault();
            doSearch();
        }
    });

    searchButton.on('click', doSearch);

    function doSearch() {
        var query = searchInput.val();

        if (!query || searchForm.hasClass('loading-spinner')) {
            return;
        }

        searchForm.addClass('loading-spinner');
        searchInput.blur();

        $.ajax({
            type: 'POST',
            async: true,
            url: baseUrl + '/portal/header/curatesearchtext',
            data: { searchText: query },
            success: function (data) {
                if (data.curatedText.length > 0 && data.curatedText != 'Search') {
                    location.href = baseUrl + '/portal/search/' + data.curatedText;
                }
            }
        });
    }
});
