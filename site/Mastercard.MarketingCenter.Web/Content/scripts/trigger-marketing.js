var DATA_UPDATED = false;
Vue.component('category-list', {
    template: '#category-list-template',
    props: ['categories', 'level'],
    methods: {
        markAsActive: function (category) {
            if (!category.isActive) {
                category.isActive = true;
                if (this.level === 0 && this.$parent.active_2nd_level !== category) {
                    if (this.$parent.active_2nd_level !== null) {
                        this.$parent.active_2nd_level.isActive = false;
                    }

                    this.$parent.active_2nd_level = category;

                    if (this.$parent.active_3th_level !== null) {
                        this.$parent.active_3th_level.isActive = false;
                    }

                    this.$parent.active_3th_level = null;
                }
                else if (this.level === 1 && this.$parent.active_3th_level !== category) {
                    if (this.$parent.active_3th_level !== null) {
                        this.$parent.active_3th_level.isActive = false;
                    }

                    this.$parent.active_3th_level = category;
                }
            }
        }
    }
});

Vue.component('category-item', {
    template: '#category-item-template',
    props: ['item'],
    methods: {
        subscribe: function (category) {
            DATA_UPDATED = true;
            category.subscribed = !category.subscribed;
        },
        markAsSelected: function (category) {
            this.$emit('active', category);
        },
    },
    computed: {
        haveChildrenSubscribed: function () {
            if (!this.item.subscribed) {
                return this.childrenSubscribed > 0;
            }
            return false;
        },
        childrenSubscribed: function () {
            if (this.item.items) {
                var childrenSubscribed = this.item.items.filter(function (el) {
                    var subChildrenSubscribed = [];
                    if (el.items) {
                        subChildrenSubscribed = el.items.filter(function (sub) {
                            return sub.subscribed === true;
                        });
                    }
                    return el.subscribed === true ? subChildrenSubscribed.length : 0;
                });
                return childrenSubscribed.length;
            }
            return 0;
        }
    }
});

Vue.component('favorite-list', {
    template: '#favorite-list-template',
    props: ['pages'],
    methods: {
        subscribe: function (page) {
            page.subscribed = !page.subscribed
        }
    }
});
