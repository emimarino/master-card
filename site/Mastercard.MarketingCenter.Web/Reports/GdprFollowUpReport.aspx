﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="GdprFollowUpReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.GdprFollowUpReport" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
        <div style="background-color: #ffffff; width: 100%; height: 100%; display: flex; justify-content: center; flex-direction: column; align-items: center;">
            <img src="<%= Mastercard.MarketingCenter.Common.Infrastructure.Constants.ImageUrls.ReportsLoader %>" alt="Loading..." />
        </div>
    </telerik:RadAjaxLoadingPanel>
    <h2 style="color: #f58025;">GDPR Follow-up Report</h2>
    <telerik:RadGrid ID="trgReport" Width="1200" AllowSorting="True" PageSize="15" AllowPaging="True" runat="server" GridLines="None" AutoGenerateColumns="False" OnNeedDataSource="trgReport_NeedDataSource" AllowFilteringByColumn="true" ShowGroupPanel="true">
        <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
            <CommandItemSettings ShowExportToWordButton="true" ShowExportToExcelButton="true" ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false" />
            <Columns>
                <telerik:GridBoundColumn HeaderText="Email" DataField="Email" HeaderStyle-Width="200"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="User" DataField="FullName"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Notification Frequency" DataField="NotificationFrequency"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Registration Date" DataField="RegistrationDate" DataFormatString="{0:MM/dd/yyyy}"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Last Login Date" DataField="LastLoginDate" DataFormatString="{0:MM/dd/yyyy}"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Months of Inactivity" DataField="LastLoginDate">
                    <ItemTemplate><%# GetInactivityDate((DateTime)Eval("LastLoginDate")) %></ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Email Type" DataField="GdprFollowUpStatus"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Sent Date" DataField="SentDate" DataFormatString="{0:MM/dd/yyyy}"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Opened Date" DataField="OpenedDate" DataFormatString="{0:MM/dd/yyyy}"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
            <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
        </ClientSettings>
        <PagerStyle Mode="NextPrevAndNumeric" />
        <HeaderStyle Width="100px" />
        <ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="False" />
    </telerik:RadGrid>
    <asp:PlaceHolder ID="phNoResults" runat="server" Visible="false">
        <p style="color: #F58025;"><em>No results found.</em></p>
    </asp:PlaceHolder>
</asp:Content>