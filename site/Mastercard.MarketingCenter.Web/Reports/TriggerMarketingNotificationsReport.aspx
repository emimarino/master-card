﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="TriggerMarketingNotificationsReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.TriggerMarketingNotificationsReport" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="mc" TagName="ContentActivityDetailFilterDisplay" Src="~/Reports/UserControls/ContentActivityDetailFilterDisplay.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
        <div style="background-color: #ffffff; width: 100%; height: 100%; display: flex; justify-content: center; flex-direction: column; align-items: center;">
            <img src="<%= Mastercard.MarketingCenter.Common.Infrastructure.Constants.ImageUrls.ReportsLoader %>" alt="Loading..." />
        </div>
    </telerik:RadAjaxLoadingPanel>
    <div class="clr" style="margin-top: 10px;">
        <a id="A1" href="~/report/triggermarketingnotificationslanding" runat="server">< Search again</a>
    </div>
    <h2 style="color: #f58025;">Email Notifications Report</h2>
    <p><strong>List of Email Notifications</strong></p>
    <p><mc:ReportDateRangeDisplay id="ucDateRangeDisplay" runat="server"></mc:ReportDateRangeDisplay></p>
    <telerik:RadGrid ID="trgReport" Width="1200" AllowSorting="True" PageSize="15" AllowPaging="True" runat="server" GridLines="None" AutoGenerateColumns="False" OnNeedDataSource="trgReport_NeedDataSource" AllowFilteringByColumn="true" ShowGroupPanel="true" OnItemCommand="trgReport_ItemCommand">
        <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
            <CommandItemSettings ShowRefreshButton="" ShowExportToWordButton="true" ShowExportToExcelButton="true"
                ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false" />
            <Columns>
                <telerik:GridBoundColumn HeaderText="Notification Date" DataField="NotificationDate" DataFormatString="{0:MM/dd/yyyy}" Visible="false"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Notification Date" SortExpression="NotificationDate" UniqueName="NotificationDateLink" DataField="NotificationDate">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" CommandArgument='<%# String.Format("{0}?notificationDate={1}", DataBinder.Eval(Container.DataItem,"TrackingType"), DataBinder.Eval(Container.DataItem,"NotificationDate", "{0:MM/dd/yyyy}")) %>' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"NotificationDate", "{0:MM/dd/yyyy}") %>' runat="server" />
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Notification Frequency" DataField="NotificationFrequency"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Recipients" DataField="Recipients"></telerik:GridBoundColumn>
                <%--<telerik:GridBoundColumn HeaderText="Opened Emails" DataField="OpenedEmails"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Total Emails" DataField="TotalEmails"></telerik:GridBoundColumn>--%>
                <telerik:GridBoundColumn HeaderText="Open Email Rate" DataField="OpenEmailRate" DataFormatString="{0:P}"></telerik:GridBoundColumn>
                <%--<telerik:GridBoundColumn HeaderText="Clicked Links" DataField="ClickedLinks"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Total Links" DataField="TotalLinks"></telerik:GridBoundColumn>--%>
                <telerik:GridBoundColumn HeaderText="Click Rate" DataField="ClickRate" DataFormatString="{0:P}"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
            <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
        </ClientSettings>
        <PagerStyle Mode="NextPrevAndNumeric" />
        <HeaderStyle Width="100px" />
        <ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="False" />
    </telerik:RadGrid>

    <asp:PlaceHolder ID="phNoResults" runat="server" Visible="false">
        <p style="color: #F58025;"><em>No results found, please try your search again.</em></p>
    </asp:PlaceHolder>
</asp:Content>