﻿using System;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class UserRegistrationReportLanding : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnValidateCode.OnClientClick = (Master as ReportLanding)?.GetLoader();
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            if (ucDateRangeSelector.Validate())
            {
                Response.Redirect($"~/report/userregistration?sd={ucDateRangeSelector.StartDate.Value.ToString("MM/dd/yyyy")}&ed={ucDateRangeSelector.EndDate.Value.ToString("MM/dd/yyyy")}&fmp={chkMasterCard.Checked}&feu={chkExpiredUsers.Checked}");
            }
        }
    }
}