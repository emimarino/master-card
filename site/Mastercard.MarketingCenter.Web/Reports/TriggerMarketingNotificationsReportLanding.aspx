﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportLanding.Master" AutoEventWireup="true" CodeBehind="TriggerMarketingNotificationsReportLanding.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.TriggerMarketingNotificationsReportLanding" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="color: #f58025;">Email Notifications Report</h2>
    <p><strong>List of Email Notifications</strong></p>
    <p>Please specify a date range for the report:</p>
    <mc:ReportDateRangeSelector id="ucDateRangeSelector" runat="server"></mc:ReportDateRangeSelector>
    <div style="padding-top: 2px;">
        <div class="btn_left" style="float: left; margin-left: 20px; margin-top: 2px;">
            <asp:LinkButton ID="btnValidateCode" runat="server" Text="Go" CssClass="btn_right" OnClick="btnGo_Click"></asp:LinkButton>
        </div>
    </div>
    <div class="clr report-notes">
        <em></em>
    </div>
    <script type="text/javascript" src="/admin/Content/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="/admin/Content/js/report/report-loader.js"></script>
</asp:Content>