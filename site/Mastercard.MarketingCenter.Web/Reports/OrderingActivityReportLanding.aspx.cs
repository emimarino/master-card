﻿using System;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class OrderingActivityReportLanding : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnValidateCode.OnClientClick = (Master as ReportLanding)?.GetLoader();
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            if (ucDateRangeSelector.Validate())
            {
                string reportTypeUrl = "orderingactivity";

                int selectedReportType = 0;

                if (int.TryParse(rblReportType.SelectedValue, out selectedReportType) && selectedReportType == 1)
                {
                    reportTypeUrl = "orderingactivityissuer";
                }

                Response.Redirect($"~/report/{reportTypeUrl}?sd={ucDateRangeSelector.StartDate.Value.ToString("MM/dd/yyyy")}&ed={ucDateRangeSelector.EndDate.Value.ToString("MM/dd/yyyy")}");
            }
        }
    }
}