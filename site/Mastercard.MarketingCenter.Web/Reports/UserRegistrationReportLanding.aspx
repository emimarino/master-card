﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportLanding.Master" AutoEventWireup="true" CodeBehind="UserRegistrationReportLanding.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.UserRegistrationReportLanding" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="color:#f58025;">User Registration Details Report</h2>
    <p>Please specify a date range for the report:</p>
    <mc:ReportDateRangeSelector id="ucDateRangeSelector" runat="server"></mc:ReportDateRangeSelector>
    <div style="padding-top:2px;">
        <div class="btn_left" style="float:left;margin-left:20px;margin-top:2px;">
            <asp:LinkButton ID="btnValidateCode" runat="server" Text="Go" CssClass="btn_right" OnClick="btnGo_Click"></asp:LinkButton>
        </div>
    </div>
    <div class="clr report-notes">
        <em>User Registration data effective as of 2/2009.</em>
    </div>
    <div class="clr" style="margin-top: 20px;" runat="server">
        <p><em>OPTIONAL.</em> Please indicate if you would like the following group(s) included in this report:</p>
        <asp:CheckBox ID="chkMasterCard" Text="Mastercard Employees" runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:CheckBox ID="chkExpiredUsers" Text="Expired Users" runat="server" />
    </div>
    <script type="text/javascript" src="/admin/Content/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="/admin/Content/js/report/report-loader.js"></script>
</asp:Content>
