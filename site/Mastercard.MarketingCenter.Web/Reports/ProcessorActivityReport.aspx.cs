﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class ProcessorActivityReport : BaseReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();
            var results = reportRepository.GetProcessorActivityReport(StartDate, EndDate).ToList();
            trgReport.DataSource = results;
            if (results.Count == 0)
            {
                phReport.Visible = false;
                phNoResults.Visible = true;
            }
        }

        protected void trgReport_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "redirect")
            {
                Response.Redirect("~/report/processoractivitydetail/{0}?{1}".F(e.CommandArgument, Request.QueryString));
            }

            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName || e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToWordCommandName)
            {
                trgReport.MasterTableView.GetColumn("Processor").Visible = true;
                trgReport.MasterTableView.GetColumn("ProcessorLink").Visible = false;
            }
        }
    }
}