﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Services;
using Slam.Cms.Data;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class EstimatedDistributionProgramDetailReport : BaseReportPage
    {
        private SlamContext _slamContext;
        protected void Page_Load(object sender, EventArgs e)
        {
            _slamContext = DependencyResolver.Current.GetService<SlamContext>();
            if (!IsPostBack)
            {
                var id = Page.RouteData.Values["id"].ToString();
                var contentItem = _slamContext.CreateQuery().FilterContentItemId(id).FilterStatus(FilterStatus.None).Get().FirstOrDefault();

                var program = contentItem as ProgramDTO;
                if (program != null)
                {
                    lblContentDetails.Text = program.Title;
                }

                aBack.HRef = $"~/report/estimateddistributionprogram?sd={StartDate.ToString("MM/dd/yyyy")}&ed={EndDate.ToString("MM/dd/yyyy")}&fmp={FilterMasterCardUsers}&audience={string.Join(";", AudienceSegmentationIds)}&type=1";

                if (User.IsInPermission(Constants.Permissions.CanChangeSegments))
                {
                    trgReport.MasterTableView.GetColumn("AudienceSegments").Display = true;
                }
                else
                {
                    trgReport.MasterTableView.GetColumn("AudienceSegments").Display = false;
                }

                ucFilterDisplay.Setup(segmentationService.GetSegmentationsByRegion(userContext.SelectedRegion), AudienceSegmentationIds, FilterMasterCardUsers, FilterVendorProcessor);
            }
        }

        protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();
            var results = reportRepository.GetEstimatedDistributionProgramDetailReport(StartDate, EndDate, GetAudienceSegmentationIdsForReport(), Page.RouteData.Values["id"].ToString(), FilterMasterCardUsers.Value).ToList();
            var assets = _slamContext.CreateQuery()
                .FilterContentTypes<DownloadableAssetDTO>()
                .FilterContentItemId(results.Select(r => r.ContentItemId).ToArray())
                .Get<DownloadableAssetDTO>();
            results = results.Where(r => assets.Any(d => d.PrimaryContentItemId == r.ContentItemId && d.StatusID >= 3)).ToList();
            foreach (var result in results)
            {
                result.MostRecentDownload = result.MostRecentDownload?.Date;
                result.LastRequestDate = result.LastRequestDate?.Date;
                var asset = assets.FirstOrDefault(d => d.PrimaryContentItemId == result.ContentItemId);
                if (asset != null)
                {
                    result.AssetTitle = asset.Title;
                }
                if (result.Issuer != null)
                {
                    result.IssuerName = result.Issuer.Title;
                }
                if (result.Segmentations.Any())
                {
                    result.AudienceSegments = string.Join(", ", result.Segmentations.Select(s => s.SegmentationName));
                }
            }

            trgReport.DataSource = results;
        }
    }
}