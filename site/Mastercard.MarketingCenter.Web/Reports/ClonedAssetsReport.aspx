﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="ClonedAssetsReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.ClonedAssetsReport" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register tagPrefix="mc" tagname="ContentActivityDetailFilterDisplay" src="~/Reports/UserControls/ContentActivityDetailFilterDisplay.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel  ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
        <div style="background-color:#ffffff; width:100%; height:100%; display: flex; justify-content: center; flex-direction: column; align-items: center;">
            <img src="<%= Mastercard.MarketingCenter.Common.Infrastructure.Constants.ImageUrls.ReportsLoader %>" alt="Loading..." />
        </div>
    </telerik:RadAjaxLoadingPanel >
    <h2 style="color:#f58025;">Cloned Assets Report</h2>
    <telerik:RadGrid ID="trgReport" Width="800" 
        AllowSorting="True" 
        PageSize="15" 
        AllowPaging="True" 
        runat="server" 
        Gridlines="None" 
        AutoGenerateColumns="False" 
        OnNeedDataSource="trgReport_NeedDataSource" 
        AllowFilteringByColumn="true" 
        ShowGroupPanel="true" 
        OnItemCommand="trgReport_ItemCommand" 
        OnPreRender="trgReport_PreRender">
        <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
            <CommandItemSettings ShowRefreshButton="" ShowExportToWordButton="true" ShowExportToExcelButton="true"
                ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false"/>
            <Columns>
                <telerik:GridBoundColumn HeaderText="Type" DataField="ContentType" FilterControlWidth="60"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Title" DataField="Title" HeaderStyle-Width="250" Visible="true" SortExpression="Title" GroupByExpression="Title group by Title" UniqueName="TitleLink"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Cloned by" DataField="ClonedBy" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Date" DataField="Date" FilterControlWidth="50" DataFormatString="{0:yyyy-MM-dd}"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
            <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
        </ClientSettings>
        <PagerStyle Mode="NextPrevAndNumeric" />
        <HeaderStyle Width="100px" />
        <ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="False" />
    </telerik:RadGrid>
    <asp:PlaceHolder ID="phNoResults" runat="server" Visible="false">
        <p style="color:#F58025;"><em>No results found.</em></p>
    </asp:PlaceHolder>
</asp:Content>