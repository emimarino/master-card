﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class ContentActivityPageReport : BaseReportPage
    {
        private IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }
        private bool exporting = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ucFilterDisplay.Setup(segmentationService.GetSegmentationsByRegion(userContext.SelectedRegion), AudienceSegmentationIds, FilterMasterCardUsers, FilterVendorProcessor);
            }
        }

        protected void trgReport_PreRender(object sender, EventArgs e)
        {
            if (exporting)
            {
                foreach (var header in trgReport.MasterTableView.GetItems(GridItemType.Header))
                {
                    foreach (TableCell cell in header.Cells)
                    {
                        cell.Style["text-align"] = "left";
                    }
                }
            }
        }

        protected void trgReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();
            
            var results = reportRepository.GetContentActivityPageReport(StartDate, EndDate, GetAudienceSegmentationIdsForReport(), FilterMasterCardUsers.Value, FilterVendorProcessor.Value, userContext.SelectedRegion).ToList();

            if (results.Any())
            {
                results.Insert(0, new Data.Entities.Reporting.ContentActivityPageReportResultItem
                {
                    Title = "[All]",
                    ContentItemId = "All-pages",
                    TotalHits = results.Sum(x => x.TotalHits),
                    Generic = results.Sum(x => x.Generic),
                    Marquee = results.Sum(x => x.Marquee),
                    Tab = results.Sum(x => x.Tab),
                    Navigation = results.Sum(x => x.Navigation),
                    CrossSellBox = results.Sum(x => x.CrossSellBox),
                    ProductFinder = results.Sum(x => x.ProductFinder),
                    Email = results.Sum(x => x.Email),
                    MyFavorites = results.Sum(x => x.Email),
                    MostPopular = results.Sum(x => x.MostPopular)
                });
                trgReport.DataSource = results;
            }
            else
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }
        }

        protected void trgReport_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "redirect")
            {
                Response.Redirect($"{_urlService.GetContentActivityReportURL()}/{{0}}?{{1}}".F(e.CommandArgument, Request.QueryString));
            }

            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName || e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToWordCommandName)
            {
                if (e.CommandName == RadGrid.ExportToPdfCommandName)
                {
                    exporting = true;
                    trgReport.ExportSettings.Pdf.PageHeight = Unit.Parse("162mm");
                    trgReport.ExportSettings.Pdf.PageWidth = Unit.Parse("400mm");
                }

                trgReport.MasterTableView.GetColumn("Title").Visible = true;
                trgReport.MasterTableView.GetColumn("TitleLink").Visible = false;
            }
        }
    }
}