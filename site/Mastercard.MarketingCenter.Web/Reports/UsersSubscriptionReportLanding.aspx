﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportLanding.Master" AutoEventWireup="true" CodeBehind="UsersSubscriptionReportLanding.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.UsersSubscriptionReportLanding" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="color: #f58025;">User Subscriptions and Favorites Report</h2>
    <p><strong>List of Active User's Subscriptions and Favorites</strong></p>

    <div class="clr" style="height: 50px;">
        <p>Please specify a date range for the report:</p>
        <mc:ReportDateRangeSelector id="ucDateRangeSelector" runat="server"></mc:ReportDateRangeSelector>
    </div>

    <div class="clr" style="margin-top: 20px;" runat="server" id="optionalSection">
        <p><em>OPTIONAL.</em> Please indicate if you would like the following group(s) included in this report:</p>
        <asp:CheckBox ID="chkMasterCard" Text="Mastercard Employees" runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:CheckBox ID="chkVendor" Text="Vendor" runat="server" />
    </div>

    <div style="padding-top: 2px; width: 400px; margin-top: 20px;">
        <div class="btn_left" style="float: left; margin-left: 20px; margin-top: 2px;">
            <asp:LinkButton ID="btnValidateCode" runat="server" Text="Go" CssClass="btn_right" OnClick="btnGo_Click"></asp:LinkButton>
        </div>
    </div>
    <div class="clr report-notes">
        <em></em>
    </div>
    <script type="text/javascript" src="/admin/Content/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="/admin/Content/js/report/report-loader.js"></script>
</asp:Content>