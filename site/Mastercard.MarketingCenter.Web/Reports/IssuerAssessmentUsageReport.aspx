﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="IssuerAssessmentUsageReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.IssuerAssessmentUsageReport" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register tagPrefix="mc" tagname="ContentActivityDetailFilterDisplay" src="~/Reports/UserControls/ContentActivityDetailFilterDisplay.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel  ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
        <div style="background-color: #ffffff; width: 100%; height: 100%; display: flex; justify-content: center; flex-direction: column; align-items: center;">
            <img src="<%= Mastercard.MarketingCenter.Common.Infrastructure.Constants.ImageUrls.ReportsLoader %>" alt="Loading..." />
        </div>
    </telerik:RadAjaxLoadingPanel >
    <div class="clr" style="margin-bottom:10px;">
        <a id="A1" href="~/report/issuerassessmentusagelanding" runat="server">< Search again</a>
    </div>
    <h2 style="color:#f58025;">Issuer Assessment Usage Report</h2>
    <p>
        <mc:ReportDateRangeDisplay id="ucDateRangeDisplay" runat="server"></mc:ReportDateRangeDisplay>
    </p>
    <p><strong>All Issuer Assessment Usage Report</strong></p>
	<mc:ContentActivityDetailFilterDisplay ID="ucFilterDisplay" runat="server"></mc:ContentActivityDetailFilterDisplay>
    <telerik:RadGrid ID="trgReport" Width="800" AllowSorting="True" PageSize="15" AllowPaging="True" runat="server" Gridlines="None" AutoGenerateColumns="False" OnNeedDataSource="trgReport_NeedDataSource" AllowFilteringByColumn="false" ShowGroupPanel="true" OnItemCommand="trgReport_ItemCommand">
        <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="false">
            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
            <CommandItemSettings ShowRefreshButton="" ShowExportToWordButton="true" ShowExportToExcelButton="true"
                ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false"/>
            <Columns>
				<telerik:GridBoundColumn HeaderText="Issuers" DataField="Issuers" Visible="false"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Issuers" HeaderStyle-Width="50" SortExpression="Issuers" UniqueName="IssuersLink" DataField="Issuers">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkIssuers" CommandArgument='issuers' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"Issuers") %>' runat="server"/>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Users" DataField="Users" Visible="false"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Users" HeaderStyle-Width="50" SortExpression="Users" UniqueName="UsersLink" DataField="Users">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkUsers" CommandArgument='users' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"Users") %>' runat="server"/>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
				<telerik:GridBoundColumn HeaderText="Issuer Reports" DataField="IssuerReports" Visible="false"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Issuer Reports" HeaderStyle-Width="50" SortExpression="IssuerReports" UniqueName="IssuerReportsLink" DataField="IssuerReports">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkIssuerReports" CommandArgument='reports' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"IssuerReports") %>' runat="server"/>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
				<telerik:GridBoundColumn HeaderText="Visits" DataField="Visits" Visible="false"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Visits" HeaderStyle-Width="50" SortExpression="Visits" UniqueName="VisitsLink" DataField="Visits">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkVisits" CommandArgument='visits' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"Visits") %>' runat="server"/>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Downloads" DataField="Downloads" Visible="false"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Downloads" HeaderStyle-Width="60" SortExpression="Downloads" UniqueName="DownloadsLink" DataField="Downloads">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkDownloads" CommandArgument='downloads' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"Downloads") %>' runat="server"/>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>				
                <telerik:GridBoundColumn HeaderText="Lead gen opt-in" DataField="LeadGens" Visible="false"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Lead gen opt-in" HeaderStyle-Width="100" SortExpression="LeadGens" UniqueName="LeadGensLink" DataField="LeadGens">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkLeadGens" CommandArgument='leadgens' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"LeadGens") %>' runat="server"/>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
				<telerik:GridBoundColumn HeaderText="Lead gen opt-in unique users" DataField="LeadGenUniqueUsers" Visible="false"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Lead gen opt-in unique users" HeaderStyle-Width="100" SortExpression="LeadGenUniqueUsers" UniqueName="LeadGenUniqueUsersLink" DataField="LeadGenUniqueUsers">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkLeadGenUniqueUsers" CommandArgument='leadgens' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"LeadGenUniqueUsers") %>' runat="server"/>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
				<telerik:GridBoundColumn HeaderText="Matching errors" DataField="MatchingErrors" Visible="false"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Matching errors" HeaderStyle-Width="100" SortExpression="MatchingErrors" UniqueName="MatchingErrorsLink" DataField="MatchingErrors">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkMatchingErrors" CommandArgument='matchingerrors' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"MatchingErrors") %>' runat="server"/>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
				<telerik:GridBoundColumn HeaderText="Matching errors unique users" DataField="MatchingErrorsUniqueUsers" Visible="false"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Matching errors unique users" HeaderStyle-Width="100" SortExpression="MatchingErrorsUniqueUsers" UniqueName="MatchingErrorsUniqueUsersLink" DataField="MatchingErrorsUniqueUsers">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkMatchingErrorsUniqueUsers" CommandArgument='matchingerrors' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"MatchingErrorsUniqueUsers") %>' runat="server"/>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
				<telerik:GridBoundColumn HeaderText="Matching errors unique issuers" DataField="MatchingErrorsUniqueIssuers" Visible="false"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Matching errors unique issuers" HeaderStyle-Width="100" SortExpression="MatchingErrorsUniqueIssuers" UniqueName="MatchingErrorsUniqueIssuersLink" DataField="MatchingErrorsUniqueIssuers">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkMatchingErrorsUniqueIssuers" CommandArgument='matchingerrors' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"MatchingErrorsUniqueIssuers") %>' runat="server"/>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>				
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
            <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
        </ClientSettings>
        <PagerStyle Mode="NextPrevAndNumeric" />
        <HeaderStyle Width="100px" />
        <ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="False" />        
    </telerik:RadGrid>

    <asp:PlaceHolder ID="phNoResults" runat="server" Visible="false">
        <p style="color:#F58025;"><em>No results found in this date range.  Please try your search again, taking note of the effective date for the data available.</em></p>
    </asp:PlaceHolder>

</asp:Content>