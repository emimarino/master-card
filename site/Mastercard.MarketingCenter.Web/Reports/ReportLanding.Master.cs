﻿using Mastercard.MarketingCenter.Services;
using System;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class ReportLanding : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegionalizeService.SetCurrentCulture(RegionalizeService.DefaultRegion, RegionalizeService.DefaultLanguage); //always default to English-US
        }

        public string GetLoader()
        {
            return string.Format("displayLoader(\".reports\", \"{0}\")", Common.Infrastructure.Constants.ImageUrls.ReportsLoader);
        }
    }
}