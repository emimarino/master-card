﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities.Reporting;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core;
using Mastercard.MarketingCenter.Web.Core.Services;
using Slam.Cms.Configuration;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class MyFavoritesImagesReport : BaseReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void trgReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var _userSubscriptionService = DependencyResolver.Current.GetService<UserSubscriptionService>();
            var contentItemUserSubscriptions = _userSubscriptionService.GetSubscribedContentItemUserSubscriptions(userContext.SelectedRegion);

            var _slamContext = DependencyResolver.Current.GetService<SlamContext>();
            var results = _slamContext.CreateQuery().FilterStatus(FilterStatus.Live)
                                                    .FilterContentTypes(typeof(AssetDTO),
                                                                        typeof(AssetFullWidthDTO),
                                                                        typeof(DownloadableAssetDTO),
                                                                        typeof(OrderableAssetDTO),
                                                                        typeof(PageDTO),
                                                                        typeof(ProgramDTO),
                                                                        typeof(WebinarDTO))
                                                    .Cache(SlamQueryCacheBehavior.NoCache)
                                                    .Get()
                                                    .Select(ci => ci.AsContentItemListItem())
                                                    .GroupJoin(contentItemUserSubscriptions,
                                                            ci => ci.ContentItem.ContentItemId,
                                                            us => us.ContentItemId,
                                                            (ci, us) => new
                                                            {
                                                                ci.ContentItem.ContentItemId,
                                                                ci.ContentType,
                                                                ci.Title,
                                                                ci.Summary,
                                                                ci.MainImageUrl,
                                                                HasUserSubscriptions = us.DefaultIfEmpty().Count(s => s != null) > 0
                                                            });

            var myFavoritesImages = new List<MyFavoritesImagesResultItem>();
            if (results.Any())
            {
                var urlService = DependencyResolver.Current.GetService<IUrlService>();
                foreach (var result in results)
                {
                    var filename = result.MainImageUrl;
                    var imageType = string.Empty;
                    var imageUrl = string.Empty;
                    int imageWidth = 0;
                    int imageHeight = 0;
                    string hasRightResolution = "No";

                    if (string.IsNullOrWhiteSpace(filename))
                    {
                        imageType = "Default Image";
                        imageUrl = "/portal/content/images/component-favorites/favorite-empty.png";
                        hasRightResolution = "Yes";
                    }
                    else
                    {
                        imageType = result.ContentType == "Page" ? "Header Background Image" : "Main Image";
                        var filePath = Path.Combine(Server.MapPath(ConfigurationManager.Solution.Settings["ImagesFolder"]), filename.TrimStart('/')); ;
                        if (File.Exists(filePath))
                        {
                            imageUrl = urlService.GetStaticImageURL(filename, 120, 120, true, 95, false);
                            using (var fileStream = File.Open(filePath, FileMode.Open))
                            {
                                using (Image image = Image.FromStream(fileStream))
                                {
                                    imageWidth = image.Width;
                                    imageHeight = image.Height;
                                    hasRightResolution = imageWidth >= 120 && imageHeight >= 120 ? "Yes" : "No";
                                }
                            }
                        }
                    }

                    myFavoritesImages.Add(new MyFavoritesImagesResultItem
                    {
                        ContentItemId = result.ContentItemId,
                        ContentType = result.ContentType,
                        Title = result.Title,
                        Description = result.Summary.StripHtml().TrimEllipsisWord(100),
                        ImageType = imageType,
                        ImageUrl = imageUrl,
                        ImageWidth = imageWidth,
                        ImageHeight = imageHeight,
                        HasRightResolution = hasRightResolution,
                        HasUserSubscriptions = result.HasUserSubscriptions ? "Yes" : "No"
                    });
                }
            }
            else
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }

            trgReport.DataSource = myFavoritesImages;
        }

        protected void trgReport_ItemCommand(object source, GridCommandEventArgs e)
        {
        }

        protected void lnkClearFilters_Click(object source, EventArgs e)
        {
            Response.Redirect("~/report/myfavoritesimages");
        }
    }
}