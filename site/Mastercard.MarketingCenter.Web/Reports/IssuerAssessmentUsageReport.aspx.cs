﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class IssuerAssessmentUsageReport : BaseReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ucFilterDisplay.Setup(Enumerable.Empty<Segmentation>(), Enumerable.Empty<string>(), FilterMasterCardUsers, FilterVendorProcessor);
            }
        }

        protected void trgReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();
            var results = reportRepository.GetIssuerAssessmentUsageReport(StartDate, EndDate, FilterMasterCardUsers.Value, FilterVendorProcessor.Value).ToList();
            trgReport.DataSource = results;
            if (results.Count == 0)
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }
        }

        protected void trgReport_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "redirect")
            {
                string url = string.Empty;
                switch (e.CommandArgument.ToString())
                {
                    case "leadgens":
                        url = "~/report/issuerassessmentusageleadgens?{0}".F(Request.QueryString);
                        break;
                    case "matchingerrors":
                        url = "~/report/issuerassessmentusagematchingerrors?{0}".F(Request.QueryString);
                        break;
                    default:
                        url = "~/report/issuerassessmentusagedetail/{0}?{1}".F(e.CommandArgument, Request.QueryString);
                        break;
                }
                Response.Redirect(url);
            }

            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName || e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToWordCommandName)
            {
                trgReport.MasterTableView.GetColumn("Issuers").Visible = true;
                trgReport.MasterTableView.GetColumn("IssuersLink").Visible = false;
                trgReport.MasterTableView.GetColumn("Users").Visible = true;
                trgReport.MasterTableView.GetColumn("UsersLink").Visible = false;
                trgReport.MasterTableView.GetColumn("Visits").Visible = true;
                trgReport.MasterTableView.GetColumn("VisitsLink").Visible = false;
                trgReport.MasterTableView.GetColumn("Downloads").Visible = true;
                trgReport.MasterTableView.GetColumn("DownloadsLink").Visible = false;
                trgReport.MasterTableView.GetColumn("LeadGens").Visible = true;
                trgReport.MasterTableView.GetColumn("LeadGensLink").Visible = false;
                trgReport.MasterTableView.GetColumn("LeadGenUniqueUsers").Visible = true;
                trgReport.MasterTableView.GetColumn("LeadGenUniqueUsersLink").Visible = false;
                trgReport.MasterTableView.GetColumn("MatchingErrors").Visible = true;
                trgReport.MasterTableView.GetColumn("MatchingErrorsLink").Visible = false;
                trgReport.MasterTableView.GetColumn("MatchingErrorsUniqueUsers").Visible = true;
                trgReport.MasterTableView.GetColumn("MatchingErrorsUniqueUsersLink").Visible = false;
                trgReport.MasterTableView.GetColumn("MatchingErrorsUniqueIssuers").Visible = true;
                trgReport.MasterTableView.GetColumn("MatchingErrorsUniqueIssuersLink").Visible = false;
                trgReport.MasterTableView.GetColumn("IssuerReports").Visible = true;
                trgReport.MasterTableView.GetColumn("IssuerReportsLink").Visible = false;
            }
        }
    }
}