﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="ContentDetailReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.ContentDetailReport" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="refreshContent">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
        <div style="background-color: #ffffff; width: 100%; height: 100%; display: flex; justify-content: center; flex-direction: column; align-items: center;">
            <img src="<%= Mastercard.MarketingCenter.Common.Infrastructure.Constants.ImageUrls.ReportsLoader %>" alt="Loading..." />
        </div>
    </telerik:RadAjaxLoadingPanel>
    <div class="clr" style="margin-bottom: 10px;">
        <h2 style="color: #f58025;">Content Report</h2>
        <p>
            <a id="aBack" runat="server">< Back to Content report</a>
        </p>
    </div>
    <telerik:RadGrid ID="trgReport" Width="800" AllowSorting="True" PageSize="15" AllowPaging="True" ClientSettings-Resizing-AllowColumnResize="true" runat="server" GridLines="None" AutoGenerateColumns="False" OnNeedDataSource="trgReport_NeedDataSource" AllowFilteringByColumn="true" ShowGroupPanel="true">
        <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
            <CommandItemSettings ShowRefreshButton="true" ShowExportToWordButton="true" ShowExportToExcelButton="true"
                ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false" />
            <Columns>
                <telerik:GridBoundColumn HeaderText="Content Type" DataField="ContentType" HeaderStyle-Width="100" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Title" DataField="Title" HeaderStyle-Width="100" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Region" DataField="RegionName" HeaderStyle-Width="100" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Creation Date" DataField="CreationDate" HeaderStyle-Width="100" FilterControlWidth="50" DataFormatString="{0:MM/dd/yyyy}"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Created By" DataField="CreatedBy" HeaderStyle-Width="100" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Last Updated Date" DataField="LastUpdatedDate" HeaderStyle-Width="100" FilterControlWidth="50" DataFormatString="{0:MM/dd/yyyy}"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Status Name" DataField="StatusName" HeaderStyle-Width="100" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Business Owner" DataField="BusinessOwner" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Width="100" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Live Content Url" HeaderStyle-Width="200">
                    <ItemTemplate>
                        <a href='<%# string.Concat(baseFrontendUrl.TrimEnd('/'), DataBinder.Eval(Container.DataItem, "LiveContentUrl")) %>' target="_parent"><%# string.Concat(baseFrontendUrl.TrimEnd('/'), DataBinder.Eval(Container.DataItem, "LiveContentUrl")) %></a>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
            <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
        </ClientSettings>
        <PagerStyle Mode="NextPrevAndNumeric" />
        <HeaderStyle Width="100px" />
        <ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="false" />
    </telerik:RadGrid>
    <asp:PlaceHolder ID="phNoResults" runat="server" Visible="false">
        <p><em>There are no content matching data.</em></p>
    </asp:PlaceHolder>
</asp:Content>
