﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class SearchTermReport : BaseReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();
            var uc = DependencyResolver.Current.GetService<UserContext>();
            var results = reportRepository.GetSearchTermsReport(StartDate, EndDate,uc.SelectedRegion?.Trim()??"").ToList();
            trgReport.DataSource = results;
            if (results.Count == 0)
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }
        }
    }
}