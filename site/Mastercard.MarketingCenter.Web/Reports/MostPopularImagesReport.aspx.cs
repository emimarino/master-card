﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities.Reporting;
using Mastercard.MarketingCenter.Domo.Repository;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core;
using Slam.Cms.Configuration;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class MostPopularImagesReport : BaseReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void trgReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var _domoActivityRepository = DependencyResolver.Current.GetService<IDomoActivityRepository>();
            var domoActivity = _domoActivityRepository.GetAllActivity().GroupBy(da => new
            {
                ContentItemId = da.ContentId
            })
                                                                       .Select(da => new
                                                                       {
                                                                           da.Key.ContentItemId,
                                                                           Visits = da.Count()
                                                                       });

            var _slamContext = DependencyResolver.Current.GetService<SlamContext>();
            var results = _slamContext.CreateQuery().FilterContentItemId(domoActivity.Select(da => da.ContentItemId).ToArray())
                                                    .FilterStatus(FilterStatus.Live)
                                                    .FilterContentTypes(typeof(PageDTO), typeof(ProgramDTO))
                                                    .Cache(SlamQueryCacheBehavior.NoCache)
                                                    .Get()
                                                    .Select(ci => ci.AsContentItemListItem())
                                                    .Join(domoActivity,
                                                            ci => ci.ContentItem.ContentItemId,
                                                            da => da.ContentItemId,
                                                            (ci, da) => new
                                                            {
                                                                ci.ContentItem.ContentItemId,
                                                                ci.ContentType,
                                                                ci.Title,
                                                                ci.Summary,
                                                                ci.MainImageUrl,
                                                                da.Visits
                                                            });

            var mostPopularImages = new List<MostPopularImagesResultItem>();
            if (results.Any())
            {
                var urlService = DependencyResolver.Current.GetService<IUrlService>();
                foreach (var result in results)
                {
                    var filename = result.MainImageUrl;
                    var imageType = string.Empty;
                    var imageUrl = string.Empty;
                    int imageWidth = 0;
                    int imageHeight = 0;
                    string hasRightResolution = "No";

                    if (string.IsNullOrWhiteSpace(filename))
                    {
                        imageType = "Default Image";
                        imageUrl = "/portal/content/images/component-favorites/popular-empty.jpg";
                        hasRightResolution = "Yes";
                    }
                    else
                    {
                        imageType = result.ContentType == "Page" ? "Header Background Image" : "Main Image";
                        var filePath = Path.Combine(Server.MapPath(ConfigurationManager.Solution.Settings["ImagesFolder"]), filename.TrimStart('/')); ;
                        if (File.Exists(filePath))
                        {
                            imageUrl = urlService.GetStaticImageURL(filename, 250, 100, true, 95, false);
                            using (var fileStream = File.Open(filePath, FileMode.Open))
                            {
                                using (Image image = Image.FromStream(fileStream))
                                {
                                    imageWidth = image.Width;
                                    imageHeight = image.Height;
                                    hasRightResolution = imageWidth >= 500 ? "Yes" : "No";
                                }
                            }
                        }
                    }

                    mostPopularImages.Add(new MostPopularImagesResultItem
                    {
                        ContentItemId = result.ContentItemId,
                        ContentType = result.ContentType,
                        Title = result.Title,
                        Description = result.Summary.StripHtml().TrimEllipsisWord(100),
                        Visits = result.Visits,
                        ImageType = imageType,
                        ImageUrl = imageUrl,
                        ImageWidth = imageWidth,
                        ImageHeight = imageHeight,
                        HasRightResolution = hasRightResolution
                    });
                }
            }
            else
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }

            trgReport.DataSource = mostPopularImages;
        }

        protected void trgReport_ItemCommand(object source, GridCommandEventArgs e)
        {
        }

        protected void lnkClearFilters_Click(object source, EventArgs e)
        {
            Response.Redirect("~/report/mostpopularimages");
        }
    }
}