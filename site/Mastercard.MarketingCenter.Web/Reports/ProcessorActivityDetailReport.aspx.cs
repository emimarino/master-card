﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class ProcessorActivityDetailReport : BaseReportPage
    {
        public new UserContext userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            targetLabel.Text = user.IsInPermission(Constants.Permissions.CanManageProcessors) ? "Processor:" : "Issuer:";
            if (!IsPostBack)
            {
                var selectedRegion = userContext.SelectedRegion;
                var processorRepository = DependencyResolver.Current.GetService<ProcessorRepository>();
                var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();
                var processorId = Page.RouteData.Values["id"].ToString();
                litProcessor.Text = (user.IsInPermission(Constants.Permissions.CanManageProcessors) ? ("under \"" + processorRepository.GetProcessors().FirstOrDefault(p => p.ProcessorId == processorId).Title + "\"") : "");
                aBack.HRef = (user.IsInPermission(Constants.Permissions.CanManageProcessors) ? "~/report/processoractivity?{0}".F(Request.QueryString) : "");
                litExcludedIssuersCount.Text = reportRepository.GetProcessorActivityDetailExcludedIssuersReport((user.IsInPermission(Constants.Permissions.CanManageProcessors) ? Page.RouteData.Values["id"].ToString() : ""), StartDate, EndDate, selectedRegion).ToString();
            }

            if (user.IsInPermission(Constants.Permissions.CanManageProcessors))
            {
                aBack.Visible = true;
            }
            else
            {
                aBack.Visible = false;
            }
        }

        protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var selectedRegion = userContext.SelectedRegion;
            var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();

            trgReport.DataSource = reportRepository.GetProcessorActivityDetailReport((user.IsInPermission(Constants.Permissions.CanManageProcessors) ? Page.RouteData.Values["id"].ToString() : ""), StartDate, EndDate, selectedRegion).ToList();
        }
    }
}