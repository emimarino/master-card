﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Slam.Cms.Data;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class EstimatedDistributionProgramReport : BaseReportPage
    {
        private bool exporting = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ucFilterDisplay.Setup(segmentationService.GetSegmentationsByRegion(userContext.SelectedRegion), AudienceSegmentationIds, FilterMasterCardUsers.Value, null);
            }
        }

        protected void trgReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var slamContext = DependencyResolver.Current.GetService<SlamContext>();
            var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();
            var results = reportRepository.GetEstimatedDistributionProgramReport(StartDate, EndDate, GetAudienceSegmentationIdsForReport(), userContext.SelectedRegion, FilterMasterCardUsers.Value).ToList();

            var programs = slamContext
                .CreateQuery()
                .FilterContentTypes<ProgramDTO>()
                .FilterStatus(FilterStatus.None)
                .Get<ProgramDTO>();
            results = results.Where(r => programs.Any(d => d.ContentItemId == r.ContentItemId && d.StatusID >= 3)).ToList();
            foreach (var result in results)
            {
                result.MostRecentDownload = result.MostRecentDownload?.Date;
                var program = programs.FirstOrDefault(d => d.ContentItemId == result.ContentItemId);
                if (program != null)
                {
                    result.Title = program.Title;
                }
            }

            trgReport.DataSource = results;
            if (results.Count == 0)
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }
        }

        protected void trgReport_PreRender(object sender, EventArgs e)
        {
            if (exporting)
            {
                foreach (var header in trgReport.MasterTableView.GetItems(GridItemType.Header))
                {
                    foreach (TableCell cell in header.Cells)
                    {
                        cell.Style["text-align"] = "left";
                    }
                }
            }
        }

        protected void trgReport_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "redirect")
            {
                Response.Redirect("~/report/estimateddistributionprogramdetail/{0}?{1}".F(e.CommandArgument, Request.QueryString));
            }

            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName || e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToWordCommandName)
            {
                if (e.CommandName == RadGrid.ExportToPdfCommandName)
                {
                    exporting = true;
                    trgReport.ExportSettings.Pdf.PageHeight = Unit.Parse("162mm");
                    trgReport.ExportSettings.Pdf.PageWidth = Unit.Parse("400mm");
                }

                trgReport.MasterTableView.GetColumn("Title").Visible = true;
                trgReport.MasterTableView.GetColumn("TitleLink").Visible = false;
            }
        }
    }
}