﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportLanding.Master" AutoEventWireup="true" CodeBehind="ContentReportLanding.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.ContentReportLanding" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="color: #f58025;">Content Report</h2>
    <div class="clr" style="margin-top: 20px;">
        <p><em>Step <%: StepNumber %>: </em>Please specify a date range for the report:</p>
        <mc:ReportDateRangeSelector ID="trdpCreationRangeDate" runat="server"></mc:ReportDateRangeSelector>
    </div>
    <div style="margin-top: 40px; clear:both;">
        <em>Content data effective as of 10/2011.</em>
    </div>
    <% if (IsInPermission)
        { %>
    <div class="clr" style="margin-top: 25px;">
        <p><em>Step <%: StepNumber %>: OPTIONAL.</em> Please specify the report region:</p>
        <asp:CheckBoxList ID="cblReportRegion" runat="server">
        </asp:CheckBoxList>
    </div>
    <% } %>    
    <div class="clr" style="margin-top: 30px;">
        <p><em>Step <%: StepNumber %>: OPTIONAL.</em> Please indicate if you would like to search by keyword in this report:</p>
        <asp:TextBox ID="tbKeyword" runat="server">
        </asp:TextBox>
    </div>

    <div style="padding-top: 20px;">
        <div class="btn_left" style="float: left; margin-left: 20px; margin-top: 2px;">
            <asp:LinkButton ID="btnValidateCode" runat="server" Text="Go" CssClass="btn_right" OnClick="btnGo_Click"></asp:LinkButton>
        </div>
    </div>
    <script type="text/javascript" src="/admin/Content/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="/admin/Content/js/report/report-loader.js"></script>
    <script type="text/javascript" src="/admin/Content/js/report/content-report.js"></script>
</asp:Content>
