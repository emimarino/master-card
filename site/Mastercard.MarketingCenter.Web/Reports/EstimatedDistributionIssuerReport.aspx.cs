﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Slam.Cms.Data;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class EstimatedDistributionIssuerReport : BaseReportPage
    {
        private bool _exporting;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ucFilterDisplay.Setup(segmentationService.GetSegmentationsByRegion(userContext.SelectedRegion), AudienceSegmentationIds, FilterMasterCardUsers != null && FilterMasterCardUsers.Value, null);
            }
        }

        protected void trgReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            DependencyResolver.Current.GetService<SlamContext>();
            var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();
            var results = reportRepository.GetEstimatedDistributionIssuerReport(StartDate, EndDate, GetAudienceSegmentationIdsForReport(), userContext.SelectedRegion, FilterMasterCardUsers != null && FilterMasterCardUsers.Value).ToList();
            results.ForEach(r =>
            {
                r.MostRecentDownload = r.MostRecentDownload?.Date;
            });
            trgReport.DataSource = results;
            if (results.Count == 0)
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }
        }

        protected void trgReport_PreRender(object sender, EventArgs e)
        {
            if (_exporting)
            {
                foreach (var header in trgReport.MasterTableView.GetItems(GridItemType.Header))
                {
                    foreach (TableCell cell in header.Cells)
                    {
                        cell.Style["text-align"] = "left";
                    }
                }
            }
        }

        protected void trgReport_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "redirect")
            {
                Response.Redirect("~/report/estimateddistributionissuerdetail/{0}?{1}".F(e.CommandArgument, Request.QueryString));
            }

            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName || e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToWordCommandName)
            {
                if (e.CommandName == RadGrid.ExportToPdfCommandName)
                {
                    _exporting = true;
                    trgReport.ExportSettings.Pdf.PageHeight = Unit.Parse("162mm");
                    trgReport.ExportSettings.Pdf.PageWidth = Unit.Parse("400mm");
                }
                trgReport.MasterTableView.GetColumn("Title").Visible = true;
                trgReport.MasterTableView.GetColumn("TitleLink").Visible = false;
            }
        }
    }
}