﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="PendingRegistrationsReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.PendingRegistrationsReport" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
        <div style="background-color: #ffffff; width: 100%; height: 100%"></div>
    </telerik:RadAjaxLoadingPanel>
    <asp:PlaceHolder ID="phParentRedirect" runat="server" Visible="false">
        <script type="text/javascript">
            window.parent.location = '<%= PendingRegistrationUrl %>';
        </script>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phMain" runat="server">
        <div class="clr" style="margin-bottom: 10px;">
            <h2 style="color: #f58025;">Pending Registrations Report</h2>
            <asp:LinkButton ID="lnkClearFilters" runat="server" Text="Back to start" OnClick="lnkClearFilters_Click"></asp:LinkButton>
        </div>
        <telerik:RadGrid ID="trgReport" Width="800" AllowSorting="True" PageSize="15" AllowPaging="True" runat="server" GridLines="None" AutoGenerateColumns="False" OnNeedDataSource="trgReport_NeedDataSource" OnItemDataBound="trgReport_ItemDataBound" AllowFilteringByColumn="true" ShowGroupPanel="true" OnItemCommand="trgReport_ItemCommand">
            <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
                <PagerStyle Mode="NextPrevNumericAndAdvanced" />
                <CommandItemSettings ShowRefreshButton="" ShowExportToWordButton="true" ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false" />
                <Columns>
                    <telerik:GridBoundColumn HtmlEncode="true" HeaderText="Date Registered" DataField="DateAdded" DataFormatString="{0:MM/dd/yyyy}" FilterControlWidth="50"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HtmlEncode="true" HeaderText="First Name" DataField="FirstName" HtmlEncode="true"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HtmlEncode="true" HeaderText="Last Name" DataField="LastName"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HtmlEncode="true" HeaderText="Email" DataField="Email" Visible="false"></telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Email" HeaderStyle-Width="200" SortExpression="Email" UniqueName="EmailLink" DataField="Email">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"GUID") %>' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"Email") %>' runat="server" />
                        </ItemTemplate>
                        <ItemStyle CssClass="rad_grid_hyperlink" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn HtmlEncode="true" HeaderText="FI Name" DataField="FinancialInstitution" FilterControlWidth="50"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HtmlEncode="true" HeaderText="Processor" DataField="Processor" FilterControlWidth="50"></telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
                <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
            </ClientSettings>
            <PagerStyle Mode="NextPrevAndNumeric" />
            <HeaderStyle Width="100px" />
            <ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
            <GroupingSettings ShowUnGroupButton="true" CaseSensitive="false" />
        </telerik:RadGrid>
        <asp:PlaceHolder ID="phNoResults" runat="server" Visible="false">
            <p><em>There are no pending registrations.</em></p>
        </asp:PlaceHolder>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phLoading" runat="server" Visible="false">
        <p>Loading...</p>
    </asp:PlaceHolder>
</asp:Content>
