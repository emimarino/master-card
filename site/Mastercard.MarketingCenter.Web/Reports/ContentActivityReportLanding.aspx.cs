﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Configuration;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class ContentActivityReportLanding : System.Web.UI.Page
    {
        public ProcessorService _processorService { get { return DependencyResolver.Current.GetService<ProcessorService>(); } }
        public IIssuerService _issuerService { get { return DependencyResolver.Current.GetService<IIssuerService>(); } }
        public bool CanChangeSegments { get { return User.IsInPermission(Constants.Permissions.CanChangeSegments); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var userContext = DependencyResolver.Current.GetService<UserContext>();
                chkMasterCard.Visible = userContext.SelectedRegion.Equals(RegionalizeService.GlobalRegion, StringComparison.InvariantCultureIgnoreCase) || _issuerService.GetMastercardIssuersByRegion(userContext.SelectedRegion).Any();
                chkVendor.Visible = _processorService.GetProcessors().Any(p =>
                    p.ProcessorId == MarketingCenterDbConstants.Processors.APlus
                    && p.Issuers.Any(ip => ip.RegionId == userContext.SelectedRegion));
                step4Section.Visible = chkMasterCard.Visible || chkVendor.Visible;

                var segmentationServices = DependencyResolver.Current.GetService<ISegmentationService>();
                var segmentations = segmentationServices.GetSegmentationsByRegion(userContext.SelectedRegion).ToList();
                segmentations.Insert(0, new Segmentation { SegmentationName = Resources.Shared.All, SegmentationId = RegionConfigManager.DefaultSegmentation });
                ddlSegmentations.DataSource = segmentations;
                ddlSegmentations.DataTextField = "SegmentationName";
                ddlSegmentations.DataValueField = "SegmentationId";
                ddlSegmentations.AppendDataBoundItems = true;
                ddlSegmentations.DataBind();
                ddlSegmentations.SelectedIndex = 0;
            }

            btnValidateCode.OnClientClick = (Master as ReportLanding)?.GetLoader();
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            if (ucDateRangeSelector.Validate())
            {
                string reportTypeUrl;
                switch (int.Parse(rblReportType.SelectedValue))
                {
                    case 0:
                        reportTypeUrl = "contentactivitypage";
                        break;
                    case 1:
                        reportTypeUrl = "contentactivityprogram";
                        break;
                    case 2:
                        reportTypeUrl = "contentactivityasset";
                        break;
                    case 3:
                        reportTypeUrl = "contentactivitytag";
                        break;
                    case 4:
                        reportTypeUrl = "contentactivitywebinar";
                        break;
                    default:
                        reportTypeUrl = "contentactivityoffer";
                        break;
                }

                var segmentations = (from ListItem item in ddlSegmentations.Items where item.Selected && !string.IsNullOrWhiteSpace(item.Value) select item.Value);

                Response.Redirect($"~/report/{reportTypeUrl}?sd={ucDateRangeSelector.StartDate.Value.ToString("MM/dd/yyyy")}&ed={ucDateRangeSelector.EndDate.Value.ToString("MM/dd/yyyy")}&fmp={!chkMasterCard.Checked}&fvp={!chkVendor.Checked}&audience={string.Join(";", (from ListItem item in ddlSegmentations.Items where item.Selected && !string.IsNullOrWhiteSpace(item.Value) select item.Value))}&type={rblReportType.SelectedValue}");
            }
        }
    }
}