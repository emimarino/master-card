﻿using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Modules;
using System;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class GdprFollowUpReport : BaseReportPage
    {
        protected IReportService _reportService { get { return WebCoreModule.GetService<IReportService>(); } }

        protected void trgReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var results = _reportService.GetGdprFollowUpReport(userContext.SelectedRegion).ToList();
            trgReport.DataSource = results;
            if (!results.Any())
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }
        }

        public string GetInactivityDate(DateTime lastLoginDate)
        {
            DateTime inactivityDate = DateTime.MinValue + (DateTime.Now - lastLoginDate);
            int years = inactivityDate.Year - 1;
            int months = inactivityDate.Month - 1;
            int days = inactivityDate.Day - 1;
            return $"{(years * 12) + months}m and {days}d";
        }
    }
}