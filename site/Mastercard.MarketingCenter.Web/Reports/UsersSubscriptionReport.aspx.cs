﻿using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class UsersSubscriptionReport : BaseReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ucFilterDisplay.Setup(Enumerable.Empty<Data.Entities.Segmentation>(), Enumerable.Empty<string>(), FilterMasterCardUsers, FilterVendorProcessor);
            }
        }

        protected void trgReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var reportService = DependencyResolver.Current.GetService<IReportService>();
            var results = reportService.GetUsersSubscriptionReport(StartDate, EndDate, FilterMasterCardUsers.Value, FilterVendorProcessor.Value, userContext.SelectedRegion).ToList();
            if (results.Any())
            {
                results.Insert(0, new Data.Entities.Reporting.UsersSubscriptionReportResultItem
                {
                    UserName = "[All]",
                    UserId = -1,
                    IssuerName = "-",
                    Favorites = results.Sum(x => x.Favorites),
                    Frequency = string.Join(", ", results.Where(y => y.Favorites > 0).Select(x => x.Frequency).ToList().Distinct()),
                    LastSavedDate = results.Max(x => x.LastSavedDate)
                });
                trgReport.DataSource = results;
            }
            else
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }
        }

        protected void trgReport_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "redirect")
            {
                Response.Redirect($"~/report/userssubscriptiondetail/{e.CommandArgument}?{Request.QueryString}");
            }

            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName || e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToWordCommandName)
            {
                trgReport.MasterTableView.GetColumn("UserName").Visible = true;
                trgReport.MasterTableView.GetColumn("UserLink").Visible = false;
            }
        }
    }
}