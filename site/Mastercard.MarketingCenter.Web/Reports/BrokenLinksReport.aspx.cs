﻿using Mastercard.MarketingCenter.Data;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class BrokenLinksReport : BaseReportPage
    {
        private BrokenContentLinkRepository _brokenContentLinkRepository { get { return DependencyResolver.Current.GetService<BrokenContentLinkRepository>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void trgReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var results = _brokenContentLinkRepository.GetByRegion(userContext.SelectedRegion).ToList();
            trgReport.DataSource = results;
            if (!results.Any())
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }
        }

        protected void trgReport_ItemCommand(object source, GridCommandEventArgs e)
        {
            var results = _brokenContentLinkRepository.GetByRegion(userContext.SelectedRegion).ToList();
            trgReport.DataSource = results;
            if (!results.Any())
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }
        }
    }
}