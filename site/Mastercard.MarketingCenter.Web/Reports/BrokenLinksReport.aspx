﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="BrokenLinksReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.BrokenLinksReport" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:radajaxmanager id="RadAjaxManager1" runat="server">
		<ClientEvents OnRequestStart="onRequestStart" />
		<AjaxSettings>
			<telerik:AjaxSetting AjaxControlID="trgReport">
				<UpdatedControls>
					<telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
				</UpdatedControls>
			</telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="refreshContent">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
            </UpdatedControls>
            </telerik:AjaxSetting>
		</AjaxSettings>
	</telerik:radajaxmanager>
    <telerik:radajaxloadingpanel id="ralp" runat="server" initialdelaytime="0" mindisplaytime="1000" transparency="25">
		<div style="background-color: #ffffff; width: 100%; height: 100%; display: flex; justify-content: center; flex-direction: column; align-items: center;">
            <img src="<%= Mastercard.MarketingCenter.Common.Infrastructure.Constants.ImageUrls.ReportsLoader %>" alt="Loading..." />
		</div>
	</telerik:radajaxloadingpanel>
    <div class="clr" style="margin-bottom: 10px;">
        <h2 style="color: #f58025;">Broken Links Report</h2>
    </div>
    <telerik:radgrid id="trgReport" width="800" allowsorting="True" pagesize="15" allowpaging="True" clientsettings-resizing-allowcolumnresize="true" runat="server" gridlines="None" autogeneratecolumns="False" onneeddatasource="trgReport_NeedDataSource" allowfilteringbycolumn="true" showgrouppanel="true" onitemcommand="trgReport_ItemCommand">
		<MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
			<PagerStyle Mode="NextPrevNumericAndAdvanced" />
			<CommandItemSettings ShowRefreshButton="true" ShowExportToWordButton="true" ShowExportToExcelButton="true"
				ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false" />
			<Columns>
				<telerik:GridBoundColumn HeaderText="Item Id" DataField="SourceItemId" HeaderStyle-Width="100" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Title" HeaderStyle-Width="100" FilterControlWidth="50">
                    <ItemTemplate>
                        <a href='<%# DataBinder.Eval(Container.DataItem,"BackEndUrl") %>' target="_parent"><%# DataBinder.Eval(Container.DataItem,"Title") %></a>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
				<telerik:GridBoundColumn HeaderText="Region" DataField="RegionId" HeaderStyle-Width="100" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Content Type" DataField="ContentType" HeaderStyle-Width="100" FilterControlWidth="50"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn HeaderText="Backend Url" DataField="BackEndUrl" HeaderStyle-Width="100" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Frontend Url" HeaderStyle-Width="200">
                    <ItemTemplate>
                        <a href='<%# DataBinder.Eval(Container.DataItem,"FrontEndUrl") %>' target="_parent"><%# DataBinder.Eval(Container.DataItem,"FrontEndUrl") %></a>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Broken Link Text" DataField="BrokenLinkText" HeaderStyle-Width="100" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Broken Link Reference" HeaderStyle-Width="200">
                    <ItemTemplate>
                        <a href='<%# DataBinder.Eval(Container.DataItem,"BrokenLinkReference") %>' target="_parent"><%# DataBinder.Eval(Container.DataItem,"BrokenLinkReference") %></a>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
				<telerik:GridBoundColumn HeaderText="Broken Content Status" DataField="BrokenContentStatus"  HeaderStyle-Width="100" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Referrer Url" DataField="ReferrerUrl" HeaderStyle-Width="100" FilterControlWidth="50"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn HeaderText="Last Date Requested" DataField="LastRequested" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Width="100" FilterControlWidth="50"></telerik:GridBoundColumn>
			</Columns>
		</MasterTableView>
		<ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
			<Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
		</ClientSettings>
		<PagerStyle Mode="NextPrevAndNumeric" />
		<HeaderStyle Width="100px" />
		<ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
		<GroupingSettings ShowUnGroupButton="true" CaseSensitive="false" />
	</telerik:radgrid>
    <asp:PlaceHolder ID="phNoResults" runat="server" Visible="false">
        <p><em>There are no broken content links matching data.</em></p>
    </asp:PlaceHolder>
</asp:Content>
