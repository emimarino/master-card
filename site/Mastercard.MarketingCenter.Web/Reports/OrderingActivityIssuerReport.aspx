﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="OrderingActivityIssuerReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.OrderingActivityIssuerReport" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
        <div style="background-color: #ffffff; width: 100%; height: 100%; display: flex; justify-content: center; flex-direction: column; align-items: center;">
            <img src="<%= Mastercard.MarketingCenter.Common.Infrastructure.Constants.ImageUrls.ReportsLoader %>" alt="Loading..." />
        </div>
    </telerik:RadAjaxLoadingPanel>

    <div class="clr" style="margin-bottom: 10px;">
        <asp:HyperLink runat="server" NavigateUrl="~/report/orderingactivitylanding">&lt; Search again</asp:HyperLink>
    </div>
    <h2 style="color: #f58025;">Ordering Activity Report</h2>
    <p>
        <mc:ReportDateRangeDisplay id="ucDateRangeDisplay" runat="server"></mc:ReportDateRangeDisplay>
    </p>
    <p><strong>All Issuers Report</strong></p>

    <telerik:RadGrid ID="trgReport" Width="800" AllowSorting="True" PageSize="15" AllowPaging="True" runat="server" GridLines="None" AutoGenerateColumns="False" OnNeedDataSource="trgReport_NeedDataSource" AllowFilteringByColumn="true" ShowGroupPanel="true" OnItemCommand="trgReport_ItemCommand">
        <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
            <CommandItemSettings ShowRefreshButton="true" ShowExportToWordButton="true" ShowExportToExcelButton="true"
                ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false" />
            <Columns>
                <telerik:GridBoundColumn HeaderText="Issuer" DataField="IssuerName" Visible="false"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Issuer Title" HeaderStyle-Width="250" SortExpression="IssuerName" UniqueName="IssuerLink" DataField="IssuerName">
                    <ItemTemplate>
                        <asp:LinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem,"IssuerID") %>' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"IssuerName") %>' runat="server" />
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Processors" DataField="Processors" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="RPs" DataField="RPs" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Users" DataField="Users" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Downloads" DataField="Downloads" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="E.D. Orders" DataField="EDOrders" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="P.O.D. Orders" DataField="PODOrders" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Total Cost Ordered" DataField="TotalCostOrdered" FilterControlWidth="50"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
            <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
        </ClientSettings>
        <PagerStyle Mode="NextPrevAndNumeric" />
        <HeaderStyle Width="100px" />
        <ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="False" />
    </telerik:RadGrid>

    <asp:PlaceHolder ID="phNoResults" runat="server" Visible="false">
        <p style="color: #F58025;"><em>No results found in this date range.  Please try your search again, taking note of the effective date for the data available.</em></p>
    </asp:PlaceHolder>
</asp:Content>
