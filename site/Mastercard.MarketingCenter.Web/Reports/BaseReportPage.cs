﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public class BaseReportPage : System.Web.UI.Page
    {
        private IPrincipal _user = DependencyResolver.Current.GetService<IPrincipal>();
        public IPrincipal user { get { return _user; } set { _user = value; } }

        private UserContext _userContext = DependencyResolver.Current.GetService<UserContext>();
        public UserContext userContext { get { return _userContext; } set { _userContext = value; } }

        private ISegmentationService _segmentationService = DependencyResolver.Current.GetService<ISegmentationService>();
        public ISegmentationService segmentationService { get { return _segmentationService; } set { _segmentationService = value; } }

        protected DateTime StartDate { get; set; }
        protected DateTime EndDate { get; set; }
        protected IEnumerable<Segmentation> Segmentations { get; set; }
        protected IEnumerable<string> AudienceSegmentationIds { get; set; }
        protected bool? FilterMasterCardUsers { get; set; }
        protected bool? FilterVendorProcessor { get; set; }
        protected bool? FilterExpiredUsers { get; set; }
        protected int ReportType { get; set; }

        protected override void OnInit(EventArgs e)
        {
            ExtractReportDateRange();
            ExtractAdditionalParameters();
            base.OnInit(e);
        }

        protected void ExtractReportDateRange()
        {
            if (Request.QueryString["sd"] != null)
            {
                DateTime startDate;
                DateTime.TryParseExact(Request.QueryString["sd"].ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate);
                StartDate = startDate;
            }

            if (Request.QueryString["ed"] != null)
            {
                DateTime endDate;
                DateTime.TryParseExact(Request.QueryString["ed"].ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDate);
                EndDate = endDate;
            }
        }

        protected void ExtractAdditionalParameters()
        {
            if (!string.IsNullOrWhiteSpace(Request.QueryString["audience"]))
            {
                AudienceSegmentationIds = Request.QueryString["audience"].ToString().Split(';');
            }
            else
            {
                AudienceSegmentationIds = Enumerable.Empty<string>();
            }

            if (Request.QueryString["fmp"] != null)
            {
                FilterMasterCardUsers = bool.Parse(Request.QueryString["fmp"].ToString());
            }

            if (Request.QueryString["fvp"] != null)
            {
                FilterVendorProcessor = bool.Parse(Request.QueryString["fvp"].ToString());
            }

            if (Request.QueryString["feu"] != null)
            {
                FilterExpiredUsers = bool.Parse(Request.QueryString["feu"].ToString());
            }

            if (Request.QueryString["type"] != null)
            {
                ReportType = int.Parse(Request.QueryString["type"].ToString());
            }
        }

        protected string GetReportTypeUrl(int reportType)
        {
            switch (reportType)
            {
                case 0:
                    return "contentactivitypage";
                case 1:
                    return "contentactivityprogram";
                case 2:
                    return "contentactivityasset";
                case 4:
                    return "contentactivitywebinar";
                case 5:
                    return "contentactivityoffer";
                default:
                    return "contentactivitytag";
            }
        }

        protected IEnumerable<string> GetAudienceSegmentationIdsForReport()
        {
            if (AudienceSegmentationIds.Any(s => s.Equals(RegionConfigManager.DefaultSegmentation, StringComparison.InvariantCultureIgnoreCase)))
            {
                AudienceSegmentationIds = segmentationService.GetSegmentationsByRegion(userContext.SelectedRegion).Select(s => s.SegmentationId).ToArray();
            }

            return AudienceSegmentationIds;
        }
    }
}