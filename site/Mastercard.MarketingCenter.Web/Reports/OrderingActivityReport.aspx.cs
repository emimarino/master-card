﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Common;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class OrderingActivityReport : BaseReportPage
    {
        private IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }

        protected void trgReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();
            var programsData = reportRepository.GetOrderingActivityForProgramReport(StartDate, EndDate).ToList();
            var nonProgramData = reportRepository.GetOrderingActivityForNonProgramReport(StartDate, EndDate).FirstOrDefault();

            if (nonProgramData != null)
            {
                nonProgramData.RelatedProgramID = "0";
                nonProgramData.RelatedProgram = "[non-Program assets ordering]";
                programsData.Add(nonProgramData); // We add the non programs data first to keep consistent the sum of all results.
            }

            if (programsData.Any())
            {
                programsData.Add(new Data.Entities.Reporting.OrderingActivityReportResultItem
                {
                    RelatedProgramID = "All",
                    RelatedProgram = "[All]",
                    Downloads = programsData.Sum(x => x.Downloads),
                    EDOrders = programsData.Sum(x => x.EDOrders),
                    FIs = programsData.Sum(x => x.FIs),
                    PODOrders = programsData.Sum(x => x.PODOrders),
                    Processors = programsData.Sum(x => x.Processors),
                    TotalCostOrdered = programsData.Sum(x => x.TotalCostOrdered),
                    Users = programsData.Sum(x => x.Users)
                });
            }
            else
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }

            trgReport.DataSource = programsData.OrderBy(o => o.RelatedProgram.Equals("[All]") || o.RelatedProgram.Equals("[non-Program assets ordering]") ? 0 : 1)
                                               .ThenBy(o => o.RelatedProgram);
        }

        protected void trgReport_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "redirect")
            {
                Response.Redirect(_urlService.GetOrderingActivityDetailProgramReportURL(e.CommandArgument.ToString(), Request.QueryString.ToString()));
            }

            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName || e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToWordCommandName)
            {
                trgReport.MasterTableView.GetColumn("RelatedProgram").Visible = true;
                trgReport.MasterTableView.GetColumn("RelatedProgramLink").Visible = false;
            }
        }
    }
}