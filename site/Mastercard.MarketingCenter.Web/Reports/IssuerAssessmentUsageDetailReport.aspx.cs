﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Slam.Cms.Common;
using Slam.Cms.Data;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class IssuerAssessmentUsageDetailReport : BaseReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                aBack.HRef = "~/report/issuerassessmentusage?{0}".F(Request.QueryString);

                var reportType = Page.RouteData.Values["reportType"].ToString();
                lblReport.Text = reportType.First().ToString().ToUpper() + reportType.Substring(1);

                ucFilterDisplay.Setup(Enumerable.Empty<Data.Entities.Segmentation>(), Enumerable.Empty<string>(), FilterMasterCardUsers, FilterVendorProcessor);
            }
        }

        protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();
            var reportType = Page.RouteData.Values["reportType"].ToString();
            var report = reportRepository.GetIssuerAssessmentUsageDetailReport(StartDate, EndDate, FilterMasterCardUsers.Value, FilterVendorProcessor.Value);

            switch (reportType.ToString())
            {
                case "issuers":
                    trgReport.DataSource = report.OrderBy(iat => iat.Issuer).ThenBy(iat => iat.Date).ToList();
                    break;
                case "users":
                    trgReport.DataSource = report.OrderBy(iat => iat.Date).ToList();
                    break;
                case "visits":
                    trgReport.DataSource = report.OrderBy(iat => iat.VisitID).ThenBy(iat => iat.Date).ToList();
                    break;
                case "downloads":
                    trgReport.DataSource = report.Where(iat => iat.ItemType == "Download").OrderBy(iat => iat.Date).ToList();
                    break;
                case "reports":
                    trgReport.DataSource = report.Where(iat => iat.ItemType == "Report").OrderBy(iat => iat.Date).ToList();
                    break;
                default:
                    trgReport.DataSource = report.OrderBy(iat => iat.Date).ToList();
                    break;
            }
        }
    }
}