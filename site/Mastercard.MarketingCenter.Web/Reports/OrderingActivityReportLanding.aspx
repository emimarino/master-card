﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportLanding.Master" AutoEventWireup="true" CodeBehind="OrderingActivityReportLanding.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.OrderingActivityReportLanding" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="color: #f58025;">Ordering Activity Report</h2>

    <div class="clr" style="height: 50px;">
        <p><em>Step 1: </em>Please specify a date range for the report:</p>
        <mc:ReportDateRangeSelector id="ucDateRangeSelector" runat="server"></mc:ReportDateRangeSelector>
    </div>

    <div class="clr" style="margin-top:20px;">
        <p><em>Step 2: </em>Please specify the type of report:</p>
        <asp:RadioButtonList ID="rblReportType" runat="server">
            <asp:ListItem Text="Programs" Value="0" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Issuers" Value="1"></asp:ListItem>
        </asp:RadioButtonList>
    </div>

    <div style="padding-top: 20px;">
        <div class="btn_left" style="float: left; margin-left: 20px; margin-top: 2px;">
            <asp:LinkButton ID="btnValidateCode" runat="server" Text="Go" CssClass="btn_right" OnClick="btnGo_Click"></asp:LinkButton>
        </div>
    </div>
    <div class="clr report-notes">
        <em>Orders data effective as of 6/2009. [Medium Term] Downloads and Program-related Asset data effective as of 10/2011.<br />
            Estimated Distribution/Advisors tag/Customer Consent data effective as of 1/2012.</em>
    </div>
    <script type="text/javascript" src="/admin/Content/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="/admin/Content/js/report/report-loader.js"></script>
</asp:Content>