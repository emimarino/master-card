﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="SubscribableItemsDetailReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.SubscribableItemsDetailReport" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
        <div style="background-color: #ffffff; width: 100%; height: 100%"></div>
    </telerik:RadAjaxLoadingPanel>
    <div class="clr" style="margin-top: 10px;">
        <a id="A1" href="~/report/subscribableitemslanding" runat="server">< Search again</a>
    </div>
    <h2 style="color: #f58025;">Subscribable Items Report</h2>
    <p>
        <a id="aBack" runat="server">< Back to List of All Items Available for Subscription and Favoriting</a>
    </p>
    <p>
        <strong>Subscribable Items Report by
            <asp:Literal ID="ltItemType" runat="server"></asp:Literal>:</strong>
        <asp:Label ID="lblItemTitle" runat="server" CssClass="orange"></asp:Label>
    </p>
    <p>
        <mc:ReportDateRangeDisplay id="ucDateRangeDisplay" runat="server"></mc:ReportDateRangeDisplay>
    </p>
    <telerik:RadGrid ID="trgReport" Width="1200" AllowSorting="True" PageSize="15" AllowPaging="True" runat="server" GridLines="None" AutoGenerateColumns="False" OnNeedDataSource="trgReport_NeedDataSource" AllowFilteringByColumn="true" ShowGroupPanel="true">
        <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
            <CommandItemSettings ShowRefreshButton="" ShowExportToWordButton="true" ShowExportToExcelButton="true"
                ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false" />
            <Columns>
                <telerik:GridBoundColumn HeaderText="Item" DataField="ContentItemName"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="User" DataField="UserName"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Issuer" DataField="IssuerName"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Notification Frequency" DataField="Frequency"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Favorite Date" DataField="SavedDate" DataFormatString="{0:MM/dd/yyyy}"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
            <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
        </ClientSettings>
        <PagerStyle Mode="NextPrevAndNumeric" />
        <HeaderStyle Width="100px" />
        <ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="False" />
    </telerik:RadGrid>
</asp:Content>