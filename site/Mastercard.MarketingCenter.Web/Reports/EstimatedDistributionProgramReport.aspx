﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="EstimatedDistributionProgramReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.EstimatedDistributionProgramReport" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register tagPrefix="mc" tagname="ContentActivityDetailFilterDisplay" src="~/Reports/UserControls/ContentActivityDetailFilterDisplay.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
                <div style="background-color:#ffffff; width:100%; height:100%"></div>
    </telerik:RadAjaxLoadingPanel>
    <div style="margin-top:10px;">
        <a id="A1" href="~/report/estimateddistributionlanding" runat="server">< Search again</a>
    </div>
    <h2 style="color:#f58025;">Estimated Distribution Report</h2>
    <p>
        <mc:ReportDateRangeDisplay id="ucDateRangeDisplay" runat="server"></mc:ReportDateRangeDisplay>
    </p>
    <strong>ED Report grouped by Programs</strong>
    <mc:ContentActivityDetailFilterDisplay ID="ucFilterDisplay" runat="server"></mc:ContentActivityDetailFilterDisplay>
    <telerik:RadGrid ID="trgReport" Width="800" AllowSorting="True" PageSize="15" AllowPaging="True" runat="server" Gridlines="None" AutoGenerateColumns="False" OnNeedDataSource="trgReport_NeedDataSource" AllowFilteringByColumn="true" ShowGroupPanel="true" OnItemCommand="trgReport_ItemCommand"  OnPreRender="trgReport_PreRender">
        <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
            <CommandItemSettings ShowRefreshButton="" ShowExportToWordButton="true" ShowExportToExcelButton="true"
                ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false"/>
            <Columns>
                <telerik:GridBoundColumn HeaderText="Title" DataField="Title" Visible="false"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Title" HeaderStyle-Width="250" SortExpression="Title" UniqueName="TitleLink" DataField="Title">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"ContentItemId") %>' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"Title") %>' runat="server"/>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Number of Downloads" DataField="TotalDownloads" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridDateTimeColumn HeaderText="Most Recent Download" DataField="MostRecentDownload" DataFormatString="{0:MM/dd/yyyy}" DataType="System.DateTime" HtmlEncode="false" HeaderStyle-Width="120" FilterControlWidth="100"></telerik:GridDateTimeColumn>
                <telerik:GridBoundColumn HeaderText="Total Estimated Distribution" DataField="TotalEstimatedDistribution" FilterControlWidth="50"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
            <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
        </ClientSettings>
        <PagerStyle Mode="NextPrevAndNumeric" />
        <HeaderStyle Width="100px" />
        <ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="False" />        
    </telerik:RadGrid>

    <asp:PlaceHolder ID="phNoResults" runat="server" Visible="false">
        <p style="color:#F58025;"><em>No results found in this date range.  Please try your search again, taking note of the effective date for the data available.</em></p>
    </asp:PlaceHolder>

</asp:Content>
