﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class TriggerMarketingNotificationsDetailReport : BaseReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var triggerMarketingSubscription = Page.RouteData.Values["triggerMarketingSubscription"].ToString();
                DateTime notificationDate;
                if (DateTime.TryParseExact(Request.QueryString["notificationDate"].ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out notificationDate) &&
                    (triggerMarketingSubscription.Equals(Constants.TrackingTypes.TriggerMarketingSubscriptionDaily, StringComparison.InvariantCultureIgnoreCase) ||
                     triggerMarketingSubscription.Equals(Constants.TrackingTypes.TriggerMarketingSubscriptionWeekly, StringComparison.InvariantCultureIgnoreCase)))
                {
                    lblNotificationDetails.Text = $"{notificationDate.ToString("MM/dd/yyyy")} - {(triggerMarketingSubscription == Constants.TrackingTypes.TriggerMarketingSubscriptionDaily ? "Daily" : "Weekly")}";
                }

                aBack.HRef = $"~/report/triggermarketingnotifications?{Request.QueryString}";
            }
        }

        protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var triggerMarketingSubscription = Page.RouteData.Values["triggerMarketingSubscription"].ToString();
            DateTime notificationDate;
            if (DateTime.TryParseExact(Request.QueryString["notificationDate"].ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out notificationDate) &&
                (triggerMarketingSubscription.Equals(Constants.TrackingTypes.TriggerMarketingSubscriptionDaily, StringComparison.InvariantCultureIgnoreCase) ||
                 triggerMarketingSubscription.Equals(Constants.TrackingTypes.TriggerMarketingSubscriptionWeekly, StringComparison.InvariantCultureIgnoreCase)))
            {
                var reportService = DependencyResolver.Current.GetService<IReportService>();
                trgReport.DataSource = reportService.GetTriggerMarketingNotificationsDetailReport(triggerMarketingSubscription, notificationDate, userContext.SelectedRegion).ToList();
            }
        }
    }
}