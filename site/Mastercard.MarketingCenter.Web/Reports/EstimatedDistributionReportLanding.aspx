﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportLanding.Master" AutoEventWireup="true" CodeBehind="EstimatedDistributionReportLanding.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.EstimatedDistributionReportLanding" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/admin/Content/css/plugins/chosen/chosen.css" rel="stylesheet" />
    <style>
        /* CHOSEN PLUGIN */
        .chosen-container-single .chosen-single {
            background: #ffffff;
            box-shadow: none;
            -moz-box-sizing: border-box;
            background-color: #FFFFFF;
            border: 1px solid darkgray;
            border-radius: 2px;
            cursor: text;
            height: auto !important;
            margin: 0;
            overflow: hidden;
            font-size: 11px;
            position: relative;
            width: 402px;
        }

        .chosen-container .chosen-drop {
            width: 412px;
        }

        .chosen-container-multi .chosen-choices {
            min-height: unset !important;
            padding-left: 8px;
            width: 400px;
            border-color: darkgray;
        }

            .chosen-container-multi .chosen-choices li.search-choice {
                height: auto !important;
                min-height: unset !important;
                font-size: 11px;
                margin: 1px 5px 1px 0;
                padding: 1px 20px 1px 5px;
            }

                .chosen-container-multi .chosen-choices li.search-choice .search-choice-close {
                    top: 3px;
                }

            .chosen-container-multi .chosen-choices li.search-field input[type="text"] {
                margin: 0;
                padding: 0;
                height: auto !important;
                color: #444;
                font-size: 11px;
                font-family: Verdana, Arial, Helvetica, sans-serif;
                line-height: 20px;
            }
    </style>
    <script type="text/javascript" src="/admin/Content/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="/admin/Content/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/admin/Content/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script type="text/javascript" src="/admin/Content/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="/admin/Content/js/plugins/chosen/chosen.jquery.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2 style="color: #f58025;">Estimated Distribution Report</h2>

    <div class="clr" style="height: 50px;">
        <p><em>Step 1: </em>Please specify a date range for the report:</p>
        <mc:ReportDateRangeSelector ID="ucDateRangeSelector" runat="server"></mc:ReportDateRangeSelector>
    </div>
    <div class="clr" style="margin-top: 20px;">
        <p><em>Step 2: </em>Please specify the type of report:</p>
        <asp:RadioButtonList ID="rblReportType" runat="server">
            <asp:ListItem Text="Assets" Value="0" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Programs" Value="1"></asp:ListItem>
            <asp:ListItem Text="Issuers" Value="2"></asp:ListItem>
        </asp:RadioButtonList>
    </div>

    <%  if (CanChangeSegments)
        { %>
    <div class="clr" style="margin-top: 20px;">
        <p><em>Step 3: </em>Please select the Audience Segment(s) to include in this report:</p>
        <asp:ListBox runat="server" ID="ddlSegmentations" SelectionMode="Multiple" Width="440" CssClass="chosen-choices" TabIndex="-1" multiple="multiple" data-placeholder="Select Segmentation..." />
    </div>
    <% } %>

    <div class="clr" style="margin-top: 20px;" runat="server" id="step4Section">
        <p><em>Step <%=(CanChangeSegments)?"4":"3" %>: OPTIONAL.</em> Please indicate if you would like the following group to be included in this report:</p>
        <asp:CheckBox ID="chkMasterCard" Text="Mastercard Employees" runat="server" />
    </div>

    <div style="padding-top: 2px; width: 400px; margin-top: 20px;">
        <div class="btn_left" style="float: right; margin-left: 20px;">
            <asp:LinkButton ID="btnValidateCode" runat="server" Text="Go" CssClass="btn_right" OnClick="btnGo_Click"></asp:LinkButton>
        </div>
    </div>
    <script type="text/javascript" src="/admin/Content/js/inspinia.js"></script>
</asp:Content>