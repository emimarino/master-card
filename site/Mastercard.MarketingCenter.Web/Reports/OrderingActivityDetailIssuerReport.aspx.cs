﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Common;
using System;
using System.Linq;
using System.Web.Mvc;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class OrderingActivityDetailIssuerReport : BaseReportPage
    {
        private IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }
        private IIssuerService _issuerService { get { return DependencyResolver.Current.GetService<IIssuerService>(); } }
        private ReportRepository _reportRepository { get { return DependencyResolver.Current.GetService<ReportRepository>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var id = Page.RouteData.Values["id"].ToString().ToLower();
                if (id.Equals("all"))
                {
                    lblIssuer.Text = "[All]";
                }
                else
                {
                    var issuer = _issuerService.GetIssuerById(id);
                    lblIssuer.Text = issuer?.Title ?? string.Empty;
                }

                aBack.HRef = _urlService.GetOrderingActivityIssuersReportURL(Request.QueryString.ToString());
            }
        }

        protected void trgReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var id = Page.RouteData.Values["id"].ToString();
            trgReport.DataSource = _reportRepository.GetOrderingActivityForIssuerDetailReport(id, StartDate, EndDate).OrderBy(o => o.Date).ToList();
            trgReport.Columns.FindByUniqueName("FinancialInstitutionDiscriminator").Visible = id.ToLower().Equals("all");
        }

        protected void trgReport_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "redirect")
            {
                string referrer = $"&ReturnUrl={Server.UrlEncode(_urlService.GetOrderingActivityDetailIssuerReportURL(Page.RouteData.Values["id"].ToString(), Request.QueryString.ToString()))}";
                Response.Redirect(_urlService.GetOrderDetailsReportURL(e.CommandArgument.ToString(), referrer));
            }
        }
    }
}