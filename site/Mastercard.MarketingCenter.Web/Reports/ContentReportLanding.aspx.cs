﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Reports.UserControls;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class ContentReportLanding : BaseReportPage
    {
        private IRegionService _regionService = DependencyResolver.Current.GetService<IRegionService>();
        private IContentItemService _contentItemService = DependencyResolver.Current.GetService<IContentItemService>();
        private IUrlService _urlService = DependencyResolver.Current.GetService<IUrlService>();
        private IEnumerable<Region> Regions { get; set; }
        protected bool IsInPermission { get; set; }
        private int stepNumber = 1;

        protected int StepNumber
        {
            get { return stepNumber++; }
            set { StepNumber = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            IsInPermission = user.IsInPermission(Constants.Permissions.CanViewAllRegionContentReport);
            btnValidateCode.OnClientClick = (Master as ReportLanding)?.GetLoader();
            if (IsInPermission)
            {
                if (!IsPostBack)
                {
                    var allListItem = new ListItem("All Regions", "all");
                    allListItem.Selected = true;
                    cblReportRegion.Items.Add(allListItem);

                    foreach (Region region in _regionService.GetAll())
                    {
                        cblReportRegion.Items.Add(new ListItem(region.Name, region.IdTrimmed));
                    }
                }
            }
            else
            {
                var region = _regionService.GetById(userContext.SelectedRegion);
                var regionItem = new ListItem(region.Name, region.IdTrimmed);
                regionItem.Selected = true;
                cblReportRegion.Items.Add(regionItem);
                cblReportRegion.Enabled = false;
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            if (trdpCreationRangeDate.Validate() && IsValidDateRanges(trdpCreationRangeDate))
            {
                Response.Redirect(_urlService.GetContentDetailReportLink(trdpCreationRangeDate.StartDate, trdpCreationRangeDate.EndDate, cblReportRegion.SelectedValue, tbKeyword.Text));
            }
        }

        private bool IsValidDateRanges(ReportDateRangeSelector dateRangeSelector)
        {
            if (dateRangeSelector.StartDate.HasValue && dateRangeSelector.EndDate.HasValue && dateRangeSelector.StartDate <= dateRangeSelector.EndDate)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}