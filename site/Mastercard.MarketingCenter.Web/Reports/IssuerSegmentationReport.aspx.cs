﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Modules;
using Slam.Cms.Common;
using System.Linq;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class IssuerSegmentationReport : BaseReportPage
    {
        protected IIssuerService _issuerService { get { return WebCoreModule.GetService<IIssuerService>(); } }

        protected void trgReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var results = from i in _issuerService.GetIssuersWithSegmentation(userContext.SelectedRegion).SelectMany(i => i.IssuerSegmentations)
                          select new
                          {
                              IssuerName = i.Issuer.Title,
                              SegmentationId = i.Segmentation.SegmentationId,
                              SegmentationName = i.Segmentation.SegmentationName
                          };

            trgReport.DataSource = results.Distinct().OrderBy(i => i.IssuerName);
            if (!results.Any())
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }
        }

        protected void trgReport_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "redirect")
            {
                Response.Redirect("~/report/issuersegmentationdetail/{0}?{1}".F(e.CommandArgument, Request.QueryString));
            }

            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName || e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToWordCommandName)
            {
                trgReport.MasterTableView.GetColumn("Segmentation").Visible = true;
                trgReport.MasterTableView.GetColumn("SegmentationLink").Visible = false;
            }
        }
    }
}