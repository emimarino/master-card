﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="UserRegistrationReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.UserRegistrationReport" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
        <div style="background-color: #ffffff; width: 100%; height: 100%; display: flex; justify-content: center; flex-direction: column; align-items: center;">
            <img src="<%= Mastercard.MarketingCenter.Common.Infrastructure.Constants.ImageUrls.ReportsLoader %>" alt="Loading..." />
        </div>
    </telerik:RadAjaxLoadingPanel>
    <div style="margin-top:10px;">
        <a id="A1" href="~/report/userregistrationlanding" runat="server">< Search again</a>
    </div>
    <h2 style="color:#f58025;">User Registration Details Report</h2>
    <p>
        <mc:ReportDateRangeDisplay id="ucDateRangeDisplay" runat="server"></mc:ReportDateRangeDisplay>
    </p>
    <p><strong>User Registration Details Report</strong></p>

    <telerik:RadGrid ID="trgReport" Width="800" AllowSorting="True" PageSize="15" AllowPaging="True" runat="server" Gridlines="None" AutoGenerateColumns="False" OnNeedDataSource="trgReport_NeedDataSource" AllowFilteringByColumn="true" ShowGroupPanel="true" OnItemCommand="trgReport_ItemCommand" OnItemDataBound="trgReport_ItemDataBound">
        <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
            <CommandItemSettings ShowRefreshButton="" ShowExportToWordButton="true" ShowExportToExcelButton="true"
                ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false"/>
            <Columns>
                <telerik:GridBoundColumn HeaderText="Date Registered" DataField="RegistrationDate" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Width="120" FilterControlWidth="80"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Last Login Date" DataField="LastLoginDate" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Width="120" FilterControlWidth="80"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="First Name" DataField="FirstName" FilterControlWidth="60"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Last Name" DataField="LastName" FilterControlWidth="60"></telerik:GridBoundColumn>
                 <telerik:GridBoundColumn HeaderText="Country" DataField="Country" FilterControlWidth="60"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Email" DataField="Email" Visible="false"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Email" HeaderStyle-Width="200" SortExpression="Email" UniqueName="EmailLink" DataField="Email">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"UserName") %>' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"Email") %>' runat="server"/>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="FI Name" DataField="FI" FilterControlWidth="60"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Audience Segment" DataField="AudienceSegments" FilterControlWidth="60"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Processor Name" DataField="Processor" FilterControlWidth="60"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Email Consent" DataField="EmailConsent" FilterControlWidth="60"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
            <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
        </ClientSettings>
        <PagerStyle Mode="NextPrevAndNumeric" />
        <HeaderStyle Width="100px" />
        <ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="False" />        
    </telerik:RadGrid>

    <asp:PlaceHolder ID="phNoResults" runat="server" Visible="false">
        <p style="color:#F58025;"><em>No results found in this date range.  Please try your search again, taking note of the effective date for the data available.</em></p>
    </asp:PlaceHolder>

</asp:Content>
