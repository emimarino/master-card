﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities.Reporting;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class UsersSubscriptionDetailReport : BaseReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var userId = 0;
                int.TryParse(Page.RouteData.Values["userId"].ToString(), out userId);
                if (userId > 0)
                {
                    var userRepository = DependencyResolver.Current.GetService<IUserRepository>();
                    var user = userRepository.GetUser(userId);

                    lblUser.Text = $"{user.FullName} - {user.Issuer?.Title}";
                }

                aBack.HRef = $"~/report/userssubscription?{Request.QueryString}";
            }
        }

        protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var userId = 0;
            int.TryParse(Page.RouteData.Values["userId"].ToString(), out userId);                        
            var reportService = DependencyResolver.Current.GetService<ReportService>();
            if (userId > 0)
            {                
                trgReport.DataSource = reportService.GetUsersSubscriptionDetailReport(StartDate, EndDate, userId, userContext.SelectedRegion).ToList();
                trgReport.MasterTableView.GetColumn("UserFullName").Display = false;
                trgReport.MasterTableView.GetColumn("Email").Display = false;
            }
            else if (userId == -1)
            {
                trgReport.DataSource = reportService.GetUsersSubscriptionListDetailReport(StartDate, EndDate, userContext.SelectedRegion).ToList();
            }
        }
    }
}