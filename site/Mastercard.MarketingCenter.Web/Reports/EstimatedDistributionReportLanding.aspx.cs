﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Configuration;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class EstimatedDistributionReportLanding : System.Web.UI.Page
    {
        public bool CanChangeSegments { get { return User.IsInPermission(Constants.Permissions.CanChangeSegments); } }
        public IIssuerService _issuerService { get { return DependencyResolver.Current.GetService<IIssuerService>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var userContext = DependencyResolver.Current.GetService<UserContext>();
                var mastercardIssuers = _issuerService.GetMastercardIssuersByRegion(userContext.SelectedRegion);
                chkMasterCard.Visible = mastercardIssuers.Any();
                step4Section.Visible = chkMasterCard.Visible;

                var segmentationServices = DependencyResolver.Current.GetService<ISegmentationService>();
                var segmentations = segmentationServices.GetSegmentationsByRegion(userContext.SelectedRegion).ToList();
                segmentations.Insert(0, new Segmentation { SegmentationName = Resources.Shared.All, SegmentationId = RegionConfigManager.DefaultSegmentation });
                ddlSegmentations.DataSource = segmentations;
                ddlSegmentations.DataTextField = "SegmentationName";
                ddlSegmentations.DataValueField = "SegmentationId";
                ddlSegmentations.AppendDataBoundItems = true;
                ddlSegmentations.DataBind();
                ddlSegmentations.SelectedIndex = 0;
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            if (ucDateRangeSelector.Validate())
            {
                string reportTypeUrl;
                switch (int.Parse(rblReportType.SelectedValue))
                {
                    case 0:
                        reportTypeUrl = "estimateddistributionasset";
                        break;
                    case 1:
                        reportTypeUrl = "estimateddistributionprogram";
                        break;
                    case 2:
                        reportTypeUrl = "estimateddistributionissuer";
                        break;
                    default:
                        reportTypeUrl = "estimateddistributionasset";
                        break;
                }

                Response.Redirect($"~/report/{reportTypeUrl}?sd={ucDateRangeSelector.StartDate.Value.ToString("MM/dd/yyyy")}&ed={ucDateRangeSelector.EndDate.Value.ToString("MM/dd/yyyy")}&fmp={!chkMasterCard.Checked}&audience={string.Join(";", (from ListItem item in ddlSegmentations.Items where item.Selected && !string.IsNullOrWhiteSpace(item.Value) select item.Value))}&type={rblReportType.SelectedValue}");
            }
        }
    }
}