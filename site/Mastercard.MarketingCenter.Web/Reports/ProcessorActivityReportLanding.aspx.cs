﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services;
using System;
using System.Security.Principal;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class ProcessorActivityReportLanding : System.Web.UI.Page
    {
        IPrincipal user { get { return DependencyResolver.Current.GetService<IPrincipal>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            btnValidateCode.OnClientClick = (Master as ReportLanding)?.GetLoader();
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            if (ucDateRangeSelector.Validate())
            {
                if (user.IsInPermission(Constants.Permissions.CanManageProcessors))
                {
                    Response.Redirect($"~/report/processoractivity?sd={ucDateRangeSelector.StartDate.Value.ToString("MM/dd/yyyy")}&ed={ucDateRangeSelector.EndDate.Value.ToString("MM/dd/yyyy")}");
                }
                else
                {
                    Response.Redirect($"~/report/processoractivitydetail/-1?sd={ucDateRangeSelector.StartDate.Value.ToString("MM/dd/yyyy")}&ed={ucDateRangeSelector.EndDate.Value.ToString("MM/dd/yyyy")}");
                }
            }
        }
    }
}