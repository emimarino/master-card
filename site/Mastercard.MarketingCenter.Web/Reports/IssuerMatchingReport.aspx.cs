﻿using Mastercard.MarketingCenter.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
	public partial class IssuerMatchingReport : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
		}

		protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
		{
			var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();
			var results = reportRepository.GetIssuerMatchingReport().ToList();
			trgReport.DataSource = results;
			if (results.Count == 0)
			{
				trgReport.Visible = false;
				phNoResults.Visible = true;
			}
		}

		protected void trgReport_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
		{
		}

		protected void lnkClearFilters_Click(object source, EventArgs e)
		{
			Response.Redirect("~/report/issuermatching");
		}
	}
}