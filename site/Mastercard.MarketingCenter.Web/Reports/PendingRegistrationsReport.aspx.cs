﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Linq;
using System.Web.Mvc;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class PendingRegistrationsReport : System.Web.UI.Page
    {
        protected string PendingRegistrationUrl { get; set; }
        IPendingRegistrationService _pendingRegistrationService { get { return DependencyResolver.Current.GetService<IPendingRegistrationService>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["guid"] != null)
                {
                    PendingRegistrationUrl = "{0}/registration/pending/{1}".F(Slam.Cms.Configuration.ConfigurationManager.Environment.FrontEndUrl.TrimEnd('/'), Request.QueryString["guid"]);
                    phParentRedirect.Visible = true;
                    trgReport.Visible = false;
                    phMain.Visible = false;
                    phLoading.Visible = true;
                }
            }
        }

        protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var userContext = DependencyResolver.Current.GetService<UserContext>();
            var results = _pendingRegistrationService.GetPendingRegistrationsByStatus(Common.Infrastructure.Constants.PendingRegistrationStatus.Pending, userContext.SelectedRegion).ToList();
            trgReport.DataSource = results;
            if (!results.Any())
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }
        }

        protected void trgReport_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "redirect")
            {
                Response.Redirect("~/report/pendingregistrations?guid={0}".F(e.CommandArgument));
            }

            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName || e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToWordCommandName)
            {
                trgReport.MasterTableView.GetColumn("Email").Visible = true;
                trgReport.MasterTableView.GetColumn("EmailLink").Visible = false;
            }
        }

        protected void lnkClearFilters_Click(object source, EventArgs e)
        {
            Response.Redirect("~/report/pendingregistrations");
        }
    }
}