﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Common;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class OrderingActivityIssuerReport : BaseReportPage
    {
        private IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }

        protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();
            var data = reportRepository.GetOrderingActivityForIssuerReport(StartDate, EndDate).ToList();
            if (data.Any())
            {
                data.Add(new Data.Entities.Reporting.OrderingActivityIssuerReportResultItem
                {
                    IssuerID = "All",
                    IssuerName = "[All]",
                    Downloads = data.Sum(x => x.Downloads),
                    EDOrders = data.Sum(x => x.EDOrders),
                    PODOrders = data.Sum(x => x.PODOrders),
                    Processors = data.Sum(x => x.Processors),
                    RPs = data.Sum(x => x.RPs),
                    TotalCostOrdered = data.Sum(x => x.TotalCostOrdered),
                    Users = data.Sum(x => x.Users)
                });
            }
            else
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }

            trgReport.DataSource = data.OrderBy(o => o.IssuerName.Equals("[All]") ? 0 : 1).ThenBy(o => o.IssuerName);
        }

        protected void trgReport_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "redirect")
            {
                Response.Redirect(_urlService.GetOrderingActivityDetailIssuerReportURL(e.CommandArgument.ToString(), Request.QueryString.ToString()));
            }

            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName || e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToWordCommandName)
            {
                trgReport.MasterTableView.GetColumn("IssuerName").Visible = true;
                trgReport.MasterTableView.GetColumn("IssuerLink").Visible = false;
            }
        }
    }
}