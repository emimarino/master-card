﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="TriggerMarketingNotificationsDetailReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.TriggerMarketingNotificationsDetailReport" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
        <div style="background-color: #ffffff; width: 100%; height: 100%"></div>
    </telerik:RadAjaxLoadingPanel>
    <div class="clr" style="margin-top: 10px;">
        <a id="A1" href="~/report/triggermarketingnotificationslanding" runat="server">< Search again</a>
    </div>
    <h2 style="color: #f58025;">Email Notifications Report</h2>
    <p>
        <a id="aBack" runat="server">< Back to List of All Email Notifications</a>
    </p>
    <p>
        <strong>Notification details: </strong>
        <asp:Label ID="lblNotificationDetails" runat="server" CssClass="orange"></asp:Label>
    </p>
    <telerik:RadGrid ID="trgReport" Width="1200" AllowSorting="True" PageSize="15" AllowPaging="True" runat="server" GridLines="None" AutoGenerateColumns="False" OnNeedDataSource="trgReport_NeedDataSource" AllowFilteringByColumn="true" ShowGroupPanel="true">
        <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
            <CommandItemSettings ShowRefreshButton="" ShowExportToWordButton="true" ShowExportToExcelButton="true"
                ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false" />
            <Columns>
                <telerik:GridBoundColumn HeaderText="User" DataField="UserName"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Issuer" DataField="IssuerName"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Opened Email" DataField="Opened">
                    <ItemTemplate><%# (Boolean.Parse(Eval("Opened").ToString())) ? "Yes" : "No" %></ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Links Sent" DataField="TotalLinks"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Click Rate" DataField="ClickRate" DataFormatString="{0:P}"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
            <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
        </ClientSettings>
        <PagerStyle Mode="NextPrevAndNumeric" />
        <HeaderStyle Width="100px" />
        <ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="False" />
    </telerik:RadGrid>
</asp:Content>