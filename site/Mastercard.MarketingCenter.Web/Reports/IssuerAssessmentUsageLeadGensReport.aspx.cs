﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Slam.Cms.Common;
using Slam.Cms.Data;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class IssuerAssessmentUsageLeadGensReport : BaseReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                aBack.HRef = "~/report/issuerassessmentusage?{0}".F(Request.QueryString);
                ucFilterDisplay.Setup(Enumerable.Empty<Data.Entities.Segmentation>(), Enumerable.Empty<string>(), FilterMasterCardUsers, FilterVendorProcessor);
            }
        }

        protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();
            trgReport.DataSource = reportRepository.GetIssuerAssessmentUsageLeadGensReport(StartDate, EndDate, FilterMasterCardUsers.Value, FilterVendorProcessor.Value).OrderBy(iat => iat.Date).ToList();
        }
    }
}