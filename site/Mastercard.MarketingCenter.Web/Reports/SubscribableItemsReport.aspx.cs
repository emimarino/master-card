﻿using Mastercard.MarketingCenter.Web.Core.Services;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class SubscribableItemsReport : BaseReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var reportService = DependencyResolver.Current.GetService<ReportService>();
            var results = reportService.GetSubscribableItemsReport(StartDate, EndDate, userContext.SelectedRegion).ToList();
            if (results.Any())
            {
                results.Insert(0, new Data.Entities.Reporting.SubscribableItemsReportResultItem
                {
                    Title = "[All]",
                    ItemId = "All-Items",
                    Type = string.Join(", ", results.Select(x => x.Type).ToList().Distinct()),
                    Favorites = results.Sum(x => x.Favorites)
                });
                trgReport.DataSource = results;
            }
            else
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }
        }

        protected void trgReport_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "redirect")
            {
                Response.Redirect($"~/report/subscribableitemsdetail/{e.CommandArgument}?{Request.QueryString}");
            }

            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName || e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToWordCommandName)
            {
                trgReport.MasterTableView.GetColumn("Title").Visible = true;
                trgReport.MasterTableView.GetColumn("ItemLink").Visible = false;
            }
        }
    }
}