﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportDateRangeDisplay.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.UserControls.ReportDateRangeDisplay" %>

FROM <strong><asp:Literal ID="ltStartDate" runat="server"></asp:Literal></strong> TO <strong><asp:Literal ID="ltEndDate" runat="server"></asp:Literal></strong>