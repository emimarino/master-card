﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentActivityDetailFilterDisplay.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.UserControls.ContentActivityDetailFilterDisplay" %>

<asp:PlaceHolder ID="audienceFilter" runat="server">
<p style="margin-top:5px;margin-bottom:0px;">Customer Audience Segment being reported: <em><asp:Literal ID="litAudienceSegment" runat="server"></asp:Literal></em></p>
</asp:PlaceHolder>
<asp:PlaceHolder ID="groupFilter" runat="server">
<p style="margin-top:5px;">Group(s) activity included in addition to Customers: <em><asp:Literal ID="litGroups" runat="server"></asp:Literal></em></p>
</asp:PlaceHolder>