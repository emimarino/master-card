﻿using Mastercard.MarketingCenter.Data.Entities;
using Slam.Cms.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Web.Reports.UserControls
{
    public partial class ContentActivityDetailFilterDisplay : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void Setup(IEnumerable<Segmentation> segmentations, IEnumerable<string> audienceSegmentationIds, bool? filterMasterCardUsers, bool? filterVendorProcessor)
        {
            if (audienceSegmentationIds.Any())
            {
                audienceFilter.Visible = true;
                litAudienceSegment.Text = audienceSegmentationIds.Any(s => s.Equals(RegionConfigManager.DefaultSegmentation, StringComparison.InvariantCultureIgnoreCase)) ?
                                            Resources.Shared.All :
                                            string.Join(" + ", segmentations.Where(s => audienceSegmentationIds.Contains(s.SegmentationId)).Select(s => s.SegmentationName));
            }

            groupFilter.Visible = filterMasterCardUsers.HasValue || filterVendorProcessor.HasValue;
            var filters = new List<string>();
            if (filterMasterCardUsers.HasValue && !filterMasterCardUsers.Value)
                filters.Add("Mastercard Employees");
            if (filterVendorProcessor.HasValue && !filterVendorProcessor.Value)
                filters.Add("Vendor");
            if ((!filterMasterCardUsers.HasValue || (filterMasterCardUsers.HasValue && filterMasterCardUsers.Value)) &&
                (!filterVendorProcessor.HasValue || (filterVendorProcessor.HasValue && filterVendorProcessor.Value)))
                filters.Add("(None)");

            litGroups.Text = string.Join(", ", filters);
        }
    }
}