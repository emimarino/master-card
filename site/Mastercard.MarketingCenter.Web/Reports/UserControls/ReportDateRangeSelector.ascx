﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportDateRangeSelector.ascx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.UserControls.ReportDateRangeSelector" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<div id="report-date-range-selector">
    <label id="lblStartDate" runat="server">Start Date:</label>
    <telerik:RadDatePicker ID="trdpStartDate" runat="server" Width="90">
        <Calendar>
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-CssClass="rcToday"></telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <label id="lblEndDate" runat="server">End Date:</label>
    <telerik:RadDatePicker ID="trdpEndDate" runat="server" Width="90">
        <Calendar>
            <SpecialDays>
                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-CssClass="rcToday"></telerik:RadCalendarDay>
            </SpecialDays>
        </Calendar>
    </telerik:RadDatePicker>
    <asp:PlaceHolder ID="phError" runat="server" Visible="false">
        <p class="error">
            <asp:Literal ID="litError" runat="server"></asp:Literal>
        </p>
    </asp:PlaceHolder>
</div>