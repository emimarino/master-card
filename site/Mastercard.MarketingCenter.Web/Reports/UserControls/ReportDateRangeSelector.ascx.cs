﻿using Mastercard.MarketingCenter.Common.Extensions;
using System;

namespace Mastercard.MarketingCenter.Web.Reports.UserControls
{
    public partial class ReportDateRangeSelector : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public bool Validate()
        {
            if (trdpStartDate.SelectedDate == null || trdpEndDate.SelectedDate == null)
            {
                phError.Visible = true;
                litError.Text = "Please enter valid date values.";
                return false;
            }

            if (!trdpStartDate.DateInput.InvalidTextBoxValue.IsNullOrEmpty() || !trdpEndDate.DateInput.InvalidTextBoxValue.IsNullOrEmpty())
            {
                phError.Visible = true;
                litError.Text = "Please enter values in a valid date format.";
                return false;
            }

            if (trdpStartDate.SelectedDate.Value > trdpEndDate.SelectedDate.Value)
            {
                phError.Visible = true;
                litError.Text = "Please re-enter the dates. The End Date cannot be earlier than the Start Date.";
                return false;
            }
            phError.Visible = false;
            return true;
        }

        public DateTime? StartDate { get { return trdpStartDate.SelectedDate; } }

        public DateTime? EndDate { get { return trdpEndDate.SelectedDate; } }
    }
}