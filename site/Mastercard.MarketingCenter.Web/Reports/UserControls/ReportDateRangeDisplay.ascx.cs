﻿using System;
using System.Globalization;

namespace Mastercard.MarketingCenter.Web.Reports.UserControls
{
    public partial class ReportDateRangeDisplay : System.Web.UI.UserControl
    {
        protected DateTime StartDate { get; set; }
        protected DateTime EndDate { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ExtractReportDateRange();
                ltStartDate.Text = StartDate.ToShortDateString();
                ltEndDate.Text = EndDate.ToShortDateString();
            }
        }

        protected void ExtractReportDateRange()
        {
            if (Request.QueryString["sd"] != null)
            {
                DateTime startDate;
                DateTime.TryParseExact(Request.QueryString["sd"].ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate);
                StartDate = startDate;
            }

            if (Request.QueryString["ed"] != null)
            {
                DateTime endDate;
                DateTime.TryParseExact(Request.QueryString["ed"].ToString(), "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDate);
                EndDate = endDate;
            }
        }
    }
}