﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="IssuerAssessmentUsageDetailReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.IssuerAssessmentUsageDetailReport" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register tagPrefix="mc" tagname="ContentActivityDetailFilterDisplay" src="~/Reports/UserControls/ContentActivityDetailFilterDisplay.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel  ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
                <div style="background-color:#ffffff; width:100%; height:100%"></div>
    </telerik:RadAjaxLoadingPanel >
    <div class="clr" style="margin-bottom:10px;">
        <a id="A1" href="~/report/issuerassessmentusagelanding" runat="server">< Search again</a>
    </div>
    <h2 style="color:#f58025;">Issuer Assessment Usage Report</h2>
    <p>
        <mc:ReportDateRangeDisplay id="ucDateRangeDisplay" runat="server"></mc:ReportDateRangeDisplay>
        <a id="aBack" runat="server">< Back to All Issuer Assessment Usage Report</a>
    </p>
    <p><strong>Issuer Assessment Usage Details Report: </strong> <asp:Label ID="lblReport" runat="server" CssClass="orange"></asp:Label></p>
	<mc:ContentActivityDetailFilterDisplay ID="ucFilterDisplay" runat="server"></mc:ContentActivityDetailFilterDisplay>
    <telerik:RadGrid ID="trgReport" Width="1200" AllowSorting="True" PageSize="15" AllowPaging="True" runat="server" Gridlines="None" AutoGenerateColumns="False" OnNeedDataSource="trgReport_NeedDataSource" AllowFilteringByColumn="true" ShowGroupPanel="true">
        <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
            <CommandItemSettings ShowRefreshButton="" ShowExportToWordButton="true" ShowExportToExcelButton="true"
                ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false"/>
            <Columns>
                <telerik:GridBoundColumn HeaderText="User" DataField="User" FilterControlWidth="50"></telerik:GridBoundColumn>				
                <telerik:GridBoundColumn HeaderText="Audience Segment" DataField="AudienceSegments" FilterControlWidth="50"></telerik:GridBoundColumn>
				<telerik:GridBoundColumn HeaderText="Processor" DataField="Processor" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Issuer" DataField="Issuer" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Date" DataField="Date" DataFormatString="{0:MM/dd/yyyy}" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Item Type" DataField="ItemType" FilterControlWidth="50"></telerik:GridBoundColumn>                
				<telerik:GridTemplateColumn HeaderText="What" HeaderStyle-Width="200" DataField="What" SortExpression="What">
                    <ItemTemplate>
                        <a href='<%# DataBinder.Eval(Container.DataItem,"What") %>' target="_blank"><%# DataBinder.Eval(Container.DataItem,"What") %></a>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
				<telerik:GridTemplateColumn HeaderText="From" HeaderStyle-Width="200" DataField="From" SortExpression="From">
                    <ItemTemplate>
                        <a href='<%# DataBinder.Eval(Container.DataItem,"From") %>' target="_blank"><%# DataBinder.Eval(Container.DataItem,"From") %></a>
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Source" DataField="Source" FilterControlWidth="50"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
            <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
        </ClientSettings>
        <PagerStyle Mode="NextPrevAndNumeric" />
        <HeaderStyle Width="100px" />
        <ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="False" />        
    </telerik:RadGrid>
</asp:Content>