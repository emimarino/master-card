﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="ProcessorActivityReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.ProcessorActivityReport" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
        <div style="background-color: #ffffff; width: 100%; height: 100%; display: flex; justify-content: center; flex-direction: column; align-items: center;">
            <img src="<%= Mastercard.MarketingCenter.Common.Infrastructure.Constants.ImageUrls.ReportsLoader %>" alt="Loading..." />
        </div>
    </telerik:RadAjaxLoadingPanel>
    <div style="margin-top:10px;">
        <a id="A1" href="~/report/processoractivitylanding" runat="server">< Search again</a>
    </div>
    <h2 style="color:#f58025;">FI - User - Logins Report</h2>
    <p>
        <mc:ReportDateRangeDisplay id="ucDateRangeDisplay" runat="server"></mc:ReportDateRangeDisplay>
    </p>
    <p><strong>All Processors Report</strong></p>

    <asp:PlaceHolder ID="phReport" runat="server">
        <telerik:RadGrid ID="trgReport" Width="800" AllowSorting="True" PageSize="20" AllowPaging="True" runat="server" Gridlines="None" AutoGenerateColumns="False" OnNeedDataSource="trgReport_NeedDataSource" AllowFilteringByColumn="true" ShowGroupPanel="true" OnItemCommand="trgReport_ItemCommand">
            <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
                <PagerStyle Mode="NextPrevNumericAndAdvanced" />
                <CommandItemSettings ShowRefreshButton="" ShowExportToWordButton="true" ShowExportToExcelButton="true"
                    ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false"/>
                <Columns>
                    <telerik:GridBoundColumn HeaderText="Processor" DataField="Processor" Visible="false"></telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Processor" HeaderStyle-Width="250" SortExpression="Processor" UniqueName="ProcessorLink" DataField="Processor">
                        <ItemTemplate>
                            <asp:LinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem,"ProcessorId") %>' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"Processor") %>' runat="server"/>
                        </ItemTemplate>
                        <ItemStyle CssClass="rad_grid_hyperlink" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn HeaderText="FIs Added" DataField="FIsAdded" SortExpression="FIsAdded"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="User Registrations" DataField="UserRegistrations" SortExpression="UserRegistrations"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="Active Users*" DataField="ActiveUsers" SortExpression="ActiveUsers"></telerik:GridBoundColumn>
                    <telerik:GridBoundColumn HeaderText="User Logins" DataField="UserLogins" SortExpression="UserLogins"></telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
                <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
            </ClientSettings>
            <PagerStyle Mode="NextPrevAndNumeric" />
            <HeaderStyle Width="100px" />
            <ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
            <GroupingSettings ShowUnGroupButton="true" CaseSensitive="False" />        
        </telerik:RadGrid>
    
        <div style="margin-top:10px;">
            <em>* The "Active Users" column refers to how many unique users have logged in at least once within the time period being reported.</em>
        </div>
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="phNoResults" runat="server" Visible="false">
        <p style="color:#F58025;"><em>No results found in this date range.  Please try your search again, taking note of the effective date for the data available.</em></p>
    </asp:PlaceHolder>

</asp:Content>
