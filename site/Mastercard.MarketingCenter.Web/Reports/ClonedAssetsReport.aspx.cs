﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using Mastercard.MarketingCenter.Data;
using Slam.Cms.Common;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class ClonedAssetsReport : BaseReportPage
    {
        private bool exporting = false;

        protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();
            var results = reportRepository.GetClonedAssetsReport(userContext.SelectedRegion).ToList();
            trgReport.DataSource = results;
            if (!results.Any())
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }
        }

        protected void trgReport_PreRender(object sender, EventArgs e)
        {
            if (exporting)
            {
                foreach (var header in trgReport.MasterTableView.GetItems(GridItemType.Header))
                {
                    foreach (TableCell cell in header.Cells)
                    {
                        cell.Style["text-align"] = "left";
                    }
                }
            }
        }

        protected void trgReport_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName || e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToWordCommandName)
            {
                if (e.CommandName == RadGrid.ExportToPdfCommandName)
                {
                    exporting = true;
                    trgReport.ExportSettings.Pdf.PageHeight = Unit.Parse("162mm");
                    trgReport.ExportSettings.Pdf.PageWidth = Unit.Parse("400mm");
                }
            }
        }
    }
}