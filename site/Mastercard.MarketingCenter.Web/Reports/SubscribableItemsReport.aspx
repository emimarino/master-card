﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="SubscribableItemsReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.SubscribableItemsReport" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
        <div style="background-color: #ffffff; width: 100%; height: 100%; display: flex; justify-content: center; flex-direction: column; align-items: center;">
            <img src="<%= Mastercard.MarketingCenter.Common.Infrastructure.Constants.ImageUrls.ReportsLoader %>" alt="Loading..." />
        </div>
    </telerik:RadAjaxLoadingPanel>
    <div class="clr" style="margin-top: 10px;">
        <a id="A2" href="~/report/subscribableitemslanding" runat="server">< Search again</a>
    </div>
    <h2 style="color: #f58025;">Subscribable Items Report</h2>
    <p><strong>List of Items Available for Subscription and Favoriting</strong></p>
    <p>
        <mc:ReportDateRangeDisplay id="ucDateRangeDisplay" runat="server"></mc:ReportDateRangeDisplay>
    </p>
    <telerik:RadGrid ID="trgReport" Width="1200" AllowSorting="True" PageSize="15" AllowPaging="True" runat="server" GridLines="None" AutoGenerateColumns="False" OnNeedDataSource="trgReport_NeedDataSource" AllowFilteringByColumn="true" ShowGroupPanel="true" OnItemCommand="trgReport_ItemCommand">
        <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
            <CommandItemSettings ShowRefreshButton="" ShowExportToWordButton="true" ShowExportToExcelButton="true"
                ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false" />
            <Columns>
                <telerik:GridBoundColumn HeaderText="Item" DataField="Title" Visible="false"></telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Item" SortExpression="Title" UniqueName="ItemLink" DataField="Title">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"ItemId") %>' CommandName="redirect" Text='<%# DataBinder.Eval(Container.DataItem,"Title") %>' runat="server" />
                    </ItemTemplate>
                    <ItemStyle CssClass="rad_grid_hyperlink" />
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn HeaderText="Type" DataField="Type"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Favorites" DataField="Favorites"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
            <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
        </ClientSettings>
        <PagerStyle Mode="NextPrevAndNumeric" />
        <HeaderStyle Width="100px" />
        <ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="False" />
    </telerik:RadGrid>

    <asp:PlaceHolder ID="phNoResults" runat="server" Visible="false">
        <p style="color: #F58025;"><em>No results found, please try your search again.</em></p>
    </asp:PlaceHolder>
</asp:Content>