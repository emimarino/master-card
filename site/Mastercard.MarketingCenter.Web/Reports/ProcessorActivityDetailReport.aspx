﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="ProcessorActivityDetailReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.ProcessorActivityDetailReport" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
                <div style="background-color:#ffffff; width:100%; height:100%"></div>
    </telerik:RadAjaxLoadingPanel>
    <div style="margin-top:10px;">
        <a id="A1" href="~/report/processoractivitylanding" runat="server">< Search again</a>
    </div>
    <h2 style="color:#f58025;">FI - User - Logins Report</h2>
    <p>
        <mc:ReportDateRangeDisplay id="ucDateRangeDisplay" runat="server"></mc:ReportDateRangeDisplay>
      <a id="aBack" runat="server">< Back to All Processors</a>
    </p>
    <p><strong>FI Details Report by <asp:Label ID="targetLabel" runat="server"></asp:Label></strong> <asp:Label ID="lbrProcessor" runat="server" CssClass="orange"></asp:Label></p>
    <telerik:RadGrid ID="trgReport" Width="800" AllowSorting="True" PageSize="15" AllowPaging="True" runat="server" Gridlines="None" AutoGenerateColumns="False" OnNeedDataSource="trgReport_NeedDataSource" AllowFilteringByColumn="true" ShowGroupPanel="true">
        <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
            <CommandItemSettings ShowRefreshButton="" ShowExportToWordButton="true" ShowExportToExcelButton="true"
                ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false"/>
            <Columns>
                <telerik:GridBoundColumn HeaderText="Issuer" DataField="Issuer" HeaderStyle-Width="250" ></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="User Registrations" DataField="UserRegistrations" ></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Active Users" DataField="ActiveUsers" ></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="User Logins" DataField="UserLogins" ></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
            <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
        </ClientSettings>
        <PagerStyle Mode="NextPrevAndNumeric" />
        <HeaderStyle Width="100px" />
        <ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="False" />        
    </telerik:RadGrid>
    <div class="clr">
        <p><em>PLEASE NOTE:</em> Not shown in the grid above are <strong><asp:Literal ID="litExcludedIssuersCount" runat="server"></asp:Literal></strong> other Financial Institutions <asp:Literal ID="litProcessor" runat="server"></asp:Literal> that have no data for the date period specified.</p>
    </div>
</asp:Content>
