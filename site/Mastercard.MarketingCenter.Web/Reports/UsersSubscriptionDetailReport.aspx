﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="UsersSubscriptionDetailReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.UsersSubscriptionDetailReport" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
        <div style="background-color: #ffffff; width: 100%; height: 100%"></div>
    </telerik:RadAjaxLoadingPanel>
    <div class="clr" style="margin-top: 10px;">
        <a id="A1" href="~/report/userssubscriptionlanding" runat="server">< Search again</a>
    </div>
    <h2 style="color: #f58025;">User Subscriptions and Favorites Report</h2>
    <p>
        <a id="aBack" runat="server">< Back to List of All Active User's Subscriptions and Favorites</a>
    </p>
    <p>
        <strong>Active Subscriptions and Favorites for User: </strong>
        <asp:Label ID="lblUser" runat="server" CssClass="orange"></asp:Label>
    </p>
    <p>
        <mc:ReportDateRangeDisplay id="ucDateRangeDisplay" runat="server"></mc:ReportDateRangeDisplay>
    </p>
    <telerik:RadGrid ID="trgReport" Width="1200" AllowSorting="True" PageSize="15" AllowPaging="True" runat="server" GridLines="None" AutoGenerateColumns="False" OnNeedDataSource="trgReport_NeedDataSource" AllowFilteringByColumn="true" ShowGroupPanel="true">
        <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
            <CommandItemSettings ShowRefreshButton="" ShowExportToWordButton="true" ShowExportToExcelButton="true"
                ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false" />
            <Columns>
                <telerik:GridBoundColumn HeaderText="Name" DataField="UserFullName"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Email" DataField="Email"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Items" DataField="Title"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Type" DataField="Type"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Favorite Date" DataField="SavedDate" DataFormatString="{0:MM/dd/yyyy}"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
            <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
        </ClientSettings>
        <PagerStyle Mode="NextPrevAndNumeric" />
        <HeaderStyle Width="100px" />
        <ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="False" />
    </telerik:RadGrid>
</asp:Content>