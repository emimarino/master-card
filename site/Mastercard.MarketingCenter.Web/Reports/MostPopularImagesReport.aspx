﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/Reports.Master" AutoEventWireup="true" CodeBehind="MostPopularImagesReport.aspx.cs" Inherits="Mastercard.MarketingCenter.Web.Reports.MostPopularImagesReport" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="trgReport">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="trgReport" LoadingPanelID="ralp" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="ralp" runat="server" InitialDelayTime="0" MinDisplayTime="1000" Transparency="25">
        <div style="background-color: #ffffff; width: 100%; height: 100%"></div>
    </telerik:RadAjaxLoadingPanel>
    <div class="clr" style="margin-bottom: 10px;">
        <h2 style="color: #f58025;">Most Popular Images Report</h2>
        <asp:LinkButton ID="lnkClearFilters" runat="server" Text="Back to start" OnClick="lnkClearFilters_Click"></asp:LinkButton>
    </div>
    <telerik:RadGrid ID="trgReport" Width="1200" AllowSorting="True" PageSize="15" AllowPaging="True" runat="server" GridLines="None" AutoGenerateColumns="False" OnNeedDataSource="trgReport_NeedDataSource" AllowFilteringByColumn="true" ShowGroupPanel="true" OnItemCommand="trgReport_ItemCommand">
        <MasterTableView Width="100%" CommandItemDisplay="Top" Font-Size="8pt" Font-Names="Tahoma" TableLayout="Fixed" ShowGroupFooter="true" AllowFilteringByColumn="true">
            <PagerStyle Mode="NextPrevNumericAndAdvanced" />
            <CommandItemSettings ShowRefreshButton="" ShowExportToWordButton="true" ShowExportToExcelButton="true"
                ShowExportToCsvButton="true" ShowExportToPdfButton="true" ShowAddNewRecordButton="false" />
            <Columns>
                <telerik:GridBoundColumn HeaderText="Content Item Id" DataField="ContentItemId" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Content Type" DataField="ContentType" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Title" DataField="Title" FilterControlWidth="150" HeaderStyle-Width="200"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Description" DataField="Description" FilterControlWidth="350" HeaderStyle-Width="400"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Visits" DataField="Visits" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Image Type" DataField="ImageType" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridImageColumn HeaderText="Image" DataImageUrlFields="ImageUrl" FilterControlWidth="200" HeaderStyle-Width="300" ImageWidth="250" ImageHeight="100"></telerik:GridImageColumn>
                <telerik:GridBoundColumn HeaderText="Image Width" DataField="ImageWidth" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Image Height" DataField="ImageHeight" FilterControlWidth="50"></telerik:GridBoundColumn>
                <telerik:GridBoundColumn HeaderText="Right Resolution" DataField="HasRightResolution" FilterControlWidth="50"></telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="true" ReorderColumnsOnClient="true" AllowDragToGroup="True">
            <Resizing AllowRowResize="True" EnableRealTimeResize="True" ResizeGridOnColumnResize="True" AllowColumnResize="True"></Resizing>
        </ClientSettings>
        <PagerStyle Mode="NextPrevAndNumeric" />
        <HeaderStyle Width="100px" />
        <ExportSettings ExportOnlyData="true" IgnorePaging="true"></ExportSettings>
        <GroupingSettings ShowUnGroupButton="true" CaseSensitive="false" />
    </telerik:RadGrid>
    <asp:PlaceHolder ID="phNoResults" runat="server" Visible="false">
        <p><em>There are no most popular images.</em></p>
    </asp:PlaceHolder>
</asp:Content>
