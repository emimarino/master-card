﻿using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class TriggerMarketingNotificationsReport : BaseReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var reportService = DependencyResolver.Current.GetService<IReportService>();
            var results = reportService.GetTriggerMarketingNotificationsReport(StartDate, EndDate, userContext.SelectedRegion).ToList();
            trgReport.DataSource = results;
            if (!results.Any())
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }
        }

        protected void trgReport_ItemCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
        {
            if (e.CommandName == "redirect")
            {
                Response.Redirect($"~/report/triggermarketingnotificationsdetail/{e.CommandArgument}&sd={StartDate.ToString("MM/dd/yyyy")}&ed={EndDate.ToString("MM/dd/yyyy")}");
            }

            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName || e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToWordCommandName)
            {
                trgReport.MasterTableView.GetColumn("NotificationDate").Visible = true;
                trgReport.MasterTableView.GetColumn("NotificationDateLink").Visible = false;
            }
        }
    }
}