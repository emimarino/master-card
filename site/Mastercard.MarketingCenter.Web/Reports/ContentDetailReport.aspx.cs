﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class ContentDetailReport : BaseReportPage
    {
        private IContentItemService _contentItemService = DependencyResolver.Current.GetService<IContentItemService>();
        private IUrlService _urlService = DependencyResolver.Current.GetService<IUrlService>();
        private string selectedRegion { get; set; }
        private string selectedCreatedStartDate { get; set; }
        private string selectedCreatedEndDate { get; set; }
        private string selectedKeyword { get; set; }
        protected string baseFrontendUrl { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            baseFrontendUrl = _urlService.GetFrontEndHomeURL();

            if (!IsPostBack)
            {
                aBack.HRef = _urlService.GetContentReportLink();
            }

            selectedRegion = Request.QueryString.Get("region");
            selectedCreatedStartDate = Request.QueryString.Get("creationStartDate");
            selectedCreatedEndDate = Request.QueryString.Get("creationEndDate");
            selectedKeyword = Request.QueryString.Get("keyword");

            if (!user.IsInPermission(Constants.Permissions.CanViewAllRegionContentReport) && !userContext.SelectedRegion.Equals(selectedRegion))
            {
                Response.Redirect(_urlService.GetUnauthorizedLink());
            }
        }

        protected void trgReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            IEnumerable<ContentReportDTO> contents;
            DateTime.TryParse(selectedCreatedStartDate, out DateTime createdStartDate);
            DateTime.TryParse(selectedCreatedEndDate, out DateTime createdEndDate);

            if (!selectedRegion.Equals("all"))
            {
                contents = _contentItemService.GetContents(createdStartDate, createdEndDate, selectedKeyword, selectedRegion).ToList();
            }
            else
            {
                contents = _contentItemService.GetContents(createdStartDate, createdEndDate, selectedKeyword).ToList();
            }

            trgReport.DataSource = contents.ToList();

            if (!contents.Any())
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }
        }
    }
}