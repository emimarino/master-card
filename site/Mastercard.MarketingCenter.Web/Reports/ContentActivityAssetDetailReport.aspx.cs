﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Services;
using Slam.Cms.Data;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class ContentActivityAssetDetailReport : BaseReportPage
    {
        private bool exporting = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var id = Page.RouteData.Values["id"].ToString();
                var slamContext = DependencyResolver.Current.GetService<SlamContext>();

                if (id.ToLower().Contains("all"))
                {
                    trgReport.Columns.FindByUniqueName("TitleDiscriminator").Visible = true;
                    lblContentDetails.Text = id.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
                }
                else
                {
                    var contentItem = slamContext.CreateQuery().FilterContentItemId(id).Get().FirstOrDefault();

                    var asset = contentItem as AssetDTO;
                    if (asset != null)
                        lblContentDetails.Text = asset.Title;

                    var assetFullWidth = contentItem as AssetFullWidthDTO;
                    if (assetFullWidth != null)
                        lblContentDetails.Text = assetFullWidth.Title;

                    var downloadableAsset = contentItem as DownloadableAssetDTO;
                    if (downloadableAsset != null)
                        lblContentDetails.Text = downloadableAsset.Title;

                    var orderableAsset = contentItem as OrderableAssetDTO;
                    if (orderableAsset != null)
                        lblContentDetails.Text = orderableAsset.Title;

                    var program = contentItem as ProgramDTO;
                    if (program != null)
                        lblContentDetails.Text = program.Title;

                    var webinar = contentItem as WebinarDTO;
                    if (webinar != null)
                        lblContentDetails.Text = webinar.Title;

                    var page = contentItem as PageDTO;
                    if (page != null)
                        lblContentDetails.Text = page.Title;
                }

                string reportType;
                string reportTypeUrl = GetReportTypeUrl(ReportType);
                switch (ReportType)
                {
                    case 0:
                        reportType = "Page";
                        break;
                    case 1:
                        reportType = "Program";
                        break;
                    case 2:
                        reportType = "Asset";
                        break;
                    case 4:
                        reportType = "Webinar";
                        break;
                    case 5:
                        reportType = "Offer";
                        break;
                    default:
                        var tagRepository = DependencyResolver.Current.GetService<TagRepository>();
                        if (!id.ToLower().Contains("all"))
                        {
                            var tag =
                            tagRepository.GetTags().FirstOrDefault(
                                t => t.TagID == id);
                            lblContentDetails.Text = tag.DisplayName;
                        }
                        reportType = "Tag";
                        break;
                }
                trgReport.Columns.FindByUniqueName("TitleDiscriminator").HeaderText += reportType ?? "item";
                ltBackAssetType.Text = reportType + "s";
                ltAssetType.Text = reportType;
                aBack.HRef = $"~/report/{reportTypeUrl}?sd={StartDate.ToString("MM/dd/yyyy")}&ed={EndDate.ToString("MM/dd/yyyy")}&fmp={FilterMasterCardUsers}&fvp={FilterVendorProcessor}&audience={string.Join(";", AudienceSegmentationIds)}&type={ReportType}";

                if (User.IsInPermission(Constants.Permissions.CanChangeSegments))
                {
                    trgReport.MasterTableView.GetColumn("AudienceSegments").Display = true;
                }
                else
                {
                    trgReport.MasterTableView.GetColumn("AudienceSegments").Display = false;
                }

                ucFilterDisplay.Setup(segmentationService.GetSegmentationsByRegion(userContext.SelectedRegion), AudienceSegmentationIds, FilterMasterCardUsers, FilterVendorProcessor);
            }
        }

        protected void trgReport_PreRender(object sender, EventArgs e)
        {
            if (exporting)
            {
                foreach (var header in trgReport.MasterTableView.GetItems(GridItemType.Header))
                {
                    foreach (TableCell cell in header.Cells)
                    {
                        cell.Style["text-align"] = "left";
                    }
                }
            }
        }

        protected void trgReport_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.ExportToPdfCommandName)
            {
                exporting = true;
                trgReport.ExportSettings.Pdf.PageHeight = Unit.Parse("162mm");
                trgReport.ExportSettings.Pdf.PageWidth = Unit.Parse("400mm");
            }
        }

        protected void trgReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();
            switch (ReportType)
            {
                case 2: // assets
                    trgReport.DataSource =
                    reportRepository.GetContentActivityAssetDetailReport(Page.RouteData.Values["id"].ToString(),
                                                                         StartDate,
                                                                         EndDate,
                                                                         GetAudienceSegmentationIdsForReport(),
                                                                         FilterMasterCardUsers.Value,
                                                                         FilterVendorProcessor.Value,
                                                                         userContext.SelectedRegion).ToList();
                    break;
                case 3: // tags
                    trgReport.DataSource =
                    reportRepository.GetContentActivityTagDetailReport(Page.RouteData.Values["id"].ToString(),
                                                                         StartDate,
                                                                         EndDate,
                                                                         GetAudienceSegmentationIdsForReport(),
                                                                         FilterMasterCardUsers.Value,
                                                                         FilterVendorProcessor.Value,
                                                                         userContext.SelectedRegion).ToList();
                    break;                                                        
                default:
                    trgReport.DataSource =
                    reportRepository.GetContentActivityDetailReport(Page.RouteData.Values["id"].ToString(),
                                                                         StartDate,
                                                                         EndDate,
                                                                         GetAudienceSegmentationIdsForReport(),
                                                                         FilterMasterCardUsers.Value,
                                                                         FilterVendorProcessor.Value,
                                                                         userContext.SelectedRegion).ToList();
                    break;
            }
        }
    }
}