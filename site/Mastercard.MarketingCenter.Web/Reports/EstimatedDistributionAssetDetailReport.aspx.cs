﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Services;
using Slam.Cms.Data;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class EstimatedDistributionAssetDetailReport : BaseReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var id = Page.RouteData.Values["id"].ToString();
                var slamContext = DependencyResolver.Current.GetService<SlamContext>();
                var contentItem = slamContext.CreateQuery().FilterContentItemId(id).FilterStatus(FilterStatus.None).Get().FirstOrDefault();

                var downloadableAsset = contentItem as DownloadableAssetDTO;
                if (downloadableAsset != null)
                {
                    lblContentDetails.Text = downloadableAsset.Title;
                }

                aBack.HRef = $"~/report/estimateddistributionasset?sd={StartDate.ToString("MM/dd/yyyy")}&ed={EndDate.ToString("MM/dd/yyyy")}&fmp={FilterMasterCardUsers}&audience={string.Join(";", AudienceSegmentationIds)}&type=0";

                if (User.IsInPermission(Constants.Permissions.CanChangeSegments))
                {
                    trgReport.MasterTableView.GetColumn("AudienceSegments").Display = true;
                }
                else
                {
                    trgReport.MasterTableView.GetColumn("AudienceSegments").Display = false;
                }

                ucFilterDisplay.Setup(segmentationService.GetSegmentationsByRegion(userContext.SelectedRegion), AudienceSegmentationIds, FilterMasterCardUsers, FilterVendorProcessor);
            }
        }

        protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();
            var results = reportRepository.GetEstimatedDistributionAssetDetailReport(StartDate, EndDate, GetAudienceSegmentationIdsForReport(), Page.RouteData.Values["id"].ToString(), FilterMasterCardUsers.Value).ToList();
            foreach (var result in results)
            {
                result.LastRequestDate = result.LastRequestDate?.Date;
                result.MostRecentDownload = result.MostRecentDownload?.Date;
                if (result.Issuer != null)
                {
                    result.IssuerName = result.Issuer.Title;
                }
                if (result.Segmentations.Any())
                {
                    result.AudienceSegments = string.Join(", ", result.Segmentations.Select(s => s.SegmentationName));
                }
            }

            trgReport.DataSource = results;
        }
    }
}