﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Interfaces;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Models.Report;
using Slam.Cms.Data;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class IssuerSegmentationDetailReport : BaseReportPage
    {
        protected SlamContext SlamContext
        {
            get
            {
                return DependencyResolver.Current.GetService<SlamContext>();
            }
        }

        protected ISegmentationService SegmentationServices
        {
            get
            {
                return DependencyResolver.Current.GetService<ISegmentationService>();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                aBack.HRef = "~/report/issuersegmentation?{0}".F(Request.QueryString);

                var segmentationId = Page.RouteData.Values["segmentationId"].ToString();
                var segmentations = SegmentationServices.GetSegmentationsByRegion(userContext.SelectedRegion);
                var segmentation = segmentations.FirstOrDefault(s => s.SegmentationId == segmentationId);
                lblSegment.Text = segmentation?.SegmentationName;
            }
        }

        protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var segmentationIds = Page.RouteData.Values["segmentationId"].ToString().Split().ToList();
            var data = SlamContext.CreateQuery()
                .FilterSegmentedWith(segmentationIds, false)
                .Get();

            var models = data.Select(x => new IssuerSegmentationDetailReportModel
                {
                    ContentItemId = x.ContentItemId,
                    ContentName = (x is IContentItemWithTitle)? ((IContentItemWithTitle)x).Title : null
                });

            trgReport.DataSource = models;
        }
    }
}