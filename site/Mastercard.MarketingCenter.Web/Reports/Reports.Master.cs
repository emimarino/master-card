﻿using Mastercard.MarketingCenter.Services;
using System;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class Reports : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegionalizeService.SetCurrentCulture(RegionalizeService.DefaultRegion, RegionalizeService.DefaultLanguage); //always default to English-US
        }
    }
}