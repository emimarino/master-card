﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data.Entities.Reporting;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class UserRegistrationReport : BaseReportPage
    {
        UserContext uc
        {
            get
            {
                return DependencyResolver.Current.GetService<UserContext>();
            }
        }

        IReportService ReportService
        {
            get
            {
                return DependencyResolver.Current.GetService<IReportService>();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void trgReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var results = ReportService.GetUserRegistrationReport(StartDate, EndDate, uc.SelectedRegion, false, !FilterMasterCardUsers.Value, !FilterExpiredUsers.Value).ToList();

            if (results.Any())
            {
                trgReport.DataSource = results;
                if (User.IsInPermission(Constants.Permissions.CanChangeSegments))
                {
                    trgReport.MasterTableView.GetColumn("AudienceSegments").Display = true;
                }
                else
                {
                    trgReport.MasterTableView.GetColumn("AudienceSegments").Display = false;
                }
            }
            else
            {
                trgReport.Visible = false;
                phNoResults.Visible = true;
            }
        }

        protected void trgReport_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item != null && e.Item.DataItem != null && (e.Item.ItemType == GridItemType.Item || e.Item.ItemType == GridItemType.AlternatingItem))
            {
                var dataItem = (UserRegistrationReportResultItem)e.Item.DataItem;
                if (dataItem.IsDisabled)
                {
                    var linkButton1 = e.Item.FindControl("LinkButton1") as LinkButton;
                    linkButton1.Visible = false;
                    linkButton1.Parent.Controls.Add(new Literal() { Text = dataItem.Email });
                }
            }
        }

        protected void trgReport_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "redirect")
            {
                Response.Redirect("~/WebFormsPages/UserProfileNoChrome.aspx?loginid={0}&{1}".F(e.CommandArgument, Request.QueryString));
            }

            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName || e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToWordCommandName)
            {
                trgReport.MasterTableView.GetColumn("Email").Visible = true;
                trgReport.MasterTableView.GetColumn("EmailLink").Visible = false;
            }
        }
    }
}