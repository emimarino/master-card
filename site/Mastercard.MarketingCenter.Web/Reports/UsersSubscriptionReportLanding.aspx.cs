﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class UsersSubscriptionReportLanding : System.Web.UI.Page
    {
        public ProcessorService _processorService { get { return DependencyResolver.Current.GetService<ProcessorService>(); } }
        public IIssuerService _issuerService { get { return DependencyResolver.Current.GetService<IIssuerService>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var userContext = DependencyResolver.Current.GetService<UserContext>();
                var processors = _processorService.GetProcessors();
                var mastercardIssuers = _issuerService.GetMastercardIssuersByRegion(userContext.SelectedRegion);
                chkMasterCard.Visible = mastercardIssuers.Any();
                chkVendor.Visible = processors.Any(
                    p => p.ProcessorId == MarketingCenterDbConstants.Processors.APlus
                    && p.Issuers.Any(ip => ip.RegionId == userContext.SelectedRegion));
                optionalSection.Visible = chkMasterCard.Visible || chkVendor.Visible;
            }

            btnValidateCode.OnClientClick = (Master as ReportLanding)?.GetLoader();
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            if (ucDateRangeSelector.Validate())
            {
                Response.Redirect($"~/report/userssubscription?sd={ucDateRangeSelector.StartDate.Value.ToString("MM/dd/yyyy")}&ed={ucDateRangeSelector.EndDate.Value.ToString("MM/dd/yyyy")}&fmp={!chkMasterCard.Checked}&fvp={!chkVendor.Checked}");
            }
        }
    }
}