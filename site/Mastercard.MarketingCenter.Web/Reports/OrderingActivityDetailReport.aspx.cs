﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Common;
using System;
using System.Linq;
using System.Web.Mvc;
using Telerik.Web.UI;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class OrderingActivityDetailReport : BaseReportPage
    {
        private IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }
        private IContentItemService _contentItemService { get { return DependencyResolver.Current.GetService<IContentItemService>(); } }
        private ReportRepository _reportRepository { get { return DependencyResolver.Current.GetService<ReportRepository>(); } }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var id = Page.RouteData.Values["id"].ToString().ToLower();
                if (id.Equals("all"))
                {
                    lblProgram.Text = "[All]";
                }
                else if (id.Equals("0"))
                {
                    lblProgram.Text = "[non-Program assets ordering]";
                }
                else
                {
                    var program = _contentItemService.GetProgram(id);
                    lblProgram.Text = program?.Title ?? string.Empty;
                }

                aBack.HRef = _urlService.GetOrderingActivityProgramsReportURL(Request.QueryString.ToString());
            }
        }

        protected void trgReport_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            var id = Page.RouteData.Values["id"].ToString().ToLower();
            if (id.Equals("0"))
            {
                trgReport.DataSource = _reportRepository.GetOrderingActivityForNonProgramDetailReport(StartDate, EndDate).OrderBy(o => o.Date).ToList();
            }
            else if (id.Equals("all"))
            {
                var nonPrograms = _reportRepository.GetOrderingActivityForNonProgramDetailReport(StartDate, EndDate).OrderBy(o => o.Date).ToList();
                var allPrograms = _reportRepository.GetOrderingActivityForProgramDetailReport(id, StartDate, EndDate).OrderBy(o => o.Date).ToList();
                allPrograms.AddRange(nonPrograms);
                trgReport.DataSource = allPrograms;
                trgReport.Columns.FindByUniqueName("RelatedProgramsDiscriminator").Visible = true;
            }
            else
            {
                trgReport.DataSource = _reportRepository.GetOrderingActivityForProgramDetailReport(id, StartDate, EndDate).OrderBy(o => o.Date).ToList();
            }
        }

        protected void trgReport_ItemCommand(object source, GridCommandEventArgs e)
        {
            if (e.CommandName == "redirect")
            {
                string referrer = $"&ReturnUrl={Server.UrlEncode(_urlService.GetOrderingActivityDetailProgramReportURL(Page.RouteData.Values["id"].ToString(), Request.QueryString.ToString()))}";
                Response.Redirect(_urlService.GetOrderDetailsReportURL(e.CommandArgument.ToString(), referrer));
            }

            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToPdfCommandName || e.CommandName == RadGrid.ExportToCsvCommandName || e.CommandName == RadGrid.ExportToWordCommandName)
            {
                trgReport.MasterTableView.GetColumn("OrderNumber").Visible = true;
                trgReport.MasterTableView.GetColumn("OrderNumberLink").Visible = false;
            }
        }
    }
}