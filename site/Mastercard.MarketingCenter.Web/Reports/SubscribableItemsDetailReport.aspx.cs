﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Services;
using Slam.Cms.Data;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class SubscribableItemsDetailReport : BaseReportPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var itemId = Page.RouteData.Values["itemId"].ToString();
                if (!itemId.IsNullOrEmpty())
                {
                    string itemType = string.Empty;
                    string itemTitle = string.Empty;
                    var reportService = DependencyResolver.Current.GetService<ReportService>();
                    
                    if (itemId.Equals("All-Items"))
                    {
                        var allTypes = reportService.GetSubscribableItemsReport(StartDate, EndDate, userContext.SelectedRegion)
                            .Where(x => x.Favorites > 0)
                            .Select(x => x.Type).ToList().Distinct();
                        itemType = string.Join(", ", allTypes);
                        itemTitle = "All";
                    }                   
                    else
                    {
                        reportService.PopulateSubscribableItemsDetailReportHeader(itemId, out itemType, out itemTitle);
                    }                 
                    ltItemType.Text = itemType;
                    lblItemTitle.Text = itemTitle;
                }

                aBack.HRef = $"~/report/subscribableitems?{Request.QueryString}";
            }
        }
        
        protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var itemId = Page.RouteData.Values["itemId"].ToString();
            if (!itemId.IsNullOrEmpty())
            {
                var reportService = DependencyResolver.Current.GetService<IReportService>();
                if (itemId.Equals("All-Items"))
                {                    
                    trgReport.DataSource = reportService.GetSubscribableItemsListDetailReport(StartDate, EndDate, userContext.SelectedRegion).ToList();                    
                }
                else
                {                    
                    trgReport.DataSource = reportService.GetSubscribableItemsDetailReport(StartDate, EndDate, itemId, userContext.SelectedRegion).ToList();
                    trgReport.MasterTableView.GetColumn("ContentItemName").Display = false;
                }
            }
        }
    }
}