﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Slam.Cms.Data;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.Reports
{
    public partial class EstimatedDistributionIssuerDetailReport : BaseReportPage
    {
        private SlamContext _slamContext;
        protected void Page_Load(object sender, EventArgs e)
        {
            _slamContext = DependencyResolver.Current.GetService<SlamContext>();
            if (!IsPostBack)
            {
                aBack.HRef = $"~/report/estimateddistributionissuer?sd={StartDate.ToString("MM/dd/yyyy")}&ed={EndDate.ToString("MM/dd/yyyy")}&fmp={FilterMasterCardUsers}&audience={string.Join(";", AudienceSegmentationIds)}&type=2";

                ucFilterDisplay.Setup(segmentationService.GetSegmentationsByRegion(userContext.SelectedRegion), AudienceSegmentationIds, FilterMasterCardUsers, FilterVendorProcessor);
            }
        }

        protected void trgReport_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            var reportRepository = DependencyResolver.Current.GetService<ReportRepository>();
            var results = reportRepository.GetEstimatedDistributionIssuerDetailReport(StartDate, EndDate, GetAudienceSegmentationIdsForReport(), Page.RouteData.Values["id"].ToString(), FilterMasterCardUsers.Value).ToList();
            var items = _slamContext.CreateQuery().FilterContentTypes<DownloadableAssetDTO, ProgramDTO>().Get();
            results = results.Where(r => items.Any(d => d.PrimaryContentItemId == r.ContentItemId && d.StatusID >= 3)).ToList();
            foreach (var result in results)
            {
                result.MostRecentDownload = result.MostRecentDownload?.Date;
                result.LastRequestDate = result.LastRequestDate?.Date;
                var download = items.FirstOrDefault(d => d.PrimaryContentItemId == result.ContentItemId);
                if (download != null && download is DownloadableAssetDTO)
                {
                    result.AssetTitle = ((DownloadableAssetDTO)download).Title;
                }
                if (result.RelatedProgram != null)
                {
                    var program = items.FirstOrDefault(d => d.ContentItemId == result.RelatedProgram.ProgramContentItemId);
                    if (program != null && program is ProgramDTO)
                    {
                        result.Program = ((ProgramDTO)program).Title;
                    }
                }
                if (result.Issuer != null)
                {
                    lblContentDetails.Text = result.Issuer.Title;
                }
            }

            trgReport.DataSource = results;
        }
    }
}