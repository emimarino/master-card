﻿█████████████████████████████████████████████████████████████████████████████████████████████████████
[#SiteName]	
 
Create your password
 
 
Dear [#FirstName],
 
Thank you for registering with the Mastercard Marketing Center.
 
To finalize your account, please click on the link below to confirm your account
details and create a password.
 
[#TargetUrl]
 
Thank You.
 	
 
Mastercard is a registered trademark, and the circles design is a trademark of Mastercard International
Incorporated.
 
©[#CurrentYear] Mastercard. All rights reserved. Mastercard.
 