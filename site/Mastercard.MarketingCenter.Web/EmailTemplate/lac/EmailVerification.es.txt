﻿█████████████████████████████████████████████████████████████████████████████████████████████████████
[#SiteName]
 
Cree su contraseña
 
 
Estimado/a [#FirstName],
 
Gracias por registrarse en el Centro de Marketing de Mastercard.
 
Para completar su cuenta, haga clic en el enlace a continuación para
confirmar los detalles de su cuenta y crear una contraseña.
 
[#TargetUrl]
 
Gracias.
 	
 
Mastercard is a registered trademark, and the circles design is a trademark of Mastercard International
Incorporated.
 
©[#CurrentYear] Mastercard. All rights reserved. Mastercard.