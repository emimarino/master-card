﻿using Mastercard.MarketingCenter.Common.Extensions;
using Slam.Cms.Common;
using Slam.Cms.Data;
using StackExchange.Profiling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Core.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class CacheInvalidatorFilterAttribute : ActionFilterAttribute
    {
        private readonly string identifier = Guid.NewGuid().ToString("n");

        public CacheInvalidatorFilterAttribute()
        {
            IgnoreControllers = new string[] { };
        }

        public string[] IgnoreControllers { get; set; }
        public string SelectedRegion { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.IsChildAction || filterContext.ActionDescriptor.ControllerDescriptor.ControllerName.In(IgnoreControllers))
            {
                return;
            }

            using (MiniProfiler.Current.Step("CacheInvalidator"))
            {
                CheckCacheInvalidation(filterContext.Controller.ViewBag.SelectedRegion.ToString());
            }

            base.OnActionExecuting(filterContext);
        }

        protected void CheckCacheInvalidation(string selectedRegion)
        {
            var sqlCacheRepository = DependencyResolver.Current.GetService<ISqlCacheRepository>();

            var cacheSignals = sqlCacheRepository.GetAllInvalidators();
            if (!cacheSignals.Any())
            {
                return;
            }

            var invalidatedIds = new List<string>();
            foreach (var cacheSignal in cacheSignals)
            {
                if (cacheSignal.Id.IsNullOrWhiteSpace())
                {
                    continue;
                }

                bool isRegionTag = cacheSignal.Id.StartsWith("RegionTag_", StringComparison.InvariantCultureIgnoreCase);
                bool isMostPopularActivity = cacheSignal.Id.StartsWith("MostPopularActivity_", StringComparison.InvariantCultureIgnoreCase);

                if (!(isRegionTag || isMostPopularActivity) ||
                    cacheSignal.Id.Equals($"RegionTag_{selectedRegion}", StringComparison.InvariantCultureIgnoreCase) ||
                    cacheSignal.Id.Equals($"MostPopularActivity_{selectedRegion}", StringComparison.InvariantCultureIgnoreCase))
                {
                    Invalidate(cacheSignal.Id, isRegionTag, isMostPopularActivity);

                    invalidatedIds.Add(cacheSignal.Id);
                }
            }

            if (invalidatedIds.Any())
            {
                sqlCacheRepository.MarkInvalidated(invalidatedIds);
            }
        }

        public void Invalidate(string id, bool isRegionTag, bool isMostPopularActivity)
        {
            var slamContext = DependencyResolver.Current.GetService<SlamContext>();

            if (isRegionTag)
            {
                slamContext.InvalidateAllTagsWithType();
            }
            else if (isMostPopularActivity)
            {
                var cachingService = DependencyResolver.Current.GetService<ICachingService>();
                cachingService.Delete(id);
            }
            else
            {
                switch (id)
                {
                    case "tag":
                        slamContext.InvalidateTags();
                        break;
                    case "user":
                        slamContext.InvalidateUsers();
                        break;
                }
            }
        }
    }
}