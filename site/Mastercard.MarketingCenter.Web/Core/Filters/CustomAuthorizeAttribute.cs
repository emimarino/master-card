﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Web.Core.Services;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Mastercard.MarketingCenter.Web.Core.Filters
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        // For some reason Autofac is not resolving these dependencies. Probably because its an old version
        public AuthorizationService AuthorizationService
        {
            get
            {
                return DependencyResolver.Current.GetService<AuthorizationService>();
            }
        }

        public MarketingCenterDbContext Context
        {
            get
            {
                return DependencyResolver.Current.GetService<MarketingCenterDbContext>();
            }
        }

        private string _accessRoleName;

        public CustomAuthorizeAttribute() : base()
        {
        }

        public CustomAuthorizeAttribute(string accessRoleName) : base()
        {
            _accessRoleName = accessRoleName;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var authorized = false;

            if (!base.AuthorizeCore(httpContext))
            {
                return authorized;
            }

            if (httpContext == null || httpContext.User == null || httpContext.User.Identity == null || httpContext.User.Identity.Name == null ||
                httpContext.Request == null || httpContext.Request.RequestContext == null || httpContext.Request.RequestContext.RouteData == null)
            {
                return authorized;
            }

            authorized = AuthorizationService.Authorize(httpContext.Request.RequestContext.RouteData);

            if (!string.IsNullOrEmpty(_accessRoleName))
            {
                authorized = authorized && httpContext.User.IsInRole(_accessRoleName);
            }

            return authorized;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
            else if (filterContext.HttpContext.Session["ChangedRegion"] != null) //ChangedRegion is only set by the ChangeRegion action meaning the admin's region was just changed
            {
                filterContext.HttpContext.Session.Remove("ChangedRegion");
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    { "action", "Index" },
                    { "controller", "Home" }
                });
            }
            else
            {
                filterContext.Result = new HttpStatusCodeResult(404);
            }
        }
    }
}