﻿using System;
using System.Web.Mvc;
using Mastercard.MarketingCenter.Web.Core.Services;
using StackExchange.Profiling;
using System.Linq;

namespace Mastercard.MarketingCenter.Web.Core.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class PageTrackingAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            using (MiniProfiler.Current.Step("PageTrackingAttribute"))
            {
                if (!filterContext.IsChildAction && !filterContext.ActionDescriptor.GetCustomAttributes(typeof(IgnorePageTrackingAttribute), true).Any())
                {
                    var pageTrackingService = DependencyResolver.Current.GetService<IPageTrackingService>();
                    pageTrackingService.TrackPageAccess();
                }

                base.OnActionExecuting(filterContext);
            }
        }
    }

    public class IgnorePageTrackingAttribute : ActionFilterAttribute
    {
    }
}