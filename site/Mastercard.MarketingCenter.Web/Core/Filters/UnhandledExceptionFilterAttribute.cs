﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Exceptions;
using Pulsus;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Core.Filters
{
    internal class UnhandledExceptionFilterAttribute : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception is NotFoundException)
            {
                LogManager.EventFactory.Create()
                                       .Level(LoggingEventLevel.Information)
                                       .AddTags(Common.Infrastructure.Constants.ErrorCodes.NotFound)
                                       .Text("Not Found")
                                       .AddData("Exception Message", filterContext.Exception.Message)
                                       .AddData("Exception InnerException", filterContext.Exception.InnerException)
                                       .AddData("Exception Data", filterContext.Exception.Data)
                                       .AddData("Exception StackTrace", filterContext.Exception.StackTrace)
                                       .Push();

                // 404
                UrlService.RedirectTo(filterContext, "Error", "Error404");
            }
            else if (filterContext.Exception is AuthorizationException)
            {
                LogManager.EventFactory.Create()
                                       .Level(LoggingEventLevel.Information)
                                       .AddTags(Common.Infrastructure.Constants.ErrorCodes.AccessDenied)
                                       .Text("Access Denied")
                                       .AddData("Exception Message", filterContext.Exception.Message)
                                       .AddData("Exception InnerException", filterContext.Exception.InnerException)
                                       .AddData("Exception Data", filterContext.Exception.Data)
                                       .AddData("Exception StackTrace", filterContext.Exception.StackTrace)
                                       .Push();

                // 403
                // When handling 403 we should not redirect so that the URL does not change. Only set the status code
                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.StatusCode = 403;
            }
            else if (filterContext.Exception is ProgramExpiredException)
            {
                LogManager.EventFactory.Create()
                                       .Level(LoggingEventLevel.Information)
                                       .AddTags(Common.Infrastructure.Constants.ErrorCodes.ProgramExpired)
                                       .Text("Program Expired")
                                       .AddData("Exception Message", filterContext.Exception.Message)
                                       .AddData("Exception InnerException", filterContext.Exception.InnerException)
                                       .AddData("Exception Data", filterContext.Exception.Data)
                                       .AddData("Exception StackTrace", filterContext.Exception.StackTrace)
                                       .Push();

                UrlService.RedirectTo(filterContext, "Error", Common.Infrastructure.Constants.ErrorCodes.ProgramExpired);
            }
            else
            {
                LogManager.EventFactory.Create()
                                       .Level(LoggingEventLevel.Error)
                                       .AddTags("error unhandled exception")
                                       .Text("Unhandled exception error in Portal")
                                       .AddData("Exception Message", filterContext.Exception.Message)
                                       .AddData("Exception InnerException", filterContext.Exception.InnerException)
                                       .AddData("Exception Data", filterContext.Exception.Data)
                                       .AddData("Exception StackTrace", filterContext.Exception.StackTrace)
                                       .Push();

                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.StatusCode = 500;
                filterContext.Result = new RedirectResult("/portal/error");
            }
        }
    }
}