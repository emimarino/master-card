﻿using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Web.Core.Modules;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public abstract class BaseTabPageViewModel
    {
        private readonly SlamContext slamContext;
        private readonly UserContext userContext;

        private PageDTO _page;

        public PageDTO Page
        {
            get { return _page; }
            set
            {
                if (value == null)
                {
                    _page = new PageDTO() { Uri = string.Empty, Tags = new List<Slam.Cms.Data.Tag>(new Slam.Cms.Data.Tag[] { new Slam.Cms.Data.Tag { Identifier = userContext.SelectedLanguage } }) };
                    _pageIsPlaceholder = true;
                }
                else
                {
                    _page = value;
                }
            }
        }

        private bool _pageIsPlaceholder = false;
        public bool PageIsPlaceholder { get { return _pageIsPlaceholder; } }

        public CarouselSlideViewModel CarouselSlides { get; set; }
        public NarrowPanelList InsightsCrossSellBox { get; set; }
        public NarrowPanelList ProductsCrossSellBox { get; set; }
        public NarrowPanelList MarketingCrossSellBox { get; set; }
        public NarrowPanelList SolutionsCrossSellBox { get; set; }
        public NarrowPanelList SafetySecurityCrossSellBox { get; set; }
        public NarrowPanelList SegmentsCrossSellBox { get; set; }

        protected BaseTabPageViewModel()
        {
            slamContext = WebCoreModule.GetService<SlamContext>();
            userContext = WebCoreModule.GetService<UserContext>();
        }

        public void SetupSectionsCrossSellBoxes()
        {
            if (Page != null && !string.IsNullOrWhiteSpace(Page.Uri))
            {
                if (!Page.Uri.StartsWith("products", StringComparison.InvariantCultureIgnoreCase))
                {
                    ProductsCrossSellBox = new NarrowPanelList()
                    {
                        Title = Resources.Shared.Products,
                        Footer = Resources.Shared.Products,
                        FooterUrl = Constants.Urls.ProductsOverview,
                        RenderThumbnail = true,
                        ContentItemList = GetContentItemListItemsForCrossSellBox(MarketingCenterDbConstants.CrossSellBoxes.Products)
                    };
                }

                if (!Page.Uri.StartsWith("marketing", StringComparison.InvariantCultureIgnoreCase))
                {
                    MarketingCrossSellBox = new NarrowPanelList()
                    {
                        Title = Resources.Shared.Marketing,
                        Footer = Resources.Shared.Marketing,
                        FooterUrl = Constants.Urls.MarketingOverview,
                        RenderThumbnail = true,
                        ContentItemList = GetContentItemListItemsForCrossSellBox(MarketingCenterDbConstants.CrossSellBoxes.Marketing)
                    };
                }

                if (!Page.Uri.StartsWith("insights", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (userContext?.SelectedRegion?.Equals("us", StringComparison.OrdinalIgnoreCase) ?? true)
                    {
                        InsightsCrossSellBox = new NarrowPanelList()
                        {
                            Title = Resources.Shared.ThoughtLeadership,
                            Footer = Resources.Shared.ThoughtLeadership,
                            FooterUrl = Constants.Urls.ThoughtLeadershipOverview,
                            RenderThumbnail = true,
                            ContentItemList = GetContentItemListItemsForCrossSellBox(MarketingCenterDbConstants.CrossSellBoxes.Insights)
                        };
                    }
                    else
                    {
                        InsightsCrossSellBox = new NarrowPanelList()
                        {
                            Title = Resources.Shared.Insights,
                            Footer = Resources.Shared.Insights,
                            FooterUrl = Constants.Urls.InsightsOverview,
                            RenderThumbnail = true,
                            ContentItemList = GetContentItemListItemsForCrossSellBox(MarketingCenterDbConstants.CrossSellBoxes.Insights)
                        };
                    }
                }

                if (!Page.Uri.StartsWith("solutions", StringComparison.InvariantCultureIgnoreCase))
                {
                    SolutionsCrossSellBox = new NarrowPanelList()
                    {
                        Title = Resources.Shared.Solutions,
                        Footer = Resources.Shared.Solutions,
                        FooterUrl = Constants.Urls.SolutionsOverview,
                        RenderThumbnail = true,
                        ContentItemList = GetContentItemListItemsForCrossSellBox(MarketingCenterDbConstants.CrossSellBoxes.Solutions)
                    };
                }

                if (!Page.Uri.StartsWith("safety-security", StringComparison.InvariantCultureIgnoreCase))
                {
                    SafetySecurityCrossSellBox = new NarrowPanelList()
                    {
                        Title = Resources.Shared.SafetySecurity,
                        Footer = Resources.Shared.SafetySecurity,
                        FooterUrl = Constants.Urls.SafetySecurityOverview,
                        RenderThumbnail = true,
                        ContentItemList = GetContentItemListItemsForCrossSellBox(MarketingCenterDbConstants.CrossSellBoxes.SafetySecurity)
                    };
                }

                if (!Page.Uri.StartsWith("segments", StringComparison.InvariantCultureIgnoreCase))
                {
                    SegmentsCrossSellBox = new NarrowPanelList()
                    {
                        Title = Resources.Shared.Segments,
                        Footer = Resources.Shared.Segments,
                        FooterUrl = Constants.Urls.SegmentsOverview,
                        RenderThumbnail = true,
                        ContentItemList = GetContentItemListItemsForCrossSellBox(MarketingCenterDbConstants.CrossSellBoxes.Segments)
                    };
                }
            }
        }

        private IEnumerable<ContentItemListItem> GetContentItemListItemsForCrossSellBox(string crossSellBox)
        {
            var tags = Page.Tags.Select(t => t.Identifier);
            return slamContext.CreateQuery()
                              .FilterTagBrowserTypes()
                              .FilterSegmentations(WebCoreModule.GetService<IMarketingCenterWebApplicationService>()?.GetCurrentSegmentationIds())
                              .FilterTagBrowserTypesForSpecialAudience()
                              .FilterRelatedToTags(tags)
                              .FilterRelatedToRequiredTags(userContext.SelectedLanguage)
                              .FilterShowInCrossSellBox(crossSellBox)
                              .FilterExpiredAssets()
                              .Take(2)
                              .TagBrowserTypesOrderBy(tags)
                              .Get()
                              .Select(a => a.AsContentItemListItem());
        }
    }
}