﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class CalendarCampaignCategoriesModel
    {
        public IEnumerable<CalendarCampaignCategoryModel> CampaignCategories { get; set; }
    }

    public class CalendarCampaignCategoryModel
    {
        public string ItemId { get; set; }
        public string Title { get; set; }
        public int Order { get; set; }
        public string MarketingCalendarUrl { get; set; }
    }
}