﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class CalendarMarketsModel
    {
        public IEnumerable<CalendarMarketModel> Markets { get; set; }
    }

    public class CalendarMarketModel
    {
        public string Identifier { get; set; }
        public string Title { get; set; }
    }
}