﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class CalendarContentItemCampaignEventsModel
    {
        public IEnumerable<CalendarContentItemModel> ContentItems { get; set; }
        public DateTime CalendarStartDate { get; set; }
        public DateTime CalendarEndDate { get; set; }
        public string RegionIdentifier { get; set; }
        public IEnumerable<CalendarCampaignCategoryModel> CampaignCategories { get; set; }
        public IEnumerable<CalendarMarketModel> Markets { get; set; }
    }

    public class CalendarContentItemModel
    {
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string FrontEndUrl { get; set; }
        public IEnumerable<string> MarketIdentifiers { get; set; }
        public string CampaignCategoryId { get; set; }
        public string CampaignCategoryTitle { get; set; }
        public int Priority { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public IEnumerable<CalendarCampaignEventModel> CampaignEvents { get; set; }
    }

    public class CalendarCampaignEventModel
    {
        public string ItemId { get; set; }
        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool AlwaysOn { get; set; }

        public double EventLength { get { return (EndDate - StartDate).TotalDays; } }
    }

    public class ContentItemCampaignEventModel
    {
        public ContentItemBase ContentItem { get; set; }
        public CampaignEvent CampaignEvent { get; set; }
        public int Row { get; set; }
        public ContentItemForCalendar ContentItemForCalendar { get; set; }
    }
}