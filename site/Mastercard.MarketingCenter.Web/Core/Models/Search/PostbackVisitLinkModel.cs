﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mastercard.MarketingCenter.Web.Core.Models.Search
{
    public class PostbackVisitLinkModel
    {
        public string UrlVisited { get; set; }
    }
}