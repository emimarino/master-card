﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mastercard.MarketingCenter.Web.Core.Models.Search
{
    public class SearchModel
    {
        public string SearchQuery { get; set; }
        public int ResultsCount { get; set; }
    }
}