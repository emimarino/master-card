﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mastercard.MarketingCenter.Web.Core.Models.Search
{
    public class SearchViewModel
    {
        public string SearchTitle { get; set; }
        public int SearchQuantity { get; set; }        
    }
}