﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mastercard.MarketingCenter.Web.Core.Models.Search
{
    public class SearchListViewModel
    {
        public string SearchQuery { get; set; }
        public int PageIndex { get; set; }
        public int Start { get; set; }
        public int Length { get; set; }
        public string SortBy { get; set; }
        public string SortDirection { get; set; }
    }
}