﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class ContactFormModel
    {
        public User User { get; set; }
        public Issuer Issuer { get; set; }
    }
}