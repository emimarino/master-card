﻿using Mastercard.MarketingCenter.Data.DTOs;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class OptimizeModel
    {
        public IEnumerable<RecommendationDTO> Recommendations { get; set; }
        public string TrackingType { get; set; }
    }
}