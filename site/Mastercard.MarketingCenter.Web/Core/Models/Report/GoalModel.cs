﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class GoalModel
    {
        public ReportHeaderModel ReportHeaderModel { get; set; }
        public IEnumerable<OptimizationCategory> OptimizationCategories { get; set; }
        public string ShortDescription { get; set; }
        public string Title { get; set; }
        public string ReturnUrl { get; set; }
        public string TrackingType { get; set; }
        public Issuer Issuer { get; set; }
    }
}