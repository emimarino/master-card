﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class ReportHeaderModel
    {
        public string Name { get; set; }
        public ImportedIca ImportedIca { get; set; }
    }
}