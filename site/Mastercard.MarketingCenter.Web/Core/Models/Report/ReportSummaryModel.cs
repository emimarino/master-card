﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class ReportSummaryModel
    {
        public ReportAssessmentModel ReportAssessment { get; set; }
        public ReportHeaderModel ReportHeader { get; set; }
        public Issuer Issuer { get; set; }
        public bool Error { get; set; }
        public bool ShowReportCover { get; set; }
        public string TrackingType { get; set; }
        public bool ShareReport { get; set; }
    }
}