﻿using Mastercard.MarketingCenter.Data.DTOs;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class OptimizationCategory
    {
        public IEnumerable<DownloadableAssetDTO> Assets { get; set; }
        public string CategoryTitle { get; set; }
    }
}