﻿namespace Mastercard.MarketingCenter.Web.Core.Models.Report
{
    public enum ReportFlag
    {
        Red,
        Yellow,
        Green
    }
}