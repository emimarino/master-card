﻿using Mastercard.MarketingCenter.Data;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class DashboardLandingModel
    {
        public string TrackingType { get; set; }
    }
}