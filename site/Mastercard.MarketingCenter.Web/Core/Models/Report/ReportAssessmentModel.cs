﻿using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class ReportAssessmentModel
    {
        public IList<AssessmentCriteriaDTO> RedAssessments { get; set; }
        public IList<AssessmentCriteriaDTO> YellowAssessments { get; set; }
        public IList<AssessmentCriteriaDTO> GreenAssessments { get; set; }

        public IDictionary<string, RecommendationDTO> Recommendations { get; set; }

        public ReportAssessmentModel()
        {
            RedAssessments = new List<AssessmentCriteriaDTO>();
            YellowAssessments = new List<AssessmentCriteriaDTO>();
            GreenAssessments = new List<AssessmentCriteriaDTO>();

            Recommendations = new Dictionary<string, RecommendationDTO>();
        }

        public void AddRecommendations(IEnumerable<RecommendationDTO> recommendations)
        {
            foreach (var recommendation in recommendations)
            {
                if (!Recommendations.ContainsKey(recommendation.ContentItemId))
                    Recommendations.Add(recommendation.ContentItemId, recommendation);
            }
        }
    }
}