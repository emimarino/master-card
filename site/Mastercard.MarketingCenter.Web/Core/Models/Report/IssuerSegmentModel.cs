﻿namespace Mastercard.MarketingCenter.Web.Core.Models.Report
{
    public class IssuerSegmentationDetailReportModel
    {
        public string ContentItemId { get; set; }

        public string ContentName { get; set; }
    }
}