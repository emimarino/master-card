﻿using Slam.Cms.Data;
using System;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class ContentItemListItem
    {
        public string MainImageUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string IconId { get; set; }
        public string ResourceUrl { get; set; }
        public string Title { get; set; }
        public string Header { get; set; }
        public string Summary { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool Featured { get; set; }
        public ContentItem ContentItem { get; set; }
        public string ContentType { get; set; }
        public int Order { get; set; }
        public bool EnableFavoriting { get; set; }
        public string ContentNotificationImageUrl { get; set; }
    }
}