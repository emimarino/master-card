﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models.ContentItems
{
    public class ApplicableMarketsViewModel
    {
        public IEnumerable<string> Applicable { get; set; }
        public IEnumerable<string> Excluded { get; set; }
    }
}