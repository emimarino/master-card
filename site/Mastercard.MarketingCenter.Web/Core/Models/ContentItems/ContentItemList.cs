﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class ContentItemList
    {
        public IEnumerable<ContentItemListItem> ContentItems { get; set; }
    }
}