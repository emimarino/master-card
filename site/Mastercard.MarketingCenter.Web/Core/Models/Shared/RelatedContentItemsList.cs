﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class RelatedContentItemsList
    {
        public IEnumerable<ContentItemListItem> RelatedContentItems { get; set; }
    }
}