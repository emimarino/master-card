﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class NarrowPanelList
    {
        public string Title { get; set; }
        public string Footer { get; set; }
        public string FooterUrl { get; set; }
        public bool RenderThumbnail { get; set; }
        public IEnumerable<ContentItemListItem> ContentItemList { get; set; }
    }
}