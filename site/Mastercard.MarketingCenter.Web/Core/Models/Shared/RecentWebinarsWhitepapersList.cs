﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class RecentWebinarsWhitepapersList
    {
        public IEnumerable<ContentItemListItem> WebinarsWhitepapers { get; set; }
    }
}