﻿using Mastercard.MarketingCenter.Services.Services;
using System;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class RssFeedList
    {
        public Common.Infrastructure.PagedList<RssFeedItem> RssFeedItems { get; set; }
        public int CurrentPage { get; set; }
        public int Total { get { return RssFeedItems.Total; } }

        public int PreviousPage { get { return CurrentPage > 1 ? CurrentPage - 1 : 1; } }
        public int NextPage { get { return CurrentPage < PageCount ? CurrentPage + 1 : CurrentPage; } }

        public bool HasPreviousPage { get { return CurrentPage > 1; } }
        public bool HasNextPage { get { return CurrentPage < PageCount; } }

        public int PageSize { get { return Constants.Configuration.GetInsightsRssFeedPageSize; } }
        public int PageCount { get { return Total > 0 ? (int)(Math.Ceiling((Total / (double)PageSize))) : 0; } }
    }
}