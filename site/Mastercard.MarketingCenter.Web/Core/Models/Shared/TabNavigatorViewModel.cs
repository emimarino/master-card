﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class TabNavigatorViewModel
    {
        public string Controller { get; set; }
        public string ActiveTab { get; set; }
        public List<TabNavigatorTabViewModel> Tabs { get; set; }
    }

    public class TabNavigatorTabViewModel
    {
        public string Name { get; set; }
        public string Action { get; set; }
        public string Url { get; set; }
    }
}