﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class ContentInsightList
    {
        public IEnumerable<ContentItemListItem> ContentItemListItems { get; set; }
    }
}