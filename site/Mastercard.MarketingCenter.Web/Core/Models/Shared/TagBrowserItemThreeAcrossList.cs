﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class TagBrowserItemThreeAcrossList
    {
        public string Title { get; set; }
        public string FooterText { get; set; }
        public string TitleFooterUrl { get; set; }
        public IEnumerable<ContentItemListItem> TagBrowserItems { get; set; }
    }
}