﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class AttachmentPreviewFile : PreviewFile
    {
        public Attachment Attachment { get; set; }
    }
}