﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class CarouselSlideViewModel
    {
        public IEnumerable<CarouselSlideItem> Slides { get; set; }
    }
}