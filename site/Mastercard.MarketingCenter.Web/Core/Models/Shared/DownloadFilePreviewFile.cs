﻿using Mastercard.MarketingCenter.Services.Entities;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class DownloadFilePreviewFile : PreviewFile
    {
        public DownloadFile DownloadFile { get; set; }
    }
}