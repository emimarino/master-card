﻿namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class PreviewFile
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string Type { get; set; }
    }
}