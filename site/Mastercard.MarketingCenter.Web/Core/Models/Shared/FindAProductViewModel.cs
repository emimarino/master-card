﻿using Slam.Cms;
using Slam.Cms.Data;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class FindAProductViewModel
    {
        public IEnumerable<ContentItemListItem> ContentItemListItems { get; set; }
        public string FindAProductBoxHtml { get; set; }
        public SitemapNode ProductsSiteMapNode { get; set; }
        public IDictionary<TagTreeNode, IDictionary<TagTreeNode, IList<ContentItemListItem>>> ProductBoxItems { get; set; }
    }
}