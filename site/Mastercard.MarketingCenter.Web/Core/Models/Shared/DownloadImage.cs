﻿namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class DownloadImage
    {
        public string ImageTitle { get; set; }
        public string ImageUrl { get; set; }
    }
}