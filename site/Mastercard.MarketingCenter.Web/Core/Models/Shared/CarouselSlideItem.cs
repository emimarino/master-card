﻿namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class CarouselSlideItem
    {
        public string Title { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public string InnerDescription { get; set; }
        public string ButtonText { get; set; }
        public string Url { get; set; }
        public int Order { get; set; }
        public string ContentType { get; set; }
    }
}