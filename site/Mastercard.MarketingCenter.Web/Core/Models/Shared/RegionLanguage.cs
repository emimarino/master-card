﻿namespace Mastercard.MarketingCenter.Web.Core.Models.Shared
{
    public class RegionLanguage
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string RestrictTo { get; set; }
    }
}