﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class TagBrowserItemTwoAcrossListMarketingTab
    {
        public string Title { get; set; }
        public IEnumerable<ContentItemListItem> ContentItemListItems { get; set; }
        public bool ShowIfContentItemListsItemsIsEmpty { get; set; }
    }
}