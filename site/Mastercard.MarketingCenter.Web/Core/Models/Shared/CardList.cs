﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class CardList
    {
        public bool AvoidRenderBackToTop { get; set; }
        public bool IsWide { get; set; }
        public string Title { get; set; }
        public IEnumerable<ContentItemListItem> TagBrowserItems { get; set; }
    }
}