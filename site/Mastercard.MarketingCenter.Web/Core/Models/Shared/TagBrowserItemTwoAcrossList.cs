﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class TagBrowserItemTwoAcrossList
    {
        public string Title { get; set; }
        public string BrowseAllText { get; set; }
        public int BrowseAllCount { get; set; }
        public string BrowseAllUrl { get; set; }
        public IEnumerable<ContentItemListItem> TagBrowserItems { get; set; }
    }
}