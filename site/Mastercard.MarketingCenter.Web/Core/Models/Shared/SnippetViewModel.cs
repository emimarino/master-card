﻿using Mastercard.MarketingCenter.Data.DTOs;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class SnippetViewModel
    {
        public SnippetDTO Snippet { get; set; }
    }
}