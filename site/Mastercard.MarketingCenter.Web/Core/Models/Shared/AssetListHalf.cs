﻿using Mastercard.MarketingCenter.Data.DTOs;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class AssetListHalf
    {
        public IEnumerable<DownloadableAssetDTO> DownloadableAssets { get; set; }
    }
}