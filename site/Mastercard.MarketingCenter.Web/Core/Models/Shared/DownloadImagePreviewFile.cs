﻿namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class DownloadImagePreviewFile : PreviewFile
    {
        public DownloadImage DownloadImage { get; set; }
    }
}