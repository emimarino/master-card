﻿namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class ContentPreferencesViewModel
    {
        public int UserSubscriptionFrequency { get; set; }
        public bool IsUserSubscribedToContent { get; set; }
        public bool ContentPreferencesMessageDisplayed { get; set; }
        public string ContentPreferencesButtonHtml { get; set; }
    }
}