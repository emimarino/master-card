﻿namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class ContentItemCloneToRegionViewModel
    {
        public string RegionNames { get; set; }
    }
}