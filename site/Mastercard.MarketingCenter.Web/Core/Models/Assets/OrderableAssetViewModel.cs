﻿using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class OrderableAssetViewModel : BaseAssetViewModel
    {
        public string Title { get; set; }
        public bool EnableFavoriting { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string CallToAction { get; set; }
        public string MainImage { get; set; }
        public string Sku { get; set; }
        public bool? Vdp { get; set; }
        public string PDFPreview { get; set; }
        public string ElectronicDeliveryFile { get; set; }

        public ProgramDTO RelatedProgram { get; set; }
        public PricingSchedule PricingSchedule { get; set; }
        public string PrintOnDemandError { get; set; }
        public string ElectronicDeliveryError { get; set; }
        public string BatchPrintError { get; set; }
        public int? Quantity { get; set; }
        public Issuer Issuer { get; set; }
        public bool Archived { get; set; }
    }
}