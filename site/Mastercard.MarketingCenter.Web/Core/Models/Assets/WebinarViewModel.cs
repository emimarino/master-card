﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class WebinarViewModel : BaseAssetViewModel
    {
        public string Title { get; set; }
        public bool EnableFavoriting { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public DateTime? Date { get; set; }
        public string VideoFile { get; set; }
        public string StaticVideoImage { get; set; }
        public bool? ShowImageInsteadVideo { get; set; }
        public string Link { get; set; }
        public string LinkText { get; set; }
    }
}