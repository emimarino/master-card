﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class AttachmentsAndCallToActionViewModel
    {
        public IEnumerable<Attachment> Attachments { get; set; }
        public string CallToAction { get; set; }
        public string MainImage { get; set; }
        public bool PreviewFiles { get; set; }
    }
}