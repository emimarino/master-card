﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Web.Core.Modules;
using Slam.Cms;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Tag = Mastercard.MarketingCenter.Data.Entities.Tag;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public abstract class BaseAssetViewModel
    {
        private readonly SlamContext slamContext;
        private readonly UserContext userContext;
        private readonly AssetRepository assetRepository;
        private readonly Sitemap sitemap;

        public string ContentItemId { get; set; }
        public string PrimaryContentItemId { get; set; }
        public string FrontEndUrl { get; set; }
        public string AssetType { get; set; }
        public string CrossSellBox { get; set; }
        public bool PreviewFiles { get; set; }
        public IEnumerable<Attachment> Attachments { get; set; }
        public IEnumerable<Tag> BrowseAllMaterialTags { get; set; }
        public RelatedContentItemsList RelatedContentItemsList { get; set; }
        public NarrowPanelList InsightsCrossSellBox { get; set; }
        public NarrowPanelList ProductsCrossSellBox { get; set; }
        public NarrowPanelList MarketingCrossSellBox { get; set; }
        public NarrowPanelList SolutionsCrossSellBox { get; set; }
        public NarrowPanelList SegmentsCrossSellBox { get; set; }
        public NarrowPanelList SafetySecurityCrossSellBox { get; set; }

        protected BaseAssetViewModel()
        {
            slamContext = DependencyResolver.Current.GetService<SlamContext>();
            assetRepository = DependencyResolver.Current.GetService<AssetRepository>();
            userContext = DependencyResolver.Current.GetService<UserContext>();
            sitemap = DependencyResolver.Current.GetService<Sitemap>();
        }

        public void SetupSectionsCrossSellBoxes()
        {
            if (!AssetType.StartsWith(MarketingCenterDbConstants.AssetTypes.Product) && CrossSellBox != MarketingCenterDbConstants.CrossSellBoxes.Products)
            {
                var node = sitemap.FindNode(sitemap.RootNode, n => n.Key.Equals(MarketingCenterDbConstants.CrossSellBoxes.Products));
                if (node != null)
                {
                    ProductsCrossSellBox = new NarrowPanelList()
                    {
                        Title = node.Title,
                        Footer = node.Title,
                        FooterUrl = node.Url,
                        RenderThumbnail = true,
                        ContentItemList = GetContentItemListItemsForCrossSellBox(MarketingCenterDbConstants.CrossSellBoxes.Products)
                    };
                }
            }

            if (!AssetType.StartsWith(MarketingCenterDbConstants.AssetTypes.Marketing) && CrossSellBox != MarketingCenterDbConstants.CrossSellBoxes.Marketing)
            {
                var node = sitemap.FindNode(sitemap.RootNode, n => n.Key.Equals(MarketingCenterDbConstants.CrossSellBoxes.Marketing, StringComparison.OrdinalIgnoreCase));
                if (node != null)
                {
                    MarketingCrossSellBox = new NarrowPanelList()
                    {
                        Title = node.Title,
                        Footer = node.Title,
                        FooterUrl = node.Url,
                        RenderThumbnail = true,
                        ContentItemList = GetContentItemListItemsForCrossSellBox(MarketingCenterDbConstants.CrossSellBoxes.Marketing)
                    };
                }
            }

            if (!AssetType.StartsWith(MarketingCenterDbConstants.AssetTypes.Insight) && CrossSellBox != MarketingCenterDbConstants.CrossSellBoxes.Insights)
            {
                var node = sitemap.FindNode(sitemap.RootNode, n => n.Key.Equals(MarketingCenterDbConstants.CrossSellBoxes.Insights, StringComparison.OrdinalIgnoreCase));
                if (node != null)
                {
                    InsightsCrossSellBox = new NarrowPanelList()
                    {
                        Title = node.Title,
                        Footer = node.Title,
                        FooterUrl = node.Url,
                        RenderThumbnail = true,
                        ContentItemList = GetContentItemListItemsForCrossSellBox(MarketingCenterDbConstants.CrossSellBoxes.Insights)
                    };
                }
            }

            if (!AssetType.StartsWith(MarketingCenterDbConstants.AssetTypes.Solution) && CrossSellBox != MarketingCenterDbConstants.CrossSellBoxes.Solutions)
            {
                var node = sitemap.FindNode(sitemap.RootNode, n => n.Key.Equals(MarketingCenterDbConstants.CrossSellBoxes.Solutions, StringComparison.OrdinalIgnoreCase));
                if (node != null)
                {
                    SolutionsCrossSellBox = new NarrowPanelList()
                    {
                        Title = node.Title,
                        Footer = node.Title,
                        FooterUrl = node.Url,
                        RenderThumbnail = true,
                        ContentItemList = GetContentItemListItemsForCrossSellBox(MarketingCenterDbConstants.CrossSellBoxes.Solutions)
                    };
                }
            }

            if (!AssetType.StartsWith(MarketingCenterDbConstants.AssetTypes.Segment) && CrossSellBox != MarketingCenterDbConstants.CrossSellBoxes.Segments)
            {
                var node = sitemap.FindNode(sitemap.RootNode, n => n.Key.Equals(MarketingCenterDbConstants.CrossSellBoxes.Segments, StringComparison.OrdinalIgnoreCase));
                if (node != null)
                {
                    SegmentsCrossSellBox = new NarrowPanelList()
                    {
                        Title = node.Title,
                        Footer = node.Title,
                        FooterUrl = node.Url,
                        RenderThumbnail = true,
                        ContentItemList = GetContentItemListItemsForCrossSellBox(MarketingCenterDbConstants.CrossSellBoxes.Segments)
                    };
                }
            }

            if (!AssetType.StartsWith(MarketingCenterDbConstants.AssetTypes.SafetySecurity) && CrossSellBox != MarketingCenterDbConstants.CrossSellBoxes.SafetySecurity)
            {
                var node = sitemap.FindNode(sitemap.RootNode, n => n.Key.Equals(Regex.Replace(MarketingCenterDbConstants.CrossSellBoxes.SafetySecurity, "\\W+", string.Empty), StringComparison.OrdinalIgnoreCase));
                if (node != null)
                {
                    SafetySecurityCrossSellBox = new NarrowPanelList()
                    {
                        Title = node.Title,
                        Footer = node.Title,
                        FooterUrl = node.Url,
                        RenderThumbnail = true,
                        ContentItemList = GetContentItemListItemsForCrossSellBox(MarketingCenterDbConstants.CrossSellBoxes.SafetySecurity)
                    };
                }
            }
        }

        private IEnumerable<ContentItemListItem> GetContentItemListItemsForCrossSellBox(string crossSellBox)
        {
            var tags = assetRepository.GetContentItemTags(this.ContentItemId).Select(t => t.Identifier);
            if (tags.Any())
            {
                return slamContext.CreateQuery()
                  .FilterTagBrowserTypes()
                  .FilterSegmentations(WebCoreModule.GetService<IMarketingCenterWebApplicationService>()?.GetCurrentSegmentationIds())
                  .FilterTagBrowserTypesForSpecialAudience()
                  .FilterRelatedToTags(tags)
                  .FilterRelatedToRequiredTags(userContext.SelectedLanguage)
                  .FilterShowInCrossSellBox(crossSellBox)
                  .Filter("ContentItem.ContentItemId <> '{0}'".F(this.ContentItemId))
                  .FilterExpiredAssets()
                  .Take(2)
                  .TagBrowserTypesOrderBy()
                  .Get()
                  .Select(a => a.AsContentItemListItem());
            }

            return null;
        }
    }
}