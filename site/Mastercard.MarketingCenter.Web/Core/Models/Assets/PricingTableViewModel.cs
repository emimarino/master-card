﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class PricingTableViewModel
    {
        public string Title { get; set; }
        public string TableHeader { get; set; }
        public PricingTable PricingTable { get; set; }
        public bool ContactPrinter { get; set; }
        public decimal? MinimumPrice { get; set; }
        public string PricingNotes { get; set; }
    }
}