﻿using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class AssetFullWidthViewModel : BaseAssetViewModel
    {
        public string Title { get; set; }
        public string BottomAreaDescription { get; set; }
        public Issuer Issuer { get; set; }
        public ProgramDTO RelatedProgram { get; set; }
        public ContentItemCloneToRegionViewModel RegionsApplicableToClone { get; set; }
        public bool EnableFavoriting { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
    }
}