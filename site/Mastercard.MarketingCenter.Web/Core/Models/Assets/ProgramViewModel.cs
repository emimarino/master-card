﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Mastercard.MarketingCenter.Data;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class ProgramViewModel : BaseAssetViewModel
    {
        public string Title { get; set; }
        public bool EnableFavoriting { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string MainImage { get; set; }
        public bool? ShowOrderPeriods { get; set; }
        public DateTime? PrintedOrderPeriodStartDate { get; set; }
        public string PrintedOrderPeriodAlternateDisplayText { get; set; }
        public string PrintedOrderPeriodAlternateDateText { get; set; }
        public DateTime? PrintedOrderPeriodEndDate { get; set; }
        public DateTime? OnlineOrderPeriodStartDate { get; set; }
        public string OnlineOrderPeriodAlternateDisplayText { get; set; }
        public string OnlineOrderPeriodAlternateDateText { get; set; }
        public DateTime? OnlineOrderPeriodEndDate { get; set; }
        public DateTime? InMarketStartDate { get; set; }
        public DateTime? InMarketEndDate { get; set; }
        public string InMarketAlternateDisplayText { get; set; }
        public string InMarketAlternateDateText { get; set; }
        public string CallToAction { get; set; }
        public IEnumerable<ContentItemListItem> RelatedAssets { get; set; }
    }
}