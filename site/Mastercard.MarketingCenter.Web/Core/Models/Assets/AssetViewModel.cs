﻿using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class AssetViewModel : BaseAssetViewModel
    {
        public string Title { get; set; }
        public bool EnableFavoriting { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string CallToAction { get; set; }
        public string MainImage { get; set; }
        public Issuer Issuer { get; set; }
        public ProgramDTO RelatedProgram { get; set; }
        public ContentItemCloneToRegionViewModel RegionsApplicableToClone { get; set; }
    }
}