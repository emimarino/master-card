﻿namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class BrowseAllMaterialViewModel
    {
        public bool Render { get; set; }
        public string Url { get; set; }
        public int Count { get; set; }
    }
}