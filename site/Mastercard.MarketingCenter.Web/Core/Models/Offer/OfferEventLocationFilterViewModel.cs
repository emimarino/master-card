﻿using Mastercard.MarketingCenter.Services.Models;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class OfferEventLocationFilterViewModel
    {
        public IEnumerable<EventLocationModel> EventLocations { get; set; }
    }
}