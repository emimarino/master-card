﻿using Mastercard.MarketingCenter.Services.Models;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class OfferDetailsViewModel
    {
        public OfferDetailsModel Offer { get; set; }
        public CarouselSlideViewModel CarouselSlides { get; set; }
        public IEnumerable<DownloadImage> DownloadImages { get; set; }
    }
}