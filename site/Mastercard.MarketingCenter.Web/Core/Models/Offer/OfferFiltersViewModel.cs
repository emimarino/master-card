﻿using Mastercard.MarketingCenter.Services.Models;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class OfferFiltersViewModel
    {
        public IEnumerable<CategoryModel> Categories { get; set; }
        public IEnumerable<CardExclusivityModel> CardExclusivities { get; set; }
        public IEnumerable<MarketApplicabilityModel> MarketApplicabilities { get; set; }
        public bool ShowMarketApplicabilityFilter { get; set; }
    }
}