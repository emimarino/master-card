﻿using Mastercard.MarketingCenter.Services.Models;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class OfferListViewModel
    {
        public IEnumerable<OfferListModel> Offers { get; set; }
        public IEnumerable<string> CategoryIds { get; set; }
        public IEnumerable<string> CardExclusivityIds { get; set; }
        public IEnumerable<string> MarketApplicabilityIds { get; set; }
        public IEnumerable<int> EventLocationIds { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Text { get; set; }
        public int Start { get; set; }
        public int Length { get; set; }
        public string SortBy { get; set; }
        public string SortDirection { get; set; }
        public bool OfferQuantityOnly { get; set; }
    }
}