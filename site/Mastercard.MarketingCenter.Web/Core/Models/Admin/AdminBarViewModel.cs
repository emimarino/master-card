﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class AdminBarViewModel
    {
        public bool ShowUserName { get; set; }
        public string UserName { get; set; }
        public string AdminUrl { get; set; }
        public bool PreviewMode { get; set; }
        public bool PageHasContentItemId { get; set; }
        public string ContentItemId { get; set; }
        public string EditContentItemUrl { get; set; }
        public string CloneContentItemUrl { get; set; }
        public IList<Region> UserAdminRegions { get; set; }
        public bool UserIsAdminInCurrentRegion { get; set; }
    }
}