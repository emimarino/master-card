﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mastercard.MarketingCenter.Web.Core.Models.Api
{
    public class ChromeApiResponse
    {
        public string Head { get; set; }
        public string Header { get; set; }
        public string Footer { get; set; }
    }
}