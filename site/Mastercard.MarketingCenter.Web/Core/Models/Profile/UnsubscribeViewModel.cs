﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mastercard.MarketingCenter.Web.Core.Models.Profile
{
    public class UnsubscribeViewModel
    {
        public string ImageUrl { get; set; }
    }
}