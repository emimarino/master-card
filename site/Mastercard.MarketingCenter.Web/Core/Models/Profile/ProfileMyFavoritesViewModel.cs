﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class ProfileMyFavoritesViewModel
    {
        public IEnumerable<ContentItemUserSubscription> ContentItemUserSubscriptions { get; set; }
        public IEnumerable<string> AssignedContentItemIds { get; set; }
        public IEnumerable<string> SelectedContentItemIds { get; set; }
        public string ReturnUrl { get; set; }
    }
}