﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class ProfileIndexViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Title { get; set; }
        public string IssuerName { get; set; }
        public string Processor { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string IssuerIca { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public IEnumerable<State> States { get; set; }
        public string ZipCode { get; set; }
        public string ReturnUrl { get; set; }
        public string Language { get; set; }
        public string Country { get; set; }
        public Dictionary<string, string> Countries { get; set; }
        public Dictionary<string, string> Languages { get; set; }
        public string OptOutText { get; set; }
        public string OptOutLink { get; set; }
    }
}