﻿namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class ProfileEmailNotificationsViewModel
    {
        public int Frequency { get; set; }
        public string ReturnUrl { get; set; }
    }
}