﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class ToggleSubscriptionViewModel
    {
        public bool showFirstTimeSubscribe { get; set; }
        public bool showNeverFrecuencyMessage { get; set; }
        public bool subscribed { get; set; }
    }
}