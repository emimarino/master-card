﻿using System.Collections.Generic;
using Mastercard.MarketingCenter.Data;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class ProfileSubscriptionViewModel
    {
        public string ContentItemId { get; set; }
        public bool IsUserSubscribed { get; set; }
        public bool HideSubscriptions { get; set; }
        public string FrecuencyText { get; set; }
        public bool showFirstTimeSubscribe { get; set; }
        public bool showNeverFrecuencyMessage { get; set; }
    }
}