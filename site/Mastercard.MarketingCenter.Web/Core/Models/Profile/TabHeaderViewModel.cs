﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class TabHeaderViewModel
    {
        public string Controller { get; set; }
        public string ActiveTab { get; set; }
        public List<TabHeaderTabViewModel> Tabs { get; set; }
    }

    public class TabHeaderTabViewModel
    {
        public string Name { get; set; }
        public string Action { get; set; }
        public string ReturnUrl { get; set; }
    }
}