﻿using Mastercard.MarketingCenter.Data.DTOs;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class CalendarViewModel
    {
        public PageDTO Page { get; set; }
    }
}