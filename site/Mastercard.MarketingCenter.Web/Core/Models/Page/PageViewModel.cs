﻿using Mastercard.MarketingCenter.Data.DTOs;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class PageViewModel
    {
        public PageDTO Page { get; set; }
    }
}