﻿using Mastercard.MarketingCenter.Data.DTOs;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class MasterCardResourcesViewModel
    {
        public PageDTO Page { get; set; }
    }
}