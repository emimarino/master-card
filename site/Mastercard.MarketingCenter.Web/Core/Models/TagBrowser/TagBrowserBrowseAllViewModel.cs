﻿using Slam.Cms.Data;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class TagBrowserBrowseAllViewModel
    {
        public IEnumerable<TagTreeNode> Columns { get; set; }
    }
}