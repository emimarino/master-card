﻿using Slam.Cms;
using Slam.Cms.Data;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class TagBrowserIndexViewModel
    {
        public TagBrowserIndexViewModel()
        {
            SelectedTags = new List<string>();
        }

        public TagTree TagTree { get; set; }
        public IEnumerable<string> SelectedTags { get; set; }
        public IEnumerable<SitemapNode> BreadcrumbNodes { get; set; }
        public SitemapNode LastNode { get; set; }
        public string LastTag { get; set; }
        public int ResultsCount { get; set; }
        public ContentItemList Results { get; set; }
        public Pager Pager { get; set; }
        public NarrowPanelList FeaturedItems { get; set; }
        public string LastTagPageContent { get; set; }
    }
}