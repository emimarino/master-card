﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models.Consent
{
    public class ConsentLanguageViewModel
    {
        public IEnumerable<int> ConsentFileDataId { get; set; }
        public string ConsentFileData { get; set; }
    }
}