﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mastercard.MarketingCenter.Web.Core.Models.Consent
{
    public class ConsentDataViewModel
    {
        public string Body { get; set; }
    }
}