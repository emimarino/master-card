﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class ConsentListViewModel
    {
        private const string constConsentLanguageText = "consentlanguagetext";
        public IEnumerable<ConsentFileData> consentFileData { get; set; }
        public string ConsentsForApprove
        {
            get
            {
                return string.Join(",", ConsentFiles.Select(cf => cf.ConsentFileDataId.ToString()));
            }
        }
        public string ConsentLanguageText
        {
            get
            {
                return consentFileData.First(cfd => cfd.DocumentType == constConsentLanguageText).ConsentData;
            }
        }
        public IEnumerable<ConsentFileData> ConsentFiles
        {
            get
            {
                return consentFileData.Where(cfd => cfd.DocumentType != constConsentLanguageText);
            }
        }
        public bool ApprovalNeeded
        {
            get
            {
                return consentFileData.Any();
            }
        }
        public bool Any()
        {
            return consentFileData.Any();
        }

        public string GetCsv()
        {
            return string.Join(",", consentFileData.Select(x => x.ConsentFileDataId.ToString()));
        }
    }
}