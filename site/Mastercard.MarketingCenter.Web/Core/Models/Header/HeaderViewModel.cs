﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Models;
using Mastercard.MarketingCenter.Web.Core.Models.Shared;
using Slam.Cms;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class HeaderViewModel
    {
        public SitemapNode CurrentSitemapNode { get; set; }
        public IEnumerable<RegionLanguage> Languages { get; set; }
        public IEnumerable<Region> Regions { get; set; }
        public bool CanChangeRegions { get; set; }
        public bool CanSearchContent { get; set; }
        public bool CanChangeSegments { get; set; }
        public string SegmentationLabel { get; set; }
        public IEnumerable<SegmentationModel> Segmentations { get; set; }
        public IEnumerable<string> CurrentSegmentationIds { get; set; }
        public bool CanSeeNoneSegmentationButton { get; set; }
        public string SegmentationIds { get; set; }
        public IEnumerable<SitemapNode> SitemapByPermissions { get; set; }
        public bool BusinessOwnerExpirationManagementEnabled { get; set; }
        public bool CanUseShoppingCart { get; set; }
        public string ProcessorLogo { get; set; }
        public bool IsInAdminRole { get; set; }
    }
}