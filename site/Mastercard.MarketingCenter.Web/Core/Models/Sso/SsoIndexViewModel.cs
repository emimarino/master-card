﻿namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class SsoIndexViewModel
    {
        public string TargetUrl { get; set; }
        public string Token { get; set; }
        public string RedirectTo { get; set; }
    }
}