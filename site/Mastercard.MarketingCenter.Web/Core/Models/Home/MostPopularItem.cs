﻿using System;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class MostPopularItem
    {
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public string MainImageUrl { get; set; }
        public string ResourceUrl { get; set; }
        public DateTime LastVisitDate { get; set; }
        public int VisitCount { get; set; }
    }
}