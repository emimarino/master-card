﻿using Mastercard.MarketingCenter.Data.DTOs;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class HomeIndexViewModel
    {
        public CarouselSlideViewModel CarouselSlides { get; set; }
        public ContentInsightList ContentInsightList { get; set; }
        public IEnumerable<QuickLinkDTO> QuickLinks { get; set; }
        public FindAProductViewModel FindAProductViewModel { get; set; }
        public bool ShowOptimizationDashboard { get; set; }
        public string PartialSiteName { get; set; }
        public string SiteName { get; set; }
        public ContentPreferencesViewModel ContentPreferencesViewModel { get; set; }
        public string HomeBanner { get; set; }
        public string HomeSubMarqueeText { get; set; }
        public bool TriggerMarketingCustomizedHomepageEnabled { get; set; }
    }
}