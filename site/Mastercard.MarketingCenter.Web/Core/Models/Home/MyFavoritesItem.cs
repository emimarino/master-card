﻿using System;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class MyFavoritesItem
    {
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string MainImageUrl { get; set; }
        public string ResourceUrl { get; set; }
        public DateTime SavedDate { get; set; }
    }
}