﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class MostPopularViewModel
    {
        public IEnumerable<MostPopularItem> MostPopularItems { get; set; }
        public int LessItemsQuantity { get; set; }
    }
}