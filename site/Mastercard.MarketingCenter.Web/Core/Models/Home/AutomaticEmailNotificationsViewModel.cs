﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class AutomaticEmailNotificationsViewModel
    {
        public string Title { get; set; }
        public string AutomaticEmailNotificationsBody{ get; set; }
    }
}