﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class MyFavoritesViewModel
    {
        public IEnumerable<MyFavoritesItem> MyFavoritesItems { get; set; }
    }
}