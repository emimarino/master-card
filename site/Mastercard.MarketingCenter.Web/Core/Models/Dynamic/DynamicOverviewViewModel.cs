﻿using Slam.Cms.Configuration;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class DynamicOverviewViewModel : BaseOverviewTabPageViewModel
    {
        public List<TagBrowserItemThreeAcrossList> sectionList { get; set; }
        public string Title { get; set; }
        public string PageEditUrl { get { return string.Format("{0}/Content/Create/1?title={1}", ConfigurationManager.Environment.AdminUrl.TrimEnd('/'), Title); } }

    }
}