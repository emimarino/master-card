﻿using Slam.Cms.Configuration;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Web.Core.Models
{
    public class DynamicSectionViewModel : BaseDynamicTabPageViewModel
    {
        public List<TagBrowserItemTwoAcrossListMarketingTab> Lists { get; set; }
        public string TabTitle { get; set; }
        public string PageEditUrl { get { return string.Format("{0}/Content/Create/1?title={1}", ConfigurationManager.Environment.AdminUrl.TrimEnd('/'), TabTitle); } }
    }
}