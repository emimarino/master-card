﻿using Hangfire;
using Hangfire.Server;
using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using StackExchange.Profiling;
using System;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class ClonedItemNotificationProcessJob
    {
        private static object clonedItemNotificationProcessLock = new object();
        private readonly IClonesSourceNotificationServices _clonedItemsService;

        public ClonedItemNotificationProcessJob(IClonesSourceNotificationServices clonedItemsService)
        {
            _clonedItemsService = clonedItemsService;
        }

        public void StartNextJob(string jobId)
        {
            BackgroundJob.ContinueWith<ClonedItemNotificationEmailBuilderJob>(jobId, (j) => j.Execute());
        }

        public void Execute(PerformContext context)
        {
            using (MiniProfiler.Current.Step("ClonedItemNotificationProcessJob"))
            {
                lock (clonedItemNotificationProcessLock)
                {
                    LogManager.EventFactory.Create()
                        .Text("Starting ClonedItemNotificationProcessJob...")
                        .Level(LoggingEventLevel.Trace)
                        .Push();

                    //adds next job to send emails trigger
                    StartNextJob(context != null ? context.BackgroundJob.Id : Guid.NewGuid().ToString());
                    LogManager.EventFactory.Create()
                                .Text("ClonedItemNotificationEmailBuilderJob Scheduled to Fire and Forget...")
                                .Level(LoggingEventLevel.Trace)
                                .Push();

                    //this jobs Gathers metadata needed for the notifications and saves it in a table to be used afterwards
                    _clonedItemsService.ProcessSourceUpdateNotifications();

                    LogManager.EventFactory.Create()
                                               .Text($"Successfully executed ClonedItemNotificationProcessJob...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();
                }
            }
        }
    }
}