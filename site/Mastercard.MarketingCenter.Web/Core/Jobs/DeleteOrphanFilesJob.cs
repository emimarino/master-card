﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using StackExchange.Profiling;
using System;
using System.IO;
using System.Linq;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class DeleteOrphanFilesJob
    {
        private static object deleteOrphanFilesLock = new object();
        private readonly IFileService _fileService;
        private readonly ISettingsService _settingsService;

        public DeleteOrphanFilesJob(IFileService fileService, ISettingsService settingsService)
        {
            _fileService = fileService;
            _settingsService = settingsService;
        }

        public void Execute()
        {
            using (MiniProfiler.Current.Step("DeleteOrphanFilesJob"))
            {
                lock (deleteOrphanFilesLock)
                {
                    try
                    {
                        LogManager.EventFactory.Create()
                                               .AddTags("DeleteOrphanFiles")
                                               .Text("Starting DeleteOrphanFilesJob...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();

                        int filesDeleted = 0;
                        string uploadKeys = _settingsService.GetUploadedFoldersKeys();
                        if (uploadKeys.IsNullOrWhiteSpace())
                        {
                            filesDeleted += ProcessFiles("ImagesFolder");
                            filesDeleted += ProcessFiles("FilesFolder");
                            filesDeleted += ProcessFiles("VideosFolder");
                        }
                        else
                        {
                            uploadKeys.Split(',').ToList().ForEach(k => { filesDeleted += ProcessFiles(k); });
                        }

                        LogManager.EventFactory.Create()
                                               .AddTags("DeleteOrphanFiles")
                                               .Text($"Successfully executed DeleteOrphanFilesJob with {filesDeleted} orphan files deleted...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();
                    }
                    catch (Exception ex)
                    {
                        LogManager.EventFactory.Create()
                                               .AddTags("DeleteOrphanFiles")
                                               .Text("Error executing DeleteOrphanFilesJob...")
                                               .Level(LoggingEventLevel.Error)
                                               .AddData("Exception Message", ex.Message)
                                               .AddData("Exception InnerException", ex.InnerException)
                                               .AddData("Exception Data", ex.Data)
                                               .AddData("Exception StackTrace", ex.StackTrace)
                                               .Push();
                        throw;
                    }
                }
            }
        }

        private int ProcessFiles(string filesFolderKey)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath(_settingsService.GetSlamSolutionSetting(filesFolderKey));
            if (path.IsNullOrWhiteSpace())
            {
                throw new InvalidOperationException($"Key '{filesFolderKey}' not found in configuration file or folder not found");
            }

            int filesDeleted = 0;
            DirectoryInfo filesFolder = new DirectoryInfo(Path.GetFullPath(path));
            foreach (FileInfo file in filesFolder.GetFiles("*", SearchOption.AllDirectories))
            {
                string filename = filesFolder.FullName.Equals(file.Directory.FullName, StringComparison.InvariantCultureIgnoreCase) ? file.Name : $"{file.Directory.Name}/{file.Name}";
                if (_fileService.GetAllFilesByFilename(filename).Any())
                {
                    DeleteOrphanFile(filesFolderKey, file);
                    filesDeleted++;
                }
            }

            return filesDeleted;
        }

        private void DeleteOrphanFile(string folderKey, FileInfo file)
        {
            string fileFullName = file.FullName;
            string fileDirectoryName = Path.GetDirectoryName(fileFullName);
            File.Delete(fileFullName);
            if (!Directory.EnumerateFileSystemEntries(fileDirectoryName).Any())
            {
                Directory.Delete(fileDirectoryName, true);
            }

            _fileService.InsertDeletedOrphanFile(folderKey, fileFullName);
        }
    }
}