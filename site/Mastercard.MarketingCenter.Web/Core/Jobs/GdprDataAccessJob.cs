﻿using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using StackExchange.Profiling;
using System;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class GdprDataAccessJob
    {
        private static object gdprDataAccessLock = new object();
        private readonly IGdpApiService _gdpApiService;

        public GdprDataAccessJob(IGdpApiService gdpApiService)
        {
            _gdpApiService = gdpApiService;
        }

        public void Execute()
        {
            using (MiniProfiler.Current.Step("GdprDataAccessJob"))
            {
                lock (gdprDataAccessLock)
                {
                    try
                    {
                        LogManager.EventFactory.Create()
                        .Text("Starting GdprDataAccessJob...")
                        .Level(LoggingEventLevel.Trace)
                        .Push();

                        _gdpApiService.ObtainGdpRequests();

                        LogManager.EventFactory.Create()
                                                .Text($"Successfully executed GdprDataAccessJob...")
                                                .Level(LoggingEventLevel.Trace)
                                                .Push();
                    }
                    catch (Exception ex)
                    {
                        LogManager.EventFactory.Create()
                           .Text("Error executing GdprDataAccessJob...")
                           .Level(LoggingEventLevel.Error)
                           .AddException(ex)
                           .AddData("Exception Message", ex.Message)
                           .AddData("Exception InnerException", ex.InnerException)
                           .AddData("Exception Data", ex.Data)
                           .Push();
                    }
                }
            }
        }
    }
}