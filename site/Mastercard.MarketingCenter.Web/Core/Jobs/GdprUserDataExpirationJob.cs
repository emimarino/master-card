﻿using Hangfire.Server;
using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using StackExchange.Profiling;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class GdprUserDataExpirationJob
    {
        private static object gdprUserDataExpirationLock = new object();
        private readonly IGdpApiService _gdpApiService;

        public GdprUserDataExpirationJob(IGdpApiService gdpApiService)
        {
            _gdpApiService = gdpApiService;
        }

        public void Execute(PerformContext context)
        {
            using (MiniProfiler.Current.Step("GdprUserDataExpirationJob"))
            {
                lock (gdprUserDataExpirationLock)
                {
                    LogManager.EventFactory.Create()
                                       .Text("Starting GdprUserDataExpirationJob...")
                                       .Level(LoggingEventLevel.Trace)
                                       .Push();

                    var hasError = false;
                    var exceptions = (ICollection<Exception>)new List<Exception>();
                    var expiringUsersCount = _gdpApiService.ApplyUserPrivacyDataRetention(out hasError, out exceptions);

                    var endLog = LogManager.EventFactory.Create()
                                            .Text($"{(hasError ? "With Errors" : "Successfully")} executed GdprUserDataExpirationJob with {expiringUsersCount} users expired...")
                                            .Level(LoggingEventLevel.Trace);

                    foreach (var ex in exceptions)
                    {
                        endLog.AddException(ex);
                    }
                    endLog.Push();

                    if (context != null && hasError && exceptions.Any())
                    {
                        throw new AggregateException("Finished With Errors", exceptions);
                    }
                }
            }
        }
    }
}