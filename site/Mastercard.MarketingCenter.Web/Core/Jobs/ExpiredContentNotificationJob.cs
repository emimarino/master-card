﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using StackExchange.Profiling;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class ExpiredContentNotificationJob
    {
        private static object expiredContentNotificationLock = new object();
        private readonly IContentItemService _contentItemServices;
        private readonly IEmailService _emailService;
        private readonly ISettingsService _settingsService;
        private readonly IRegionService _regionService;

        public ExpiredContentNotificationJob(IContentItemService contentItemServices, IEmailService emailService, ISettingsService settingsService, IRegionService regionService)
        {
            _contentItemServices = contentItemServices;
            _emailService = emailService;
            _settingsService = settingsService;
            _regionService = regionService;
        }

        public void Execute()
        {
            using (MiniProfiler.Current.Step("ExpiredContentNotificationJob"))
            {
                lock (expiredContentNotificationLock)
                {
                    try
                    {
                        LogManager.EventFactory.Create()
                            .Text("Starting ExpiredContentNotificationJob...")
                            .Level(LoggingEventLevel.Trace)
                            .Push();

                        int mailsSent = ProcessMails();

                        LogManager.EventFactory.Create()
                            .Text($"Finishing ExpiredContentNotificationJob with {mailsSent} mails sent...")
                            .Level(LoggingEventLevel.Trace)
                            .Push();
                    }
                    catch (Exception ex)
                    {
                        LogManager.EventFactory.Create()
                           .Text("Error executing ExpiredContentNotificationJob...")
                           .Level(LoggingEventLevel.Error)
                           .AddData("Exception Message", ex.Message)
                           .AddData("Exception InnerException", ex.InnerException)
                           .AddData("Exception Data", ex.Data)
                           .AddData("Exception StackTrace", ex.StackTrace)
                           .Push();
                        throw;
                    }
                }
            }
        }

        private int ProcessMails()
        {
            var sentNotifications = 0;
            foreach (var region in _regionService.GetAll())
            {
                var programsAboutToExpire = _settingsService.GetProgramPrintOrderDateExpirationEnabled(region.IdTrimmed) ?
                                            EmailOrder(_contentItemServices.GetProgramsByPrintedOrderPeriodEndDateAsContentItemDTOs(region.IdTrimmed)) :
                                            null;

                var businessOwnerContent = _contentItemServices.GetBusinessOwnerContentAboutToExpire(region.IdTrimmed);
                if (businessOwnerContent.Any())
                {
                    sentNotifications++;
                    _emailService.SendContentAboutToExpireAdminEmail(region.IdTrimmed, 0, EmailOrder(businessOwnerContent.SelectMany(boc => boc.ContentItems)), programsAboutToExpire);

                    // if Business Owner Expiration Management is disabled or a BO is NULL or is blocked, it should not need to be notified
                    if (_settingsService.GetBusinessOwnerExpirationManagementEnabled(region.IdTrimmed))
                    {
                        foreach (var businessOwner in businessOwnerContent.Where(boc => boc.UserId > 0 && !boc.IsBlocked && !boc.IsDisabled))
                        {
                            var businessOwnerContentItems = businessOwner.ContentItems.Where(boci => MarketingCenterDbConstants.ContentTypeIds.BusinessOwnerNotificationIds.Contains(boci.ContentTypeId));
                            if (businessOwnerContentItems.Any())
                            {
                                sentNotifications++;
                                _emailService.SendContentAboutToExpireEmail(region.IdTrimmed, businessOwner.UserId, businessOwner.Email, businessOwner.Name, EmailOrder(businessOwnerContentItems));
                            }
                        }
                    }
                }
                else if (programsAboutToExpire != null && programsAboutToExpire.Any())
                {
                    sentNotifications++;
                    _emailService.SendContentAboutToExpireAdminEmail(region.IdTrimmed, 0, null, programsAboutToExpire);
                }
            }

            return sentNotifications;
        }

        private IOrderedEnumerable<ContentItemDTO> EmailOrder(IEnumerable<ContentItemDTO> contentItems)
        {
            return contentItems.OrderBy(ci => ci.ExpirationDate).ThenBy(ci => ci.Title);
        }
    }
}