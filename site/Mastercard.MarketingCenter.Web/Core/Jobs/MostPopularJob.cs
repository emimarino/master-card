﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using StackExchange.Profiling;
using System;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class MostPopularJob
    {
        private static object mostPopularLock = new object();
        private readonly IMostPopularService _mostPopularService;
        private readonly IUnitOfWork _unitOfWork;

        public MostPopularJob(IMostPopularService mostPopularService, IUnitOfWork unitOfWork)
        {
            _mostPopularService = mostPopularService;
            _unitOfWork = unitOfWork;
        }

        public void RefreshMostPopularData()
        {
            using (MiniProfiler.Current.Step("MostPopularJob"))
            {
                lock (mostPopularLock)
                {
                    try
                    {
                        LogManager.EventFactory.Create()
                                               .Text($"Starting MostPopularJob...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();

                        try
                        {
                            var mostPopularProcess = _mostPopularService.CreateProcess();
                            _unitOfWork.Commit();

                            _mostPopularService.ProcessMostPopularActivity(mostPopularProcess);
                        }
                        finally
                        {
                            _unitOfWork.Commit();
                        }

                        LogManager.EventFactory.Create()
                                               .Text($"Successfully executed MostPopularJob...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();
                    }
                    catch (Exception ex)
                    {
                        LogManager.EventFactory.Create()
                                               .Text($"Error executing MostPopularJob...")
                                               .Level(LoggingEventLevel.Error)
                                               .AddData("Exception Message", ex.Message)
                                               .AddData("Exception InnerException", ex.InnerException)
                                               .AddData("Exception Data", ex.Data)
                                               .AddData("Exception StackTrace", ex.StackTrace)
                                               .Push();
                        throw;
                    }
                }
            }
        }
    }
}