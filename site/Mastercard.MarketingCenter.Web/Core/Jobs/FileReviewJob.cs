﻿using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using StackExchange.Profiling;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class FileReviewJob
    {
        private static object fileReviewLock = new object();
        private readonly IFileService _fileService;
        private readonly ISettingsService _settingsService;

        public FileReviewJob(IFileService fileService, ISettingsService settingsService)
        {
            _fileService = fileService;
            _settingsService = settingsService;
        }

        public void Execute()
        {
            using (MiniProfiler.Current.Step("FileReviewJob"))
            {
                lock (fileReviewLock)
                {
                    try
                    {
                        LogManager.EventFactory.Create()
                                               .AddTags("FileReview")
                                               .Text("Starting FileReviewJob...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();

                        int filesReviewed = 0;
                        var fileReviewFolders = _settingsService.GetFileReviewFolders();
                        if (fileReviewFolders.Any())
                        {
                            _fileService.CleanUpReviewedFiles();
                            fileReviewFolders.ForEach(folder => { filesReviewed += _fileService.ReviewFiles(folder); });
                        }
                        else
                        {
                            throw new InvalidOperationException("File review folder not found");
                        }

                        LogManager.EventFactory.Create()
                                               .AddTags("FileReview")
                                               .Text($"Successfully executed FileReviewJob with {filesReviewed} files reviewed...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();
                    }
                    catch (Exception ex)
                    {
                        LogManager.EventFactory.Create()
                                               .AddTags("FileReview")
                                               .Text("Error executing FileReviewJob...")
                                               .Level(LoggingEventLevel.Error)
                                               .AddData("Exception Message", ex.Message)
                                               .AddData("Exception InnerException", ex.InnerException)
                                               .AddData("Exception Data", ex.Data)
                                               .AddData("Exception StackTrace", ex.StackTrace)
                                               .AddException(ex)
                                               .Push();
                        throw ex;
                    }
                }
            }
        }

        public void FileMove()
        {
            using (MiniProfiler.Current.Step("FileReviewJob"))
            {
                lock (fileReviewLock)
                {
                    try
                    {
                        LogManager.EventFactory.Create()
                                               .AddTags("FileMove")
                                               .Text("Starting FileReviewJob...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();

                        int filesMoved = 0;
                        var fileReviewFolders = _settingsService.GetFileReviewFolders();
                        string fileReviewMoveFolder = _settingsService.GetFileReviewMoveFolder();
                        DateTime fileReviewMoveFolderDate = _settingsService.GetFileReviewMoveFolderDate();
                        if (fileReviewFolders.Any())
                        {
                            fileReviewFolders.ForEach(folder => { filesMoved += _fileService.MoveReviewedFiles(folder, fileReviewMoveFolder, fileReviewMoveFolderDate); });
                        }
                        else
                        {
                            throw new InvalidOperationException("File review folder not found");
                        }

                        LogManager.EventFactory.Create()
                                               .AddTags("FileMove")
                                               .Text($"Successfully executed FileReviewJob with {filesMoved} files moved...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();
                    }
                    catch (Exception ex)
                    {
                        LogManager.EventFactory.Create()
                                               .AddTags("FileMove")
                                               .Text("Error executing FileReviewJob...")
                                               .Level(LoggingEventLevel.Error)
                                               .AddData("Exception Message", ex.Message)
                                               .AddData("Exception InnerException", ex.InnerException)
                                               .AddData("Exception Data", ex.Data)
                                               .AddData("Exception StackTrace", ex.StackTrace)
                                               .AddException(ex)
                                               .Push();
                        throw ex;
                    }
                }
            }
        }

        public void FileCopy()
        {
            using (MiniProfiler.Current.Step("FileReviewJob"))
            {
                lock (fileReviewLock)
                {
                    try
                    {
                        LogManager.EventFactory.Create()
                                               .AddTags("FileCopy")
                                               .Text("Starting FileReviewJob...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();

                        int filesCopied = 0;
                        var fileReviewFolders = _settingsService.GetFileReviewFolders();
                        string fileReviewCopyFolder = _settingsService.GetFileReviewCopyFolder();
                        DateTime fileReviewCopyFolderStartDate = _settingsService.GetFileReviewCopyFolderStartDate();
                        DateTime fileReviewCopyFolderEndDate = _settingsService.GetFileReviewCopyFolderEndDate();
                        if (fileReviewFolders.Any())
                        {
                            fileReviewFolders.ForEach(folder => { filesCopied += _fileService.CopyReviewedFiles(folder, fileReviewCopyFolder, fileReviewCopyFolderStartDate, fileReviewCopyFolderEndDate); });
                        }
                        else
                        {
                            throw new InvalidOperationException("File review folder not found");
                        }

                        LogManager.EventFactory.Create()
                                               .AddTags("FileCopy")
                                               .Text($"Successfully executed FileReviewJob with {filesCopied} files copied...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();
                    }
                    catch (Exception ex)
                    {
                        LogManager.EventFactory.Create()
                                               .AddTags("FileCopy")
                                               .Text("Error executing FileReviewJob...")
                                               .Level(LoggingEventLevel.Error)
                                               .AddData("Exception Message", ex.Message)
                                               .AddData("Exception InnerException", ex.InnerException)
                                               .AddData("Exception Data", ex.Data)
                                               .AddData("Exception StackTrace", ex.StackTrace)
                                               .AddException(ex)
                                               .Push();
                        throw ex;
                    }
                }
            }
        }
    }
}