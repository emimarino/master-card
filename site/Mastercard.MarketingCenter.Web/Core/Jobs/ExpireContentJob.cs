﻿using Pulsus;
using StackExchange.Profiling;
using System.Configuration;
using System.Data.SqlClient;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class ExpireContentJob
    {
        private static object expireContentLock = new object();
        private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["MasterCardMarketingCenter"].ConnectionString;

        public void Execute()
        {
            using (MiniProfiler.Current.Step("ExpireContentJob"))
            {
                lock (expireContentLock)
                {
                    LogManager.EventFactory.Create()
                                           .Text("Starting ExpireContentJob...")
                                           .Level(LoggingEventLevel.Trace)
                                           .Push();

                    using (var connection = new SqlConnection(ConnectionString))
                    {
                        using (var command = connection.CreateCommand())
                        {
                            command.CommandType = System.Data.CommandType.StoredProcedure;
                            command.CommandText = "ExpireContent";

                            command.Connection.Open();
                            command.ExecuteNonQuery();
                            command.Connection.Close();
                        }
                    }

                    LogManager.EventFactory.Create()
                                           .Text("Successfully executed ExpireContentJob...")
                                           .Level(LoggingEventLevel.Trace)
                                           .Push();
                }
            }
        }
    }
}