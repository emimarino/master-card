﻿using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using StackExchange.Profiling;
using System;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class SiteTrackingJob
    {
        private static object siteTrackingLock = new object();
        private readonly ITrackingService _trackingService;

        public SiteTrackingJob(ITrackingService trackingService)
        {
            _trackingService = trackingService;
        }

        public void RefreshSiteTrackingData()
        {
            using (MiniProfiler.Current.Step("SiteTrackingJob"))
            {
                lock (siteTrackingLock)
                {
                    try
                    {
                        LogManager.EventFactory.Create()
                                               .Text($"Starting SiteTrackingJob...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();

                        _trackingService.ProcessSiteTracking();

                        LogManager.EventFactory.Create()
                                               .Text($"Successfully executed SiteTrackingJob...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();
                    }
                    catch (Exception ex)
                    {
                        LogManager.EventFactory.Create()
                                               .Text($"Error executing SiteTrackingJob...")
                                               .Level(LoggingEventLevel.Error)
                                               .AddData("Exception Message", ex.Message)
                                               .AddData("Exception InnerException", ex.InnerException)
                                               .AddData("Exception Data", ex.Data)
                                               .AddData("Exception StackTrace", ex.StackTrace)
                                               .Push();
                        throw;
                    }
                }
            }
        }
    }
}