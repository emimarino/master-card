﻿using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using StackExchange.Profiling;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class ConsentsUpdateJob
    {
        private static object consentsUpdateLock = new object();
        private readonly IConsentManagementService _consentManagementService;

        public ConsentsUpdateJob(IConsentManagementService consentManagementService)
        {
            _consentManagementService = consentManagementService;
        }

        public void Execute()
        {
            using (MiniProfiler.Current.Step("ConsentsUpdateJob"))
            {
                lock (consentsUpdateLock)
                {
                    LogManager.EventFactory.Create()
                        .Text("Starting ConsentsUpdateJob...")
                        .Level(LoggingEventLevel.Trace)
                        .Push();

                    _consentManagementService.RetrieveConsentFilesAndVersions();

                    LogManager.EventFactory.Create()
                                           .Text($"Successfully executed ConsentsUpdateJob...")
                                           .Level(LoggingEventLevel.Trace)
                                           .Push();
                }
            }
        }
    }
}