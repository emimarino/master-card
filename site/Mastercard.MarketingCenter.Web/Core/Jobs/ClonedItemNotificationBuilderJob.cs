﻿using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using StackExchange.Profiling;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class ClonedItemNotificationEmailBuilderJob
    {
        private static object clonedItemNotificationEmailBuilderLock = new object();
        private readonly IClonesSourceNotificationServices _clonedItemsService;

        public ClonedItemNotificationEmailBuilderJob(IClonesSourceNotificationServices clonedItemsService)
        {
            _clonedItemsService = clonedItemsService;
        }

        public void Execute()
        {
            using (MiniProfiler.Current.Step("ClonedItemNotificationEmailBuilderJob"))
            {
                lock (clonedItemNotificationEmailBuilderLock)
                {
                    LogManager.EventFactory.Create()
                        .Text("Starting ClonedItemNotificationEmailBuilderJob...")
                        .Level(LoggingEventLevel.Trace)
                        .Push();

                    //this job builds the notification Emails ans saves it so the mail dispatcher will send them afterwards
                    _clonedItemsService.BuildSourceUpdateNotificationEmails();

                    LogManager.EventFactory.Create()
                                           .Text($"Successfully executed ClonedItemNotificationEmailBuilderJob...")
                                           .Level(LoggingEventLevel.Trace)
                                           .Push();
                }
            }
        }
    }
}