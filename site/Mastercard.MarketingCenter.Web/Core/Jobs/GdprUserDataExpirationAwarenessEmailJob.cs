﻿using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using StackExchange.Profiling;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class GdprUserDataExpirationAwarenessEmailJob
    {
        private static object gdprUserDataExpirationAwarenessEmailLock = new object();
        private readonly IGdpApiService _gdpApiService;

        public GdprUserDataExpirationAwarenessEmailJob(IGdpApiService gdpApiService)
        {
            _gdpApiService = gdpApiService;
        }

        public void Execute(string region)
        {
            using (MiniProfiler.Current.Step("GdprUserDataExpirationAwarenessEmailJob"))
            {
                lock (gdprUserDataExpirationAwarenessEmailLock)
                {
                    LogManager.EventFactory.Create()
                        .Text("Starting ExpiredContentNotificationJob...")
                        .Level(LoggingEventLevel.Trace)
                        .Push();

                    int followUpEmails = _gdpApiService.ProcessFollowUpEmails(region);

                    LogManager.EventFactory.Create()
                                            .Text($"Successfully executed ExpiredContentNotificationJob with {followUpEmails} Follow-Up emails sent...")
                                            .Level(LoggingEventLevel.Trace)
                                            .Push();
                }
            }
        }
    }
}