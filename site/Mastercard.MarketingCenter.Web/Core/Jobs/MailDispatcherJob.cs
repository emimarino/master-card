﻿using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using StackExchange.Profiling;
using System;
using System.Linq;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class MailDispatcherJob
    {
        private static object mailDispatcherLock = new object();
        private readonly IMailDispatcherService _mailDispatcherService;

        public MailDispatcherJob(IMailDispatcherService mailDispatcherService)
        {
            _mailDispatcherService = mailDispatcherService;
        }

        public void Execute()
        {
            using (MiniProfiler.Current.Step("MailDispatcherJob"))
            {
                lock (mailDispatcherLock)
                {
                    try
                    {
                        LogManager.EventFactory.Create()
                            .Text("Starting MailDispatcherJob...")
                            .Level(LoggingEventLevel.Trace)
                            .Push();

                        int mailsSent = ProcessMails();

                        LogManager.EventFactory.Create()
                            .Text(string.Format("Finishing MailDispatcherJob with {0} mails sent...", mailsSent.ToString()))
                            .Level(LoggingEventLevel.Trace)
                            .Push();
                    }
                    catch (Exception ex)
                    {
                        LogManager.EventFactory.Create()
                           .Text("Error executing MailDispatcherJob...")
                           .Level(LoggingEventLevel.Error)
                           .AddData("Exception Message", ex.Message)
                           .AddData("Exception InnerException", ex.InnerException)
                           .AddData("Exception Data", ex.Data)
                           .Push();
                    }
                }
            }
        }

        private int ProcessMails()
        {
            var mailsSent = 0;
            var pendingMails = _mailDispatcherService.GetPendingMails();

            foreach (var mail in pendingMails.ToList())
            {
                if (_mailDispatcherService.SendMailDispatcher(mail))
                {
                    mailsSent++;
                }
            }

            return mailsSent;
        }
    }
}