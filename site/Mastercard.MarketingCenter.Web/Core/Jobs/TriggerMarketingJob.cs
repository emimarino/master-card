﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Models;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Services;
using Pulsus;
using Slam.Cms.Data;
using StackExchange.Profiling;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using Infrastructure = Mastercard.MarketingCenter.Common.Infrastructure;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class TriggerMarketingJob
    {
        private static object triggerMarketingLock = new object();
        private readonly TriggerMarketingRepository _triggerMarketingRepository;
        private readonly UserSubscriptionService _userSubscriptionService;
        private readonly SlamContext _slamContext;
        private readonly IUserService _userService;
        private readonly IRegionalizeService _regionalizeService;
        private readonly IMailDispatcherService _mailDispatcherService;
        private readonly IEmailService _emailService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAnonymousTokenService _authorizationService;
        private readonly IUrlService _urlService;

        public TriggerMarketingJob(
            TriggerMarketingRepository triggerMarketingRepository,
            UserSubscriptionService userSubscriptionService,
            SlamContext slamContext,
            IUserService userServices,
            IRegionalizeService regionalizeService,
            IMailDispatcherService mailDispatcherService,
            IEmailService emailService,
            IUnitOfWork unitOfWork,
            IUrlService urlService,
            IAnonymousTokenService authorizationService)
        {
            _triggerMarketingRepository = triggerMarketingRepository;
            _userSubscriptionService = userSubscriptionService;
            _slamContext = slamContext;
            _userService = userServices;
            _regionalizeService = regionalizeService;
            _mailDispatcherService = mailDispatcherService;
            _emailService = emailService;
            _unitOfWork = unitOfWork;
            _authorizationService = authorizationService;
            _urlService = urlService;
        }

        public void ExecuteDaily()
        {
            Execute("Daily", MarketingCenterDbConstants.UserSubscriptionFrequency.Daily, Infrastructure.Constants.TrackingTypes.TriggerMarketingSubscriptionDaily);
        }

        public void ExecuteWeekly()
        {
            Execute("Weekly", MarketingCenterDbConstants.UserSubscriptionFrequency.Weekly, Infrastructure.Constants.TrackingTypes.TriggerMarketingSubscriptionWeekly);
        }

        private void Execute(string frequencyLabel, int frequency, string trackingType)
        {
            using (MiniProfiler.Current.Step("TriggerMarketingJob"))
            {
                lock (triggerMarketingLock)
                {
                    try
                    {
                        LogManager.EventFactory.Create()
                                               .Text($"Starting {frequencyLabel} TriggerMarketingJob...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();

                        int notificationEmailsSent = ProcessFrequency(frequency, trackingType);

                        LogManager.EventFactory.Create()
                                               .Text($"Successfully executed {frequencyLabel} TriggerMarketingJob with {notificationEmailsSent.ToString()} notification emails sent...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();
                    }
                    catch (Exception ex)
                    {
                        LogManager.EventFactory.Create()
                                               .Text($"Error executing {frequencyLabel} TriggerMarketingJob...")
                                               .Level(LoggingEventLevel.Error)
                                               .AddData("Exception Message", ex.Message)
                                               .AddData("Exception InnerException", ex.InnerException)
                                               .AddData("Exception Data", ex.Data)
                                               .AddData("Exception StackTrace", ex.StackTrace)
                                               .Push();
                        throw;
                    }
                    finally
                    {
                        _unitOfWork.Commit();
                    }
                }
            }
        }

        private int ProcessFrequency(int frequency, string trackingType)
        {
            int triggerMarketingNotificationsCount = 0;
            DateTime sentDate = DateTime.Now;

            var triggerMarketingNotifications = _userSubscriptionService.GetUserTriggerMarketingNotifications(frequency, sentDate);
            foreach (var userNotification in triggerMarketingNotifications)
            {
                CreateUserNotificationMail(userNotification, trackingType);
                triggerMarketingNotificationsCount++;
            }

            switch (frequency)
            {
                case MarketingCenterDbConstants.UserSubscriptionFrequency.Daily:
                    _triggerMarketingRepository.SaveSentDailyDate(sentDate);
                    break;
                case MarketingCenterDbConstants.UserSubscriptionFrequency.Weekly:
                    _triggerMarketingRepository.SaveSentWeeklyDate(sentDate);
                    break;
                default:
                    break;
            }

            return triggerMarketingNotificationsCount;
        }

        private void CreateUserNotificationMail(UserTriggerMarketingNotification userNotification, string trackingType)
        {
            var user = _userService.GetUserByUserName(userNotification.UserName);
            string _region = string.IsNullOrEmpty(user.Region) ? RegionalizeService.DefaultRegion : user.Region.Trim();
            string _language = string.IsNullOrEmpty(user.Language) ? RegionalizeService.DefaultLanguage : user.Language.Trim();

            int emailLinks = 0;
            string subject = RegionalizeService.RegionalizeSetting("TriggerMarketing.Subject", _region);
            string fromAddress = RegionalizeService.RegionalizeSetting("TriggerMarketing.FromAddress", _region);
            string fromName = RegionalizeService.RegionalizeSetting("TriggerMarketing.FromName", _region);

            var mailDispatcher = _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, userNotification.UserEmail, string.Empty, userNotification.UserId, userNotification.UserRegionid, trackingType, emailLinks, mailDispatcherStatusId: MarketingCenterDbConstants.MailDispatcherStatus.Creating);

            mailDispatcher.Body = GetUserNotificationMailBody(userNotification, mailDispatcher.MailDispatcherId.ToString(), subject, user.Profile.FirstName, trackingType, ref emailLinks, _region, _language).Replace("[#MailDispatcherTrackingPixelUrl]", _urlService.GetTrackingPixelUrl(mailDispatcher.RegionId, mailDispatcher.MailDispatcherId));
            mailDispatcher.TotalLinks = emailLinks;
            mailDispatcher.MailDispatcherStatusId = MarketingCenterDbConstants.MailDispatcherStatus.New;

            _mailDispatcherService.SaveMailDispatcher(mailDispatcher);
            _unitOfWork.Commit();
        }

        private string AddUnsubscribeLink(string orginalString, string region, string language, int userId)
        {
            if (!string.IsNullOrEmpty(orginalString) && orginalString.Contains("[#Unsubscribe]"))
            {
                return orginalString.Replace("[#Unsubscribe]", _emailService.GetTemplate("Unsubscribe.TemplateFile", region, language, false).Replace("[#UnsubscribeLink]", _authorizationService.SetAnonymousTokenData(new AnonymousUserProxy { ClaimsTo = Infrastructure.Constants.AnonymousClaims.Unsubscribe, UserId = userId, RegionId = region, OptionalParameter = language }, region)));
            }

            return orginalString;
        }

        private string GetUserNotificationMailBody(UserTriggerMarketingNotification userNotification, string mailDispatcherId, string subject, string userFirstName, string trackingType, ref int emailLinks, string region, string language)
        {
            var body = _emailService.GetTemplate("TriggerMarketing.TemplateFile", region, language, false);
            body = AddUnsubscribeLink(body, region, language, userNotification.UserId);
            body = body.Replace("[#UserFirstName]", userFirstName);
            body = body.Replace("[#Subject]", subject);
            body = body.Replace("[#HeaderImageUrl]", _emailService.GetEmailTemplateHeaderImageUrl(region));
            body = body.Replace("[#CurrentYear]", DateTime.Now.Year.ToString());
            body = body.Replace("[#PreferencesUrl]", $"{RegionalizeService.GetBaseUrl(region).TrimEnd('/')}/{ConfigurationManager.AppSettings["TriggerMarketing.PreferencesUrl"].TrimStart('/')}");
            body = body.Replace("[#ContactUsAddress]", RegionalizeService.RegionalizeSetting("TriggerMarketing.ContactUsAddress", region));
            body = body.Replace("[#EnvDomain]", RegionalizeService.GetBaseUrl(region).TrimEnd('/'));
            var section = _emailService.GetTemplate("TriggerMarketing.SectionTemplateFile", region, language, false, true);

            body = body.Replace("[#DynamicContent]", section.Replace("[#SectionTitle]", _regionalizeService.RegionalizeResource("Forms", "MyFavorites", region, language)) +
                                                     GetNotificationContent(userNotification, mailDispatcherId, trackingType, ref emailLinks, language, region));
            return body;
        }

        private string GetNotificationContent(UserTriggerMarketingNotification userNotification, string mailDispatcherId, string trackingType, ref int emailLinks, string language, string region)
        {
            string featuredNotifications = string.Empty;
            string commonNotifications = string.Empty;

            var notifications = GetAllContentItemTriggerMarketingNotifications(userNotification);
            foreach (var tmInfo in notifications.Where(tm => tm.Notification != null && tm.ParentContentItemId.IsNullOrEmpty()).OrderByDescending(n => n.SavedDate))
            {
                var contentItem = _slamContext.SetKnownContentTypes(typeof(PageDTO).GetAssembly())
                                                      .CreateQuery()
                                                      .FilterContentItemId(tmInfo.ContentItemId)
                                                      .FilterRegion(tmInfo.Notification.RegionId)
                                                      .Cache(SlamQueryCacheBehavior.NoCache)
                                                      .GetFirstOrDefault();
                if (contentItem != null)
                {
                    emailLinks++;
                    var item = contentItem.AsContentItemListItem();

                    string notification;
                    notification = _emailService.GetTemplate("TriggerMarketing.NotificationTemplateFile", region, language, false, true);

                    notification = notification.Replace("[#NotificationUrl]", GetNotificationUrl(item.ResourceUrl, mailDispatcherId, trackingType, emailLinks, region));
                    notification = notification.Replace("[#NotificationPublishedDate]", GetNotificationPublishedDateLabel(tmInfo.SavedDate.ToStringForEmail(new CultureInfo(language)), region, language));

                    if (tmInfo.Notification.Featured)
                    {
                        notification = notification.Replace("[#IsNewBold]", tmInfo.IsNew ? GetNotificationNewLabel(region, language) : string.Empty);
                        notification = notification.Replace("[#NotificationTitleBold]", item.Title);
                        notification = notification.Replace("[#IsNew]", string.Empty);
                        notification = notification.Replace("[#NotificationTitle]", string.Empty);

                        featuredNotifications += notification;
                    }
                    else
                    {
                        notification = notification.Replace("[#IsNew]", tmInfo.IsNew ? GetNotificationNewLabel(region, language) : string.Empty);
                        notification = notification.Replace("[#NotificationTitle]", item.Title);
                        notification = notification.Replace("[#IsNewBold]", string.Empty);
                        notification = notification.Replace("[#NotificationTitleBold]", string.Empty);

                        commonNotifications += notification;
                    }
                }
            }

            return $"{featuredNotifications}{commonNotifications}";
        }

        private string GetNotificationNewLabel(string region, string language)
        {
            return $"({_regionalizeService.RegionalizeResource("Shared", "New", region, language).ToLower()})&nbsp;";
        }

        private string GetNotificationPublishedDateLabel(string publishedDate, string region, string language)
        {
            return $"({_regionalizeService.RegionalizeResource("Shared", "Published", region, language).ToLower()}&nbsp;{publishedDate})";
        }

        private string GetNotificationUrl(string resourceUrl, string mailDispatcherId, string trackingType, int linkNumber, string region)
        {
            return $"{RegionalizeService.GetBaseUrl(region).TrimEnd('/')}/{resourceUrl.TrimStart('/')}?trackingType={trackingType}-{mailDispatcherId}-{linkNumber}";
        }

        private IEnumerable<ContentItemTriggerMarketingNotification> GetAllContentItemTriggerMarketingNotifications(UserTriggerMarketingNotification userNotification)
        {
            var allContentItemTriggerMarketingNotification = new List<ContentItemTriggerMarketingNotification>();
            foreach (var tmInfo in userNotification.Notifications)
            {
                allContentItemTriggerMarketingNotification.Add(tmInfo);
                allContentItemTriggerMarketingNotification.AddRange(tmInfo.Children);

                if (!tmInfo.IsKnownParent && !tmInfo.ParentContentItemId.IsNullOrEmpty() &&
                    allContentItemTriggerMarketingNotification.FirstOrDefault(un => un.ContentItemId == tmInfo.ParentContentItemId) == null)
                {
                    var children = userNotification.Notifications.Where(n => !n.IsKnownParent && !n.ParentContentItemId.IsNullOrEmpty() && n.ParentContentItemId == tmInfo.ParentContentItemId);
                    var notification = new ContentItemTriggerMarketingNotification
                    {
                        ContentItemId = tmInfo.ParentContentItemId,
                        ParentContentItemId = null,
                        IsKnownParent = false,
                        IsNew = false,
                        SavedDate = children.Max(c => c.SavedDate),
                        Notification = new TriggerMarketingNotification
                        {
                            ContentItemId = tmInfo.ParentContentItemId,
                            RegionId = tmInfo.Notification.RegionId,
                            Notify = true,
                            Featured = false,
                            Reasons = string.Empty,
                            IsNew = false,
                            UserId = userNotification.UserId,
                            UserFullName = userNotification.UserFullName,
                            UserEmail = userNotification.UserEmail,
                            UserSubscriptionFrequencyId = userNotification.UserSubscriptionFrequencyId,
                            SavedDate = children.Max(c => c.SavedDate),
                            ParentContentItemId = null
                        }
                    };

                    allContentItemTriggerMarketingNotification.Add(notification);
                }
            }

            return allContentItemTriggerMarketingNotification.GroupBy(tm => tm.ContentItemId).Select(g => g.OrderByDescending(c => c.SavedDate).FirstOrDefault());
        }
    }
}