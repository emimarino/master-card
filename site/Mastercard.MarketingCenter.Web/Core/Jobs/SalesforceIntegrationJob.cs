﻿using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Strategies.Salesforce;
using Pulsus;
using StackExchange.Profiling;
using System;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class SalesforceIntegrationJob
    {
        private static object salesforceIntegrationJobLock = new object();
        private readonly ISalesforceApiService _salesforceApiService;
        private readonly ISalesforceService _salesforceService;
        private readonly IEmailService _emailService;

        public SalesforceIntegrationJob(ISalesforceApiService salesforceApiService, ISalesforceService salesforceService, IEmailService emailService)
        {
            _salesforceApiService = salesforceApiService;
            _salesforceService = salesforceService;
            _emailService = emailService;
        }

        public void Execute()
        {
            using (MiniProfiler.Current.Step("SalesforceIntegrationJob"))
            {
                lock (salesforceIntegrationJobLock)
                {
                    try
                    {
                        LogManager.EventFactory.Create()
                                               .AddTags("Salesforce")
                                               .Text("Starting SalesforceIntegrationJob...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();

                        //this job send user, download and order activities to salesforce web api converted to salesforce tasks
                        var orchestrator = new SalesforceTasksOrchestrator(_salesforceService, _emailService)
                        {
                            InputOutputStrategy = new SalesforceStrategy
                            {
                                SalesforceService = _salesforceService,
                                SalesforceApiService = _salesforceApiService
                            }
                        };
                        orchestrator.Start();

                        LogManager.EventFactory.Create()
                                               .AddTags("Salesforce")
                                               .Text($"Successfully executed SalesforceIntegrationJob...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();
                    }
                    catch (Exception ex)
                    {
                        LogManager.EventFactory.Create()
                                               .AddTags("Salesforce")
                                               .Text($"Error executing SalesforceIntegrationJob...")
                                               .Level(LoggingEventLevel.Error)
                                               .AddData("Exception Message", ex.Message)
                                               .AddData("Exception InnerException", ex.InnerException)
                                               .AddData("Exception Data", ex.Data)
                                               .AddData("Exception StackTrace", ex.StackTrace)
                                               .AddException(ex)
                                               .Push();
                        throw;
                    }
                }
            }
        }
    }
}