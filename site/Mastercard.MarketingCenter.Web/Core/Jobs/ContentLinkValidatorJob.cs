﻿using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using Slam.Cms;
using StackExchange.Profiling;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class ContentLinkValidatorJob
    {
        private static object contentLinkValidatorLock = new object();
        private readonly IContentLinkValidatorService _linkValidatorService;
        private readonly Sitemap _sitemap;
        private readonly IRegionService _regionService;

        public ContentLinkValidatorJob(IContentLinkValidatorService linkValidatorService, IRegionService regionService)
        {
            _linkValidatorService = linkValidatorService;
            _sitemap = new MastercardSitemap(new UserContext(RegionalizeService.DefaultRegion, RegionalizeService.DefaultLanguage));
            _regionService = regionService;
        }

        public void Execute()
        {
            using (MiniProfiler.Current.Step("ContentLinkValidatorJob"))
            {
                lock (contentLinkValidatorLock)
                {
                    LogManager.EventFactory.Create()
                        .Text("Starting ContentLinkValidatorJob...")
                        .Level(LoggingEventLevel.Trace)
                        .Push();

                    HashSet<string> urls = new HashSet<string>();
                    foreach (var region in _regionService.GetAll())
                    {
                        urls.UnionWith(((MastercardSitemap)_sitemap).GetUrls(region.IdTrimmed));
                    }

                    _linkValidatorService.UpdateContentBrokenLinks(urls.ToList());

                    LogManager.EventFactory.Create()
                                           .Text($"Successfully executed ContentLinkValidatorJob...")
                                           .Level(LoggingEventLevel.Trace)
                                           .Push();
                }
            }
        }
    }
}