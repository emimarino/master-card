﻿using Mastercard.MarketingCenter.Data;
using Pulsus;
using StackExchange.Profiling;
using System;
using System.Web.Configuration;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class DeletePublishedTriggerMarketingsJob
    {
        private static object deletePublishedTriggerMarketingsLock = new object();

        public void Execute()
        {
            using (MiniProfiler.Current.Step("DeletePublishedTriggerMarketingsJob"))
            {
                lock (deletePublishedTriggerMarketingsLock)
                {
                    try
                    {
                        LogManager.EventFactory.Create()
                                               .Text("Starting DeletePublishedTriggerMarketingsJob...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();

                        var triggerMarketingRepository = new TriggerMarketingRepository(new MarketingCenterDbContext());
                        var deletionMonths = string.IsNullOrEmpty(WebConfigurationManager.AppSettings["DeletePublishedTriggerMarketings.DeletionMonths"])
                                             ? 12
                                             : Convert.ToInt32(WebConfigurationManager.AppSettings["DeletePublishedTriggerMarketings.DeletionMonths"]);

                        int deletedPublishedTriggerMarketings = triggerMarketingRepository.DeletePublishedTriggerMarketings(deletionMonths);

                        LogManager.EventFactory.Create()
                                               .Text($"Successfully executed DeletePublishedTriggerMarketingsJob with {deletedPublishedTriggerMarketings} records deleted...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();
                    }
                    catch (Exception ex)
                    {
                        LogManager.EventFactory.Create()
                                               .Text("Error executing DeletePublishedTriggerMarketingsJob...")
                                               .Level(LoggingEventLevel.Error)
                                               .AddData("Exception Message", ex.Message)
                                               .AddData("Exception InnerException", ex.InnerException)
                                               .AddData("Exception Data", ex.Data)
                                               .AddData("Exception StackTrace", ex.StackTrace)
                                               .Push();
                    }
                }
            }
        }
    }
}