﻿using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using StackExchange.Profiling;
using System;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class ContentItemVisitsJob
    {
        private static object contentItemVisitsLock = new object();
        private readonly ITrackingService _trackingService;

        public ContentItemVisitsJob(ITrackingService trackingService)
        {
            _trackingService = trackingService;
        }

        public void RefreshContentItemVisitsData()
        {
            using (MiniProfiler.Current.Step("ContentItemVisitsJob"))
            {
                lock (contentItemVisitsLock)
                {
                    try
                    {
                        LogManager.EventFactory.Create()
                                               .Text($"Starting ContentItemVisitsJob...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();

                        _trackingService.ProcessContentItemVisits();

                        LogManager.EventFactory.Create()
                                               .Text($"Successfully executed ContentItemVisitsJob...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();
                    }
                    catch (Exception ex)
                    {
                        LogManager.EventFactory.Create()
                                               .Text($"Error executing ContentItemVisitsJob...")
                                               .Level(LoggingEventLevel.Error)
                                               .AddData("Exception Message", ex.Message)
                                               .AddData("Exception InnerException", ex.InnerException)
                                               .AddData("Exception Data", ex.Data)
                                               .AddData("Exception StackTrace", ex.StackTrace)
                                               .Push();
                        throw;
                    }
                }
            }
        }
    }
}