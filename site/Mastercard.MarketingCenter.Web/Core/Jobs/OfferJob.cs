﻿using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using StackExchange.Profiling;
using System;

namespace Mastercard.MarketingCenter.Web.Core.Jobs
{
    public class OfferJob
    {
        private static object offerLock = new object();
        private readonly IOfferService _offerService;

        public OfferJob(IOfferService offerService)
        {
            _offerService = offerService;
        }

        public void RefreshPricelessOffers()
        {
            using (MiniProfiler.Current.Step("OfferJob"))
            {
                lock (offerLock)
                {
                    try
                    {
                        LogManager.EventFactory.Create()
                                               .Text($"Starting RefreshPricelessOffers...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();

                        _offerService.ImportPricelessOffers();

                        _offerService.ProcessPricelessOffers();

                        _offerService.ProcessUnmatchedPricelessOffers();

                        LogManager.EventFactory.Create()
                                               .Text($"Successfully executed RefreshPricelessOffers...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();
                    }
                    catch (Exception ex)
                    {
                        LogManager.EventFactory.Create()
                                               .Text($"Error executing RefreshPricelessOffers...")
                                               .Level(LoggingEventLevel.Error)
                                               .AddData("Exception Message", ex.Message)
                                               .AddData("Exception InnerException", ex.InnerException)
                                               .AddData("Exception Data", ex.Data)
                                               .AddData("Exception StackTrace", ex.StackTrace)
                                               .Push();
                        throw;
                    }
                }
            }
        }

        public void DeleteOldImportedOffers()
        {
            using (MiniProfiler.Current.Step("OfferJob"))
            {
                lock (offerLock)
                {
                    try
                    {
                        LogManager.EventFactory.Create()
                                               .Text($"Starting DeleteOldImportedOffers...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();

                        _offerService.CleanUpImportedOffers();

                        LogManager.EventFactory.Create()
                                               .Text($"Successfully executed DeleteOldImportedOffers...")
                                               .Level(LoggingEventLevel.Trace)
                                               .Push();
                    }
                    catch (Exception ex)
                    {
                        LogManager.EventFactory.Create()
                                               .Text($"Error executing DeleteOldImportedOffers...")
                                               .Level(LoggingEventLevel.Error)
                                               .AddData("Exception Message", ex.Message)
                                               .AddData("Exception InnerException", ex.InnerException)
                                               .AddData("Exception Data", ex.Data)
                                               .AddData("Exception StackTrace", ex.StackTrace)
                                               .Push();
                        throw;
                    }
                }
            }
        }
    }
}