﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mastercard.MarketingCenter.Data;
using StackExchange.Profiling;
using Mastercard.MarketingCenter.Services;

namespace Mastercard.MarketingCenter.Web.Core.HttpModules
{
    public class VanityUrlHttpModule : IHttpModule
    {
        IDisposable _profile;
        public void Init(HttpApplication context)
        {
            _profile = MiniProfiler.Current.Step("VanityURLHttpModule");
            context.BeginRequest += context_BeginRequest;
            context.PostAuthenticateRequest += context_PostAuthenticateRequest;
        }

        private static void InitializeCache()
        {
            var vanityUrlRepository = DependencyResolver.Current.GetService<VanityUrlRepository>();
            var vanityUrls = vanityUrlRepository.GetAll();

            foreach (var vanityUrl in vanityUrls)
            {
                var url = "/" + vanityUrl.Url.TrimEnd('/').TrimStart('/') + "/";
                var parsedVanityUrl = "/" + vanityUrl.VanityURL.TrimEnd('/').TrimStart('/') + "/";
                VanityUrlCache.AddUrl(parsedVanityUrl, url, vanityUrl.RegionId);
            }

            vanityUrlRepository.Dispose();
        }

        public void context_BeginRequest(object sender, EventArgs e)
        {
            using (MiniProfiler.Current.Step("BeginRequest"))
            {
                if (!VanityUrlCache.IsInitialized)
                {
                    InitializeCache();
                }

                var context = ((HttpApplication)sender).Context;
                var uc = DependencyResolver.Current.GetService<UserContext>();

                string portalUrl = new Uri(Slam.Cms.Configuration.ConfigurationManager.Environment.FrontEndUrl).AbsolutePath;
                string url = "/" + context.Request.Url.AbsolutePath.Replace(portalUrl, string.Empty).TrimEnd('/').TrimStart('/') + "/";
                string vanityUrl = string.IsNullOrEmpty(uc?.SelectedRegion?.Trim()) ? string.Empty : VanityUrlCache.GetVanityUrl(url, uc.SelectedRegion?.Trim());

                if (!string.IsNullOrEmpty(vanityUrl))
                {
                    var redirectUrl = portalUrl.TrimEnd('/') + "/" + vanityUrl.Replace(portalUrl, string.Empty).TrimStart('/');
                    context.RewritePath(redirectUrl, false);
                }
            }
        }

        protected void context_PostAuthenticateRequest(object sender, EventArgs e)
        {
            using (MiniProfiler.Current.Step("PostAuthenticateRequest"))
            {
                var context = ((HttpApplication)sender).Context;
                if (context.Request.IsAuthenticated)
                {
                    var sitemap = DependencyResolver.Current.GetService<Slam.Cms.Sitemap>();

                    var node = sitemap.FindNode(sitemap.RootNode, n => n.Url.ToLower() == (context.Server.UrlDecode(context.Request.Url.AbsolutePath).TrimEnd('/')).ToLower());

                    if (sitemap.excludedUrls.Any(eurl => !string.IsNullOrEmpty(eurl) && (context.Request.Url.GetLeftPart(UriPartial.Path)).ToLower().EndsWith(eurl.ToLower())))
                    {
                        context.Response.Redirect(sitemap.RootNode.Url);
                        return;
                    }

                    if (node != null)
                    {
                        context.Items.Add("CurrentSitemapNodeKey", node.Key);
                        if ((node.Children.Count > 0 || !node.MustHaveSubsections) && node.Parent != null)
                        {
                            var service = DependencyResolver.Current.GetService<Services.IPageTrackingService>();
                            service.TrackPageAccess();

                            string trackingType = context.Request.QueryString["trackingType"];
                            context.Items.Add("OriginalRequestUrl", context.Request.Url.ToString());
                            if (node.Parent.Key != Constants.DefaultSitemapNodeKey)
                            {
                                context.RewritePath(($"/portal/Dynamic/DynamicPage?nodeKey={node.Key}&parentKey={node.Parent.Key}{(!string.IsNullOrWhiteSpace(trackingType) ? $"&trackingType={trackingType}" : string.Empty)}"), false);
                            }
                            else
                            {
                                context.RewritePath(($"/portal/Dynamic/Overview?nodeKey={node.Key}{(!string.IsNullOrWhiteSpace(trackingType) ? $"&trackingType={trackingType}" : string.Empty)}"), false);
                            }
                        }
                    }
                }
            }
        }

        public void Dispose()
        {
            _profile?.Dispose();
        }
    }

    public static class VanityUrlCache
    {
        private static readonly ConcurrentDictionary<string, ConcurrentDictionary<string, string>> Cache = new ConcurrentDictionary<string, ConcurrentDictionary<string, string>>();

        public static bool IsInitialized => Cache.Any();

        public static void AddUrl(string url, string vanityUrl, string regionId)
        {
            var regionalCache = Cache.GetOrAdd(regionId?.Trim(), (new ConcurrentDictionary<string, string>()));
            var key = url.ToLowerInvariant();
            var value = vanityUrl.ToLowerInvariant();

            regionalCache.TryAdd(key, value);
        }

        public static void Invalidate()
        {
            Cache.Clear();
        }

        public static string GetVanityUrl(string url, string regionId)
        {
            var key = url.ToLowerInvariant();
            ConcurrentDictionary<string, string> regionalCache = null;
            Cache.TryGetValue(regionId?.Trim(), out regionalCache);
            if (regionalCache == null)
            {
                return string.Empty;
            }

            return regionalCache.ContainsKey(key) ? regionalCache[key] : string.Empty;
        }
    }
}