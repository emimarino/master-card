﻿using System;
using System.Web;
using Slam.Cms.Configuration;
using System.Net;
using Slam.Cms.Common;
using System.Linq;
using Mastercard.MarketingCenter.Common.Extensions;

namespace Mastercard.MarketingCenter.Web.Core.HttpModules
{
    public class IcsModule : IHttpModule
    {
        #region IHttpModule Members

        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += context_BeginRequest;
        }

        protected void context_BeginRequest(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;
            if (context.Request.RawUrl.Contains(".ics"))
            {
                string trackingUrl = "";
                if (context.Request.UrlReferrer != null && !context.Request.UrlReferrer.Authority.Equals(context.Request.Url.Authority))
                {
                    trackingUrl = ConfigurationManager.Environment.Settings["OmnitureTrackingReferrerUrlFormat"].F("{0}__{1}".F(context.Request.UrlReferrer.ToString(), context.Request.Url.Segments.Last()));
                }
                else if (context.Request.UrlReferrer == null && context.Request.QueryString["CMP"] != null)
                {
                    string relevantQueryString = "";
                    foreach (string queryStringKey in context.Request.QueryString.AllKeys)
                    {
                        if (!queryStringKey.Equals("CMP"))
                        {
                            relevantQueryString += "{0}={1}&".F(queryStringKey, context.Request.QueryString[queryStringKey]);
                        }
                    }
                    trackingUrl = ConfigurationManager.Environment.Settings["OmnitureTrackingEmpUrlFormat"].F(relevantQueryString, context.Request.Url.Segments.Last());
                }

                if (!trackingUrl.IsNullOrEmpty())
                {
                    HttpWebRequest request = WebRequest.Create(trackingUrl) as HttpWebRequest;
                    if (request != null)
                    {
                        request.Accept = "*/*";
                        request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.0.3705)";
                        request.Method = "GET";
                        request.KeepAlive = true;
                        request.GetResponse();
                    }
                }
            }
        }

        #endregion
    }
}