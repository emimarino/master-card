﻿using System;
using System.Web;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Core.Results
{
    public class ImagePixelResult : ActionResult
    {
        private static readonly byte[] Imgbytes = Convert.FromBase64String("R0lGODlhAQABAIAAANvf7wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==");

        public override void ExecuteResult(ControllerContext context)
        {
            HttpResponseBase httpResponse = context.HttpContext.Response;
            httpResponse.ContentType = "image/gif";
            httpResponse.AppendHeader("Content-Length", Imgbytes.Length.ToString());
            httpResponse.Cache.SetLastModified(DateTime.Now);
            httpResponse.Cache.SetCacheability(HttpCacheability.NoCache);
            httpResponse.Expires = -1500;
            httpResponse.Cache.SetNoStore();
            httpResponse.ExpiresAbsolute = DateTime.Now.AddYears(-1);
            httpResponse.BinaryWrite(Imgbytes);
        }
    }
}