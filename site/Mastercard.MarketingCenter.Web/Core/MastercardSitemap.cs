﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Web.Core.Models.Shared;
using Slam.Cms;
using Slam.Cms.Common;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Linq;

namespace Mastercard.MarketingCenter.Web.Core
{
    public class MastercardSitemap : Sitemap
    {
        private static ConcurrentDictionary<KeyValuePair<string, DateTime>, XElement> AvailableRegionNodes;
        private static ConcurrentDictionary<KeyValuePair<string, DateTime>, HashSet<string>> PerRegionExcludedURls;

        override public HashSet<string> excludedUrls { get { return GetExcludedUrls(SelectedRegion); } }
        private IDictionary<string, string> _languages;
        public IDictionary<string, string> LanguagesList { get { return _languages; } private set { _languages = value; } }

        private IDictionary<string, XElement> _sitemaps;
        public IDictionary<string, XElement> SitemapsList { get { return _sitemaps; } private set { _sitemaps = value; } }

        private string _region;
        public string SelectedRegion { get { return _region?.Trim(); } private set { _region = value; } }

        private string _language;
        public string SelectedLanguage { get { return _language?.Trim(); } private set { _language = value; } }

        public static string DefaultLanguage { get { return System.Configuration.ConfigurationManager.AppSettings["DefaultLanguage"]; } }
        public static string DefaultRegion { get { return System.Configuration.ConfigurationManager.AppSettings["DefaultRegion"]; } }
        public UserContext UserContext { get { return _userContext; } private set { _userContext = value; } }

        private XElement _sitemapRoot;
        private UserContext _userContext;

        public static Dictionary<string, string> AvailableRegionSitemaps { get; private set; }
        public static Dictionary<string, string> AvailableLanguages { get; private set; }

        static MastercardSitemap()
        {
            AvailableRegionNodes = new ConcurrentDictionary<KeyValuePair<string, DateTime>, XElement>();
            PerRegionExcludedURls = new ConcurrentDictionary<KeyValuePair<string, DateTime>, HashSet<string>>();
            AvailableRegionSitemaps = new Dictionary<string, string>();
            AvailableLanguages = new Dictionary<string, string>();
            var regionFiles = System.IO.Directory.GetFiles(GetExecutionFolder()).Where(f => f.Contains("Region.") && f.EndsWith(".config"));
            foreach (var regionFile in regionFiles)
            {
                var region = regionFile.Substring((regionFile.LastIndexOf("Region.") + "Region.".Length), (regionFile.IndexOf(".config") - "Region".Length - regionFile.LastIndexOf("Region.") - 1))?.Trim();
                if (!region.IsNullOrEmpty() && !(AvailableRegionSitemaps.Contains(new KeyValuePair<string, string>(region, regionFile))))
                {
                    AvailableRegionSitemaps.Add(region, regionFile);
                    var document = XDocument.Load(regionFile);
                    foreach (var language in GetLanguages(document.Root))
                    {
                        if (!AvailableLanguages.ContainsKey(language.Key))
                            AvailableLanguages.Add(language.Key, language.Value.Name);
                    }
                }
            }
        }

        private static string GetExecutionFolder()
        {
            try
            {
                return System.Web.HttpRuntime.AppDomainAppPath;
            }
            catch (Exception)
            {
                return AppDomain.CurrentDomain.BaseDirectory;
            }
        }

        public MastercardSitemap()
        {
            _userContext = DependencyResolver.Current.GetService<UserContext>();
        }

        public MastercardSitemap(UserContext userContext)
        {
            _userContext = userContext;
        }

        private XElement GetRootNode(string __region = null, bool loadInClass = true)
        {
            SelectedRegion = __region == null ? (_userContext.SelectedRegion.IsNullOrEmpty() ? DefaultRegion : _userContext.SelectedRegion) : __region;
            SelectedRegion = (AvailableRegionSitemaps.ContainsKey(SelectedRegion) ? SelectedRegion : (AvailableRegionSitemaps.ContainsKey(SelectedRegion.ToLower()) ? SelectedRegion.ToLower() : DefaultRegion))?.Trim();
            SelectedRegion = SelectedRegion?.Trim();
            if (SelectedRegion.IsNullOrEmpty())
            {
                throw new ArgumentNullException("Region not Found");
            }

            if (!System.IO.File.Exists(AvailableRegionSitemaps[SelectedRegion]))
            {
                throw new ArgumentNullException("Region Config File not Found");
            }

            XElement returnElement = null;
            if (AvailableRegionNodes.Any(n => n.Key.Key.Equals(SelectedRegion, StringComparison.OrdinalIgnoreCase)))
            {
                var tempNode = new KeyValuePair<KeyValuePair<string, DateTime>, XElement>();

                if (isExpired(AvailableRegionNodes, out tempNode, SelectedRegion))
                {
                    XElement removed = null;
                    AvailableRegionNodes.TryRemove(AvailableRegionNodes.First(n => n.Key.Key.Equals(SelectedRegion, StringComparison.OrdinalIgnoreCase)).Key, out removed);
                    AvailableRegionNodes.TryAdd(new KeyValuePair<string, DateTime>(SelectedRegion, DateTime.Now), XElement.Load(AvailableRegionSitemaps[SelectedRegion]));
                    returnElement = AvailableRegionNodes.First(n => n.Key.Key.Equals(SelectedRegion, StringComparison.OrdinalIgnoreCase)).Value;
                }
                else
                {
                    returnElement = tempNode.Value;
                }
            }
            else
            {
                AvailableRegionNodes.TryAdd(new KeyValuePair<string, DateTime>(SelectedRegion, DateTime.Now), XElement.Load(AvailableRegionSitemaps[SelectedRegion]));
                returnElement = AvailableRegionNodes.First(n => n.Key.Key.Equals(SelectedRegion, StringComparison.OrdinalIgnoreCase)).Value;
            }

            if (loadInClass)
            {
                _sitemapRoot = returnElement;
            }

            return returnElement;
        }

        private bool isExpired<T>(ConcurrentDictionary<KeyValuePair<string, DateTime>, T> cache, out KeyValuePair<KeyValuePair<string, DateTime>, T> returnObject, string _region = "")
        {
            string region = _region?.Trim();
            if (region.IsNullOrEmpty())
            {
                region = SelectedRegion ?? DefaultRegion;
            }
            region?.Trim();

            var temp = cache.FirstOrDefault(co => co.Key.Key.ToLower() == region.ToLower());
            var result = temp.Key.Value < System.IO.File.GetLastWriteTime(AvailableRegionSitemaps[region]);
            returnObject = (result ? (new KeyValuePair<KeyValuePair<string, DateTime>, T>()) : temp);

            return result;
        }

        public string GetSetting(string key, string _region = null, bool canDefault = false)
        {
            string region = _region?.Trim();
            Func<XElement, string, XElement> getElementgetsettingValue = (setNode, keyValue) =>
            {
                return setNode?.Elements()?.FirstOrDefault(n => n.Attribute("name")?.Value.Equals(keyValue, StringComparison.OrdinalIgnoreCase) ?? false);
            };

            Func<XElement, string> getValue = (element) =>
            {
                return element?.Attribute("value")?.Value ?? "";
            };

            XElement setting = null;
            if (region != null)
            {
                setting = getElementgetsettingValue(GetRootNode(region).Element("settings"), key);
            }

            var final = (setting == null && canDefault) ?
                    getValue(getElementgetsettingValue(GetRootNode(DefaultRegion).Element("settings"), key)) :
                          (canDefault && (string.IsNullOrEmpty(getValue(setting))) ?
                                    getValue(getElementgetsettingValue(GetRootNode(DefaultRegion).Element("settings"), key)) :
                                   getValue(setting));

            return final;
        }

        public string TermsOfUse { get { return GetSetting("TermsOfUse"); } }
        public string PrivacyNotice { get { return GetSetting("PrivacyNotice"); } }

        public HashSet<string> GetUrls(string region, bool ignoreExcludeUrl = true)
        {
            var urls = new HashSet<string>();

            if (AvailableRegionSitemaps.ContainsKey(region))
            {
                var rootNode = XElement.Load(AvailableRegionSitemaps[region]);

                foreach (var sitemapNode in rootNode.Element("sitemaps").Elements())
                {
                    urls.UnionWith(GetSitemapNodeUrls(sitemapNode, ignoreExcludeUrl));
                }

                if (urls.Any(url => string.IsNullOrEmpty(url)))
                {
                    throw new Exception($"All nodes with a url attribute defined from {(!string.IsNullOrEmpty(region) ? $"the {region} sitemap" : "all sitemaps")} should have a value defined.");
                }
            }

            return urls;
        }

        private static HashSet<string> GetSitemapNodeUrls(XElement sitemapNode, bool ignoreExcludeUrl)
        {
            var urls = new HashSet<string>();

            if (sitemapNode.Attribute("url") != null &&
               (ignoreExcludeUrl || sitemapNode.Attribute("excludeurl") == null || !sitemapNode.Attribute("excludeurl").Value.Equals("true", StringComparison.OrdinalIgnoreCase)))
            {
                urls.Add(sitemapNode.Attribute("url").Value);
            }

            foreach (var innerSitemapNode in sitemapNode.Elements("SitemapNode"))
            {
                urls.UnionWith(GetSitemapNodeUrls(innerSitemapNode, ignoreExcludeUrl));
            }

            return urls;
        }

        public HashSet<string> GetExcludedUrls(string _regionId)
        {
            var urls = new HashSet<string>();
            string regionId = _regionId?.Trim();
            if (PerRegionExcludedURls.Any(eu => eu.Key.Key.Equals(regionId, StringComparison.OrdinalIgnoreCase)))
            {
                var tempNode = new KeyValuePair<KeyValuePair<string, DateTime>, HashSet<string>>();
                if (isExpired(PerRegionExcludedURls, out tempNode, SelectedRegion))
                {
                    var actualRegion = GetUrls(regionId);
                    foreach (var region in AvailableRegionSitemaps)
                    {
                        if (region.Key != regionId)
                        {
                            urls.UnionWith(GetUrls(region.Key, false).Except(actualRegion));
                        }
                    }

                    HashSet<string> dummy = null;
                    PerRegionExcludedURls.TryRemove(tempNode.Key, out dummy);
                    PerRegionExcludedURls.TryAdd(new KeyValuePair<string, DateTime>(regionId, DateTime.Now), urls);
                }
                else
                {
                    return tempNode.Value;
                }
            }
            else
            {
                var actualRegion = GetUrls(regionId);
                foreach (var region in AvailableRegionSitemaps)
                {
                    if (region.Key != regionId)
                    {
                        urls.UnionWith(GetUrls(region.Key, false).Except(actualRegion));
                    }
                }

                PerRegionExcludedURls.TryAdd(new KeyValuePair<string, DateTime>(regionId, DateTime.Now), urls);
            }

            return urls;
        }

        //add cach to all sitemaps to avoid reloading everytimee and test this
        private void ParseNode(SitemapBuilder builder, XElement node, string parent = null)
        {
            if (parent == null)
            {
                parent = node.Parent.Name.ToString().ToLower() == "sitemap" ? string.Empty : node.Parent.Attribute("key").Value;
            }

            string key = MakeUniqueNodeKey(builder, node.Attribute("key").Value + (node.Attribute("url") == null ? "-section" : ""));

            string title = node.Attribute(SelectedLanguage) != null ? node.Attribute(SelectedLanguage).Value : (node.Attribute(DefaultLanguage) != null ? node.Attribute(DefaultLanguage).Value : "");
            if (!title.IsNullOrEmpty()) //If there isn't any title, including the default language, don't add this node to the sitemap
            {
                builder.AddNode(parent, key, title,
                                node.Attribute("url") != null ? node.Attribute("url").Value : node.Parent.Attribute("url").Value,
                                node.Attribute("location") != null ? node.Attribute("location").Value : null,
                                node.Attribute("id") != null ? node.Attribute("id").Value : null,
                                node.Attribute("singletagnode") != null ? node.Attribute("singletagnode").Value : null,
                                node.Attribute("style") != null ? node.Attribute("style").Value : null,
                                node.Attribute("musthavesubsections") != null ? node.Attribute("musthavesubsections").Value : null);
            }

            if (node.HasElements)
            {
                foreach (var singleNode in node.Elements())
                {
                    ParseNode(builder, singleNode, key);
                }
            }
        }

        private string MakeUniqueNodeKey(SitemapBuilder builder, string key)
        {
            bool findAvailableKey = true;
            string tryKey = string.Empty;
            int attempt = 1;
            while (findAvailableKey)
            {
                if (attempt == 1) // For the firt attempt try just the key
                {
                    tryKey = key;
                }
                else
                {
                    tryKey = string.Format("{0}#{1}", key, attempt.ToString());
                }

                if (builder.Nodes.Count(n => n.Key == tryKey) > 0)
                {
                    attempt++;
                }
                else
                {
                    findAvailableKey = false;
                }
            }

            return tryKey;
        }

        private static IDictionary<string, RegionLanguage> GetLanguages(XElement document)
        {
            var languages = new Dictionary<string, RegionLanguage>();

            foreach (var language in document.Element("languages").Elements())
            {
                languages.Add(language.Attribute("code").Value.ToString(), new RegionLanguage
                {
                    Code = language.Attribute("code").Value.ToString(),
                    Name = language.Attribute("name").Value.ToString(),
                    RestrictTo = language.Attribute("restrictTo") == null ? string.Empty : language.Attribute("restrictTo").Value.ToString()
                });
            }

            return languages;
        }

        override public IDictionary<string, string> GetLanguages()
        {
            GetRootNode();
            return FilterdByLanguage(GetLanguages(_sitemapRoot), _userContext.SelectedLanguage);
        }

        public IDictionary<string, string> GetLanguages(string __region)
        {
            return GetLanguages(__region, _userContext.SelectedLanguage);
        }

        public IDictionary<string, string> GetLanguages(string region, string language)
        {
            string _region = region?.Trim();
            return FilterdByLanguage(GetLanguages(GetRootNode(_region)), language);
        }

        private IDictionary<string, string> FilterdByLanguage(IDictionary<string, RegionLanguage> languages, string language)
        {
            return languages.Where(k => language.IsNullOrEmpty() || k.Value.RestrictTo.IsNullOrEmpty() || k.Value.RestrictTo == language).Select(k => k.Value).ToDictionary(k => k.Code, k => k.Name);
        }

        protected override void OnCreating(SitemapBuilder builder)
        {
            XElement document = GetRootNode();
            LanguagesList = FilterdByLanguage(GetLanguages(document), _userContext.SelectedLanguage);
            SelectedLanguage = LanguagesList.ContainsKey(_userContext.SelectedLanguage) ? _userContext.SelectedLanguage : LanguagesList.ContainsKey(DefaultLanguage) ? DefaultLanguage : LanguagesList.FirstOrDefault().Key;

            SitemapsList = GetSitemaps(document);
            var _sitemap = SitemapsList.ContainsKey(SelectedLanguage) ? SitemapsList[SelectedLanguage] : SitemapsList.ContainsKey(DefaultLanguage) ? SitemapsList[DefaultLanguage] : SitemapsList.FirstOrDefault().Value;

            var rootNode = _sitemap.Element("SitemapNode");

            ParseNode(builder, rootNode);
        }

        private static IDictionary<string, XElement> GetSitemaps(XElement document)
        {
            Dictionary<string, XElement> Sitemaps = new Dictionary<string, XElement>();

            foreach (var sitemap in document.Element("sitemaps").Elements())
            {
                Sitemaps.Add(sitemap.Attribute("code").Value.ToString(), sitemap);
            }

            return Sitemaps;
        }
    }
}