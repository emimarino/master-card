﻿using Mastercard.MarketingCenter.Data.Entities;
using Slam.Cms.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;

namespace Mastercard.MarketingCenter.Web.Core.Controls
{
    public class HeaderUserControl : UserControl
    {
        protected IEnumerable<Segmentation> _segmentations;
        protected IEnumerable<string> _currentSegmentationIds;

        protected string GetSegmentationLabel(string language)
        {
            return _currentSegmentationIds.Any(s => !s.Equals(RegionConfigManager.DefaultSegmentation, StringComparison.InvariantCultureIgnoreCase)) && _segmentations.Any(s => _currentSegmentationIds.Contains(s.SegmentationId)) ?
                    string.Join(" + ", _segmentations.Where(s => _currentSegmentationIds.Contains(s.SegmentationId)).Select(s => GetSegmentationTranslatedLabel(s, language))) :
                    _currentSegmentationIds.Any(s => s.Equals(RegionConfigManager.NoneSegmentation, StringComparison.InvariantCultureIgnoreCase)) ? Resources.Shared.None  : Resources.Shared.All;
        }

        protected string GetSegmentationTranslatedLabel(Segmentation segmentation, string language)
        {
            var content = segmentation.SegmentationTranslatedContents.FirstOrDefault(s => s.LanguageCode.Equals(language, StringComparison.InvariantCultureIgnoreCase));
            if (content != null)
            {
                return content.SegmentationName;
            }

            return segmentation.SegmentationName;
        }

        protected string GetSegmentationActive(string segmentationId)
        {
            return segmentationId.Equals(RegionConfigManager.DefaultSegmentation, StringComparison.InvariantCultureIgnoreCase) ?
                    (_currentSegmentationIds.Any(s => !s.Equals(RegionConfigManager.DefaultSegmentation, StringComparison.InvariantCultureIgnoreCase)) 
                    && (_segmentations.Any(s => _currentSegmentationIds.Contains(s.SegmentationId)) || !_currentSegmentationIds.Contains(segmentationId)) ? string.Empty : "active")
                    : (_currentSegmentationIds.Contains(segmentationId) ? "active" : string.Empty);
        }

        protected string GetSegmentationSelected(string segmentationId)
        {
            return segmentationId.Equals(RegionConfigManager.DefaultSegmentation, StringComparison.InvariantCultureIgnoreCase) ?
                    (_currentSegmentationIds.Any(s => !s.Equals(RegionConfigManager.DefaultSegmentation, StringComparison.InvariantCultureIgnoreCase)) && (_segmentations.Any(s => _currentSegmentationIds.Contains(s.SegmentationId)) || !_currentSegmentationIds.Contains(segmentationId)) ? string.Empty : "selected=\"selected\"") :
                    (_currentSegmentationIds.Contains(segmentationId) ? "selected=\"selected\"" : string.Empty);
        }
    }
}