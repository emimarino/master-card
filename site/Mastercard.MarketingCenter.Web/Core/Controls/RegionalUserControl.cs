﻿using Mastercard.MarketingCenter.Services;
using Slam.Cms;

namespace Mastercard.MarketingCenter.Web.Core.Controls
{
    public class RegionalUserControl : HeaderUserControl
    {
        public UserContext UserContext { get; set; }
        public Sitemap Sitemap { get; set; }
    }
}