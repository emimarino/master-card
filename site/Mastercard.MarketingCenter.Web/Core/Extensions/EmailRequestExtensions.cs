﻿using AWS.Email;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Newtonsoft.Json;
using System;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Core.Extensions
{
    public static class EmailRequestExtensions
    {
        public static EmailRequest LogMailsSent(this EmailRequest mailRequest, User currentUser)
        {
            var mailSentRepository = DependencyResolver.Current.GetService<MailSentRepository>();

            foreach (var recipient in mailRequest.Recipients)
            {
                var emailMessage = new EmailMessage();
                emailMessage.LoadRecipient(recipient);
                emailMessage.BuildMessageFromTemplate(mailRequest.Template);

                mailSentRepository.Add(new MailSent
                {
                    Date = DateTime.Now,
                    MailData = JsonConvert.SerializeObject(emailMessage),
                    UserId = currentUser.UserId
                });
            }

            return mailRequest;
        }
    }
}