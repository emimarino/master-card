﻿using Mastercard.MarketingCenter.Data;
using Slam.Cms.Data;
using System;

namespace Mastercard.MarketingCenter.Web.Core.Extensions
{
    public static class TagTreeNodeExtensions
    {
        public static bool IsMarketTag(this TagTreeNode node)
        {
            bool marketTag = false;
            if (node.Parent.TagCategory == null)
            {
                marketTag = node.Parent.IsMarketTag();
            }
            else
            {
                marketTag = node.Parent.TagCategory.TagCategoryId.Equals(MarketingCenterDbConstants.TagCategories.Markets, StringComparison.OrdinalIgnoreCase);
            }

            return marketTag;
        }
    }
}