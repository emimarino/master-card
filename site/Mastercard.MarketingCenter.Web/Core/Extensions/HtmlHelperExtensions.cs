﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Models;
using Mastercard.MarketingCenter.Web.Core.Models.Report;
using Mastercard.MarketingCenter.Web.Core.Modules;
using Mastercard.MarketingCenter.Web.Registration;
using Slam.Cms;
using Slam.Cms.Common.Interfaces;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Compilation;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.UI;
using Page = System.Web.UI.Page;

namespace Mastercard.MarketingCenter.Web.Core.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static HtmlString RenderControl<T>(this HtmlHelper helper, string path) where T : UserControl
        {
            return RenderControl<T>(helper, path, null);
        }

        public static HtmlString RenderWithRegistrationMasterpage(this HtmlHelper html, string body)
        {
            var page = (LegalContent)BuildManager.CreateInstanceFromVirtualPath(@"~/Registration/LegalContent.aspx", typeof(Page));

            if (page != null)
            {
                page.SetBody(body);

                using (var sw = new StringWriter())
                {
                    HttpContext.Current.Server.Execute(page, sw, false);
                    return new HtmlString(sw.ToString());
                }
            }
            else
            {
                throw new Exception("Could not load Page for legal content.");
            }
        }

        public static HtmlString RenderControl<T>(this HtmlHelper helper, string path, Action<T> action) where T : UserControl
        {
            Page page = new Page();
            T control = (T)page.LoadControl(path);
            page.Controls.Add(control);

            if (action != null)
            {
                action(control);
            }

            using (StringWriter sw = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, sw, false);
                return new HtmlString(sw.ToString());
            }
        }

        public static IHtmlString RenderTrackPageScript(this HtmlHelper helper)
        {
            const string script = "<script>$(function() { $.ajax({ type: 'POST', async: true, url: '/portal/tracking/trackpage' }); });</script>";

            return helper.Raw(script);
        }

        public static IHtmlString ContentItemListItemThumbnailOrIcon(this HtmlHelper helper, ContentItemListItem contentItemListItem, string @class = null)
        {
            return helper.ContentItemListItemImage(
                        new UrlHelper(helper.ViewContext.RequestContext).ContentItemListItemImageUrl(contentItemListItem),
                        @class,
                        new Dictionary<string, object>() { { "max-width", "100%" } }
                    );
        }

        public static IHtmlString LinkToGoalAction(this HtmlHelper helper, RecommendationDTO recommendation, string trackingType)
        {
            return helper.RouteLink(
                        recommendation.DisplayName,
                        "ReportGoal",
                        new { contentItemId = recommendation.ContentItemId, trackingType = trackingType },
                        new { tracking_type = trackingType }
                    );
        }

        public static string GetFlagText(this HtmlHelper helper, ReportFlag reportFlag)
        {
            switch (reportFlag)
            {
                case ReportFlag.Green:
                    return Resources.Optimization.Mastered;
                case ReportFlag.Yellow:
                    return Resources.Optimization.NeedsImprovement;
                case ReportFlag.Red:
                    return Resources.Optimization.NeedsImmediateAttention;
            }

            return string.Empty;
        }

        public static IHtmlString ContentItemListItemIcon(this HtmlHelper helper, ContentItemListItem contentItemListItem, string @class = null)
        {
            var iconService = DependencyResolver.Current.GetService<IIconService>();
            return helper.ContentItemListItemImage(iconService.GetIconUrl(contentItemListItem.IconId), @class, new { width = 69 });
        }

        public static IHtmlString AssetMainImage(this HtmlHelper helper, string fileName, string @class = null)
        {
            return helper.ContentItemListItemImage(new UrlHelper(helper.ViewContext.RequestContext).AssetMainImageUrl(fileName), @class, new { width = 728 });
        }

        public static IHtmlString AssetInBoxMainImage(this HtmlHelper helper, string fileName, string @class = null)
        {
            return helper.ContentItemListItemImage(new UrlHelper(helper.ViewContext.RequestContext).AssetInBoxMainImageUrl(fileName), @class, null);
        }

        public static IHtmlString ContentItemListItemImage(this HtmlHelper helper, string fileName, string @class, object attributes)
        {
            var imageTag = new TagBuilder("img");
            imageTag.MergeAttribute("src", fileName);
            if (!@class.IsNullOrWhiteSpace())
            {
                imageTag.AddCssClass(@class);
            }

            if (attributes is Dictionary<string, object>)
            {
                var attributesDictionary = attributes as Dictionary<string, object>;
                imageTag.MergeAttributes(attributesDictionary);
            }
            else
            {
                var attributesDictionary = new RouteValueDictionary(attributes);
                imageTag.MergeAttributes(attributesDictionary);
            }

            return imageTag.ToString(TagRenderMode.SelfClosing).AsHtmlString();
        }

        private static IEnumerable<string> GetSnippetNames(string value)
        {
            var matches = Regex.Matches(value, @"\[\[(.*?)\]\]");
            return from Match m in matches select m.Value.Replace("[[", string.Empty).Replace("]]", string.Empty);
        }

        public static IHtmlString RawWithSnippets(this HtmlHelper helper, string value, bool filterByLanguage = false)
        {
            if (value == null)
            {
                return new HtmlString(string.Empty);
            }

            value = Regex.Replace(value, "<img(.+?)src=[\"']/mastercard-admin/HtmlImages/(.+?)[\"'](.+?)>", "<img$1src=\"/portal/file/image?url=/mastercard-admin/HtmlImages/$2&width=10000&height=10000&crop=False&jpegQuality=100\"$3>");

            value = value
                        .Replace("<sup>™</sup>", "™")
                        .Replace("™", "<sup>™</sup>")
                        .Replace("<sup>®</sup>", "®")
                        .Replace("®", "<sup>®</sup>")
                        .Replace("<sup>©</sup>", "©")
                        .Replace("©", "<sup>©</sup>");

            var slamContext = WebCoreModule.GetService<SlamContext>();
            var userContext = WebCoreModule.GetService<UserContext>();
            var marketingCenterWebApplicationService = WebCoreModule.GetService<IMarketingCenterWebApplicationService>();
            var snippetNames = GetSnippetNames(value);

            if (!snippetNames.Any())
            {
                return helper.Raw(value);
            }

            foreach (var snippetName in snippetNames)
            {
                var snippetFilter = slamContext?.CreateQuery()
                    .FilterSegmentations(marketingCenterWebApplicationService?.GetCurrentSegmentationIds())
                    .FilterSnippetTagBrowserTypesForCurrentSpecialAudience()
                    .Filter("Snippet.Title = '{0}'".F(snippetName));

                if (filterByLanguage)
                {
                    snippetFilter = snippetFilter?.FilterTaggedWith(userContext?.SelectedLanguage);
                }

                var snippet = snippetFilter?.Get<SnippetDTO>()
                                            .OrderBy(s => s.CreatedDate)
                                            .FirstOrDefault();
                if (snippet != null)
                {
                    value = value.Replace("[[{0}]]".F(snippetName), snippet.Html);
                }
                else
                {
                    value = value.Replace("[[{0}]]".F(snippetName), string.Empty);
                }
            }

            // because a snippet could have references to other snippets
            return helper.RawWithSnippets(value);
        }

        public static MvcHtmlString BuildContentItemSubscriptionManager(this HtmlHelper helper, IEnumerable<ContentItemUserSubscription> contentItemUserSubscriptions)
        {
            var div = new TagBuilder("div");
            div.Attributes.Add("class", "mc-accordion-list pages");

            var sb = new StringBuilder();

            foreach (var contentItemUserSubscription in contentItemUserSubscriptions)
            {
                var slamContext = WebCoreModule.GetService<SlamContext>();
                var contentItem = slamContext.CreateQuery().FilterContentItemId(contentItemUserSubscription.ContentItemId).GetFirstOrDefault();

                if (contentItem != null && (contentItem.StatusID == Status.Published || contentItem.StatusID == Status.PublishedAndDraft))
                {
                    var item = contentItem.AsContentItemListItem();
                    if (item.EnableFavoriting)
                    {
                        sb.AppendFormat(@"<div class='mc-accordion-item subscribed'>
                                              <div class='mc-accordion-title'>
                                                  <a href='javascript:void(0);' class='title-link'>{0}</a>
                                                  <div class='info'>
                                                      <span class='text'></span>
                                                      <a class='action-link all sub' href='javascript:void(0);'><span>Subscribe</span></a>
                                                  </div>
                                              </div>
                                              <input id='{1}' type='hidden' name='SelectedContentItemIds' value='{1}' />
                                          </div>",
                                     item.Title,
                                     contentItemUserSubscription.ContentItemId
                                   );
                    }
                }
            }

            div.InnerHtml = sb.ToString();

            return MvcHtmlString.Create(div.ToString());
        }

        public static MvcHtmlString BuildProfileMyFavoritesPages(this HtmlHelper helper, IEnumerable<ContentItemUserSubscription> contentItemUserSubscriptions)
        {
            var script = new TagBuilder("script");
            var sb = new StringBuilder();

            foreach (var contentItemUserSubscription in contentItemUserSubscriptions)
            {
                var slamContext = WebCoreModule.GetService<SlamContext>();
                var contentItem = slamContext.CreateQuery().FilterContentItemId(contentItemUserSubscription.ContentItemId).GetFirstOrDefault();

                if (contentItem != null && (contentItem.StatusID == Status.Published || contentItem.StatusID == Status.PublishedAndDraft))
                {
                    var item = contentItem.AsContentItemListItem();
                    if (item.EnableFavoriting)
                    {
                        sb.Append("{");
                        sb.AppendFormat(@" id: '{0}', title: '{1}', subscribed: true ", contentItemUserSubscription.ContentItemId, HttpUtility.HtmlEncode(item.Title));
                        sb.Append("},");
                    }
                }
            }

            if (sb.Length > 0)
            {
                sb.Length--;
            }

            script.InnerHtml = string.Format("var pages = [{0}];", sb.ToString());

            return MvcHtmlString.Create(script.ToString());
        }

        public static AttachmentPreviewFile GetAttachmentPreviewFile(this HtmlHelper helper, Attachment attachment)
        {
            helper.GetPreviewFileDetails(attachment.FileUrl, out string previewFileUrl, out string previewFileType);
            return new AttachmentPreviewFile
            {
                Title = attachment.DisplayName,
                Url = previewFileUrl,
                Type = previewFileType,
                Attachment = attachment
            };
        }

        public static DownloadFilePreviewFile GetDownloadFilePreviewFile(this HtmlHelper helper, DownloadFile downloadFile)
        {
            helper.GetPreviewFileDetails(downloadFile.Filename, out string previewFileUrl, out string previewFileType);
            return new DownloadFilePreviewFile
            {
                Title = downloadFile.DisplayName,
                Url = previewFileUrl,
                Type = previewFileType,
                DownloadFile = downloadFile
            };
        }

        public static DownloadImagePreviewFile GetDownloadImagePreviewFile(this HtmlHelper helper, DownloadImage downloadImage)
        {
            helper.GetPreviewFileDetails(downloadImage.ImageUrl, out string previewFileUrl, out string previewFileType, true);
            return new DownloadImagePreviewFile
            {
                Title = downloadImage.ImageTitle,
                Url = previewFileUrl,
                Type = previewFileType,
                DownloadImage = downloadImage
            };
        }

        private static void GetPreviewFileDetails(this HtmlHelper helper, string filePath, out string previewFileUrl, out string previewFileType, bool mapToImagesFolder = false)
        {
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            switch (Path.GetExtension(filePath).Replace(".", "").ToLower())
            {
                case "bmp":
                case "gif":
                case "jpe":
                case "jpeg":
                case "jpg":
                case "png":
                case "tif":
                case "tiff":
                    previewFileUrl = urlHelper.GetPreviewImageUrl(filePath, mapToImagesFolder);
                    previewFileType = "image";
                    break;
                case "pdf":
                    previewFileUrl = urlHelper.GetPreviewFileUrl(filePath);
                    previewFileType = "pdf";
                    break;
                case "zip":
                    var zipFileService = DependencyResolver.Current.GetService<IZipFileService>();
                    previewFileUrl = urlHelper.GetPreviewImageUrl(zipFileService.GetZipPreviewImage(filePath));
                    previewFileType = previewFileUrl != null ? "image" : null;
                    break;
                default:
                    previewFileUrl = null;
                    previewFileType = null;
                    break;
            }
        }

        public static string GetAbsoluteOrRelativeUri(this HtmlHelper helper, string nodeUri, string baseSiteUrl)
        {
            if (nodeUri.ToLowerInvariant().Contains("http://") || nodeUri.ToLowerInvariant().Contains("https://"))
            {
                return nodeUri;
            }

            return baseSiteUrl + nodeUri;
        }

        public static IHtmlString RenderHotjarScript()
        {
            return new HtmlString(GetHotjarScript());
        }

        public static IHtmlString RenderHotjarScript(this HtmlHelper helper)
        {
            return helper.Raw(GetHotjarScript());
        }

        public static string GetHotjarScript()
        {
            var userContext = WebCoreModule.GetService<UserContext>();
            var settingsService = WebCoreModule.GetService<ISettingsService>();

            if ((!userContext.User?.ExcludedFromDomoApi ?? true) &&
                (!userContext.User?.Issuer?.ExcludedFromDomoApi ?? true) &&
                settingsService.GetHotjarEnabled(userContext.SelectedRegion))
            {
                string hotjarId = HttpContext.Current.User.Identity.IsAuthenticated ?
                                  settingsService.GetHotjarId(userContext.SelectedRegion) :
                                  settingsService.GetHotjarDefaultId();

                return $@"<script type=""text/javascript"">
                            (function (h, o, t, j, a, r) {{
                            h.hj = h.hj || function() {{ (h.hj.q = h.hj.q || []).push(arguments) }};
                            h._hjSettings = {{ hjid: {hotjarId}, hjsv: 6 }};
                            a = o.getElementsByTagName('head')[0];
                            r = o.createElement('script'); r.async = 1;
                            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
                            a.appendChild(r);
                            }})(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
                          </script>";
            }

            return string.Empty;
        }

        public static IHtmlString RenderPardotScript()
        {
            return new HtmlString(GetPardotScript());
        }

        public static IHtmlString RenderPardotScript(this HtmlHelper helper)
        {
            return helper.Raw(GetPardotScript());
        }

        public static string GetPardotScript()
        {
            var userContext = WebCoreModule.GetService<UserContext>();
            var settingsService = WebCoreModule.GetService<ISettingsService>();
            var previewMode = WebCoreModule.GetService<IPreviewMode>();

            if (settingsService.GetPardotEnabled(userContext.SelectedRegion, userContext.SelectedLanguage) && !previewMode.Enabled)
            {
                return $@"<script type=""text/javascript"">
                            piAId = '{settingsService.GetPardotAccountId(userContext.SelectedRegion, userContext.SelectedLanguage)}';
                            piCId = '{settingsService.GetPardotCampaignId(userContext.SelectedRegion, userContext.SelectedLanguage)}';
                            piHostname = 'pi.pardot.com';
                            (function() {{
                            function async_load()
                            {{ var s = document.createElement('script'); s.type = 'text/javascript'; s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js'; var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c); }}
                            if(window.attachEvent)
                            {{ window.attachEvent('onload', async_load); }}
                            else {{ window.addEventListener('load', async_load, false); }}
                            }})();
                          </script>";
            }

            return string.Empty;
        }

        public static IHtmlString RenderOneTrustScript()
        {
            return new HtmlString(GetOneTrustScript());
        }

        public static IHtmlString RenderOneTrustScript(this HtmlHelper helper)
        {
            return helper.Raw(GetOneTrustScript());
        }

        public static string GetOneTrustScript()
        {
            var userContext = WebCoreModule.GetService<UserContext>();
            var settingsService = WebCoreModule.GetService<ISettingsService>();

            if (settingsService.GetOneTrustEnabled(userContext.EnvironmentRegion))
            {
                return $@"<!-- OneTrust Cookies Consent Notice start -->
                          <script src=""https://cdn.cookielaw.org/scripttemplates/otSDKStub.js"" data-document-language=""true"" type=""text/javascript"" charset=""UTF-8"" data-domain-script=""{settingsService.GetOneTrustDataDomainScript(userContext.EnvironmentRegion)}""></script>
                          <script type=""text/javascript"">
                            function OptanonWrapper() {{
                                var eOT = new Event('OneTrustGroupsUpdated');
                                document.dispatchEvent(eOT);
                            }}
                          </script>
                          <!-- OneTrust Cookies Consent Notice end -->";
            }

            return string.Empty;
        }

        public static IHtmlString RenderOneTrustButton()
        {
            return new HtmlString(GetOneTrustButton());
        }

        public static IHtmlString RenderOneTrustButton(this HtmlHelper helper)
        {
            return helper.Raw(GetOneTrustButton());
        }

        public static string GetOneTrustButton()
        {
            var userContext = WebCoreModule.GetService<UserContext>();
            var settingsService = WebCoreModule.GetService<ISettingsService>();
            var urlService = WebCoreModule.GetService<IUrlService>();

            if (settingsService.GetOneTrustEnabled(userContext.EnvironmentRegion))
            {
                return $@"{Styles.Render(urlService.GetStaticOneTrustCss())}
                          <!-- OneTrust Cookies Settings button start -->
                          <button id=""ot-sdk-btn"" class=""ot-sdk-show-settings"" data-exclude=""true"">{Resources.Shared.OneTrustCookiesSettingsButton}</button>
                          <!-- OneTrust Cookies Settings button end -->";
            }

            return string.Empty;
        }

        public static IHtmlString RenderAdobeLaunchHeaderScript(string pageName)
        {
            return new HtmlString(GetAdobeLaunchHeaderScript(pageName));
        }

        public static IHtmlString RenderAdobeLaunchHeaderScript(this HtmlHelper helper, string pageName)
        {
            return helper.Raw(GetAdobeLaunchHeaderScript(pageName));
        }

        public static string GetAdobeLaunchHeaderScript(string pageName)
        {
            var userContext = WebCoreModule.GetService<UserContext>();
            var settingsService = WebCoreModule.GetService<ISettingsService>();

            if (settingsService.GetAdobeLaunchEnabled(userContext.EnvironmentRegion))
            {
                return $@"<!—- Adobe data layer and Launch Scripts start -->
                          <script type=""text/javascript"">
                          var di = {{
                            pageName: ""{pageName}"",
                            siteName: ""{settingsService.GetSiteName()}"",
                            region:  ""{(HttpContext.Current.User.Identity.IsAuthenticated ? userContext.SelectedRegion.ToUpperInvariant() : "Global")}"",
                            country: ""{(HttpContext.Current.User.Identity.IsAuthenticated ? userContext.GetUserCountryCode() : "Global")}"",
                            language: ""{userContext.GetSelectedLanguageCode()}"",
                            LoginStatus: ""{(HttpContext.Current.User.Identity.IsAuthenticated ? "LoggedIn" : "Guest")}""
                          }};
                          </script>
                          <script src=""//assets.adobedtm.com/acdc00762da2/433017433345/{settingsService.GetAdobeLaunchJsFile(userContext.EnvironmentRegion)}"" type=""text/plain"" class=""optanon-category-C006""></script>
                          <!—- Adobe data layer and Launch Scripts end -->";
            }

            return string.Empty;
        }

        public static IHtmlString RenderAdobeLaunchBottomScript()
        {
            return new HtmlString(GetAdobeLaunchBottomScript());
        }

        public static IHtmlString RenderAdobeLaunchBottomScript(this HtmlHelper helper)
        {
            return helper.Raw(GetAdobeLaunchBottomScript());
        }

        public static string GetAdobeLaunchBottomScript()
        {
            var userContext = WebCoreModule.GetService<UserContext>();
            var settingsService = WebCoreModule.GetService<ISettingsService>();

            if (settingsService.GetAdobeLaunchEnabled(userContext.EnvironmentRegion))
            {
                return $@"<!—- Adobe Launch bottom scripts start -->
                          <script type=""text/javascript"">
                          function whenAvailable(name, callback) {{
                            var interval = 10; //ms
                            window.setTimeout(function() {{
                                if (window[name]) {{
                                    callback(window[name]);
                                }}
                                else {{
                                    window.setTimeout(arguments.callee, interval);
                                }}
                            }}, interval); 
                          }}
                          whenAvailable(""_satellite"", function(t) {{
                            _satellite.pageBottom();
                          }});
                          </script>
                          <!—- Adobe Launch bottom scripts end -->";
            }

            return string.Empty;
        }
    }
}