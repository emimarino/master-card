﻿using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Core.Extensions
{
    public static class MastercardSitemapExtensions
    {
        public static SitemapNode FilterSitemapByPermissions(this MastercardSitemap mastercardSitemap, bool isAdmin, SitemapNode node = null)
        {
            var _settingsService = DependencyResolver.Current.GetService<ISettingsService>();

            SitemapNode currentSitemapNode = new SitemapNode();
            if (node != null)
            {
                currentSitemapNode = node;
            }

            if (!(isAdmin ? _settingsService.GetAdminFrontEndOffersEnabled(mastercardSitemap.UserContext.SelectedRegion) : _settingsService.GetEndUserFrontEndOffersEnabled(mastercardSitemap.UserContext.SelectedRegion)))
            {
                currentSitemapNode.FilterKeys(new string[] { _settingsService.GetOffersSitemapKey(mastercardSitemap.UserContext.SelectedRegion) });
            }

            if (!(isAdmin ? _settingsService.GetAdminFrontEndMarketingCalendarEnabled(mastercardSitemap.UserContext.SelectedRegion) : _settingsService.GetEndUserFrontEndMarketingCalendarEnabled(mastercardSitemap.UserContext.SelectedRegion)))
            {
                currentSitemapNode.FilterKeys(new string[] { _settingsService.GetMarketingCalendarSitemapKey(mastercardSitemap.UserContext.SelectedRegion) });
            }

            if (!(isAdmin ? _settingsService.GetMerchantsAcquirersAdminEnabled(mastercardSitemap.UserContext.SelectedRegion) : _settingsService.GetMerchantsAcquirersEndUserEnabled(mastercardSitemap.UserContext.SelectedRegion)))
            {
                currentSitemapNode.FilterKeys(new string[] { _settingsService.GetMerchantsAcquirersSitemapKey(mastercardSitemap.UserContext.SelectedRegion) });
            }

            return currentSitemapNode;
        }
    }
}