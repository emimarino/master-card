﻿using System;
using System.Web;

namespace Mastercard.MarketingCenter.Web.Core.Extensions
{
    public static class SecurityExtensions
    {
        public static void DisableCache(this HttpCachePolicy cache)
        {
            cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            cache.SetCacheability(HttpCacheability.NoCache);
            cache.SetNoStore();
        }
    }
}