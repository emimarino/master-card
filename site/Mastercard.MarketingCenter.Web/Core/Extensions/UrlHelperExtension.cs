﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Models;
using System;
using System.IO;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Core
{
    public static class UrlHelperExtension
    {
        private static Random _random = new Random(Guid.NewGuid().GetHashCode());

        public static string ContentItemListItemImageUrl(this UrlHelper urlHelper, ContentItemListItem contentItemListItem)
        {
            return urlHelper.ContentItemListItemImageUrl(contentItemListItem, 135, 135, true, 95);
        }

        public static string ContentItemListItemImageUrl(this UrlHelper urlHelper, ContentItemListItem contentItemListItem, int width, int height, bool crop, int jpegQuality)
        {
            if (string.IsNullOrEmpty(contentItemListItem.ThumbnailUrl))
            {
                var iconService = DependencyResolver.Current.GetService<IIconService>();
                return iconService.GetIconUrl(contentItemListItem.IconId);
            }

            return urlHelper.ImageUrl(contentItemListItem.ThumbnailUrl, width, height, crop, jpegQuality);
        }

        public static string AssetMainImageUrl(this UrlHelper urlHelper, string fileName)
        {
            return urlHelper.ImageUrl(fileName, 728, 1000, false);
        }

        public static string AssetInBoxMainImageUrl(this UrlHelper urlHelper, string fileName)
        {
            return urlHelper.ImageUrl(fileName, 181, 300, false);
        }

        public static string ImageUrl(this UrlHelper urlHelper, string url, int width = 0, int height = 0, bool crop = true, int jpegQuality = 85, bool rootPath = false)
        {
            if (url.IsAbsoluteUrl())
            {
                url = DependencyResolver.Current.GetService<IImageService>().GetImageFromUrl(url, false);
                rootPath = true;
            }

            return urlHelper.Content(DependencyResolver.Current.GetService<IUrlService>().GetStaticImageURL(url, width, height, crop, jpegQuality, rootPath));
        }

        public static string RandomDefaultMyFavoritesContentItemImageUrl(this UrlHelper urlHelper, string defaultUrl)
        {
            var urlService = DependencyResolver.Current.GetService<IUrlService>();
            var totalImages = DependencyResolver.Current.GetService<ISettingsService>().GetDefaultMyFavoritesTotalImages();
            string imageUrl = defaultUrl;
            if (totalImages > 0)
            {
                var index = _random.Next(1, totalImages);
                imageUrl = urlService.GetDefaultMyFavoritesImageURL(index.ToString());
            }

            return imageUrl;
        }

        public static string RandomDefaultMostPopularContentItemImageUrl(this UrlHelper urlHelper, string defaultUrl)
        {
            var urlService = DependencyResolver.Current.GetService<IUrlService>();
            var totalImages = DependencyResolver.Current.GetService<ISettingsService>().GetDefaultMostPopularTotalImages();
            string imageUrl = defaultUrl;
            if (totalImages > 0)
            {
                var index = _random.Next(1, totalImages);
                imageUrl = urlService.GetDefaultMostPopularImageURL(index.ToString());
            }

            return imageUrl;
        }

        public static string AttachmentIconUrl(this UrlHelper urlHelper, string file)
        {
            var tokens = file.Split('.');
            var extension = tokens[tokens.Length - 1].ToLower();
            string icon;
            switch (extension)
            {
                case "pdf": icon = "pc-pdf.gif"; break;
                case "doc":
                case "docx": icon = "pc-doc.gif"; break;
                case "xls":
                case "xlsx": icon = "pc-excel.gif"; break;
                case "ppt":
                case "pptx": icon = "pc-ppt.gif"; break;
                case "zip":
                case "rar": icon = "pc-zip.gif"; break;
                default: icon = "pc-txt.gif"; break;
            }

            return urlHelper.Content("~/content/images/icons/" + icon);
        }

        public static string AttachmentIconClass(this UrlHelper urlHelper, string file)
        {
            var tokens = file.Split('.');
            var extension = tokens[tokens.Length - 1].ToLower();
            string iconClass;
            switch (extension)
            {
                case "pdf": iconClass = "-pdf-"; break;
                case "doc":
                case "docx": iconClass = "-word-"; break;
                case "xls":
                case "xlsx": iconClass = "-exel-"; break;
                case "ppt":
                case "pptx": iconClass = "-powerpoint-"; break;
                case "zip":
                case "rar": iconClass = "-archive-"; break;
                default: iconClass = "-"; break;
            }

            return urlHelper.Content("fa-file" + iconClass + "o");
        }

        public static string GetPreviewImageUrl(this UrlHelper urlHelper, string imageUrl, bool mapToImagesFolder = false)
        {
            if (!imageUrl.IsNullOrWhiteSpace())
            {
                string imagePath;
                if (imageUrl.IsAbsoluteUrl())
                {
                    imagePath = DependencyResolver.Current.GetService<IImageService>().GetImageFromUrl(imageUrl, false);
                }
                else
                {
                    var settingsService = DependencyResolver.Current.GetService<ISettingsService>();
                    imagePath = Path.Combine(mapToImagesFolder ? settingsService.GetImagesFolder() : settingsService.GetFilesFolder(), imageUrl.TrimStart('/').TrimStart('\\'));
                }

                return urlHelper.ImageUrl(imagePath, 458, 500, false, 85, true);
            }

            return string.Empty;
        }

        public static string GetPreviewFileUrl(this UrlHelper urlHelper, string fileUrl)
        {
            return urlHelper.Action("Download", "File", new { url = fileUrl });
        }
    }
}