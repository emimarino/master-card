﻿using System.Collections.Generic;
using System.Linq;
using Mastercard.MarketingCenter.Data;
using Slam.Cms.Common;
using Slam.Cms.Data;
using System.Resources;
using System.Text.RegularExpressions;
using System.Reflection;
using StackExchange.Profiling;
using Mastercard.MarketingCenter.Common.Extensions;

namespace Mastercard.MarketingCenter.Web.Core
{
    public static class TagTreeExtensions
    {
        public static IEnumerable<TagTreeNode> ForMastercardFlyout(this TagTree tagTree)
        {
            using (MiniProfiler.Current.Step("For Mastercard Flyout"))
            {
                var list = new List<TagTreeNode>();

                var column1 = ForFlyoutAddColumn(list);

                var businessObjectives = tagTree.Root.Children.FirstOrDefault(t => t.Id == MarketingCenterDbConstants.TagCategories.BusinessObjectives);

                if (businessObjectives != null)
                {
                    column1.Children.Add(businessObjectives);
                }

                var commChannels = tagTree.Root.Children.FirstOrDefault(t => t.Id == MarketingCenterDbConstants.TagCategories.CommunicationChannels);
                if (commChannels != null)
                {
                    column1.Children.Add(commChannels);
                }

                var insights = tagTree.Root.Children.FirstOrDefault(t => t.Id == MarketingCenterDbConstants.TagCategories.Insights);
                if (insights != null)
                {
                    column1.Children.Add(insights);
                }

                var column2 = ForFlyoutAddColumn(list);

                var segmentAudiences = tagTree.Root.Children.FirstOrDefault(t => t.Id == MarketingCenterDbConstants.TagCategories.SegmentsAudiences);
                if (segmentAudiences != null)
                {
                    column2.Children.Add(segmentAudiences);
                }

                var marketing = tagTree.Root.Children.FirstOrDefault(t => t.Id == MarketingCenterDbConstants.TagCategories.Marketing);
                if (marketing != null)
                {
                    column2.Children.Add(marketing);
                }

                var solutions = tagTree.Root.Children.FirstOrDefault(t => t.Id == MarketingCenterDbConstants.TagCategories.Solutions);
                if (solutions != null)
                {
                    column2.Children.Add(solutions);
                }

                var column3 = ForFlyoutAddColumn(list);

                var cardTypes = tagTree.Root.Children.FirstOrDefault(t => t.Id == MarketingCenterDbConstants.TagCategories.CardTypes);
                if (cardTypes != null)
                {
                    column3.Children.Add(cardTypes);
                }

                var contentTypes = tagTree.Root.Children.FirstOrDefault(t => t.Id == MarketingCenterDbConstants.TagCategories.ContentTypes);
                if (contentTypes != null)
                {
                    column3.Children.Add(contentTypes);
                }

                var markets = tagTree.Root.Children.FirstOrDefault(t => t.Id == MarketingCenterDbConstants.TagCategories.Markets);
                bool includeMarkets = markets != null && markets.Children.Count(n => n.ContentItemCount > 0) > 1;

                var languages = tagTree.Root.Children.FirstOrDefault(t => t.Id == MarketingCenterDbConstants.TagCategories.Languages);
                bool includeLanguages = languages != null && languages.Children.Count(n => n.ContentItemCount > 0) > 1;

                var products = tagTree.Root.Children.Where(t => t.Id == MarketingCenterDbConstants.TagCategories.Products).FirstOrDefault();
                bool includeProducts = products != null;

                var column4 = ForFlyoutAddColumn(list);

                if (includeMarkets)
                {
                    if (includeProducts)
                    {
                        column3.Children.Add(products);
                    }

                    column4.Children.Add(markets);

                    if (includeLanguages)
                    {
                        column4.Children.Add(languages);
                    }
                }
                else
                {
                    if (includeProducts)
                    {
                        column4.Children.Add(products);
                    }
                    if (includeLanguages)
                    {
                        column4.Children.Add(languages);
                    }
                }

                return list;
            }
        }

        private static TagTreeNode ForFlyoutAddColumn(List<TagTreeNode> list)
        {
            var columnNumber = list.Count + 1;
            var newColumn = new TagTreeNode()
            {
                Id = "column{0}".F(columnNumber),
                Text = "Column {0}".F(columnNumber),
                Type = TagTreeNodeType.Special
            };

            list.Add(newColumn);
            return list.Last();
        }

        public static TagTree FlattenMarketTags(this TagTree tagTree)
        {
            using (MiniProfiler.Current.Step("Flatten Market Tags"))
            {
                var regionTags = tagTree.FindNodes(t => t.Tag != null && t.Parent != null && t.Parent.TagCategory != null && t.Parent.TagCategory.TagCategoryId.Equals(MarketingCenterDbConstants.TagCategories.Markets, System.StringComparison.OrdinalIgnoreCase));
                foreach (var regionTag in regionTags)
                {
                    if (regionTag.Children.Count > 0)
                    {
                        regionTag.Parent.Children.Remove(regionTag);
                        foreach (var marketTag in regionTag.Children)
                        {
                            marketTag.Parent = regionTag.Parent;
                            regionTag.Parent.Children.Add(marketTag);
                        }
                    }
                }

                var markets = tagTree.FindNode(t => t.TagCategory != null && t.TagCategory.TagCategoryId.Equals(MarketingCenterDbConstants.TagCategories.Markets, System.StringComparison.OrdinalIgnoreCase));
                var orderedChildren = markets.Children.OrderBy(t => t.Text).ToList();
                markets.Children.Clear();
                foreach (var child in orderedChildren)
                    markets.Children.Add(child);
            }

            return tagTree;
        }

        public static TagTree ExpandRegionMarketTags(this TagTree tagTree, IEnumerable<ContentItem> contentItems)
        {
            using (MiniProfiler.Current.Step("Expand Region Market Tags"))
            {
                foreach (var node in tagTree.FindNodes(n => n.Tag != null && n.Parent != null && n.Parent.Parent != null && n.Parent.Parent.TagCategory != null && n.Parent.Parent.TagCategory.TagCategoryId == MarketingCenterDbConstants.TagCategories.Markets))
                {
                    //Market tag assignments either just are leaf assignments or "root" assignments.
                    //If an item has the tag assigned for the current node or just its parent without other Market tags then it should be counted
                    node.ContentItemCount += contentItems.Count(ci => ci.Tags.Count(t => t.Identifier.In(node.Parent.Identifiers)) == node.Parent.Identifiers.Length || ci.Tags.Any(t => t.Identifier == node.Identifier));
                    if (node.ContentItemCount > 0)
                    {
                        node.Identifier = node.Parent.Identifier + "/" + node.Identifier;
                    }
                };
            }

            return tagTree;
        }

        public static TagTree RemoveTagCategoriesWithSingleTag(this TagTree tagTree, string[] tagCategories)
        {
            using (MiniProfiler.Current.Step("Remove Tag Categories With Single Tag"))
            {
                var categoryNodes = tagTree.FindNodes(t => t.TagCategory != null && tagCategories.Contains(t.TagCategory.TagCategoryId));
                foreach (var categoryNode in categoryNodes)
                {
                    if (categoryNode.Children.Count(n => n.ContentItemCount > 0) < 2 && !categoryNode.Children.Any(n => n.IsSelected))
                    {
                        tagTree.Root.Children.Remove(categoryNode);
                    }
                }
            }
            return tagTree;
        }

        public static TagTree TranslateTagCategories(this TagTree tagTree, string languageCode)
        {
            using (MiniProfiler.Current.Step("Translate Tag Categories"))
            {
                ResourceManager resources = new ResourceManager("Resources.TagCategories", Assembly.Load("App_GlobalResources"));
                foreach (var tagCategory in tagTree.Root.Children)
                {
                    if (tagCategory.TagCategory != null)
                    {
                        tagCategory.Text = resources.GetString(Regex.Replace(tagCategory.TagCategory.Title, "[\\s\\W]", string.Empty), new System.Globalization.CultureInfo(languageCode)) ?? tagCategory.Text;
                    }
                }
            }
            return tagTree;
        }

        public static void TranslateNode(TagTreeNode node, string languageCode)
        {
            if (node.Tag != null)
            {
                node.Tag.Translate(languageCode);
                node.Text = node.Tag.DisplayName;
            }

            foreach (var child in node.Children)
                TranslateNode(child, languageCode);
        }
    }
}