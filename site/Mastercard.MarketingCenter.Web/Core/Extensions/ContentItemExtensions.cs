﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Models;
using Mastercard.MarketingCenter.Web.Core.Modules;
using Pulsus;
using Slam.Cms;
using Slam.Cms.Data;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Core
{
    public static class ContentItemExtensions
    {        
        public static ContentItemListItem AsContentItemListItem(this ContentItem contentItem, Func<ContentItem, bool> isFeatured = null)
        {
            var model = new ContentItemListItem
            {
                ContentType = contentItem.GetType().Name.Replace("DTO", ""),
                ContentItem = contentItem,
                Title = "",
                MainImageUrl = "",
                ThumbnailUrl = "",
                ResourceUrl = "",
                ModifiedDate = contentItem.ModifiedDate,
                Featured = isFeatured != null ? isFeatured(contentItem) : false,
                EnableFavoriting = false,
                ContentNotificationImageUrl = ""
            };

            var asset = contentItem as AssetDTO;
            if (asset != null)
            {
                model.CustomizeContentItemListItem(asset);
            }

            var assetFullWidth = contentItem as AssetFullWidthDTO;
            if (assetFullWidth != null)
            {
                model.CustomizeContentItemListItem(assetFullWidth);
            }

            var downloadableAsset = contentItem as DownloadableAssetDTO;
            if (downloadableAsset != null)
            {
                model.CustomizeContentItemListItem(downloadableAsset);
            }

            var orderableAsset = contentItem as OrderableAssetDTO;
            if (orderableAsset != null)
            {
                model.CustomizeContentItemListItem(orderableAsset);
            }

            var program = contentItem as ProgramDTO;
            if (program != null)
            {
                model.CustomizeContentItemListItem(program);
            }

            var webinar = contentItem as WebinarDTO;
            if (webinar != null)
            {
                model.CustomizeContentItemListItem(webinar);
            }

            var link = contentItem as Data.Entities.Link;
            if (link != null)
            {
                model.CustomizeContentItemListItem(link);
            }

            var youtubeVideo = contentItem as YoutubeVideoDTO;
            if (youtubeVideo != null)
            {
                model.CustomizeContentItemListItem(youtubeVideo);
            }

            var marquee = contentItem as MarqueeSlideDTO;
            if (marquee != null)
            {
                model.CustomizeContentItemListItem(marquee);
            }

            var recommendation = contentItem as RecommendationDTO;
            if (recommendation != null)
            {
                model.CustomizeContentItemListItem(recommendation);
            }

            var page = contentItem as PageDTO;
            if (page != null)
            {
                model.CustomizeContentItemListItem(page);
            }

            var offer = contentItem as OfferDTO;
            if (offer != null)
            {
                model.CustomizeContentItemListItem(offer);
            }

            return model;
        }

        public static bool IsMarqueeSlideItem(this ContentItemListItem item)
        {
            return IsMarqueeSlide(item.ContentType);
        }

        public static bool IsMarqueeSlideItem(this CarouselSlideItem item)
        {
            return IsMarqueeSlide(item.ContentType);
        }

        private static bool IsMarqueeSlide(string contentType)
        {
            return contentType.Equals("MarqueeSlide", StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool IsWebinarSlideItem(this ContentItemListItem item)
        {
            return IsWebinar(item.ContentType);
        }

        public static bool IsWebinarSlideItem(this CarouselSlideItem item)
        {
            return IsWebinar(item.ContentType);
        }

        private static bool IsWebinar(string contentType)
        {
            return contentType.Equals("Webinar", StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool IsYoutubeVideoSlideItem(this ContentItemListItem item)
        {
            return IsYoutubeVideo(item.ContentType);
        }

        public static bool IsYoutubeVideoSlideItem(this CarouselSlideItem item)
        {
            return IsYoutubeVideo(item.ContentType);
        }

        private static bool IsYoutubeVideo(string contentType)
        {
            return contentType.Equals("YoutubeVideo", StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool IsImageSlideItem(this ContentItemListItem item)
        {
            return IsImage(item.ContentType);
        }

        public static bool IsImageSlideItem(this CarouselSlideItem item)
        {
            return IsImage(item.ContentType);
        }

        private static bool IsImage(string contentType)
        {
            return contentType.Equals("Image", StringComparison.InvariantCultureIgnoreCase);
        }

        private static void CustomizeContentItemListItem(this ContentItemListItem model, YoutubeVideoDTO video)
        {
            model.Title = video.Title;
            model.ContentType = "YoutubeVideo";
            model.Order = video.OrderNumber;
            model.ResourceUrl = GetYoutubeVideoUrl(video);
            model.ThumbnailUrl = GetYoutubeVideoImage(model.ResourceUrl);
        }

        private static void CustomizeContentItemListItem(this ContentItemListItem model, MarqueeSlideDTO marquee)
        {
            model.Title = marquee.Title;
            model.ThumbnailUrl = marquee.Image;
            model.ResourceUrl = marquee.MainUrl;
            model.ContentType = "MarqueeSlide";
            model.Order = marquee.OrderNumber;
            model.Header = marquee.LongDescription;
        }

        private static void CustomizeContentItemListItem(this ContentItemListItem model, AssetDTO asset)
        {
            model.Title = asset.Title;
            model.Summary = asset.ShortDescription;
            model.MainImageUrl = asset.MainImage;
            model.ThumbnailUrl = !asset.ThumbnailImage.IsNullOrWhiteSpace() ? asset.ThumbnailImage : asset.MainImage;
            int order;
            int.TryParse(asset.PriorityLevel, out order);
            model.Order = order;

            model.IconId = asset.IconID;
            model.CustomizeResourceUrl(asset.FrontEndUrl);

            model.EnableFavoriting = asset.EnableFavoriting;
            model.ContentNotificationImageUrl = !asset.MainImage.IsNullOrWhiteSpace() ? asset.MainImage : asset.ThumbnailImage;
        }

        private static void CustomizeContentItemListItem(this ContentItemListItem model, AssetFullWidthDTO assetFullWidth)
        {
            model.Title = assetFullWidth.Title;
            model.Summary = assetFullWidth.ShortDescription;
            model.ThumbnailUrl = assetFullWidth.ThumbnailImage;
            int order;
            int.TryParse(assetFullWidth.PriorityLevel, out order);
            model.Order = order;

            model.IconId = assetFullWidth.IconID;
            model.CustomizeResourceUrl(assetFullWidth.FrontEndUrl);

            model.EnableFavoriting = assetFullWidth.EnableFavoriting;
            model.ContentNotificationImageUrl = assetFullWidth.ThumbnailImage;
        }

        private static void CustomizeContentItemListItem(this ContentItemListItem model, DownloadableAssetDTO downloadableAsset)
        {
            model.Title = downloadableAsset.Title;
            model.Summary = downloadableAsset.ShortDescription;
            model.MainImageUrl = downloadableAsset.MainImage;
            model.ThumbnailUrl = !downloadableAsset.ThumbnailImage.IsNullOrWhiteSpace() ? downloadableAsset.ThumbnailImage : downloadableAsset.MainImage;
            int order;
            int.TryParse(downloadableAsset.PriorityLevel, out order);
            model.Order = order;

            model.IconId = downloadableAsset.IconID;
            model.CustomizeResourceUrl(downloadableAsset.FrontEndUrl);

            model.EnableFavoriting = downloadableAsset.EnableFavoriting;
            model.ContentNotificationImageUrl = !downloadableAsset.MainImage.IsNullOrWhiteSpace() ? downloadableAsset.MainImage : downloadableAsset.ThumbnailImage;
        }

        private static void CustomizeContentItemListItem(this ContentItemListItem model, ProgramDTO program)
        {
            model.Title = program.Title;
            model.Summary = program.ShortDescription;
            model.MainImageUrl = program.MainImage;
            model.ThumbnailUrl = !program.ThumbnailImage.IsNullOrWhiteSpace() ? program.ThumbnailImage : program.MainImage;
            int order;
            int.TryParse(program.PriorityLevel, out order);
            model.Order = order;

            model.IconId = program.IconID;
            model.CustomizeResourceUrl(program.FrontEndUrl);

            model.EnableFavoriting = program.EnableFavoriting;
            model.ContentNotificationImageUrl = !program.MainImage.IsNullOrWhiteSpace() ? program.MainImage : program.ThumbnailImage;
        }

        private static void CustomizeContentItemListItem(this ContentItemListItem model, RecommendationDTO recommendation)
        {
            model.Title = recommendation.DisplayName;
            model.Summary = recommendation.ShortDescription;

            var tag = recommendation.Tags.FirstOrDefault();
            var url = "report/goal/";
            if (tag != null)
                url += tag.Identifier;

            model.CustomizeResourceUrl(url);
        }

        private static void CustomizeContentItemListItem(this ContentItemListItem model, OrderableAssetDTO orderableAsset)
        {
            model.Title = orderableAsset.Title;
            model.Summary = orderableAsset.ShortDescription;
            model.MainImageUrl = orderableAsset.MainImage;
            model.ThumbnailUrl = !orderableAsset.ThumbnailImage.IsNullOrWhiteSpace() ? orderableAsset.ThumbnailImage : !orderableAsset.MainImage.IsNullOrWhiteSpace() ? orderableAsset.MainImage : orderableAsset.PDFPreview;
            int order;
            int.TryParse(orderableAsset.PriorityLevel, out order);
            model.Order = order;

            model.IconId = orderableAsset.IconID;
            model.CustomizeResourceUrl(orderableAsset.FrontEndUrl);

            model.EnableFavoriting = orderableAsset.EnableFavoriting;
            model.ContentNotificationImageUrl = !orderableAsset.MainImage.IsNullOrWhiteSpace() ? orderableAsset.MainImage : !orderableAsset.PDFPreview.IsNullOrWhiteSpace() ? orderableAsset.PDFPreview : orderableAsset.ThumbnailImage;
        }

        private static void CustomizeContentItemListItem(this ContentItemListItem model, WebinarDTO webinar)
        {
            model.Title = webinar.Title;
            model.Summary = webinar.ShortDescription;
            model.ThumbnailUrl = !webinar.ThumbnailImage.IsNullOrWhiteSpace() ? webinar.ThumbnailImage : webinar.StaticVideoImage;
            int order;
            int.TryParse(webinar.PriorityLevel, out order);
            model.Order = order;

            model.IconId = webinar.IconID;
            model.CustomizeResourceUrl(webinar.FrontEndUrl);

            model.EnableFavoriting = webinar.EnableFavoriting;
            model.ContentNotificationImageUrl = !webinar.StaticVideoImage.IsNullOrWhiteSpace() ? webinar.StaticVideoImage : webinar.ThumbnailImage;
        }

        private static void CustomizeContentItemListItem(this ContentItemListItem model, PageDTO page)
        {
            model.Title = page.Title;
            if (!page.ShortDescription.IsNullOrWhiteSpace() && !page.ShortDescription.StripHtml().IsNullOrWhiteSpace())
            {
                model.Summary = page.ShortDescription;
            }
            else if (!page.HeaderParagraph.IsNullOrWhiteSpace() && !page.HeaderParagraph.StripHtml().IsNullOrWhiteSpace())
            {
                model.Summary = page.HeaderParagraph;
            }
            model.MainImageUrl = page.HeaderBackgroundImage;

            model.CustomizeResourceUrl(page.FrontEndUrl);

            model.EnableFavoriting = page.EnableFavoriting;
            model.ContentNotificationImageUrl = !page.HeaderBackgroundImage.IsNullOrWhiteSpace() ? page.HeaderBackgroundImage : !page.HeaderAsideImage.IsNullOrWhiteSpace() ? page.HeaderAsideImage : !page.BottomBackgroundImage.IsNullOrWhiteSpace() ? page.BottomBackgroundImage : page.BottomAsideImage;

            model.ThumbnailUrl = model.ContentNotificationImageUrl;
        }

        private static void CustomizeContentItemListItem(this ContentItemListItem model, OfferDTO offer)
        {
            model.Title = offer.Title;            
            model.Summary = offer.ShortDescription;                       
            model.MainImageUrl = offer.MainImage;
            model.CustomizeResourceUrl(offer.FrontEndUrl);            
            model.ThumbnailUrl = model.ContentNotificationImageUrl;
        }

        private static void CustomizeContentItemListItem(this ContentItemListItem model, Data.Entities.Link link)
        {
            if (link.ContentItemId != link.PrimaryContentItemId)
            {
                return;
            }

            model.Title = link.Title;
            model.Summary = link.ShortDescription;
            model.ThumbnailUrl = link.ThumbnailImage;
            int.TryParse(link.PriorityLevel, out int order);
            model.Order = order;

            var slamContext = WebCoreModule.GetService<SlamContext>();
            if (slamContext != null)
            {
                var linkTags = slamContext.CreateQuery()
                    .FilterContentItemId(link.ContentItemId)
                    .IncludeTags()
                    .Get<Data.Entities.Link>()
                    .FirstOrDefault()
                    .Tags;

                model.ResourceUrl = (new UrlHelper(HttpContext.Current.Request.RequestContext)).TagBrowser("tagbrowser")
                                                                                               .AddTags(linkTags.Select(t => t.Identifier))
                                                                                               .Render();
            }
            else
            {
                model.CustomizeResourceUrl(link.FrontEndUrl);
            }
        }        

        private static void CustomizeResourceUrl(this ContentItemListItem model, string resourceUrl)
        {
            model.ResourceUrl = $"/portal/{resourceUrl.TrimStart('/')}";
        }

        public static CarouselSlideItem AsCarouselSlideItem(this string imageUrl)
        {
            var model = new CarouselSlideItem
            {
                Title = string.Empty,
                Image = imageUrl,
                Description = string.Empty,
                InnerDescription = string.Empty,
                ButtonText = string.Empty,
                Url = string.Empty,
                Order = 0,
                ContentType = "Image"
            };

            return model;
        }        

        public static CarouselSlideItem AsCarouselSlideItem(this ContentItem contentItem)
        {
            var model = new CarouselSlideItem
            {
                Title = string.Empty,
                Image = string.Empty,
                Description = string.Empty,
                InnerDescription = string.Empty,
                ButtonText = string.Empty,
                Url = string.Empty,
                Order = 0,
                ContentType = contentItem.GetType().Name
            };

            var marquee = contentItem as MarqueeSlideDTO;
            if (marquee != null)
            {
                model.CustomizeCarouselSlideItem(marquee);
            }

            var youtubeVideo = contentItem as YoutubeVideoDTO;
            if (youtubeVideo != null)
            {
                model.CustomizeCarouselSlideItem(youtubeVideo);
            }

            return model;
        }

        private static void CustomizeCarouselSlideItem(this CarouselSlideItem model, MarqueeSlideDTO marquee)
        {
            model.Title = marquee.Title;
            model.Image = marquee.Image;
            model.Description = marquee.LongDescription;
            model.InnerDescription = marquee.InnerDescription;
            model.ButtonText = marquee.ButtonText;
            model.Url = marquee.MainUrl;
            model.Order = marquee.OrderNumber;
            model.ContentType = "MarqueeSlide";
        }

        private static void CustomizeCarouselSlideItem(this CarouselSlideItem model, YoutubeVideoDTO video)
        {            
            model.Title = video.Title;
            model.Url = GetYoutubeVideoUrl(video);
            model.Image = video.StaticVideoImage.IsNullOrEmpty() ? "https://img.youtube.com/vi/" + GetYoutubeVideoImage(model.Url) : DependencyResolver.Current.GetService<IUrlService>().GetStaticImageURL(video.StaticVideoImage, 1400, 468, true, 95, false);
            model.Order = video.OrderNumber;
            model.ContentType = "YoutubeVideo";
        }

        private static string GetYoutubeVideoUrl(YoutubeVideoDTO video)
        {
            Uri uri;
            if (Uri.TryCreate(video.Url, UriKind.Absolute, out uri))
            {
                if (!string.IsNullOrEmpty(uri.Query) && HttpUtility.ParseQueryString(uri.Query)["v"] != null)
                {
                    return HttpUtility.ParseQueryString(uri.Query)["v"];
                }
                else
                {
                    return uri.Segments.Last().Trim('/');
                }
            }
            else
            {
                LogManager.EventFactory.Create()
                    .Level(LoggingEventLevel.Error)
                    .Text($"Cannot parse the URL: {video.Url} for Youtube Content Item with ID: {video.ContentItemId}")
                    .AddData("YoutubeContentItem", video)
                    .Push();
            }

            return null;
        }

        private static string GetYoutubeVideoImage(string youtubeVideoUrl)
        {
            if (!youtubeVideoUrl.IsNullOrWhiteSpace())
            {
                return $"{youtubeVideoUrl.TrimEnd('/')}/maxresdefault.jpg";
            }

            return null;
        }
    }
}