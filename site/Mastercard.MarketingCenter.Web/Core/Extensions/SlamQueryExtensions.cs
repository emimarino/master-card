﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Web.Core.Modules;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mastercard.MarketingCenter.Web.Core
{
    public static class SlamQueryExtensions
    {
        private static string tagBrowserTypesFilterTemplate = "Asset.{0}, AssetFullWidth.{0}, DownloadableAsset.{0}, OrderableAsset.{0}, Program.{0}, Link.{0}, Webinar.{0}";

        public static SlamQuery FilterSnippetTagBrowserTypesForCurrentSpecialAudience(this SlamQuery slamQuery, string audience = null)
        {
            var currentSpecialAudience = string.Empty;

            if (audience.IsNullOrEmpty())
            {
                var service = WebCoreModule.GetService<IMarketingCenterWebApplicationService>();
                if (service != null)
                {
                    currentSpecialAudience = service.GetCurrentSpecialAudience();
                }
            }
            else
            {
                currentSpecialAudience = audience;
            }

            string[] stringSeparators = new string[] { ";#" };

            var specialAudiences = currentSpecialAudience.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

            var filter = string.Empty;

            filter = @"( (LEN(COALESCE(
                            Snippet.{0}, '')) = 0)".F("RestrictToSpecialAudience");

            for (int i = 0; i < specialAudiences.Count(); i++)
            {
                filter += @"
                            OR
                            (COALESCE(
                            Snippet.{0}, '') like '%;#{1};#%')".F("RestrictToSpecialAudience", specialAudiences[i]);
            }

            filter += ")";

            return slamQuery.Filter(filter);
        }

        public static SlamQuery FilterTagBrowserTypesShowInTagBrowser(this SlamQuery slamQuery)
        {
            var filter =
                    @"(
                        (COALESCE(
                        Asset.{0}, 
                        AssetFullWidth.{0},
                        DownloadableAsset.{0},
                        OrderableAsset.{0},
                        Program.{0},
                        Link.{0},
                        Webinar.{0}, 1) = 1)
                      )".F("ShowInTagBrowser");

            return slamQuery.Filter(filter);
        }

        public static SlamQuery FilterTagBrowserTypes(this SlamQuery slamQuery)
        {
            return slamQuery.FilterContentTypes(
                typeof(AssetDTO),
                typeof(AssetFullWidthDTO),
                typeof(DownloadableAssetDTO),
                typeof(OrderableAssetDTO),
                typeof(ProgramDTO),
                typeof(Link),
                typeof(WebinarDTO));
        }

        public static SlamQuery FilterRelatedItemTypes(this SlamQuery slamQuery)
        {
            return slamQuery.FilterContentTypes(
                typeof(AssetDTO),
                typeof(AssetFullWidthDTO),
                typeof(DownloadableAssetDTO),
                typeof(OrderableAssetDTO),
                typeof(ProgramDTO),
                typeof(PageDTO),
                typeof(WebinarDTO),
                typeof(OfferDTO));
        }

        public static SlamQuery FilterMarketingTabPageTypes(this SlamQuery slamQuery)
        {
            return slamQuery.FilterContentTypes(
                typeof(ProgramDTO),
                typeof(Link));
        }

        public static SlamQuery FilterShowInCrossSellBox(this SlamQuery slamQuery, string crossSellBox)
        {
            var filter = new StringBuilder("(");
            filter.AppendFormat("Asset.ShowInCrossSellBox = '{0}' or ", crossSellBox);
            filter.AppendFormat("AssetFullWidth.ShowInCrossSellBox = '{0}' or ", crossSellBox);
            filter.AppendFormat("DownloadableAsset.ShowInCrossSellBox = '{0}' or ", crossSellBox);
            filter.AppendFormat("OrderableAsset.ShowInCrossSellBox = '{0}' or ", crossSellBox);
            filter.AppendFormat("Program.ShowInCrossSellBox = '{0}' or ", crossSellBox);
            filter.AppendFormat("Link.ShowInCrossSellBox = '{0}' or ", crossSellBox);
            filter.AppendFormat("Webinar.ShowInCrossSellBox = '{0}'", crossSellBox);
            filter.Append(")");
            return slamQuery.Filter(filter.ToString());
        }

        public static SlamQuery FilterEnableFavoritingTypes(this SlamQuery slamQuery)
        {
            return slamQuery.FilterContentTypes(
                typeof(AssetDTO),
                typeof(AssetFullWidthDTO),
                typeof(DownloadableAssetDTO),
                typeof(OrderableAssetDTO),
                typeof(PageDTO),
                typeof(ProgramDTO),
                typeof(WebinarDTO));
        }

        public static SlamQuery FilterByEnableFavoriting(this SlamQuery slamQuery, bool enableFavoriting)
        {
            var filter = new StringBuilder("(");
            filter.AppendFormat($"Asset.EnableFavoriting = '{enableFavoriting}' or ");
            filter.AppendFormat($"AssetFullWidth.EnableFavoriting = '{enableFavoriting}' or ");
            filter.AppendFormat($"DownloadableAsset.EnableFavoriting = '{enableFavoriting}' or ");
            filter.AppendFormat($"OrderableAsset.EnableFavoriting = '{enableFavoriting}' or ");
            filter.AppendFormat($"Page.EnableFavoriting = '{enableFavoriting}' or ");
            filter.AppendFormat($"Program.EnableFavoriting = '{enableFavoriting}' or ");
            filter.AppendFormat($"Webinar.EnableFavoriting = '{enableFavoriting}'");
            filter.Append(")");
            return slamQuery.Filter(filter.ToString());
        }

        public static SlamQuery TagBrowserTypesOrderBy(this SlamQuery slamQuery)
        {
            var filter = ("SharedTags DESC, COALESCE(" + tagBrowserTypesFilterTemplate + ", 100) ASC").F("PriorityLevel");
            return slamQuery.OrderBy(filter);
        }

        public static SlamQuery TagBrowserTypesOrderBy(this SlamQuery slamQuery, IEnumerable<string> tagIdentifiers)
        {
            if (tagIdentifiers != null && tagIdentifiers.Count() > 0)
            {
                return slamQuery.OrderBy(("SharedTags DESC, COALESCE(" + tagBrowserTypesFilterTemplate + ", 100) ASC").F("PriorityLevel"));
            }
            else
            {
                return slamQuery.OrderBy(("COALESCE(" + tagBrowserTypesFilterTemplate + ", 100) ASC").F("PriorityLevel"));
            }
        }

        public static SlamQuery OrderByFeatureAndPriorityLevel(this SlamQuery slamQuery)
        {
            var filter = ("FeatureLevel DESC, COALESCE(" + tagBrowserTypesFilterTemplate + ", 100) ASC").F("PriorityLevel");
            return slamQuery.OrderBy(filter);
        }

        public static SlamQuery FilterExpiredAssets(this SlamQuery slamQuery)
        {
            return slamQuery.Filter(
                @"(
		            ContentItem.ContentItemId NOT IN (
			            (
				            SELECT CASE 
						            WHEN orderableassetcontentitemid IS NOT NULL
							            THEN orderableassetcontentitemid
						            WHEN downloadableassetcontentitemid IS NOT NULL
							            THEN downloadableassetcontentitemid
						            ELSE p.contentitemid
						            END
				            FROM (
					            SELECT CASE 
							            WHEN printedorderperiodenddate IS NULL
								            THEN onlineorderperiodenddate
							            WHEN onlineorderperiodenddate IS NULL
								            THEN printedorderperiodenddate
							            WHEN printedorderperiodenddate >= onlineorderperiodenddate
								            THEN printedorderperiodenddate
							            WHEN onlineorderperiodenddate >= printedorderperiodenddate
								            THEN onlineorderperiodenddate
							            END AS expdate
						            ,p.contentitemid
					            FROM program p
					            JOIN contentitem ci ON p.ContentItemId = ci.ContentItemId
					            WHERE ci.StatusID NOT IN (
							            8
							            ,6
							            )
					            ) AS pd
				            LEFT JOIN orderableassetrelatedprogram oa ON pd.contentitemid = oa.programcontentitemid
				            LEFT JOIN downloadableassetrelatedprogram da ON pd.contentitemid = da.programcontentitemid
				            LEFT JOIN program p ON pd.contentitemid = p.contentitemid
				            WHERE expdate < GETDATE()
				            )
			
			            UNION
			
			            (
				            SELECT pd.contentitemid
				            FROM (
					            SELECT CASE 
							            WHEN printedorderperiodenddate IS NULL
								            THEN onlineorderperiodenddate
							            WHEN onlineorderperiodenddate IS NULL
								            THEN printedorderperiodenddate
							            WHEN printedorderperiodenddate >= onlineorderperiodenddate
								            THEN printedorderperiodenddate
							            WHEN onlineorderperiodenddate >= printedorderperiodenddate
								            THEN onlineorderperiodenddate
							            END AS expdate
						            ,p.contentitemid
					            FROM program p
					            JOIN contentitem ci ON p.ContentItemId = ci.ContentItemId
					            WHERE ci.StatusID NOT IN (
							            8
							            ,6
							            )
					            ) AS pd
				            WHERE expdate < GETDATE()
				            )
			            )
		            )");
        }

        public static SlamQuery FilterTagBrowserTypesForSpecialAudience(this SlamQuery slamQuery, string audience = null)
        {
            var currentSpecialAudience = string.Empty;

            if (audience.IsNullOrEmpty())
            {
                var service = WebCoreModule.GetService<IMarketingCenterWebApplicationService>();
                if (service != null)
                {
                    currentSpecialAudience = service.GetCurrentSpecialAudience();
                }
            }
            else
            {
                currentSpecialAudience = audience;
            }

            string[] stringSeparators = new string[] { ";#" };

            var specialAudiences = currentSpecialAudience.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

            string filter = @"( (LEN(COALESCE(
                            Asset.{0}, 
                            AssetFullWidth.{0},
                            DownloadableAsset.{0},
                            OrderableAsset.{0},
                            Program.{0},
                            Webinar.{0}, '')) = 0)".F("RestrictToSpecialAudience");

            for (int i = 0; i < specialAudiences.Count(); i++)
            {
                filter += @"
                            OR
                            (COALESCE(
                            Asset.{0}, 
                            AssetFullWidth.{0},
                            DownloadableAsset.{0},
                            OrderableAsset.{0},
                            Program.{0},
                            Webinar.{0}, '') like '%;#{1};#%')".F("RestrictToSpecialAudience", specialAudiences[i]);
            }

            filter += ")";

            return slamQuery.Filter(filter);
        }

        public static SlamQuery FilterMarqueeSlideTagBrowserTypesForSpecialAudience(this SlamQuery slamQuery, string audience = null)
        {
            var currentSpecialAudience = string.Empty;

            if (audience.IsNullOrEmpty())
            {
                var service = WebCoreModule.GetService<IMarketingCenterWebApplicationService>();
                if (service != null)
                {
                    currentSpecialAudience = service.GetCurrentSpecialAudience();
                }
            }
            else
            {
                currentSpecialAudience = audience;
            }

            string[] stringSeparators = new string[] { ";#" };

            var specialAudiences = currentSpecialAudience.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

            var filter = string.Empty;

            filter = @"( (LEN(COALESCE(
                            MarqueeSlide.{0}, '')) = 0)".F("RestrictToSpecialAudience");

            for (int i = 0; i < specialAudiences.Count(); i++)
            {
                filter += @"
                            OR
                            (COALESCE(
                            MarqueeSlide.{0}, '') like '%;#{1};#%')".F("RestrictToSpecialAudience", specialAudiences[i]);
            }

            filter += ")";

            return slamQuery.Filter(filter);
        }

        public static SlamQuery FilterByExpiration(this SlamQuery slamQuery)
        {
            var filter = string.Format("(ContentItem.ExpirationDate is null or ContentItem.ExpirationDate >= '{0}')",
                                        DateTime.Now.RemoveTimePart().ToStringForQuery());

            return slamQuery.Filter(filter);
        }

        public static SlamQuery FilterByCrossBorderRegion(this SlamQuery slamQuery, string region)
        {
            return slamQuery.Filter($@"(ContentItem.RegionId = '{region}' OR ContentItem.ContentItemID IN
                                       (SELECT ContentItemId FROM {MarketingCenterDbConstants.Tables.ContentItemCrossBorderRegion}
                                        WHERE RegionId = '{region}'))");
        }

        public static SlamQuery FilterByRegionVisibility(this SlamQuery slamQuery, string region)
        {
            return slamQuery.Filter($@"(ContentItem.ContentItemId NOT IN
                                       (SELECT ContentItemId FROM {MarketingCenterDbConstants.Tables.ContentItemRegionVisibility}
                                        WHERE RegionId = '{region}' AND Value <> 1))");
        }
    }
}