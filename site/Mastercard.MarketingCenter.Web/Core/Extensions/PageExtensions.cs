﻿using System.Reflection;
using System.Web.UI;

namespace Mastercard.MarketingCenter.Web.Core.Extensions
{
    public static class PageExtensions
    {
        public static string GetSiteName(this Page page)
        {
            var resources = new System.Resources.ResourceManager("Resources.Shared", Assembly.Load("App_GlobalResources"));
            return resources.GetString(Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["ApplicationNameResource"]) ?? Resources.Shared.MosSiteName;
        }
    }
}