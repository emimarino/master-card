﻿using System;
using System.Web.Configuration;

namespace Mastercard.MarketingCenter.Web.Core
{
    public static class Constants
    {
        public static string DefaultSitemapNodeKey { get { return "home"; } }

        public static class Configuration
        {
            public static readonly int GetInsightsRssFeedPageSize =
                Convert.ToInt32(WebConfigurationManager.AppSettings["InsightsRssFeed.PageSize"]) == 0
                    ? 4
                    : Convert.ToInt32(WebConfigurationManager.AppSettings["InsightsRssFeed.PageSize"]);
        }

        public static class Urls
        {
            public const string InsightsOverview = "/portal/insights";
            public const string MarketingOverview = "/portal/marketing";
            public const string ProductsOverview = "/portal/products";
            public const string SafetySecurityOverview = "/portal/safety-security";
            public const string SegmentsOverview = "/portal/segments";
            public const string SolutionsOverview = "/portal/solutions";
            public const string ThoughtLeadershipOverview = "/portal/thoughtleadership";
            public const string MastercardManagedServices = "/portal/page/mastercardmanagedservices";
            public const string MastercardB2BAnalytics = "/portal/program/f7c6/mastercard-b2b-analytics";
        }

        public static class EDMTokens
        {
            public const string UserName = "[[User Name]]";
        }
    }
}