﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using System.Linq;
using System.Security.Principal;
using System.Web.Routing;

namespace Mastercard.MarketingCenter.Web.Core.Services
{
    public class AuthorizationService
    {
        private WebRoleAccessRepository _webRoleAccessDao { get; set; }
        private IPrincipal _user { get; set; }

        public AuthorizationService(WebRoleAccessRepository webRoleAccessDao, IPrincipal user)
        {
            _webRoleAccessDao = webRoleAccessDao;
            _user = user;
        }

        public bool Authorize(RouteData routeData)
        {
            string area = (routeData.Values["Area"] == null) ? string.Empty : routeData.Values["Area"].ToString();
            string controller = (routeData.Values["Controller"] == null) ? string.Empty : routeData.Values["Controller"].ToString();
            string action = (routeData.Values["Action"] == null) ? "Index" : routeData.Values["Action"].ToString();
            string id = (routeData.Values["Id"] == null) ? string.Empty : routeData.Values["Id"].ToString();

            if (string.Equals(controller, "Home", System.StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }

            var accessibleRoutes = _webRoleAccessDao.GetByRoute(area, controller, action, id).ToList();

            return !accessibleRoutes.Any() || accessibleRoutes.Any(a => (string.IsNullOrEmpty(a.RoleType) || _user.IsInRole(a.RoleType)) && (string.IsNullOrEmpty(a.RequiredRolePermissionName) || _user.IsInPermission(a.RequiredRolePermissionName)));
        }
    }
}