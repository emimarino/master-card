﻿using Mastercard.MarketingCenter.Web.Controllers;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Mastercard.MarketingCenter.Web.Core.Services
{
    public static class HeaderService
    {
        static IControllerFactory Factory { get { return DependencyResolver.Current.GetService<IControllerFactory>() ?? new DefaultControllerFactory(); ; } }

        public static string Header(HttpContext context, bool canChangeRegions = true, bool canSearchContent = true, string region = null, string language = null)
        {
            return RenderPartial("Header", context, canChangeRegions, canSearchContent, region, language);
        }

        public static string RenderPartial(string partialName, HttpContext context, bool canChangeRegions, bool canSearchContent, string region, string language)
        {
            var sb = new StringBuilder();
            TextWriter writer = new StringWriter(sb);

            var rt = new RouteData();
            rt.Values.Add("controller", "header");
            rt.Values.Add("action", partialName);

            var httpCtx = new HttpContextWrapper(context);
            var headerController = new HeaderController(region, language);
            var ctx = new ControllerContext(new RequestContext(httpCtx, rt), headerController);
            headerController.ControllerContext = ctx;
            var model = headerController.GetHeaderViewModel(canChangeRegions, canSearchContent);
            var view = ViewEngines.Engines.FindPartialView(ctx, partialName).View;

            var vctx = new ViewContext(ctx, view,
                                       new ViewDataDictionary { Model = model },
                                       new TempDataDictionary(), writer);

            view.Render(vctx, writer);
            writer.Flush();

            return sb.ToString();
        }
    }
}