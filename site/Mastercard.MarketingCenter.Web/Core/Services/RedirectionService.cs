﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Exceptions;
using Slam.Cms.Configuration;
using System.Web;

namespace Mastercard.MarketingCenter.Web.Core.Services
{
    public class RedirectionService
    {
        private HttpContextBase _httpContext;
        private MastercardAuthenticationService _authenticationService;

        public string AdminRedirectionUrl
        {
            get
            {
                var adminBaseUrl = ConfigurationManager.Environment.AdminUrl.TrimEnd('/');
                return adminBaseUrl;
            }
        }

        public RedirectionService(HttpContextBase httpContext, MastercardAuthenticationService authenticationService)
        {
            _httpContext = httpContext;
            _authenticationService = authenticationService;
        }

        public void RequireSsl()
        {
            if (_httpContext.Request.Url.Scheme == "http")
            {
                _httpContext.Response.Redirect("https://{0}{1}".F(_httpContext.Request.Url.Authority, _httpContext.Request.Url.PathAndQuery));
            }
        }

        public void RedirectToAdmin()
        {
            if (!_httpContext.User.IsInAdminRole())
            {
                throw new AuthorizationException("Unauthorized");
            }

            _authenticationService.CreateOrUpdateSlamAuthenticationCookie();
            _authenticationService.CreateOrUpdateSlamPreviewModeCookie();

            _httpContext.Response.Redirect(AdminRedirectionUrl);
        }
    }
}