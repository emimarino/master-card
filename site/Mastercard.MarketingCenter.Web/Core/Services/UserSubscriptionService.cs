﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Common;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml.Linq;

namespace Mastercard.MarketingCenter.Web.Core.Services
{
    public class UserSubscriptionService
    {
        private readonly SlamContext _slamContext;
        private readonly SlamContextService _slamContextService;
        private readonly UserSubscriptionRepository _userSubscriptionRepository;
        private readonly AssetRepository _assetRepository;
        private readonly ICalendarService _calendarService;
        private readonly IUserRepository _userRepository;

        public UserSubscriptionService(
            SlamContext slamContext,
            SlamContextService slamContextService,
            UserSubscriptionRepository userSubscriptionRepository,
            AssetRepository assetRepository,
            ICalendarService calendarService,
            IUserRepository userRepository)
        {
            _slamContext = slamContext;
            _slamContextService = slamContextService;
            _userSubscriptionRepository = userSubscriptionRepository;
            _assetRepository = assetRepository;
            _calendarService = calendarService;
            _userRepository = userRepository;
        }

        public ContentItemUserSubscription SaveContentItemUserSubscription(string contentItemId, int userId, bool subscribed)
        {
            return _userSubscriptionRepository.SaveContentItemUserSubscription(contentItemId, userId, subscribed);
        }

        public UserSubscription GetUserSubscription(int userId)
        {
            return _userSubscriptionRepository.GetUserSubscription(userId);
        }

        public void SaveUserSubscription(int userId, int frequency)
        {
            _userSubscriptionRepository.SaveUserSubscription(userId, frequency);
        }

        public IEnumerable<ContentItemUserSubscription> GetSubscribedContentItemUserSubscriptions(string regionId)
        {
            return _userSubscriptionRepository.GetSubscribedContentItemUserSubscriptions(regionId);
        }

        public IEnumerable<ContentItemUserSubscription> GetSubscribedContentItemUserSubscriptions(int userId, string regionId)
        {
            return _userSubscriptionRepository.GetSubscribedContentItemUserSubscriptions(userId, regionId);
        }

        public bool IsUserSubscribedToContent(int userId, string regionId)
        {
            var contentItemUserSubscriptions = _userSubscriptionRepository.GetSubscribedContentItemUserSubscriptions(userId, regionId);

            foreach (var userSubscription in contentItemUserSubscriptions)
            {
                var contentItem = _slamContext.CreateQuery().FilterContentItemId(userSubscription.ContentItemId).FilterRegion(regionId).GetFirstOrDefault();
                if (contentItem != null && (contentItem.StatusID == Status.Published || contentItem.StatusID == Status.PublishedAndDraft) && contentItem.AsContentItemListItem().EnableFavoriting)
                {
                    return true;
                }
            }

            return false;
        }

        public int GetUserSubscriptionFrequency(int userId)
        {
            var userSubscription = _userSubscriptionRepository.GetUserSubscription(userId);
            return userSubscription == null ? MarketingCenterDbConstants.UserSubscriptionFrequency.Undefined : userSubscription.UserSubscriptionFrequencyId;
        }

        public string GetUserSubscriptionFrecuencyMessage(int userId)
        {
            var frecuencyId = _userSubscriptionRepository.GetUserSubscription(userId)?.UserSubscriptionFrequencyId ?? MarketingCenterDbConstants.UserSubscriptionFrequency.Default;
            if (frecuencyId == MarketingCenterDbConstants.UserSubscriptionFrequency.Daily)
            {
                return Resources.Forms.EmailNotifications_Daily;
            }
            else if (frecuencyId == MarketingCenterDbConstants.UserSubscriptionFrequency.Weekly)
            {
                return Resources.Forms.EmailNotifications_Weekly;
            }

            return Resources.Forms.EmailNotifications_Weekly;
        }

        /// <summary>
        /// Toggles subscription for a particular ContentItemId
        /// </summary>
        /// <param name="contentItemId">ContentItemId</param>
        /// <param name="userId">UserId</param>
        /// <returns>True if subscribed, False if unsubscribed</returns>
        public bool ToggleContentItemUserSubscription(string contentItemId, int userId)
        {
            var userSubscription = GetContentItemUserSubscription(contentItemId, userId);
            if (userSubscription == null)
            {
                return SaveContentItemUserSubscription(contentItemId, userId, true).Subscribed;
            }

            return SaveContentItemUserSubscription(contentItemId, userId, !userSubscription.Subscribed).Subscribed;
        }

        public IEnumerable<UserTriggerMarketingNotification> GetUserTriggerMarketingNotifications(int frequency, DateTime sentDate)
        {
            var triggerMarketingNotifications = GetTriggerMarketingNotifications()
                .Where(tm => tm.UserSubscriptionFrequencyId == frequency &&
                             tm.SavedDate < sentDate &&
                             (frequency == MarketingCenterDbConstants.UserSubscriptionFrequency.Daily ? !tm.SentDailyDate.HasValue :
                             frequency == MarketingCenterDbConstants.UserSubscriptionFrequency.Weekly ? !tm.SentWeeklyDate.HasValue : false))
                .ToList();

            return ParseToUserContentItemTriggerMarketingNotifications(triggerMarketingNotifications);
        }

        private IEnumerable<UserTriggerMarketingNotification> ParseToUserContentItemTriggerMarketingNotifications(IEnumerable<TriggerMarketingNotification> triggerMarketingNotifications)
        {
            return from cin in triggerMarketingNotifications
                   group cin by new
                   {
                       cin.UserId,
                       cin.UserName,
                       cin.UserFullName,
                       cin.UserEmail,
                       cin.UserSubscriptionFrequencyId,
                       cin.RegionId
                   }
                   into un
                   select new UserTriggerMarketingNotification
                   {
                       UserId = un.Key.UserId,
                       UserName = un.Key.UserName,
                       UserFullName = un.Key.UserFullName,
                       UserEmail = un.Key.UserEmail,
                       UserSubscriptionFrequencyId = un.Key.UserSubscriptionFrequencyId,
                       UserRegionid = un.Key.RegionId,
                       Notifications = from n in un
                                       group n by new
                                       {
                                           n.ContentItemId,
                                           n.ParentContentItemId
                                       }
                                       into ci
                                       select new ContentItemTriggerMarketingNotification
                                       {
                                           ContentItemId = ci.Key.ContentItemId,
                                           ParentContentItemId = ci.Key.ParentContentItemId,
                                           IsKnownParent = un.Any(n => n.ContentItemId == ci.Key.ParentContentItemId),
                                           IsNew = ci.Any(i => i.IsNew),
                                           SavedDate = ci.Max(i => i.SavedDate),
                                           Notification = ci.OrderByDescending(i => i.SavedDate).FirstOrDefault(),
                                           Children = from n in un.Where(n => n.ParentContentItemId == ci.Key.ContentItemId)
                                                      group n by new
                                                      {
                                                          n.ContentItemId
                                                      }
                                                      into c
                                                      select new ContentItemTriggerMarketingNotification
                                                      {
                                                          ContentItemId = c.Key.ContentItemId,
                                                          IsNew = c.Any(i => i.IsNew),
                                                          SavedDate = c.Max(i => i.SavedDate),
                                                          Notification = c.OrderByDescending(i => i.SavedDate).FirstOrDefault()
                                                      }
                                       }
                   };
        }

        private IEnumerable<TriggerMarketingNotification> GetTriggerMarketingNotifications()
        {
            var contentItemNotifications = GetContentItemTriggerMarketingNotifications();
            var relatedProgramDynamicNotifications = GetRelatedProgramDynamicTriggerMarketingNotifications();

            var siteMapDynamicNotifications = GetSiteMapDynamicNotifications();
            var marketingCalendarNotifications = FilterUserNotificationsBySpecialAudience(GetMarketingCalendarNotifications());

            return contentItemNotifications.Union(relatedProgramDynamicNotifications).Union(siteMapDynamicNotifications).Union(marketingCalendarNotifications);
        }

        public bool ShowNeverFrecuencyMessage(Data.Entities.User user)
        {
            var userFrecuency = GetUserSubscriptionFrequency(user.UserId);
            return userFrecuency == MarketingCenterDbConstants.UserSubscriptionFrequency.Never && !user.Profile.NeverFrecuencyMessageDisplayed;
        }

        public void SetNeverFrecuencyMessageAsShown(int userId)
        {
            var user = _userRepository.GetUser(userId);
            if (user == null)
            {
                throw new Exception("User not found");
            }

            user.Profile.NeverFrecuencyMessageDisplayed = true;
            user.Profile.NeverFrecuencyMessageDisplayedDateTime = DateTime.Now;
            _userRepository.Commit();
        }

        public bool ShowFirstTimeSubscribe(int userId)
        {
            var userFrecuency = GetUserSubscriptionFrequency(userId);
            return userFrecuency != MarketingCenterDbConstants.UserSubscriptionFrequency.Never && !UserHasSubscriptions(userId);
        }

        public bool UserHasSubscriptions(int userId)
        {
            return _userSubscriptionRepository.GetContentItemUserSubscriptionsByUserId(userId)?.Any() ?? false;
        }

        private IEnumerable<TriggerMarketingNotification> GetMarketingCalendarNotifications()
        {
            var notifications = new List<TriggerMarketingNotification>();
            var siteMapNodes = (new SiteMapNode()).GetSiteMapNodes();
            foreach (var region in MastercardSitemap.AvailableRegionSitemaps)
            {
                // omitting regions where Marketing Calendar is not enabled
                bool.TryParse(RegionalizeService.RegionalizeSetting("MarketingCalendar.Enabled", region.Key.Trim(), false), out bool marketingCalendarEnabled);
                if (marketingCalendarEnabled)
                {
                    var parentNodes = siteMapNodes.Where(n => n.ChildrenNodes.Any(c => c.Key == "marketing-calendar") && n.Region == region.Key);
                    foreach (var parentNode in parentNodes)
                    {
                        var calendarNode = parentNode.ChildrenNodes.FirstOrDefault(c => c.Key == "marketing-calendar");
                        if (calendarNode != null && calendarNode.Url != null)
                        {
                            var page = _slamContextService.GetPageByNodeUrl(calendarNode.Url, parentNode.Region, false, false);
                            if (page != null)
                            {
                                var calendarPrograms = _calendarService.GetContentItemCampaignEventsFilterByIds(_slamContextService.GetCalendarContentItems(parentNode.Region, null, null, null, filterSpecialAudience: false).Select(x => x.ContentItemId).ToArray());

                                notifications.AddRange(GetDynamicTriggerMarketingNotifications(page.ContentItemId, calendarPrograms.Select(ci => ci.ContentItemId).ToArray()));
                            }
                        }
                    }
                }
            }

            return notifications;
        }

        private List<TriggerMarketingNotification> FilterUserNotificationsBySpecialAudience(IEnumerable<TriggerMarketingNotification> notifications)
        {
            List<TriggerMarketingNotification> filteredNotifications = new List<TriggerMarketingNotification>();
            foreach (var notification in notifications)
            {
                var hasAccess = _slamContextService.HasAccessToSpecialAudience(notification.UserSpecialAudienceAccess, notification.ContentItemId);
                if (hasAccess)
                {
                    filteredNotifications.Add(notification);
                }
            }

            return filteredNotifications;
        }

        public ContentItemUserSubscription GetContentItemUserSubscription(string contentItemId, int userId)
        {
            return _userSubscriptionRepository.GetContentItemUserSubscription(contentItemId, userId);
        }


        private IEnumerable<TriggerMarketingNotification> GetSiteMapDynamicNotifications()
        {
            List<SiteMapNode> siteMapNodes = (new SiteMapNode()).GetSiteMapNodes();
            var dynamicNotifications = new List<TriggerMarketingNotification>();

            foreach (var node in siteMapNodes)
            {
                var page = _slamContextService.GetPageByNodeUrl(node.Url, node.Region, false, false);
                if (page != null)
                {
                    var contentItems = new List<ContentItem>();

                    foreach (var currentNode in node.ChildrenNodes)
                    {
                        foreach (var childrenKey in currentNode.ChildrenKeys)
                        {
                            if (node.IsOverview)
                            {
                                contentItems.AddRange(_slamContextService.GetDynamicOverviewItems(currentNode.Key, childrenKey, false).Take(3));
                            }
                            else
                            {
                                contentItems.AddRange(_slamContextService.GetDynamicPageItems(currentNode.Key, childrenKey, false, currentNode.SingleTagNodes != null && currentNode.SingleTagNodes.ContainsKey(childrenKey) ? bool.Parse(currentNode.SingleTagNodes[childrenKey]) : false));
                            }
                        }
                    }

                    dynamicNotifications.AddRange(GetDynamicTriggerMarketingNotifications(page.ContentItemId, contentItems.Select(ci => ci.ContentItemId).ToArray()));
                }
            }

            return dynamicNotifications;
        }

        public IEnumerable<TriggerMarketingNotification> GetContentItemTriggerMarketingNotifications()
        {
            return from tm in _userSubscriptionRepository.GetContentItemTriggerMarketings()
                   join cius in _userSubscriptionRepository.GetSubscribedContentItemUserSubscriptions() on tm.ContentItemId equals cius.ContentItemId
                   join us in _userSubscriptionRepository.GetEnabledUserSubscriptions() on cius.UserId equals us.UserId
                   select new TriggerMarketingNotification
                   {
                       ContentItemId = tm.ContentItemId,
                       RegionId = tm.ContentItem.RegionId.Trim(),
                       Notify = tm.Notify,
                       Featured = tm.Featured,
                       Reasons = tm.Reasons,
                       IsNew = tm.IsNew,
                       UserId = us.UserId,
                       UserName = us.User.UserName,
                       UserFullName = us.User.FullName,
                       UserEmail = us.User.Email,
                       UserSubscriptionFrequencyId = us.UserSubscriptionFrequencyId,
                       SavedDate = tm.SavedDate,
                       SentDailyDate = tm.SentDailyDate,
                       SentWeeklyDate = tm.SentWeeklyDate,
                       ParentContentItemId = null,
                       UserSpecialAudienceAccess = us.User.Issuer.SpecialAudienceAccess
                   };
        }

        public IEnumerable<TriggerMarketingNotification> GetDynamicTriggerMarketingNotifications(string parentContentItemId, string[] dynamicContentItemIds)
        {
            return from tm in _userSubscriptionRepository.GetContentItemTriggerMarketings().Where(ci => dynamicContentItemIds.Contains(ci.ContentItemId))
                   join cius in _userSubscriptionRepository.GetSubscribedContentItemUserSubscriptions() on parentContentItemId equals cius.ContentItemId
                   join us in _userSubscriptionRepository.GetEnabledUserSubscriptions() on cius.UserId equals us.UserId
                   select new TriggerMarketingNotification
                   {
                       ContentItemId = tm.ContentItemId,
                       RegionId = tm.ContentItem.RegionId.Trim(),
                       Notify = tm.Notify,
                       Featured = tm.Featured,
                       Reasons = tm.Reasons,
                       IsNew = tm.IsNew,
                       UserId = us.UserId,
                       UserName = us.User.UserName,
                       UserFullName = us.User.FullName,
                       UserEmail = us.User.Email,
                       UserSubscriptionFrequencyId = us.UserSubscriptionFrequencyId,
                       SavedDate = tm.SavedDate,
                       SentDailyDate = tm.SentDailyDate,
                       SentWeeklyDate = tm.SentWeeklyDate,
                       ParentContentItemId = parentContentItemId,
                       UserSpecialAudienceAccess = us.User.Issuer.SpecialAudienceAccess
                   };
        }

        public IEnumerable<TriggerMarketingNotification> GetRelatedProgramDynamicTriggerMarketingNotifications()
        {
            var contentItemTriggerMarketings = _userSubscriptionRepository.GetContentItemTriggerMarketings();
            var contentItemUserSubscriptions = _userSubscriptionRepository.GetSubscribedContentItemUserSubscriptions();
            var userSubscriptions = _userSubscriptionRepository.GetEnabledUserSubscriptions();

            var darpContentItemNotifications = from tm in contentItemTriggerMarketings
                                               join darp in _assetRepository.GetDownloadableAssetRelatedPrograms() on tm.ContentItemId equals darp.DownloadableAssetContentItemId
                                               join cius in contentItemUserSubscriptions on darp.ProgramContentItemId equals cius.ContentItemId
                                               join us in userSubscriptions on cius.UserId equals us.UserId
                                               select new TriggerMarketingNotification
                                               {
                                                   ContentItemId = tm.ContentItemId,
                                                   RegionId = tm.ContentItem.RegionId.Trim(),
                                                   Notify = tm.Notify,
                                                   Featured = tm.Featured,
                                                   Reasons = tm.Reasons,
                                                   IsNew = tm.IsNew,
                                                   UserId = us.UserId,
                                                   UserName = us.User.UserName,
                                                   UserFullName = us.User.FullName,
                                                   UserEmail = us.User.Email,
                                                   UserSubscriptionFrequencyId = us.UserSubscriptionFrequencyId,
                                                   SavedDate = tm.SavedDate,
                                                   SentDailyDate = tm.SentDailyDate,
                                                   SentWeeklyDate = tm.SentWeeklyDate,
                                                   ParentContentItemId = darp.ProgramContentItemId,
                                                   UserSpecialAudienceAccess = us.User.Issuer.SpecialAudienceAccess
                                               };

            var oarpContentItemNotifications = from tm in contentItemTriggerMarketings
                                               join oarp in _assetRepository.GetOrderableAssetRelatedPrograms() on tm.ContentItemId equals oarp.OrderableAssetContentItemId
                                               join cius in contentItemUserSubscriptions on oarp.ProgramContentItemId equals cius.ContentItemId
                                               join us in userSubscriptions on cius.UserId equals us.UserId
                                               select new TriggerMarketingNotification
                                               {
                                                   ContentItemId = tm.ContentItemId,
                                                   RegionId = tm.ContentItem.RegionId.Trim(),
                                                   Notify = tm.Notify,
                                                   Featured = tm.Featured,
                                                   Reasons = tm.Reasons,
                                                   IsNew = tm.IsNew,
                                                   UserId = us.UserId,
                                                   UserName = us.User.UserName,
                                                   UserFullName = us.User.FullName,
                                                   UserEmail = us.User.Email,
                                                   UserSubscriptionFrequencyId = us.UserSubscriptionFrequencyId,
                                                   SavedDate = tm.SavedDate,
                                                   SentDailyDate = tm.SentDailyDate,
                                                   SentWeeklyDate = tm.SentWeeklyDate,
                                                   ParentContentItemId = oarp.ProgramContentItemId,
                                                   UserSpecialAudienceAccess = us.User.Issuer.SpecialAudienceAccess
                                               };

            return darpContentItemNotifications.Union(oarpContentItemNotifications);
        }

        private class SiteMapNode
        {
            public string Region { get; set; }
            public string Url { get; set; }
            public bool IsOverview { get; set; }
            public List<SiteMapNodeChildren> ChildrenNodes { get; set; }

            public class SiteMapNodeChildren
            {
                public string Key { get; set; }
                public string Url { get; set; }
                public IEnumerable<string> ChildrenKeys { get; set; }
                public IDictionary<string, string> SingleTagNodes { get; set; }
            }

            public List<SiteMapNode> GetSiteMapNodes()
            {
                var sitemapFiles = MastercardSitemap.AvailableRegionSitemaps;
                List<SiteMapNode> siteMapNodes = new List<SiteMapNode>();

                foreach (var sitemapFile in sitemapFiles)
                {
                    var rootNode = XElement.Load(sitemapFile.Value);
                    foreach (var sitemap in rootNode.Element("sitemaps").Elements())
                    {
                        foreach (var node in sitemap.Element("SitemapNode").Elements())
                        {
                            if (node.Attribute("url") != null)
                            {
                                List<SiteMapNodeChildren> childrenSiteMapNode = new List<SiteMapNodeChildren>();

                                foreach (var key in node.Elements("SitemapNode"))
                                {
                                    childrenSiteMapNode.Add(new SiteMapNodeChildren
                                    {
                                        Key = key.Attribute("key").Value,
                                        Url = key.Attribute("url")?.Value,
                                        ChildrenKeys = key.Elements("SitemapNode").Select(n => n.Attribute("key").Value),
                                        SingleTagNodes = key.Elements("SitemapNode").Where(n => n.Attribute("singletagnode") != null ? bool.Parse(n.Attribute("singletagnode").Value) : false).ToDictionary(k => (string)k.Attribute("key"), v => (string)v.Attribute("singletagnode"))

                                    });
                                }

                                siteMapNodes.Add(new SiteMapNode
                                {
                                    Region = sitemapFile.Key,
                                    Url = node.Attribute("url").Value,
                                    IsOverview = true,
                                    ChildrenNodes = childrenSiteMapNode
                                });
                            }

                            foreach (var subNode in node.Elements("SitemapNode"))
                            {
                                if (subNode.Attribute("url") != null)
                                {
                                    List<SiteMapNodeChildren> childrenSiteMapNode = new List<SiteMapNodeChildren>();
                                    childrenSiteMapNode.Add(new SiteMapNodeChildren
                                    {
                                        Key = subNode.Attribute("key").Value,
                                        ChildrenKeys = subNode.Elements("SitemapNode").Select(n => n.Attribute("key").Value),
                                        SingleTagNodes = subNode.Elements("SitemapNode").Where(n => n.Attribute("singletagnode") != null ? bool.Parse(n.Attribute("singletagnode").Value) : false).ToDictionary(k => (string)k.Attribute("key"), v => (string)v.Attribute("singletagnode"))
                                    });

                                    siteMapNodes.Add(new SiteMapNode
                                    {
                                        Region = sitemapFile.Key,
                                        Url = subNode.Attribute("url").Value,
                                        IsOverview = false,
                                        ChildrenNodes = childrenSiteMapNode
                                    });
                                }
                            }
                        }
                    }
                }

                return siteMapNodes;
            }
        }
    }
}