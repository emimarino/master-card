﻿using System;
using System.Web;

namespace Mastercard.MarketingCenter.Web.Core.Services
{
    public class HttpHeaderService
    {
        private HttpContextBase httpContext;

        public HttpHeaderService(HttpContextBase httpContext)
        {
            this.httpContext = httpContext;
        }

        public void SetNoCacheHeaders()
        {
            httpContext.Response.CacheControl = "no-cache";
            httpContext.Response.Expires = -1;

            httpContext.Response.ExpiresAbsolute = new DateTime(1900, 1, 1);
            httpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        }
    }
}