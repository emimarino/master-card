﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Mastercard.MarketingCenter.Web.Core.Services
{
    public interface IPageTrackingService
    {
        void TrackPageAccess();
        void TrackClientClick(string what, string how);
        void TrackTagHit(string[] tags);
        void TrackMailDispatcher(int mailDispatcherId);
    }
}