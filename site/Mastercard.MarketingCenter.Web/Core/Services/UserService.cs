﻿using Mastercard.MarketingCenter.Data;
using System.Text.RegularExpressions;

namespace Mastercard.MarketingCenter.Web.Core.Services
{
    public class UserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public static string VerifyNewPasswordIsValid(string newPassword, string confirmPassword, string email)
        {
            string errors = string.Empty;

            if (newPassword.Trim().Length == 0)
            {
                errors += string.IsNullOrWhiteSpace(errors) ? Resources.Errors.PasswordNewEmpty : "<br />" + Resources.Errors.PasswordNewEmpty;
            }

            if (confirmPassword.Trim().Length == 0)
            {
                errors += string.IsNullOrWhiteSpace(errors) ? Resources.Errors.PasswordConfirmEmpty : "<br />" + Resources.Errors.PasswordConfirmEmpty;
            }

            if (confirmPassword.Trim() != newPassword.Trim())
            {
                errors += string.IsNullOrWhiteSpace(errors) ? Resources.Errors.PasswordUnmatch : "<br />" + Resources.Errors.PasswordUnmatch;
            }
            else
            {
                if (newPassword.Length < 8)
                {
                    errors += string.IsNullOrWhiteSpace(errors) ? Resources.Errors.PasswordLenghtMin : "<br />" + Resources.Errors.PasswordLenghtMin;
                }
                else if (newPassword.Length >= 40)
                {
                    errors += string.IsNullOrWhiteSpace(errors) ? Resources.Errors.PasswordLenghtMax : "<br />" + Resources.Errors.PasswordLenghtMax;
                }
                else
                {
                    int differentTypes = 0;

                    Regex regex = new Regex(@"^(?=.*\d)");
                    if (regex.IsMatch(newPassword))
                    {
                        differentTypes++;
                    }

                    regex = new Regex(@"^(?=.*[a-zA-Z])");
                    if (regex.IsMatch(newPassword))
                    {
                        differentTypes++;
                    }

                    regex = new Regex(@"^(?=.*[\W])");
                    if (regex.IsMatch(newPassword))
                    {
                        differentTypes++;
                    }

                    if (differentTypes < 2)
                    {
                        errors += string.IsNullOrWhiteSpace(errors) ? Resources.Errors.PasswordDifferentTypes : "<br />" + Resources.Errors.PasswordDifferentTypes;
                    }

                    regex = new Regex(@"^(?=.*[\s])");
                    if (regex.IsMatch(newPassword))
                    {
                        errors += string.IsNullOrWhiteSpace(errors) ? Resources.Errors.PasswordInvalidCharacters : "<br />" + Resources.Errors.PasswordInvalidCharacters;
                    }

                    regex = new Regex(@"^(?=.*(.)\1{2})");
                    if (regex.IsMatch(newPassword))
                    {
                        errors += string.IsNullOrWhiteSpace(errors) ? Resources.Errors.PasswordRepeatedCharacters : "<br />" + Resources.Errors.PasswordRepeatedCharacters;
                    }

                    string username = email.Split('@')[0];
                    if (newPassword.Contains(email) || newPassword.Contains(username))
                    {
                        errors += string.IsNullOrWhiteSpace(errors) ? Resources.Errors.PasswordAccountName : "<br />" + Resources.Errors.PasswordAccountName;
                    }
                }
            }

            return errors;
        }
    }
}