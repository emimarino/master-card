﻿using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CommonConstants = Mastercard.MarketingCenter.Common.Infrastructure.Constants;

namespace Mastercard.MarketingCenter.Web.Core.Services
{
    public class MarketingCenterWebApplicationService : IMarketingCenterWebApplicationService
    {
        private HttpContextBase _httpContext;
        private UserContext _userContext;
        private ISegmentationService _segmentationService;

        public MarketingCenterWebApplicationService(HttpContextBase httpContext, UserContext userContext, ISegmentationService segmentationService)
        {
            _httpContext = httpContext;
            _userContext = userContext;
            _segmentationService = segmentationService;
        }

        public IEnumerable<string> GetCurrentSegmentationIds()
        {
            var segmentationsSecuence = _httpContext?.Session != null ? (_httpContext.Session["CurrentSegmentation"] as string[]) ?? new string[] { string.Empty } : null;
            var regionSegmentations = _segmentationService.GetSegmentationsByRegion(_userContext?.SelectedRegion)?.Select(s => s.SegmentationId).ToArray();

            if (_httpContext.Session != null &&
                (_httpContext.Session["CurrentSegmentation"] == null ||
                segmentationsSecuence.SequenceEqual(System.Array.Empty<string>()) ||
                !(segmentationsSecuence.Contains(RegionConfigManager.DefaultSegmentation) || regionSegmentations.Intersect(segmentationsSecuence).Any())))
            {
                var userSegmentations = regionSegmentations.Intersect(_segmentationService.GetSegmentationsByUserName(_userContext.User?.UserName)?.Select(s => s.SegmentationId).ToArray()).ToArray();
                if (userSegmentations != null &&
                    (!userSegmentations.Any() || userSegmentations.Count() > 1) &&
                    _httpContext.User.IsInPermission(CommonConstants.Permissions.CanChangeSegments) &&
                    _httpContext.User.IsInFullAdminRole())
                {
                    _httpContext.Session["CurrentSegmentation"] = new string[] { RegionConfigManager.DefaultSegmentation };
                }
                else if (userSegmentations == null || !userSegmentations.Any())
                {
                    _httpContext.Session["CurrentSegmentation"] = new string[] { string.Empty };
                }
                else
                {
                    _httpContext.Session["CurrentSegmentation"] = userSegmentations;
                }
            }

            return _httpContext.Session != null ? (_httpContext.Session["CurrentSegmentation"] as string[]) ?? new string[] { string.Empty } : new string[] { string.Empty };
        }

        public string GetCurrentSpecialAudience()
        {
            if (_userContext?.SelectedRegion?.ToLower() != "us")
            {
                return string.Empty;
            }

            return (_userContext.User?.Issuer?.SpecialAudienceAccess ?? string.Empty).ToLower();
        }
    }
}