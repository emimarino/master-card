﻿using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Web.Core.Models;
using Mastercard.MarketingCenter.Web.Core.Modules;
using Slam.Cms;
using Slam.Cms.Data;
using StackExchange.Profiling;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Web.Core.Services
{
    public class SlamContextService
    {
        protected IMarketingCenterWebApplicationService _marketingCenterWebApplicationService { get { return WebCoreModule.GetService<IMarketingCenterWebApplicationService>(); } }
        public readonly static IEnumerable<Type> _calendarTypes = typeof(MarketingCenterDbContext).GetProperties().SelectMany(prop => prop.PropertyType.GetGenericArguments()).Where(t => !typeof(ContentItemForCalendar).Equals(t) && typeof(ContentItemForCalendar).IsAssignableFrom(t)).Select(ct => ct);
        public readonly static string _CalendarFilter = $"( {string.Join(" OR ", _calendarTypes.Select(ct => $"{ct.Name}.ShowInMarketingCalendar = 1 "))} ) AND ( { string.Join(" or ", _calendarTypes.Select(ct => $"{ct.Name}.ShowInMarketingCalendar Is NOT NULL "))} )";

        private readonly SlamContext _slamContext;
        private readonly string _selectedRegion;
        private readonly string _selectedLanguage;

        public SlamContextService(SlamContext slamContext, string selectedRegion, string selectedLanguage)
        {
            _slamContext = slamContext;
            _selectedRegion = selectedRegion;
            _selectedLanguage = selectedLanguage;
        }

        #region Public

        public PageDTO GetPageByUri(string pageUri, bool includeTags = false, string[] segmentationIds = null)
        {
            var page = _slamContext.CreateQuery()
                                   .FilterContentTypes<PageDTO>()
                                   .FilterTaggedWith(_selectedLanguage)
                                   .FilterSegmentations(segmentationIds != null ? segmentationIds : _marketingCenterWebApplicationService?.GetCurrentSegmentationIds())
                                   .Filter($"Page.Uri = N'{pageUri}'");

            if (includeTags)
            {
                page = page.IncludeTags();
            }

            return page.Take(1).Get<PageDTO>().FirstOrDefault();
        }

        public IEnumerable<AssessmentCriteriaDTO> GetAssessmentCriterias(SlamQueryCacheBehavior? cacheBehavior = null)
        {
            var query = _slamContext.CreateQuery()
                                    .FilterContentTypes<AssessmentCriteriaDTO>()
                                    .IncludeTags();

            if (cacheBehavior.HasValue)
            {
                query = query.Cache(cacheBehavior.Value);
            }

            return query.Get<AssessmentCriteriaDTO>();
        }

        public IEnumerable<RecommendationDTO> GetRecommendations(SlamQueryCacheBehavior? cacheBehavior = null)
        {
            var query = _slamContext.CreateQuery()
                                    .FilterContentTypes<RecommendationDTO>()
                                    .IncludeTags();

            if (cacheBehavior.HasValue)
            {
                query = query.Cache(cacheBehavior.Value);
            }

            return query.Get<RecommendationDTO>().OrderBy(o => o.DisplayName);
        }

        public SlamQueryResult<MarqueeSlideDTO> GetHomeMarqueeSlides(SlamQueryCacheBehavior? cacheBehavior = null)
        {
            using (MiniProfiler.Current.Step("GetHomeMarqueeSlides"))
            {
                var query = _slamContext.CreateQuery()
                                        .FilterContentTypes<MarqueeSlideDTO>()
                                        .FilterFeatureLocatedWithAny(new[] { MarketingCenterDbConstants.FeatureLocations.Home,
                                                                             MarketingCenterDbConstants.FeatureLocations.HomeCarousel})
                                        .FilterTaggedWith(_selectedLanguage)
                                        .FilterByExpiration()
                                        .IncludeFeatureLocations()
                                        .FilterSegmentations(_marketingCenterWebApplicationService?.GetCurrentSegmentationIds())
                                        .FilterMarqueeSlideTagBrowserTypesForSpecialAudience();

                if (cacheBehavior.HasValue)
                {
                    query = query.Cache(cacheBehavior.Value);
                }

                return query.Get<MarqueeSlideDTO>();
            }
        }

        public IEnumerable<ContentItemListItem> GetRelatedItems(IEnumerable<string> relatedItems)
        {
            if (!relatedItems.Any())
            {
                return null;
            }

            return GetRelatedItemsBaseQuery(relatedItems).Get().Select(a => a.AsContentItemListItem());
        }

        public IEnumerable<ContentItemListItem> GetRelatedOffers(IEnumerable<string> relatedItems)
        {
            if (!relatedItems.Any())
            {
                return null;
            }

            return GetRelatedItemsBaseQuery(relatedItems).IncludeAllRegions()
                                                         .FilterByCrossBorderRegion(_selectedRegion)
                                                         .FilterByRegionVisibility(_selectedRegion)
                                                         .Get()
                                                         .Select(a => a.AsContentItemListItem());
        }

        private SlamQuery GetRelatedItemsBaseQuery(IEnumerable<string> relatedItems)
        {
            return _slamContext.CreateQuery()
                               .FilterRelatedItemTypes()
                               .FilterSegmentedWithAny(_marketingCenterWebApplicationService?.GetCurrentSegmentationIds())
                               .FilterTagBrowserTypesForSpecialAudience()
                               .FilterContentItemId(relatedItems.ToArray())
                               .FilterExpiredAssets();
        }

        #endregion

        #region Home              

        public SlamQueryResult<QuickLinkDTO> GetHomeQuickLinks(SlamQueryCacheBehavior? cacheBehavior = null)
        {
            using (MiniProfiler.Current.Step("GetHomeQuickLinks"))
            {
                var query = _slamContext.CreateQuery()
                                        .FilterContentTypes<QuickLinkDTO>()
                                        .FilterTaggedWith(_selectedLanguage)
                                        .FilterByExpiration();

                if (cacheBehavior.HasValue)
                {
                    query = query.Cache(cacheBehavior.Value);
                }

                return query.Get<QuickLinkDTO>();
            }
        }

        public SlamQueryResult<YoutubeVideoDTO> GetHomeYoutubeVideos(SlamQueryCacheBehavior? cacheBehavior = null)
        {
            using (MiniProfiler.Current.Step("GetHomeYoutubeVideos"))
            {
                var query = _slamContext.CreateQuery()
                                        .FilterContentTypes<YoutubeVideoDTO>()
                                        .FilterFeatureLocatedWithAny(new[] { MarketingCenterDbConstants.FeatureLocations.Home,
                                                                             MarketingCenterDbConstants.FeatureLocations.HomeCarousel})
                                        .FilterTaggedWith(_selectedLanguage)
                                        .FilterByExpiration()
                                        .IncludeFeatureLocations();

                if (cacheBehavior.HasValue)
                {
                    query = query.Cache(cacheBehavior.Value);
                }

                return query.Get<YoutubeVideoDTO>();
            }
        }

        public SnippetDTO GetProductBoxSnippet(SlamQueryCacheBehavior? cacheBehavior = null)
        {
            var segmentationIds = _marketingCenterWebApplicationService.GetCurrentSegmentationIds();
            var query = _slamContext.CreateQuery()
                                    .FilterSegmentations(segmentationIds)
                                    .FilterSnippetTagBrowserTypesForCurrentSpecialAudience()
                                    .Filter("Snippet.Title = 'Find-a-Product-Box'")
                                    .FilterTaggedWith(_selectedLanguage);

            if (cacheBehavior.HasValue)
            {
                query = query.Cache(cacheBehavior.Value);
            }

            return query.Get<SnippetDTO>()
                        .OrderBy(s => s.CreatedDate)
                        .FirstOrDefault();
        }

        public SnippetDTO GetHomeHeaderSnippet(SlamQueryCacheBehavior? cacheBehavior = null)
        {
            var query = _slamContext.CreateQuery()
                                    .Filter("Snippet.Title = 'Home-Banner'")
                                    .FilterTaggedWith(_selectedLanguage);

            if (cacheBehavior.HasValue)
            {
                query = query.Cache(cacheBehavior.Value);
            }

            return query.Get<SnippetDTO>()
                        .OrderBy(s => s.CreatedDate)
                        .FirstOrDefault();
        }

        public SnippetDTO GetHomeSubMarqueeSnippet(SlamQueryCacheBehavior? cacheBehavior = null)
        {
            var query = _slamContext.CreateQuery()
                    .Filter("Snippet.Title = 'Home-SubMarquee'")
                    .FilterTaggedWith(_selectedLanguage);

            if (cacheBehavior.HasValue)
            {
                query = query.Cache(cacheBehavior.Value);
            }

            return query
                .Get<SnippetDTO>()
                .OrderBy(s => s.CreatedDate)
                .FirstOrDefault();
        }

        public SlamQueryResult<SnippetDTO> GetContentPreferencesButtonSnippets(SlamQueryCacheBehavior? cacheBehavior = null)
        {
            var query = _slamContext.CreateQuery()
                                    .FilterSegmentations(_marketingCenterWebApplicationService?.GetCurrentSegmentationIds())
                                    .FilterSnippetTagBrowserTypesForCurrentSpecialAudience()
                                    .Filter("Snippet.Title = 'Content-Preferences-Button'")
                                    .IncludeTags();

            if (cacheBehavior.HasValue)
            {
                query = query.Cache(cacheBehavior.Value);
            }

            return query.Get<SnippetDTO>();
        }

        public SlamQueryResult<ContentItem> GetHomeProducts(string[] segmentationIds = null, string audience = null, SlamQueryCacheBehavior? cacheBehavior = null)
        {
            var query = _slamContext.CreateQuery().FilterByTagCategory(new List<string> { MarketingCenterDbConstants.TagCategories.Products, MarketingCenterDbConstants.TagCategories.CardTypes })
                                                  .FilterFeaturedOnLocation(MarketingCenterDbConstants.FeatureLocations.Home, FilterOperator.GreaterOrEqual, 0)
                                                  .FilterTaggedWith(_selectedLanguage)
                                                  .FilterSegmentations(segmentationIds != null ? segmentationIds : _marketingCenterWebApplicationService?.GetCurrentSegmentationIds())
                                                  .FilterTagBrowserTypes()
                                                  .FilterExpiredAssets()
                                                  .IncludeTags();

            query = string.IsNullOrWhiteSpace(audience) ? query.FilterTagBrowserTypesForSpecialAudience() : query.FilterTagBrowserTypesForSpecialAudience(audience);

            if (cacheBehavior.HasValue)
            {
                query = query.Cache(cacheBehavior.Value);
            }

            return query.Get();
        }

        public SlamQueryResult<ContentItem> GetContentItemsFilterByIds(string[] contentItemIds, SlamQueryCacheBehavior? cacheBehavior = null)
        {
            var query = _slamContext.CreateQuery().FilterContentItemId(contentItemIds)
                                                  .FilterStatus(FilterStatus.Live)
                                                  .FilterTaggedWith(_selectedLanguage)
                                                  .FilterSegmentations(_marketingCenterWebApplicationService?.GetCurrentSegmentationIds())
                                                  .FilterTagBrowserTypesForSpecialAudience()
                                                  .FilterExpiredAssets();
            if (cacheBehavior.HasValue)
            {
                query = query.Cache(cacheBehavior.Value);
            }

            return query.Get();
        }

        #endregion

        #region Dynamic

        public PageDTO GetPageByNodeUrl(string nodeUrl, string region, bool filterByLanguage = true, bool includeTags = false)
        {
            var page = _slamContext.CreateQuery()
                                   .FilterRegion(region)
                                   .FilterContentTypes<PageDTO>()
                                   .Filter("Page.Uri = " + "'" + nodeUrl.Replace(@"/portal/", "").Replace(@"portal/", "") + "'");

            if (filterByLanguage)
            {
                page = page.FilterTaggedWith(_selectedLanguage);
            }

            if (includeTags)
            {
                page = page.IncludeTags();
            }

            return page.Take(1).Get<PageDTO>().FirstOrDefault();
        }

        public IEnumerable<CarouselSlideItem> GetDynamicCarouselSlides(string nodeLocation)
        {
            return _slamContext.CreateQuery()
                               .FilterContentTypes<MarqueeSlideDTO, YoutubeVideoDTO>()
                               .FilterSegmentations(_marketingCenterWebApplicationService?.GetCurrentSegmentationIds())
                               .FilterMarqueeSlideTagBrowserTypesForSpecialAudience()
                               .FilterLocationByName()
                               .FilterTaggedWith(_selectedLanguage)
                               .FilterFeaturedOnLocation(nodeLocation, FilterOperator.GreaterOrEqual, 0)
                               .FilterByExpiration()
                               .Get()
                               .Select(ci => ci.AsCarouselSlideItem())
                               .OrderBy(ci => ci.Order);
        }

        public IEnumerable<ContentItem> GetDynamicOverviewItems(string key, string childrenKey, bool filterByLanguage = true, bool singleTagNode = false)
        {
            var items = _slamContext.CreateQuery()
                                    .FilterSegmentations(_marketingCenterWebApplicationService?.GetCurrentSegmentationIds())
                                    .FilterFeaturedOnTag((singleTagNode ? childrenKey : key), FilterOperator.GreaterOrEqual, 0)
                                    .FilterFeaturedOnTag(childrenKey, FilterOperator.GreaterOrEqual, 0)
                                    .FilterExpiredAssets();

            if (filterByLanguage)
            {
                items = items.FilterTaggedWith(_selectedLanguage);
            }

            return items.Take(3).OrderByFeaturedOnTag(childrenKey).OrderByFeatureAndPriorityLevel().Get();
        }

        public IEnumerable<ContentItem> GetDynamicPageItems(string key, string childrenKey, bool filterByLanguage = true, bool singleTagNode = false)
        {
            var items = _slamContext.CreateQuery()
                                    .FilterFeaturedOnTag((singleTagNode ? childrenKey : key), FilterOperator.GreaterOrEqual, 0)
                                    .FilterFeaturedOnAnyTag(childrenKey, FilterOperator.GreaterOrEqual, 0)
                                    .FilterExpiredAssets()
                                    .FilterSegmentations(_marketingCenterWebApplicationService?.GetCurrentSegmentationIds());

            if (filterByLanguage)
            {
                items = items.FilterTaggedWith(_selectedLanguage);
            }

            return items.OrderByFeaturedOnTag(childrenKey.ToLower()).OrderByFeatureAndPriorityLevel().Get();
        }

        #endregion

        #region Cache Related

        public bool ReloadCacheQueries()
        {
            SlamQueryCacheBehavior behavior = SlamQueryCacheBehavior.NoCache;
            try
            {
                GetAssessmentCriterias(behavior);
                GetRecommendations(behavior);
                GetHomeMarqueeSlides(behavior);
                GetHomeYoutubeVideos(behavior);
                GetHomeQuickLinks(behavior);
                GetTreeNode(behavior);
            }
            catch (Exception ex)
            {
                Pulsus.LogManager.EventFactory.Create()
                                              .Text("The Cache Queries could not be reloaded: " + ex.ToString()).Push()
                                              .Level(Pulsus.LoggingEventLevel.Information);
                return false;
            }

            return true;
        }

        public void InvalidateAllTags()
        {
            _slamContext.InvalidateAllTags(GetKeyByRegionAndLanguage());
        }

        #endregion

        #region Browse All

        public IList<TagTreeNode> GetTreeNode(SlamQueryCacheBehavior? cacheBehavior = null, string overrideRegion = null, string overrideLanguage = null)
        {
            var cacheId = GetKeyByRegionAndLanguage(overrideRegion, overrideLanguage);
            IList<TagTreeNode> items;
            SlamQueryResult<ContentItem> contentItems;

            if (_slamContext.Cache.GetList<TagTreeNode>(cacheId) == null)
            {
                using (MiniProfiler.Current.Step("GetTreeNode Query"))
                {
                    var query = _slamContext.CreateQuery()
                                        .FilterRegion(overrideRegion ?? _selectedRegion) //override for Chrome API
                                        .FilterTagBrowserTypes()
                                        .FilterSegmentations(_marketingCenterWebApplicationService?.GetCurrentSegmentationIds())
                                        .FilterTagBrowserTypesForSpecialAudience()
                                        .FilterExpiredAssets()
                                        .FilterTagBrowserTypesShowInTagBrowser()
                                        .IncludeTags();

                    if (cacheBehavior.HasValue)
                    {
                        query = query.Cache(cacheBehavior.Value);
                    }

                    contentItems = query.Get();
                }

                using (MiniProfiler.Current.Step("GetTreeNode Build Tree"))
                {
                    items = _slamContext.GetTagTree()
                    .Translate(overrideLanguage ?? _selectedLanguage)
                    .TranslateTagCategories(overrideLanguage ?? _selectedLanguage)
                    .UpdateContentItemCount(contentItems)
                    .ExpandRegionMarketTags(contentItems)
                    .FlattenMarketTags()
                    .RemoveTagsWithNoItems()
                    .RemoveTagCategoriesWithNoTags()
                    .RemoveTagCategoriesWithSingleTag(new[] { MarketingCenterDbConstants.TagCategories.Markets, MarketingCenterDbConstants.TagCategories.Languages })
                    .ForMastercardFlyout()
                    .OrderBy(x => x.Text)
                    .ToList();
                }

                _slamContext.Cache.SaveList(items, cacheId);
            }

            items = _slamContext.Cache.GetList<TagTreeNode>(cacheId);

            return items;
        }

        #endregion

        #region Calendar

        public SlamQueryResult<ContentItem> GetCalendarContentItems(string region = null, string language = null, string specialAudience = null, string[] segmentationIds = null, SlamQueryCacheBehavior? cacheBehavior = null, bool includeExpiredPrograms = false, bool filterSpecialAudience = true)
        {
            var query = GetCalendarContentItemsBaseFilteredQuery(region, language, segmentationIds, cacheBehavior, includeExpiredPrograms);

            if (filterSpecialAudience)
            {
                query.FilterTagBrowserTypesForSpecialAudience(!string.IsNullOrWhiteSpace(specialAudience) ? specialAudience : _marketingCenterWebApplicationService?.GetCurrentSpecialAudience());
            }

            return query.Get();
        }

        public bool HasAccessToSpecialAudience(string specialAudience, string contentItemId)
        {

            var query = GetCalendarContentItemsBaseFilteredQuery().FilterContentItemId(new string[] { contentItemId })
                .FilterTagBrowserTypesForSpecialAudience(specialAudience);

            return query.Get().Count() > 0;
        }

        #endregion

        #region Private

        private string GetKeyByRegionAndLanguage(string region = null, string language = null)
        {
            return $"{region ?? _selectedRegion}_{language ?? _selectedLanguage}";
        }

        private SlamQuery GetCalendarContentItemsBaseFilteredQuery(string region = null, string language = null, string[] segmentationIds = null, SlamQueryCacheBehavior? cacheBehavior = null, bool includeExpiredPrograms = false)
        {
            var query = _slamContext.CreateQuery()
                                                  .FilterRegion(string.IsNullOrWhiteSpace(region) ? _selectedRegion : region)
                                                  .FilterTaggedWith(string.IsNullOrWhiteSpace(language) ? _selectedLanguage : language)
                                                  .FilterSegmentations(segmentationIds != null ? segmentationIds : _marketingCenterWebApplicationService?.GetCurrentSegmentationIds())
                                                  .Filter(_CalendarFilter);
            if (includeExpiredPrograms)
            {
                query = query.AllowExpired()
                    // To include only one version of the expired program
                    .Filter("(ContentItem.ContentItemId NOT LIKE '%-p' OR NOT EXISTS (SELECT TOP 1 1 FROM ContentItem c1 WITH (NOLOCK) WHERE c1.ContentItemId = ContentItem.PrimaryContentItemId AND c1.StatusID = 9))")
                    // When including expired programs, still limit expired programs to those that expired in last 14 months
                    .Filter("(ContentItem.ExpirationDate IS NULL OR ContentItem.ExpirationDate >= dateadd(month, - 14, getdate()))");
            }
            else
            {
                query = query.FilterExpiredAssets();
            }
            if (cacheBehavior.HasValue)
            {
                query = query.Cache(cacheBehavior.Value);
            }

            return query;
        }

        #endregion
    }
}