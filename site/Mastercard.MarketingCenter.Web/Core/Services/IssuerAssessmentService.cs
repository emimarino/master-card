﻿using Dapper;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Models;
using Pulsus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Mastercard.MarketingCenter.Web.Core.Services
{
    public class IssuerAssessmentService
    {
        private readonly IDbConnection _connection;
        private readonly SlamContextService _slamContextService;
        private readonly ISettingsService _settingsService;

        public IssuerAssessmentService(IDbConnection connection, SlamContextService slamContextService, ISettingsService settingsService)
        {
            _connection = connection;
            _slamContextService = slamContextService;
            _settingsService = settingsService;
        }

        public ReportAssessmentModel GetReportAssessment(string cid)
        {
            var assessmentReport = _connection.Query("select * from MetricHeatmap where cid = @cid", new { cid }).FirstOrDefault() as IDictionary<string, object>;
            if (assessmentReport == null)
            {
                return null;
            }

            var reportAssessmentCriteriasModel = new ReportAssessmentModel();
            var assessmentCriterias = _slamContextService.GetAssessmentCriterias();
            var recommendations = _slamContextService.GetRecommendations();

            foreach (var assessmentCriteria in assessmentCriterias)
            {
                var assessmentColumnName = assessmentCriteria.AssessmentColumnName;
                var value = assessmentReport[assessmentColumnName];
                if (value == null)
                {
                    LogManager.EventFactory.Create()
                                           .Text($"Error on Assess Your Portfolio Report...")
                                           .Level(LoggingEventLevel.Error)
                                           .AddData("Message", $"The database column: {assessmentColumnName} especified in the Business Goal: {assessmentCriteria.Title} doesn't exists.")
                                           .AddData("Method", "GetReportAssessment")
                                           .AddData("CID", cid)
                                           .AddData("AssessmentCriteriaTitle", assessmentCriteria.Title)
                                           .AddData("AssessmentColumnName", assessmentColumnName)
                                           .Push();
                }
                else
                {
                    var strValue = Convert.ToString(value);
                    switch (strValue)
                    {
                        case "R":
                            reportAssessmentCriteriasModel.RedAssessments.Add(assessmentCriteria);
                            reportAssessmentCriteriasModel.AddRecommendations(GetYellowRedRecommendations(assessmentCriteria.ContentItemId, recommendations));
                            break;
                        case "G":
                            reportAssessmentCriteriasModel.GreenAssessments.Add(assessmentCriteria);
                            reportAssessmentCriteriasModel.AddRecommendations(GetGreenRecommendations(assessmentCriteria.ContentItemId, recommendations));
                            break;
                        case "Y":
                            reportAssessmentCriteriasModel.YellowAssessments.Add(assessmentCriteria);
                            reportAssessmentCriteriasModel.AddRecommendations(GetYellowRedRecommendations(assessmentCriteria.ContentItemId, recommendations));
                            break;
                    }
                }
            }

            return reportAssessmentCriteriasModel;
        }

        private IEnumerable<RecommendationDTO> GetYellowRedRecommendations(string assessmentCriteriaId, IEnumerable<RecommendationDTO> recommendations)
        {
            const string sql = @"select yra.RecommendationID from AssessmentCriteria ac 
                                 join YRAssessmentCriteriaRecommendation yra on ac.ContentItemId = yra.ContentItemID 
                                 where ac.ContentItemID = @assessmentCriteriaId";

            var ids = _connection.Query<string>(sql, new { assessmentCriteriaId });

            return recommendations.Where(o => ids.Contains(o.ContentItemId));
        }

        private IEnumerable<RecommendationDTO> GetGreenRecommendations(string assessmentCriteriaId, IEnumerable<RecommendationDTO> recommendations)
        {
            const string sql = @"select yra.RecommendationID from AssessmentCriteria ac 
                                 join GreenAssessmentCriteriaRecommendation yra on ac.ContentItemId = yra.ContentItemID 
                                 where ac.ContentItemID = @assessmentCriteriaId";

            var ids = _connection.Query<string>(sql, new { assessmentCriteriaId });

            return recommendations.Where(o => ids.Contains(o.ContentItemId));
        }

        public string CreateShareKey(int userId, string cid)
        {
            var shareKey = new ShareKey
            {
                UserID = userId,
                ShareKeyStatusID = MarketingCenterDbConstants.UserKeyStatus.Available,
                Key = Guid.NewGuid().ToString(),
                Cid = cid,
                EmailTo = _settingsService.GetShareReportToAddress(),
                DateKeyGenerated = DateTime.Now
            };

            var context = new MarketingCenterDbContext();
            context.ShareKeys.Add(shareKey);
            context.SaveChanges();

            return shareKey.Key;
        }

        public ShareKey GetShareKeyByKey(string key)
        {
            var context = new MarketingCenterDbContext();
            return (from sk in context.ShareKeys where sk.Key == key select sk).FirstOrDefault();
        }
    }
}