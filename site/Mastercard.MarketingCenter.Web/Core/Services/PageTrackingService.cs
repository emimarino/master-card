﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using SiteTrackingPageGroup = Mastercard.MarketingCenter.Common.Infrastructure.Constants.SiteTracking.PageGroup;

namespace Mastercard.MarketingCenter.Web.Core.Services
{
    public class PageTrackingService : IPageTrackingService
    {
        private readonly HttpContextBase _httpContext;
        private readonly TagHitRepository _tagHitRepository;
        private readonly AssetRepository _assetRepository;
        private readonly UserContext _userContext;
        private readonly ICookieService _cookieService;
        private readonly IMailDispatcherService _mailDispatcherService;
        private readonly ITrackingService _trackingService;
        private readonly IUnitOfWork _unitOfWork;

        public string RegionId { get { return _userContext.SelectedRegion; } }

        public PageTrackingService(
            HttpContextBase httpContext,
            TagHitRepository tagHitRepository,
            AssetRepository assetRepository,
            UserContext userContext,
            ICookieService cookieService,
            IMailDispatcherService mailDispatcherService,
            ITrackingService trackingService,
            IUnitOfWork unitOfWork)
        {
            _httpContext = httpContext;
            _tagHitRepository = tagHitRepository;
            _assetRepository = assetRepository;
            _userContext = userContext;
            _cookieService = cookieService;
            _mailDispatcherService = mailDispatcherService;
            _trackingService = trackingService;
            _unitOfWork = unitOfWork;
        }

        public void TrackPageAccess()
        {
            if (ExcludeRequest())
            {
                return;
            }

            string user = GetUser();
            long visitId = GetVisitId();
            string trackingType = _httpContext.Request.QueryString["trackingType"];
            string how = string.IsNullOrWhiteSpace(trackingType) ? "generic" : trackingType;
            string contentItemId = GetContentItemId(_httpContext.Request.Url.PathAndQuery, _httpContext.Request.UrlReferrer?.PathAndQuery, how, user, out string pageGroup, out ElectronicDeliveryCreateData electronicDeliveryCreateData);
            string who = electronicDeliveryCreateData != null ? electronicDeliveryCreateData.UserName : user;
            string regionId = electronicDeliveryCreateData != null ? electronicDeliveryCreateData.Region : RegionId;

            if (!_httpContext.Request.Url.PathAndQuery.StartsWith("javascript:"))
            {
                string from = IsExternalReferrer() ? _httpContext.Request.UrlReferrer?.AbsoluteUri : _httpContext.Request.UrlReferrer?.PathAndQuery;

                _trackingService.TrackPageVisit(regionId, visitId, _httpContext.Request.Url.PathAndQuery, who, from, how, contentItemId, pageGroup);
                _unitOfWork.Commit();
            }
            else
            {
                _trackingService.TrackPageVisit(regionId, visitId, _httpContext.Request.Url.PathAndQuery, who, null, how, contentItemId, null);
                _unitOfWork.Commit();
            }

            if (!string.IsNullOrWhiteSpace(trackingType) &&
                (trackingType.Contains(Common.Infrastructure.Constants.TrackingTypes.TriggerMarketingSubscriptionDaily) ||
                 trackingType.Contains(Common.Infrastructure.Constants.TrackingTypes.TriggerMarketingSubscriptionWeekly)))
            {
                int.TryParse(trackingType.Replace(Common.Infrastructure.Constants.TrackingTypes.TriggerMarketingSubscriptionDaily, string.Empty)
                                         .Replace(Common.Infrastructure.Constants.TrackingTypes.TriggerMarketingSubscriptionWeekly, string.Empty)
                                         .Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0],
                             out int mailDispatcherId);
                TrackMailDispatcher(mailDispatcherId);
            }
        }

        public void TrackClientClick(string what, string how)
        {
            if (ExcludeRequest() || string.IsNullOrEmpty(what))
            {
                return;
            }

            string user = GetUser();
            long visitId = GetVisitId();
            how = string.IsNullOrWhiteSpace(how) ? "generic" : how;
            string contentItemId = GetContentItemId(what, _httpContext.Request.UrlReferrer?.PathAndQuery, how, user, out string pageGroup, out ElectronicDeliveryCreateData electronicDeliveryCreateData);
            string who = electronicDeliveryCreateData != null ? electronicDeliveryCreateData.UserName : user;
            string regionId = electronicDeliveryCreateData != null ? electronicDeliveryCreateData.Region : RegionId;

            if (!what.StartsWith("javascript:"))
            {
                _trackingService.TrackPageVisit(regionId, visitId, what, who, _httpContext.Request?.UrlReferrer?.PathAndQuery, how, contentItemId, pageGroup);
                _unitOfWork.Commit();
            }
            else
            {
                _trackingService.TrackPageVisit(regionId, visitId, what, who, _httpContext.Request.UrlReferrer.PathAndQuery, how, contentItemId, null);
                _unitOfWork.Commit();
            }
        }

        private string GetContentItemId(string urlRequest, string urlReferrer, string how, string user, out string pageGroup, out ElectronicDeliveryCreateData electronicDeliveryCreateData)
        {
            electronicDeliveryCreateData = null;
            var contentItemId = _httpContext.Request.RequestContext.RouteData.Values["contentItemId"]?.ToString();
            if (string.IsNullOrEmpty(contentItemId))
            {
                contentItemId = _httpContext.Request.RequestContext.RouteData.Values["cid"] != null ? $"cid:{_httpContext.Request.RequestContext.RouteData.Values["cid"]}" : null;
            }

            bool isLibraryDownload = urlRequest.Contains("/librarydownload");
            bool isElectronicDownload = urlRequest.Contains("/electronicdownload");
            if (isLibraryDownload || isElectronicDownload)
            {
                pageGroup = isLibraryDownload ? SiteTrackingPageGroup.LibraryDownload : SiteTrackingPageGroup.ElectronicDownload;

                var asset = Regex.Match(urlReferrer ?? string.Empty, "(?<=/asset/)(\\w+)");
                if (asset.Success)
                {
                    contentItemId = asset.Value;
                }
                else if (isLibraryDownload)
                {
                    var filename = Regex.Match(urlRequest, "(?<=/librarydownload\\?url=)(.*)");
                    if (filename.Success)
                    {
                        var downloadableAsset = _assetRepository.GetDownloadableAssetByFilename(HttpUtility.UrlDecode(filename.Value));
                        if (downloadableAsset != null)
                        {
                            contentItemId = downloadableAsset.ContentItemId;
                        }
                        else
                        {
                            pageGroup = SiteTrackingPageGroup.LibraryDownloadError;
                        }
                    }
                    else
                    {
                        pageGroup = SiteTrackingPageGroup.LibraryDownloadError;
                    }
                }

                if (isElectronicDownload)
                {
                    var electronicDeliveryId = Regex.Match(urlRequest.ToUpperInvariant(), "[0-9A-F]{8}[-]?([0-9A-F]{4}[-]?){3}[0-9A-F]{12}[)}]?");
                    if (electronicDeliveryId.Success)
                    {
                        bool findByElectronicDeliveryId = true;
                        var estimatedDistribution = _assetRepository.GetDownloadableAssetEstimatedDistributionByElectronicDeliveryId(electronicDeliveryId.Value);
                        if (estimatedDistribution != null)
                        {
                            if (string.IsNullOrEmpty(contentItemId))
                            {
                                contentItemId = estimatedDistribution.ContentItemId;
                            }

                            findByElectronicDeliveryId = estimatedDistribution.ElectronicDelivery == null;
                            if (!findByElectronicDeliveryId &&
                               (estimatedDistribution.ElectronicDelivery.DownloadCount < 0 || estimatedDistribution.ElectronicDelivery.ExpirationDate < DateTime.Now))
                            {
                                pageGroup = SiteTrackingPageGroup.ElectronicDownloadError;
                            }
                        }

                        if (findByElectronicDeliveryId)
                        {
                            var electronicDelivery = _assetRepository.GetElectronicDelivery(electronicDeliveryId.Value);
                            if (electronicDelivery != null)
                            {
                                if (string.IsNullOrEmpty(contentItemId))
                                {
                                    if (electronicDelivery.OrderItem != null)
                                    {
                                        contentItemId = electronicDelivery.OrderItem.ItemId;
                                    }
                                    else
                                    {
                                        var downloadableAsset = _assetRepository.GetDownloadableAssetByFilename(electronicDelivery.FileLocation);
                                        if (downloadableAsset != null)
                                        {
                                            contentItemId = downloadableAsset.ContentItemId;
                                        }
                                        else
                                        {
                                            pageGroup = SiteTrackingPageGroup.ElectronicDownloadError;
                                        }
                                    }
                                }

                                if (electronicDelivery.DownloadCount < 0 || electronicDelivery.ExpirationDate < DateTime.Now)
                                {
                                    pageGroup = SiteTrackingPageGroup.ElectronicDownloadError;
                                }
                            }
                            else
                            {
                                pageGroup = SiteTrackingPageGroup.ElectronicDownloadError;
                            }
                        }

                        if (user.IsNullOrWhiteSpace())
                        {
                            electronicDeliveryCreateData = _assetRepository.GetElectronicDeliveryCreateData(electronicDeliveryId.Value);
                        }
                    }
                    else
                    {
                        pageGroup = SiteTrackingPageGroup.ElectronicDownloadError;
                    }
                }
            }
            else
            {
                pageGroup = IsReportPage(urlRequest, how, contentItemId) ? SiteTrackingPageGroup.ReportPage : null;
            }

            return contentItemId;
        }

        private long GetVisitId()
        {
            long visitID;
            var visitId = _cookieService.Get("VisitID");
            if (visitId == null)
            {
                var urlReferrer = string.Empty;
                if (IsExternalReferrer())
                {
                    urlReferrer = _httpContext.Request.UrlReferrer.AbsoluteUri;
                }

                visitID = _trackingService.CreateVisitId(urlReferrer);
                _cookieService.Set("VisitID", visitID.ToString(), null);
            }
            else
            {
                visitID = long.Parse(visitId);
            }

            return visitID;
        }

        private string GetUser()
        {
            if (_userContext.User != null && !string.IsNullOrEmpty(_userContext.User.UserName))
            {
                return _userContext.User.UserName;
            }

            return null;
        }

        public void TrackTagHit(string[] tags)
        {
            if (ExcludeRequest())
            {
                return;
            }

            _tagHitRepository.TrackTagHit(tags, GetVisitId(), GetUser());
        }

        private bool IsExternalReferrer()
        {
            if (_httpContext.Request.UrlReferrer != null &&
                (_httpContext.Request.UrlReferrer.Host != _httpContext.Request.Url.Host ||
                 !_httpContext.Request.UrlReferrer.PathAndQuery.Contains(_httpContext.Request.ApplicationPath)))
            {
                return true;
            }

            return false;
        }

        private bool IsReportPage(string urlRequest, string how, string contentItemId)
        {
            Uri.TryCreate(_httpContext.Request.Url, urlRequest, out Uri url);
            string[] actionPages = ConfigurationManager.AppSettings["ReportGroup.ActionPages"].ToString().Split(',');
            foreach (var action in actionPages)
            {
                if (url.IsRouteMatch("Report", action) ||
                   (_httpContext.Request.UrlReferrer != null && _httpContext.Request.UrlReferrer.IsRouteMatch("Report", action)) ||
                   (!string.IsNullOrEmpty(how) && !string.IsNullOrEmpty(contentItemId) && how.EndsWith(string.Format("downloadasset-{0}", contentItemId))))
                {
                    return true;
                }
            }

            return false;
        }

        public void TrackMailDispatcher(int mailDispatcherId)
        {
            var mailDispatcher = _mailDispatcherService.GetMailDispatcherById(mailDispatcherId);
            if (mailDispatcher != null)
            {
                if (mailDispatcher.MailDispatcherStatusId != MarketingCenterDbConstants.MailDispatcherStatus.Opened)
                {
                    mailDispatcher.MailDispatcherStatusId = MarketingCenterDbConstants.MailDispatcherStatus.Opened;
                    _mailDispatcherService.SaveMailDispatcher(mailDispatcher);
                    _unitOfWork.Commit();
                }
            }
            else
            {
                LogManager.EventFactory.Create()
                       .Text("Error Tracking Mail Dispatcher...")
                       .Level(LoggingEventLevel.Error)
                       .AddData("MailDispatcherId", mailDispatcherId)
                       .AddData("Message", "Mail Dispatcher not found")
                       .Push();
            }
        }

        private bool ExcludeRequest()
        {
            try
            {
                var referrer = _httpContext.Request.UrlReferrer;
                if (referrer == null)
                    return false;

                List<string> exclude = ConfigurationManager.AppSettings["Tracking.Blacklist"].ToString().Split(';').Select(x => x.Trim()).ToList();

                if (exclude.Any(x => x.ToLower().Equals(referrer.Host)))
                    return true;

                return false;
            }
            catch
            {
                return false;
            }
        }
    }
}