﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.Reporting;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Web.Core.Services
{
    public class ReportService
    {
        private readonly SlamContext _slamContext;
        private readonly ReportRepository _reportRepository;
        private readonly UserSubscriptionRepository _userSubscriptionRepository;

        public ReportService(SlamContext slamContext, ReportRepository reportRepository, UserSubscriptionRepository userSubscriptionRepository)
        {
            _slamContext = slamContext;
            _reportRepository = reportRepository;
            _userSubscriptionRepository = userSubscriptionRepository;
        }

        public IEnumerable<UsersSubscriptionDetailReportResultItem> GetUsersSubscriptionDetailReportQuery(List<ContentItemUserSubscription> contentItemUserSubscriptions)
        {
            List<UsersSubscriptionDetailReportResultItem> contentUserSubscriptions = new List<UsersSubscriptionDetailReportResultItem>();
            var contentItemList = _slamContext.CreateQuery().AllowExpired().FilterStatus(FilterStatus.GetAll).FilterContentItemId(contentItemUserSubscriptions.Where(x => !(x.ContentItemId.Contains("-p"))).Select(x => x.ContentItemId).ToArray()).Get().ToList().Select(x => x.AsContentItemListItem());
            foreach(var userSuscription in contentItemUserSubscriptions)
            {
                var item = contentItemList.Where(x => x.ContentItem.ContentItemId == userSuscription.ContentItemId).FirstOrDefault();
                if (item.EnableFavoriting)
                {
                    var subscriptionsByItem = contentItemUserSubscriptions.Where(s => s.ContentItemId.Equals(item.ContentItem.ContentItemId));                    
                    contentUserSubscriptions.Add(new UsersSubscriptionDetailReportResultItem
                    {
                        Title = item.Title,
                        Type = item.ContentType.AddSpacesToSentence(true),
                        SavedDate = userSuscription.SavedDate,
                        UserFullName = userSuscription.User.FullName,
                        Email = userSuscription.User.Email
                    });                    
                }
            }          
            return contentUserSubscriptions.OrderByDescending(us => us.SavedDate);
        }

        public IEnumerable<UsersSubscriptionDetailReportResultItem> GetUsersSubscriptionDetailReport(DateTime startDate, DateTime endDate, int userId, string regionId)
        {
            var contentItemUserSubscriptions = _userSubscriptionRepository.GetSubscribedContentItemUserSubscriptions(startDate, endDate, regionId).Where(x => x.UserId == userId).OrderByDescending(ci => ci.SavedDate);
            return (GetUsersSubscriptionDetailReportQuery(contentItemUserSubscriptions.ToList()));
        }

        public IEnumerable<UsersSubscriptionDetailReportResultItem> GetUsersSubscriptionListDetailReport(DateTime startDate, DateTime endDate, string regionId)
        {
            var contentItemUserSubscriptions = _userSubscriptionRepository.GetSubscribedContentItemUserSubscriptions(startDate, endDate, regionId).OrderByDescending(ci => ci.SavedDate);
            return (GetUsersSubscriptionDetailReportQuery(contentItemUserSubscriptions.ToList()));
        }

        public IEnumerable<SubscribableItemsReportResultItem> GetSubscribableItemsReport(DateTime startDate, DateTime endDate, string regionId)
        {
            return _slamContext.CreateQuery()
                               .FilterEnableFavoritingTypes()
                               .FilterByEnableFavoriting(true)
                               .AllowExpired()
                               .FilterStatus(FilterStatus.GetAll)
                               .Get()
                               .Select(a => a.AsContentItemListItem())
                               .GroupJoin(_userSubscriptionRepository.GetSubscribedContentItemUserSubscriptions(startDate, endDate, regionId),
                                          item => item.ContentItem.ContentItemId,
                                          userSubscription => userSubscription.ContentItemId,
                                          (item, userSubscriptions) => new SubscribableItemsReportResultItem
                                          {
                                              ItemId = item.ContentItem.ContentItemId,
                                              Title = item.Title,
                                              Type = item.ContentType.AddSpacesToSentence(true),
                                              Favorites = userSubscriptions.Count()
                                          })
                               .OrderByDescending(us => us.Favorites)
                               .ThenBy(us => us.Title);
        }

        public void PopulateSubscribableItemsDetailReportHeader(string itemId, out string itemType, out string itemTitle)
        {
            itemType = string.Empty;
            itemTitle = string.Empty;
            var contentItem = _slamContext.CreateQuery().FilterContentItemId(itemId).Get().FirstOrDefault();
            if (contentItem != null && (contentItem.StatusID == Status.Published || contentItem.StatusID == Status.PublishedAndDraft))
            {
                var item = contentItem.AsContentItemListItem();
                if (item.EnableFavoriting)
                {
                    itemType = item.ContentType.AddSpacesToSentence(true);
                    itemTitle = item.Title;
                }
            }
        }
    }
}