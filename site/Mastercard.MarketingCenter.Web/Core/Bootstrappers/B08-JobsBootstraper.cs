﻿using Hangfire;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Domo.Services;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Web.Core.Jobs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq.Expressions;
using System.Web.Configuration;
using System.Web.Hosting;

namespace Mastercard.MarketingCenter.Web.Core.Bootstrappers
{
    public class B08JobsBootstrapper : IBootstrapper
    {
        public void Execute()
        {
            if (HostingEnvironment.ApplicationHost.GetSiteName().Equals(WebConfigurationManager.AppSettings["JobScheduler.IIS.SiteName"], StringComparison.OrdinalIgnoreCase))
            {
                #region ClonedItemNotificationProcess
                RecurringJob.AddOrUpdate<ClonedItemNotificationProcessJob>("ClonedItemNotificationProcess",
                     (j) => j.Execute(null),
                     WebConfigurationManager.AppSettings["JobTimer.ClonedItemNotificationProcess"]
                     );

                #endregion

                #region ExpireContent

                RecurringJob.AddOrUpdate<ExpireContentJob>("ExpireContentJob",
                    (j) => j.Execute(),
                    WebConfigurationManager.AppSettings["JobTimer.ExpireContentJob"]
                    );

                #endregion

                #region ExpireContentNotification

                RecurringJob.AddOrUpdate<ExpiredContentNotificationJob>("ExpiredContentNotificationJob",
                    (j) => j.Execute(),
                    WebConfigurationManager.AppSettings["JobTimer.ExpiredContentNotificationJob"]
                    );

                #endregion

                #region MailDispatcher

                RecurringJob.AddOrUpdate<MailDispatcherJob>("MailDispatcher",
                    (j) => j.Execute(),
                    WebConfigurationManager.AppSettings["JobTimer.MailDispatcher"]
                    );

                #endregion

                #region TriggerMarketing

                RecurringJob.AddOrUpdate<TriggerMarketingJob>("TriggerMarketing.Daily",
                    (tm) => tm.ExecuteDaily(),
                    WebConfigurationManager.AppSettings["JobTimer.TriggerMarketingDaily"]
                    );

                RecurringJob.AddOrUpdate<TriggerMarketingJob>("TriggerMarketing.Weekly",
                    (tm) => tm.ExecuteWeekly(),
                    WebConfigurationManager.AppSettings["JobTimer.TriggerMarketingWeekly"]
                    );

                #endregion

                #region DeletePublishedTriggerMarketings

                RecurringJob.AddOrUpdate<DeletePublishedTriggerMarketingsJob>("TriggerMarketing.Deletions",
                    (tm) => tm.Execute(),
                    WebConfigurationManager.AppSettings["JobTimer.TriggerMarketingDeletions"]
                    );

                #endregion

                #region UpdateDomoData

                RecurringJob.AddOrUpdate<DomoServices>("UpdateDomoData",
                    (j) => j.UpdateData(),
                    ConfigurationManager.AppSettings["JobTimer.UpdateDomoData"]
                    );

                RecurringJob.AddOrUpdate<IDownloadsService>("UpdateDownloadsData",
                    (j) => j.ProcessData(),
                    ConfigurationManager.AppSettings["JobTimer.UpdateDomoDownloadData"]
                    );

                RecurringJob.AddOrUpdate<DomoServices>("ArchiveDomoData",
                    (j) => j.ArchiveData(),
                    ConfigurationManager.AppSettings["JobTimer.ArchiveDomoData"]
                    );

                #endregion

                #region UserDataExpirationJob

                AddJobIfEnabled<GdprUserDataExpirationJob>("GDPR.UserDataExpirationJob", "JobTimer.GdprUserDataExpirationJob", "GDPR.UserDataExpirationJob.Enabled", null, (gdpr) => gdpr.Execute(null), true, true); /*added allow default settings and region to null so the value obtained comes from the AppSetings element*/

                #endregion

                #region UserDataExpirationAwarenessEmailJob

                var possibleRegionsFroUserExpirationEmail = new List<string> { { "ap" }, { "ca" }, { "de" }, { "demo" }, { "fmm" }, { "lac" }, { "mea" }, { "us" } };
                possibleRegionsFroUserExpirationEmail.ForEach(region =>
                ExecuteGdprUserDataExpirationAwarenessEmailJob(region)
                );

                ExecuteGdprUserDataExpirationAwarenessEmailJob("us");

                #endregion

                #region ContentLinkValidatorJob

                RecurringJob.AddOrUpdate<ContentLinkValidatorJob>("ContentLinkValidator",
                    (j) => j.Execute(),
                    WebConfigurationManager.AppSettings["JobTimer.ContentLinkValidator"]
                    );

                #endregion

                #region MostPopular

                RecurringJob.AddOrUpdate<MostPopularJob>("RefreshMostPopularData",
                    (mp) => mp.RefreshMostPopularData(),
                    ConfigurationManager.AppSettings["JobTimer.RefreshMostPopularData"]
                    );

                #endregion

                #region ContentItemVisits

                RecurringJob.AddOrUpdate<ContentItemVisitsJob>("RefreshContentItemVisitsData",
                    (civ) => civ.RefreshContentItemVisitsData(),
                    ConfigurationManager.AppSettings["JobTimer.RefreshContentItemVisitsData"]
                    );

                #endregion

                #region SiteTracking

                RecurringJob.AddOrUpdate<SiteTrackingJob>("RefreshSiteTrackingData",
                    (st) => st.RefreshSiteTrackingData(),
                    ConfigurationManager.AppSettings["JobTimer.RefreshSiteTrackingData"]
                    );

                #endregion

                #region PricelessOffers

                AddJobIfEnabled<OfferJob>("RefreshPricelessOffers", "JobTimer.RefreshPricelessOffers", "PricelessApiClient.Enabled", null, (o) => o.RefreshPricelessOffers(), true, true);

                AddJobIfEnabled<OfferJob>("DeleteOldImportedOffers", "JobTimer.DeleteOldImportedOffers", "PricelessApiClient.Enabled", null, (o) => o.DeleteOldImportedOffers(), true, true);

                #endregion

                #region Salesforce

                AddJobIfEnabled<SalesforceIntegrationJob>("SalesforceIntegration", "JobTimer.SalesforceIntegration", "SalesforceIntegration.Enabled", null, (si) => si.Execute(), true, true);

                #endregion

                #region Files

                RecurringJob.AddOrUpdate<FileReviewJob>("FileReview",
                    (fr) => fr.Execute(),
                    ConfigurationManager.AppSettings["JobTimer.FileReview"]
                    );

                RecurringJob.AddOrUpdate<FileReviewJob>("FileMove",
                    (fr) => fr.FileMove(),
                    ConfigurationManager.AppSettings["JobTimer.FileReview"]
                    );

                RecurringJob.AddOrUpdate<FileReviewJob>("FileCopy",
                    (fr) => fr.FileCopy(),
                    ConfigurationManager.AppSettings["JobTimer.FileReview"]
                    );

                #endregion
            }
        }

        private static void ExecuteGdprUserDataExpirationAwarenessEmailJob(string region)
        {
            var jobName = $"GDPR.UserDataExpirationAwarenessEmailJob.{(region.Equals(RegionalizeService.GlobalRegion, StringComparison.InvariantCultureIgnoreCase) ? "GLOBAL" : region.ToUpperInvariant())}";
            AddJobIfEnabled<GdprUserDataExpirationAwarenessEmailJob>(jobName, "JobTimer.GdprUserDataExpirationAwarenessEmailJob", "ExpirationAwarenessEmail.AwarenessFollowUp.Enabled", region,
                (gdpr) => gdpr.Execute(region));
        }

        private static void AddJobIfEnabled<T>(string jobName, string timerSetting, string enabledKey, string region, Expression<Action<T>> methodCall, bool enableCanDefault = false, bool timerCanDefault = false)
        {
            bool.TryParse(RegionalizeService.RegionalizeSetting(enabledKey, region, enableCanDefault), out bool jobEnabled);
            if (jobEnabled)
            {
                RecurringJob.AddOrUpdate<T>(jobName, methodCall, RegionalizeService.RegionalizeSetting(timerSetting, region, timerCanDefault));
            }
            else
            {
                RecurringJob.RemoveIfExists(jobName);
            }
        }
    }
}