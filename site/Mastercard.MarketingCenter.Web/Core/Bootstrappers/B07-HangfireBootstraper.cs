﻿using Autofac;
using Hangfire;
using Hangfire.Dashboard;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Microsoft.Owin;
using Owin;
using System;
using System.Configuration;
using System.Web.Configuration;
using System.Web.Hosting;

[assembly: OwinStartup(typeof(Mastercard.MarketingCenter.Web.Core.Bootstrappers.B07HangfireBootstrapper))]
namespace Mastercard.MarketingCenter.Web.Core.Bootstrappers
{
    public class B07HangfireBootstrapper : IBootstrapper
    {
        public void Configuration(IAppBuilder app)
        {
            if (HostingEnvironment.ApplicationHost.GetSiteName().Equals(WebConfigurationManager.AppSettings["JobScheduler.IIS.SiteName"], StringComparison.OrdinalIgnoreCase))
            {
                app.UseHangfireDashboard("/hangfire", new DashboardOptions
                {
                    Authorization = new[] { new HangFireAuthorizationFilter() }
                });

                int maxJobs;
                int.TryParse(WebConfigurationManager.AppSettings.Get("JobScheduler.IIS.MaxJobs"), out maxJobs);
                var options = new BackgroundJobServerOptions() { WorkerCount = maxJobs > 0 ? maxJobs : 2 };
                app.UseHangfireServer(options);
                var monitor = JobStorage.Current.GetMonitoringApi();
                if (monitor.ProcessingCount() > 0)
                {
                    foreach (var job in monitor.ProcessingJobs(0, (int)monitor.ProcessingCount()))
                    {
                        BackgroundJob.Requeue(job.Key);
                    }
                }
            }
        }

        public void Execute()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["MastercardMarketingCenter"].ConnectionString;
            GlobalConfiguration.Configuration.UseSqlServerStorage(connectionString);
        }
    }

    public class HangFireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            // In case you need an OWIN context, use the next line, `OwinContext` class
            // is the part of the `Microsoft.Owin` package.
            var owinContext = new OwinContext(context.GetOwinEnvironment());

            // Allow all authenticated users to see the Dashboard (potentially dangerous).
            return owinContext.Authentication.User.Identity.IsAuthenticated;
        }
    }

    public class ContainerJobActivator : JobActivator
    {
        private IContainer _container;

        public ContainerJobActivator(IContainer container)
        {
            _container = container;
        }

        public override object ActivateJob(Type type)
        {
            return _container.Resolve(type);
        }
    }
}