﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using System.Web.Http;

namespace Mastercard.MarketingCenter.Web.Core.Bootstrappers
{
    public class B03WebApiBootstrapper : IBootstrapper
    {
        public void Execute()
        {
            LoadWebApiRoutes(GlobalConfiguration.Configuration);
        }

        private void LoadWebApiRoutes(HttpConfiguration httpConfiguration)
        {
            httpConfiguration.Routes.MapHttpRoute(
                name: "DefaultByRegion",
                routeTemplate: "api/{controller}/{region}/{language}",
                defaults: new { region = "us", language = "en" }
            );
        }
    }
}