﻿using System.Web.Configuration;
using AWS.Web.Data;
using Mastercard.MarketingCenter.Common.Infrastructure;

namespace Mastercard.MarketingCenter.Web.Core.Bootstrappers
{
	public class B04DatabaseInitializerBootstrapper : IBootstrapper
	{
		public void Execute()
		{
            DataServices.ConnectionString = WebConfigurationManager.ConnectionStrings["MasterCardMarketingCenter"].ConnectionString;
			DataServices.DisableCaching();
		}
	}
}