﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Core.Bootstrappers
{
    public class B09CacheBootstrapper : IBootstrapper
    {
        public void Execute()
        {
            var offerService = DependencyResolver.Current.GetService<IOfferService>();
            offerService.ReloadCardExclusivityModel();
            offerService.ReloadCategoryModel();
            offerService.ReloadOfferModel();
            offerService.ReloadMarketApplicabilityModel();
        }
    }
}