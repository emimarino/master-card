﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Slam.Cms;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Mastercard.MarketingCenter.Web.Core.Bootstrappers
{
    public class B05RoutingBootstrapper : IBootstrapper
    {
        public void Execute()
        {
            var routes = RouteTable.Routes;

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });

            LoadWebFormsRoutes(routes);
            LoadMvcRoutes(routes);

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        private void LoadWebFormsRoutes(RouteCollection routes)
        {
            var emtpyRoute = new RouteValueDictionary() { { "Controller", "" } };
            // routes with route values
            routes.MapPageRoute("ApproveOrder", "approveorder/{orderid}", "~/WebFormsPages/ApproveOrder.aspx", false, emtpyRoute);
            routes.MapPageRoute("RejectOrder", "rejectorder/{orderid}", "~/WebFormsPages/RejectOrder.aspx", false, emtpyRoute);
            routes.MapPageRoute("CreateAccount", "createaccount/{id}", "~/registration/CreatePassword.aspx", false, emtpyRoute);
            routes.MapPageRoute("CheckEmail", "checkemail/{email}/{region}", "~/registration/CheckYourEmail.aspx", false, new RouteValueDictionary() { { "Controller", "" }, { "region", "" } });
            routes.MapPageRoute("ForgotPasswordEmail", "forgotpassword/{email}", "~/registration/forgotpassword.aspx", false, emtpyRoute);
            routes.MapPageRoute("ResetPasswordUserKey", "resetpassword/{userkey}", "~/registration/ResetPassword.aspx", false, emtpyRoute);
            routes.MapPageRoute("OrderConfirmation", "orderconfirmation/{orderid}", "~/WebFormsPages/OrderConfirmation.aspx", false, emtpyRoute);
            routes.MapPageRoute("OrderDetails", "orderdetails/{orderid}", "~/WebFormsPages/ProofReview.aspx", false, emtpyRoute);
            routes.MapPageRoute("SendDataFiles", "senddatafile/{orderid}", "~/WebFormsPages/SendDataFile.aspx", false, emtpyRoute);
            routes.MapPageRoute("ElectronicDownload", "electronicdownload/{key}/{sku}", "~/WebFormsPages/ElectronicDelivery.aspx", false, new RouteValueDictionary() { { "Controller", "" }, { "sku", "" } });
            routes.MapPageRoute("RegistrationPending", "registration/pending/{guid}", "~/WebFormsPages/PendingRegistration.aspx", false, emtpyRoute);

            // routes without route values
            routes.MapPageRoute("Contact", "contact", "~/WebFormsPages/Contact.aspx", false, emtpyRoute);
            routes.MapPageRoute("OrderHistory", "orderhistory", "~/WebFormsPages/OrderHistory.aspx", false, emtpyRoute);
            routes.MapPageRoute("Register", "register", "~/registration/RegisterUser.aspx", false, emtpyRoute);            
            routes.MapPageRoute("ForgotPassword", "forgotpassword", "~/registration/forgotpassword.aspx", false, emtpyRoute);
            routes.MapPageRoute("ResetPassword", "resetpassword", "~/registration/ResetPassword.aspx", false, emtpyRoute);
            routes.MapPageRoute("ChangePassword", "changepassword", "~/registration/ChangePassword.aspx", false, emtpyRoute);
            routes.MapPageRoute("InvalidLink", "InvalidLink", "~/registration/InvalidLink.aspx", false, emtpyRoute);
            routes.MapPageRoute("ShoppingCart", "shoppingcart", "~/WebFormsPages/ShoppingCart.aspx", false, emtpyRoute);
            routes.MapPageRoute("EstimatedDistribution", "estimateddistribution", "~/WebFormsPages/EstimatedDistribution.aspx", false, emtpyRoute);
            routes.MapPageRoute("ShippingInformation", "shippinginformation", "~/WebFormsPages/ShippingInformation.aspx", false, emtpyRoute);
            routes.MapPageRoute("ShippingAllocation", "shippingallocation", "~/WebFormsPages/ShippingAllocation.aspx", false, emtpyRoute);
            routes.MapPageRoute("ReviewOrder", "revieworder", "~/WebFormsPages/ReviewOrder.aspx", false, emtpyRoute);
            routes.MapPageRoute("Checkout", "checkout", "~/WebFormsPages/CheckOut.aspx", false, emtpyRoute);
            routes.MapPageRoute("ThankYou", "thankyou", "~/WebFormsPages/Thankyou.aspx", false, emtpyRoute);
            routes.MapPageRoute("SalesForce", "salesforce", "~/WebFormsPages/SalesForce.aspx", false, emtpyRoute);

            routes.MapPageRoute("Logout", "logout", "~/WebFormsPages/Logout.aspx", false, emtpyRoute);

            routes.Add("ClonedAssetsReport", new Route("report/clonedassetsreport", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/ClonedAssetsReport.aspx", true)));
            routes.Add("ContentActivityReportLanding", new Route("report/contentactivitylanding", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/ContentActivityReportLanding.aspx", true)));
            routes.Add("ContentActivityDetailReport", new Route("report/contentactivitydetail/{id}", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/ContentActivityAssetDetailReport.aspx", true)));
            routes.Add("ContentActivityProgramReport", new Route("report/contentactivityprogram", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/ContentActivityProgramReport.aspx", true)));
            routes.Add("ContentActivityProgramAsset", new Route("report/contentactivityasset", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/ContentActivityAssetReport.aspx", true)));
            routes.Add("ContentActivityProgramPage", new Route("report/contentactivitypage", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/ContentActivityPageReport.aspx", true)));
            routes.Add("ContentActivityProgramTag", new Route("report/contentactivitytag", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/ContentActivityTagReport.aspx", true)));
            routes.Add("ContentActivityProgramWebinar", new Route("report/contentactivitywebinar", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/ContentActivityWebinarReport.aspx", true)));
            routes.Add("ContentActivityProgramOffer", new Route("report/contentactivityoffer", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/ContentActivityOfferReport.aspx", true)));
            routes.Add("ProcessorActivityReportLanding", new Route("report/processoractivitylanding", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/ProcessorActivityReportLanding.aspx", true)));
            routes.Add("ProcessorActivityReport", new Route("report/processoractivity", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/ProcessorActivityReport.aspx", true)));
            routes.Add("ProcessorActivityDetailReport", new Route("report/processoractivitydetail/{id}", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/ProcessorActivityDetailReport.aspx", true)));
            routes.Add("UserRegistrationReportLanding", new Route("report/userregistrationlanding", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/UserRegistrationReportLanding.aspx", true)));
            routes.Add("UserRegistrationReport", new Route("report/userregistration", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/UserRegistrationReport.aspx", true)));
            routes.Add("OrderingActivityReportLanding", new Route("report/orderingactivitylanding", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/OrderingActivityReportLanding.aspx", true)));
            routes.Add("OrderingActivityReport", new Route("report/orderingactivity", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/OrderingActivityReport.aspx", true)));
            routes.Add("OrderingActivityDetailReport", new Route("report/orderingactivitydetail/{id}", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/OrderingActivityDetailReport.aspx", true)));
            routes.Add("OrderingActivityReportIssuer", new Route("report/orderingactivityissuer", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/OrderingActivityIssuerReport.aspx", true)));
            routes.Add("OrderingActivityDetailReportIssuer", new Route("report/orderingactivitydetailissuer/{id}", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/OrderingActivityDetailIssuerReport.aspx", true)));
            routes.Add("SearchTermReportLanding", new Route("report/searchtermlanding", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/SearchTermReportLanding.aspx", true)));
            routes.Add("SearchTermReport", new Route("report/searchterm", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/SearchTermReport.aspx", true)));
            routes.Add("PendingRegistrationsReport", new Route("report/pendingregistrations", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/PendingRegistrationsReport.aspx", true)));
            routes.Add("IssuerAssessmentUsageReportLanding", new Route("report/issuerassessmentusagelanding", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/IssuerAssessmentUsageReportLanding.aspx", true)));
            routes.Add("IssuerAssessmentUsageReport", new Route("report/issuerassessmentusage", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/IssuerAssessmentUsageReport.aspx", true)));
            routes.Add("IssuerAssessmentUsageDetailReport", new Route("report/issuerassessmentusagedetail/{reportType}", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/IssuerAssessmentUsageDetailReport.aspx", true)));
            routes.Add("IssuerAssessmentUsageLeadGensReport", new Route("report/issuerassessmentusageleadgens", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/IssuerAssessmentUsageLeadGensReport.aspx", true)));
            routes.Add("IssuerAssessmentUsageMatchingErrorsReport", new Route("report/issuerassessmentusagematchingerrors", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/IssuerAssessmentUsageMatchingErrorsReport.aspx", true)));
            routes.Add("IssuerMatchingReport", new Route("report/issuermatching", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/IssuerMatchingReport.aspx", true)));
            routes.Add("MyFavoritesImagesReport", new Route("report/myfavoritesimages", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/MyFavoritesImagesReport.aspx", true)));
            routes.Add("MostPopularImagesReport", new Route("report/mostpopularimages", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/MostPopularImagesReport.aspx", true)));

            routes.Add("EstimatedDistributionReportLanding", new Route("report/estimateddistributionlanding", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/EstimatedDistributionReportLanding.aspx", true)));
            routes.Add("EstimatedDistributionAssetDetailReport", new Route("report/estimateddistributionassetdetail/{id}", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/EstimatedDistributionAssetDetailReport.aspx", true)));
            routes.Add("EstimatedDistributionProgramDetailReport", new Route("report/estimateddistributionprogramdetail/{id}", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/EstimatedDistributionProgramDetailReport.aspx", true)));
            routes.Add("EstimatedDistributionIssuerDetailReport", new Route("report/estimateddistributionissuerdetail/{id}", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/EstimatedDistributionIssuerDetailReport.aspx", true)));
            routes.Add("EstimatedDistributionProgramReport", new Route("report/estimateddistributionprogram", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/EstimatedDistributionProgramReport.aspx", true)));
            routes.Add("EstimatedDistributionProgramAsset", new Route("report/estimateddistributionasset", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/EstimatedDistributionAssetReport.aspx", true)));
            routes.Add("EstimatedDistributionProgramIssuer", new Route("report/estimateddistributionissuer", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/EstimatedDistributionIssuerReport.aspx", true)));

            routes.Add("IssuerSegmentationReport", new Route("report/issuersegmentation", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/IssuerSegmentationReport.aspx", true)));
            routes.Add("IssuerSegmentationDetailReport", new Route("report/issuersegmentationdetail/{segmentationId}", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/IssuerSegmentationDetailReport.aspx", true)));

            routes.Add("BrokenLinksReport", new Route("report/brokenlinks", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/BrokenLinksReport.aspx", true)));

            routes.Add("ContentReport", new Route("report/contentreport", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/ContentReportLanding.aspx", true)));
            routes.Add("ContentDetailsReport", new Route("report/contentreportdetail", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/ContentDetailReport.aspx", true)));

            routes.Add("UsersSubscriptionReportLanding", new Route("report/userssubscriptionlanding", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/UsersSubscriptionReportLanding.aspx", true)));
            routes.Add("UsersSubscriptionReport", new Route("report/userssubscription", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/UsersSubscriptionReport.aspx", true)));
            routes.Add("UsersSubscriptionDetailReport", new Route("report/userssubscriptiondetail/{userId}", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/UsersSubscriptionDetailReport.aspx", true)));
            routes.Add("SubscribableItemsReportLanding", new Route("report/subscribableitemslanding", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/SubscribableItemsReportLanding.aspx", true)));
            routes.Add("SubscribableItemsReport", new Route("report/subscribableitems", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/SubscribableItemsReport.aspx", true)));
            routes.Add("SubscribableItemsDetailReport", new Route("report/subscribableitemsdetail/{itemId}", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/SubscribableItemsDetailReport.aspx", true)));
            routes.Add("TriggerMarketingNotificationsReportLanding", new Route("report/triggermarketingnotificationslanding", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/TriggerMarketingNotificationsReportLanding.aspx", true)));
            routes.Add("TriggerMarketingNotificationsReport", new Route("report/triggermarketingnotifications", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/TriggerMarketingNotificationsReport.aspx", true)));
            routes.Add("TriggerMarketingNotificationsDetailReport", new Route("report/triggermarketingnotificationsdetail/{triggerMarketingSubscription}", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/TriggerMarketingNotificationsDetailReport.aspx", true)));

            routes.Add("GdprFollowUpReport", new Route("report/gdprfollowup", null, new RouteValueDictionary(new { controller = new PageConstraint() }), new PageRouteHandler("~/Reports/GdprFollowUpReport.aspx", true)));
        }

        private void LoadMvcRoutes(RouteCollection routes)
        {
            routes.MapRoute("Admin", "admin/{action}", new { controller = "Admin", action = "Index" });
            routes.MapRoute("Profile", "profile", new { controller = "Profile", action = "Index" });
            routes.MapRoute("CacheInvalidateRolePermissions", "cache/invalidaterolepermissions/{id}/{region}", new { controller = "Cache", action = "InvalidateRolePermissions" });
            routes.MapRoute("Cache", "cache/{action}/{id}", new { controller = "Cache", action = "Index", id = UrlParameter.Optional });

            routes.MapRoute("HeaderSegmentChanged", "header/segmentchanged", new { controller = "Header", action = "SegmentChanged" });
            routes.MapRoute("HeaderCurateSearchText", "header/curatesearchtext", new { controller = "Header", action = "CurateSearchText" });
            routes.MapRoute("HeaderFeedback", "header/feedback/{giveFeedback}", new { controller = "Header", action = "Feedback" });
            routes.MapRoute("FileDownload", "file/download", new { controller = "File", action = "Download" });
            routes.MapRoute("HtmlDownload", "file/htmldownload/{id}/{title}", new { controller = "File", action = "HtmlDownload" });
            routes.MapRoute("FileLibraryDownload", "file/librarydownload", new { controller = "File", action = "LibraryDownload" });
            routes.MapRoute("Error404", "error404", new { controller = "Error", action = "Error404" });
            routes.MapRoute("Error", "error/{*code}", new { controller = "Error", action = "Error" });

            routes.MapRoute("Dynamic", "dynamic", new { controller = "Dynamic", action = "Overview" });
            routes.MapRoute("Page", "page/{pageurl}", new { controller = "Page", action = "Page" });
            routes.MapTagBrowser("tagbrowser", "Tag Browser", new { controller = "TagBrowser", action = "Index" });

            routes.MapRoute("DetailPagesAsset", "asset/{contentItemId}/{*title}", new { controller = "ContentDetail", action = "Asset" });
            routes.MapRoute("DetailPagesProgram", "program/{contentItemId}/{*title}", new { controller = "ContentDetail", action = "Program" });
            routes.MapRoute("DetailPagesWebinar", "webinar/{contentItemId}/{*title}", new { controller = "ContentDetail", action = "Webinar" });

            // these are for matching old content from the old site routes
            routes.MapRoute("OldItem", "item/{globalid}/{*title}", new { controller = "ContentDetail", action = "MigratedContent" });
            routes.MapRoute("OldDownload", "Download/{globalid}/{*title}", new { controller = "ContentDetail", action = "MigratedContent" });

            routes.MapRoute("MasterCardResources", "mc-resources", new { controller = "Page", action = "MasterCardResources" });
            routes.MapRoute("Calendar", "calendar", new { controller = "Calendar", action = "Index" });

            routes.MapRoute("SendContactForm", "report/SendContactForm", new { controller = "Report", action = "SendContactForm" });
            routes.MapRoute("SendShareForm", "report/SendShareForm", new { controller = "Report", action = "SendShareForm" });
            routes.MapRoute("Optimize", "optimization/optimizationresources", new { controller = "Report", action = "Optimize" });
            routes.MapRoute("ReportGoal", "optimization/optimizationresource/{contentItemId}/{returnToOptimize}", new { controller = "Report", action = "Goal", returnToOptimize = UrlParameter.Optional });
            routes.MapRoute("Report", "assessyourportfolio/{showReportCover}", new { controller = "Report", action = "Index", showReportCover = UrlParameter.Optional });
            routes.MapRoute("DashboardLanding", "optimization", new { controller = "Report", action = "DashboardLanding" });
            routes.MapRoute("ReportShare", "report/share/{key}", new { controller = "Report", action = "Share" });
            routes.MapRoute("ReportAdmin", "report/{cid}/{showReportCover}", new { controller = "Report", action = "Admin", showReportCover = UrlParameter.Optional });

            routes.MapRoute("SegmentsSummary", "segments/summary", new { controller = "Dynamic", action = "DynamicPage", nodeKey = "segments-summary", parentKey = "segments" });
            routes.MapRoute("SSO", "sso/{region}", new { controller = "Sso", action = "Index" });
            routes.MapRoute("SearchResults", "search/GetSearchResults", new { controller = "Search", action = "GetSearchResults" });
            routes.MapRoute("Search", "search/{*searchQuery}", new { controller = "Search", action = "Index", searchQuery = UrlParameter.Optional });

            routes.MapRoute("OfferLanding", "offers", new { controller = "Offer", action = "Index" });
            routes.MapRoute("OfferList", "offer/list", new { controller = "Offer", action = "List" });
            routes.MapRoute("OfferFilters", "offer/filters", new { controller = "Offer", action = "Filters" });
            routes.MapRoute("OfferEventLocationFilter", "offer/eventlocationfilter", new { controller = "Offer", action = "EventLocationFilter" });
            routes.MapRoute("OfferRefreshPricelessOffers", "offer/refreshpricelessoffers", new { controller = "Offer", action = "RefreshPricelessOffers" });
            routes.MapRoute("OfferDetails", "offer/{contentItemId}/{*title}", new { controller = "Offer", action = "Details" });
        }

        public class PageConstraint : IRouteConstraint
        {
            public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
            {
                if (routeDirection == RouteDirection.IncomingRequest)
                {
                    return true;
                }

                return false;
            }
        }
    }
}