﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Web.Core.Filters;
using Slam.Cms;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Core.Bootstrappers
{
    public class B01MvcBootstrapper : IBootstrapper
    {
        public void Execute()
        {
            BoC.Web.Mvc.PrecompiledViews.ApplicationPartRegistry.Register(typeof(Slam.Cms.Admin.Areas.AdminBrokenLink.Controllers.BrokenLinkController).Assembly);

            AreaRegistration.RegisterAllAreas();

            ModelBinderProviders.BinderProviders.Add(new SlamModelBinderProvider());

            ModelBinders.Binders.Add(typeof(TagBrowserParameters), new TagBrowserParametersModelBinder());
            ModelBinders.Binders.Add(typeof(PagerConfiguration), new PagerConfigurationModelBinder());

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new SlamViewEngine());

            RegisterGlobalFilters(GlobalFilters.Filters);
        }

        private static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            // adding Unhandled Exception filter to all requests
            filters.Add(new UnhandledExceptionFilterAttribute());
        }
    }
}