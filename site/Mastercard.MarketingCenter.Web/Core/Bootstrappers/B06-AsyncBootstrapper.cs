﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services;

namespace Mastercard.MarketingCenter.Web.Core.Bootstrappers
{
    public class B06AsyncBootstrapper : IBootstrapper
    {
        public void Execute()
        {
            int dummySearchAutoComplete;
            var callerSearchAutoComplete = new AsyncSearchAutoCompleteItemsMethodCaller(SearchAutoCompleteService.GetSearchAutoCompleteItems);
            callerSearchAutoComplete.BeginInvoke(3000, out dummySearchAutoComplete, SearchAutoCompleteService.CallbackMethod, null);
        }
    }
}