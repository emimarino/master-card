﻿using Autofac;
using Autofac.Integration.Mvc;
using Hangfire;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Domo.Repository;
using Mastercard.MarketingCenter.Domo.Services;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Modules;
using Mastercard.MarketingCenter.Web.Core.Modules;
using System.Reflection;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Core.Bootstrappers
{
    public class B02AutofacBootstrapper : IBootstrapper
    {
        public void Execute()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(Assembly.GetExecutingAssembly())
                   .PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);

            builder.RegisterControllers(typeof(Slam.Cms.Admin.Areas.AdminBrokenLink.Controllers.BrokenLinkController).Assembly)
                   .PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies);

            builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinderProvider();

            builder.RegisterType<Filters.CustomAuthorizeAttribute>().PropertiesAutowired();
            builder.RegisterFilterProvider();

            builder.RegisterModule(new AutofacWebTypesModule());
            builder.RegisterModule(new ContextModule(true));
            builder.RegisterModule(new WebCoreModule());
            builder.RegisterModule(new ServicesModule(true, true));
            builder.RegisterModule(new AutoMapperModule());
            builder.RegisterModule(new JobsModule());

            builder.RegisterModule<DomoPersistanceModule>();
            builder.RegisterModule<DomoServiceModule>();

            var container = builder.Build();

            var provider = new StandaloneLifetimeScopeProvider(container);
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container, provider));
            GlobalConfiguration.Configuration.UseActivator(new ContainerJobActivator(container));
        }
    }
}