﻿using Autofac;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Web.Core.Jobs;

namespace Mastercard.MarketingCenter.Web.Core.Modules
{
    public class JobsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<TriggerMarketingJob>();
            builder.RegisterType<DeletePublishedTriggerMarketingsJob>();
            builder.RegisterType<GdprUserDataExpirationJob>();
            builder.RegisterType<GdprDataAccessJob>();
            builder.RegisterType<GdprUserDataExpirationAwarenessEmailJob>();
            builder.RegisterType<ExpireContentJob>();
            builder.RegisterType<MailDispatcherJob>();
            builder.RegisterType<ExpiredContentNotificationJob>();
            builder.RegisterType<DomoServices>();
            builder.RegisterType<ConsentsUpdateJob>();
            builder.RegisterType<ClonedItemNotificationProcessJob>();
            builder.RegisterType<ClonedItemNotificationEmailBuilderJob>();
            builder.RegisterType<ContentLinkValidatorJob>();
            builder.RegisterType<MostPopularJob>();
            builder.RegisterType<ContentItemVisitsJob>();
            builder.RegisterType<SiteTrackingJob>();
            builder.RegisterType<OfferJob>();
			builder.RegisterType<SalesforceIntegrationJob>();
            builder.RegisterType<FileReviewJob>();
        }
    }
}