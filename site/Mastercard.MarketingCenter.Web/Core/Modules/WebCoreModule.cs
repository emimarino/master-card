﻿using Autofac;
using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Services;
using Newtonsoft.Json;
using Slam.Cms;
using Slam.Cms.Common.Security;
using Slam.Cms.Data;
using System.Configuration;
using System.Web;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Web.Core.Modules
{
    public class WebCoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<GdpApiService>().As<IGdpApiService>();
            builder.RegisterType<PageTrackingService>().As<IPageTrackingService>();
            builder.RegisterType<HttpHeaderService>();
            builder.RegisterType<RedirectionService>();
            builder.RegisterType<IssuerAssessmentService>();
            builder.RegisterType<UserSubscriptionService>();
            builder.RegisterType<AuthorizationService>();
            builder.RegisterType<Services.UserService>();
            builder.RegisterType<MarketingCenterWebApplicationService>().As<IMarketingCenterWebApplicationService>().InstancePerLifetimeScope();
            builder.RegisterType<MastercardSitemap>().As<Sitemap>().InstancePerLifetimeScope();
            builder.RegisterType<ReportService>(); // TODO: move code from Web.Core.ReportService into Services.Services.ReportService
            builder.RegisterType<HttpContextCachingService>().As<ICachingService>();

            builder.Register(c => new ContentLinkValidatorService(c.Resolve<BrokenContentLinkRepository>(), c.Resolve<ContentItemRepository>(), new MastercardSitemap(new UserContext("en", "us")), c.Resolve<ISettingsService>())).As<IContentLinkValidatorService>();

            builder.Register(c =>
            {
                return new SlamQueryInitializer(GetRegion(HttpContext.Current));
            });

            //SlamContextService
            builder.Register(c =>
            {
                return new SlamContextService(c.Resolve<SlamContext>(), GetRegion(HttpContext.Current), GetLanguage(HttpContext.Current));
            });
        }

        private string GetRegion(HttpContext _context)
        {
            if (_context != null)
            {
                var cookieName = GetService<CookieNameProviderService>().GetUserContextCookieName();
                var ucCookie = _context.Request.Cookies.Get(cookieName);
                if (ucCookie != null)
                {
                    var ucCookieJson = EncryptionService.Decrypt(ucCookie.Value);
                    var ucCookieObject = JsonConvert.DeserializeObject<Common.Infrastructure.UserContextCookie>(ucCookieJson);

                    return string.IsNullOrEmpty(ucCookieObject.SelectedRegion) ? ConfigurationManager.AppSettings["DefaultRegion"] : ucCookieObject.SelectedRegion;
                }
            }

            return ConfigurationManager.AppSettings["DefaultRegion"];
        }

        private string GetLanguage(HttpContext _context)
        {
            if (_context != null)
            {
                var cookieName = GetService<CookieNameProviderService>().GetUserContextCookieName();
                var ucCookie = _context.Request.Cookies.Get(cookieName);
                if (ucCookie != null)
                {
                    var ucCookieJson = EncryptionService.Decrypt(ucCookie.Value);
                    var ucCookieObject = JsonConvert.DeserializeObject<Common.Infrastructure.UserContextCookie>(ucCookieJson);

                    return string.IsNullOrEmpty(ucCookieObject.SelectedLanguage) ? ConfigurationManager.AppSettings["DefaultLanguage"] : ucCookieObject.SelectedLanguage;
                }

            }

            return ConfigurationManager.AppSettings["DefaultLanguage"];
        }

        public static T GetService<T>()
        {
            try
            {
                return DependencyResolver.Current.GetService<T>();
            }
            catch
            {
                return default(T);
            }
        }
    }
}