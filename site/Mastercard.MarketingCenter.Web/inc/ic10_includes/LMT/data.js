﻿var data = {
    'tabs': [
		{ name: 'LMT<br>&nbsp;', hrefTabName: 'smbiz', pdfHref: '/portal/inc/ic10_includes/print_insight_smbiz_10.pdf', swithButtonHref: '/portal/calendar/smbiz',
		    categories: [
				{ name: 'MARKET TRENDS AND INSIGHTS',
				    types: [
						{ name: 'Consumer Pulse Reports', color: 'tan',
						    tooltip: '<p>Understanding the economic mindset and behaviors of U.S. consumers can help your organization make better business decisions. MasterCard&rsquo;s Consumer Pulse Report provides an overview of consumers&rsquo; attitudes and behavior using third-party research, insight into consumer spending with SpendingPulse&trade; 24-month sales trends, and up-to-date key consumer confidence indices.</p>',
						    items: [
								{ name: 'February 2010',
								    tooltip: '<p>As American consumers focus on minimizing their own financial risk, they aren&rsquo;t just getting by with less, they&rsquo;re also doing (and helping others) more</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g31d">Read more</a>.</p>',
								    months: { february: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g31d'} }
								},
								{ name: 'March 2010', color: 'tan',
								    tooltip: '<p>As consumer analysts begin to predict the shape of a rebound, they are looking for millennials and gen xers to lead the way.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g367">Read more</a>.</p>',
								    months: { march: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g367'} }
								},
								{ name: 'April 2010',
								    tooltip: '<p>America&rsquo;s general economic mood is increasingly stable, but without any major signals from the job market, recovery has yet to take on an accelerated momentum.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g368">Read more</a>.</p>',
								    months: { april: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g368'} }
								},
								{ name: 'May 2010',
								    tooltip: '<p>American consumers who are opening their pocketbooks are increasingly asking companies and brands for the three c’s: customization, convenience, and control.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g3c9">Read more</a>.</p>',
								    months: { may: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g3c9'} }
								},
								{ name: 'June 2010',
								    tooltip: '<p>The freeze on expenses may finally be thawing as American consumers seek hot summer travel deals.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g3ec">Read more</a>.</p>',
								    months: { june: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g3ec'} }
								},
								{ name: 'July 2010',
								    tooltip: '<p>The gender divide is narrowing as women cautiously spearhead the economic recovery while men continue to reel as one of the biggest victims of the “mancession”</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g432">Read more</a>.</p>',
								    months: { july: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g432'} }
								},
								{ name: 'August 2010',
								    tooltip: '<p>The age of the frugal shopper shows no signs of abating, much to the dismay of retailers hoping that consumers will start opening up their wallets, even as new services emerge to make bargain shopping easier.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g4b1">Read more</a>.</p>',
								    months: { august: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4b1' } }
								},
								{ name: 'September 2010',
									tooltip: '<p>Savvy travelers looking for savings and convenience are finding a slew of new mobile and online tools that offer the benefits of a travel agent and concierge at their fingertips.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g4b9">Read more</a>.</p>',
								    months: { september: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4b9'} }
								},
								{ name: 'October 2010',
								   tooltip: '<p>With the holiday season set to begin, retailers are banking flexible payment schedules and aggressive promotions to coax even the staunchest of scrooges to come out to the stores this year.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g4c7">Read more</a>.</p>',
								    months: { october: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4c7'} }
								},
								{ name: 'November 2010',
								    months: { november: true }
								},
								{ name: 'December 2010',
								    months: { december: true }
								}

							]
						},
						{ name: 'MasterCard Advisors SpendingPulse&trade;', color: 'tan', months: { allyear: true },
						    tooltip: '<p>MasterCard Advisors SpendingPulse&trade;, a macroeconomic indicator, tracks retail sales for the U.S., segmented by category and region, as well as total sales in the U.K. Based on aggregated sales activity in the MasterCard payments network, coupled with estimates for all other payments forms including cash and check, SpendingPulse&trade; helps issuers and merchants determine the optimal timing of marketing campaigns. </p>', whiteTooltip: '<p>For more information, contact us at <a href="mailto:partnerships@mastercard.com">partnerships@mastercard.com</a>.</p>'

						},
						{ name: '2010 Quarterly Economic Update',
						    tooltip: '<p>Stay informed on macroeconomic market conditions, key indicators, consumer sentiment, and industry trends with quarterly webinars hosted by MasterCard. Each webinar offers the expertise of leading economists. Insights into consumer attitudes, behaviors, and spending, as well as viewpoints and recommended actions, are shared by MasterCard experts.</p>',
						    items: [
								{ name: 'March 2010', color: 'blue',
								    tooltip: '<p>Actionable insights from the March 3, 2010, Webinar include: </p> \<ul><li><em>A Macroeconomic Overview of the U.S. Economy</em> by Bart van Ark, vice president and chief economist at The Conference Board.</li> <li><em>Debit’s Gain, Credit’s Loss: Not a Zero-Sum Game</em> by Prasad Iyer, vice president, Marketing Intelligence and Planning</li> <li><em>What It All Means</em> by Ted Iacobuzio, senior vice president, Marketing Intelligence and Planning</li></ul> <p>For more information about these webinars and how to access presentation replays, <a href="https://optimization.mastercard.com/portal/insights/webinars">click here.</a></p>',
								    months: { march: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'June 2010', color: 'blue',
								    tooltip: '<p>Topics discussed in the June 9, 2010 webinar included:</p> \<ul><li><em>U.S. Economic Overview</em> by Kathy Bostjancic, senior economic advisor at The Conference Board.</li> <li><em>The Affluent Consumer by</em> Kimberly Purcell, vice president, MasterCard Global Insights</li> <li><em>Overdraft Rule (Regulation E) Discussion</em> by Richard Rozbicki, vice president, MasterCard Product Management</li></ul> <p>For more information about these webinars and how to access presentation replays, <a href="/portal/insights/webinars">click here.</a></p>',
								    months: { june: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'September 2010', color: 'blue',
								    tooltip: '<p>Topics discussed at the Sept 16, 2010 webinar included: <ul><li><em>U.S. Economic Overview</em> by Bart van Ark, vice president and chief economist at The Conference Board</li><li><em>State of the U.S. Consumer</em> by Theodore Iacobuzio, vice president, MasterCard Global Insights </li><li><em>New Business Models for No-Longer-Profitable Customers</em> by Ben Isaacson, vice president, MasterCard Global Insights</li> </ul>For more infomation about these webinars, <a href="/portal/insights/webinars">click here</a>.</p>',
								    months: { september: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'December 2010', color: 'blue',
								    tooltip: '<p>Join us in December for the webinar: <a target="_blank" href="/portal/insights/webinars">click here</a> to register.</p>',
								    months: { december: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								}
							]
						},
						{ name: 'MasterCard Webinar Series', color: 'tan',
						    tooltip: '<p>In this series, MasterCard experts examine key trends in the payments industry critical to the success of community banks and credit unions in the current economic environment. Pre-register for the free MasterCard Worldwide Webinar Series, presented in partnership with Callahan &amp; Associates and CreditUnions.com.</p>',
						    items: [
								{ name: 'Credit Unions and Small Business: A Mutual Opportunity',
								    tooltip: '<p>Join us in November for the webinar: <a target="_blank" href="http://www.creditunions.com/emails10/CUtv/Mastercard_Webinar_Register.html">click here</a> to register.</p> ',						
								    months: { november: { active: true, icon: 'video', href: 'http://www.creditunions.com/emails10/CUtv/Mastercard_Webinar_Register.html', newWindow: true} }
								}
							]
						},
						{ name: 'MasterCard Commecial Payments Insights Series (webinars)', color: 'blue',
						    tooltip: '<p>MasterCard brings you this insights series to help optimize your payments business opportunities with large corporate and middle market companies, and small business owners.</p>',
						    items: [
								{ name: 'Small Business Insights',
								    tooltip: '<p>For more information and upcoming topics, register <a target="_blank" href="https://www.eiseverywhere.com/ehome/index.php?eventid=16355&eb=currentblast">here</a>.',						
								    months: { 
										february: true,
										march: true,
										may: true,
										june: true,
										november: true
										}
								}
							]
						},
						{ name: 'White Paper', color: 'green',
						    tooltip: '<p>MasterCard offers thoughtful perspectives and its viewpoint on a range of issues facing the payments industry, including such topics as new business models for a new economy, the impact of regulatory changes, and insights on consumer payment preferences. Check back often, as topics are updated frequently.</p>',
						    items: [
								{ name: 'Acquisition, Penetration, and Onboarding: Increasing Revenue By Expanding and Optimizing a Debit Card Portfolio',
								    tooltip: '<p>Despite the complex criteria that lead small business customers to one issuer—or another—there is one constant for every financial institution looking to improve its performance: The more successfully an issuer is able to motivate particular spending behaviors in the weeks immediately following an account acquisition, the more value the account will have.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g3ce">Read more</a>.</p>',
								    months: { june: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g3ce'} }
								},
								{ name: 'Credit Unions and Small Business: A Mutual Opportunity',
						
								    months: { september: true }
								}
							]
						}
					]
				},
				{ name: 'Portfolio Performance',
				    types: [
						{ name: 'MasterCard Advisors PortfolioAnalytics', months: { allyear: true },
						    tooltip: '<p>You can gain a consolidated view of your entire MasterCard payments business&mdash;from consumer and commercial credit and debit to prepaid programs&mdash;with PortfolioAnalytics. Use this Web-based tool to evaluate and understand your operational, marketing, and fraud performance, then leverage these benchmarked insights to make fact-based, strategic decisions that can help drive the overall profitability of your programs and portfolio.</p>',
						    whiteTooltip: '<p>Contact your MasterCard Account Representative for more information.</p>'
						},
						{ name: 'Quarterly Product Trends and Insights ', color: 'blue',
						    tooltip: '<p>You can make better business decisions about your consumer debit, credit, and small business offerings, including features, benefits, and pricing, with consumer insights and industry trends from MasterCard and third-party sources.</p>',
								whiteTooltip: '<p>According to the Aite Group Small Business Survey, approximately 90% of the largest US financial institutions consider winning the small business customer segment very important to the overall success of their institution.<br><a href="https://optimization.mastercard.com/portal/Download/g4b2">Read more</a>.</p>',
								months: { 
									february: true,
									may: true,
									august:{ active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4b2' },
									november: true
									}
						    /*items: [
								{ name: 'Small Business in Challenging Economy',
								    months: { february: true }
								},
								{ name: 'Small Business Credit',
								   
								    months: {
								        may: true
								    }
								},
								{ name: 'Small Business Debit',
								    months: {
								        may: true
								    }
								},
								{ name: 'Quarterly Product Trends and Insights',
								    months: {
								        august: true
								    }
								},
								{ name: 'Quarterly Product Trends and Insights',
								    months: {
								        november: true
								    }
								}
							]*/
						}
					]
				},
				{ name: 'REGULATORY AND PUBLIC POLICY',
				    types: [
						{ name: 'Public Policy Update', color: 'green',
						    tooltip: '<p>Attend these webinar briefings and learn more about U.S. legislative and regulatory changes, both pending and approved, as well as policy trends and projected future legislation, to find out how they impact your payments business. This is also an opportunity to ask questions of MasterCard public policy experts.</p>', whiteTooltip: '<p>Your MasterCard Account Representative can provide details before the next event. Watch your e-mail for the invitation. </p>',
						    months: {
						        april: true,
						        may: true,
						        july: true,
						        october: true,
						        november: true
						    }
						}
					]
				},
				{ name: 'REPUTATION MANAGEMENT',
				    types: [
						{ name: 'Financial Institutions Reputation Study Results ', color: 'tan',
						    tooltip: '<p>MasterCard sponsors qualitative research to assess public perceptions and expectations of financial institutions and the reputation of payments industry participants. Our Reputation Study will help you understand reputation risks and what is important to consumers in the current environment, as well as identify opportunities to rebuild trust.</p>', whiteTooltip: '<p>For more information, contact us at <a href="mailto:partnerships@mastercard.com">partnerships@mastercard.com</a>.</p>',
						    months: {
						        january: true,
						        february: true
						    }
						}
					]
				}
			]
		},
		{ name: 'LMT<br>&nbsp;', hrefTabName: 'credit', pdfHref: '/portal/inc/ic10_includes/print_insight_credit_10.pdf', swithButtonHref: '/portal/calendar/credit',
		    categories: [
				{ name: 'MARKET TRENDS AND INSIGHTS',
				    types: [
						{ name: 'Consumer Insights Briefs', color: 'tan',
						    tooltip: '<p>Find out how consumer attitudes and behaviors are affected by a changing economy, with highlights from the latest research sponsored by MasterCard. Recommended marketing strategies will help financial institutions leverage these insights and connect with consumers. Check back often, as topics are updated monthly. </p>',
						    items: [
								{ name: 'Capturing the Online Spend of Affluent Consumers', months: { april: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g36a'} },
								    tooltip: '<p>Affluent consumers make 67 percent ?ore purchases online than do non-affluent consumers.</p><p><a href="https://optimization.mastercard.com/portal/Download/g36a">Read more</a>.</p>'
								},
								{ name: 'Providing Value to Affluent Consumers',
								    tooltip: '<p>66% of affluent consumers say that providing the best value for money is important in their selection of a brand.</p><p><a href="https://optimization.mastercard.com/portal/Download/g3c8">Read more</a>.</p>',
								    months: { may: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g3c8'} }
								},
								{ name: 'Taking A Page From Financial Innovators&rsquo; Books',
								    tooltip: '<p>Sixty-one percent of consumers are taking active measures to control their expenditures.</p><p><a href="https://optimization.mastercard.com/portal/Download/g4ae">Read more</a>.</p>',
								    months: { august: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4ae'} }
								},
								{ name: 'U.S. Premium Product Offerings Are Not Capturing Affluent Consumers&rsquo; Attention',
								    tooltip: '<p>In June 2010, Affluent Consumers opened just two percent of new credit card accounts.</p><p><a href="https://optimization.mastercard.com/portal/Download/g4ba">Read more</a>.</p>',
								    months: { september: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4ba'} }
								},
								{ name: 'Myth Versus Reality in Consumer Reloadable Prepaid Cards',
								    tooltip: '<p>Sixty-eight percent of prepaid card users have checking accounts.</p><p><a href="https://optimization.mastercard.com/portal/Download/g4c6">Read more</a>.</p>',
								    months: { october: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4c6'} }
								},
								{ name: 'New Consumer Insights',
								    months: {
								        november: true,
								        december: true
								    }
								}
							]
						},
						{ name: 'Consumer Pulse Reports', color: 'blue',
						    tooltip: '<p>Understanding the economic mindset and behaviors of U.S. consumers can help your organization make better business decisions. MasterCard&rsquo;s Consumer Pulse Report provides an overview of consumers&rsquo; attitudes and behavior using third-party research, insight into consumer spending with SpendingPulse&trade; 24-month sales trends, and up-to-date key consumer confidence indices.</p>',
						    items: [
								{ name: 'February 2010',
								    tooltip: '<p>As American consumers focus on minimizing their own financial risk, they aren&rsquo;t just getting by with less, they&rsquo;re also doing (and helping others) more.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g31d">Read more</a>.</p>',
								    months: { february: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g31d'} }
								},
								{ name: 'March 2010',
								    tooltip: '<p>As consumer analysts begin to predict the shape of a rebound, they are looking for millennials and gen xers to lead the way.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g367">Read more</a>.</p>',
								    months: { march: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g367'} }
								},
								{ name: 'April 2010',
								    tooltip: '<p>America&rsquo;s general economic mood is increasingly stable, but without any major signals from the job market, recovery has yet to take on an accelerated momentum.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g368">Read more</a>.</p> ',
								    months: { april: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g368'} }
								},
								{ name: 'May 2010',
								    tooltip: '<p>American consumers who are opening their pocketbooks are increasingly asking companies and brands for the three c’s: customization, convenience, and control.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g3c9">Read more</a>.</p>',
								    months: { may: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g3c9'} }
								},
								{ name: 'June 2010',
								    tooltip: '<p>The freeze on expenses may finally be thawing as American consumers seek hot summer travel deals.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g3ec">Read more</a>.</p>',
								    months: { june: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g3ec'} }
								},
								{ name: 'July 2010',
								    tooltip: '<p>The gender divide is narrowing as women cautiously spearhead the economic recovery while men continue to reel as one of the biggest victims of the “mancession”</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g432">Read more</a>.</p>',
								    months: { july: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g432'} }
								},
								{ name: 'August 2010',
								    tooltip: '<p>The age of the frugal shopper shows no signs of abating, much to the dismay of retailers hoping that consumers will start opening up their wallets, even as new services emerge to make bargain shopping easier.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g4b1">Read more</a>.</p>',
								    months: { august: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4b1' } }
								},
								{ name: 'September 2010',
									tooltip: '<p>Savvy travelers looking for savings and convenience are finding a slew of new mobile and online tools that offer the benefits of a travel agent and concierge at their fingertips.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g4b9">Read more</a>.</p>',
								    months: { september: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4b9'} }
								},
								{ name: 'October 2010',
								   tooltip: '<p>With the holiday season set to begin, retailers are banking flexible payment schedules and aggressive promotions to coax even the staunchest of scrooges to come out to the stores this year.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g4c7">Read more</a>.</p>',
								    months: { october: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4c7'} }
								},
								{ name: 'November 2010',
								    months: { november: true }
								},
								{ name: 'December 2010',
								    months: { december: true }
								}
							]
						},
						{ name: 'MasterCard Advisors SpendingPulse&trade;', color: 'tan', months: { allyear: true },
						    tooltip: '<p>MasterCard Advisors SpendingPulse&trade;, a macroeconomic indicator, tracks retail sales for the U.S., segmented by category and region, as well as total sales in the U.K. Based on aggregated sales activity in the MasterCard payments network, coupled with estimates for all other payments forms including cash and check, SpendingPulse&trade; helps issuers and merchants determine the optimal timing of marketing campaigns. </p>', whiteTooltip: '<p>For more information, contact us at <a href="mailto:partnerships@mastercard.com">partnerships@mastercard.com</a>.</p>'

						},
						{ name: '2010 Quarterly Economic Update', color: 'green',
						    tooltip: '<p>Stay informed on macroeconomic market conditions, key indicators, consumer sentiment, and industry trends with quarterly webinars hosted by MasterCard. Each webinar offers the expertise of leading economists. Insights into consumer attitudes, behaviors, and spending, as well as viewpoints and recommended actions, are shared by MasterCard experts.</p>',
						    items: [
								{ name: 'March 2010',
								    tooltip: '<p>Actionable insights from the March 3, 2010, Webinar include: </p><ul><li><em>A Macroeconomic Overview of the U.S. Economy</em> by Bart van Ark, vice president and chief economist at The Conference Board.</li> <li><em>Debit’s Gain, Credit’s Loss: Not a Zero-Sum Game</em> by Prasad Iyer, vice president, Marketing Intelligence and Planning</li> <li><em>What It All Means</em> by Ted Iacobuzio, senior vice president, Marketing Intelligence and Planning</li></ul><p>For more information about these webinars and how to access presentation replays, <a href="https://optimization.mastercard.com/portal/insights/webinars">click here.</a></p>',
								    months: { march: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'June 2010',
								    tooltip: '<p>Topics discussed in the June 9, 2010 webinar included:</p> \<ul><li><em>U.S. Economic Overview</em> by Kathy Bostjancic, senior economic advisor at The Conference Board.</li> <li><em>The Affluent Consumer by</em> Kimberly Purcell, vice president, MasterCard Global Insights</li> <li><em>Overdraft Rule (Regulation E) Discussion</em> by Richard Rozbicki, vice president, MasterCard Product Management</li></ul> <p>For more information about these webinars and how to access presentation replays, <a href="/portal/insights/webinars">click here.</a></p>',
								    months: { june: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'September 2010', color: 'blue',
								    tooltip: '<p>Topics discussed at the Sept 16, 2010 webinar included: <ul><li><em>U.S. Economic Overview</em> by Bart van Ark, vice president and chief economist at The Conference Board</li><li><em>State of the U.S. Consumer</em> by Theodore Iacobuzio, vice president, MasterCard Global Insights </li><li><em>New Business Models for No-Longer-Profitable Customers</em> by Ben Isaacson, vice president, MasterCard Global Insights</li> </ul>For more infomation about these webinars and how to access presentation replays, <a href="/portal/insights/webinars">click here</a>.</p>',
								    months: { september: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'December 2010', color: 'blue',
									tooltip: '<p>Join us in December for the webinar: <a target="_blank" href="/portal/insights/webinars">click here</a> to register.</p>',
								    months: { december: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								}
							]
						},
						{ name: 'MasterCard Webinar Series', color: 'tan',
						    tooltip: '<p>In this series, MasterCard experts examine key trends in the payments industry critical to the success of community banks and credit unions in the current economic environment. Pre-register for the free MasterCard Worldwide Webinar Series, presented in partnership with Callahan & Associates and CreditUnions.com.</p>',
						    items: [
								{ name: 'Opportunity: Why credit unions should enter/re-enter the Credit Card Business',
								    tooltip: '<p>Recent macro trends related to regulation, consumer lending and the credit markets make it a favorable operating environment for credit unions. Ben Isaacson: Senior Business Leader, Global Insights, shares insights on building an effective strategy for entering (or expanding) the credit card business. </p><br><p>For more information about these webinars and how to access presentation replays, <a href="/portal/insights/webinars">click here</a>.</p>',
								    months: { june: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'The Appeal of the Affluent Customer',
								    tooltip: '<p>Kim Purcell: Business Leader, Global Insights provides a closer look at economics and revenues of affluent customers and shares our recommendations for targeting and delivering value to this high-opportunity segment.</p><br><p>For more information about these webinars and how to access presentation replays, <a href="/portal/insights/webinars">click here</a>.</p>',
								    months: { july: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'The Changing Landscape of Rewards',
								    tooltip: '<p>With increased expectations on the part of consumers for personalized, relevant, and convenient rewards, the loyalty landscape is changing dramatically to include new technologies, consumer approaches and funding mechanisms. Andrea Gilman, Senior Business Leader, Commerce Solutions will cover rapidly evolving trends including merchant-funded rewards, consumer networks, value shopping, mobile and digital technologies and provide actionable approaches for your loyalty strategy.</p><br><p>For more information about these webinars and how to access presentation replays, <a href="/portal/insights/webinars">click here</a>.</p>',
								    months: { september: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'Credit Unions and Gen &lsquo;Mix&rsquo;',
								    tooltip: '<p>Generation &lsquo;Mix&rsquo; should be a perfect match with credit unions. Gen Mix, which includes the older end of Gen Y and the younger end of Gen X, distrusts large corporations and values local, personal and authentic, attributes embodied by credit unions. Stacy Styles, Senior Business Leader, Intelligence and Planning will provide insights about Gen Mix and share our perspective on what do credit unions need to know to better reach them.</p><br><p>For more information about these webinars and how to access presentation replays, <a href="/portal/insights/webinars">click here</a>.</p>',
								    months: { september: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								}
							]
						},
						{ name: 'White Paper', color: 'blue',
						    tooltip: '<p>MasterCard offers thoughtful perspectives and its viewpoint on a range of issues facing the payments industry, including such topics as new business models for a new economy, the impact of regulatory changes, and insights on consumer payment preferences. Check back often, as topics are updated frequently.</p>',
						    items: [
								{ name: 'Consumer Coping Strategies in a Time of Uncertainty', months: { january: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g31c '} },
								    tooltip: '<p>Post-recession, consumers are paying down debt and increasing savings as they weigh their credit card options in light of the Credit CARD Act.</p><p><a href="https://optimization.mastercard.com/portal/Download/g31c ">Read more</a>.<p>'
								},
								{ name: 'Debit&rsquo;s Gain, Credit&rsquo;s Loss: Not a Zero-Sum Game', months: { march: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g31e'} },
								    tooltip: '<p>The steady growth of debit card purchase volume</strong> in the U.S. is largely attributable to debit&rsquo;s taking share from cash and checks. The decline in credit card purchase volume is primarily due to an absolute decline in consumer spending.</p><p><a href="https://optimization.mastercard.com/portal/Download/g31e">Read more</a>.</p>'
								},
								{ name: 'A New Dynamic in U.S. Credit?Cards',
								    tooltip: '<p>From 2000-2009, U.S. credit cards consistently delivered 3% return on 3% assets&mdash;the most profitable retail banking product. In 2009, they lost money.</p><p><a href="https://optimization.mastercard.com/portal/Download/g3ed">Read more</a>.<p>',
								    months: { june: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g3ed'}}
								},
								{ name: 'Maximizing Value From Affluent Consumers', months: { june: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g3ee'} },
								    tooltip: '<p>Deposit accounts gernerate 87 percent of payments revenue from affluent consumers.</p><p><a href="https://optimization.mastercard.com/portal/Download/g3ee">Read more</a>.<p>'
								},
								{ name: 'Turning Newly Unprofitable Consumers Into Profitable Ones',
									months: { november: true} }
							]
						}
					]
				},
				{ name: 'Portfolio Performance',
				    types: [
						{ name: 'MasterCard Advisors PortfolioAnalytics', months: { allyear: true },
						    tooltip: '<p>You can gain a consolidated view of your entire MasterCard payments business&mdash;from consumer and commercial credit and debit to prepaid programs&mdash;with PortfolioAnalytics. Use this Web-based tool to evaluate and understand your operational, marketing, and fraud performance, then leverage these benchmarked insights to make fact-based, strategic decisions that can help drive the overall profitability of your programs and portfolio.</p>',
						    whiteTooltip: '<p>Contact your MasterCard Account Representative for more information.</p>'
						},
						{ name: 'Quarterly Product Trends and Insights ', color: 'green',
						    tooltip: '<p>You can make better business decisions about your consumer debit, credit, and small business offerings, including features, benefits, and pricing, with consumer insights and industry trends from MasterCard and third-party sources.</p>',
						    whiteTooltip: '<p>A recessionary mindset remains entrenched in the marketplace for most consumers, while the affluent represent an opportunity to gain lower-risk, higher-spending cardholders.</p><p><a href="https://optimization.mastercard.com/portal/Download/g4b0">Read more</a>.</p>',
								months: {
									february: true,
									may: true,
									august: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4b0'},
									november: true
									}
							/*items: [
								{ name: 'Consumer Credit Review Q4 2009',
								    //tooltip: '<p>With unemployment still high, only 35% of consumers have positive feelings about their personal finances over the next six months, translating into expectations of very little growth (1%) in real consumer spending in 2010.</p>',
								    months: {
								        february: { active: true }
								    }
								},
								{ name: 'U.S. Consumer Credit Product Trends and Insights',
								    //tooltip: '<p>As consumption share in U.S. GDP rises, it has become less of an important source for recovery.</p> \
									//<p><a href="https://optimization.mastercard.com/portal/Download/g3ca">Read more</a>.</p>',
								    months: {
								        may: true
								    }
								},
								{ name: 'U.S. Consumer Credit Product Trends and Insights Q2 2010',
								    tooltip: '<p>A recessionary mindset remains entrenched in the marketplace for most consumers, while the affluent represent an opportunity to gain lower-risk, higher-spending cardholders.</p><p><a href="https://optimization.mastercard.com/portal/Download/g4b0">Read more</a>.</p>',
								        months: { august: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4b0' } }
								    
								},
								{ name: 'Quarterly Product Trends and Insights',
								    months: {
								        november: true
								    }
								}
							]*/

						}
					]
				},
				{ name: 'REGULATORY AND PUBLIC POLICY',
				    types: [
						{ name: 'Public Policy Update', color: 'blue',
						    tooltip: '<p>Attend these webinar briefings and learn more about U.S. legislative and regulatory changes, both pending and approved, as well as policy trends and projected future legislation, to find out how they impact your payments business. This is also an opportunity to ask questions of MasterCard public policy experts.</p>',
						    whiteTooltip: '<p>Your MasterCard Account Representative can provide details before the next event. Watch your e-mail for the invitation.</p>',
						    months: {
						        april: true,
						        may: true,
						        july: true,
						        october: true,
						        november: true
						    }
						}
					]
				},
				{ name: 'REPUTATION MANAGEMENT',
				    types: [
						{ name: 'Financial Institutions Reputation Study Results ', color: 'green',
						    tooltip: '<p>MasterCard sponsors qualitative research to assess public perceptions and expectations of financial institutions and the reputation of payments industry participants. Our Reputation Study will help you understand ?eputation risks and what is important to consumers in the current environment, as well as identify opportunities to rebuild trust.</p>',
						    whiteTooltip: '<p> For more information, contact us at <a href="mailto:partnerships@mastercard.com">partnerships@mastercard.com</a>.</p>',
						    months: {
						        january: true,
						        february: true
						    }
						}
					]
				}
			]
		},
		{ name: 'LMT<br>&nbsp;', hrefTabName: 'debit', pdfHref: '/portal/inc/ic10_includes/print_insight_debit_10.pdf', swithButtonHref: '/portal/calendar/debit',
		    categories: [
				{ name: 'MARKET TRENDS AND INSIGHTS',
				    types: [
						{ name: 'Consumer Insights Briefs', color: 'tan',
						    tooltip: '<p>Find out how consumer attitudes and behaviors are affected by a changing economy, with highlights from the latest research sponsored by MasterCard. Recommended marketing strategies will help financial institutions leverage these insights and connect with consumers. Check back often, as topics are updated monthly. </p>',
						    items: [
								{ name: 'The Migration from Cash to Debit', months: { january: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g31f'} },
								    tooltip: '<p>Debit is growing strongly, as 36% of consumers who use debit as their primary payment method have done so for fewer than two years.</p><p><a href="https://optimization.mastercard.com/portal/Download/g31f">Read more</a>.</p>'
								},
								{ name: 'Using Debit to Drive Deposit Growth', months: { february: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g36c'} },
								    tooltip: '<p>Debit rewards are a profitable engagement strategy, as 50% of consumers say they would switch banks if they found a debit card with rewards or incentives they liked.</p><p><a href="https://optimization.mastercard.com/portal/Download/g36c">Read more</a>.</p>'
								},
								{ name: 'Targeted Strategies to Drive Debit Growth', months: { march: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g36d'} },
								    tooltip: '<p>Prime debit cardholders increased use of debit by 17% year-over-year in Q4 2009, as research shows debit use is growing more rapidly among consumers with higher credit scores.</p><p><a href="https://optimization.mastercard.com/portal/Download/g36d">Read more</a>.</p>'
								},
								{ name: 'Capturing the Online Spend of Affluent Consumers', months: { april: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g36a'} },
								    tooltip: '<p>Affluent consumers make 67 percent more purchases online than do non-affluent consumers.</p><p><a href="https://optimization.mastercard.com/portal/Download/g36a">Read more</a>.</p>'
								},
								{ name: 'Providing Value to Affluent Consumers',
								    tooltip: '<p>66% of affluent consumers say that providing the best value for money is important in their selection of a brand.</p><p><a href=" https://optimization.mastercard.com/portal/Download/g3c8">Read more</a>.</p>',
								    months: { may: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g3c8'} }
								},
								{ name: 'Taking A Page From Financial Innovators&rsquo; Books',
								    tooltip: '<p>Sixty-one percent of consumers are taking active measures to control their expenditures.</p><p><a href="https://optimization.mastercard.com/portal/Download/g4ae">Read more</a>.</p>',
								    months: { august: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4ae'} }
								},
								{ name: 'U.S. Premium Product Offerings Are Not Capturing Affluent Consumers&rsquo; Attention',
								    tooltip: '<p>In June 2010, Affluent Consumers opened just two percent of new credit card accounts.</p><p><a href="https://optimization.mastercard.com/portal/Download/g4ba">Read more</a>.</p>',
								    months: { september: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4ba'} }
								},
								{ name: 'Myth Versus Reality in Consumer Reloadable Prepaid Cards',
								    tooltip: '<p>Sixty-eight percent of prepaid card users have checking accounts.</p><p><a href="https://optimization.mastercard.com/portal/Download/g4c6">Read more</a>.</p>',
								    months: { october: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4c6'} }
								},
								{ name: 'New Consumer Insights  ',
								    months: {
								        november: true,
								        december: true
									}
								}
							]
						},
						{ name: 'Consumer Pulse Reports', color: 'blue',
						    tooltip: '<p>Understanding the economic mindset and behaviors of U.S. consumers can help your organization make better business decisions. MasterCard&rsquo;s Consumer Pulse Report provides an overview of consumers&rsquo; attitudes and behavior using third-party research, insight into consumer spending with SpendingPulse&trade; 24-month sales trends, and up-to-date key consumer confidence indices.</p>',
						    items: [
								{ name: 'February 2010',
								    tooltip: '<p>As American consumers focus on minimizing their own financial risk, they aren&rsquo;t just getting by with less, they&rsquo;re also doing (and helping others) more.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g31d">Read more</a>.</p> \
									',
								    months: { february: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g31d'} }
								},
								{ name: 'March 2010', color: 'tan',
								    tooltip: '<p>As consumer analysts begin to predict the shape of a rebound, they are looking for millennials and gen xers to lead the way.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g367">Read more</a>.</p> \
									',
								    months: { march: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g367'} }
								},
								{ name: 'April 2010',
								    tooltip: '<p>America&rsquo;s general economic mood is increasingly stable, but without any major signals from the job market, recovery has yet to take on an accelerated momentum.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g368">Read more</a>.</p> \
									',
								    months: { april: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g368'} }
								},
								{ name: 'May 2010',
								    tooltip: '<p>American consumers who are opening their pocketbooks are increasingly asking companies and brands for the three c’s: customization, convenience, and control.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g3c9">Read more</a>.</p>',
								    months: { may: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g3c9'} }
								},
								{ name: 'June 2010',
								    tooltip: '<p>The freeze on expenses may finally be thawing as American consumers seek hot summer travel deals.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g3ec">Read more</a>.</p>',
								    months: { june: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g3ec'} }
								},
								{ name: 'July 2010',
								    tooltip: '<p>The gender divide is narrowing as women cautiously spearhead the economic recovery while men continue to reel as one of the biggest victims of the “mancession”</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g432">Read more</a>.</p>',
								    months: { july: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g432'} }
								},
								{ name: 'August 2010',
								    tooltip: '<p>The age of the frugal shopper shows no signs of abating, much to the dismay of retailers hoping that consumers will start opening up their wallets, even as new services emerge to make bargain shopping easier.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g4b1">Read more</a>.</p>',
								    months: { august: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4b1' } }
								},
								{ name: 'September 2010',
									tooltip: '<p>Savvy travelers looking for savings and convenience are finding a slew of new mobile and online tools that offer the benefits of a travel agent and concierge at their fingertips.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g4b9">Read more</a>.</p>',
								    months: { september: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4b9'} }
								},
								{ name: 'October 2010',
								   tooltip: '<p>With the holiday season set to begin, retailers are banking flexible payment schedules and aggressive promotions to coax even the staunchest of scrooges to come out to the stores this year.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g4c7">Read more</a>.</p>',
								    months: { october: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4c7'} }
								},
								{ name: 'November 2010',
								    months: { november: true }
								},
								{ name: 'December 2010',
								    months: { december: true }
								}
							]
						},
						{ name: 'MasterCard Advisors SpendingPulse&trade;', color: 'tan', months: { allyear: true },
						    tooltip: '<p>MasterCard Advisors SpendingPulse&trade;, a macroeconomic indicator, tracks retail sales for the U.S., segmented by category and region, as well as total sales in the U.K. Based on aggregated sales activity in the MasterCard payments network, coupled with estimates for all other payments forms including cash and check, SpendingPulse&trade; helps issuers and merchants determine the optimal timing of marketing campaigns. </p>', whiteTooltip: '<p>For more information, contact us at <a href="mailto:partnerships@mastercard.com">partnerships@mastercard.com</a>.</p>'

						},
						{ name: '2010 Quarterly Economic Update', color: 'green',
						    tooltip: '<p>Stay informed on macroeconomic market conditions, key indicators, consumer sentiment, and industry trends with quarterly webinars hosted by MasterCard. Each webinar offers the expertise of leading economists. Insights into consumer attitudes, behaviors, and spending, as well as viewpoints and recommended actions, are shared by MasterCard experts.</p>',
						    items: [
								{ name: 'March 2010',
								    tooltip: '<p>Actionable insights from the March 3, 2010, Webinar include: </p> \<ul><li><em>A Macroeconomic Overview of the U.S. Economy</em> by Bart van Ark, vice president and chief economist at The Conference Board.</li> <li><em>Debit’s Gain, Credit’s Loss: Not a Zero-Sum Game</em> by Prasad Iyer, vice president, Marketing Intelligence and Planning</li> <li><em>What It All Means</em> by Ted Iacobuzio, senior vice president, Marketing Intelligence and Planning</li></ul> <p>For more information about these webinars and how to access presentation replays, <a href="https://optimization.mastercard.com/portal/insights/webinars">click here.</a></p>',
								    months: { march: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'June 2010',
								    tooltip: '<p>Topics discussed in the June 9, 2010 webinar included:</p> \<ul><li><em>U.S. Economic Overview</em> by Kathy Bostjancic, senior economic advisor at The Conference Board.</li> <li><em>The Affluent Consumer by</em> Kimberly Purcell, vice president, MasterCard Global Insights</li> <li><em>Overdraft Rule (Regulation E) Discussion</em> by Richard Rozbicki, vice president, MasterCard Product Management</li></ul> <p>For more information about these webinars and how to access presentation replays, <a href="/portal/insights/webinars">click here.</a></p>',
								    months: { june: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'September 2010', color: 'blue',
								    tooltip: '<p>Topics discussed at the Sept 16, 2010 webinar included: <ul><li><em>U.S. Economic Overview</em> by Bart van Ark, vice president and chief economist at The Conference Board</li><li><em>State of the U.S. Consumer</em> by Theodore Iacobuzio, vice president, MasterCard Global Insights </li><li><em>New Business Models for No-Longer-Profitable Customers</em> by Ben Isaacson, vice president, MasterCard Global Insights</li> </ul>For more infomation about these webinars and how to access presentation replays, <a href="/portal/insights/webinars">click here</a>.</p>',
								    months: { september: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'December 2010', color: 'blue',
								    tooltip: '<p>Join us in December for the webinar: <a target="_blank" href="/portal/insights/webinars">click here</a> to register.</p>',
								    months: { december: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								}
							]
						},
						{ name: 'MasterCard Webinar Series', color: 'tan',
						    tooltip: '<p>In this series, MasterCard experts examine key trends in the payments industry critical to the success of community banks and credit unions in the current economic environment. Pre-register for the free MasterCard Worldwide Webinar Series, presented in partnership with Callahan & Associates and CreditUnions.com.</p>',
						    items: [
								{ name: 'The Appeal of the Affluent Customer',
								    tooltip: '<p>Kim Purcell: Business Leader, Global Insights provides a closer look at economics and revenues of affluent customers and shares our recommendations for targeting and delivering value to this high-opportunity segment.</p><br><p>For more information about these webinars and how to access presentation replays, <a href="/portal/insights/webinars">click here</a>.</p>',
								    months: { july: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'Optimizing Your Profitability in a Rebounding Economy',
								    tooltip: '<p>As the world emerges from the economic downturn, independent banks and credit unions have a unique opportunity to grow wallet share, deepen customer relationships and drive net account growth.  Ben Colvin, Global Practice Leader, MasterCard Advisors Retail Banking and Debit Card Practice provides current insights and a framework to help build your debit program’s profitability.</p><br><p>For more information about these webinars and how to access presentation replays, <a href="/portal/insights/webinars">click here</a>.</p>',
								    months: { july: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'The Changing Landscape of Rewards',
								    tooltip: '<p>With increased expectations on the part of consumers for personalized, relevant, and convenient rewards, the loyalty landscape is changing dramatically to include new technologies, consumer approaches and funding mechanisms. Andrea Gilman, Senior Business Leader, Commerce Solutions will cover rapidly evolving trends including merchant-funded rewards, consumer networks, value shopping, mobile and digital technologies and provide actionable approaches for your loyalty strategy.</p><br><p>For more information about these webinars and how to access presentation replays, <a href="/portal/insights/webinars">click here</a>.</p>',
								    months: { september: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'Credit Unions and Gen &lsquo;Mix&rsquo;',
								    tooltip: '<p>Generation &lsquo;Mix&rsquo; should be a perfect match with credit unions. Gen Mix, which includes the older end of Gen Y and the younger end of Gen X, distrusts large corporations and values local, personal and authentic, attributes embodied by credit unions. Stacy Styles, Senior Business Leader, Intelligence and Planning will provide insights about Gen Mix and share our perspective on what do credit unions need to know to better reach them.</p><br><p>For more information about these webinars and how to access presentation replays, <a href="/portal/insights/webinars">click here</a>.</p>',
								    months: { september: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								}
							]
						},
						{ name: 'White Paper', color: 'tan',
						    tooltip: '<p>MasterCard offers thoughtful perspectives and its viewpoint on a range of issues facing the payments industry, including such topics as new business models for a new economy, the impact of regulatory changes, and insights on consumer payment preferences. Check back often, as topics are updated frequently.</p>',
						    items: [
								{ name: 'Debit&rsquo;s Gain, Credit&rsquo;s Loss: Not a Zero-Sum Game', months: { march: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g31e'} },
								    tooltip: '<p>The steady growth of debit card purchase volume in the U.S. is largely attributable to debit&rsquo;s taking share from cash and checks. The decline in credit card purchase volume is primarily due to an absolute decline in consumer spending.</p><p><a href="https://optimization.mastercard.com/portal/Download/g31e">Read more</a>.<p>'
								},
								{ name: 'Maximizing Value From Affluent Consumers', months: { june: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g3ee'} },
								    tooltip: '<p>Deposit accounts gernerate 87 percent of payments revenue from affluent consumers.</p><p><a href="https://optimization.mastercard.com/portal/Download/g3ee">Read more</a>.<p>'
								},
								{ name: 'Strengthening the Core', months: { june: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g3e3'} },
								    tooltip: '<p>A Practical Approach to Increasing Checking Account and Debit Card Profitability in a Rebounding Economy.</p><p><a href="https://optimization.mastercard.com/portal/Download/g3e3">Read more</a>.<p>'
								},
								{ name: 'Turning Newly Unprofitable Consumers Into Profitable Ones',
									months: { november: true} }
							]
						}
					]
				},
				{ name: 'Portfolio Performance',
				    types: [
						{ name: 'MasterCard Advisors PortfolioAnalytics', months: { allyear: true },
						    tooltip: '<p>You can gain a consolidated view of your entire MasterCard payments business&mdash;from consumer and commercial credit and debit to prepaid programs&mdash;with PortfolioAnalytics. Use this Web-based tool to evaluate and understand your operational, marketing, and fraud performance, then leverage these benchmarked insights to make fact-based, strategic decisions that can help drive the overall profitability of your programs and portfolio.</p>',
						    WhiteTooltip: '<p>Contact your MasterCard Account Representative for more information.</p>'
						},
						{ name: 'Quarterly Product Trends and Insights ', color: 'green',
						    tooltip: '<p>You can make better business decisions about your consumer debit, credit, and small business offerings, including features, benefits, and pricing, with consumer insights and industry trends from MasterCard and third-party sources.</p>',
							whiteTooltip: '<p>While mass consumer spending is stagnant, affluent spending plans are more stable and spending on furniture/furnishings, luxury retail, and women&rsquo;s apparel is experiencing positive growth.</p><p><a href="https://optimization.mastercard.com/portal/Download/g4af">Read more</a>.</p>',
								months: {
									february: true,
									may: true,
									august: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4af' },
									november: true
									}
							/*items: [
								{ name: 'U.S. Debit Insights Q1 2010',
								   // tooltip: '<p>U.S. consumer spending reductions and increased savings continue.</p> \
									//<p><a href="https://optimization.mastercard.com/portal/Download/g3cb">Read more</a>.</p>',
								    months: {
								        may: true
								    }
								},
								{ name: 'U.S. Consumer Credit Product Trends and Insights Q2 2010',
								    tooltip: '<p>While mass consumer spending is stagnant, affluent spending plans are more stable and spending on furniture/furnishings, luxury retail, and women&rsquo;s apparel is experiencing positive growth.</p><p><a href="https://optimization.mastercard.com/portal/Download/g4af">Read more</a>.</p>',
								        months: { august: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4af' } }
								    
								},
								{ name: 'Quarterly Product Trends and Insights',
								    months: {
								        november: true
								    }
								}
							]*/

						}

					]
				},
				{ name: 'REGULATORY AND PUBLIC POLICY',
				    types: [
						{ name: 'Public Policy Update', color: 'tan',
						    tooltip: '<p>Attend these webinar briefings and learn more about U.S. legislative and regulatory changes, both pending and approved, as well as policy trends and projected future legislation, to find out how they impact your payments business. This is also an opportunity to ask questions of MasterCard public policy experts.</p>',
						    whiteTooltip: '<p>Your MasterCard Account Representative can provide details before the next event. Watch your e-mail for the invitation.</p>',
						    months: {
						        april: true,
						        may: true,
						        july: true,
						        october: true,
						        november: true
						    }
						}
					]
				},
				{ name: 'REPUTATION MANAGEMENT',
				    types: [
						{ name: 'Financial Institutions Reputation Study Results ', color: 'blue',
						    tooltip: '<p>MasterCard sponsors qualitative research to assess public perceptions and expectations of financial institutions and the reputation of payments industry participants. Our Reputation Study will help you understand reputation risks and what is important to consumers in the current environment, as well as identify opportunities to rebuild trust.</p>',
						    whiteTooltip: '<p>For more information, contact us at <a href="mailto:partnerships@mastercard.com">partnerships@mastercard.com</a>.</p>',
						    months: {
						        january: true,
						        february: true
						    }
						}
					]
				}
			]
		},
		{ name: 'LMT<br>&nbsp;', hrefTabName: 'prepaid', pdfHref: '/portal/inc/ic10_includes/print_insight_prepaid_10.pdf', swithButtonHref: '/portal/calendar/prepaid',
		    categories: [
				{ name: 'MARKET TRENDS AND INSIGHTS',
				    types: [
						{ name: 'Consumer Insights Briefs', color: 'tan',
						    tooltip: '<p>Find out how consumer attitudes and behaviors are affected by a changing economy, with highlights from the latest research sponsored by MasterCard. Recommended marketing strategies will help financial institutions leverage these insights and connect with consumers. Check back often, as topics are updated monthly. </p>',
						    items: [
							{ name: 'Engage Students with Reloadable Prepaid Cards',
							    tooltip: '<p>Seventy-seven percent of students are interested in a card that helps them buy what they want or need while sticking to a budget.</p>',
							    months: {
							        july: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g433' }
							    }
							},
							{ name: 'Taking A Page From Financial Innovators&rsquo; Books',
								tooltip: '<p>Sixty-one percent of consumers are taking active measures to control their expenditures.</p><p><a href="https://optimization.mastercard.com/portal/Download/g4ae">Read more</a>.</p>',
								months: { august: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4ae'} }
							},
							{ name: 'U.S. Premium Product Offerings Are Not Capturing Affluent Consumers&rsquo; Attention',
								    tooltip: '<p>In June 2010, Affluent Consumers opened just two percent of new credit card accounts.</p><p><a href="https://optimization.mastercard.com/portal/Download/g4ba">Read more</a>.</p>',
								    months: { september: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4ba'} }
							},
							{ name: 'Myth Versus Reality in Consumer Reloadable Prepaid Cards',
								    tooltip: '<p>Sixty-eight percent of prepaid card users have checking accounts.</p><p><a href="https://optimization.mastercard.com/portal/Download/g4c6">Read more</a>.</p>',
								    months: { october: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4c6'} }
							},
							{ name: 'New Consumer Insights  ',
								months: {
									november: true,
									december: true
								}
							}

							]
						},
						{ name: 'Consumer Pulse Reports', color: 'blue',
						    tooltip: '<p>Understanding the economic mindset and behaviors of U.S. consumers can help your organization make better business decisions. MasterCard&rsquo;s Consumer Pulse Report provides an overview of consumers&rsquo; attitudes and behavior using third-party research, insight into consumer spending with SpendingPulse&trade; 24-month sales trends, and up-to-date key consumer confidence indices.</p>',
						    items: [
								{ name: 'February 2010',
								    tooltip: '<p>As American consumers focus on minimizing their own financial risk, they aren&rsquo;t just getting by with less, they&rsquo;re also doing (and helping others) more.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g31d">Read more</a>.</p>',
								    months: { february: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g31d'} }
								},
								{ name: 'March 2010', color: 'tan',
								    tooltip: '<p>As consumer analysts begin to predict the shape of a rebound, they are looking for millennials and gen xers to lead the way.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g367">Read more</a>.</p> \
									',
								    months: { march: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g367'} }
								},
								{ name: 'April 2010',
								    tooltip: '<p>America&rsquo;s general economic mood is increasingly stable, but without any major signals from the job market, recovery has yet to take on an accelerated momentum.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g368">Read more</a>.</p> \
									',
								    months: { april: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g368'} }
								},
								{ name: 'May 2010',
								    tooltip: '<p>American consumers who are opening their pocketbooks are increasingly asking companies and brands for the three c’s: customization, convenience, and control.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g3c9">Read more</a>.</p>',
								    months: { may: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g3c9'} }
								},
								{ name: 'June 2010',
								    tooltip: '<p>The freeze on expenses may finally be thawing as American consumers seek hot summer travel deals.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g3ec">Read more</a>.</p>',
								    months: { june: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g3ec'} }
								},
								{ name: 'July 2010',
								    tooltip: '<p>The gender divide is narrowing as women cautiously spearhead the economic recovery while men continue to reel as one of the biggest victims of the “mancession”</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g432">Read more</a>.</p>',
								    months: { july: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g432'} }
								},
								{ name: 'August 2010',
								    tooltip: '<p>The age of the frugal shopper shows no signs of abating, much to the dismay of retailers hoping that consumers will start opening up their wallets, even as new services emerge to make bargain shopping easier.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g4b1">Read more</a>.</p>',
								    months: { august: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4b1' } }
								},
								{ name: 'September 2010',
									tooltip: '<p>Savvy travelers looking for savings and convenience are finding a slew of new mobile and online tools that offer the benefits of a travel agent and concierge at their fingertips.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g4b9">Read more</a>.</p>',
								    months: { september: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4b9'} }
								},
								{ name: 'October 2010',
								   tooltip: '<p>With the holiday season set to begin, retailers are banking flexible payment schedules and aggressive promotions to coax even the staunchest of scrooges to come out to the stores this year.</p> \
									<p><a href="https://optimization.mastercard.com/portal/Download/g4c7">Read more</a>.</p>',
								    months: { october: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4c7'} }
								},
								{ name: 'November 2010',
								    months: { november: true }
								},
								{ name: 'December 2010',
								    months: { december: true }
								}

							]
						},
						{ name: 'MasterCard Advisors SpendingPulse&trade;', color: 'tan', months: { allyear: true },
						    tooltip: '<p>MasterCard Advisors SpendingPulse&trade;, a macroeconomic indicator, tracks retail sales for the U.S., segmented by category and region, as well as total sales in the U.K. Based on aggregated sales activity in the MasterCard payments network, coupled with estimates for all other payments forms including cash and check, SpendingPulse&trade; helps issuers and merchants determine the optimal timing of marketing campaigns. </p>', whiteTooltip: '<p>For more information, contact us at <a href="mailto:partnerships@mastercard.com">partnerships@mastercard.com</a>.</p>'

						},
						{ name: '2010 Quarterly Economic Update', color: 'green',
						    tooltip: '<p>Stay informed on macroeconomic market conditions, key indicators, consumer sentiment, and industry trends with quarterly webinars hosted by MasterCard. Each webinar offers the expertise of leading economists. Insights into consumer attitudes, behaviors, and spending, as well as viewpoints and recommended actions, are shared by MasterCard experts.</p>',
						    items: [
								{ name: 'March 2010',
								    tooltip: '<p>Actionable insights from the March 3, 2010, Webinar include: </p> <ul><li><em>A Macroeconomic Overview of the U.S. Economy</em> by Bart van Ark, vice president and chief economist at The Conference Board.</li> <li><em>Debit’s Gain, Credit’s Loss: Not a Zero-Sum Game</em> by Prasad Iyer, vice president, Marketing Intelligence and Planning</li> <li><em>What It All Means</em> by Ted Iacobuzio, senior vice president, Marketing Intelligence and Planning</li></ul> <p>For more information about these webinars and how to access presentation replays, <a href="https://optimization.mastercard.com/portal/insights/webinars">click here.</a></p>',
								    months: { march: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'June 2010',
								    tooltip: '<p>Topics discussed in the June 9, 2010 webinar included:</p> \<ul><li><em>U.S. Economic Overview</em> by Kathy Bostjancic, senior economic advisor at The Conference Board.</li> <li><em>The Affluent Consumer by</em> Kimberly Purcell, vice president, MasterCard Global Insights</li> <li><em>Overdraft Rule (Regulation E) Discussion</em> by Richard Rozbicki, vice president, MasterCard Product Management</li></ul> <p>For more information about these webinars and how to access presentation replays, <a href="/portal/insights/webinars">click here.</a></p>',
								    months: { june: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'September 2010', color: 'blue',
								    tooltip: '<p>Topics discussed at the Sept 16, 2010 webinar included: <ul><li><em>U.S. Economic Overview</em> by Bart van Ark, vice president and chief economist at The Conference Board</li><li><em>State of the U.S. Consumer</em> by Theodore Iacobuzio, vice president, MasterCard Global Insights </li><li><em>New Business Models for No-Longer-Profitable Customers</em> by Ben Isaacson, vice president, MasterCard Global Insights</li> </ul>For more infomation about these webinars and how to access presentation replays, <a href="/portal/insights/webinars">click here</a>.</p>',
								    months: { september: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'December 2010', color: 'blue',
								    tooltip: '<p>Join us in December for the webinar: <a target="_blank" href="/portal/insights/webinars">click here</a> to register.</p>',
								    months: { december: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								}
							]
						},
						{ name: 'MasterCard Webinar Series', color: 'tan',
						    tooltip: '<p>In this series, MasterCard experts examine key trends in the payments industry critical to the success of community banks and credit unions in the current economic environment. Pre-register for the free MasterCard Worldwide Webinar Series, presented in partnership with Callahan & Associates and CreditUnions.com.</p>',
						    items: [
								{ name: 'The Changing Landscape of Rewards',
								    tooltip: '<p>With increased expectations on the part of consumers for personalized, relevant, and convenient rewards, the loyalty landscape is changing dramatically to include new technologies, consumer approaches and funding mechanisms. Andrea Gilman, Senior Business Leader, Commerce Solutions will cover rapidly evolving trends including merchant-funded rewards, consumer networks, value shopping, mobile and digital technologies and provide actionable approaches for your loyalty strategy.</p> <br><p>For more information about these webinars and how to access presentation replays, <a href="/portal/insights/webinars">click here</a>.</p>',
								    months: { september: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								},
								{ name: 'Credit Unions and Gen &lsquo;Mix&rsquo;',
								    tooltip: '<p>Generation &lsquo;Mix&rsquo; should be a perfect match with credit unions. Gen Mix, which includes the older end of Gen Y and the younger end of Gen X, distrusts large corporations and values local, personal and authentic, attributes embodied by credit unions. Stacy Styles, Senior Business Leader, Intelligence and Planning will provide insights about Gen Mix and share our perspective on what do credit unions need to know to better reach them.</p><br><p>For more information about these webinars and how to access presentation replays, <a href="/portal/insights/webinars">click here</a>.</p>',
								    months: { september: { active: true, icon: 'video', href: '/portal/insights/webinars'} }
								}
							]
						},
						{ name: 'White Paper', color: 'blue',
						    tooltip: '<p>MasterCard offers thoughtful perspectives and its viewpoint on a range of issues facing the payments industry, including such topics as new business models for a new economy, the impact of regulatory changes, and insights on consumer payment preferences. Check back often, as topics are updated frequently.</p>',
						    items: [
								{ name: 'Health Savings Accounts: What New Research Shows', 
									tooltip: '<p>Health Savings Accounts (HSAs) present a huge opportunity to manage soaring healthcare costs. Health plans, third-party administrators, and employers are increasingly adopting these tax-free savings accounts that individuals can use in conjunction with high deductible health plans (HDHPs).  This white paper shares insights from new MasterCard research and analysis.<br><a href="https://optimization.mastercard.com/portal/Download/g4ca">Read more</a>.</p>',
									months: { 
										october: {active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/Download/g4ca'}
									}
								
								},
								{ name: 'Turning Newly Unprofitable Customers Into Profitable Ones', months: { november: true} },
								{ name: 'The Corporate Prepaid Opportunity: Rapid Global Growth Paves the Way', months: { november: true} }
							]
						}
					]
				},
				{ name: 'Portfolio Performance',
				    types: [
						{ name: 'MasterCard Advisors PortfolioAnalytics', months: { allyear: true },
						    tooltip: '<p>You can gain a consolidated view of your entire MasterCard payments business&mdash;from consumer and commercial credit and debit to prepaid programs&mdash;with PortfolioAnalytics. Use this Web-based tool to evaluate and understand your operational, marketing, and fraud performance, then leverage these benchmarked insights to make fact-based, strategic decisions that can help drive the overall profitability of your programs and portfolio.</p>',
						    whiteTooltip: '<p>Contact your MasterCard Account Representative for more information.</p>'
						}
					]
				},
				{ name: 'REGULATORY AND PUBLIC POLICY',
				    types: [
						{ name: 'Public Policy Update', color: 'tan',
						    tooltip: '<p>Attend these webinar briefings and learn more about U.S. legislative and regulatory changes, both pending and approved, as well as policy trends and projected future legislation, to find out how they impact your payments business. This is also an opportunity to ask questions of MasterCard public policy experts.</p>',
						    whiteTooltip: '<p>Your MasterCard Account Representative can provide details before the next event. Watch your e-mail for the invitation.</p>',
						    months: {
						        april: true,
						        may: true,
						        july: true,
						        october: true,
						        november: true
						    }
						}
					]
				},
				{ name: 'REPUTATION MANAGEMENT',
				    types: [
						{ name: 'Financial Institutions Reputation Study Results ', color: 'blue',
						    tooltip: '<p>MasterCard sponsors qualitative research to assess public perceptions and expectations of financial institutions and the reputation of payments industry participants. Our Reputation Study will help you understand reputation risks and what is important to consumers in the current environment, as well as identify opportunities to rebuild trust. </p>',
						    whiteTooltip: '<p>For more information, contact us at <a href="mailto:partnerships@mastercard.com">partnerships@mastercard.com</a>.</p>',
						    months: {
						        january: true,
						        february: true
						    }
						}
					]
				}
			]
		}
	]
};
var updatedMonth = 'SEP/2010';