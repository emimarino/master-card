﻿var linkURL = 'http://optimization.mastercard.com/portal/asset/';
var data = {
	'tabs': [
		{ name: 'MasterCard<br>Insights', hrefTabName: 'insightscal', pdfHref: '/portal/inc/ic10_includes/print_insight.pdf', swithButtonHref: '/portal/page/marketing-calendar-smbiz',
			categories: [
				{ name: 'MARKET TRENDS AND INSIGHTS',
					types: [
						{ name: 'MasterCard Top of Mind', color: 'tan',
							tooltip: '<p>MasterCard provides insights for the U.S. market from the MasterCard Heart of Commerce&trade; and Payments Perspectives global blogs. Based on original and third-party research, these blogs offer insights and key findings on payments trends, consumer behavior, and emerging opportunities to help you stay up to date on industry developments and apply these insights to your business.</p>',
							months: { allyear: { active: true, href: '/portal/tagbrowser/top-of-mind'} },
							whiteTooltip: '<p>For more information, <a href="/portal/tagbrowser/top-of-mind">click here</a>.</p>'
						},
						{ name: 'MasterCard Advisors SpendingPulse&trade;', color: 'tan', months: { allyear: true },
							tooltip: '<p>MasterCard Advisors SpendingPulse&trade;, a macroeconomic indicator, tracks retail sales for the U.S., segmented by category and region, as well as total sales in the U.K. Based on aggregated sales activity in the MasterCard payments network, coupled with estimates for all other payments forms including cash and check, SpendingPulse&trade; helps issuers and merchants determine the optimal timing of marketing campaigns. </p>',
							whiteTooltip: '<p>Contact your MasterCard Account Representative for more information.</p>'
						},
						{ name: 'Insights Presentation:<br>U. S. Economic Update', color: 'green',
							tooltip: '<p>Stay informed on macroeconomic market conditions, key indicators, consumer sentiment, and industry trends with quarterly webinars hosted by MasterCard. Each webinar offers the expertise of leading economists. Insights into consumer attitudes, behaviors, and spending, as well as viewpoints and recommended actions, are shared by MasterCard experts.</p>',
							items: [
								{ name: 'March 2012',
									tooltip: '<p>On March 22, 2012, Mastercard will host a customer webinar on the state of the U.S. economy. The presentation will feature:</p><ul><li><strong>Kathy Bostjancic</strong>, Senior Economic Advisor at The Conference Board, will provide a macroeconomic overview of the U.S. economy and share her perspective on the impact of changes in key indicators.<li><strong>Mike Angus</strong>, Group Head, Payments and Emerging Markets Knowledge Center at MasterCard Advisors, will share insights on the use of cash as a payment instrument, and some of the consequences and costs of using cash, as well as opportunities to drive cash payments onto electronic platforms.<li><strong>Pete Reville</strong>, Vice President, Consumer and Market Intelligence at MasterCard, will share how U.S. consumers view mobile payments and how these consumers compare to the rest of the world.</ul><p><a href="/portal/page/webinars">Click here</a>  to register.</p>',
									months: { march: { active: true, icon: 'video', href: '/portal/page/webinars', newWindow: true} }
								},
								{ name: 'June 2012',
									months: { june: true }
								},
								{ name: 'October 2012', color: 'blue',
									months: { october: true }
								}
							]
						},
						{ name: 'White Paper', color: 'green',
							tooltip: '<p>MasterCard offers thoughtful perspectives and its viewpoint on a range of issues facing the payments industry, including such topics as new business models for a new economy, the impact of regulatory changes, and insights on consumer payment preferences. Check back often, as topics are updated frequently.</p>',
							items: [
								{ name: 'Remaining Relevant: Seven Consumer Trends',
									months: { january: { active: true, icon: 'pdf', href: 'https://optimization.mastercard.com/portal/asset/426a/Remaining-Relevant-Seven-Consumer-Trends-to-Watch-and-Act-On'} },
									tooltip: '<p>Technology is changing everything. Stay ahead of competitors with decisions based on emerging consumer trends.</p><p><a href="https://optimization.mastercard.com/portal/asset/426a/Remaining-Relevant-Seven-Consumer-Trends-to-Watch-and-Act-On">Read more</a>.</p>'
								},
								{ name: 'Creating a Debit Card Payment Habit',
									months: { january: { active: true, icon: 'pdf', href: linkURL + '42a9'} },
									tooltip: '<p>A new study from MasterCard Advisors outlines a five-step framework to migrate consumers from cash to adopting debit as their primary payment tool.</p><p><a href="' + linkURL + '42a9">Read more</a>.</p>'
								},
								{ name: 'Consumer Spending Outlook',
									months: { january: { active: true, icon: 'pdf', href: linkURL + '42af'} },
									tooltip: '<p>With the key drivers of household consumption&mdash;employment, wage, and credit growth&mdash;not working, it’s important to understand what will spur the economy.</p><p><a href="' + linkURL + '42af">Read more</a>.</p>'
								},
								{ name: 'Mobile Device Revolution',
									months: { january: { active: true, icon: 'pdf', href: linkURL + '42b0'} },
									tooltip: '<p>The shift from plastic cards to mobile devices&mdash;the greatest change in form factors in 30 years&mdash;has sweeping implications for the payments industry.</p><p><a href="' + linkURL + '42b0">Read more</a>.</p>'
								},
								{ name: 'Prepaid Rebate and Incentive Cards',
									months: { april: true }
								},
								{ name: 'Mexico/U.S. Cross-border eCommerce',
									months: { may: true }
								},
								{ name: 'Global Mobile Readiness',
									months: { may: true }
								},
								{ name: 'Shifting Payment Dynamics',
									months: { june: true }
								}
							]
						},
						{ name: 'Comparative Cardholders Dynamics Perspectives ', color: 'tan',
							tooltip: '<p>The Comparative Cardholders Dynamics Perspectives from MasterCard Advisors provides a window into the attitudes, behaviors, and spending patterns of consumers based on a series of studies conducted annually with more than 50,000 U.S. consumers and small business owners. These studies can help you benchmark your performance against peers, uncover the drivers for top-of-wallet and loyalty, and optimize marketing efforts.</p>',
							items: [
								{ name: 'Debit Top-line Report',
									months: { april: { active: true, icon: 'pdf', href: linkURL + '44aa'} },
									tooltip: '<p>The primary intent of this study is to provide deeper insights into consumer behavior and preferences on debit cards, as well as to support the decision making process for cardholder acquisition, marketing and targeting messages at debit cardholders.</p><p><a href="' + linkURL + '44aa">Read more</a>.</p>'
								},
								{ name: 'Credit Top-line Report',
									months: { april: { active: true, icon: 'pdf', href: linkURL + '44ab'} },
									tooltip: '<p>This study provides insight into the credit card marketplace and allows financial institutions that issue credit cards to benchmark the responses of their cardholders to those of other issuers.</p><p><a href="' + linkURL + '44ab">Read more</a>.</p>'
								},
								{ name: 'Small Business',
									months: { september: true }
								},
								{ name: 'eCommerce Study',
									months: { october: true }
								},
								{ name: 'Rewards',
									months: { november: true }
								},
								{ name: 'Satisfaction and Loyalty Study',
									months: { december: true }
								}
							]
						}
					]
				},
				{ name: 'Portfolio Performance',
					types: [
						{ name: 'MasterCard Advisors PortfolioAnalytics', months: { allyear: true },
							tooltip: '<p>You can gain a consolidated view of your entire MasterCard payments business&mdash;from consumer and commercial credit and debit to prepaid programs&mdash;with PortfolioAnalytics. Use this Web-based tool to evaluate and understand your operational, marketing, and fraud performance, then leverage these benchmarked insights to make fact-based, strategic decisions that can help drive the overall profitability of your programs and portfolio.</p>',
							whiteTooltip: '<p>For more information, <a href="/portal/program/234/PortfolioAnalytics">click here</a>.</p>'
						},
						{ name: 'Quarterly Trends and Insights', color: 'blue',
							tooltip: '<p>You can make better business decisions about your consumer debit, credit, and small business offerings, including features, benefits, and pricing, with consumer insights and industry trends from MasterCard and third-party sources.</p>',
							items: [
								{ name: 'Consumer',
									tooltip: '<p>As unemployment falls and consumer spending rises, issuers can boost  consumer confidence with education and tools that support their pragmatic needs.</p><p><a href="' + linkURL + '447b/State-of-the-US-Consumer">Read more</a>.</p>',
									months: {
										february: { active: true, icon: 'pdf', href: linkURL + '447b/State-of-the-US-Consumer' },
										may: true,
										august: true,
										november: true
									}
								}

							]
						}
					]
				}
			]
		}

	]
};
var updatedMonth = 'Updated: MAR/2012';