var hasConsole = (typeof console == "undefined" || typeof console.log == "undefined") ? false : true;
(function($) {
	var self = null;

	$.fn.autoComplete = function(o) {
		o = jQuery.extend({
			valueSep: ';', 				// separator for different values
			minChars: 1, 				// minimum chars typed by user before suggestion list is shown
			meth: "get", 				// AJAX method (get/post)
			varName: "input", 			// variable name in the query string of the url
			className: "autocomplete", 	// classname of the list
			timeout: 2000, 				// idle time before hiding the list, set to 0 to disable the timeout
			delay: 0, 				    // delay before showing the list
			offsetY: 0, 				// y offset of the list
			showNoResults: false, 		// show a message when no results were found
			noResults: "No results were found.", // the message when no results were found
			showMoreResults: false, 	// show a message when more results were found than the maximum
			moreResults: "More results were found", // the message when more results were found than the maximum
			cache: false, 				// cache the received data
			maxEntries: 100, 			// maximum number of suggestions cached
			maxResults: 25, 			// maximum number of suggestions shown in the list
			onAjaxError: null, 			// function to execute when an AJAX error occurred
			maxHeight: 0, 				// maximum height of the list, set to 0 to disable maximum height
			setWidth: true, 			// set to true to define a max/min width
			minWidth: 215, 				// the minimum width of the list when setWidth is true
			maxWidth: 215, 				// the maximum width of the list when setWidth is true
			useNotifier: true, 			// use an icon notifier (spinner) between AJAX calls?
			showAnimProperties: { height: '100%' }, // show animation options, see http://api.jquery.com/animate/ 
			// note: width and height settings will be animated when set, but will be overruled with calculated settings
			showAnimSpeed: 'fast', 		// the duration of the show animation, see http://api.jquery.com/animate/ 
			hideAnimProperties: { opacity: '0' }, // hide animation options, see http://api.jquery.com/animate/
			hideAnimSpeed: 'fast',		// hide animation duration, see http://api.jquery.com/animate/
			showFooter: false
		}, o);
		return this.each(function() {
			new $.autoComplete(this, o);
		});
	};

	$.autoComplete = function(e, o) {
		this.field = $(e);
		this.options = jQuery.extend(o, this.options);
		this.KEY_UP = 38;
		this.KEY_DOWN = 40;
		this.KEY_ESC = 27;
		this.KEY_RETURN = 13;
		this.KEY_TAB = 9;
		this.KEY_HOME = 36;
		this.KEY_END = 35;
		this.KEY_PAGEUP = 33;
		this.KEY_PAGEDOWN = 34;

		this.init();
	};

	$.autoComplete.prototype = {
		init: function() {
			var self = this;

			this.sInp = ""; // input value 
			this.nInpC = 0; // input value length
			this.aSug = []; // suggestions array
			this.aSugRev = []; // suggestions array shifted for categories
			this.iHigh = 0; // level of list selection
			this.wasVisible = true;
			this.lastKey = 0;
			this.lastEvent = '';

			if (this.options.useNotifier) this.field.addClass('ac_field');
			this.field.keydown(function(e) { return self.handleKeyEvent(e); });
			this.field.keypress(function(e) { return self.handleKeyEvent(e); });

			this.field.keyup(function(e) {
				if (!e) e = window.event;
				var key = e.keyCode || e.which;

				self.lastKey = 0;
				self.lastEvent = '';

				if (key != self.KEY_RETURN && key != self.KEY_TAB && key != self.KEY_ESC && key != self.KEY_UP && key != self.KEY_DOWN && key != self.KEY_PAGEDOWN && key != self.KEY_PAGEUP) {
					self.getSuggestions(self.field.val());
				}

				return true;

			});

			this.field.blur(function() { self.resetTimeout(); return true; });
			this.field.attr('Autocomplete', 'off');
		},
		handleKeyEvent: function(e) {
			if (!e) e = window.event;
			var key = e.keyCode || e.which;
			var returnVal = true;
			var self = this;
			var changeCount = 1;

			if ($('#' + self.acID).length) {
				if (!e.altKey && !e.ctrlKey && !e.shiftKey) {
					switch (key) {
						case self.KEY_RETURN:
							self.setHighlightedValue();

							if (this.field.attr('url')) {
								location.href = this.field.attr('url');
								e.stopPropagation();
								returnVal = false;
							} else {
								returnVal = true;
							}
							break;
						case self.KEY_TAB:
							self.setHighlightedValue();
							returnVal = false;
							break;
						case self.KEY_ESC:
							self.clearSuggestions();
							e.stopPropagation();
							returnVal = false;
							break;
						case self.KEY_HOME:
						case self.KEY_END:
							self.changeHighlight(key, $('#ac_ul').children().length);
							e.stopPropagation();
							returnVal = false;
							break;
						case self.KEY_PAGEUP:
						case self.KEY_PAGEDOWN:
							if (this.options.maxHeight > 0) {
								changeCount = ($("#ac_ul").height() - ($(".ac_highlight").outerHeight() / 2)) / $(".ac_highlight").outerHeight();
								changeCount = Math.floor(changeCount - 0.5);
							} else {
								changeCount = 10;
							}
						case self.KEY_UP:
						case self.KEY_DOWN:
							if (e.type == 'keydown' || (self.lastEvent != 'keydown' && self.lastKey == key) || self.lastKey != key) {
								if ($('#' + self.acID).length) {
									self.changeHighlight(key, changeCount);
								}
							}
							returnVal = false;
							break;
					}
				}
			}
			if (key == self.KEY_RETURN) {
				$.ajax({
					type: 'POST',
					async: true,
					url: '/portal/header/curatesearchtext',
					data: { searchText: $('#search-field').val() },
					success: function (data) {
						if (data.curatedText.length > 0 && data.curatedText != 'Search')
							location.href = '/portal/search/' + data.curatedText;
					}
				});
				e.stopPropagation();
				returnVal = false;
			}

			self.lastKey = key;
			self.lastEvent = e.type;

			return returnVal;
		},
		getSuggestions: function(val) {
			var lastInput = this.getLastInput(val);
			var self = this;

			if (val == this.sInp) return false;

			this.wasVisible = $('#' + this.acID).length;

			$('#' + this.acID).remove();

			this.sInp = val;

			if (lastInput.length < this.options.minChars) {
				this.aSug = [];
				this.nInpC = val.length;
				return false;
			}
			var ol = this.nInpC;
			this.nInpC = val.length ? val.length : 0;
			var l = this.aSug.length;
			if (this.options.cache && (this.nInpC > ol) && l && (l < this.options.maxEntries)) {
				var arr = new Array();
				var oldval = '';
				$(this.aSug).each(function(i) {
					if (oldval == this.value) return true;
					oldval = this.value;
					if (this.value.toLowerCase().indexOf(lastInput.toLowerCase()) != -1)
						arr.push(this);
				});
				this.aSug = arr;
				this.createList(this.aSug);
			}
			else {
				clearTimeout(this.ajID);
				this.ajID = setTimeout(function() { self.doAjaxRequest(self.sInp); }, this.options.delay);
			}
			document.helper = this;
			return false;
		},
		getLastInput: function(str) {
			var ret = str;
			if (undefined != this.options.valueSep) {
				var idx = ret.lastIndexOf(this.options.valueSep);
				ret = idx == -1 ? ret : ret.substring(idx + 1, ret.length);
			}
			return ret;
		},
		doAjaxRequest: function(input) {
			if ($.trim(input) == '') return false;

			var self = this;

			if (input != this.field.val())
				return false;

			this.sInp = this.getLastInput(this.sInp);
			if ($.trim(this.sInp) == '') return false;

			if (typeof this.options.script == 'undefined')
				throw ('You have to specify a server script to make ajax calls!');
			else if (typeof this.options.script == 'function')
				var url = this.options.script(encodeURIComponent(this.sInp));
			else
				var url = this.options.script + this.options.varName + '=' + encodeURIComponent(this.sInp);

			if (!url) return false;

			if (this.options.useNotifier) this.field.removeClass('ac_field').addClass('ac_field_busy');
			var options = {
				url: url,
				type: self.options.meth,
				success: function(req) {
					if (self.options.useNotifier) {
						self.field.removeClass('ac_field_busy').addClass('ac_field');
					}
					self.setSuggestions(req, input);
				},
				error: (typeof self.options.onAjaxError == 'function') ? function(status) {
					if (self.options.useNotifier) {
						self.field.removeClass('ac_field_busy').addClass('ac_field');
					}
					self.options.onAjaxError(status);
				} : function(status) {
					if (self.options.useNotifier) {
						self.field.removeClass('ac_field_busy').addClass('ac_field');
					}
					if (hasConsole) console.log('AJAX error: ', status);
				}
			};
			$.ajax(options);
		},
		setSuggestions: function(req, input) {
			if (input != this.field.val())
				return false;

			if (this.options.json) {

				this.iHigh = 0;
				this.aSug = [];
				this.aSugRev = [];

				this.aSug = req;
			}
			this.acID = 'ac_' + this.field.attr('id');
			this.createList(this.aSug);
		},
		createList: function(arr) {
			var self = this;

			var animSpeed = this.wasVisible ? 0 : this.options.showAnimSpeed;
			$('#' + this.acID).remove();

			this.killTimeout();

			if (arr.length == 0 && !this.options.showNoResults) return false;

			var div = $('<div></div>').addClass(this.options.className).attr('id', this.acID);

			var ul = $('<ul></ul>').attr('id', 'ac_ul');

			if (arr.length == 0 && this.options.showNoResults) {
				var li = $('<li></li>').addClass('ac_warning').html(this.options.noResults);
				ul.append(li);

				var lastInput = this.getLastInput(this.sInp);
			} else {
				var maxRowAll = 15;
				var rowAll = 0;

				var lastInput = this.getLastInput(this.sInp);

				this.aSugRev.push({ info: '' });

				var catHeader = '<h4>All</h4>';
				var liCategory = $('<li></li>').addClass('isCategory').addClass('categoryFirst').html(catHeader);
				ul.append(liCategory);

				for (var i = 0; i < arr.length; i++) {

					var tagUrl = arr[i].info;
					var val = arr[i].value;
					var st = val.toLowerCase().indexOf(lastInput.toLowerCase());
					var output = val.substring(0, st) + '<b>' + val.substring(st, st + lastInput.length) + '</b>' + val.substring(st + lastInput.length);
					//var a = $('<a></a>').attr('href', '/portal/search/' + escape(tagUrl)).attr('rel', i).html(output);
					var a = $('<a></a>').attr('href', tagUrl).attr('rel', i).html(output);
					var li = $('<li></li>').html(a);

					rowAll = rowAll + 1;

					var isSynonym = false;

					if (val.indexOf('(') != -1 && val.lastIndexOf(')') == val.length - 1) {
						var synonymTag = val.substring(0, val.indexOf('('));
						var synonym = val.substring(val.indexOf('(') + 1, val.lastIndexOf(')'));
						if (synonymTag.toLowerCase().indexOf(lastInput.toLowerCase()) != -1 && synonym.toLowerCase().indexOf(lastInput.toLowerCase()) == -1)
							isSynonym = true;
					}

					if (!isSynonym && (rowAll <= maxRowAll)) {
						ul.append(li);
						this.aSugRev.push(arr[i]);
					}
				}
				if (arr.length > self.options.maxResults && this.options.showMoreResults) {
					var li = $('<li></li>').addClass('ac_message').html(this.options.moreResults);
					ul.append(li);
				}
			}

			if (this.options.showFooter) {
				var footerText = 'Search for "' + lastInput + '" >'
				var aFooter = $('<a></a>').attr('href', '/portal/search/' + escape(lastInput)).html(footerText);
				var liFooter = $('<li></li>').addClass('last').html(aFooter);
				ul.append(liFooter);    
			}

			div.append(ul);
			ul.scroll(function() { $("#ac_ul").addClass("ac_tmpClass").removeClass("ac_tmpClass"); });

			var pos = this.field.offset();

			var w = (this.options.setWidth && this.field.outerWidth() < this.options.minWidth) ? this.options.minWidth : (this.options.setWidth && this.field.outerWidth() > this.options.maxWidth) ? this.options.maxWidth : this.field.outerWidth();

			div.css('left', pos.left)
		       .css('top', pos.top + this.field.outerHeight() + this.options.offsetY)
		       .width(w)
		       .css('min-height', this.options.maxHeight > 0 ? '20px' : '')
		       .css('max-height', this.options.maxHeight > 0 ? this.options.maxHeight + 'px' : '')
		       .mouseover(function() { self.killTimeout(); })
		       .mouseout(function() { self.resetTimeout(); });

			$(document.body).append(div);

			if (this.options.maxHeight > 0) {
				if ($.browser.msie) {
					var tmpDiv = $('<div style="width:100px; height:100px; overflow:hidden; position:absolute; top:-1000px; left:-1000px;"></div>');
					var tmpInnerDiv = $('<div style="height:200px;">');
					$(document.body).append(tmpDiv.append(tmpInnerDiv));
					var width1 = tmpInnerDiv.innerWidth();
					tmpDiv.css('overflow-y', 'scroll');
					var width2 = tmpInnerDiv.innerWidth();
					tmpDiv.remove();
					var scrollbarWidth = width1 - width2;

					if ($.browser.version < 7) {
						ul.height("100%");
						if (div.outerHeight() > this.options.maxHeight - ((header.outerHeight(true) + footer.outerHeight(true)) * 2)) {
							div.css('height', this.options.maxHeight - ((header.outerHeight(true) + footer.outerHeight(true)) * 2) + 'px');
							ul.css({ 'padding-right': scrollbarWidth, 'overflow-x': 'hidden' });
							ul.width(ul.width() - scrollbarWidth);
						}
					} else if ($.browser.version < 8) {
						ul.height(div.height() - (header.outerHeight(true) + footer.outerHeight(true)));
						if (ul[0].scrollHeight > ul[0].offsetHeight) {
							ul.css({ 'padding-right': scrollbarWidth + 'px', 'overflow-x': 'hidden' });
						}
					} else {
						ul.height(div.height() - (header.outerHeight(true) + footer.outerHeight(true)));
					}
				} else {
					ul.height(div.height() - (header.outerHeight(true) + footer.outerHeight(true)));

					if (div.height() < this.options.maxHeight) {
						ul.css('overflow', 'hidden');
						ul.scrollTop(0);
					}
				}
			} else {
				ul.css('overflow', 'hidden');
			}

			var divHeight = div.height();
			var divDisplay = div.css('display');
			if (this.options.showAnimProperties.height) {
				this.options.showAnimProperties.height = divHeight + 'px';
			}
			if (this.options.showAnimProperties.width) {
				this.options.showAnimProperties.width = w + 'px';
			}
			div.hide()
		       .height(this.options.showAnimProperties.height ? 0 : divHeight)
		       .width(this.options.showAnimProperties.width ? 0 : w)
		       .css('opacity', this.options.showAnimProperties.opacity ? 0 : div.css('opacity'))
		       .css('display', divDisplay)
		       .animate(this.options.showAnimProperties, animSpeed, 'linear');
		},
		changeHighlight: function(key, count) {
			var list = $('#ac_ul');

			if (!list.length) return false;
			var n = (key == this.KEY_DOWN || key == this.KEY_PAGEDOWN || key == this.KEY_END) ? this.iHigh + count : this.iHigh - count;
			n = this.ensureIndexIsWithinBounds(n, list);
			if ($(list.children().eq(n)).hasClass('isCategory')) n = (key == this.KEY_DOWN || key == this.KEY_PAGEDOWN || key == this.KEY_END) ? n + count : n - count;
			n = this.ensureIndexIsWithinBounds(n, list);
			this.setHighlight(n);
		},
		ensureIndexIsWithinBounds: function(n, list) {
			n = (n >= list.children().length) ? list.children().length - 1 : ((n < 0) ? 0 : n);
			return n;
		},
		setHighlight: function(n) {
			var list = $('#ac_ul');
			if (!list.length) return false;
			this.iHigh = Number(n);
			list.children().removeClass('ac_highlight').eq(this.iHigh).addClass('ac_highlight');
			this.killTimeout();
		},
		clearHighlight: function() {
			var list = $('#ac_ul');
			if (!list.length) return false;
			list.children().removeClass('ac_highlight');
			this.iHigh = 0;
		},
		setHighlightedValue: function() {
			if (this.iHigh >= 0) {
				if (!this.aSugRev[this.iHigh]) return;
				if (null != this.options.valueSep) {
					var str = this.getLastInput(this.field.val());
					var idx = this.field.val().lastIndexOf(str);
					str = this.aSugRev[this.iHigh].value;
					if (str) this.sInp = idx == -1 ? str : this.field.val().substring(0, idx) + str;
					this.field.val(this.sInp);
					this.field.attr('url', this.aSugRev[this.iHigh].info);
				} else {
					var str = this.getLastInput(this.field.val());
					var idx = this.field.val().lastIndexOf(str);
					str = this.aSugRev[this.iHigh].value;
					this.sInp = idx == -1 ? str : this.field.val().substring(0, idx) + str;
					this.field.val(this.sInp);
					this.field.attr('url', this.aSugRev[this.iHigh].info);
				}
				this.field.focus();
				if (this.field.selectionStart) {
					this.field.setSelectionRange(this.sInp.length, this.sInp.length);
				}
				this.clearSuggestions();
				if (typeof this.options.callback == 'function')
					this.options.callback(this.aSugRev[this.iHigh]);
			}

			this.aSug = [];
			this.aSugRev = [];
		},
		killTimeout: function() {
			clearTimeout(this.toID);
		},
		resetTimeout: function() {
			this.killTimeout();
			var self = this;
			if (self.options.timeout > 0) {
				this.toID = setTimeout(function() { self.clearSuggestions(); }, self.options.timeout);
			}
		},
		clearSuggestions: function() {
			this.killTimeout();
			$('#' + this.acID).animate(this.options.hideAnimProperties, this.options.hideAnimSpeed, 'linear', function() { $(this).remove(); });
		}

	}
})(jQuery);
