// Add a new file name and title for each new video.
// Array ID is for quick refrence.
// [ID, File Name, Title, Height, Width]
var videoList = [
	[0, 'Mastercard_Series_1.mp4', 'Opportunities to Enter the Credit Card Space'], //webinar
	[1, 'Mastercard_Series_2.mp4', 'The State of the U.S. Consumer'], //webinar
	[2, 'MemberRelationships_Final.mp4', 'Grow From Within'], //webinar
	[3, 'MCMP.mp4', 'Mastercard&reg; MarketPlace&trade;', '720', '428'],  //demo
	[4, 'Full_Event_2.mp4', 'Gen Mix Webinar'], //webinar
	[5, 'Full_Event_Rewards.mp4', 'The Changing Landscape of Rewards'], //webinar
	[6, 'CustomersPlusCarolyn.mp4', ''] //webinar
]
//end of list
var videoHeight = '720';
var videoWidth = '504';
var videoId;
var videoQuery = window.location.search.substr(1).split('&');
for (var i in videoQuery) {
	if (videoQuery[i].split('=')[0] == 'id') {
		videoId = videoQuery[i].split('=')[1];
	}
}

if (!videoList[videoId][3] == '') {
	videoHeight = videoList[videoId][3];
}
if (!videoList[videoId][4] == '') {
	videoWidth = videoList[videoId][4];
}
if (videoId < videoList.length) {
	var so = new SWFObject('/portal/inc/scripts/videoplayer/player-licensed.swf', 'mpl', videoHeight, videoWidth, '9');
	so.addParam('allowfullscreen', 'true');
	so.addParam('allowscriptaccess', 'always');
	so.addParam('wmode', 'opaque');
	so.addVariable('file', '/portal/replays/videos/' + videoList[videoId][1]);
	document.getElementById('video_title').innerHTML = '<h2>' + videoList[videoId][2] + '</h2>';
	so.write('Video');

} else {
	document.write('No video selected')
}
