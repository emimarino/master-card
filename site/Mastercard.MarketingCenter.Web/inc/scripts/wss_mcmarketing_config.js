// *********************************
// *** Mastercard Optimization Solutions Config
// *** Last Updated 03-27-09
// *********************************

// set account info
var wsstag_prodAcct = "DM581203DAND;DM541028L2ZA";
var wsstag_testAcct = "DM54051699BZ;DM54051673CC";

// js to retrieve and Page filename from url
var wsstag_filename = "Indeterminate";

//var wsstag_path = document.location.pathname;
var wsstag_re = new RegExp ('.*marketingcenter\.mastercard\.com/(.*)/(.*)') ;
var wsstag_matches = document.location.href.match(wsstag_re); 

// need to confirm we have a match
if (wsstag_matches) {
   if (wsstag_matches[2] == "") {
      wsstag_matches[2] = "default.aspx";
   }
   wsstag_filename = wsstag_matches[2];
   // Setup Category Information
	if(typeof(wsstag_category) == "undefined"){
		var wsstag_category = wsstag_matches[1];	
	}
}
else{
	var wsstag_re3 = new RegExp ('.*/(.*)') ;
	var wsstag_matches3 = document.location.href.match(wsstag_re3); 
	
	// need to confirm we have a match
	if (wsstag_matches3) {
	      if (wsstag_matches3[1] == "") {
    		  wsstag_matches3[1] = "default.aspx";
		  }
		wsstag_filename = wsstag_matches3[1];
	}
	else {
		wsstag_filename = "default.aspx";
	}
}

//Check for a predefined page ID
if (typeof(wsstag_pageId)!="undefined") {
	wsstag_filename = wsstag_pageId;
}

//Check to see if the category is still undefined
if(typeof(wsstag_category) == "undefined"){
	var wsstag_category = "home/";	
}

//Check to see if a querytsring exists
var wsstag_querystring = document.location.search;
if(wsstag_querystring.indexOf('GP=') != -1 || wsstag_querystring.indexOf('CMP=') != -1){
	var wsstag_re2 = new RegExp ('.*(GP|CMP)=([^&]*)');
	var wsstag_qsMatch = wsstag_querystring.match(wsstag_re2);
	if (wsstag_qsMatch){
		if(wsstag_filename == "default.aspx"){
	        	wsstag_filename = "default.aspx" & wsstag_qsMatch[0];
		}
		if((document.cookie.indexOf(wsstag_qsMatch[2]) == -1) && (wsstag_qsMatch[1].indexOf("KNC-") == -1)){
			var wsstag_campId = wsstag_qsMatch[2];
			document.cookie = "hbx.dcmp: " + wsstag_campId;
		}
		else{
			var wsstag_campId = "";
		}		
	}
}
else{
	var wsstag_campId = "";
}

// **************************************
// *** DO NOT MAKE CHANGES BELOW HERE *** 
// **************************************
function _wsstag_clean(input) {
   // replace any html / ascii codes (&xx;)(&#xxx) with spaces
   var re = new RegExp('\&#?\\w+;?', 'g');
   var input = input.replace(re, ' ') ;

   // ensure characters in ascii decimal values of 32 - 126
   // include 32 for spaces
   // maps to hex values of x20 - x7E
   var re = new RegExp ('[^\\x20-\\x7E]', 'g') ;
   var input = input.replace(re, '') ;

   // remove illegal characters
   // ' " & ! # $ % ^ * : | / \ < > ~ ;
   // replace '|' with ' ' 
   var re = new RegExp ('[|]', 'g');
   var input = input.replace(re, '--');
   // replace others with ''
   var re = new RegExp ('[\'\"\&!#\$%^\*:\/\\<>~;]', 'g') ;
   var input = input.replace(re, '') ;

   // replace spaces with the pluses
   // replace spaces in succession with one plus
   var re = new RegExp (' +', 'g') ;
   var input = input.replace(re, '+') ;
   
   return input;
}

var _hbEC=0,_hbE=new Array;
function _hbEvent(a,b){
   b=_hbE[_hbEC++]=new Object();
   b._N=a;b._C=0;return b;
}
var hbx=_hbEvent("pv");

hbx.vpc="HBX0100u";
hbx.gn="wa.mastercard.com";

// set environment specific data
if (wsstag_envResult == wsstag_prodEnv) {
   // prod data
   hbx.acct=wsstag_prodAcct;
} else {
   // test data
   hbx.acct=wsstag_testAcct;   
}

hbx.pn = wsstag_filename;
hbx.mlc = wsstag_category + ";/Mastercard Moments AU/" + wsstag_category;

function _wsstag_swfClick(swflid,swflpos){
	_hbLink(swflid,swflpos);
}

function _wsstag_exitLink(swfexit){
	_hbExitLink(swfexit);
}

function _wsstag_customMetric(cmlid, cmlpos){
	_hbSet('lv.id',cmlid);
	_hbSet('lv.pos',cmlpos);	
	_hbSend();
}

// Listener function for media tracking
//Checks for any streaming media and calls the tags accordingly
var indexWM = document.body.innerHTML.toLowerCase().indexOf('playerwm');
if(indexWM != -1){
	var wsstag_videoType = 'playerwm';
}
else{
	var indexRP = document.body.innerHTML.toLowerCase().indexOf('playerrmp');
	if(indexRP != -1){
		var wsstag_videoType = 'playerrmp';
	}
	else{
		var indexQT = document.body.innerHTML.toLowerCase().indexOf('playerqt');
		if(indexQT != -1){
			var wsstag_videoType = 'playerqt';	
		}
	}
}

if(typeof(wsstag_videoType)!="undefined"){ 
  hbx.media = wsstag_videoType;
  var wsstag_jsMediaURL = "/js/hbxmedia.js";
  document.write("<scr"+"ipt src=\""+wsstag_jsMediaURL+"\"></scr"+"ipt>");
} 

function dynamic_fsCommand(lidlpos, newURL){
//	alert("lidlpos: " + lidlpos)
//	alert("newURL: " + newURL);
	if(lidlpos.indexOf("|") != -1){
		var linkArray = lidlpos.split("|");
		var swflid = linkArray[0];
		var swflpos = linkArray[1];
	}
	else{
		var swflid = lidlpos;
		var swflpos = "Indeterminate";
	}
	_hbLink(swflpos, swflid);
	//Launch the desired URL, if one exists
	if(newURL != null && newURL != "undefined" && newURL != ""){
		if(newURL.indexOf("|") != -1){
			var urlArray = newURL.split("|");
			newURL = urlArray[0];
			var urlMethod = urlArray[1];
		}
		else{
			var urlMethod = "_self";	
		}
		window.open(newURL, urlMethod);
	}
}

function dynamic_pvCommand(mlcpn){
//	alert("mlcpn: " + mlcpn)
	if(pnmlc.indexOf("|") != -1){
		var mlcpnArray = mlcpn.split("|");
		var swfmlc = mlcpnArray[0];
		var swfpn = mlcpnArray[1];
	}
	else{
		var swfpn = mlcpn;
		var swfmlc = "Default";
	}
	_hbPageView(swfpn, swfmlc);
}


// the defaults across the application
hbx.pndef = "title";
hbx.ctdef = "full";
hbx.lt = "auto";  // link tracking setting
hbx.cmp = ""; // placeholder 
hbx.cmpn = "ppass_cid"; // campaign id - in query
hbx.dcmp = wsstag_campId;
hbx.hra = ""; // placeholder 
hbx.hqsp = "ppass_att"; // campaign response attribute - query
hbx.gp = "";  // placeholder
hbx.gpn = "ppass_gp";  // campagin goal - in query
hbx.hcn = ""; // conversion attribute
hbx.hcv = ""; // conversion value
hbx.lvm = "300"; // link views max chars
hbx.dft = "full"; //download link tracking
hbx.dlf = ".sit,.zip,.doc,.xls,.ppt,.pdf,.eps,.psd"; //download file types