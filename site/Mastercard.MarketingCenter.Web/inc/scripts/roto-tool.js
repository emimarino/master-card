
jQuery(function ($) {
    $('#slideshow').cycle({
        fx: 'fade',
        pager: '#roto_nav',
        timeout: 10000,
        speed: 1000
    });
});



jQuery(document).ready(function () {
   $('#roto_wrapper, .slide, .roto_lp_wrapper').css({
       opacity: 0, visibility: "visible"
   }).animate({ opacity: 1 }, 1000);
   
});