// **********************************
// *** Mastercard Optimization Solutions Include
// *** Last Updated 03-27-09
// **********************************

// setup paths to js files
// - use relative path starting from domain name to keep the protocol intact
var wsstag_jsConfigURL = document.location.protocol + "//" + "<RELATIVE URL>/js/wss_mcmarketing_config.js";
var wsstag_jsImplURL = document.location.protocol + "//" + "<RELATIVE URL>/js/hbx.js";

// set ON/OFF flag
if (typeof(wsstag_enabled) == 'undefined')
var wsstag_enabled = "true";
var wsstag_batch = "false";
var wsstag_location = document.location;
var wsstag_doctitle = document.title;

// Disable the tag if the page contains an iFrame
if (window.frames.length > 0) {
		 wsstag_enabled = "false";
		 if(typeof(wsstag_iFrame_override) != "undefined"){
		 		 wsstag_enabled="true";
		 }
}

// set prod env value
var wsstag_prodEnv = "prod";  // used to compare against environment result
var wsstag_testEnv = "test";

// determine current environment
var wsstag_hostname = location.hostname;
// check for typical non-prod domains as well as IP addresses
var wsstag_regex = new RegExp("\\b(dev|stage|stag.*|test|localhost|\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})\\b", "i");
var wsstag_matches = wsstag_hostname.match(wsstag_regex);

var wsstag_envResult = "test";
if(wsstag_matches == null) {
	if (location.href.indexOf('optimization.mastercard.com') != -1) {
		wsstag_envResult = "prod";
	}
}

// include js files
if (wsstag_enabled == "true") {
   document.write("<scr"+"ipt src=\""+wsstag_jsConfigURL+"\"></scr"+"ipt>");
   document.write("<scr"+"ipt defer src=\""+wsstag_jsImplURL+"\"></scr"+"ipt>");
}