// SMALL BIZ
var cal1 = new MarketingCalendar('Calendar1', 2010);
var catg1 = new CategoryGroup('Acquisition');
var cat1 = new Category('Turnkey Marketing Materials');
var cat2 = new Category('Marketing Programs');


var catg2 = new CategoryGroup('Activation');
var cat3 = new Category('Turnkey Marketing Materials');
var cat4 = new Category('Marketing Programs');


var catg3 = new CategoryGroup('Usage');
var cat5 = new Category('Turnkey Marketing Materials');
var cat6 = new Category('Marketing Programs');
var cat7 = new Category('Experiences and Offers');
var cat8 = new Category('Turnkey Marketing Assets');

var catg4 = new CategoryGroup('Retention');
var cat9 = new Category('Marketing Programs');

var catg5 = new CategoryGroup('Marketing, Planning, and Execution');
var cat10 = new Category('Propensity Models');
var cat11 = new Category('Tracking/Analysis');

// CATEGORY GROUP 1 -- Acquisition
cal1.addCategoryGroup(catg1);


// Turnkey Marketing Materials
cal1.addCategory(cat1);

var p  = new Promo('Product Issuance');
p.setLink('');
p.setOngoing("Help issuers effectively leverage debit opportunity as cardholders shift away from cash and checks. Drive engagement and cardholder loyalty from the early stages of an account relationship with two comprehensive <em>Debit Tool Kits</em>&mdash;one for small business and one for debit (consumer). Each DVD contains strategic planning tools, industry best practices guides, and more than 40 customizable cardholder collateral and direct mail pieces. Debit turnkey marketing materials include prospect letter, upgrade package, and branch materials (posters, tent cards, and take one brochures). For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);


// Marketing Programs
cal1.addCategory(cat2);

var p  = new Promo('Priceless Partnership');
p.setLink('/portal/marketing/pricelesspromo');
p.setOrderPeriod('20101010', '20101031');
//p.setInMarket('20110101', '20110331', "This program is designed to give employees further incentive to sell new MasterCard products. Every time a participating branch or call center employee submits a validated application for a new MasterCard product, he/she receives an online Instant Win Game Play. This promotion runs quarterly, with each quarter featuring its own unique theme, prize structure, and chances to win. A variety of marketing materials are available.<Br><br> For more information on the Priceless Partnership&trade; Program, please contact your MasterCard Account Representative.");
cal1.addPromo(p);


var p  = new Promo('Employee Training');
p.setLink('');
p.setOrderPeriod('20100101', '20100131');
p.setInMarket('20100401', '20100630', "This program is designed to educate and train employees on the benefits of MasterCard products. These self-paced modules offer a combination of standard and customized content sections. Each module focuses on MasterCard benefits, card benefits, issuer benefits, and frequently asked questions and answers. The training module is also integrated with the Priceless Partnership&trade; incentive module.<br><br>For more information on the Priceless Partnership&trade; Program, please contact your MasterCard Account Representative.");
cal1.addPromo(p);



// CATEGORY GROUP 2 -- Activation
cal1.addCategoryGroup(catg2);

// Turnkey Marketing Materials
cal1.addCategory(cat3);

var p  = new Promo('Product Activation');
p.setLink('');
p.setOngoing("Help issuers effectively leverage debit opportunity as cardholders shift away from cash and checks. Drive engagement and cardholder loyalty from the early stages of an account relationship with two comprehensive <em>Debit Tool Kits</em>&mdash;one for small business and one for debit (consumer). Each DVD contains strategic planning tools, industry best practices guides, and more than 40 customizable cardholder collateral and direct mail pieces. Debit turnkey marketing materials include educational inserts (convenience, flexibility, and security) and new card fulfillment materials (card carrier, welcome kit, activation stickers, welcome call, scripts, and VRU messaging).<Br><Br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

// Marketing Programs
cal1.addCategory(cat4);

var p  = new Promo('Low Usage Cash Rewards');
p.setLink('/portal/marketing/cash-rewards');
p.setOrderPeriod('20101001', '20101031');
//p.setInMarket('20110201', '20110228', "Help drive incremental transactions and dollar volume with the Cash Rewards program, offering cash incentives (statement credit) for cardholders who reach predetermined transaction levels during the promotion periods. Incentives are typically tiered to successfully motivate inactive and low-usage cardholders.<Br><Br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p  = new Promo('Conversion 3-Step Program');
p.setLink('');
p.setOngoing("Our Conversion 3-Step Program is a comprehensive tool for product conversions, migrations, and product upgrades to complement your welcome kit. Use this 3-step program to increase awareness, enhance card activation, and drive usage. <br><Br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);



// CATEGORY GROUP 3 -- Usage
cal1.addCategoryGroup(catg3);


// Turnkey Marketing Materials
cal1.addCategory(cat5);

var p  = new Promo('Money Manager Tool&trade;');
p.setLink('');
p.setOngoing("Enhance cardholder relationships by offering them access to an easy-to-use financial management tool that enables cardholders to review and track their spending on their MasterCard card. Cardholders can easily establish a budget and track spending against that budget, create customized categories, track by merchant, and identify spending patterns. Sign up for MasterCard Money Manager&trade; and inform cardholders of tool availability and benefits with turnkey marketing materials. Materials include statement inserts, brochures, customer service frequently asked questions, Web banners, statement messaging, and promotional e-mails. (Turnkey marketing materials available for Debit only. Materials can be modified for Credit, Small Business, and Prepaid cards).<br><Br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p  = new Promo('Debit Usage Building');
p.setLink('');
p.setOngoing("Help issuers effectively leverage debit opportunity as cardholders shift away from cash and checks. Drive engagement and cardholder loyalty from the early stages of an account relationship with two comprehensive <em>Debit Tool Kits</em>&mdash;one for small business and one for debit (consumer). Each DVD contains strategic planning tools, industry best practices guides, and more than 40 customizable cardholder collateral and direct mail pieces. Debit turnkey marketing materials include statement insert, self mailers, and Web banners.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p  = new Promo('Bill Payment Opportunities');
p.setLink('');
p.setOngoing("MasterCard has developed customized consumer statement inserts designed to inform cardholders about the ease and convenience of signing up for automatic bill payment. It also helps to drive usage and retention for issuers. These turnkey inserts are available in both generic and rewards versions. Available as art on disk.<br><br>Please visit www.mastercardonline.com for more information.");
cal1.addPromo(p);

// Marketing Programs
cal1.addCategory(cat6);

var p = new Promo('Baseball Sweepstakes');
p.setLink('');
p.setOrderPeriod('20101101', '20110315');
//p.setInMarket('20110201', '20110315', "MLB&trade; Opening Day is a tradition for many baseball fans, marking the beginning of a new season. MasterCard starts the year with an exciting Major League Baseball<sup>&reg;</sup> promotion that will run February 1 through March 15, giving your cardholders the chance to win a trip for four to a 2011 MLB&trade; Opening Day game of the winner's choice.<br><Br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p = new Promo('City Experience Sweepstakes');
p.setLink('');
p.setOrderPeriod('20100201', '20100514');
p.setInMarket('20100515', '20100630', "Help drive activation and cardholder usage with this exciting promotion! MasterCard is giving two cardholders the chance to win a trip for an ultimate weekend getaway to a selected city! Plus, each winner gets to bring three guests! <br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p = new Promo('MLB&trade; World Series');
p.setLink('');
p.setOrderPeriod('20100503', '20100731');
p.setInMarket('20100801', '20100915', "MasterCard cardholders have the chance to win a trip for four to the 2010 MLB&trade; World Series<sup>&reg;</sup> game. Every time your cardholders use their MasterCard cards between August 1 and September 15, 2010, they&rsquo;ll automatically be entered for a chance to win a 3-day/2-night trip to a 2010 MLB&trade; World Series<sup>&reg;</sup> game.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p = new Promo('GRAMMY Awards');
p.setLink('/portal/marketing/grammy-awards');
p.setOrderPeriod('20100702', '20101130');
p.setInMarket('20101001', '20101130', "With this exciting MasterCard promotion, your cardholders will have the chance to win a VIP Grammy Awards Experience! One lucky winner and a guest will have the chance to see their favorite musical artists on the red carpet and hear them perform live at the Grammy Awards!<Br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p = new Promo('Category Expansion');
p.setLink('');
p.setOrderPeriod('20100607', '20100705');
p.setInMarket('20101001', '20101031', "Direct marketing program targeting cardholders who do not currently spend or have minimal spend in everyday spend categories such as casual dining restaurant, grocery, gas, and pharmacy. Cardholders are offered an incentive to make at least three transactions in the targeted category during the promotion period. Designed to help increase card spend in high-frequency categories.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p = new Promo('Seasonal Spend Program');
p.setLink('');
p.setOrderPeriod('20100701', '20100731');
p.setInMarket('20101101', '20101231', "Help increase share of wallet and debit/credit card transaction frequency during key spend periods with this direct marketing program targeting low (average of 1-4 transactions per month) and medium active (average of 5-8 transactions per month) cardholders. Cardholders have the opportunity to win promotion-specific merchandise.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p = new Promo('Recurring Payments');
p.setLink('/portal/marketing/recurring-pymt');
p.setOrderPeriod('20100901', '20100930');
//p.setInMarket('20110101', '20110131', "Help increase activation and spend on recurring payment transactions! This direct marketing program designed to help increase activation and spend on recurring payment transactions. This is a cost-effective way to promote benefits and convenience of recurring payments by offering cardholders incentives (statement credit, miles, or points) when making recurring payments. The April 2009 program yielded response rates ranging from 3.5 to 5.6 percent, as well as an increase in non-recurring payment transactions of 10 to 25 percent.* <Br><br>For more information, please contact your MasterCard Account Representative. <br><Br>*April 2009 Recurring Payments Campaign.");
cal1.addPromo(p);


var p = new Promo('National Bill Payment');
p.setLink('/portal/marketing/natl-pymt');
p.setOrderPeriod('20100701', '20101031');
p.setInMarket('20100901', '20101130', "Use your MasterCard debit or credit card to pay your bills, especially your telephone, cable, and insurance bills and you&rsquo;ll be automatically entered for a  chance to win $60,000.");
cal1.addPromo(p);


// Experiences and Offers
cal1.addCategory(cat7);

var p  = new Promo('MasterCard EZ Savings&trade;');
p.setLink('');
p.setOngoing("Now MasterCard BusinessCard&reg; works even harder for you and your small business cardholders. Use Easy Savings to drive acquisition, activation, usage, and retention. Your business cardholders can enjoy automatic rebates at great merchant partners across the travel, entertainment, and business tools categories. This program is easy to use; cardholders simply sign up at <u>www.mastercardeasysavings.com</u> and use their MasterCard BusinessCard at any one of the participating partners. Rebate savings add up automatically, and the savings are easy to see on their monthly statement, or they can track them online.");
cal1.addPromo(p);

// Turnkey Marketing Assets
cal1.addCategory(cat8);

//var p = new Promo('U.S. Sponsorships Portfolio'); 
var p = new Promo('US Sponsorships Portfolio');
p.setLink('');
p.setOngoing("Enhance cardholder loyalty by building custom programs choosing from existing MasterCard sponsorship portfolio. (Some restrictions apply).<br><Br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

// CATEGORY GROUP 4 -- Retention
cal1.addCategoryGroup(catg4);

// Marketing Programs
cal1.addCategory(cat9);

var p = new Promo('Proactive Retention');
p.setLink('');
p.setOrderPeriod('20100501', '20100531');
p.setInMarket('20100901', '20100930', "Taking care of the customers you&rsquo;ve won is important. This program helps you keep your most valuable customers. Using analytics and modeling, high value cardholders that are likely to attrite are identified. These cardholders are sent communications that reinforces core product benefits to enhance the overall customer relationship.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);


// CATEGORY GROUP 4 -- Marketing, Planning, and Execution
cal1.addCategoryGroup(catg5);

// Propensity Models
cal1.addCategory(cat10);

var p = new Promo('Propensity Models');
p.setLink('');
p.setOngoing("Help improve campaign performance with these analytical support tools. Propensity Models help identify current card users with certain propensities, such as those at risk of attrition, those most likely to start using the card again after being dormant, and potential small businesses &quot;hidden&quot; among consumer accounts. Models include Spend Attrition, Early-Month-on-Books, and Category Expansion.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

// Tracking/Analysis
cal1.addCategory(cat11);

var p = new Promo('Portfolio Analytics');
p.setLink('');
p.setOngoing("<i>Portfolio Analytics</i> provides customer-specific information about its marketing, risk, operations, and fraud. This data, in conjunction with information about specific regions and countries, helps facilitate more effective portfolio marketing strategies and overall management.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p = new Promo('Campaign Management');
p.setLink('');
p.setOngoing("<i>Campaign Management</i> is an interactive information-enabled application tailored specifically to the needs of the payments industry. It helps issuers to<ul style='margin-top:0px;margin-bottom:0px;'><li>Increase marketing productivity by automatic cardholder selection and segmentation, and  building portfolios and marketing cells in real time</li><li>Improve campaign results by analyzing prior transaction behavior, card specific metrics, and user-defined fields to target those cardholders most likely to respond</li><li>Track and report cardholder behavior before, during, and after a campaign in order to analyze a promotion's effectiveness and modify it accordingly for future campaigns</li><li>Campaign Management's advanced capabilities and ease of use may help you realize more revenue potential from your marketing campaigns. Campaign Management is available to issuers through Portfolio Analytics.</li></ul><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

cal1.buildCalendar();
