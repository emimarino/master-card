// SMALL BIZ
var cal1 = new MarketingCalendar('Calendar1', 2011);
var catg1 = new CategoryGroup('Acquisition');
var cat1 = new Category('Turnkey Marketing Materials');


var catg2 = new CategoryGroup('Activation');
var cat2 = new Category('Turnkey Marketing Materials');


var catg3 = new CategoryGroup('Usage');
var cat3 = new Category('Turnkey Marketing Materials');
var cat4 = new Category('Marketing Programs');
var cat5 = new Category('Experiences and Offers');
var cat6 = new Category('Turnkey Marketing Assets');

var catg4 = new CategoryGroup('Retention');
var cat7 = new Category('Marketing Programs');



// CATEGORY GROUP 1 -- Acquisition
cal1.addCategoryGroup(catg1);


// Turnkey Marketing Materials
cal1.addCategory(cat1);
var p  = new Promo('Priceless Partnership');
p.setLink('/portal/program/8d7/MasterCard-Priceless-Partnership-Program');
p.setOrderPeriod('20110701', '20110731');
p.setInMarket('20111001', '20111231', "This program is designed to give employees further incentive to sell new MasterCard products. Every time a participating branch or call center employee submits a validated application for a new MasterCard product, he/she receives an online Instant Win Game Play. This promotion runs quarterly, with each quarter featuring its own unique theme, prize structure, and chances to win. A variety of marketing materials are available.<br><br>For more information on the Priceless Partnership&trade; Program, please contact your MasterCard Account Representative. ");
cal1.addPromo(p);

var p  = new Promo('Employee Training');
p.setLink('');
p.setOrderPeriod('20110301', '20110331');
p.setInMarket('20110701', '20110929', "This program is designed to educate and train employees on the benefits of MasterCard products. These self-paced modules offer a combination of standard and customized content sections. Each module focuses on MasterCard benefits, card benefits, issuer benefits, and frequently asked questions and answers. The training module is also integrated with the Priceless Partnership&trade; incentive module.<br><br>For more information on the Priceless Partnership&trade; Program, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p  = new Promo('Product Issuance');
p.setLink('/portal/products');
p.setOngoing("Drive engagement and cardholder loyalty from the early stages of an account relationship with comprehensive customizable cardholder collateral and direct mail pieces. Turnkey marketing materials including prospect letter, upgrade package, and branch materials including posters, tent cards, and take one brochures (Turnkey marketing materials available for Small Business Debit only).<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

// CATEGORY GROUP 2 -- Activation
cal1.addCategoryGroup(catg2);

// Turnkey Marketing Materials
cal1.addCategory(cat2);

var p  = new Promo('Product Activation');
p.setLink('https://mas.mastercard.net/pkmsvouchfor?molprod&https://mbe2stl101.mastercard.net/debit/debitonline/sb_practices/sb_activation_usage.aspx');
p.setOngoing("Drive engagement and cardholder loyalty from the early stages of an account relationship with customizable cardholder collateral and direct mail pieces (Turnkey marketing materials available for Small Business Debit only).<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

// CATEGORY GROUP 3 -- Usage
cal1.addCategoryGroup(catg3);


// Turnkey Marketing Materials
cal1.addCategory(cat3);

/*var p  = new Promo('Money Manager Tool&trade;');
p.setLink('/portal/products/money-manager');
p.setOngoing("Enhance cardholder relationships by offering them access to an easy-to-use financial management tool that enables cardholders to review and track their spending on their MasterCard card. Cardholders can easily establish a budget and track spending against that budget, create customized categories, track by merchant, and identify spending patterns. Sign up for MasterCard Money Manager&trade; and inform cardholders of tool availability and benefits with turnkey marketing materials. Materials include statement inserts, brochures, customer service frequently asked questions, Web banners, statement messaging, and promotional e-mails. (Turnkey marketing materials available for Debit only. Materials can be modified for Small Business.)<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);*/

var p  = new Promo('Usage Building');
p.setLink('');
p.setOngoing("Drive engagement and cardholder loyalty from the early stages of an account relationship with customizable cardholder collateral and direct mail pieces (Turnkey marketing materials available for Small Business Debit only).<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p  = new Promo('Digital Assets')
p.setLink('/portal/program/1bf/Budgeting-Bill-Pay-and-Savings-Tool');
p.setOngoing("MasterCard Co-Brand Program is a turnkey solution that allows you to leverage web content in an efficient and cost-effective manner to help you meet your business objectives.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);


// Marketing Programs
cal1.addCategory(cat4);

var p = new Promo('Q1 Baseball Sweepstakes');
p.setLink('');
p.setOrderPeriod('20101101', '20101130');
p.setInMarket('20110201', '20110315', "Drive MasterCard&reg; card usage through this exciting promotion designed to be a home run for your customers. <br><Br> MasterCard starts 2011 with an exciting baseball promotion that will run February 1 through March 15, giving your cardholders the chance to win a three-day/two-night trip for four to a 2011 baseball game. Imagine starting the year with a trip with 3 guests and sitting in the stands to celebrate the start of a new season with their favorite team. Priceless.");
cal1.addPromo(p);

var p = new Promo('Ultimate NYC Weekend');
p.setLink('');
p.setOrderPeriod('20110201', '20110321');
p.setInMarket('20110516', '20110630', "Help drive activation and cardholder usage with this exciting promotion! MasterCard is giving 2 Grand Prize winners and the chance to win a trip for an ultimate weekend getaway to New York City! Each winner and their guest will receive VIP treatment in the big city including a shopping spree, a trip to a spa, a night on the town listening to jazz and more! Turnkey marketing materials include statement insert, banner ads, statement message, newsletter and e-mail copy, and online banners.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p = new Promo('Q3 Baseball Sweepstakes');
p.setLink('');
p.setOrderPeriod('20110502', '20110531');
p.setInMarket('20110801', '20110915', "MasterCard cardholders have the chance to win a trip for four to the 2011 MLB&trade; World Series<sup>&reg;</sup> game. Every time your cardholders use their MasterCard cards between August 1 and September 15, 2011, they'll automatically be entered for a chance to win a 3-day/2-night trip to a 2011 MLB&trade; World Series<sup>&reg;</sup> game.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p = new Promo('GRAMMY Awards');
p.setLink('');
p.setOrderPeriod('20110822', '20110930');
p.setInMarket('20111001', '20111130', "With this exciting MasterCard promotion, your cardholders will have the chance to win a VIP Grammy Awards Experience! One lucky winner and a guest will have the chance to see their favorite musical artists on the red carpet and hear them perform live at the Grammy Awards!<br><br>For more information, please contact your MasterCard Account Representative. ");
cal1.addPromo(p);

var p = new Promo('Category Expansion');
p.setLink('/portal/program/8d9/Category-Expansion');
p.setOrderPeriod('20110601', '20110630');
p.setInMarket('20111001', '20111031', "Direct marketing program targeting cardholders who do not currently spend or have minimal spend in everyday spend categories such as casual dining restaurant, grocery, gas, and pharmacy. Cardholders are offered an incentive to make at least three transactions in the targeted category during the promotion period. Designed to help increase card spend in high-frequency categories.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

/*var p = new Promo('Seasonal Spend Program');
p.setLink('');
p.setOrderPeriod('20110301', '20110330');
p.setInMarket('20110701', '20110831', "Help increase share of wallet and debit/credit card transaction frequency during key spend periods with this direct marketing program targeting low (average of 1-4 transactions per month) and medium active (average of 5-8 transactions per month) cardholders. Cardholders have the opportunity to win promotion-specific merchandise.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);*/

var p = new Promo('Recurring Payments');
p.setLink('/portal/program/8d8/Recurring-Payments');
p.setOrderPeriod('20110601', '20110630');
p.setInMarket('20111001', '20111031', "Help increase activation and spend on recurring payment transactions! This direct marketing program designed to help increase activation and spend on recurring payment transactions. This is a cost-effective way to promote benefits and convenience of recurring payments by offering cardholders incentives (statement credit, miles, or points) when making recurring payments. The March 2010 program results yielded response rates ranging from 2 to 6.3 percent, as well as an increase in non-recurring payment transactions of 2.1 to 8.3 percent.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

// Experiences and Offers
cal1.addCategory(cat5);

var p  = new Promo('MasterCard Easy Savings&trade;');
p.setLink('/portal/program/8f1/The-MasterCard-Easy-Savings-Program');
p.setOngoing("Now MasterCard BusinessCard&reg; works even harder for you and your small business cardholders. Use Easy Savings to drive acquisition, activation, usage, and retention. Your business cardholders can enjoy automatic rebates at great merchant partners across the travel, entertainment, and business tools categories. This program is easy to use; cardholders simply sign up at <u>www.mastercardeasysavings.com</u> and use their MasterCard BusinessCard at any one of the participating partners. Rebate savings add up automatically, and the savings are easy to see on their monthly statement, or they can track them online.");
cal1.addPromo(p);

// Turnkey Marketing Assets
cal1.addCategory(cat6);

var p = new Promo('U.S. Sponsorships');
p.setLink('');
p.setOngoing("Enhance cardholder loyalty by building custom programs choosing from existing MasterCard sponsorship portfolio. (Some restrictions apply).<br><br>For more information, please contact your MasterCard Account Representative. ");
cal1.addPromo(p);

// CATEGORY GROUP 4 -- Retention
cal1.addCategoryGroup(catg4);

// Marketing Programs
cal1.addCategory(cat7);

var p = new Promo('Proactive Retention');
p.setLink('');
p.setOrderPeriod('20110104', '20110129');
p.setInMarket('20110401', '20110430', "Taking care of the customers you&rsquo;ve won is important. This program helps you keep your most valuable customers. Using analytics and modeling, high value cardholders that are likely to attrite are identified. These cardholders are sent communications that reinforces core product benefits to enhance the overall customer relationship. ");
cal1.addPromo(p);

cal1.buildCalendar();
