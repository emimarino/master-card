// PREPAID
var cal1 = new MarketingCalendar('Calendar1', 2011);
var catg1 = new CategoryGroup('Acquisition');
var cat1 = new Category('Turnkey Marketing Materials');


var catg2 = new CategoryGroup('Activation');
var cat2 = new Category('Turnkey Marketing Materials');


var catg3 = new CategoryGroup('Usage');
var cat3 = new Category('Turnkey Marketing Materials');
var cat4 = new Category('Marketing Programs');
var cat5 = new Category('Experiences and Offers');
var cat6 = new Category('Turnkey Marketing Assets');

var catg4 = new CategoryGroup('Retention');
var cat7 = new Category('Turnkey Marketing Materials');



// CATEGORY GROUP 1 -- Acquisition
cal1.addCategoryGroup(catg1);


// Turnkey Marketing Materials
cal1.addCategory(cat1);
var p  = new Promo('Priceless Partnership');
p.setLink('/portal/program/8d7/MasterCard-Priceless-Partnership-Program');
p.setOrderPeriod('20110701', '20110731');
p.setInMarket('20111001', '20111231', "This program is designed to give employees further incentive to sell new MasterCard products. Every time a participating branch or call center employee submits a validated application for a new MasterCard product, he/she receives an online Instant Win Game Play. This promotion runs quarterly, with each quarter featuring its own unique theme, prize structure, and chances to win. A variety of marketing materials are available.<br><br>For more information on the Priceless Partnership&trade; Program, please contact your MasterCard Account Representative. ");
cal1.addPromo(p);


var p  = new Promo('Product Issuance');
p.setLink('/portal/products');
p.setOngoing("Drive engagement and cardholder loyalty from the early stages of an account relationship with comprehensive customizable cardholder collateral and direct mail pieces. Turnkey marketing materials(prospect letter, upgrade package, and branch materials including posters, tent cards, and take one brochures).<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p  = new Promo('Consumer Toolkit');
p.setLink('/portal/products/prepaid-overview');
p.setOngoing("Consumers are seeking the convenience and security that only prepaid cards from MasterCard can provide. Given the increasing popularity of gift, travel, and reloadable cards in today&rsquo;s society, now is clearly the time to capitalize on the burgeoning demand for these products. The MasterCard&reg; Prepaid Toolkit offers a comprehensive collection of consumer marketing and program materials to help you establish and enhance your prepaid card portfolios. The toolkit includes a wide range of materials including statement inserts, door clings, and more. These materials are available free-of-charge as a benefit of your relationship with MasterCard.");
cal1.addPromo(p);

var p  = new Promo('Commercial Toolkit');
p.setLink('/portal/products/prepaid-overview');
p.setOngoing("Whether the need is for a large market, middle market, small business, or public sector payment solution, MasterCard offers a Commercial Prepaid Solution to bring speed and efficiency through the benefits of electronic payments. The MasterCard&reg; Prepaid Toolkit offers a comprehensive collection of commercial marketing and program materials to help you establish and enhance your prepaid card portfolios. The toolkit includes a wide range of materials including statement inserts, door clings, and more. These materials are available free-of-charge as a benefit of your relationship with MasterCard.");
cal1.addPromo(p);

var p  = new Promo('Consumer Materials');
p.setLink('/portal/products/prepaid-overview');
p.setOngoing("MasterCard offers robust prepaid card consumer education and awareness materials to help unbanked and underbanked consumers understand the benefits of a reloadable prepaid card.  Educating consumers on these benefits help consumers understand the value of prepaid cards as well as change misperceptions. &ldquo;Prepaid for everyday transactions&rdquo; and &ldquo;Choosing your card and understanding fees&rdquo; are the first two volumes available of what will be six volumes of education and awareness materials.");
cal1.addPromo(p);

// CATEGORY GROUP 2 -- Activation
cal1.addCategoryGroup(catg2);

// Turnkey Marketing Materials
cal1.addCategory(cat2);

var p  = new Promo('Product Activation');
p.setLink('');
p.setOngoing("Drive engagement and cardholder loyalty from the early stages of an account relationship with customizable cardholder collateral and direct mail pieces.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

// CATEGORY GROUP 3 -- Usage
cal1.addCategoryGroup(catg3);


// Turnkey Marketing Materials
cal1.addCategory(cat3);

/*var p  = new Promo('Money Manager Tool&trade;');
p.setLink('/portal/products/money-manager');
p.setOngoing("Enhance cardholder relationships by offering them access to an easy-to-use financial management tool that enables cardholders to review and track their spending on their MasterCard card. Cardholders can easily establish a budget and track spending against that budget, create customized categories, track by merchant, and identify spending patterns. Sign up for MasterCard Money Manager&trade; and inform cardholders of tool availability and benefits with turnkey marketing materials. Materials include statement inserts, brochures, customer service frequently asked questions, Web banners, statement messaging, and promotional e-mails. (Turnkey marketing materials available for Debit only. Materials can be modified for Prepaid cards.)<br><Br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);*/

var p  = new Promo('Digital Assets')
p.setLink('/portal/program/8f4/Budgeting-Bill-Pay-and-Savings-Tool');
p.setOngoing("MasterCard Co-Brand Program is a turnkey solution that allows you to leverage web content in an efficient and cost-effective manner to help you meet your business objectives.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

// Marketing Programs
cal1.addCategory(cat4);

var p = new Promo('Q1 Baseball Sweepstakes');
p.setLink('');
p.setOrderPeriod('20101101', '20101130');
p.setInMarket('20110201', '20110315', "Drive MasterCard&reg; card usage through this exciting promotion designed to be a home run for your customers. <br><Br> MasterCard starts 2011 with an exciting baseball promotion that will run February 1 through March 15, giving your cardholders the chance to win a three-day/two-night trip for four to a 2011 baseball game. Imagine starting the year with a trip with 3 guests and sitting in the stands to celebrate the start of a new season with their favorite team. Priceless.");
cal1.addPromo(p);

var p = new Promo('Ultimate NYC Weekend');
p.setLink('');
p.setOrderPeriod('20110201', '20110321');
p.setInMarket('20110516', '20110630', "Help drive activation and cardholder usage with this exciting promotion! MasterCard is giving 2 Grand Prize winners and the chance to win a trip for an ultimate weekend getaway to New York City! Each winner and their guest will receive VIP treatment in the big city including a shopping spree, a trip to a spa, a night on the town listening to jazz and more! Turnkey marketing materials include statement insert, banner ads, statement message, newsletter and e-mail copy, and online banners.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p = new Promo('Q3 Baseball Sweepstakes');
p.setLink('');
p.setOrderPeriod('20110502', '20110531');
p.setInMarket('20110801', '20110915', "MasterCard cardholders have the chance to win a trip for four to the 2011 MLB&trade; World Series<sup>&reg;</sup> game. Every time your cardholders use their MasterCard cards between August 1 and September 15, 2011, they'll automatically be entered for a chance to win a 3-day/2-night trip to a 2011 MLB&trade; World Series<sup>&reg;</sup> game.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p = new Promo('GRAMMY Awards');
p.setLink('');
p.setOrderPeriod('20110822', '20110930');
p.setInMarket('20111001', '20111130', "With this exciting MasterCard promotion, your cardholders will have the chance to win a VIP Grammy Awards Experience! One lucky winner and a guest will have the chance to see their favorite musical artists on the red carpet and hear them perform live at the Grammy Awards!<br><br>For more information, please contact your MasterCard Account Representative. ");
cal1.addPromo(p);

var p = new Promo('Tax Payments Program');
p.setLink('/portal/program/1c0/MasterCard-Tax-Payment-Program');
p.setOngoing("Enhance cardholder relationships with a comprehensive set of marketing materials to help you promote card usage for federal tax payments. Cardholders receive a lower convenience fee of 1.95 % on consumer and business tax payments when they pay through Value Payments. MasterCard customized tax statement inserts are designed to inform cardholders about the ease of paying taxes with a MasterCard card while increasing usage and transactions in the tax payment category.");
cal1.addPromo(p);

// Experiences and Offers
cal1.addCategory(cat5);

var p = new Promo('MasterCard MarketPlace&trade;');
p.setLink('/portal/program/8ef/MasterCard-MarketPlace');
p.setOngoing("MasterCard MarketPlace&trade; motivates acquisitions and card usage frequency among your signature debit and prepaid cardholders. The program rewards everyday spending at virtually no cost to issuers. This complimentary turnkey offers program provides your MasterCard debit and prepaid cardholders with hundreds of offers each week from merchants nationwide&mdash;a continual stream of weekly and limited-time discounts ranging from 5% to 50%!");
cal1.addPromo(p);

var p  = new Promo('Valentine&rsquo;s Day Promotion');
p.setLink('/portal/program/8ef/MasterCard-MarketPlace');
p.setOrderPeriod('20101215', '20110201');
p.setInMarket('20110117', '20110214', "Encourage MasterCard Debit, Credit, and Prepaid usage and savings with Valentine&rsquo;s Day themed emails and web banners.");
cal1.addPromo(p);

var p  = new Promo('Mother&rsquo;s Day Promotion');
p.setLink('/portal/program/8ef/MasterCard-MarketPlace');
p.setOrderPeriod('20110228', '20110411');
p.setInMarket('20110411', '20110508', "Encourage MasterCard Credit, Debit, and Prepaid usage and savings with Mother&rsquo;s Day themed emails and web banners.");
cal1.addPromo(p);

var p  = new Promo('Dads & Grads Promotion');
p.setLink('/portal/program/8ef/MasterCard-MarketPlace');
p.setOrderPeriod('20110321', '20110516');
p.setInMarket('20110516', '20110630', "Encourage MasterCard Credit, Debit, and Prepaid usage and savings with Father&rsquo;s Day and Graduation themed emails and web banners.");
cal1.addPromo(p);

var p  = new Promo('Summer Savings');
p.setLink('/portal/program/8ef/MasterCard-MarketPlace');
p.setOrderPeriod('20110418', '20110620');
p.setInMarket('20110620', '20110730', "Encourage MasterCard Debit, Credit, and Prepaid usage and savings with summer themed emails and web banners.");
cal1.addPromo(p);

var p  = new Promo('Back to School');
p.setLink('/portal/program/8ef/MasterCard-MarketPlace');
p.setOrderPeriod('20110613', '20110815');
p.setInMarket('20110718', '20110915', "Encourage MasterCard Debit, Credit, and Prepaid usage and savings with back-to-school themed emails and web banners.");
cal1.addPromo(p);

var p  = new Promo('Holiday');
p.setLink('/portal/program/1/MasterCard-MarketPlace-Holiday-Program');
p.setOrderPeriod('20111001', '20111215');
p.setInMarket('20111130', '20120102', "Encourage MasterCard Debit, Credit, and Prepaid usage and savings with back-to-school themed emails and web banners.");
cal1.addPromo(p);

var p = new Promo('Priceless Moments');
p.setLink('');
p.setOngoing("Help enhance cardholder loyalty with the Priceless Moments<sup>&reg;</sup> Collection. This year-round program leverages exciting priceless sports and entertainment experiences. Each turnkey package comes with a support team to help execute prizes/experience. These programs can be marketed on a local, national, or global level to suit customer&rsquo;s needs.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

// Turnkey Marketing Assets
cal1.addCategory(cat6);

var p = new Promo('U.S. Sponsorships');
p.setLink('');
p.setOngoing("Enhance cardholder loyalty by building custom programs choosing from existing MasterCard sponsorship portfolio. (Some restrictions apply).<br><br>For more information, please contact your MasterCard Account Representative. ");
cal1.addPromo(p);

// CATEGORY GROUP 4 -- Retention
cal1.addCategoryGroup(catg4);

// Turnkey marketing materials
cal1.addCategory(cat7);

var p = new Promo('Consumer Education');
p.setLink('');
p.setOngoing("MasterCard has developed education programs that are specifically designed to help consumers develop sound money management skills. These ready-to-use education sheets and complementing statement inserts encourage responsible spending, saving, and borrowing behavior with your cardholders.");
cal1.addPromo(p);

cal1.buildCalendar();
