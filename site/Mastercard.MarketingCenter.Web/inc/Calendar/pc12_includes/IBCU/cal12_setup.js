﻿var cal1 = new MarketingCalendar('Calendar1', 2012);
var catg1 = new CategoryGroup('Acquisition');
var cat1 = new Category('Turnkey Marketing Materials');

var catg2 = new CategoryGroup('Usage');
var cat2 = new Category('Turnkey Marketing Materials');
var cat3 = new Category('Marketing Programs');
var cat4 = new Category('Experiences and Offers');

var catg3 = new CategoryGroup('Retention');
var cat6 = new Category('Turnkey Marketing Materials');



// CATEGORY GROUP 1 -- Acquisition
cal1.addCategoryGroup(catg1);


// Turnkey Marketing Materials
cal1.addCategory(cat1);

var p = new Promo('Commercial Toolkit');
p.setLink('/portal/products/corporate');
p.setOngoing("Whether you need payment solutions for a large market, middle market, small business, or public sector, MasterCard offers commercial card solutions that bring speed and efficiency with the benefits of electronic payments and superior enhanced reporting.<br><br>Commercial Toolkits are available for all Credit and Prepaid products.");
cal1.addPromo(p);

var p = new Promo('Priceless Partnership&trade;');
p.setLink('/portal/program/8d7/MasterCard-Priceless-Partnership-Program');
p.setOrderPeriod('20120501', '20120601');
p.setInMarket('20120702', '20121231', "Join us for our second phase of the Priceless Partnership Program, which will run from July 2 through December 31st. This program now runs twice a year with unique theme, prize structure, and chances to win! Check back soon for more information.<br/><br/>MasterCard Priceless Partnership&trade; Program is available for all Small Business, Credit, Debit, and Prepaid products.");
cal1.addPromo(p);

// CATEGORY GROUP 3 -- Usage
cal1.addCategoryGroup(catg2);


// Turnkey Marketing Materials
cal1.addCategory(cat2);

var p = new Promo('Digital Assets');
p.setLink('/portal/program/1bf/Budgeting-Bill-Pay-and-Savings-Tool');
p.setOngoing("Let MasterCard’s prebuilt tools enhance your online presence. Strengthen and differentiate your cardholder program with our turnkey budgeting, bill pay, and savings tool—all at no additional cost to you.<br><br>Digital Assets are available for all Small Business, Credit, Debit, and Prepaid products.");
cal1.addPromo(p);

var p = new Promo('MasterCard Priceless Pointers');
p.setLink('/portal/program/1c2/MasterCard-Priceless-Pointers');
p.setOngoing("Strengthen your cardholder program with interactive financial management tools at no additional cost to you. Leverage MasterCard’s Priceless Pointers to help your cardholders spend smarter in today’s trying economic times—and to help drive engagement and build loyalty.<br><br>MasterCard Priceless Pointers are available for all Small Business, Credit, Debit, and Prepaid products.");
cal1.addPromo(p);

var p = new Promo('MasterCard PriceAssure&trade;');
p.setLink('/portal/program/1bd/MasterCard-PriceAssure');
p.setOngoing("Attract new customers and boost loyalty with MasterCard PriceAssure, a unique card loyalty solution that helps cardholders get the best possible prices on airfare. A valuable tool for MasterCard cardholders, PriceAssure allows them to book travel with confidence in a volatile pricing environment.<br><br>MasterCard PriceAssure is available for all Small Business, Credit, and Debit products.");
cal1.addPromo(p);

var p = new Promo('Priceless New York');
p.setLink('/portal/program/1c3/Priceless-New-York');
p.setOngoing("MasterCard’s Priceless New York appeals to affluent cardholders by providing special experiences and privileged access in the world’s greatest cities. With guided tours of FAO Schwarz, sleepovers at the Bronx Zoo, and much more, Priceless New York gives cardholders their own key to the city.<br><br>MasterCard Priceless New York is available for Credit World Elite Products.");
cal1.addPromo(p);

// Marketing Programs
cal1.addCategory(cat3);

var p = new Promo('SU2C Restaurant');
p.setLink('https://optimization.mastercard.com/portal/program/1bb/Stand-Up-To-Cancer-with-MasterCard');
p.setOrderPeriod('20120420', '20120630');
p.setInMarket('20120710', '20120928', "<strong style='font-weight:bold;'>Stand Up To Cancer Restaurant</strong> <br /> <br />Help your MasterCard cardholders support cancer research simply by using their MasterCard card.<br /><br />Stand Up To Cancer with MasterCard is available for all Small Business, Credit, Debit, and Prepaid products.");
cal1.addPromo(p);

var p = new Promo('SU2C eCommerce');
p.setLink('https://optimization.mastercard.com/portal/program/1bb/Stand-Up-To-Cancer-with-MasterCard');
p.setOrderPeriod('20120801', '20120831');
p.setInMarket('20121105', '20121219', "<strong style='font-weight:bold;'>Stand Up To Cancer eCommerce</strong> <br /> <br />Help your MasterCard cardholders support cancer research simply by using their MasterCard card. Check back soon for more information.<br /><br />Stand Up To Cancer with MasterCard is available for all Small Business, Credit, Debit, and Prepaid products.");
cal1.addPromo(p);

var p = new Promo('Q3 MLB&trade; World Series&reg;');
p.setLink('https://optimization.mastercard.com/portal/program/42d1/2012-Q3-MasterCard-MLB-World-Series-Promotion');
p.setOrderPeriod('20120215', '20120330');
p.setInMarket('20120701', '20120901', "Every time your cardholders use their MasterCard cards between July 1 and September 1, 2012, they’ll automatically be entered for a chance to win a 3-day/2-night trip for four to the 2012 MLB&trade; World Series&reg;. <Br><br>2012 MLB&trade; World Series&reg; Promotion is available for all Small Business, Credit, Debit, and Prepaid products.");
cal1.addPromo(p);

var p = new Promo('Travel Program');
//p.setLink('');
p.setInMarket('20120401', '20121231', "Help drive preference and usage amongst domestic and cross-border affluent travelers with the MasterCard&reg; 2012 Travel Program. Target cardholders with compelling offers from select merchants beginning this Spring.  Check back soon for more information.");
cal1.addPromo(p);

var p = new Promo('2012 GRAMMY&reg; Awards');
p.setLink('/portal/program/f1/2012-MasterCard-GRAMMY-Awards-Promotion');
p.setOrderPeriod('20120515', '20120630');
p.setInMarket('20120915', '20121115', "With this exciting MasterCard promotion, your cardholders will have the chance to win a VIP Grammy Awards Experience! One lucky winner and a guest will have the chance to see their favorite musical artists on the red carpet and hear them perform live at the Grammy Awards! Check back soon for more information.<Br><br>2012 MasterCard GRAMMY&reg; Awards Promotion is available for all Small Business, Credit, Debit, and Prepaid products.");
cal1.addPromo(p);

var p = new Promo('&ldquo;Something for the Fans&rdquo;');
p.setLink('https://optimization.mastercard.com/portal/program/43e6/Something-for-the-Fans-Giveaway-Sweepstakes');
p.setInMarket('20120201', '20121231', "Promoted via www.facebook.com/mastercard, the &ldquo;Something for the Fans Giveaway&rdquo; sweepstakes will run from February 1, 2012 through December 31, 2012.<br><br>&ldquo;Something for the Fans Giveaway&rdquo; Sweepstakes is available for all Small Business, Credit, Debit, and Prepaid products.");
cal1.addPromo(p);



var p = new Promo('Category Expansion Incentives');
p.setLink('/portal/program/8d9/Category-Expansion');
p.setOngoing("Designed to help increase card spend in high-frequency categories, our category expansion incentives include direct marketing programs targeting cardholders who do not currently spend or who have minimal spend in everyday spend categories such as casual dining restaurants, grocery, gas, and pharmacy. Cardholders are offered an incentive to make at least three transactions in the targeted category during the promotion period.<br><br>Category Expansion Incentives are available for all Small Business, Credit, and Debit products.");
cal1.addPromo(p);

var p = new Promo('Recurring Payments Incentives');
p.setLink('/portal/program/8d8/Recurring-Payments');
p.setOngoing("Help increase activation and spend on recurring payment transactions with this direct marketing program, which offers cardholders incentives such as statement credit, miles, or points for making recurring payments.<br><br>Recurring Payments Incentives are available for all Small Business, Credit, and Debit products.");
cal1.addPromo(p);

var p = new Promo('Tax Payments');
p.setLink('/portal/program/1c0/MasterCard-Tax-Payment-Program');
p.setOngoing("Enhance cardholder relationships with a comprehensive set of marketing materials to help you promote card usage for federal tax payments. Cardholders receive a lower convenience fee of just 1.95 percent on consumer and business tax payments when they pay through Value Payments. MasterCard customized tax statement inserts are designed to inform cardholders about the ease of paying taxes with a MasterCard card while increasing usage and transactions in the tax payment category.<br><br>Tax Payment Tools are available for all Credit, Debit, and Prepaid products.");
cal1.addPromo(p);


// Experiences and Offers
cal1.addCategory(cat4);

var p = new Promo('MasterCard Easy Savings&trade;');
p.setLink('/portal/program/8f1/The-MasterCard-Easy-Savings-Program');
p.setOngoing("Your business cardholders can enjoy automatic rebates at great merchant partners across the travel, entertainment, and business tools categories. This program is easy to use, and the rebate savings add up automatically. Cardholders like that the savings are easy to see on their monthly statements or online.<br><br>MasterCard Easy Savings are available for all Small Business products.");
cal1.addPromo(p);

var p = new Promo('MasterCard World Elite');
p.setLink('/portal/program/425a/MasterCard-World-Elite');
p.setOngoing("Stop by and check out the newly refreshed World Elite card. With a new premium brand mark, a new print and online advertising campaign, and new turnkey marketing materials, plus an updated Website, the refreshed World Elite card is sure to delight high-spending account holders.<br><br>MasterCard World Elite is available for Credit World Elite Products.");
cal1.addPromo(p);


// CATEGORY GROUP 4 -- Retention
cal1.addCategoryGroup(catg3);


// Turnkey marketing materials
cal1.addCategory(cat6);

var p = new Promo('Consumer Education');
p.setLink('/portal/program/1c1/MasterCard-Consumer-Credit-Education');
p.setOngoing("MasterCard has developed segment-specific education that targets the new to credit, those with damaged credit, and young adults. These ready-to-use educational materials and complementary statement inserts help encourage responsible spending, saving, and borrowing behavior for your cardholders.<br><br>Consumer Education is available for all Credit products.");
cal1.addPromo(p);

cal1.buildCalendar();