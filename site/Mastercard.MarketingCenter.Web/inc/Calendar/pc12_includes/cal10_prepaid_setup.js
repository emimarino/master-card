// PREPAID
var cal1 = new MarketingCalendar('Calendar1', 2010);
var catg1 = new CategoryGroup('Acquisition');
var cat1 = new Category('Turnkey Marketing Materials');
var cat2 = new Category('Marketing Programs');

var catg2 = new CategoryGroup('Usage');
var cat3 = new Category('Turnkey Marketing Materials');
var cat4 = new Category('Marketing Programs');
var cat5 = new Category('Experiences and Offers');
var cat6 = new Category('Turnkey Marketing Assets');

var catg3 = new CategoryGroup('Retention');
var cat7 = new Category('Turnkey Marketing Materials');


// CATEGORY GROUP 1 -- Acquisition
cal1.addCategoryGroup(catg1);


// Turnkey Marketing Materials
cal1.addCategory(cat1);


var p  = new Promo('Consumer Toolkit');
p.setLink('');
p.setOngoing("Consumers are seeking the convenience and security that only prepaid cards from MasterCard can provide. Given the increasing popularity of gift, travel, and reloadable cards in today&rsquo;s society, now is clearly the time to capitalize on the burgeoning demand for these products. The MasterCard&reg; Prepaid Toolkit offers a comprehensive collection of consumer marketing and program materials to help you establish and enhance your prepaid card portfolios. The toolkit includes a wide range of materials including statement inserts, door clings, and more. These materials are available free-of-charge as a benefit of your relationship with MasterCard.");
cal1.addPromo(p);

var p  = new Promo('Commercial Toolkit');
p.setLink('');
p.setOngoing("Whether the need is for a large market, middle market, small business, or public sector payment solution, MasterCard offers a Commercial Prepaid Solution to bring speed and efficiency through the benefits of electronic payments. The MasterCard&reg; Prepaid Toolkit offers a comprehensive collection of commercial marketing and program materials to help you establish and enhance your prepaid card portfolios. The toolkit includes a wide range of materials including statement inserts, door clings, and more. These materials are available free-of-charge as a benefit of your relationship with MasterCard.");
cal1.addPromo(p);

var p  = new Promo('Consumer Materials');
p.setLink('');
p.setOngoing("MasterCard offers robust prepaid card consumer education and awareness materials to help unbanked and underbanked consumers understand the benefits of a reloadable prepaid card.  Educating consumers on these benefits help consumers understand the value of prepaid cards as well as change misperceptions. &ldquo;Prepaid for everyday transactions&rdquo; and &ldquo;Choosing your card and understanding fees&rdquo; are the first two volumes available of what will be six volumes of education and awareness materials.");
cal1.addPromo(p);


// Marketing Programs
cal1.addCategory(cat2);

var p = new Promo('Priceless Partnership');
p.setLink('/portal/marketing/pricelesspromo');
p.setOrderPeriod('20101010', '20101031');
//p.setInMarket('20110101', '20110331', "This program is designed to give employees further incentive to sell new MasterCard products. Every time a participating branch or call center employee submits a validated application for a new MasterCard product, he/she receives an online Instant Win Game Play. This promotion runs quarterly, with each quarter featuring its own unique theme, prize structure, and chances to win. A variety of marketing materials are available.<Br><br> For more information on the Priceless Partnership&trade; Program, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p  = new Promo('Employee Training');
p.setLink('');
p.setOrderPeriod('20100101', '20100131');
p.setInMarket('20100401', '20100630', "This program is designed to educate and train employees on the benefits of MasterCard products. These self-paced modules offer a combination of standard and customized content sections. Each module focuses on MasterCard benefits, card benefits, issuer benefits, and frequently asked questions and answers. The training module is also integrated with the Priceless Partnership&trade; incentive module.<br><br>For more information on the Priceless Partnership&trade; Program, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

cal1.addCategoryGroup(catg2);
// Turnkey Marketing Materials
cal1.addCategory(cat3);

var p = new Promo('Money Manager Tool&trade;');
p.setLink('');
p.setOngoing("Enhance cardholder relationships by offering them access to an easy-to-use financial management tool that enables cardholders to review and track their spending on their MasterCard card. Cardholders can easily establish a budget and track spending against that budget, create customized categories, track by merchant, and identify spending patterns. Sign up for MasterCard Money Manager&trade; and inform cardholders of tool availability and benefits with turnkey marketing materials. Materials include statement inserts, brochures, customer service frequently asked questions, Web banners, statement messaging, and promotional e-mails. (Turnkey marketing materials available for Debit only. Materials can be modified for Credit, Small Business, and Prepaid cards).<br><Br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

//var p = new Promo('Tax Payments Program');
//p.setLink('http://mcpromotions.agendanyc.com/info.jsp?fileName=tax_payment_program.html&menu=newgrowth.html');
//p.setOngoing("Enhance cardholder relationships with a comprehensive set of marketing materials to help you promote card usage for federal tax payments. Cardholders receive a lower convenience fee of 1.95 % on consumer and business tax payments when they pay through Value Payments. MasterCard customized tax statement inserts are designed to inform cardholders about the ease of paying taxes with a MasterCard card while increasing usage and transactions in the tax payment category.");
//cal1.addPromo(p);

// Marketing Programs
cal1.addCategory(cat4);

var p = new Promo('Baseball Sweepstakes');
p.setLink('');
p.setOrderPeriod('20101101', '20110315');
//p.setInMarket('20110201', '20110315', "MLB&trade; Opening Day is a tradition for many baseball fans, marking the beginning of a new season. MasterCard starts the year with an exciting Major League Baseball<sup>&reg;</sup> promotion that will run February 1 through March 15, giving your cardholders the chance to win a trip for four to a 2011 MLB&trade; Opening Day game of the winner�s choice. Opening Day marks a new beginning, and for four lucky MasterCard<sup>&reg;</sup> cardholders, sitting in the stands to celebrate the start of a new season with their favorite team is priceless.<br><Br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p = new Promo('City Experience Sweepstakes');
p.setLink('');
p.setOrderPeriod('20100201', '20100514');
p.setInMarket('20100515', '20100630', "Help drive activation and cardholder usage with this exciting promotion! MasterCard is giving two cardholders the chance to win a trip for an ultimate weekend getaway to a selected city! Plus, each winner gets to bring three guests! <br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p = new Promo('MLB&trade; World Series');
p.setLink('');
p.setOrderPeriod('20100503', '20100731');
p.setInMarket('20100801', '20100915', "MasterCard cardholders have the chance to win a trip for four to the 2010 MLB&trade; World Series<sup>&reg;</sup> game. Every time your cardholders use their MasterCard cards between August 1 and September 15, 2010, they'll automatically be entered for a chance to win a 3-day/2-night trip to a 2010 MLB&trade; World Series<sup>&reg;</sup> game.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p = new Promo('GRAMMY Awards');
p.setLink('/portal/marketing/grammy-awards');
p.setOrderPeriod('20100702', '20101130');
p.setInMarket('20101001', '20101130', "With this exciting MasterCard promotion, your cardholders will have the chance to win a VIP Grammy Awards Experience! One lucky winner and a guest will have the chance to see their favorite musical artists on the red carpet and hear them perform live at the Grammy Awards!<Br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

var p = new Promo('National Bill Payment');
p.setLink('/portal/marketing/natl-pymt');
p.setOrderPeriod('20100701', '20101031');
p.setInMarket('20100901', '20101130', "Use your MasterCard debit or credit card to pay your bills, especially your telephone, cable, and insurance bills and you&rsquo;ll be automatically entered for a  chance to win $60,000.");
cal1.addPromo(p);

var p = new Promo('Dream Big, Live Large');
p.setLink('/portal/marketing/dream-big');
p.setOrderPeriod('20100801', '20100915');
p.setInMarket('20100915', '20101031', "When it comes to prizes, cardholders may have different wants. Whether it&rsquo;s a trip to visit family or updating their living room with the most current electronics &ndash; this sweespstakes encourages cardholders to dream about the prize that is most significant to them. Encourage cardholders to increase the frequency of transactions on their card and display your brand identity prominitely in association with this sweepstakes!");
cal1.addPromo(p);

// Experiences and Offers
cal1.addCategory(cat5);

var p = new Promo('MasterCard Marketplace&trade;');
p.setLink('/portal/marketing/marketplace');
p.setOngoing("MasterCard&rsquo;s successful Savings program motivates acquisitions and card usage frequency among your signature debit and prepaid cardholders. The program rewards everyday spending at virtually no cost to issuers. This complimentary turnkey offers program provides your MasterCard debit and prepaid cardholders with hundreds of offers each week from merchants nationwide&mdash;a continual stream of weekly and limited-time discounts ranging from 5% to 50%!");
cal1.addPromo(p);

//var p  = new Promo('Overwhelming Offers');
//p.setLink('');
//p.setOngoing("OO&rsquo;s may increase Credit, Debit, and Prepaid cardholder engagement with daily deals of more than 50% off top brand products, Limited quantities available and limited time offers. See offer details for more information. Reservation required.");
//cal1.addPromo(p);

var p  = new Promo('Valentine&rsquo;s Day Promotion');
p.setLink('/portal/marketing/marketplace');
p.setOrderPeriod('20091224', '20100214');
p.setInMarket('20091224', '20100214', "Encourage MasterCard Credit, Debit, and Prepaid usage and savings with Valentine&rsquo;s Day themed emails and web banners.");
cal1.addPromo(p);

var p  = new Promo('Mother&rsquo;s Day Promotion');
p.setLink('/portal/marketing/marketplace');
p.setOrderPeriod('20100402', '20100509');
p.setInMarket('20100402', '20100509', "Encourage MasterCard Credit, Debit, and Prepaid usage and savings with Mother&rsquo;s Day themed emails and web banners.");
cal1.addPromo(p);

var p  = new Promo('Dads & Grads Promotion');
p.setLink('/portal/marketing/marketplace');
p.setOrderPeriod('20100517', '20100621');
p.setInMarket('20100517', '20100621', "Encourage MasterCard Credit, Debit, and Prepaid usage and savings with Father&rsquo;s Day and Graduation themed emails and web banners.");
cal1.addPromo(p);

// Turnkey Marketing Assets
cal1.addCategory(cat6);

//var p = new Promo('U.S. Sponsorships Portfolio');
var p = new Promo('US Sponsorships Portfolio');
p.setLink('');
p.setOngoing("Enhance cardholder loyalty by building custom programs choosing from existing MasterCard sponsorship portfolio. (Some restrictions apply).<br><Br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

cal1.addCategoryGroup(catg3);
// Turnkey marketing materials
cal1.addCategory(cat7);

var p = new Promo('Consumer Education');
p.setLink('');
p.setOngoing("MasterCard has developed education programs that are specifically designed to help consumers develop sound money management skills. These ready-to-use education sheets and complementing statement inserts encourage responsible spending, saving, and borrowing behavior with your cardholders.<br><br>For more information, please contact your MasterCard Account Representative.");
cal1.addPromo(p);

cal1.buildCalendar();