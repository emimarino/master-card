// PREPAID
var cal1 = new MarketingCalendar('Calendar1', 2009);
var catg1 = new CategoryGroup('Promotional Programs');
var cat1 = new Category('Mass Usage Promotions');
var cat2 = new Category('MasterCard Savings Program');

var catg2 = new CategoryGroup('Product Messaging');
var cat3 = new Category('Consumer Prepaid Awareness');
var cat5 = new Category('Priceless Marketing Collateral');
var cat6 = new Category('Prepaid Consumer Education & Awareness Collateral');



// CATEGORY GROUP 1 -- PROMOTIONAL PROGRAMS
cal1.addCategoryGroup(catg1);


// MASS USAGE PROMOTIONS
cal1.addCategory(cat1);

var p  = new Promo('Opening Day Sweepstakes');
p.setLink('');
p.setOrderPeriod('20081117', '20090130');
p.setInMarket('20090201', '20090315', "Motivate your cardholders to use their cards! MasterCard starts the year with an exciting Major League Baseball&reg; promotion that gives your cardholders the chance to win a trip for four to a 2009 MLB&trade; Opening Day game of the winner&rsquo;s choice. Opening Day marks a new beginning, and for four lucky MasterCard&reg; cardholders, celebrating the start of a new season will be priceless.");
cal1.addPromo(p);

var p  = new Promo('NYC Experience');
p.setLink('');
p.setOrderPeriod('20090302', '20090401');
p.setInMarket('20090515', '20090630', "MasterCard will be developing a usage promotion in the entertainment space. Theme to be announced.");
cal1.addPromo(p);

var p  = new Promo('MLB World Series');
p.setLink('');
p.setOrderPeriod('20090515', '20090615');
p.setInMarket('20090801', '20090915', "MasterCard is giving one cardholder and three guests the chance to watch history in the making. Every time your cardholders use their MasterCard cards between August 1 and September 15, 2009, they&rsquo;ll automatically be entered for a chance to win a 3-day/2-night trip to a 2009 MLB&trade; World Series&reg; game.");
cal1.addPromo(p);

var p  = new Promo('GRAMMY Awards');
p.setLink('info.jsp?fileName=grammys.html&menu=newgrowth.html');
p.setOrderPeriod('20090728', '20091014');
p.setInMarket('20091015', '20091215', "MasterCard has developed an exciting promotion designed to help your issuers grow their business by driving cardholder card usage during the October&ndash;December shopping period. Leveraging the strength of the Priceless brand, this promotion offers cardholders a chance to win a truly exciting, unique VIP experience&ndash;an opportunity to rub shoulders with some of the most famous names in the music business at their biggest celebration of the year!<br><br>Two lucky Grand Prize winners and a guest will each receive a complete GRAMMY Awards VIP experience, from a backstage tour during rehearsals to the red carpet and the GRAMMY Awards show. Plus, the winners will each receive a gift card to spend on clothes, a makeover, or whatever they choose!");
cal1.addPromo(p);

var p  = new Promo('Summer Promotion');
p.setLink('');
p.setOrderPeriod('20090309', '20090415');
p.setInMarket('20090701', '20090831', "Capture incremental spend in the peak summer season. MasterCard will provide tools to help you to drive usage. Double entries will be awarded for tapping with MasterCard PayPass&reg;.");
cal1.addPromo(p);

var p  = new Promo('Arnold Palmer Invitational');
p.setLink('info.jsp?fileName=2009_arnold_palmer.html&menu=newgrowth.html');
p.setOrderPeriod('20091001', '20091115');
//p.setInMarket('20100102', '20100215', "Your cardholders won't need to find a big screen to watch their favorite pro at the 2010 Arnold Palmer Invitational presented by MasterCard&mdash;we'll help them see their favorite pro sink a hole-in-one in person.");
cal1.addPromo(p);

//var p  = new Promo('Holiday Promotion');
//p.setLink('info.jsp?fileName=holiday_promo.html&menu=newgrowth.html');
//p.setOrderPeriodText('20090830', '20090930','Holiday Program Details Coming Soon!');
//p.setInMarket('20091116', '20091223', "Holiday Program Details Coming Soon!");
//cal1.addPromo(p);


// MERCHANT OFFERINGS
cal1.addCategory(cat2);

var p  = new Promo('MasterCard Savings');
p.setLink('');
p.setOngoing("MasterCard&rsquo;s successful Savings program motivates acquisitions and card usage frequency among your signature debit and prepaid cardholders. The program rewards everyday spending at virtually no cost to issuers. This complimentary turnkey offers program provides your MasterCard debit and prepaid cardholders with hundreds of offers each week from merchants nationwide&mdash;a continual stream of weekly and limited-time discounts ranging from 5% to 50%!");
cal1.addPromo(p);

var p  = new Promo('Dads and Grads');
p.setLink('');
p.setOrderPeriod('20090501', '20090615');
p.setInMarket('20090501', '20090615', "Our current Dads and Grads promotion encourages your cardholders to save while purchasing gifts for the dads and grads in their lives.");
cal1.addPromo(p);

var p  = new Promo('Summer Savings');
p.setLink('');
p.setOrderPeriod('20090601', '20090830');
p.setInMarket('20090601', '20090830', "This summertime promotion is designed to increase the everyday purchases cardholders make during the summer. ");
cal1.addPromo(p);

var p  = new Promo('Back to School');
p.setLink('');
p.setOrderPeriod('20090701', '20090915');
p.setInMarket('20090701', '20090915', "Encourage MasterCard Debit and Prepaid usage AND savings with Back to school themed emails and web banners.");
cal1.addPromo(p);

var p  = new Promo('Overwhelming Offers');
p.setLink('');
p.setOrderPeriod('20090921', '20091031');
p.setInMarket('20090921', '20091031', "The MasterCard Savings program will be providing deep discounts on a variety of items in numerous categories for MasterCard Debit and Prepaid cardholders. More details coming soon.");
cal1.addPromo(p);

var p  = new Promo('Savings Holiday Promotion');
p.setLink('');
p.setOrderPeriod('20091015', '20100102');
p.setInMarket('20091015', '20100102', "Encourage MasterCard Debit and Prepaid cardholders to save while shopping this holiday season. Winter themed emails and banners will be available for customization.");
cal1.addPromo(p);

//CATEGORY GROUP 2 -- PRODUCT MESSAGING
cal1.addCategoryGroup(catg2);

// CONSUMER PREPAID AWARENESS
cal1.addCategory(cat3);

var p  = new Promo('Consumer Toolkit');
p.setLink('');
p.setOngoing("Consumers are seeking the convenience and security that only prepaid cards from MasterCard can provide. Given the increasing popularity of gift, travel, and reloadable cards in today&rsquo;s society, now is clearly the time to capitalize on the burgeoning demand for these products. The MasterCard&reg; Prepaid Toolkit offers a comprehensive collection of consumer marketing and program materials to help you establish and enhance your prepaid card portfolios. The toolkit includes a wide range of materials including statement inserts, door clings, and more. These materials are available free-of-charge as a benefit of your relationship with MasterCard.");
cal1.addPromo(p);


//PRICELESS MARKETING COLLATERAL
cal1.addCategory(cat5);

var p  = new Promo('Packaging/ Branch Materials');
p.setLink('');
p.setOngoing("Prepaid Priceless packaging is available in 3 versions: Gift, Birthday, and Holiday. Additional Priceless Prepaid marketing collateral includes: Posters, Tent Card, J-Hooks, Door Cling, Dangler, and Mobile.");
cal1.addPromo(p);


//PREPAID CONSUMER EDUCATION & AWARENESS COLLATERAL
cal1.addCategory(cat6);

var p  = new Promo('Consumer Materials');
p.setLink('');
p.setOngoing("MasterCard offers robust prepaid card consumer education and awareness materials to help unbanked and underbanked consumers understand the benefits of a reloadable prepaid card.  Educating consumers on these benefits help consumers understand the value of prepaid cards as well as change misperceptions. &ldquo;Prepaid for everyday transactions&rdquo; and &ldquo;Choosing your card and understanding fees&rdquo; are the first two volumes available of what will be six volumes of education and awareness materials.");
cal1.addPromo(p);



cal1.buildCalendar();