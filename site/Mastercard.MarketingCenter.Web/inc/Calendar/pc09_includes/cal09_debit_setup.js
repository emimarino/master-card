// DEBIT
var cal1 = new MarketingCalendar('Calendar1', 2009);
var catg1 = new CategoryGroup('Promotional Programs');
var cat3 = new Category('Mass Usage Promotions');
var cat6 = new Category('MasterCard Savings Program');

var catg2 = new CategoryGroup('Product Messaging');
var cat7 = new Category('<em>PayPass&reg;</em> Awareness');
var cat8 = new Category('Debit Penetration');
var cat9 = new Category('Category Expansion');

var catg4 = new CategoryGroup('Sales Tools');
var cat13 = new Category('Employee Training');


// CATEGORY GROUP 1 -- PROMOTIONAL PROGRAMS
cal1.addCategoryGroup(catg1);


// MASS USAGE PROGRAMS
cal1.addCategory(cat3);

var p  = new Promo('Opening Day Sweepstakes');
p.setLink('');
p.setOrderPeriod('20081117', '20090130');
p.setInMarket('20090201', '20090315', "Motivate your cardholders to use their cards! MasterCard starts the year with an exciting Major League Baseball&reg; promotion that gives your cardholders the chance to win a trip for four to a 2009 MLB&trade; Opening Day game of the winner&rsquo;s choice. Opening Day marks a new beginning, and for four lucky MasterCard&reg; cardholders, celebrating the start of a new season will be priceless.");
cal1.addPromo(p);

var p  = new Promo('NYC Experience');
p.setLink('');
p.setOrderPeriod('20090302', '20090401');
p.setInMarket('20090515', '20090630', "MasterCard will be developing a usage promotion in the entertainment space. Theme to be announced.");
cal1.addPromo(p);

var p  = new Promo('MLB World Series');
p.setLink('');
p.setOrderPeriod('20090515', '20090615');
p.setInMarket('20090801', '20090915', "MasterCard is giving one cardholder and three guests the chance to watch history in the making. Every time your cardholders use their MasterCard cards between August 1 and September 15, 2009, they&rsquo;ll automatically be entered for a chance to win a 3-day/2-night trip to a 2009 MLB&trade; World Series&reg; game.");
cal1.addPromo(p);

var p  = new Promo('GRAMMY Awards');
p.setLink('info.jsp?fileName=grammys.html&menu=newgrowth.html');
p.setOrderPeriod('20090728', '20091014');
p.setInMarket('20091015', '20091215', "MasterCard has developed an exciting promotion designed to help your issuers grow their business by driving cardholder card usage during the October&ndash;December shopping period. Leveraging the strength of the Priceless brand, this promotion offers cardholders a chance to win a truly exciting, unique VIP experience&ndash;an opportunity to rub shoulders with some of the most famous names in the music business at their biggest celebration of the year!<br><br>Two lucky Grand Prize winners and a guest will each receive a complete GRAMMY Awards VIP experience, from a backstage tour during rehearsals to the red carpet and the GRAMMY Awards show. Plus, the winners will each receive a gift card to spend on clothes, a makeover, or whatever they choose!");
cal1.addPromo(p);

var p  = new Promo('Summer Promotion');
p.setLink('');
p.setOrderPeriod('20090309', '20090415');
p.setInMarket('20090701', '20090831', "Capture incremental spend in the peak summer season. MasterCard will provide tools to help you to drive usage. Double entries will be awarded for tapping with MasterCard PayPass&reg;.");
cal1.addPromo(p);

var p  = new Promo('Arnold Palmer Invitational');
p.setLink('info.jsp?fileName=2009_arnold_palmer.html&menu=newgrowth.html');
p.setOrderPeriod('20091001', '20091115');
//p.setInMarket('20100102', '20100215', "Your cardholders won't need to find a big screen to watch their favorite pro at the 2010 Arnold Palmer Invitational presented by MasterCard&mdash;we'll help them see their favorite pro sink a hole-in-one in person.");
cal1.addPromo(p);

//var p  = new Promo('Holiday Promotion');
//p.setLink('info.jsp?fileName=holiday_promo.html&menu=newgrowth.html');
//p.setOrderPeriodText('20090830', '20090930','Holiday Program Details Coming Soon!');
//p.setInMarket('20091116', '20091223', "Holiday Program Details Coming Soon!");
//cal1.addPromo(p);


// MERCHANT OFFERINGS
cal1.addCategory(cat6);

//var p  = new Promo('Merchant Offers');
//p.setLink('');
//p.setOngoing("Merchant offers will be available throughout the year to help support your acquisition, activation, and usage strategies.");
//cal1.addPromo(p);


var p  = new Promo('MasterCard Savings');
p.setLink('');
p.setOngoing("MasterCard&rsquo;s successful Savings program motivates acquisitions and card usage frequency among your signature debit and prepaid cardholders. The program rewards everyday spending at virtually no cost to issuers. This complimentary turnkey offers program provides your MasterCard debit and prepaid cardholders with hundreds of offers each week from merchants nationwide&mdash;a continual stream of weekly and limited-time discounts ranging from 5% to 50%!");
cal1.addPromo(p);

var p  = new Promo('Dads and Grads');
p.setLink('');
p.setOrderPeriod('20090501', '20090615');
p.setInMarket('20090501', '20090615', "Our current Dads and Grads promotion encourages your cardholders to save while purchasing gifts for the dads and grads in their lives.");
cal1.addPromo(p);

var p  = new Promo('Summer Savings');
p.setLink('');
p.setOrderPeriod('20090601', '20090830');
p.setInMarket('20090601', '20090830', "This summertime promotion is designed to increase the everyday purchases cardholders make during the summer. ");
cal1.addPromo(p);

var p  = new Promo('Back to School');
p.setLink('');
p.setOrderPeriod('20090701', '20090915');
p.setInMarket('20090701', '20090915', "Encourage MasterCard Debit and Prepaid usage AND savings with Back to school themed emails and web banners.");
cal1.addPromo(p);

var p  = new Promo('Overwhelming Offers');
p.setLink('');
p.setOrderPeriod('20090921', '20091031');
p.setInMarket('20090921', '20091031', "The MasterCard Savings program will be providing deep discounts on a variety of items in numerous categories for MasterCard Debit and Prepaid cardholders. More details coming soon.");
cal1.addPromo(p);

var p  = new Promo('Savings Holiday Promotion');
p.setLink('');
p.setOrderPeriod('20091015', '20100102');
p.setInMarket('20091015', '20100102', "Encourage MasterCard Debit and Prepaid cardholders to save while shopping this holiday season. Winter themed emails and banners will be available for customization.");
cal1.addPromo(p);


//CATEGORY GROUP 2 -- PRODUCT MESSAGING
cal1.addCategoryGroup(catg2);

//// PAYPASS AWARENESS
//cal1.addCategory(cat7);

//var p  = new Promo('Direct Mail, Inserts');
//p.setLink('');
//p.setOngoing("Educate cardholders on MasterCard <i>PayPass</i>&reg; to help drive card activation and usage. Marketing materials available are Direct Mail and Statement Inserts.<br><br>Contact your MasterCard Relationship Manager.<br><br>To read more about MasterCard <i>PayPass</i>&reg; please visit www.mastercardonline.com.");
//cal1.addPromo(p);

//DEBIT PENETRATION
cal1.addCategory(cat8);

//var p  = new Promo('Convenience, Flexibility');
//p.setLink('');
//p.setOngoing("<i>Versions Available: Hispanic, Generic, Rewards</i><br>MasterCard customized tax statement inserts are designed to inform cardholders about the ease of paying taxes with a MasterCard card while increasing usage and transactions in the tax payment category. These turnkey inserts, which are available in English and Spanish for both the generic and rewards version, may be utilized by credit, debit, and business portfolios. Available as art on disk.<br><br>Contact your MasterCard Relationship Manager.");
//cal1.addPromo(p);

var p  = new Promo('Themed Statement');
p.setLink('');
p.setOngoing("Educate cardholders on the features and benefits of the Debit MasterCard card to help drive card activation and usage. Multiple messaging options create awareness of the benefits so they will begin to use their debit cards to pay for everyday purchases. Marketing materials available are Statement Inserts.");
cal1.addPromo(p);
//CATEGORY EXPANSION
cal1.addCategory(cat9);

var p  = new Promo('Pay Taxes');
p.setLink('');
p.setOngoing("Mastercard Offers is an assortment of compelling cardholder offers for you to mail as printed statement inserts or to incorporate into your own activation, usage, and retention communications.<br><br>Contact your MasterCard Relationship Manager.");
cal1.addPromo(p);

var p  = new Promo('Bill Payment Opportunities');
p.setLink('');
p.setOngoing("<i>Versions available: Generic, Rewards</i><br>MasterCard has developed customized consumer statement inserts designed to inform cardholders about the ease and convenience of signing up for automatic bill payment. It also helps to drive usage and retention for issuers. These turnkey inserts are available in both generic and rewards versions. Available as art on disk.<br><br>Please visit www.mastercardonline.com for more information.");
cal1.addPromo(p);


// CATEGORY GROUP 4 -- SALES TOOLS
cal1.addCategoryGroup(catg4);

// EMPLOYEE INCENTIVE 
cal1.addCategory(cat13);

var p = new Promo('Debit Product');
p.setLink('');
p.setOngoing("Tools available include the Debit MasterCard Branch Trainer&rsquo;s Guide, Employee Guide, and Lay-By-Card with Benefits-At-A-Glance, which is designed to educate employees on the benefits of debit. This allows staff to confidently explain and promote the many uses of debit.");
cal1.addPromo(p);


cal1.buildCalendar();