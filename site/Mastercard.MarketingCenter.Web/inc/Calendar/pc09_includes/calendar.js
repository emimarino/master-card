//TODO: handle overlapping items
//TODO: calculate bubble position/direction

MarketingCalendar = function(id, year) {

	this.id = id;
	this.elem = document.getElementById(this.id);
	this.categories = new Array();
	this.categorygroups = new Array();
	
	this.year = year;
	
	this.currDate = new Date();
	//this.currDate = new Date('03/01/2007 00:00:00'); // TEMP!!
	
	this.startMonth = 9;
	this.startYear = this.year-1;
	
	this.months = new Array('JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC');
	this.numMonths = 15;
	this.errors = '';
	
	this.imgDir = 'images/';
	
	// these must match the stylesheet
	this.titleWidth = 100;
	this.monthWidth = 36;
	this.dayWidth = this.monthWidth/30;
	// this.promoHeight = 47;
	
	this.addCategoryGroup = function(categorygroup) { 
		newCategoryGroup = this.categorygroups.length; // gets the index # for the new category
		this.categorygroups[newCategoryGroup] = categorygroup;
	}
	this.addCategory = function(category) { 
		currentCategoryGroup = this.categorygroups.length-1; // gets the index # for the current category
		newCategory = this.categorygroups[currentCategoryGroup].categories.length; // gets the index # for the new category
		this.categorygroups[currentCategoryGroup].categories[newCategory] = category;
	}
	this.addPromo = function(promo) {
		currentCategory = this.categorygroups[currentCategoryGroup].categories.length-1; // gets the index # for the current category
		newPromo = this.categorygroups[currentCategoryGroup].categories[currentCategory].promos.length; // gets the index for the new promo
		this.categorygroups[currentCategoryGroup].categories[currentCategory].promos[newPromo] = promo;
	}
	this.buildCalendar = function() {
		this.elem.className = 'calendar';
		this.buildHeaders();
		for(var i=0;i<this.categorygroups.length;i++)  this.buildCategoryGroup(this.categorygroups[i]);
		//for(var i=0;i<this.categories.length;i++)  this.buildCategory(this.categories[i]);
		if(this.errors != '') alert(this.errors);
		this.buildBubble();
	}
	this.buildHeaders = function() {

		// Create the header block
		headerElem = document.createElement('div');
		headerElem.className = 'header row';
		
		// Create the title cell and add it to the item
		headerElem.appendChild(this.buildTitle('Category'));
		
		// Loop through the months and add a header cell for each
		for(var i = 0;i<this.numMonths;i++) headerElem.appendChild(this.buildMonth(i, true));
		
		// Add it to the calendar
		this.elem.appendChild(headerElem);
	}
	this.buildBubble = function() {
		var bubbleElem = document.createElement('div');
		bubbleElem.id = this.id + 'Bubble';
		bubbleElem.className = 'bubble';
		this.elem.appendChild(bubbleElem);
	}
	this.buildCategoryGroup = function(categorygroup) {
		
		// Create a category row
		var categorygroupElem = document.createElement('div');
		categorygroupElem.className = 'categorygroup row';
		
		//categorygroupElem.style.left = '200px';
		//categorygroupElem.className = 'categorygroup ongoinggroup';
		
		// Create the title cell and add it to the category
		categorygroupElem.appendChild(this.buildGroupTitle(categorygroup.grouptitle));
		
		// Loop through the months and add a cell for each
		//for(var i = 0;i<this.numMonths;i++) categorygroupElem.appendChild(this.buildMonth(i, false));
		// Add it to the calendar
		this.elem.appendChild(categorygroupElem);
		//alert(categorygroup.categories[0].title + ', ' + categorygroup.categories[1].title + ', ' + categorygroup.categories[2].title);
		
		//alert(categorygroup.categories.length);
		
		// Loop through the category's promos and add them
		for(var i=0;i<categorygroup.categories.length;i++) this.elem.appendChild(this.buildCategory(categorygroup.categories[i]));
		
		// Clear out the category's promos so the next calendar can rebuild them
		categorygroup.categories = new Array();
		
		return categorygroupElem;
	}
	this.buildCategory = function(category) {
		
		// Create a category row
		var categoryElem = document.createElement('div');
		categoryElem.className = 'category row';
		
		// Create the title cell and add it to the category
		categoryElem.appendChild(this.buildCategoryTitle(category.categorytitle));
		
		// Loop through the months and add a cell for each
		//for(var i = 0;i<this.numMonths;i++) categoryElem.appendChild(this.buildMonth(i, false));
		
		// Add it to the calendar
		this.elem.appendChild(categoryElem);
		
		// Loop through the category's promos and add them
		//for(var i=0;i<category.promos.length;i++) this.elem.appendChild(this.buildPromo(category.promos[i]));
		for(var i=0;i<category.promos.length;i++) categoryElem.appendChild(this.buildPromo(category.promos[i]));
		
		// Clear out the category's promos so the next calendar can rebuild them
		category.promos = new Array();
		
		return categoryElem;
	}
	this.buildPromo = function(promo) {
	
		// Create a promo row element
		promoElem = document.createElement('div');
		var title = this.id + '_' + promo.title;
		promoElem.id = title;
		promoElem.className = 'promo row';
		
		// Create the title cell and add it to the promo
		promoElem.appendChild(this.buildTitle(promo.title));
		
		// Loop through the months and add a cell for each
		for(var i = 0;i<this.numMonths;i++) promoElem.appendChild(this.buildMonth(i, false));
		
		// Check if the 'Order Period' start day has passed. If so, add links to this promo's items if a link has been set.
		//if(promo.items[0].type != 'Ongoing' && promo.link) {
		if(promo.link) {
			var setLink = false;
			var start = promo.items[0].start.toString();
			var startYear = start.substr(0,4);
			var startMonth = trimZero(start.substr(4,2));
			var startDay = trimZero(start.substr(6,2));
			var orderStart = new Date(startMonth + '/' + startDay + '/' + startYear + ' 00:00:00');
			if(orderStart <= this.currDate) setLink = true;
		}
		
			
		// Loop through the items and add them
		for(var i = 0;i<promo.items.length;i++) {
			
			// Make shorthand variable
			x = promo.items[i];
			
			// Create the item element
			itemElem = document.createElement('div');
			
			// Highlight the row on mouseover
			if(x.bubble) {
				itemElem.setAttribute('bubble', x.bubble);
				itemElem.onmouseover = function() { 
					document.getElementById(title).className = 'promo row on';
					showBubble(this);
				}
				itemElem.onmouseout  = function() {
					document.getElementById(title).className = 'promo row';
					hideBubble();
				}
			}
			else {
				itemElem.onmouseover = function() { document.getElementById(title).className = 'promo row on'; }
				itemElem.onmouseout  = function() { document.getElementById(title).className = 'promo row'; }
			}
			
			// If this is an ongoing event, do special stuff
			if(x.type == 'Ongoing') {
				itemElem.style.left = this.titleWidth+1  + 'px';
				itemElem.className = 'item ongoing';
			}
			else if (x.type == 'OngoingDates') {
				itemElem.style.left = '420px';
				itemElem.className = 'item ongoingdates';
			}
			else {
				
				// Make sure dates are strings
				var start = x.start.toString();
				var end = x.end.toString();
				
				// Create date objects (might not need to do this)
				var startYear = start.substr(0,4); 
				var startMonth = trimZero(start.substr(4,2)); 
				var startDay = trimZero(start.substr(6,2));
				var startDate = new Date();
				startDate.setFullYear(startYear, startMonth, startDay);
			
				var endYear = end.substr(0,4);
				var endMonth = trimZero(end.substr(4,2));
				var endDay = trimZero(end.substr(6,2));
				var endDate = new Date();
				endDate.setFullYear(endYear, endMonth, endDay);
				
			
				// Add the type and dates to the item as text
				//itemElem.innerHTML = '<i></i><u></u><span><b>' + x.type + '</b>' + startMonth + '/' + startDay + '-' + endMonth + '/' + endDay + '</span>';
				
				// ASL 2009-07-08:
				if (x.type == 'Order Period Text')
				{
					itemElem.innerHTML = '<i></i><u></u><span><b>TBD</b></span>';
				}
				else
				{
					// ASL: This version suppresses the text for OP & IM
					if (startMonth == endMonth)
					{
						itemElem.innerHTML = '<i></i><u></u><span>' + startMonth + '/' + startDay + '-<br>' + endMonth + '/' + endDay + '</span>';
					}
					else
					{
						itemElem.innerHTML = '<i></i><u></u><span>' + startMonth + '/' + startDay + '-' + endMonth + '/' + endDay + '</span>';
					}
				}
				
				// ASL: This version suppresses the dates text from being displayed
				//itemElem.innerHTML = '<i></i><u></u><span><b>' + x.type + '</b></span>';
				
				// Calculate the starting position of the item
				startDay = 1;
				endDay = 31; // round up to the end of the month
				var offset = this.monthWidth*3; // if the first month is from the previous year
				var offset2 = startYear < this.year && this.monthWidth*12; // if an item starts in the previous year
				itemPos = this.titleWidth + ((parseInt(startMonth)-1)*this.monthWidth) + offset + (parseInt(startDay)*this.dayWidth) - offset2;
				if(itemPos < this.titleWidth) itemPos = this.titleWidth;
				itemElem.style.left = itemPos + 'px';
		
				// Calculate the width of the item
				startMonthSpecial = endYear > startYear ? startMonth-12 : startMonth;
				diffMonths = endMonth - startMonthSpecial;
				diffDays = endDay - startDay;
				itemWidth = diffMonths*this.monthWidth + diffDays*this.dayWidth;
				if(itemWidth + itemPos > this.titleWidth + this.monthWidth*this.numMonths) itemWidth = this.titleWidth + this.monthWidth*this.numMonths - itemPos;
				itemElem.style.width = itemWidth + 'px';
		
				// Set classnames based on if the item continutes off the calendar
				if(startYear < this.year) itemElem.className = 'item continueleft';
				else if (endYear > this.year) itemElem.className = 'item continueright';
				else itemElem.className = 'item';
		
				// Add on to the classname depending on what kind of item it is
				switch(x.type) {
					case 'Order Period': itemElem.className = itemElem.className + ' orderperiod'; break;
					// ASL 2009-07-08:
					case 'Order Period Text': itemElem.className = itemElem.className + ' orderperiod'; break;
					case 'In Market': itemElem.className = itemElem.className + ' inmarket'; break;
				}
			
				// Check if the item overlaps another
				for(var j=0;j<promo.items.length;j++) {
					if(j!=i) { // don't compare it to itself 
						if(x.type != 'Ongoing' && x.start < promo.items[j].end && x.end > promo.items[j].start) {
							itemElem.className = itemElem.className + (x.type == 'Order Period' ? ' over' : ' under');
						}
					}
				}
			}
			
			// If setLink is true, create an onclick event
			if(setLink) {
				var link = promo.link;
				itemElem.style.cursor = 'pointer';
				itemElem.className = itemElem.className + ' active';
				
				if(link.indexOf('http:') == -1 || link.indexOf('https:') == -1 )
				{
					itemElem.onclick = function() { window.location=link; }
				}
				if(link.indexOf('http:') > -1 || link.indexOf('https:') > -1 )
				{
					itemElem.onclick = function() { 
						window.open(link,'External_Site'); 
					}
				}
			}
			
			// Add the item to the promo row
			promoElem.appendChild(itemElem);
		}
		
		return promoElem;
	}
	this.buildMonth = function(i, header) {
		// Calculate the month/year
		var tempDate = new Date();
		// ASL: (033109) The next line of code was added to fix a Javascript bug that's triggered 
		// only when today's date is the 31st and the next month only has 30 days in it 
		// (e.g. march 31st -> april, may 31st -> june, august 31st -> september).
		tempDate.setDate(1);
		tempDate.setMonth(this.startMonth);
		tempDate.setFullYear(this.startYear);
		
		var m = tempDate.getMonth()+i;
		if(m >= 12) {
			m = m-12;
			tempDate.setFullYear(2009);
		}
		tempDate.setMonth(m);
		
		// Create the month element
		monthElem = document.createElement('div');
		
		if(tempDate.getMonth() == this.currDate.getMonth() && tempDate.getFullYear() == this.currDate.getFullYear()) monthElem.className = 'month current';
		else if(tempDate.getFullYear() != this.year) monthElem.className = 'month offyear';
		else monthElem.className = 'month';
		
		// If this is the header cell, display the month
		if(header) monthElem.innerHTML = '<span>' + this.months[tempDate.getMonth()] + '</span>';
		
		return monthElem;
	}
	this.buildTitle = function(title) {
		titleElem = document.createElement('div');
		titleElem.className = 'title';
		titleElem.innerHTML = '<span>' + title + '</span>';
		return titleElem;
	}
	this.buildCategoryTitle = function(categorytitle) {
		categorytitleElem = document.createElement('div');
		categorytitleElem.className = 'categorytitle';
		categorytitleElem.innerHTML = categorytitle;
		return categorytitleElem;
	}
	this.buildGroupTitle = function(grouptitle) {
		grouptitleElem = document.createElement('div');
		grouptitleElem.className = 'grouptitle';
		grouptitleElem.innerHTML = grouptitle;
		return grouptitleElem;
	}
}

CategoryGroup = function(grouptitle) {
	this.grouptitle = grouptitle;
	this.categories = new Array();
}
Category = function(categorytitle) {
	this.categorytitle = categorytitle;
	this.promos = new Array();
}
Promo = function(title) {
	this.title = title;
	this.link;
	this.items = new Array();
	
	this.setOrderPeriod = function(start, end) {
		this.items[this.items.length] = new Item('Order Period', start, end);
	}
	//ASL 2009-07-08:
	this.setOrderPeriodText = function(start, end, text) {
		this.items[this.items.length] = new Item('Order Period Text', start, end, text);
	}
	this.setInMarket = function(start, end, bubble) {
		this.items[this.items.length] = new Item('In Market', start, end, bubble);
	}
	this.setOngoing = function(bubble) {
		this.items[this.items.length] = new Item('Ongoing', '20071001', '', bubble);
	}
	//ASL 2007-06-25:
	this.setOngoingDates = function(bubble, start, end) {
		this.items[this.items.length] = new Item('OngoingDates', start, end, bubble);
	}
	this.setLink = function(href) {
		this.link = href;
	}
}
// ASL 2009-07-08:  added "text"
Item = function(type, start, end, bubble, text) {
	this.type = type;
	this.start = start;
	this.end = end;
	this.bubble	= bubble;
	this.text = text;
}

function getCalendarDate(now)
{
   var months = new Array(13);
   months[0]  = "January";
   months[1]  = "February";
   months[2]  = "March";
   months[3]  = "April";
   months[4]  = "May";
   months[5]  = "June";
   months[6]  = "July";
   months[7]  = "August";
   months[8]  = "September";
   months[9]  = "October";
   months[10] = "November";
   months[11] = "December";
   var monthnumber = now.getMonth();
   var monthname   = months[monthnumber];
   var monthday    = now.getDate();
   var year        = now.getYear();
   if(year < 2000) { year = year + 1900; }
   var dateString = monthname +
                    ' ' +
                    monthday +
                    ', ' +
                    year;
					
					var hour   = now.getHours();
   var minute = now.getMinutes();
   var second = now.getSeconds();
   var ap = "AM";
   if (hour   > 11) { ap = "PM";             }
   if (hour   > 12) { hour = hour - 12;      }
   if (hour   == 0) { hour = 12;             }
   if (hour   < 10) { hour   = "0" + hour;   }
   if (minute < 10) { minute = "0" + minute; }
   if (second < 10) { second = "0" + second; }
   var timeString = hour +
                    ':' +
                    minute +
                    ':' +
                    second +
                    " " +
                    ap;
   return dateString + ' ' + timeString;
}

// Bubbles
function showBubble(item) {
	var bubble = document.createElement('div');
	bubble.id = 'CalendarBubble';
	bubble.className = 'block';
	var top = getPosY(item);
	var itemLeft = getPosX(item);
	var left = (itemLeft > 540) ? 540 : itemLeft; 
	
	var text = item.getAttribute('bubble');
	bubble.innerHTML = '<div class="bt"><div></div></div><div class="i1"><div class="i2"><div class="i3">' + text + '</div></div></div><div class="bb"><div></div></div>';
	document.body.appendChild(bubble);
	
	// Get the bubble's height
	bubble.style.visibility = 'hidden';
	var height = bubble.offsetHeight;
	
	// Position the bubble
	bubble.style.top = top - height - 13 + 'px';
	bubble.style.left = left + 'px';
	bubble.style.visibility = 'visible';
	
	// Create the bubble arrow 
	var arrow = document.createElement('div');
	arrow.id = 'CalendarBubbleArrow';
	arrow.style.top = top - 11 + 'px';
	arrow.style.left = itemLeft + 10 + 'px';
	document.body.appendChild(arrow);
}
function hideBubble() {
	document.body.removeChild(document.getElementById('CalendarBubble'));
	document.body.removeChild(document.getElementById('CalendarBubbleArrow'));
}



// Utility functions
function trimZero(n) {
	while (n.substr(0,1) == '0' && n.length>1) { n = n.substr(1,9999); }
	return n;
}

function getPosX(obj) {
	var curleft = 0;
	if (obj.offsetParent){
		while (obj.offsetParent){
			curleft += obj.offsetLeft
			obj = obj.offsetParent;}}
	else if (obj.x) curleft += obj.x;
	return curleft;
}
function getPosY(obj){
	var curtop = 0;
	if (obj.offsetParent){
		while (obj.offsetParent){
			curtop += obj.offsetTop
			obj = obj.offsetParent;}}
	else if (obj.y) curtop += obj.y;
	return curtop;
}