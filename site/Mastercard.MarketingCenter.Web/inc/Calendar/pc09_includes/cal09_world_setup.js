// WORLD
var cal1 = new MarketingCalendar('Calendar1', 2009);
var catg1 = new CategoryGroup('Promotional Programs');
var cat3 = new Category('Mass Usage Promotions');
var cat5 = new Category('Merchant Offerings');

var catg2 = new CategoryGroup('Product Messaging');
var cat6 = new Category('Upgrade Communications');
var cat7 = new Category('Benefits Reinforcement');


// CATEGORY GROUP 1 -- PROMOTIONAL PROGRAMS
cal1.addCategoryGroup(catg1);


// MASS USAGE PROMOTIONS
cal1.addCategory(cat3);

var p  = new Promo('Summer Promotion');
p.setLink('');
p.setOrderPeriod('20090309', '20090415');
p.setInMarket('20090701', '20090831', "Capture incremental spend in the peak summer season. MasterCard will provide tools to help you to drive usage. Double entries will be awarded for tapping with MasterCard PayPass&reg;.");
cal1.addPromo(p);

var p  = new Promo('Arnold Palmer Invitational');
p.setLink('info.jsp?fileName=2009_arnold_palmer.html&menu=newgrowth.html');
p.setOrderPeriod('20091001', '20091115');
//p.setInMarket('20100102', '20100215', "Your cardholders won't need to find a big screen to watch their favorite pro at the 2010 Arnold Palmer Invitational presented by MasterCard&mdash;we'll help them see their favorite pro sink a hole-in-one in person.");
cal1.addPromo(p);

//var p  = new Promo('Holiday Promotion');
//p.setLink('info.jsp?fileName=holiday_promo.html&menu=newgrowth.html');
//p.setOrderPeriodText('20090830', '20090930','Holiday Program Details Coming Soon!');
//p.setInMarket('20091116', '20091223', "Holiday Program Details Coming Soon!");
//cal1.addPromo(p);

// MERCHANT OFFERINGS
cal1.addCategory(cat5);

var p  = new Promo('Experiences and Offers');
p.setLink('');
p.setOngoing("MasterCard Experiences and Offers provide you with a portfolio of flexible assts that can be personalized to fit your cardholder&rsquo;s needs. Enriching experiences, special offers, and preferred access from an exceptional selection of merchants may help you connect with your affluent cardholders.");
cal1.addPromo(p);


// CATEGORY GROUP 2 -- PRODUCT MESSAGING
cal1.addCategoryGroup(catg2);

// UPGRADE COMMUNICATIONS
cal1.addCategory(cat6);

var p  = new Promo('Upgrade Communication');
p.setLink('');
p.setOrderPeriod('20090601', '20091231');
p.setInMarket('20090601', '20091231', "Turnkey and customized materials will be developed to introduce the new World MasterCard value proposition and communicate overall product positioning and enhanced features and benefits. This program is ongoing starting in May 2009. For more information please contact your MasterCard Representative.");
cal1.addPromo(p);


// BENEFITS REINFORCEMENT
cal1.addCategory(cat7);

var p  = new Promo('Travel Benefits, Concierge');
p.setLink('');
p.setOrderPeriod('20090601', '20091231');
p.setInMarket('20090601', '20091231', "Turnkey and customizable materials including inserts, e-mails and copy points/blocks will be developed to reinforce new core product benefits such as World Advisor, World Rewards, Global Protection, and Worldwide Experiences.");
cal1.addPromo(p);




cal1.buildCalendar();
