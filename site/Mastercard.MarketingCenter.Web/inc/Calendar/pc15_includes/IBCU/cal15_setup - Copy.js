﻿var cal1 = new MarketingCalendar('Calendar1', 2014);
var catg1 = new CategoryGroup('Acquisition');
var cat1 = new Category('Turnkey Marketing Materials');

var catg2 = new CategoryGroup('Activation');
var cat7 = new Category('Turnkey Marketing Materials');

var catg3 = new CategoryGroup('Usage');
var cat2 = new Category('Turnkey Marketing Materials');
var cat3 = new Category('Marketing Programs');
var cat4 = new Category('Experiences and Offers');

var catg4 = new CategoryGroup('Retention');
var cat6 = new Category('Turnkey Marketing Materials');

var catg5 = new CategoryGroup('Insights');
var cat8 = new Category('Turnkey Marketing Materials');



// CATEGORY GROUP 1 -- Acquisition
//cal1.addCategoryGroup(catg1);


// Turnkey Marketing Materials
//cal1.addCategory(cat1);

//var p = new Promo('Priceless Partnership&trade; Ph1');
//p.setLink('https://optimization.mastercard.com/portal/program/8d7/MasterCard-Priceless-Partnership-Program');
//p.setOrderPeriod('20131104', '20131213');
//p.setInMarket('20140203', '20140630', "Power your employee performance with our cost-effective, internal marketing program. Use MasterCard Priceless Partnership&trade; to motivate employees at the branch, call center, or head office.<br/><br/>The MasterCard Priceless Partnership&trade; Program is available for all Small Business, Credit, Debit, and Prepaid products.");
//cal1.addPromo(p);

//var p = new Promo('Priceless Partnership&trade; Ph2');
//p.setLink('https://optimization.mastercard.com/portal/program/4d08/MasterCard-Priceless-Partnership-Program---Phase-2');
//p.setOrderPeriod('20140421', '20140613');
//p.setInMarket('20140804', '20141231', "Power your employee performance with our cost-effective, internal marketing program. Use MasterCard Priceless Partnership&trade; to motivate employees at the branch, call center, or head office.<br/><br/>The MasterCard Priceless Partnership&trade; Program is available for all Small Business, Credit, Debit, and Prepaid products.");
//cal1.addPromo(p);

// CATEGORY GROUP 2 -- Activation
cal1.addCategoryGroup(catg2);

// Turnkey Marketing Materials
cal1.addCategory(cat7);

var p = new Promo('Benefits Reinforcement');
p.setLink('');
p.setOngoing("MasterCard responds to evolving consumer needs with innovative payment products that deliver the benefits, rewards, and enhancements that best address them. Our family of well-known card products and programs is designed to serve all consumer segments, from the underserved to the elite affluent.<br><br>Consumer materials are available for Credit, Charge, Debit, and Prepaid products.");
cal1.addPromo(p);

// CATEGORY GROUP 3 -- Usage
cal1.addCategoryGroup(catg3);


// Turnkey Marketing Materials
cal1.addCategory(cat2);

var p = new Promo('Travel Programs');
p.setLink('https://optimization.mastercard.com/portal/page/mc-travel');
p.setOngoing("Drive usage during our new travel campaign by promoting travel benefits to your cardholders.");
cal1.addPromo(p);

var p = new Promo('Priceless Cities&reg;');
p.setLink('https://optimization.mastercard.com/portal/program/4571/Priceless-Cities');
p.setOngoing("Drive usage and bring a little more priceless to the lives of your cardholders. The Priceless&reg; Cities program delivers amazing experiences, privileged access and offers that celebrate the world’s greatest cities.<br><br>Priceless Cities&reg; is available for all Small Business, Credit, Debit, and Prepaid products.");
cal1.addPromo(p);

var p = new Promo('Stand Up to Cancer');
p.setLink('');
p.setOrderPeriod('20140430', '20140901');
p.setInMarket('20140701', '20140915', "Promote card use. Make a difference. Help mazimize MasterCard's donation to cancer research by encouraging cardholders to help make a difference when they pay for meals with their MasterCard this summer.");
cal1.addPromo(p);

var p = new Promo('Priceless Surprises');
p.setLink('https://optimization.mastercard.com/portal/program/4cde/MasterCard-Priceless-Surprises');
p.setOngoing("Help drive preference and usage among MasterCard cardholders with Priceless Surprises.");
cal1.addPromo(p);

var p = new Promo('Safety &amp; Security');
p.setLink('https://optimization.mastercard.com/portal/program/4c9c/Safety-and-Security-Messaging');
p.setOngoing("Reassure your cardholders that their MasterCard comes with strong consumer benefits that provide peace-of-mind, most notably with zero liability coverage.");
cal1.addPromo(p);

var p = new Promo('Fuel Rewards Network&trade;');
p.setLink('https://optimization.mastercard.com/portal/program/4936/Fuel-Rewards-Network-FRN');
p.setOngoing("Offering MasterCard cardholders a chance to earn valuable savings at the pump is an opportunity to drive top of wallet behavior.<br /><br />The Fuel Rewards Network&trade; program is available for all Credit and Debit products.");
cal1.addPromo(p);

var p = new Promo('Tax Payments');
p.setLink('https://optimization.mastercard.com/portal/program/1c0/2015-MasterCard-IRS-Tax-Payment-Program');
p.setOngoing("Enhance cardholder relationships with a comprehensive set of marketing materials to help you promote card usage for federal tax payments.<br /><br />2015 Tax Payments Tools are available for Credit and Debit products.");
cal1.addPromo(p);

var p = new Promo('MasterPass&trade;');
p.setLink('https://optimization.mastercard.com/portal/program/4a78/MasterPass-');
p.setOngoing("e-Commerce sales in the U.S. continue to grow rapidly. Consumers are increasingly using connected devices &ndash; computers, tablets, and smartphones &ndash; to make purchases. With MasterPass&trade;, a new global solution from MasterCard, you can offer your cardholders a secure way to check out faster online. This flexible, fast-to-market solution can help you deepen cardholder relationships and drive higher spend. Consumers get a better way to pay while enhancing and simplifying the physical and digital shopping experience.  Learn more about the MasterPass suite of services.");
cal1.addPromo(p);

var p = new Promo('Product Toolkits');
p.setLink('');
p.setOngoing("MasterCard responds to evolving consumer needs with innovative payment products that deliver the benefits, rewards, and enhancements that best address them. Our family of well-known card products and programs is designed to serve all consumer segments, from the underserved to the elite affluent.<br /><br />Consumer materials are available for Credit, Charge, Debit, and Prepaid products.");
cal1.addPromo(p);

var p = new Promo('MasterCard Easy Savings&reg; for Small Business');
p.setLink('https://optimization.mastercard.com/portal/program/8f1/MasterCard-Easy-Savings-Program');
p.setOngoing("Your business cardholders can enjoy automatic rebates at great merchant partners across the travel, entertainment, and business tools categories. This program is easy to use, and the rebate savings add up automatically. Cardholders like that the savings are easy to see on their monthly statements or online.<br><br>MasterCard Easy Savings are available for all Small Business products.");
cal1.addPromo(p);

var p = new Promo('Easy Savings&reg; for Commercial');
p.setLink('https://optimization.mastercard.com/portal/program/45ee/MasterCard-Easy-Savings-Program-for-Commercial');
p.setOngoing("MasterCard Easy Savings&reg; Program for Commercial is a merchant-funded, MasterCard-managed, automatic rebate program for your U.S.-based MasterCard middle market cardholders that will effectively grow your business.<br /><br />MasterCard Easy Savings for Commercial is available for all Small Business Products.");
cal1.addPromo(p);

var p = new Promo('MasterCard Business Network');
p.setLink('https://optimization.mastercard.com/portal/program/460a/MasterCard-Business-Network');
p.setOngoing("Help business cardholders conveniently purchase supplies with discounts on 650,000+ products, book business trips with free integrated travel planner, review restaurant ratings and manage expense reports with receipt image upload and approvals through smartphone—all in one place.<br /><br />The MasterCard Business Network is available for all Small Business products.");
cal1.addPromo(p);

var p = new Promo('MasterCard EMV');
p.setLink('https://optimization.mastercard.com/portal/page/MasterCard-EMV');
p.setOngoing("Payments technology is rapidly evolving and EMV chip cards are the next generation of powerful payment vehicles. As you prepare for the future, the EMV standard serves as the backbone for new payment technologies that will drive continued growth and innovation. MasterCard is here to help you evaluate your chip options and determine the right strategy and course of action for your business &ndash; along with end-to-end support for your EMV upgrade or conversion.");
cal1.addPromo(p);

var p = new Promo('Bill Pay Program');
p.setLink('https://optimization.mastercard.com/portal/program/4d65/MasterCard-Bill-Payment-Promotion');
p.setOrderPeriod('20140530', '20140930');
p.setInMarket('20140801', '20141031', "Give MasterCard cardholders a chance to win a prize (last year it was $60,000) for all their purchases including their monthly recurring bills.");
cal1.addPromo(p);

var p = new Promo('Category Expansion');
p.setLink('https://optimization.mastercard.com/portal/program/8d9/Category-Expansion');
p.setOngoing("Cardholders who spend across a diverse set of merchant categories are more engaged and loyal to your card products.<br /><br />MasterCard Advisors can help you incite this behavior through a set of merchant-themed usage programs, including everyday spend (EDS) with gas/grocery/drugstore, restaurant, home improvement/do-it-yourself (DIY), or travel. MasterCard Advisors will use our segmentation, proven creative designs, and fulfillment and analytics capabilities to deliver this program for you from beginning to end.");
cal1.addPromo(p);

var p = new Promo('Low Usage Campaign');
p.setLink('https://optimization.mastercard.com/portal/program/8da/Low-Usage-Campaign');
p.setOngoing("Increase cardholder usage and product engagement with our Low Usage Cash Rewards campaign.<br /><br />Through our turnkey process, you can deliver a compelling promotional offer to your low-spend cardholders, designed to increase their usage. MasterCard Advisors will use our segmentation, proven creative designs, and fulfillment and analytics capabilities to deliver this program for you from beginning to end.");
cal1.addPromo(p);

var p = new Promo('Recurring Payments Incentive');
p.setLink('https://optimization.mastercard.com/portal/program/8d8/Recurring-Payments');
p.setOngoing("Help increase activation and spend on recurring payment transactions with this direct marketing program, which offers cardholders incentives such as statement credit, miles, or points for making recurring payments.<br><br>This MasterCard Advisors Recurring Payments program is available for all Small Business, Credit, and Debit products.");
cal1.addPromo(p);

// Marketing Programs
// cal1.addCategory(cat3);

// Experiences and Offers
// cal1.addCategory(cat4);


// CATEGORY GROUP 4 -- Retention
cal1.addCategoryGroup(catg4);


// Turnkey marketing materials
cal1.addCategory(cat6);

var p = new Promo('Proactive Retention');
p.setLink('https://optimization.mastercard.com/portal/program/8db/Proactive-Retention');
p.setOngoing("Taking care of the customers you’ve won is important. This MasterCard Advisors program helps you keep your most valuable customers. Using analytics and modeling, high value cardholders that are likely to attrite are identified. These cardholders are sent communications that reinforce core product benefits to enhance the overall customer relationship.");
cal1.addPromo(p);


// CATEGORY GROUP 5 -- Insights
cal1.addCategoryGroup(catg5);

// Turnkey marketing materials
cal1.addCategory(cat8);

var p = new Promo('Insights');
p.setLink('https://optimization.mastercard.com/portal/insights');
p.setOngoing("MasterCard is committed to providing financial institutions with the insights and strategies needed to adapt and thrive. We analyze trends in the payments industry - consumer attitudes, behaviors, and spending; macroeconomic conditions; regulatory developments; and other issues - to support your business with best practices, successful product and marketing strategies, and insights that help you connect with your customers and better serve their needs.");
cal1.addPromo(p);

cal1.buildCalendar();