// WORLD
var cal1 = new MarketingCalendar('Calendar1', 2009);
var catg1 = new CategoryGroup('Promotional Programs');
var cat1 = new Category('Activation');
var cat2 = new Category('World MasterCard Revitalization');
var cat3 = new Category('Mass Usage Promotions');
var cat4 = new Category('Targeted Usage Campaigns');
var cat5 = new Category('Merchant Offerings');

var catg2 = new CategoryGroup('Product Messaging');
var cat6 = new Category('Upgrade Communications');
var cat7 = new Category('Benefits Reinforcement');

var catg3 = new CategoryGroup('Planning Tools');
var cat8 = new Category('Rewards');
var cat9 = new Category('Targeting');
var cat10 = new Category('Tracking/Analysis');

var catg4 = new CategoryGroup('Sales Tools');
var cat11 = new Category('Incentive Program');


// CATEGORY GROUP 1 -- PROMOTIONAL PROGRAMS
cal1.addCategoryGroup(catg1);


// ACTIVATION
cal1.addCategory(cat1);

var p  = new Promo('Early Activation');
p.setLink('info.jsp?fileName=early_activation.html&menu=newgrowth.html');
p.setOrderPeriod('20090401', '20090430');
p.setInMarket('20090601', '20091231', "The longer cardholders wait to use their cards, the less likely they are to use them. So MasterCard has developed the MasterCard Early Activation Program to provide the tools that you need to increase activation and usage of newly acquired cards.");
cal1.addPromo(p);



// WORLD MASTERCARD RE-LAUNCH
cal1.addCategory(cat2);

var p  = new Promo('Revitalization Usage');
p.setLink('info.jsp?fileName=world_revite.html&menu=newgrowth.html');
p.setOrderPeriod('20090515', '20090615');
p.setInMarket('20090901', '20091031', "World MasterCard has been redesigned to deliver affluent cardholders even more compelling rewards, worldwide experiences, and enhanced service and protection. To reinforce these great new benefits and drive card transactions, MasterCard will develop an engaging usage based campaign that will coincide with the launch of new World MasterCard brand advertising.");
cal1.addPromo(p);



// MASS USAGE PROMOTIONS
cal1.addCategory(cat3);

var p  = new Promo('Summer Promotion');
p.setLink('info.jsp?fileName=2009_summer_promo.html&menu=newgrowth.html');
p.setOrderPeriod('20090309', '20090415');
p.setInMarket('20090701', '20090831', "Capture incremental spend in the peak summer season. MasterCard will provide tools to help you to drive usage. Double entries will be awarded for tapping with MasterCard PayPass&reg;.");
cal1.addPromo(p);

var p  = new Promo('2010 Arnold Palmer Invitational');
p.setLink('info.jsp?fileName=2009_arnold_palmer.html&menu=newgrowth.html');
p.setOrderPeriod('20091001', '20091115');
//p.setInMarket('20100102', '20100215', "Your cardholders won't need to find a big screen to watch their favorite pro at the 2010 Arnold Palmer Invitational presented by MasterCard&mdash;we'll help them see their favorite pro sink a hole-in-one in person.");
cal1.addPromo(p);

//var p  = new Promo('Holiday Promotion');
//p.setLink('info.jsp?fileName=holiday_promo.html&menu=newgrowth.html');
//p.setOrderPeriodText('20090830', '20090930','Holiday Program Details Coming Soon!');
//p.setInMarket('20091116', '20091223', "Holiday Program Details Coming Soon!");
//cal1.addPromo(p);

// TARGETED USAGE CAMPAIGNS
cal1.addCategory(cat4);

var p  = new Promo('Recurring Bill Payment Program');
p.setLink('info.jsp?fileName=bill_payment.html&menu=newgrowth.html');
p.setOrderPeriod('20090601', '20090630');
p.setInMarket('20091001', '20091031', "Recurring payments represent a way to offer your customers a convenient way to pay bills and save time in the process. It eliminates the hassle and expense of writing checks, saves on postage, and makes it easier to keep track of expenses which can help cardholders stay in control of their monthly payments. This program helps motivate your cardholders to set up more recurring payments by offering an incentive for each incremental recurring payment and provides you with an opportunity to grow.");
cal1.addPromo(p);

var p  = new Promo('Restaurant Campaign');
p.setLink('info.jsp?fileName=category_expansion.html&menu=newgrowth.html');
p.setOrderPeriod('20090202', '20090227');
p.setInMarket('20090601', '20090630', "This campaign will be designed to encourage cardholders to increase the number of purchases they make in the revenue-generating restaurant category.");
cal1.addPromo(p);

var p  = new Promo('Low/Medium Active Holiday');
p.setLink('info.jsp?fileName=holiday_low-med-active.html&menu=newgrowth.html');
p.setOrderPeriod('20090720', '20090817');
p.setInMarket('20091101', '20100102', "This program is a targeted direct marketing campaign leveraging the Holiday National Sweepstakes offering opportunities for cardholders to win promotion-specific merchandise. The Low Active promotion is targeted to cardholders with an average of 1&ndash;3 transactions per month and the Medium Active promotion is targeted to cardholders with an average of 4&ndash;7 transactions per month.");
cal1.addPromo(p);

var p  = new Promo('Commerce Coalition');
p.setLink('info.jsp?fileName=commerce_coalition.html&menu=newgrowth.html');
p.setOrderPeriod('20090508', '20090817');
p.setInMarket('20091101', '20091231', "The Commerce Coalition program helps issuers drive sustained usage and preference among active, engaged affluent cardholders. The highly targeted direct marketing program is powered by a proprietary MasterCard analytics engine, leveraging spend behavior to deliver personalized merchant offers to affluent consumers. Starting with an inventory of as many as 20 merchants participating in each campaign, MasterCard selects the five most relevant, compelling offers to mail to each cardholder. <br><br>2009 Campaigns:<ul style='margin-top:0px;margin-bottom:0px;'><li>Celebrate!&ndash; May/June&mdash;focused on the spring gift-giving holidays including Mother&rsquo;s Day, Father&rsquo;s Day, graduation, and weddings. <i>Opt in by January.</i></li><li>Back to School&ndash; August/September&mdash;focused on consumer electronics, home furnishings, and apparel. <i>Opt in by June.</i></li><li>Holiday&ndash; Mid-November/December&mdash;seasonal gift-giving. <i>Opt in by September.</i></li></ul><br>Contact your MasterCard Relationship Manager.");
cal1.addPromo(p);

// MERCHANT OFFERINGS
cal1.addCategory(cat5);

var p  = new Promo('Experiences and Offers');
p.setLink('http://www.mastercardworldwide.com/experiences');
p.setOngoing("MasterCard Experiences and Offers provide you with a portfolio of flexible assts that can be personalized to fit your cardholder&rsquo;s needs. Enriching experiences, special offers, and preferred access from an exceptional selection of merchants may help you connect with your affluent cardholders.");
cal1.addPromo(p);


// CATEGORY GROUP 2 -- PRODUCT MESSAGING
cal1.addCategoryGroup(catg2);

// UPGRADE COMMUNICATIONS
cal1.addCategory(cat6);

var p  = new Promo('Upgrade Communication');
p.setLink('');
p.setOrderPeriod('20090601', '20091231');
p.setInMarket('20090601', '20091231', "Turnkey and customized materials will be developed to introduce the new World MasterCard value proposition and communicate overall product positioning and enhanced features and benefits. This program is ongoing starting in May 2009. For more information please contact your MasterCard Representative.");
cal1.addPromo(p);


// BENEFITS REINFORCEMENT
cal1.addCategory(cat7);

var p  = new Promo('Travel Benefits, Concierge');
p.setLink('');
p.setOrderPeriod('20090601', '20091231');
p.setInMarket('20090601', '20091231', "Turnkey and customizable materials including inserts, e-mails and copy points/blocks will be developed to reinforce new core product benefits such as World Advisor, World Rewards, Global Protection, and Worldwide Experiences.");
cal1.addPromo(p);


// CATEGORY GROUP 3 -- PLANNING TOOLS
cal1.addCategoryGroup(catg3);


// REWARDS
cal1.addCategory(cat8);

var p  = new Promo('Assessment & Enhancements');
p.setLink('');
p.setOngoing("Rewards Competitive Market Scan provides an unparalleled, value-added view of the rewards marketplace. This subscription service helps an issuer meet one of the main challenges in today&rsquo;s market.<br><br>A Rewards Diagnostic takes an in-depth look at the features, operations, marketing practices, and metrics of your card rewards program. The Rewards Diagnostic is targeted to the issuer&rsquo;s needs and business objectives.");
cal1.addPromo(p);


// TARGETING
cal1.addCategory(cat9);

var p = new Promo('Portfolio Diagnostics');
p.setLink('');
p.setOngoing("A <b><i>Portfolio Diagnostic</i></b> analyzes the issuer&rsquo;s transactional and customer data (as provided by the Issuer). A segmentation analysis identifies opportunity segments to pursue targeted penetration, activation, and usage-related marketing campaigns to optimize the performance of the portfolio. MasterCard (through Advisors) has propensity models that assist issuers with identifying current card users with the propensities such as those at risk of attrition, predict those most likely to start using the card again after being dormant, and helping to potentially determine small businesses &ldquo;hidden&rdquo; amount consumer accounts.<br><br>Contact your MasterCard Relationship Manager.");
cal1.addPromo(p);


// TRACKING/ANALYSIS
cal1.addCategory(cat10);

var p = new Promo('Portfolio Analytics');
p.setLink('');
p.setOngoing("<i>Portfolio Analytics</i> provides customer-specific information about its marketing, risk, operations, and fraud. This data, in conjunction with information about specific regions and countries, may facilitate more effective portfolio marketing strategies and overall management.<br><br>Contact your MasterCard Relationship Manager.");
cal1.addPromo(p);

var p = new Promo('Campaign Management');
p.setLink('');
p.setOngoing("<i>Campaign Management</i> is an interactive information-enabled application tailored specifically to the needs of the payments industry. It helps issuers to<ul style='margin-top:0px;margin-bottom:0px;'><li>Increase marketing productivity by automatic cardholder selection and segmentation, and  building portfolios and marketing cells in real time</li><li>Improve campaign results by analyzing prior transaction behavior, card specific metrics, and user-defined fields to target those cardholders most likely to respond</li><li>Track and report cardholder behavior before, during, and after a campaign in order to analyze a promotion's effectiveness and modify it accordingly for future campaigns</li><li>Campaign Management's advanced capabilities and ease of use may help you realize more revenue potential from your marketing campaigns. Campaign Management is available to issuers through Portfolio Analytics.</li></ul><br>Contact your MasterCard Relationship Manager.");
cal1.addPromo(p);


// CATEGORY GROUP 4 -- SALES TOOLS
cal1.addCategoryGroup(catg4);

// INCENTIVE PROGRAMS
cal1.addCategory(cat11);

var p = new Promo('Priceless Partnership');
p.setLink('info.jsp?fileName=priceless_partnership.html&menu=newgrowth.html');
p.setOngoing("This program is designed to further incent employees to sell new MasterCard products. Every time a participating Branch or Call Center employee submits a validated application for a new MasterCard product, they receive an online Instant Win Game Play. This promotion runs quarterly with each quarter featuring its own unique theme, prize structure, and chances to win.<br><br>A variety of marketing materials are available.<br><br>For more information on the Priceless Partnership&trade; Program, please contact your MasterCard Relationship Manager.");
cal1.addPromo(p);


cal1.buildCalendar();
