﻿var rootPortalUrl = window.location.protocol + "//" + window.location.host;

var cal1 = new MarketingCalendar('Calendar1', 2014);
var catg1 = new CategoryGroup('Acquisition');
var cat1 = new Category('Turnkey Marketing Materials');

var catg2 = new CategoryGroup('Activation');
var cat7 = new Category('Turnkey Marketing Materials');

var catg3 = new CategoryGroup('Usage');
var cat2 = new Category('Turnkey Marketing Materials');
var cat3 = new Category('Marketing Programs');
var cat4 = new Category('Experiences and Offers');

var catg4 = new CategoryGroup('Retention');
var cat6 = new Category('Turnkey Marketing Materials');

var catg5 = new CategoryGroup('Insights');
var cat8 = new Category('Turnkey Marketing Materials');



// CATEGORY GROUP 1 -- Acquisition
//cal1.addCategoryGroup(catg1);


// Turnkey Marketing Materials
//cal1.addCategory(cat1);

//var p = new Promo('Priceless Partnership™ Ph1');
//p.setLink(rootPortalUrl + '/portal/program/8d7/MasterCard-Priceless-Partnership-Program');
//p.setOrderPeriod('20131104', '20131213');
//p.setInMarket('20140203', '20140630', "Power your employee performance with our cost-effective, internal marketing program. Use MasterCard Priceless Partnership™ to motivate employees at the branch, call center, or head office.<br/><br/>The MasterCard Priceless Partnership™ Program is available for all Small Business, Credit, Debit, and Prepaid products.");
//cal1.addPromo(p);

//var p = new Promo('Priceless Partnership™ Ph2');
//p.setLink(rootPortalUrl + '/portal/program/4d08/MasterCard-Priceless-Partnership-Program---Phase-2');
//p.setOrderPeriod('20140421', '20140613');
//p.setInMarket('20140804', '20141231', "Power your employee performance with our cost-effective, internal marketing program. Use MasterCard Priceless Partnership™ to motivate employees at the branch, call center, or head office.<br/><br/>The MasterCard Priceless Partnership™ Program is available for all Small Business, Credit, Debit, and Prepaid products.");
//cal1.addPromo(p);

// CATEGORY GROUP 2 -- Activation
cal1.addCategoryGroup(catg2);

// Turnkey Marketing Materials
cal1.addCategory(cat7);

var p = new Promo('Benefits Reinforcement');
p.setLink('');
p.setOngoing("MasterCard responds to evolving consumer needs with innovative payment products that deliver the benefits, rewards, and enhancements that best address them. Our family of well-known card products and programs is designed to serve all consumer segments, from the underserved to the elite affluent.<br><br>Consumer materials are available for Credit, Charge, Debit, and Prepaid products.");
cal1.addPromo(p);

// CATEGORY GROUP 3 -- Usage
cal1.addCategoryGroup(catg3);


// Turnkey Marketing Materials
cal1.addCategory(cat2);

var p = new Promo('Android Pay');
p.setLink(rootPortalUrl + '/portal/program/5121/MasterCard-with-Android-Pay');
//p.setOrderPeriod('20150801', '20151231');
p.setInMarket('20150801', '20151231', "Google has introduced Android Pay, which will enable MasterCard cardholders to use their Android phones for everyday purchases in-store and within Android apps.");
cal1.addPromo(p);

var p = new Promo('Samsung Pay');
p.setLink(rootPortalUrl + '/portal/program/504f/MasterCard-with-Samsung-Pay');
//p.setOrderPeriod('20150701', '20151231');
p.setInMarket('20150701', '20151231', "This initiative, which leverages the MasterCard Digital Enablement Service (MDES), has the potential to scale mobile payments rapidly as it expands both the population of consumers who can use mobile phones to easily make purchases, as well as the universe of merchant acceptance. MasterCard will work jointly with issuers, merchants, and acquirers to drive consumer adoption.");
cal1.addPromo(p);

var p = new Promo('Travel Programs');
p.setLink(rootPortalUrl + '/portal/page/mc-travel');
p.setOngoing("Drive usage during our new travel campaign by promoting travel benefits to your cardholders.");
cal1.addPromo(p);

var p = new Promo('Priceless Cities®');
p.setLink(rootPortalUrl + '/portal/program/4571/Priceless-Cities');
p.setOngoing("Drive usage and bring a little more priceless to the lives of your cardholders. The Priceless® Cities program delivers amazing experiences, privileged access and offers that celebrate the worldâs greatest cities.<br><br>Priceless Cities® is available for all Small Business, Credit, Debit, and Prepaid products.");
cal1.addPromo(p);

var p = new Promo('Stand Up to Cancer');
p.setLink(rootPortalUrl + '/portal/program/5090/Priceless-Causes-with-Stand-Up-To-Cancer-2015-Marketing-Program');
//p.setOngoing("Promote card use. Make a difference. Help mazimize MasterCard's donation to cancer research by encouraging cardholders to help make a difference when they pay for meals with their MasterCard this summer.<br/><br/>Program Timing: TBD.");
p.setOrderPeriod('20150415', '20150901');
p.setInMarket('20150715', '20150915', "Promote card use. Make a difference. Help maximize MasterCard’s donation to cancer research by encouraging cardholders to help make a difference when they pay for meals with their MasterCard this summer.");
cal1.addPromo(p);

var p = new Promo('Priceless Surprises');
p.setLink(rootPortalUrl + '/portal/program/4cde/MasterCard-Priceless-Surprises');
p.setOngoing("Help drive preference and usage among MasterCard cardholders with Priceless Surprises.");
cal1.addPromo(p);

var p = new Promo('Safety & Security');
p.setLink(rootPortalUrl + '/portal/program/4c9c/Safety-and-Security-Messaging');
p.setOngoing("Reassure your cardholders that their MasterCard comes with strong consumer benefits that provide peace-of-mind, most notably with zero liability coverage.");
cal1.addPromo(p);

var p = new Promo('Sam’s Club');
p.setLink(rootPortalUrl + '/portal/program/5137/Sams-Club-2015-Offer-for-MasterCard-Cardholders-IBCU');
p.setOrderPeriod('20150601', '20151231');
p.setInMarket('20150601', '20151231', "Join or renew as a Sam’s Plus™ member and receive a $25 gift card.  MasterCard cardholders can use this special offer to discover more of the exciting services a Sam’s Plus membership will bring them – Cash Rewards, Instant Savings, and Pharmacy Savings.");
cal1.addPromo(p);

var p = new Promo('2015 Sheraton Promotion');
p.setLink(rootPortalUrl + '/portal/program/50ba/2015-Sheraton-Hotels-+-Resorts-Summer-Promotion');
p.setOrderPeriod('20150401', '20150821');
p.setInMarket('20150401', '20150907', "Target affluent MasterCard cardholders with this special offer from Sheraton, good through Labor Day Weekend 2015. Cardholders can book two weekend nights at participating Sheraton Hotels & Resorts in North America and Latin America and they can save 20% when they use their MasterCard.");
cal1.addPromo(p);

var p = new Promo('2015 Starwood Promotion');
p.setLink(rootPortalUrl + '/portal/program/4c14/2015-Starwood-Hotels-and-Resorts-Offers-for-MasterCard-Cardholders');
p.setOngoing("Drive usage with special offers for MasterCard® cardholders at Starwood Properties through December 2015.");
cal1.addPromo(p);

var p = new Promo('Leading Hotels of the World');
p.setLink(rootPortalUrl + '/portal/program/506f/Leading-Hotels-of-the-World-2015-Marketing-Campaign');
p.setOrderPeriod('20150101', '20151215');
p.setInMarket('20150101', '20151231', "Target affluent MasterCard cardholders with this special offer from Leading Hotels of the World. Stay 5 nights, pay for 4 with this special promotion at select properties around the world.");
cal1.addPromo(p);

var p = new Promo('Fuel Rewards Network™');
p.setLink(rootPortalUrl + '/portal/program/4936/Fuel-Rewards-Network-FRN');
p.setOngoing("Offering MasterCard cardholders a chance to earn valuable savings at the pump is an opportunity to drive top of wallet behavior.<br /><br />The Fuel Rewards Network™ program is available for all Credit and Debit products.");
cal1.addPromo(p);

var p = new Promo('Tax Payments');
p.setLink(rootPortalUrl + '/portal/program/1c0/2015-MasterCard-IRS-Tax-Payment-Program');
p.setOngoing("Enhance cardholder relationships with a comprehensive set of marketing materials to help you promote card usage for federal tax payments.<br /><br />2015 Tax Payments Tools are available for Credit and Debit products.");
cal1.addPromo(p);

var p = new Promo('MasterPass™');
p.setLink(rootPortalUrl + '/portal/program/4a78/MasterPass-');
p.setOngoing("e-Commerce sales in the U.S. continue to grow rapidly. Consumers are increasingly using connected devices – computers, tablets, and smartphones – to make purchases. With MasterPass™, a new global solution from MasterCard, you can offer your cardholders a secure way to check out faster online. This flexible, fast-to-market solution can help you deepen cardholder relationships and drive higher spend. Consumers get a better way to pay while enhancing and simplifying the physical and digital shopping experience.  Learn more about the MasterPass suite of services.");
cal1.addPromo(p);

var p = new Promo('Consumer Credit Toolkit');
p.setLink(rootPortalUrl + '/portal/asset/4fce/Consumer-Credit-Optimization-Toolkit');
p.setOngoing("MasterCard responds to evolving consumer needs with innovative payment products that deliver the benefits, rewards, and enhancements that best address them. Our family of well-known card products and programs is designed to serve all consumer segments, from the underserved to the elite affluent.<br /><br />Consumer materials are available for Credit.");
cal1.addPromo(p);

var p = new Promo('Consumer Debit Toolkit');
p.setLink(rootPortalUrl + '/portal/asset/508a/Consumer-Debit-Optimization-Toolkit');
p.setOngoing("The toolkit contains everything you need to know about the MasterCard debit value propositions in the U.S. and gives you the necessary tools to help maximize your results.");
cal1.addPromo(p);

var p = new Promo('Easy Savings® - Small Business');
p.setLink(rootPortalUrl + '/portal/program/8f1/MasterCard-Easy-Savings-Program');
p.setOngoing("Your business cardholders can enjoy automatic rebates at great merchant partners across the travel, entertainment, and business tools categories. This program is easy to use, and the rebate savings add up automatically. Cardholders like that the savings are easy to see on their monthly statements or online.<br><br>The MasterCard Easy Savings program is available for all Small Business products.");
cal1.addPromo(p);

var p = new Promo('Easy Savings® - SB Quarterly');
p.setLink(rootPortalUrl + '/portal/program/4f79/MasterCard-Easy-Savings-Program-for-Small-Business---Quarterly-Materials');
p.setOngoing("Keep the MasterCard Easy Savings® Program for Small Business fresh with cardholders with themed creative assets that are available during specific times throughout the year.");
cal1.addPromo(p);

var p = new Promo('Easy Savings® - Commercial');
p.setLink(rootPortalUrl + '/portal/program/45ee/MasterCard-Easy-Savings-Program-for-Commercial');
p.setOngoing("MasterCard Easy Savings® Program for Commercial is a merchant-funded, MasterCard-managed, automatic rebate program for your U.S.-based MasterCard middle market cardholders that will effectively grow your business.<br /><br />MasterCard Easy Savings for Commercial is available for all Small Business Products.");
cal1.addPromo(p);

var p = new Promo('MasterCard Business Network');
p.setLink(rootPortalUrl + '/portal/program/460a/MasterCard-Business-Network');
p.setOngoing("Help business cardholders conveniently purchase supplies with discounts on 650,000+ products, book business trips with free integrated travel planner, review restaurant ratings and manage expense reports with receipt image upload and approvals through smartphoneâall in one place.<br /><br />The MasterCard Business Network is available for all Small Business products.");
cal1.addPromo(p);

var p = new Promo('MasterCard Golf Benefits');
p.setLink(rootPortalUrl + '/portal/program/50c6/MasterCard-Golf-Benefits');
p.setOrderPeriod('20150420', '20151231');
p.setInMarket('20150420', '20151231', "Golfers love playing new and exciting courses, and appreciate the finer parts of the game. With that in mind MasterCard, in association with the PGA TOUR® and other partners, has put together special access exclusive to MasterCard cardholders.");
cal1.addPromo(p);

var p = new Promo('MasterCard Golf Benefits');
p.setLink('https://optimization.mastercard.com/portal/program/50c6/MasterCard-Golf-Benefits');
p.setOrderPeriod('20150420', '20151231');
p.setInMarket('20150420', '20151231', "Golfers love playing new and exciting courses, and appreciate the finer parts of the game. With that in mind MasterCard, in association with the PGA TOUR&reg; and other partners, has put together special access exclusive to MasterCard cardholders.");
cal1.addPromo(p);

var p = new Promo('MasterCard EMV');
p.setLink(rootPortalUrl + '/portal/page/MasterCard-EMV');
p.setOngoing("Payments technology is rapidly evolving and EMV chip cards are the next generation of powerful payment vehicles. As you prepare for the future, the EMV standard serves as the backbone for new payment technologies that will drive continued growth and innovation. MasterCard is here to help you evaluate your chip options and determine the right strategy and course of action for your business – along with end-to-end support for your EMV upgrade or conversion.");
cal1.addPromo(p);

var p = new Promo('Bill Pay Program');
p.setLink('');
//p.setOngoing("Give MasterCard cardholders a chance to win a prize (last year it was $60,000) for all their purchases including their monthly recurring bills.<br/><br/>Program Timing: TBD.");
p.setOrderPeriod('20150701', '20150930');
p.setInMarket('20150701', '20150930', "Give MasterCard cardholders a chance to win a prize for all their purchases including their monthly recurring bills.<br/><br/>Program Timing: TBD.");
cal1.addPromo(p);

var p = new Promo('Category Expansion');
p.setLink(rootPortalUrl + '/portal/program/8d9/Category-Expansion');
p.setOngoing("Cardholders who spend across a diverse set of merchant categories are more engaged and loyal to your card products.<br /><br />MasterCard Advisors can help you incite this behavior through a set of merchant-themed usage programs, including everyday spend (EDS) with gas/grocery/drugstore, restaurant, home improvement/do-it-yourself (DIY), or travel. MasterCard Advisors will use our segmentation, proven creative designs, and fulfillment and analytics capabilities to deliver this program for you from beginning to end.");
cal1.addPromo(p);

var p = new Promo('Low Usage Campaign');
p.setLink(rootPortalUrl + '/portal/program/8da/Low-Usage-Campaign');
p.setOngoing("Increase cardholder usage and product engagement with our Low Usage Cash Rewards campaign.<br /><br />Through our turnkey process, you can deliver a compelling promotional offer to your low-spend cardholders, designed to increase their usage. MasterCard Advisors will use our segmentation, proven creative designs, and fulfillment and analytics capabilities to deliver this program for you from beginning to end.");
cal1.addPromo(p);

var p = new Promo('Recurring Payments Incentive');
p.setLink(rootPortalUrl + '/portal/program/8d8/Recurring-Payments');
p.setOngoing("Help increase activation and spend on recurring payment transactions with this direct marketing program, which offers cardholders incentives such as statement credit, miles, or points for making recurring payments.<br><br>This MasterCard Advisors Recurring Payments program is available for all Small Business, Credit, and Debit products.");
cal1.addPromo(p);

// Marketing Programs
// cal1.addCategory(cat3);

// Experiences and Offers
// cal1.addCategory(cat4);


// CATEGORY GROUP 4 -- Retention
cal1.addCategoryGroup(catg4);


// Turnkey marketing materials
cal1.addCategory(cat6);

var p = new Promo('Proactive Retention');
p.setLink(rootPortalUrl + '/portal/program/8db/Proactive-Retention');
p.setOngoing("Taking care of the customers youâve won is important. This MasterCard Advisors program helps you keep your most valuable customers. Using analytics and modeling, high value cardholders that are likely to attrite are identified. These cardholders are sent communications that reinforce core product benefits to enhance the overall customer relationship.");
cal1.addPromo(p);


// CATEGORY GROUP 5 -- Insights
cal1.addCategoryGroup(catg5);

// Turnkey marketing materials
cal1.addCategory(cat8);

var p = new Promo('Insights');
p.setLink(rootPortalUrl + '/portal/insights');
p.setOngoing("MasterCard is committed to providing financial institutions with the insights and strategies needed to adapt and thrive. We analyze trends in the payments industry - consumer attitudes, behaviors, and spending; macroeconomic conditions; regulatory developments; and other issues - to support your business with best practices, successful product and marketing strategies, and insights that help you connect with your customers and better serve their needs.");
cal1.addPromo(p);

cal1.buildCalendar();