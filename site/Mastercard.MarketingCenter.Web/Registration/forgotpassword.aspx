﻿<%@ Page Language="C#" MasterPageFile="~/Registration/MMPRegistration.master" AutoEventWireup="true" Inherits="Registration_forgotpassword" Title="Forgot Your Password" CodeBehind="forgotpassword.aspx.cs" %>

<%@ Import Namespace="Mastercard.MarketingCenter.Web.Core" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadThemePlaceHolder" runat="server">
    <% var themePath = "/portal/content/themes/orange/"; %>
    <!-- libs -->
    <link rel="stylesheet" href="<%=themePath%>libs/slick/slick.css" />
    <!-- custom-->
    <link rel="stylesheet" href="<%=themePath%>css/fonts/fonts.css" />
    <link rel="stylesheet" href="<%=themePath%>css/font-awesome.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/bootstrap-grid.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/bootstrap-modal.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/main.css" />
    <link rel="stylesheet" href="<%=themePath%>css/responsive.css" />
    <!-- libs -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<%=themePath%>libs/bootstrap.modal.min.js"></script>
    <script src="<%=themePath%>libs/slick/slick.min.js"></script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-spaces-in spaces-vertical-in">
        <asp:Panel ID="pForgotContent" runat="server" DefaultButton="btnsubmit">
            <div class="row">
                <header class="content-header intro col-lg-10">
                    <h1 style="margin-bottom: 0;"><%=Resources.Forms.ForgotPassword_Title%></h1>
                    <p><%=String.Format(Resources.Forms.ForgotPassword_Description, SiteName)%></p>
                </header>
            </div>
            <div class="form row">
                <asp:PlaceHolder runat="server" ID="panelError" Visible="false">
                    <p class="alert alert-warning">
                        <asp:Label ID="lblerror" runat="server" Visible="False"></asp:Label>
                        <asp:Button ID="btnVerificationEmail" runat="server" OnClick="btnVerificationEmail_Click" Style="display: none" />
                        <script type="text/javascript">
                            function sendVerificationEmail() {
                                $("input[id$='btnVerificationEmail']").click();
                            }
                        </script>
                    </p>
                </asp:PlaceHolder>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label class="control-label"><%=Resources.Forms.Email%> <span class="req-mark">*</span></label>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group btn-group">
                        <div class="control-label"><span class="req-mark">* <%=Resources.Forms.RequiredField%></span></div>
                        <asp:Button ID="btnsubmit" CssClass="btn" runat="server" OnClick="btnsubmit_Click" Text="<%$Resources:Shared,Submit%>" />
                        <asp:Button ID="btncancel" CssClass="btn" runat="server" OnClick="btncancel_Click" Text="<%$Resources:Shared,Cancel%>" />
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div class="print_navigation"></div>
    <asp:Panel ID="pMessageContent" runat="server" Visible="False">
        <section class="content-spaces-in spaces-vertical-in hot-colors">
            <h1 class="h3"><strong><%=Resources.Shared.ThankYou%></strong></h1>
            <%=ForgotPasswordSuccessMessage%>
            <p>
                <asp:Button ID="btncontinue" BorderWidth="0" CssClass="btn" runat="server" OnClick="btncontinue_Click" Text="<%$Resources:Shared,Continue%>" />
            </p>
        </section>
    </asp:Panel>
</asp:Content>
