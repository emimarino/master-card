﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mastercard.MarketingCenter.Web.Registration
{
    public partial class LegalContent : System.Web.UI.Page
    {
        private string _bodyString;
        public string BodyString { get { return _bodyString; } set { _bodyString = value; } }

        public void SetBody(string bodyString)
        {
            _bodyString = bodyString;
        }
    }
}