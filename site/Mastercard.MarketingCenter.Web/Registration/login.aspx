﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="MMPRegistration.master" Inherits="Registration_login" CodeBehind="login.aspx.cs" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadThemePlaceHolder" runat="server">
    <% var themePath = "/portal/content/themes/orange/"; %>
    <!-- libs -->
    <link rel="stylesheet" href="<%=themePath%>libs/slick/slick.css" />
    <!-- custom-->
    <link rel="stylesheet" href="<%=themePath%>css/fonts/fonts.css" />
    <link rel="stylesheet" href="<%=themePath%>css/font-awesome.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/bootstrap-grid.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/bootstrap-modal.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/main.css" />
    <link rel="stylesheet" href="<%=themePath%>css/responsive.css" />
    <!-- libs -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<%=themePath%>libs/bootstrap.modal.min.js"></script>
    <script src="<%=themePath%>libs/slick/slick.min.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadJavascriptPlaceHolder" runat="server">
    ​<script src="/portal/content/themes/orange/libs/jquery-ui.dialog.min.js" language="javascript" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="SignInFormPlaceHolder" runat="server">
    <div class="sign-in-choices content-spaces-in">
        <div class="row">
            <section class="choice sign-in col-sm-6 col-md-5 col-lg-4">
                <h1 class="h2"><%=Resources.Forms.Login_ReturningUser%></h1>
                <div class="form sign-in-form clearfix">
                    <asp:PlaceHolder ID="returningUserPlaceHolder" runat="server">
                        <asp:Login ID="loginbox" runat="server" RememberMeSet="false" RenderOuterTable="false">
                            <LayoutTemplate>
                                <p class="alert alert-warning">
                                    <asp:Label ID="FailureText" runat="server" />
                                    <asp:Button ID="btnVerificationEmail" runat="server" OnClick="btnVerificationEmail_Click" Style="display: none" UseSubmitBehavior="false" />
                                    <script type="text/javascript">
                                        function sendVerificationEmail() {
                                            $("input[id$='btnVerificationEmail']").click();
                                        }
                                    </script>
                                </p>
                                <div class="form-group">
                                    <label runat="server" class="form-label"><%=Resources.Forms.UserName%><span class="req-mark">*</span></label>
                                    <asp:TextBox ID="UserName" autocomplete="off" runat="server" CssClass="form-control" />
                                </div>
                                <div class="form-group">
                                    <label runat="server" class="form-label"><%=Resources.Forms.Password%><span class="req-mark">*</span></label>
                                    <asp:TextBox ID="password" TextMode="Password" autocomplete="off" runat="server" CssClass="form-control" />
                                </div>
                                <div class="form-group form-submitter">
                                    <asp:Button ID="login" BorderWidth="0" CommandName="Login" Text="<%$Resources:Shared,SignIn%>" runat="server" CssClass="btn" />
                                </div>
                                <div class="form-group forgot-link">
                                    <a runat="server" class="control-link" href="/portal/forgotpassword"><%=Resources.Forms.Login_ForgotPassword%></a>
                                </div>
                            </LayoutTemplate>
                        </asp:Login>
                    </asp:PlaceHolder>
                </div>
            </section>
            <div class="separator col-md-1 col-lg-4"><span class="label h2"><%=Resources.Shared.Or%></span></div>
            <section class="choice register col-sm-6 col-md-5 col-lg-4">
                <h1 class="h2"><%=Resources.Forms.Login_FirstTimeUser%></h1>
                <a class="btn" href="/portal/register" title="<%=Resources.Shared.Register%>"><%=Resources.Shared.Register%></a>
            </section>
        </div>
    </div>
    <%--Login Footer Page--%>
    <asp:Literal runat="server" ID="LoginFooterPageLiteral" />
</asp:Content>

<asp:Content ID="ConsentRegion" ContentPlaceHolderID="ConsentPlaceholder" runat="server">
    <style>
        .content-spaces-in .mc-lead {
            margin-top: 0;
            margin-bottom: 10px;
            font-size: 20px;
        }
    </style>

    <section class="content-spaces-in" style="min-height: 500px;">
        <div class="inner">
            <asp:Label ID="consentTitle" runat="server" CssClass="h2"></asp:Label>
            <asp:HiddenField ID="acceptedConsentList" runat="server" />
            <asp:PlaceHolder ID="acceptConsentHeadUs" runat="server">
                <p class="mc-lead">
                    <!--@Model.ConsentLanguageText-->
                    <asp:Literal ID="ConsentLanguageTextLiteral" runat="server"></asp:Literal>
                </p>
            </asp:PlaceHolder>
            <p class="alert alert-warning">
                <asp:Label ID="lblError" runat="server" CssClass="span_1" Visible="False"></asp:Label>
            </p>

            <br />
            <div class="form-group btn-group">
                <asp:PlaceHolder ID="termsAcceptanceCheckNonUs" runat="server">
                    <label class="form-label">
                        <input type="checkbox" id="chkTermsAcceptance" runat="server" />
                        <asp:Literal runat="server" ID="termsOfUseAgreement"></asp:Literal>
                        <span class="req-mark">*</span>
                    </label>
                    <br />
                    <br />
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="emailAgreementAcceptance" runat="server">
                    <label class="form-label">
                        <input type="checkbox" id="chkEmailAcceptance" runat="server" />
                        <asp:Literal runat="server" ID="emailMarketingAgreement"></asp:Literal>
                        <span class="req-mark">*</span>
                    </label>
                    <br />
                    <br />
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="consentAgreementWithdrawUs" runat="server">
                    <label class="form-label">
                        <asp:Literal runat="server" ID="emailMarketingAgreementWithdraw"></asp:Literal>
                    </label>
                    <br />
                    <br />
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="acceptConsentBottomNonUs" runat="server">
                    <p class="mc-lead">
                        <!--@Model.ConsentLanguageText-->
                        <asp:Literal ID="ConsentLanguageTextLiteralBottom" runat="server"></asp:Literal>
                    </p>
                </asp:PlaceHolder>
                <div class="paddt">
                    <asp:LinkButton runat="server" ID="cmdConsentSubmit" CssClass="btn"></asp:LinkButton>
                    <asp:LinkButton runat="server" ID="cmdConsentCancel" href="login.aspx" CssClass="btn action-btn"></asp:LinkButton>
                </div>
            </div>
        </div>
    </section>

</asp:Content>
