﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Services;
using System;
using System.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;

public partial class Registration_ResetPassword : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.Page.Title = Resources.Forms.ResetPassword_Title;
    }

    RegistrationService _registrationService { get { return DependencyResolver.Current.GetService<RegistrationService>(); } }
    IUserRepository _userRepository { get { return DependencyResolver.Current.GetService<IUserRepository>(); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.RouteData.Values["userkey"] != null)
            {
                string key = Page.RouteData.Values["userkey"].ToString();

                var userKeyService = DependencyResolver.Current.GetService<IUserKeyService>();
                var userKey = userKeyService.GetByKey(key);

                if (userKey != null)
                {
                    switch (userKey.UserKeyStatusID)
                    {
                        case MarketingCenterDbConstants.UserKeyStatus.Available:
                            int expirationTime = 0;
                            if (ConfigurationManager.AppSettings["ForgotPassword.ExpirationTime"] != null)
                                expirationTime = int.Parse(ConfigurationManager.AppSettings["ForgotPassword.ExpirationTime"]);
                            if (userKey.DateKeyGenerated.AddHours(expirationTime) < DateTime.Now)
                            {
                                userKeyService.ChangeUserKeyStatus(userKey.Key, MarketingCenterDbConstants.UserKeyStatus.Expired);
                                FillExpiredOrUsedContent();
                            }
                            else
                                FillUserDetail(userKey.UserID);
                            break;
                        case MarketingCenterDbConstants.UserKeyStatus.Expired:
                            FillExpiredOrUsedContent();
                            break;
                        case MarketingCenterDbConstants.UserKeyStatus.Used:
                            FillExpiredOrUsedContent();
                            break;
                        default:
                            FillExpiredOrUsedContent();
                            break;
                    }
                }
                else
                    FillExpiredOrUsedContent();
            }
            else
                FillExpiredOrUsedContent();
        }
    }

    private void FillUserDetail(int userID)
    {
        lblerror.Visible = false;
        pResetContent.Visible = true;
        pResetInvalidContent.Visible = false;

        User user = _userRepository.GetUser(userID);
        string userName = Membership.GetUserNameByEmail(user.Email);

        txtEmail.Text = (user.Email != null) ? user.Email.ToString() : string.Empty;

        ViewState["UserName"] = userName;
    }

    private void FillExpiredOrUsedContent()
    {
        lblerror.Visible = false;
        pResetContent.Visible = false;
        pResetInvalidContent.Visible = true;
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        panelError.Visible = false;
        lblerror.Visible = false;
        lblerror.Text = string.Empty;
        if (Page.RouteData.Values["userkey"] != null && ViewState["UserName"] != null)
        {
            lblerror.Text = Mastercard.MarketingCenter.Web.Core.Services.UserService.VerifyNewPasswordIsValid(txtNewPassword.Text, txtConfirmPassword.Text, txtEmail.Text);

            if (!string.IsNullOrWhiteSpace(lblerror.Text))
            {
                lblerror.Visible = true;
                panelError.Visible = true;
                return;
            }

            var userKeyService = DependencyResolver.Current.GetService<IUserKeyService>();

            string userKey = Page.RouteData.Values["userkey"].ToString();
            string userName = ViewState["UserName"].ToString();

            _registrationService.SetUserPassword(userName, txtNewPassword.Text.Trim());
            userKeyService.ChangeUserKeyStatusForRelatedKeys(userKey, MarketingCenterDbConstants.UserKeyStatus.Used);

            pMessageContent.Visible = true;
            pResetContent.Visible = false;
            panelError.Visible = false;
        }
    }

    protected void btncontinue_Click(object sender, EventArgs e)
    {
        Response.Redirect(FormsAuthentication.DefaultUrl);
    }

    protected void btnforgot_Click(object sender, EventArgs e)
    {
        Response.Redirect("/portal/forgotpassword");
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("/portal/Registration/login.aspx");
    }
}