﻿using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Services;
using System;
using System.Web.Mvc;
using System.Web.Security;

public partial class Registration_ChangePassword : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Page.Title = Resources.Forms.ChangePassword_Title;
    }

    public UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
    public LoginService _loginService { get { return DependencyResolver.Current.GetService<LoginService>(); } }
    public RegistrationService _registrationService { get { return DependencyResolver.Current.GetService<RegistrationService>(); } }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        panelError.Visible = false;
        lblerror.Visible = false;
        lblerror.Text = string.Empty;

        MembershipUser user = MembershipService.GetUser(_userContext.User.UserName);
        string currentPassword = user.GetPassword();

        if (txtCurrentPassword.Text.Trim().Length == 0)
        {
            lblerror.Text += string.IsNullOrWhiteSpace(lblerror.Text) ? Resources.Errors.PasswordCurrentEmpty : "<br />" + Resources.Errors.PasswordCurrentEmpty;
        }
        else if (txtCurrentPassword.Text.Trim() != currentPassword.Trim())
        {
            lblerror.Text += string.IsNullOrWhiteSpace(lblerror.Text) ? Resources.Errors.PasswordIncorrect : "<br />" + Resources.Errors.PasswordIncorrect;
            lblerror.Text += Resources.Errors.PasswordIncorrectUseForgot;
        }

        string errors = Mastercard.MarketingCenter.Web.Core.Services.UserService.VerifyNewPasswordIsValid(txtNewPassword.Text, txtConfirmPassword.Text, user.Email);
        lblerror.Text += string.IsNullOrWhiteSpace(lblerror.Text) ? errors : "<br />" + errors;

        if (!string.IsNullOrWhiteSpace(lblerror.Text))
        {
            lblerror.Visible = true;
            panelError.Visible = true;
            return;
        }

        _registrationService.SetUserPassword(_userContext.User.UserName, txtNewPassword.Text.Trim());

        if (Membership.Provider.ValidateUser(_userContext.User.UserName, txtNewPassword.Text.Trim()))
        {
            _loginService.TrackLogin(_userContext.User.UserName, Membership.Provider.Name);
        }

        pMessageContent.Visible = true;
        pChangeContent.Visible = false;
        panelError.Visible = false;
    }

    protected void btncontinue_Click(object sender, EventArgs e)
    {
        FormsAuthentication.RedirectFromLoginPage(_userContext.User.UserName, true);
        Response.Redirect(FormsAuthentication.DefaultUrl);
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("/portal/profile");
    }
}