﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="MMPRegistration.master" Inherits="Registration_CreatePassword" Title="Create Your Account" Codebehind="CreatePassword.aspx.cs" %>
<%@ Import Namespace="Mastercard.MarketingCenter.Web.Core.Extensions" %>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadThemePlaceHolder" runat="server">
    <% var themePath = "/portal/content/themes/orange/"; %>
    <!-- libs -->
    <link rel="stylesheet" href="<%=themePath%>libs/slick/slick.css" />
    <!-- custom-->
    <link rel="stylesheet" href="<%=themePath%>css/fonts/fonts.css" />
    <link rel="stylesheet" href="<%=themePath%>css/font-awesome.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/bootstrap-grid.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/bootstrap-modal.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/main.css" />
    <link rel="stylesheet" href="<%=themePath%>css/responsive.css" />
    <!-- libs -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<%=themePath%>libs/bootstrap.modal.min.js"></script>
    <script src="<%=themePath%>libs/slick/slick.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-spaces-in spaces-vertical-in">
        <div class="row">
            <header class="content-header intro col-lg-10">
                <h1 style="margin-bottom: 0;"><%=Resources.Forms.CreateAccount_Subtitle%></h1>
                <p><%=string.Format(Resources.Forms.CreateAccount_Description, this.GetSiteName())%></p>
                <asp:PlaceHolder runat="server" ID="phError" Visible="false">
                <p class="alert alert-warning">
                    <asp:Label runat="server" ID="lblError"></asp:Label>
                </p>
                </asp:PlaceHolder>
            </header>
        </div>            
        <div class="form row">
            <div class="col-sm-5">
                <div class="form-group">
                    <label class="control-label"><%=Resources.Forms.UserName%> <span class="req-mark">*</span></label>
                    <asp:Label ID="txtUserName" runat="server" CssClass="form-control"></asp:Label>
                </div>
                <div class="form-group">
                    <label class="control-label"><%=Resources.Forms.Password%> <span class="req-mark">*</span></label>
                    <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="control-label"><%=Resources.Forms.ConfirmPassword%> <span class="req-mark">*</span></label>
                    <asp:TextBox ID="txtConfirmPassword" TextMode="Password" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="form-group btn-group">
                    <div class="control-label"><span class="req-mark">* <%=Resources.Forms.RequiredField%></span></div>
                    <asp:Button ID="btnSubmit" runat="server" CssClass="btn" Text="<%$Resources:Shared,Submit%>" OnClick="btnSubmit_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>