﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Extensions;
using Mastercard.MarketingCenter.Web.Core.Models;
using Pulsus;
using Slam.Cms.Data;
using StackExchange.Profiling;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Registration_login : System.Web.UI.Page
{
    private IConsentManagementService _consentManagementService { get { return DependencyResolver.Current.GetService<IConsentManagementService>(); } }
    private MastercardAuthenticationService _authenticationService { get { return DependencyResolver.Current.GetService<MastercardAuthenticationService>(); } }
    protected bool ShowHoverDialogOnPageLoading { get; set; }
    protected bool DisableDialog = false;
    protected string DefaultLanguage { get { return System.Configuration.ConfigurationManager.AppSettings["DefaultLanguage"]; } }
    protected LoginService _loginService { get { return DependencyResolver.Current.GetService<LoginService>(); } }
    public RegistrationService _registrationService { get { return DependencyResolver.Current.GetService<RegistrationService>(); } }
    public UserSubscriptionRepository _userSubscriptionRepository { get { return DependencyResolver.Current.GetService<UserSubscriptionRepository>(); } }
    public IUserService _userService { get { return DependencyResolver.Current.GetService<IUserService>(); } }
    public IEmailService _emailService { get { return DependencyResolver.Current.GetService<IEmailService>(); } }
    public IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }
    private ISettingsService _settingsService { get { return DependencyResolver.Current.GetService<ISettingsService>(); } }
    private SlamContext _slamContext { get { return DependencyResolver.Current.GetService<SlamContext>(); } }
    private UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        loginbox.LoggingIn += login_LoggingIn;
        loginbox.Authenticate += login_Authenticate;
        loginbox.LoggedIn += loginbox_LoggedIn;
        cmdConsentSubmit.Click += CmdConsentSubmit_Click;
        this.Page.Title = this.GetSiteName();
        MiniProfiler.Current.Step("Init");
    }

    private void CmdConsentSubmit_Click(object sender, EventArgs e)
    {
        lblError.Text = string.Empty;
        lblError.Visible = false;

        if (ValidateConsentForm())
        {
            ReacceptConsents();
            Response.Redirect(GetReturnUrl());
        }
        else
        {
            lblError.Visible = true;
        }
    }
    private bool ValidateConsentForm()
    {
        var errors = new List<string>();
        if ((termsAcceptanceCheckNonUs.Visible && !chkTermsAcceptance.Checked) || (!termsAcceptanceCheckNonUs.Visible && emailAgreementAcceptance.Visible && !chkEmailAcceptance.Checked))
        {
            errors.Add(Resources.Errors.ConsentNotGiven);
        }
        if (errors.Any())
        {
            errors.ForEach(error => lblError.Text += error + "<br/>");
            return false;
        }
        else
            return true;
    }
    private void ReacceptConsents()
    {
        var acceptedConsentListIds = acceptedConsentList.Value.Split(',').Select(x => int.Parse(x));
        _consentManagementService.InsertUserConsentByEmail(loginbox.UserName, acceptedConsentListIds);

        string username = _registrationService.GetUserNameForEmail(loginbox.UserName);
        var user = _userService.GetUserByUserName(username);
        if (user.Region?.Trim() != "us")
        {
            int frequency = MarketingCenterDbConstants.UserSubscriptionFrequency.Never;
            var subscription = _userSubscriptionRepository.GetUserSubscription(user.UserId);
            if (subscription == null)
            {
                frequency = chkEmailAcceptance.Checked ? MarketingCenterDbConstants.UserSubscriptionFrequency.Weekly : MarketingCenterDbConstants.UserSubscriptionFrequency.Never;
            }
            else
            {
                frequency = subscription.UserSubscriptionFrequencyId;
                if (emailAgreementAcceptance.Visible && chkEmailAcceptance.Checked && (subscription.UserSubscriptionFrequencyId == MarketingCenterDbConstants.UserSubscriptionFrequency.Never || subscription.UserSubscriptionFrequencyId == MarketingCenterDbConstants.UserSubscriptionFrequency.Undefined))
                {
                    frequency = MarketingCenterDbConstants.UserSubscriptionFrequency.Weekly;
                }
                else if (emailAgreementAcceptance.Visible && !chkEmailAcceptance.Checked)
                {
                    frequency = MarketingCenterDbConstants.UserSubscriptionFrequency.Never;
                }
            }
            _userSubscriptionRepository.SaveUserSubscription(user.UserId, frequency);
        }
        PostLoginSteps(username);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        using (MiniProfiler.Current.Step("Load"))
        {
            Response.Cache.DisableCache();
            AutoLoginForHandOff();

            LoadDynamicSections();

            if (!this.IsPostBack)
            {
                var cookie = Request.Cookies["AGE.MC.MMP.LoginID"];
                if (cookie != null && cookie.Value != null)
                {
                    loginbox.UserName = cookie.Value;
                    loginbox.FindControl("password").Focus();
                }
                else
                {
                    loginbox.FindControl("UserName").Focus();
                }

                DisableDialog = Page.User.Identity.IsAuthenticated;
            }
        }
    }

    private void LoadDynamicSections()
    {
        using (MiniProfiler.Current.Step("LoadDynamicSections"))
        {
            var loginFooterPage = _slamContext.CreateQuery()
                                              .FilterRegion(RegionalizeService.UnauthenticatedRegion)
                                              .Filter($"Page.Uri LIKE '%{_settingsService.GetSiteApplicationShortName().ToLower()}/loginfooter%'")
                                              .FilterTaggedWithAny(new string[] { _userContext.BrowserLanguage, DefaultLanguage }, true)
                                              .OrderBy($"CASE WHEN cift.Identifier = '{_userContext.BrowserLanguage}' THEN 0 ELSE 1 END")
                                              .Get<PageDTO>()
                                              .FirstOrDefault();

            LoginFooterPageLiteral.Text = loginFooterPage?.IntroCopy;
        }
    }

    protected void AutoLoginForHandOff()
    {
        using (MiniProfiler.Current.Step("AutoLoginForHandOff"))
        {
            if (Request.QueryString["ReturnUrl"] != null && Server.UrlDecode(Request.QueryString["ReturnUrl"].ToLower()).Contains("logintoken="))
            {
                try
                {
                    var returnUrl = Server.UrlDecode(Request.QueryString["ReturnUrl"].ToLower());
                    var loginToken = Server.UrlDecode(returnUrl.Substring(returnUrl.IndexOf("logintoken=") + 11));
                    loginToken = loginToken.Substring(0, loginToken.IndexOf('&'));

                    var usernameAndPassword = _authenticationService.GetUsernameAndPasswordFromLoginToken(new Guid(loginToken));
                    if (usernameAndPassword != null && Membership.Provider.ValidateUser(usernameAndPassword[0], usernameAndPassword[1]))
                    {
                        if (Request.QueryString["ReturnUrl"] != null && Server.UrlDecode(Request.QueryString["ReturnUrl"].ToLower()).Contains("adminframeseturl"))
                        {
                            returnUrl = Server.UrlDecode(returnUrl.Substring(returnUrl.IndexOf("adminframeseturl=") + 17));
                            FormsAuthentication.SetAuthCookie(usernameAndPassword[0], false);
                            _authenticationService.RemoveSessionIdCookie();
                            Response.Redirect(returnUrl);
                        }
                        else
                        {
                            FormsAuthentication.RedirectFromLoginPage(usernameAndPassword[0], false);
                        }
                    }
                }
                catch
                {
                    throw new Exception(Resources.Errors.AutoLoginHandoffFailed);
                }
            }
        }
    }

    void login_LoggingIn(object sender, LoginCancelEventArgs e)
    {
        using (MiniProfiler.Current.Step("Loggin In"))
        {
            loginbox.UserName = loginbox.UserName.Trim();

            Label LabelError = (Label)loginbox.FindControl("FailureText");
            if ((loginbox.UserName.Trim() == "") && (loginbox.Password.Trim() == ""))
            {
                LabelError.Text = Resources.Errors.UsernameAndPasswordEmpty;
                e.Cancel = true;
                return;
            }
            if (loginbox.UserName.Trim() == "")
            {
                LabelError.Text = Resources.Errors.UsernameEmpty;
                e.Cancel = true;
                return;

            }
            if (loginbox.Password.Trim() == "")
            {
                LabelError.Text = Resources.Errors.PasswordEmpty;
                e.Cancel = true;
                return;
            }

            Regex r = new Regex(@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$", RegexOptions.IgnoreCase);
            if (!r.IsMatch(loginbox.UserName))
            {
                LabelError.Text = Resources.Errors.EmailAddressInvalid;
                e.Cancel = true;
                return;
            }
            else
            {
                string username = _registrationService.GetUserNameForEmail(loginbox.UserName);
                if (username.Trim().Length == 0)
                {
                    LabelError.Text = Resources.Errors.EmailInvalid;
                    e.Cancel = true;
                    return;
                }

                MembershipUser membershipUser = MembershipService.GetUser(username);
                if (membershipUser == null)
                {
                    LabelError.Text = Resources.Errors.EmailInvalid;
                    e.Cancel = true;
                    return;
                }
                else if (!membershipUser.IsApproved)
                {
                    LabelError.Text = string.Format(Resources.Errors.Login_AlreadyRegistered, loginbox.UserName.ToString());
                    e.Cancel = true;
                    return;
                }

                var user = _userService.GetUserByUserName(membershipUser.UserName);

                if (user != null && (user == null || user.IsDisabled || user.IsBlocked))
                {
                    LabelError.Text = Resources.Errors.UnableProcessCredentials;

                    var region = user.Region;
                    if (!string.IsNullOrEmpty(region) && region.Trim().ToLower() != "us")
                    {
                        LabelError.Text = Resources.Errors.UnableProcessCredentialsMMC;
                    }

                    e.Cancel = true;
                    return;
                }
                else if (membershipUser.IsLockedOut)
                {
                    LabelError.Text = Resources.Errors.AccountLockedOut;
                    e.Cancel = true;
                    return;
                }

                Session["LastLoginDate"] = membershipUser.LastLoginDate;

                if (Membership.Provider.ValidateUser(username, loginbox.Password))
                {
                    //Consent management: checks if new consent is required, then creates the redirects
                    var isConsentExpired = _consentManagementService.IsAnyUserConsentExpired(username, user.Locale);
                    if (isConsentExpired)
                    {
                        ShowConsentForm(username);
                        e.Cancel = true;
                        return;
                    }
                    PostLoginSteps(username);
                }
                else
                {
                    LabelError.Text = Resources.Errors.UsernameAndPasswordIncorrect;
                    e.Cancel = true;
                }
            }
        }
    }

    void ShowConsentForm(string username)
    {
        ContentPlaceHolder SignInPlaceholder = (ContentPlaceHolder)Page.Master.FindControl("SignInFormPlaceHolder");
        ContentPlaceHolder ConsentPlaceholder = (ContentPlaceHolder)Page.Master.FindControl("ConsentPlaceholder");
        Literal RegistrationLayoutHeaderPlaceholder = (Literal)Page.Master.FindControl("RegistrationLayoutHeaderLiteral");
        Literal RegistrationLayoutFooterLiteral = (Literal)Page.Master.FindControl("RegistrationLayoutFooterLiteral");

        SignInPlaceholder.Visible = false;
        ConsentPlaceholder.Visible = true;
        RegistrationLayoutHeaderPlaceholder.Visible = false;
        RegistrationLayoutFooterLiteral.Visible = false;

        var user = _userService.GetUserByUserName(username);
        var language = (string.IsNullOrEmpty(user.Language) ? RegionalizeService.DefaultLanguage : user.Language);
        var region = (string.IsNullOrEmpty(user.Region) ? RegionalizeService.DefaultRegion : user.Region);
        var locale = (string.IsNullOrEmpty(user.Locale) ? RegionalizeService.DefaultCulture : user.Locale);
        var expiredContent = _consentManagementService.GetExpiredConsentForUser(username, locale);
        var viewModel = new ConsentListViewModel { consentFileData = expiredContent };
        var subscription = _userSubscriptionRepository.GetUserSubscription(user.UserId);

        ShowEmailMarketingAgreementTexts(language, region);

        acceptConsentHeadUs.Visible = region?.Trim() == "us";
        termsAcceptanceCheckNonUs.Visible = region?.Trim() != "us";
        consentAgreementWithdrawUs.Visible = region?.Trim() == "us";
        acceptConsentBottomNonUs.Visible = region?.Trim() != "us";
        emailAgreementAcceptance.Visible = subscription == null ? true : (subscription.UserSubscriptionFrequencyId == MarketingCenterDbConstants.UserSubscriptionFrequency.Undefined || subscription.UserSubscriptionFrequencyId == MarketingCenterDbConstants.UserSubscriptionFrequency.Never);

        consentTitle.Text = GetConsentTitle(region, language);
        cmdConsentSubmit.Text = GetButtonSubmitText(region, language);
        cmdConsentCancel.Text = GetButtonCancelText(region, language);
        ConsentLanguageTextLiteral.Text = viewModel.ConsentLanguageText;
        ConsentLanguageTextLiteralBottom.Text = viewModel.ConsentLanguageText;

        acceptedConsentList.Value = viewModel.ConsentsForApprove;
    }

    void PostLoginSteps(string username)
    {
        var user = _userService.GetUserByUserName(username);
        var returnUrl = GetReturnUrl();

        var cookie = Request.Cookies.Get("AGE.MC.MMP.LoginID");
        if (cookie != null)
        {
            cookie.Expires = DateTime.Now.AddDays(-1);
        }

        HttpCookie hc = new HttpCookie("AGE.MC.MMP.LoginID", loginbox.UserName);
        hc.Expires = DateTime.Now.AddMonths(1);
        Response.Cookies.Add(hc);

        _loginService.TrackLogin(username, Membership.Provider.Name);
        FormsAuthentication.SetAuthCookie(username, loginbox.RememberMeSet);
        _authenticationService.RemoveSessionIdCookie();

        if (user.Region != null)
        {
            _userContext.SetUserSelectedLanguage(user.Language.IsNullOrEmpty() ? _userContext.SelectedLanguage : user.Language);
            _userContext.SetUserSelectedRegion(user.Region.IsNullOrEmpty() ? _userContext.SelectedRegion : user.Region.Trim());
        }

        ManageAdminRedirect(username, loginbox.Password);

        FormsAuthentication.RedirectFromLoginPage(username, false);
        if (returnUrl.Length > 0)
        {
            Response.Redirect(returnUrl, false);
        }
        else
        {
            Response.Redirect(FormsAuthentication.DefaultUrl);
        }
    }

    private string GetReturnUrl()
    {
        return Request.QueryString["ReturnUrl"] != null ? Request.QueryString["ReturnUrl"].ToString().Trim() : string.Empty;
    }

    void login_Authenticate(object sender, AuthenticateEventArgs e)
    {
        using (MiniProfiler.Current.Step("Authenticate"))
        {
            string username = _registrationService.GetUserNameForEmail(loginbox.UserName);

            e.Authenticated = Membership.Provider.ValidateUser(loginbox.UserName, loginbox.Password);
            Session.Abandon();

            EnsureNoExistingFBACookies();

            FormsAuthentication.RedirectFromLoginPage(username, false);
        }
    }

    void loginbox_LoggedIn(object sender, EventArgs e)
    {
        using (MiniProfiler.Current.Step("LoggedIn"))
        {
            string username = _registrationService.GetUserNameForEmail(loginbox.UserName);
            System.Threading.Thread.Sleep(500);
            _loginService.TrackLogin(username, Membership.Provider.Name);
            FormsAuthentication.SetAuthCookie(username, loginbox.RememberMeSet);
        }
    }

    protected void EnsureNoExistingFBACookies()
    {
        for (int i = 0; i < Response.Cookies.Count; i++)
        {
            if (Response.Cookies[i].Name.Equals(FormsAuthentication.FormsCookieName, StringComparison.OrdinalIgnoreCase))
            {
                Response.Cookies[i].Expires = DateTime.Now.AddYears(-50);
            }
        }
    }

    protected void ManageAdminRedirect(string username, string password)
    {
        using (MiniProfiler.Current.Step("ManageAdminRedirect"))
        {
            _authenticationService.RemoveSlamAuthenticationCookie();
            if (Request.QueryString["Admin"] != "true")
            {
                return;
            }

            var loginToken = _authenticationService.GetNewToken(username, password).ToString();
            var adminUrl = Slam.Cms.Configuration.ConfigurationManager.Environment.AdminUrl.TrimEnd('/');
            Response.Redirect(Request.QueryString["ReturnUrl"] != null ? string.Format("{0}/{1}?t={2}", adminUrl, Request.QueryString["ReturnUrl"].TrimStart('/'), loginToken) : string.Format("{0}/GlobalSettings/default.aspx?t={1}", adminUrl, loginToken));
        }
    }
    private void ShowEmailMarketingAgreementTexts(string language, string region)
    {
        emailMarketingAgreement.Text = GetEmailMarketingAgreementText(region, language);
        emailMarketingAgreementWithdraw.Text = GetEmailMarketingAgreementWithdrawText(region, language);
        termsOfUseAgreement.Text = "   " + GetTermsOfUseAgreementText(region, language);
    }

    protected string GetEmailMarketingAgreementText(string region, string language)
    {
        return Resources.Forms.ResourceManager.GetString("EmailMarketingAgreement", new CultureInfo(RegionalizeService.GetCulture(region, language)));
    }

    protected string GetEmailMarketingAgreementWithdrawText(string region, string language)
    {
        return Resources.Forms.ResourceManager.GetString("EmailMarketingAgreementWithdraw", new CultureInfo(RegionalizeService.GetCulture(region, language)));
    }

    protected string GetTermsOfUseAgreementText(string region, string language)
    {
        return Resources.Forms.ResourceManager.GetString("TermsOfUseAgreement", new CultureInfo(RegionalizeService.GetCulture(region, language)));
    }

    protected string GetButtonSubmitText(string region, string language)
    {
        return Resources.Shared.ResourceManager.GetString("Submit", new CultureInfo(RegionalizeService.GetCulture(region, language)));
    }

    protected string GetButtonCancelText(string region, string language)
    {
        return Resources.Shared.ResourceManager.GetString("Cancel", new CultureInfo(RegionalizeService.GetCulture(region, language)));
    }

    protected string GetConsentTitle(string region, string language)
    {
        return Resources.Forms.ResourceManager.GetString("Consent_Title", new CultureInfo(RegionalizeService.GetCulture(region, language)));
    }

    protected void btnVerificationEmail_Click(object sender, EventArgs e)
    {
        var user = _userService.GetUserByEmail(loginbox.UserName.Trim());
        if (user != null)
        {
            _emailService.SendEmailVerificationEmail(user.Email, user.Profile.FirstName, user.UserName, this.GetSiteName(), user.UserId, user.Region, user.Language, user.Profile.LastName);
            Response.Redirect(_urlService.GetCheckEmailURL(user.Email, user.Region));
        }
    }

    protected override void OnError(EventArgs e)
    {
        var ex = Server.GetLastError();
        LogManager.EventFactory.Create()
                                .Text($"Error executing in login page...")
                                .Level(LoggingEventLevel.Error)
                                .AddData("Exception Message", ex.Message)
                                .AddData("Exception InnerException", ex.InnerException)
                                .AddData("Exception Data", ex.Data)
                                .AddData("Exception StackTrace", ex.StackTrace)
                                .AddData("Path", Request.Path)
                                .Push();
        Response.Redirect(FormsAuthentication.DefaultUrl);
    }
}