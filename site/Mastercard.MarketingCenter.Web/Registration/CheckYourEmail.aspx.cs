﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Extensions;
using System;
using System.Web.Configuration;
using System.Web.Mvc;

public partial class CheckYourEmail : System.Web.UI.Page
{
    public IUserService _userServices { get { return DependencyResolver.Current.GetService<IUserService>(); } }
    public ISettingsService _settingsServices { get { return DependencyResolver.Current.GetService<ISettingsService>(); } }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Page.Title = Resources.Forms.CheckEmail_Title;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Page.RouteData.Values["email"] != null)
            {
                lblMessage.Text = Page.RouteData.Values["email"].ToString();
                apasswordlink.HRef = $"mailto:{SupportEmail}?subject={Server.HtmlEncode(WebConfigurationManager.AppSettings["CheckEmail.Subject"])}";
            }
        }
    }

    public string SupportEmail
    {
        get
        {
            return _settingsServices.GetRegisterHelpTo(Page.RouteData.Values["region"].ToString().IsNullOrEmpty() ?
                                                                             _userServices.GetUserByEmail(Page.RouteData.Values["email"].ToString())?.Region :
                                                                             Page.RouteData.Values["region"].ToString());
        }
    }

    public string FutureEmailSpamFilter
    {
        get
        {
            return string.Format(Resources.Forms.CheckEmail_FutureEmailSpamFilter, this.GetSiteName(),
                _settingsServices.GetRegisterHelpSecondTo(Page.RouteData.Values["region"].ToString().IsNullOrEmpty() ?
                                                                             _userServices.GetUserByEmail(Page.RouteData.Values["email"].ToString())?.Region :
                                                                             Page.RouteData.Values["region"].ToString()));
        }
    }
}