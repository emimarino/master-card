﻿<%@ Page Language="C#" MasterPageFile="~/WebFormsPages/MMP.master" AutoEventWireup="true" Inherits="Registration_ChangePassword" Title="Change Your Password" CodeBehind="ChangePassword.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-spaces-in spaces-vertical-in">
        <asp:Panel ID="pChangeContent" runat="server" DefaultButton="btnsubmit">
            <div class="row">
                <header class="content-header intro col-lg-10">
                    <h1 style="margin-bottom: 0;"><%=Resources.Forms.ChangePassword_Title%></h1>
                    <%=Resources.Forms.PasswordRequirements_Description%>
                    <asp:PlaceHolder runat="server" ID="panelError" Visible="false">
                        <p class="alert alert-warning">
                            <asp:Label ID="lblerror" runat="server" Visible="False"></asp:Label>
                        </p>
                    </asp:PlaceHolder>
                </header>
            </div>
            <br>
            <div class="form row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label"><%=Resources.Forms.CurrentPassword%> <span class="req-mark">*</span></label>
                        <asp:TextBox ID="txtCurrentPassword" TextMode="Password" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <hr />
                    <div class="form-group">
                        <label class="control-label"><%=Resources.Forms.NewPassword%> <span class="req-mark">*</span></label>
                        <asp:TextBox ID="txtNewPassword" TextMode="Password" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><%=Resources.Forms.ConfirmPassword%> <span class="req-mark">*</span></label>
                        <asp:TextBox ID="txtConfirmPassword" TextMode="Password" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group btn-group">
                        <div class="control-label"><span class="req-mark">* <%=Resources.Forms.RequiredField%></span></div>
                        <asp:Button ID="btnsubmit" CssClass="btn" runat="server" OnClick="btnsubmit_Click" Text="<%$Resources:Shared,Submit%>" />
                        <asp:Button ID="btncancel" CssClass="btn" runat="server" OnClick="btncancel_Click" Text="<%$Resources:Shared,Cancel%>" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pMessageContent" runat="server" Visible="False">
            <div class="row">
                <header class="content-header intro col-lg-10">
                    <h1 style="margin-bottom: 0;"><%=Resources.Shared.ThankYou%></h1>
                    <p><%=Resources.Forms.PasswordChangeSuccessMessage%></p>
                    <p>
                        <asp:Button ID="btncontinue" CssClass="btn" runat="server" OnClick="btncontinue_Click" Text="<%$Resources:Shared,Continue%>" />
                    </p>
                </header>
            </div>
        </asp:Panel>
    </div>
</asp:Content>