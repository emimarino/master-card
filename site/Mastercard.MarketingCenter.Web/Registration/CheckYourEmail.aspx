﻿<%@ Page Language="C#" MasterPageFile="MMPRegistration.master" AutoEventWireup="true" Inherits="CheckYourEmail" Title="Check your Email" CodeBehind="CheckYourEmail.aspx.cs" %>

<%@ Import Namespace="Mastercard.MarketingCenter.Web.Core.Extensions" %>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadThemePlaceHolder" runat="server">
    <% var themePath = "/portal/content/themes/orange/"; %>
    <!-- libs -->
    <link rel="stylesheet" href="<%=themePath%>libs/slick/slick.css" />
    <!-- custom-->
    <link rel="stylesheet" href="<%=themePath%>css/fonts/fonts.css" />
    <link rel="stylesheet" href="<%=themePath%>css/font-awesome.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/bootstrap-grid.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/bootstrap-modal.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/main.css" />
    <link rel="stylesheet" href="<%=themePath%>css/responsive.css" />
    <!-- libs -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<%=themePath%>libs/bootstrap.modal.min.js"></script>
    <script src="<%=themePath%>libs/slick/slick.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-spaces-in spaces-vertical-in">
        <div class="row">
            <header class="content-header intro col-lg-10">
                <h1 style="margin-bottom: 0;"><%=Resources.Forms.CheckEmail_Title%></h1>
                <p>
                    <%=Resources.Forms.CheckEmail_CompleteRegistration.Split(new string[] { "{0}" }, StringSplitOptions.None)[0]%>
                    <asp:Label ID="lblMessage" runat="server" Text="Label"></asp:Label>
                     <%=(Resources.Forms.CheckEmail_CompleteRegistration.Contains("{0}")?Resources.Forms.CheckEmail_CompleteRegistration.Split(new string[] { "{0}" }, StringSplitOptions.None)[1]:"")%>
                </p>
                <p>
                    <%=Resources.Forms.CheckEmail_NotReceivedVerificationEmail.Split(new string[] { "{0}" }, StringSplitOptions.None)[0]%>
                    <a id="apasswordlink" runat="server"><%= SupportEmail %></a>
                    <%=Resources.Forms.CheckEmail_NotReceivedVerificationEmail.Split(new string[] { "{0}" }, StringSplitOptions.None)[1]%>
                </p>
                <p><%= FutureEmailSpamFilter %></p>
            </header>
        </div>
    </div>
</asp:Content>
