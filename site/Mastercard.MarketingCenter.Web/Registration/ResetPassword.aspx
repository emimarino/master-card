﻿<%@ Page Language="C#" MasterPageFile="~/Registration/MMPRegistration.master" AutoEventWireup="true" Inherits="Registration_ResetPassword" Title="Reset Your Password" CodeBehind="ResetPassword.aspx.cs" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadThemePlaceHolder" runat="server">
    <% var themePath = "/portal/content/themes/orange/"; %>
    <!-- libs -->
    <link rel="stylesheet" href="<%=themePath%>libs/slick/slick.css" />
    <!-- custom-->
    <link rel="stylesheet" href="<%=themePath%>css/fonts/fonts.css" />
    <link rel="stylesheet" href="<%=themePath%>css/font-awesome.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/bootstrap-grid.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/bootstrap-modal.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/main.css" />
    <link rel="stylesheet" href="<%=themePath%>css/responsive.css" />
    <!-- libs -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<%=themePath%>libs/bootstrap.modal.min.js"></script>
    <script src="<%=themePath%>libs/slick/slick.min.js"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-spaces-in spaces-vertical-in">
        <asp:Panel ID="pResetContent" runat="server" DefaultButton="btnsubmit">
            <div class="row">
                <header class="content-header intro col-lg-10">
                    <h1 style="margin-bottom: 0;"><%=Resources.Forms.ResetPassword_Title%></h1>
                    <%=Resources.Forms.PasswordRequirements_Description%>
                </header>
            </div>
            <div class="row">
                <div class="form col-sm-5">
                    <asp:PlaceHolder runat="server" ID="panelError" Visible="false">
                        <p class="alert alert-warning">
                            <asp:Label ID="lblerror" runat="server" CssClass="span_1" Visible="False"></asp:Label>
                        </p>
                    </asp:PlaceHolder>
                    <div class="form-group">
                        <label class="control-label"><%=Resources.Forms.EmailAddress%></label>
                        <asp:Label ID="txtEmail" runat="server" CssClass="form-control"></asp:Label>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><%=Resources.Forms.NewPassword%> <span class="req-mark">*</span></label>
                        <asp:TextBox ID="txtNewPassword" TextMode="Password" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label"><%=Resources.Forms.ConfirmPassword%> <span class="req-mark">*</span></label>
                        <asp:TextBox ID="txtConfirmPassword" TextMode="Password" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group btn-group">
                        <div class="control-label"><span class="req-mark">* <%=Resources.Forms.RequiredField%></span></div>
                        <asp:Button ID="btnsubmit" CssClass="btn" runat="server" OnClick="btnsubmit_Click" Text="<%$Resources:Shared,Submit%>" />
                        <asp:Button ID="btncancel" CssClass="btn" runat="server" OnClick="btncancel_Click" Text="<%$Resources:Shared,Cancel%>" />
                    </div>
                </div>
            </div>
        </asp:Panel>

        <asp:Panel ID="pResetInvalidContent" runat="server" Visible="False">
            <div class="row">
                <header class="content-header intro col-lg-10">
                    <h1 style="margin-bottom: 0;"><%=Resources.Errors.PasswordResetLinkInvalid%></h1>
                    <p><%=Resources.Errors.PasswordResetLinkInvalidUseForgot%></p>
                    <p>
                        <asp:Button ID="btnforgot" CssClass="btn" runat="server" OnClick="btnforgot_Click" Text="<%$Resources:Shared,Continue%>" />
                    </p>
                </header>
            </div>
        </asp:Panel>

        <asp:Panel ID="pMessageContent" runat="server" Visible="False">
            <div class="row">
                <header class="content-header intro col-lg-10">
                    <h1 style="margin-bottom: 0;"><%=Resources.Shared.ThankYou%></h1>
                    <p><%=Resources.Forms.PasswordChangeSuccessMessage%></p>
                    <p>
                        <asp:Button ID="btncontinue" CssClass="btn" runat="server" OnClick="btncontinue_Click" Text="<%$Resources:Shared,Continue%>" />
                    </p>
                </header>
            </div>
        </asp:Panel>
    </div>
</asp:Content>