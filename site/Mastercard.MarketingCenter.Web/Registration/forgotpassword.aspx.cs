﻿using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core.Extensions;
using System;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

public partial class Registration_forgotpassword : System.Web.UI.Page
{
    protected string SiteName { get { return this.GetSiteName(); } }

    IUserService _userService { get { return DependencyResolver.Current.GetService<IUserService>(); } }
    IEmailService _emailService { get { return DependencyResolver.Current.GetService<IEmailService>(); } }
    IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }

    public string ForgotPasswordSuccessMessage
    {
        get;
        set;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.Page.Title = Resources.Forms.ForgotPassword_Title;
        ForgotPasswordSuccessMessage = string.Empty;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblerror.Visible = false;
            pForgotContent.Visible = true;
            if (Page.RouteData.Values["email"] != null)
            {
                var email = Page.RouteData.Values["email"].ToString();
                var user = Membership.GetUserNameByEmail(email);

                if (!string.IsNullOrEmpty(user))
                {
                    txtEmail.Text = email;
                }
                else
                {
                    throw new HttpException(404, Resources.Errors.EmailAddressNotFound);
                }
            }
        }
    }
    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (ValidateEmail())
        {
            string membershipUser = Membership.GetUserNameByEmail(txtEmail.Text.Trim());
            if (!string.IsNullOrEmpty(membershipUser))
            {
                MembershipUser u = MembershipService.GetUser(membershipUser);
                if (u.IsApproved)
                {
                    var user = _userService.GetUserByUserName(u.UserName);

                    var userKeyService = DependencyResolver.Current.GetService<IUserKeyService>();
                    var userKey = userKeyService.CreateUserKey(user.UserId);
                    var expirationTime = ConfigurationManager.AppSettings["ForgotPassword.ExpirationTime"];
                    var regionCode = user.Region.ToLower().Trim();
                    if (string.IsNullOrEmpty(regionCode) || regionCode == "us")
                    {
                        ForgotPasswordSuccessMessage = Resources.Forms.ForgotPassword_SuccessMessageUS;
                    }
                    else
                    {
                        ForgotPasswordSuccessMessage = Resources.Forms.ForgotPassword_SuccessMessage;
                    }

                    _emailService.SendForgotPasswordEmail(txtEmail.Text.Trim(), user.Profile.FirstName, expirationTime, userKey.Key, this.GetSiteName(), user.UserId, user.Region?.Trim(), user.Language?.Trim(), user.Profile.LastName);
                    pMessageContent.Visible = true;
                    pForgotContent.Visible = false;
                    panelError.Visible = false;
                }
                else
                {
                    lblerror.Text = Resources.Errors.ForgotPassword_AlreadyRegistered;
                    lblerror.Visible = true;
                    panelError.Visible = true;
                }
            }
            else
            {
                lblerror.Text = Resources.Errors.ForgotPassword_EmailAddressNotFound;
                lblerror.Visible = true;
                panelError.Visible = true;
            }
        }
    }

    protected void btncontinue_Click(object sender, EventArgs e)
    {
        Response.Redirect(FormsAuthentication.DefaultUrl);
    }

    protected bool ValidateEmail()
    {
        bool isValid = true;
        lblerror.Text = string.Empty;
        lblerror.Visible = false;
        panelError.Visible = false;
        Regex r = new Regex(@"^([a-zA-Z0-9_\'\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

        if (!r.IsMatch(txtEmail.Text.Trim()))
        {
            lblerror.Text = Resources.Errors.EmailAddressInvalid;
            lblerror.Visible = true;
            panelError.Visible = true;
            isValid = false;
        }

        if (txtEmail.Text.Trim().Length == 0)
        {
            lblerror.Text = Resources.Errors.EmailEmpty;
            lblerror.Visible = true;
            panelError.Visible = true;
            isValid = false;
        }

        return isValid;
    }

    protected void btncancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(FormsAuthentication.DefaultUrl);
    }

    protected void btnVerificationEmail_Click(object sender, EventArgs e)
    {
        var user = _userService.GetUserByEmail(txtEmail.Text.Trim());
        if (user != null)
        {
            _emailService.SendEmailVerificationEmail(user.Email, user.Profile.FirstName, user.UserName, this.GetSiteName(), user.UserId, user.Region, user.Language, user.Profile.LastName);
            Response.Redirect(_urlService.GetCheckEmailURL(user.Email, user.Region));
        }
    }
}