﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core;
using Mastercard.MarketingCenter.Web.Core.Extensions;
using Mastercard.MarketingCenter.Web.Core.Models;
using Slam.Cms;
using Slam.Cms.Data;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RegisterUser : System.Web.UI.Page
{
    #region Properties

    private UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
    public localRes Res { get { return new localRes(this); } }
    public class localRes
    {
        private RegisterUser parent;
        public localRes(RegisterUser parent) { this.parent = parent; }
        private string getProperty(string prop, string _Class = "Forms")
        {
            try
            {
                string final = parent.GetGlobalResourceObject(_Class, (prop + (parent.Region.ToLower() == RegionalizeService.DefaultRegion.ToLower() ? RegionalizeService.DefaultRegion.ToUpper() : ""))).ToString();
                if (final == null)
                {
                    final = HttpContext.GetGlobalResourceObject(_Class, prop).ToString();
                }

                return final;
            }
            catch
            {
                return HttpContext.GetGlobalResourceObject(_Class, prop).ToString();
            }
        }

        public string ZipCode
        {
            get
            {
                return getProperty("ZipCode");
            }
        }

        public string State
        {
            get
            {
                return getProperty("State");
            }
        }

        public string StateEmpty
        {
            get
            {
                return getProperty("StateEmpty", "Errors");
            }
        }

        public string ZipCodeEmpty
        {
            get
            {
                return getProperty("ZipCodeEmpty", "Errors");
            }
        }
    }

    public ConsentListViewModel _consents { get; set; }

    private ConcurrentDictionary<string, IDictionary<string, string>> _allLanguages;
    public ConcurrentDictionary<string, IDictionary<string, string>> AllLanguages
    {
        get
        {
            return _allLanguages;
        }
        set
        {
            _allLanguages = value;
        }
    }

    protected List<Mastercard.MarketingCenter.Data.Entities.Region> AllRegions;

    private string _region = null;
    public string Region
    {
        get
        {
            return _region;
        }
        set
        {
            _region = value;
        }
    }

    public string CurrentLanguage
    {
        get
        {
            return ddlLanguage.Items.Count == 1 || ddlLanguage.Items.Count > 1 && ddlLanguage.SelectedIndex > 0 ?
                    ddlLanguage.SelectedValue.Trim() :
                    MastercardSitemap.AvailableLanguages.ContainsKey(_userContext.BrowserLanguage) ?
                        _userContext.BrowserLanguage :
                        RegionalizeService.DefaultLanguage;
        }
    }

    private CultureInfo _cultureInfo;
    private CultureInfo CultureInfo
    {
        get
        {
            if (_cultureInfo == null)
            {
                _cultureInfo = new CultureInfo(RegionalizeService.GetCulture(Region, CurrentLanguage));
            }

            return _cultureInfo;
        }
    }

    protected string EmailMarketingAgreement
    {
        get
        {
            return Resources.Forms.ResourceManager.GetString("EmailMarketingAgreement", CultureInfo);
        }
    }

    protected string EmailMarketingAgreementWithdraw
    {
        get
        {
            return Resources.Forms.ResourceManager.GetString("EmailMarketingAgreementWithdraw", CultureInfo);
        }
    }

    protected string TermsOfUseAgreement
    {
        get
        {
            return Resources.Forms.ResourceManager.GetString("TermsOfUseAgreement", CultureInfo);
        }
    }

    #endregion

    #region Services

    protected MastercardSitemap _sitemap { get { return (MastercardSitemap)DependencyResolver.Current.GetService<Sitemap>(); } }
    private SlamContext _slamContext { get { return DependencyResolver.Current.GetService<SlamContext>(); } }
    private IIssuerService _issuerService { get { return DependencyResolver.Current.GetService<IIssuerService>(); } }
    private ITagService _tagServices { get { return DependencyResolver.Current.GetService<ITagService>(); } }
    private RegistrationService _registrationService { get { return DependencyResolver.Current.GetService<RegistrationService>(); } }
    private IPendingRegistrationService _pendingRegistrationService { get { return DependencyResolver.Current.GetService<IPendingRegistrationService>(); } }
    IConsentManagementService _consentManagementService { get { return DependencyResolver.Current.GetService<IConsentManagementService>(); } }
    IEmailService _emailService { get { return DependencyResolver.Current.GetService<IEmailService>(); } }
    IUserService _userService { get { return DependencyResolver.Current.GetService<IUserService>(); } }
    IUrlService _urlService { get { return DependencyResolver.Current.GetService<IUrlService>(); } }
    ICaptchaService _captchaService { get { return DependencyResolver.Current.GetService<ICaptchaService>(); } }
    ISettingsService _settingsService { get { return DependencyResolver.Current.GetService<ISettingsService>(); } }
    IPardotIntegrationService _pardotIntegrationService { get { return DependencyResolver.Current.GetService<IPardotIntegrationService>(); } }
    IRegionService _regionService { get { return DependencyResolver.Current.GetService<IRegionService>(); } }

    #endregion

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Page.Title = Resources.Forms.Register_Title;
    }

    protected ICaptcha Captcha { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (AllRegions == null)
        {
            var regionService = DependencyResolver.Current.GetService<IRegionService>();
            AllRegions = regionService.GetAll().ToList().Where(r => MastercardSitemap.AvailableRegionSitemaps.ContainsKey(r.IdTrimmed)).ToList();
        }

        if (AllLanguages == null)
        {
            AllLanguages = GetAllLanguages(CurrentLanguage);
        }

        if (Region == null && !string.IsNullOrEmpty(ddlCountry.SelectedValue))
        {
            Region = string.IsNullOrEmpty(regionHidden.Value) ? GetRegion(ddlCountry.SelectedValue) : (regionHidden.Value);
        }

        if (Region == null)
        {
            Region = _userContext.SelectedRegion;
        }

        if (!IsPostBack)
        {
            UpdateCulture();
            FillStates();
            FillCountries();
            UpdateConsentLanguage();
        }
        else
        {
            FillCountries(ddlCountry?.SelectedValue);
            try
            {
                System.Web.Helpers.AntiForgery.Validate();
            }
            catch
            {
                Context.RewritePath("/");
            }
        }

        if (RegionalizeService.Countries == null && ddlCountry.Items.Count > 0)
        {
            var categoryChildren = _slamContext.GetTagTree()?.FindNode(tt => (tt.TagCategory != null ? tt.TagCategory.TagCategoryId.Equals(MarketingCenterDbConstants.TagCategories.Markets, StringComparison.OrdinalIgnoreCase) : false))?.Children;
            List<TagTreeNode> temp = new List<TagTreeNode>();
            if (categoryChildren != null)
            {
                foreach (var n in categoryChildren)
                {
                    temp.AddRange(TagTree.GetLeast(n));
                }
            }

            RegionalizeService.Countries = new ConcurrentDictionary<string, string>(temp?.ToDictionary(c => c.Identifier, c => c.Text));
        }

        if (RegionalizeService.Languages == null && AllLanguages.Count > 0)
        {
            RegionalizeService.Languages = new ConcurrentDictionary<string, string>(AllLanguages.SelectMany(c => c.Value).Distinct());
        }

        UpdateMasterPageDynamicContent();
        CreateCaptchaBinding();
    }

    private void UpdateConsentLanguage()
    {
        var locale = string.IsNullOrEmpty(Region) ? null : ddlLanguage.SelectedValue.Trim() + '-' + Region.Substring(0, 2);
        if (!string.IsNullOrEmpty(locale))
        {
            var registerServiceFunctionCodes = _settingsService.GetRegisterServiceFunctionCodes()?.Split(',');
            if (registerServiceFunctionCodes == null)
            {
                throw new Exception("Mising 'ServiceFunctionCodes.Register' configuration key.");
            }

            var serviceFunctionCode = registerServiceFunctionCodes[0];
            var lastestConsents = _consentManagementService.GetLatestConsents(locale, serviceFunctionCode);
            _consents = new ConsentListViewModel { consentFileData = lastestConsents };
            acceptedConsents.Value = _consents.GetCsv();
        }
    }

    private void UpdateCulture()
    {
        if (Region != null)
        {
            if (UserBelongsToRegion(RegionalizeService.DefaultRegion) ||
                _slamContext.GetTagTree().FindNodes(n => n.Identifier?.ToLower().Contains(CurrentLanguage) ?? false).Any())
            {
                RegionalizeService.SetCurrentCulture(Region, CurrentLanguage);
            }
            else
            {
                RegionalizeService.SetCurrentCulture(Region, RegionalizeService.DefaultLanguage);
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("/portal/Registration/login.aspx");
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        /* RegistrationServices.VerifyUserIsRegistered.  
           On submit, first check if GetUserNameForEmail returns nothing, if it returns a username then call
           VerifyUserIsRegistered and show the appropriate error message as described in RD.  If VerifyUserExists was false, 
           if RegisterUser is true, pass user to ~/checkemail/{email}* (yes that is an asterisk, put it in the route so the routing
           will accept all characters in the email address just in case), otherwise show the error as described  in the RD.*/

        UpdateCulture();
        UpdateConsentLanguage();

        lblError.Text = string.Empty;
        lblError.Visible = false;
        placeHolderMessage.Visible = false;
        if (ValidateForm())
        {
            string firstName = txtFirstName.Text.Trim();
            string lastName = txtLastName.Text.Trim();
            string title = txtTitle.Text.Trim();
            string issuerName = txtIssuerName.Text.Trim();
            string processor = txtProcessor.Text.Trim();
            string phone = txtPhone.Text.Trim();
            string mobile = txtMobile.Text.Trim();
            string fax = txtFax.Text.Trim();
            string email = txtEmail.Text.Trim();
            string country = ddlCountry.SelectedValue.Trim();
            string language = ddlLanguage.SelectedValue.Trim();
            string address1 = txtAddress1.Text.Trim();
            string address2 = txtAddress2.Text.Trim();
            string address3 = txtAddress3.Text.Trim();
            string city = txtCity.Text.Trim();
            string state = (!(Region?.Equals("us", StringComparison.OrdinalIgnoreCase) ?? true) ? txtState.Text?.Trim() : ddlState.SelectedItem.Text);
            string zipCode = txtZipCode.Text.Trim();
            bool acceptedEmailSubscriptions = chkEmailAcceptance.Checked;
            lblError.Visible = false;

            var issuer = _issuerService.GetIssuerByDomain(email.GetEmailDomain(), Region);
            if (issuer != null)
            {
                var user = _userService.CreateUser(
                    firstName,
                    lastName,
                    issuerName,
                    address1,
                    address2,
                    address3,
                    city,
                    state,
                    zipCode,
                    phone,
                    fax,
                    mobile,
                    email,
                    issuer.IssuerId,
                    processor,
                    title,
                    country,
                    language,
                    Region,
                    acceptedConsents.Value,
                    acceptedEmailSubscriptions
                );

                if (user != null)
                {
                    if (_settingsService.GetPardotIntegrationEnabled(Region))
                    {
                        _pardotIntegrationService.SubmitPardotForm(_pardotIntegrationService.GetPardotIntegrationModel(
                            Region,
                            firstName,
                            lastName,
                            title,
                            issuerName,
                            processor,
                            phone,
                            mobile,
                            fax,
                            email,
                            country,
                            language,
                            address1,
                            address2,
                            address3,
                            city,
                            state,
                            zipCode,
                            acceptedConsents.Value,
                            acceptedEmailSubscriptions
                        ));
                    }

                    _emailService.SendEmailVerificationEmail(email, user.Profile.FirstName, user.UserName, this.GetSiteName(), user.UserId, user.Region, user.Language, user.Profile.LastName);
                    Response.Redirect(_urlService.GetCheckEmailURL(email, user.Region));
                }
                else
                {
                    lblError.Visible = true;
                    placeHolderMessage.Visible = true;
                    lblError.Text = Resources.Errors.RegistrationFailed;
                }

                if (string.IsNullOrEmpty(acceptedConsents.Value))
                {
                    //We are raising a hard error here. The reason for this is that we MUST have consents, if not we're not complying with GDPR rules.
                    throw new Exception("Missing consent document.");
                }
            }
            else
            {
                lblError.Visible = true;
                placeHolderMessage.Visible = true;
                lblError.Text = Resources.Errors.RegistrationFailed;
            }
        }
        else
        {
            placeHolderMessage.Visible = true;
            lblError.Visible = true;
        }

        if (!string.IsNullOrEmpty(lblError.Text))
        {
            FocusOn(lblError);
        }
    }

    private void FocusOn(Control control)
    {
        ClientScript.RegisterClientScriptBlock(GetType(), Guid.NewGuid().ToString(), "$(document).ready(function(){ window.removeEventListener('load',WebForm_RestoreScrollPosition); });", true);
        ClientScript.RegisterClientScriptBlock(GetType(), Guid.NewGuid().ToString(), "$(document).ready(function(){ window.onload=function(){$(document).scrollTop($('#" + control.ClientID + "').closest('form').offset().top)}});", true);
    }

    protected void FillStates()
    {
        ddlState.DataSource = _regionService.GetStates().ToList();
        ddlState.DataTextField = "StateName";
        ddlState.DataValueField = "StateID";

        ddlState.DataBind();
        ddlState.Items.Insert(0, new ListItem(Resources.Forms.DropdownSelect, "0"));
    }

    protected void FillCountries(string selectedKey = null)
    {
        RegionalizeService.Countries = new ConcurrentDictionary<string, string>(_tagServices.GetAllCountries(CurrentLanguage));
        ddlCountry.DataSource = RegionalizeService.Countries.OrderBy(c => c.Value);
        ddlCountry.DataTextField = "Value";
        ddlCountry.DataValueField = "Key";
        ddlCountry.DataBind();
        if (ddlCountry.Items.Count > 1)
        {
            ddlCountry.Items.Insert(0, new ListItem(Resources.Forms.DropdownSelect, "0"));
        }
        if (!string.IsNullOrEmpty(selectedKey))
        {
            ddlCountry.Items.FindByValue(selectedKey).Selected = true;
        }
    }

    protected ConcurrentDictionary<string, IDictionary<string, string>> GetAllLanguages(string language)
    {
        ConcurrentDictionary<string, IDictionary<string, string>> temp = new ConcurrentDictionary<string, IDictionary<string, string>>();
        foreach (var reg in AllRegions)
        {
            temp.TryAdd(reg.IdTrimmed, GetLanguages(reg.IdTrimmed, language));
        }

        RegionalizeService.Languages = new ConcurrentDictionary<string, string>(temp.SelectMany(c => c.Value).Distinct());

        return temp;
    }

    protected IDictionary<string, string> GetLanguages(string region, string language)
    {
        var languages = _sitemap.GetLanguages(region, language);
        if (!languages.Any())
        {
            languages = _sitemap.GetLanguages(region, RegionalizeService.DefaultLanguage);
        }

        return languages;
    }

    protected void FillLanguages(string region)
    {
        IDictionary<string, string> dict = new Dictionary<string, string>();
        AllLanguages.TryGetValue(region, out dict);

        ddlLanguage.DataSource = dict;
        ddlLanguage.DataTextField = "Value";
        ddlLanguage.DataValueField = "Key";
        ddlLanguage.DataBind();
        if (ddlLanguage.Items.Count > 1)
        {
            ddlLanguage.Items.Insert(0, new ListItem(Resources.Forms.DropdownSelect, "0"));
            ddlLanguage.Enabled = true;
        }
        else if (ddlLanguage.Items.Count == 1)
        {
            ddlLanguage.SelectedIndex = 0;
            ddlLanguage.Enabled = false;
        }
    }

    private TagTreeNode FindParent(TagTreeNode node, string cty)
    {
        if (node?.Identifier.ToLower() == cty?.ToLower() && cty != null)
        {
            if (node?.Parent?.TagCategory?.TagCategoryId?.ToLower() != MarketingCenterDbConstants.TagCategories.Markets)
            {
                return node?.Parent;
            }
            else
            {
                return node;
            }
        }

        foreach (var reg in node?.Children)
        {
            var final = FindParent(reg, cty);
            if (final != null)
            {
                return reg.Parent;
            }
        }

        return null;
    }

    protected string GetRegion(string country)
    {
        if (country == null || (country == "0" && (ddlCountry.Items.Count > 1)))
        {
            return null;
        }

        TagTreeNode temp = null;

        foreach (var reg in GetRegions())
        {
            temp = FindParent(reg, country);
            while (temp != null && temp?.Parent != null && temp?.Parent?.TagCategory?.TagCategoryId?.ToLower() != MarketingCenterDbConstants.TagCategories.Markets)
            {
                temp = temp.Parent;
            }
            if (temp != null)
            {
                break;
            }
        }

        return AllRegions.FirstOrDefault(r => r.TagId.Equals(temp?.Tag?.TagId, StringComparison.InvariantCultureIgnoreCase))?.IdTrimmed;
    }

    protected IList<TagTreeNode> GetRegions()
    {
        return _slamContext.GetTagTree()?.FindNode(tt => (tt.TagCategory != null ? tt.TagCategory.TagCategoryId.Equals(MarketingCenterDbConstants.TagCategories.Markets, StringComparison.OrdinalIgnoreCase) : false))?.Children;
    }

    public bool UserBelongsToRegion(string ExpectedRegion)
    {
        if (string.IsNullOrEmpty(ExpectedRegion))
        {
            return false;
        }

        return Region?.Equals(ExpectedRegion.Trim(), StringComparison.OrdinalIgnoreCase) ?? false;
    }

    private void ValidateTextBoxesForHtmlContent(ControlCollection collection)
    {
        foreach (Control control in collection)
        {
            if (control is TextBox)
            {
                var textBox = control as TextBox;
                textBox.Text = textBox.Text.StripHtml();
            }
            if (control.Controls != null)
            {
                ValidateTextBoxesForHtmlContent(control.Controls);
            }
        }
    }

    public bool ValidateForm()
    {
        ValidateTextBoxesForHtmlContent(Controls);

        var errors = new List<string>();

        if (txtFirstName.Text.Trim().Length == 0)
        {
            errors.Add(!string.IsNullOrEmpty(Resources.Errors.FirstNameEmpty) ? Resources.Errors.FirstNameEmpty : HttpContext.GetGlobalResourceObject("Errors", "FirstNameEmpty", new CultureInfo(RegionalizeService.DefaultLanguage)).ToString());
        }

        if (txtLastName.Text.Trim().Length == 0)
        {
            errors.Add(!string.IsNullOrEmpty(Resources.Errors.LastNameEmpty) ? Resources.Errors.LastNameEmpty : HttpContext.GetGlobalResourceObject("Errors", "LastNameEmpty", new CultureInfo(RegionalizeService.DefaultLanguage)).ToString());
        }

        if (txtTitle.Text.Trim().Length == 0)
        {
            errors.Add(!string.IsNullOrEmpty(Resources.Errors.TitleEmpty) ? Resources.Errors.TitleEmpty : HttpContext.GetGlobalResourceObject("Errors", "TitleEmpty", new CultureInfo(RegionalizeService.DefaultLanguage)).ToString());
        }

        if (txtIssuerName.Text.Trim().Length == 0)
        {
            errors.Add(!string.IsNullOrEmpty(Resources.Errors.FinancialInstitutionEmpty) ? Resources.Errors.FinancialInstitutionEmpty : HttpContext.GetGlobalResourceObject("Errors", "FinancialInstitutionEmpty", new CultureInfo(RegionalizeService.DefaultLanguage)).ToString());
        }

        if (txtAddress1.Text.Trim().Length == 0 && (UserBelongsToRegion("us") || UserBelongsToRegion("de")))
        {
            errors.Add(!string.IsNullOrEmpty(Resources.Errors.Address1Empty) ? Resources.Errors.Address1Empty : HttpContext.GetGlobalResourceObject("Errors", "Address1Empty", new CultureInfo(RegionalizeService.DefaultLanguage)).ToString());
        }

        if (txtCity.Text.Trim().Length == 0 && (UserBelongsToRegion("us") || UserBelongsToRegion("de")))
        {
            errors.Add(!string.IsNullOrEmpty(Resources.Errors.CityEmpty) ? Resources.Errors.CityEmpty : HttpContext.GetGlobalResourceObject("Errors", "CityEmpty", new CultureInfo(RegionalizeService.DefaultLanguage)).ToString());
        }

        if (ddlState.SelectedIndex == -1 && string.IsNullOrEmpty(txtState.Text) && UserBelongsToRegion("us"))
        {
            errors.Add(!string.IsNullOrEmpty(Resources.Errors.StateEmpty) ? Resources.Errors.StateEmpty : HttpContext.GetGlobalResourceObject("Errors", "StateEmpty", new CultureInfo(RegionalizeService.DefaultLanguage)).ToString());
        }

        if (txtZipCode.Text.Trim().Length == 0 && (UserBelongsToRegion("us") || UserBelongsToRegion("de")))
        {
            errors.Add(!string.IsNullOrEmpty(Res.ZipCodeEmpty) ? Res.ZipCodeEmpty : HttpContext.GetGlobalResourceObject("Errors", "ZipCodeEmptyUS", new CultureInfo(RegionalizeService.DefaultLanguage)).ToString());
        }

        if (txtPhone.Text.Trim().Length == 0)
        {
            errors.Add(!string.IsNullOrEmpty(Resources.Errors.PhoneEmpty) ? Resources.Errors.PhoneEmpty : HttpContext.GetGlobalResourceObject("Errors", "PhoneEmpty", new CultureInfo(RegionalizeService.DefaultLanguage)).ToString());
        }

        if (((ddlCountry.SelectedValue.Trim().Length == 0) || (ddlCountry.SelectedValue == "0") || Region == null) && ddlCountry.Items.Count > 1)
        {
            errors.Add(!string.IsNullOrEmpty(Resources.Errors.CountryEmpty) ? Resources.Errors.CountryEmpty : HttpContext.GetGlobalResourceObject("Errors", "CountryEmpty", new CultureInfo(RegionalizeService.DefaultLanguage)).ToString());
        }

        if (((ddlLanguage.SelectedValue.Trim().Length == 0) || (ddlLanguage.SelectedValue == "0")) && ddlLanguage.Items.Count > 1)
        {
            errors.Add(!string.IsNullOrEmpty(Resources.Errors.LanguageEmpty) ? Resources.Errors.LanguageEmpty : HttpContext.GetGlobalResourceObject("Errors", "LanguageEmpty", new CultureInfo(RegionalizeService.DefaultLanguage)).ToString());
        }

        if ((ddlState.SelectedIndex == -1 || ddlState.SelectedValue == "0") && UserBelongsToRegion("us"))
        {
            errors.Add(!string.IsNullOrEmpty(Resources.Errors.StateEmpty) ? Resources.Errors.StateEmpty : HttpContext.GetGlobalResourceObject("Errors", "StateEmpty", new CultureInfo(RegionalizeService.DefaultLanguage)).ToString());
        }

        if (Region != "us" && !chkTermsAcceptance.Checked)
        {
            errors.Add(!string.IsNullOrEmpty(Resources.Errors.TermsAcceptanceNotConsented) ? Resources.Errors.TermsAcceptanceNotConsented : HttpContext.GetGlobalResourceObject("Errors", "TermsAcceptanceNotConsented", new CultureInfo(RegionalizeService.DefaultLanguage)).ToString());
        }

        if (Region == "us" && !chkEmailAcceptance.Checked)
        {
            errors.Add(!string.IsNullOrEmpty(Resources.Errors.EmailAcceptanceNotConsented) ? Resources.Errors.EmailAcceptanceNotConsented : HttpContext.GetGlobalResourceObject("Errors", "EmailAcceptanceNotConsented", new CultureInfo(RegionalizeService.DefaultLanguage)).ToString());
        }

        var email = txtEmail.Text.Trim();
        if (email.Length == 0)
        {
            errors.Add(!string.IsNullOrEmpty(Resources.Errors.EmailAddressEmpty) ? Resources.Errors.EmailAddressEmpty : HttpContext.GetGlobalResourceObject("Errors", "EmailAddressEmpty", new CultureInfo(RegionalizeService.DefaultLanguage)).ToString());
        }
        else
        {
            Regex r = new Regex(@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$", RegexOptions.IgnoreCase);

            if (!r.IsMatch(email))
            {
                errors.Add(!string.IsNullOrEmpty(Resources.Errors.EmailAddressInvalid) ? Resources.Errors.EmailAddressInvalid : HttpContext.GetGlobalResourceObject("Errors", "EmailAddressInvalid", new CultureInfo(RegionalizeService.DefaultLanguage)).ToString());
                errors.Add(!errors.Any() ? "Error" : string.Empty);
            }
            else
            {
                var userName = _registrationService.GetUserNameForEmail(email);
                if (!string.IsNullOrEmpty(userName))
                {
                    if (_registrationService.VerifyUserCompletedRegistration(userName) == false)
                    {
                        errors.Add(string.Format(Resources.Errors.Register_AlreadyRegistered, email));
                        errors.Add(!errors.Any() ? "Error" : string.Empty);
                    }
                    else
                    {
                        errors.Add(string.Format(Resources.Errors.Register_AlreadyRegisteredAndApproved, email));
                        errors.Add(!errors.Any() ? "Error" : string.Empty);
                    }
                }

                if (!errors.Any())
                {
                    var previousPendingRegistrations = _pendingRegistrationService.GetPendingRegistrationsByEmail(email);
                    if (previousPendingRegistrations.Any())
                    {
                        errors.Add(Resources.Forms.Register_PreviousPendingRegistrations);
                    }
                }

                var domain = email.GetEmailDomain(); // get Domain Name
                var issuer = _issuerService.GetIssuerByDomain(domain, Region); // check User domain exists on Issuer table, this method returns ListId of given domain if Find otherwise return empty
                if (issuer == null || issuer.RequiresRegistrationReview && _settingsService.GetPendingReviewRegistrationEnabledByRegion(Region)) // if domain name does not exists or exist but the issuer requires registration review
                {
                    if (!_issuerService.VerifyDomainIsValid(domain))
                    {
                        var otherRequiredFieldsCompleted = !errors.Any();

                        errors.Add(string.Format(Resources.Errors.Register_BlacklistDomain, domain, _settingsService.GetRegisterHelpTo(Region ?? "us")));
                        errors.Add(!errors.Any() ? "Error" : string.Empty);

                        if (otherRequiredFieldsCompleted)
                        {
                            _emailService.SendEmailBadDomainEmail(
                                GetStringOrSpaceIfEmpty(txtFirstName.Text),
                                GetStringOrSpaceIfEmpty(txtLastName.Text),
                                GetStringOrSpaceIfEmpty(txtIssuerName.Text),
                                GetStringOrSpaceIfEmpty(txtPhone.Text),
                                GetStringOrSpaceIfEmpty(txtMobile.Text),
                                GetStringOrSpaceIfEmpty(txtFax.Text),
                                GetStringOrSpaceIfEmpty(email),
                                GetStringOrSpaceIfEmpty(txtAddress1.Text),
                                GetStringOrSpaceIfEmpty(txtAddress2.Text),
                                GetStringOrSpaceIfEmpty(txtAddress3.Text),
                                GetStringOrSpaceIfEmpty(txtCity.Text),
                                string.IsNullOrEmpty(txtState.Text) ? ((ddlState.SelectedIndex > 0) ? GetStringOrSpaceIfEmpty(ddlState.SelectedItem.Text) : " ") : txtState.Text,
                                GetStringOrSpaceIfEmpty(txtZipCode.Text),
                                GetStringOrSpaceIfEmpty(ddlCountry.SelectedItem.Text),
                                this.GetSiteName(),
                                Region,
                                GetStringOrSpaceIfEmpty(ddlLanguage.SelectedItem.Text)
                            );
                        }
                    }
                    else // its a valid pre-approved domain
                    {
                        if (!errors.Any())
                        {
                            errors.Add(string.Format(Resources.Forms.Register_SuccessPendingRegistration, _settingsService.GetRegisterHelpSecondTo(Region ?? "us"), this.GetSiteName()));

                            _pendingRegistrationService.AddPendingRegistration(
                                txtFirstName.Text.Trim(),
                                txtLastName.Text.Trim(),
                                txtIssuerName.Text.Trim(),
                                txtAddress1.Text.Trim(),
                                txtAddress2.Text.Trim(),
                                txtAddress3.Text.Trim(),
                                txtCity.Text.Trim(),
                                new Regex("\\d").IsMatch(ddlState.SelectedItem.Value) && ddlState.SelectedValue != "0" ? int.Parse(ddlState.SelectedItem.Value) : 1,
                                txtZipCode.Text.Trim(),
                                txtPhone.Text.Trim(),
                                txtFax.Text.Trim(),
                                txtMobile.Text.Trim(),
                                email,
                                txtTitle.Text.Trim(),
                                txtProcessor.Text.Trim(),
                                ddlCountry.SelectedValue.Trim(),
                                ddlLanguage.SelectedValue.Trim(),
                                Region?.Trim(),
                                (!UserBelongsToRegion("us") ? txtState.Text : ""),
                                this.GetSiteName(),
                                acceptedConsents.Value,
                                chkEmailAcceptance.Checked
                            );

                            errors.ForEach(error => lblError.Text += error + "<br/>");
                            return false;
                        }
                        else
                        {
                            errors.ForEach(error => lblError.Text += error + "<br/>");
                            return false;
                        }
                    }
                }
            }
        }

        if (errors.Any())
        {
            errors.ForEach(error => lblError.Text += error + "<br/>");
            return false;
        }
        else
        {
            return true;
        }
    }

    private string GetStringOrSpaceIfEmpty(string s)
    {
        return (string.IsNullOrEmpty(s)) ? " " : s;
    }

    public void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        UpdateCulture();

        if ((((DropDownList)sender).Items.Count > 0 && (((DropDownList)sender).SelectedValue == "0")))
        {
            return;
        }

        lblError.Text = string.Empty;
        var newRegion = GetRegion(((DropDownList)sender).SelectedValue);
        if (newRegion == null)
        {
            return;
        }

        Region = newRegion;
        regionHidden.Value = Region;
        UpdateCulture();
        AllLanguages = GetAllLanguages(ddlCountry.SelectedValue.Substring(0, 2));
        FillLanguages(Region);
        UpdateDropdownSelect();
        UpdateConsentLanguage();
        UpdateMasterPageDynamicContent();
        UncheckEmailAcceptance();
        UncheckTermsAcceptance();
    }

    private void UpdateMasterPageDynamicContent()
    {
        if (Master is MMPRegistration)
        {
            _userContext.SetUserSelectedLanguage(CurrentLanguage);
            var masterPage = Master as MMPRegistration;
            masterPage.Locale = string.IsNullOrEmpty(Region) ? null : Region.Substring(0, 2);
            masterPage.LoadDynamicContent();
        }
    }

    protected void ddlLanguage_SelectedIndexChanged(object sender, EventArgs e)
    {
        UpdateConsentLanguage();
        UncheckEmailAcceptance();
        UncheckTermsAcceptance();
        UpdateDropdownSelect();
    }

    private void UncheckEmailAcceptance()
    {
        chkEmailAcceptance.Checked = false;
    }

    private void UncheckTermsAcceptance()
    {
        chkTermsAcceptance.Checked = false;
    }

    private void CreateCaptchaBinding()
    {
        Captcha = _captchaService.BindInvisibleReCaptcha();
    }

    private void UpdateDropdownSelect()
    {
        UpdateCulture();

        if (ddlCountry.Items.Count > 1)
        {
            ddlCountry.Items[0].Text = Resources.Forms.DropdownSelect;
        }

        if (ddlState.Items.Count > 1)
        {
            ddlState.Items[0].Text = Resources.Forms.DropdownSelect;
        }

        if (ddlLanguage.Items.Count > 1)
        {
            ddlLanguage.Items[0].Text = Resources.Forms.DropdownSelect;
        }
    }

    protected void btnVerificationEmail_Click(object sender, EventArgs e)
    {
        var user = _userService.GetUserByEmail(txtEmail.Text.Trim());
        if (user != null)
        {
            _emailService.SendEmailVerificationEmail(user.Email, user.Profile.FirstName, user.UserName, this.GetSiteName(), user.UserId, user.Region, user.Language, user.Profile.LastName);
            Response.Redirect(_urlService.GetCheckEmailURL(user.Email, user.Region));
        }
    }
}