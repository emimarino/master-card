﻿<%@ Page Language="C#" MasterPageFile="MMPRegistration.master" AutoEventWireup="true" Inherits="RegisterUser" Title="Register" CodeBehind="RegisterUser.aspx.cs" MaintainScrollPositionOnPostback="true" %>

<%@ Import Namespace="System.Threading" %>
<%@ Import Namespace="Mastercard.MarketingCenter.Web.Core.Extensions" %>

<asp:Content ID="Content3" ContentPlaceHolderID="HeadThemePlaceHolder" runat="server">
    <%= System.Web.Optimization.Styles.Render(DependencyResolver.Current.GetService<Mastercard.MarketingCenter.Services.Interfaces.IUrlService>().GetStaticContentCss()) %>
    <!-- libs -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/portal/content/themes/orange/libs/bootstrap.modal.min.js"></script>
    <script src="/portal/content/themes/orange/libs/slick/slick.min.js"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadJavascriptPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="content-spaces-in spaces-vertical-in">
        <header class="content-header intro">
            <h1 style="margin-bottom: 0;"><%=string.Format(Resources.Forms.Register_Subtitle, this.GetSiteName())%></h1>
            <p><%=Resources.Forms.Register_Description%></p>
        </header>
        <div class="form">
            <%= System.Web.Helpers.AntiForgery.GetHtml() %>
            <asp:PlaceHolder ID="placeHolderMessage" runat="server" Visible="false">
                <p class="alert alert-warning">
                    <asp:Label ID="lblError" runat="server" CssClass="span_1" Visible="False"></asp:Label>
                    <asp:Button ID="btnVerificationEmail" runat="server" OnClick="btnVerificationEmail_Click" Style="display: none" />
                    <script type="text/javascript">
                        function sendVerificationEmail() {
                            $("input[id$='btnVerificationEmail']").click();
                        }
                    </script>
                </p>
            </asp:PlaceHolder>
            <div class="row columns">
                <div class="column col-sm-5">
                    <div class="form-group">
                        <label class="form-label"><%=Resources.Forms.FirstName%><span class="req-mark">*</span></label>
                        <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" autocomplete="new-firstname"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="form-label"><%=Resources.Forms.LastName%><span class="req-mark">*</span></label>
                        <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" autocomplete="new-lastname"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="form-label"><%=Resources.Forms.Title%><span class="req-mark">*</span></label>
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="form-control" autocomplete="new-title"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="form-label"><%=Resources.Forms.FinancialInstitution%><span class="req-mark">*</span></label>
                        <asp:TextBox ID="txtIssuerName" runat="server" CssClass="form-control" autocomplete="new-issuername"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="form-label">
                            <%=Resources.Forms.Processor%>
                        </label>
                        <label class="label-help"><%= Resources.Forms.ProcessorsHelp %></label>
                        <asp:TextBox ID="txtProcessor" runat="server" CssClass="form-control" autocomplete="new-processor"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="form-label"><%=Resources.Forms.Phone%> <span class="req-mark">*</span> </label>
                        <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" autocomplete="new-phone"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="form-label"><%=Resources.Forms.Mobile%></label>
                        <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control" autocomplete="new-mobile"></asp:TextBox>
                    </div>
                    <% if (Region == null || (Region.ToLower() == "us"))
                        {%>
                    <div class="form-group">
                        <label class="form-label"><%=Resources.Forms.Fax%></label>
                        <asp:TextBox ID="txtFax" runat="server" CssClass="form-control" autocomplete="new-fax"></asp:TextBox>
                    </div>
                    <% } %>
                    <div class="form-group">
                        <label class="form-label"><%=Resources.Forms.BusinessEmailAddress%><span class="req-mark">*</span></label>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" autocomplete="new-email"></asp:TextBox>
                    </div>
                </div>
                <div class="cols-separator col-sm-2"></div>
                <div class="column col-sm-5">
                    <div class="form-group">
                        <label class="form-label"><%=Resources.Forms.Country%><span class="req-mark">*</span></label>
                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true" autocomplete="new-country"></asp:DropDownList>
                    </div>

                    <asp:HiddenField ID="regionHidden" runat="server" />

                    <div class="form-group">
                        <label class="form-label"><%=Resources.Forms.Language%><span class="req-mark">*</span></label>
                        <asp:DropDownList ID="ddlLanguage" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged" autocomplete="new-language"></asp:DropDownList>
                    </div>

                    <div class="form-group">
                        <label class="form-label">
                            <%=Resources.Forms.Address1%>
                            <% if (Region != null && (Region.ToLower() == "us" || Region.ToLower() == "de"))
                                {%><span class="req-mark">*</span><%} %>
                        </label>
                        <label class="label-help"><%= Resources.Forms.Address1Help %> </label>
                        <asp:TextBox ID="txtAddress1" runat="server" CssClass="form-control" autocomplete="new-address1"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="form-label"><%=Resources.Forms.Address2%></label>
                        <asp:TextBox ID="txtAddress2" runat="server" CssClass="form-control" autocomplete="new-address2"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="form-label"><%=Resources.Forms.Address3%></label>
                        <asp:TextBox ID="txtAddress3" runat="server" CssClass="form-control" autocomplete="new-address3"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="form-label">
                            <%=Resources.Forms.City%>
                            <% if (Region != null && (Region.ToLower() == "us" || Region.ToLower() == "de"))
                                {%><span class="req-mark">*</span><%} %>
                        </label>
                        <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" autocomplete="new-city"></asp:TextBox>
                    </div>

                    <%if (Region != null && Region.ToLower().Equals("US".ToLower()))
                        { %>
                    <div class="form-group">
                        <label class="form-label"><%=Resources.Forms.StateUS %><span class="req-mark">*</span></label>
                        <asp:DropDownList ID="ddlState" runat="server" CssClass="form-control" autocomplete="new-state"></asp:DropDownList>
                    </div>
                    <%} %>

                    <%else
                        {%>
                    <div class="form-group">
                        <label class="form-label"><%=Resources.Forms.State %></label>
                        <asp:TextBox ID="txtState" runat="server" CssClass="form-control" autocomplete="new-state"></asp:TextBox>
                    </div>

                    <%} %>

                    <div class="form-group">
                        <%if (Region != null && (Region.ToLower() == "us"))
                            {%>
                        <label class="form-label"><%=Resources.Forms.ZipCodeUS %><span class="req-mark">*</span></label>
                        <%} %>
                        <%else
                            {%>
                        <label class="form-label">
                            <%=Resources.Forms.ZipCode %>
                            <% if (Region != null && Region.ToLower() == "de")
                                {%><span class="req-mark">*</span><%} %>
                        </label>
                        <%} %>
                        <asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control" autocomplete="new-zipcode"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="form-group btn-group">
                <%
                    if (Captcha.IsConfigurationValid())
                    {
                %>
                <div id="CaptchaComponent"
                    class="g-recaptcha"
                    data-badge="bottomleft"
                    data-sitekey="<%= Captcha.Parameters["SiteKey"] %>"
                    data-callback="validateCaptcha"
                    data-size="invisible"
                    data-bind="ContentPlaceHolder1_btnSubmit">
                </div>
                <%: System.Web.Optimization.Scripts.Render(DependencyResolver.Current.GetService<Mastercard.MarketingCenter.Services.Interfaces.IUrlService>().GetStaticCaptchaValidatorJs()) %>
                <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                <%
                    }
                %>
                <div class="control-label">
                    <span class="req-mark">* <%= Resources.Forms.RequiredField  %></span>
                </div>
                <%
                    if (_consents != null && _consents.Any())
                    {
                        if (Region == "us")
                        {
                %>
                <label class="form-label"><%=_consents.ConsentLanguageText %></label>
                <%
                    }
                    if (Region != "us")
                    {
                %>
                <label class="form-label">
                    <input type="checkbox" id="chkTermsAcceptance" runat="server" />
                    <%= TermsOfUseAgreement +((Region == "us")?"":"<span class=\"req-mark\">* </span>") %>
                </label>
                <%
                    }
                %>
                <label class="form-label">
                    <input type="checkbox" id="chkEmailAcceptance" runat="server" />
                    <%= EmailMarketingAgreement +((Region == "us")?"<span class=\"req-mark\">* </span>":"") %>
                </label>
                <%      
                    if (Region == "us")
                    {
                %>
                <label class="form-label">
                    <%= EmailMarketingAgreementWithdraw %>
                </label>
                <%
                    }
                    if (Region != "us")
                    {
                %>
                <label class="form-label"><%=_consents.ConsentLanguageText %></label>
                <%
                        }
                    }
                %>
                <asp:HiddenField ID="acceptedConsents" runat="server" />
                <asp:LinkButton ID="btnSubmit" CssClass="btn" OnClick="btnSubmit_Click" runat="server"><%= Resources.Shared.Submit %></asp:LinkButton>
                <asp:LinkButton ID="btnCancel" CssClass="btn" OnClick="btnCancel_Click" runat="server"><%= Resources.Shared.Cancel %></asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
