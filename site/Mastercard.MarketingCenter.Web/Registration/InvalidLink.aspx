﻿<%@ Page Language="C#" MasterPageFile="~/Registration/MMPRegistration.master" AutoEventWireup="true" Inherits="Registration_InvalidLink" Title="Invalid Link" CodeBehind="InvalidLink.aspx.cs" %>
<asp:Content ID="Content3" ContentPlaceHolderID="HeadThemePlaceHolder" runat="server">
    <% var themePath = "/portal/content/themes/orange/"; %>
    <!-- libs -->
    <link rel="stylesheet" href="<%=themePath%>libs/slick/slick.css" />
    <!-- custom-->
    <link rel="stylesheet" href="<%=themePath%>css/fonts/fonts.css" />
    <link rel="stylesheet" href="<%=themePath%>css/font-awesome.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/bootstrap-grid.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/bootstrap-modal.min.css" />
    <link rel="stylesheet" href="<%=themePath%>css/main.css" />
    <link rel="stylesheet" href="<%=themePath%>css/responsive.css" />
    <!-- libs -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<%=themePath%>libs/bootstrap.modal.min.js"></script>
    <script src="<%=themePath%>libs/slick/slick.min.js"></script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content-spaces-in spaces-vertical-in">
        <div class="row">
            <header class="content-header intro col-lg-10">
                <h1 style="margin-bottom: 0;"><%=Resources.Forms.InvalidLink_Subtitle%></h1>
                <p><%=Resources.Forms.InvalidLink_Description%></p>
            </header>
        </div>
    </div>
</asp:Content>