﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Web.Core;
using Mastercard.MarketingCenter.Web.Core.Extensions;
using Mastercard.MarketingCenter.Web.Core.Services;
using Slam.Cms.Data;
using System;
using System.Linq;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.UI;

public partial class MMPRegistration : MasterPage
{
    protected string SiteName
    {
        get
        {
            return Page.GetSiteName();
        }
    }

    protected string DefaultLanguage { get { return System.Configuration.ConfigurationManager.AppSettings["DefaultLanguage"]; } }
    protected string DefaultRegion { get { return System.Configuration.ConfigurationManager.AppSettings["DefaultRegion"]; } }
    public string Locale { get; set; }

    private SlamContext _slamContext { get { return DependencyResolver.Current.GetService<SlamContext>(); } }
    private UserContext _userContext { get { return DependencyResolver.Current.GetService<UserContext>(); } }
    private IPageTrackingService _pageTrackingService { get { return DependencyResolver.Current.GetService<IPageTrackingService>(); } }
    private ISettingsService _settingsService { get { return DependencyResolver.Current.GetService<ISettingsService>(); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (WebConfigurationManager.AppSettings["EnableSSL"] != null && Request.Url.Scheme != Uri.UriSchemeHttps && WebConfigurationManager.AppSettings["EnableSSL"].ToLower() == "true")
        {
            Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"] + Request.RawUrl);
        }

        if (!this.IsPostBack)
        {
            ResetUserPreferences();
            _pageTrackingService.TrackPageAccess();
        }

        LoadDynamicContent();

    }

    private void ResetUserPreferences()
    {
        if (!Request.IsAuthenticated)
        {
            string targetLanguage = MastercardSitemap.AvailableLanguages.ContainsKey(_userContext.BrowserLanguage) ? _userContext.BrowserLanguage : DefaultLanguage;
            if (_userContext.SelectedLanguage != targetLanguage || _userContext.SelectedRegion != DefaultRegion)
            {
                _userContext.SetUserSelectedLanguage(targetLanguage);
                _userContext.SetUserSelectedRegion(DefaultRegion);
                Response.Redirect(Request.Url.ToString());
            }
        }
    }

    public void LoadDynamicContent()
    {
        var registrationLayoutHeaderPage = _slamContext.CreateQuery()
                                                       .FilterRegion(RegionalizeService.UnauthenticatedRegion)
                                                       .Filter($"Page.Uri LIKE '%{_settingsService.GetSiteApplicationShortName().ToLower()}/registrationlayoutheader%'")
                                                       .FilterTaggedWithAny(new string[] { _userContext.SelectedLanguage, DefaultLanguage }, true)
                                                       .OrderBy($"CASE WHEN cift.Identifier = '{_userContext.SelectedLanguage}' THEN 0 ELSE 1 END")
                                                       .Get<PageDTO>()
                                                       .FirstOrDefault();

        RegistrationLayoutHeaderLiteral.Text = registrationLayoutHeaderPage?.IntroCopy;

        var registrationLayoutFooterPage = _slamContext.CreateQuery()
                                                       .FilterRegion(RegionalizeService.UnauthenticatedRegion)
                                                       .Filter($@"({(Locale.IsNullOrWhiteSpace() ? string.Empty : $"Page.Uri LIKE '%{Locale}/registrationlayoutfooter%' OR")}
                                                                  Page.Uri LIKE '%{_settingsService.GetSiteApplicationShortName().ToLower()}/registrationlayoutfooter%')")
                                                       .FilterTaggedWithAny(new string[] { _userContext.SelectedLanguage, DefaultLanguage }, true)
                                                       .OrderBy($@"CASE WHEN Page.Uri LIKE '%{Locale}/registrationlayoutfooter%' THEN 0 ELSE 1 END,
                                                                   CASE WHEN cift.Identifier = '{_userContext.SelectedLanguage}' THEN 0 ELSE 1 END")
                                                       .Get<PageDTO>()
                                                       .FirstOrDefault();

        RegistrationLayoutFooterLiteral.Text = registrationLayoutFooterPage?.IntroCopy;
    }

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.Load += new EventHandler(Page_Load);
    }
}