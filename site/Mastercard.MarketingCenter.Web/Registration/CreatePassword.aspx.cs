﻿using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Data;
using System;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;

public partial class Registration_CreatePassword : System.Web.UI.Page
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        Page.Title = Resources.Forms.CreateAccount_Title;
    }

    public LoginService _loginService { get { return DependencyResolver.Current.GetService<LoginService>(); } }
    public RegistrationService _registrationService { get { return DependencyResolver.Current.GetService<RegistrationService>(); } }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MembershipUser user = MembershipService.GetUser(Page.RouteData.Values["id"].ToString());
            if (user == null || user.IsApproved)
            {
                Response.Redirect("/portal/InvalidLink");
            }

            FillUserDetail();
        }
    }

    private void FillUserDetail()
    {
        MembershipUser user = MembershipService.GetUser(Page.RouteData.Values["id"].ToString());
        if (user != null)
        {
            if (user.IsApproved)
            {
                _loginService.TrackLogin(user.UserName, Membership.Provider.Name);
                FormsAuthentication.RedirectToLoginPage();

                return;
            }

            txtUserName.Text = (user.Email != null) ? user.Email.ToString() : "";
            ViewState["UserName"] = (user.UserName != null) ? user.UserName.ToString() : "";
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        phError.Visible = false;
        lblError.Visible = false;
        lblError.Text = string.Empty;
        if (Page.RouteData.Values["id"] != null && ViewState["UserName"] != null)
        {
            lblError.Text += Mastercard.MarketingCenter.Web.Core.Services.UserService.VerifyNewPasswordIsValid(txtPassword.Text, txtConfirmPassword.Text, txtUserName.Text);

            if (!string.IsNullOrWhiteSpace(lblError.Text))
            {
                lblError.Visible = true;
                phError.Visible = true;
                return;
            }

            _registrationService.SetUserPassword(ViewState["UserName"].ToString(), txtPassword.Text.Trim());

            Response.Redirect(FormsAuthentication.DefaultUrl);
        }
    }
}