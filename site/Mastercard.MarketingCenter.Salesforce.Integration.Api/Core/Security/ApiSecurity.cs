﻿using System.Configuration;

namespace Mastercard.MarketingCenter.Salesforce.Integration.Attributes
{
    public static class ApiSecurity
    {
        public static bool Validate(string userName, string password)
        {
            string user = ConfigurationManager.AppSettings["Username"];
            string pwd = ConfigurationManager.AppSettings["Password"];

            if (userName == user && password == pwd)
            {
                return true;
            }

            return false;
        }
    }
}