﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.DTO.Salesforce;
using Mastercard.MarketingCenter.Salesforce.Integration.Attributes;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Mastercard.MarketingCenter.Salesforce.Integration.Api.Controllers
{
    [BasicAuthentication]
    public class SalesforceController : ApiController
    {
        private readonly ISalesforceService _salesforceService;
        private readonly ISalesforceApiService _salesforceApiService;

        public SalesforceController(ISalesforceService salesforceService, ISalesforceApiService salesforceApiService)
        {
            _salesforceService = salesforceService;
            _salesforceApiService = salesforceApiService;
        }

        [HttpGet]
        [Route("all-salesforce-tasks")]
        public IEnumerable<SalesforceTask> AllSalesforceTasks(DateTime from, DateTime to)
        {
            return _salesforceService.GetAllSalesforceTasks(from, to);
        }

        [HttpGet]
        [Route("salesforce-tasks")]
        public IEnumerable<SalesforceTaskQueryResponseDTO> SalesforceTasks([FromUri] string[] taskIds)
        {
            return _salesforceApiService.GetSalesforceTasks(taskIds);
        }

        [HttpGet]
        [Route("completed-salesforce-tasks")]
        public IEnumerable<object> CompletedSalesforceTasks(DateTime from, DateTime to)
        {
            var completedSalesforceTasks = _salesforceService.GetCompletedSalesforceTasks(from, to);
            return from cst in completedSalesforceTasks
                   join st in _salesforceApiService.GetSalesforceTasks(completedSalesforceTasks.Select(st => st.TaskId)) on cst.TaskId equals st.TaskId
                   select new
                   {
                       SalesforceTask = cst,
                       SalesforceTaskResponse = st
                   };
        }

        [HttpGet]
        [Route("salesforce-contacts")]
        public IEnumerable<SalesforceContactQueryResponseDTO> SalesforceContacts([FromUri] string[] emails)
        {
            return _salesforceApiService.GetSalesforceContacts(emails);
        }
    }
}