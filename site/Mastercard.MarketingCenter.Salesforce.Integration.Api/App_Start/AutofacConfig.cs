﻿using Autofac;
using Autofac.Integration.WebApi;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Salesforce.Integration.Api.Controllers;
using Mastercard.MarketingCenter.Services.Modules;
using System.Web.Http;

namespace Mastercard.MarketingCenter.Salesforce.Integration.Api
{
    public class AutofacConfig
    {
        public static void Register()
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(typeof(SalesforceController).Assembly);
            builder.RegisterType<MarketingCenterDbContext>().InstancePerLifetimeScope();
            builder.RegisterModule<SalesforceIntegrationModule>();

            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(builder.Build()); //Set the WebApi DependencyResolver
        }
    }
}