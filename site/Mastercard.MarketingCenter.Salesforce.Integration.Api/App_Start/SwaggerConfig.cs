using Mastercard.MarketingCenter.Salesforce.Integration.Api;
using Slam.Cms.Configuration;
using Swashbuckle.Application;
using System;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using WebActivatorEx;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace Mastercard.MarketingCenter.Salesforce.Integration.Api
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "Mastercard MarketingCenter Salesforce API");
                        c.BasicAuth("basic").Description("Basic HTTP Authentication");
                        c.OperationFilter<AddAuthorizationHeaderParameterOperationFilter>();
                        c.DescribeAllEnumsAsStrings();
                        c.RootUrl(ResolveBasePath);
                    })
                .EnableSwaggerUi(c =>
                    {
                        c.CustomAsset("index", Assembly.GetAssembly(typeof(SwaggerConfig)), "Mastercard.MarketingCenter.Salesforce.Integration.Api.Content.index.html");
                    });
        }

        private static string ResolveBasePath(HttpRequestMessage message)
        {
            var salesforceUrl = new Uri(ConfigurationManager.Environment.SalesforceUrl);
            var virtualPathRoot = message.GetRequestContext().VirtualPathRoot;
            if (salesforceUrl.Authority.Equals(message.RequestUri.Authority, StringComparison.InvariantCultureIgnoreCase) &&
                !salesforceUrl.Scheme.Equals(message.RequestUri.Scheme, StringComparison.InvariantCultureIgnoreCase))
            {
                return new Uri(new Uri($"{salesforceUrl.Scheme}://{message.RequestUri.Host}", UriKind.Absolute), virtualPathRoot).AbsoluteUri;
            }
            else
            {
                return new Uri(new Uri($"{message.RequestUri.Scheme}://{message.RequestUri.Host}", UriKind.Absolute), virtualPathRoot).AbsoluteUri.TrimEnd('/');
            }
        }
    }
}