﻿using Mastercard.MarketingCenter.Common.Interfaces;
using System;
using System.Web;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Common.Filters
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class FeatureActionAttribute : AuthorizeAttribute
    {
        private readonly bool _anyFeatureIsOn;
        private readonly AbstractFeatureFlag _abstractFeatureFlag = DependencyResolver.Current.GetService<AbstractFeatureFlag>();

        /// <summary>
        /// This Attribute will hide the Action in the case the feature flagas either are off or absent value for the region
        /// </summary>
        /// <param name="featureKeys"> This are the keys of the features that enable this action</param>
        public FeatureActionAttribute(params string[] featureKeys)
        {
            _anyFeatureIsOn = _abstractFeatureFlag.IsAnyOn(featureKeys);
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return _anyFeatureIsOn;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!_anyFeatureIsOn)
            {
                filterContext.Result = new HttpNotFoundResult();
            }
        }
    }
}