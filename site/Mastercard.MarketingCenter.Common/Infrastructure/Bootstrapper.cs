﻿using System;
using System.Linq;
using System.Reflection;

namespace Mastercard.MarketingCenter.Common.Infrastructure
{
    public static class Bootstrapper
    {
        public static void Execute()
        {
            var bootstrappers = Assembly.GetCallingAssembly()
                                        .GetTypes()
                                        .Where(t => t != typeof(IBootstrapper) && typeof(IBootstrapper).IsAssignableFrom(t))
                                        .OrderBy(t => t.Name);

            foreach (var bootstrapper in bootstrappers)
            {
                var instance = Activator.CreateInstance(bootstrapper) as IBootstrapper;
                if (instance == null)
                {
                    throw new Exception($"Cannot create instance of {bootstrapper.FullName}");
                }

                instance.Execute();
            }
        }
    }
}