﻿namespace Mastercard.MarketingCenter.Common.Infrastructure
{
    public static class Constants
    {
        public const string ApiVerificationHash = "ee14b82643b7f48f6363b638e0cd5a9d";
        public const string CheckboxNullValue = "[null]";
        public const string ContentItemIdFieldName = "ContentItemId";
        public const string DateTimeFormat = "MM/dd/yyyy";
        public const string FeatureAdminGroup = "Feature Admin";
        public const string FieldPrefix = "fieldDef.FieldDefinition.";
        public const string IsNewValue = "IsNewValue";
        public const string ShowAsFeaturedLinkValue = "Show as Featured Link";
        public const string TriggerMarketingNotificationsValue = "Trigger Marketing Notifications";
        public const string UserContextCookieSuffix = "uc";

        public static class AnonymousClaims
        {
            public const string Unsubscribe = "Unsubscribe";
        }

        public static class Roles
        {
            public const string DevAdmin = "DevAdmin";
            public const string McAdmin = "MCAdmin";
            public const string Printer = "Printer";
            public const string Processor = "Processor";
            public const string ContentProducer = "ContentProducer";
            public const string ReportAdmin = "ReportAdmin";
            public const string GlobalView = "GlobalView";

            public static readonly string[] AdminRoles = { DevAdmin, McAdmin, Printer, Processor, ContentProducer };
            public static readonly string[] FullAdminRoles = { DevAdmin, McAdmin, ContentProducer };
            public static readonly string[] FrontEndRoles = { ReportAdmin, GlobalView };
        }

        public static class Permissions
        {
            public const string CanAssignIcas = "CanAssignIcas";
            public const string CanChangeSegments = "CanChangeSegments";
            public const string CanHaveEmptyProcessors = "CanHaveEmptyProcessors";
            public const string CanHaveEmptySegments = "CanHaveEmptySegments";
            public const string CanIndexInsertsBundleReport = "CanIndexInsertsBundleReport";
            public const string CanIndexPromotionReport = "CanIndexPromotionReport";
            public const string CanManageEnableFavoriting = "CanManageEnableFavoriting";
            public const string CanManageExpirationDate = "CanManageExpirationDate";
            public const string CanManageIssuerAssessmentUsageLanding = "CanManageIssuerAssessmentUsageLanding";
            public const string CanManageIssuerMatcher = "CanManageIssuerMatcher";
            public const string CanManageOrders = "CanManageOrders";
            public const string CanManageOrderingActivityLanding = "CanManageOrderingActivityLanding";
            public const string CanManagePricelessOffers = "CanManagePricelessOffers";
            public const string CanManageProcessors = "CanManageProcessors";
            public const string CanManageTriggerMarketing = "CanManageTriggerMarketing";
            public const string CanSetPrinterQueueAdmin = "CanSetPrinterQueueAdmin";
            public const string CanSetReportAdmin = "CanSetReportAdmin";
            public const string CanSetRequestEstimatedDistribution = "CanSetRequestEstimatedDistribution";
            public const string CanUseShoppingCart = "CanUseShoppingCart";
            public const string CanViewAllRegionContentReport = "CanViewAllRegionContentReport";
            public const string CanViewFindAProduct = "CanViewFindAProduct";
            public const string CanViewOptimizationDashboard = "CanViewOptimizationDashboard";
            public const string CanSeeNoneSegmentationButton = "CanSeeNoneSegmentationButton";
            public const string HasAlternativeExpiration = "HasAlternativeExpiration";
        }

        public static class TrackingTypes
        {
            public const string Calendar = "calendar";
            public const string CarouselSlide = "carouselslide";
            public const string ContactUs = "contactus";
            public const string ContentCommentNotification = "ContentCommentNotification";
            public const string ElectronicDeliveryDownloadableAsset = "electronicdeliverydownloadableasset";
            public const string ElectronicDeliveryOrderApproved = "electronicdeliveryorderapproved";
            public const string EmailBadDomain = "emailbaddomain";
            public const string EmailNotification = "emailnotification";
            public const string EmailVerification = "emailverification";
            public const string ExpiredContent = "expiredcontent";
            public const string Forgot = "forgotpassword";
            public const string FtpAccount = "ftpaccount";
            public const string FtpAccountReceived = "ftpaccountreceived";
            public const string GdprDeletedFollowUp = "gdprdeletedfollowup";
            public const string GdprFirstFollowUp = "gdprfirstfollowup";
            public const string GdprSecondFollowUp = "gdprsecondfollowup";
            public const string InsertsBundleAdmin = "insertsbundleadmin";
            public const string InsertsBundlePreFulfillment = "insertsbundleprefulfillment";
            public const string InsertsBundleUser = "insertsbundleuser";
            public const string LeadGen = "leadgen";
            public const string MostPopular = "mostpopular";
            public const string MyFavorites = "myfavorites";
            public const string NewOrder = "neworder";
            public const string NewPendingRegistration = "newpendingregistration";
            public const string OrderApproval = "orderapproval";
            public const string OrderConfirmation = "orderconfirmation";
            public const string OrderRejected = "orderrejected";
            public const string OrderShipped = "ordershipped";
            public const string OrderStatus = "orderstatus";
            public const string OrderStatusForPrinter = "orderstatusforprinter";
            public const string PendingCorrection = "pendingcorrection";
            public const string PendingRegistrationApproved = "pendingregistrationapproved";
            public const string PendingRegistrationRejected = "pendingregistrationrejected";
            public const string ProofApproved = "proofapproved";
            public const string ProofReady = "proofready";
            public const string ProofReadyReminder = "proofreadyreminder";
            public const string ShareReport = "sharereport";
            public const string SourceUpdateNotification = "sourceupdatenotification";
            public const string TriggerMarketingSubscriptionDaily = "subscriptiondaily";
            public const string TriggerMarketingSubscriptionWeekly = "subscriptionweekly";
            public const string PricelessOfferUnmatched = "pricelessofferunmatched";
            public const string PricelessOfferException = "pricelessofferexception";
            public const string VdpOrderCompleted = "vdpordercompleted";
            public const string VdpRetrieveFtpAccountReminder = "vdpretrieveftpaccountreminder";
            public const string VdpSendFtpAccountReminder = "vdpsendftpaccountreminder";
            public const string WrongICA = "wrongica";
        }

        public static class ImageUrls
        {
            public const string EmailTemplateHeaderUs = "/content/images/emails/email_template_header_us.png";
            public const string EmailTemplateHeaderGlobal = "/content/images/emails/email_template_header_global.png";
            public const string ReportsLoader = "/portal/content/images/loading.gif";
        }

        public static class PendingRegistrationStatus
        {
            public const string Approved = "Approved";
            public const string Pending = "Pending";
            public const string Rejected = "Rejected";
        }

        public class SearchOrderOption
        {
            public const string Relevance = "Relevance";
            public const string Visits = "Visits";
            public const string RecentlyUpdatedDate = "DaysSinceUpdated";
        }

        public class SearchOrderDirection
        {
            public const string Ascendant = "Asc";
            public const string Descendant = "Desc";
        }

        public class OfferOrderOption
        {
            public const string Alphabetically = "Title";
            public const string Newest = "CreationDate";
            public const string EndingSoon = "ValidityPeriodToDate";
        }

        public class OfferOrderDirection
        {
            public const string Ascendant = "Asc";
            public const string Descendant = "Desc";
        }

        public static class OfferState
        {
            public const string Yes = "Yes";
            public const string No = "No";
            public const string Pending = "Pending Review";
        }

        public static class UserRegistrationReport
        {
            public const string DisabledMessage = "GDPR Expired";
        }

        public static class SiteTracking
        {
            public static class PageGroup
            {
                public const string ElectronicDownload = "electronicdownload";
                public const string ElectronicDownloadError = "electronicdownload-error";
                public const string LibraryDownload = "librarydownload";
                public const string LibraryDownloadError = "librarydownload-error";
                public const string ReportPage = "reportpage";
            }
        }

        public static class ErrorCodes
        {
            public const string AccessDenied = "accessdenied";
            public const string InvalidOrder = "invalidorder";
            public const string NotFound = "notfound";
            public const string ProgramExpired = "programexpired";
        }
    }
}