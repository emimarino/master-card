﻿namespace Mastercard.MarketingCenter.Common.Infrastructure
{
    public interface IBootstrapper
    {
        void Execute();
    }
}
