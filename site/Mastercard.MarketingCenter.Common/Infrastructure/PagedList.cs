﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Common.Infrastructure
{
    public class PagedList<T> where T : class
    {
        public PagedList(IEnumerable<T> data, int total)
        {
            Data = data;
            Total = total;
        }

        public int Total { get; set; }
        public IEnumerable<T> Data { get; set; }
    }
}
