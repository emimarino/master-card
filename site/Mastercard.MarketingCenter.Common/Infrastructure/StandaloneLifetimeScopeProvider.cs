﻿using Autofac;
using Autofac.Integration.Mvc;
using System;
using System.Globalization;
using System.Web;

namespace Mastercard.MarketingCenter.Common.Infrastructure
{
    public class StandaloneLifetimeScopeProvider : RequestLifetimeScopeProvider, ILifetimeScopeProvider
    {
        private static ILifetimeScope _standaloneLifeTimeScope;

        private static ILifetimeScope LifetimeScope
        {
            get
            {
                if (HttpContext.Current == null)
                {
                    return _standaloneLifeTimeScope;
                }

                return (ILifetimeScope)HttpContext.Current.Items[typeof(ILifetimeScope)];
            }
            set
            {
                if (HttpContext.Current == null)
                {
                    _standaloneLifeTimeScope = value;
                }
                else
                {
                    HttpContext.Current.Items[typeof(ILifetimeScope)] = value;
                }
            }
        }

        public new void EndLifetimeScope()
        {
            // No-op if HttpContext.Current is null
            if (HttpContext.Current != null)
            {
                base.EndLifetimeScope();
            }
        }

        public new ILifetimeScope GetLifetimeScope(Action<ContainerBuilder> configurationAction)
        {
            if (LifetimeScope == null && (LifetimeScope = GetLifetimeScopeCore(configurationAction)) == null)
                throw new InvalidOperationException(
                    string.Format(
                        (IFormatProvider)CultureInfo.CurrentCulture,
                        "Null LifeTimeScope returned",
                        new object[1]
                        {
                             GetType().FullName
                        }));

            return LifetimeScope;
        }

        void ILifetimeScopeProvider.EndLifetimeScope()
        {
            EndLifetimeScope();
        }

        public StandaloneLifetimeScopeProvider(ILifetimeScope scope) : base(scope)
        {
        }
    }
}