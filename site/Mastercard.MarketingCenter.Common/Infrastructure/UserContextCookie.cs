﻿using Newtonsoft.Json;

namespace Mastercard.MarketingCenter.Common.Infrastructure
{
    public class UserContextCookie
    {
        [JsonProperty("sl")]
        public string SelectedLanguage { get; set; }

        [JsonProperty("sr")]
        public string SelectedRegion { get; set; }
    }
}