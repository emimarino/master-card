﻿using System;
using System.Web;
using System.Web.Security;

namespace Mastercard.MarketingCenter.Common.Infrastructure
{
    public class CustomHeaderForNonAuthorizedModule : IHttpModule
    {
        private static readonly object CustomHeaderForNonAuthorizedKey = new object();

        public static void CustomHeaderForNonAuthorized(HttpContext context)
        {
            context.Items[CustomHeaderForNonAuthorizedKey] = true;
        }

        public static void CustomHeaderForNonAuthorized(HttpContextBase context)
        {
            context.Items[CustomHeaderForNonAuthorizedKey] = true;
        }

        public void Init(HttpApplication context)
        {
            context.PostLogRequest += Context_PostLogRequest;
            context.EndRequest += OnEndRequest;
        }

        private void Context_PostLogRequest(object sender, EventArgs e)
        {
            var context = (HttpApplication)sender;
            var response = context.Response;
            var request = context.Request;

            if (response.StatusCode == 401
                && request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                CustomHeaderForNonAuthorized(context.Context);
            }
        }

        private void OnEndRequest(object source, EventArgs args)
        {
            var context = (HttpApplication)source;
            var request = context.Request;
            var response = context.Response;
            var isLoginForm = FormsAuthentication.LoginUrl != null ? request.Url.AbsolutePath.Contains(FormsAuthentication.LoginUrl) : false;

            if (context.Context.Items.Contains(CustomHeaderForNonAuthorizedKey) || isLoginForm)
            {
                var url = isLoginForm ? (request.UrlReferrer != null ? request.UrlReferrer.AbsoluteUri : request.Url.AbsoluteUri) : response.RedirectLocation;
                response.AddHeader("MASTERCARD_UNAUTHENTICATED_REDIRECT", url);
            }
        }

        public void Dispose()
        {
        }
    }
}