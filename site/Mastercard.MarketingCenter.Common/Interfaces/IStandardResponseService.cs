﻿using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Common.Interfaces
{
    public interface IStandardResponseService
    {

        JsonResult GetErrorResponse(string code, string text);
    }
}