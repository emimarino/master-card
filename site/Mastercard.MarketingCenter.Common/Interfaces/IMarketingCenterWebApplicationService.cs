﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastercard.MarketingCenter.Common.Interfaces
{
    public interface IMarketingCenterWebApplicationService
    {
        IEnumerable<string> GetCurrentSegmentationIds();
        string GetCurrentSpecialAudience();
    }
}
