﻿using System;

namespace Mastercard.MarketingCenter.Common.Interfaces
{
    public interface ICookieService
    {
        string Get(string name);

        void Set(string name, string value, string domain, DateTime? expires = null);
    }
}