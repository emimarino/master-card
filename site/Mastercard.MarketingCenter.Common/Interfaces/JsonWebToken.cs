﻿using Mastercard.MarketingCenter.Common.Extensions;
using System;

namespace Mastercard.MarketingCenter.Common.Interfaces
{
    public abstract class JsonWebToken
    {
        public string ExpirationDate { get; }
        public JsonWebToken()
        {
            ExpirationDate = DateTime.UtcNow.AddMinutes(10).ToUnixTimestamp().ToString();
        }
    }
}