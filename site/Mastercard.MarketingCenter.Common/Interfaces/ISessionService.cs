﻿using System;

namespace Mastercard.MarketingCenter.Common.Interfaces
{
    public interface ISessionService
    {
        object Get(string name);

        void Set(string name, object value);
    }
}