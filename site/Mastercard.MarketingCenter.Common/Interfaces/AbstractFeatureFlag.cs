﻿namespace Mastercard.MarketingCenter.Common.Interfaces
{
    public abstract class AbstractFeatureFlag
    {
        protected string _region = null;
        public abstract bool IsOn(string key, string region = null);

        public bool this[string key]
        {
            get { return IsOn(key, _region); }
        }
        public bool this[string key, string region]
        {
            get { return IsOn(key, region); }
        }
        public abstract bool IsAnyOn(string[] featureKeys, string region = null);
        public abstract bool AllAreOn(string[] featureKeys, string region = null);
    }
}