﻿namespace Mastercard.MarketingCenter.Common.Interfaces
{
    public interface IMembershipProvider
    {
        /// <summary>
        /// Determines if current principal HttpContext.Current.User is in role
        /// </summary>
        /// <param name="role">a role to check</param>
        /// <returns></returns>
        bool CurrentUserIsInRole(string role);
    }
}