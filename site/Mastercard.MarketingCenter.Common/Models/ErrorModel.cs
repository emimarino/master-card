﻿namespace Mastercard.MarketingCenter.Common.Models
{
    public class ErrorModel
    {
        public string ErrorCode { get; set; }
        public string ErrorText { get; set; }
    }
}