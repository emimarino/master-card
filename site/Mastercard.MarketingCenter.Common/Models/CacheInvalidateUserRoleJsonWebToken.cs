﻿using Mastercard.MarketingCenter.Common.Interfaces;

namespace Mastercard.MarketingCenter.Common.Models
{
    public class CacheInvalidateUserRoleJsonWebToken : JsonWebToken
    {
        public string UserId { get; set; }
    }
}