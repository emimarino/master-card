﻿using Mastercard.MarketingCenter.Common.Interfaces;

namespace Mastercard.MarketingCenter.Common.Models
{
    public class CacheInvalidateCategoryModelJsonWebToken : JsonWebToken
    {
    }
}