﻿namespace Mastercard.MarketingCenter.Common.Models
{
    public class AnonymousUserProxy
    {
        public int UserId { get; set; }
        public string ClaimsTo { get; set; }
        public string OptionalParameter { get; set; }
        public string RegionId { get; set; }
    }
}