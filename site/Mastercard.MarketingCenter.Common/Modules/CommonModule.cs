﻿using Autofac;
using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Common.Services;

namespace Mastercard.MarketingCenter.Common.Modules
{
    public class CommonModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<WebCookieService>().As<ICookieService>();
            builder.RegisterType<WebSessionService>().As<ISessionService>().InstancePerLifetimeScope();
            builder.RegisterType<IdGeneratorService>();
            builder.RegisterType<WebMembershipProvider>().As<IMembershipProvider>();
            builder.RegisterType<StandardResponseService>().As<IStandardResponseService>();
        }
    }
}