﻿using Mastercard.MarketingCenter.Common.Interfaces;
using System.Web;

namespace Mastercard.MarketingCenter.Common.Services
{
    public class WebSessionService : ISessionService
    {
        public object Get(string name)
        {
            return HttpContext.Current.Session[name];
        }

        public void Set(string name, object value)
        {
            HttpContext.Current.Session[name] = value;
        }
    }
}