﻿using Mastercard.MarketingCenter.Common.Interfaces;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Common.Services
{
    public class StandaloneCookieService : ICookieService
    {
        private readonly IDictionary<string, string> _cookies = new Dictionary<string, string>();

        public string Get(string name)
        {
            if (!this._cookies.ContainsKey(name))
                return null;

            return this._cookies[name];
        }

        public void Set(string name, string value, string domain, DateTime? expires = null)
        {
            this._cookies[name] = value;
        }
    }
}