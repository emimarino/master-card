﻿using Mastercard.MarketingCenter.Common.Extensions;
using System.Web.Security;

namespace Mastercard.MarketingCenter.Common.Services
{
    public static class MembershipService
    {
        public static MembershipUser GetUser(string userName)
        {
            return Membership.GetUser(userName.RemoveMembershipProviderName());
        }
    }
}