﻿using Mastercard.MarketingCenter.Common.Interfaces;
using System;
using System.Web;

namespace Mastercard.MarketingCenter.Common.Services
{
    public class WebCookieService: ICookieService
    {
        public string Get(string name)
        {
            var cookie = HttpContext.Current.Request.Cookies.Get(name);
            if (cookie == null)
                return null;

            return cookie.Value;
        }

        public void Set(string name, string value, string domain, DateTime? expires = null)
        {
            var cookie = new HttpCookie(name, value);

            if(!string.IsNullOrEmpty(domain))
            {
                cookie.Domain = domain;
            }

            if(expires.HasValue)
            {
                cookie.Expires = expires.Value;
            }

            HttpContext.Current.Response.AppendCookie(cookie);
        }
    }
}