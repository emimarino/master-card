﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;

namespace Mastercard.MarketingCenter.Common.Services
{
    public static class VideoFileService
    {
        public static bool ContainsHtmlVideo(string htmlText, string videoName)
        {
            List<string> videoNames = new List<string>();
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlText);
            var desc = doc.DocumentNode.Descendants();
            desc.ToList().ForEach(t =>
            {
                if (t.Attributes.Contains("class"))
                {
                    if ((t.Attributes["class"]?.Value.ToLowerInvariant().Contains("data-video") ?? false) ||
                        (t.Attributes["class"]?.Value.ToLowerInvariant().Contains("video-link") ?? false) ||
                        (t.Attributes["class"]?.Value.ToLowerInvariant().Contains("video-player-embed") ?? false))
                    {
                        videoNames.Add(t.Attributes["data-video-name"]?.Value.Trim() ?? string.Empty);
                        videoNames.Add(t.Attributes["data-video"]?.Value.Trim() ?? string.Empty);
                        videoNames.Add(t.Attributes["name"]?.Value.Trim() ?? string.Empty);
                    }
                }
            });

            return videoNames.Distinct().Select(vn => vn.Split(':')[0].Split('.')[0]).Any(vn => vn.Equals(videoName.Split('.')[0], StringComparison.InvariantCultureIgnoreCase));
        }
    }
}