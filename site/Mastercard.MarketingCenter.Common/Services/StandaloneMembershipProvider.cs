﻿using Mastercard.MarketingCenter.Common.Interfaces;

namespace Mastercard.MarketingCenter.Common.Services
{
    public class StandaloneMembershipProvider : IMembershipProvider
    {
        public bool CurrentUserIsInRole(string role)
        {
            return true;
        }
    }
}