﻿using System.Web.Configuration;

namespace Mastercard.MarketingCenter.Common.Services
{
    public class CookieNameProviderService
    {
        private const string prefix = "_uc";
        private const string userContextCookiePrefixConfigurationKey = "UserContextCookiePrefix";
        private string _suffix;

        public CookieNameProviderService()
        {
            _suffix = WebConfigurationManager.AppSettings.Get(userContextCookiePrefixConfigurationKey);
        }

        public string GetUserContextCookieName()
        {
            return string.Format("{0}.{1}", prefix, _suffix);
        }
    }
}