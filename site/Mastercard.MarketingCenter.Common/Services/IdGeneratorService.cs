﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Configuration;

namespace Mastercard.MarketingCenter.Common.Services
{
    public class IdGeneratorService
    {
        private readonly IDbConnection _connection;

        public IdGeneratorService(IDbConnection connection)
        {
            _connection = connection;
        }

        public string GetNewUniqueId()
        {
            var newId = string.Empty;
            ExecuteInTransaction(conn =>
            {
                var nextId = GetCurrentIdCore(conn) + 1;

                using (var command = conn.CreateCommand())
                {
                    command.CommandText = "UPDATE IdSeed SET IdSeed = @nextId";
                    var parameter = new SqlParameter("@nextId", SqlDbType.VarChar) { Value = nextId };
                    command.Parameters.Add(parameter);
                    command.ExecuteNonQuery();
                }

                newId = GetCurrentId();
            });

            return newId;
        }

        public string GetCurrentId()
        {
            var lastId = 0L;

            ExecuteInTransaction(conn =>
            {
                lastId = GetCurrentIdCore(conn);
            });

            return lastId.ToString("X").ToLowerInvariant();
        }

        private static long GetCurrentIdCore(IDbConnection conn)
        {
            var lastId = 0L;
            using (var command = conn.CreateCommand())
            {
                command.CommandText = "SELECT IdSeed FROM IDSEED";
                lastId = Convert.ToInt64(command.ExecuteScalar());
            }

            if (lastId != 0)
            {
                return lastId;
            }

            var initialIdSeed = Convert.ToInt32(WebConfigurationManager.AppSettings["InitialIdSeed"]);

            using (var command = conn.CreateCommand())
            {
                command.CommandText = "INSERT INTO IDSEED VALUES(@initialIdSeed)";
                var parameter = new SqlParameter("@initialIdSeed", SqlDbType.Int) { Value = initialIdSeed };
                command.Parameters.Add(parameter);
                command.ExecuteNonQuery();
            }

            lastId = initialIdSeed;

            return lastId;
        }

        private void ExecuteInTransaction(Action<IDbConnection> action)
        {
            if (_connection.State == ConnectionState.Closed)
            {
                _connection.Open();
            }

            using (var tx = new TransactionScope())
            {
                action(_connection);

                tx.Complete();
            }
        }
    }
}