﻿using Mastercard.MarketingCenter.Common.Interfaces;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Common.Services
{
    public class StandaloneSessionService : ISessionService
    {
        private readonly IDictionary<string, object> _state = new ConcurrentDictionary<string, object>();

        public object Get(string name)
        {
            return _state.ContainsKey(name)? _state[name] : null;
        }

        public void Set(string name, object value)
        {
            _state[name] = value;
        }
    }
}