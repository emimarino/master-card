﻿using Mastercard.MarketingCenter.Common.Interfaces;
using System.Web;

namespace Mastercard.MarketingCenter.Common.Services
{
    public class WebMembershipProvider : IMembershipProvider
    {
        public bool CurrentUserIsInRole(string role)
        {
            return HttpContext.Current.User.IsInRole(role);
        }
    }
}