﻿using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Common.Models;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Common.Services
{
    public class StandardResponseService : IStandardResponseService
    {
        public JsonResult GetErrorResponse(string code, string text)
        {
            return new JsonResult
            {
                Data = new ErrorModel
                {
                    ErrorCode = code,
                    ErrorText = text
                }
            };
        }
    }
}