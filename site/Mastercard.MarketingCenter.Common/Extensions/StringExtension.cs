﻿using HtmlAgilityPack;
using Mastercard.MarketingCenter.Common.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;

namespace Mastercard.MarketingCenter.Common.Extensions
{
    public static class StringExtension
    {
        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static string F(this string str, params object[] args)
        {
            if (str.IsNullOrEmpty())
            {
                return str;
            }

            return string.Format(str, args);
        }

        public static bool IsNullOrWhiteSpace(this string inputString)
        {
            return string.IsNullOrWhiteSpace(inputString);
        }

        public static string GetAsDraftContentItemId(this string str)
        {
            return $"{str.GetAsLiveContentItemId()}-p";
        }

        public static string GetAsLiveContentItemId(this string str)
        {
            return str.Replace("-p", string.Empty);
        }

        public static bool IsDraftContentItemId(this string str)
        {
            return str.EndsWith("-p");
        }

        public static string TrimEllipsis(this string text, int max)
        {
            return string.IsNullOrEmpty(text) || text.Length <= max ? text : text.Substring(0, max) + "...";
        }

        public static string TrimEllipsisWord(this string text, int max)
        {
            return string.IsNullOrEmpty(text) || text.Length <= max ? text : (text.IndexOf(' ', max) > 0 ? text.Substring(0, text.IndexOf(' ', max)) : text.Substring(0, max)) + "...";
        }

        public static string StripHtml(this string text)
        {
            return string.IsNullOrEmpty(text) ? text : Regex.Replace(text, @"<(.|\n)*?>", string.Empty);
        }

        public static string DecodeHtml(this string text)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(text);

            return (string.IsNullOrEmpty(text) ? text : HttpUtility.HtmlDecode(doc.DocumentNode.InnerHtml));
        }

        public static string FindHtmlElement(this string text, string xpath)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(text);
            doc.OptionFixNestedTags = true;

            string value = null;
            var navigator = (HtmlNodeNavigator)doc.CreateNavigator();
            try
            {
                value = navigator.SelectSingleNode(xpath).Value;
            }
            catch (Exception)
            {
                // omit
            }

            return value;
        }

        public static string RemoveHtmlTags(this string text)
        {
            return Regex.Replace(text.Trim(), @"<[^>]*>", string.Empty);
        }

        public static string RemoveInvalidFileNameChars(this string text)
        {
            string file = string.Concat(text.Split(Path.GetInvalidFileNameChars(), StringSplitOptions.RemoveEmptyEntries));
            if (file.Length > 250)
            {
                file = file.Substring(0, 250);
            }

            return file;
        }

        /// <summary>
        /// Produces optional, URL-friendly version of a title, "like-this-one". 
        /// hand-tuned for speed, reflects performance refactoring contributed
        /// by John Gietzen (user otac0n) 
        /// </summary>
        public static string ToURLFriendly(this string title)
        {
            if (title == null) return "";

            const int maxlen = 80;
            int len = title.Length;
            bool prevdash = false;
            var sb = new StringBuilder(len);
            char c;

            for (int i = 0; i < len; i++)
            {
                c = title[i];
                if ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9'))
                {
                    sb.Append(c);
                    prevdash = false;
                }
                else if (c >= 'A' && c <= 'Z')
                {
                    // tricky way to convert to lowercase
                    sb.Append((char)(c | 32));
                    prevdash = false;
                }
                else if (c == ' ' || c == ',' || c == '.' || c == '/' || c == '\\' || c == '-' || c == '_' || c == '=')
                {
                    if (!prevdash && sb.Length > 0)
                    {
                        sb.Append('-');
                        prevdash = true;
                    }
                }
                else if ((int)c >= 128)
                {
                    int prevlen = sb.Length;
                    sb.Append(RemapInternationalCharToAscii(c));

                    if (prevlen != sb.Length)
                    {
                        prevdash = false;
                    }
                }
                if (i == maxlen)
                {
                    break;
                }
            }

            return prevdash ? sb.ToString().Substring(0, sb.Length - 1) : sb.ToString();
        }

        public static string stringCleanPartialUrl(this string url)
        {
            var re = new Regex(@"(\?|&|\s)", (RegexOptions.Compiled | RegexOptions.CultureInvariant));

            return re.Replace(url, string.Empty);
        }

        public static string RemapInternationalCharToAscii(this char c)
        {
            string s = c.ToString().ToLowerInvariant();
            if ("àåáâäãåą".Contains(s))
            {
                return "a";
            }
            else if ("èéêëę".Contains(s))
            {
                return "e";
            }
            else if ("ìíîïı".Contains(s))
            {
                return "i";
            }
            else if ("òóôõöøőð".Contains(s))
            {
                return "o";
            }
            else if ("ùúûüŭů".Contains(s))
            {
                return "u";
            }
            else if ("çćčĉ".Contains(s))
            {
                return "c";
            }
            else if ("żźž".Contains(s))
            {
                return "z";
            }
            else if ("śşšŝ".Contains(s))
            {
                return "s";
            }
            else if ("ñń".Contains(s))
            {
                return "n";
            }
            else if ("ýÿ".Contains(s))
            {
                return "y";
            }
            else if ("ğĝ".Contains(s))
            {
                return "g";
            }
            else if (c == 'ř')
            {
                return "r";
            }
            else if (c == 'ł')
            {
                return "l";
            }
            else if (c == 'đ')
            {
                return "d";
            }
            else if (c == 'ß')
            {
                return "ss";
            }
            else if (c == 'Þ')
            {
                return "th";
            }
            else if (c == 'ĥ')
            {
                return "h";
            }
            else if (c == 'ĵ')
            {
                return "j";
            }
            else
            {
                return "";
            }
        }

        public static string AddSpacesToSentence(this string str, bool preserveAcronyms)
        {
            if (str.IsNullOrWhiteSpace())
            {
                return string.Empty;
            }

            StringBuilder newText = new StringBuilder(str.Length * 2);
            newText.Append(str[0]);
            for (int i = 1; i < str.Length; i++)
            {
                if (char.IsUpper(str[i]))
                    if ((str[i - 1] != ' ' && !char.IsUpper(str[i - 1])) ||
                        (preserveAcronyms && char.IsUpper(str[i - 1]) &&
                         i < str.Length - 1 && !char.IsUpper(str[i + 1])))
                    {
                        newText.Append(' ');
                    }

                newText.Append(str[i]);
            }

            return newText.ToString();
        }

        public static IEnumerable<string> ParseEmailAddresses(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return null;
            }

            return input.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string RawWithSpecialCharacters(this string str)
        {
            return str.Replace("<sup>™</sup>", "™")
                      .Replace("™", "<sup>™</sup>")
                      .Replace("<sup>®</sup>", "®")
                      .Replace("®", "<sup>®</sup>")
                      .Replace("<sup>©</sup>", "©")
                      .Replace("©", "<sup>©</sup>");
        }

        public static bool IsAnchorLink(this string str)
        {
            return str.StartsWith("#");
        }

        public static bool IsEmailLink(this string str)
        {
            return str.ToLower().StartsWith("mailto:");
        }

        public static bool IsInlineJavascript(this string str)
        {
            return str.ToLower().StartsWith("javascript:");
        }

        public static bool IsValidEmailAddress(this string email)
        {
            if (email.IsNullOrEmpty())
            {
                return false;
            }

            var regex = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{1,4}$");

            return regex.IsMatch(email);
        }

        public static string GetEmailDomain(this string email)
        {
            if (!email.IsValidEmailAddress())
            {
                return string.Empty;
            }

            return email.Substring(email.IndexOf("@") + 1, (email.Length - email.IndexOf("@") - 1));
        }

        public static string EnsureStartsWithValue(this string str, string value)
        {
            return str.IsNullOrWhiteSpace() ? string.Empty : str.StartsWith(value) ? str : $"{value}{str}";
        }

        public static string GetQueryClearOfSynonyms(this string searchQuery)
        {
            var regex = new Regex(@"\(([^\)]+)\)");
            return System.Net.WebUtility.UrlDecode(regex.Replace(searchQuery.Replace("quotenc", "\"\""), string.Empty).Trim());
        }

        public static bool IsAbsoluteUrl(this string url)
        {
            return Uri.TryCreate(url, UriKind.Absolute, out _);
        }

        public static Dictionary<string, string> GetOptionsFromSettings(this string str)
        {
            return str.TrimEnd().Split(new[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries).ToDictionary(
                o => o.Split(new[] { "||" }, StringSplitOptions.RemoveEmptyEntries).First(),
                o => o.Split(new[] { "||" }, StringSplitOptions.RemoveEmptyEntries).Last());
        }

        public static string ToRelativeUri(this string str)
        {
            return str.Replace("\\", "/");
        }

        public static string ToFieldKey(this string str)
        {
            return str.Replace(Constants.FieldPrefix, string.Empty).TrimStart('_');
        }

        public static bool IsValidImage(this string text)
        {
            switch (Path.GetExtension(text).Replace(".", "").ToLower())
            {
                case "bmp":
                case "gif":
                case "jpe":
                case "jpeg":
                case "jpg":
                case "png":
                case "tif":
                case "tiff":
                    return true;
                default:
                    return false;
            }
        }

        public static string GetMD5Hash(this string text)
        {
            var md5 = MD5.Create();
            return text.GetHash(md5);
        }

        private static string GetHash(this string text, HashAlgorithm hashAlgorithm)
        {
            return BitConverter.ToString(hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(text))).Replace("-", "").ToLower();
        }

        public static string RemoveMembershipProviderName(this string userName)
        {
            return userName.IsNullOrWhiteSpace() ? string.Empty : Regex.Replace(userName, $"{GetMembershipProviderName()}:", string.Empty, RegexOptions.IgnoreCase).Trim();
        }

        public static string IncludeMembershipProviderName(this string userName)
        {
            return userName.IsNullOrWhiteSpace() ? string.Empty : $"{GetMembershipProviderName()}:{userName.RemoveMembershipProviderName()}";
        }

        private static string GetMembershipProviderName()
        {
            return Membership.Provider.Name.IsNullOrWhiteSpace() ? "MasterCardMembers" : Membership.Provider.Name;
        }
    }
}