﻿using Mastercard.MarketingCenter.Common.Interfaces;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Common.Extensions
{
    static public class IEnumerableExtension
    {
        public static ICollection<T> IncludeByFeatureKey<T>(this ICollection<T> listToFilter, T instance, params string[] featureKeys)
        {
            var abstractFeatureFlag = DependencyResolver.Current.GetService<AbstractFeatureFlag>();
            var anyFeatureIsOn = abstractFeatureFlag.IsAnyOn(featureKeys);
            if (anyFeatureIsOn)
            {
                listToFilter.Add(instance);
            }

            return listToFilter;
        }

        public static bool IsNull<T>(this IEnumerable<T> data)
        {
            return data == null;
        }
    }
}