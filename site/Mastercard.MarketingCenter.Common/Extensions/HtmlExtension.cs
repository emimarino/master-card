﻿using Mastercard.MarketingCenter.Common.Interfaces;
using System.Web;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Common.Extensions
{
    public static class HtmlExtension
    {
        private static AbstractFeatureFlag _abstractFeatureFlag { get { return DependencyResolver.Current.GetService<AbstractFeatureFlag>(); } }

        public static HtmlString RenderByFeatureIfAnyIsOff(this HtmlHelper helper, HtmlString body, string[] featureKeys, string region = null)
        {
            if (!_abstractFeatureFlag.AllAreOn(featureKeys))
            {
                return body;
            }
            else
            {
                return new HtmlString(string.Empty);
            }
        }

        public static HtmlString RenderByFeatureIfAnyIsOff(this HtmlHelper helper, string body, string[] featureKeys, string region = null)
        {
            return RenderByFeatureIfAnyIsOff(helper, new HtmlString(body), featureKeys, region);
        }

        public static HtmlString RenderByFeatureIfAnyIsOn(this HtmlHelper helper, HtmlString body, string[] featureKeys, string region = null)
        {
            if (_abstractFeatureFlag.IsAnyOn(featureKeys))
            { return body; }
            else
            {
                return new HtmlString(string.Empty);
            }
        }

        public static HtmlString RenderByFeatureIfAnyIsOn(this HtmlHelper helper, string body, string[] featureKeys, string region = null)
        {
            return RenderByFeatureIfAnyIsOn(helper, new HtmlString(body), featureKeys, region);
        }
    }
}