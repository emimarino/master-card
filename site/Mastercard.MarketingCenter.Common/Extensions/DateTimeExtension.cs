﻿using System;
using System.Globalization;

namespace Mastercard.MarketingCenter.Common.Extensions
{
    public static class DateTimeExtension
    {
        private static string GetOrdinalSuffix(string num)
        {
            if (num.EndsWith("11")) return "th";
            if (num.EndsWith("12")) return "th";
            if (num.EndsWith("13")) return "th";
            if (num.EndsWith("1")) return "st";
            if (num.EndsWith("2")) return "nd";
            if (num.EndsWith("3")) return "rd";

            return "th";
        }
        public static string GetStringDate(this DateTime dateTime, CultureInfo culture = null)
        {
            var dateString = dateTime.ToUniversalTime().ToString("MMMM d", culture ?? CultureInfo.InvariantCulture);
            return $"{dateString}{GetOrdinalSuffix(dateString)}";
        }

        public static bool IsBetween(this DateTime dateTime, DateTime periodStart, DateTime periodEnd, bool openPeriod = false)
        {
            var dateIsAfterBeginning = openPeriod ? (periodStart < dateTime) : (periodStart <= dateTime);
            var dateIsBeforeEnding = openPeriod ? (periodEnd > dateTime) : (periodEnd >= dateTime);
            return dateIsAfterBeginning && dateIsBeforeEnding;
        }

        public static DateTime RemoveTimePart(this DateTime input)
        {
            return new DateTime(input.Year, input.Month, input.Day, 0, 0, 0);
        }

        public static string ToStringForQuery(this DateTime input)
        {
            return input.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static string ToStringForEmail(this DateTime input, IFormatProvider provider = null)
        {
            return input.ToString("MMM dd, yyyy", provider);
        }

        public static int ToUnixTimestamp(this DateTime input)
        {
            return (int)(input - new DateTime(1970, 1, 1)).TotalSeconds;
        }
    }
}