﻿using System.IO;

namespace Mastercard.MarketingCenter.Common.Extensions
{
    public static class FileExtension
    {
        public static string GetMimeType(this string filename)
        {
            var extension = Path.GetExtension(filename);
            if (string.IsNullOrEmpty(extension))
                return "application/octet-stream";

            switch (extension.Replace(".", "").ToLower())
            {
                case "doc":
                case "dot":
                case "docx":
                    return "application/msword";
                case "jpe":
                case "jpeg":
                case "jpg":
                    return "image/jpeg";
                case "gif":
                    return "image/gif";
                case "png":
                    return "image/png";
                case "pdf":
                    return "application/pdf";
                default:
                    return "application/octet-stream";
            }
        }
    }
}