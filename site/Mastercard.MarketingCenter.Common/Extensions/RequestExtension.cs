﻿using Microsoft.Web.Administration;
using System;
using System.Linq;
using System.Web;

namespace Mastercard.MarketingCenter.Common.Extensions
{
    public static class RequestExtension
    {
        public static ConfigurationSection httpErrors
        {
            get
            {
                var errs = WebConfigurationManager.GetSection("system.webServer/httpErrors");
                return (errs.GetAttribute("errorMode")?.Value?.ToString() != "2") ? errs : null;
            }
        }

        public static void RedirectError(this HttpContext Context, string fallBackValue, Exception ex, string defaultValue = null, bool changeAddress = false)
        {
            var response = Context.Response;
            var request = Context.Request;
            var server = Context.Server;

            string finalRedirectUrl = defaultValue;
            if (string.IsNullOrEmpty(finalRedirectUrl))
            {
                //if no url to redirect after error is specified it looks for the default url in httperrors web.config element
                finalRedirectUrl = httpErrors.GetCollection()
                    .FirstOrDefault(el => (el.GetAttributeValue("statusCode")?.ToString() == "500"))?.GetAttributeValue("path")?.ToString() ??
                           httpErrors.GetAttributeValue("errorMode")?.ToString() ?? fallBackValue;
            }

            response.Clear();

            Context.RewritePath(finalRedirectUrl, "", "");

            if (changeAddress)
            {
                response.Headers.Remove("Refresh");
                response.Headers.Add("Refresh", ("0;url=" + finalRedirectUrl));
            }

            server.ClearError();
        }
    }
}