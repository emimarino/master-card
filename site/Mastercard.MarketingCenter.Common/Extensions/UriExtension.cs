﻿using Mastercard.MarketingCenter.Common.Models;
using System;
using System.Web;

namespace Mastercard.MarketingCenter.Common.Extensions
{
    public static class UriExtension
    {
        public static string GetRouteParameterValue(this Uri uri, string paramaterName)
        {
            RouteInfo routeInfo = new RouteInfo(uri, HttpContext.Current.Request.ApplicationPath);
            return routeInfo.RouteData == null ? null : (routeInfo.RouteData.Values[paramaterName] != null ? routeInfo.RouteData.Values[paramaterName].ToString() : null);
        }

        public static bool IsRouteMatch(this Uri uri, string controllerName, string actionName)
        {
            RouteInfo routeInfo = new RouteInfo(uri, HttpContext.Current.Request.ApplicationPath);
            return (routeInfo.RouteData != null
                    && routeInfo.RouteData.Values["controller"] != null
                    && routeInfo.RouteData.Values["action"] != null
                    && routeInfo.RouteData.Values["controller"].ToString() == controllerName
                    && routeInfo.RouteData.Values["action"].ToString() == actionName);
        }

        public static Uri AddParameter(this Uri uri, string name, string value)
        {
            var uriBuilder = new UriBuilder(uri.AbsoluteUri);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query[name] = value;
            uriBuilder.Query = query.ToString();
            return uriBuilder.Uri;
        }

        public static Uri RemoveParameter(this Uri uri, string name)
        {
            var uriBuilder = new UriBuilder(uri.AbsoluteUri);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query.Remove(name);
            uriBuilder.Query = query.ToString();
            return uriBuilder.Uri;
        }
    }
}