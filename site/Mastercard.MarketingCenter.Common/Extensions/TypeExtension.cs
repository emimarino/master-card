﻿using System;
using System.Reflection;

namespace Mastercard.MarketingCenter.Common.Extensions
{
    public static class TypeExtension
    {
        public static Assembly GetAssembly(this Type type)
        {
            return Assembly.GetAssembly(type);
        }

        public static bool Implements<T>(this Type type) where T : class
        {
            return typeof(T).IsAssignableFrom(type);
        }
    }
}