﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Services.Models
{
    public class GroupKeyValueItem
    {
        public string GroupId { get; set; }
        public string GroupValue { get; set; }
        public IEnumerable<KeyValueItem> GroupItems { get; set; }
    }
}