﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Services.Models
{
    public class ListResult
    {
        public IEnumerable<string> Columns { get; set; }
        public IEnumerable<dynamic> Items { get; set; }
        public string IdColumn { get; set; }
    }
}