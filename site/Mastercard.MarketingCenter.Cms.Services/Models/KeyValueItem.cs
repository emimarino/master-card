﻿namespace Mastercard.MarketingCenter.Cms.Services.Models
{
    public class KeyValueItem
    {
        public string Id { get; set; }
        public string Value { get; set; }
    }
}