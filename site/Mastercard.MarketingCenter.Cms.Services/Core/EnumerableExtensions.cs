﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Services.Core
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<ContentTypeFieldDefinition> FilterByPermission(this IEnumerable<ContentTypeFieldDefinition> fieldDefinitions)
        {
            var result = new List<ContentTypeFieldDefinition>();
            var principal = DependencyResolver.Current.GetService<IPrincipal>();

            foreach (var fieldDef in fieldDefinitions)
            {
                if (!string.IsNullOrEmpty(fieldDef?.Permission))
                {
                    if (principal.IsInPermission(fieldDef.Permission))
                    {
                        result.Add(fieldDef);
                    }
                }
                else
                {
                    result.Add(fieldDef);
                }
            }

            return (result.Any()) ? result : fieldDefinitions;
        }

        public static IEnumerable<ContentTypeFieldDefinition> FilterBySetting(this IEnumerable<ContentTypeFieldDefinition> fieldDefinitions, string region)
        {
            var result = new List<ContentTypeFieldDefinition>();
            var principal = DependencyResolver.Current.GetService<IPrincipal>();

            foreach (var fieldDef in fieldDefinitions)
            {
                if (!string.IsNullOrWhiteSpace(fieldDef?.EnabledBySetting))
                {
                    bool.TryParse(RegionalizeService.RegionalizeSetting(fieldDef.EnabledBySetting, region.Trim(), false), out bool settingEnabled);
                    if (settingEnabled)
                    {
                        result.Add(fieldDef);
                    }
                }
                else
                {
                    result.Add(fieldDef);
                }
            }

            return (result.Any()) ? result : fieldDefinitions;
        }

        public static IEnumerable<ListFieldDefinition> FilterBySetting(this IEnumerable<ListFieldDefinition> fieldDefinitions, string region)
        {
            var result = new List<ListFieldDefinition>();

            foreach (var fieldDef in fieldDefinitions)
            {
                if (!string.IsNullOrWhiteSpace(fieldDef?.EnabledBySetting))
                {
                    bool settingEnabled = false;
                    bool.TryParse(RegionalizeService.RegionalizeSetting(fieldDef.EnabledBySetting, region.Trim(), false), out settingEnabled);
                    if (settingEnabled)
                    {
                        result.Add(fieldDef);
                    }
                }
                else
                {
                    result.Add(fieldDef);
                }
            }

            return (result.Any()) ? result : fieldDefinitions;
        }

        public static IEnumerable<ListFieldDefinition> FilterByPermission(this IEnumerable<ListFieldDefinition> fieldDefinitions)
        {
            var result = new List<ListFieldDefinition>();
            var principal = DependencyResolver.Current.GetService<IPrincipal>();

            foreach (var fieldDef in fieldDefinitions)
            {
                if (!string.IsNullOrEmpty(fieldDef?.Permission))
                {
                    if (principal.IsInPermission(fieldDef.Permission))
                    {
                        result.Add(fieldDef);
                    }
                }
                else
                {
                    result.Add(fieldDef);
                }
            }

            return (result.Count > 0) ? result : fieldDefinitions;
        }

        public static IEnumerable<ListFieldDefinition> ExcludeTrackingFields(this IEnumerable<ListFieldDefinition> fieldDefinitions)
        {
            return
                fieldDefinitions
                .Where(o => o.FieldDefinition.FieldTypeCode != FieldType.TrackingCreatedDate)
                .Where(o => o.FieldDefinition.FieldTypeCode != FieldType.TrackingCreatedBy)
                .Where(o => o.FieldDefinition.FieldTypeCode != FieldType.TrackingModifiedBy)
                .Where(o => o.FieldDefinition.FieldTypeCode != FieldType.TrackingUpdatedDate);
        }

        public static bool HasTrackingFields(this IEnumerable<ListFieldDefinition> listFieldDefinitions)
        {
            return listFieldDefinitions.Any(
                o => FieldType.TrackingFieldsTypes.Any(f => o.FieldDefinition.FieldTypeCode == f));
        }

        public static string GetFieldNameByTypeCode(this IEnumerable<ListFieldDefinition> listFieldDefinitions, string fieldTypeCode)
        {
            var field =
                listFieldDefinitions.FirstOrDefault(
                    o => o.FieldDefinition.FieldTypeCode == fieldTypeCode);

            if (field != null)
            {
                return field.FieldDefinition.Name;
            }

            return string.Empty;
        }

        public static IEnumerable<ListFieldDefinition> ExcludeRegionField(this IEnumerable<ListFieldDefinition> fieldDefinitions)
        {
            return fieldDefinitions
                .Where(o => o.FieldDefinition.FieldTypeCode != FieldType.Region);
        }

        public static IEnumerable<ContentTypeFieldDefinition> FilterByBusinessOwner(this IEnumerable<ContentTypeFieldDefinition> fieldDefinitions, bool showPublishedOnly)
        {
            // hide some fields from Business Owner interface for Published Assets (wierd logic, better to remove it)
            var hiddenFieldsForPublishedInBO = new List<string> {
                    MarketingCenterDbConstants.FieldNames.Language,
                    MarketingCenterDbConstants.FieldNames.IconId,
                    MarketingCenterDbConstants.FieldNames.AssetType,
                    MarketingCenterDbConstants.FieldNames.Filename
            };

            return fieldDefinitions
                .Where(fd => (fd.FieldDefinition.Name.Equals(MarketingCenterDbConstants.FieldNames.ContentItemId, StringComparison.InvariantCultureIgnoreCase)
                    || fd.BusinessOwnerOrder != null)
                    && (!showPublishedOnly || !hiddenFieldsForPublishedInBO.Contains(fd.FieldDefinition.Name)));
        }
    }
}