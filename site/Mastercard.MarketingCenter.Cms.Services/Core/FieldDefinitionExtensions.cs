﻿using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Services.Core
{
    public static class FieldDefinitionExtensions
    {
        public static bool IsDecimalNumber(this FieldDefinition fieldDefinition)
        {
            if (!string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                var settings = fieldDefinition.GetSettings<NumberSettings>();
                return settings != null && settings.IsDecimal;
            }

            return false;
        }

        public static ChoiceSettings GetChoiceSettings(this FieldDefinition fieldDefinition)
        {
            ChoiceSettings result;
            try
            {
                result = fieldDefinition.GetSettings<ChoiceSettings>();
            }
            catch
            {
                result = new ChoiceSettings
                {
                    Options = fieldDefinition.CustomSettings?.GetOptionsFromSettings() ?? new Dictionary<string, string>(),
                    IncludeEmptyChoice = fieldDefinition.Required
                };

            }

            return result;
        }

        public static IEnumerable<FieldDefinition> GetTranslatedFieldDefinitions(this FieldDefinition fieldDefinition, bool parentFieldRequired)
        {
            var fieldDefinitions = new List<FieldDefinition>();
            if (fieldDefinition.FieldTypeCode == FieldType.Translations && !string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                var translationService = DependencyResolver.Current.GetService<TranslationServices>();
                var settings = fieldDefinition.GetSettings<TranslationsSettings>();
                fieldDefinitions.AddRange(translationService.GetTranslatedTagFieldDefinitions(settings.FieldDefinitionIds));
                foreach (var field in fieldDefinitions)
                {
                    if (settings.RequiredFieldDefinitionIds != null && settings.RequiredFieldDefinitionIds.Length > 0)
                    {
                        field.Required = settings.RequiredFieldDefinitionIds.Any(d => d == field.FieldDefinitionId);
                    }
                    else
                    {
                        field.Required = parentFieldRequired;
                    }
                }
            }

            return fieldDefinitions;
        }

        public static IEnumerable<TranslationsLanguage> GetAvailableLanguages(this FieldDefinition fieldDefinition)
        {
            var languages = new List<TranslationsLanguage>();
            if (fieldDefinition.FieldTypeCode == FieldType.Translations && !string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                var settings = fieldDefinition.GetSettings<TranslationsSettings>();
                if (settings.Languages != null && settings.Languages.Length > 0)
                {
                    languages.AddRange(settings.Languages);
                }
                else
                {
                    var userContext = DependencyResolver.Current.GetService<UserContext>();
                    var translationService = DependencyResolver.Current.GetService<TranslationServices>();

                    if (userContext != null && translationService != null)
                    {
                        languages.AddRange(translationService.GetAvailableLanguagesForRegion(userContext.SelectedRegion));
                    }
                }
            }

            return languages;
        }

        public static IEnumerable<ColorChoice> GetColorChoiceOptions(this FieldDefinition fieldDefinition)
        {
            var colorChoiceOptions = new List<ColorChoice>();
            if (fieldDefinition.FieldTypeCode == FieldType.ColorChoice && !string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                colorChoiceOptions.AddRange(fieldDefinition.GetSettings<ColorChoiceSettings>().ColorChoices);
            }

            return colorChoiceOptions;
        }

        public static string GetTableName(this FieldDefinition fieldDefinition)
        {
            if (fieldDefinition.FieldTypeCode == FieldType.Translations && !string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                return fieldDefinition.GetSettings<TranslationsSettings>().TranslatedContentTableName;
            }

            return string.Empty;
        }

        public static string[] GetToggleFieldNames(this FieldDefinition fieldDefinition)
        {
            if ((fieldDefinition.FieldTypeCode == FieldType.Boolean || fieldDefinition.FieldTypeCode == FieldType.RegionInt)
                && !string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                return fieldDefinition.GetSettings<BooleanSettings>().DataToggle;
            }

            return new string[0];
        }

        public static bool HideFieldsOnToggle(this FieldDefinition fieldDefinition)
        {
            if ((fieldDefinition.FieldTypeCode == FieldType.Boolean || fieldDefinition.FieldTypeCode == FieldType.RegionInt)
                && !string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                return fieldDefinition.GetSettings<BooleanSettings>().DataToggleHide;
            }

            return false;
        }

        public static bool ResetFieldValuesOnToggle(this FieldDefinition fieldDefinition)
        {
            if ((fieldDefinition.FieldTypeCode == FieldType.Boolean || fieldDefinition.FieldTypeCode == FieldType.RegionInt)
                && !string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                return fieldDefinition.GetSettings<BooleanSettings>().DataToggleResetValues;
            }

            return false;
        }

        public static int GetRegionIntValue(this FieldDefinition fieldDefinition, object idFieldValue, string regionId, bool useDefaultValue)
        {
            if (fieldDefinition.FieldTypeCode == FieldType.RegionInt && !string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                var regionIntSettings = fieldDefinition.GetSettings<RegionIntSettings>();
                regionIntSettings.ContentItemId = idFieldValue.ToString();
                regionIntSettings.RegionId = regionId;

                return DependencyResolver.Current.GetService<ICommonService>().GetContentItemRegionIntValue(regionIntSettings, useDefaultValue && !string.IsNullOrWhiteSpace(fieldDefinition.DefaultValue) ? Convert.ToByte(fieldDefinition.DefaultValue) : 0);
            }

            return 0;
        }

        public static string GetRelatedTableNameRegionIntValue(this FieldDefinition fieldDefinition)
        {
            //TODO
            if (fieldDefinition.FieldTypeCode == FieldType.RegionInt && !string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                var regionIntSettings = fieldDefinition.GetSettings<RegionIntSettings>();
                return regionIntSettings.RelatedTableField;
            }

            return null;
        }

        public static string GetRegionTitleValue(this FieldDefinition fieldDefinition, string idFieldName, object idFieldValue, string regionId)
        {
            if (fieldDefinition.FieldTypeCode == FieldType.RegionTitle && !string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                var regionTitleSettings = fieldDefinition.GetSettings<RegionTitleSettings>();
                regionTitleSettings.TableIdField = idFieldName;
                regionTitleSettings.TableIdValue = idFieldValue.ToString();
                regionTitleSettings.CurrentRegionId = regionId;

                return DependencyResolver.Current.GetService<ICommonService>().GetItemRegionTitle(regionTitleSettings);
            }

            return string.Empty;
        }

        public static string GetSwitchDescription(this FieldDefinition fieldDefinition)
        {
            if ((fieldDefinition.FieldTypeCode == FieldType.Boolean || fieldDefinition.FieldTypeCode == FieldType.RegionInt)
                && !string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                return fieldDefinition.GetSettings<BooleanSettings>().SwitchDescription;
            }

            return null;
        }

        public static string GetValueFromJsonCustomSettings(this FieldDefinition fieldDefinition, string settingKey)
        {
            dynamic customSettingsObject = null;
            if (string.IsNullOrEmpty(fieldDefinition.CustomSettings) || string.IsNullOrEmpty(settingKey))
            {
                return string.Empty;
            }
            try
            {
                customSettingsObject = fieldDefinition.GetSettings<dynamic>();
            }
            catch
            {
                Pulsus.LogManager.EventFactory.Create().Level(Pulsus.LoggingEventLevel.Error).Text("Custom settings are not in Json format.").Push();
            }

            return customSettingsObject != null ? (string)customSettingsObject[settingKey] : string.Empty;
        }

        public static IEnumerable<FieldDefinition> ExcludeTranslations(this IEnumerable<FieldDefinition> fieldDefinitions)
        {
            return fieldDefinitions.Where(o => o.FieldTypeCode != FieldType.Translations);
        }

        public static IEnumerable<FieldDefinition> GetSelectClauseColumns(this IEnumerable<FieldDefinition> fieldDefinitions)
        {
            return fieldDefinitions
                .Where(o => o.FieldTypeCode != FieldType.MultipleLookup)
                .Where(o => o.FieldTypeCode != FieldType.MultipleLookupGroup)
                .Where(o => o.FieldTypeCode != FieldType.FeatureOn)
                .Where(o => o.FieldTypeCode != FieldType.Attachment)
                .Where(o => o.FieldTypeCode != FieldType.Tag)
                .Where(o => o.FieldTypeCode != FieldType.TriggerMarketing)
                .Where(o => o.FieldTypeCode != FieldType.CampaignEvents)
                .Where(o => o.FieldTypeCode != FieldType.RegionTitle)
                .Where(o => o.FieldTypeCode != FieldType.RegionInt)
                .Where(o => o.FieldTypeCode != FieldType.SecondaryImage)
                .Where(o => o.FieldTypeCode != FieldType.Location);
        }

        public static List<string> GetItemsClauseForInsert(this IEnumerable<FieldDefinition> fieldDefinitions, IDictionary<string, object> contentData)
        {
            var definitions = fieldDefinitions as FieldDefinition[] ?? fieldDefinitions.ToArray();
            var multipleLookupFields = definitions.Where(o => o.FieldTypeCode == FieldType.MultipleLookup || o.FieldTypeCode == FieldType.MultipleLookupGroup).Select(o => o.Name);
            var tagsFields = definitions.Where(o => o.FieldTypeCode == FieldType.Tag).Select(o => o.Name);
            var featureOnFields = definitions.Where(o => o.FieldTypeCode == FieldType.FeatureOn).Select(o => o.Name);
            var attachmentsFields = definitions.Where(o => o.FieldTypeCode == FieldType.Attachment).Select(o => o.Name);
            var secondaryImagesFields = definitions.Where(o => o.FieldTypeCode == FieldType.SecondaryImage).Select(o => o.Name);
            var vanityUrlFields = definitions.Where(o => o.FieldTypeCode == FieldType.VanityUrl && o.Required).Select(o => o.Name).ToList();
            var translationsFields = definitions.Where(o => o.FieldTypeCode == FieldType.Translations).Select(o => o.Name);
            var triggerMarketingFields = definitions.Where(o => o.FieldTypeCode == FieldType.TriggerMarketing).Select(o => o.Name);
            var campaignEventsFields = definitions.Where(o => o.FieldTypeCode == FieldType.CampaignEvents).Select(o => o.Name);
            var regionTitleFields = definitions.Where(o => o.FieldTypeCode == FieldType.RegionTitle).Select(o => o.Name);
            var regionIntFields = definitions.Where(o => o.FieldTypeCode == FieldType.RegionInt).Select(o => o.Name);
            var LocationFields = definitions.Where(o => o.FieldTypeCode == FieldType.Location).Select(o => o.Name);

            var itemsClause = contentData.Keys
                .Where(o => !multipleLookupFields.Contains(o))
                .Where(o => !tagsFields.Contains(o))
                .Where(o => !featureOnFields.Contains(o))
                .Where(o => !attachmentsFields.Contains(o))
                .Where(o => !translationsFields.Contains(o))
                .Where(o => !triggerMarketingFields.Contains(o))
                .Where(o => !campaignEventsFields.Contains(o))
                .Where(o => !regionTitleFields.Contains(o))
                .Where(o => !regionIntFields.Contains(o))
                .Where(o => !secondaryImagesFields.Contains(o))
                .Where(o => !LocationFields.Contains(o))
                .ToList();

            if (!itemsClause.Any(vanityUrlFields.Contains))
            {
                itemsClause.AddRange(vanityUrlFields);
            }

            return itemsClause;
        }

        public static void SetListItemModifiedTrackingFields(this IEnumerable<FieldDefinition> fieldDefinitions, IDictionary<string, object> itemData, int userId)
        {
            var updateTrackingField = fieldDefinitions.FirstOrDefault(o => o.FieldTypeCode == FieldType.TrackingUpdatedDate);

            if (updateTrackingField != null)
            {
                itemData[updateTrackingField.Name] = DateTime.Now;
            }

            var modifiedByField = fieldDefinitions.FirstOrDefault(o => o.FieldTypeCode == FieldType.TrackingModifiedBy);

            if (modifiedByField != null)
            {
                itemData[modifiedByField.Name] = userId;
            }
        }

        public static void SetVanityUrl(this IEnumerable<FieldDefinition> fieldDefinitions, string itemId, IDictionary<string, object> data)
        {
            var vanityUrlFields = fieldDefinitions.Where(o => o.FieldTypeCode == FieldType.VanityUrl);

            foreach (var vanityUrlField in vanityUrlFields)
            {
                string parsedUrl;
                if (data.ContainsKey(vanityUrlField.Name))
                {
                    var url = Convert.ToString(data[vanityUrlField.Name]);
                    parsedUrl = url.Replace("{id}", itemId);
                }
                else // falback logic when the field for the url is empty
                {
                    if (string.IsNullOrEmpty(vanityUrlField.CustomSettings))
                    {
                        throw new InvalidOperationException("No CustomSettings defined for the VanityUrl field: " + vanityUrlField.Name);
                    }

                    var settings = JsonConvert.DeserializeObject<VanityUrlSettings>(vanityUrlField.CustomSettings);
                    string titleData;

                    // if the data object contains a Title field we use that for building the url
                    if (data.ContainsKey(MarketingCenterDbConstants.FieldNames.Title) && !string.IsNullOrEmpty(Convert.ToString(data[MarketingCenterDbConstants.FieldNames.Title])))
                    {
                        titleData = Convert.ToString(data[MarketingCenterDbConstants.FieldNames.Title]);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(settings.DefaultTitleField) &&
                            data.ContainsKey(settings.DefaultTitleField)
                        ) // if not we use the field specified in the settings
                        {
                            titleData = Convert.ToString(data[settings.DefaultTitleField]);
                        }
                        else
                        {
                            throw new InvalidOperationException("The field used for DefaultTitle is invalid: " + settings.DefaultTitleField);
                        }
                    }

                    parsedUrl = $"/{settings.UrlPath.Trim('/').Replace("{id}", itemId)}/{titleData}";
                }

                data[vanityUrlField.Name] = UrlService.GetPartialUrl(parsedUrl).stringCleanPartialUrl();
            }
        }

        public static void OverrideBusinessOwnerAssetTypeCustomSettings(this FieldDefinition fieldDefinition, ViewDataDictionary viewData)
        {
            if (fieldDefinition.Name.Equals(MarketingCenter.Data.MarketingCenterDbConstants.FieldNames.AssetType))
            {
                var originalOptions = fieldDefinition.GetChoiceSettings();
                viewData["BusinessOwnerAssetTypes"] = new ChoiceSettings() { Options = originalOptions.BusinessOwnerOptions };
            }
        }

        public static string GetMultipleLookupReadOnlyValues(this FieldDefinition fieldDefinition, object id, string regionId)
        {
            if ((fieldDefinition.FieldTypeCode == FieldType.MultipleLookup || fieldDefinition.FieldTypeCode == FieldType.MultipleLookupGroup) 
                && !string.IsNullOrEmpty(fieldDefinition.CustomSettings))
            {
                var lookupSettings = fieldDefinition.GetSettings<LookupSettings>();
                lookupSettings.RegionId = regionId;
                if (id != null && lookupSettings.AssociationKeysMap.Any() && !string.IsNullOrEmpty(lookupSettings.AssociationTableName))
                {
                    return string.Join(",", DependencyResolver.Current.GetService<LookupServices>().GetSelectedLookupValues(id, lookupSettings));
                }
            }

            return string.Empty;
        }
    }
}