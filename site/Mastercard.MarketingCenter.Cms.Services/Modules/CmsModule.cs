﻿using Autofac;
using Mastercard.MarketingCenter.Cms.Services.Infrastructure;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Cms.Services.Services.Issuers;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Data;

namespace Mastercard.MarketingCenter.Cms.Services.Modules
{
    public class CmsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AttachmentServices>();
            builder.RegisterType<SecondaryImageServices>();
            builder.RegisterType<CloningService>().As<ICloningService>();
            builder.RegisterType<CmsDbConnection>().As<ICmsDbConnection>().SingleInstance(); /// IMPORTANT => SingleInstance() guarantees multiple transactions integrity, do not delete it.
            builder.RegisterType<CommonService>().As<ICommonService>();
            builder.RegisterType<ContentItemDao>();
            builder.RegisterType<ContentItemVersionDao>();
            builder.RegisterType<ContentService>();
            builder.RegisterType<ContentTypeDao>();
            builder.RegisterType<ContentTypeService>();
            builder.RegisterType<CustomizationServices>();
            builder.RegisterType<FeatureLocationDao>();
            builder.RegisterType<FeatureOnServices>();
            builder.RegisterType<FieldDataNormalizationContext>().As<IFieldDataNormalizationContext>();
            builder.RegisterType<FieldDefinitionDao>();
            builder.RegisterType<GroupServices>();
            builder.RegisterType<HttpContextCachingService>().As<ICachingService>();
            builder.RegisterType<ListDao>();
            builder.RegisterType<ListItemTrackingServices>();
            builder.RegisterType<LookupServices>();
            builder.RegisterType<MatchingServices>();
            builder.RegisterType<NavigationService>().As<INavigationService>();
            builder.RegisterType<ReportServices>();
            builder.RegisterType<SearchServices>();
            builder.RegisterType<SegmentationTranslatedContentDao>();
            builder.RegisterType<TagCategoryDao>();
            builder.RegisterType<TagDao>();
            builder.RegisterType<TagHierarchyPositionDao>();
            builder.RegisterType<TagServices>();
            builder.RegisterType<TagTranslatedContentDao>();
            builder.RegisterType<TranslationServices>();
            builder.RegisterType<TriggerMarketingServices>();

            builder.RegisterAssemblyTypes(System.Reflection.Assembly.GetExecutingAssembly())
                   .Where(t => typeof(IFieldDataRetrieval).IsAssignableFrom(t))
                   .AsImplementedInterfaces();
        }
    }
}