﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class GroupServices
    {
        private readonly ICmsDbConnection _connection;
        private readonly IUserRepository _userRepository;
        private readonly GroupDao _groupDao;

        public GroupServices(ICmsDbConnection connection, IUserRepository userRepository, GroupDao groupDao)
        {
            _connection = connection;
            _userRepository = userRepository;
            _groupDao = groupDao;
        }

        public void CreateGroup(Group group)
        {
            _groupDao.Add(group);
        }

        public void DeleteGroup(int id)
        {
            var group = _groupDao.GetById(id);
            //delete user group
            _groupDao.Delete(group);
        }

        public void RemoveUser(int id, int userid)
        {
            var group = _groupDao.GetById(id);
            var user = _userRepository.GetUser(userid);
            group.Users.Remove(user);
        }
    }
}