﻿using Mastercard.MarketingCenter.Cms.Services.Core;
using Mastercard.MarketingCenter.Cms.Services.Models;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Data.Extensions;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class LookupServices
    {
        private readonly ICmsDbConnection _connection;
        public LookupServices(ICmsDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<KeyValueItem> GetLookupValues(LookupSettings settings, bool showCurrentRegionId = false)
        {
            string sql = string.Empty;
            if (settings.ContentTypeLookup)
            {
                sql = $@"SELECT t.{settings.TableIdField}, t.{settings.TableValueField} FROM {settings.TableName} t, ContentItem ci 
                         WHERE t.ContentItemID = ci.ContentItemId AND ci.RegionId = '{settings.RegionId}' 
                         AND t.ContentItemID NOT LIKE '%-p' AND (ci.ExpirationDate > GETDATE() OR ci.ExpirationDate IS NULL) 
                         AND (ci.StatusID NOT IN ('6', '8', '9'))";
            }
            else
            {
                sql = $"SELECT {settings.TableIdField}, {settings.TableValueField} FROM {settings.TableName}";
                if (!string.IsNullOrWhiteSpace(settings.TableRegionField))
                {
                    sql += $" WHERE {settings.TableRegionField} = '{settings.RegionId}'";
                }
            }

            var vals = _connection.Query(sql)
                                  .Select(o => ((IDictionary<string, object>)o))
                                  .Select(o =>
                                              new KeyValueItem
                                              {
                                                  Id = Convert.ToString(o[settings.TableIdField]),
                                                  Value = Convert.ToString(o[settings.TableValueField])
                                              });

            if (settings.IdsToExclude != null)
            {
                vals = vals.Where(x => !settings.IdsToExclude.Any(y => string.Equals(y.Trim(), x.Id.Trim(), StringComparison.InvariantCultureIgnoreCase)));
            }

            if (!showCurrentRegionId && settings.ExcludeCurrentRegionId)
            {
                vals = vals.Where(x => !string.Equals(settings.RegionId?.Trim(), x.Id.Trim(), StringComparison.InvariantCultureIgnoreCase));
            }

            return vals;
        }

        public IEnumerable<string> GetSelectedLookupValues(object id, LookupSettings settings)
        {
            var sql = $@"SELECT t.{settings.TableIdField}
                         FROM {settings.TableName} t
                         JOIN {settings.AssociationTableName} a ON a.{settings.AssociationKeysMap.First()} = t.{settings.TableIdField}
                         WHERE a.{settings.AssociationKeysMap.Last()} = @id";

            return _connection.Query<string>(sql, new { id });
        }

        public void SetLookupValues(object id, LookupSettings settings, IEnumerable<string> values)
        {
            _connection.ExecuteInTransaction(() =>
            {
                if (settings.ContentTypeLookup && !settings.RegionId.IsNullOrWhiteSpace())
                {
                    _connection.Execute($@"
                                            DELETE t
                                            FROM [{settings.AssociationTableName}] t
                                            JOIN [ContentItem] ci ON t.[{settings.AssociationKeysMap.First()}] = ci.[ContentItemID]
                                            WHERE t.[{settings.AssociationKeysMap.Last()}] = '{id}' AND ci.[RegionId] = '{settings.RegionId}'
                                        ");
                }
                else
                {
                    _connection.Execute($"DELETE {settings.AssociationTableName} WHERE {settings.AssociationKeysMap.Last()} = @id", new { id });
                }

                foreach (var value in values)
                {
                    AddLookupValue(id, settings, value);
                }
            });
        }

        private void AddLookupValue(object id, LookupSettings settings, object value)
        {
            var insertClause = $"INSERT INTO {settings.AssociationTableName} ({settings.AssociationKeysMap.First()}, {settings.AssociationKeysMap.Last()}) VALUES (@value, @id)";
            _connection.Execute(insertClause, new { id, value });
        }

        public void PopulateMultipleLookupValues(IDictionary<string, object> contentData, IEnumerable<FieldDefinition> fieldDefinitions, string itemId)
        {
            foreach (var fieldDefinition in fieldDefinitions.Where(o => o.FieldTypeCode == FieldType.MultipleLookup || o.FieldTypeCode == FieldType.MultipleLookupGroup || o.FieldTypeCode == FieldType.CampaignEvents))
            {
                if (!string.IsNullOrEmpty(fieldDefinition.CustomSettings))
                {
                    var lookupSettings = fieldDefinition.GetSettings<LookupSettings>();
                    if (lookupSettings.AssociationKeysMap.Any() || !string.IsNullOrEmpty(lookupSettings.AssociationTableName))
                    {
                        contentData.AddOrUpdate(fieldDefinition.Name, string.Join(",", GetSelectedLookupValues(itemId, lookupSettings)));
                    }
                }
            }
        }

        public void SetMultipleLookupValues(IDictionary<string, object> contentData, IEnumerable<FieldDefinition> fieldDefinitions, string itemId, string regionId)
        {
            foreach (var multipleLookupField in fieldDefinitions.Where(o => o.FieldTypeCode == FieldType.MultipleLookup || o.FieldTypeCode == FieldType.MultipleLookupGroup || o.FieldTypeCode == FieldType.CampaignEvents))
            {
                var lookupValues = Enumerable.Empty<string>();
                if (contentData.ContainsKey(multipleLookupField.Name) && !string.IsNullOrEmpty(Convert.ToString(contentData[multipleLookupField.Name])))
                {
                    lookupValues = Convert.ToString(contentData[multipleLookupField.Name]).Split(',');
                }

                var lookupSettings = multipleLookupField.GetSettings<LookupSettings>();
                lookupSettings.RegionId = regionId;

                SetLookupValues(itemId, lookupSettings, lookupValues);
            }
        }

        public IEnumerable<GroupKeyValueItem> GetMultipleLookupGroupValues(LookupGroupSettings settings)
        {
            string sql = string.Empty;
            if (settings.ContentTypeLookup)
            {
                var isContentItemTable = settings.ContentItemTableValueField && settings.ContentItemTableNames != null && settings.ContentItemTableNames.Any();
                StringBuilder contentItemJoin = new StringBuilder();
                StringBuilder contentItemValueField = new StringBuilder();
                if (isContentItemTable)
                {
                    contentItemValueField.AppendLine($@"ContentItemValueField = CASE");
                    foreach (var tableName in settings.ContentItemTableNames)
                    {
                        contentItemJoin.AppendLine($@"LEFT JOIN {tableName} WITH (nolock) ON {tableName}.ContentItemId = ci.ContentItemId");
                        contentItemValueField.AppendLine($@"WHEN {tableName}.ContentItemId IS NOT NULL THEN {tableName}.{settings.TableValueField}");
                    }
                    contentItemValueField.AppendLine($@"END");
                }

                string regionApplicabilityJoinClause = string.Empty;
                string regionApplicabilityWhereClause = string.Empty;
                string regionVisibilityJoinClause = string.Empty;
                string regionVisibilityWhereClause = string.Empty;
                if (settings.IncludeCrossBorderRegions && !settings.RegionApplicabilityTableName.IsNullOrWhiteSpace())
                {
                    regionApplicabilityJoinClause += $" LEFT JOIN {settings.RegionApplicabilityTableName} ON t.ContentItemID = {settings.RegionApplicabilityTableName}.ContentItemId AND {settings.RegionApplicabilityTableName}.RegionId = '{settings.RegionId}'";
                    regionApplicabilityWhereClause += $" OR t.ContentItemId = {settings.RegionApplicabilityTableName}.ContentItemId";

                    if (!settings.RegionApplicabilityTableName.IsNullOrWhiteSpace())
                    {
                        regionVisibilityJoinClause += $" LEFT JOIN {settings.RegionVisibilityTableName} ON t.ContentItemID = {settings.RegionVisibilityTableName}.ContentItemId AND {settings.RegionVisibilityTableName}.RegionId = '{settings.RegionId}'";
                        regionVisibilityWhereClause += $" AND COALESCE({settings.RegionVisibilityTableName}.[Value], 1) = 1";
                    }
                }
                
                sql = $@"SELECT * FROM (
                         SELECT g.{settings.GroupTableIdField}, g.{settings.GroupTableValueField}, t.{settings.TableIdField}, {(isContentItemTable ? contentItemValueField.ToString() : $"t.{settings.TableValueField} AS ContentItemValueField")}
                         FROM   {settings.TableName} t
                         JOIN   {settings.GroupTableName} g ON t.{settings.TableGroupField} = g.{settings.GroupTableIdField}
                         JOIN   ContentItem ci ON t.ContentItemId = ci.ContentItemId
                         {regionApplicabilityJoinClause}
                         {regionVisibilityJoinClause}
                         {(isContentItemTable ? contentItemJoin.ToString() : string.Empty)}
                         WHERE  t.ContentItemID = ci.ContentItemId AND (ci.RegionId = '{settings.RegionId}' {regionApplicabilityWhereClause})
                         {regionVisibilityWhereClause}
                         AND    t.ContentItemID NOT LIKE '%-p' 
                         AND    (ci.ExpirationDate > GETDATE() OR ci.ExpirationDate IS NULL) 
                         AND    (ci.StatusID NOT IN ('6', '8', '9'))) r
                         WHERE  ContentItemValueField is not null";
            }
            else
            {
                sql = $@"SELECT g.{settings.GroupTableIdField}, g.{settings.GroupTableValueField}, t.{settings.TableIdField}, t.{settings.TableValueField} AS ContentItemValueField
                         FROM   {settings.TableName} t
                         JOIN   {settings.GroupTableName} g ON  t.{settings.TableGroupField} = g.{settings.GroupTableIdField}";

                if (!string.IsNullOrWhiteSpace(settings.TableRegionField))
                {
                    sql += $" WHERE {settings.TableRegionField} = '{settings.RegionId}'";
                }
            }

            return _connection.Query(sql)
                              .Select(o => ((IDictionary<string, object>)o))
                              .GroupBy(o => new
                              {
                                  GroupId = o[settings.TableGroupField],
                                  GroupValue = o[settings.GroupTableValueField]
                              })
                              .Select(o => new GroupKeyValueItem
                              {
                                  GroupId = o.Key.GroupId.ToString(),
                                  GroupValue = o.Key.GroupValue.ToString(),
                                  GroupItems = o.Select(g => new KeyValueItem
                                  {
                                      Id = Convert.ToString(g[settings.TableIdField]),
                                      Value = Convert.ToString(g["ContentItemValueField"])
                                  })
                              });
        }

        public void CleanUpMultipleLookupValues(IDictionary<string, object> contentData, IDictionary<string, object> originalContentData, IEnumerable<FieldDefinition> fieldDefinitions, string itemId)
        {
            if (originalContentData != null)
            {
                foreach (var multipleLookupField in fieldDefinitions.Where(o => (o.FieldTypeCode == FieldType.MultipleLookup || o.FieldTypeCode == FieldType.CampaignEvents) && o.GetSettings<LookupSettings>().CleanUp))
                {
                    var deleteLookupValues = Enumerable.Empty<string>();
                    if (contentData.ContainsKey(multipleLookupField.Name) && !string.IsNullOrEmpty(Convert.ToString(contentData[multipleLookupField.Name])) && originalContentData.ContainsKey(multipleLookupField.Name))
                    {
                        deleteLookupValues = Convert.ToString(originalContentData[multipleLookupField.Name]).Split(',').Where(od => !Convert.ToString(contentData[multipleLookupField.Name]).Split(',').Contains(od));
                    }

                    DeleteLookupValues(multipleLookupField.GetSettings<LookupSettings>(), deleteLookupValues);
                }
            }
        }

        public void DeleteLookupValues(LookupSettings settings, IEnumerable<string> values)
        {
            _connection.ExecuteInTransaction(() =>
            {
                _connection.Execute($"DELETE {settings.TableName} WHERE {settings.TableIdField} in (@ids)", new { ids = string.Join(", ", values) });
            });
        }
    }
}