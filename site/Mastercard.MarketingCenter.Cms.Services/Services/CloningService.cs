﻿using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Slam.Cms.Data;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class CloningService : ICloningService
    {
        private readonly ContentService _contentService;
        private readonly ContentTypeDao _contentTypeDao;

        public CloningService(ContentService contentService, ContentTypeDao contentTypeDao)
        {
            _contentService = contentService;
            _contentTypeDao = contentTypeDao;
        }

        public string Clone(
            string contentTypeId,
            IDictionary<string, object> contentData,
            int userId,
            string regionId)
        {
            var contentType = _contentTypeDao.GetById(contentTypeId);
            return _contentService.AddContentCore(contentData, userId, contentType, Status.DraftOnly, regionId?.Trim());
        }

        public IEnumerable<ContentTypeFieldDefinition> GetOnlyInterRegionCloning(IEnumerable<ContentTypeFieldDefinition> fieldDefinitions)
        {
            return fieldDefinitions.Where(fd => fd.InterRegionCloning);
        }

        public IEnumerable<ContentTypeFieldDefinition> FilterOutFieldsForClones(IDictionary<string, object> contentItemData, IEnumerable<ContentTypeFieldDefinition> fieldDefinitions, bool isAConfirmedClone = false)
        {
            // if the content item is not a clone the method filters the fields which are applicable only in clones based on a custom setting
            var hasSourceItemId = contentItemData.ContainsKey("OriginalContentItemId") && !string.IsNullOrEmpty((string)contentItemData["OriginalContentItemId"]);

            if (!(isAConfirmedClone || hasSourceItemId))
            {
                var fieldsForNonClones = fieldDefinitions.Where(fd => !(fd.FieldDefinition.CustomSettings?.Contains("OnlyInClones") ?? false));
                return fieldsForNonClones;
            }

            return fieldDefinitions;
        }
    }
}