﻿using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class SearchServices
    {
        private readonly ICmsDbConnection _connection;
        private readonly IAuthorizationService _authorizationService;

        public SearchServices(ICmsDbConnection connection, IAuthorizationService authorizationService)
        {
            _connection = connection;
            _authorizationService = authorizationService;
        }

        public IEnumerable<SearchResult> SearchFor(string searchTerm, string RegionId)
        {
            var results = _connection.Query<SearchResult>("CmsSearch",
                                                          new { searchTerm, RegionId }, commandType: CommandType.StoredProcedure);
            return results.AsQueryable()
                          .Where(r =>
                              (!string.IsNullOrEmpty(r.ContentTypeId) && _authorizationService.Authorize(null, "Content", "Edit", r.ContentTypeId, null, null)) ||
                              (r.ListId.HasValue && _authorizationService.Authorize(null, "List", "Edit", r.ListId.ToString(), null, null))
                          );
        }
    }

    public class SearchResult
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string ItemType { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string FrontEndUrl { get; set; }
        public int? ListId { get; set; }
        public string ContentTypeId { get; set; }
        public int Rank { get; set; }
    }
}