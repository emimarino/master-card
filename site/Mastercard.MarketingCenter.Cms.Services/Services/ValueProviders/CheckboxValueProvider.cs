﻿using System;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Services.Services.ValueProviders
{
    public class CheckboxValueProvider : ValueProvider
    {
        public override object GetData(object data)
        {
            if (string.IsNullOrEmpty(Convert.ToString(data))) 
                return string.Empty;

            return ";#" + string.Join(";#", Convert.ToString(data).Split(',').ToArray()) + ";#";
        }
    }
}