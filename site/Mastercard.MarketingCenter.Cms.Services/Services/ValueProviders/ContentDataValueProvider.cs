﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Services.Services.ValueProviders
{
    public class ContentDataValueProvider
    {
        public IDictionary<string, object> GetContentData(IEnumerable<FieldDefinition> fieldDefs, IDictionary<string, object> contentData)
        {
            var resultData = new Dictionary<string, object>(contentData);

            foreach (var fieldDef in fieldDefs)
            {
                var valueProvider = ValueProviderFactory.Create(fieldDef.FieldType);

                if (valueProvider == null) continue;

                var data = contentData.ContainsKey(fieldDef.Name) ? contentData[fieldDef.Name] : null;

                resultData[fieldDef.Name] = valueProvider.GetData(data);
            }

            return resultData;
        }
    }
}