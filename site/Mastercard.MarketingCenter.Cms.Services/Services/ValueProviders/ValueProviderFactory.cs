﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Cms.Services.Services.ValueProviders
{
    public static class ValueProviderFactory
    {
        public static ValueProvider Create(FieldType fieldType)
        {
            switch (fieldType.FieldTypeCode)
            {
                case FieldType.Checkbox:
                    return new CheckboxValueProvider();
                case FieldType.Customization:
                    return new CustomizationValueProvider();
                case FieldType.Number:
                    return new NumberValueProvider();
                case FieldType.FeatureOn:
                    return new NullValueProvider();
                default:
                    return null;
            }
        }
    }
}