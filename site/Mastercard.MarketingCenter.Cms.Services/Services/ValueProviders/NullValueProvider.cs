﻿namespace Mastercard.MarketingCenter.Cms.Services.Services.ValueProviders
{
    public class NullValueProvider : ValueProvider
    {
        public override object GetData(object data)
        {
            return null;
        }
    }
}