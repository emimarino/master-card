﻿using System;

namespace Mastercard.MarketingCenter.Cms.Services.Services.ValueProviders
{
    public class NumberValueProvider : ValueProvider
    {
        public override object GetData(object data)
        {
            if (data == null || string.IsNullOrEmpty(Convert.ToString(data).Trim()))
                return null;

            return data;
        }
    }
}