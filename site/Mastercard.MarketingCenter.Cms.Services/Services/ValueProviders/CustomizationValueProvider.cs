﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Cms.Services.Services.ValueProviders
{
    public class CustomizationValueProvider : ValueProvider
    {
        public override object GetData(object data)
        {
            var model = data as CustomizationField;

            if (model != null)
                return model.Customizable;

            return null;
        }
    }
}