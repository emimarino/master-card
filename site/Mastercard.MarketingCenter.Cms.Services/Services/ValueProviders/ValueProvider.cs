﻿namespace Mastercard.MarketingCenter.Cms.Services.Services.ValueProviders
{
    public abstract class ValueProvider
    {
        public abstract object GetData(object data);
    }
}