﻿using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Data.Extensions;
using Mastercard.MarketingCenter.Services.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Services.Services.Validators
{
    public class DateTimeFieldValidator : FieldValidator
    {
        public override ErrorItem ValidateField(FieldDefinition field, IDictionary<string, object> itemData, IEnumerable<FieldDefinition> fieldDefinitions, IDictionary<string, object> additionalItemData)
        {
            if (field.FieldType.FieldTypeCode == FieldType.DateTime)
            {
                var data = itemData[field.Name];
                DateTime dateOut;
                if (!DateTime.TryParse(Convert.ToString(data), out dateOut))
                {
                    if (Convert.ToString(data).Length > field.Length)
                        return new ErrorItem
                        {
                            Key = field.Name,
                            ErrorMessage = $"{field.Description} is not a valid DateTime value"
                        };
                }
                else if (!string.IsNullOrEmpty(field.CustomSettings))
                {
                    var dateSettings = field.GetSettings<DateTimeSettings>();
                    DateTime otherFieldDate;
                    DateTime startDate = dateOut;
                    DateTime endDate = dateOut;
                    string startDateName = field.Name;
                    string endDateName = field.Name;

                    if (dateSettings.MustBeAfterNow && dateOut.Date < DateTime.Now.Date)
                    {
                        return new ErrorItem
                        {
                            Key = field.Name,
                            ErrorMessage = $"{field.Description} can't be before current date."
                        };
                    }

                    if (!string.IsNullOrEmpty(dateSettings.GreaterThanFieldName))
                    {
                        var otherFieldData = itemData.GetData(dateSettings.GreaterThanFieldName);
                        if (otherFieldData != null &&
                            DateTime.TryParse(Convert.ToString(otherFieldData), out otherFieldDate))
                        {
                            startDate = otherFieldDate;
                            endDate = dateOut;
                            startDateName = dateSettings.GreaterThanFieldName;
                            endDateName = field.Name;
                            if (endDate < startDate)
                                return new ErrorItem
                                {
                                    Key = field.Name,
                                    ErrorMessage = $"{field.Description} should be after than the {fieldDefinitions.First(o => o.Name == dateSettings.GreaterThanFieldName).Description}"
                                };

                        }
                    }
                    else if (!string.IsNullOrEmpty(dateSettings.LowerThanFieldName))
                    {
                        var otherFieldData = itemData.GetData(dateSettings.LowerThanFieldName);
                        if (otherFieldData != null &&
                            DateTime.TryParse(Convert.ToString(otherFieldData), out otherFieldDate))
                        {
                            startDate = dateOut;
                            endDate = otherFieldDate;
                            startDateName = field.Name;
                            endDateName = dateSettings.LowerThanFieldName;
                            if (startDate > endDate)
                                return new ErrorItem
                                {
                                    Key = field.Name,
                                    ErrorMessage = $"{field.Description} should be before than the {fieldDefinitions.First(o => o.Name == dateSettings.LowerThanFieldName).Description}"
                                };
                        }
                    }

                    if (!string.IsNullOrEmpty(dateSettings.UniqueSpanInTableName) && dateSettings.UniqueSpanInFieldNames != null && startDateName != endDateName)
                    {
                        var _listDao = DependencyResolver.Current.GetService<ListDao>();
                        var list = _listDao.GetByTableName(dateSettings.UniqueSpanInTableName);
                        var columns = new Dictionary<FieldDefinition, string>();
                        dateSettings.UniqueSpanInFieldNames.ToList().ForEach(fieldName => columns.Add(new FieldDefinition { Name = fieldName }, itemData[fieldName].ToString()));
                        var startDateColumn = new KeyValuePair<string, DateTime>(startDateName, startDate);
                        var endDateColumn = new KeyValuePair<string, DateTime>(endDateName, endDate);
                        var _contentService = DependencyResolver.Current.GetService<ContentService>();
                        if (_contentService.ExistItemInDateRange(list, startDateColumn, endDateColumn, columns, itemData.FirstOrDefault(i => i.Key == list.GetIdField().Name)))
                        {
                            string errMsg = string.Empty;
                            var fieldDescriptions = fieldDefinitions.Where(fd => dateSettings.UniqueSpanInFieldNames.Contains(fd.Name)).Select(fd => fd.Description).ToList();
                            fieldDescriptions.ForEach(d => errMsg += (d == fieldDescriptions.First() ? d : d == fieldDescriptions.Last() ? " and " + d : ", " + d));
                            return new ErrorItem
                            {
                                Key = field.Name,
                                ErrorMessage = $"The dates, {startDate.ToString("d")} to {endDate.ToString("d")}, conflict with existing {list.Name} with the same {errMsg}."
                            };
                        }
                    }
                }
            }

            return null;
        }
    }
}