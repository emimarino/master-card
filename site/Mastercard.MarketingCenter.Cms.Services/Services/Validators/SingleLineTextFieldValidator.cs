﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Services.Extensions;
using System;
using System.Collections.Generic;
using System.Net;

namespace Mastercard.MarketingCenter.Cms.Services.Services.Validators
{
    public class SingleLineTextFieldValidator : FieldValidator
    {
        public override ErrorItem ValidateField(FieldDefinition field, IDictionary<string, object> itemData, IEnumerable<FieldDefinition> fieldDefinitions, IDictionary<string, object> additionalItemData)
        {
            var data = itemData[field.Name];
            if (field.FieldType.FieldTypeCode == FieldType.SingleLineText && !string.IsNullOrEmpty(Convert.ToString(data).Trim()) && !string.IsNullOrEmpty(field.CustomSettings))
            {
                var settings = field.GetSettings<SingleLineTextSettings>();
                if (settings != null && settings.IsYoutubeVideoUrl)
                {
                    try
                    {
                        // We call the youtube validation url, if video is not valid, it returns a WebRequest exception error.
                        WebRequest.Create($"https://www.youtube.com/oembed?url={data}").GetResponse();
                    }
                    catch
                    {
                        return new ErrorItem
                        {
                            Key = field.Name,
                            ErrorMessage = $"{field.Description} does not have a valid YouTube video url."
                        };
                    }
                }
            }

            return null;
        }
    }
}