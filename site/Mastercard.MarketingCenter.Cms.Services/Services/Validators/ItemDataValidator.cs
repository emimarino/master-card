﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Services.Services.Validators
{
    public class ItemDataValidator
    {
        private static readonly List<FieldValidator> FieldValidators = new List<FieldValidator>
        {
            new FieldLengthValidator(),
            new DateTimeFieldValidator(),
            new NumberFieldValidator(),
            new RequiredFieldValidator(),
            new TranslationsFieldValidator(),
            new FileTypeValidator(),
            new UniqueFieldValidator(),
            new CampaignEventsFieldValidator(),
            new SingleLineTextFieldValidator()
        };

        public IEnumerable<ErrorItem> ValidateData(IEnumerable<FieldDefinition> fieldDefinitions, IDictionary<string, object> itemData, IDictionary<string, object> additionalItemData)
        {
            var errors = new List<ErrorItem>();

            foreach (var field in fieldDefinitions)
            {
                if ((field.FieldTypeCode == FieldType.MultipleLookup || field.FieldTypeCode == FieldType.MultipleLookupGroup) && field.Required && !itemData.ContainsKey(field.Name))
                {
                    itemData.Add(field.Name, null);
                }

                if (itemData.ContainsKey(field.Name))
                {
                    FieldValidators.ForEach(validator =>
                    {
                        var valResult = validator.ValidateField(field, itemData, fieldDefinitions, additionalItemData);
                        if (valResult != null)
                        {
                            errors.Add(valResult);
                        }
                    });
                }
            }

            return errors;
        }

        public IEnumerable<ErrorItem> ValidateExpirationDate(bool mustExpire, DateTime? expirationDate, DateTime? currentExpirationDate, bool mustBeAtLeastADayFromNow = false, bool expirationCanBeInThePast = false)
        {
            var errors = new List<ErrorItem>();

            if (mustExpire && expirationDate == null)
            {
                errors.Add(new ErrorItem { Key = "ExpirationDate", ErrorMessage = "Expiration Date is required" });
            }
            if (expirationDate != null && expirationDate.GetValueOrDefault().Date > DateTime.Now.AddMonths(18).Date)
            {
                errors.Add(new ErrorItem { Key = "ExpirationDate", ErrorMessage = "Expiration Date cannot be later than 18 months from now" });
            }
            if (expirationCanBeInThePast && !mustBeAtLeastADayFromNow && expirationDate != null && expirationDate.GetValueOrDefault() < DateTime.Now)
            {
                errors.Add(new ErrorItem { Key = "ExpirationDate", ErrorMessage = "Expiration Date cannot be in the past" });
            }
            if (expirationCanBeInThePast && mustBeAtLeastADayFromNow && expirationDate != null && expirationDate.GetValueOrDefault().Date < DateTime.Now.AddDays(1).Date)
            {
                errors.Add(new ErrorItem { Key = "ExpirationDate", ErrorMessage = "Expiration date must be at least a day from now" });
            }
            if (currentExpirationDate != null && expirationDate.GetValueOrDefault().Date <= currentExpirationDate.GetValueOrDefault().Date)
            {
                errors.Add(new ErrorItem { Key = "ExpirationDate", ErrorMessage = $"The asset's expiration date cannot be older than the published asset expiration: {currentExpirationDate.GetValueOrDefault().Date.ToString("MM/dd/yyyy")}" });
            }

            return errors;
        }
    }
}