﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Services.Services.Validators
{
    public class CampaignEventsFieldValidator : FieldValidator
    {
        public override ErrorItem ValidateField(FieldDefinition field, IDictionary<string, object> itemData, IEnumerable<FieldDefinition> fieldDefinitions, IDictionary<string, object> additionalItemData)
        {
            if (field.FieldType.FieldTypeCode == FieldType.CampaignEvents)
            {
                var showInMarketingCalendarField = fieldDefinitions.FirstOrDefault(f => f.Name == MarketingCenterDbConstants.FieldNames.ShowInMarketingCalendar);
                bool showInMarketingCalendar = true;
                if (showInMarketingCalendarField != null && itemData.ContainsKey(showInMarketingCalendarField.Name))
                {
                    showInMarketingCalendar = (bool)itemData[showInMarketingCalendarField.Name];
                }

                if (showInMarketingCalendar)
                {
                    DateTime inMarketStartDate = DateTime.MinValue;
                    DateTime inMarketEndDate = DateTime.MaxValue;

                    var inMarketStartDateField = fieldDefinitions.FirstOrDefault(f => f.Name == MarketingCenterDbConstants.FieldNames.InMarketStartDate);
                    var inMarketEndDateField = fieldDefinitions.FirstOrDefault(f => f.Name == MarketingCenterDbConstants.FieldNames.InMarketEndDate);

                    if (inMarketStartDateField != null && inMarketEndDateField != null &&
                        itemData.ContainsKey(inMarketStartDateField.Name) &&
                        itemData.ContainsKey(inMarketEndDateField.Name))
                    {
                        inMarketStartDate = itemData[inMarketStartDateField.Name] == null ? inMarketStartDate : (DateTime)itemData[inMarketStartDateField.Name];
                        inMarketEndDate = itemData[inMarketEndDateField.Name] == null ? inMarketEndDate : (DateTime)itemData[inMarketEndDateField.Name];
                    }

                    DateTime expirationDate = DateTime.MaxValue;
                    if (additionalItemData.ContainsKey("MustExpire") && (bool)additionalItemData["MustExpire"] &&
                        additionalItemData.ContainsKey(MarketingCenterDbConstants.FieldNames.ExpirationDate) &&
                        additionalItemData[MarketingCenterDbConstants.FieldNames.ExpirationDate] != null)
                    {
                        expirationDate = (DateTime)additionalItemData[MarketingCenterDbConstants.FieldNames.ExpirationDate];
                    }

                    string message = string.Empty;
                    List<string> eventsPastExpiration = new List<string>();

                    var data = itemData[field.Name];
                    var campaignEventIds = Convert.ToString(data)?.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries) ?? new string[0];
                    if (campaignEventIds.Length > 0)
                    {
                        var calendarService = DependencyResolver.Current.GetService<ICalendarService>();
                        var campaignEvents = calendarService.GetCampaignEventsFilterByIds(campaignEventIds);
                        if (campaignEvents != null)
                        {
                            var campaignCategories = campaignEvents.Where(ce => !ce.AlwaysOn).GroupBy(e => e.CampaignCategories);
                            foreach (var campaignCategory in campaignCategories)
                            {
                                var campaignCategoryEvents = campaignCategory.OrderByDescending(e => e.AlwaysOn).ThenBy(e => e.StartDate);
                                var previousEvent = campaignCategoryEvents.First();

                                var minStartDate = previousEvent.StartDate;
                                var maxEndDate = campaignCategoryEvents.Max(e => e.EndDate);

                                if (minStartDate.Value < inMarketStartDate)
                                {
                                    message += "Campaign Events cannot start prior to In Market Start Date.<br/>";
                                }
                                if (maxEndDate.Value > inMarketEndDate)
                                {
                                    message += "Campaign Events cannot end after In Market End Date.<br/>";
                                }

                                if (previousEvent.EndDate.Value > expirationDate)
                                {
                                    eventsPastExpiration.Add(previousEvent.Title);
                                }

                                if (campaignCategoryEvents.Count() > 1) //checking for overlap is only relevant for multiple events
                                {
                                    foreach (var campaignEvent in campaignCategoryEvents.Skip(1))
                                    {
                                        if (campaignEvent.EndDate.Value > expirationDate)
                                        {
                                            eventsPastExpiration.Add(campaignEvent.Title);
                                        }
                                        previousEvent = campaignEvent;
                                    }
                                }

                            }
                        }
                    }
                    else
                    {
                        message = $"Must have {field.Description} when Show on Calendar is enabled";
                    }

                    if (!string.IsNullOrEmpty(message) || eventsPastExpiration.Count > 0)
                    {
                        foreach (var e in eventsPastExpiration)
                        {
                            message += $"The Event \"{e}\" extends beyond the Program Expiration Date.<br />";
                        }

                        return new ErrorItem
                        {
                            Key = field.Name,
                            ErrorMessage = message
                        };
                    }
                }
            }
            return null;
        }
    }
}