﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Services.Services.Validators
{
    public class TranslationsFieldValidator : FieldValidator
    {
        private string RemoveHtml(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                return (new Regex(@"(<\s*/?\s*p\s*>)|(<\s*/?\s*br\s*>)", (RegexOptions.IgnoreCase | RegexOptions.Compiled))).Replace(input, string.Empty);
            }

            return input;
        }

        public override ErrorItem ValidateField(FieldDefinition field, IDictionary<string, object> itemData, IEnumerable<FieldDefinition> fieldDefinitions, IDictionary<string, object> additionalItemData)
        {
            if (field.FieldType.FieldTypeCode == FieldType.Translations && !string.IsNullOrEmpty(field.CustomSettings))
            {
                var settings = field.GetSettings<TranslationsSettings>();
                if (settings != null && settings.Languages == null)
                {
                    var userContext = DependencyResolver.Current.GetService<UserContext>();
                    var translationService = DependencyResolver.Current.GetService<TranslationServices>();
                    settings.Languages = translationService.GetAvailableLanguagesForRegion(userContext.SelectedRegion).ToArray();
                }

                var data = itemData[field.Name];
                if (!string.IsNullOrEmpty(Convert.ToString(data).Trim().Trim('{', '}')))
                {
                    var jsonData = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, object>>>(System.Web.HttpUtility.HtmlDecode(Convert.ToString(data)));
                    if (settings != null && settings.Languages.Count() > 0 && jsonData != null)
                    {
                        var _fieldDefinitionDao = DependencyResolver.Current.GetService<FieldDefinitionDao>();
                        if (field.Required)
                        {
                            if (settings.RequiredFieldDefinitionIds != null && settings.RequiredFieldDefinitionIds.Length > 0)
                            {
                                foreach (var requireFieldDefinitionId in settings.RequiredFieldDefinitionIds)
                                {
                                    var requiredField = _fieldDefinitionDao.GetById(requireFieldDefinitionId);

                                    if ((!jsonData.Keys.Any(k => k.Equals(settings.Languages.First().Code, StringComparison.OrdinalIgnoreCase)) ||
                                        jsonData.Any(d => d.Key.Equals(settings.Languages.First().Code, StringComparison.OrdinalIgnoreCase) && d.Value.Where(v => v.Key == requiredField.Name).Any(v => string.IsNullOrWhiteSpace(RemoveHtml(v.Value?.ToString()))))))
                                    {
                                        return new ErrorItem
                                        {
                                            Key = field.Name,
                                            ErrorMessage = $"{field.Description} must have values in {settings.Languages.First().Name} {requiredField.Description}"
                                        };
                                    }
                                }
                            }
                            else
                            {
                                //If the field is required, (only) the first set of values for the first item in the language list must have values set
                                var validKeys = jsonData.Keys.Where(k => k.Equals(settings.Languages.First().Code, StringComparison.OrdinalIgnoreCase));
                                var emptyValues = jsonData.FirstOrDefault(v => v.Key.Equals(settings.Languages.First().Code, StringComparison.OrdinalIgnoreCase))
                                                          .Value.Where(v => string.IsNullOrWhiteSpace(RemoveHtml(v.Value?.ToString())));
                                if (!validKeys.Any())
                                {
                                    return new ErrorItem
                                    {
                                        Key = field.Name,
                                        ErrorMessage = $"{field.Description} must have values in {settings.Languages.First().Name}"
                                    };
                                }
                                else if (emptyValues.Any())
                                {
                                    return new ErrorItem
                                    {
                                        Key = $"{field.Description}_{emptyValues.First().Key}_{settings.Languages.First().Code}",
                                        ErrorMessage = $"{emptyValues.First().Key.AddSpacesToSentence(true)} in {field.Description} must have values in {settings.Languages.First().Name}"
                                    };
                                }
                            }
                        }
                    }
                }
                else if (field.Required)
                {
                    return new ErrorItem
                    {
                        Key = field.Name,
                        ErrorMessage = $"{field.Description} must have values in {settings.Languages.First().Name}"
                    };
                }
            }

            return null;
        }
    }
}