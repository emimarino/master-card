﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Services.Extensions;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Services.Services.Validators
{
    public class NumberFieldValidator : FieldValidator
    {
        public override ErrorItem ValidateField(FieldDefinition field, IDictionary<string, object> itemData, IEnumerable<FieldDefinition> fieldDefinitions, IDictionary<string, object> additionalItemData)
        {
            var data = itemData[field.Name];
            if (field.FieldType.FieldTypeCode == FieldType.Number && !string.IsNullOrEmpty(Convert.ToString(data).Trim()))
            {
                if (!string.IsNullOrEmpty(field.CustomSettings))
                {
                    var settings = field.GetSettings<NumberSettings>();

                    if (settings != null && settings.IsDecimal)
                    {
                        decimal numberOut;

                        if (!decimal.TryParse(Convert.ToString(data).Replace(".", System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator).Replace(",", System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator), out numberOut))
                        {
                            return new ErrorItem
                            {
                                Key = field.Name,
                                ErrorMessage = $"{field.Description} is not a valid decimal number value"
                            };
                        }
                    }
                }
                else
                {
                    int numberOut;
                    if (!int.TryParse(Convert.ToString(data), out numberOut))
                    {
                        return new ErrorItem
                        {
                            Key = field.Name,
                            ErrorMessage = $"{field.Description} is not a valid number value"
                        };
                    }
                }
            }

            return null;
        }
    }
}