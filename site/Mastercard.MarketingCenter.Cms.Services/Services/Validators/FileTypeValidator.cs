﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Services.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Services.Services.Validators
{
    public class FileTypeValidator : FieldValidator
    {
        public override ErrorItem ValidateField(FieldDefinition field, IDictionary<string, object> itemData, IEnumerable<FieldDefinition> fieldDefinitions, IDictionary<string, object> additionalItemData)
        {
            var data = itemData[field.Name];
            if (FieldType.FileFieldsTypes.Any(f => field.FieldTypeCode == f) && !string.IsNullOrEmpty(Convert.ToString(data).Trim()))
            {
                return ValidateFile(field, data, CheckFileExtension);
            }
            if (!string.IsNullOrEmpty(field.CustomSettings) && field.CustomSettings.Contains("AcceptFiles") && !string.IsNullOrEmpty(Convert.ToString(data).Trim()))
            {
                return ValidateFile(field, data, CheckFile);
            }

            return null;
        }

        private ErrorItem ValidateFile(FieldDefinition field, object data, Func<string, FieldDefinition, ErrorItem> validate)
        {
            if (FieldType.FileListFieldsTypes.Any(f => field.FieldTypeCode == f))
            {
                var listErrorItems = new List<ErrorItem>();
                var listData = JsonConvert.DeserializeObject<List<string>>(Convert.ToString(data));
                foreach (var i in listData)
                {
                    listErrorItems.Add(validate(i, field));
                }

                return listErrorItems.FirstOrDefault(ei => ei != null);
            }

            return validate(data.ToString(), field);
        }

        private ErrorItem CheckFileExtension(string filename, FieldDefinition field)
        {
            if (string.IsNullOrEmpty(Path.GetExtension(Convert.ToString(filename).Trim())))
            {
                return new ErrorItem
                {
                    Key = field.Name,
                    ErrorMessage = $"{field.Description} has an empty file format."
                };
            }

            return null;
        }

        private ErrorItem CheckFile(string filename, FieldDefinition field)
        {
            var settings = field.GetSettings<FileTypeSettings>();
            if (settings != null && settings.AcceptFiles?.Length > 0 &&
                       !settings.AcceptFiles.Any(af => af.Equals(GetMimeType(Convert.ToString(filename).Trim()), StringComparison.OrdinalIgnoreCase) ||
                                                       af.Equals(Path.GetExtension(Convert.ToString(filename).Trim()), StringComparison.OrdinalIgnoreCase)))
            {
                string errMsg = string.Empty;
                settings.AcceptFiles.ToList().ForEach(s => errMsg += ("\n " + s));

                return new ErrorItem
                {
                    Key = field.Name,
                    ErrorMessage = $"{field.Description} does not have a valid file format. Enter one of the following: {errMsg}"
                };
            }

            return null;
        }

        private string GetMimeType(string filename)
        {
            var extension = Path.GetExtension(filename);
            if (string.IsNullOrEmpty(extension))
            {
                return "application/octet-stream";
            }

            switch (extension.Replace(".", "").ToLower())
            {
                case "doc":
                case "dot":
                case "docx":
                    return "application/msword";
                case "jpe":
                case "jpeg":
                case "jpg":
                    return "image/jpeg";
                case "gif":
                    return "image/gif";
                case "png":
                    return "image/png";
                case "pdf":
                    return "application/pdf";
                default:
                    return "application/octet-stream";
            }
        }
    }
}