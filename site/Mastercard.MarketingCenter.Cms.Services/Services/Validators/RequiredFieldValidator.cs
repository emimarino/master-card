﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Mastercard.MarketingCenter.Cms.Services.Services.Validators
{
    public class RequiredFieldValidator : FieldValidator
    {
        private string RemoveHtml(string input, ref FieldDefinition field)
        {
            if (!string.IsNullOrEmpty(input) && field.FieldType.FieldTypeCode.Equals(FieldType.HtmlText, StringComparison.OrdinalIgnoreCase))
            {
                return (new Regex(@"(<\s*/?\s*p\s*>)|(<\s*/?\s*br\s*>)", (RegexOptions.IgnoreCase | RegexOptions.Compiled))).Replace(input, string.Empty);
            }

            return input;
        }

        public override ErrorItem ValidateField(FieldDefinition field, IDictionary<string, object> itemData, IEnumerable<FieldDefinition> fieldDefinitions, IDictionary<string, object> additionalItemData)
        {
            var data = itemData[field.Name];
            if (field.Required && (data == null || string.IsNullOrEmpty(RemoveHtml(Convert.ToString(data).Trim(), ref field))))
            {
                return new ErrorItem
                {
                    Key = field.Name,
                    ErrorMessage = $"{field.Description} is required."
                };
            }

            if (field.DefaultValue != null)
            {
                if (field.Name.ToLower() == "uri" && data.ToString().Trim().ToLower() == field.DefaultValue.Trim().ToLower())
                {
                    return new ErrorItem
                    {
                        Key = field.Name,
                        ErrorMessage = $"{field.Description} must be added after 'page\'"
                    };
                }
            }

            return null;
        }
    }
}