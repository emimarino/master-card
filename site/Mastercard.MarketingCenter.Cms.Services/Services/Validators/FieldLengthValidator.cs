﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Services.Services.Validators
{
    public class FieldLengthValidator : FieldValidator
    {
        public override ErrorItem ValidateField(FieldDefinition field, IDictionary<string, object> itemData, IEnumerable<FieldDefinition> fieldDefinitions, IDictionary<string, object> additionalItemData)
        {
            if (field.Length > 0)
            {
                var data = itemData[field.Name];
                if (Convert.ToString(data).Length > field.Length)
                {
                    return new ErrorItem
                    {
                        Key = field.Name,
                        ErrorMessage = $"{field.Description} exceeds the max length(field max length: {field.Length})"
                    };
                }
            }

            return null;
        }
    }
}