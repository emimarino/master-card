﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Services.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Cms.Services.Services.Validators
{
    public class UniqueFieldValidator : FieldValidator
    {
        public override ErrorItem ValidateField(FieldDefinition field, IDictionary<string, object> itemData, IEnumerable<FieldDefinition> fieldDefinitions, IDictionary<string, object> additionalItemData)
        {
            if (!string.IsNullOrEmpty(field.CustomSettings) && field.CustomSettings.Contains("FieldNames") && field.CustomSettings.Contains("TableName"))
            {
                var settings = field.GetSettings<UniqueFieldSettings>();
                if (settings != null && !string.IsNullOrWhiteSpace(settings.TableName) && settings.FieldNames.Count() > 0 && settings.FieldNames.All(fn => fieldDefinitions.Select(fd => fd.Name).Contains(fn) && itemData.Keys.Contains(fn)))
                {
                    var columns = new Dictionary<FieldDefinition, string>();
                    settings.FieldNames.ToList().ForEach(fieldName => columns.Add(new FieldDefinition { Name = fieldName }, itemData[fieldName].ToString()));

                    if (DependencyResolver.Current.GetService<ContentService>().ExistItemData(settings.TableName, fieldDefinitions, columns, itemData.FirstOrDefault(i => i.Key == fieldDefinitions.FirstOrDefault(o => o.FieldType.IsIdentifierField()).Name), settings.IgnoreWhiteSpaces, settings.IgnoreNullOrEmpty))
                    {
                        string errMsg = string.Empty;
                        var fieldDescriptions = fieldDefinitions.Where(fd => settings.FieldNames.Contains(fd.Name) && fd.Name != "RegionId")
                                                                .Select(fd => (fd.FieldTypeCode.Equals(FieldType.Translations)) ? "Title" : fd.Description)
                                                                .ToList();
                        fieldDescriptions.ForEach(d => errMsg += (d == fieldDescriptions.First() ? d : d == fieldDescriptions.Last() ? $" and {d}" : $", {d}"));

                        var existsOnRegion = fieldDefinitions.Any(fd => fd?.Name != "RegionId");

                        string entityTitle = DependencyResolver.Current.GetService<ICommonService>().GetEntityTitle(settings.TableName);
                        var entityTitleStartsWithAVowel = new List<string> { "a", "e", "i", "o", "u" }.Any(c => entityTitle.StartsWith(c, StringComparison.OrdinalIgnoreCase));

                        return new ErrorItem
                        {
                            Key = field.Name,
                            ErrorMessage = $"{(entityTitleStartsWithAVowel ? "An" : "A")} {entityTitle.TrimEnd('s')} with the same {errMsg} already exists{(existsOnRegion ? " in this region" : string.Empty)}."
                        };
                    }
                }
            }

            return null;
        }
    }
}