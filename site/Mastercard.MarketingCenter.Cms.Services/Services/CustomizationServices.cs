﻿using Mastercard.MarketingCenter.Services.Interfaces;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class CustomizationServices
    {
        private readonly ICmsDbConnection _connection;

        public CustomizationServices(ICmsDbConnection connection)
        {
            _connection = connection;
        }

        public IEnumerable<CustomizationInfo> GetContentItemCustomizations(string contentItemId)
        {
            const string sql = @"select co.*,
                                    case
		                                when (select aco.Required from OrderableAssetCustomizationOption aco where aco.ContentItemID = @contentItemId and aco.CustomizationOptionID = co.CustomizationOptionID) = 1 then 'Required'
		                                when (select aco.Required from OrderableAssetCustomizationOption aco where aco.ContentItemID = @contentItemId and aco.CustomizationOptionID = co.CustomizationOptionID) = 0 then 'Optional'
		                                when (select aco.Required from OrderableAssetCustomizationOption aco where aco.ContentItemID = @contentItemId and aco.CustomizationOptionID = co.CustomizationOptionID) is NULL then 'Off'
	                                end as State
                                 from CustomizationOption co";

            return _connection.Query<CustomizationInfo>(sql, new { contentItemId });
        }

        public void SaveContentItemCustomizations(string contentItemId, IEnumerable<string> requiredCustomizationIds, IEnumerable<string> optionalCustomizationIds)
        {
            _connection.ExecuteInTransaction(() =>
            {
                _connection.Execute("DELETE OrderableAssetCustomizationOption WHERE ContentItemID = @contentItemId",
                                    new { contentItemId });

                foreach (var optionalCustomizationId in optionalCustomizationIds)
                {
                    _connection.Execute("INSERT INTO OrderableAssetCustomizationOption VALUES(@contentItemId, @optionalCustomizationId, 0)",
                                        new { contentItemId, optionalCustomizationId });
                }

                foreach (var requiredCustomizationId in requiredCustomizationIds)
                {
                    _connection.Execute("INSERT INTO OrderableAssetCustomizationOption VALUES(@contentItemId, @requiredCustomizationId, 1)",
                                        new { contentItemId, requiredCustomizationId });
                }
            });
        }

        public IEnumerable<CustomizationInfo> GetAllCustomizations()
        {
            return _connection.Query<CustomizationInfo>("select co.*, 'Off' as State from CustomizationOption co");
        }
    }

    public enum CustomizationState
    {
        Off = 1,
        Optional = 2,
        Required = 3
    }

    public class CustomizationInfo
    {
        public string CustomizationOptionId { get; set; }

        public string FieldType { get; set; }
        public string FriendlyName { get; set; }
        public string Instructions { get; set; }
        public string InternalName { get; set; }

        public CustomizationState State { get; set; }
    }
}