﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class ReportServices
    {
        private readonly ContentTypeDao _contentTypeDao;
        private readonly ICmsDbConnection _connection;

        public ReportServices(ICmsDbConnection connection, ContentTypeDao contentTypeDao)
        {
            _connection = connection;
            _contentTypeDao = contentTypeDao;
        }

        public PagedList<dynamic> GetClonedAssetsNewSourceUpdates(
            int startIndex,
            int recordCount,
            List<string> sqlColumns,
            IDictionary<string, bool> orderByClause,
            string regionId)
        {
            var sqlSelects = _contentTypeDao
                    .GetAll()
                    .ToList()
                    .Where(x => x.Clonable)
                    .Select(ct =>
                            $@"SELECT '{ct.Title}' AS ContentType, cloned.ContentItemId, cloned.Title,
                                sourceStatus.Name as SourceAssetStatus
                            FROM {ct.TableName} cloned
                            INNER JOIN ContentItem clonedItem ON clonedItem.ContentItemId = cloned.ContentItemId
                            INNER JOIN ContentItem sourceItem ON sourceItem.ContentItemID = cloned.OriginalContentItemId
                            INNER JOIN Status sourceStatus ON sourceStatus.StatusID = sourceItem.StatusID
                            WHERE 
                                clonedItem.RegionId = '{regionId.Trim()}'
                                and sourceItem.ModifiedDate > clonedItem.ModifiedDate
                                and cloned.NotifyWhenSourceChanges = 1
                                and (clonedItem.StatusID<>4 or (clonedItem.StatusID=4 and clonedItem.contentitemid not like '%-p') )");

            var query = string.Join(" UNION ", sqlSelects);

            var sql = BuildPagedSql(query, sqlColumns, orderByClause, startIndex, recordCount);

            return ExecuteQueryPaged(sql);
        }

        public PagedList<dynamic> GetHomeReports(
            int startIndex,
            int recordCount,
            List<string> sqlColumns,
            IDictionary<string, bool> orderByClause,
            string regionId,
            DateTime? fromDate,
            DateTime? toDate,
            int excludeReviewState = -1,
            bool onlyBusinessOwnerRequest = false
            )
        {
            sqlColumns.Add("CASE WHEN result.ContentItemId LIKE '%-p' THEN (SELECT CreatedDate FROM ContentItem WHERE ContentItemId = SUBSTRING(result.ContentItemId, 1, LEN(result.ContentItemId) - 2)) ELSE CreatedDate END AS CreatedDate");
            sqlColumns.Add("CASE WHEN result.ContentItemId LIKE '%-p' THEN (SELECT ExpirationDate FROM ContentItem WHERE ContentItemId = SUBSTRING(result.ContentItemId, 1, LEN(result.ContentItemId) - 2)) ELSE ExpirationDate END AS ExpirationDate");
            sqlColumns.Add("ModifiedDate");
            sqlColumns.Add("BusinessOwner");
            sqlColumns.Add("BusinessOwnerRequest");

            var contentTypes = _contentTypeDao
                    .GetAll();
            if (onlyBusinessOwnerRequest)
            {
                contentTypes = contentTypes.Where(ct => ct.BusinessOwnerCanRequest);
            }

            var sqlSelects =
                contentTypes
                    .Where(ct => ct.CanExpire)
                    .ToList()
                    .Select(ct => $@"SELECT '{ct.Title}' AS ContentType,
                                            {ct.TableName}.ContentItemId,
                                            {ct.TableName}.Title,
                                            ContentItem.CreatedDate,
                                            ContentItem.ExpirationDate,
                                            ContentItem.ModifiedDate,
                                            {(MarketingCenterDbConstants.ContentTypeIds.BusinessOwnerIds.Contains(ct.ContentTypeId) ?
                                            "u.[FullName]" : "''")} as BusinessOwner,
                                            ers.Name as BusinessOwnerRequest
                                     FROM {ct.TableName}
                                     JOIN ContentItem ON ContentItem.ContentItemId = {ct.TableName}.ContentItemId
                                     INNER JOIN ExpirationReviewState ers ON ContentItem.ReviewStateId = ers.ReviewStateId
                                     {(MarketingCenterDbConstants.ContentTypeIds.BusinessOwnerIds.Contains(ct.ContentTypeId) ?
                                     $"LEFT JOIN [User] u ON u.UserID = {ct.TableName}.BusinessOwnerID" : string.Empty)}
                                     WHERE  (ContentItem.StatusId != 6 AND ContentItem.StatusId != 8) 
                                            AND ers.ReviewStateId != '{excludeReviewState}' 
                                            AND (ContentItem.ContentItemId NOT LIKE '%-p') 
                                            AND (ContentItem.RegionId = '{regionId.Trim()}')
                                            {(toDate.HasValue && fromDate.HasValue ? $@"
                                            AND (ContentItem.ExpirationDate > '{fromDate.Value.ToString("yyyy-MM-dd HH:mm:ss")}'
                                            AND ContentItem.ExpirationDate < '{toDate.Value.ToString("yyyy-MM-dd HH:mm:ss")}')" :
                                            string.Empty)}");

            var query = string.Join(" UNION ", sqlSelects);
            if (!string.IsNullOrEmpty(query))
            {
                var sql = BuildPagedSql(query, sqlColumns, orderByClause, startIndex, recordCount);

                return ExecuteQueryPaged(sql);
            }
            else
            {
                return new PagedList<dynamic>(new List<dynamic>(), 0);
            }
        }

        protected string BuildPagedSql(
            string query,
            IEnumerable<string> sqlColumns,
            IDictionary<string, bool> orderBy,
            int startIndex,
            int recordCount)
        {
            var orderBySql = new List<string>();
            foreach (var a in orderBy)
            {
                orderBySql.Add(a.Key + " " + (a.Value ? " ASC " : " DESC "));
            }

            var sb = new StringBuilder();
            sb.AppendLine(($"SELECT COUNT(*) FROM ({query}) AS resultCount;"));
            sb.AppendLine($@"SELECT * 
                             FROM (SELECT ROW_NUMBER() OVER 
                                  (ORDER BY {string.Join(" , ", orderBySql.ToArray())}) AS RowNum, {string.Join(",", sqlColumns)} FROM ({query})
                             AS result) b
                             WHERE RowNum > {startIndex} AND RowNum <= {(startIndex + recordCount)}
                             ORDER BY RowNum;");

            return sb.ToString();
        }

        protected PagedList<dynamic> ExecuteQueryPaged(string sql)
        {
            int count;
            IEnumerable<dynamic> items;
            using (var multi = _connection.QueryMultiple(sql))
            {
                count = multi.Read<int>().Single();
                items = multi.Read<dynamic>();
            }

            return new PagedList<dynamic>(items, count);
        }
    }
}