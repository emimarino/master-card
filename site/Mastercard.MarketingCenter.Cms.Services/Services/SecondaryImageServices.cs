﻿using Mastercard.MarketingCenter.Cms.Services.Core;
using Mastercard.MarketingCenter.Cms.Services.Infrastructure;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Data.Extensions;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class SecondaryImageServices
    {
        private readonly ICmsDbConnection _connection;
        private readonly ContentTypeDao _contentTypeDao;
        private readonly ICommonService _commonService;

        public SecondaryImageServices(ICmsDbConnection connection, ContentTypeDao contentTypeDao, IContentItemService contentItemServices, ICommonService commonService)
        {
            _connection = connection;
            _contentTypeDao = contentTypeDao;
            _commonService = commonService;
        }

        public IEnumerable<SecondaryImageInfo> GetSecondaryImages(string contentItemId)
        {
            if (_connection.ExecuteScalar<int>($"SELECT Count(ContentItemId) FROM ContentItem WHERE ContentItemId = '{contentItemId}'") <= 0)
                return Enumerable.Empty<SecondaryImageInfo>();

            var secondaryImageSettings = GetSecondaryImageSettings(contentItemId);
            var sql = string.Format(@"SELECT * FROM {0} WHERE ContentItemId = @contentItemId", secondaryImageSettings.TableName);

            return _connection.Query<SecondaryImageInfo>(sql, new { contentItemId });
        }

        public void SetSecondaryImages(string itemId, IDictionary<string, object> data, IEnumerable<FieldDefinition> fieldDefinitions)
        {
            var secondaryImageFieldDef = fieldDefinitions.FirstOrDefault(o => o.FieldTypeCode == FieldType.SecondaryImage);
            if (secondaryImageFieldDef != null)
            {
                var attachments = Enumerable.Empty<string>();
                var value = data.GetData(secondaryImageFieldDef.Name);
                if (!string.IsNullOrEmpty(Convert.ToString(value).Trim()))
                {
                    attachments = JsonConvert.DeserializeObject<List<string>>(Convert.ToString(value));
                }

                SaveSecondaryImages(itemId, attachments);
            }
        }

        public void SaveSecondaryImages(string contentItemId, IEnumerable<string> secondaryImages)
        {
            _connection.ExecuteInTransaction(() =>
            {
                var secondaryImageSettings = GetSecondaryImageSettings(contentItemId);
                var deleteSql = $"DELETE {secondaryImageSettings.TableName} WHERE ContentItemId = @contentItemId";

                _connection.ExecuteScalar(deleteSql, new { contentItemId });

                foreach (var secondaryImage in secondaryImages)
                {
                    var insertSql = $"INSERT INTO {secondaryImageSettings.TableName} (ContentItemId, ImageUrl) VALUES(@contentItemId,@secondaryImage)";
                    _connection.ExecuteScalar(insertSql, new { contentItemId, secondaryImage });
                }
            });
        }

        private SecondaryImageSettings GetSecondaryImageSettings(string contentItemId)
        {
            var contentTypeId = _commonService.GetContentTypeId(contentItemId);
            var contentType = _contentTypeDao.GetById(contentTypeId);
            var secondaryImageContentTypeFieldDef = contentType.ContentTypeFieldDefinitions.FirstOrDefault(o => o.FieldDefinition.FieldTypeCode == FieldType.SecondaryImage);

            if (secondaryImageContentTypeFieldDef != null &&
                !string.IsNullOrEmpty(secondaryImageContentTypeFieldDef.FieldDefinition.CustomSettings))
            {
                return secondaryImageContentTypeFieldDef.FieldDefinition.GetSettings<SecondaryImageSettings>();
            }

            throw new InvalidOperationException(string.Format("No Attachment field defined for ContentItemId: {0}", contentItemId));
        }

        public void PopulateSecondaryImagesValues(IDictionary<string, object> contentData, IEnumerable<FieldDefinition> fieldDefinitions, string itemId)
        {
            foreach (var fieldDefinition in fieldDefinitions.Where(o => o.FieldTypeCode == FieldType.SecondaryImage))
            {
                var finalSecondaryImages = GetSecondaryImages(itemId).Select(a => a.ImageUrl);
                if (contentData.ContainsKey(fieldDefinition.Name))
                {
                    var fieldData = (string)contentData[fieldDefinition.Name];
                    if (!string.IsNullOrEmpty(fieldData))
                    {
                        var newAttachments = JsonConvert.DeserializeObject<IEnumerable<string>>((string)contentData[fieldDefinition.Name]);
                        finalSecondaryImages = newAttachments.ToList();
                    }
                    contentData[fieldDefinition.Name] = JsonConvert.SerializeObject(finalSecondaryImages);
                }
                else
                {
                    var newAttachmentsJSON = JsonConvert.SerializeObject(finalSecondaryImages);
                    contentData.Add(fieldDefinition.Name, newAttachmentsJSON);
                }
            }
        }
    }

    public class SecondaryImageInfo
    {
        public string AttachmentId { get; set; }
        public string ContentItemId { get; set; }
        public string ImageUrl { get; set; }
    }
}
