﻿using Mastercard.MarketingCenter.Data.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Services.Services.ItemCustomEvents
{
    public class AssetItemCustomEvent : ItemCustomEvent
    {
        public AssetItemCustomEvent(ICmsDbConnection connection) : base(connection)
        {
        }

        public override void OnUpdateItem(string contentItemId, IDictionary<string, object> contentData)
        {
            var relatedProgramId = contentData.GetData("RelatedProgram");
            if (relatedProgramId != null)
            {
                UpdateItemRelationship("OrderableAssetContentItemId", "OrderableAssetRelatedProgram",
                    "ProgramContentItemId", relatedProgramId.ToString());
                UpdateItemRelationship("DownloadableAssetContentItemId", "DownloadableAssetRelatedProgram",
                    "ProgramContentItemId", relatedProgramId.ToString());
            }
        }
    }
}