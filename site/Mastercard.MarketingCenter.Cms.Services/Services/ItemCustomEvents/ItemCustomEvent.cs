﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Services.Services.ItemCustomEvents
{
    public abstract class ItemCustomEvent
    {
        protected ICmsDbConnection Connection { get; set; }

        protected ItemCustomEvent(ICmsDbConnection connection)
        {
            Connection = connection;
        }

        public virtual void OnUpdateDraftItem(string contentItemId, IDictionary<string, object> contentData)
        {
        }

        public virtual void OnUpdateItem(string contentItemId, IDictionary<string, object> contentData)
        {
        }

        protected void UpdateItemRelationship(string contentItemIdColumn, string relationshipTableName, string relationshipKeyColumn, string contentItemId)
        {
            var relatedItemIds = Connection.Query<string>($"SELECT {contentItemIdColumn} FROM {relationshipTableName} where {relationshipKeyColumn} = @contentItemId",
                                                          new { contentItemId = contentItemId.GetAsLiveContentItemId() });

            var draftItemId = contentItemId.GetAsDraftContentItemId();

            var existDraftItem = Connection.ExecuteScalar<int>("SELECT Count(*) FROM ContentItem WHERE ContentItemID = @draftItemId",
                                                               new { draftItemId }) > 0;

            if (!existDraftItem)
            {
                return;
            }

            foreach (var relatedItemId in relatedItemIds)
            {
                var exist =
                    Connection.ExecuteScalar<int>($"SELECT Count(*) FROM {relationshipTableName} WHERE {relationshipKeyColumn} = @draftItemId and {contentItemIdColumn} = @relatedItemId",
                                                  new { draftItemId, relatedItemId }) > 0;

                if (!exist)
                {
                    Connection.Execute($"INSERT INTO {relationshipTableName} ({contentItemIdColumn},{relationshipKeyColumn}) VALUES(@relatedItemId, @draftItemId)",
                                       new { relatedItemId, draftItemId });
                }
            }
        }
    }
}