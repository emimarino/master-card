﻿using Mastercard.MarketingCenter.Services.Interfaces;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Services.Services.ItemCustomEvents
{
    public class ProgramItemCustomEvent : ItemCustomEvent
    {
        public ProgramItemCustomEvent(ICmsDbConnection connection) : base(connection)
        {
        }

        public override void OnUpdateDraftItem(string contentItemId, IDictionary<string, object> contentData)
        {
            UpdateItemRelationship("OrderableAssetContentItemId", "OrderableAssetRelatedProgram", "ProgramContentItemId", contentItemId);
            UpdateItemRelationship("DownloadableAssetContentItemId", "DownloadableAssetRelatedProgram", "ProgramContentItemId", contentItemId);
        }
    }
}