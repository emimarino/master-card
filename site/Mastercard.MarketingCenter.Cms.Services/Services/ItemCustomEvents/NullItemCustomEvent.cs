﻿using Mastercard.MarketingCenter.Services.Interfaces;

namespace Mastercard.MarketingCenter.Cms.Services.Services.ItemCustomEvents
{
    public class NullItemCustomEvent : ItemCustomEvent
    {
        public NullItemCustomEvent(ICmsDbConnection connection) : base(connection)
        {
        }
    }
}