﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;

namespace Mastercard.MarketingCenter.Cms.Services.Services.ItemCustomEvents
{
    public static class ItemCustomEventFactory
    {
        public static ItemCustomEvent GetItemCustomEvent(ContentType contentType, ICmsDbConnection connection)
        {
            if (contentType.TableName == "Program")
            {
                return new ProgramItemCustomEvent(connection);
            }

            if (contentType.TableName == "OrderableAsset" || contentType.TableName == "DownloadableAsset")
            {
                return new AssetItemCustomEvent(connection);
            }

            return new NullItemCustomEvent(connection);
        }
    }
}