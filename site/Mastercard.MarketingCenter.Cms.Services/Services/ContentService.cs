﻿using Mastercard.MarketingCenter.Cms.Services.Core;
using Mastercard.MarketingCenter.Cms.Services.Infrastructure;
using Mastercard.MarketingCenter.Cms.Services.SerializationWrappers;
using Mastercard.MarketingCenter.Cms.Services.Services.ItemCustomEvents;
using Mastercard.MarketingCenter.Cms.Services.Services.Validators;
using Mastercard.MarketingCenter.Cms.Services.Services.ValueProviders;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Data.Extensions;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Newtonsoft.Json;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class ContentService
    {
        private readonly ICmsDbConnection _connection;
        private readonly ContentTypeService _contentTypeService;
        private readonly LookupServices _lookupServices;
        private readonly TagServices _tagServices;
        private readonly ContentItemVersionDao _contentItemVersionDao;
        private readonly ListDao _listDao;
        private readonly IdGeneratorService _idGeneratorService;
        private readonly CustomizationServices _customizationServices;
        private readonly FeatureOnServices _featureOnServices;
        private readonly AttachmentServices _attachmentServices;
        private readonly SecondaryImageServices _secondaryImageServices;
        private readonly TriggerMarketingServices _triggerMarketingServices;
        private readonly FieldDefinitionDao _fieldDefinitionDao;
        private readonly TranslationServices _translationService;
        private readonly UserContext _userContext;
        private readonly IFieldDataNormalizationContext _fieldDataNormalizationContext;
        private readonly IContentItemService _contentItemService;
        private readonly ICommonService _commonService;
        private readonly IEventLocationService _eventLocationService;
        private readonly IUrlService _urlService;

        public ContentService(
            ICmsDbConnection connection,
            ContentTypeService contentTypeService,
            FieldDefinitionDao fieldDefinitionDao,
            LookupServices lookupServices,
            TagServices tagServices,
            ContentItemVersionDao contentItemVersionDao,
            ListDao listDao,
            IdGeneratorService idGeneratorService,
            CustomizationServices customizationServices,
            FeatureOnServices featureOnServices,
            AttachmentServices attachmentServices,
            SecondaryImageServices secondaryImageServices,
            TriggerMarketingServices triggerMarketingServices,
            TranslationServices translationService,
            UserContext userContext,
            IContentItemService contentItemService,
            IFieldDataNormalizationContext fieldDataNormalizationContext,
            ICommonService commonService,
            IUrlService urlService,
            IEventLocationService eventLocationService)
        {
            _connection = connection;
            _contentTypeService = contentTypeService;
            _lookupServices = lookupServices;
            _tagServices = tagServices;
            _contentItemVersionDao = contentItemVersionDao;
            _listDao = listDao;
            _idGeneratorService = idGeneratorService;
            _customizationServices = customizationServices;
            _featureOnServices = featureOnServices;
            _attachmentServices = attachmentServices;
            _secondaryImageServices = secondaryImageServices;
            _fieldDefinitionDao = fieldDefinitionDao;
            _triggerMarketingServices = triggerMarketingServices;
            _translationService = translationService;
            _userContext = userContext;
            _contentItemService = contentItemService;
            _fieldDataNormalizationContext = fieldDataNormalizationContext;
            _commonService = commonService;
            _urlService = urlService;
            _eventLocationService = eventLocationService;
        }

        public PagedList<dynamic> GetContentFor(ContentType contentType, string regionId, int startIndex = 0, int length = 10, string searchValue = null, IDictionary<string, bool> sortColumns = null, int? statusId = null, bool? isExpired = false, bool showPendings = false)
        {
            var contentTypeFieldDefs = contentType.ContentTypeFieldDefinitions
                .OrderBy(o => o.Order)
                .Select(o => o.FieldDefinition)
                .GetSelectClauseColumns()
                .ToList();

            var searcheableFields = contentType.ContentTypeFieldDefinitions.Where(o => o.Searcheable).Select(o => o.FieldDefinition);

            IEnumerable<FieldDefinition> regionTitleFields = null;
            IEnumerable<FieldDefinition> regionIntFields = null;
            if (contentType.HasCrossBorderRegions)
            {
                regionTitleFields = contentType.ContentTypeFieldDefinitions.Select(o => o.FieldDefinition).Where(o => o.FieldTypeCode == FieldType.RegionTitle);
                contentTypeFieldDefs.AddRange(regionTitleFields);
                regionIntFields = contentType.ContentTypeFieldDefinitions.Select(o => o.FieldDefinition).Where(o => o.FieldTypeCode == FieldType.RegionInt);
                contentTypeFieldDefs.AddRange(regionIntFields);
            }

            return GetSelectResult(contentType.TableName, contentTypeFieldDefs, startIndex, length, searchValue, searcheableFields, sortColumns,
                true, regionId?.Trim(), statusId, isExpired: isExpired, hasCrossBorderRegions: contentType.HasCrossBorderRegions, regionTitleFields: regionTitleFields, regionIntFields: regionIntFields,
                showPendings: showPendings);
        }

        public PagedList<dynamic> GetContentFor(List contentList, string regionId, int startIndex, int length, string searchValue = null, IDictionary<string, bool> sortColumns = null, bool? isExpired = false)
        {
            var contentListFieldDefs = contentList.ListFieldDefinitions
                .OrderBy(o => o.Order)
                .Select(o => o.FieldDefinition)
                .GetSelectClauseColumns()
                .ToList();

            var searcheableFields = contentList.ListFieldDefinitions.Where(o => o.Searcheable).Select(o => o.FieldDefinition);

            var tableResults = GetSelectResult(contentList.TableName, contentListFieldDefs, startIndex, length, searchValue, searcheableFields, sortColumns, contentTypeQuery: false, regionId: regionId?.Trim(), extraWhereClause: contentList.WhereClause, isExpired: isExpired);

            return tableResults;
        }

        public IDictionary<string, object> GetContentItemData(string contentItemId)
        {
            var contentTypeId = _commonService.GetContentTypeId(contentItemId);
            var contentType = _contentTypeService.GetById(contentTypeId);

            return GetItemData(contentItemId, contentType.TableName, contentType.Title, contentType.ContentTypeFieldDefinitions.Select(o => o.FieldDefinition));
        }

        public IDictionary<string, object> GetListItemData(int listId, string listItemId)
        {
            var list = _listDao.GetById(listId);
            var fields = list.ListFieldDefinitions
                .AsEnumerable()
                .FilterByPermission();

            return GetItemData(
                listItemId,
                list.TableName,
                list.Name,
                fields.Select(o => o.FieldDefinition));
        }

        public string AddItemContent(int listId, IDictionary<string, object> contentData, int userId, string regionId = "us")
        {
            var list = _listDao.GetById(listId);
            string newListItemId = string.Empty;
            var fieldDefinitions = list.ListFieldDefinitions.Select(o => o.FieldDefinition).ToList();
            var idField = fieldDefinitions.First(o => o.FieldType.IsIdentifierField());

            _contentItemService.SetListItemTrackingFields(contentData, fieldDefinitions, userId);

            _connection.ExecuteInTransaction(() =>
            {
                newListItemId = _idGeneratorService.GetNewUniqueId();
                contentData[idField.Name] = newListItemId;

                var regionField = list.GetRegionField();
                if (regionField != null && !contentData.ContainsKey(regionField.Name))
                {
                    contentData.Add(regionField.Name, regionId?.Trim());
                }

                InsertItemContent(contentData, fieldDefinitions, list.TableName, newListItemId, regionId);
                SetTranslations(list.TableName, newListItemId, idField.FieldDefinitionId, contentData, fieldDefinitions, regionId);
            });

            return newListItemId;
        }

        public string AddContent(string contentTypeId, IDictionary<string, object> contentData, int userId, string regionId, ContentActions action, int reviewStateId = 0)
        {
            var contentType = _contentTypeService.GetById(contentTypeId);
            var statusId = 0;

            switch (action)
            {
                case ContentActions.SaveAsDraft:
                case ContentActions.SaveAndPreview:
                    statusId = Status.DraftOnly;
                    break;
                case ContentActions.SaveLive:
                    statusId = Status.Published;
                    break;
            }

            return contentData.HasContentItemId()
                ? AddContentCore(contentData, userId, contentType, statusId, regionId?.Trim(), contentData.GetContentItemId(), reviewStateId: reviewStateId)
                : AddContentCore(contentData, userId, contentType, statusId, regionId?.Trim(), reviewStateId: reviewStateId);
        }

        internal string AddContentCore(IDictionary<string, object> contentData, int userId, ContentType contentType, int statusId, string regionId, string itemId = null, int reviewStateId = 0)
        {
            string newContentItemId = string.Empty;
            var date = DateTime.Now;
            const string contentItemInsert = "INSERT INTO ContentItem (contentItemId, primaryContentItemId, contentTypeId, frontEndUrl, statusId, CreatedByUserID, ModifiedByUserID, CreatedDate, ModifiedDate, regionId, reviewStateId) Values(@contentItemId,@primaryContentItemId, @contentTypeId, @frontEndUrl, @statusId, @userId, @userId, @date, @date, @regionId, @reviewStateId)";

            var fieldDefinitions = contentType.ContentTypeFieldDefinitions.Select(o => o.FieldDefinition).ToList();

            _connection.ExecuteInTransaction(() =>
            {
                newContentItemId = itemId ?? _idGeneratorService.GetNewUniqueId();

                fieldDefinitions.SetVanityUrl(newContentItemId, contentData);

                var frontEndUrl = _urlService.SetFrontEndUrl(contentData, contentType.FrontEndUrl, newContentItemId);

                // we add the Content Item ID to the data dictionary
                contentData.SetContentItemId(Convert.ToString(newContentItemId));

                _connection.Execute(contentItemInsert,
                    new
                    {
                        contentItemId = newContentItemId,
                        primaryContentItemId = newContentItemId.GetAsLiveContentItemId(),
                        contentTypeId = contentType.ContentTypeId,
                        frontEndUrl,
                        statusId,
                        userId,
                        date,
                        regionId,
                        reviewStateId
                    });

                InsertItemContent(contentData, fieldDefinitions, contentType.TableName, newContentItemId, regionId);

                _tagServices.SetTags(newContentItemId, contentData);

                UpdateTriggerMarketing(newContentItemId, statusId, contentData, fieldDefinitions);
            });

            return newContentItemId;
        }

        private void InsertItemContent(IDictionary<string, object> contentData, IEnumerable<FieldDefinition> fieldDefinitions, string tableName, string newItemId, string regionId)
        {
            contentData = CleanUri(contentData);
            var fieldDefs = fieldDefinitions as FieldDefinition[] ?? fieldDefinitions.ToArray();
            var itemsClause = fieldDefs.GetItemsClauseForInsert(contentData);

            var customizationData = GetCustomizationFieldData(contentData, fieldDefs);
            var featureOnData = GetFeatureOnData(contentData, fieldDefs);

            var contentDataValueProvider = new ContentDataValueProvider();
            contentData = contentDataValueProvider.GetContentData(fieldDefs, contentData);
            var valuesClause = string.Join(",", itemsClause.Select(o => $"@{o}").ToArray());

            var itemInsert = "INSERT INTO " + tableName + " (" + string.Join(",", itemsClause) + ") VALUES(" + valuesClause + ")";

            _connection.Execute(itemInsert, _connection.GetDynamicParameters(contentData));
            _lookupServices.SetMultipleLookupValues(contentData, fieldDefs, newItemId, regionId);
            _attachmentServices.SetAttachments(newItemId, contentData, fieldDefs);
            _secondaryImageServices.SetSecondaryImages(newItemId, contentData, fieldDefs);
            _eventLocationService.SetEventLocations(newItemId, contentData, fieldDefs, _connection);
            SetRegionIntFields(newItemId, regionId, contentData, fieldDefs);

            if (customizationData != null)
            {
                _customizationServices.SaveContentItemCustomizations(newItemId, customizationData.RequiredCustomizableOptions, customizationData.OptionalCustomizableOptions);
            }

            if (featureOnData != null)
            {
                _featureOnServices.SaveContentItemFeatureOn(newItemId, featureOnData);
            }
        }

        public void UpdateListItem(int listId, IDictionary<string, object> itemData, int userId, string regionId)
        {
            var list = _listDao.GetById(listId);
            var fieldDefinitions = list.ListFieldDefinitions.Select(o => o.FieldDefinition).ToList();

            fieldDefinitions.SetListItemModifiedTrackingFields(itemData, userId);

            UpdateContentCore(itemData, fieldDefinitions, list.TableName, regionId?.Trim());
        }

        public void UpdateContent(IDictionary<string, object> contentData, ContentActions action, int userId, string regionId, bool keepOriginalContentItemData = false, int reviewStateId = 0, IDictionary<string, object> originalContentData = null)
        {
            _connection.ExecuteInTransaction(() =>
            {
                if (action == ContentActions.Restore)
                {
                    UpdateContentItemStatus(Status.DraftOnly, contentData.GetContentItemId());
                }
                else
                {
                    UpdateContentCore(contentData, action, userId, regionId?.Trim(), keepOriginalContentItemData, reviewStateId, originalContentData);
                }
            });
        }

        private IDictionary<string, object> PopulateOriginalContentItemData(string idValue, IDictionary<string, object> sourceData, IDictionary<string, object> targetData,
                    IEnumerable<FieldDefinition> fieldDefinitions, bool isAddingContentItemBaseFields = false)
        {
            Dictionary<string, object> newData = new Dictionary<string, object>(targetData);
            _lookupServices.PopulateMultipleLookupValues(newData, fieldDefinitions, idValue);
            _tagServices.PopulateTagsValues(newData, fieldDefinitions, idValue);
            _featureOnServices.PopulateFeatureOnValues(newData, fieldDefinitions, idValue);
            _attachmentServices.PopulateAttachmentValues(newData, fieldDefinitions, idValue);
            _secondaryImageServices.PopulateSecondaryImagesValues(newData, fieldDefinitions, idValue);
            _translationService.PopulateTranslationsValues(newData, fieldDefinitions, idValue);

            if (isAddingContentItemBaseFields)
            {
                _contentItemService.PopulateContentItemBaseValues(newData, idValue);
            }

            foreach (var field in sourceData)
            {
                if (newData.ContainsKey(field.Key))
                {
                    newData[field.Key] = field.Value;
                }
                else
                {
                    newData.Add(field.Key, field.Value);
                }
            }

            return newData;
        }

        private void UpdateContentCore(IDictionary<string, object> contentData, ContentActions action, int userId, string regionId, bool keepOriginalContentItemData = false, int reviewStateId = 0, IDictionary<string, object> originalContentData = null)
        {
            contentData = CleanUri(contentData);
            var contentItemId = contentData.GetContentItemId();
            var contentTypeId = _commonService.GetContentTypeId(contentItemId);
            var contentType = _contentTypeService.GetById(contentTypeId);
            var fieldDefinitions = contentType.ContentTypeFieldDefinitions.Select(o => o.FieldDefinition);

            if (originalContentData != null)
            {
                IDictionary<string, object> extraOriginalContentData = GetContentItemData(contentItemId); //in case originalContentData is a partial set, fill in more
                extraOriginalContentData = PopulateOriginalContentItemData(contentItemId, extraOriginalContentData, originalContentData, fieldDefinitions);
                if (extraOriginalContentData != null && extraOriginalContentData.Any())
                {
                    foreach (var item in extraOriginalContentData.Where(eocd => originalContentData.Any(ocd => ocd.Key.Equals(eocd.Key, StringComparison.InvariantCultureIgnoreCase) && ocd.Value != eocd.Value)
                                                                             || !originalContentData.Any(ocd => ocd.Key.Equals(eocd.Key, StringComparison.InvariantCultureIgnoreCase))))
                    {
                        var existsInOriginalData = originalContentData.ContainsKey(item.Key) && originalContentData[item.Key] != null && (originalContentData[item.Key] is string ? (string)originalContentData[item.Key] != "" : true);

                        if (!existsInOriginalData)
                        {
                            originalContentData[item.Key] = item.Value;
                        }
                        if (originalContentData[item.Key] is string)
                        {
                            originalContentData[item.Key] = WebUtility.HtmlDecode(((string)originalContentData[item.Key]));

                        }
                    }
                }

                if (keepOriginalContentItemData)
                {
                    contentData = PopulateOriginalContentItemData(contentItemId, contentData, originalContentData, fieldDefinitions);
                }
            }

            var enumerable = fieldDefinitions as FieldDefinition[] ?? fieldDefinitions.ToArray();
            var definitions = fieldDefinitions as FieldDefinition[] ?? enumerable.ToArray();
            switch (action)
            {
                case ContentActions.SaveAsDraft:
                    contentItemId = SaveDraft(contentData, contentType, definitions, originalContentData, userId, regionId?.Trim(), reviewStateId);
                    UpdateContentItemStatus(GetDraftStatus(contentItemId), contentItemId);
                    UpdateTriggerMarketing(contentItemId, GetDraftStatus(contentItemId), contentData, definitions);
                    break;
                case ContentActions.SaveAndPreview:
                    contentItemId = SaveDraft(contentData, contentType, enumerable, originalContentData, userId, regionId?.Trim(), reviewStateId);
                    UpdateContentItemStatus(GetDraftStatus(contentItemId), contentItemId);
                    UpdateTriggerMarketing(contentItemId, GetDraftStatus(contentItemId), contentData, definitions);
                    break;
                case ContentActions.SaveCrossBorder:
                    contentItemId = SaveCrossBorder(contentData, contentType, enumerable, originalContentData, userId, regionId?.Trim());
                    break;
                default:
                    contentItemId = SaveLive(contentData, contentType, enumerable, originalContentData, userId, regionId?.Trim());
                    UpdateContentItemStatus(Status.Published, contentItemId);
                    UpdateTriggerMarketing(contentItemId, Status.Published, contentData, definitions);
                    break;
            }

            UpdateFrontEndUrl(contentData, contentType.FrontEndUrl, contentItemId);
            CallOnUpdateItem(contentType, contentData, contentItemId);
        }

        private int GetDraftStatus(string contentItemId)
        {
            if (!_commonService.HasDraft(contentItemId, false))
            {
                return Status.DraftOnly;
            }

            var currentStatus = _commonService.GetContentItemStatus(contentItemId.GetAsLiveContentItemId());

            return currentStatus == Status.Published || currentStatus == Status.PublishedAndDraft
                ? Status.PublishedAndDraft
                : Status.DraftOnly;
        }

        public void UpdateContentExpirationDate(DateTime? expirationDate, string contentItemId, bool saveIsPublish = false, bool mustUpdateReviewState = false)
        {
            if (saveIsPublish)
            {
                _connection.ExecuteScalar("UPDATE ContentItem SET ExpirationDate = @ExpirationDate  WHERE ContentItemID = @contentItemId or ContentItemID = @draftContentItemId",
                 new
                 {
                     ExpirationDate = expirationDate,
                     contentItemId = contentItemId.GetAsLiveContentItemId(),
                     draftContentItemId = contentItemId.GetAsDraftContentItemId()
                 });
            }
            else
            {
                _connection.ExecuteScalar("UPDATE ContentItem SET ExpirationDate = @ExpirationDate  WHERE ContentItemID = @contentItemId",
               new
               {
                   ExpirationDate = expirationDate,
                   contentItemId = contentItemId
               });
            }
            if (mustUpdateReviewState)
            {
                _contentItemService.UpdateReviewState(contentItemId);
            }
        }

        private void UpdateContentItemStatus(int statusId, string contentItemId)
        {
            _connection.ExecuteScalar(
                "UPDATE ContentItem SET StatusId = @statusId , ModifiedDate=GETDATE() WHERE ContentItemID = @contentItemId",
                new { statusId, contentItemId });
        }

        private string SaveLive(IDictionary<string, object> contentData, ContentType contentType, IEnumerable<FieldDefinition> fieldDefinitions, IDictionary<string, object> originalContentData, int userId, string regionId)
        {
            var contentItemId = contentData.GetContentItemId().GetAsLiveContentItemId();

            var definitions = fieldDefinitions as FieldDefinition[] ?? fieldDefinitions.ToArray();
            if (_commonService.HasDraft(contentItemId))
            {
                var draftContentItemId = contentItemId.GetAsDraftContentItemId();
                contentData.SetContentItemId(draftContentItemId);
                UpdateContentCore(contentData, definitions, contentType.TableName, regionId?.Trim(), originalContentData, userId);
                UpdateContentItemStatus(Status.PublishedAndDraft, draftContentItemId);
            }

            contentData.SetContentItemId(contentItemId);
            UpdateContentCore(contentData, definitions, contentType.TableName, regionId?.Trim(), originalContentData, userId);

            var electronicDeliveryFile = Convert.ToString(contentData.GetData("ElectronicDeliveryFile")).Trim();
            if (contentType.TableName == MarketingCenterDbConstants.Tables.OrderableAsset && !string.IsNullOrEmpty(electronicDeliveryFile))
            {
                UpdateElectronicDeliveryFileLocation(contentItemId, electronicDeliveryFile);
            }

            return contentItemId;
        }

        private string SaveCrossBorder(IDictionary<string, object> contentData, ContentType contentType, IEnumerable<FieldDefinition> fieldDefinitions, IDictionary<string, object> originalContentData, int userId, string regionId)
        {
            var contentItemId = contentData.GetContentItemId().GetAsLiveContentItemId();
            var availableFields = contentType.ContentTypeFieldDefinitions.Where(x => x.ReadOnlyInCrossBorderRegion == false || x.FieldDefinition.FieldTypeCode == FieldType.GuidPrimaryKey || x.FieldDefinition.FieldTypeCode == FieldType.IntPrimaryKey || x.FieldDefinition.FieldTypeCode == FieldType.RegionTitle).Select(x => x.FieldDefinitionId);
            var fielDefNames = fieldDefinitions.Where(x => availableFields.Contains(x.FieldDefinitionId)).Select(o => o.Name);
            var filteredData = contentData.Where(x => fielDefNames.Contains(x.Key)).ToDictionary(x => x.Key, x => x.Value);
            var filteredOriginalContentData = originalContentData.Where(x => fielDefNames.Contains(x.Key)).ToDictionary(x => x.Key, x => x.Value);
            int.TryParse(originalContentData[MarketingCenterDbConstants.ContentItemBase.ModifiedByUserID].ToString(), out int modifiedUserId);
            DateTime.TryParse(originalContentData[MarketingCenterDbConstants.ContentItemBase.ModifiedDate].ToString(), out DateTime modifiedDate);
            filteredOriginalContentData.Add(MarketingCenterDbConstants.ContentItemBase.ModifiedByUserID, modifiedUserId);
            filteredOriginalContentData.Add(MarketingCenterDbConstants.ContentItemBase.ModifiedDate, modifiedDate);
            var definitions = fieldDefinitions.Where(x => availableFields.Contains(x.FieldDefinitionId)) as FieldDefinition[] ?? fieldDefinitions.Where(x => availableFields.Contains(x.FieldDefinitionId)).ToArray();

            contentData.SetContentItemId(contentItemId);
            UpdateContentCore(filteredData, definitions, contentType.TableName, regionId?.Trim(), filteredOriginalContentData, userId);

            return contentItemId;
        }

        private string SaveDraft(IDictionary<string, object> contentData, ContentType contentType, IEnumerable<FieldDefinition> fieldDefinitions, IDictionary<string, object> originalContentData, int userId, string regionId, int reviewStateId = 0)
        {
            var contentItemId = contentData.GetContentItemId();
            if (_commonService.GetContentItemStatus(contentItemId) == Status.Published)
            {
                var draftContentItemId = contentItemId.GetAsDraftContentItemId();
                contentData.SetContentItemId(draftContentItemId);

                if (_commonService.HasDraft(contentItemId, filterDeleted: false))
                {
                    UpdateContentCore(contentData, fieldDefinitions, contentType.TableName, regionId?.Trim(), originalContentData, userId);
                }
                else
                {
                    AddContentCore(contentData, userId, contentType, Status.PublishedAndDraft, regionId?.Trim(), draftContentItemId, reviewStateId);
                }

                CallDraftCustomItemEvent(contentType, contentData, contentItemId);
                return draftContentItemId;
            }

            UpdateContentCore(contentData, fieldDefinitions, contentType.TableName, regionId?.Trim(), originalContentData, userId);

            return contentItemId;
        }

        private void CallDraftCustomItemEvent(ContentType contentType, IDictionary<string, object> contentData, string contentItemId)
        {
            var itemCustomEvent = ItemCustomEventFactory.GetItemCustomEvent(contentType, _connection);
            itemCustomEvent.OnUpdateDraftItem(contentItemId, contentData);
        }

        private void CallOnUpdateItem(ContentType contentType, IDictionary<string, object> contentData, string contentItemId)
        {
            var itemCustomEvent = ItemCustomEventFactory.GetItemCustomEvent(contentType, _connection);
            itemCustomEvent.OnUpdateItem(contentItemId, contentData);
        }

        public IDictionary<string, object> GetContentVersionForHistory(string contentItemId, string contentTypeId, IDictionary<string, object> contentData)
        {
            return PopulateOriginalContentItemData(contentItemId, GetContentItemData(contentItemId), contentData,
                _contentTypeService.GetById(contentTypeId)
                    .ContentTypeFieldDefinitions
                    .Select(o => o.FieldDefinition), true);
        }

        public IEnumerable<ContentItemVersion> GetLastContentHistoryFor(string id)
        {
            return _contentItemVersionDao.GetByContentItemId(id).OrderByDescending(x => x.ContentItemVersionId).Take(1).ToList();
        }

        public IEnumerable<ErrorItem> ValidateData(IEnumerable<FieldDefinition> fieldDefinitions, IDictionary<string, object> itemData, IDictionary<string, object> additionalItemData = null)
        {
            return new ItemDataValidator().ValidateData(fieldDefinitions, itemData, additionalItemData);
        }

        public IEnumerable<ErrorItem> ValidateExpirationDate(bool mustExpire, DateTime? expirationDate, DateTime? currentExpirationDate, bool mustBeAtLeastADayFromNow = false, bool expirationCanBeInThePast = false)
        {
            return new ItemDataValidator().ValidateExpirationDate(mustExpire, expirationDate, currentExpirationDate, mustBeAtLeastADayFromNow, expirationCanBeInThePast);
        }

        public IEnumerable<FieldDefinition> SetToggledFieldsToOptional(IEnumerable<FieldDefinition> fields, IDictionary<string, object> itemData)
        {
            var toggleFields = fields.Where(f => (f.FieldTypeCode == FieldType.Boolean || f.FieldTypeCode == FieldType.RegionInt) && !string.IsNullOrEmpty(f.CustomSettings));
            foreach (var toggleField in toggleFields)
            {
                var settings = toggleField.GetSettings<BooleanSettings>();
                if (itemData.ContainsKey(toggleField.Name) && settings.DataToggle != null && settings.DataToggleHide == (bool)itemData[toggleField.Name])
                {
                    foreach (var fieldName in settings.DataToggle)
                    {
                        var field = fields.FirstOrDefault(f => f.Name == fieldName);
                        if (field != null)
                        {
                            field.Required = false;
                        }
                    }
                }
            }

            return fields;
        }

        private void UpdateContentCore(IDictionary<string, object> data, IEnumerable<FieldDefinition> fieldDefinitions, string tableName, string regionId, IDictionary<string, object> originalData = null, int? userId = null)
        {
            var definitions = fieldDefinitions as FieldDefinition[] ?? fieldDefinitions.ToArray();
            var fieldDefs = definitions.ToList();
            var multipleLookupFields = fieldDefs.Where(o => o.FieldTypeCode == FieldType.MultipleLookup || o.FieldTypeCode == FieldType.MultipleLookupGroup).Select(o => o.Name);
            var tagFields = fieldDefs.Where(o => o.FieldTypeCode == FieldType.Tag).Select(o => o.Name);
            var featureOnFields = fieldDefs.Where(o => o.FieldTypeCode == FieldType.FeatureOn).Select(o => o.Name);
            var attachmentFields = fieldDefs.Where(o => o.FieldTypeCode == FieldType.Attachment).Select(o => o.Name);
            var translationsFields = fieldDefs.Where(o => o.FieldTypeCode == FieldType.Translations).Select(o => o.Name);
            var triggerMarketingFields = fieldDefs.Where(o => o.FieldTypeCode == FieldType.TriggerMarketing).Select(o => o.Name);
            var campaignEventsFields = fieldDefs.Where(o => o.FieldTypeCode == FieldType.CampaignEvents).Select(o => o.Name);
            var regionTitleFields = fieldDefs.Where(o => o.FieldTypeCode == FieldType.RegionTitle).Select(o => o.Name);
            var regionIntFields = fieldDefs.Where(o => o.FieldTypeCode == FieldType.RegionInt).Select(o => o.Name);
            var secondaryImages = fieldDefs.Where(o => o.FieldTypeCode == FieldType.SecondaryImage).Select(o => o.Name);
            var locationFields = fieldDefs.Where(o => o.FieldTypeCode == FieldType.Location).Select(o => o.Name);

            var itemId = data.GetItemId(definitions);
            var customizationField = GetCustomizationFieldData(data, fieldDefs);
            var featureOnItems = GetFeatureOnData(data, fieldDefs);

            var setClause = string.Join(",", data.Keys
                .Where(o => o != MarketingCenterDbConstants.FieldNames.ContentItemId)
                .Where(o => !multipleLookupFields.Contains(o)) // we need to exlude from the query the MultipleLookup fields
                .Where(o => !tagFields.Contains(o)) // also exclude Tag fields
                .Where(o => !featureOnFields.Contains(o)) // exclude FeatureOnFields
                .Where(o => !attachmentFields.Contains(o)) // exclude Attachment Fields
                .Where(o => !translationsFields.Contains(o)) // exclude Translations Fields
                .Where(o => !triggerMarketingFields.Contains(o)) // exclude TriggerMarketing Fields
                .Where(o => !campaignEventsFields.Contains(o)) // exclude CampaignEvents Fields
                .Where(o => !regionTitleFields.Contains(o)) // exclude RegionTitle Fields
                .Where(o => !regionIntFields.Contains(o)) // exclude RegionInt Fields
                .Where(o => !secondaryImages.Contains(o)) // exclude SecondaryImages Fields
                .Where(o => !locationFields.Contains(o)) // also exclude Location fields
                .Where(o =>
                        {
                            var expirationDateIsAField = fieldDefs.Any(fd => fd.Name.Equals(MarketingCenterDbConstants.FieldNames.ExpirationDate, StringComparison.InvariantCultureIgnoreCase));
                            var tableIsContentItem = tableName.Equals(MarketingCenterDbConstants.Tables.ContentItem, StringComparison.InvariantCultureIgnoreCase);
                            var fieldIsNotExpiration = (o != MarketingCenterDbConstants.FieldNames.ExpirationDate && !tableIsContentItem);
                            return (fieldIsNotExpiration || tableIsContentItem || expirationDateIsAField);
                        }
                )
                .Select(o => string.Format("{0}=@{0}", o)).ToArray());

            var idField = fieldDefs.First(o => o.FieldType.IsIdentifierField());

            var contentDataValueProvider = new ContentDataValueProvider();
            var updateData = contentDataValueProvider.GetContentData(fieldDefs, data);
            var updateParams = _connection.GetDynamicParameters(updateData);

            int originalModifiedByUserID = default(int);
            DateTime originalLastModifyDate = default(DateTime);

            if (originalData != null)
            {
                var IsOriginalModifiedByUserIDParsed = int.TryParse(originalData[MarketingCenterDbConstants.ContentItemBase.ModifiedByUserID].ToString(), out originalModifiedByUserID);
                var IsOriginalLastModifyDateParsed = DateTime.TryParse(originalData[MarketingCenterDbConstants.ContentItemBase.ModifiedDate].ToString(), out originalLastModifyDate);

                if (!IsOriginalLastModifyDateParsed || !IsOriginalLastModifyDateParsed)
                {
                    var contentItem = _contentItemService.GetContentItem(itemId);
                    originalModifiedByUserID = contentItem.ModifiedByUserId;
                    originalLastModifyDate = contentItem.ModifiedDate;
                }
            }

            _connection.ExecuteInTransaction(() =>
            {
                if (!setClause.IsNullOrEmpty())
                {
                    _connection.Execute("UPDATE " + tableName + " SET " + setClause +
                                    " WHERE " + idField.Name + " = @" + idField.Name, updateParams);
                }

                _lookupServices.SetMultipleLookupValues(data, fieldDefs, itemId, regionId);
                SetTranslations(tableName, itemId, idField.FieldDefinitionId, data, fieldDefs, regionId);
                if (fieldDefinitions.Any(fd => fd.CustomSettings != null && fd.CustomSettings.Contains("CleanUp")))
                {
                    _lookupServices.CleanUpMultipleLookupValues(data, originalData, fieldDefinitions.Where(fd => fd.CustomSettings != null && fd.CustomSettings.Contains("CleanUp")), itemId);
                }

                if (customizationField != null)
                {
                    _customizationServices.SaveContentItemCustomizations(itemId, customizationField.RequiredCustomizableOptions, customizationField.OptionalCustomizableOptions);
                }

                if (data.IsContentItem())
                // just update tags and add version info when we are updating Content Items and not List Items
                {
                    _tagServices.SetTags(itemId, data);
                    SetTrackingFields(itemId, userId.GetValueOrDefault());
                    _attachmentServices.SetAttachments(itemId, data, definitions);
                    _secondaryImageServices.SetSecondaryImages(itemId, data, definitions);
                    _eventLocationService.SetEventLocations(itemId, data, definitions, _connection);
                    SetRegionIntFields(itemId, regionId, data, definitions);

                    if (featureOnItems != null)
                    {
                        _featureOnServices.SaveContentItemFeatureOn(itemId, featureOnItems);
                    }

                    if (originalData != null)
                    {
                        CreateNewItemVersion(originalData, itemId, fieldDefinitions, originalModifiedByUserID, originalLastModifyDate);
                    }
                }
            });
        }

        private void SetTrackingFields(string id, int userId)
        {
            _connection.Execute("UPDATE ContentItem SET ModifiedByUserID = @userId, ModifiedDate = @date WHERE ContentItemID = @id",
                                new { userId, date = DateTime.Now, id });
        }

        private void UpdateFrontEndUrl(IDictionary<string, object> data, string frontEndUrl, string contentItemId)
        {
            _connection.Execute("UPDATE ContentItem SET FrontEndUrl = @frontEndUrl WHERE ContentItemId = @contentItemId or ContentItemId = @draftContentItemId",
                                new { frontEndUrl = _urlService.SetFrontEndUrl(data, frontEndUrl, contentItemId), contentItemId = contentItemId.GetAsLiveContentItemId(), draftContentItemId = contentItemId.GetAsDraftContentItemId() });
        }

        private void UpdateElectronicDeliveryFileLocation(string contentItemId, string fileLocation)
        {
            _connection.ExecuteScalar("UPDATE ElectronicDelivery SET FileLocation = @fileLocation " +
                                      "FROM ElectronicDelivery ed " +
                                      "JOIN OrderItem oi ON oi.OrderItemID = ed.OrderItemID " +
                                      "WHERE oi.ItemID = @contentItemId",
                                      new { contentItemId, fileLocation });
        }

        private static void SetVanityUrl(string itemId, IDictionary<string, object> data, IEnumerable<FieldDefinition> fieldDefinitions)
        {
            var vanityUrlFields = fieldDefinitions.Where(o => o.FieldTypeCode == FieldType.VanityUrl);
            foreach (var vanityUrlField in vanityUrlFields)
            {
                string parsedUrl;
                if (data.ContainsKey(vanityUrlField.Name))
                {
                    var url = Convert.ToString(data[vanityUrlField.Name]);
                    parsedUrl = url.Replace("{id}", itemId);
                }
                else // falback logic when the field for the url is empty
                {
                    if (string.IsNullOrEmpty(vanityUrlField.CustomSettings))
                    {
                        throw new InvalidOperationException("No CustomSettings defined for the VanityUrl field: " + vanityUrlField.Name);
                    }

                    var settings = vanityUrlField.GetSettings<VanityUrlSettings>();
                    string titleData;

                    // if the data object contains a Title field we use that for building the url
                    if (data.ContainsKey(MarketingCenterDbConstants.FieldNames.Title) && !string.IsNullOrEmpty(Convert.ToString(data[MarketingCenterDbConstants.FieldNames.Title])))
                    {
                        titleData = Convert.ToString(data[MarketingCenterDbConstants.FieldNames.Title]);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(settings.DefaultTitleField) &&
                            data.ContainsKey(settings.DefaultTitleField)
                        ) // if not we use the field specified in the settings
                        {
                            titleData = Convert.ToString(data[settings.DefaultTitleField]);
                        }
                        else
                        {
                            throw new InvalidOperationException("The field used for DefaultTitle is invalid: " + settings.DefaultTitleField);
                        }
                    }

                    parsedUrl = $"/{settings.UrlPath.Trim('/').Replace("{id}", itemId)}/{titleData}";
                }

                data[vanityUrlField.Name] = UrlService.GetPartialUrl(parsedUrl).stringCleanPartialUrl();
            }
        }

        private void SetTranslations(string parentTableName, string itemId, int idFieldDefinitionId, IDictionary<string, object> data, IEnumerable<FieldDefinition> fieldDefinitions, string regionId)
        {
            var definitions = fieldDefinitions as FieldDefinition[] ?? fieldDefinitions.ToArray();
            foreach (var translationsField in definitions.Where(o => o.FieldTypeCode == FieldType.Translations))
            {
                var translationsSettings = translationsField.GetSettings<TranslationsSettings>();

                var idField = definitions.FirstOrDefault(f => f.FieldDefinitionId == idFieldDefinitionId);
                var subFields = new List<FieldDefinition>(new[] { idField });
                subFields.AddRange(_fieldDefinitionDao.GetAll().Where(f => translationsSettings.FieldDefinitionIds.Contains(f.FieldDefinitionId)).ToList());

                var translationData = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, object>>>(HttpUtility.HtmlDecode(data[translationsField.Name].ToString()));

                _connection.Execute($"delete from {translationsSettings.TranslatedContentTableName} where {idField.Name} = '{itemId}'");
                foreach (KeyValuePair<string, Dictionary<string, object>> item in translationData)
                {
                    item.Value.Add(idField.Name, itemId);
                    InsertItemContent(item.Value, subFields, translationsSettings.TranslatedContentTableName, itemId, regionId);
                }

                if (!string.IsNullOrEmpty(translationsSettings.ListViewFieldName) && translationData.Count > 0 && translationData.First().Value.Any(f => f.Key == translationsSettings.ListViewFieldName))
                {
                    _connection.Execute($@"update {parentTableName} 
                        set {translationsSettings.ListViewFieldName} = '{translationData.First().Value.First(d => d.Key == translationsSettings.ListViewFieldName).Value}'
                        where {idField.Name} = '{itemId}'");
                }
            }
        }

        public static ContentItemTriggerMarketing GetTriggerMarketingValues(string triggerMarketingValue)
        {
            string[] stringSeparators = { ";#" };
            var triggerMarketing = triggerMarketingValue.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries).Where(tm => tm != ",");
            var enumerable = triggerMarketing as string[] ?? triggerMarketing.ToArray();

            var triggerMarketingInfo = new ContentItemTriggerMarketing
            {
                IsNew = enumerable.Contains(Constants.IsNewValue),
                Notify = enumerable.Contains(Constants.TriggerMarketingNotificationsValue)
            };
            triggerMarketingInfo.Featured = triggerMarketingInfo.Notify && enumerable.Contains(Constants.ShowAsFeaturedLinkValue);
            triggerMarketingInfo.Reasons = triggerMarketingInfo.Notify ? (enumerable.FirstOrDefault(tm => tm != Constants.TriggerMarketingNotificationsValue && tm != Constants.ShowAsFeaturedLinkValue && tm != Constants.IsNewValue) == null ? string.Empty : enumerable.FirstOrDefault(tm => tm != Constants.TriggerMarketingNotificationsValue && tm != Constants.ShowAsFeaturedLinkValue && tm != Constants.IsNewValue)) : string.Empty;

            return triggerMarketingInfo;
        }

        private void UpdateTriggerMarketing(string contentItemId, int statusId, IDictionary<string, object> data, IEnumerable<FieldDefinition> fieldDefinitions)
        {
            var triggerMarketingFieldDef = fieldDefinitions.FirstOrDefault(o => o.FieldTypeCode == FieldType.TriggerMarketing);

            if (triggerMarketingFieldDef != null)
            {
                var triggerMarketingValue = data.GetData(triggerMarketingFieldDef.Name)?.ToString();
                if (triggerMarketingValue == null)
                {
                    return;
                }

                var triggerMarketingInfo = GetTriggerMarketingValues(triggerMarketingValue);

                triggerMarketingInfo.ContentItemId = contentItemId;
                triggerMarketingInfo.StatusId = statusId;

                _triggerMarketingServices.SaveTriggerMarketing(triggerMarketingInfo);
            }
        }

        private static IEnumerable<FeatureTagItem> GetFeatureOnData(IDictionary<string, object> data, IEnumerable<FieldDefinition> fieldDefinitions)
        {
            var featureOnField = fieldDefinitions.FirstOrDefault(o => o.FieldTypeCode == FieldType.FeatureOn);
            if (featureOnField != null && data.ContainsKey(featureOnField.Name))
            {
                var tagFeatureItems = data[featureOnField.Name] as IEnumerable<FeatureTagItem>;
                if (tagFeatureItems != null)
                {
                    return tagFeatureItems;
                }
            }
            else
            {
                return null;
            }

            return Enumerable.Empty<FeatureTagItem>();
        }

        private static CustomizationField GetCustomizationFieldData(IDictionary<string, object> data, IEnumerable<FieldDefinition> fieldDefinitions)
        {
            var customizationField = fieldDefinitions.FirstOrDefault(o => o.FieldTypeCode == FieldType.Customization);

            if (customizationField != null)
            {
                return data[customizationField.Name] as CustomizationField;
            }

            return null;
        }

        public IDictionary<string, object> GetDefaultValueData(IDictionary<string, object> data, IEnumerable<FieldDefinition> fieldDefinitions, string regionId, bool isNewItem = false, IEnumerable<string> preLoadedDefaultValueNames = null, bool isClone = false)
        {
            if (fieldDefinitions.Any(fd => fd.DefaultValue != null))
            {
                var defaultValuedFields = fieldDefinitions.Where(fd => fd.DefaultValue != null || fd.FieldTypeCode == FieldType.UserLookup);
                if (preLoadedDefaultValueNames != null && preLoadedDefaultValueNames.Count() > 0)
                {
                    defaultValuedFields = defaultValuedFields.Where(fd => !preLoadedDefaultValueNames.Contains(fd.Name)); //pre loaded default values are ignored to avoid replacing the by the default
                }

                foreach (var fieldDefinition in defaultValuedFields)
                {
                    string defaultValue = GetFieldDefinitionDefaultValue(
                        fieldDefinition,
                        regionId,
                        fieldDefinition.FieldTypeCode != FieldType.MultipleLookup && fieldDefinition.FieldTypeCode != FieldType.MultipleLookupGroup);

                    // omit setting a default value:
                    // if this is existing item and user provided some value
                    object v;
                    var valueProvided = data.TryGetValue(fieldDefinition.Name, out v);
                    var providedValue = v?.ToString();
                    if (!isNewItem && valueProvided && !string.IsNullOrEmpty(providedValue)
                        || (fieldDefinition.FieldTypeCode == FieldType.MultipleLookup || fieldDefinition.FieldTypeCode == FieldType.MultipleLookupGroup)
                        && defaultValue == null || (isClone && fieldDefinition.Name.Equals(MarketingCenterDbConstants.FieldNames.BusinessOwnerId))) // for multipleLookup no regional value defined
                    {
                        continue;
                    }

                    if (string.IsNullOrEmpty(providedValue) || isNewItem)
                    {
                        data.Remove(fieldDefinition.Name);
                    }

                    data.Add(fieldDefinition.Name, defaultValue);
                }
            }

            return data;
        }

        public string GetFieldDefinitionDefaultValue(FieldDefinition fieldDefinition, string regionId, bool includeOwnDefaultValue = true)
        {
            var defaultValue = includeOwnDefaultValue ? fieldDefinition.DefaultValue : null;
            if (!string.IsNullOrEmpty(fieldDefinition.CustomSettings) &&
                fieldDefinition.CustomSettings.Contains("RegionDefaultValues"))
            {
                var settings = fieldDefinition.GetSettings<RegionDefaultValueSettings>();
                if (settings != null && settings.RegionDefaultValues?.Length > 0)
                {
                    defaultValue = settings.RegionDefaultValues.FirstOrDefault(rdv => regionId.Trim().Equals(rdv.Region, StringComparison.InvariantCultureIgnoreCase))?.DefaultValue ?? fieldDefinition.DefaultValue;
                }
            }

            return defaultValue;
        }

        public string GetFieldDefinitionDefaultValue(string fieldDefinitionName, string regionId)
        {
            var fieldDefinition = _fieldDefinitionDao.GetByName(fieldDefinitionName);
            return fieldDefinition != null ? GetFieldDefinitionDefaultValue(fieldDefinition, regionId) : string.Empty;
        }

        private void CreateNewItemVersion(IDictionary<string, object> originalData, string itemId, IEnumerable<FieldDefinition> fieldDefinitions, int lastModifiedUserId, DateTime modifiedDate)
        {
            var serializer = new JsonSerializationWrapper();

            _contentItemVersionDao.Add(new ContentItemVersion
            {
                ContentItemId = itemId,
                DateSaved = DateTime.Now,
                SerializationData = serializer.Serialize(RetrieveDataValues(originalData, fieldDefinitions)),
                LastModificationByUserId = lastModifiedUserId,
                LastModifyDate = modifiedDate
            });
        }

        public IDictionary<string, object> RetrieveDataValues(IDictionary<string, object> originalData, IEnumerable<FieldDefinition> fieldDefinition)
        {
            return _fieldDataNormalizationContext.Apply(originalData, fieldDefinition);
        }

        public dynamic ExecuteSelect(object itemId, List<string> sqlColumns, string tableName, FieldDefinition idColumn)
        {
            return _connection.Query($"SELECT {string.Join(",", sqlColumns.ToArray())} FROM {tableName} WHERE {idColumn.Name} = @itemId", new { itemId }).FirstOrDefault();
        }

        private dynamic ExecuteSelect(List<string> sqlColumns, string tableName, Dictionary<FieldDefinition, string> columns, bool ignoreWhiteSpaces = false, bool ignoreNullOrEmpty = false)
        {
            string where = string.Empty;
            foreach (var column in columns)
            {
                if (!string.IsNullOrEmpty(where))
                {
                    where += " AND ";
                }

                where += ignoreWhiteSpaces ? $"REPLACE({column.Key.Name}, ' ', '') = REPLACE('{column.Value}', ' ', '')" : $"{column.Key.Name} = '{column.Value.TrimStart()}'";
                where += ignoreNullOrEmpty ? $"AND REPLACE({column.Key.Name}, ' ', '') <> ''" : string.Empty;
            }

            return _connection.Query($"SELECT {string.Join(",", sqlColumns.ToArray())} FROM {tableName} WHERE {where}").FirstOrDefault();
        }

        private dynamic ExecuteSelectInRange(List<string> sqlColumns, string tableName, KeyValuePair<FieldDefinition, DateTime> startDateColumn, KeyValuePair<FieldDefinition, DateTime> endDateColumn, Dictionary<FieldDefinition, string> columns)
        {
            string where = $"(({startDateColumn.Key.Name} >= '{startDateColumn.Value.ToString("yyyy-MM-dd HH:mm:ss")}' AND {endDateColumn.Key.Name} <= '{endDateColumn.Value.ToString("yyyy-MM-dd HH:mm:ss")}') or ({startDateColumn.Key.Name} <= '{startDateColumn.Value.ToString("yyyy-MM-dd HH:mm:ss")}' AND {endDateColumn.Key.Name} >= '{startDateColumn.Value.ToString("yyyy-MM-dd HH:mm:ss")}') or ({startDateColumn.Key.Name} <= '{endDateColumn.Value.ToString("yyyy-MM-dd HH:mm:ss")}' AND {endDateColumn.Key.Name} >= '{endDateColumn.Value.ToString("yyyy-MM-dd HH:mm:ss")}'))";
            foreach (var column in columns)
            {
                if (!string.IsNullOrEmpty(where))
                {
                    where += " AND ";
                }

                where += $"{column.Key.Name} = '{column.Value.TrimStart()}'";
            }

            return _connection.Query($"SELECT {string.Join(",", sqlColumns.ToArray())} FROM {tableName} WHERE {where}").FirstOrDefault();
        }

        public static IDictionary<string, object> CleanUri(IDictionary<string, object> data)
        {
            if (data.ContainsKey("Uri"))
            {
                data["Uri"] = UrlService.GetPartialUrl(data["Uri"].ToString().stringCleanPartialUrl());
            }

            return data;
        }

        private PagedList<dynamic> GetSelectResult(string tablename, IEnumerable<FieldDefinition> fieldDefinitions, int startIndex, int length,
         string searchValue, IEnumerable<FieldDefinition> searcheableFields, IDictionary<string, bool> sortColumns, bool contentTypeQuery,
         string regionId, int? statusId = null, string extraWhereClause = null, bool? isExpired = false,
         bool hasCrossBorderRegions = false, IEnumerable<FieldDefinition> regionTitleFields = null, IEnumerable<FieldDefinition> regionIntFields = null, bool showPendings = false)
        {
            var definitions = fieldDefinitions as FieldDefinition[] ?? fieldDefinitions.ToArray();
            var idFieldDefinition = definitions.First(o => o.FieldType.IsIdentifierField());
            var endIndex = startIndex + length;
            var sqlColumns = definitions.Select(o => o.Name).ToList();

            if (!contentTypeQuery && definitions.All(d => d.FieldType.FieldTypeCode != FieldType.Region))
            {
                regionId = null;
            }

            ExpandTranslationsField(definitions, sqlColumns, searcheableFields);

            var whereClause = GetWhereClause(contentTypeQuery, tablename, searchValue, searcheableFields, regionId?.Trim(), statusId, isExpired, hasCrossBorderRegions, showPendings);

            if (!string.IsNullOrEmpty(extraWhereClause))
            {
                if (string.IsNullOrEmpty(whereClause))
                {
                    whereClause = "WHERE ";
                }
                else
                {
                    whereClause += "AND ";
                }

                whereClause += $"({extraWhereClause})";
            }

            var orderByClause = $"{tablename}.{idFieldDefinition.Name}";
            if (sortColumns != null && sortColumns.Any())
            {
                var order = sortColumns.Select(sortColumn => MapSortColumn(tablename, sortColumn, contentTypeQuery)).ToList();
                orderByClause = string.Join(",", order);
            }

            var query = GetSelectQuery(tablename, startIndex, contentTypeQuery, whereClause, sqlColumns, orderByClause, endIndex, regionId, hasCrossBorderRegions, regionTitleFields, regionIntFields, showPendings);

            int count;
            IEnumerable<dynamic> items;

            using (var multi = _connection.QueryMultiple(query, new { searchValue = $"%{searchValue}%" }))
            {
                count = multi.Read<int>().Single();
                items = multi.Read<dynamic>();
            }

            return new PagedList<dynamic>(items, count);
        }

        private static string MapSortColumn(string tablename, KeyValuePair<string, bool> sortColumn, bool isContentTypeQuery)
        {
            var key = sortColumn.Key;
            var orderTable = tablename;

            switch (key.ToLowerInvariant())
            {
                case "statusname":
                    key = "Name";
                    orderTable = "Status";
                    break;
                case "createddate":
                    key = "CreatedDate";
                    orderTable = "ContentItem";
                    break;
                case "modifieddate":
                    key = "ModifiedDate";
                    orderTable = "ContentItem";
                    break;
                case "expirationdate":
                    key = "ExpirationDate";
                    orderTable = "ContentItem";
                    break;
                case "sourceregion":
                    key = "RegionId";
                    orderTable = "ContentItem";
                    break;
                case "contentitemregionvisibility":
                    key = "Value";
                    orderTable = "ContentItemRegionVisibility";
                    break;
                case "regionapplicability":
                    key = "CrossBorderCount";
                    orderTable = "RegionApplicability";
                    break;
            }

            if (isContentTypeQuery)
            {
                return $"{orderTable}.{key} {(sortColumn.Value ? string.Empty : "desc")}";
            }

            return $"{key} {(sortColumn.Value ? string.Empty : "desc")}";
        }

        private static string GetWhereClause(bool contentTypeQuery, string tableName, string searchValue, IEnumerable<FieldDefinition> searcheableFields,
            string regionId, int? statusId, bool? isExpired, bool hasCrossBorderRegions,
            bool showPendings)
        {
            var whereClause = string.Empty;

            var fieldDefinitions = searcheableFields as FieldDefinition[] ?? searcheableFields.ToArray();
            if (!string.IsNullOrEmpty(searchValue) && fieldDefinitions.Any())
            {
                var whereFilter = fieldDefinitions.Select(searcheableField =>
                                                          $"{(searcheableField.FieldTypeCode == FieldType.GuidPrimaryKey ? $"{tableName}.{searcheableField.Name}" : searcheableField.Name)} LIKE @searchValue")
                                                  .ToList();
                whereClause = $" WHERE ({string.Join(" OR ", whereFilter)})";
            }

            if (contentTypeQuery)
            {
                if (string.IsNullOrEmpty(whereClause))
                {
                    whereClause += " WHERE ";
                }
                else
                {
                    whereClause += " AND ";
                }

                if (statusId.HasValue && statusId > 0)
                {
                    whereClause += $"(ContentItem.StatusId = {statusId})";
                }
                else
                {
                    whereClause += $"(ContentItem.StatusId != {Status.Archived} AND ContentItem.StatusId != {Status.Deleted})";
                }

                if (!string.IsNullOrEmpty(regionId))
                {
                    whereClause += $@" AND (ContentItem.RegionId = '{regionId.Trim()}'
                                            {(hasCrossBorderRegions && statusId != Status.Archived && (!isExpired.HasValue || !isExpired.Value) ?
                                            $" OR {tableName}.ContentItemId = {MarketingCenterDbConstants.Tables.ContentItemCrossBorderRegion}.ContentItemId" :
                                            string.Empty)}
                                           )";
                }
                if (isExpired != null)
                {
                    if ((bool)isExpired)
                    {
                        whereClause += " AND (ContentItem.ExpirationDate<getdate())";
                    }
                    else
                    {
                        whereClause += " AND (ContentItem.ExpirationDate>getdate() or ContentItem.ExpirationDate is null )";
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(regionId))
                {
                    whereClause += (whereClause.Contains(" WHERE ") ? " AND " : " WHERE ") + $" RegionId = '{regionId}' ";
                }
                if (isExpired != null)
                {
                    if ((bool)isExpired)
                    {
                        whereClause += (whereClause.Contains(" WHERE ") ? " AND " : " WHERE ") + $"({tableName}.ExpirationDate < getdate())";
                    }
                    else
                    {
                        whereClause += (whereClause.Contains(" WHERE ") ? " AND " : " WHERE ") + $"({tableName}.ExpirationDate > getdate() or {tableName}.ExpirationDate is null)";
                    }
                }
            }

            return whereClause;
        }

        private static string GetSelectQuery(string tableName, int startIndex, bool contentTypeQuery, string whereClause, List<string> sqlColumns, string orderByClause, int endIndex, string regionId, bool hasCrossBorderRegions, IEnumerable<FieldDefinition> regionTitleFields, IEnumerable<FieldDefinition> regionIntFields, bool showPendings)
        {
            if (!hasCrossBorderRegions)
            {
                sqlColumns.Add("1 AS CanArchive");
                sqlColumns.Add("1 AS CanDelete");
                sqlColumns.Add("1 AS CanDeleteOnlyDraft");
                sqlColumns.Add("1 AS CanRestore");
            }

            var sb = new StringBuilder();
            // pagination and order by query
            if (contentTypeQuery)
            {
                sqlColumns.Add("StatusName");
                sqlColumns.Add("StatusId");
                sqlColumns.Add("FrontEndUrl");
                sqlColumns.Add("CASE WHEN result.ContentItemId LIKE '%-p' THEN (SELECT CreatedDate FROM ContentItem WHERE ContentItemId = SUBSTRING(result.ContentItemId, 1, LEN(result.ContentItemId) - 2)) ELSE CreatedDate END AS CreatedDate");
                sqlColumns.Add("CASE WHEN result.ContentItemId LIKE '%-p' THEN (SELECT ExpirationDate FROM ContentItem WHERE ContentItemId = SUBSTRING(result.ContentItemId, 1, LEN(result.ContentItemId) - 2)) ELSE ExpirationDate END AS ExpirationDate");
                sqlColumns.Add("ModifiedDate");

                string regionTitleFieldsSelectClause = string.Empty;
                string regionIntFieldsSelectClause = string.Empty;
                string regionIntFieldsJoinClause = string.Empty;
                string regionApplicabilityJoinClause = string.Empty;
                if (hasCrossBorderRegions)
                {
                    sqlColumns.Add($"CASE WHEN result.RegionApplicability > 0 THEN 'Cross-Border' ELSE 'Local' END AS RegionApplicability");
                    sqlColumns.Add($"CASE WHEN result.RegionId = '{regionId}' THEN 1 ELSE 0 END AS CanArchive");
                    sqlColumns.Add($"CASE WHEN result.RegionId = '{regionId}' THEN 1 ELSE 0 END AS CanDelete");
                    sqlColumns.Add($"CASE WHEN result.RegionId = '{regionId}' THEN 1 ELSE 0 END AS CanDeleteOnlyDraft");
                    sqlColumns.Add($"CASE WHEN result.RegionId = '{regionId}' THEN 1 ELSE 0 END AS CanRestore");

                    foreach (var titleField in regionTitleFields.Where(rbf => !rbf.CustomSettings.IsNullOrEmpty()))
                    {
                        var regionTitleSettings = titleField.GetSettings<RegionTitleSettings>();
                        regionTitleFieldsSelectClause += $", (SELECT r.[Name] FROM [Region] r JOIN [{regionTitleSettings.TableName}] x ON r.[Id] = x.[{regionTitleSettings.TableRegionField}] WHERE x.[ContentItemId] = ContentItem.ContentItemID) AS '{titleField.Name}'";
                    }

                    foreach (var intField in regionIntFields.Where(rbf => !rbf.CustomSettings.IsNullOrEmpty()))
                    {
                        var regionIntSettings = intField.GetSettings<RegionIntSettings>();
                        regionIntFieldsSelectClause += $", COALESCE({regionIntSettings.TableName}.[Value], 0) AS '{intField.Name}'";
                        regionIntFieldsJoinClause += $" LEFT JOIN {regionIntSettings.TableName} ON ContentItem.ContentItemID = {regionIntSettings.TableName}.ContentItemId AND {regionIntSettings.TableName}.RegionId = '{regionId}'";
                    }

                    regionIntFieldsSelectClause += $", RegionApplicability.CrossBorderCount AS 'RegionApplicability'";
                    regionApplicabilityJoinClause += $" LEFT JOIN (SELECT ContentItemId,COUNT(ContentItemId) CrossBorderCount FROM {MarketingCenterDbConstants.Tables.ContentItemCrossBorderRegion} GROUP BY ContentItemId) RegionApplicability ON {tableName}.ContentItemID = RegionApplicability.ContentItemId";
                    regionApplicabilityJoinClause += $" LEFT JOIN {MarketingCenterDbConstants.Tables.ContentItemCrossBorderRegion} ON ContentItem.ContentItemID = {MarketingCenterDbConstants.Tables.ContentItemCrossBorderRegion}.ContentItemId AND {MarketingCenterDbConstants.Tables.ContentItemCrossBorderRegion}.RegionId = '{regionId}'";
                }

                var draftItemsFilter = $@" {tableName}.ContentItemId in (select ContentItemId
                                    from ContentItem
                                    EXCEPT
                                    select PrimaryContentItemID
                                    from ContentItem
                                    where ContentItemId like '%-p' and StatusId != {Status.Deleted})";

                if (string.IsNullOrEmpty(whereClause))
                {
                    whereClause = " WHERE " + draftItemsFilter;
                }
                else
                {
                    whereClause += " AND " + draftItemsFilter;
                    whereClause += showPendings ? $" AND {MarketingCenterDbConstants.Tables.ContentItemRegionVisibility}.[Value] = {MarketingCenterDbConstants.ContentItemRegionVisibilityValues.Pending}" : string.Empty;
                }

                sb.AppendLine(string.Format(@"SELECT COUNT(*)
                                        FROM {0}
                                        JOIN ContentItem on ContentItem.ContentItemId = {0}.ContentItemId
                                        {3}
                                        {2}
                                        {1}", tableName, whereClause, regionApplicabilityJoinClause, regionIntFieldsJoinClause));

                sb.AppendLine(string.Format(@"SELECT  {0}
                                            FROM    ( SELECT    ROW_NUMBER() OVER (Order By {1}) AS RowNum, {2}.*, Status.Name as StatusName, Status.StatusId, ContentItem.FrontEndUrl, ContentItem.CreatedDate, ContentItem.ModifiedDate, ContentItem.ExpirationDate, ContentItem.RegionId
                                                      {6}
                                                      {7}
                                                      FROM      {2}
                                                      JOIN ContentItem on ContentItem.ContentItemID = {2}.ContentItemId 
                                                      JOIN Status on ContentItem.StatusID = Status.StatusID
                                                      {8}
                                                      {9}
                                                      {3}
                                                    ) AS result
                                            WHERE   RowNum > {4} 
                                                AND RowNum <= {5}
                                            ORDER BY RowNum",
                                            string.Join(",", sqlColumns),
                                            orderByClause,
                                            tableName,
                                            whereClause,
                                            startIndex,
                                            endIndex,
                                            regionTitleFieldsSelectClause,
                                            regionIntFieldsSelectClause,
                                            regionIntFieldsJoinClause,
                                            regionApplicabilityJoinClause
                                            ));
            }
            else
            {
                sb.AppendLine($@"SELECT COUNT(*)
                                        FROM {tableName}
                                        {whereClause}");

                sb.AppendLine($@"SELECT  {string.Join(",", sqlColumns)}
                                    FROM    ( SELECT    ROW_NUMBER() OVER (Order By {orderByClause}) AS RowNum, *
                                              FROM      {tableName} {whereClause}
                                            ) AS result
                                    WHERE   RowNum > {startIndex} 
                                        AND RowNum <= {endIndex}
                                    ORDER BY RowNum");
            }

            return sb.ToString();
        }

        private static void ExpandTranslationsField(IEnumerable<FieldDefinition> fieldDefinitions, List<string> sqlColumns, IEnumerable<FieldDefinition> searcheableFields)
        {
            var translationsField = fieldDefinitions.FirstOrDefault(d => d.FieldTypeCode == FieldType.Translations);
            if (translationsField != null)
            {
                sqlColumns.Remove(translationsField.Name);
                if (!string.IsNullOrEmpty(translationsField.CustomSettings))
                {
                    var idFieldDefinition = fieldDefinitions.First(o => o.FieldType.IsIdentifierField());
                    var translationsSettings = translationsField.GetSettings<TranslationsSettings>();
                    if (translationsSettings != null && !string.IsNullOrEmpty(translationsSettings.ListViewFieldName))
                    {
                        sqlColumns.Add(string.Format("{0} as {1}", translationsSettings.ListViewFieldName, translationsField.Name));
                    }

                    var searchableTranslationsField = searcheableFields.FirstOrDefault(f => f.Name == translationsField.Name);
                    if (searchableTranslationsField != null)
                    {
                        searchableTranslationsField.Name = translationsSettings.ListViewFieldName;
                    }
                }
            }
        }

        private static void ExpandTranslationsField(IEnumerable<FieldDefinition> fieldDefinitions, List<string> sqlColumns, Dictionary<FieldDefinition, string> columns)
        {
            var translationsField = fieldDefinitions.FirstOrDefault(d => d.FieldTypeCode == FieldType.Translations);
            if (translationsField != null && translationsField.CustomSettings != null)
            {
                var columnField = columns.FirstOrDefault(f => f.Key.Name == translationsField.Name);
                if (columnField.Value != null)
                {
                    ExpandTranslationsField(fieldDefinitions, sqlColumns, columns.Keys);
                    var translationsSettings = translationsField.GetSettings<TranslationsSettings>();
                    if (translationsSettings != null && !string.IsNullOrEmpty(translationsSettings.ListViewFieldName))
                    {
                        var translationData = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, object>>>(HttpUtility.HtmlDecode(columnField.Value));
                        if (translationData.Values.Count > 0)
                        {
                            columns[columnField.Key] = translationData.FirstOrDefault().Value?.FirstOrDefault(d => d.Key == translationsSettings.ListViewFieldName).Value?.ToString() ?? string.Empty;
                        }
                    }
                }
            }
        }

        private IDictionary<string, object> GetItemData(object itemId, string tableName, string title, IEnumerable<FieldDefinition> fieldDefinitions)
        {
            var contentTypeFieldDefs = fieldDefinitions
                .ExcludeTranslations()
                .GetSelectClauseColumns()
                .ToList();

            var sqlColumns = contentTypeFieldDefs.Select(o => o.Name).Distinct().ToList();

            var idColumn = contentTypeFieldDefs.FirstOrDefault(o => o.FieldType.IsIdentifierField());

            if (idColumn == null)
                throw new InvalidOperationException("No ID Field defined for Content Type:" + title);

            var contentItem = ExecuteSelect(itemId, sqlColumns, tableName, idColumn);

            if (contentItem != null)
            {
                foreach (var val in (IDictionary<string, object>)contentItem)
                {
                    if (val.Value != null)
                    {
                        if (contentTypeFieldDefs.Any(ctf => ctf.Name == val.Key && ctf.FieldType.FieldTypeCode.ToLower().Trim() == "number"))
                        {
                            ((IDictionary<string, object>)contentItem)[val.Key] = val.Value.ToString().Replace(",", ".");
                        }
                    }
                }
            }

            if (contentItem != null)
            {
                return (IDictionary<string, object>)contentItem;
            }

            return null;
        }

        public bool ExistItemData(string tableName, IEnumerable<FieldDefinition> fieldDefinitions, Dictionary<FieldDefinition, string> columns, KeyValuePair<string, object> idField, bool ignoreWhitespaces, bool ignoreNullOrEmpty)
        {
            var contentTypeFieldDefs = fieldDefinitions
                .ExcludeTranslations()
                .GetSelectClauseColumns()
                .ToList();

            var sqlColumns = contentTypeFieldDefs.Select(o => o.Name).ToList();
            ExpandTranslationsField(fieldDefinitions, sqlColumns, columns);
            var contentItem = ExecuteSelect(sqlColumns, tableName, columns, ignoreWhitespaces, ignoreNullOrEmpty);

            return idField.Equals(default(KeyValuePair<string, object>)) ?
                   contentItem != null :
                   contentItem != null && ((IDictionary<string, object>)contentItem)[idField.Key].ToString() != idField.Value.ToString();
        }

        public bool ExistItemInDateRange(List list, KeyValuePair<string, DateTime> startDate, KeyValuePair<string, DateTime> endDate, Dictionary<FieldDefinition, string> columns, KeyValuePair<string, object> idField)
        {
            var contentTypeFieldDefs = list.ListFieldDefinitions
                .Select(l => l.FieldDefinition)
                .ExcludeTranslations()
                .GetSelectClauseColumns()
                .ToList();

            var sqlColumns = contentTypeFieldDefs.Select(o => o.Name).ToList();
            dynamic contentItem = default(dynamic); // Do not simplify to 'dynamic contentItem = default;'. Team City breaks because doesn't have the latest C# version.

            var startDateDefinition = contentTypeFieldDefs.FirstOrDefault(o => o.Name.Equals(startDate.Key));
            var endDateDefinition = contentTypeFieldDefs.FirstOrDefault(o => o.Name.Equals(endDate.Key));
            if (startDateDefinition != null && endDateDefinition != null)
            {
                var startDateColumn = new KeyValuePair<FieldDefinition, DateTime>(startDateDefinition, startDate.Value);
                var endDateColumn = new KeyValuePair<FieldDefinition, DateTime>(endDateDefinition, endDate.Value);
                contentItem = ExecuteSelectInRange(sqlColumns, list.TableName, startDateColumn, endDateColumn, columns);
            }
            else
            {
                contentItem = ExecuteSelect(sqlColumns, list.TableName, columns);
            }

            return idField.Equals(default(KeyValuePair<string, object>)) ?
                   contentItem != null :
                   contentItem != null && ((IDictionary<string, object>)contentItem)[idField.Key].ToString() != idField.Value.ToString();
        }

        public void DeleteListItem(int listId, string listItemId)
        {
            var list = _listDao.GetById(listId);
            var idField = list.ListFieldDefinitions.Select(o => o.FieldDefinition).First(o => o.FieldType.IsIdentifierField());

            var sql = $"DELETE {list.TableName} WHERE {idField.Name} = @listItemId";

            _connection.Execute(sql, new { listItemId });
        }

        public string GetValidContentItemId(string itemId)
        {
            itemId = itemId.GetAsLiveContentItemId();
            if (_commonService.HasDraft(itemId))
            {
                return itemId.GetAsDraftContentItemId();
            }

            return itemId;
        }

        public IDictionary<string, object> GetDefaultContentItem(
            ContentType contentType,
            string title = null,
            Uri uri = null,
            bool generateId = false)
        {
            IDictionary<string, object> contentItemData = new Dictionary<string, object>();
            if (!string.IsNullOrEmpty(title))
            {
                contentItemData.Add(MarketingCenterDbConstants.FieldNames.Title, title);

                if (uri != null)
                {
                    var uriPath = uri.GetLeftPart(UriPartial.Path);
                    contentItemData.Add("Uri",
                        uriPath
                            .Replace(Slam.Cms.Configuration.ConfigurationManager.Environment.FrontEndUrl, string.Empty)
                            .TrimStart('/'));
                }
            }

            contentItemData.Add(MarketingCenterDbConstants.FieldNames.Language, _userContext.SelectedLanguage);
            // contentItemData.Add(MarketingCenterDbConstants.FieldNames.RegionId, _userContext.SelectedRegion);

            if (generateId)
            {
                contentItemData.Add(MarketingCenterDbConstants.FieldNames.ContentItemId, _idGeneratorService.GetNewUniqueId());
            }

            //these are the default loaded manually before calling ContentService.GetDefaultValueData  wich might otherwise replace that singled out default value
            //since contentItemData just was created it assumed to be empty and the next line is to get all prepopulated default values->
            var preLoadedDefalValueNames = contentItemData.Keys.ToList();

            contentItemData = GetDefaultValueData(
                contentItemData,
                contentType.ContentTypeFieldDefinitions.Select(o => o.FieldDefinition) as FieldDefinition[] ?? contentType.ContentTypeFieldDefinitions.Select(o => o.FieldDefinition).ToArray(),
                _userContext.SelectedRegion,
                true,
                preLoadedDefalValueNames);

            return contentItemData;
        }

        public bool UrlAlreadyExists(string regionId, string languageIdentifier, string uri, string contentItemId)
        {
            var languageIdentifierLocal = string.IsNullOrEmpty(languageIdentifier) ? _tagServices.GetLanguageTagByContentItemId(contentItemId)?.Identifier : languageIdentifier;
            if (string.IsNullOrEmpty(languageIdentifierLocal))
            {
                return false;
            }

            return _tagServices.GetLanguageTagsByIdentifier(languageIdentifierLocal).Any(t =>
            {
                var contentItem = t.ContentItems.Where(ci => ci.RegionId.Equals(regionId, StringComparison.OrdinalIgnoreCase)
                    && ci.FrontEndUrl.Equals(uri, StringComparison.OrdinalIgnoreCase)
                    && (ci.StatusID != Status.Expired && ci.StatusID != Status.Deleted && ci.StatusID != Status.Archived)); //this retrieves all Content Items with the corresponding Url, Language and region

                return (contentItem.Any(ci => !ci.ContentItemId.GetAsLiveContentItemId().Equals(contentItemId.GetAsLiveContentItemId())) && contentItem.Any());
            });
        }

        private void SetRegionIntFields(string contentItemId, string regionId, IDictionary<string, object> offerModel, IEnumerable<FieldDefinition> fieldDefinitions)
        {
            foreach (var regionIntField in fieldDefinitions.Where(o => o.FieldTypeCode == FieldType.RegionInt))
            {
                var regionIntSetting = regionIntField.GetSettings<RegionIntSettings>();
                var visibilityRegionIds = _connection.Query<string>($"SELECT RegionId FROM {regionIntSetting.TableName} WHERE ContentItemId = '{contentItemId}'").Select(x => x.Trim()).ToList();
                regionIntSetting.ContentItemId = contentItemId;
                CreateRegionVisibilityIfNotExist(visibilityRegionIds, regionId, offerModel, regionIntField, regionIntSetting);
                if (!offerModel.ContainsKey(regionIntSetting.RelatedTableField))
                {
                    CreateRegionVisibilityWithoutCrossborders(visibilityRegionIds, regionId, offerModel, regionIntField, regionIntSetting, fieldDefinitions);
                }
                else
                {
                    CreateRegionVisibilityWithCrossborders(visibilityRegionIds, regionId, offerModel, regionIntField, regionIntSetting, fieldDefinitions);
                }
            }
        }

        private void CreateRegionVisibilityWithCrossborders(List<string> visibilityRegionIds, string regionId, IDictionary<string, object> offerModel,
            FieldDefinition regionIntField, RegionIntSettings regionIntSetting, IEnumerable<FieldDefinition> fieldDefinitions)
        {
            foreach (var visibilityRegionId in visibilityRegionIds)
            {
                if (visibilityRegionId.Equals(regionId))
                {
                    regionIntSetting.RegionId = visibilityRegionId;
                    int.TryParse(offerModel.GetData(regionIntField.Name).ToString(), out int value);
                    _commonService.UpdateContentItemRegionIntValue(regionIntSetting, value);
                }
                else if (!offerModel[regionIntSetting.RelatedTableField].ToString().Contains(visibilityRegionId) && visibilityRegionId.Equals(offerModel["SourceRegion"].ToString().ToLower()))
                {
                    regionIntSetting.RegionId = visibilityRegionId;
                    _commonService.RemoveContentItemRegionIntValue(regionIntSetting);
                }
            }

            foreach (var formData in offerModel[regionIntSetting.RelatedTableField].ToString().Replace(" ", string.Empty).Split(','))
            {
                if (!visibilityRegionIds.Contains(formData))
                {
                    regionIntSetting.RegionId = formData;
                    _commonService.AddContentItemRegionIntValue(regionIntSetting);
                }
            }
        }

        private void CreateRegionVisibilityWithoutCrossborders(List<string> visibilityRegionIds, string regionId, IDictionary<string, object> offerModel, FieldDefinition regionIntField, RegionIntSettings regionIntSetting, IEnumerable<FieldDefinition> fieldDefinitions)
        {
            foreach (string visibilityRegionId in visibilityRegionIds)
            {
                if (visibilityRegionId.Equals(regionId))
                {
                    regionIntSetting.RegionId = visibilityRegionId;
                    int.TryParse(offerModel.GetData(regionIntField.Name).ToString(), out int value);
                    _commonService.UpdateContentItemRegionIntValue(regionIntSetting, value);
                }
                else if (!visibilityRegionId.Equals(offerModel["SourceRegion"].ToString().ToLower()))
                {
                    regionIntSetting.RegionId = visibilityRegionId;
                    _commonService.RemoveContentItemRegionIntValue(regionIntSetting);
                }
            }
        }

        private void CreateRegionVisibilityIfNotExist(List<string> visibilityRegionIds, string regionId, IDictionary<string, object> offerModel, FieldDefinition regionIntField, RegionIntSettings regionIntSetting)
        {
            if (!visibilityRegionIds.Contains(regionId))
            {
                regionIntSetting.RegionId = regionId;
                int.TryParse(offerModel.GetData(regionIntField.Name).ToString(), out int value);
                _commonService.AddContentItemRegionIntValue(regionIntSetting, value);
            }
        }
    }
}