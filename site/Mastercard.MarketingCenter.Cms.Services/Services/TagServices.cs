﻿using Mastercard.MarketingCenter.Cms.Services.Core;
using Mastercard.MarketingCenter.Cms.Services.Infrastructure;
using Mastercard.MarketingCenter.Cms.Services.Models;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Data.Extensions;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using static Mastercard.MarketingCenter.Data.MarketingCenterDbConstants;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class TagServices
    {
        private readonly ICmsDbConnection _connection;
        private readonly ContentTypeDao _contentTypeDao;
        private readonly TagCategoryDao _tagCategoryDao;
        private readonly TagDao _tagDao;
        private readonly TagHierarchyPositionDao _tagHierarchyPositionDao;
        private readonly ICommonService _commonService;

        public TagServices(ICmsDbConnection connection, ContentTypeDao contentTypeDao, TagCategoryDao tagCategoryDao, TagDao tagDao, TagHierarchyPositionDao tagHierarchyPositionDao, ICommonService commonService)
        {
            _connection = connection;
            _contentTypeDao = contentTypeDao;
            _tagCategoryDao = tagCategoryDao;
            _tagDao = tagDao;
            _tagHierarchyPositionDao = tagHierarchyPositionDao;
            _commonService = commonService;
        }

        public IEnumerable<KeyValueItem> GetSelectedTags(string contentItemId)
        {
            const string sql = @"SELECT t.Identifier as Id, t.DisplayName as Value
                                FROM ContentItemTag cit 
                                join Tag t on cit.TagID = t.TagID
                                where  cit.ContentItemID = @contentItemId";

            return _connection.Query<KeyValueItem>(sql, new { contentItemId });
        }

        public IEnumerable<KeyValueItem> GetItemsTagsForCategory(string categoryId, string contentItemId)
        {
            const string sql = @"SELECT DISTINCT t.Identifier as Id, t.DisplayName as Value
                                FROM ContentItemTag cit
                                join Tag t on cit.TagID = t.TagID
                                join TagHierarchyPosition thp on thp.TagID = t.TagID
                                where cit.ContentItemID = @contentItemId and thp.TagCategoryID = @categoryId";

            return _connection.Query<KeyValueItem>(sql, new { categoryId, contentItemId });
        }

        public IEnumerable<KeyValueItem> GetItemsTagsForOtherRegions(string regionId, string contentItemId)
        {
            return _connection.Query<KeyValueItem>($@"
                                                      DECLARE @RegionTagId VARCHAR(25) = (SELECT TOP 1 [TagID] FROM [Region] WHERE [Id] = '{regionId}')
                                                      SELECT DISTINCT t.[Identifier] AS 'Id', t.[DisplayName] AS 'Value'
                                                      FROM [ContentItemTag] cit
                                                      JOIN [Tag] t ON cit.[TagID] = t.[TagID]
                                                      WHERE [ContentItemId] = '{contentItemId}'
                                                      AND (t.[TagId] <> @RegionTagId
                                                           AND t.[TagId] NOT IN
                                                           (SELECT [TagID] FROM [TagHierarchyPosition]
                                                           WHERE [ParentPositionID] = (SELECT TOP 1 [PositionID] FROM [TagHierarchyPosition] WHERE [TagID] = @RegionTagId)))
                                                   ");
        }

        public void DeleteTagCategory(string tagCategoryId)
        {
            var hierarchyPositions = _tagHierarchyPositionDao.GetAll().Where(p => p.TagCategory.TagCategoryId == tagCategoryId);
            foreach (var hierarchyPosition in hierarchyPositions)
            {
                _tagHierarchyPositionDao.Delete(hierarchyPosition);
            }

            var tagCategory = _tagCategoryDao.GetAll().FirstOrDefault(t => t.TagCategoryId == tagCategoryId);
            _tagCategoryDao.Delete(tagCategory);
        }

        public void DeleteTagHierarchyPosition(int positionId)
        {
            var childPositions = _tagHierarchyPositionDao.GetAll().Where(p => p.ParentPositionId == positionId).ToList();
            foreach (var childPosition in childPositions)
            {
                DeleteTagHierarchyPosition(childPosition.PositionId);
            }

            var position = _tagHierarchyPositionDao.GetAll().FirstOrDefault(p => p.PositionId == positionId);
            _tagHierarchyPositionDao.Delete(position);
        }

        public void AddNewTagToHierarchy(string tagId, string id)
        {
            var parentPositionId = 0;
            string tagCategoryId;

            if (id.StartsWith("p"))
            {
                parentPositionId = Convert.ToInt32(id.Replace("p", string.Empty));
                tagCategoryId = _tagHierarchyPositionDao.GetAll().First(o => o.PositionId == parentPositionId).TagCategoryId;
            }
            else
            {
                tagCategoryId = id;
            }

            var reorderedPositions = false;
            var position = new TagHierarchyPosition
            {
                ParentPositionId = parentPositionId > 0 ? parentPositionId : (int?)null,
                TagId = tagId,
                TagCategoryId = tagCategoryId
            };

            //If this new position has a parent, push all positions
            //past this parent down, and set SortOrder to the parent's
            //current SortOrder value.  Otherwise set SorOrder to 0.
            if (parentPositionId > 0)
            {
                var matchedParentPosition = _tagHierarchyPositionDao.GetAll().FirstOrDefault(p => p.PositionId == parentPositionId);
                if (matchedParentPosition != null && matchedParentPosition.PositionId > 0)
                {
                    position.SortOrder = matchedParentPosition.SortOrder + 1;

                    var hierarchyPositions = (from hierarchyPosition in _tagHierarchyPositionDao.GetAll()
                                              where hierarchyPosition.SortOrder > matchedParentPosition.SortOrder
                                              orderby hierarchyPosition.SortOrder
                                              select hierarchyPosition);

                    foreach (var hierarchyPosition in hierarchyPositions)
                    {
                        hierarchyPosition.SortOrder = hierarchyPosition.SortOrder + 1;
                    }
                    reorderedPositions = true;
                }
            }
            else
            {
                position.SortOrder = 0;
            }

            _tagHierarchyPositionDao.Add(position);

            //If new position is top level then reorder all positions
            if (!reorderedPositions)
            {
                var hierarchyPositions = (from hierarchyPosition in _tagHierarchyPositionDao.GetAll()
                                          orderby hierarchyPosition.TagCategory.DisplayOrder, hierarchyPosition.SortOrder
                                          select hierarchyPosition);
                int i = 1;
                foreach (var hierarchyPosition in hierarchyPositions)
                {
                    hierarchyPosition.SortOrder = i;
                    i++;
                }
            }
        }

        public IEnumerable<Tag> GetLanguageTagsByIdentifier(string identifier)
        {
            return _tagCategoryDao.GetByTitle("Languages").TagHierarchyPositions.Select(thp => thp.Tag).Where(t => t.Identifier.Equals(identifier));
        }

        public Tag GetLanguageTagByContentItemId(string contentItemId, bool anyStatus = true)
        {
            var languageTags = _tagCategoryDao.GetByTitle("Languages").TagHierarchyPositions.Select(thp => thp.Tag);
            if (anyStatus)
            {
                return languageTags.FirstOrDefault(t => t.ContentItems.Any(ci => ci.ContentItemId.GetAsLiveContentItemId().Equals(contentItemId.GetAsLiveContentItemId(), StringComparison.InvariantCultureIgnoreCase)));
            }

            return languageTags.FirstOrDefault(t => t.ContentItems.Any(ci => ci.ContentItemId.Equals(contentItemId, StringComparison.InvariantCultureIgnoreCase)));
        }

        public void SetTags(string contentItemId, IDictionary<string, object> contentData)
        {
            var contentTypeId = _commonService.GetContentTypeId(contentItemId);
            var contentType = _contentTypeDao.GetById(contentTypeId);
            var contentTypeFieldDefs = contentType.ContentTypeFieldDefinitions;
            var insertedTagIds = new HashSet<object>();
            var tagsFieldDefs = contentTypeFieldDefs.Where(o => o.FieldDefinition.FieldTypeCode == FieldType.Tag)
                                                    .Select(o => o.FieldDefinition);

            _connection.ExecuteInTransaction(() =>
            {
                // first remove existing Tags for the content item
                _connection.Execute("DELETE [ContentItemTag] WHERE [ContentItemId] = @contentItemId", new { contentItemId });

                foreach (var fieldDefinition in tagsFieldDefs)
                {
                    // check if contentData contains a Tag field
                    if (contentData.ContainsKey(fieldDefinition.Name))
                    {
                        var tagIdentifiers = Convert.ToString(contentData[fieldDefinition.Name]).Split(',');
                        foreach (var tagIdentifier in tagIdentifiers)
                        {
                            var trimmedTagIdentifier = tagIdentifier.Trim();

                            if (string.IsNullOrEmpty(trimmedTagIdentifier))
                            {
                                continue;
                            }

                            // then insert the new tags in the ContentItemTag table
                            var tagId = _connection.ExecuteScalar("SELECT TagId FROM Tag WHERE Identifier = @tagIdentifier",
                                                                  new { tagIdentifier = trimmedTagIdentifier });

                            if (insertedTagIds.Contains(tagId))
                            {
                                continue;
                            }

                            _connection.Execute("INSERT INTO ContentItemTag (ContentItemId,TagId) VALUES(@contentItemId,@tagId)",
                                                new { contentItemId, tagId });

                            insertedTagIds.Add(tagId);
                        }
                    }
                }
            });
        }

        public void DeleteTag(string tagId)
        {
            var hierarchyPositions = _tagHierarchyPositionDao.GetAll().Where(p => p.TagId == tagId).ToList();
            foreach (var hierarchyPosition in hierarchyPositions)
            {
                DeleteTagHierarchyPosition(hierarchyPosition.PositionId);
            }

            var tagToDelete = _tagDao.GetById(tagId);
            _tagDao.Delete(tagToDelete);
        }

        public void PopulateTagsValues(IDictionary<string, object> contentData, IEnumerable<FieldDefinition> fieldDefinitions, string itemId)
        {
            foreach (var fieldDefinition in fieldDefinitions.Where(o => o.FieldTypeCode == FieldType.Tag))
            {
                if (!string.IsNullOrEmpty(fieldDefinition.CustomSettings))
                {
                    var tagSettings = fieldDefinition.GetSettings<TagSettings>();
                    var serializedValue = string.Join(",", GetItemsTagsForCategory(tagSettings.TagCategoryId, itemId).Select(t => t.Id));
                    contentData.AddOrUpdate(fieldDefinition.Name, serializedValue);
                }
            }
        }
    }
}