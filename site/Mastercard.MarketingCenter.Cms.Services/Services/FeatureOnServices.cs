﻿using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class FeatureOnServices
    {
        private readonly TagCategoryDao _tagCategoryDao;
        private readonly FeatureLocationDao _featureLocationDao;
        private readonly TagHierarchyPositionDao _tagHierarchyPositionDao;
        private readonly TagDao _tagDao;
        private const string SelectAllText = "Select All";

        public FeatureOnServices(TagCategoryDao tagCategoryDao, FeatureLocationDao featureLocationDao, TagHierarchyPositionDao tagHierarchyPositionDao, TagDao tagDao)
        {
            _tagCategoryDao = tagCategoryDao;
            _featureLocationDao = featureLocationDao;
            _tagHierarchyPositionDao = tagHierarchyPositionDao;
            _tagDao = tagDao;
        }

        public List<FeaturableTagCategory> GetFeaturableTagCategories(
            string contentTypeId,
            string userName,
            IEnumerable<Group> userGroups,
            string contentItemId)
        {
            var tagCategories = (from tagCategory in _tagCategoryDao.GetAll()
                                 where !tagCategory.Featurable.Equals("Never", StringComparison.OrdinalIgnoreCase)
                                 select new FeaturableTagCategory
                                 {
                                     Title = tagCategory.Title,
                                     Featurable = tagCategory.Featurable,
                                     Hierarchical = tagCategory.TagHierarchyPositions.Count > 0
                                 })
                                 .ToList();

            if (!tagCategories.Exists(tc => tc.Title.Equals(SelectAllText)))
            {
                var featureLocationsCategory = new FeaturableTagCategory
                {
                    Title = SelectAllText,
                    Featurable = "Always",
                    Hierarchical = false
                };

                tagCategories.Insert(0, featureLocationsCategory);
            }

            foreach (var featurableTagCategory in tagCategories)
            {
                List<FeatureTagItem> featureTags;
                if (featurableTagCategory.Title.Equals(SelectAllText))
                {
                    featureTags = GetUserFeaturableFeatureLocations(
                        contentTypeId,
                        userName,
                        userGroups,
                        contentItemId);
                }
                else
                {
                    featureTags = GetFeaturableTagsForCategory(featurableTagCategory.Title, contentItemId);

                    if (featurableTagCategory.Hierarchical && featureTags.Any())
                    {
                        featureTags = GetFeaturableTagsWithParentage(featureTags, featurableTagCategory.Title);
                    }
                }

                if (!featurableTagCategory.Featurable.Equals("Always", StringComparison.OrdinalIgnoreCase))
                {
                    featurableTagCategory.Title = "Feature on " + featurableTagCategory.Title;
                }

                featurableTagCategory.FeatureTagItems = featureTags;
            }

            return tagCategories.Where(o => o.FeatureTagItems.Any()).ToList();
        }

        public List<string> FilterTags(string contentTypeId, string userName, IEnumerable<string> tagsList)
        {
            return _tagHierarchyPositionDao.GetAll().Where(tc =>
                        (tc.TagCategory != null ? !tc.TagCategory.Featurable.Equals("Never", StringComparison.OrdinalIgnoreCase) : true) &&
                        (tc.Tag.ContentTypes.Count == 0 || tc.Tag.ContentTypes.Any(ct => ct.ContentTypeId.Equals(contentTypeId, StringComparison.OrdinalIgnoreCase))) &&
                        (tc.Tag.FeaturingUsers.Count == 0 || tc.Tag.FeaturingUsers.Any(u => u.UserName.Equals(userName))))
                        .Select(tc => tc.TagId)
                        .Where(tc => tagsList.Contains(tc))
                        .ToList();
        }

        public List<FeatureTagItem> GetFeaturableTagsWithParentage(List<FeatureTagItem> tags, string tagCategory)
        {
            var returnTags = new List<FeatureTagItem>();
            var hierarchyTags = (from tagPosition in _tagHierarchyPositionDao.GetAll()
                                 orderby tagPosition.ParentPositionId, tagPosition.SortOrder
                                 select new HierarchicalTag
                                 {
                                     Id = tagPosition.Tag.TagID,
                                     Title =
                                         string.IsNullOrEmpty(tagPosition.Tag.DisplayName)
                                             ? tagPosition.Tag.Identifier
                                             : tagPosition.Tag.DisplayName,
                                     PositionId = tagPosition.PositionId,
                                     ParentPositionId = tagPosition.ParentPositionId.HasValue ? tagPosition.ParentPositionId.Value : 0,
                                     TagCategory = tagPosition.TagCategory.Title
                                 }).ToList();

            hierarchyTags = hierarchyTags.Where(t => t.TagCategory.Equals(tagCategory, StringComparison.OrdinalIgnoreCase)).ToList();

            var parentage = new Stack<string>();
            var parentIds = new Stack<int>();

            foreach (var hierarchyItem in hierarchyTags)
            {
                while (parentIds.Count > 0 && hierarchyItem.ParentPositionId != parentIds.Peek())
                {
                    parentage.Pop();
                    parentIds.Pop();
                }

                parentage.Push(hierarchyItem.Title);
                parentIds.Push(hierarchyItem.PositionId);

                var newTagItem = new FeatureTagItem();
                var oldTagItem = tags.Find(t => t.Id == hierarchyItem.Id);
                if (oldTagItem != null)
                {
                    newTagItem.Title = String.Join(" > ", parentage.Reverse().ToArray());
                    newTagItem.Id = oldTagItem.Id;
                    newTagItem.FeatureLevel = oldTagItem.FeatureLevel;
                    newTagItem.Editable = oldTagItem.Editable;
                    newTagItem.Type = FeatureType.Tag;
                    returnTags.Add(newTagItem);
                }
            }

            return returnTags;
        }

        public void SaveContentItemFeatureOn(string contentItemId, IEnumerable<FeatureTagItem> featureTagItems)
        {
            var tagsToUpdate = _tagDao.GetAll().Where(o => o.ContentItemFeatureOnTags.Any(cift => cift.ContentItemId == contentItemId)).ToList();

            foreach (var tag in tagsToUpdate)
            {
                var featureOnTagsToRemove = tag.ContentItemFeatureOnTags.Where(o => o.ContentItemId == contentItemId).ToList();

                foreach (var featureOnTag in featureOnTagsToRemove)
                {
                    tag.ContentItemFeatureOnTags.Remove(featureOnTag);
                }
            }

            var featureLocationsToUpdate = _featureLocationDao.GetAll().Where(o => o.ContentItemFeatureLocations.Any(cifl => cifl.ContentItemId == contentItemId)).ToList();

            foreach (var featureLocation in featureLocationsToUpdate)
            {
                var contentItemFeatureLocationsToRemove = featureLocation.ContentItemFeatureLocations.Where(o => o.ContentItemId == contentItemId).ToList();

                foreach (var contentItemFeatureLocation in contentItemFeatureLocationsToRemove)
                {
                    featureLocation.ContentItemFeatureLocations.Remove(contentItemFeatureLocation);
                }
            }

            foreach (var featureTagItem in featureTagItems)
            {
                if (featureTagItem.Type == FeatureType.Tag)
                {
                    var tag = _tagDao.GetById(featureTagItem.Id);
                    tag.ContentItemFeatureOnTags.Add(new ContentItemFeatureOnTag
                    {
                        ContentItemId = contentItemId,
                        FeatureLevel = featureTagItem.FeatureLevel,
                        TagId = tag.TagID
                    });
                }
                else
                {
                    var featureLocation = _featureLocationDao.GetById(featureTagItem.Id);
                    featureLocation.ContentItemFeatureLocations.Add(new ContentItemFeatureLocation
                    {
                        ContentItemId = contentItemId,
                        FeatureLevel = featureTagItem.FeatureLevel,
                        FeatureLocationId = featureLocation.FeatureLocationId
                    });
                }
            }
        }

        public IEnumerable<FeatureTagItem> GetContentItemFeatureItems(string contentItemId)
        {
            var tagItems =
                _tagDao.GetAll().Where(o => o.ContentItemFeatureOnTags.Any(cift => cift.ContentItemId == contentItemId));

            var featureOnTags =
                tagItems.SelectMany(o => o.ContentItemFeatureOnTags)
                    .Where(o => o.ContentItemId == contentItemId)
                    .Select(
                        o =>
                            new FeatureTagItem
                            {
                                FeatureLevel = o.FeatureLevel,
                                Title = o.Tag.DisplayName,
                                Id = o.TagId,
                                Type = FeatureType.Tag
                            })
                    .ToList();

            var locationItems = _featureLocationDao.GetAll().Where(o => o.ContentItemFeatureLocations.Any(cifl => cifl.ContentItemId == contentItemId));

            var featureOnLocations =
                locationItems.SelectMany(o => o.ContentItemFeatureLocations)
                    .Where(o => o.ContentItemId == contentItemId)
                    .Select(
                        o =>
                            new FeatureTagItem
                            {
                                FeatureLevel = o.FeatureLevel,
                                Id = o.FeatureLocationId,
                                Title = o.FeatureLocation.Title,
                                Type = FeatureType.FeatureLocation
                            })
                    .ToList();

            return featureOnLocations.Concat(featureOnTags);
        }

        public List<FeatureTagItem> GetUserFeaturableTagsForCategory(string tagCategory, string contentTypeId, User user, string contentItemId)
        {
            var filterScope = !string.IsNullOrEmpty(contentTypeId);
            List<FeatureTagItem> tags;
            if (user.Groups.Any(o => o.Name == "Feature Admin"))
            {
                tags = (from tagPosition in _tagHierarchyPositionDao.GetAll()
                        where (tagPosition.Tag.ContentTypes.Any(c => filterScope ? c.ContentTypeId.Equals(contentTypeId, StringComparison.OrdinalIgnoreCase) : true) ||
                              tagPosition.Tag.ContentTypes.Count == 0)
                        select new FeatureTagItem
                        {
                            Id = tagPosition.Tag.TagID,
                            Title = string.IsNullOrEmpty(tagPosition.Tag.DisplayName) ? tagPosition.Tag.Identifier : tagPosition.Tag.DisplayName,
                            TagCategory = tagPosition.TagCategory.Title,
                            FeatureLevel = tagPosition.Tag.ContentItemFeatureOnTags.Any(cif => cif.ContentItemId.Equals(contentItemId, StringComparison.OrdinalIgnoreCase)) ? tagPosition.Tag.ContentItemFeatureOnTags.FirstOrDefault(cif => cif.ContentItemId.Equals(contentItemId, StringComparison.OrdinalIgnoreCase)).FeatureLevel : 0,
                            Editable = true
                        })
                        .ToList();
            }
            else
            {
                tags = (from tagPosition in _tagHierarchyPositionDao.GetAll()
                        where (tagPosition.Tag.ContentTypes.Any(c => filterScope ? c.ContentTypeId == contentTypeId : true) ||
                              ((tagPosition.Tag.ContentTypes.Count == 0) &&
                              tagPosition.Tag.FeaturingUsers.Any(u => u.UserName.Equals(user.UserName))))
                        select new FeatureTagItem
                        {
                            Id = tagPosition.Tag.TagID,
                            Title = string.IsNullOrEmpty(tagPosition.Tag.DisplayName) ? tagPosition.Tag.Identifier : tagPosition.Tag.DisplayName,
                            TagCategory = tagPosition.TagCategory.Title,
                            FeatureLevel = tagPosition.Tag.ContentItemFeatureOnTags.Any(cif => cif.ContentItemId == contentItemId) ? tagPosition.Tag.ContentItemFeatureOnTags.FirstOrDefault(cif => cif.ContentItemId == contentItemId).FeatureLevel : 0,
                            Editable = true
                        })
                        .Union(
                        (from tagPosition in _tagHierarchyPositionDao.GetAll()
                         where tagPosition.Tag.ContentItemFeatureOnTags.Any(cif => cif.ContentItemId == contentItemId) &&
                                !tagPosition.Tag.FeaturingUsers.Any(u => u.UserName.Equals(user.UserName))
                         select new FeatureTagItem
                         {
                             Id = tagPosition.Tag.TagID,
                             Title = string.IsNullOrEmpty(tagPosition.Tag.DisplayName) ? tagPosition.Tag.Identifier : tagPosition.Tag.DisplayName,
                             TagCategory = tagPosition.TagCategory.Title,
                             FeatureLevel = tagPosition.Tag.ContentItemFeatureOnTags.Count > 0 ? tagPosition.Tag.ContentItemFeatureOnTags.FirstOrDefault().FeatureLevel : 0,
                             Editable = false
                         }))
                        .ToList();
            }

            var contentItemTags =
                _tagDao.GetAll()
                    .Where(
                        o =>
                            o.ContentItems.Any(ci => ci.ContentItemId == contentItemId) &&
                            o.TagHierarchyPositions.Any(p => p.TagCategory.Title == tagCategory))
                            .ToList()
                    .Select(
                        o =>
                            new FeatureTagItem
                            {
                                Editable = false,
                                FeatureLevel = 0,
                                Id = o.TagID,
                                TagCategory = o.TagHierarchyPositions.FirstOrDefault() == null ? string.Empty : o.TagHierarchyPositions.FirstOrDefault().TagCategory.Title,
                                Title = string.IsNullOrEmpty(o.DisplayName) ? o.Identifier : o.DisplayName
                            });

            var allTags = contentItemTags.Union(tags).ToList();

            allTags.ForEach(t => t.Type = FeatureType.Tag);

            return allTags.Where(t => t.TagCategory.Equals(tagCategory, StringComparison.OrdinalIgnoreCase)).ToList();
        }

        public List<FeatureTagItem> GetFeaturableTagsForCategory(string tagCategory, string contentItemId)
        {
            return _tagDao.GetAll().Where(o => o.ContentItems.Any(ci => ci.ContentItemId == contentItemId) &&
                                               o.TagHierarchyPositions.Any(p => p.TagCategory.Title == tagCategory))
                                   .Select(o => new FeatureTagItem
                                   {
                                       Editable = false,
                                       FeatureLevel = 0,
                                       Id = o.TagID,
                                       TagCategory = o.TagHierarchyPositions.FirstOrDefault(p => p.TagCategory.Title == tagCategory) == null ? string.Empty : o.TagHierarchyPositions.FirstOrDefault(p => p.TagCategory.Title == tagCategory).TagCategory.Title,
                                       Title = string.IsNullOrEmpty(o.DisplayName) ? o.Identifier : o.DisplayName
                                   })
                                   .Where(t => t.TagCategory.Equals(tagCategory, StringComparison.OrdinalIgnoreCase))
                                   .ToList();
        }

        public List<FeatureTagItem> GetUserFeaturableFeatureLocations(
            string contentTypeId,
            string userName,
            IEnumerable<Group> userGroups,
            string contentItemId)
        {
            var filterScope = !String.IsNullOrEmpty(contentTypeId);

            List<FeatureTagItem> featureLocations;

            if (userGroups.Any(o => o.Name == "Feature Admin"))
            {
                featureLocations = (from featureLocation in _featureLocationDao.GetAll()
                                    where featureLocation.Scopes.Any(c => filterScope ? c.ContentTypeId == contentTypeId : true) ||
                                          featureLocation.Scopes.Count == 0
                                    select new FeatureTagItem
                                    {
                                        Id = featureLocation.FeatureLocationId,
                                        Title = featureLocation.Title,
                                        FeatureLevel = featureLocation.ContentItemFeatureLocations.Any(cif => cif.ContentItemId == contentItemId) ? featureLocation.ContentItemFeatureLocations.FirstOrDefault(cif => cif.ContentItemId == contentItemId).FeatureLevel : 0,
                                        Editable = true
                                    })
                                    .ToList();
            }
            else
            {
                featureLocations = (from featureLocation in _featureLocationDao.GetAll()
                                    where (featureLocation.Scopes.Any(c => filterScope ? c.ContentTypeId == contentTypeId : true) ||
                                           ((featureLocation.Scopes.Count == 0) &&
                                            featureLocation.Users.Any(u => u.UserName.Equals(userName))))
                                    select new FeatureTagItem
                                    {
                                        Id = featureLocation.FeatureLocationId,
                                        Title = featureLocation.Title,
                                        FeatureLevel =
                                            featureLocation.ContentItemFeatureLocations.Any(cif => cif.ContentItemId == contentItemId)
                                                ? featureLocation.ContentItemFeatureLocations.FirstOrDefault(cif => cif.ContentItemId == contentItemId).FeatureLevel
                                                : 0
                                    }).ToList();
            }

            featureLocations.ForEach(l => l.Type = FeatureType.FeatureLocation);

            return featureLocations;
        }

        public void PopulateFeatureOnValues(IDictionary<string, object> contentData, IEnumerable<FieldDefinition> fieldDefinitions, string itemId)
        {
            foreach (var fieldDefinition in fieldDefinitions.Where(o => o.FieldTypeCode == FieldType.FeatureOn))
            {
                contentData.AddOrUpdate(fieldDefinition.Name, GetContentItemFeatureItems(itemId));
            }
        }
    }
}