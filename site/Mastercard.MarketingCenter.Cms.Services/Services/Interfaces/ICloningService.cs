﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public interface ICloningService
    {
        IEnumerable<ContentTypeFieldDefinition> GetOnlyInterRegionCloning(IEnumerable<ContentTypeFieldDefinition> fieldDefinitions);

        /// <summary>
        /// Is used for both Copy and Clone functionality
        /// </summary>
        string Clone(
            string contentTypeId,
            IDictionary<string, object> contentData,
            int userId,
            string regionId);

        IEnumerable<ContentTypeFieldDefinition> FilterOutFieldsForClones(IDictionary<string, object> contentItemData, IEnumerable<ContentTypeFieldDefinition> fieldDefinitions, bool isAConfirmedClone);
    }
}