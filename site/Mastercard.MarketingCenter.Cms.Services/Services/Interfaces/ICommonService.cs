﻿using Mastercard.MarketingCenter.Data.Entities.CustomSettings;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public interface ICommonService
    {
        string GetContentTypeId(string contentItemId);
        bool HasDraft(string contentItemId, bool filterDeleted = true);
        int GetContentItemStatus(string contentItemId);
        int GetContentItemRegionIntValue(RegionIntSettings settings, int defaultValue);
        void AddContentItemRegionIntValue(RegionIntSettings settings, int value = 2);
        void UpdateContentItemRegionIntValue(RegionIntSettings settings, int value);
        void RemoveContentItemRegionIntValue(RegionIntSettings settings);
        string GetItemRegionTitle(RegionTitleSettings settings);
        string GetEntityTitle(string tableName);
    }
}