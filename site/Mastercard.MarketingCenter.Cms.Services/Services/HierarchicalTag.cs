﻿namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class HierarchicalTag
    {
        public string Id
        {
            get;
            set;
        }

        public int ParentPositionId
        {
            get;
            set;
        }

        public int PositionId
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string TagCategory
        {
            get;
            set;
        }
    }
}