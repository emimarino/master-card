﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Data;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class CommonService : ICommonService
    {
        private readonly ICmsDbConnection _connection;
        public CommonService(ICmsDbConnection connection)
        {
            _connection = connection;
        }

        public string GetContentTypeId(string contentItemId)
        {
            return _connection.ExecuteScalar<string>("SELECT ContentTypeId FROM ContentItem WHERE ContentItemId = @contentItemId", new { contentItemId });
        }

        public bool HasDraft(string contentItemId, bool filterDeleted = true)
        {
            var draftContentItemId = contentItemId.GetAsDraftContentItemId();
            var query = "SELECT COUNT(*) FROM ContentItem WHERE ContentItemId = @contentItemId";

            if (filterDeleted)
            {
                query += " and StatusId != @deletedStatus";
            }

            return _connection.ExecuteScalar<int>(query, new { contentItemId = draftContentItemId, deletedStatus = Status.Deleted }) != 0;
        }

        public int GetContentItemStatus(string contentItemId)
        {
            return _connection.ExecuteScalar<int>("SELECT StatusId FROM ContentItem WHERE ContentItemId = @contentItemId", new { contentItemId });
        }

        public int GetContentItemRegionIntValue(RegionIntSettings settings, int defaultValue)
        {
            var value = _connection.ExecuteScalar($@"
                                                     SELECT [Value]
                                                     FROM {settings.TableName} 
                                                     WHERE [ContentItemId] = '{settings.ContentItemId}'
                                                     AND [RegionId] = '{settings.RegionId}'
                                                  ");

            return value == null ? defaultValue : (int)value;
        }        

        public void AddContentItemRegionIntValue(RegionIntSettings settings, int value = 2)
        {
            _connection.ExecuteInTransaction(() =>
            {                
                _connection.ExecuteScalar($@"
                                             INSERT INTO {settings.TableName} ([ContentItemId], [RegionId], [Value])
                                             VALUES ('{settings.ContentItemId}', '{settings.RegionId}', @value)
                                          ", new { value });
            });
        }

        public void RemoveContentItemRegionIntValue(RegionIntSettings settings)
        {
            _connection.ExecuteInTransaction(() =>
            {
                _connection.ExecuteScalar($@"
                                             DELETE {settings.TableName} 
                                             WHERE [ContentItemId] = '{settings.ContentItemId}'
                                             AND [RegionId] = '{settings.RegionId}'
                                          ");
            });
        }

        public void UpdateContentItemRegionIntValue(RegionIntSettings settings, int value)
        {
            _connection.ExecuteInTransaction(() =>
            {
                _connection.ExecuteScalar($@"
                                             UPDATE {settings.TableName}
                                             SET [Value] = '{value}'
                                             WHERE [ContentItemId] = '{settings.ContentItemId}' AND [RegionId] = '{settings.RegionId}'                                           
                                          ");
            });
        }

        public string GetItemRegionTitle(RegionTitleSettings settings)
        {
            var sql = $@"WITH RegionName AS (
                            SELECT	[Id], [Name]
                            FROM	[Region] r
                            JOIN	[{settings.TableName}] x ON r.[Id] = x.[{settings.TableRegionField}]
                            WHERE	x.[{settings.TableIdField}] = '{settings.TableIdValue}'
                            UNION ALL 
                            SELECT NULL, (SELECT TOP 1 [Name] FROM [Region] WHERE [Id] = '{settings.CurrentRegionId}')
                        )
                        SELECT TOP 1 [Name]
                        FROM   [RegionName]
                        ORDER BY [Id] DESC";

            return _connection.ExecuteScalar<string>(sql);
        }

        public string GetEntityTitle(string tableName)
        {
            return _connection.ExecuteScalar<string>("SELECT TOP 1 [Title] FROM (SELECT [Title] FROM [ContentType] WHERE [TableName] = @tableName UNION SELECT [Name] AS [Title] FROM [List] WHERE [TableName] = @tableName) t", new { tableName });
        }
    }
}