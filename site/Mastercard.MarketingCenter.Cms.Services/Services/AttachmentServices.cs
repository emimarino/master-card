﻿using Mastercard.MarketingCenter.Cms.Services.Core;
using Mastercard.MarketingCenter.Cms.Services.Infrastructure;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Data.Extensions;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class AttachmentServices
    {
        private readonly ICmsDbConnection _connection;
        private readonly ContentTypeDao _contentTypeDao;
        private readonly IContentItemService _contentItemServices;
        private readonly ICommonService _commonService;

        public AttachmentServices(ICmsDbConnection connection, ContentTypeDao contentTypeDao, IContentItemService contentItemServices, ICommonService commonService)
        {
            _connection = connection;
            _contentTypeDao = contentTypeDao;
            _contentItemServices = contentItemServices;
            _commonService = commonService;
        }

        public IEnumerable<AttachmentInfo> GetAttachments(string contentItemId)
        {
            if (_connection.ExecuteScalar<int>($"SELECT Count(ContentItemId) FROM ContentItem WHERE ContentItemId = '{contentItemId}'") <= 0)
                return Enumerable.Empty<AttachmentInfo>();

            var attachmentSettings = GetAttachmentSettings(contentItemId);
            var sql = string.Format(@"SELECT * FROM {0} WHERE ContentItemId = @contentItemId", attachmentSettings.TableName);

            return _connection.Query<AttachmentInfo>(sql, new { contentItemId });
        }

        public void SaveAttachments(string contentItemId, IEnumerable<string> attachments)
        {
            _connection.ExecuteInTransaction(() =>
            {
                var attachmentSettings = GetAttachmentSettings(contentItemId);
                var deleteSql = $"DELETE {attachmentSettings.TableName} WHERE ContentItemId = @contentItemId";

                _connection.ExecuteScalar(deleteSql, new { contentItemId });

                foreach (var attachment in attachments)
                {
                    var insertSql = $"INSERT INTO {attachmentSettings.TableName} (ContentItemId, FileUrl) VALUES(@contentItemId,@attachment)";
                    _connection.ExecuteScalar(insertSql, new { contentItemId, attachment });
                }
            });
        }

        private AttachmentSettings GetAttachmentSettings(string contentItemId)
        {
            var contentTypeId = _commonService.GetContentTypeId(contentItemId);
            var contentType = _contentTypeDao.GetById(contentTypeId);
            var attachmentContentTypeFieldDef = contentType.ContentTypeFieldDefinitions.FirstOrDefault(o => o.FieldDefinition.FieldTypeCode == FieldType.Attachment);

            if (attachmentContentTypeFieldDef != null &&
                !string.IsNullOrEmpty(attachmentContentTypeFieldDef.FieldDefinition.CustomSettings))
            {
                return attachmentContentTypeFieldDef.FieldDefinition.GetSettings<AttachmentSettings>();
            }

            throw new InvalidOperationException(string.Format("No Attachment field defined for ContentItemId: {0}", contentItemId));
        }

        public void SetAttachments(string itemId, IDictionary<string, object> data, IEnumerable<FieldDefinition> fieldDefinitions)
        {
            var attachmentFieldDef = fieldDefinitions.FirstOrDefault(o => o.FieldTypeCode == FieldType.Attachment);
            if (attachmentFieldDef != null)
            {
                var attachments = Enumerable.Empty<string>();
                var value = data.GetData(attachmentFieldDef.Name);
                if (!string.IsNullOrEmpty(Convert.ToString(value).Trim()))
                {
                    attachments = JsonConvert.DeserializeObject<List<string>>(Convert.ToString(value));
                }

                SaveAttachments(itemId, attachments);
            }
        }

        public void PopulateAttachmentValues(IDictionary<string, object> contentData, IEnumerable<FieldDefinition> fieldDefinitions, string itemId)
        {
            foreach (var fieldDefinition in fieldDefinitions.Where(o => o.FieldTypeCode == FieldType.Attachment))
            {
                var finalAttachments = GetAttachments(itemId).Select(a => a.FileUrl);
                if (contentData.ContainsKey(fieldDefinition.Name))
                {
                    var fieldData = (string)contentData[fieldDefinition.Name];
                    if (!string.IsNullOrEmpty(fieldData))
                    {
                        var newAttachments = JsonConvert.DeserializeObject<IEnumerable<string>>((string)contentData[fieldDefinition.Name]);
                        finalAttachments = newAttachments.ToList();
                    }
                    contentData[fieldDefinition.Name] = JsonConvert.SerializeObject(finalAttachments);
                }
                else
                {
                    var newAttachmentsJSON = JsonConvert.SerializeObject(finalAttachments);
                    contentData.Add(fieldDefinition.Name, newAttachmentsJSON);
                }
            }
        }

        public class AttachmentInfo
        {
            public string AttachmentId { get; set; }
            public string ContentItemId { get; set; }
            public string FileUrl { get; set; }
        }
    }
}