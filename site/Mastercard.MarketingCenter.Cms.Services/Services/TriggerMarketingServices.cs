﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class TriggerMarketingServices
    {
        private readonly ICmsDbConnection _connection;
        private readonly ContentTypeDao _contentTypeDao;
        private readonly IContentItemService _contentItemServices;
        private readonly ICommonService _commonService;

        public TriggerMarketingServices(ICmsDbConnection connection, ContentTypeDao contentTypeDao, IContentItemService contentItemServices, ICommonService commonService)
        {
            _connection = connection;
            _contentTypeDao = contentTypeDao;
            _contentItemServices = contentItemServices;
            _commonService = commonService;
        }

        public IEnumerable<ContentItemTriggerMarketing> GetTriggerMarketing(string contentItemId)
        {
            contentItemId = contentItemId.GetAsLiveContentItemId();

            if (_contentItemServices.GetContentItem(contentItemId) == null)
            {
                return Enumerable.Empty<ContentItemTriggerMarketing>();
            }

            var triggerMarketingSettings = GetTriggerMarketingSettings(contentItemId);

            var sql = $"SELECT * FROM {triggerMarketingSettings.TableName} WHERE ContentItemId = @contentItemId AND StatusId <> @statusId";

            return _connection.Query<ContentItemTriggerMarketing>(sql, new { contentItemId, statusId = Status.Published });
        }

        public void SaveTriggerMarketing(ContentItemTriggerMarketing triggerMarketing)
        {
            _connection.ExecuteInTransaction(() =>
            {
                triggerMarketing.ContentItemId = triggerMarketing.ContentItemId.GetAsLiveContentItemId();
                var triggerMarketingSettings = GetTriggerMarketingSettings(triggerMarketing.ContentItemId);

                var deleteSql = string.Format(@"DELETE {0} WHERE ContentItemId = @contentItemId AND StatusId <> @statusId", triggerMarketingSettings.TableName);
                _connection.ExecuteScalar(deleteSql, new { contentItemId = triggerMarketing.ContentItemId, statusId = Status.Published });

                if (triggerMarketing.StatusId != Status.Published || triggerMarketing.Notify)
                {
                    var insertSql = $@"INSERT INTO {triggerMarketingSettings.TableName}
                                       (ContentItemId, Notify, Featured, Reasons, StatusId, IsNew, SavedDate)
                                       VALUES (@contentItemId, @notify, @featured, @reasons, @statusId, @isNew, @savedDate)";

                    _connection.ExecuteScalar(insertSql,
                                              new
                                              {
                                                  contentItemId = triggerMarketing.ContentItemId,
                                                  notify = triggerMarketing.Notify,
                                                  featured = triggerMarketing.Featured,
                                                  reasons = triggerMarketing.Reasons,
                                                  statusId = triggerMarketing.StatusId,
                                                  isNew = triggerMarketing.IsNew,
                                                  savedDate = DateTime.Now
                                              });
                }
            });
        }

        private TriggerMarketingSettings GetTriggerMarketingSettings(string contentItemId)
        {
            var contentTypeId = _commonService.GetContentTypeId(contentItemId);
            var contentType = _contentTypeDao.GetById(contentTypeId);
            var triggerMarketingContentTypeFieldDef = contentType.ContentTypeFieldDefinitions.FirstOrDefault(o => o.FieldDefinition.FieldTypeCode == FieldType.TriggerMarketing);

            if (triggerMarketingContentTypeFieldDef != null && !string.IsNullOrEmpty(triggerMarketingContentTypeFieldDef.FieldDefinition.CustomSettings))
            {
                return triggerMarketingContentTypeFieldDef.FieldDefinition.GetSettings<TriggerMarketingSettings>();
            }

            throw new InvalidOperationException($"No Trigger Marketing field defined for ContentItemId: {contentItemId}");
        }

        class TriggerMarketingSettings
        {
            public string TableName { get; set; }
        }
    }
}