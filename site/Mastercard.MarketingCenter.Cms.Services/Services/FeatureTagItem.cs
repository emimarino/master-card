﻿namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class FeatureTagItem
    {
        public string Id
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public int FeatureLevel
        {
            get;
            set;
        }

        public bool Editable
        {
            get;
            set;
        }

        public string TagCategory
        {
            get;
            set;
        }

        public FeatureType Type
        {
            get;
            set;
        }
    }
}