﻿using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class ContentTypeService
    {
        private readonly ContentItemDao _contentItemDao;
        private readonly ContentTypeDao _contentTypeDao;

        public ContentTypeService(ContentItemDao contentItemDao, ContentTypeDao contentTypeDao)
        {
            _contentItemDao = contentItemDao;
            _contentTypeDao = contentTypeDao;
        }

        public void DeleteContentType(string contentTypeId)
        {
            var contentType = _contentTypeDao.GetById(contentTypeId);

            if (_contentItemDao.GetAll().Any(o => o.ContentTypeId == contentTypeId))
            {
                throw new InvalidOperationException(string.Format("Cannot delete Content Type: {0} because is being referenced by another content.", contentType.Title));
            }

            _contentTypeDao.Delete(contentType);
        }

        public ContentType GetById(string id)
        {
            return _contentTypeDao.GetById(id);
        }
    }
}