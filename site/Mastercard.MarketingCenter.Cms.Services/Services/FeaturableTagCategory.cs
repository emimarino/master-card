﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class FeaturableTagCategory
    {
        public string Title
        {
            get;
            set;
        }

        public string Featurable
        {
            get;
            set;
        }

        public bool Hierarchical
        {
            get;
            set;
        }

        public List<FeatureTagItem> FeatureTagItems { get; set; }
    }
}