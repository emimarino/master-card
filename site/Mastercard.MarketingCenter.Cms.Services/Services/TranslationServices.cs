﻿using Mastercard.MarketingCenter.Cms.Services.Core;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class TranslationServices
    {
        private readonly ICmsDbConnection _connection;
        private readonly TagTranslatedContentDao _tagTranslatedContentDao;
        private readonly SegmentationTranslatedContentDao _segmentationTranslatedContentDao;
        private readonly FieldDefinitionDao _fieldDefinitionDao;

        public TranslationServices(ICmsDbConnection connection, TagTranslatedContentDao tagTranslatedContentDao, SegmentationTranslatedContentDao segmentationTranslatedContentDao, FieldDefinitionDao fieldDefinitionDao)
        {
            _connection = connection;
            _tagTranslatedContentDao = tagTranslatedContentDao;
            _segmentationTranslatedContentDao = segmentationTranslatedContentDao;
            _fieldDefinitionDao = fieldDefinitionDao;
        }

        public IEnumerable<IDictionary<string, object>> GetTranslatedContentForItemId(string idFieldName, string itemId, string tableName)
        {
            var sql = $"SELECT * FROM {tableName} WHERE {idFieldName} = @itemId";

            return _connection.Query(sql, new { itemId }).Select(t => t as IDictionary<string, object>);
        }

        public IDictionary<string, object> GetTranslatedContentForItemIdAndLanguage(string idFieldName, string itemId, string languageCode, string tableName)
        {
            var sql = $"SELECT * FROM {tableName} WHERE {idFieldName} = @itemId AND LanguageCode = @languageCode";

            return _connection.Query(sql, new { itemId, languageCode }).FirstOrDefault() as IDictionary<string, object>;
        }

        public IEnumerable<TagTranslatedContent> GetTagTranslatedContentForTagId(string tagId)
        {
            return _tagTranslatedContentDao.GetAll().Where(t => t.TagId == tagId).ToList();
        }

        public TagTranslatedContent GetTagTranslatedContentForLanguageAndTagId(string tagId, string languageCode)
        {
            return _tagTranslatedContentDao.GetAll().FirstOrDefault(t => t.TagId == tagId && t.LanguageCode == languageCode);
        }

        public IEnumerable<SegmentationTranslatedContent> GetSegmentationTranslatedContentForSegmentationId(string segmentationId)
        {
            return _segmentationTranslatedContentDao.GetAll().Where(s => s.SegmentationId == segmentationId).ToList();
        }

        public SegmentationTranslatedContent GetSegmentationTranslatedContentForLanguageAndSegmentationId(string segmentationId, string languageCode)
        {
            return _segmentationTranslatedContentDao.GetAll().FirstOrDefault(s => s.SegmentationId == segmentationId && s.LanguageCode == languageCode);
        }

        public IEnumerable<FieldDefinition> GetTranslatedTagFieldDefinitions(int[] fieldDefinitionIds)
        {
            return _fieldDefinitionDao.GetAll().Where(f => fieldDefinitionIds.Contains(f.FieldDefinitionId)).ToList();
        }

        public IEnumerable<TranslationsLanguage> GetAvailableLanguagesForRegion(string regionId)
        {
            var regionConfig = XDocument.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, string.Format("Region.{0}.config", regionId)));
            return regionConfig.Descendants("language").Select(l => new TranslationsLanguage
            {
                Code = l.Attribute("code").Value,
                Name = l.Attribute("name").Value
            });
        }

        public void PopulateTranslationsValues(IDictionary<string, object> contentData, IEnumerable<FieldDefinition> fieldDefinitions, string itemId)
        {
            var idField = fieldDefinitions.First(o => o.FieldType.IsIdentifierField());

            foreach (var fieldDefinition in fieldDefinitions.Where(o => o.FieldTypeCode == FieldType.Translations))
            {
                if (!string.IsNullOrEmpty(fieldDefinition.CustomSettings))
                {
                    var translationsSettings = fieldDefinition.GetSettings<TranslationsSettings>();
                    contentData[fieldDefinition.Name] = JsonConvert.SerializeObject(
                                GetTranslatedContentForItemId(idField.Name, itemId, translationsSettings.TranslatedContentTableName)
                                .ToList()
                                .ToDictionary(k => k["LanguageCode"].ToString(),
                                                v => v.Where(f => GetTranslatedTagFieldDefinitions(translationsSettings.FieldDefinitionIds)
                                                                .Select(fd => fd.Name).Contains(f.Key) || f.Key == "LanguageCode")
                                                    .ToDictionary(kk => kk.Key,
                                                                    vv => vv.Value)));
                }
            }
        }
    }
}