﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Services.Services.Issuers
{
    public class MatchingServices
    {
        private readonly IIssuerService _issuerService;
        private readonly IImportedIcaRepository _importedIcaRepository;
        private readonly ICmsDbConnection _connection;

        public MatchingServices(ICmsDbConnection connection, IIssuerService issuerService, IImportedIcaRepository importedIcaRepository)
        {
            _connection = connection;
            _issuerService = issuerService;
            _importedIcaRepository = importedIcaRepository;
        }

        public void RunAutoMatching()
        {
            var issuers = GetActiveIssuers();
            var importedIssuers = _importedIcaRepository.GetAll();

            var notMatchedIssuers = issuers.Where(o => string.IsNullOrEmpty(o.Cid));

            var matchDate = DateTime.Now;

            foreach (var notMatchedIssuer in notMatchedIssuers)
            {
                var importedIssuerByName = importedIssuers.FirstOrDefault(o => o.LegalName == notMatchedIssuer.Title);

                if (importedIssuerByName != null)
                {
                    UpdateIssuerAsUnconfirmed(importedIssuerByName.Cid, matchDate, notMatchedIssuer.IssuerId);
                }
            }
        }

        public void MatchIssuer(string issuerId, string cid, User currentUser)
        {
            var sourceIssuer = _importedIcaRepository.GetByCid(cid);
            var issuerToMatch = _issuerService.GetIssuerById(issuerId);

            issuerToMatch.Cid = sourceIssuer.Cid;
            issuerToMatch.MatchDate = DateTime.Now;
            issuerToMatch.MatchConfirmed = true;
            issuerToMatch.MatchedByUserId = currentUser.UserId;
        }

        public void ClearMatch(string issuerId)
        {
            var issuer = _issuerService.GetIssuerById(issuerId);
            issuer.Cid = null;
            issuer.MatchDate = null;
            issuer.MatchConfirmed = null;
            issuer.MatchedBy = null;
            issuer.UnconfirmedCid = null;
        }

        public IEnumerable<IssuerData> GetActiveIssuers()
        {
            return _connection.Query<IssuerData>("RetrieveActiveIssuers", commandType: CommandType.StoredProcedure)
                .GroupBy(i => new
                {
                    i.IssuerId,
                    i.BillingId,
                    i.MatchConfirmed,
                    i.Title,
                    i.Processor,
                    i.UserCount,
                    i.ModifiedData,
                    i.DateCreated,
                    i.LastLoginDate,
                    i.Cid,
                    i.HeatmapData
                })
                .Select(i => new IssuerData
                {
                    IssuerId = i.Key.IssuerId,
                    BillingId = i.Key.BillingId,
                    MatchConfirmed = i.Key.MatchConfirmed,
                    Title = i.Key.Title,
                    Processor = i.Key.Processor,
                    UserCount = i.Key.UserCount,
                    ModifiedData = i.Key.ModifiedData,
                    DateCreated = i.Key.DateCreated,
                    LastLoginDate = i.Key.LastLoginDate,
                    Cid = i.Key.Cid,
                    HeatmapData = i.Key.HeatmapData,
                    AudienceSegments = string.Join(", ", i.Select(s => s.AudienceSegments))
                });
        }

        private void UpdateIssuerAsUnconfirmed(string unconfirmedCid, DateTime matchDate, string issuerId)
        {
            _connection.Execute(
                "UPDATE Issuer SET UnconfirmedCid = @unconfirmedCid, MatchConfirmed = 0, MatchDate = @MatchDate, MatchedByUserId = 1917 WHERE IssuerId = @IssuerId",
                new
                {
                    unconfirmedCid,
                    MatchDate = matchDate,
                    IssuerId = issuerId
                });
        }
    }
}