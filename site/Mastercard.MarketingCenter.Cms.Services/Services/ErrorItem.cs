﻿namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class ErrorItem
    {
        public string Key { get; set; }
        public string ErrorMessage { get; set; }
    }
}