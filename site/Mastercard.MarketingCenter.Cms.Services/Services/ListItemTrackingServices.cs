﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Services.Services
{
    public class ListItemTrackingServices
    {
        private readonly ICmsDbConnection _connection;
        private readonly ListDao _listDao;

        public ListItemTrackingServices(ICmsDbConnection connection, ListDao listDao)
        {
            _connection = connection;
            _listDao = listDao;
        }

        public ListItemTrackingFieldInfo GetTrackingFieldsInfo(string id, int listId)
        {
            var list = _listDao.GetById(listId);
            var trackingFields = list.ListFieldDefinitions.GetTrackingFields().ToList();
            var idFieldName = list.ListFieldDefinitions.First(o => o.FieldDefinition.FieldTypeCode == FieldType.GuidPrimaryKey).FieldDefinition.Name;

            var modifiedBy = trackingFields.FirstOrDefault(o => o.FieldTypeCode == FieldType.TrackingModifiedBy);
            var createdBy = trackingFields.FirstOrDefault(o => o.FieldTypeCode == FieldType.TrackingCreatedBy);
            var selectClauseFields = trackingFields.Select(o => $"Item.{o.Name}").ToList();

            var joinClause = string.Empty;

            if (modifiedBy != null)
            {
                joinClause = $" JOIN [User] ModifiedUser on ModifiedUser.UserId = Item.{modifiedBy.Name}";
                selectClauseFields.Add("ModifiedUser.FullName ModifiedUserName");
            }

            if (createdBy != null)
            {
                joinClause += $" JOIN [{MarketingCenterDbConstants.Tables.User}] CreatedUser on CreatedUser.UserId = Item.{createdBy.Name}";
                selectClauseFields.Add("CreatedUser.FullName CreatedUserName");
            }

            var sql = $"SELECT {string.Join(",", selectClauseFields)} FROM {list.TableName} Item {joinClause} WHERE Item.{idFieldName} = @id";

            IDictionary<string, object> result = _connection.Query(sql, new { id }).FirstOrDefault();

            var trackingInfo = new ListItemTrackingFieldInfo();

            if (result == null)
            {
                return trackingInfo;
            }

            foreach (var trackingField in trackingFields)
            {
                switch (trackingField.FieldTypeCode)
                {
                    case FieldType.TrackingCreatedDate:
                        trackingInfo.CreatedDate = Convert.ToDateTime(result[trackingField.Name]);
                        break;

                    case FieldType.TrackingCreatedBy:
                        trackingInfo.CreatedBy = Convert.ToString(result["CreatedUserName"]);
                        break;

                    case FieldType.TrackingModifiedBy:
                        trackingInfo.ModifiedBy = Convert.ToString(result["ModifiedUserName"]);
                        break;

                    case FieldType.TrackingUpdatedDate:
                        trackingInfo.ModifiedDate = (DateTime?)(result[trackingField.Name]);
                        break;
                }
            }

            return trackingInfo;
        }
    }

    public class ListItemTrackingFieldInfo
    {
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}