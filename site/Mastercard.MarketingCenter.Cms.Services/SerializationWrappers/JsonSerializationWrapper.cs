﻿using Newtonsoft.Json;

namespace Mastercard.MarketingCenter.Cms.Services.SerializationWrappers
{
    public class JsonSerializationWrapper : ISerializationProvider
    {
        public string Serialize<T>(T o)
        {
            return JsonConvert.SerializeObject(o);
        }

        public T Deserialize<T>(string o)
        {
            return JsonConvert.DeserializeObject<T>(o);
        }
    }
}