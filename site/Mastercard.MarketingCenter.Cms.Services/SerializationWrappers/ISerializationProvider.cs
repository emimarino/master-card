﻿namespace Mastercard.MarketingCenter.Cms.Services.SerializationWrappers
{
    interface ISerializationProvider
    {
        string Serialize<T>(T o);
        T Deserialize<T>(string o);
    }
}