﻿using Dapper;
using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using static Dapper.SqlMapper;

namespace Mastercard.MarketingCenter.Cms.Services.Infrastructure
{
    public class CmsDbConnection : ICmsDbConnection
    {
        private readonly IDbConnection _connection;
        private readonly ISettingsService _settingsService;
        private IDbTransaction _transaction;
        private IDbConnection _failoverConnection;
        private int _nestedTransactions;
        private int _failoverAttempts;
        private int _failoverRetries;
        private int _failoverDelay;

        public CmsDbConnection(IDbConnection dbConnection, ISettingsService settingsService)
        {
            _connection = dbConnection;
            _settingsService = settingsService;
            _transaction = null;
            _failoverConnection = null;
            _nestedTransactions = 0;
            _failoverAttempts = 0;
            _failoverRetries = 0;
            _failoverDelay = 0;
        }

        #region Properties

        public IDbConnection Connection
        {
            get { return _failoverConnection ?? _connection; }
        }

        public string ConnectionString
        {
            get { return Connection.ConnectionString; }
            set { Connection.ConnectionString = value; }
        }

        public int ConnectionTimeout
        {
            get { return Connection.ConnectionTimeout; }
        }

        public string Database
        {
            get { return Connection.Database; }
        }

        public ConnectionState State
        {
            get { return Connection.State; }
        }

        #endregion

        #region TransactionMethods

        public IDbTransaction BeginTransaction()
        {
            if (_transaction == null)
            {
                _transaction = Connection.BeginTransaction();
            }
            else
            {
                _nestedTransactions++;
            }

            return _transaction;
        }

        public IDbTransaction BeginTransaction(IsolationLevel il)
        {
            if (_transaction == null)
            {
                _transaction = Connection.BeginTransaction(il);
            }
            else
            {
                _nestedTransactions++;
            }

            return _transaction;
        }

        private void EndTransaction()
        {
            if (_transaction != null && _nestedTransactions == 0)
            {
                _transaction.Commit();
                _transaction = null;
            }
            else
            {
                _nestedTransactions--;
            }
        }

        private bool RollbackTransaction()
        {
            if (_transaction != null && _nestedTransactions == 0)
            {
                if (_transaction.Connection != null)
                {
                    _transaction.Rollback();
                }
                _transaction = null;
                return true;
            }
            else
            {
                _nestedTransactions--;
                return false;
            }
        }

        public void ExecuteInTransaction(Action action)
        {
            try
            {
                ArgumentNotNull(action, "action");

                if (Connection.State == ConnectionState.Closed)
                {
                    Connection.Open();
                }

                BeginTransaction();
                action();
                EndTransaction();
            }
            catch (Exception ex)
            {
                if (RollbackTransaction() && _failoverConnection == null)
                {
                    LogManager.EventFactory.Create()
                                           .Text($"Transaction Exception in Admin - Using Principal Server")
                                           .Level(LoggingEventLevel.Warning)
                                           .AddTags("failover server")
                                           .AddData("Exception Message", ex.Message)
                                           .AddData("Exception InnerException", ex.InnerException)
                                           .AddData("Exception Data", ex.Data)
                                           .AddData("Exception StackTrace", ex.StackTrace)
                                           .AddData("Action Method", action.Method.ToString())
                                           .AddData("Action Method Name", action.Method.Name)
                                           .AddData("Action Method Parameters", string.Join(",", action.Method.GetParameters().Select(p => p.Name)))
                                           .AddData("Action Method ReflectedType FullName", action.Method.ReflectedType.FullName)
                                           .Push();

                    _failoverAttempts = 0;
                    _failoverRetries = _settingsService.GetFailoverConnectionRetries();
                    _failoverDelay = _settingsService.GetFailoverConnectionDelay();
                    do
                    {
                        try
                        {
                            _failoverAttempts++;
                            LogManager.EventFactory.Create()
                                                   .Text($"Starting attempt {_failoverAttempts} - Using Failover Server")
                                                   .Level(LoggingEventLevel.Information)
                                                   .AddTags("failover server")
                                                   .AddData("Action Method", action.Method.ToString())
                                                   .AddData("Action Method Name", action.Method.Name)
                                                   .AddData("Action Method Parameters", string.Join(",", action.Method.GetParameters().Select(p => p.Name)))
                                                   .AddData("Action Method ReflectedType FullName", action.Method.ReflectedType.FullName)
                                                   .Push();

                            using (_failoverConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["MasterCardMarketingCenterFailover"].ConnectionString))
                            {
                                _failoverConnection.Open();
                                ExecuteInTransaction(action);
                                _failoverConnection.Close();
                            }

                            LogManager.EventFactory.Create()
                                                   .Text($"Transaction Finished Succesfully in attempt {_failoverAttempts} - Using Failover Server")
                                                   .Level(LoggingEventLevel.Warning)
                                                   .AddTags("failover server")
                                                   .AddData("Action Method", action.Method.ToString())
                                                   .AddData("Action Method Name", action.Method.Name)
                                                   .AddData("Action Method Parameters", string.Join(",", action.Method.GetParameters().Select(p => p.Name)))
                                                   .AddData("Action Method ReflectedType FullName", action.Method.ReflectedType.FullName)
                                                   .Push();
                            break;
                        }
                        catch (Exception exx)
                        {
                            if (_failoverAttempts == _failoverRetries)
                            {
                                LogManager.EventFactory.Create()
                                                       .Text($"Transaction Exception in Admin - Using Failover Server")
                                                       .Level(LoggingEventLevel.Error)
                                                       .AddTags("failover server")
                                                       .AddData("Exception Message", exx.Message)
                                                       .AddData("Exception InnerException", exx.InnerException)
                                                       .AddData("Exception Data", exx.Data)
                                                       .AddData("Exception StackTrace", exx.StackTrace)
                                                       .AddData("Action Method", action.Method.ToString())
                                                       .AddData("Action Method Name", action.Method.Name)
                                                       .AddData("Action Method Parameters", string.Join(",", action.Method.GetParameters().Select(p => p.Name)))
                                                       .AddData("Action Method ReflectedType FullName", action.Method.ReflectedType.FullName)
                                                       .Push();

                                throw exx;
                            }

                            LogManager.EventFactory.Create()
                                                   .Text($"Transaction Exception in attempt {_failoverAttempts} - Using Failover Server")
                                                   .Level(LoggingEventLevel.Information)
                                                   .AddTags("failover server")
                                                   .AddData("Exception Message", exx.Message)
                                                   .AddData("Exception InnerException", exx.InnerException)
                                                   .AddData("Exception Data", exx.Data)
                                                   .AddData("Exception StackTrace", exx.StackTrace)
                                                   .AddData("Action Method", action.Method.ToString())
                                                   .AddData("Action Method Name", action.Method.Name)
                                                   .AddData("Action Method Parameters", string.Join(",", action.Method.GetParameters().Select(p => p.Name)))
                                                   .AddData("Action Method ReflectedType FullName", action.Method.ReflectedType.FullName)
                                                   .Push();

                            Thread.Sleep(_failoverAttempts * _failoverDelay);
                        }
                        finally
                        {
                            _failoverConnection.Dispose();
                            _failoverConnection = null;
                        }
                    }
                    while (true);
                }
                else
                {
                    throw ex;
                }
            }
        }

        private void ArgumentNotNull(object value, string parameterName, string message = "")
        {
            if (value == null)
            {
                throw new ArgumentNullException(parameterName, message);
            }
        }

        #endregion

        #region IDbConnectionMethods

        public void ChangeDatabase(string databaseName)
        {
            Connection.ChangeDatabase(databaseName);
        }

        public void Close()
        {
            Connection.Close();
        }

        public IDbCommand CreateCommand()
        {
            return Connection.CreateCommand();
        }

        public void Dispose()
        {
            Connection.Dispose();
        }

        public void Open()
        {
            Connection.Open();
        }

        #endregion

        #region Dapper

        public int Execute(string sql, object param = null, int? commandTimeout = default(int?), CommandType? commandType = default(CommandType?))
        {
            return Connection.Execute(sql, param, _transaction, commandTimeout, commandType);
        }

        public object ExecuteScalar(string sql, object param = null, int? commandTimeout = default(int?), CommandType? commandType = default(CommandType?))
        {
            return Connection.ExecuteScalar(sql, param, _transaction, commandTimeout, commandType);
        }

        public T ExecuteScalar<T>(string sql, object param = null, int? commandTimeout = default(int?), CommandType? commandType = default(CommandType?))
        {
            return Connection.ExecuteScalar<T>(sql, param, _transaction, commandTimeout, commandType);
        }

        public IEnumerable<dynamic> Query(string sql, object param = null, bool buffered = true, int? commandTimeout = default(int?), CommandType? commandType = default(CommandType?))
        {
            return Connection.Query(sql, param, _transaction, buffered, commandTimeout, commandType);
        }

        public IEnumerable<T> Query<T>(string sql, object param = null, bool buffered = true, int? commandTimeout = default(int?), CommandType? commandType = default(CommandType?))
        {
            return Connection.Query<T>(sql, param, _transaction, buffered, commandTimeout, commandType);
        }

        public GridReader QueryMultiple(string sql, object param = null, int? commandTimeout = default(int?), CommandType? commandType = default(CommandType?))
        {
            return Connection.QueryMultiple(sql, param, _transaction, commandTimeout, commandType);
        }

        public DynamicParameters GetDynamicParameters()
        {
            return new DynamicParameters();
        }

        public DynamicParameters GetDynamicParameters(object template)
        {
            return new DynamicParameters(template);
        }

        #endregion
    }
}