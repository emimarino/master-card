﻿namespace Mastercard.MarketingCenter.Cms.Services.Infrastructure
{
    public enum ContentActions
    {
        SaveAsDraft,
        SaveLive,
        Restore,
        SaveAndPreview,
        SaveCrossBorder
    }
}