﻿namespace Mastercard.MarketingCenter.Cms.Services.Infrastructure
{
    public enum BusinessOwnerActions
    {
        Extend,
        NotExtend,
        Create,
        Update
    }
}