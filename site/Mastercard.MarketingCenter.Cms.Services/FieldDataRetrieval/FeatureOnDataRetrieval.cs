﻿using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Services.FieldDataRetrieval
{
    public class FeatureOnDataRetrieval : IFieldDataRetrieval
    {
        private readonly ITagService _tagService;
        private readonly FeatureLocationDao _featureLocationDao;

        public FeatureOnDataRetrieval(ITagService tagService, FeatureLocationDao featureLocationDao)
        {
            _tagService = tagService;
            _featureLocationDao = featureLocationDao;
        }

        public string GetSerializedEntity(KeyValuePair<string, object> originalData, FieldDefinition fieldDefinition, Func<object, string> serializeMethod)
        {
            if (originalData.Value != null && (originalData.Value is IEnumerable<object>))
            {
                var tagsIds = ((IEnumerable<dynamic>)originalData.Value).Where(od => od.Type == FeatureType.Tag).Select(od => (string)od.Id).ToArray();
                var featureLocationIds = ((IEnumerable<dynamic>)originalData.Value).Where(od => od.Type == FeatureType.FeatureLocation).Select(od => (string)od.Id).ToArray();
                if ((tagsIds != null && tagsIds.Any()) || (featureLocationIds != null && featureLocationIds.Any()))
                {
                    var featureLocationValues = featureLocationIds != null ? _featureLocationDao.GetAllById(featureLocationIds).Select(fl => new { fl.Title, fl.FeatureLocationId }).ToList() : null;
                    var tagValues = tagsIds != null ? _tagService.GetTags(tagsIds) : null;
                    return serializeMethod(new { tags = tagValues, featureLocation = featureLocationValues });
                }
            }
            return Convert.ToString(originalData.Value);
        }

        public bool ValidateFieldType(FieldDefinition fieldDefinition)
        {
            return FieldType.FeatureOn.Equals(fieldDefinition.FieldType.FieldTypeCode);
        }
    }
}