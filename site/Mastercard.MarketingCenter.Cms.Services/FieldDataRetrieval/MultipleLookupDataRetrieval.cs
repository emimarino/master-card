﻿using Mastercard.MarketingCenter.Cms.Services.Core;
using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Services.FieldDataRetrieval
{
    public class MultipleLookupDataRetrieval : IFieldDataRetrieval
    {
        private readonly LookupServices _lookupServices;
        public MultipleLookupDataRetrieval(LookupServices lookupServices)
        {
            _lookupServices = lookupServices;
        }

        public string GetSerializedEntity(KeyValuePair<string, object> originalData, FieldDefinition fieldDefinition, Func<object, string> serializeMethod)
        {
            if (originalData.Value != null && (originalData.Value is string) || (originalData.Value is int))
            {
                var values = originalData.Value.ToString().Split(',').Select(v => v.Trim());
                var lookupKeyValue = _lookupServices.GetLookupValues(fieldDefinition.GetSettings<LookupSettings>()).Where(mlv => values.Contains(mlv.Id));
                if (lookupKeyValue != null && lookupKeyValue.Any())
                {
                    return Convert.ToString(serializeMethod(lookupKeyValue));
                }
            }

            return Convert.ToString(originalData.Value);
        }

        public bool ValidateFieldType(FieldDefinition fieldDefinition)
        {
            return FieldType.MultipleLookup.Equals(fieldDefinition.FieldType.FieldTypeCode);
        }
    }
}