﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Services.FieldDataRetrieval
{
    public class TagDataRetrieval : IFieldDataRetrieval
    {
        public ITagService _tagService;
        public TagDataRetrieval(ITagService tagService)
        {
            _tagService = tagService;
        }

        public string GetSerializedEntity(KeyValuePair<string, object> originalData, FieldDefinition fieldDefinition, Func<object, string> serializeMethod)
        {
            if (originalData.Value != null && (originalData.Value is IEnumerable<string> || originalData.Value is string) || (originalData.Value is int))
            {
                var tagValues = _tagService.GetTagsByIdentifier(originalData.Value?.ToString()?.Split(',')).Select(x => x.Identifier);
                if (tagValues != null && tagValues.Any())
                {
                    return serializeMethod(tagValues);
                }
            }

            return Convert.ToString(originalData.Value);
        }

        public bool ValidateFieldType(FieldDefinition fieldDefinition)
        {
            return FieldType.Tag.Equals(fieldDefinition.FieldType.FieldTypeCode);
        }
    }
}