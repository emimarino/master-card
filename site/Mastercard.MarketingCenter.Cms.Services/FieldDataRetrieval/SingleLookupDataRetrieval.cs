﻿using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Services.FieldDataRetrieval
{
    public class SingleLookupDataRetrieval : IFieldDataRetrieval
    {
        public LookupServices _lookupServices;
        public SingleLookupDataRetrieval(LookupServices lookupServices)
        {
            _lookupServices = lookupServices;
        }

        public string GetSerializedEntity(KeyValuePair<string, object> originalData, FieldDefinition fieldDefinition, Func<object, string> serializeMethod)
        {
            if (originalData.Value != null && (originalData.Value is string) || (originalData.Value is int))
            {
                var lookupKeyValue = _lookupServices.GetLookupValues(fieldDefinition.GetSettings<LookupSettings>()).FirstOrDefault(kv => kv.Id.Equals(originalData.Value));
                if (lookupKeyValue != null && !string.IsNullOrEmpty(lookupKeyValue.Value))
                {
                    return serializeMethod(lookupKeyValue);
                }
            }

            return Convert.ToString(originalData.Value);
        }

        public bool ValidateFieldType(FieldDefinition fieldDefinition)
        {
            return FieldType.SingleLookup.Equals(fieldDefinition.FieldType.FieldTypeCode);
        }
    }
}