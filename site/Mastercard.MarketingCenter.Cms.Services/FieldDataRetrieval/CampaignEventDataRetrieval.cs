﻿using Mastercard.MarketingCenter.Cms.Services.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Cms.Services.FieldDataRetrieval
{
    public class CampaignEventDataRetrieval : IFieldDataRetrieval
    {
        private readonly LookupService _lookupServices;
        private readonly ListDao _listDao;
        private readonly ContentService _contentService;
        private readonly IFieldDataNormalizationContext _fieldDataNormalizationContext;

        public CampaignEventDataRetrieval(LookupService lookupServices, ContentService contentService, ListDao listDao, IFieldDataNormalizationContext fieldDataNormalizationContext)
        {
            _lookupServices = lookupServices;
            _contentService = contentService;
            _listDao = listDao;
            _fieldDataNormalizationContext = fieldDataNormalizationContext;
        }

        public string GetSerializedEntity(KeyValuePair<string, object> originalData, FieldDefinition fieldDefinition, Func<object, string> serializeMethod)
        {
            if (originalData.Value != null && (originalData.Value is string))
            {
                var ids = ((string)originalData.Value).Split(',');
                var returnValue = new List<IDictionary<string, object>>();
                foreach (var id in ids)
                {
                    var itemData = _contentService.GetListItemData(MarketingCenterDbConstants.ListIds.CampaignEventID, id);
                    var itemFields = _listDao.GetById(MarketingCenterDbConstants.ListIds.CampaignEventID).ListFieldDefinitions.Select(lf => lf.FieldDefinition);
                    if (itemData != null && itemFields != null && itemData.Any())
                    {
                        var dataNormalizationContext = _fieldDataNormalizationContext;
                        var normalizedData = dataNormalizationContext.Apply(itemData, itemFields);
                        returnValue.Add(normalizedData);
                    }
                }
                if (returnValue.Any())
                {
                    return serializeMethod(returnValue);
                }
            }

            return Convert.ToString(originalData.Value);
        }

        public bool ValidateFieldType(FieldDefinition fieldDefinition)
        {
            return FieldType.CampaignEvents.Equals(fieldDefinition.FieldType.FieldTypeCode, StringComparison.OrdinalIgnoreCase);
        }
    }
}