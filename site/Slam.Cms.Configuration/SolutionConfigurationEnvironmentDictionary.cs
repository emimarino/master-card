﻿using System.Collections.Generic;

namespace Slam.Cms.Configuration
{
    public class SolutionConfigurationEnvironmentDictionary : Dictionary<string, SolutionConfigurationEnvironment>
    {
        public SolutionConfigurationEnvironmentDictionary(SolutionConfiguration configuration)
        {
            configuration["Environments.Environment"].ForEach(e =>
                Add(e["Name"],
                new SolutionConfigurationEnvironment(e["Name"], e["AdminUrl"], e["FrontEndUrl"], e["StaticUrl"], e["ApiUrl"], e["SalesforceUrl"], e["OtherUrls"]))
            );
        }
    }

    public class SolutionConfigurationEnvironment
    {
        public string AdminUrl
        {
            get;
            set;
        }

        public string FrontEndUrl
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string OtherUrls
        {
            get;
            set;
        }

        public string StaticUrl
        {
            get;
            set;
        }

        public string ApiUrl
        {
            get;
            set;
        }

        public string SalesforceUrl
        {
            get;
            set;
        }

        public SolutionConfigurationEnvironment(string name, string adminUrl, string frontEndUrl, string staticUrl, string apiUrl, string salesforceUrl, string otherUrls)
        {
            Name = name;
            AdminUrl = adminUrl;
            FrontEndUrl = frontEndUrl;
            OtherUrls = otherUrls;
            StaticUrl = staticUrl;
            ApiUrl = apiUrl;
            SalesforceUrl = salesforceUrl;
        }
    }
}