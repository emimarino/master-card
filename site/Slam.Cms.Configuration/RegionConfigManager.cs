﻿using System.Xml.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Concurrent;

namespace Slam.Cms.Configuration
{
    public class RegionConfigManager
    {
        private static ConcurrentDictionary<KeyValuePair<string, DateTime>, XElement> AvailableRegionNodes;

        private string _region;
        public string SelectedRegion
        {
            get
            {
                return _region.Trim();
            }
        }
        public static string DefaultRegion
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["DefaultRegion"];
            }
        }

        private string _language;
        public string SelectedLanguage
        {
            get
            {
                return _language.Trim();
            }
        }
        public static string DefaultLanguage
        {
            get
            {
                return System.Configuration.ConfigurationManager.AppSettings["DefaultLanguage"];
            }
        }

        public static string DefaultSegmentation
        {
            get
            {
                return "all";
            }
        }

        public static string NoneSegmentation
        {
            get
            {
                return "none";
            }
        }

        private XElement _sitemapRoot;
        public static Dictionary<string, string> AvailableRegionSitemaps
        {
            get; private set;
        }
        public static Dictionary<string, string> AvailableLanguages
        {
            get; private set;
        }

        private static string GetExecutionFolder()
        {
            try
            {
                return System.Web.HttpRuntime.AppDomainAppPath;
            }
            catch (Exception)
            {
                return AppDomain.CurrentDomain.BaseDirectory;
            }
        }

        static RegionConfigManager()
        {
            AvailableRegionNodes = new ConcurrentDictionary<KeyValuePair<string, DateTime>, XElement>();
            AvailableRegionSitemaps = new Dictionary<string, string>();
            AvailableLanguages = new Dictionary<string, string>();
            var regionFiles = System.IO.Directory.GetFiles(GetExecutionFolder()).Where(f => f.Contains("Region.") && f.EndsWith(".config"));
            foreach (var regionFile in regionFiles)
            {
                var region = regionFile.Substring((regionFile.LastIndexOf("Region.") + "Region.".Length), (regionFile.IndexOf(".config") - "Region".Length - regionFile.LastIndexOf("Region.") - 1)).Trim();
                if (!string.IsNullOrEmpty(region) && !(AvailableRegionSitemaps.Contains(new KeyValuePair<string, string>(region, regionFile))))
                {
                    AvailableRegionSitemaps.Add(region, regionFile);
                    var document = XDocument.Load(regionFile);
                    foreach (var language in GetLanguages(document.Root))
                    {
                        if (!AvailableLanguages.ContainsKey(language.Key))
                        {
                            AvailableLanguages.Add(language.Key, language.Value);
                        }
                    }
                }
            }
        }

        public RegionConfigManager(string region, string language)
        {
            _language = language.Trim();
            _region = region.Trim();
        }

        public RegionConfigManager()
        {
        }

        private XElement GetRootNode(string __region, bool loadInClass = true)
        {
            if (__region == null)
            {
                return null;
            }

            _region = __region.Trim();
            _region = AvailableRegionSitemaps.ContainsKey(_region) ? _region : (AvailableRegionSitemaps.ContainsKey(_region.ToLower()) ? _region.ToLower() : DefaultRegion);

            if (string.IsNullOrEmpty(_region))
            {
                throw new ArgumentNullException("Region not Found");
            }

            if (!System.IO.File.Exists(AvailableRegionSitemaps[_region]))
            {
                throw new ArgumentNullException("Region Config File not Found");
            }

            XElement returnElement = null;
            if (AvailableRegionNodes.Any(n => n.Key.Key.Equals(_region, StringComparison.OrdinalIgnoreCase)))
            {
                var tempNode = new KeyValuePair<KeyValuePair<string, DateTime>, XElement>();

                if (IsExpired(AvailableRegionNodes, out tempNode, _region))
                {
                    XElement removed = null;
                    AvailableRegionNodes.TryRemove(AvailableRegionNodes.First(n => n.Key.Key.Equals(_region, StringComparison.OrdinalIgnoreCase)).Key, out removed);
                    AvailableRegionNodes.TryAdd(new KeyValuePair<string, DateTime>(_region, DateTime.Now), XElement.Load(AvailableRegionSitemaps[_region]));
                    returnElement = AvailableRegionNodes.First(n => n.Key.Key.Equals(_region, StringComparison.OrdinalIgnoreCase)).Value;
                }
                else
                {
                    returnElement = tempNode.Value;
                }
            }
            else
            {
                AvailableRegionNodes.TryAdd(new KeyValuePair<string, DateTime>(_region, DateTime.Now), XElement.Load(AvailableRegionSitemaps[_region]));
                returnElement = AvailableRegionNodes.First(n => n.Key.Key.Equals(_region, StringComparison.OrdinalIgnoreCase)).Value;
            }

            if (loadInClass)
            {
                _sitemapRoot = returnElement;
            }

            return returnElement;
        }

        private bool IsExpired<T>(ConcurrentDictionary<KeyValuePair<string, DateTime>, T> cache, out KeyValuePair<KeyValuePair<string, DateTime>, T> returnObject, string region = "")
        {
            if (string.IsNullOrEmpty(region))
            {
                region = SelectedRegion ?? DefaultRegion;
            }

            var temp = cache.FirstOrDefault(co => co.Key.Key.ToLower() == region.ToLower());
            var result = temp.Key.Value < System.IO.File.GetLastWriteTime(AvailableRegionSitemaps[_region]);
            returnObject = (result ? (new KeyValuePair<KeyValuePair<string, DateTime>, T>()) : temp);

            return result;
        }

        public string GetSetting(string key, string _region = null, bool canDefault = false)
        {
            string region = _region?.Trim();
            Func<XElement, string, XElement> getElementgetsettingValue = (setNode, keyValue) => { return setNode?.Elements()?.FirstOrDefault(n => n.Attribute("name")?.Value.Equals(keyValue, StringComparison.OrdinalIgnoreCase) ?? false); };
            Func<XElement, string> getValue = (element) => { return element?.Attribute("value")?.Value ?? ""; };
            XElement setting = null;
            if (region != null)
            {
                setting = getElementgetsettingValue(GetRootNode(region).Element("settings"), key);
            }

            var final = (setting == null && canDefault) ?
                            getValue(getElementgetsettingValue(GetRootNode(DefaultRegion).Element("settings"), key)) :
                            (canDefault && (string.IsNullOrEmpty(getValue(setting))) ?
                                getValue(getElementgetsettingValue(GetRootNode(DefaultRegion).Element("settings"), key)) :
                                getValue(setting));
            return final;
        }

        public string TermsOfUse { get { return GetSetting("TermsOfUse"); } }
        public string PrivacyNotice { get { return GetSetting("PrivacyNotice"); } }

        public HashSet<string> GetUrls(string _reg)
        {
            string reg = _reg?.Trim();
            var set = new HashSet<string>();
            var fileText = System.IO.File.ReadAllText(AvailableRegionSitemaps[reg]);

            var regex = new Regex("url\\s*=\\s*\"([^\"]*)\"");
            var matches = regex.Matches(fileText);

            foreach (Match match in matches)
            {
                set.Add(match.Groups[1].Value);
            }

            return set;
        }

        private static IDictionary<string, string> GetLanguages(XElement document)
        {
            Dictionary<string, string> Languages = new Dictionary<string, string>();

            foreach (var language in document.Element("languages").Elements())
            {
                Languages.Add(language.Attribute("code").Value.ToString(), language.Attribute("name").Value.ToString());
            }

            return Languages;
        }

        public IDictionary<string, string> GetLanguages(string _region)
        {
            string region = _region.Trim();
            return GetLanguages(GetRootNode(region));
        }
    }
}