﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Slam.Cms.Configuration
{
    public class EnvironmentConfiguration : Configuration
    {
        private EnvironmentConfigurationConnectionStringDictionary _connectionStrings;
        private ConfigurationSettingDictionary _settings;
        private ConfigurationApplicationDictionary _applications;
        private SolutionConfigurationEnvironment _environment;

        public string AdminUrl
        {
            get
            {
                return _environment.AdminUrl;
            }
        }

        public ConfigurationApplicationDictionary Applications
        {
            get
            {
                return _applications;
            }
        }

        public EnvironmentConfigurationConnectionStringDictionary ConnectionStrings
        {
            get
            {
                return _connectionStrings;
            }
        }

        public string FrontEndUrl
        {
            get
            {
                return _environment.FrontEndUrl;
            }
        }

        public string Name
        {
            get
            {
                return _environment.Name;
            }
        }

        public ConfigurationSettingDictionary Settings
        {
            get
            {
                return _settings;
            }
        }

        public string StaticUrl
        {
            get
            {
                return _environment.StaticUrl;
            }
        }

        public string ApiUrl
        {
            get
            {
                return _environment.ApiUrl;
            }
        }

        public string SalesforceUrl
        {
            get
            {
                return _environment.SalesforceUrl;
            }
        }

        public EnvironmentConfiguration() : base()
        {
            if (HttpContext.Current != null)
            {
                string applicationPath = "";
                if (!string.IsNullOrEmpty(HttpContext.Current.Request.ApplicationPath.TrimStart('/')))
                {
                    applicationPath = HttpContext.Current.Request.ApplicationPath.TrimStart('/');
                }
                string currentBaseUrl = $"{HttpContext.Current.Request.Url.Scheme}://{HttpContext.Current.Request.Url.Authority}/{applicationPath}".TrimEnd('/');
                var environments = ConfigurationManager.Solution.Environments
                                                                .Where(e => e.Value.AdminUrl.TrimEnd('/').Equals(currentBaseUrl, StringComparison.OrdinalIgnoreCase) ||
                                                                            e.Value.FrontEndUrl.TrimEnd('/').Equals(currentBaseUrl, StringComparison.OrdinalIgnoreCase) ||
                                                                            e.Value.StaticUrl.TrimEnd('/').Equals(currentBaseUrl, StringComparison.OrdinalIgnoreCase) ||
                                                                            e.Value.ApiUrl.TrimEnd('/').Equals(currentBaseUrl, StringComparison.OrdinalIgnoreCase) ||
                                                                            e.Value.SalesforceUrl.TrimEnd('/').Equals(currentBaseUrl, StringComparison.OrdinalIgnoreCase) ||
                                                                            e.Value.OtherUrls.Split(',').Any(s => s.TrimEnd('/').Equals(currentBaseUrl, StringComparison.OrdinalIgnoreCase)))
                                                                .ToList();
                if (environments.Count > 0)
                {
                    _environment = environments[0].Value;
                }
                else
                {
                    throw new FileNotFoundException($"Environment config not found.  SLAM CMS Configuration expects an Environment to be defined in the Solution.config mapped to the URL {currentBaseUrl}");
                }
            }
            else
            {
                _environment = new SolutionConfigurationEnvironment("", "", "", "", "", "", "");
            }

            if (_environment == null)
            {
                throw new Exception("Could not find current configuration environment. Please check Solution.config.");
            }

            string environmentConfigurationFileName = $"Environment.{(string.IsNullOrEmpty(_environment.Name) ? "" : $"{_environment.Name}.")}config";
            string environmentConfigurationPath = Path.Combine(ConfigurationManager.ConfigurationFolderPath, environmentConfigurationFileName);

            //If the Environment config file is not found, and that file is not "Environment.config", throw a FileNotFoundException
            //If the Environment config file is not found, but that file is "Environment.config", create dummy Environment configuration
            if (!File.Exists(environmentConfigurationPath) && !environmentConfigurationFileName.Equals("Environment.config", StringComparison.OrdinalIgnoreCase))
            {
                throw new FileNotFoundException(environmentConfigurationFileName + " not found.  SLAM CMS Configuration expects an " + environmentConfigurationFileName + " for this application to exist at " + environmentConfigurationPath);
            }
            else if (!File.Exists(environmentConfigurationPath))
            {
                Load(new XDocument(new XElement("EnvironmentConfiguration")));
            }
            else
            {
                Load(XDocument.Load(environmentConfigurationPath));
            }
        }

        protected override void Load(XDocument configurationXml)
        {
            base.Load(configurationXml);
            _applications = new ConfigurationApplicationDictionary(this);
            _connectionStrings = new EnvironmentConfigurationConnectionStringDictionary(this);
            _settings = new ConfigurationSettingDictionary(this);
        }
    }
}