﻿using System.IO;
using System.Xml.Linq;

namespace Slam.Cms.Configuration
{
	public class SolutionConfiguration : Configuration
	{	
		private ConfigurationApplicationDictionary _applications;
		private SolutionConfigurationEnvironmentDictionary _environments;
		private ConfigurationSettingDictionary _settings;

		public ConfigurationApplicationDictionary Applications
		{
			get
			{
				return _applications;
			}
		}

		public SolutionConfigurationEnvironmentDictionary Environments
		{
			get
			{	
				return _environments;
			}
		}

		public ConfigurationSettingDictionary Settings
		{
			get
			{	
				return _settings;
			}
		}

		public SolutionConfiguration() :
			base()
		{
			string solutionConfigPath = Path.Combine(ConfigurationManager.ConfigurationFolderPath, "Solution.config");
			if (!File.Exists(solutionConfigPath))
			{
				throw new FileNotFoundException("Solution.config not found.  SLAM CMS Configuration expects the Solution.config for this application to exist at " + solutionConfigPath);
			}
			
			Load(XDocument.Load(solutionConfigPath));
		}


		protected override void Load(XDocument configurationXml)
		{
			base.Load(configurationXml);
			
			_environments = new SolutionConfigurationEnvironmentDictionary(this);
			_applications = new ConfigurationApplicationDictionary(this);
			_settings = new ConfigurationSettingDictionary(this);
		}
	}
}
