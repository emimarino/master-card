﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Slam.Cms.Configuration
{
	public class ConfigurationSettingDictionary : Dictionary<string, string>
	{
		public ConfigurationSettingDictionary(Configuration configuration)
		{
			configuration["Settings.Setting"].ForEach(s => this.Add(s["Name"], s["Value"]));
		}
	}
}