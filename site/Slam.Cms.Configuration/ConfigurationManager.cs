﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Slam.Cms.Configuration
{
    public class ConfigurationManager
    {
        private static ApplicationConfiguration _applicationConfiguration;
        public static ApplicationConfiguration Application
        {
            get
            {
                if (_applicationConfiguration == null)
                {
                    _applicationConfiguration = new ApplicationConfiguration();
                }

                return _applicationConfiguration;
            }
        }

        public static string ConfigurationFolderPath
        {
            get
            {
                DirectoryInfo configurationDirectory = new DirectoryInfo(SolutionRootFolderPath.TrimEnd('\\') + "\\config\\");
                if (!configurationDirectory.Exists)
                {
                    throw new DirectoryNotFoundException($"Configuration directory not found. SLAM CMS Configuration expects a configuration directory to exist at {configurationDirectory.FullName}");
                }

                return configurationDirectory.FullName;
            }
        }

        private static readonly object _environmentConfigurationLock = new object();
        private static IDictionary<string, EnvironmentConfiguration> _environmentConfigurations;
        public static EnvironmentConfiguration Environment
        {
            get
            {
                lock (_environmentConfigurationLock)
                {
                    if (_environmentConfigurations == null)
                    {
                        _environmentConfigurations = new Dictionary<string, EnvironmentConfiguration>();
                    }

                    EnvironmentConfiguration environmentConfiguration;
                    if (HttpContext.Current != null)
                    {
                        if (!_environmentConfigurations.ContainsKey(HttpContext.Current.Request.Url.Authority))
                        {
                            environmentConfiguration = new EnvironmentConfiguration();
                            _environmentConfigurations.Add(HttpContext.Current.Request.Url.Authority, environmentConfiguration);
                        }
                        else
                        {
                            environmentConfiguration = _environmentConfigurations[HttpContext.Current.Request.Url.Authority];
                        }
                    }
                    else
                    {
                        if (_environmentConfigurations.Any())
                        {
                            environmentConfiguration = _environmentConfigurations.FirstOrDefault().Value;
                        }
                        else
                        {
                            environmentConfiguration = new EnvironmentConfiguration();
                            if (!_environmentConfigurations.ContainsKey("default"))
                            {
                                _environmentConfigurations.Add("default", environmentConfiguration);
                            }
                        }
                    }

                    return environmentConfiguration;
                }
            }
        }

        private static string _solutionRootFolderPath;
        public static string SolutionRootFolderPath
        {
            get
            {
                if (string.IsNullOrEmpty(_solutionRootFolderPath))
                {
                    DirectoryInfo rootDirectory = FindSolutionDirectory(new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory));
                    if (rootDirectory == null)
                    {
                        rootDirectory = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory).Parent;
                    }

                    if (!rootDirectory.Exists)
                    {
                        throw new DirectoryNotFoundException("Solution root directory not found.  SLAM CMS Configuration expects this application's solution directory to exist at " + rootDirectory.FullName);
                    }

                    _solutionRootFolderPath = rootDirectory.FullName;
                }

                return _solutionRootFolderPath;
            }
        }

        private static SolutionConfiguration _solutionConfiguration;
        public static SolutionConfiguration Solution
        {
            get
            {
                if (_solutionConfiguration == null)
                {
                    _solutionConfiguration = new SolutionConfiguration();
                }

                return _solutionConfiguration;
            }
        }

        private static DirectoryInfo FindSolutionDirectory(DirectoryInfo currentDirectory)
        {
            DirectoryInfo solutionDirectory = null;
            currentDirectory = currentDirectory.Parent;
            if (currentDirectory == null)
            {
                throw new Exception("There's no config folder for the solution. Please check that you have the proper config folder and config files for the solution");
            }

            if (currentDirectory.Exists)
            {
                foreach (DirectoryInfo childDirectory in currentDirectory.GetDirectories())
                {
                    if (childDirectory.Name.Equals("config", StringComparison.OrdinalIgnoreCase) && solutionDirectory == null)
                    {
                        solutionDirectory = currentDirectory;
                        break;
                    }
                }

                if (solutionDirectory == null)
                {
                    solutionDirectory = FindSolutionDirectory(currentDirectory);
                }
            }

            return solutionDirectory;
        }
    }
}