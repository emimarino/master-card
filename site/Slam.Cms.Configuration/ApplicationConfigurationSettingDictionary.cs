﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace Slam.Cms.Configuration
{
	public class ApplicationConfigurationSettingDictionary : ConfigurationSettingDictionary
	{	
		public ApplicationConfigurationSettingDictionary(ApplicationConfiguration configuration) :
			base(configuration)
		{
			DirectoryInfo rootDirectory = new DirectoryInfo(Path.Combine(ConfigurationManager.SolutionRootFolderPath, configuration.Folder));
			if (rootDirectory.Exists)
			{
				AddSettingsFromConfigInDirectory(rootDirectory);
				foreach (DirectoryInfo directory in rootDirectory.GetDirectories())
				{
					AddSettingsFromConfigInDirectory(directory);
				}
			}
		}

		private void AddSettingsFromConfigInDirectory(DirectoryInfo directory)
		{
			foreach (FileInfo configFile in directory.GetFiles("*.config", SearchOption.AllDirectories))
			{
				XDocument configXml = XDocument.Load(configFile.FullName);
				var appSettings = from element in configXml.Descendants("appSettings").Elements("add")
								  select element;
				
				foreach (XElement appSetting in appSettings)
				{
					if (!this.ContainsKey(appSetting.Attribute("key").Value))
					{
						this.Add(appSetting.Attribute("key").Value, appSetting.Attribute("value").Value);
					}
				}
			}
		}
	}
}
