﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.Web;

namespace Slam.Cms.Configuration
{
	public class ApplicationConfiguration : Configuration
	{	
		private ApplicationConfigurationSettingDictionary _settings;
		private ConfigurationApplication _application;

		public string Folder
		{
			get
			{
				return _application.Folder;
			}
		}

		public string Name
		{
			get
			{
				return _application.Name;
			}
		}

		public string Url
		{
			get
			{
				return _application.Url;
			}
		}

		public ApplicationConfigurationSettingDictionary Settings
		{
			get
			{	
				return _settings;
			}
		}

		public ApplicationConfiguration()
		{
			var solutionApplications = ConfigurationManager.Solution.Applications.Where(a => AppDomain.CurrentDomain.BaseDirectory.ToUpper().Contains(a.Value.Folder.ToUpper())).ToList();
			var environmentApplications = ConfigurationManager.Environment.Applications.Where(a => AppDomain.CurrentDomain.BaseDirectory.ToUpper().Contains(a.Value.Folder.ToUpper())).ToList();

			if (solutionApplications.Count > 0)
			{
				_application = solutionApplications[0].Value;
			}
			else if (environmentApplications.Count > 0)
			{
				_application = environmentApplications[0].Value;
			}

			if (_application != null)
			{	
				string applicationConfigurationFileName = String.Format("Application.{0}config", String.IsNullOrEmpty(_application.Name) ? "" : _application.Name + ".");
				string applicationConfigurationPath = ConfigurationManager.ConfigurationFolderPath.TrimEnd('\\') + "\\" + applicationConfigurationFileName;
				if (File.Exists(applicationConfigurationPath))
				{
					Load(XDocument.Load(applicationConfigurationPath));
				}

				_settings = new ApplicationConfigurationSettingDictionary(this);
			}
			else
			{
				throw new NullReferenceException("No Application is defined in the Solution.config for the current application.");
			}
		}
	}
}
