﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Slam.Cms.Configuration
{
	public class ConfigurationApplicationDictionary : Dictionary<string, ConfigurationApplication>
	{
		public ConfigurationApplicationDictionary(Configuration configuration)
		{
			configuration["Applications.Application"].ForEach(a =>
				this.Add(a["Name"], new ConfigurationApplication(a["Name"], a["Folder"], a["Url"])));
		}
	}

	public class ConfigurationApplication
	{

		public string Folder
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public string Url
		{
			get;
			set;
		}

		public ConfigurationApplication(string name, string folder, string url)
		{
			Name = name;
			Folder = folder;
			Url = url;
		}
	}
}
