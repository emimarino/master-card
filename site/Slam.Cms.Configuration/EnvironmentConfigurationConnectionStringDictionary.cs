﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace Slam.Cms.Configuration
{
	public class EnvironmentConfigurationConnectionStringDictionary : Dictionary<string, EnvironmentConfigurationConnectionString>
	{
		public EnvironmentConfigurationConnectionStringDictionary(EnvironmentConfiguration configuration)
		{
			configuration["ConnectionStrings.ConnectionString"].ForEach(c =>
				this.Add(c["Name"], new EnvironmentConfigurationConnectionString(c["Name"], c["ConnectionString"])));
			
			foreach (KeyValuePair<string, ConfigurationApplication> application in configuration.Applications)
			{
				DirectoryInfo applicationDirectory = new DirectoryInfo(Path.Combine(ConfigurationManager.SolutionRootFolderPath, application.Value.Folder));
				if (applicationDirectory.Exists)
				{
					foreach (DirectoryInfo directory in applicationDirectory.GetDirectories())
					{
						foreach (FileInfo configFile in directory.GetFiles("*.config", SearchOption.AllDirectories))
						{
							XDocument configXml = XDocument.Load(configFile.FullName);
							var connectionStrings = from element in configXml.Descendants("connectionStrings").Elements("add")
													select element;

							foreach (XElement connectionString in connectionStrings)
							{
								if (!this.ContainsKey(connectionString.Attribute("name").Value))
								{
									this.Add(connectionString.Attribute("name").Value,
										new EnvironmentConfigurationConnectionString(connectionString.Attribute("name").Value,
																					 connectionString.Attribute("connectionString").Value));
								}
							}
						}
					}
				}
			}

			foreach (KeyValuePair<string, ConfigurationApplication> application in ConfigurationManager.Solution.Applications)
			{
				DirectoryInfo applicationDirectory = new DirectoryInfo(Path.Combine(ConfigurationManager.SolutionRootFolderPath, application.Value.Folder));
				if (applicationDirectory.Exists)
				{
					foreach (DirectoryInfo directory in applicationDirectory.GetDirectories())
					{
						foreach (FileInfo configFile in directory.GetFiles("*.config", SearchOption.AllDirectories))
						{
							XDocument configXml = XDocument.Load(configFile.FullName);
							var connectionStrings = from element in configXml.Descendants("connectionStrings").Elements("add")
													select element;

							foreach (XElement connectionString in connectionStrings)
							{
								if (!this.ContainsKey(connectionString.Attribute("name").Value))
								{
									this.Add(connectionString.Attribute("name").Value,
										new EnvironmentConfigurationConnectionString(connectionString.Attribute("name").Value,
																					 connectionString.Attribute("connectionString").Value));
								}
							}
						}
					}
				}
			}
		}
	}

	public class EnvironmentConfigurationConnectionString
	{
		public string ConnectionString
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public EnvironmentConfigurationConnectionString(string name, string connectionString)
		{
			Name = name;
			ConnectionString = connectionString;
		}
	}
}
