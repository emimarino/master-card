﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Slam.Cms.Configuration
{
	public class ConfigurationElement
	{
		private Dictionary<string, string> _attributes;
		public Dictionary<string, string> Attributes
		{
			get
			{
				if (_attributes == null)
				{
					_attributes = new Dictionary<string, string>();
				}
				return _attributes;
			}
		}

		public string Name
		{
			get;
			set;
		}

		public string this[string attributeName]
		{
			get
			{
				string attributeValue = "";
				if (Attributes.ContainsKey(attributeName))
				{
					attributeValue = Attributes[attributeName];
				}
				return attributeValue;
			}
		}

		public string Value
		{
			get;
			set;
		}

		public ConfigurationElement(XElement element)
		{
			Name = element.Name.LocalName;
			Value = element.Value;
			foreach (XAttribute attribute in element.Attributes())
			{
				Attributes.Add(attribute.Name.LocalName, attribute.Value);
			}
		}

		public bool HasAttribute(string attributeName)
		{
			return Attributes.ContainsKey(attributeName);
		}
	}
}
