﻿using Mastercard.MarketingCenter.Data.Metadata.GDP;
using Mastercard.MarketingCenter.DTO.GDP;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TestingApi.Controllers
{
    public class ValuesController : ApiController
    {
        public static List<GdpRequestDTO> requests;
        public static List<GdpResponseConsentDTO> consentResponses;
        public static List<GdpResponseViewDTO> viewResponses;
        public static List<GdpResponseDeleteDTO> deleteResponses;

        static ValuesController()
        {
            requests = new List<GdpRequestDTO>();
            viewResponses = new List<GdpResponseViewDTO>();
            deleteResponses = new List<GdpResponseDeleteDTO>();
            requests = new List<GdpRequestDTO>();
        }


        public IEnumerable<GdpRequestDTO> GetResponses()
        {
            return consentResponses.Cast<GdpRequestDTO>().Union(viewResponses.Cast<GdpRequestDTO>()).Union(deleteResponses.Cast<GdpRequestDTO>());
        }
        public IEnumerable<GdpResponseConsentDTO> GetConsentResponses()
        {
            return consentResponses;
        }
        public IEnumerable<GdpResponseViewDTO> GetViewResponses()
        {
            return viewResponses;
        }
        public IEnumerable<GdpResponseDeleteDTO> GetDeleteResponses()
        {
            return deleteResponses;
        }

        [HttpPost]
        public HttpResponseMessage PostRequest(GdpResponseDTO data)
        {
            var correlationId = GetCorrelationId(Request);

            if (data.GetType() == typeof(GdpResponseViewDTO))
            {
                //Do stuff here...
            }
            if (data.GetType() == typeof(GdpResponseConsentDTO))
            {
                //Do stuff here...
            }
            if (data.GetType() == typeof(GdpResponseViewDTO))
            {
                //Do stuff here...
            }

            Debug.WriteLine(data.ResponseData);

            var response = Request.CreateResponse(HttpStatusCode.Created);
            response.Headers.Add("Correlation-Id", correlationId);
            return response;
        }

        private string GetCorrelationId(HttpRequestMessage request)
        {
            if (!request.Headers.Contains("Correlation-Id"))
            {
                throw new Exception("Correlation-Id header not found");
            }

            return request.Headers.GetValues("Correlation-Id").ElementAt(0);
        }
        public HttpResponseMessage GetResults()
        {
            var correlationId = GetCorrelationId(Request);
            var results = new List<GdpRequestDTO>()
            {
                new GdpRequestDTO {
                    RequestContext ="B2B",
                    RequestId = 100,
                    RequestType = RequestType.Delete,
                    SearchKey = new RequestSearchKeyDTO{
                        Email ="cheriealice.tan@mastercard.com"
                    }
                },
                new GdpRequestDTO {
                    RequestContext ="B2B",
                    RequestId = 101,
                    RequestType = RequestType.Delete,
                    SearchKey = new RequestSearchKeyDTO{
                        Email ="nikki.martin@mastercard.com"
                    }
                },
                new GdpRequestDTO {
                    RequestContext ="B2B",
                    RequestId = 200,
                    RequestType = RequestType.View,
                    SearchKey = new RequestSearchKeyDTO{
                        Email ="fatima.lessiter@mastercard.com"
                    }
                },
                new GdpRequestDTO {
                    RequestContext ="B2B",
                    RequestId = 201,
                    RequestType = RequestType.View,
                    SearchKey = new RequestSearchKeyDTO{
                        Email ="hope.chang@mastercard.com"
                    }
                },
                new GdpRequestDTO {
                    RequestContext ="B2B",
                    RequestId = 300,
                    RequestType = RequestType.ConsentHistory,
                    SearchKey = new RequestSearchKeyDTO{
                        Email ="kristen.zhang@mastercard.com"
                    }
                },
                new GdpRequestDTO {
                    RequestContext ="B2B",
                    RequestId = 301,
                    RequestType = RequestType.ConsentHistory,
                    SearchKey = new RequestSearchKeyDTO{
                        Email ="mabel.tan@mastercard.com"
                    }
                },

            };
            var response = Request.CreateResponse<IEnumerable<GdpRequestDTO>>(HttpStatusCode.OK, results);

            response.Headers.Add("Correlation-Id", correlationId);
            response.Headers.Add("X-Total-Count", results.Count.ToString());

            return response;
        }

        [Route("Consents")]
        public IEnumerable<ConsentFileDataDTO> Consents()
        {
            return new List<ConsentFileDataDTO>() {

                 new ConsentFileDataDTO() { ConsentUseData=new List<ConsentUseDataDTO>(){ new ConsentUseDataDTO(){ ConsentData=new ConsentDataDTO(){  Data="<p>I'am a legal consent</p> " , DocumentType="legalcontent"}, UseCategory=new UseCategoryDTO(){  UseCategoryCode="pn", UseCategoryDescription="This is pn p.s. Discription was misspelled all over documentation.", UseCategoryValue="privacy-notice"} } ,
                  }  , CurrentVersion="1.2", CreatedBy="sdsd", Date="2018-01-01", EmailNotificationRequired=false, Locale="en-us", LastModifiedBy="sdsd", NotificationVersion="1.1",
                  ReacceptanceRequired=true, ReacceptedVersion="1.2", ServiceCode="mos", ServiceDescription="somthing" , ServiceFunctionCode="myFun", ServiceName="mos"
                  , ServiceType="owner", Status="thet", Thirdparty=new ThirdPartyDTO(), Uuid=Guid.NewGuid()  }
                 ,
                     new ConsentFileDataDTO() { ConsentUseData=new List<ConsentUseDataDTO>(){ new ConsentUseDataDTO(){ ConsentData=new ConsentDataDTO(){  Data="<p>I agree tossdd<a href='/portal/consent/terms-of-use'>terms of use</a></p> " , DocumentType="consentlanguagetext"}, UseCategory=new UseCategoryDTO(){  UseCategoryCode="pn", UseCategoryDescription="This is pn p.s. Discription was misspelled all over documentation.", UseCategoryValue="privacy-notice"} } ,
                  }  , CurrentVersion="1.2", CreatedBy="sdsd", Date="2018-01-01", EmailNotificationRequired=false, Locale="en-us", LastModifiedBy="sdsd", NotificationVersion="1.1",
                  ReacceptanceRequired=true, ReacceptedVersion="1.2", ServiceCode="mos", ServiceDescription="somthing" , ServiceFunctionCode="myFun", ServiceName="mos"
                  , ServiceType="owner", Status="thet", Thirdparty=new ThirdPartyDTO(), Uuid=Guid.NewGuid()  }

            };

        }

        [Route("post")]
        public void Post([FromBody]GdpResponseConsentDTO value)
        {
        }

    }
}
