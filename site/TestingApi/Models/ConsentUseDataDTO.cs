﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastercard.MarketingCenter.Services.DTOs
{
   public class ConsentUseDataDTO
    {public UseCategoryDTO UseCategory { get; set; }
        public ConsentDataDTO ConsentData { get; set; }
    }
}
