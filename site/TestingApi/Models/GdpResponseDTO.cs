﻿using Newtonsoft.Json;
using System.Collections.Generic;


namespace Mastercard.MarketingCenter.Services.DTOs
{
    public class GdpResponseDTO
    {
        [JsonProperty("serviceFunctionCode")]
        public string ServiceFunctionCode { get; set; }

        [JsonProperty("requestIdentifier")]
        public int RequestId { get; set; }

        [JsonProperty("responseCode")]
        public string ResponseCode { get; set; }




    }
   


}

