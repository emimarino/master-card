﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastercard.MarketingCenter.Services.DTOs
{
   public  class UseCategoryDTO
    {
        public string UseCategoryCode { get; set; }
        public string UseCategoryValue { get; set; }
        public string UseCategoryDiscription { get; set; }


    }
}
