﻿

using Newtonsoft.Json;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.DTOs
{
    public class UserDTO
    {

        [JsonProperty("fulltName")]
        public string FullName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("address")]
        public UserAddressDTO Address { get; set; }

        [JsonProperty("email")]
        public string  Email { get; set; }

        [JsonProperty("phoneNumbers")]
        public IList<string>  PhoneNumbers { get; set; }

        
    }
}
