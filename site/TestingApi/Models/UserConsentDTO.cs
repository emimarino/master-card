﻿using Newtonsoft.Json;
using System;

namespace Mastercard.MarketingCenter.Services.DTOs
{
    public class UserConsentDTO
    {
        [JsonProperty("userEmail")]
        public string UserEmail { get; set; }

        [JsonProperty("consentType")]
        public string ConsentType { get; set; }

        [JsonProperty("localeCode")]
        public string LocaleCode { get; set; }  

        [JsonProperty("versionID")]
        public int VersionID { get; set; }

        [JsonProperty("consentDate")]
        public DateTime ConsentDate { get; set; }

    }
}
