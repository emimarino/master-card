﻿using Newtonsoft.Json;
using System.Collections.Generic;
namespace Mastercard.MarketingCenter.Services.DTOs
{
    public class RequestSearchKeyDTO
    {
        [JsonProperty("email")]
        public string Email { get; set; }
    }
}
