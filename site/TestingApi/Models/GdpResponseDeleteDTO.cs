﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.DTOs
{
    public class GdpResponseDeleteDTO: GdpResponseDTO
    {

        [JsonProperty("responseList")]
        public IList<NonDeletedDataDTO> NonDeletedDataDTOs;

        public GdpResponseDeleteDTO()
        {
            NonDeletedDataDTOs = new List<NonDeletedDataDTO>();
        }
            

        public bool ShouldSerializeresponseList()
        {
            return (NonDeletedDataDTOs.Count > 0);

        }

    }
}
