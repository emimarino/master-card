﻿using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.DTOs
{
  public class ConsentFileDTO
{
    public Guid Uuid{get; set;}
    public string ServiceCode{get; set;}
    public string ServiceName{get; set;}
    public string ServiceFunctionCode{get; set;}
    public string ServiceDescription{get; set;}
    public string ServiceType{get; set;}
    public string CurrentVersion{get; set;}
    public string ReacceptedVersion{get; set;}
    public string NotificationVersion{get; set;}
    public string Status{get; set;}
    public string Locale{get; set;}
    public string CreatedBy{get; set;}
    public string LastModifiedBy{get; set;}
    public bool ReacceptanceRequired{get; set;}
    public bool EmailNotificationRequired{get; set;}
    public DateTime Date{get; set;}
    public ThirdPartyDTO Thirdparty{get; set;}
    public IList<ConsentUseDataDTO> ConsentUseData{get; set;}
}
}