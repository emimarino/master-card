﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.DTOs
{
    public class GdpResponseConsentDTO: GdpResponseDTO
    {

        [JsonProperty("responseList")]
        public IList<UserConsentDTO> UserConsentDTOs;

        public GdpResponseConsentDTO()
        {
            UserConsentDTOs = new List<UserConsentDTO>();
        }
            

        public bool ShouldSerializeresponseList()
        {
            return (UserConsentDTOs.Count > 0);

        }

    }
}
