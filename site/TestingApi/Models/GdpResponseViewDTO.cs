﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.DTOs
{
    public class GdpResponseViewDTO: GdpResponseDTO
    {

        [JsonProperty("responseList")]
        public IList<UserDTO> UserDTOs;

        public GdpResponseViewDTO()
        {
            UserDTOs = new List<UserDTO>();
        }
            

        public bool ShouldSerializeresponseList()
        {
            return (UserDTOs.Count > 0);

        }

    }
}
