﻿using AutoMapper;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.DTO.Salesforce;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Services.AutoMapper.Profiles
{
    public class SalesforceProfile : Profile
    {
        public SalesforceProfile()
        {
            var regionalizeService = DependencyResolver.Current.GetService<IRegionalizeService>();
            var urlService = DependencyResolver.Current.GetService<IUrlService>();

            CreateMap<SalesforceAccessTokenResponseDTO, SalesforceAccessToken>()
                .ForMember(x => x.AccessToken, opt => opt.MapFrom(source => source.AccessToken))
                .ForMember(x => x.Scope, opt => opt.MapFrom(source => source.Scope))
                .ForMember(x => x.InstanceUrl, opt => opt.MapFrom(source => source.InstanceUrl))
                .ForMember(x => x.TokenId, opt => opt.MapFrom(source => source.Id))
                .ForMember(x => x.TokenType, opt => opt.MapFrom(source => source.TokenType))
                .ForMember(x => x.CreatedDate, opt => opt.UseValue(DateTime.Now))
                .ForMember(x => x.ModifiedDate, opt => opt.UseValue(DateTime.Now));

            CreateMap<SalesforceTask, SalesforceTaskDTO>()
                .ForMember(x => x.Status, opt => opt.MapFrom(source => source.Status))
                .ForMember(x => x.WhatId, opt => opt.MapFrom(source => source.AccountId))
                .ForMember(x => x.TaskActivityType, opt => opt.MapFrom(source => source.TaskActivityType))
                .ForMember(x => x.ActivityDate, opt => opt.MapFrom(source => source.ActivityDate.ToString("yyyy-MM-dd")))
                .ForMember(x => x.WhoId, opt => opt.MapFrom(source => source.ContactId))
                .ForMember(x => x.Subject, opt => opt.MapFrom(source => source.Subject))
                .ForMember(x => x.Description, opt => opt.MapFrom(source => source.Description));

            CreateMap<SalesforceDownloadActivityDTO, SalesforceTask>()
                .ForMember(x => x.ContactEmail, opt => opt.MapFrom(source => source.Email))
                .ForMember(x => x.ContactId, opt => opt.UseValue(string.Empty))
                .ForMember(x => x.AccountId, opt => opt.UseValue(string.Empty))
                .ForMember(x => x.OwnerId, opt => opt.UseValue(string.Empty))
                .ForMember(x => x.Status, opt => opt.MapFrom(source => regionalizeService.GetSalesforceResource("Status", source.Region)))
                .ForMember(x => x.TaskActivityType, opt => opt.MapFrom(source => regionalizeService.GetSalesforceResource("TaskActivityType", source.Region)))
                .ForMember(x => x.ActivityDate, opt => opt.MapFrom(source => source.Date))
                .ForMember(x => x.Subject, opt => opt.MapFrom(source => string.Format(regionalizeService.GetSalesforceResource("Subject", source.Region), source.Region.Trim().ToUpperInvariant())))
                .ForMember(x => x.Description, opt => opt.MapFrom(source => string.Format(regionalizeService.GetSalesforceResource("DescriptionDownload", source.Region), source.File, source.Name, urlService.GetFullUrlOnFrontEnd(source.Url, source.Region))))
                .ForMember(x => x.TaskId, opt => opt.UseValue(string.Empty))
                .ForMember(x => x.SalesforceActivityTypeFieldValue, opt => opt.MapFrom(source => source.Id))
                .ForMember(x => x.SalesforceActivityTypeId, opt => opt.UseValue(MarketingCenterDbConstants.Salesforce.ActivityType.Download))
                .ForMember(x => x.SalesforceTaskStatusId, opt => opt.UseValue(MarketingCenterDbConstants.Salesforce.TaskStatus.Pending))
                .ForMember(x => x.FailedSendAttemptCount, opt => opt.UseValue(0));

            CreateMap<SalesforceOrderActivityDTO, SalesforceTask>()
                .ForMember(x => x.ContactEmail, opt => opt.MapFrom(source => source.Email))
                .ForMember(x => x.ContactId, opt => opt.UseValue(string.Empty))
                .ForMember(x => x.AccountId, opt => opt.UseValue(string.Empty))
                .ForMember(x => x.OwnerId, opt => opt.UseValue(string.Empty))
                .ForMember(x => x.Status, opt => opt.MapFrom(source => regionalizeService.GetSalesforceResource("Status", source.Region)))
                .ForMember(x => x.TaskActivityType, opt => opt.MapFrom(source => regionalizeService.GetSalesforceResource("TaskActivityType", source.Region)))
                .ForMember(x => x.ActivityDate, opt => opt.MapFrom(source => source.Date))
                .ForMember(x => x.Subject, opt => opt.MapFrom(source => string.Format(regionalizeService.GetSalesforceResource("Subject", source.Region), source.Region.Trim().ToUpperInvariant())))
                .ForMember(x => x.Description, opt => opt.MapFrom(source => string.Format(regionalizeService.GetSalesforceResource("DescriptionOrder", source.Region), source.Quantity, source.Name, urlService.GetFullUrlOnFrontEnd(source.Url, source.Region))))
                .ForMember(x => x.TaskId, opt => opt.UseValue(string.Empty))
                .ForMember(x => x.SalesforceActivityTypeFieldValue, opt => opt.MapFrom(source => source.Id))
                .ForMember(x => x.SalesforceActivityTypeId, opt => opt.UseValue(MarketingCenterDbConstants.Salesforce.ActivityType.Order))
                .ForMember(x => x.SalesforceTaskStatusId, opt => opt.UseValue(MarketingCenterDbConstants.Salesforce.TaskStatus.Pending))
                .ForMember(x => x.FailedSendAttemptCount, opt => opt.UseValue(0));

            CreateMap<SalesforceUserActivityDTO, SalesforceTask>()
                .ForMember(x => x.ContactEmail, opt => opt.MapFrom(source => source.Email))
                .ForMember(x => x.ContactId, opt => opt.UseValue(string.Empty))
                .ForMember(x => x.AccountId, opt => opt.UseValue(string.Empty))
                .ForMember(x => x.OwnerId, opt => opt.UseValue(string.Empty))
                .ForMember(x => x.Status, opt => opt.MapFrom(source => regionalizeService.GetSalesforceResource("Status", source.Region)))
                .ForMember(x => x.TaskActivityType, opt => opt.MapFrom(source => regionalizeService.GetSalesforceResource("TaskActivityType", source.Region)))
                .ForMember(x => x.ActivityDate, opt => opt.MapFrom(source => source.Date))
                .ForMember(x => x.Subject, opt => opt.MapFrom(source => string.Format(regionalizeService.GetSalesforceResource("Subject", source.Region), source.Region.Trim().ToUpperInvariant())))
                .ForMember(x => x.Description, opt => opt.MapFrom(source => regionalizeService.GetSalesforceResource("DescriptionUser", source.Region)))
                .ForMember(x => x.TaskId, opt => opt.UseValue(string.Empty))
                .ForMember(x => x.SalesforceActivityTypeFieldValue, opt => opt.MapFrom(source => source.Id))
                .ForMember(x => x.SalesforceActivityTypeId, opt => opt.UseValue(MarketingCenterDbConstants.Salesforce.ActivityType.User))
                .ForMember(x => x.SalesforceTaskStatusId, opt => opt.UseValue(MarketingCenterDbConstants.Salesforce.TaskStatus.Pending))
                .ForMember(x => x.FailedSendAttemptCount, opt => opt.UseValue(0));
        }
    }
}