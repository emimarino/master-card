﻿using AutoMapper;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Models;

namespace Mastercard.MarketingCenter.Services.AutoMapper.Profiles
{
    public class EventLocationProfile : Profile
    {
        public EventLocationProfile()
        {
            CreateMap<EventLocation, EventLocationModel>()
                .ForMember(x => x.Offers, opt => opt.MapFrom(source => source.Offers.Count));
        }
    }
}