﻿using AutoMapper;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Entities;
using Mastercard.MarketingCenter.Services.Models;
using System;
using System.Linq;

namespace Mastercard.MarketingCenter.Services.AutoMapper.Profiles
{
    public class OfferProfile : Profile
    {
        public OfferProfile()
        {
            CreateMap<Offer, OfferListModel>()
                .ForMember(x => x.Image, opt => opt.MapFrom(source => source.MainImage))
                .ForMember(x => x.DetailsUrl, opt => opt.MapFrom(source => source.ContentItem.FrontEndUrl))
                .ForMember(x => x.CreationDate, opt => opt.MapFrom(source => source.ContentItem.CreatedDate))
                .ForMember(x => x.OfferType, opt => opt.MapFrom(source => source.OfferType.Title))
                .ForMember(x => x.CategoryIds, opt => opt.MapFrom(source => source.Categories.Select(c => c.CategoryId).ToList()))
                .ForMember(x => x.CardExclusivityIds, opt => opt.MapFrom(source => source.CardExclusivities.Select(ce => ce.CardExclusivityId).ToList()))
                .ForMember(x => x.EventLocations, opt => opt.MapFrom(source => source.EventLocations.Select(c => c).ToList()))
                .ForMember(x => x.MarketApplicabilityIds, opt => opt.MapFrom(source => source.ContentItem.Tags.Where(t => t.TagHierarchyPositions.Any(thp => thp.TagCategoryId.Equals(MarketingCenterDbConstants.TagCategories.Markets))).Select(t => t.Identifier).ToList()));

            CreateMap<Offer, OfferDetailsModel>()
                .ForMember(x => x.OfferType, opt => opt.MapFrom(source => source.OfferType.Title))
                .ForMember(x => x.ShowProductUrl, opt => opt.MapFrom(source => source.OfferType.ShowProductUrl && !string.IsNullOrWhiteSpace(source.ProductUrl)))
                .ForMember(x => x.PricelessOffer, opt => opt.MapFrom(source => !string.IsNullOrWhiteSpace(source.PricelessOfferId)))
                .ForMember(x => x.Attachments, opt => opt.MapFrom(source => source.Attachments.ToList()))
                .ForMember(x => x.SecondaryImages, opt => opt.MapFrom(source => source.SecondaryImages.ToList()))
                .ForMember(x => x.CardExclusivities, opt => opt.MapFrom(source => source.CardExclusivities.ToList()))
                .ForMember(x => x.Categories, opt => opt.MapFrom(source => source.Categories.ToList()))
                .ForMember(x => x.RelatedItems, opt => opt.MapFrom(source => source.RelatedItems.ToList()))
                .ForMember(x => x.EventLocations, opt => opt.ResolveUsing(source => source.EventLocations.ToList()))
                .ForMember(x => x.DownloadFile, opt => opt.ResolveUsing(source => source));

            CreateMap<Offer, DownloadFile>()
                .ForMember(x => x.ContentItemId, opt => opt.MapFrom(source => source.ContentItemId))
                .ForMember(x => x.PrimaryContentItemId, opt => opt.MapFrom(source => source.ContentItem.PrimaryContentItemId))
                .ForMember(x => x.Title, opt => opt.MapFrom(source => source.Title))
                .ForMember(x => x.DownloadableFileExists, opt => opt.MapFrom(source => !string.IsNullOrWhiteSpace(source.DownloadZipFile)))
                .ForMember(x => x.Filename, opt => opt.MapFrom(source => source.DownloadZipFile))
                .ForMember(x => x.IsInUserMarket, opt => opt.MapFrom(source => true))
                .ForMember(x => x.RequestEstimatedDistribution, opt => opt.MapFrom(source => false))
                .ForMember(x => x.SetForElectronicDelivery, opt => opt.MapFrom(source => false))
                .ForMember(x => x.TrackingType, opt => opt.MapFrom(source => string.Empty));

            CreateMap<ImportedOfferModel, ImportedOffer>()
                .ForMember(x => x.ImportedDate, opt => opt.MapFrom(source => DateTime.Now))
                .ForMember(x => x.IsLastImportedVersion, opt => opt.MapFrom(source => true))
                .ForMember(x => x.IsProcessed, opt => opt.MapFrom(source => false))
                .ForMember(x => x.IsArchived, opt => opt.MapFrom(source => false));
        }
    }
}