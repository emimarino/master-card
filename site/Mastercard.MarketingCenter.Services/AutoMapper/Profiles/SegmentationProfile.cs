﻿using AutoMapper;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Models;
using System;
using System.Linq;

namespace Mastercard.MarketingCenter.Services.AutoMapper.Profiles
{
    public class SegmentationProfile : Profile
    {
        public SegmentationProfile()
        {
            CreateMap<Segmentation, SegmentationModel>()
                .ForMember(x => x.SegmentationName, opt => opt.ResolveUsing((source, destination, destinationMember, resolutionContext) =>
                                                           destination.SegmentationName = source.SegmentationTranslatedContents.FirstOrDefault(s =>
                                                                                          s.LanguageCode.Equals(resolutionContext.Items["Language"].ToString(),
                                                                                          StringComparison.InvariantCultureIgnoreCase))?.SegmentationName ??
                                                                                          source.SegmentationName));
        }
    }
}