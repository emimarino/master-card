﻿using AutoMapper;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Models;
using System.Linq;

namespace Mastercard.MarketingCenter.Services.AutoMapper.Profiles
{
    public class MarketApplicabilityProfile : Profile
    {
        public MarketApplicabilityProfile()
        {
            CreateMap<Tag, MarketApplicabilityModel>()
                .ForMember(x => x.MarketApplicabilityId, opt => opt.MapFrom(source => source.Identifier))
                .ForMember(x => x.Title, opt => opt.MapFrom(source => source.DisplayName))
                .ForMember(x => x.Offers, opt => opt.MapFrom(source => source.ContentItems.Count(ci => ci.ContentTypeId.Equals(MarketingCenterDbConstants.ContentTypeIds.Offer))));
        }
    }
}