﻿using AutoMapper;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;
using System;

namespace Mastercard.MarketingCenter.Services.AutoMapper.Profiles
{
    public class UploadProfile : Profile
    {
        public UploadProfile()
        {
            CreateMap<UploadActivityDTO, UploadActivity>()
                .ForMember(x => x.ActivityDate, opt => opt.MapFrom(source => DateTime.Now))
                .ForMember(x => x.UploadActivityActionId, opt => opt.MapFrom(source => source.Action))
                .ForMember(x => x.UploadActivityStatusId, opt => opt.MapFrom(source => source.Status));
        }
    }
}