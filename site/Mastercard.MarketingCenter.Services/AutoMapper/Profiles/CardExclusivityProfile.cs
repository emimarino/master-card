﻿using AutoMapper;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Models;

namespace Mastercard.MarketingCenter.Services.AutoMapper.Profiles
{
    public class CardExclusivityProfile : Profile
    {
        public CardExclusivityProfile()
        {
            CreateMap<CardExclusivity, CardExclusivityModel>()
                .ForMember(x => x.Offers, opt => opt.MapFrom(source => source.Offers.Count));
        }
    }
}