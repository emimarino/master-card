﻿using AutoMapper;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Models;

namespace Mastercard.MarketingCenter.Services.AutoMapper.Profiles
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<Category, CategoryModel>()
                .ForMember(x => x.Offers, opt => opt.MapFrom(source => source.Offers.Count));
        }
    }
}