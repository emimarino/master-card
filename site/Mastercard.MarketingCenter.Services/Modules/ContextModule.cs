﻿using Autofac;
using Mastercard.MarketingCenter.Common.Services;

namespace Mastercard.MarketingCenter.Services.Modules
{
    public class ContextModule : Module
    {
        private readonly bool _defaultLanguageIsFromBrowser = false;

        public ContextModule(bool defaultLanguageIsFromBrowser = false)
        {
            _defaultLanguageIsFromBrowser = defaultLanguageIsFromBrowser;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CookieNameProviderService>();
            builder.RegisterType<UserContext>()
                .WithParameter("defaultLanguageIsFromBrowser", this._defaultLanguageIsFromBrowser)
                .InstancePerLifetimeScope();
        }
    }
}