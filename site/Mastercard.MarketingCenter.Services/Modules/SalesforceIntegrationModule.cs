﻿using Autofac;
using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Interfaces;
using Mastercard.MarketingCenter.Data.Repositories;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Services;

namespace Mastercard.MarketingCenter.Services.Modules
{
    public class SalesforceIntegrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SalesforceRepository>().As<ISalesforceRepository>();
            builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<WebMembershipProvider>().As<IMembershipProvider>();

            builder.RegisterType<SalesforceApiService>().As<ISalesforceApiService>();
            builder.RegisterType<SalesforceService>().As<ISalesforceService>();
            builder.RegisterType<SettingsService>().As<ISettingsService>();

            builder.RegisterModule(new AutoMapperModule());
        }
    }
}