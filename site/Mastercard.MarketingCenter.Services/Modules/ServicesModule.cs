﻿using Autofac;
using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Common.Modules;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Modules;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Services;
using Slam.Cms.Admin.Data;
using Slam.Cms.Common;
using Slam.Cms.Configuration;
using Slam.Cms.Data;
using Slam.Cms.Modules;
using Slam.Cms.SqlServer;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace Mastercard.MarketingCenter.Services
{
    public class ServicesModule : Module
    {
        private readonly bool _filterByCurrentRegion = false;
        private readonly bool _profiledSqlConnenction = false;

        public ServicesModule(bool filterByCurrentRegion, bool profiledSqlConnenction)
        {
            _filterByCurrentRegion = filterByCurrentRegion;
            _profiledSqlConnenction = profiledSqlConnenction;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<LookupService>();
            builder.RegisterType<MastercardAuthenticationService>();
            builder.RegisterType<LoginService>();
            builder.RegisterType<ObjectManager>();
            builder.RegisterType<IssuerService>().As<IIssuerService>();
            builder.RegisterType<ConsentManagementService>().As<IConsentManagementService>();
            builder.RegisterType<UserPrivacyDataService>().As<IUserPrivacyDataService>();
            builder.RegisterType<RegistrationOperationsService>();
            builder.RegisterType<ProcessorService>();
            builder.RegisterType<RegistrationService>();
            builder.RegisterType<PendingRegistrationService>().As<IPendingRegistrationService>();
            builder.RegisterType<ClonesSourceNotificationServices>().As<IClonesSourceNotificationServices>();
            builder.RegisterType<MasterCardPortalDataContext>();
            builder.RegisterType<SegmentationService>().As<ISegmentationService>();
            builder.RegisterType<ContentItemService>().As<IContentItemService>();
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<ContentCommentServices>().As<IContentCommentServices>();
            builder.RegisterType<TagService>().As<ITagService>();
            builder.RegisterType<ContentReviewServices>().As<IContentReviewServices>();
            builder.RegisterType<MastercardAuthorizationService>().As<IAuthorizationService>();
            builder.RegisterType<AnonymousTokenService>().As<IAnonymousTokenService>();
            builder.RegisterType<OrderService>().As<IOrderService>();
            builder.RegisterType<ReportService>().AsSelf().As<IReportService>();
            builder.RegisterType<InsertsBundleService>();
            builder.RegisterType<ReportService>();
            builder.RegisterType<SettingsService>().As<ISettingsService>();
            builder.RegisterType<IconService>().As<IIconService>();
            builder.RegisterType<RegionService>().As<IRegionService>();
            builder.RegisterType<EmailService>().As<IEmailService>();
            builder.RegisterType<BussinesOwnerService>().As<IBussinesOwnerService>();
            builder.RegisterType<InsertsBundleService>();
            builder.RegisterType<ImportedIcaService>().As<IImportedIcaService>();
            builder.RegisterType<CalendarService>().As<ICalendarService>();
            builder.RegisterType<ErrorMessageService>().As<IErrorMessageService>();
            builder.RegisterType<MostPopularService>().As<IMostPopularService>();
            builder.RegisterType<MailDispatcherService>().As<IMailDispatcherService>();
            builder.RegisterType<TrackingService>().As<ITrackingService>();
            builder.RegisterType<UrlService>().As<IUrlService>();
            builder.RegisterType<RegionConfigManager>();
            builder.RegisterType<RegionConfigManagerFeatureFlagProvider>().As<IFeatureFlagProvider>();
            builder.RegisterType<ConfigFeatureFlag>().As<AbstractFeatureFlag>();
            builder.RegisterType<ImageService>().As<IImageService>();
            builder.RegisterType<ZipFileService>().As<IZipFileService>();
            builder.RegisterType<UserKeyService>().As<IUserKeyService>();
            builder.RegisterType<CaptchaService>().As<ICaptchaService>();
            builder.RegisterType<PardotIntegrationService>().As<IPardotIntegrationService>();
            builder.RegisterType<JsonWebTokenService>().As<IJsonWebTokenService>();
            builder.RegisterType<RegionalizeService>().As<IRegionalizeService>();
            builder.RegisterType<OfferService>().As<IOfferService>();
            builder.RegisterType<EventLocationService>().As<IEventLocationService>();
            builder.RegisterType<SearchService>().As<ISearchService>();
            builder.RegisterType<CsvFileService>().As<ICsvFileService>();
            builder.RegisterType<PricelessApiService>().As<IPricelessApiService>();
            builder.RegisterType<CountryService>().As<ICountryService>();
            builder.RegisterType<LanguageService>().As<ILanguageService>();
            builder.RegisterType<SalesforceApiService>().As<ISalesforceApiService>();
            builder.RegisterType<SalesforceService>().As<ISalesforceService>();
            builder.RegisterType<FileService>().As<IFileService>();

            builder.RegisterModule(new PersistenceModule(_profiledSqlConnenction));
            builder.RegisterModule<CommonModule>();
            builder.RegisterModule(new SlamCmsAdminDataModule());
            builder.RegisterModule(new CmsDataModule());
            builder.RegisterModule(new SlamCmsModule(_filterByCurrentRegion, typeof(AssetDTO).GetAssembly()));
            builder.RegisterType<FileUploadService>().As<IFileUploadService>().SingleInstance();

            builder.RegisterAssemblyTypes(typeof(IDefaultValueCalculator).Assembly).Where(t => typeof(IDefaultValueCalculator).IsAssignableFrom(t)).AsImplementedInterfaces();

            //injection of interface
            builder.Register<IPrincipal>(c =>
            {
                MastercardRolePrincipal principal = null;
                var context = c.Resolve<HttpContextBase>();
                if (context != null && context.User is RolePrincipal)
                {
                    var ticket = ((RolePrincipal)context.User).ToEncryptedTicket();
                    if (ticket == null)
                    {
                        principal = new MastercardRolePrincipal(context.User.Identity, c.Resolve<IAuthorizationRepository>(), c.Resolve<UserContext>(), c.Resolve<ICachingService>());
                    }
                    else
                    {
                        principal = new MastercardRolePrincipal(context.User.Identity, ticket, c.Resolve<IAuthorizationRepository>(), c.Resolve<UserContext>(), c.Resolve<ICachingService>());
                    }
                }
                return principal;
            }).InstancePerLifetimeScope();
        }
    }
}