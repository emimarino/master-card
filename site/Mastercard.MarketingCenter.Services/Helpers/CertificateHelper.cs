﻿using Mastercard.MarketingCenter.Services.Exceptions;
using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace Mastercard.MarketingCenter.Services.Helpers
{
    public static class CertificateHelper
    {
        public static X509Certificate2 GetX509ClientCertificateBySerialNumber(string certificateSerialNumber, StoreLocation store)
        {
            X509Store userCaStore = new X509Store(StoreName.My, store);
            try
            {
                userCaStore.Open(OpenFlags.ReadOnly);
                X509Certificate2Collection certificatesInStore = userCaStore.Certificates;
                X509Certificate2Collection findResult = certificatesInStore.Find(X509FindType.FindBySerialNumber, certificateSerialNumber, false);
                X509Certificate2 clientCertificate = null;
                if (findResult.Count == 1)
                {
                    clientCertificate = findResult[0];
                    CheckExpirationDate(clientCertificate.NotAfter);
                }
                else if (findResult.Count > 1)
                {
                    throw new Exception("There are more than one certificate with the same subject.");
                }
                else
                {
                    throw new Exception("Unable to locate the correct client certificate.");
                }

                return clientCertificate;
            }
            catch
            {
                throw;
            }
            finally
            {
                userCaStore.Close();
            }
        }

        public static byte[] SignSha256X509ClientCertificateBytes(X509Certificate2 certificate, byte[] payloadBytes)
        {
            var pv = certificate.GetRSAPrivateKey();
            var signaturedBytes = pv.SignData(payloadBytes, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);

            return signaturedBytes;
        }

        private static void CheckExpirationDate(DateTime expirationDate)
        {
            if (DateTime.Now > expirationDate)
            {
                throw new ExpiredCertificateException(expirationDate);
            }
        }
    }

    public enum CertificateStorageTypes
    {
        User = 1,
        Computer = 2
    }
}