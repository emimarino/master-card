﻿namespace Mastercard.MarketingCenter.Services.Entities
{
    public class DownloadFile
    {
        public string ContentItemId { get; set; }
        public string PrimaryContentItemId { get; set; }
        public string Title { get; set; }
        public bool DownloadableFileExists { get; set; }
        public string Filename { get; set; }
        public bool IsInUserMarket { get; set; }
        public bool RequestEstimatedDistribution { get; set; }
        public bool SetForElectronicDelivery { get; set; }
        public string TrackingType { get; set; }

        public string DisplayName
        {
            get
            {
                string[] tokens = Filename.Split('/');
                var fileName = tokens[tokens.Length - 1];
                fileName = fileName.Substring(0, fileName.LastIndexOf('.'));
                return fileName.Replace("\"", "‘’");
            }
        }

        public string Extension
        {
            get
            {
                string[] tokens = Filename.Split('.');
                return tokens[tokens.Length - 1];
            }
        }
    }
}