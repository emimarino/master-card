﻿namespace Mastercard.MarketingCenter.Services.Entities
{
    public class SearchAutoCompleteResultItem
    {
        private string mid, mvalue, minfo;

        public string id
        {
            get { return mid; }
            set { mid = value; }
        }

        public string value
        {
            get { return mvalue; }
            set { mvalue = value; }
        }

        public string info
        {
            get { return minfo; }
            set { minfo = value; }
        }
    }
}