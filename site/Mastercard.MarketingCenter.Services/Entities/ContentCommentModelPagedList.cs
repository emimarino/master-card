﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Entities
{
    public class ContentCommentModelPagedList
    {
        public IEnumerable<ContentCommentModel> ContentComments { get; set; }
        public int TotalComments { get; set; }
        public int PageNumber { get; set; }
        public string ContentItemId { get; set; }
        public int SenderUserId { get; set; }
        public int PageSize { get; set; } = 5;
    }
}