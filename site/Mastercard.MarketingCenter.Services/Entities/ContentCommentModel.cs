﻿using System;

namespace Mastercard.MarketingCenter.Services.Entities
{
    public class ContentCommentModel
    {
        public DateTime CreationDate { get; set; }
        public int ContentCommentId { get; set; }
        public string CommentBody { get; set; }
        public string SenderUserName { get; set; }
    }
}