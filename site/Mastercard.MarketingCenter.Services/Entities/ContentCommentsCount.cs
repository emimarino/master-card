﻿namespace Mastercard.MarketingCenter.Services.Entities
{
    public class ContentCommentsCount
    {
        public int CommentsCount { get; set; }
        public string ContentItemId { get; set; }
        public int SenderUserId { get; set; }
    }
}