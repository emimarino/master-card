﻿namespace Mastercard.MarketingCenter.Services.Entities
{
    public class ContentCommentDTO
    {
        public string ContentItemID { get; set; }
        public string CommentBody { get; set; }
    }
}