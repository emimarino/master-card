﻿using System;

namespace Mastercard.MarketingCenter.Services.Entities
{
    [Serializable]
    public class SearchAutoCompleteItem
    {
        private string mTitle, mSearchParameter, mRestrictToSegments;
        private int mRank, mSortOrder;

        public string Title
        {
            get { return mTitle; }
            set { mTitle = value; }
        }

        public string SearchParameter
        {
            get { return mSearchParameter; }
            set { mSearchParameter = value; }
        }

        public string RestrictToSegments
        {
            get { return mRestrictToSegments; }
            set { mRestrictToSegments = value; }
        }

        public int Rank
        {
            get { return mRank; }
            set { mRank = value; }
        }

        public int SortOrder
        {
            get { return mSortOrder; }
            set { mSortOrder = value; }
        }

        public SearchAutoCompleteItem() { }

        public SearchAutoCompleteItem(string title, string searchParameter, int rank, int sortOrder, string restrictToSegments)
        {
            this.mTitle = title;
            this.mSearchParameter = searchParameter;
            this.mRank = rank;
            this.mSortOrder = sortOrder;
            this.mRestrictToSegments = restrictToSegments;
        }
    }
}