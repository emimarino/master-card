﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Models;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IEventLocationService
    {
        EventLocationTree GetEventLocationTree();
        EventLocationTree GetEventLocationTreeByContentId(string contentItemId);
        EventLocationTree GetEventLocationTree(IEnumerable<EventLocation> eventLocations);
        void AddEventLocationToHierarchy(int parentEventLocationId, string title, string pricelessEventCity);
        void DeleteEventLocation(int id);
        EventLocationPositionHierarchy GetHierarchyByEventLocationId(int id);
        EventLocation GetEventLocationById(int id);
        EventLocationPositionHierarchy UpdateEventLocationHierarchy(EventLocationPositionHierarchy eventLocationHierarchy);
        void SetEventLocations(string itemId, IDictionary<string, object> data, FieldDefinition[] definitions, ICmsDbConnection connection);
        IEnumerable<EventLocation> GetEventLocationByContentItemId(string contentItemId);
        IEnumerable<EventLocation> GetEventLocations();
        bool ExistPricelessEventCity(int eventLocationId, string pricelessEventCity);
        IEnumerable<EventLocationModel> GetEventLocationModel();
        void ReloadEventLocationModel();
    }
}