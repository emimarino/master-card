﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface ICaptcha
    {
        string CaptchaValidatorTrigger
        {
            get;
        }

        string CaptchaValidatorHandler
        {
            get;
        }

        string CaptchaValidationResult { get; set; }
        Dictionary<string, string> Parameters { get; set; }
        Dictionary<string, string> GetValidationRequestParameters();
        string Discriminator { get; }
        bool IsConfigurationValid();
    }
}