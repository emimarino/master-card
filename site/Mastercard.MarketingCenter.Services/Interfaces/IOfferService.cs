﻿using Mastercard.MarketingCenter.Services.Models;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IOfferService
    {
        IEnumerable<CardExclusivityModel> GetCardExclusivities();
        IEnumerable<CategoryModel> GetCategories();
        IEnumerable<OfferListModel> GetOffersByRegion(string regionId, bool ignorePreviewMode = false);
        IEnumerable<MarketApplicabilityModel> GetMarketApplicabilities(string regionId);
        OfferDetailsModel GetOffer(string contentItemId, string regionId);
        void ReloadCardExclusivityModel();
        void ReloadCategoryModel();
        void ReloadOfferModel();
        void ReloadMarketApplicabilityModel();
        void InsertImportedOffers(IEnumerable<ImportedOfferModel> importedOffers);
        void ProcessPricelessOffers();
        void CleanUpImportedOffers();
        void ProcessUnmatchedPricelessOffers();
        void ImportPricelessOffers();
    }
}