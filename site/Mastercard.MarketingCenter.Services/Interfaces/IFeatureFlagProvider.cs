﻿namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IFeatureFlagProvider
    {
        bool? GetSetting(string featureKey, string regionKey);
    }
}