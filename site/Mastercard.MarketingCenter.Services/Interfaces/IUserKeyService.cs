﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IUserKeyService
    {
        UserKey CreateUserKey(int userId);
        UserKey GetByKey(string key);
        UserKey GetByUserName(string userName);
        UserKey GetByUserNameAndStatus(string userName, int userKeyStatusId);
        void ChangeUserKeyStatus(string key, int userKeyStatusId);
        void ChangeUserKeyStatusForRelatedKeys(string key, int userKeyStatusId);
    }
}