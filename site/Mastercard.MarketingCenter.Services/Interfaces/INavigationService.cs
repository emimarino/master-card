﻿using Mastercard.MarketingCenter.Services.Models;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface INavigationService
    {
        NavigationModel GetNavigationModel(IDictionary<string, object> routeData, NameValueCollection queryString);
    }
}