﻿namespace Mastercard.MarketingCenter.Services.Interfaces
{
    //In the future ICloningServices from  Mastercard.MarketingCenter.Cms.Services
    public interface IClonesSourceNotificationServices
    {
        void ProcessSourceUpdateNotifications();
        int BuildSourceUpdateNotificationEmails();
    }
}