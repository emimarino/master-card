﻿using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IGdpApiService
    {
        void ObtainGdpRequests();
        void ProcessGdpRequests();
        int ApplyUserPrivacyDataRetention(out bool hasError, out ICollection<Exception> exception);
        int ProcessFollowUpEmails(string region);
    }
}