﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IIconService
    {
        IEnumerable<Icon> GetAll();
        Icon GetIcon(string iconId);
        /// <summary>
        /// Returns icon URL for an icon. Ideally icons should be stored in both CMS and Web projects and a root path is taken out ISettingsService
        /// </summary>
        /// <param name="icon"></param>
        /// <returns></returns>
        string GetIconUrl(Icon icon);
        string GetIconUrl(string iconId);
    }
}