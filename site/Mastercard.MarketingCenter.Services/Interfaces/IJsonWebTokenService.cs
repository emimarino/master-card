﻿namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IJsonWebTokenService
    {
        string GetToken<T>(T model);
        bool IsValidToken(string token);
        T GetFromToken<T>(string token);
        void GetAsync(string[] urls, object model);
    }
}