﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IConsentManagementService
    {
        void RetrieveConsentFilesAndVersions();
        void InsertUserConsent(UserConsent userConsent);
        void InsertUserConsent(IEnumerable<UserConsent> userConsent);
        void InsertUserConsentByEmail(string email, IEnumerable<int> ConsentFileIds);
        IEnumerable<ConsentFileData> GetExpiredConsentForUser(string userName, string locale);
        ConsentFileData GetLatestConsentLanguageText(string useCode, string locale, string functionCode);
        ConsentFileData GetLatestLegalConsentData(string useCode, string locale, string functionCode = null);
        bool IsAnyUserConsentExpired(string username, string locale);
        IEnumerable<UserConsent> GetUserConsents(string email, int limit = 0, int offset = 0);
        IEnumerable<ConsentFileData> GetLatestConsents(string locale, string functionCode);
    }
}