﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IFieldDataNormalizationContext
    {
        IDictionary<string, object> Apply(IDictionary<string, object> originalData, IEnumerable<FieldDefinition> fieldDefinition);
    }
}