﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface ILanguageService
    {
        Language GetLanguageByIdentifier(string language);
    }
}