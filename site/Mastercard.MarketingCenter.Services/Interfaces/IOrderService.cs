﻿using Mastercard.MarketingCenter.Services.Data;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IOrderService
    {
        int? CreateOrder(int shoppingCartID);
        void JoinShipmentSubOrdersInOrder(int orderId, int shippingInformationId);
        void SplitSubOrder(int subOrderId, string splitOrderItemIds);
        int? IsOrderAlreadyExists(string OrderNo);
        void UpdateItem(int OrderID, string ItemID, string ProofPDF);
        void UpdateOrderTrackingNumber(int shippingInformationId, string trackingNumber);
        void UpdateSubOrderTrackingNumber(int subOrderId, string trackingNumber);
        void UpdatePriceAdjustment(int OrderID, decimal? PriceAdjustment, decimal? PostageCost, decimal? ShippingCost, string Description);
        void UpdateSubOrderPriceAdjustment(int subOrderId, decimal? PriceAdjustment, decimal? PostageCost, decimal? ShippingCost, string Description, bool completesOrder);
        string GetPrinterEmail(int OrderID, string status);
        List<RetrieveCustomizationsResult> GetCustomizationOptions(int Orderid);
        List<RetrieveShippingInformationResult> GetShippingInformation(int Orderid);
        List<RetrieveOrderHistoryResult> GetOrderHistory(string UserID);
        List<RetrieveAllOrderItemsResult> GetAllOrderItems(int Orderid);
        List<RetrieveOrderItemsResult> GetOrderItems(int Orderid);
        RetrieveOrderDetailResult GetOrderDetails(int Orderid);
        RetrieveSubOrderDetailResult GetSubOrderDetail(int subOrderId);
        List<RetrieveOrderLogResult> GetOrderLog(int Orderid);
        List<RetrieveOrderItemsForShippingInformationResult> GetOrderItemsForShippingInformation(int shippingInformationId);
        List<RetrieveOrderSubOrdersResult> GetSubOrders(int orderId, int shippingInformationId);
        List<RetrieveSubOrderItemsResult> GetSubOrderItems(int subOrderId);
        void SetOrderStatus(int OrderID, string status, string FeedBack, string clientIP, string UserName);
        void SetOrderStatus(int OrderID, string status);
        List<RetrieveOrdersByStatusResult> GetOrderByStatus(string Status);
        List<GetOrderMonthListResult> GetOrderMonthList();
        List<OrderDetailsReportResult> GetOrderDetailedReport(DateTime OrderDate);
        List<OrderDetailRangeReportResult> GetOrderDetailRangeReport(DateTime startDate, DateTime endDate);
        int GetOrderIDByOrderNo(string OrderNo);
        string GetOrderNumberByOrderId(int OrderId);
        string GetOrderStatus(string OrderNo);
        string GetOrderStatus(int orderId);
        RetrieveSubOrderCompletionDetailResult GetSubOrderCompletionDetail(int subOrderId);
        RetrievePromotionForOrderResult GetPromotion(int orderId);
        void UpdateCustomizationDetails(int OrderID, int ShoppingCardId, string CustomizationOptionItemID, string Data, string clientIP, string UserName);
        void SaveOrderProcessNotesCustomizationDetails(int OrderID, string clientIP, string UserName);
        VdpOrderInformation GetVdpOrderInformation(int orderId);
        void SaveVdpOrderInformation(int orderId, string description, decimal priceAdjustment, decimal postageCost);
        Order GetLatestOrderForUser(string userName);
        Order GetOrderById(int orderId);
        IList<OrderItem> GetOrderItemsForOrder(int orderId);
        void UpdateOrderItemHasAdvisorsTag(int orderItemId, bool hasAdvisorsTag);
        void UpdateOrderConsentGiven(int orderId, bool consentGiven);
    }
}