﻿using Mastercard.MarketingCenter.Services.Models;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IPardotIntegrationService
    {
        PardotIntegrationModel GetPardotIntegrationModel(string regionId, string firstName, string lastName, string title, string issuerName, string processor, string phone, string mobile, string fax, string email, string country, string language, string address1, string address2, string address3, string city, string state, string zipCode, string acceptedConsents, bool acceptedEmailSubscriptions);
        void SubmitPardotForm(PardotIntegrationModel model);
    }
}