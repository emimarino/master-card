﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IContentLinkValidatorService
    {
        void UpdateContentBrokenLinks(IList<string> excludeUrls);
    }
}