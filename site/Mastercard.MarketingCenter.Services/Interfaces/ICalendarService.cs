﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface ICalendarService
    {
        IEnumerable<CampaignCategory> GetCampaignCategoriesByRegionAndLanguage(string regionId, string languageCode);
        IEnumerable<CampaignCategory> GetCampaignCategoriesByIds(string[] campaignCategoryIds);
        IEnumerable<ContentItemCampaignEvent> GetContentItemCampaignEventsFilterByIds(string[] contentItemIds);
        IEnumerable<ContentItemCampaignEvent> GetContentItemCampaignEventsFilterByIds(string[] contentItemIds, DateTime calendarStartDate, DateTime calendarEndDate);
        IEnumerable<CampaignEvent> GetCampaignEventsFilterByIds(string[] campaignEventIds);
        DownloadCalendarKey CreateDownloadCalendarKey(int userId, string region, string language, string specialAudience, string segmentationIds, string campaignCategoryIds, string marketIdentifiers, int downloadYear);
        DownloadCalendarKey GetDownloadCalendarKey(string key);
        bool DeleteCampaignCategory(string campaignCategoryId);
    }
}