﻿using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    /// <summary>
    /// This service is responsible for creating URLs to CMS and Portal
    /// please put all methods doing above here.
    /// </summary>
    public interface IUrlService
    {
        string GetFullUrlOnFrontEnd(string realtiveUrl, bool useSecondaryHost = false);
        string GetFullUrlOnFrontEnd(string realtiveUrl, string regionId);
        string GetAdminHomeURL();
        string GetFrontEndHomeURL();
        string GetContentEditURL(string contentItemId);
        string GetBusinessOwnerHomeURL();
        string GetBusinessOwnerDetailsURL(string contentItemId);
        string GetExpiredContentExtendURL(string contentItemId);
        string GetExpiredContentDoNotExtendURL(string contentItemId);
        string GetUserProfileEditorHomeURL();
        string GetResetPasswordURL(string key);
        string GetChangePasswordURL();
        string GetInvalidLinkURL();
        string GetCreateAccountURL(string userName);
        string GetOrderDetailsURL(string orderNumber);
        string GetOrderDetailsReportURL(string orderNumber, string referrer = null);
        string GetProofFileURL(string file);
        string GetListEditURL(string listId, string itemId, string referrer = null);
        string GetPendingRegistrationFrontEndURL(string guid);
        string GetPendingRegistrationAdminURL(string guid, bool showrejected = false);
        string GetProfileAccessAdminURL(string userName, string referrer = null);
        string GetPendingRegistrationsReportURL();
        string GetUserOrderSearchURL(string comesFrom, string referrer = null);
        string GetSearchURL(string searchQuery);
        string GetInsertsBundleReportURL();
        string GetCustomizationQueueURL();
        string GetFulfillmentQueueURL();
        string GetManageIssuersURL();
        string GetPromotionReportURL();
        string GetOrderingActivityProgramsReportURL(string referrer = null);
        string GetOrderingActivityDetailProgramReportURL(string relatedProgramId, string referrer = null);
        string GetOrderingActivityIssuersReportURL(string referrer = null);
        string GetOrderingActivityDetailIssuerReportURL(string issuerId, string referrer = null);
        string GetContentActivityReportURL();
        string GetCheckEmailURL(string email, string region);
        string GetInvalidateUserRoleByTokenURL(bool useSecondaryHost = false);
        string GetReloadOfferModelByTokenURL(bool useSecondaryHost = false);
        string GetRelativeReloadOfferModelByTokenURL();
        string GetReloadCategoryModelByTokenURL(bool useSecondaryHost = false);
        string GetRelativeReloadCategoryModelByTokenURL();
        string GetReloadCardExclusivityModelByTokenURL(bool useSecondaryHost = false);
        string GetRelativeReloadCardExclusivityModelByTokenURL();
        string GetReloadEventLocationModelByTokenURL(bool useSecondaryHost = false);
        string GetContentReportLink();
        string GetContentDetailReportLink(DateTime? creationStartDate, DateTime? creationEndDate, string region, string keyword);
        string GetUnauthorizedLink();
        string GetStaticContentCss();
        string GetStaticHeaderCss();
        string GetStaticHeaderJs();
        string GetStaticTriggerMarketingCss();
        string GetStaticFavoritesCss();
        string GetStaticCalendarCss();
        string GetStaticDownloadCalendarCss();
        string GetStaticHomeBannerCss();
        string GetStaticOfferCss();
        string GetStaticShoppingCartCss();
        string GetStaticSearchCss();
        string GetStaticOneTrustCss();
        string GetStaticCssURL(string css);
        string GetStaticImageURL(string image);
        string GetStaticBrokenImageURL(int width);
        string GetLoadingGifURL();
        string GetDefaultMostPopularImageURL(string imageFilename);
        string GetDefaultMyFavoritesImageURL(string imageFilename);
        string GetStaticImageURL(string url, int? width = null, int? height = null, bool crop = false, int jpegQuality = 95, bool rootPath = false);
        string GetStaticTagBrowserJs();
        string GetStaticContentJs();
        string GetStaticTriggerMarketingJs();
        string GetStaticFavoritesJs();
        string GetStaticCalendarJs();
        string GetStaticDownloadCalendarJs();
        string GetStaticVueBundleJs();
        string GetStaticjQueryJs();
        string GetStaticFooterJs();
        string GetStaticjQueryUiJs();
        string GetStaticOfferJs();
        string GetStaticSearchJs();
        string GetStaticOfferDetailsJs();
        string GetStaticCaptchaValidatorJs();
        string GetStaticShoppingCartJs();
        string GetStaticCommonFunctionJs();
        string GetTrackingPixelUrl(string regionId, int mailDispatcherId);
        string SetFrontEndUrl(IEnumerable<KeyValuePair<string, object>> contentData, string frontEndUrl, string contentItemId);
        string GetPricelessApiProductFeedUrl();
        string GetElectronicDeliveryDownloadUrl(string electronicDeliveryId, string regionId);
        string GetShareReportUrl(string shareKey);
    }
}