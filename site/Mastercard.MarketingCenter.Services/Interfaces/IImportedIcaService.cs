﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IImportedIcaService
    {
        ImportedIca GetByCid(string cid);
        IEnumerable<ImportedIca> GetSearchImportedIcas(string searchTerm);
    }
}