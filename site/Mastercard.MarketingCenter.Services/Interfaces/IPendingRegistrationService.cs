﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IPendingRegistrationService
    {
        PendingRegistration GetPendingRegistrationsByGuid(string guid);
        IEnumerable<PendingRegistration> GetPendingRegistrationsByEmail(string email);
        IEnumerable<PendingRegistration> GetPendingRegistrationsByStatus(string status, string regionId);
        void AddPendingRegistration(string firstName, string lastName, string issuerName, string address1, string address2, string address3, string city, int stateID, string zipCode, string phone, string fax, string mobile, string email, string title, string processor, string country, string language, string regionId, string province, string siteName, string acceptedConsents, bool acceptedEmailSubscriptions);
        void RejectPendingRegistration(string guid, string reason, string adminEmail, string regionId, string siteName);
        User ApprovePendingRegistration(string guid, string issuerListItemID, string adminEmail, string domainToAdd, string siteName);
        User ApprovePendingRegistration(string guid, string name, string domains, string ica, string[] segmentationIds, string adminEmail, string processorId, string regionId, int CreatingUserId, string cid, string siteName);
        int ExpireOrphanPendingRegistration(int expirationWindowInMonths);
    }
}