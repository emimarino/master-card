﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IErrorMessageService
    {
         ErrorMessage GetErrorMessage(int listId, string eventName);
    }
}