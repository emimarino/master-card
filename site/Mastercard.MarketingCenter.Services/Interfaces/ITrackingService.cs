﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface ITrackingService
    {
        long CreateVisitId(string urlReferrer);
        void TrackPageVisit(string regionId, long visitID, string what, string who, string from, string how, string contentItemId = null, string pageGroup = null);
        void ProcessContentItemVisits();
        void ProcessSiteTracking();
    }
}