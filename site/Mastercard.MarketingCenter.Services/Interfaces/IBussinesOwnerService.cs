﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IBussinesOwnerService
    {
        IEnumerable<Region> FilterRegionsWithDisabledBOInterface(IEnumerable<Region> regions);
        void DeleteByBusinessOwner(ContentItemBase contentItem);
        void UpdateReviewState(ContentItemBase contentItem, int stateId = 0);

        bool ExtendOrNotExtend(
            IEnumerable<string> contentItemIDs,
            int senderUserId,
            bool shouldExtend,
            string extendMessage,
            int daysToExtend,
            out IEnumerable<Slam.Cms.Data.ContentItem> slamContentItems
        );

        int GetBusinessOwnerId(IDictionary<string, object> contentItemData);

        void SendContentCommentNotificationToAdmin(
            string assetName,
            string contentItemId,
            string comment,
            string region,
            string userFirstName,
            string language = null);

        void SendContentCommentNotificationToBussinessOwner(
            string assetName,
            string contentItemId,
            string comment,
            string region,
            string userFirstName,
            string businessOwnerEmail,
            string language = null);
    }
}