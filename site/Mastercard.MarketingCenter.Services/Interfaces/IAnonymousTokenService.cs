﻿using Mastercard.MarketingCenter.Common.Models;
using System;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IAnonymousTokenService
    {
        /// <summary>
        /// Return Token
        /// </summary>
        /// <param name="userproxy"></param>
        /// <returns>Partial Url containing Token</returns>
        string SetAnonymousTokenData(AnonymousUserProxy userproxy, string region);
        /// <summary>
        /// Gets data related to the annoymous token
        /// </summary>
        /// <param name="token">the token in the request.</param>
        /// <returns>Data related to the token</returns>
        AnonymousUserProxy GetAnonymousTokenData(Guid token);
    }
}