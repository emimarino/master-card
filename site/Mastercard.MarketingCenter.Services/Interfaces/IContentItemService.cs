﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IContentItemService
    {
        IEnumerable<ContentItemCloneToRegion> GetApplicableCloneToRegions(string contentItemId);
        bool IsApplicableCrossBorderRegion(string contentItemId, string regionId);
        IEnumerable<string> GetApplicableCloneToRegionsNames(string contentItemId);
        IEnumerable<BusinessOwnerDTO> GetBusinessOwnerContentAboutToExpire(string regionId);
        ContentItemBase TryToGetDraft(string contentItemId);
        ContentItemBase GetPublishedContentItem(string contentItemId);
        IEnumerable<ContentItem> GetUserOwnedAssetContentItems(int userId, int reviewState = MarketingCenterDbConstants.ExpirationReviewStates.AllAssetsWithAStateAvailable, int? expirationWindowInDays = null, bool onlyPublished = true, string filterContentItemId = "");
        void PopulateComputedDefaultValues(IDictionary<string, object> data, IEnumerable<ContentTypeFieldDefinition> contentTypeFieldDefs, Func<ContentTypeFieldDefinition, bool> filterFields = null, IDictionary<string, object> previousData = null);
        void PopulateDefaultValuesForSuggestion(IDictionary<string, object> data, int userId);
        IEnumerable<ContentItem> GetAssetContentItems(IEnumerable<string> contentItemIds, FilterStatus filterStatus = FilterStatus.LiveOrLiveAndDraft); //if you change this optional value, remeber you will change teh default value for all  implementations
        ContentItem GetAssetContentItem(string contentItemId, FilterStatus filterStatus = FilterStatus.LiveOrLiveAndDraft);
        void UpdateStatus(ContentItemBase contentItemId, int statusId, int userId);
        bool ContentHasLiveVersion(ContentItemBase contentItem);
        IEnumerable<ContentItemSegmentation> GetSegmentation(string contentItemId);
        IEnumerable<MarketingCenter.Data.Entities.Tag> GetTags(string contentItemId);
        AssetFullWidth GetAssetFullWidth(string contentItemId);
        Asset GetAsset(string contentItemId);
        OrderableAsset GetOrderableAsset(string contentItemId);
        DownloadableAsset GetDownloadableAsset(string contentItemId);
        Program GetProgram(string contentItemId);
        Webinar GetWebinar(string contentItemId);
        void DeleteOnlyDraft(ContentItemBase contentItem, int userId);
        ContentItemBase GetContentItem(string contentItemId);
        bool ContentHasMultipleVersions(ContentItemBase contentItem);
        IEnumerable<string> GetContentItemRelatedItems(string contentItemId);
        ContentItem CheckMigratedContentItem(string id);
        string GetOriginalContentItemId(IDictionary<string, object> itemData);
        void SetListItemTrackingFields(IDictionary<string, object> contentData, List<FieldDefinition> fieldDefinitions, int userId);
        void UpdateReviewState(string contentItemId, int state = 0, bool updateAlternateVersion = true);
        void UpdateReviewState(ContentItemBase contentItem, int state = 0, bool updateAlternateVersion = true);
        IEnumerable<Program> GetProgramsByPrintedOrderPeriodEndDate(string regionId, DateTime fromDate, DateTime toDate);
        IEnumerable<ContentItemDTO> GetProgramsByPrintedOrderPeriodEndDateAsContentItemDTOs(string regionId);
        IEnumerable<ContentReportDTO> GetContents(DateTime startDate, DateTime endDate, string title, string regionId = null);
        void PopulateContentItemBaseValues(Dictionary<string, object> newData, string idValue);
    }
}