﻿using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Services.Data;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IEmailService
    {
        string GetTemplate(string keyName, string region = null, string language = null, bool applyLayout = false, bool defaultToSharedTemplate = false);
        void SendContentAboutToExpireEmail(string region, int userId, string toEmail, string userName, IEnumerable<ContentItemDTO> contentAboutAboutToExpireDto);
        void SendContentAboutToExpireAdminEmail(string region, int userId, IEnumerable<ContentItemDTO> contentAboutAboutToExpireDto, IEnumerable<ContentItemDTO> programsAboutToExpire);
        void SendBulkExtensionNotification(IEnumerable<Slam.Cms.Data.ContentItem> assets, bool willExtend, string comment, int daysToExtend, string userName);
        void SendContactUsEmail(string firstName, string lastName, string email, string processor, string issuer, string phone, string message, string subject, string toEmail, string country, string siteName, int userId, string region, string language);
        void SendContentCommentNotification(string templateKey, IEnumerable<string> toAddresses, string assetName, string assetId, string comment, string region, string userFirstName, string editContentItemUrl, string language = null);
        void SendExtensionNotification(string assetName, string assetId, bool willExtend, string comment, int daysToExtend, string region, string userName, string language = null);
        void SendNewAssetNotification(string assetName, string newAssetId, string region, string userName, string language = null);
        void SendModifiedAssetNotification(string assetName, string AssetId, string region, string userName, string language = null);
        void SendNotAssetBusinessOwnerNotification(string contentItemId, string assetTitle, string businessOwnerName, string message, string region, string language = null);
        void SendEmailBadDomainEmail(string firstName, string lastName, string issuerName, string phone, string mobile, string fax, string emailAddress, string address1, string address2, string address3, string city, string state, string zipCode, string country, string siteName, string region, string language);
        void SendEmailVerificationEmail(string toEmail, string firstName, string userName, string siteName, int userId, string region, string language, string lastName = "");
        void SendForgotPasswordEmail(string toEmail, string firstName, string expirationTime, string userKey, string siteName, int userId, string region, string language, string lastName = "");
        void SendPendingRegistrationApprovedEmail(string toEmail, string firstName, string userName, string siteName, int userId, string region, string language, string lastName = "");
        void SendPendingRegistrationRejectedEmail(string toEmail, string rejectionReason, string siteName, int userId, string region, string language);
        void SendNewPendingRegistrationEmail(string firstName, string lastName, string title, string financialInstitution, string processor, string phone, string mobile, string fax, string emailAddress, string address1, string address2, string address3, string city, string province, string stateName, string zip, string country, string guid, string siteName, int userId, string region, string language);
        void SendOrderShippedEmail(string orderno, string Date, string ItemDetails, string Totals, string email, string firstName, string lastName, string shippingDetail, string specialInstructions, bool sendPartialShipmentEmail, int userId);
        void SendProofEmail(string orderno, string email, string firstName, string lastName, int userId);
        void SendProofReminderEmail(string orderno, string email, string firstName, string lastName, string frequencyInDays, int userId);
        void SendProofApprovedEmail(string orderno, int userId);
        void SendPendingCorrectionEmail(string orderno, int userId);
        void SendOrderConfirmationEmail(string orderno, string Date, string ItemDetails, string email, string firstName, string lastName, string shippingNote, string shippingTemplate, bool customizedOrder, bool readyToPrint, string vdpTemplate, string electronicDeliveryTemplate, string estimatedDistributionTemplate, bool requiresApprovalForPOD, int userId);
        void SendElectronicDeliveryOrderApprovedEmail(string orderno, string Date, string ItemDetails, string email, string firstName, string lastName, string electronicDeliveryTemplate, int userId);
        void SendElectronicDeliveryDownloadableAssetEmail(string itemName, string link, DateTime expirationDate, int maxDownloadCount, string email, string fullName, int userId, string region, string language);
        void SendOrderRejectedEmail(string orderno, string rejectionReason, string email, string firstName, string lastName, int userId);
        void SendNewOrderForApprovalEmail(string orderno, string customer, string issuer, string domain, string phone, string email, string address, string city, string state, string zip, List<RetrieveOrderItemsResult> approvableItems, string shippingAddress, int userId);
        void SendNewOrderEmail(string orderNo, bool isCustomizedOrder, bool vdpOrder, int userId, bool isRushOrder);
        void SendOrderStatusEmail(string email, string orderNo, string oldStatus, string newStatus, string notes, int userId);
        void SendOrderStatusEmailForPrinters(string orderNo, string oldStatus, string newStatus, string notes, int userId);
        void SendVdpFtpAccountInformationReceivedEmail(string orderNumber, int userId);
        void SendFtpAccountInformationEmail(string email, string orderNo, int userId);
        void SendVdpOrderCompletedEmail(string email, string orderNumber, int userId);
        void SendVdpAccountNotificationReminderEmail(string orderNumber, string frequencyInDays, int userId);
        void SendVdpAcceptDataFileReminderEmail(string orderNumber, string frequencyInDays, int userId);
        void SendVdpRetrieveFtpAccountReminderEmail(string orderNumber, string email, string firstName, string lastName, string frequencyInDays, int userId);
        void SendEmailSourceUpdateNotification(IEnumerable<IDictionary<string, string>> sourceUpdate, string toEmail, string region, string userName = null, string language = null);
        void WrongICAEmail(string userInfo, string ica, int userId);
        void SendNotificationEmail(string cid, string issuer, string domain, string ica, string name, string email, int userId);
        string GetEmailTemplateHeaderImageUrl(string region, string pathToImage = null);
        void SendLeadGenEmail(string email, string issuer, string name, string message, string reportUrl, int userId);
        void SendShareReportEmail(string email, string issuer, string name, string phone, string pdfFileName, int userId);
        void SendGdprAwarenessFollowUpEmail(string toEmail, int userId, string firstName, DateTime expirationDate, string trackingType, string region, string language, DateTime lastLoginDate);
        MarketingCenter.Data.Entities.MailDispatcher SendGdprDeletedFollowUpEmail(string toEmail, int userId, string firstName, string region, string language, bool isCreating = false);
        void SendUnmatchedPricelessOfferEmail(string unmatchedOffers);
        void SendExceptionPricelessOfferEmail(string exceptionMessage);
        void SendSalesforceExpirationCertificateEmail(DateTime expirationDate);
        void SendSalesforceAwarenessExpirationCertificateEmail(DateTime expirationDate);
    }
}