﻿namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface ICaptchaService
    {
        ICaptcha BindInvisibleReCaptcha();
    }
}