﻿namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IZipFileService
    {
        string GetZipPreviewImage(string zipFileUrl);
        string GetZipCsvFileFromUrl(string zipFileUrl, string zipFileFolder = null);
    }
}