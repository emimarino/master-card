﻿using Mastercard.MarketingCenter.Data.Entities.Reporting;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IReportService
    {
        IEnumerable<RetrieveFilterUsersResult> GetFilterUsers(string name, string email, string issuer, string processorId, string regionId);
        IEnumerable<RetrieveFilterOrdersResult> GetFilterOrders(string orderNo, string name, string email, string issuer, DateTime? startDate, DateTime? endDate, string status, string processorId);
        IEnumerable<RetrieveOrderStatusesResult> GetOrderStatuses(string processorId);
        IEnumerable<UsersSubscriptionReportResultItem> GetUsersSubscriptionReport(DateTime startDate, DateTime endDate, bool filterMasterCardUsers, bool filterVendorProcessor, string regionId);
        IEnumerable<SubscribableItemsDetailReportResultItem> GetSubscribableItemsDetailReport(DateTime startDate, DateTime endDate, string itemId, string regionId);
        IEnumerable<SubscribableItemsDetailReportResultItem> GetSubscribableItemsListDetailReport(DateTime startDate, DateTime endDate, string regionId);
        IEnumerable<TriggerMarketingNotificationsReportResultItem> GetTriggerMarketingNotificationsReport(DateTime startDate, DateTime endDate, string regionId);
        IEnumerable<TriggerMarketingNotificationsDetailReportResultItem> GetTriggerMarketingNotificationsDetailReport(string triggerMarketingSubscription, DateTime notificationDate, string regionId);
        IEnumerable<UserRegistrationReportResultItem> GetUserRegistrationReport(DateTime startDate, DateTime endDate, string region, bool excludeFromDomoApi = false, bool excludeMastercardUsers = true, bool excludeExpiredUsers = true);
        IEnumerable<GdprFollowUpReportResultItem> GetGdprFollowUpReport(string regionId);
        IEnumerable<ProgramPrintOrderDateExpirationReportResultItem> GetProgramPrintOrderDateExpirationReport(string regionId, DateTime fromDate, DateTime toDate);
    }
}