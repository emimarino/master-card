﻿using Mastercard.MarketingCenter.Services.Models;
using Mastercard.MarketingCenter.Services.Services;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IFileUploadService
    {
        void Add(Guid Identifier, string FileName);
        bool ContainsKey(Guid Identifier);
        void DeleteIncompleteUploads();
        UploadFile GetFile(Guid Identifier);
        void RemoveKey(Guid Identifier);
        int CreateUploadActivity(UploadFileInfo fileInfo, string contentItemId, string fieldDefinitionName, int userId, int action, int status);
        void SaveUploadActivityByIds(IEnumerable<int> uploadActivityIds);
    }
}