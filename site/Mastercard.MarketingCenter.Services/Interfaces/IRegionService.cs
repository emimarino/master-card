﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IRegionService
    {
        IEnumerable<Region> GetAll(bool includeGlobal = true);
        Region GetById(string id);
        IEnumerable<State> GetStates();
    }
}