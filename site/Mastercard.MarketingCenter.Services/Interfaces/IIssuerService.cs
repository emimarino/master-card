﻿using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IIssuerService
    {
        Issuer GetIssuerById(string issuerId);
        Issuer GetIssuerByCid(string cid);
        IEnumerable<Issuer> GetAllIssuers();
        IEnumerable<Issuer> GetIssuersByRegion(string regionId);
        IEnumerable<Issuer> GetIssuersBySearchRequest(string regionId, string processorId, string searchText, string showAllValue);
        Issuer GetIssuerByDomain(string domain, string regionId);
        IEnumerable<RetrieveIssuersByProcessorDTO> GetIssuersByProcessor(string processorId, string regionId);
        IEnumerable<Issuer> GetIssuersWithSegmentation(string regionId);
        string CreateIssuer(string title, string domains, string ica, string[] segmentationIds, string processorId, string regionId, int CreatingUserId, string cid = null);
        void AddIssuerDomain(string issuerId, string domain);
        void SetIssuerHasOrdered(string issuerId);
        IssuerData GetIssuerDetail(string issuerId);
        bool IsMastercardIssuer(string issuerId);
        IEnumerable<Issuer> GetMastercardIssuersByRegion(string regionId);
        bool VerifyDomainIsValid(string domain);
        void DeleteIssuer(Issuer issuer);
        Issuer GetIssuerByUserId(int userId);
        void DeleteAllMasterICAs();
        void InsertMasterICAs(List<MasterICA> masterIcas);
        MasterICA GetMasterICAByIca(int ica);
        IEnumerable<MasterICA> GetMasterICAsByBillingId(string billingId);
    }
}