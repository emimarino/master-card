﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Models;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface ISegmentationService
    {
        void InsertIssuerSegmentations(string issuerId, IEnumerable<string> segmentations);
        IEnumerable<Segmentation> GetSegmentationsByRegion(string regionId);
        IEnumerable<Segmentation> GetSegmentationsByUserName(string userName);
        IEnumerable<Segmentation> GetSegmentationsByIds(IEnumerable<string> segmentationIds);
        IEnumerable<Segmentation> GetSegmentationsByEmail(string email);
        IEnumerable<SegmentationModel> GetTranslatedSegmentations(string regionId, string language);
    }
}