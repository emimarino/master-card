﻿using Mastercard.MarketingCenter.Data.Entities;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface ICountryService
    {
        Country GetCountryByIdentifier(string country);
    }
}