﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IContentReviewServices
    {
        IEnumerable<ExpirationReviewState> GetExpirationReviewStates();
    }
}