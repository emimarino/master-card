﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Models;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IAuthorizationService
    {
        bool Authorize(string area, string controller, string action, string id, string queryString = null, string contentAction = null);
        bool Authorize(IDictionary<string, object> routeData, string queryString = null);
        bool AuthorizeBusinessOwnerForDeletion(int userId, ContentItemBase contentItem);
        bool AuthorizeBusinessOwnerForChangingReviewState(int userId, string contentItemId);
        IEnumerable<CmsRoleAccess> GetAuthorizedResourcesByUser();
        IEnumerable<Region> GetAuthorizedRegions();
        IEnumerable<RoleRegion> GetRolesByRegion(string region);
        IEnumerable<Role> GetRegionRolesForCurrentUser();
        UserRoleModel GetCurrentUserRole();
        UserRoleModel GetUserRole(int userId);
        void UpdateUserRole(int userId, int roleId, IEnumerable<string> regionIds);
    }
}