﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IMostPopularService
    {
        MostPopularProcess CreateProcess();
        void ProcessMostPopularActivity(MostPopularProcess mostPopularProcess);
        IEnumerable<MostPopularActivity> GetMostPopularActivitiesByRegion(string regionId);
    }
}