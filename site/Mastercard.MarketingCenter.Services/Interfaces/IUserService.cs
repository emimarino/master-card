﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IUserService
    {
        User GetUser(int userId);
        User GetUserByEmail(string email);
        User GetUserByUserName(string username);
        User CreateUser(string firstName, string lastName, string issuerName, string address1, string address2, string address3, string city, string state, string zipCode, string phone, string fax, string mobile, string email, string issuerId, string processor, string title, string country, string language, string regionId, string acceptedConsents, bool acceptedEmailSubscriptions);
        bool UpdateUser(User user);
        IEnumerable<User> GetAll(bool onlyEnabled = true, bool onlyMastercardUsers = false);
        IEnumerable<UserData> GetActiveUsersByIssuerId(string issuerId);
        Aspnet_Membership GetMembershipUserByEmail(string email);
        Aspnet_Membership GetMembershipUserByUserName(string userName);
        Aspnet_Membership UpdateMembershipUser(Aspnet_Membership membershipUser);
        IEnumerable<User> GetPrinterUsers();
    }
}