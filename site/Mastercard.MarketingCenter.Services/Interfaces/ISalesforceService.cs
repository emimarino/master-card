﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.DTO.Salesforce;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface ISalesforceService
    {
        void SaveUpdates();
        int CreateReport();
        void RefreshSalesforceTasks();
        void RegisterSalesforceAccessToken(SalesforceAccessTokenResponseDTO accessTokenModel);
        void UpdateSalesforceActivityReportById(int salesforceActivityReportId, DateTime? endDate = null, int? sent = null, int? failed = null, bool isSuccessed = false, int? salesforceTokenId = null, string loggingEventId = null);
        IEnumerable<SalesforceTask> GetPendingSalesforceTasks();
        IEnumerable<SalesforceTask> GetAllSalesforceTasks(DateTime from, DateTime to);
        IEnumerable<SalesforceTask> GetCompletedSalesforceTasks(DateTime from, DateTime to);
    }
}