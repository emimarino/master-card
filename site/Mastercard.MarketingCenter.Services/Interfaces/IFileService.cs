﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IFileService
    {
        void InsertDeletedOrphanFile(string folderKey, string fullname);
        void CleanUpReviewedFiles();
        int ReviewFiles(string folder);
        IEnumerable<ViewAllFiles> GetAllFilesByFilename(string filename);
        void MarkReviewedFiles();
        int MoveReviewedFiles(string folder, string newFolder, DateTime fileReviewMoveFolderDate);
        int CopyReviewedFiles(string folder, string newFolder, DateTime fileReviewCopyFolderStartDate, DateTime fileReviewCopyFolderEndDate);
    }
}