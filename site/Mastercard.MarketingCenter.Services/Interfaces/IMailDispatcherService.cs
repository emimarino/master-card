﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IMailDispatcherService
    {
        MailDispatcher GetMailDispatcherById(int mailDispatcherId);
        IEnumerable<MailDispatcher> GetPendingMails();
        MailDispatcher CreateMailDispatcher(string subject, string fromAddress, string fromName, string toAddress, string body, int userId, string regionId, string trackingType, int totalLinks = 0, string ccAddress = null, string bccAddress = null, bool addTrackingPixelUrl = false, int mailDispatcherStatusId = MarketingCenterDbConstants.MailDispatcherStatus.New, bool isBodyHtml = true, string attachmentFilename = null, bool isMultipart = false, string alternativeBody = null, bool saveChanges = true);
        void SaveMailDispatcher(MailDispatcher mailDispatcher);
        bool SendMailDispatcher(MailDispatcher mailDispatcher);
    }
}