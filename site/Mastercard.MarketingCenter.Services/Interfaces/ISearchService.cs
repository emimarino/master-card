﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Models;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface ISearchService
    {
        SearchContentResultModel GetSearchResults(string searchQuery, string selectedRegion, string fullname);
        List<FeaturedSearchResult> GetFeaturedSearchResults(string searchQuery, IEnumerable<string> currentSegmentationIds, string currentSpecialAudience, string regionId);
        IEnumerable<SearchResult> GetSearchResults(string searchQuery, IEnumerable<string> currentSegmentationIds, string currentSpecialAudience, string regionId);
        void AddNewSearchActivity(string userName, DateTime searchDateTime, string searchBoxEntry, string urlVisited, string regionId, int? searchResultsQty);
    }
}