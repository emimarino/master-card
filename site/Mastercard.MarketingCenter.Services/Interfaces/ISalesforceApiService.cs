﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.DTO.Salesforce;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface ISalesforceApiService
    {
        IEnumerable<SalesforceContactQueryResponseDTO> GetSalesforceContacts(IEnumerable<string> emails, bool registerSalesforceAccessToken = false);
        SalesforceTaskResponseDTO SendSalesforceTask(SalesforceTask salesforceTask, bool registerSalesforceAccessToken = false);
        IEnumerable<SalesforceTaskQueryResponseDTO> GetSalesforceTasks(IEnumerable<string> taskIds);
    }
}