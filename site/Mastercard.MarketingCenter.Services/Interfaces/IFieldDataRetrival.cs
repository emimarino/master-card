﻿using Mastercard.MarketingCenter.Data.Entities;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IFieldDataRetrieval
    {
        bool ValidateFieldType(FieldDefinition fieldDefinition);
        string GetSerializedEntity(KeyValuePair<string, object> originalData, FieldDefinition fieldDefinition, Func<object, string> serializeMethod);
    }
}