﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IDefaultValueCalculator
    {
        void CalculateDefault(IEnumerable<ContentTypeFieldDefinition> contentFieldDef, IDictionary<string, object> data, IDictionary<string, object> previousData = null);
    }
}