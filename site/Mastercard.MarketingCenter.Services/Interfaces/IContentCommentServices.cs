﻿using Mastercard.MarketingCenter.Services.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IContentCommentServices
    {
        int GetCommentsByContent(string contentItemId);
        bool AddNew(int senderUserId, string contentTypeId, string contentItemId, string commentBody);
        bool AddNewComments(int senderUserId, IEnumerable<ContentCommentDTO> contentComments);
        string GetExtensionComment(int numOfDays, string commentBody);
        ContentCommentModelPagedList GetPaged(string contentTypeId, string contentItemId, int pageNumber, int pageSize);
        IEnumerable<ContentCommentsCount> GetCommentsCount(int userId);
    }
}