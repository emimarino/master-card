﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface ITagService
    {
        Dictionary<string, string> GetCountries(string regionId, string language);
        Dictionary<string, string> GetAllCountries(string language);
        Dictionary<string, string> GetCountries(string regionId, string language, Slam.Cms.Data.TagTree tagTree);
        Dictionary<string, string> GetLanguages(string regionId, IDictionary<string, string> languageCodes);
        Dictionary<string, string> GetLanguages(string regionId, IDictionary<string, string> languageCodes, Slam.Cms.Data.TagTree tagTree);
        IEnumerable<Tag> GetTags(params string[] tagIds);
        IEnumerable<Tag> GetTagsByIdentifier(params string[] identifiers);
        IEnumerable<Tag> GetTagsByTagCategory(string tagCategoryId);
        Dictionary<string, string> GetTagsByIdentifiersAndLanguage(string[] identifiers, string languageCode);
    }
}