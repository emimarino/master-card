﻿using System;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IRegionalizeService
    {
        bool IsUsBaseUrl(Uri url);
        string GetLanguageTitle(string key);
        string GetCountryName(string key);
        string RegionalizeResource(string resourceFile, string resourceName, string regionId, string languageId, string appSettingKey = null, bool canDefault = true);
        string GetSalesforceResource(string resourceName, string regionId);
    }
}