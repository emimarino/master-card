﻿using System.Collections.Generic;
using System.IO;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface ICsvFileService
    {
        IEnumerable<T> ReadCsv<T>(Stream csvFileStream) where T : class;
        IEnumerable<T> ReadCsv<T>(string csvFilePath) where T : class;
    }
}