﻿using Mastercard.MarketingCenter.DTO.GDP;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IUserPrivacyDataService
    {
        string MaskUserData(string email, out ExpiredUserDTO expiredUser);
        UserDTO GetByEmail(string email);
    }
}