﻿namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IPricelessApiService
    {
        string GetProductFeedUrl();
    }
}