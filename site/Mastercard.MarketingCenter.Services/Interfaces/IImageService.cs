﻿using System.IO;
using System.Web;

namespace Mastercard.MarketingCenter.Services.Interfaces
{
    public interface IImageService
    {
        string Resize(string filename, int width, int height, string destinationPath = null, bool crop = false, int jpegQuality = 100, bool rootPath = false);
        Stream GetImage(string filename, int width, int height, bool crop, int jpegQuality, bool rootPath);
        FileInfo GetFileInfo(string filename);
        string SaveImage(HttpPostedFileBase file);
        Stream GetMappedImage(string imagePath, bool mapToImagesFolder = false);
        string GetImageFromUrl(string imageUrl, bool returnFullPath = true);
    }
}