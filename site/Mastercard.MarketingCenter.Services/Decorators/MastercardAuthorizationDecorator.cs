﻿using Mastercard.MarketingCenter.Services.Interfaces;

namespace Mastercard.MarketingCenter.Services.Decorators
{
    public abstract class MastercardAuthorizationDecorator
    {
        protected IAuthorizationService AuthorizationService;

        public MastercardAuthorizationDecorator(IAuthorizationService authorizationService)
        {
            AuthorizationService = authorizationService;
        }

        public abstract bool Authorize(string area, string controller, string action, string id, string queryString = null, string contentAction = null);
    }
}