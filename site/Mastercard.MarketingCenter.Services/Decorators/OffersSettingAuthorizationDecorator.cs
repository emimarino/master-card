﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services.Interfaces;

namespace Mastercard.MarketingCenter.Services.Decorators
{
    public class OffersSettingAuthorizationDecorator : MastercardAuthorizationDecorator
    {
        private readonly ISettingsService _settingsService;
        private readonly UserContext _userContext;

        public OffersSettingAuthorizationDecorator(IAuthorizationService AuthorizationService, ISettingsService settingsService, UserContext userContext) : base(AuthorizationService)
        {
            _settingsService = settingsService;
            _userContext = userContext;
        }

        public override bool Authorize(string area, string controller, string action, string contentType, string queryString = null, string contentAction = null)
        {
            if (!string.IsNullOrEmpty(contentType) && contentType.Equals(MarketingCenterDbConstants.ContentTypeIds.Offer))
            {
                return _settingsService.GetOffersEnabled(_userContext.SelectedRegion) ? AuthorizationService.Authorize(area, controller, action, contentType, queryString, contentAction) : false;
            }
            else
            {
                return AuthorizationService.Authorize(area, controller, action, contentType, queryString, contentAction);
            }
        }
    }
}