﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services
{
    public class ContentReviewServices : IContentReviewServices
    {
        private readonly Mastercard.MarketingCenter.Data.IContentReviewRepository _contentReviewRepository;
        public ContentReviewServices(IContentReviewRepository contentReviewRepository)
        {
            _contentReviewRepository = contentReviewRepository;
        }

        public IEnumerable<ExpirationReviewState> GetExpirationReviewStates()
        {
            return _contentReviewRepository.GetExpirationReviewStates();
        }
    }
}