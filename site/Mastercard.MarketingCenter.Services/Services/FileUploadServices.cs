﻿using AutoMapper;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Interfaces;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Mastercard.MarketingCenter.Services.Services
{
    public class FileUploadService : IFileUploadService
    {
        private IList<UploadFile> db;
        private readonly IUploadActivityRepository _uploadActivityRepository;
        private readonly IMapper _mapper;

        public FileUploadService(IUploadActivityRepository uploadActivityRepository, IMapper mapper)
        {
            db = new List<UploadFile>();
            _uploadActivityRepository = uploadActivityRepository;
            _mapper = mapper;
        }

        public bool ContainsKey(Guid Identifier)
        {
            return db.Any(f => f.Identifier == Identifier);
        }

        public void Add(Guid Identifier, string FileName)
        {
            DeleteIncompleteUploads();
            var uploadFile = new UploadFile(Identifier, FileName);
            db.Add(uploadFile);
        }

        public UploadFile GetFile(Guid Identifier)
        {
            return db.First(f => f.Identifier == Identifier);
        }

        public void RemoveKey(Guid Identifier)
        {
            db.Remove(db.First(f => f.Identifier == Identifier));
        }

        /// <summary>
        /// This method frees up memory for failed uploads
        /// </summary>
        public void DeleteIncompleteUploads()
        {
            db.Where(f => f.IsExpired()).ToList().ForEach(f => RemoveKey(f.Identifier));
        }

        public int CreateUploadActivity(UploadFileInfo fileInfo, string contentItemId, string fieldDefinitionName, int userId, int action, int status)
        {
            if (fileInfo != null)
            {
                var uploadActivityDto = new UploadActivityDTO
                {
                    ContentItemId = contentItemId,
                    Filename = fileInfo.name.Split(new char[] { '/' }).LastOrDefault(),
                    FileReference = fileInfo.name,
                    Size = fileInfo.size.Value,
                    FieldDefinitionName = fieldDefinitionName,
                    ActivityUserId = userId,
                    Action = action,
                    Status = status
                };

                return _uploadActivityRepository.AddUploadActivity(_mapper.Map<UploadActivity>(uploadActivityDto));
            }

            return 0;
        }

        public void SaveUploadActivityByIds(IEnumerable<int> uploadActivityIds)
        {
            foreach (var uploadActivityId in uploadActivityIds)
            {
                var uploadActivity = _uploadActivityRepository.GetUploadActivityById(uploadActivityId);
                if (uploadActivity != null)
                {
                    uploadActivity.DateSaved = DateTime.Now;
                    if (uploadActivity.UploadActivityStatusId == MarketingCenterDbConstants.UploadActivity.Status.Pending)
                    {
                        uploadActivity.UploadActivityStatusId = MarketingCenterDbConstants.UploadActivity.Status.Saved;
                    }
                }
            }
            _uploadActivityRepository.Commit();
        }
    }

    public class UploadFile
    {
        public Guid Identifier { get; set; }
        public string FileName { get; set; }
        private DateTime LastUpdate { get; set; }
        private IDictionary<int, byte[]> Chunks;
        private double _minutesToExpire = 10;

        public UploadFile(Guid identifier, string fileName)
        {
            Chunks = new Dictionary<int, byte[]>();

            Identifier = identifier;
            FileName = fileName;
        }

        public bool ContainsChunk(int chunk)
        {
            return Chunks.ContainsKey(chunk);
        }

        public void Add(int chunk, byte[] data)
        {
            Chunks.Add(chunk, data);
            LastUpdate = DateTime.Now;
        }

        public int Count()
        {
            return Chunks.Count;
        }

        public byte[] GetBytes()
        {
            byte[] rv = new byte[Chunks.Values.Sum(a => a.Length)];
            int offset = 0;
            foreach (byte[] array in Chunks.Values)
            {
                System.Buffer.BlockCopy(array, 0, rv, offset, array.Length);
                offset += array.Length;
            }
            return rv;
        }

        public string Save(string path, string FileName)
        {
            var guid = Guid.NewGuid().ToString();
            var postedFile = Path.GetFileName(FileName);
            var normalizedFileName = postedFile.Normalize();
            var filename = Path.Combine(guid, normalizedFileName);
            var finalPath = Path.Combine(Path.GetDirectoryName(path), filename);
            var finalDir = Path.GetDirectoryName(finalPath);

            if (!Directory.Exists(finalDir))
            {
                Directory.CreateDirectory(finalDir);
            }

            using (FileStream fileStream = new FileStream(finalPath, FileMode.Append))
            {
                foreach (var chunk in Chunks.OrderBy(c => c.Key))
                {
                    var bytes = chunk.Value;
                    fileStream.Write(bytes, 0, bytes.Count());
                }
            }
            return filename;
        }
        public bool IsExpired()
        {
            return LastUpdate.AddMinutes(_minutesToExpire) < DateTime.Now;
        }
    }
}