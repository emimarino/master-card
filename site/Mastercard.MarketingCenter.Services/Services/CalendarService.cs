﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class CalendarService : ICalendarService
    {
        private readonly ICampaignCategoryRepository _campaignCategoryRepository;
        private readonly ICampaignEventRepository _campaignEventRepository;
        private readonly IDownloadCalendarKeyRepository _downloadCalendarKeyRepository;

        public CalendarService(
            ICampaignCategoryRepository campaignCategoryRepository,
            ICampaignEventRepository campaignEventRepository,
            IDownloadCalendarKeyRepository downloadCalendarKeyRepository)
        {
            _campaignCategoryRepository = campaignCategoryRepository;
            _campaignEventRepository = campaignEventRepository;
            _downloadCalendarKeyRepository = downloadCalendarKeyRepository;
        }

        public IEnumerable<CampaignCategory> GetCampaignCategoriesByRegionAndLanguage(string regionId, string languageCode)
        {
            return _campaignCategoryRepository.GetCampaignCategoriesByRegionAndLanguage(regionId, languageCode);
        }

        public IEnumerable<CampaignCategory> GetCampaignCategoriesByIds(string[] campaignCategoryIds)
        {
            return _campaignCategoryRepository.GetAll()
                                              .FilterByIds(campaignCategoryIds);
        }

        public IEnumerable<ContentItemCampaignEvent> GetContentItemCampaignEventsFilterByIds(string[] contentItemIds)
        {
            return _campaignEventRepository.FilterMarketingCalendarEnabled(_campaignEventRepository.GetContentItemCampaignEvents()
                                                                                                   .Where(pce => contentItemIds.Contains(pce.ContentItemId)));
        }

        public static bool ContainsPeriod(DateTime outerPeriodStart, DateTime outerPeriodEnd, DateTime innerPeriodStart, DateTime innerPeriodEnd, bool openPeriod = false)
        {
            var outerPeriodStartBeforeInner = openPeriod ? (outerPeriodStart < innerPeriodStart) : (outerPeriodEnd <= innerPeriodStart);
            var outerPeriodEndsAfterInner = openPeriod ? (outerPeriodStart > innerPeriodEnd) : (outerPeriodEnd >= innerPeriodEnd);

            return outerPeriodStartBeforeInner && outerPeriodEndsAfterInner;
        }

        private bool PeriodContainsBoundaryDateOrIsWithinBoundary(ContentItemForCalendar contentItemForCalendar, CampaignEvent campaignEvent, DateTime startDate, DateTime endDate)
        {
            var result = false;
            if (contentItemForCalendar != null)
            {
                result = (campaignEvent.AlwaysOn ?
                 (contentItemForCalendar.InMarketStartDate.Value.IsBetween(startDate, endDate) || contentItemForCalendar.InMarketEndDate.Value.IsBetween(startDate, endDate) //by bolzano theorem if either endpoint is in the period the intersection is ensured
                  || ContainsPeriod(startDate, endDate, contentItemForCalendar.InMarketStartDate.Value, contentItemForCalendar.InMarketEndDate.Value)//from here on we ensure if either is subset of the other even if their boundaries do not touch
                  || ContainsPeriod(contentItemForCalendar.InMarketStartDate.Value, contentItemForCalendar.InMarketEndDate.Value, startDate, endDate))
                  :
                  (campaignEvent.StartDate.Value.IsBetween(startDate, endDate) || campaignEvent.EndDate.Value.IsBetween(startDate, endDate) //by bolzano theorem if either endpoint is in the period the intersection is ensured
                  || ContainsPeriod(startDate, endDate, campaignEvent.StartDate.Value, campaignEvent.EndDate.Value)
                  || ContainsPeriod(campaignEvent.StartDate.Value, campaignEvent.EndDate.Value, startDate, endDate))
                  );
            }

            return result;
        }

        public IEnumerable<ContentItemCampaignEvent> GetContentItemCampaignEventsFilterByIds(string[] contentItemIds, DateTime startDate, DateTime endDate)
        {
            return GetContentItemCampaignEventsFilterByIds(contentItemIds).Where(pce => PeriodContainsBoundaryDateOrIsWithinBoundary(pce.ContentItemForCalendar, pce.CampaignEvent, startDate, endDate));
        }

        public DownloadCalendarKey CreateDownloadCalendarKey(int userId, string region, string language, string specialAudience, string segmentationIds, string campaignCategoryIds, string marketIdentifiers, int downloadYear)
        {
            return _downloadCalendarKeyRepository.InsertDownloadCalendarKey(
                new DownloadCalendarKey
                {
                    UserId = userId,
                    RegionId = region,
                    Language = language,
                    SpecialAudience = specialAudience,
                    SegmentationIds = segmentationIds,
                    DownloadCalendarKeyStatusId = MarketingCenterDbConstants.UserKeyStatus.Available,
                    Key = Guid.NewGuid().ToString(),
                    CampaignCategoryIds = campaignCategoryIds,
                    MarketIdentifiers = marketIdentifiers,
                    DateKeyGenerated = DateTime.Now,
                    DownloadYear = downloadYear
                });
        }

        public DownloadCalendarKey GetDownloadCalendarKey(string key)
        {
            return _downloadCalendarKeyRepository.GetDownloadCalendarKeyByKey(key);
        }

        public IEnumerable<CampaignEvent> GetCampaignEventsFilterByIds(string[] campaignEventIds)
        {
            return _campaignEventRepository.GetCampaignEvents()
                .Include(ce => ce.CampaignCategories.Select(ct => ct.CampaignCategoryTranslatedContents))
                .Where(e => campaignEventIds.Contains(e.CampaignEventId));
        }

        public bool DeleteCampaignCategory(string campaignCategoryId)
        {
            var campaignCategory = _campaignCategoryRepository.GetCampaignCategoryById(campaignCategoryId);
            if (!campaignCategory.CampaignEvents.Any(ce => ce.ContentItemCampaignEvents.Any(cice => !cice.ContentItemForCalendar.ContentItem.StatusID.Equals(Status.Deleted))))
            {
                _campaignCategoryRepository.DeleteCampaignCategory(campaignCategory);
                return true;
            }

            return false;
        }
    }
}