﻿using Mastercard.MarketingCenter.Services.Data;
using System;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public static class FtpAccountServices
    {
        public static FtpAccount GetFtpAccountInformation(int orderId, string userId)
        {
            MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();
            var ftp = dataContext.FtpAccounts
                .Join(dataContext.Orders,
                    f => f.OrderID,
                    o => o.OrderID,
                    (f, o) => new { FtpAccount = f, VdpOrder = o })
                .SingleOrDefault(i => i.VdpOrder.UserID == userId && i.VdpOrder.OrderID == orderId);

            if (ftp != null)
            {
                return ftp.FtpAccount;
            }

            return null;
        }

        public static void ClearFtpAccountInformation(int ftpAccountId)
        {
            MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();
            FtpAccount ftp = dataContext.FtpAccounts.SingleOrDefault(f => f.FtpAccountID == ftpAccountId);

            if (ftp != null)
            {
                ftp.Server = String.Empty;
                ftp.UserName = String.Empty;
                ftp.Password = String.Empty;
                dataContext.SubmitChanges();
            }
        }
    }
}
