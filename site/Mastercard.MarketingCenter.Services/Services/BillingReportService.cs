﻿using Mastercard.MarketingCenter.Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public static class BillingReportService
    {
        public static List<RetrieveOrderMonthsResult> GetOrderMonths()
        {
            MasterCardPortalDataContext dc = new MasterCardPortalDataContext();
            return dc.RetrieveOrderMonths().ToList();
        }

        public static List<RetrieveOrderDetailReportResult> GetOrderDetailReport(DateTime orderDate)
        {
            MasterCardPortalDataContext dc = new MasterCardPortalDataContext();
            return dc.RetrieveOrderDetailReport(orderDate).ToList();
        }

        public static List<RetrieveOrderDetailRangeReportResult> GetOrderDetailRangeReport(DateTime startDate, DateTime endDate)
        {
            MasterCardPortalDataContext dc = new MasterCardPortalDataContext();
            return dc.RetrieveOrderDetailRangeReport(startDate, endDate).ToList();
        }
    }
}
