﻿using Mastercard.MarketingCenter.DTO.Priceless;
using Mastercard.MarketingCenter.Priceless.Api.OAuth1Signer.Handlers;
using Mastercard.MarketingCenter.Priceless.Api.OAuth1Signer.Utils;
using Mastercard.MarketingCenter.Services.Interfaces;
using Newtonsoft.Json;
using Pulsus;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;

namespace Mastercard.MarketingCenter.Services
{
    public class PricelessApiService : IPricelessApiService
    {
        private readonly IUrlService _urlService;
        private readonly ISettingsService _settingsService;
        private readonly string _consumerKey;
        private readonly string _signingKeyAlias;
        private readonly string _signingKeyPassword;
        private readonly string _pkcs12KeyFilePath;
        private readonly string _partnerId;
        private readonly string _secretKey;

        public PricelessApiService(IUrlService urlService, ISettingsService settingsService)
        {
            _urlService = urlService;
            _settingsService = settingsService;
            _consumerKey = _settingsService.GetPricelessApiClientConsumerKey();
            _signingKeyAlias = _settingsService.GetPricelessApiClientSigningKeyAlias();
            _signingKeyPassword = _settingsService.GetPricelessApiClientSigningKeyPassword();
            _pkcs12KeyFilePath = _settingsService.GetPricelessApiClientPkcs12KeyFilePath();
            _partnerId = _settingsService.GetPricelessApiClientPartnerId();
            _secretKey = _settingsService.GetPricelessApiClientSecretKey();
        }

        public string GetProductFeedUrl()
        {
            return GetPricelessApiResponse<ProductFeedUrlDTO>(_urlService.GetPricelessApiProductFeedUrl())?.Url;
        }

        private T GetPricelessApiResponse<T>(string url) where T : PricelessApiDTO
        {
            T model = null;
            if (File.Exists(_pkcs12KeyFilePath))
            {
                int unixTime = (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
                string time = unixTime.ToString();
                var signingKey = SecurityUtils.LoadPrivateKey(_pkcs12KeyFilePath, _signingKeyAlias, _signingKeyPassword);
                string rawSignature = $"{time}_{_partnerId}_{_secretKey}";
                var sha256 = SHA256.Create();
                var inputBytes = Encoding.UTF8.GetBytes(rawSignature);
                var hash = sha256.ComputeHash(inputBytes);
                var signature = BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
                var payload = $"{{\"partnerId\":{_partnerId}, \"time\":{time}, \"signature\":\"{signature}\"}}";
                var httpContent = new StringContent(payload, Encoding.UTF8, "application/json");

                using (var client = new HttpClient(new RequestSignerHandler(_consumerKey, signingKey)))
                {
                    client.DefaultRequestHeaders.Add("Referer", ".mastercard.com");
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                    var task = client.PostAsync(url, httpContent)
                                     .ContinueWith((response) =>
                                     {
                                         if (response.Status == System.Threading.Tasks.TaskStatus.RanToCompletion && response.Result.IsSuccessStatusCode)
                                         {
                                             model = JsonConvert.DeserializeObject<T>(response.Result.Content.ReadAsStringAsync().Result);
                                             if (model.Data is bool && model.Data == false)
                                             {
                                                 LogManager.EventFactory.Create()
                                                                    .Text("Error in Priceless Api Endpoint - No Data on the Priceless Api Response")
                                                                    .Level(LoggingEventLevel.Error)
                                                                    .AddTags("priceless_api")
                                                                    .AddData("Priceless Api Url", url)
                                                                    .AddData("Http Content", httpContent)
                                                                    .AddData("Response Status", response.Status.ToString())
                                                                    .AddData("Priceless Api Status Code", model.Code)
                                                                    .AddData("Priceless Api Status Message", model.Message)
                                                                    .Push();
                                                 model = null;
                                             }
                                         }
                                         else
                                         {
                                             LogManager.EventFactory.Create()
                                                                    .Text("Error in Priceless Api Endpoint - Response Error")
                                                                    .Level(LoggingEventLevel.Error)
                                                                    .AddTags("priceless_api")
                                                                    .AddData("Priceless Api Url", url)
                                                                    .AddData("Http Content", httpContent)
                                                                    .AddData("Response Status", response.Status.ToString())
                                                                    .AddData("Priceless Api Status Code",
                                                                             response.Status == System.Threading.Tasks.TaskStatus.RanToCompletion ?
                                                                             response.Result.StatusCode.ToString() : HttpStatusCode.InternalServerError.ToString())
                                                                    .Push();
                                             model = null;
                                         }
                                     });
                    task.Wait();
                }
            }
            else
            {
                LogManager.EventFactory.Create()
                                       .Text("Error in Priceless Api Endpoint - The system cannot find the Pkcs12 Key File")
                                       .Level(LoggingEventLevel.Error)
                                       .AddTags("priceless_api")
                                       .AddData("Priceless Api Url", url)
                                       .AddData("Priceless Api Pkcs12 Key File Path", _pkcs12KeyFilePath)
                                       .Push();
            }

            return model;
        }
    }
}