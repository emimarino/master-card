﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities.Reporting;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Services.Services
{
    public class ReportService : IReportService
    {
        private readonly ReportRepository _reportRepository;
        private IContentItemService _contentItemService
        {
            get
            {
                return DependencyResolver.Current.GetService<IContentItemService>();
            }
        }

        public ReportService(ReportRepository reportRepository)
        {
            _reportRepository = reportRepository;
        }

        public IEnumerable<RetrieveFilterUsersResult> GetFilterUsers(string name, string email, string issuer, string processorId, string regionId)
        {
            return _reportRepository.GetFilterUsers(name, email, issuer, processorId, regionId);
        }

        public IEnumerable<RetrieveFilterOrdersResult> GetFilterOrders(string orderNo, string name, string email, string issuer, DateTime? startDate, DateTime? endDate, string status, string processorId)
        {
            return _reportRepository.GetFilterOrders(orderNo, name, email, issuer, startDate, endDate, status, processorId);
        }

        public IEnumerable<RetrieveOrderStatusesResult> GetOrderStatuses(string processorId)
        {
            return _reportRepository.GetOrderStatuses(processorId);
        }

        public IEnumerable<UsersSubscriptionReportResultItem> GetUsersSubscriptionReport(DateTime startDate, DateTime endDate, bool filterMasterCardUsers, bool filterVendorProcessor, string regionId)
        {
            var contentItemUsersSubscriptions = _reportRepository.GetContentItemUsersSubscriptions(startDate, endDate, filterMasterCardUsers, filterVendorProcessor, regionId);
            return (from us in contentItemUsersSubscriptions
                    group us by new
                    {
                        us.UserId,
                        us.UserName,
                        us.IssuerId,
                        us.IssuerName,
                        us.Frequency
                    }
                    into s
                    select new UsersSubscriptionReportResultItem
                    {
                        UserId = s.Key.UserId,
                        UserName = s.Key.UserName,
                        IssuerName = s.Key.IssuerName,
                        Frequency = s.Key.Frequency,
                        Favorites = s.Count(u => u.UserId == s.Key.UserId),
                        LastSavedDate = s.Max(u => u.SavedDate)
                    })
                    .OrderByDescending(us => us.LastSavedDate);
        }

        public IEnumerable<SubscribableItemsDetailReportResultItem> GetSubscribableItemsDetailReport(DateTime startDate, DateTime endDate, string itemId, string regionId)
        {
            return _reportRepository.GetContentItemSubscribableItems(startDate, endDate, itemId, regionId).OrderByDescending(ci => ci.SavedDate);
        }

        public IEnumerable<SubscribableItemsDetailReportResultItem> GetSubscribableItemsListDetailReport(DateTime startDate, DateTime endDate, string regionId)
        {
            return _reportRepository.GetContentItemListSubscribableItems(startDate, endDate, regionId).OrderByDescending(ci => ci.SavedDate);
        }

        public IEnumerable<TriggerMarketingNotificationsReportResultItem> GetTriggerMarketingNotificationsReport(DateTime startDate, DateTime endDate, string regionId)
        {
            var triggerMarketingNotifications = _reportRepository.GetTriggerMarketingNotifications(startDate, endDate, regionId);
            var clickedLinks = _reportRepository.GetTriggerMarketingNotificationClickedLinks(startDate);

            return (from tmn in triggerMarketingNotifications
                    group tmn by new
                    {
                        tmn.Date.Date,
                        tmn.TrackingType
                    }
               into tm
                    select new TriggerMarketingNotificationsReportResultItem
                    {
                        NotificationDate = tm.Key.Date,
                        NotificationFrequency = tm.Key.TrackingType == Common.Infrastructure.Constants.TrackingTypes.TriggerMarketingSubscriptionDaily ? "Daily" : "Weekly",
                        TrackingType = tm.Key.TrackingType,
                        Recipients = tm.Select(n => n.UserId).Distinct().Count(),
                        OpenEmailRate = tm.Any() ? decimal.Divide(tm.Count(n => n.Opened), tm.Count()) : 0,
                        ClickRate = tm.Sum(n => n.TotalLinks) > 0 ? decimal.Divide(tm.Sum(n => clickedLinks.Where(l => l.MailDispatcherId == n.MailDispatcherId.ToString()).Select(l => l.LinkNumber).Distinct().Count()), tm.Sum(n => n.TotalLinks)) : 0
                    })
                    .OrderBy(n => n.NotificationDate);
        }

        public IEnumerable<TriggerMarketingNotificationsDetailReportResultItem> GetTriggerMarketingNotificationsDetailReport(string triggerMarketingSubscription, DateTime notificationDate, string regionId)
        {
            var triggerMarketingNotificationsDetail = _reportRepository.GetTriggerMarketingNotificationsDetail(triggerMarketingSubscription, notificationDate, notificationDate, regionId).ToList();
            var clickedLinks = _reportRepository.GetTriggerMarketingNotificationClickedLinks(notificationDate);

            foreach (var notification in triggerMarketingNotificationsDetail)
            {
                notification.ClickRate = notification.TotalLinks > 0 ? decimal.Divide(clickedLinks.Where(l => l.TriggerMarketingSubscription == triggerMarketingSubscription && l.MailDispatcherId == notification.MailDispatcherId.ToString()).Select(l => l.LinkNumber).Distinct().Count(), notification.TotalLinks) : 0;
            }

            return triggerMarketingNotificationsDetail.OrderByDescending(n => n.ClickRate).ThenBy(n => n.UserName);
        }

        public IEnumerable<UserRegistrationReportResultItem> GetUserRegistrationReport(DateTime startDate, DateTime endDate, string region, bool excludeFromDomoApi = false, bool excludeMastercardUsers = true, bool excludeExpiredUsers = true)
        {
            return _reportRepository.GetUserRegistrationReport(startDate, endDate, region, excludeFromDomoApi, excludeMastercardUsers, excludeExpiredUsers);
        }

        public IEnumerable<GdprFollowUpReportResultItem> GetGdprFollowUpReport(string region)
        {
            return _reportRepository.GetGdprFollowUpReport(region, new string[] { Constants.TrackingTypes.GdprFirstFollowUp, Constants.TrackingTypes.GdprSecondFollowUp });
        }

        public IEnumerable<ProgramPrintOrderDateExpirationReportResultItem> GetProgramPrintOrderDateExpirationReport(string regionId, DateTime fromDate, DateTime toDate)
        {
            return _contentItemService.GetProgramsByPrintedOrderPeriodEndDate(regionId, fromDate, toDate)
                                      .Select(p => new ProgramPrintOrderDateExpirationReportResultItem
                                      {
                                          ContentItemId = p.ContentItemId,
                                          Title = p.Title,
                                          PrintedOrderPeriodEndDate = p.PrintedOrderPeriodEndDate,
                                          InMarketEndDate = p.InMarketEndDate,
                                          ExpirationDate = p.ContentItem.ExpirationDate,
                                          ModifiedDate = p.ContentItem.ModifiedDate
                                      });
        }
    }
}