﻿using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;

namespace Mastercard.MarketingCenter.Services
{
    public class SettingsService : ISettingsService
    {
        public string GetSiteName()
        {
            var resources = new System.Resources.ResourceManager("Resources.Shared", Assembly.Load("App_GlobalResources"));
            return resources.GetString(Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["ApplicationNameResource"]) ?? resources.GetString("MosSiteName");
        }

        public IEnumerable<string> GetSiteApplicableRegions()
        {
            return Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["ApplicableRegions"].Split(',');
        }

        public string GetSiteInApplicableRegionEnvironment()
        {
            return Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["InApplicableRegionEnvironment"];
        }

        public string GetSiteApplicationShortName()
        {
            return Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["ApplicationShortName"];
        }

        public string GetTrackMailDispatcherUrl()
        {
            return ConfigurationManager.AppSettings["TrackMailDispatcher.Url"];
        }

        public IEnumerable<string> GetPhysicalFileUrls()
        {
            return ConfigurationManager.AppSettings["PhysicalFile.Urls"].ToString().Split(',');
        }

        private int GetIntSetting(string key, int? defaultValue = null)
        {
            if (!int.TryParse(ConfigurationManager.AppSettings[key], out int value) && defaultValue != null)
            {
                value = (int)defaultValue;
            }

            return value;
        }

        private bool GetBoolSetting(string key, bool? defaultValue = null, bool canDefault = true, string regionId = null)
        {
            if (!bool.TryParse(RegionalizeService.RegionalizeSetting(key, regionId, canDefault), out bool value) && defaultValue != null)
            {
                value = (bool)defaultValue;
            }

            return value;
        }

        private DateTime GetDateTimeSetting(string settingKey, string regionId = null)
        {
            DateTime.TryParse(RegionalizeService.RegionalizeSetting(settingKey, regionId), out DateTime dateTime);
            return dateTime;
        }

        public bool GetBusinessOwnerExpirationManagementEnabled(string regionId)
        {
            return GetBoolSetting("BusinessOwnerExpirationManagement.Enabled", false, true, regionId);
        }

        public bool GetProgramPrintOrderDateExpirationEnabled(string regionId)
        {
            return GetBoolSetting("ProgramPrintOrderDateExpiration.Enabled", false, true, regionId);
        }

        public string GetContentCommentNotificationContactUsAddress(string regionId)
        {
            return RegionalizeService.RegionalizeSetting($"ContentCommentNotification.ContactUsAddress", regionId);
        }

        public IEnumerable<string> GetTemplateToAddresses(string regionId, string templateKey)
        {
            return RegionalizeService.RegionalizeSetting($"{templateKey}.ToAddress", regionId).Split(';');
        }

        public string GetSlamSolutionSetting(string key)
        {
            return Slam.Cms.Configuration.ConfigurationManager.Solution.Settings[key];
        }

        public string GetUploadedFoldersKeys()
        {
            return GetSlamSolutionSetting("UploadedFoldersKeys");
        }

        public string GetFilesFolder()
        {
            return GetSlamSolutionSetting("FilesFolder");
        }

        public string GetImagesFolder()
        {
            return ConfigurationManager.AppSettings["ImagesFolder"];
        }

        public string GetThumbnailsFolder()
        {
            return ConfigurationManager.AppSettings["ThumbnailsFolder"];
        }

        public string GetDownloadImagesFolder()
        {
            return ConfigurationManager.AppSettings["DownloadImagesFolder"];
        }

        public string GetExcelFolder()
        {
            return ConfigurationManager.AppSettings["ExcelFolder"];
        }

        public IEnumerable<string> GetFileReviewFolders()
        {
            return ConfigurationManager.AppSettings["FileReview.Folders"].Split('|');
        }

        public string GetFileReviewMoveFolder()
        {
            return ConfigurationManager.AppSettings["FileReview.MoveFolder"];
        }

        public string GetFileReviewCopyFolder()
        {
            return ConfigurationManager.AppSettings["FileReview.CopyFolder"];
        }

        public DateTime GetFileReviewMoveFolderDate()
        {
            return GetDateTimeSetting("FileReview.MoveFolderDate");
        }

        public DateTime GetFileReviewCopyFolderStartDate()
        {
            return GetDateTimeSetting("FileReview.CopyFolderStartDate");
        }

        public DateTime GetFileReviewCopyFolderEndDate()
        {
            return GetDateTimeSetting("FileReview.CopyFolderEndDate");
        }

        public string GetEmailTemplateFolder()
        {
            return ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"];
        }

        public string GetCustomizationFilesUploadPath()
        {
            return ConfigurationManager.AppSettings["CustomizationFiles.UploadPath"];
        }

        public string GetAnonymousTokenParameterName()
        {
            return ConfigurationManager.AppSettings["AnonymousTokenName"];
        }
        public int GetThumbnailHeight()
        {
            return GetIntSetting("Thumbnail.Height", 135);
        }

        public int GetThumbnailWidth()
        {
            return GetIntSetting("Thumbnail.Width", 135);
        }

        public bool GetThumbnailShouldCrop()
        {
            return GetBoolSetting("Thumbnail.Crop", true);
        }

        public int GetUserDataExpirationWindowInMonths()
        {
            return GetIntSetting("UserDataExpiration.ExpirationWindowInMonths", 13);
        }

        public int GetDefaultMyFavoritesTotalImages()
        {
            return GetIntSetting("DefaultMyFavorites.TotalImages", 1);
        }

        public int GetDefaultMyFavoritesTotalItems()
        {
            return GetIntSetting("DefaultMyFavorites.TotalItems", 1);
        }

        public int GetDefaultMostPopularTotalImages()
        {
            return GetIntSetting("DefaultMostPopular.TotalImages", 1);
        }

        public int GetDefaultMostPopularTotalItems()
        {
            return GetIntSetting("DefaultMostPopular.TotalItems", 1);
        }

        public int GetDefaultMostPopularLessItems()
        {
            return GetIntSetting("DefaultMostPopular.LessItems", 1);
        }

        public string ExpiredContentAssetDetailUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["ExpiredContent.AssetDetailUrl"];
            }
        }

        public string ExpiredContentExtendURL
        {
            get
            {
                return ConfigurationManager.AppSettings["ExpiredContent.ExtendUrl"];
            }
        }

        public string ExpiredContentDoNotExtendURL
        {
            get
            {
                return ConfigurationManager.AppSettings["ExpiredContent.DoNotExtendUrl"];
            }
        }

        public bool GetShowRelatedProgramEnabled(string regionId)
        {
            return GetBoolSetting("ShowRelatedProgram.Enabled", false, true, regionId);
        }

        public bool GetPreviewFilesEnabled(string regionId)
        {
            return GetBoolSetting("PreviewFiles.Enabled", false, true, regionId);
        }

        public string[] GetPreviewFilesZipImages()
        {
            return ConfigurationManager.AppSettings["PreviewFiles.ZipImages"]?.ToLowerInvariant().Split(';');
        }

        public int GetFailoverConnectionRetries()
        {
            return GetIntSetting("FailoverConnection.Retries", 5);
        }

        public int GetFailoverConnectionDelay()
        {
            return GetIntSetting("FailoverConnection.Delay", 1000);
        }

        public int GetForgotPasswordExpirationTime()
        {
            return GetIntSetting("ForgotPassword.ExpirationTime", 24);
        }

        public int GetExpiredContentNotificationDays()
        {
            return GetIntSetting("ExpiredContent.NotificationDays", 30);
        }

        public string GetReCaptchaInvisibleSiteKey()
        {
            return ConfigurationManager.AppSettings["reCaptchaInvisibleSiteKey"];
        }

        public bool GetMailRegistrationMultipartEnabled()
        {
            return GetBoolSetting("MailDispatcher.Registration.IsMultipart", false);
        }

        public string GetEmailVerificationTemplateFile()
        {
            return ConfigurationManager.AppSettings[$"EmailVerification.TemplateFile"];
        }

        public string GetEmailVerificationTextPlainTemplateFile()
        {
            return ConfigurationManager.AppSettings[$"EmailVerification.TemplateFile.TextPlain"];
        }

        public bool GetPendingReviewRegistrationEnabledByRegion(string region)
        {
            return GetBoolSetting("ManageIssuerPendingRegistrationReview.Enabled", false, false, region);
        }

        public string GetRegisterHelpTo(string regionId)
        {
            return RegionalizeService.RegionalizeSetting("RegisterHelp.To", regionId);
        }

        public string GetRegisterHelpSecondTo(string regionId)
        {
            return RegionalizeService.RegionalizeSetting("RegisterHelpSecond.To", regionId);
        }

        public string GetRegisterServiceFunctionCodes()
        {
            return ConfigurationManager.AppSettings["ServiceFunctionCodes.Register"];
        }

        public bool GetAdobeLaunchEnabled(string regionId)
        {
            return GetBoolSetting("AdobeLaunch.Enabled", false, true, regionId);
        }

        public string GetAdobeLaunchJsFile(string regionId)
        {
            return RegionalizeService.RegionalizeSetting("AdobeLaunch.JsFile", regionId);
        }

        public bool GetHotjarEnabled(string regionId)
        {
            return GetBoolSetting("Hotjar.Enabled", false, true, regionId);
        }

        public string GetHotjarDefaultId()
        {
            return ConfigurationManager.AppSettings["Hotjar.DefaultId"];
        }

        public string GetHotjarId(string regionId)
        {
            return RegionalizeService.RegionalizeSetting("Hotjar.Id", regionId);
        }

        public bool GetOneTrustEnabled(string regionId)
        {
            return GetBoolSetting("OneTrust.Enabled", false, true, regionId);
        }

        public string GetOneTrustDataDomainScript(string regionId)
        {
            return RegionalizeService.RegionalizeSetting("OneTrust.DataDomainScript", regionId);
        }

        public bool GetPardotEnabled(string regionId, string languageId)
        {
            bool.TryParse(RegionalizeService.RegionalizeSetting("Pardot.Enabled", regionId, languageId), out bool value);
            return value;
        }

        public string GetPardotAccountId(string regionId, string languageId)
        {
            return RegionalizeService.RegionalizeSetting("Pardot.AccountId", regionId, languageId);
        }

        public string GetPardotCampaignId(string regionId, string languageId)
        {
            return RegionalizeService.RegionalizeSetting("Pardot.CampaignId", regionId, languageId);
        }

        public string GetPardotIntegrationUrl(string regionId)
        {
            return RegionalizeService.RegionalizeSetting("PardotIntegration.Url", regionId);
        }

        public bool GetPardotIntegrationEnabled(string regionId)
        {
            return GetBoolSetting("PardotIntegration.Enabled", false, true, regionId);
        }

        public string GetUserProfileEditorHome()
        {
            return ConfigurationManager.AppSettings["UserProfileEditorHome"];
        }

        public string GetBrokenImageName(int width)
        {
            string imageName = ConfigurationManager.AppSettings["BrokenImage.Name"];
            var availableWidths = ConfigurationManager.AppSettings["BrokenImage.AvailableWidths"].ToString().Split(',').Select(w => int.Parse(w)).OrderBy(w => w);
            int imageWidth = availableWidths.First(w => w >= width || w.Equals(availableWidths.Last()));

            return imageWidth > 0 ? imageName.Insert(imageName.LastIndexOf('.'), $"_{imageWidth}w") : imageName;
        }

        public bool GetOffersEnabled(string regionId)
        {
            return GetBoolSetting("Offers.Enabled", false, true, regionId);
        }

        public bool GetEndUserFrontEndOffersEnabled(string regionId)
        {
            return GetBoolSetting("ShowOffers.EndUser.Enabled", false, true, regionId);
        }

        public bool GetAdminFrontEndOffersEnabled(string regionId)
        {
            return GetBoolSetting("ShowOffers.Admin.Enabled", false, true, regionId);
        }

        public string GetOffersSitemapKey(string regionId)
        {
            return RegionalizeService.RegionalizeSetting("ShowOffers.SitemapKey", regionId);
        }

        public string GetMarketingCalendarSitemapKey(string regionId)
        {
            return RegionalizeService.RegionalizeSetting("MarketingCalendar.SitemapKey", regionId);
        }

        public bool GetMarketingCalendarEnabled(string regionId)
        {
            return GetBoolSetting("MarketingCalendar.Enabled", false, true, regionId);
        }

        public bool GetMarketingCalendarShowEndYear(string regionId)
        {
            return GetBoolSetting("MarketingCalendar.ShowEndYear", false, true, regionId);
        }

        public bool GetEndUserFrontEndMarketingCalendarEnabled(string regionId)
        {
            return GetBoolSetting("MarketingCalendar.EndUser.Enabled", false, true, regionId);
        }

        public bool GetAdminFrontEndMarketingCalendarEnabled(string regionId)
        {
            return GetBoolSetting("MarketingCalendar.Admin.Enabled", false, true, regionId);
        }

        public bool GetAwarenessFollowUpEnabled(string regionId)
        {
            return GetBoolSetting("ExpirationAwarenessEmail.AwarenessFollowUp.Enabled", false, true, regionId);
        }

        public bool GetDeletedFollowUpEnabled(string regionId)
        {
            return GetBoolSetting("ExpirationAwarenessEmail.DeletedFollowUp.Enabled", false, true, regionId);
        }

        public string GetPricelessApiClientConsumerKey()
        {
            return ConfigurationManager.AppSettings["PricelessApiClient.ConsumerKey"];
        }

        public string GetPricelessApiClientSigningKeyAlias()
        {
            return ConfigurationManager.AppSettings["PricelessApiClient.SigningKeyAlias"];
        }

        public string GetPricelessApiClientSigningKeyPassword()
        {
            return ConfigurationManager.AppSettings["PricelessApiClient.SigningKeyPassword"];
        }

        public string GetPricelessApiClientPkcs12KeyFilePath()
        {
            return BaseFileService.MapPath(ConfigurationManager.AppSettings["PricelessApiClient.Pkcs12KeyFilePath"]);
        }

        public string GetPricelessApiClientPartnerId()
        {
            return ConfigurationManager.AppSettings["PricelessApiClient.PartnerId"];
        }

        public string GetPricelessApiClientSecretKey()
        {
            return ConfigurationManager.AppSettings["PricelessApiClient.SecretKey"];
        }

        public string GetPricelessApiClientServiceEndpoint()
        {
            return ConfigurationManager.AppSettings["PricelessApiClient.ServiceEndpoint"];
        }

        public string GetPricelessApiClientDownloadFilesFolder()
        {
            return ConfigurationManager.AppSettings["PricelessApiClient.DownloadFilesFolder"];
        }

        public string GetPricelessOfferEmailUnmatchedTemplate()
        {
            return ConfigurationManager.AppSettings["PricelessOfferEmail.Unmatched.Template"];
        }

        public string GetPricelessOfferEmailUnmatchedToAddress()
        {
            return ConfigurationManager.AppSettings["PricelessOfferEmail.Unmatched.ToAddress"];
        }

        public string GetPricelessOfferEmailUnmatchedSubject()
        {
            return ConfigurationManager.AppSettings["PricelessOfferEmail.Unmatched.Subject"];
        }

        public string GetPricelessOfferEmailUnmatchedFromAddress()
        {
            return ConfigurationManager.AppSettings["PricelessOfferEmail.Unmatched.FromAddress"];
        }

        public string GetPricelessOfferEmailUnmatchedFromName()
        {
            return ConfigurationManager.AppSettings["PricelessOfferEmail.Unmatched.FromName"];
        }

        public string GetPricelessOfferEmailExceptionTemplate()
        {
            return ConfigurationManager.AppSettings["PricelessOfferEmail.Exception.Template"];
        }

        public string GetPricelessOfferEmailExceptionToAddress()
        {
            return ConfigurationManager.AppSettings["PricelessOfferEmail.Exception.ToAddress"];
        }

        public string GetPricelessOfferEmailExceptionSubject()
        {
            return ConfigurationManager.AppSettings["PricelessOfferEmail.Exception.Subject"];
        }

        public string GetPricelessOfferEmailExceptionFromAddress()
        {
            return ConfigurationManager.AppSettings["PricelessOfferEmail.Exception.FromAddress"];
        }

        public string GetPricelessOfferEmailExceptionFromName()
        {
            return ConfigurationManager.AppSettings["PricelessOfferEmail.Exception.FromName"];
        }

        public string GetPricelessOfferProcessBusinessOwnerEmail()
        {
            return ConfigurationManager.AppSettings["PricelessOfferProcess.BusinessOwnerEmail"];
        }

        public bool GetPricelessOfferDescriptionEnabled()
        {
            return GetBoolSetting("PricelessOfferDescription.Enabled", false);
        }

        public bool GetMerchantsAcquirersEndUserEnabled(string regionId)
        {
            return GetBoolSetting("MerchantsAcquirers.EndUser.Enabled", false, true, regionId);
        }

        public bool GetMerchantsAcquirersAdminEnabled(string regionId)
        {
            return GetBoolSetting("MerchantsAcquirers.Admin.Enabled", false, true, regionId);
        }

        public string GetMerchantsAcquirersSitemapKey(string regionId)
        {
            return RegionalizeService.RegionalizeSetting("MerchantsAcquirers.SitemapKey", regionId);
        }

        public string GetElectronicDeliveryDownloadUrlFormat()
        {
            return ConfigurationManager.AppSettings["ElectronicDelivery.DownloadUrlFormat"];
        }

        public string GetPortalBaseUrl()
        {
            return ConfigurationManager.AppSettings["portalBaseURL"];
        }

        public string GetSalesforceIntegrationActivityDays()
        {
            return ConfigurationManager.AppSettings["SalesforceIntegration.ActivityDays"];
        }

        public int GetPredefinedBusinessOwnerUserId()
        {
            return GetIntSetting("PredefinedBusinessOwnerUserId", 1917); // System Admin
        }

        public string GetShareReportToAddress()
        {
            return ConfigurationManager.AppSettings["ShareReport.ToAddress"];
        }

        public string GetIssuerOptimizationOptimizationSolutionsTagIdentifier()
        {
            return ConfigurationManager.AppSettings["IssuerOptimization.OptimizationSolutionsTagIdentifier"];
        }

        public string GetEvoPdfLicenseKey()
        {
            return ConfigurationManager.AppSettings["EvoPdfLicenseKey"];
        }

        public bool GetShareReportEnabled()
        {
            return GetBoolSetting("ShareReport.Enabled", false);
        }

        public string GetEMarketingLoginHandshakeTargetUrl(string regionId)
        {
            return RegionalizeService.RegionalizeSetting("eMarketingLoginHandshakeTargetUrl", regionId, false);
        }

        public string GetEMarketingLoginHandshakeId(string regionId)
        {
            return RegionalizeService.RegionalizeSetting("eMarketingLoginHandshakeId", regionId, false);
        }
    }
}