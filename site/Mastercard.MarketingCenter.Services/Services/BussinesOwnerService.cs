﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Extensions;
using Mastercard.MarketingCenter.Services.Exceptions;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services.Services
{
    public class BussinesOwnerService : IBussinesOwnerService
    {
        private readonly UserContext _userContext;
        private readonly IContentItemService _contentItemService;
        private readonly IAuthorizationService _authorizationService;
        private readonly ISettingsService _settingsService;
        private readonly IContentCommentServices _contentCommentServices;
        private readonly IEmailService _emailService;
        private readonly IUrlService _urlService;

        public BussinesOwnerService(
            UserContext userContext,
            IContentItemService contentItemService,
            IAuthorizationService authorizationService,
            ISettingsService settingsService,
            IContentCommentServices contentCommentServices,
            IEmailService emailService,
            IUrlService urlService)
        {
            _userContext = userContext;
            _contentItemService = contentItemService;
            _authorizationService = authorizationService;
            _settingsService = settingsService;
            _contentCommentServices = contentCommentServices;
            _emailService = emailService;
            _urlService = urlService;
        }

        public void DeleteByBusinessOwner(ContentItemBase contentItem)
        {
            if (!_authorizationService.AuthorizeBusinessOwnerForChangingReviewState(_userContext.User.UserId, contentItem.ContentItemId))
            {
                throw new AuthorizationException();
            }

            if (contentItem.ReviewStateId != MarketingCenterDbConstants.ExpirationReviewStates.NotABusinessOwner)
            {
                _contentItemService.DeleteOnlyDraft(contentItem, _userContext.User.UserId);
            }

            UpdateReviewState(contentItem);
        }

        public IEnumerable<Region> FilterRegionsWithDisabledBOInterface(IEnumerable<Region> regions)
        {
            return regions.Where(x => _settingsService.GetBusinessOwnerExpirationManagementEnabled(x.IdTrimmed));
        }

        /// <summary>
        /// Updates review state
        /// </summary>
        /// <param name="contentItemId"></param>
        /// <param name="state">If State is nt called it goes to the default Not available state stateId=0</param>
        /// <returns></returns>
        public void UpdateReviewState(ContentItemBase contentItem, int stateId = 0)
        {
            if (!_authorizationService.AuthorizeBusinessOwnerForChangingReviewState(_userContext.User.UserId, contentItem.ContentItemId))
            {
                throw new AuthorizationException();
            }

            _contentItemService.UpdateReviewState(contentItem, stateId);
        }

        /// <summary>
        /// Extends or not extends the specified content items <paramref name="contentItemIDs"/>
        /// </summary>
        /// <returns>true if operation was successul for all contentItems, otherwise false.</returns>
        public bool ExtendOrNotExtend(
            IEnumerable<string> contentItemIDs,
            int senderUserId,
            bool shouldExtend,
            string extendMessage,
            int daysToExtend,
            out IEnumerable<Slam.Cms.Data.ContentItem> slamContentItems)
        {
            slamContentItems = _contentItemService.GetAssetContentItems(contentItemIDs);

            var isInsertSuccessful = true;
            foreach (var slamContentItem in slamContentItems)
            {
                var comment = shouldExtend ? _contentCommentServices.GetExtensionComment(daysToExtend, extendMessage) : extendMessage;
                isInsertSuccessful &= _contentCommentServices.AddNew(senderUserId, slamContentItem.ContentTypeId, slamContentItem.ContentItemId, comment);

                var contentItem = _contentItemService.GetContentItem(slamContentItem.ContentItemId.GetAsLiveContentItemId());
                UpdateReviewState(contentItem, shouldExtend ? MarketingCenterDbConstants.ExpirationReviewStates.Extend : MarketingCenterDbConstants.ExpirationReviewStates.DoNotExtend);
            }

            return isInsertSuccessful;
        }

        public int GetBusinessOwnerId(IDictionary<string, object> contentItemData)
        {
            return contentItemData.GetDataAsInt(MarketingCenterDbConstants.FieldNames.BusinessOwnerId);
        }

        public void SendContentCommentNotificationToAdmin(
            string assetName,
            string contentItemId,
            string comment,
            string region,
            string userFirstName,
            string language = null)
        {
            var toAddresses = _settingsService.GetTemplateToAddresses(region, "ContentCommentNotification"); // sending email to "Regional Admins", TODO: get emails from DB instead of config from users which are Regional Admins

            _emailService.SendContentCommentNotification(
                "ContentCommentNotification",
                toAddresses, assetName, contentItemId, comment, region,
                userFirstName, _urlService.GetContentEditURL(contentItemId), language);
        }

        public void SendContentCommentNotificationToBussinessOwner(
            string assetName,
            string contentItemId,
            string comment,
            string region,
            string userFirstName,
            string businessOwnerEmail,
            string language = null)
        {
            _emailService.SendContentCommentNotification(
                "ContentCommentNotificationBO",
                new List<string> { businessOwnerEmail }, assetName, contentItemId,
                comment, region, userFirstName,
                _urlService.GetBusinessOwnerDetailsURL(contentItemId),
                language);
        }
    }
}