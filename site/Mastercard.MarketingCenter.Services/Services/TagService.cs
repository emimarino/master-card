﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class TagService : ITagService
    {
        private readonly SlamContext _slamContext;
        private readonly IRegionService _regionService;
        private readonly TagRepository _tagRepository;

        public TagService(SlamContext slamContext, IRegionService regionService, TagRepository tagRepository)
        {
            _slamContext = slamContext;
            _regionService = regionService;
            _tagRepository = tagRepository;
        }

        public IEnumerable<MarketingCenter.Data.Entities.Tag> GetTags(params string[] tagIds)
        {
            return _tagRepository.GetTags(tagIds);
        }

        private TagTreeNode GetCategory(string tagCategoryId, TagTree tagTree)
        {
            return tagTree?.Root?.Children?.FirstOrDefault(tt => tt.TagCategory?.TagCategoryId?.Equals(tagCategoryId, StringComparison.OrdinalIgnoreCase) ?? false);
        }

        public Dictionary<string, string> GetCountries(string regionId, string languageCode)
        {
            var tagTree = _slamContext?.GetTagTree();
            return GetCountries(regionId, languageCode, tagTree);
        }

        public Dictionary<string, string> GetCountries(string regionId, string languageCode, TagTree tagTree)
        {
            var region = _regionService.GetById(regionId);
            var root = GetCategory(MarketingCenterDbConstants.TagCategories.Markets, tagTree)?
                .Children?
                .FirstOrDefault(t => t?.Tag.TagId?.Equals(region.TagId, StringComparison.OrdinalIgnoreCase) ?? false);

            var children = root?.Children.Count != 0 ? root?.Children : (new List<TagTreeNode>() { root });

            children?.ToList().ForEach(t => t.Tag.Translate(languageCode));

            return children?.ToDictionary(n => n.Identifier, n => n.Tag.DisplayName);
        }

        /// <summary>
        /// Used to get the list of countries
        /// </summary>
        /// <param name="languageCode">The language that the names will be translated into.</param>
        /// <returns>Returns a list with the Key(identifier the region) and the translated</returns>
        public Dictionary<string, string> GetAllCountries(string languageCode)
        {
            var categoryChildren = _slamContext.GetTagTree()?.FindNode(tt => (tt.TagCategory != null ? tt.TagCategory.TagCategoryId.Equals(MarketingCenterDbConstants.TagCategories.Markets, StringComparison.OrdinalIgnoreCase) : false))?.Children;
            List<TagTreeNode> tempTagTreeNodeList = new List<TagTreeNode>();
            foreach (var n in categoryChildren.Where(c => c.Identifier != "global"))
            {
                tempTagTreeNodeList.AddRange(TagTree.GetLeast(n));
            }

            return new Dictionary<string, string>(tempTagTreeNodeList?.ToDictionary(tag => tag.Identifier, tag => { tag.Tag.Translate(languageCode); return tag.Tag.DisplayName; }));
        }

        public Dictionary<string, string> GetLanguages(
            string regionId,
            IDictionary<string, string> languageCodes)
        {
            var tagTree = _slamContext?.GetTagTree();
            return GetLanguages(regionId, languageCodes, tagTree);
        }

        public Dictionary<string, string> GetLanguages(
            string regionId,
            IDictionary<string, string> languageCodes,
            TagTree tagTree)
        {
            var root = GetCategory(MarketingCenterDbConstants.TagCategories.Languages, tagTree)?
                .Children?
                .Where(tt => languageCodes.Any(l => l.Key?.Trim()?.Equals(tt?.Identifier?.Trim() ?? "", StringComparison.OrdinalIgnoreCase) ?? false));

            root?.ToList().ForEach(t => t.Tag.Translate(regionId));

            return root?.ToDictionary(n => n.Identifier, n => n.Text);
        }

        public IEnumerable<MarketingCenter.Data.Entities.Tag> GetTagsByIdentifier(params string[] identifiers)
        {
            return _tagRepository.GetTagsByIdentifier(identifiers);
        }

        public IEnumerable<MarketingCenter.Data.Entities.Tag> GetTagsByTagCategory(string tagCategoryId)
        {
            return _tagRepository.GetTagsByTagCategory(tagCategoryId);
        }

        public Dictionary<string, string> GetTagsByIdentifiersAndLanguage(string[] identifiers, string languageCode)
        {
            return _tagRepository.GetTagsByIdentifier(identifiers)
                                 .ToDictionary(t => t.Identifier,
                                               t => t.TagTranslatedContent.FirstOrDefault(tt => tt.LanguageCode == languageCode)?.DisplayName);
        }
    }
}