﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Security;

namespace Mastercard.MarketingCenter.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IAuthorizationRepository _authorizationRepository;
        private readonly IConsentFileDataRepository _consentFileDataRepository;
        private readonly UserSubscriptionRepository _userSubscriptionRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UserService(
            IUserRepository userRepository,
            IAuthorizationRepository authorizationRepository,
            IConsentFileDataRepository consentFileDataRepository,
            UserSubscriptionRepository userSubscriptionRepository,
            IUnitOfWork unitOfWork
        )
        {
            _userRepository = userRepository;
            _authorizationRepository = authorizationRepository;
            _consentFileDataRepository = consentFileDataRepository;
            _userSubscriptionRepository = userSubscriptionRepository;
            _unitOfWork = unitOfWork;
        }

        public User GetUser(int userId)
        {
            return _userRepository.GetUser(userId);
        }

        public User CreateUser(
            string firstName,
            string lastName,
            string issuerName,
            string address1,
            string address2,
            string address3,
            string city,
            string state,
            string zipCode,
            string phone,
            string fax,
            string mobile,
            string email,
            string issuerId,
            string processor,
            string title,
            string country,
            string language,
            string regionId,
            string acceptedConsents,
            bool acceptedEmailSubscriptions
        )
        {
            if (string.IsNullOrEmpty(Membership.GetUserNameByEmail(email)))
            {
                string userName = Guid.NewGuid().ToString();
                Membership.CreateUser(userName, Membership.GeneratePassword(6, 2), email, "What is your pet Name?", "MMC", false, out MembershipCreateStatus status);
                if (status == MembershipCreateStatus.Success)
                {
                    var user = _userRepository.Insert(new User
                    {
                        UserName = userName.IncludeMembershipProviderName(),
                        Email = email,
                        Region = regionId,
                        Country = country,
                        Language = language,
                        IssuerId = issuerId,
                        IsDisabled = false,
                        IsBlocked = false,
                        ExcludedFromDomoApi = false,
                        ModifiedDate = DateTime.Now,
                        Profile = new UserProfile
                        {
                            FirstName = firstName,
                            LastName = lastName,
                            Title = title,
                            IssuerName = issuerName,
                            Processor = processor,
                            Phone = phone,
                            Mobile = mobile,
                            Fax = fax,
                            Address1 = address1,
                            Address2 = address2,
                            Address3 = address3,
                            City = city,
                            State = state,
                            Zip = zipCode,
                            FeedbackRequestDisplayed = false,
                            ContentPreferencesMessageDisplayed = true,
                            NeverFrecuencyMessageDisplayed = false,
                            NeverFrecuencyMessageDisplayedDateTime = null,
                            GdprFollowUpStatusId = null
                        }
                    });

                    _unitOfWork.Commit();

                    if (!string.IsNullOrWhiteSpace(acceptedConsents))
                    {
                        var validAcceptedConsents = _consentFileDataRepository.GetByIds(acceptedConsents.Split(',')
                                                                                                        .Select(ac =>
                                                                                                        {
                                                                                                            var consentId = -1;
                                                                                                            return int.TryParse(ac, out consentId) ? consentId : -1;
                                                                                                        }))
                                                                              .Select(cfd => cfd.ConsentFileDataId);
                        foreach (var acceptedConsentId in validAcceptedConsents)
                        {
                            _consentFileDataRepository.InsertUserConsent(new UserConsent
                            {
                                UserId = user.UserId,
                                ConsentDate = DateTime.Now,
                                ConsentFileDataId = acceptedConsentId
                            });
                        }
                    }

                    _userSubscriptionRepository.Insert(new UserSubscription
                    {
                        UserId = user.UserId,
                        UserSubscriptionFrequencyId = acceptedEmailSubscriptions ? MarketingCenterDbConstants.UserSubscriptionFrequency.Weekly : MarketingCenterDbConstants.UserSubscriptionFrequency.Never,
                        SavedDate = DateTime.Now
                    });

                    _authorizationRepository.InsertUserRoleRegion(new UserRoleRegion
                    {
                        UserId = user.UserId,
                        RoleId = _authorizationRepository.GetRoleByName(Constants.Roles.GlobalView).RoleId,
                        RegionId = regionId
                    });

                    _unitOfWork.Commit();

                    return user;
                }
            }

            return null;
        }

        public bool UpdateUser(User user)
        {
            if (user == null)
            {
                return false;
            }

            var updateUser = GetUserByUserName(user.UserName);
            if (updateUser != null)
            {
                updateUser.Email = user.Email;
                updateUser.IssuerId = user.IssuerId;
                updateUser.Region = user.Region;
                updateUser.Country = user.Country;
                updateUser.ModifiedDate = DateTime.Now;

                _userRepository.Update(updateUser);
            }

            return true;
        }

        public User GetUserByEmail(string email)
        {
            return _userRepository.GetByEmail(email);
        }

        /// <summary>
        /// Getst The user from the users table by username (the same username from aspnet_users ) 
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public User GetUserByUserName(string userName)
        {
            return string.IsNullOrEmpty(userName) ? null : _userRepository.GetUser(userName);
        }

        public IEnumerable<User> GetAll(bool onlyEnabled = true, bool onlyMastercardUsers = false)
        {
            return _userRepository.GetAll()
                                  .Where(u => (!onlyEnabled || !u.IsDisabled) &&
                                              (!onlyMastercardUsers || (!string.IsNullOrEmpty(u.IssuerId) && u.Issuer.IsMastercardIssuer)));
        }

        public IEnumerable<UserData> GetActiveUsersByIssuerId(string issuerId)
        {
            return _userRepository.GetActiveUsersByIssuerId(issuerId);
        }

        public Aspnet_Membership GetMembershipUserByEmail(string email)
        {
            return _userRepository.GetMembershipUserByEmail(email);
        }

        public Aspnet_Membership GetMembershipUserByUserName(string userName)
        {
            return _userRepository.GetMembershipUserByUserName(userName);
        }

        public Aspnet_Membership UpdateMembershipUser(Aspnet_Membership membershipUser)
        {
            return _userRepository.Update(membershipUser);
        }

        public IEnumerable<User> GetPrinterUsers()
        {
            return _authorizationRepository.GetUsersInRole(Constants.Roles.Printer)
                                           .Select(u => u.User)
                                           .Where(u => !u.IsDisabled && !u.IsBlocked);
        }
    }
}