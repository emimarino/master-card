﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Mastercard.MarketingCenter.Services.Services
{
    public static class SanitizeService
    {
        public static string CommentOutOrRemoveTag(string input, string tag, bool removeTag = true)
        {
            if (string.IsNullOrEmpty(input) || string.IsNullOrEmpty(tag))
            {
                return input;
            }

            var doc = new HtmlDocument();
            doc.LoadHtml(input);
            doc.DocumentNode.Descendants(tag).ToList().ForEach(t =>
            {
                if (removeTag)
                {
                    t.Remove();
                }
                else
                {
                    t.ParentNode.ReplaceChild(HtmlNode.CreateNode("<!--<out>" + HttpUtility.HtmlEncode(t.OuterHtml) + "</out>-->"), t);
                }
            });

            return doc.DocumentNode.OuterHtml;
        }

        public static string EscapeOrRemoveAttribute(string input, string attributeName, string tag = null, bool removeAttribute = true)
        {
            if (string.IsNullOrEmpty(input) || string.IsNullOrEmpty(attributeName))
            {
                return input;
            }

            var doc = new HtmlDocument();
            doc.LoadHtml(input);
            var desc = (string.IsNullOrEmpty(tag)) ? doc.DocumentNode.Descendants() : doc.DocumentNode.Descendants(tag);
            desc.ToList().ForEach(t =>
            {
                if (t.Attributes.Contains(attributeName))
                {
                    if (removeAttribute)
                    {
                        t.Attributes[attributeName]?.Remove();
                    }
                    else
                    {
                        t.SetAttributeValue(attributeName, HttpUtility.HtmlEncode(t.GetAttributeValue(attributeName, string.Empty)));
                    }
                }
            });

            return doc.DocumentNode.OuterHtml;
        }

        public static string RejectInvalidCharactersInString(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return str;
            }

            if (!string.IsNullOrEmpty(str))
            {
                if ((new Regex("^javascript:|[ <(.|\n)*?>] ", RegexOptions.CultureInvariant)).IsMatch(HttpUtility.UrlDecode(str)))
                {
                    throw new HttpRequestValidationException();
                }
            }

            return str;
        }

        public static HttpRequest RejectInvalidCharactersInParameters(HttpRequest request, string parameterNames, bool onlyValidated = true)
        {
            if (string.IsNullOrEmpty(parameterNames) || request == null)
            {
                return request;
            }

            List<System.Collections.Specialized.NameValueCollection> allParams =
                new List<System.Collections.Specialized.NameValueCollection>
                {
                    (onlyValidated) ? request.Form : request.Unvalidated.Form,
                    (onlyValidated) ? request.QueryString : request.Unvalidated.QueryString
                };
            Action<System.Collections.Specialized.NameValueCollection> goOverParams = (Params) =>
            {
                parameterNames.Split(',').ToList().ForEach((ParameterName) =>
                {
                    if ((Params.HasKeys() && (!string.IsNullOrEmpty(Params[ParameterName]))))
                        RejectInvalidCharactersInString(Params[ParameterName]);
                });
            };
            allParams.ForEach((pc) => goOverParams(pc));

            return request;
        }

        static public HttpRequest IsUrlInParametersInternal(HttpRequest Request, string ParameterNames, bool onlyValidated = true)
        {
            if (string.IsNullOrEmpty(ParameterNames) || Request == null || (Request?.Url?.PathAndQuery?.Contains(@"portal/api/") ?? false))
            {
                return Request;
            }

            var ParameterNamesList = ParameterNames.Split(',').ToList();
            Func<System.Collections.Specialized.NameValueCollection, bool> hasKeys = (c) =>
            {
                return c.AllKeys.Any(f => ParameterNamesList.Any(p => p.Equals(f, StringComparison.OrdinalIgnoreCase)));
            };

            if (!(hasKeys(Request.Unvalidated.Form) || hasKeys(Request.Unvalidated.QueryString) || hasKeys(Request.Unvalidated.Headers)))
            {
                return Request;
            }

            List<System.Collections.Specialized.NameValueCollection> allParams = new List<System.Collections.Specialized.NameValueCollection>();
            allParams.Add((onlyValidated) ? Request.Form : Request.Unvalidated.Form);
            allParams.Add((onlyValidated) ? Request.QueryString : Request.Unvalidated.QueryString);
            allParams.Add((onlyValidated) ? Request.Headers : Request.Unvalidated.Headers);
            Action<System.Collections.Specialized.NameValueCollection> goOverParams = (Params) =>
            {
                ParameterNamesList.ForEach((ParameterName) =>
                {
                    if ((Params.HasKeys() && (!string.IsNullOrEmpty(Params[ParameterName]))))
                        IsUrlInternal(Request, Params[ParameterName]);
                });
            };
            allParams.ForEach((pc) => goOverParams(pc));

            return Request;
        }

        static public HttpRequest IsUrlInternal(HttpRequest Request, string url)
        {
            Uri URI;
            Uri.TryCreate(url, UriKind.RelativeOrAbsolute, out URI);

            string secondaryHost = new Uri(Slam.Cms.Configuration.ConfigurationManager.Solution.Environments[Slam.Cms.Configuration.ConfigurationManager.Environment.Settings["InApplicableRegionEnvironment"]].FrontEndUrl).Host;
            var Urls = new string[] { Request.Url.Host, secondaryHost };
            if ((URI?.IsAbsoluteUri ?? true) &&
                    (!Urls.Any(ul => URI?.Host?.Equals(ul, StringComparison.OrdinalIgnoreCase) ?? false)))
            {
                if (!IsUrlWhiteListed(URI))
                {
                    throw new System.Web.HttpRequestValidationException();
                }
            }

            return Request;
        }

        static private bool IsUrlWhiteListed(Uri uri)
        {
            if (uri == null)
            {
                return false;
            }

            List<string> whiteList = ConfigurationManager.AppSettings["Referrer.WhiteList"].ToString().Split(';').Select(x => x.Trim()).ToList();

            if (whiteList.Any(x => Regex.IsMatch(x.Trim(), @"^'.*'$", RegexOptions.Compiled | RegexOptions.CultureInvariant) ? Regex.IsMatch(uri.Host, Regex.Replace(x.Trim(), @"^'(.*)'$", (m) => { return $"^{m.Groups[1].Value}$"; }, RegexOptions.Compiled | RegexOptions.CultureInvariant), RegexOptions.Compiled | RegexOptions.CultureInvariant) : uri.Host.Equals(x.Trim(), StringComparison.OrdinalIgnoreCase)))
            {
                return true;
            }

            return false;
        }
    }
}