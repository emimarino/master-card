﻿using Mastercard.MarketingCenter.Services.Data;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public static class OrderableAssetService
    {
        public static PricingSchedule GetPricingSchedule(string globalID)
        {
            MasterCardPortalDataContext dc = new MasterCardPortalDataContext();
            return (from oa in dc.OrderableAssets
                    where oa.ContentItemId == globalID
                    select oa.PricingSchedule).FirstOrDefault();
        }
    }
}