﻿using AutoMapper;
using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Extensions;
using Mastercard.MarketingCenter.DTO.Priceless;
using Mastercard.MarketingCenter.Services.Exceptions;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Models;
using Slam.Cms.Common.Interfaces;
using Slam.Cms.Data;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class OfferService : IOfferService
    {
        private static readonly object importedOfferLock = new object();
        private readonly IMapper _mapper;
        private readonly IPreviewMode _previewMode;
        private readonly ICachingService _cachingService;
        private readonly IOfferRepository _offerRepository;
        private readonly IRegionService _regionService;
        private readonly IEmailService _emailService;
        private readonly IPricelessApiService _pricelessApiService;
        private readonly IZipFileService _zipFileService;
        private readonly ICsvFileService _csvFileService;
        private readonly ISettingsService _settingsService;

        public OfferService(IMapper mapper, IPreviewMode previewMode, ICachingService cachingService, IOfferRepository offerRepository, IRegionService regionService, IEmailService emailServices, IPricelessApiService pricelessApiService, IZipFileService zipFileService, ICsvFileService csvFileService, ISettingsService settingsService)
        {
            _mapper = mapper;
            _previewMode = previewMode;
            _cachingService = cachingService;
            _offerRepository = offerRepository;
            _regionService = regionService;
            _emailService = emailServices;
            _pricelessApiService = pricelessApiService;
            _zipFileService = zipFileService;
            _csvFileService = csvFileService;
            _settingsService = settingsService;
        }

        public IEnumerable<CardExclusivityModel> GetCardExclusivities()
        {
            string cacheKey = _cachingService.GetCardExclusivityModelCacheKey();
            if (_cachingService.Get<IEnumerable<CardExclusivityModel>>(cacheKey) == null)
            {
                _cachingService.Save(cacheKey, _mapper.Map<IEnumerable<CardExclusivityModel>>(_offerRepository.GetCardExclusivities()));
            }

            return _cachingService.Get<IEnumerable<CardExclusivityModel>>(cacheKey);
        }

        public IEnumerable<CategoryModel> GetCategories()
        {
            string cacheKey = _cachingService.GetCategoryModelCacheKey();
            if (_cachingService.Get<IEnumerable<CategoryModel>>(cacheKey) == null)
            {
                _cachingService.Save(cacheKey, _mapper.Map<IEnumerable<CategoryModel>>(_offerRepository.GetCategories()));
            }

            return _cachingService.Get<IEnumerable<CategoryModel>>(cacheKey);
        }

        public IEnumerable<OfferListModel> GetOffersByRegion(string regionId, bool ignorePreviewMode = false)
        {
            if (ignorePreviewMode || !_previewMode.Enabled)
            {
                string cacheKey = _cachingService.GetOfferModelCacheKey(regionId);
                if (_cachingService.Get<IEnumerable<OfferListModel>>(cacheKey) == null)
                {
                    _cachingService.Save(cacheKey, _mapper.Map<IEnumerable<OfferListModel>>(GetOffers(regionId, ignorePreviewMode)));
                }

                return _cachingService.Get<IEnumerable<OfferListModel>>(cacheKey);
            }

            return _mapper.Map<IEnumerable<OfferListModel>>(GetOffers(regionId));
        }

        public void ReloadCardExclusivityModel()
        {
            _cachingService.Delete(_cachingService.GetCardExclusivityModelCacheKey());
            GetCardExclusivities();
        }

        public void ReloadCategoryModel()
        {
            _cachingService.Delete(_cachingService.GetCategoryModelCacheKey());
            GetCategories();
        }

        public void ReloadOfferModel()
        {
            foreach (var region in _regionService.GetAll())
            {
                _cachingService.Delete(_cachingService.GetOfferModelCacheKey(region.IdTrimmed));
                GetOffersByRegion(region.IdTrimmed, true);
            }
        }

        public void ReloadMarketApplicabilityModel()
        {
            foreach (var region in _regionService.GetAll())
            {
                _cachingService.Delete(_cachingService.GetMarketApplicabilityModelCacheKey(region.IdTrimmed));
                GetMarketApplicabilities(region.IdTrimmed);
            }
        }

        public OfferDetailsModel GetOffer(string contentItemId, string regionId)
        {
            return _mapper.Map<OfferDetailsModel>(GetOffers(regionId).GetById(contentItemId, _previewMode.Enabled));
        }

        private IQueryable<Offer> GetOffers(string regionId, bool ignorePreviewMode = false)
        {
            var offers = _offerRepository.GetByRegion(regionId).FilterByDate();

            if (ignorePreviewMode || !_previewMode.Enabled)
            {
                return offers.FilterByVisibility(regionId).FilterByStatus(Status.Published);
            }

            return offers.FilterByStatus(new int[] { Status.Published, Status.PublishedAndDraft, Status.DraftOnly }).FilterByDraftView();
        }

        public IEnumerable<MarketApplicabilityModel> GetMarketApplicabilities(string regionId)
        {
            string cacheKey = _cachingService.GetMarketApplicabilityModelCacheKey(regionId);
            if (_cachingService.Get<IEnumerable<MarketApplicabilityModel>>(cacheKey) == null)
            {
                _cachingService.Save(cacheKey, _mapper.Map<IEnumerable<MarketApplicabilityModel>>(_offerRepository.GetMarketApplicabilities(regionId)));
            }

            return _cachingService.Get<IEnumerable<MarketApplicabilityModel>>(cacheKey);
        }

        public void InsertImportedOffers(IEnumerable<ImportedOfferModel> importedOffers)
        {
            _offerRepository.InsertImportedOffers(_mapper.Map<IEnumerable<ImportedOffer>>(importedOffers));
        }

        public void ProcessPricelessOffers()
        {
            lock (importedOfferLock)
            {
                _offerRepository.ProcessPricelessOffers(_settingsService.GetPricelessOfferProcessBusinessOwnerEmail());
            }
        }

        public void ProcessUnmatchedPricelessOffers()
        {
            lock (importedOfferLock)
            {
                var lastPricelessOffersProcess = _offerRepository.GetPricelessOffersProcesses()
                                                                 .OrderByDescending(x => x.ProcessEndDate)
                                                                 .FirstOrDefault();

                if (lastPricelessOffersProcess != null && lastPricelessOffersProcess.OffersUnmatched > 0 && !lastPricelessOffersProcess.OffersUnmatchedData.IsNullOrEmpty())
                {
                    _emailService.SendUnmatchedPricelessOfferEmail((new OfferUnmatchedDTO(lastPricelessOffersProcess.OffersUnmatchedData)).ToString());
                }
            }
        }

        public void CleanUpImportedOffers()
        {
            lock (importedOfferLock)
            {
                _offerRepository.ArchiveImportedOffers();
                _offerRepository.DeleteArchivedImportedOffers();
            }
        }

        public void ImportPricelessOffers()
        {
            lock (importedOfferLock)
            {
                var downloadUrl = _pricelessApiService.GetProductFeedUrl();
                if (downloadUrl.IsNullOrWhiteSpace())
                {
                    string exceptionMessage = "There was a problem when trying to get the download url from the priceless api.";
                    _emailService.SendExceptionPricelessOfferEmail(exceptionMessage);
                    throw new PricelessOfferException(exceptionMessage);
                }

                var csvFile = _zipFileService.GetZipCsvFileFromUrl(downloadUrl, _settingsService.GetPricelessApiClientDownloadFilesFolder());
                if (csvFile.IsNullOrWhiteSpace())
                {
                    string exceptionMessage = $"There was a problem when trying to download or open the zip csv file from the download url priceless api. Priceless Api Download Url: '{downloadUrl}'";
                    _emailService.SendExceptionPricelessOfferEmail(exceptionMessage);
                    throw new PricelessOfferException(exceptionMessage);
                }

                var offers = _csvFileService.ReadCsv<ImportedOfferModel>(csvFile);
                InsertImportedOffers(offers);
            }
        }
    }
}