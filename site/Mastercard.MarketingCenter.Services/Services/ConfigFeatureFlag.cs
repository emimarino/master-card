﻿using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services.Services
{
    public class ConfigFeatureFlag : AbstractFeatureFlag
    {
        private readonly IEnumerable<IFeatureFlagProvider> _providers;

        public ConfigFeatureFlag(IEnumerable<IFeatureFlagProvider> providers, UserContext _userContext)
        {
            _providers = providers;
            _region = _userContext.SelectedRegion;
        }

        public ConfigFeatureFlag(IEnumerable<IFeatureFlagProvider> providers)
        {
            _providers = providers;
        }

        public override bool IsOn(string key, string region = null)
        {
            var values = _providers.Select(p => p.GetSetting(key, region ?? _region));
            var anyIstrue = values.Any(v => v==true);
            var noneIsFalse = values.All(v => v!=false );
            return anyIstrue && noneIsFalse;
        }

        public override bool IsAnyOn(string[] featureKeys, string region = null)
        {
            return  featureKeys.Any(fk => this.IsOn(fk));
        }

        public override bool AllAreOn(string[] featureKeys, string region = null)
        {
           
            return featureKeys.All(fk => this.IsOn(fk));
        }
    }
}


