﻿using Mastercard.MarketingCenter.Services.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace Mastercard.MarketingCenter.Services
{
    public static class ShoppingCartService
    {
        public static int CreateShoppingCart(string UserID)
        {
            MasterCardPortalDataContext datacontext = new MasterCardPortalDataContext();
            ShoppingCart scart = new ShoppingCart();
            scart.UserID = UserID;
            scart.DateCreated = DateTime.Now;
            scart.DatedCheckedOut = null;
            scart.LastUpdated = DateTime.Now;
            datacontext.ShoppingCarts.InsertOnSubmit(scart);
            datacontext.SubmitChanges();
            return scart.CartID;
        }

        public static void UpdateShoppingCartBillingICA(int shoppingCartId, int billingICA)
        {
            MasterCardPortalDataContext dc = new MasterCardPortalDataContext();

            var cart = (from c in dc.ShoppingCarts where c.CartID == shoppingCartId select c).FirstOrDefault();
            if (cart != null)
            {
                cart.BillingID = billingICA;
                dc.SubmitChanges();
            }
        }

        public static int? GetActiveShoppingCart(string UserID)
        {
            int? shoppingcartid = null;
            MasterCardPortalDataContext datacontext = new MasterCardPortalDataContext();

            var q = from a in datacontext.ShoppingCarts
                    where a.UserID.Equals(UserID) && a.DatedCheckedOut.Equals(null)
                    select a;

            if (q.Count() > 0)
            {
                shoppingcartid = q.First().CartID;
            }
            return shoppingcartid;
        }

        public static ShoppingCart GetShoppingCartById(int shoppingCartId)
        {
            MasterCardPortalDataContext dc = new MasterCardPortalDataContext();

            return (from sc in dc.ShoppingCarts where sc.CartID == shoppingCartId select sc).FirstOrDefault();
        }

        public static bool CartHasMultipleShippingAddresses(int shoppingCartId)
        {
            MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();

            var q = from shippingInfo in dataContext.ShippingInformations
                    where shippingInfo.ShoppingCartID.Equals(shoppingCartId)
                    select shippingInfo;

            return q.Count() > 1;
        }

        public static void SaveElectronicDeliveryCartItem(int shoppingCartId, string globalId)
        {
            MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();
            if (!dataContext.CartItems.Any(c => c.ContentItemID.Equals(globalId) && c.CartID.Equals(shoppingCartId) && c.ElectronicDelivery))
            {
                CartItem item = new CartItem();
                item.CartID = shoppingCartId;
                item.ContentItemID = globalId;
                item.ElectronicDelivery = true;
                item.Quantity = 1;
                dataContext.CartItems.InsertOnSubmit(item);
                dataContext.SubmitChanges();
            }
        }

        public static void SaveShoppingCartItem(int ShoppingCartId, string GlobalID, int shippingInformationId, int Quantity, bool forceNew)
        {
            MasterCardPortalDataContext datacontext = new MasterCardPortalDataContext();

            if (shippingInformationId == 0)
            {
                var q = from a in datacontext.CartItems
                        where a.ContentItemID.Equals(GlobalID) && a.CartID.Equals(ShoppingCartId) && !a.ElectronicDelivery
                        select a;

                CartItem cartitem = null;
                if (q.Count() == 0)
                {
                    cartitem = new CartItem();
                    cartitem.CartID = ShoppingCartId;
                    cartitem.ContentItemID = GlobalID;
                    cartitem.Quantity = Quantity;
                    datacontext.CartItems.InsertOnSubmit(cartitem);
                }
                else
                {
                    cartitem = q.FirstOrDefault();
                    cartitem.Quantity = Quantity;
                }
            }
            else
            {
                var q = from a in datacontext.CartItems
                        where a.ContentItemID.Equals(GlobalID) && a.CartID.Equals(ShoppingCartId)
                                && a.ShippingInformationID.Equals(shippingInformationId) && !a.ElectronicDelivery
                        select a;

                CartItem cartitem = null;
                if (q.Count() == 0)
                {
                    var second = from a in datacontext.CartItems
                                 where a.ContentItemID.Equals(GlobalID) && a.CartID.Equals(ShoppingCartId) && !a.ElectronicDelivery
                                 select a;

                    cartitem = null;
                    if (second.Count() == 0 || forceNew)
                    {
                        cartitem = new CartItem();
                        cartitem.CartID = ShoppingCartId;
                        cartitem.ContentItemID = GlobalID;
                        cartitem.Quantity = Quantity;
                        cartitem.ShippingInformationID = shippingInformationId;
                        datacontext.CartItems.InsertOnSubmit(cartitem);
                    }
                    else
                    {
                        foreach (CartItem item in second)
                        {
                            item.Quantity = Quantity;
                            item.ShippingInformationID = shippingInformationId;
                        }
                    }
                }
                else
                {
                    cartitem = q.FirstOrDefault();
                    cartitem.Quantity = Quantity;
                    cartitem.ShippingInformationID = shippingInformationId;
                }
            }
            datacontext.SubmitChanges();
        }

        public static void ResetCustomizationData(int ShoppingCardId)
        {
            MasterCardPortalDataContext datacontext = new MasterCardPortalDataContext();

            var customizations = from a in datacontext.CustomizationDetails
                                 where a.ShoppingCartID.Equals(ShoppingCardId)
                                 select a;

            List<CustomizationDetail> customizationsToDelete = new List<CustomizationDetail>();
            foreach (CustomizationDetail customization in customizations)
            {
                customizationsToDelete.Add(customization);
            }

            if (customizationsToDelete.Count > 0)
            {
                datacontext.CustomizationDetails.DeleteAllOnSubmit(customizationsToDelete);
                datacontext.SubmitChanges();
            }
        }

        public static void SaveCustomizationData(int ShoppingCardId, string CustomizationOptionItemID, string Data)
        {
            MasterCardPortalDataContext datacontext = new MasterCardPortalDataContext();

            CustomizationDetail customizationdetailitem = customizationdetailitem = new CustomizationDetail();
            customizationdetailitem.ShoppingCartID = ShoppingCardId;
            customizationdetailitem.CustomizationOptionID = CustomizationOptionItemID;
            customizationdetailitem.CustomizationData = Data;
            datacontext.CustomizationDetails.InsertOnSubmit(customizationdetailitem);

            datacontext.SubmitChanges();
        }

        public static void RemoveShoppingCartItem(int ShoppingCardId, string GlobalID, bool electronicDelivery)
        {
            MasterCardPortalDataContext datacontext = new MasterCardPortalDataContext();
            var q = from a in datacontext.CartItems
                    where a.ContentItemID.Equals(GlobalID) && a.CartID.Equals(ShoppingCardId) && a.ElectronicDelivery == electronicDelivery
                    select a;

            foreach (CartItem cartItem in q)
            {
                datacontext.CartItems.DeleteOnSubmit(cartItem);
                datacontext.SubmitChanges();
            }
        }

        public static void CleanShoppingCartItems(int ShoppingCardId)
        {
            MasterCardPortalDataContext datacontext = new MasterCardPortalDataContext();
            var q = from a in datacontext.CartItems
                    where a.CartID.Equals(ShoppingCardId) && a.Quantity.Equals(0)
                    select a;

            List<int> shippingInformationToCheck = new List<int>();
            foreach (CartItem cartItem in q)
            {
                if (!shippingInformationToCheck.Contains(cartItem.ShippingInformationID.Value))
                {
                    shippingInformationToCheck.Add(cartItem.ShippingInformationID.Value);
                }
                datacontext.CartItems.DeleteOnSubmit(cartItem);
                datacontext.SubmitChanges();
            }

            foreach (int shippingInformationId in shippingInformationToCheck)
            {
                var items = from a in datacontext.CartItems
                            where a.CartID.Equals(ShoppingCardId) && a.ShippingInformationID.Equals(shippingInformationId)
                            select a;

                if (items.Count() == 0)
                {
                    var shipping = from shippingInfo in datacontext.ShippingInformations
                                   where shippingInfo.ShippingInformationID.Equals(shippingInformationId)
                                   select shippingInfo;

                    ShippingInformation shippingInformation = new ShippingInformation();
                    if (shipping.Count() > 0)
                    {
                        shippingInformation = shipping.FirstOrDefault();
                        datacontext.ShippingInformations.DeleteOnSubmit(shippingInformation);
                        datacontext.SubmitChanges();
                    }
                }
            }
        }

        public static DataTable GetPriceTable(string PricingSchedule, decimal scheduleMinimumPrice)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("MinQuantity", System.Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("MaxQuantity", System.Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("Price", System.Type.GetType("System.Decimal")));
            dt.Columns.Add(new DataColumn("Comment", System.Type.GetType("System.String")));
            dt.AcceptChanges();
            int j = 0;

            string[] prices = Regex.Replace(PricingSchedule.Replace("<br />", "|"), @"<(.|\n)*?>", string.Empty).Split('|');

            if (prices.Length > 0)
            {
                for (int i = 0; i < prices.Length; i++)
                {
                    j = i + 1;
                    string Next = string.Empty;
                    if (j < prices.Length)
                    {
                        Next = prices[j];
                    }
                    string s = prices[i];

                    string[] values = s.Split(',');
                    string[] NextValues = Next.Split(',');

                    decimal UnitPrice = 0;
                    if (values.Length == 2)
                    {
                        if (i == 0)
                        {
                            DataRow drFirst = dt.NewRow();
                            UnitPrice = decimal.Parse(values[1]);
                            int FirstMin = 1;
                            int FirstMax = int.Parse(values[0]) - 1;
                            decimal MinimunPrice = scheduleMinimumPrice;
                            if (MinimunPrice == 0)
                            {
                                MinimunPrice = (UnitPrice * (FirstMax + 1));
                            }
                            drFirst["MinQuantity"] = FirstMin;
                            drFirst["MaxQuantity"] = FirstMax;
                            drFirst["Price"] = MinimunPrice;
                            drFirst["Comment"] = "Minimum";
                            dt.Rows.Add(drFirst);

                        }
                        if (i == prices.Length - 1)
                        {

                            DataRow dr = dt.NewRow();
                            UnitPrice = decimal.Parse(values[1]);

                            int MaxQunatity = int.Parse(values[0]);

                            dr["MinQuantity"] = DBNull.Value;
                            dr["MaxQuantity"] = MaxQunatity;
                            dr["Price"] = UnitPrice;
                            dr["Comment"] = "per item";
                            dt.Rows.Add(dr);

                        }
                        else
                        {
                            if (NextValues.Length == 2)
                            {
                                DataRow dr = dt.NewRow();
                                UnitPrice = decimal.Parse(values[1]);
                                int MinQunatity = int.Parse(values[0]);
                                int MaxQunatity = int.Parse(NextValues[0]) - 1;

                                dr["MinQuantity"] = MinQunatity;
                                dr["MaxQuantity"] = MaxQunatity;
                                dr["Price"] = UnitPrice;
                                dr["Comment"] = "per item";
                                dt.Rows.Add(dr);
                            }
                        }
                    }
                }
            }
            dt.AcceptChanges();
            return dt;

        }

        public static AssetPrice GetAssetPrice(string globalId, int quantity, bool electronicDelivery)
        {
            MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();
            AssetPrice assetPrice = new AssetPrice();

            RetrieveOrderableAssetsResult asset = dataContext.RetrieveOrderableAssets(globalId).FirstOrDefault();
            if (asset != null)
            {
                if (electronicDelivery)
                {
                    assetPrice.ItemPrice = asset.DownloadPrice.Value;
                    assetPrice.TotalPrice = quantity * asset.DownloadPrice.Value;
                }
                else
                {
                    PricingSchedule pricingSchedule = OrderableAssetService.GetPricingSchedule(globalId);

                    if (asset.PricingTable != null && pricingSchedule != null && !pricingSchedule.UnitPrice.HasValue)
                    {
                        DataTable dtPrice = GetPriceTable(asset.PricingTable, asset.MinimumPrice);
                        if (dtPrice.Rows.Count > 0)
                        {
                            DataView dvPrice = new DataView(dtPrice, "( ( " + quantity + "  >= MinQuantity and " + quantity + " <= MaxQuantity ) or  ( MinQuantity is null ))", "MaxQuantity", DataViewRowState.CurrentRows);
                            if (dvPrice.Count > 0)
                            {
                                if (dvPrice[0]["Comment"] != DBNull.Value && dvPrice[0]["Comment"].ToString().ToLower() == "minimum")
                                {
                                    assetPrice.TotalPrice = decimal.Parse(dvPrice[0]["price"].ToString());
                                    assetPrice.ItemPrice = 0;
                                }
                                else
                                {
                                    assetPrice.ItemPrice = decimal.Parse(dvPrice[0]["price"].ToString());
                                    assetPrice.TotalPrice = decimal.Parse(dvPrice[0]["price"].ToString()) * quantity;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (pricingSchedule != null)
                        {
                            if (pricingSchedule.UnitPrice.HasValue)
                            {
                                assetPrice.ItemPrice = pricingSchedule.UnitPrice.Value;
                                assetPrice.TotalPrice = pricingSchedule.UnitPrice.Value * quantity;
                            }
                        }
                    }
                }
            }
            return assetPrice;
        }

        public static List<RetrieveAllShoppingCartItemsByUserIdResult> GetAllShoppingCartItemsByUserId(string UserID)
        {
            var obj_MasterCardPortalDataContext = new MasterCardPortalDataContext();
            return new List<RetrieveAllShoppingCartItemsByUserIdResult>(obj_MasterCardPortalDataContext.RetrieveAllShoppingCartItemsByUserId(UserID));
        }

        public static List<RetrieveShoppingCartByUserIdResult> GetShoppingCartByUserId(string UserID)
        {
            var obj_MasterCardPortalDataContext = new MasterCardPortalDataContext();
            return obj_MasterCardPortalDataContext.RetrieveShoppingCartByUserId(UserID).ToList();
        }

        public static List<RetrieveCustomizationsResult> GetCustomizationOptions(string UserID, int ShoppingCartID, bool calculateOptions)
        {
            var obj_MasterCardPortalDataContext = new MasterCardPortalDataContext();
            return new List<RetrieveCustomizationsResult>(obj_MasterCardPortalDataContext.RetrieveCustomizations(UserID, ShoppingCartID, null, calculateOptions));
        }

        public static List<CartItem> GetShoppingCartItems(int cartId)
        {
            using (MasterCardPortalDataContext dc = new MasterCardPortalDataContext())
            {
                return (from ci in dc.CartItems where ci.CartID == cartId select ci).ToList();
            }
        }

        public static bool ShoppingCartContainsAssetsWithApproval(int cartId)
        {
            using (var dc = new MasterCardPortalDataContext())
            {
                var q = from ci in dc.CartItems
                        join oa in dc.OrderableAssets on ci.ContentItemID equals oa.ContentItemId
                        where
                            ci.CartID == cartId && oa.ApproverEmailAddress != null && oa.ApproverEmailAddress.Length > 0

                        select ci;

                return q.ToList().Any();
            }
        }

        public static void UpdateShoppingCartItemEstimatedDistribution(int cartId, string contentItemId, int estimatedDistribution)
        {
            using (MasterCardPortalDataContext dc = new MasterCardPortalDataContext())
            {
                var cartItem = (from ci in dc.CartItems where ci.CartID == cartId && ci.ContentItemID == contentItemId && ci.ElectronicDelivery select ci)
                               .FirstOrDefault();

                if (cartItem != null)
                {
                    cartItem.EstimatedDistribution = estimatedDistribution;
                    dc.SubmitChanges();
                }
            }
        }

        public static int GetPODContentItemTotalQuantityForShoppingCart(int cartId, string contentItemId)
        {
            using (MasterCardPortalDataContext dc = new MasterCardPortalDataContext())
            {
                var total = (from ci in dc.CartItems
                             where ci.CartID == cartId && ci.ContentItemID == contentItemId && !ci.ElectronicDelivery
                             select ci).Sum(x => x.Quantity ?? 0);

                return total;
            }
        }
    }

    public struct AssetPrice
    {
        public decimal TotalPrice { get; set; }
        public decimal ItemPrice { get; set; }
    }
}