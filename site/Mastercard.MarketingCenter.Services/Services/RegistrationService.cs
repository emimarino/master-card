﻿namespace Mastercard.MarketingCenter.Services
{
    public class RegistrationService
    {
        private readonly RegistrationOperationsService _registrationOperationsService;

        public RegistrationService(RegistrationOperationsService registrationOperationsService)
        {
            _registrationOperationsService = registrationOperationsService;
        }

        public void SetUserPassword(string username, string password)
        {
            _registrationOperationsService.SetUserPassword(username, password);
        }

        public string GetUserNameForEmail(string email)
        {
            return _registrationOperationsService.GetUserNameForEmail(email);
        }

        public bool VerifyUserCompletedRegistration(string username)
        {
            return _registrationOperationsService.VerifyUserCompletedRegistration(username);
        }
    }
}