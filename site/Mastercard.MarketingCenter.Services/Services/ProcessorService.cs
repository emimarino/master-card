﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class ProcessorService
    {
        private readonly ProcessorRepository _processorRepository;

        public ProcessorService(ProcessorRepository processorRepository)
        {
            _processorRepository = processorRepository;
        }

        public IQueryable<Processor> GetProcessors()
        {
            return _processorRepository.GetProcessors();
        }

        public Processor GetProcessorById(string processorId)
        {
            return _processorRepository.GetProcessorById(processorId);
        }

        public Processor GetProcessorByIssuerId(string issuerId)
        {
            return _processorRepository.GetProcessorsByIssuerId(issuerId).FirstOrDefault();
        }
    }
}