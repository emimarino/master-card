﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Entities;
using Mastercard.MarketingCenter.Services.Exceptions;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    internal class ContentCommentServices : IContentCommentServices
    {
        private readonly ContentCommentRepository _contentCommentRepository;
        private readonly AssetRepository _assetRepository;
        private readonly IAuthorizationService _authorizationService;
        private readonly UserContext _userContext;

        public ContentCommentServices(
            ContentCommentRepository contentCommentRepository,
            AssetRepository assetRepository,
            IAuthorizationService authorizationService,
            UserContext userContext)
        {
            _contentCommentRepository = contentCommentRepository;
            _assetRepository = assetRepository;
            _authorizationService = authorizationService;
            _userContext = userContext;
        }

        public bool AddNewComments(int senderUserId, IEnumerable<ContentCommentDTO> contentComments)
        {
            var result = true;
            foreach (var contentComment in contentComments)
            {
                result &= AddNew(senderUserId, contentComment.ContentItemID, contentComment.CommentBody);
            }

            return result;
        }

        private void AuthorizeCommentAccess(int requestingUserId, string contentItemId, string contentTypeId)
        {
            bool hasAccess = _authorizationService.Authorize(null, "Content", "Edit", contentTypeId, null, null) ||
                             _assetRepository.IsUserABusinessOwner(requestingUserId, contentItemId);

            if (!hasAccess)
            {
                throw new AuthorizationException();
            }
        }

        public string GetExtensionComment(int numOfDays, string commentBody)
        {
            return $"Extension request: {numOfDays} days. {commentBody}";
        }

        public bool AddNew(int senderUserId, string contentTypeId, string contentItemId, string commentBody)
        {
            this.AuthorizeCommentAccess(senderUserId, contentItemId, contentTypeId);

            return AddNew(senderUserId, contentItemId, commentBody);
        }

        private bool AddNew(int senderUserId, string contentItemId, string commentBody)
        {
            return _contentCommentRepository.Add(
                new ContentComment
                {
                    CommentBody = commentBody,
                    ContentItemId = contentItemId.GetAsLiveContentItemId(),
                    SenderUserId = senderUserId,
                    CreationDate = DateTime.Now
                }) != null;
        }

        public ContentCommentModelPagedList GetPaged(string contentTypeId, string contentItemId, int pageNumber, int pageSize = 5)
        {
            // authorizing an actual current user, not a senderUserId
            this.AuthorizeCommentAccess(_userContext.User.UserId, contentItemId, contentTypeId);

            contentItemId = contentItemId.GetAsLiveContentItemId();
            return new ContentCommentModelPagedList
            {
                ContentComments = _contentCommentRepository.GetCommentsPaged(contentItemId, pageNumber, pageSize)
                .Select(cc => new ContentCommentModel
                {
                    CommentBody = cc.CommentBody,
                    ContentCommentId = cc.ContentCommentId,
                    SenderUserName = cc.SenderUser.FullName,
                    CreationDate = cc.CreationDate
                }),
                ContentItemId = contentItemId,
                PageNumber = pageNumber,
                PageSize = pageSize,
                TotalComments = _contentCommentRepository.GetCommentsByContent(contentItemId)
            };
        }

        public int GetCommentsByContent(string contentItemId)
        {
            return _contentCommentRepository.GetCommentsByContent(contentItemId);
        }

        public IEnumerable<ContentCommentsCount> GetCommentsCount(int userId)
        {
            return _contentCommentRepository.GetCommentsCount(userId)
                .Select(cc => new ContentCommentsCount
                {
                    CommentsCount = cc.Count(),
                    ContentItemId = cc.Key,
                    SenderUserId = userId
                });
        }
    }
}