﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Models;
using Pulsus;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.Security;

namespace Mastercard.MarketingCenter.Services
{
    public class MastercardRolePrincipal : RolePrincipal
    {
        private UserRoleModel _userRole;
        private UserRoleModel UserRole
        {
            get
            {
                if (_userRole == null)
                {
                    _userRole = GetUserRole();
                }

                return _userRole;
            }
        }

        private UserContext _userContext;
        private IIdentity _user;
        private IAuthorizationRepository _authenticationRepository;
        private readonly ICachingService _cachingService;

        public MastercardRolePrincipal(
            IIdentity identity,
            string encryptedTicket,
            IAuthorizationRepository authenticationRepository,
            UserContext userContext,
            ICachingService cachingService)
            : base(identity, encryptedTicket)
        {
            _user = identity;
            _authenticationRepository = authenticationRepository;
            _userContext = userContext;
            _cachingService = cachingService;
        }

        public MastercardRolePrincipal(
            IIdentity identity,
            IAuthorizationRepository authenticationRepository,
            UserContext userContext,
            ICachingService cachingService)
            : base(identity)
        {
            _user = identity;
            _authenticationRepository = authenticationRepository;
            _userContext = userContext;
            _cachingService = cachingService;
        }

        public override bool IsInRole(string role)
        {
            if (!_user.IsAuthenticated || !UserRole.IsInCurrentRegion)
            {
                return false;
            }

            bool isInRole = UserRole.Name.Trim().Equals(role.Trim(), StringComparison.InvariantCultureIgnoreCase);
            if (!isInRole && IsInPermission(role))
            {
                LogManager.EventFactory.Create()
                                       .AddTags("MastercardRolePrincipal")
                                       .Text($"Wrong use of 'IsInRole', please use 'IsInPermission' for: '{role}'")
                                       .Level(LoggingEventLevel.Information)
                                       .Push();
                return true;
            }

            return isInRole;
        }

        public bool IsInPermission(string permission)
        {
            if (!_user.IsAuthenticated || !UserRole.IsInCurrentRegion)
            {
                return false;
            }

            return GetRolePermissions(UserRole.RoleId, _userContext.SelectedRegion).Any(r => r.Name.Trim().Equals(permission.Trim(), StringComparison.InvariantCultureIgnoreCase));
        }

        public bool IsInPermission(string permission, string region)
        {
            if (!_user.IsAuthenticated)
            {
                return false;
            }

            return GetRolePermissions(UserRole.RoleId, region).Any(r => r.Name.Trim().Equals(permission.Trim(), StringComparison.InvariantCultureIgnoreCase));
        }

        public bool IsInAdminRole(bool currentRegion = true)
        {
            if (!_user.IsAuthenticated || (currentRegion && !UserRole.IsInCurrentRegion))
            {
                return false;
            }

            return Constants.Roles.AdminRoles.Any(r => r.Equals(UserRole.Name, StringComparison.InvariantCultureIgnoreCase));
        }

        public bool IsInFullAdminRole(bool currentRegion = true)
        {
            if (!_user.IsAuthenticated || (currentRegion && !UserRole.IsInCurrentRegion))
            {
                return false;
            }

            return Constants.Roles.FullAdminRoles.Any(r => r.Equals(UserRole.Name, StringComparison.InvariantCultureIgnoreCase));
        }

        public bool IsInFullAdminRole(string region)
        {
            if (!_user.IsAuthenticated)
            {
                return false;
            }

            return Constants.Roles.FullAdminRoles.Any(r => r.Equals(UserRole.Name, StringComparison.InvariantCultureIgnoreCase)) &&
                   UserRole.Regions.Any(r => r.RegionId.Trim().Equals(region.Trim(), StringComparison.InvariantCultureIgnoreCase));
        }

        public string GetUserRoleName(bool currentRegion = true)
        {
            if (!_user.IsAuthenticated || (currentRegion && !UserRole.IsInCurrentRegion))
            {
                return string.Empty;
            }

            return UserRole.Name;
        }

        public UserRoleModel GetUserRole(int userId = 0)
        {
            userId = userId == 0 ? _userContext.User.UserId : userId;
            string cacheKey = _cachingService.GetUserRoleCacheKey(userId, _userContext.SelectedRegion);
            if (_cachingService.Get<UserRoleModel>(cacheKey) == null)
            {
                var userRole = _authenticationRepository.GetUserRoles(userId)
                                                        .GroupBy(r => r.Role)
                                                        .Select(g => new UserRoleModel
                                                        {
                                                            RoleId = g.Key.RoleId,
                                                            Name = g.Key.Name,
                                                            Description = g.Key.Description,
                                                            DisplayOrder = g.Key.DisplayOrder,
                                                            ShowRegions = g.Key.ShowRegions,
                                                            IsInCurrentRegion = g.Any(r => r.RegionId.Equals(_userContext.SelectedRegion, StringComparison.InvariantCultureIgnoreCase)),
                                                            Regions = g.Select(r => new UserRoleRegionModel
                                                            {
                                                                RegionId = r.RegionId,
                                                                Name = r.Region.Name
                                                            })
                                                        })
                                                        .First();

                _cachingService.Save(cacheKey, userRole, new TimeSpan(1, 0, 0));
            }

            return _cachingService.Get<UserRoleModel>(cacheKey);
        }

        public IList<Permission> GetRolePermissions(int roleId, string region)
        {
            string cacheKey = _cachingService.GetRolePermissionCacheKey(roleId, region);
            if (_cachingService.Get<IList<Permission>>(cacheKey) == null)
            {
                var rolePermissions = _authenticationRepository.GetRolePermissionsByRegion(roleId, region).ToList();

                _cachingService.Save(cacheKey, rolePermissions);
            }

            return _cachingService.Get<IList<Permission>>(cacheKey);
        }

        public void InvalidateUserRole(int userId = 0)
        {
            userId = userId == 0 ? _userContext.User.UserId : userId;
            var keys = _cachingService.Keys.Where(x => x.StartsWith(_cachingService.GetUserRoleCacheKey(userId)));
            foreach (var key in keys)
            {
                _cachingService.Delete(key);
            }
        }

        public void InvalidateRolePermissions(int roleId, string region)
        {
            _cachingService.Delete(_cachingService.GetRolePermissionCacheKey(roleId, region));
        }
    }
}