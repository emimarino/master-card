﻿using CsvHelper;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Mastercard.MarketingCenter.Services
{
    public class CsvFileService : BaseFileService, ICsvFileService
    {
        public CsvFileService()
        {
        }

        /// <summary>
        /// Read a csv file.
        /// </summary>
        /// <param name="csvFileStream">the stream of the source csv file</param>
        /// <returns></returns>
        public IEnumerable<T> ReadCsv<T>(Stream csvFileStream) where T : class
        {
            using (var reader = new StreamReader(csvFileStream))
            {
                return MapCsv<T>(reader);
            }
        }

        /// <summary>
        /// Read a csv file.
        /// </summary>
        /// <param name="csvFilePath">the path of the source csv file</param>
        /// <returns></returns>
        public IEnumerable<T> ReadCsv<T>(string csvFilePath) where T : class
        {
            var fullCsvFilePath = MapPath(csvFilePath);
            if (File.Exists(fullCsvFilePath))
            {
                using (var reader = new StreamReader(fullCsvFilePath))
                {
                    return MapCsv<T>(reader);
                }
            }

            return null;
        }

        private IEnumerable<T> MapCsv<T>(StreamReader reader) where T : class
        {
            Regex pattern = new Regex(@"""sep=(?<separator>.*?)""");
            Match match = pattern.Match(reader.ReadLine());
            string separator = match.Groups["separator"].Value;
            if (string.IsNullOrEmpty(separator))
            {
                reader.DiscardBufferedData();
                reader.BaseStream.Seek(0, SeekOrigin.Begin);
            }

            using (var csvReader = new CsvReader(reader))
            {
                if (!string.IsNullOrEmpty(separator))
                {
                    csvReader.Configuration.Delimiter = separator;
                }

                return csvReader.GetRecords<T>().ToList();
            }
        }
    }
}