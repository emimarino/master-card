﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Decorators;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Models;
using Slam.Cms.Data;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Services.Services
{
    public class MastercardAuthorizationService : IAuthorizationService
    {
        private readonly CmsRoleAccessRepository _cmsRoleAccessRepository;
        private readonly IAuthorizationRepository _authenticationRepository;
        private readonly MastercardRolePrincipal _mastercardRolePrincipal;
        private readonly AssetRepository _assetRepository;
        private readonly IRegionService _regionService;
        private readonly UserContext _userContext;
        private readonly ISettingsService _settingsService;

        public MastercardAuthorizationService(
            CmsRoleAccessRepository cmsRoleAccessRepository,
            IAuthorizationRepository authenticationRepository,
            IPrincipal principal,
            AssetRepository assetRepository,
            IRegionService regionService,
            UserContext userContext,
            ISettingsService settingsService)
        {
            _cmsRoleAccessRepository = cmsRoleAccessRepository;
            _authenticationRepository = authenticationRepository;
            _mastercardRolePrincipal = principal as MastercardRolePrincipal;
            _assetRepository = assetRepository;
            _regionService = regionService;
            _userContext = userContext;
            _settingsService = settingsService;
        }

        public bool Authorize(string area, string controller, string action, string id, string queryString = null, string contentAction = null)
        {
            if (string.Equals(controller, "Home", System.StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }

            var requiredRoles = _cmsRoleAccessRepository.GetByRoute(area, controller, action, id, queryString, contentAction)
                                                        .ToList()
                                                        .Where(r => _mastercardRolePrincipal.IsInRole(r.RoleType));

            requiredRoles = FilterMarketingCalendarRoutes(requiredRoles);

            if (requiredRoles.Any(a => !string.IsNullOrEmpty(a.RequiredRolePermissionName))) //if the accessible routes have permission requirements one of them must be matched
            {
                requiredRoles = requiredRoles.Where(a => !string.IsNullOrEmpty(a.RequiredRolePermissionName));
            }

            if (requiredRoles.Any(a => !string.IsNullOrEmpty(a.QueryString))) //if the accessible routes have query string requirements one of them must be matched
            {
                requiredRoles = requiredRoles.Where(a => !string.IsNullOrEmpty(a.QueryString));
            }

            if (requiredRoles.Any(a => a.RequiredRolePermissionName == null))
            {
                // CmsRoleAccess don't have records to match, request should be authorized
                return true;
            }

            requiredRoles = requiredRoles.Where(a => _mastercardRolePrincipal.IsInPermission(a.RequiredRolePermissionName));

            return requiredRoles.Any();
        }

        public bool Authorize(IDictionary<string, object> routeData, string queryString = null)
        {
            string area = (routeData["Area"] == null) ? string.Empty : routeData["Area"].ToString();
            string controller = (routeData["Controller"] == null) ? string.Empty : routeData["Controller"].ToString();
            string action = (routeData["Action"] == null) ? "Index" : routeData["Action"].ToString();
            string id = (routeData["Id"] == null) ? string.Empty : routeData["Id"].ToString();

            if (string.IsNullOrEmpty(id))
            {
                id = routeData["listId"] == null ? string.Empty : routeData["listId"].ToString();
            }

            return AuthorizeWithDecorators(_userContext, _settingsService, area, controller, action, id, queryString);
        }

        private bool AuthorizeWithDecorators(UserContext userContext, ISettingsService settingsService, string area, string controller, string action, string id, string queryString, string contentAction = null)
        {
            var decorator = new OffersSettingAuthorizationDecorator(this, settingsService, userContext);
            return decorator.Authorize(area, controller, action, id, queryString, contentAction);
        }

        public bool AuthorizeBusinessOwnerForChangingReviewState(int userId, string contentItemId)
        {
            return _assetRepository.IsUserABusinessOwner(userId, contentItemId);
        }

        public bool AuthorizeBusinessOwnerForDeletion(int userId, ContentItemBase contentItem)
        {
            return (contentItem.StatusID == Status.DraftOnly || contentItem.StatusID == Status.PublishedAndDraft)
                && _assetRepository.IsUserABusinessOwner(userId, contentItem.ContentItemId);
        }

        public IEnumerable<CmsRoleAccess> GetAuthorizedResourcesByUser()
        {
            var accessibleRoutes = _cmsRoleAccessRepository
                .Select<CmsRoleAccess>()
                .ToList()
                .Where(a => _mastercardRolePrincipal.IsInRole(a.RoleType));

            return accessibleRoutes.ToList();
        }

        public IEnumerable<Region> GetAuthorizedRegions()
        {
            var userRegions = GetCurrentUserRole().Regions.Select(r => r.RegionId.Trim());
            foreach (var region in _regionService.GetAll())
            {
                if (userRegions.Contains(region.IdTrimmed) && _mastercardRolePrincipal.IsInAdminRole())
                {
                    yield return region;
                }
            }
        }

        private IEnumerable<CmsRoleAccess> FilterMarketingCalendarRoutes(IEnumerable<CmsRoleAccess> accessibleRoutes)
        {
            var marketingCalendarEnabled = false;
            bool.TryParse(RegionalizeService.RegionalizeSetting("MarketingCalendar.Enabled", _userContext.SelectedRegion.Trim(), false), out marketingCalendarEnabled);
            if (!marketingCalendarEnabled && accessibleRoutes != null)
            {
                accessibleRoutes = accessibleRoutes.Where(r => r != null && (r.Id == null || (r.Id != null && !r.Id.Equals(MarketingCenterDbConstants.MarketingCalendarListIds.CampaignCategory) &&
                    !r.Id.Equals(MarketingCenterDbConstants.MarketingCalendarListIds.CampaignEvent))));
            }

            return accessibleRoutes;
        }

        public IEnumerable<RoleRegion> GetRolesByRegion(string region)
        {
            return _authenticationRepository.GetRolesByRegion(region);
        }

        public IEnumerable<Role> GetRegionRolesForCurrentUser()
        {
            return _authenticationRepository.GetRolesByRegion(_userContext.SelectedRegion.Trim())
                                            .ToList()
                                            .Where(r => string.IsNullOrEmpty(r.PermissionName) || _mastercardRolePrincipal.IsInPermission(r.PermissionName))
                                            .Select(r => r.Role);
        }

        public UserRoleModel GetCurrentUserRole()
        {
            return GetUserRole(_userContext.User.UserId);
        }

        public UserRoleModel GetUserRole(int userId)
        {
            return _mastercardRolePrincipal.GetUserRole(userId);
        }

        public void UpdateUserRole(int userId, int roleId, IEnumerable<string> regionIds)
        {
            _authenticationRepository.GetUserRoles(userId).ForEach(r => _authenticationRepository.DeleteUserRoleRegion(r));

            if (regionIds == null || !regionIds.Any())
            {
                regionIds = _authenticationRepository.GetRegionsByRole(roleId)
                                                     .ToList()
                                                     .Where(r => string.IsNullOrEmpty(r.PermissionName) || _mastercardRolePrincipal.IsInPermission(r.PermissionName))
                                                     .Select(r => r.RegionId);
            }

            regionIds.ForEach(regionId => _authenticationRepository.InsertUserRoleRegion(new UserRoleRegion { UserId = userId, RoleId = roleId, RegionId = regionId }));

            _authenticationRepository.Commit();
        }
    }
}