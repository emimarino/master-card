﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mastercard.MarketingCenter.Services.Data;

namespace Mastercard.MarketingCenter.Services
{
    public static class ShippingServices
    {
        public static void DeleteShippingInformation(int shoppingCartId, int shippingInformationId)
        {
            MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();

            var q = from shippingInfo in dataContext.ShippingInformations
                    where shippingInfo.ShippingInformationID.Equals(shippingInformationId)
                    select shippingInfo;

            if (q.Count() > 0)
            {
                ShippingInformation shippingInformation = q.FirstOrDefault();
                dataContext.ShippingInformations.DeleteOnSubmit(shippingInformation);
                dataContext.SubmitChanges();
            }

            var cartItems = from cartItem in dataContext.CartItems
                            where cartItem.ShippingInformationID.Equals(shippingInformationId) &&
                                  cartItem.CartID.Equals(shoppingCartId) 
                            select cartItem;

            if (cartItems.Count() > 0)
            {
                CartItem item = cartItems.FirstOrDefault();
                dataContext.CartItems.DeleteOnSubmit(item);
                dataContext.SubmitChanges();
            }
        }

        public static int GetDefaultShippingInformationIdForCart(int shoppingCartId)
        {
            MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();

            var q = from shippingInfo in dataContext.ShippingInformations
                    where shippingInfo.ShoppingCartID.Equals(shoppingCartId)
                    select shippingInfo;

            ShippingInformation shippingInformation = q.FirstOrDefault();
            return shippingInformation != null ? shippingInformation.ShippingInformationID : 0;
        }

        public static List<ShippingInformation> GetShippingInformationForCart(int shoppingCartId)
        {
            MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();

            var q = from shippingInfo in dataContext.ShippingInformations
                    where shippingInfo.ShoppingCartID.Equals(shoppingCartId)
                    select shippingInfo;

            IEnumerable<ShippingInformation> shippingInformation = q.Cast<ShippingInformation>();
            return shippingInformation.FirstOrDefault() != null ? shippingInformation.ToList() : new List<ShippingInformation>();
        }

        public static ShoppingCart GetShoppingCartOptions(int shoppingCartId)
        {
            MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();

            var q = from cart in dataContext.ShoppingCarts
                    where cart.CartID.Equals(shoppingCartId)
                    select cart;

            IEnumerable<ShoppingCart> shoppingCart = q.Cast<ShoppingCart>();
            return shoppingCart.SingleOrDefault();
        }

        public static int SetShippingDetails(int shoppingCartId, int shippingInformationId, string contactName, string phone, string address)
        {
            MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();

            var q = from shippingInfo in dataContext.ShippingInformations
                    where shippingInfo.ShippingInformationID.Equals(shippingInformationId)
                    select shippingInfo;

            ShippingInformation shippingInformation = new ShippingInformation();
            if (q.Count() == 0)
            {
                shippingInformation = new ShippingInformation();
                shippingInformation.ContactName = contactName;
                shippingInformation.ShoppingCartID = shoppingCartId;
                shippingInformation.Phone = phone;
                shippingInformation.Address = address;
                dataContext.ShippingInformations.InsertOnSubmit(shippingInformation);
            }
            else
            {
                shippingInformation = q.FirstOrDefault();
                shippingInformation.ContactName = contactName;
                shippingInformation.ShoppingCartID = shoppingCartId;
                shippingInformation.Phone = phone;
                shippingInformation.Address = address;
            }
            dataContext.SubmitChanges();
            return shippingInformation.ShippingInformationID;
        }
        
        public static void UpdateShoppingCart(int shoppingCardId, string shippingInstructions, bool rushOrder)
        {
            MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();

            var shoppingCarts = from cart in dataContext.ShoppingCarts
                                where cart.CartID.Equals(shoppingCardId)
                                select cart;

            ShoppingCart shoppingCart = null;
            if (shoppingCarts.Count() > 0)
            {
                shoppingCart = shoppingCarts.FirstOrDefault();
                shoppingCart.ShippingInstructions = shippingInstructions;
                shoppingCart.RushOrder = rushOrder;
            }
            dataContext.SubmitChanges();
        }
    }
}
