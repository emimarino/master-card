﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class PendingRegistrationService : IPendingRegistrationService
    {
        private readonly ProcessorService _processorService;
        private readonly IEmailService _emailService;
        private readonly IPendingRegistrationRepository _pendingRegistrationRepository;
        private readonly IIssuerService _issuerService;
        private readonly ISettingsService _settingsService;
        private readonly IUserService _userService;
        private readonly IPardotIntegrationService _pardotIntegrationService;

        public PendingRegistrationService(ProcessorService processorService, IEmailService emailService, IPendingRegistrationRepository pendingRegistrationRepository, IIssuerService issuerService, ISettingsService settingsService, IUserService userService, IPardotIntegrationService pardotIntegrationService)
        {
            _processorService = processorService;
            _emailService = emailService;
            _pendingRegistrationRepository = pendingRegistrationRepository;
            _issuerService = issuerService;
            _settingsService = settingsService;
            _userService = userService;
            _pardotIntegrationService = pardotIntegrationService;
        }

        public PendingRegistration GetPendingRegistrationsByGuid(string guid)
        {
            return _pendingRegistrationRepository.GetByGuid(guid);
        }

        public IEnumerable<PendingRegistration> GetPendingRegistrationsByEmail(string email)
        {
            return _pendingRegistrationRepository.GetByEmail(email);
        }

        public IEnumerable<PendingRegistration> GetPendingRegistrationsByStatus(string status, string regionId)
        {
            return _pendingRegistrationRepository.GetByStatus(status, regionId);
        }

        public void AddPendingRegistration(string firstName, string lastName, string issuerName, string address1, string address2, string address3, string city, int stateID, string zipCode, string phone, string fax, string mobile, string email, string title, string processor, string country, string language, string regionId, string province, string siteName, string acceptedConsents, bool acceptedEmailSubscriptions)
        {
            var newGuid = Guid.NewGuid();
            _pendingRegistrationRepository.Insert(new PendingRegistration
            {
                FirstName = firstName,
                LastName = lastName,
                FinancialInstitution = issuerName,
                Phone = phone,
                Mobile = mobile,
                Fax = fax,
                Email = email,
                Address1 = address1,
                Address2 = address2,
                Address3 = address3,
                City = city,
                StateID = stateID,
                Zip = zipCode,
                Title = title,
                Processor = processor,
                DateAdded = DateTime.Now,
                GUID = newGuid,
                Status = Constants.PendingRegistrationStatus.Pending,
                RegionId = regionId.Trim(),
                Country = country,
                Language = language,
                Province = province,
                AcceptedConsents = acceptedConsents,
                AcceptedEmailSubscriptions = acceptedEmailSubscriptions
            });
            _pendingRegistrationRepository.Commit();

            var pr = _pendingRegistrationRepository.GetByGuid(newGuid.ToString());
            _emailService.SendNewPendingRegistrationEmail(pr.FirstName, pr.LastName, pr.Title, pr.FinancialInstitution, pr.Processor, pr.Phone, pr.Mobile, pr.Fax, pr.Email, pr.Address1, pr.Address2, pr.Address3, pr.City, pr.Province, pr.RegionId.Trim().Equals("us", StringComparison.OrdinalIgnoreCase) && pr.State != null ? pr.State.StateName : string.Empty, pr.Zip, pr.Country, pr.GUID.ToString(), siteName, 0, pr.RegionId, pr.Language);
        }

        public void RejectPendingRegistration(string guid, string reason, string adminEmail, string regionId, string siteName)
        {
            UpdatePendingRegistrationStatus(guid, Constants.PendingRegistrationStatus.Rejected, adminEmail, reason);

            var pr = _pendingRegistrationRepository.GetByGuid(guid);

            _emailService.SendPendingRegistrationRejectedEmail(pr.Email, pr.RejectionReason, siteName, 0, pr.RegionId, pr.Language);
        }

        public User ApprovePendingRegistration(string guid, string issuerListItemID, string adminEmail, string domainToAdd, string siteName)
        {
            var pr = _pendingRegistrationRepository.GetByGuid(guid);
            if (pr != null)
            {
                var issuer = _issuerService.GetIssuerById(issuerListItemID);
                var processor = issuer.Processors?.FirstOrDefault();
                string state = pr.RegionId.Trim().Equals("us", StringComparison.OrdinalIgnoreCase) ? pr.State.StateName : pr.Province;

                if (!string.IsNullOrEmpty(domainToAdd))
                {
                    _issuerService.AddIssuerDomain(issuer.IssuerId, domainToAdd);
                }

                var user = _userService.CreateUser(
                    pr.FirstName,
                    pr.LastName,
                    pr.FinancialInstitution,
                    pr.Address1,
                    pr.Address2,
                    pr.Address3,
                    pr.City,
                    state,
                    pr.Zip,
                    pr.Phone,
                    pr.Fax,
                    pr.Mobile,
                    pr.Email,
                    issuer.IssuerId,
                    pr.Processor,
                    pr.Title,
                    pr.Country,
                    pr.Language,
                    pr.RegionId,
                    pr.AcceptedConsents,
                    pr.AcceptedEmailSubscriptions
                );

                if (user != null)
                {
                    UpdatePendingRegistrationStatus(guid, Constants.PendingRegistrationStatus.Approved, adminEmail, null);

                    _emailService.SendPendingRegistrationApprovedEmail(pr.Email, pr.FirstName, user.UserName, siteName, user.UserId, pr.RegionId, pr.Language, pr.LastName);

                    if (_settingsService.GetPardotIntegrationEnabled(pr.RegionId))
                    {
                        _pardotIntegrationService.SubmitPardotForm(_pardotIntegrationService.GetPardotIntegrationModel(
                            pr.RegionId,
                            pr.FirstName,
                            pr.LastName,
                            pr.Title,
                            pr.FinancialInstitution,
                            pr.Processor,
                            pr.Phone,
                            pr.Mobile,
                            pr.Fax,
                            pr.Email,
                            pr.Country,
                            pr.Language,
                            pr.Address1,
                            pr.Address2,
                            pr.Address3,
                            pr.City,
                            state,
                            pr.Zip,
                            pr.AcceptedConsents,
                            pr.AcceptedEmailSubscriptions
                        ));
                    }

                    return user;
                }
            }

            return null;
        }

        public User ApprovePendingRegistration(string guid, string name, string domains, string ica, string[] segmentationIds, string adminEmail, string processorId, string regionId, int CreatingUserId, string cid, string siteName)
        {
            var pr = _pendingRegistrationRepository.GetByGuid(guid);
            if (pr != null)
            {
                var processor = (processorId != "-1") ? _processorService.GetProcessorById(processorId) : null;
                var issuerId = _issuerService.CreateIssuer(name, domains, ica, segmentationIds, processor == null ? null : processor.ProcessorId, regionId, CreatingUserId, cid);
                string state = pr.RegionId.Trim().Equals("us", StringComparison.OrdinalIgnoreCase) ? pr.State.StateName : pr.Province;

                var user = _userService.CreateUser(
                    pr.FirstName,
                    pr.LastName,
                    pr.FinancialInstitution,
                    pr.Address1,
                    pr.Address2,
                    pr.Address3,
                    pr.City,
                    state,
                    pr.Zip,
                    pr.Phone,
                    pr.Fax ?? string.Empty,
                    pr.Mobile ?? string.Empty,
                    pr.Email,
                    issuerId,
                    pr.Processor,
                    pr.Title,
                    pr.Country,
                    pr.Language,
                    pr.RegionId,
                    pr.AcceptedConsents,
                    pr.AcceptedEmailSubscriptions
                );

                if (user != null)
                {
                    UpdatePendingRegistrationStatus(guid, Constants.PendingRegistrationStatus.Approved, adminEmail, null);

                    _emailService.SendPendingRegistrationApprovedEmail(pr.Email, pr.FirstName, user.UserName, siteName, user.UserId, pr.RegionId, pr.Language, pr.LastName);

                    if (_settingsService.GetPardotIntegrationEnabled(pr.RegionId))
                    {
                        _pardotIntegrationService.SubmitPardotForm(_pardotIntegrationService.GetPardotIntegrationModel(
                            pr.RegionId,
                            pr.FirstName,
                            pr.LastName,
                            pr.Title,
                            pr.FinancialInstitution,
                            pr.Processor,
                            pr.Phone,
                            pr.Mobile,
                            pr.Fax,
                            pr.Email,
                            pr.Country,
                            pr.Language,
                            pr.Address1,
                            pr.Address2,
                            pr.Address3,
                            pr.City,
                            state,
                            pr.Zip,
                            pr.AcceptedConsents,
                            pr.AcceptedEmailSubscriptions
                        ));
                    }

                    return user;
                }
            }

            return null;
        }

        private void UpdatePendingRegistrationStatus(string guid, string status, string adminEmail, string reason)
        {
            var pr = _pendingRegistrationRepository.GetByGuid(guid);
            if (pr != null)
            {
                pr.DateStatusChanged = DateTime.Now;
                pr.StatusChangedBy = adminEmail;
                pr.Status = status;
                if (status == Constants.PendingRegistrationStatus.Rejected)
                {
                    pr.RejectionReason = reason;
                }

                _pendingRegistrationRepository.Update(pr);
                _pendingRegistrationRepository.Commit();
            }
        }

        public int ExpireOrphanPendingRegistration(int expirationWindowInMonths)
        {
            // Use new database connection to ensure data integrity
            using (var marketingCenterDbContext = new MarketingCenterDbContext())
            {
                using (var pendingRegistrationRepository = new PendingRegistrationRepository(marketingCenterDbContext, new UserRepository(marketingCenterDbContext, new StandaloneMembershipProvider())))
                {
                    var expiredPendingRegistrations = pendingRegistrationRepository.GetExpiredPendingRegistrations(DateTime.Now.AddMonths(-expirationWindowInMonths)).ToList();
                    foreach (var pendingRegistration in expiredPendingRegistrations)
                    {
                        pendingRegistration.Address1 = Mask(pendingRegistration.Address1);
                        pendingRegistration.Address2 = Mask(pendingRegistration.Address2);
                        pendingRegistration.Address3 = Mask(pendingRegistration.Address3);
                        pendingRegistration.City = Mask(pendingRegistration.City);
                        pendingRegistration.Country = Mask(pendingRegistration.Country);
                        pendingRegistration.Fax = Mask(pendingRegistration.Fax);
                        pendingRegistration.FirstName = Mask(pendingRegistration.FirstName);
                        pendingRegistration.LastName = Mask(pendingRegistration.LastName);
                        pendingRegistration.Mobile = Mask(pendingRegistration.Mobile);
                        pendingRegistration.Phone = Mask(pendingRegistration.Phone);
                        pendingRegistration.Title = Mask(pendingRegistration.Title);
                        pendingRegistration.Zip = Mask(pendingRegistration.Zip);
                        pendingRegistration.Email = Mask(pendingRegistration.Email);
                        pendingRegistration.FinancialInstitution = Mask(pendingRegistration.FinancialInstitution);
                        pendingRegistration.Processor = Mask(pendingRegistration.Processor);
                        pendingRegistration.Province = Mask(pendingRegistration.Province);

                        pendingRegistration.Status = "Expired";
                        pendingRegistration.DateStatusChanged = DateTime.Now;
                        pendingRegistration.RejectionReason = "Expired by GDPR policy.";
                        pendingRegistration.StatusChangedBy = "superadmin@admin.com";

                        pendingRegistrationRepository.Update(pendingRegistration);
                    }

                    if (expiredPendingRegistrations.Any())
                    {
                        pendingRegistrationRepository.Commit();
                    }

                    return expiredPendingRegistrations.Count;
                }
            }
        }

        private string Mask(string value)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : Guid.NewGuid().ToString();
        }
    }
}