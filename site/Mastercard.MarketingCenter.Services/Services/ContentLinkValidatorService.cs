﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using EnvironmentConfig = Slam.Cms.Configuration.ConfigurationManager;

namespace Mastercard.MarketingCenter.Services
{
    public class ContentLinkValidatorService : IContentLinkValidatorService
    {
        private readonly BrokenContentLinkRepository _brokenContentLinkRepository;
        private readonly ContentItemRepository _contentItemRepository;
        private readonly Sitemap _sitemap;
        private readonly ISettingsService _settingsService;
        private IEnumerable<SnippetIndex> _snippets = null;

        public ContentLinkValidatorService(BrokenContentLinkRepository brokenContentLinkRepository, ContentItemRepository contentItemRepository, Sitemap sitemap, ISettingsService settingsService)
        {
            _brokenContentLinkRepository = brokenContentLinkRepository;
            _contentItemRepository = contentItemRepository;
            _sitemap = sitemap;
            _settingsService = settingsService;
        }

        public void UpdateContentBrokenLinks(IList<string> excludeUrls)
        {
            var remainingBrokenLinks = _brokenContentLinkRepository.Select<BrokenContentLink>().ToList();
            var updatedBrokenLinks = new List<BrokenContentLink>();

            foreach (var item in _brokenContentLinkRepository.GetPotentialBrokenContentLinkContent())
            {
                ProcessItemField(excludeUrls, remainingBrokenLinks, updatedBrokenLinks, item, item.Text1);
                ProcessItemField(excludeUrls, remainingBrokenLinks, updatedBrokenLinks, item, item.Text2);
                ProcessItemField(excludeUrls, remainingBrokenLinks, updatedBrokenLinks, item, item.Text3);
            }

            foreach (var remainingBrokenLink in remainingBrokenLinks)
            {
                _brokenContentLinkRepository.Delete(remainingBrokenLink);
            }

            foreach (var updatedBrokenLink in updatedBrokenLinks)
            {
                if (updatedBrokenLink.BrokenContentLinkId == 0)
                {
                    _brokenContentLinkRepository.Insert(updatedBrokenLink);
                }
                else
                {
                    _brokenContentLinkRepository.Update(updatedBrokenLink);
                }
            }

            _brokenContentLinkRepository.Commit();
        }

        private void ProcessItemField(IList<string> excludeUrls, List<BrokenContentLink> remainingBrokenLinks, List<BrokenContentLink> updatedBrokenLinks, PotentialBrokenContentLinkContent item, string fieldValue)
        {
            CheckLinks(excludeUrls, remainingBrokenLinks, updatedBrokenLinks, item, fieldValue);
            CheckSnippets(remainingBrokenLinks, updatedBrokenLinks, item, fieldValue);
            CheckSearchTerms(excludeUrls, remainingBrokenLinks, updatedBrokenLinks, item, fieldValue);
            CheckMarqueeSlides(excludeUrls, remainingBrokenLinks, updatedBrokenLinks, item, fieldValue);
        }

        private void CheckMarqueeSlides(IList<string> excludeUrls, List<BrokenContentLink> remainingBrokenLinks, List<BrokenContentLink> updatedBrokenLinks, PotentialBrokenContentLinkContent item, string fieldValue)
        {
            var matches = Regex.Matches(fieldValue ?? string.Empty, @"^/portal/[^//]*/[^//]*/.*");
            if (matches.Count > 0 && !excludeUrls.Contains(matches[0].Value) && item.ContentTypeId != null && item.ContentTypeId.Equals(MarketingCenterDbConstants.ContentTypeIds.MarqueeSlide))
            {
                var brokenLinks = CheckForBrokenMarqueeSlide(remainingBrokenLinks, item.Id, item.Title, item.ItemType, item.RegionId, matches[0].Value.Replace("/portal", string.Empty));
                UpdateBrokenLinks(brokenLinks, updatedBrokenLinks);
            }
        }

        private BrokenContentLink[] CheckForBrokenMarqueeSlide(List<BrokenContentLink> remainingBrokenLinks, string id, string title, string itemType, string regionId, string potentialBrokenLink)
        {
            var brokenContents = new List<BrokenContentLink>();
            var potentialBrokenContentItem = _contentItemRepository.GetByFrontEndUrl(potentialBrokenLink);
            var potentialBrokenLinkStatus = _brokenContentLinkRepository.GetBrokenContentStatus(potentialBrokenContentItem?.ContentItemId, null);

            if (potentialBrokenContentItem != null && !potentialBrokenContentItem.ContentItemId.IsDraftContentItemId() && !potentialBrokenLinkStatus.Equals(MarketingCenterDbConstants.ContentStatus.Published))
            {
                brokenContents.Add(CreateBrokenMarqueeSlide(remainingBrokenLinks, id, title, itemType, regionId, potentialBrokenContentItem, potentialBrokenLinkStatus));
            }
            return brokenContents.ToArray();
        }

        private BrokenContentLink CreateBrokenMarqueeSlide(List<BrokenContentLink> remainingBrokenLinks, string id, string title, string itemType, string regionId, ContentItemBase brokenContentItem, string brokenContentItemStatus)
        {
            BrokenContentLink brokenContent = null;
            brokenContent = remainingBrokenLinks.FirstOrDefault(l => l.BrokenLinkReference != null && l.BrokenLinkReference.ToLower().Equals($"{EnvironmentConfig.Environment.FrontEndUrl}{brokenContentItem.FrontEndUrl}".ToLower()));
            if (brokenContent != null)
            {
                remainingBrokenLinks.Remove(brokenContent);
            }
            else
            {
                brokenContent = new BrokenContentLink();
            }

            brokenContent.ContentType = itemType;
            brokenContent.LastRequested = DateTime.UtcNow;
            brokenContent.ReferrerUrl = null;
            brokenContent.BrokenLinkReference = brokenContentItem.FrontEndUrl == null ? null : GetBrokenLinkReference(brokenContentItem.FrontEndUrl);
            brokenContent.BrokenLinkText = title;
            brokenContent.BackEndUrl = EnvironmentConfig.Environment.AdminUrl + $"/Content/Edit?id={id}";
            brokenContent.RegionId = regionId;
            brokenContent.SourceItemId = id;
            brokenContent.Title = title;
            brokenContent.BrokenContentStatus = brokenContentItemStatus;
            return brokenContent;
        }

        private void CheckSearchTerms(IList<string> excludeUrls, List<BrokenContentLink> remainingBrokenLinks, List<BrokenContentLink> updatedBrokenLinks, PotentialBrokenContentLinkContent item, string fieldValue)
        {
            var matches = Regex.Matches(fieldValue ?? string.Empty, @"^/portal/[^//]*/[^//]*/.*");
            if (matches.Count > 0 && !excludeUrls.Contains(matches[0].Value) && item.ContentTypeId == null)
            {
                var brokenLinks = CheckForBrokenSearchTerm(remainingBrokenLinks, item.Id, item.Title, item.ItemType, item.RegionId, matches[0].Value.Replace("/portal", string.Empty));
                UpdateBrokenLinks(brokenLinks, updatedBrokenLinks);
            }
        }

        private BrokenContentLink[] CheckForBrokenSearchTerm(List<BrokenContentLink> remainingBrokenLinks, string id, string title, string itemType, string regionId, string potentialBrokenLink)
        {
            var brokenContents = new List<BrokenContentLink>();
            var potentialBrokenContentItem = _contentItemRepository.GetByFrontEndUrl(potentialBrokenLink);
            var potentialBrokenLinkStatus = _brokenContentLinkRepository.GetBrokenContentStatus(potentialBrokenContentItem?.ContentItemId, null);

            if (potentialBrokenContentItem != null && !potentialBrokenContentItem.ContentItemId.IsDraftContentItemId() && !potentialBrokenLinkStatus.Equals(MarketingCenterDbConstants.ContentStatus.Published))
            {
                brokenContents.Add(CreateBrokenSearchTerm(remainingBrokenLinks, id, title, itemType, regionId, potentialBrokenContentItem, potentialBrokenLinkStatus));
            }
            return brokenContents.ToArray();
        }

        private BrokenContentLink CreateBrokenSearchTerm(List<BrokenContentLink> remainingBrokenLinks, string id, string title, string itemType, string regionId, ContentItemBase brokenContentItem, string brokenContentItemStatus)
        {
            BrokenContentLink brokenContent = null;
            brokenContent = remainingBrokenLinks.FirstOrDefault(l => l.BrokenLinkReference != null && l.BrokenLinkReference.ToLower().Equals($"{EnvironmentConfig.Environment.FrontEndUrl}{brokenContentItem.FrontEndUrl}".ToLower()));
            if (brokenContent != null)
            {
                remainingBrokenLinks.Remove(brokenContent);
            }
            else
            {
                brokenContent = new BrokenContentLink();
            }

            brokenContent.ContentType = itemType;
            brokenContent.LastRequested = DateTime.UtcNow;
            brokenContent.ReferrerUrl = null;
            brokenContent.BrokenLinkReference = brokenContentItem.FrontEndUrl == null ? null : GetBrokenLinkReference(brokenContentItem.FrontEndUrl);
            brokenContent.BrokenLinkText = title;
            brokenContent.BackEndUrl = EnvironmentConfig.Environment.AdminUrl + $"/List/Edit?listId={ MarketingCenterDbConstants.MarketingCalendarListIds.SearchTerm }&id={ id }";
            brokenContent.RegionId = regionId;
            brokenContent.SourceItemId = id;
            brokenContent.Title = title;
            brokenContent.BrokenContentStatus = brokenContentItemStatus;
            return brokenContent;
        }

        private void CheckSnippets(List<BrokenContentLink> remainingBrokenLinks, List<BrokenContentLink> updatedBrokenLinks, PotentialBrokenContentLinkContent item, string fieldValue)
        {
            var matches = Regex.Matches(fieldValue ?? string.Empty, @"\[\[[a-zA-Z0-9\-_]+\]\]");
            if (matches.Count > 0)
            {
                var brokenLinks = CheckForBrokenSnippet(remainingBrokenLinks, item.FrontEndUrl, item.Id, item.Title, item.ItemType, item.RegionId, item.ContentTypeId);
                UpdateBrokenLinks(brokenLinks, updatedBrokenLinks);
            }
        }

        private BrokenContentLink[] CheckForBrokenSnippet(List<BrokenContentLink> remainingBrokenLinks, string frontEndUrl, string id, string title, string itemType, string regionId, string contentTypeId)
        {
            var brokenContents = new List<BrokenContentLink>();
            _snippets = _snippets ?? _brokenContentLinkRepository.FindSnippetFrontEndReferences();
            var snippets = _snippets.Where(s => s.ReferenceId.Equals(id) && !s.SnippetId.IsDraftContentItemId()).Select(s => new
            {
                SnippetIndex = s,
                SnippetStatus = _brokenContentLinkRepository.GetBrokenContentStatus(s.SnippetId, null)
            }).ToArray();
            var brokenSnippets = snippets.Where(s => s.SnippetIndex.ReferenceId.Equals(id) && !s.SnippetStatus.Equals(MarketingCenterDbConstants.ContentStatus.Published));

            foreach (var snippet in brokenSnippets)
            {
                brokenContents.Add(CreateBrokenSnippet(remainingBrokenLinks, frontEndUrl, id, title, itemType, regionId, contentTypeId, snippet.SnippetIndex, snippet.SnippetStatus));
            }

            return brokenContents.ToArray();
        }

        private BrokenContentLink CreateBrokenSnippet(List<BrokenContentLink> remainingBrokenLinks, string frontEndUrl, string id, string title, string itemType, string regionId, string contentTypeId, SnippetIndex snippetIndex, string snippetStatus)
        {
            BrokenContentLink brokenContent = null;
            brokenContent = remainingBrokenLinks.FirstOrDefault(l => l.FrontEndUrl == frontEndUrl);
            if (brokenContent != null)
            {
                remainingBrokenLinks.Remove(brokenContent);
            }
            else
            {
                brokenContent = new BrokenContentLink();
            }

            var isBrokenLinkInContent = contentTypeId != null;
            brokenContent.ContentType = itemType;
            brokenContent.LastRequested = DateTime.UtcNow;
            brokenContent.ReferrerUrl = null;
            brokenContent.BrokenLinkReference = snippetIndex.SnippetFrontendUrl == null || contentTypeId == MarketingCenterDbConstants.ContentTypeIds.MarqueeSlide ? null : GetBrokenLinkReference(snippetIndex.FrontEndUrl);
            brokenContent.BrokenLinkText = snippetIndex.SnippetTitle;
            brokenContent.BackEndUrl = EnvironmentConfig.Environment.AdminUrl + (isBrokenLinkInContent ? $"/Content/Edit?id={id}" : $"/List/Edit?listId={id}");
            brokenContent.FrontEndUrl = frontEndUrl == null ? null : $"{ EnvironmentConfig.Environment.FrontEndUrl}/{frontEndUrl.Substring(frontEndUrl.IndexOf("portal/") >= 0 ? frontEndUrl.IndexOf("portal/") + 7 : 0)}";
            brokenContent.RegionId = regionId;
            brokenContent.SourceItemId = id;
            brokenContent.Title = title;
            brokenContent.BrokenContentStatus = snippetStatus;

            return brokenContent;
        }

        private void CheckLinks(IList<string> excludeUrls, List<BrokenContentLink> remainingBrokenLinks, List<BrokenContentLink> updatedBrokenLinks, PotentialBrokenContentLinkContent item, string fieldValue)
        {
            var matches = Regex.Matches(fieldValue ?? string.Empty, @"<\s*a[^>]*href=""(?<href>[^""]+)""[^>]*>(?<text>[^<]*)<\s*/\s*a\s*>");
            foreach (Match match in matches)
            {
                var brokenLinks = ProcessLink(excludeUrls, remainingBrokenLinks, match.Groups["href"]?.Value?.Trim(), item.FrontEndUrl, item.Id, item.Title, item.ItemType, item.RegionId, match.Groups["text"]?.Value?.Trim(), item.ContentTypeId);
                UpdateBrokenLinks(brokenLinks, updatedBrokenLinks);
            }
        }

        private BrokenContentLink[] ProcessLink(IList<string> excludeUrls, List<BrokenContentLink> remainingBrokenLinks, string linkReference, string referrerUrl, string itemId, string title, string itemType, string region, string linkText, string contentTypeId)
        {
            var links = new List<BrokenContentLink>();

            if (IsPotentialBrokenLink(excludeUrls, linkReference))
            {
                var allFrontEndReferences = GetFrontEndReference(itemId, contentTypeId).Where(frontendReference => frontendReference == null || frontendReference.Item1.Equals(MarketingCenterDbConstants.ContentStatus.PublishedId)).Select(t => t?.Item2).ToArray();
                foreach (var frontEndUrl in allFrontEndReferences)
                {
                    links.Add(CreateBrokenLink(remainingBrokenLinks, linkReference, referrerUrl, frontEndUrl, contentTypeId, itemType, linkText, region, title, itemId));
                }
            }

            return links.ToArray();
        }

        private BrokenContentLink CreateBrokenLink(List<BrokenContentLink> remainingBrokenLinks, string linkReference, string referrerUrl, string frontEndUrl, string contentTypeId, string itemType, string linkText, string region, string title, string itemId)
        {
            BrokenContentLink link = null;
            link = remainingBrokenLinks.FirstOrDefault(l => l.BrokenLinkReference == linkReference && l.ReferrerUrl == referrerUrl && l.FrontEndUrl == frontEndUrl);
            if (link != null)
            {
                remainingBrokenLinks.Remove(link);
            }
            else
            {
                link = new BrokenContentLink();
            }

            var isBrokenLinkInContent = contentTypeId != null;
            var isBrokenLinkSearchTerm = itemType.Equals(MarketingCenterDbConstants.BrokenLinkSearchTerms.ContentType);
            link.ContentType = itemType;
            link.LastRequested = DateTime.UtcNow;
            link.ReferrerUrl = referrerUrl == null ? null : $"{ EnvironmentConfig.Environment.FrontEndUrl}/{referrerUrl.Substring(referrerUrl.IndexOf("portal/") >= 0 ? referrerUrl.IndexOf("portal/") + 7 : 0)}";
            link.BrokenLinkReference = linkReference == null ? null : GetBrokenLinkReference(linkReference);
            link.BrokenLinkText = linkText;
            link.BackEndUrl = EnvironmentConfig.Environment.AdminUrl + (isBrokenLinkInContent ? $"/Content/Edit?id={itemId}" : isBrokenLinkSearchTerm ? $"/List/Edit?listId={MarketingCenterDbConstants.MarketingCalendarListIds.SearchTerm}&id={itemId}" : $"/List/Edit?listId={itemId}");
            link.FrontEndUrl = frontEndUrl == null ? null : $"{ EnvironmentConfig.Environment.FrontEndUrl}/{frontEndUrl.Substring(frontEndUrl.IndexOf("portal/") >= 0 ? frontEndUrl.IndexOf("portal/") + 7 : 0)}";
            link.RegionId = region;
            link.SourceItemId = itemId;
            link.Title = title;
            link.BrokenContentStatus = GetBrokenContentStatusFromLinkReference(linkReference);

            return link;
        }

        private string GetBrokenLinkReference(string linkReference)
        {
            return linkReference.IsAbsoluteUrl() || linkReference.StartsWith("http:") || linkReference.StartsWith("https:") ? linkReference :
                   $"{EnvironmentConfig.Environment.FrontEndUrl}/{linkReference.Substring(linkReference.IndexOf("portal/") >= 0 ? linkReference.IndexOf("portal/") + 7 : 0)}";
        }

        private bool IsPotentialBrokenLink(IList<string> excludeUrls, string linkReference)
        {
            var isAnchorLink = linkReference.IsAnchorLink();
            var isEmailLink = linkReference.IsEmailLink();
            var isInlineJavascript = linkReference.IsInlineJavascript();
            var isAbsoluteUri = CreatePotentialBrokenLink(linkReference)?.IsAbsoluteUri ?? false;
            var isHostnameUri = Uri.CheckHostName(linkReference.Split('/')[0]) == UriHostNameType.Unknown; //if url begins with just a hostname, skip it
            var isInExcludedUrls = excludeUrls.Contains(linkReference);
            var isPhysicalFile = _settingsService.GetPhysicalFileUrls().Any(s => linkReference.ToLower().Contains(s));
            var isExistingPhysicalFile = File.Exists(AppDomain.CurrentDomain.BaseDirectory + linkReference.Replace("/portal", string.Empty).TrimStart('/').Replace("/", "\\"));
            var isExistingContentItem = _brokenContentLinkRepository.VerifyContentItemExistsByFrontEndUrl(linkReference.Replace("/portal", string.Empty));

            return !isAnchorLink && !isEmailLink && !isInlineJavascript && !isAbsoluteUri && isHostnameUri && !isInExcludedUrls && (!isPhysicalFile || (isPhysicalFile && !isExistingPhysicalFile)) && !isExistingContentItem;
        }

        private void UpdateBrokenLinks(BrokenContentLink[] brokenLinks, List<BrokenContentLink> updatedBrokenLinks)
        {
            foreach (var brokenLink in brokenLinks)
            {
                if (brokenLink != null && !updatedBrokenLinks.Any(l => l.SourceItemId.Equals(brokenLink.SourceItemId) && l.ReferrerUrl == brokenLink.ReferrerUrl && l.BrokenLinkReference == brokenLink.BrokenLinkReference))
                {
                    updatedBrokenLinks.Add(brokenLink);
                }
            }
        }

        private Uri CreatePotentialBrokenLink(string linkReference)
        {
            Uri.TryCreate(linkReference, UriKind.RelativeOrAbsolute, out Uri uri);
            return uri;
        }

        private string GetBrokenContentStatusFromLinkReference(string link)
        {
            var relativeUrl = link.Substring(link.IndexOf("portal/") >= 0 ? link.IndexOf("portal/") + 7 : 0);

            if (relativeUrl.Contains("page/") || relativeUrl.Contains("inc/"))
            {
                return _brokenContentLinkRepository.GetBrokenContentStatus(null, relativeUrl);
            }
            else
            {
                var id = Regex.Match(relativeUrl, @"\/(.*?)\/").Value.Replace("/", string.Empty);
                return _brokenContentLinkRepository.GetBrokenContentStatus(id, null);
            }
        }

        private Tuple<int, string>[] GetFrontEndReference(string itemId, string contentTypeId)
        {
            var currentTypeName = GetContentTypeName(contentTypeId);
            _snippets = _snippets ?? _brokenContentLinkRepository.FindSnippetFrontEndReferences();
            switch (contentTypeId)
            {
                case MarketingCenterDbConstants.ContentTypeIds.Snippet:
                    var all = _snippets.Where(s => s.SnippetId == itemId);
                    var refs = all.Where(a => a.ContentTypeId != MarketingCenterDbConstants.ContentTypeIds.Snippet).SelectMany(ci => GetFrontEndReference(ci.ReferenceId, ci.ContentTypeId), (SnippetIndex parent, Tuple<int, string> child) => new Tuple<int, string>(parent.StatusId, child.Item2)).ToArray();
                    return refs.Count() > 0 ? refs : new Tuple<int, string>[] { null };
                case MarketingCenterDbConstants.ContentTypeIds.Page:
                    return new Tuple<int, string>[] { new Tuple<int, string>(_contentItemRepository.GetById(itemId).StatusID, _contentItemRepository.GetById(itemId).FrontEndUrl) };
                case MarketingCenterDbConstants.ContentTypeIds.MarqueeSlide:
                    return GetMarqueeFrontEndItemurls(itemId);
                default:
                    return currentTypeName != null ? new Tuple<int, string>[] { new Tuple<int, string>(MarketingCenterDbConstants.ContentStatus.PublishedId, $"{EnvironmentConfig.Environment.FrontEndUrl}/{currentTypeName}/{itemId}") } : new Tuple<int, string>[] { null };
            }
        }

        public Tuple<int, string>[] GetMarqueeFrontEndItemurls(string itemId)
        {
            var urls = new List<string>();
            var assignedLocations = _brokenContentLinkRepository.GetContentFeaturedLocations(itemId);
            foreach (var location in assignedLocations)
            {
                var node = _sitemap.FindNode(_sitemap.RootNode, n => n.Location.Equals(location, StringComparison.InvariantCultureIgnoreCase));
                var maxRuns = 9999;
                while (node != null && maxRuns > 0)
                {
                    maxRuns = maxRuns - 1;
                    node = _sitemap.FindNode(_sitemap.RootNode, n => n.Location.Equals(location, StringComparison.InvariantCultureIgnoreCase) && !n.Url.Equals(node.Url));
                    if (node.Url != null && !urls.Contains(node.Url))
                    {
                        urls.Add(node.Url);
                    }
                }
            }

            return urls.Select(u => new Tuple<int, string>(MarketingCenterDbConstants.ContentStatus.PublishedId, $"{EnvironmentConfig.Environment.FrontEndUrl}/{u}")).ToArray();
        }

        private string GetContentTypeName(string contentTypeId)
        {
            switch (contentTypeId)
            {
                case "a":
                case "ac":
                case "b":
                case "c":
                case "d":
                    return "Asset";
                case "1":
                    return "Page";
                case "e":
                    return "Program";
                case "f":
                    return "Webinar";
                default:
                    return null;
            }
        }
    }
}