﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using Mastercard.MarketingCenter.Services.Data;

namespace Mastercard.MarketingCenter.Services
{
	public static class ToolkitServices
	{
		public static string GetInsertsBundleToolkitPageUrl()
		{
			if (HttpContext.Current.Cache["InsertsBundleToolkitPageUrl"] == null) {
				MasterCardPortalDataContext dc = new MasterCardPortalDataContext();
				Page page = dc.Pages.Where(p => p.Title.Trim().Equals("Inserts Bundle Program")).FirstOrDefault();
				if (page != null)
					HttpContext.Current.Cache["InsertsBundleToolkitPageUrl"] = "/portal/Page/" + page.ContentItemId;
				else 
					return null;
			}
			return HttpContext.Current.Cache["InsertsBundleToolkitPageUrl"].ToString();
		}
	}
}
