﻿using AutoMapper;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.DTO.Salesforce;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Strategies.Salesforce;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services.Services
{
    public class SalesforceApiService : ISalesforceApiService
    {
        private readonly ISalesforceService _salesforceService;
        private readonly IMapper _mapper;
        private readonly int _rangeCount = 100;

        public SalesforceApiService(ISalesforceService salesforceService, IMapper mapper)
        {
            _salesforceService = salesforceService;
            _mapper = mapper;
        }

        public IEnumerable<SalesforceContactQueryResponseDTO> GetSalesforceContacts(IEnumerable<string> emails, bool registerSalesforceAccessToken = false)
        {
            if (emails != null && emails.Any())
            {
                var salesforceContacts = new List<SalesforceContactQueryResponseDTO>();
                var emailsList = emails.Distinct().Select(e => e.Replace("'", @"\'")).ToList();
                for (int i = 0; i < emailsList.Count; i += _rangeCount)
                {
                    var salesforceObject = new SalesforceQueryObject(SalesforceApiRequestExtension.GetContactQuery(emailsList.GetRange(i, Math.Min(_rangeCount, emailsList.Count - i))));
                    if (registerSalesforceAccessToken)
                    {
                        salesforceObject.Execute(_salesforceService.RegisterSalesforceAccessToken);
                    }
                    else
                    {
                        salesforceObject.Execute();
                    }

                    salesforceContacts.AddRange(JsonConvert.DeserializeObject<List<SalesforceContactQueryResponseDTO>>(salesforceObject.Result));
                }

                return salesforceContacts.GroupBy(r => r.Email).Where(c => c != null).Select(c => c.First());
            }

            return null;
        }

        public SalesforceTaskResponseDTO SendSalesforceTask(SalesforceTask salesforceTask, bool registerSalesforceAccessToken = false)
        {
            var salesforceObject = new SalesforceTaskObject(_mapper.Map<SalesforceTaskDTO>(salesforceTask));
            if (registerSalesforceAccessToken)
            {
                salesforceObject.Execute(_salesforceService.RegisterSalesforceAccessToken);
            }
            else
            {
                salesforceObject.Execute();
            }

            return JsonConvert.DeserializeObject<SalesforceTaskResponseDTO>(salesforceObject.Result);
        }

        public IEnumerable<SalesforceTaskQueryResponseDTO> GetSalesforceTasks(IEnumerable<string> taskIds)
        {
            if (taskIds != null && taskIds.Any())
            {
                var salesforceTasks = new List<SalesforceTaskQueryResponseDTO>();
                var taskIdsList = taskIds.Distinct().ToList();
                for (int i = 0; i < taskIdsList.Count; i += _rangeCount)
                {
                    var salesforceObject = new SalesforceQueryObject(SalesforceApiRequestExtension.GetTaskQuery(taskIdsList.GetRange(i, Math.Min(_rangeCount, taskIdsList.Count - i))));
                    salesforceObject.Execute();
                    salesforceTasks.AddRange(JsonConvert.DeserializeObject<List<SalesforceTaskQueryResponseDTO>>(salesforceObject.Result));
                }

                return salesforceTasks;
            }

            return null;
        }
    }
}