﻿using AutoMapper;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Models;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class SegmentationService : ISegmentationService
    {
        private readonly IMapper _mapper;
        private readonly ISegmentationRepository _segmentationRepository;

        public SegmentationService(IMapper mapper, ISegmentationRepository segmentationRepository)
        {
            _mapper = mapper;
            _segmentationRepository = segmentationRepository;
        }

        public void InsertIssuerSegmentations(string issuerId, IEnumerable<string> segmentationIds)
        {
            foreach (var segmentationId in segmentationIds)
            {
                _segmentationRepository.Insert(new IssuerSegmentation
                {
                    IssuerId = issuerId,
                    SegmentationId = segmentationId
                });
            }
        }

        public IEnumerable<Segmentation> GetSegmentationsByRegion(string regionId)
        {
            return _segmentationRepository.GetByRegion(regionId).OrderBy(s => s.SegmentationName);
        }

        public IEnumerable<Segmentation> GetSegmentationsByUserName(string userName)
        {
            return _segmentationRepository.GetByUserName(userName)?.OrderBy(s => s.SegmentationName);
        }

        public IEnumerable<Segmentation> GetSegmentationsByIds(IEnumerable<string> segmentationIds)
        {
            return segmentationIds != null && segmentationIds.Any() ? _segmentationRepository.GetAll().Where(s => segmentationIds.Contains(s.SegmentationId)) : null;
        }

        public IEnumerable<Segmentation> GetSegmentationsByEmail(string email)
        {
            return _segmentationRepository.GetByEmail(email)?.OrderBy(s => s.SegmentationName);
        }

        public IEnumerable<SegmentationModel> GetTranslatedSegmentations(string regionId, string language)
        {
            return _mapper.Map<IEnumerable<SegmentationModel>>(_segmentationRepository.GetByRegion(regionId), opts => opts.Items["Language"] = language)
                          .OrderBy(s => s.SegmentationName);
        }
    }
}