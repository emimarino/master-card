﻿namespace Mastercard.MarketingCenter.Services.Services
{
    public class RssFeedItem
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string Link { get; set; }
        public string Id { get; set; }
    }
}