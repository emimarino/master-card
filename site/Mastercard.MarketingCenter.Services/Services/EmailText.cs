﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Mastercard.MarketingCenter.Services
{
    public class EmailText
    {
        private readonly DataTable _dt = new DataTable();
        private int _rowId;
        private int _numToStart;
        private int _remainingWordCount;
        private readonly StringBuilder _sbResult = new StringBuilder();

        public void SetDataTable(string word, int width, int columnCounter, int columns, string align)
        {
            bool needToAddRow = false;
            int lastWordBreak;
            if (word.Length - _numToStart >= width)
            {
                _remainingWordCount = width;
                CheckAddRow(columns);
                needToAddRow = true;
                lastWordBreak = word.Substring(_numToStart, _remainingWordCount).LastIndexOf(" ", StringComparison.Ordinal);
            }
            else
            {
                CheckAddRow(columns);
                _remainingWordCount = word.Length - _numToStart;
                lastWordBreak = word.Substring(_numToStart, _remainingWordCount).Length;
            }
            StringBuilder sb = new StringBuilder();

            string newWord;
            if (lastWordBreak > 0)
            {
                newWord = word.Substring(_numToStart, lastWordBreak);
            }
            else
            {
                newWord = word.Substring(_numToStart, _remainingWordCount); // for count
            }

            string sbString = newWord;
            string trimedSbString = newWord.Trim();
            if (align == "Left")
                sb.Append(trimedSbString); // for saving to table
            for (int y = trimedSbString.Length; y <= width; y++)
            {
                sb.Append(@" ");
            }

            if (align == "Right")
            {
                sb.Append(trimedSbString);
            }

            _dt.Rows[_rowId][columnCounter] = sb.ToString();
            _numToStart = _numToStart + sbString.Length;

            if (needToAddRow)
            {
                _rowId += 1;
                SetDataTable(word, width, columnCounter, columns, align);
            }
            else
            {
                _rowId = 0;
                _numToStart = 0;
            }
        }

        public string GenerateText(List<TableItem> originalData)
        {
            for (int i = 0; i < originalData.Count; i++)
            {
                DataColumn dc = new DataColumn("text" + i, Type.GetType("System.String"));
                _dt.Columns.Add(dc);
            }

            for (int i = 0; i < originalData.Count; i++)
            {
                string word = originalData[i].Head;
                SetDataTable(word, originalData[i].Width, i, originalData.Count, originalData[i].Align);
            }

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < _dt.Rows.Count; i++)
            {
                for (int y = 0; y < _dt.Columns.Count; y++)
                {
                    if (_dt.Rows[i][y].ToString() == "")
                    {
                        int width = originalData[y].Width;
                        StringBuilder tempSpace = new StringBuilder();
                        for (int z = 0; z <= width; z++)
                        {
                            tempSpace.Append(@" ");

                        }
                        _dt.Rows[i][y] = tempSpace.ToString();
                    }
                    sb.Append(_dt.Rows[i][y]);
                }
                WriteLine(sb.ToString(), true);
                sb.Remove(0, sb.Length);
            }

            return _sbResult.ToString();
        }

        private void CheckAddRow(int columns)
        {
            if (_dt.Rows.Count <= _rowId)
            {
                DataRow dr = _dt.NewRow();
                for (int i = 0; i < columns; i++)
                {
                    dr[i] = "";
                }
                _dt.Rows.Add(dr);
            }
        }

        private void WriteLine(string txtString, bool newline)
        {
            _sbResult.Append(txtString);
            if (newline)
            {
                _sbResult.Append(Environment.NewLine);
            }
        }
    }

    public struct TableItem
    {
        public string Head { get; set; }

        public int Width { get; set; }

        public string Align { get; set; }
    }
}