﻿using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;

namespace Mastercard.MarketingCenter.Services
{
    public class OrderService : IOrderService
    {
        private MasterCardPortalDataContext _context;
        private readonly UserContext _userContext;
        private readonly IIssuerService _issuerService;

        public OrderService(MasterCardPortalDataContext context, UserContext userContext, IIssuerService issuerService)
        {
            _context = context;
            _userContext = userContext;
            _issuerService = issuerService;
        }

        public int? CreateOrder(int shoppingCartID)
        {
            string userId = string.Empty;
            string issuerId = _userContext.User.IssuerId;

            int? orderId = IsOrderAlreadyExists(shoppingCartID.ToString());
            if (orderId == null)
            {
                ShoppingCart cart = _context.ShoppingCarts.SingleOrDefault(c => c.CartID.Equals(shoppingCartID));
                if (cart != null)
                {
                    string status = "Order Created";
                    List<RetrieveCustomizationsResult> ls = ShoppingCartService.GetCustomizationOptions(_userContext.User.UserName, shoppingCartID, true);
                    if (ls.Count == 0)
                    {
                        status = "Fulfillment";
                    }

                    ShoppingCart cartOptions = ShippingServices.GetShoppingCartOptions(shoppingCartID);
                    Order order = new Order();
                    order.OrderNumber = shoppingCartID.ToString();
                    order.OrderDate = DateTime.Now;
                    order.OrderStatus = status;
                    order.SpecialInstructions = cartOptions.ShippingInstructions;
                    order.RushOrder = cartOptions.RushOrder.HasValue ? cartOptions.RushOrder.Value : false;
                    order.UserID = cart.UserID;
                    order.BillingID = cart.BillingID;

                    if (!string.IsNullOrEmpty(cart.PromotionID))
                    {
                        PromoCode promotion = _context.PromoCodes.SingleOrDefault(p => p.PromoCodeID == cart.PromotionID);
                        PromotionCodeResponse promotionResponse = PromotionService.ApplyPromotionCode(promotion.PromotionCode, shoppingCartID, issuerId);
                        if (string.IsNullOrEmpty(promotionResponse.ErrorMessage))
                        {
                            order.PromotionCode = promotion.PromotionCode;
                            order.PromotionDescription = promotionResponse.Description;
                            order.PromotionAmount = promotionResponse.DiscountAmount < promotionResponse.OrderTotal ? promotionResponse.DiscountAmount : promotionResponse.OrderTotal;

                            PromoCodeUsedByIssuer promotionsUsedByIssuer = _context.PromoCodeUsedByIssuers.FirstOrDefault(p => p.PromoCodeID == promotion.PromoCodeID && p.IssuerID == issuerId);
                            if (promotionsUsedByIssuer == null)
                            {
                                promotionsUsedByIssuer = new PromoCodeUsedByIssuer();
                                promotionsUsedByIssuer.PromoCodeID = cart.PromotionID;
                                promotionsUsedByIssuer.IssuerID = issuerId;
                                promotionsUsedByIssuer.NumPromotionsUsed = 1;
                                _context.PromoCodeUsedByIssuers.InsertOnSubmit(promotionsUsedByIssuer);
                            }
                            else
                            {
                                promotionsUsedByIssuer.NumPromotionsUsed++;
                            }

                            promotion.TotalFundingCurrentAmount += promotionResponse.DiscountAmount;
                        }
                    }

                    _context.Orders.InsertOnSubmit(order);
                    _context.SubmitChanges();
                    orderId = order.OrderID;
                    userId = cart.UserID;

                    Dictionary<string, OrderItem> itemTotals = new Dictionary<string, OrderItem>();

                    List<RetrieveShoppingCartByUserIdResult> uniqueItems = ShoppingCartService.GetShoppingCartByUserId(order.UserID);
                    var cartItems = ShoppingCartService.GetShoppingCartItems(shoppingCartID);
                    List<string> assetsWithMinimums = new List<string>();
                    foreach (RetrieveShoppingCartByUserIdResult item in uniqueItems)
                    {
                        if (!item.ElectronicDelivery)
                        {
                            OrderItem newUniqueItem = new OrderItem();
                            newUniqueItem.ItemQuantity = (int)item.Quantity;
                            newUniqueItem.ItemPrice = ShoppingCartService.GetAssetPrice(item.ContentItemID, (int)item.Quantity, item.ElectronicDelivery).ItemPrice;

                            itemTotals.Add(item.ContentItemID, newUniqueItem);
                        }
                    }

                    List<RetrieveAllShoppingCartItemsByUserIdResult> lsCartItem = ShoppingCartService.GetAllShoppingCartItemsByUserId(order.UserID);

                    bool electronicDeliveryOnly = true;
                    bool hasElectronicDeliveryItems = false;
                    bool hasVdpItems = false;
                    bool pendingApproval = false;

                    Dictionary<int, int?> subOrders = new Dictionary<int, int?>();
                    foreach (RetrieveAllShoppingCartItemsByUserIdResult c in lsCartItem)
                    {
                        OrderItem oitem = new OrderItem();

                        int? subOrderId = null;
                        if (c.ShippingInformationID != null)
                        {
                            if (subOrders.ContainsKey(c.ShippingInformationID.Value))
                            {
                                subOrderId = subOrders[c.ShippingInformationID.Value];
                            }
                            else
                            {
                                SubOrder subOrder = new SubOrder();
                                subOrder.Completed = false;
                                subOrder.Description = "";
                                subOrder.OrderID = (int)orderId;
                                subOrder.ShippingInformationID = c.ShippingInformationID.Value;
                                _context.SubOrders.InsertOnSubmit(subOrder);
                                _context.SubmitChanges();

                                subOrderId = subOrder.SubOrderID;

                                subOrders.Add(c.ShippingInformationID.Value, subOrderId);
                            }
                        }

                        oitem.ElectronicDelivery = c.ElectronicDelivery;
                        oitem.VDP = c.VDP.Value;

                        oitem.ItemID = c.ContentItemID;
                        oitem.ItemName = c.Title;
                        oitem.SKU = c.SKU;
                        oitem.OrderID = (int)orderId;
                        oitem.ShippingInformationID = c.ShippingInformationID;
                        oitem.ItemQuantity = (int)c.Quantity;
                        oitem.Modified = DateTime.Now;
                        oitem.SubOrderID = subOrderId;
                        oitem.ApproverEmailAddress = c.ApproverEmailAddress;
                        if (!pendingApproval)
                        {
                            pendingApproval = !string.IsNullOrEmpty(oitem.ApproverEmailAddress);
                        }

                        if (c.VDP.HasValue && c.VDP.Value)
                        {
                            hasVdpItems = true;
                        }
                        if (c.ElectronicDelivery)
                        {
                            hasElectronicDeliveryItems = true;
                            oitem.Customizations = false;

                            oitem.ItemPrice = ShoppingCartService.GetAssetPrice(c.ContentItemID, c.Quantity.Value, true).TotalPrice;
                            _context.OrderItems.InsertOnSubmit(oitem);
                            _context.SubmitChanges();

                            var electronicDeliveryID = Guid.NewGuid();
                            var electronicDelivery = new ElectronicDelivery
                            {
                                ElectronicDeliveryID = electronicDeliveryID,
                                OrderItemID = oitem.OrderItemID,
                                FileLocation = c.ElectronicDeliveryFile,
                                DownloadCount = Convert.ToInt32(WebConfigurationManager.AppSettings["ElectronicDelivery.MaxDownload"]),
                                ExpirationDate = DateTime.Now.AddDays(Convert.ToInt32(WebConfigurationManager.AppSettings["ElectronicDelivery.ExpirationDays"]))
                            };

                            _context.ElectronicDeliveries.InsertOnSubmit(electronicDelivery);
                            _context.ElectronicDeliveryCreateDatas.InsertOnSubmit(
                                new ElectronicDeliveryCreateData
                                {
                                    ElectronicDeliveryID = electronicDeliveryID,
                                    ContentItemId = c.ContentItemID,
                                    UserName = _userContext.User.UserName,
                                    Region = _userContext.SelectedRegion
                                }
                            );

                            // Estimated Distribution
                            oitem.EstimatedDistribution = cartItems.Where(ci => ci.ContentItemID == c.ContentItemID).FirstOrDefault().EstimatedDistribution;
                        }
                        else
                        {
                            oitem.Customizations = c.Customizable != null && c.Customizable.Value;

                            electronicDeliveryOnly = false;

                            if (itemTotals[c.ContentItemID].ItemPrice > 0)
                            {
                                oitem.ItemPrice = itemTotals[c.ContentItemID].ItemPrice * c.Quantity.Value;
                            }
                            else if (!assetsWithMinimums.Contains(c.ContentItemID))
                            {
                                oitem.ItemPrice = ShoppingCartService.GetAssetPrice(c.ContentItemID, itemTotals[c.ContentItemID].ItemQuantity.Value, false).TotalPrice;
                                assetsWithMinimums.Add(c.ContentItemID);
                            }
                            else
                            {
                                oitem.ItemPrice = 0;
                            }

                            _context.OrderItems.InsertOnSubmit(oitem);
                        }
                    }

                    _context.SubmitChanges();
                    _context.UpdateCustomizationAndShippingInfomation(shoppingCartID, orderId);

                    if (hasVdpItems)
                    {
                        SubOrder subOrder = new SubOrder();
                        subOrder.Completed = false;
                        subOrder.Description = "";
                        subOrder.OrderID = (int)orderId;
                        subOrder.ShippingInformationID = 0;
                        _context.SubOrders.InsertOnSubmit(subOrder);
                        _context.SubmitChanges();

                        var orderItemsForUpdate = (from o in _context.OrderItems
                                                   where o.OrderID.Equals(orderId)
                                                   select o);

                        foreach (OrderItem item in orderItemsForUpdate)
                        {
                            if (item.VDP)
                            {
                                item.SubOrderID = subOrder.SubOrderID;
                            }
                        }

                        _context.SubmitChanges();
                    }

                    if (hasElectronicDeliveryItems)
                    {
                        SubOrder subOrder = new SubOrder();
                        subOrder.Completed = true;
                        subOrder.Description = "";
                        subOrder.OrderID = (int)orderId;
                        subOrder.DateCompleted = DateTime.Now;
                        subOrder.ShippingInformationID = 0;
                        _context.SubOrders.InsertOnSubmit(subOrder);
                        _context.SubmitChanges();

                        var orderItemsForUpdate = (from o in _context.OrderItems
                                                   where o.OrderID.Equals(orderId)
                                                   select o);

                        foreach (OrderItem item in orderItemsForUpdate)
                        {
                            if (item.ElectronicDelivery)
                            {
                                item.SubOrderID = subOrder.SubOrderID;
                            }
                        }

                        _context.SubmitChanges();
                    }

                    if (electronicDeliveryOnly)
                    {
                        var orderForUpdate = (from o in _context.Orders
                                              where o.OrderID.Equals(orderId)
                                              select o).SingleOrDefault();
                        if (orderForUpdate != null)
                        {
                            orderForUpdate.DateCompleted = DateTime.Now;
                            orderForUpdate.OrderStatus = "Shipped";
                            _context.SubmitChanges();
                        }
                    }
                }
            }
            if (!string.IsNullOrWhiteSpace(issuerId))
            {
                _issuerService.SetIssuerHasOrdered(issuerId);
            }

            return orderId;
        }

        public void JoinShipmentSubOrdersInOrder(int orderId, int shippingInformationId)
        {
            var result = from subOrder in _context.SubOrders
                         where subOrder.OrderID.Equals(orderId) && subOrder.ShippingInformationID.Equals(shippingInformationId)
                         orderby subOrder.SubOrderID
                         select subOrder;

            if (result.Count() > 0)
            {
                int joinedSubOrderId = 0;
                foreach (var subOrder in result)
                {
                    if (joinedSubOrderId == 0)
                    {
                        joinedSubOrderId = subOrder.SubOrderID;
                    }
                    else
                    {
                        IEnumerable<OrderItem> subOrderItems = (from item in _context.OrderItems
                                                                where item.SubOrderID.Equals(subOrder.SubOrderID)
                                                                select item);

                        foreach (OrderItem subOrderItem in subOrderItems)
                        {
                            subOrderItem.SubOrderID = joinedSubOrderId;
                        }
                        _context.SubOrders.DeleteOnSubmit(subOrder);
                    }
                }
                _context.SubmitChanges();
            }
        }

        public void SplitSubOrder(int subOrderId, string splitOrderItemIds)
        {
            string[] orderItemIds = splitOrderItemIds.Split(',');

            var result = from subOrder in _context.SubOrders
                         where subOrder.SubOrderID.Equals(subOrderId)
                         select subOrder;

            if (result.Any())
            {
                SubOrder subOrder = result.FirstOrDefault();
                SubOrder newSubOrder = new SubOrder();
                newSubOrder.Completed = false;
                newSubOrder.Description = "";
                newSubOrder.OrderID = subOrder.OrderID;
                newSubOrder.ShippingInformationID = subOrder.ShippingInformationID;

                _context.SubOrders.InsertOnSubmit(newSubOrder);
                _context.SubmitChanges();

                IEnumerable<OrderItem> subOrderItems = (from item in _context.OrderItems
                                                        where orderItemIds.Contains(item.OrderItemID.ToString())
                                                        select item);

                foreach (OrderItem subOrderItem in subOrderItems)
                {
                    subOrderItem.SubOrderID = newSubOrder.SubOrderID;
                }

                _context.SubmitChanges();
            }
        }

        public int? IsOrderAlreadyExists(string OrderNo)
        {
            int? returnorderid = null;
            var q = from a in _context.Orders
                    where a.OrderNumber.Equals(OrderNo)
                    select a;
            if (q.Count() > 0)
                returnorderid = q.FirstOrDefault().OrderID;
            return returnorderid;
        }

        public void UpdateItem(int OrderID, string ItemID, string ProofPDF)
        {
            var q = from a in _context.OrderItems
                    where a.ItemID.Equals(ItemID) && a.OrderID.Equals(OrderID)
                    select a;

            foreach (OrderItem item in q)
            {
                item.ProofPDF = ProofPDF;
                item.Modified = DateTime.Now;
            }
            _context.SubmitChanges();
        }

        public void UpdateOrderTrackingNumber(int shippingInformationId, string trackingNumber)
        {
            var q = from shippingInfo in _context.ShippingInformations
                    where shippingInfo.ShippingInformationID.Equals(shippingInformationId)
                    select shippingInfo;

            ShippingInformation shippingInformation = new ShippingInformation();
            if (q.Count() > 0)
            {
                shippingInformation = q.FirstOrDefault();
                shippingInformation.OrderTrackingNumber = trackingNumber;
            }
            _context.SubmitChanges();
        }

        public void UpdateSubOrderTrackingNumber(int subOrderId, string trackingNumber)
        {
            var q = from subOrder in _context.SubOrders
                    where subOrder.SubOrderID.Equals(subOrderId)
                    select subOrder;

            SubOrder order = new SubOrder();
            if (q.Count() > 0)
            {
                order = q.FirstOrDefault();
                order.TrackingNumber = trackingNumber;
            }
            _context.SubmitChanges();
        }

        public void UpdatePriceAdjustment(int OrderID, decimal? PriceAdjustment, decimal? PostageCost, decimal? ShippingCost, string Description)
        {
            var q = from a in _context.Orders
                    where a.OrderID.Equals(OrderID)
                    select a;

            if (q.Count() > 0)
            {
                Order item = q.FirstOrDefault();

                item.PostageCost = PostageCost;
                item.PriceAdjustment = PriceAdjustment;
                item.ShippingCost = ShippingCost;
                item.Description = Description;
                _context.SubmitChanges();
            }
        }

        public void UpdateSubOrderPriceAdjustment(int subOrderId, decimal? PriceAdjustment, decimal? PostageCost, decimal? ShippingCost, string Description, bool completesOrder)
        {
            var q = from a in _context.SubOrders
                    where a.SubOrderID.Equals(subOrderId)
                    select a;

            if (q.Count() > 0)
            {
                SubOrder item = q.FirstOrDefault();

                item.PostageCost = PostageCost;
                item.PriceAdjustment = PriceAdjustment;
                item.ShippingCost = ShippingCost;
                item.Description = Description;
                item.Completed = true;
                item.DateCompleted = DateTime.Now;
                if (completesOrder)
                {
                    item.Order.DateCompleted = DateTime.Now;
                }
                _context.SubmitChanges();
            }
        }

        public string GetPrinterEmail(int OrderID, string status)
        {
            string sreturn = string.Empty;
            var q = from a in _context.OrderProcessNotes
                    where a.OrderID.Equals(OrderID)
                    orderby a.ID descending
                    select a;

            if (q.Count() > 0)
            {
                OrderProcessNote item = q.FirstOrDefault();
                sreturn = item.Approval;
            }
            return sreturn;
        }

        public List<RetrieveCustomizationsResult> GetCustomizationOptions(int Orderid)
        {
            return new List<RetrieveCustomizationsResult>(_context.RetrieveCustomizations(null, null, Orderid, false));
        }

        public List<RetrieveShippingInformationResult> GetShippingInformation(int Orderid)
        {
            return new List<RetrieveShippingInformationResult>(_context.RetrieveShippingInformation(null, Orderid));
        }

        public List<RetrieveOrderHistoryResult> GetOrderHistory(string UserID)
        {
            return new List<RetrieveOrderHistoryResult>(_context.RetrieveOrderHistory(UserID));
        }

        public List<RetrieveAllOrderItemsResult> GetAllOrderItems(int Orderid)
        {
            return new List<RetrieveAllOrderItemsResult>(_context.RetrieveAllOrderItems(Orderid.ToString()));
        }

        public List<RetrieveOrderItemsResult> GetOrderItems(int Orderid)
        {
            return new List<RetrieveOrderItemsResult>(_context.RetrieveOrderItems(Orderid.ToString()));
        }

        public RetrieveOrderDetailResult GetOrderDetails(int Orderid)
        {
            return _context.RetrieveOrderDetail(Orderid.ToString()).FirstOrDefault();
        }

        public RetrieveSubOrderDetailResult GetSubOrderDetail(int subOrderId)
        {
            return _context.RetrieveSubOrderDetail(subOrderId).FirstOrDefault();
        }

        public List<RetrieveOrderLogResult> GetOrderLog(int Orderid)
        {
            return new List<RetrieveOrderLogResult>(_context.RetrieveOrderLog(Orderid));
        }

        public List<RetrieveOrderItemsForShippingInformationResult> GetOrderItemsForShippingInformation(int shippingInformationId)
        {
            return new List<RetrieveOrderItemsForShippingInformationResult>(_context.RetrieveOrderItemsForShippingInformation(shippingInformationId));
        }

        public List<RetrieveOrderSubOrdersResult> GetSubOrders(int orderId, int shippingInformationId)
        {
            return new List<RetrieveOrderSubOrdersResult>(_context.RetrieveOrderSubOrders(orderId, shippingInformationId));
        }

        public List<RetrieveSubOrderItemsResult> GetSubOrderItems(int subOrderId)
        {
            return new List<RetrieveSubOrderItemsResult>(_context.RetrieveSubOrderItems(subOrderId));
        }

        public void SetOrderStatus(int OrderID, string status, string FeedBack, string clientIP, string UserName)
        {
            var od = from a in _context.Orders
                     where a.OrderID.Equals(OrderID)
                     select a;

            Order order = od.FirstOrDefault();

            OrderProcessNote OrderProcess = new OrderProcessNote();
            OrderProcess.Approval = UserName;
            OrderProcess.Date = DateTime.Now.Date;
            OrderProcess.Note = FeedBack;
            OrderProcess.OrderID = OrderID;
            OrderProcess.IP = clientIP;
            _context.OrderProcessNotes.InsertOnSubmit(OrderProcess);

            order.OrderStatus = status;
            _context.SubmitChanges();
        }

        public void SetOrderStatus(int OrderID, string status)
        {
            var od = from a in _context.Orders
                     where a.OrderID.Equals(OrderID)
                     select a;

            Order order = od.FirstOrDefault();
            order.OrderStatus = status;
            _context.SubmitChanges();
        }

        public List<RetrieveOrdersByStatusResult> GetOrderByStatus(string Status)
        {
            return new List<RetrieveOrdersByStatusResult>(_context.RetrieveOrdersByStatus(Status));
        }
        public List<GetOrderMonthListResult> GetOrderMonthList()
        {
            return new List<GetOrderMonthListResult>(_context.GetOrderMonthList());
        }

        public List<OrderDetailsReportResult> GetOrderDetailedReport(DateTime OrderDate)
        {
            return new List<OrderDetailsReportResult>(_context.OrderDetailsReport(OrderDate));
        }

        public List<OrderDetailRangeReportResult> GetOrderDetailRangeReport(DateTime startDate, DateTime endDate)
        {
            return new List<OrderDetailRangeReportResult>(_context.OrderDetailRangeReport(startDate, endDate));
        }

        public int GetOrderIDByOrderNo(string OrderNo)
        {
            var od = from a in _context.Orders
                     where a.OrderNumber.Equals(OrderNo)
                     select a;


            Order order = od.FirstOrDefault();
            if (order != null)
                return Convert.ToInt32(order.OrderID);
            else
                return 0;
        }

        public string GetOrderNumberByOrderId(int OrderId)
        {
            Order order = _context.Orders.SingleOrDefault(o => o.OrderID.Equals(OrderId));
            if (order != null)
            {
                return order.OrderNumber;
            }

            return null;
        }

        public string GetOrderStatus(string OrderNo)
        {
            var od = from a in _context.Orders
                     where a.OrderNumber.Equals(OrderNo)
                     select a;


            Order order = od.FirstOrDefault();
            if (order != null)
                return order.OrderStatus.ToString();
            else
                return string.Empty;
        }

        public string GetOrderStatus(int orderId)
        {
            string status = string.Empty;
            Order order = _context.Orders.SingleOrDefault(o => o.OrderID == orderId);

            if (order != null)
            {
                status = order.OrderStatus;
            }

            return status;
        }

        public RetrieveSubOrderCompletionDetailResult GetSubOrderCompletionDetail(int subOrderId)
        {
            return _context.RetrieveSubOrderCompletionDetail(subOrderId).FirstOrDefault();
        }

        public RetrievePromotionForOrderResult GetPromotion(int orderId)
        {
            return _context.RetrievePromotionForOrder(orderId).SingleOrDefault();
        }

        public void UpdateCustomizationDetails(int OrderID, int ShoppingCardId, string CustomizationOptionItemID, string Data, string clientIP, string UserName)
        {
            CustomizationDetail custDetails = _context.CustomizationDetails.SingleOrDefault(c => c.OrderID == OrderID && c.CustomizationOptionID == CustomizationOptionItemID);
            if (custDetails != null)
            {
                //Update Customization Detail
                custDetails.ShoppingCartID = ShoppingCardId;
                custDetails.CustomizationOptionID = CustomizationOptionItemID;
                custDetails.CustomizationData = Data;
                _context.SubmitChanges();
            }
        }

        public void SaveOrderProcessNotesCustomizationDetails(int OrderID, string clientIP, string UserName)
        {
            OrderProcessNote OrderProcess = new OrderProcessNote();
            OrderProcess.Approval = UserName;
            OrderProcess.Date = System.DateTime.Now.Date;
            OrderProcess.Note = "Customization details updated.";
            OrderProcess.OrderID = OrderID;
            OrderProcess.IP = clientIP;
            _context.OrderProcessNotes.InsertOnSubmit(OrderProcess);
            _context.SubmitChanges();
        }

        public VdpOrderInformation GetVdpOrderInformation(int orderId)
        {
            return _context.VdpOrderInformations.SingleOrDefault(i => i.OrderID == orderId);
        }

        public void SaveVdpOrderInformation(int orderId, string description, decimal priceAdjustment, decimal postageCost)
        {
            VdpOrderInformation vdpInfo = _context.VdpOrderInformations.SingleOrDefault(i => i.OrderID == orderId);
            if (vdpInfo == null)
            {
                vdpInfo = new VdpOrderInformation();
                vdpInfo.OrderID = orderId;
                vdpInfo.Description = description;
                vdpInfo.PriceAdjustment = priceAdjustment;
                vdpInfo.PostageCost = postageCost;
                vdpInfo.Completed = true;
                _context.VdpOrderInformations.InsertOnSubmit(vdpInfo);
                _context.SubmitChanges();
            }
            else
            {
                vdpInfo.Description = description;
                vdpInfo.PriceAdjustment = priceAdjustment;
                vdpInfo.PostageCost = postageCost;
                vdpInfo.Completed = true;
                _context.SubmitChanges();
            }

            var orderItems = from o in _context.OrderItems
                             where o.OrderID.Equals(orderId)
                             select o;

            int subOrderId = 0;
            foreach (OrderItem orderItem in orderItems)
            {
                if (orderItem.VDP)
                {
                    subOrderId = orderItem.SubOrderID.Value;
                }
            }

            if (subOrderId > 0)
            {
                var subOrders = from s in _context.SubOrders
                                where s.SubOrderID.Equals(subOrderId)
                                select s;

                foreach (SubOrder subOrder in subOrders)
                {
                    subOrder.Completed = true;
                    subOrder.DateCompleted = DateTime.Now;
                }
                _context.SubmitChanges();

                RetrieveSubOrderCompletionDetailResult completionDetail = GetSubOrderCompletionDetail(subOrderId);
                if (completionDetail.CompletedSiblingSubOrders == completionDetail.SiblingSubOrders)
                {
                    string strHostName = System.Net.Dns.GetHostName();
                    string clientIPAddress = System.Net.Dns.GetHostAddresses(strHostName).GetValue(0).ToString();
                    SetOrderStatus(orderId, "Shipped", "Order Shipped/Fulfilled", clientIPAddress, _userContext.User.UserName);
                }
            }
        }

        public Order GetLatestOrderForUser(string userName)
        {
            return (from o in _context.Orders where o.UserID.Contains(userName) orderby o.OrderDate descending select o).FirstOrDefault();
        }

        public Order GetOrderById(int orderId)
        {
            return _context.Orders.SingleOrDefault(o => o.OrderID == orderId);
        }

        public IList<OrderItem> GetOrderItemsForOrder(int orderId)
        {
            return (from oi in _context.OrderItems where oi.OrderID == orderId select oi).ToList();
        }

        public void UpdateOrderItemHasAdvisorsTag(int orderItemId, bool hasAdvisorsTag)
        {
            var orderItem = _context.OrderItems.FirstOrDefault(oi => oi.OrderItemID == orderItemId);
            if (orderItem != null)
            {
                orderItem.HasAdvisorsTag = hasAdvisorsTag;
                _context.SubmitChanges();
            }
        }

        public void UpdateOrderConsentGiven(int orderId, bool consentGiven)
        {
            var order = _context.Orders.FirstOrDefault(o => o.OrderID == orderId);
            if (order != null)
            {
                order.ConsentGiven = consentGiven;
                _context.SubmitChanges();
            }
        }
    }
}