﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using Newtonsoft.Json;
using Slam.Cms.Common.Security;
using Slam.Cms.Configuration;
using StackExchange.Profiling;
using System.Web;
using System.Web.Security;

namespace Mastercard.MarketingCenter.Services
{
    public class UserContext
    {
        private HttpContextBase _context;
        private readonly string _userContextCookieName;
        private readonly IUserService _userService;
        private readonly IIssuerService _issuerService;
        private readonly ICookieService _cookieService;
        private readonly ICountryService _countryService;
        private readonly ILanguageService _languageService;
        private readonly bool _defaultLanguageIsFromBrowser;

        private User _user;
        public User User
        {
            get
            {
                if (_user == null && _userService != null && _context != null) // validation for simple constructor: "UserContext(string region, string language)"
                {
                    _user = _userService.GetUserByUserName(_context.User.Identity.Name);
                }

                return _user;
            }
        }

        private string _region;
        public string SelectedRegion { get { return _region?.Trim() ?? string.Empty; } }

        public string EnvironmentRegion
        {
            get
            {
                if (!ConfigurationManager.Environment.Settings["ApplicableRegions"].Contains(SelectedRegion))
                {
                    if (ConfigurationManager.Environment.Settings["ApplicableRegions"].Contains(RegionalizeService.DefaultRegion))
                    {
                        return RegionalizeService.DefaultRegion;
                    }

                    return RegionalizeService.GlobalRegion;
                }

                return SelectedRegion;
            }
        }

        private string _language;
        public string SelectedLanguage { get { return _language?.Trim() ?? string.Empty; } }

        private string _browserLanguage;
        public string BrowserLanguage
        {
            get
            {
                if (string.IsNullOrEmpty(_browserLanguage))
                {
                    _browserLanguage = RegionalizeService.GetBrowserLanguage();
                }

                return _browserLanguage;
            }
        }

        /// <summary>
        /// Currently IsMastercardUser is being stored in Session
        /// </summary>
        public bool IsMastercardUser
        {
            get
            {
                var session = HttpContext.Current.Session;
                if (session != null && session["CurrentUserIsMastercardEmployee"] == null)
                {
                    var issuer = User != null ? _issuerService.GetIssuerById(User.IssuerId) : null;
                    HttpContext.Current.Session["CurrentUserIsMastercardEmployee"] = issuer != null ? issuer.IsMastercardIssuer : false;
                }

                return session != null ? (bool)session["CurrentUserIsMastercardEmployee"] : false;
            }
        }

        public UserContext(
            HttpContextBase context,
            IUserService userService,
            CookieNameProviderService cookieNameProviderService,
            IIssuerService issuerService,
            ICookieService cookieService,
            ICountryService countryService,
            ILanguageService languageService,
            bool defaultLanguageIsFromBrowser)
        {
            using (MiniProfiler.Current.Step("UserContext (constructor)"))
            {
                _context = context;
                _issuerService = issuerService;
                _userService = userService;
                _userContextCookieName = cookieNameProviderService.GetUserContextCookieName();
                _cookieService = cookieService;
                _countryService = countryService;
                _languageService = languageService;
                GetUserContextCookie();
                _defaultLanguageIsFromBrowser = defaultLanguageIsFromBrowser;
            }
        }

        public UserContext(string region, string language)
        {
            _region = region;
            _language = language;
        }

        private void GetUserContextCookie()
        {
            var ucCookie = _cookieService.Get(_userContextCookieName);
            if (ucCookie != null)
            {
                var ucCookieJson = EncryptionService.Decrypt(ucCookie);
                var ucCookieObject = JsonConvert.DeserializeObject<Common.Infrastructure.UserContextCookie>(ucCookieJson);

                _region = ucCookieObject.SelectedRegion.IsNullOrEmpty() ? RegionalizeService.DefaultRegion : ucCookieObject.SelectedRegion;
                _language = ucCookieObject.SelectedLanguage.IsNullOrEmpty() ? BrowserLanguage : ucCookieObject.SelectedLanguage;
            }
            else
            {
                _region = RegionalizeService.DefaultRegion;
                _language = _defaultLanguageIsFromBrowser ? BrowserLanguage : RegionalizeService.DefaultLanguage;
            }
        }

        public void SetUserSelectedRegion(string region)
        {
            _region = region;

            SetUserContextCookie();
        }

        public void SetUserSelectedLanguage(string language)
        {
            _language = language;

            SetUserContextCookie();
        }

        private void SetUserContextCookie()
        {
            var encryptedValue = EncryptionService.Encrypt(JsonConvert.SerializeObject(
                    new Common.Infrastructure.UserContextCookie
                    {
                        SelectedLanguage = _language,
                        SelectedRegion = _region
                    }));

            if (_cookieService != null)
            {
                _cookieService.Set(_userContextCookieName ?? string.Empty, encryptedValue, FormsAuthentication.CookieDomain);
            }
        }

        public string GetUserCountryCode()
        {
            return User == null ? SelectedRegion : _countryService.GetCountryByIdentifier(User.Country)?.Code;
        }

        public string GetSelectedLanguageCode()
        {
            return _languageService.GetLanguageByIdentifier(
                                                            !SelectedLanguage.IsNullOrEmpty() ? SelectedLanguage :
                                                            !User?.Language.IsNullOrEmpty() ?? false ? User.Language :
                                                            !BrowserLanguage.IsNullOrEmpty() ? BrowserLanguage :
                                                            RegionalizeService.DefaultLanguage
                                                           )?.Code;
        }
    }
}