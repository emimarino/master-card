﻿using Mastercard.MarketingCenter.Services.Interfaces;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Services
{
    public class CaptchaService : ICaptchaService
    {
        private readonly ISettingsService _settingsService;

        public CaptchaService(ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        public ICaptcha BindInvisibleReCaptcha()
        {
            return new InvisibleReCaptcha(_settingsService.GetReCaptchaInvisibleSiteKey());
        }
    }

    class InvisibleReCaptcha : ICaptcha
    {
        public InvisibleReCaptcha(string reCaptchaInvisibleSiteKey)
        {
            reCaptchaSiteKey = reCaptchaInvisibleSiteKey;
            Parameters = new Dictionary<string, string> { { "SiteKey", reCaptchaInvisibleSiteKey }, { "Token", string.Empty }, { "VerificationEndpoint", string.Empty } };
        }

        private static string reCaptchaSiteKey { get; set; }
        public string CaptchaValidatorTrigger { get; set; }
        public string CaptchaValidatorHandler { get; set; }
        public string CaptchaValidationResult { get; set; }
        public Dictionary<string, string> Parameters { get; set; }
        public string Discriminator { get; }

        public Dictionary<string, string> GetValidationRequestParameters()
        {
            return new Dictionary<string, string>();
        }

        public bool IsConfigurationValid()
        {
            return !string.IsNullOrEmpty(reCaptchaSiteKey);
        }
    }
}