﻿using Mastercard.MarketingCenter.Common.Services;
using System.Linq;
using System.Web.Security;

namespace Mastercard.MarketingCenter.Services
{
    public class RegistrationOperationsService
    {
        public RegistrationOperationsService()
        {
        }

        public Data.RetrieveIssuerResult GetIssuerListForDomain(string domain, string regionId)
        {
            var mcontext = new Data.MasterCardPortalDataContext();
            return mcontext.RetrieveIssuer(domain, regionId).FirstOrDefault();
        }

        public void SetUserPassword(string userName, string password)
        {
            MembershipUser user = MembershipService.GetUser(userName);
            user.IsApproved = true;
            Membership.UpdateUser(user);
            user.UnlockUser();
            user.ChangePassword(user.GetPassword(), password);
        }

        public string GetUserNameForEmail(string email)
        {
            return Membership.GetUserNameByEmail(email) ?? string.Empty;
        }

        public bool VerifyUserCompletedRegistration(string username)
        {
            bool registered = false;
            if (!string.IsNullOrEmpty(username))
            {
                MembershipUser user = MembershipService.GetUser(username);
                registered = user.IsApproved;
            }

            return registered;
        }
    }
}