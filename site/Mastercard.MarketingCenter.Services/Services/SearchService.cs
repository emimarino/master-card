﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Models;
using Slam.Cms.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services.Services
{
    public class SearchService : ISearchService
    {
        private readonly IMarketingCenterWebApplicationService _marketingCenterWebApplicationService;
        private readonly ISegmentationService _segmentationService;
        private readonly ISearchRepository _searchRepository;

        public SearchService(IMarketingCenterWebApplicationService marketingCenterWebApplicationService, ISegmentationService segmentationService,
            ISearchRepository searchRepository)
        {
            _marketingCenterWebApplicationService = marketingCenterWebApplicationService;
            _segmentationService = segmentationService;
            _searchRepository = searchRepository;
        }

        public SearchContentResultModel GetSearchResults(string searchQuery, string selectedRegion, string specialAudienceAccess)
        {
            IEnumerable<string> currentSegmentationIds = GetCurrentSegmentationIds(selectedRegion);
            var clearQuery = searchQuery.GetQueryClearOfSynonyms();
            SearchContentResultModel AllResults = new SearchContentResultModel()
            {
                SearchResults = GetSearchResults(clearQuery, currentSegmentationIds, specialAudienceAccess, selectedRegion),
                FeaturedSearchResults = GetFeaturedSearchResults(clearQuery, currentSegmentationIds, specialAudienceAccess, selectedRegion),
            };

            return AllResults;
        }

        public List<FeaturedSearchResult> GetFeaturedSearchResults(string searchQuery, IEnumerable<string> currentSegmentationIds, string specialAudienceAccess, string regionId)
        {
            return _searchRepository.GetFeaturedSearchResults(searchQuery, currentSegmentationIds, specialAudienceAccess, regionId);
        }

        public void AddNewSearchActivity(string userName, DateTime searchDateTime, string searchBoxEntry, string urlVisited, string regionId, int? searchResultsQty)
        {
            _searchRepository.AddNewSearchActivity(userName, searchDateTime, searchBoxEntry, urlVisited, regionId, searchResultsQty);
        }

        public IEnumerable<SearchResult> GetSearchResults(string searchQuery, IEnumerable<string> currentSegmentationIds, string specialAudienceAccess, string regionId)
        {
            return _searchRepository.GetSearchResults(searchQuery, currentSegmentationIds, specialAudienceAccess, regionId);
        }

        private IEnumerable<string> GetCurrentSegmentationIds(string selectedRegion)
        {
            var currentSegmentationIds = _marketingCenterWebApplicationService.GetCurrentSegmentationIds();
            if (currentSegmentationIds.Any(s => s.Equals(RegionConfigManager.DefaultSegmentation, StringComparison.InvariantCultureIgnoreCase)))
            {
                currentSegmentationIds = _segmentationService.GetSegmentationsByRegion(selectedRegion).Select(s => s.SegmentationId).ToArray();
            }

            return currentSegmentationIds;
        }
    }
}