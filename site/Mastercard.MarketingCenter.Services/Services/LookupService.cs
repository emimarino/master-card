﻿using Mastercard.MarketingCenter.Services.Data;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class LookupService
    {
        private MasterCardPortalDataContext _context;

        public LookupService(MasterCardPortalDataContext context)
        {
            _context = context;
        }

        public List<RetrieveIssuerByTextResult> GetUserIssuers(string text, string processorId, string regionId)
        {
            return _context.RetrieveIssuerByText(text, processorId, regionId).ToList();
        }

        public List<RetrieveOrderNoByTextResult> GetOrders(string text, string processorId)
        {
            return _context.RetrieveOrderNoByText(text, processorId).ToList();
        }

        public List<RetrieveUserByTextResult> GetUserNames(string text, string processorId, string regionId)
        {
            return _context.RetrieveUserByText(text, processorId, regionId).ToList();
        }

        public List<RetrieveUserEmailByTextResult> GetUserEmails(string text, string processorId, string regionId)
        {
            return _context.RetrieveUserEmailByText(text, processorId, regionId).ToList();
        }

        public List<RetrieveUsersFromOrdersResult> GetOrderNames(string text, string processorId)
        {
            return _context.RetrieveUsersFromOrders(text, processorId).ToList();
        }

        public List<RetrieveUserEmailsFromOrdersResult> GetOrderEmails(string text, string processorId)
        {
            return _context.RetrieveUserEmailsFromOrders(text, processorId).ToList();
        }

        public List<RetrieveIssuersFromOrdersResult> GetOrderIssuers(string text, string processorId)
        {
            return _context.RetrieveIssuersFromOrders(text, processorId).ToList();
        }
    }
}