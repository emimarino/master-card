﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Services
{
    public class MostPopularService : IMostPopularService
    {
        readonly private IMostPopularRepository _mostPopularRepository;
        private readonly ICachingService _cachingService;
        private readonly ISqlCacheRepository _sqlCacheRepository;
        private readonly RegionRepository _regionRepository;

        public MostPopularService(
            IMostPopularRepository mostPopularRepository,
            ICachingService cachingService,
            ISqlCacheRepository sqlCacheRepository,
            RegionRepository regionRepository)
        {
            _mostPopularRepository = mostPopularRepository;
            _cachingService = cachingService;
            _sqlCacheRepository = sqlCacheRepository;
            _regionRepository = regionRepository;
        }

        public MostPopularProcess CreateProcess()
        {
            var mostPopularProcess = new MostPopularProcess { ProcessDate = DateTime.Now };

            return this._mostPopularRepository.Insert(mostPopularProcess);
        }

        public void ProcessMostPopularActivity(MostPopularProcess mostPopularProcess)
        {
            var endDate = DateTime.Today.AddDays(1);
            var startDate = endDate.AddDays(-7);

            try
            {
                _mostPopularRepository.ProcessMostPopularActivity(startDate, endDate, ConfigurationManager.AppSettings["RefreshMostPopularData.ContentTypeIds"], mostPopularProcess.Id);
                _mostPopularRepository.GetAllMostPopularActivities().Where(mpa => mpa.MostPopularProcessId != mostPopularProcess.Id).ForEach(mpa => _mostPopularRepository.DeleteMostPopularActivity(mpa));
            }
            catch (Exception)
            {
                _mostPopularRepository.GetMostPopularActivitiesByProcessId(mostPopularProcess.Id).ForEach(mpa => _mostPopularRepository.DeleteMostPopularActivity(mpa));
                _mostPopularRepository.DeleteMostPopularProcess(mostPopularProcess);

                throw;
            }
            finally
            {
                _regionRepository.GetAll().ForEach(r => _sqlCacheRepository.SaveRegionInvalidator($"MostPopularActivity_{r.IdTrimmed}"));
            }
        }

        public IEnumerable<MostPopularActivity> GetMostPopularActivitiesByRegion(string regionId)
        {
            var mostPopularActivities = _cachingService.Get<IList<MostPopularActivity>>($"MostPopularActivity_{regionId}");
            if (mostPopularActivities == null)
            {
                mostPopularActivities = _mostPopularRepository.GetMostPopularActivitiesByRegion(regionId).ToList();
                _cachingService.Save($"MostPopularActivity_{regionId}", mostPopularActivities);
            }

            return mostPopularActivities;
        }
    }
}