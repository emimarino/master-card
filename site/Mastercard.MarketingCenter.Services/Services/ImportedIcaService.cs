﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class ImportedIcaService : IImportedIcaService
    {
        readonly private IImportedIcaRepository _importedIcaRepository;

        public ImportedIcaService(IImportedIcaRepository importedIcaRepository)
        {
            _importedIcaRepository = importedIcaRepository;
        }

        public ImportedIca GetByCid(string cid)
        {
            return _importedIcaRepository.GetByCid(cid);
        }

        public IEnumerable<ImportedIca> GetSearchImportedIcas(string searchTerm)
        {
            return _importedIcaRepository.GetAllActive()
                                         .GroupBy(ii => ii.Cid)
                                         .Select(ii => ii.FirstOrDefault(i => i.ImportedIcaId == ii.Max(m => m.ImportedIcaId)))
                                         .Where(o => string.IsNullOrEmpty(searchTerm) ||
                                                     o.LegalName.Contains(searchTerm) ||
                                                     o.Cid == searchTerm ||
                                                     o.HqCityName.Contains(searchTerm) ||
                                                     o.HqProvinceName.Contains(searchTerm))
                                         .Include(i => i.MetricHeatmap);
        }
    }
}