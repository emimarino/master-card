﻿using Mastercard.MarketingCenter.Common.Extensions;
using Pulsus;
using System.IO;
using System.Net;
using System.Net.Http;

namespace Mastercard.MarketingCenter.Services
{
    public abstract class BaseFileService
    {
        internal static string MapPath(string path, bool isDirectory = false, bool createDirectoryIfNotExists = true)
        {
            if (path.IsNullOrEmpty())
            {
                return string.Empty;
            }

            string mappedPath = System.Web.Hosting.HostingEnvironment.MapPath(path);
            if (isDirectory && !Directory.Exists(mappedPath))
            {
                if (!createDirectoryIfNotExists)
                {
                    return string.Empty;
                }

                Directory.CreateDirectory(mappedPath);
            }

            return mappedPath;
        }

        internal static void DownloadFileFromUrl(string fileUrl, string filePath)
        {
            using (var file = File.Create(filePath))
            {
                using (var client = new HttpClient())
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                    var task = client.GetAsync(fileUrl)
                                     .ContinueWith((response) =>
                                     {
                                         if (response.Status != System.Threading.Tasks.TaskStatus.RanToCompletion || !response.Result.IsSuccessStatusCode)
                                         {
                                             LogManager.EventFactory.Create()
                                                                    .Text("Error downloading file from url...")
                                                                    .Level(LoggingEventLevel.Error)
                                                                    .AddTags("download_file")
                                                                    .AddData("Download File Url", fileUrl)
                                                                    .AddData("Download File Path", filePath)
                                                                    .AddData("Response Status", response.Status.ToString())
                                                                    .AddData("Response Status Code",
                                                                             response.Status == System.Threading.Tasks.TaskStatus.RanToCompletion ?
                                                                             response.Result.StatusCode.ToString() : HttpStatusCode.InternalServerError.ToString())
                                                                    .Push();
                                         }
                                         else
                                         {
                                             response.Result.Content.ReadAsStreamAsync().Result.CopyToAsync(file);
                                         }
                                     });
                    task.Wait();
                }
            }
        }
    }
}