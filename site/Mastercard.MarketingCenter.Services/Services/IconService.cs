﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services
{
    public class IconService : IIconService
    {
        private readonly IconRepository _iconRepository;

        public IconService(IconRepository iconRepository)
        {
            _iconRepository = iconRepository;
        }

        public IEnumerable<Icon> GetAll()
        {
            return _iconRepository.GetAll();
        }

        public Icon GetIcon(string iconId)
        {
            return _iconRepository.GetIcon(iconId);
        }

        public string GetIconUrl(Icon icon)
        {
            return _iconRepository.GetIconUrl(icon);
        }

        public string GetIconUrl(string iconId)
        {
            return GetIconUrl(GetIcon(iconId));
        }
    }
}