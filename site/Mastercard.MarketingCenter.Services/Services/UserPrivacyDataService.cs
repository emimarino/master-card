﻿using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Metadata.GDP;
using Mastercard.MarketingCenter.Domo.Services;
using Mastercard.MarketingCenter.DTO.GDP;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class UserPrivacyDataService : IUserPrivacyDataService
    {
        private readonly IUserService _userService;
        private readonly IActivityService _domoActivityService;
        private readonly IDownloadsService _domoDownloadsService;
        private readonly ISettingsService _settingsService;

        public UserPrivacyDataService(
            IUserService userService,
            IActivityService domoActivityService,
            IDownloadsService domoDownloadsService,
            ISettingsService settingsService)
        {
            _userService = userService;
            _domoActivityService = domoActivityService;
            _domoDownloadsService = domoDownloadsService;
            _settingsService = settingsService;
        }

        public UserDTO GetByEmail(string email)
        {
            User user = _userService.GetUserByEmail(email);
            if (user == null)
            {
                return null;
            }

            return new UserDTO
            {
                FullName = user.FullName,
                FirstName = user.Profile.FirstName,
                LastName = user.Profile.LastName,
                Address = new UserAddressDTO
                {
                    Address1 = user.Profile.Address1,
                    Address2 = user.Profile.Address2,
                    Address3 = user.Profile.Address3,
                    City = user.Profile.City,
                    Country = user.Country,
                    PostalCode = user.Profile.Zip,
                    Province = user.Profile.State,
                    State = user.Profile.State
                },
                Email = email,
                PhoneNumbers = new List<string>
                {
                    user.Profile.Phone,
                    user.Profile.Mobile
                }
            };
        }

        public string MaskUserData(string email, out ExpiredUserDTO expiredUser)
        {
            expiredUser = null;
            // Use new database connection to ensure data integrity
            using (var marketingCenterDbContext = new MarketingCenterDbContext())
            {
                using (var userRepository = new UserRepository(marketingCenterDbContext, new StandaloneMembershipProvider()))
                {
                    User user = userRepository.GetByEmail(email);
                    if (user == null)
                    {
                        return ResponseCode.NotFound;
                    }

                    var userFirstName = user.Profile.FirstName;

                    user.Email = user.UserName; //Email should stay unique, but because it must be masked it should not remain a valid email, so just use the username
                    user.IsDisabled = true;
                    user.ModifiedDate = DateTime.Now;
                    user.Profile.GdprFollowUpStatusId = MarketingCenterDbConstants.GdprFollowUpStatus.Deleted;
                    user.Profile.Address1 = Mask(user.Profile.Address1);
                    user.Profile.Address2 = Mask(user.Profile.Address2);
                    user.Profile.Address3 = Mask(user.Profile.Address3);
                    user.Profile.City = Mask(user.Profile.City);
                    user.Profile.Fax = Mask(user.Profile.Fax);
                    user.Profile.FirstName = Mask(user.Profile.FirstName);
                    user.Profile.LastName = Mask(user.Profile.LastName);
                    user.Profile.Mobile = Mask(user.Profile.Mobile);
                    user.Profile.Phone = Mask(user.Profile.Phone);
                    user.Profile.State = Mask(user.Profile.State);
                    user.Profile.Title = Mask(user.Profile.Title);
                    user.Profile.Zip = Mask(user.Profile.Zip);
                    user.Profile.IssuerName = Mask(user.Profile.IssuerName);
                    user.Profile.Processor = Mask(user.Profile.Processor);

                    userRepository.Update(user);

                    var membershipUser = userRepository.GetMembershipUserByEmail(email) ?? userRepository.GetMembershipUserByUserName(user.UserName);
                    if (membershipUser != null)
                    {
                        membershipUser.Email = user.UserName;
                        membershipUser.LoweredEmail = user.UserName.ToLower();
                        userRepository.Update(membershipUser);
                    }

                    var pendingRegistrationRepository = new PendingRegistrationRepository(marketingCenterDbContext, userRepository);
                    foreach (var pendingRegistration in pendingRegistrationRepository.GetByEmail(email).ToList())
                    {
                        pendingRegistration.Address1 = user.Profile.Address1;
                        pendingRegistration.Address2 = user.Profile.Address2;
                        pendingRegistration.Address3 = user.Profile.Address3;
                        pendingRegistration.City = user.Profile.City;
                        pendingRegistration.Country = user.Country;
                        pendingRegistration.Fax = user.Profile.Fax;
                        pendingRegistration.FirstName = user.Profile.FirstName;
                        pendingRegistration.LastName = user.Profile.LastName;
                        pendingRegistration.Mobile = user.Profile.Mobile;
                        pendingRegistration.Phone = user.Profile.Phone;
                        pendingRegistration.Title = user.Profile.Title;
                        pendingRegistration.Zip = user.Profile.Zip;
                        pendingRegistration.Email = user.Email;
                        pendingRegistration.FinancialInstitution = user.Profile.IssuerName;
                        pendingRegistration.Processor = user.Profile.Processor;
                        pendingRegistration.Province = user.Profile.State;

                        pendingRegistration.Status = "Expired";
                        pendingRegistration.DateStatusChanged = DateTime.Now;
                        pendingRegistration.RejectionReason = "Expired by GDPR policy.";
                        pendingRegistration.StatusChangedBy = "superadmin@admin.com";

                        pendingRegistrationRepository.Update(pendingRegistration);
                    }

                    var assetRepository = new AssetRepository(marketingCenterDbContext);
                    // find all Assets for which specified User is a Business Owner, replace to default Business Owner
                    assetRepository.UpdateBusinessOwnerAssets(user.UserId, _settingsService.GetPredefinedBusinessOwnerUserId());

                    marketingCenterDbContext.SaveChanges();

                    _domoActivityService.UpdateActivityUserData(user);
                    _domoDownloadsService.UpdateDownloadUserData(user);

                    if (_settingsService.GetDeletedFollowUpEnabled(user.Region.Trim()))
                    {
                        expiredUser = new ExpiredUserDTO
                        {
                            UserId = user.UserId,
                            Email = email,
                            FirstName = userFirstName,
                            Region = user.Region.Trim(),
                            Language = string.IsNullOrEmpty(user.Language) ? RegionalizeService.DefaultLanguage : user.Language.Trim()
                        };
                    }

                    return ResponseCode.Success;
                }
            }
        }

        private string Mask(string value)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : Guid.NewGuid().ToString();
        }
    }
}