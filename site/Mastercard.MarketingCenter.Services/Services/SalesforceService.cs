﻿using AutoMapper;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Interfaces;
using Mastercard.MarketingCenter.DTO.Salesforce;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Services.Services
{
    public class SalesforceService : ISalesforceService
    {
        private readonly ISalesforceRepository _salesforceRepository;
        private readonly IMapper _mapper;
        private readonly ISettingsService _settingsService;
        private int CurrentReportId { get; set; }

        public SalesforceService(ISalesforceRepository salesforceRepository, IMapper mapper, ISettingsService settingsService)
        {
            _salesforceRepository = salesforceRepository;
            _mapper = mapper;
            _settingsService = settingsService;
        }

        public void SaveUpdates()
        {
            _salesforceRepository.Commit();
        }

        public int CreateReport()
        {
            CurrentReportId = _salesforceRepository.CreateSalesforceActivityReport();
            return CurrentReportId;
        }

        public void RefreshSalesforceTasks()
        {
            var endDateTime = DateTime.Now;
            var startDateTime = endDateTime.AddDays(-int.Parse(_settingsService.GetSalesforceIntegrationActivityDays()));

            _salesforceRepository.AddSalesforceTasks(_mapper.Map<IEnumerable<SalesforceTask>>(_salesforceRepository.GetDownloadActivities(startDateTime, endDateTime)));
            _salesforceRepository.AddSalesforceTasks(_mapper.Map<IEnumerable<SalesforceTask>>(_salesforceRepository.GetOrderActivities(startDateTime, endDateTime)));
            _salesforceRepository.AddSalesforceTasks(_mapper.Map<IEnumerable<SalesforceTask>>(_salesforceRepository.GetUserActivities(startDateTime, endDateTime)));
        }

        public void RegisterSalesforceAccessToken(SalesforceAccessTokenResponseDTO accessTokenModel)
        {
            _salesforceRepository.UpdateSalesforceActivityReportById(CurrentReportId, salesforceTokenId: _salesforceRepository.SaveSalesforceToken(_mapper.Map<SalesforceAccessToken>(accessTokenModel)));
        }

        public void UpdateSalesforceActivityReportById(int salesforceActivityReportId, DateTime? endDate = null, int? sent = null, int? failed = null, bool isSuccessed = false, int? salesforceTokenId = null, string loggingEventId = null)
        {
            _salesforceRepository.UpdateSalesforceActivityReportById(salesforceActivityReportId, endDate, sent, failed, isSuccessed, salesforceTokenId, loggingEventId);
        }

        public IEnumerable<SalesforceTask> GetPendingSalesforceTasks()
        {
            return _salesforceRepository.GetSalesforceTasks()
                                        .Where(st => st.SalesforceTaskStatusId == MarketingCenterDbConstants.Salesforce.TaskStatus.Pending
                                                  || st.SalesforceTaskStatusId == MarketingCenterDbConstants.Salesforce.TaskStatus.Failed)
                                        .OrderBy(st => st.ActivityDate);
        }

        public IEnumerable<SalesforceTask> GetAllSalesforceTasks(DateTime from, DateTime to)
        {
            return _salesforceRepository.GetSalesforceTasks(from, to)
                                        .Include(st => st.SalesforceActivityType)
                                        .Include(st => st.SalesforceTaskStatus)
                                        .Include(st => st.SalesforceActivityReport)
                                        .OrderBy(st => st.ActivityDate);
        }

        public IEnumerable<SalesforceTask> GetCompletedSalesforceTasks(DateTime from, DateTime to)
        {
            return _salesforceRepository.GetSalesforceTasks(from, to)
                                        .Where(st => st.SalesforceTaskStatusId == MarketingCenterDbConstants.Salesforce.TaskStatus.Completed)
                                        .OrderBy(st => st.ActivityDate);
        }
    }
}