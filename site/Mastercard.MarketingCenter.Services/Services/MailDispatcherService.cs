﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using Newtonsoft.Json;
using Pulsus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Configuration;

namespace Mastercard.MarketingCenter.Services
{
    public class MailDispatcherService : IMailDispatcherService
    {
        private readonly MailDispatcherRepository _mailDispatcherRepository;
        private readonly IUrlService _urlService;

        public MailDispatcherService(MailDispatcherRepository mailDispatcherRepository, IUrlService urlService)
        {
            _mailDispatcherRepository = mailDispatcherRepository;
            _urlService = urlService;
        }

        public MailDispatcher GetMailDispatcherById(int mailDispatcherId)
        {
            return _mailDispatcherRepository.GetMailDispatcherById(mailDispatcherId);
        }

        public IEnumerable<MailDispatcher> GetPendingMails()
        {
            return _mailDispatcherRepository.GetAll().Where(md => md.MailDispatcherStatusId == MarketingCenterDbConstants.MailDispatcherStatus.New ||
                                                                  md.MailDispatcherStatusId == MarketingCenterDbConstants.MailDispatcherStatus.Failed);
        }

        private bool AreEmailsValid(string emails)
        {
            var areEmailsValid = true;
            var regexToValidateEmail = new Regex(@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$", RegexOptions.IgnoreCase);
            var emailsToValidate = emails.Split(';', ',');
            foreach (var singleEmail in emailsToValidate)
            {
                areEmailsValid = areEmailsValid && regexToValidateEmail.IsMatch(singleEmail);
            }

            return areEmailsValid;
        }

        public MailDispatcher CreateMailDispatcher(string subject, string fromAddress, string fromName, string toAddress, string body, int userId, string regionId, string trackingType, int totalLinks = 0, string ccAddress = null, string bccAddress = null, bool addTrackingPixelUrl = false, int mailDispatcherStatusId = MarketingCenterDbConstants.MailDispatcherStatus.New, bool isBodyHtml = true, string attachmentFilename = null, bool isMultipart = false, string alternativeBody = null, bool saveChanges = true)
        {
            if (!AreEmailsValid(fromAddress) || !AreEmailsValid(toAddress))
            {
                throw new Exception($"Invalid Email - From: {fromAddress} To: {toAddress}");
            }

            var mailDispatcher = new MailDispatcher
            {
                FromAddress = fromAddress,
                FromName = fromName,
                ToAddress = toAddress,
                CCAddress = ccAddress,
                BCCAddress = bccAddress,
                Subject = subject,
                Body = body,
                IsBodyHtml = isBodyHtml,
                UserId = userId,
                RegionId = regionId,
                MailDispatcherStatusId = mailDispatcherStatusId,
                FailedSendAttemptCount = 0,
                CreatedDate = DateTime.Now,
                ModifiedDate = DateTime.Now,
                TrackingType = trackingType,
                TotalLinks = totalLinks,
                AttachmentFilename = attachmentFilename,
                IsMultipart = isMultipart,
                TextPlainBody = alternativeBody
            };

            _mailDispatcherRepository.Insert(mailDispatcher);

            if (saveChanges)
            {
                _mailDispatcherRepository.Commit();

                if (addTrackingPixelUrl)
                {
                    mailDispatcher.Body = mailDispatcher.Body.Replace("[#MailDispatcherTrackingPixelUrl]", _urlService.GetTrackingPixelUrl(mailDispatcher.RegionId, mailDispatcher.MailDispatcherId));
                    _mailDispatcherRepository.Commit();
                }
            }

            return mailDispatcher;
        }

        public void SaveMailDispatcher(MailDispatcher mailDispatcher)
        {
            if (mailDispatcher != null)
            {
                mailDispatcher.ModifiedDate = DateTime.Now;
            }
        }

        public bool SendMailDispatcher(MailDispatcher mailDispatcher)
        {
            var mailSent = false;
            if (mailDispatcher != null)
            {
                var maxFailedSendAttempts = string.IsNullOrEmpty(WebConfigurationManager.AppSettings["MailDispatcher.MaxFailedSendAttempts"]) ? 1 :
                                            Convert.ToInt32(WebConfigurationManager.AppSettings["MailDispatcher.MaxFailedSendAttempts"]);
                try
                {
                    string testFromAddress = WebConfigurationManager.AppSettings["MailDispatcher.OverrideFromAddress"];
                    var mailMessage = new MailMessage
                    {
                        From = new MailAddress(
                                string.IsNullOrEmpty(testFromAddress) ? mailDispatcher.FromAddress : testFromAddress,
                                string.IsNullOrEmpty(testFromAddress) ? mailDispatcher.FromName : $"[TEST FROM: {mailDispatcher.FromAddress} - {mailDispatcher.FromName}]"
                               ),
                        Subject = mailDispatcher.Subject,
                        IsBodyHtml = mailDispatcher.IsBodyHtml && !mailDispatcher.IsMultipart,
                        Body = mailDispatcher.IsMultipart ? mailDispatcher.TextPlainBody : mailDispatcher.Body
                    };

                    if (mailDispatcher.IsMultipart)
                    {
                        // Html content must be inserted like an alternate view in the last position due email clients follow to RFC 2822: 5.1.4.  Alternative Subtype
                        // this means that order could be used to override the priority of the content type (last inserted - more higher priority).
                        mailMessage.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(mailDispatcher.Body, Encoding.UTF8, "text/html"));
                    }

                    if (!string.IsNullOrEmpty(mailDispatcher.AttachmentFilename))
                    {
                        var attachment = new System.Net.Mail.Attachment(mailDispatcher.AttachmentFilename);
                        mailMessage.Attachments.Add(attachment);
                    }

                    string testToAddress = WebConfigurationManager.AppSettings["MailDispatcher.OverrideToAddress"];
                    var mailTo = string.Empty;
                    if (string.IsNullOrEmpty(testToAddress))
                    {
                        mailTo = mailDispatcher.ToAddress;
                        SplitMailAddress(mailDispatcher.CCAddress, mailMessage.CC);
                        SplitMailAddress(mailDispatcher.BCCAddress, mailMessage.Bcc);
                    }
                    else
                    {
                        var regionOverrideToAddress = RegionalizeService.RegionalizeSetting("MailDispatcher.OverrideToAddress", mailDispatcher.RegionId, false);
                        mailTo = !string.IsNullOrEmpty(regionOverrideToAddress) ? regionOverrideToAddress : testToAddress;
                        mailMessage.Subject = $"[{mailDispatcher.RegionId.ToUpper()} TEST TO:{mailDispatcher.ToAddress}] {mailMessage.Subject}";
                    }

                    SplitMailAddress(mailTo, mailMessage.To);

                    new SmtpClient().Send(mailMessage);
                    mailSent = true;
                    mailDispatcher.MailDispatcherStatusId = MarketingCenterDbConstants.MailDispatcherStatus.Sent;

                    mailMessage.Attachments.Clear(); //attachments cannot be serialized, but this is still recorded in MailDispatcher
                    mailMessage.AlternateViews.Clear(); //alternate views are a type of attachment, due to hangfire can't serialize the content must be erased it.
                    mailDispatcher.MailData = JsonConvert.SerializeObject(mailMessage);
                }
                catch (Exception ex)
                {
                    mailDispatcher.ErrorMessage = string.Format("Exception Message: {0} - Exception InnerException: {1} - Exception Data: {2}", ex.Message, ex.InnerException, ex.Data);
                    mailDispatcher.FailedSendAttemptCount++;
                    if (!mailSent) // If the email was sent, probably the error was on the data saving, so we don't force to send the email again.
                    {
                        mailDispatcher.MailDispatcherStatusId = mailDispatcher.FailedSendAttemptCount >= maxFailedSendAttempts ?
                                                                MarketingCenterDbConstants.MailDispatcherStatus.Error :
                                                                MarketingCenterDbConstants.MailDispatcherStatus.Failed;
                    }

                    LogManager.EventFactory.Create()
                       .Text("Error Sending Mail Dispatcher...")
                       .Level(LoggingEventLevel.Error)
                       .AddData("Mail Sent", mailSent)
                       .AddData("MailDispatcherId", mailDispatcher.MailDispatcherId)
                       .AddData("Exception Message", ex.Message)
                       .AddData("Exception InnerException", ex.InnerException)
                       .AddData("Exception Data", ex.Data)
                       .Push();
                }
                finally
                {
                    SaveMailDispatcher(mailDispatcher);
                    _mailDispatcherRepository.Commit(); // We save email data immediately instead of using unit of work process.
                }
            }

            return mailSent;
        }

        private void SplitMailAddress(string address, MailAddressCollection mailAddressCollection)
        {
            if (!string.IsNullOrWhiteSpace(address))
            {
                string[] addresses = address.Split(new char[] { ',', ';' });
                for (int i = 0; i < addresses.Length; i++)
                {
                    if (addresses[i].Trim().Length > 0)
                    {
                        mailAddressCollection.Add(new MailAddress(addresses[i]));
                    }
                }
            }
        }
    }
}