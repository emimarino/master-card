﻿using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class ClonesSourceNotificationServices : IClonesSourceNotificationServices
    {
        public readonly IClonesRepository _clonesRepository;
        public readonly RegionRepository _regionRepository;
        public readonly IUserRepository _userRepository;
        public readonly IEmailService _emailService;

        public ClonesSourceNotificationServices(IClonesRepository clonesRepository, RegionRepository regionRepository, IUserRepository userRepository, IEmailService emailService)
        {
            _clonesRepository = clonesRepository;
            _regionRepository = regionRepository;
            _userRepository = userRepository;
            _emailService = emailService;
        }

        public int BuildSourceUpdateNotificationEmails()
        {
            var mailsAdded = 0;
            var activeSourceNotifications = _clonesRepository.GetActiveCloneSourcesNotificationGroupedByRegion();

            foreach (var regionalNotifications in activeSourceNotifications.ToList())
            {
                string region = regionalNotifications.Key;
                var setting = (string)RegionalizeService.RegionalizeSetting("SourceUpdateNotification.RegionalAdminsEmails", region) ?? string.Empty;
                var emailList = setting.Replace(" ", string.Empty).Split(',');
                var notificationsDictionary = regionalNotifications.Select(n => { return new Dictionary<string, string> { { "[#CompareHref]", $"{RegionalizeService.GetBaseUrl(region).TrimEnd('/')}/admin/Content/Edit?id={ n.CloneContentItemId }&showComparison=true&cloneRegion={region.Trim()}" }, { "[#CloneTitle]", n.CloneTitle }, { "[#CloneStatus]", n.SourceStatusName } }; });
                emailList.ToList().ForEach(ea =>
                   {
                       var userName = _userRepository.GetByEmail(ea)?.FullName?.ToString();
                       if (userName == null)
                       {
                           _emailService.SendEmailSourceUpdateNotification(notificationsDictionary, ea, region);
                       }
                       else
                       {
                           _emailService.SendEmailSourceUpdateNotification(notificationsDictionary, ea, region, userName);
                       }
                       mailsAdded++;
                   });

                _clonesRepository.UpdateSate(regionalNotifications);
            }

            return mailsAdded;
        }

        public void ProcessSourceUpdateNotifications()
        {
            //populates and updates entries in CloneSourceNotification wich includes the metadata to create emails afterwards.
            _clonesRepository.PopulateCloneSourceNotification();
        }
    }
}