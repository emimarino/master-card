﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using System;
using System.Collections.Concurrent;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;

namespace Mastercard.MarketingCenter.Services
{
    public class RegionalizeService : IRegionalizeService
    {
        static ConcurrentDictionary<string, string> _countries;
        public static ConcurrentDictionary<string, string> Countries
        {
            get
            {
                return _countries;
            }
            set
            {
                _countries = value;
            }
        }

        private static ConcurrentDictionary<string, string> _languages;
        public static ConcurrentDictionary<string, string> Languages
        {
            get
            {
                return _languages;
            }
            set
            {
                _languages = value;
            }
        }

        private static Slam.Cms.Configuration.RegionConfigManager RegionConfigManager
        {
            get
            {
                return new Slam.Cms.Configuration.RegionConfigManager();
            }
        }

        public static string DefaultLanguage
        {
            get
            {
                return ConfigurationManager.AppSettings["DefaultLanguage"].Trim();
            }
        }

        public static string DefaultRegion
        {
            get
            {
                return ConfigurationManager.AppSettings["DefaultRegion"].Trim();
            }
        }

        public static string GlobalRegion
        {
            get
            {
                return ConfigurationManager.AppSettings["GlobalRegion"].Trim();
            }
        }

        public static string UnauthenticatedRegion
        {
            get
            {
                return "global";
            }
        }

        public static string DefaultCulture
        {
            get
            {
                return $"{DefaultLanguage.ToLower()}-{DefaultRegion.ToUpper()}";
            }
        }

        public bool IsUsBaseUrl(Uri url)
        {
            return url.GetLeftPart(UriPartial.Authority).TrimEnd('/').Equals(ConfigurationManager.AppSettings["AGE.MC.MMP.BaseUrl"].TrimEnd('/'), StringComparison.InvariantCultureIgnoreCase);
        }

        public string GetCountryName(string key)
        {
            string name = string.Empty;

            if (Countries == null ? false : Countries.ContainsKey(key))
            {
                name = Countries != null ? Countries[key]?.ToString() : null;
            }

            if (Countries != null ? false : Countries.ContainsKey(key.ToLower()))
            {
                name = Countries != null ? Countries[key.ToLower()]?.ToString() : null;
            }

            if (Countries != null ? false : Countries.ContainsKey(key.ToUpper()))
            {
                name = Countries != null ? Countries[key.ToUpper()]?.ToString() : null;
            }

            return string.IsNullOrEmpty(name) ? key : name;
        }

        /// This is the language key get the language title from tags or sitemap
        public string GetLanguageTitle(string key)
        {
            string name = string.Empty;

            if (Languages == null ? false : Languages.ContainsKey(key))
            {
                name = Languages != null ? Languages[key]?.ToString() : null;
            }

            if (Languages == null ? false : Languages.ContainsKey(key.ToLower()))
            {
                name = Languages != null ? Languages[key.ToLower()]?.ToString() : null;
            }

            if (Languages == null ? false : Languages.ContainsKey(key.ToUpper()))
            {
                name = Languages != null ? Languages[key.ToUpper()]?.ToString() : null;
            }

            return string.IsNullOrEmpty(name) ? key : name;
        }

        public static string GetBaseUrl(string regionId)
        {
            if (regionId.Trim().Equals("us", StringComparison.InvariantCultureIgnoreCase) || string.IsNullOrWhiteSpace(regionId.Trim()))
            {
                return ConfigurationManager.AppSettings["AGE.MC.MMP.BaseUrl"].ToString();
            }
            else
            {
                return ConfigurationManager.AppSettings["AGE.MC.GMC.BaseUrl"].ToString();
            }
        }

        public static string RegionalizeTemplate(string filePath, string regionId, string languageId = "", string templateFolder = null)
        {
            regionId = string.IsNullOrEmpty(regionId) ? DefaultRegion : regionId;
            languageId = string.IsNullOrEmpty(languageId) ? DefaultLanguage : languageId;

            if (string.IsNullOrWhiteSpace(templateFolder))
            {
                templateFolder = ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"];
            }

            Func<string, string, string, string> getNewPath = (_filePath, _region, _language) =>
            {
                string filename = _filePath.Contains('\\') ? _filePath.Substring(_filePath.LastIndexOf('\\'), (_filePath.Length - 1)) : _filePath;
                string[] strings = filename.Split('.');

                bool folderExists = Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\" + templateFolder + ((!string.IsNullOrEmpty(_region)) && (_region.ToLower() != DefaultRegion) ? (_region) : DefaultRegion.ToUpperInvariant()));

                return ((!string.IsNullOrEmpty(_region)) && (_region.ToLower() != DefaultRegion) && folderExists ? (_region) : DefaultRegion.ToUpperInvariant()) + "\\" + string.Join(".", strings.Take(strings.Length - 1).ToArray()) + ((!string.IsNullOrEmpty(_language)) && (_language.ToLower() != DefaultLanguage) ? ("." + _language) : "") + "." + strings[strings.Length - 1];
            };

            Func<string, bool> fileExists = (_newPath) =>
            {
                return File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, templateFolder, _newPath));
            };

            string newPath = getNewPath(filePath.Trim(), regionId.Trim(), languageId.Trim());
            if (fileExists(newPath))
            {
                return newPath;
            }
            else
            {
                newPath = getNewPath(filePath.Trim(), regionId.Trim(), DefaultLanguage);
                return (!fileExists(newPath)) ? getNewPath(filePath.Trim(), DefaultRegion, DefaultLanguage) : newPath;
            }
        }

        public string RegionalizeResource(string resourceFile, string resourceName, string regionId, string languageId, string appSettingKey = null, bool canDefault = true)
        {
            return HttpContext.GetGlobalResourceObject(
                                                        resourceFile?.Trim() ?? "",
                                                        resourceName?.Trim() ?? "",
                                                        new CultureInfo(GetCulture(regionId, languageId))
                                                      )?.ToString() ?? RegionalizeSetting(appSettingKey, regionId, canDefault);
        }

        public static string RegionalizeSetting(string appSettingKey, string regionId = null, bool canDefault = true)
        {
            return string.IsNullOrWhiteSpace(RegionConfigManager.GetSetting(appSettingKey?.Trim(), regionId?.Trim(), canDefault)?.ToString()) ?
                   (canDefault ? ConfigurationManager.AppSettings[appSettingKey?.Trim()]?.ToString() : null) :
                   RegionConfigManager.GetSetting(appSettingKey?.Trim(), regionId?.Trim(), canDefault)?.ToString();
        }

        public static string RegionalizeSetting(string appSettingKey, string regionId, string languageId)
        {
            string setting = RegionalizeSetting($"{appSettingKey}.{languageId.ToLowerInvariant().Trim()}", regionId, false);
            if (setting.IsNullOrEmpty())
            {
                setting = RegionalizeSetting(appSettingKey, regionId, false);
            }

            return setting;
        }

        public static string GetCulture(string regionId, string languageId)
        {
            return $"{languageId?.Trim().ToLower() ?? DefaultLanguage.ToLower()}-{regionId?.Trim().ToUpper() ?? DefaultRegion.ToUpper()}";
        }

        public static void SetCurrentCulture(string regionId, string languageId)
        {
            if (languageId.Equals("deu") && !regionId.Equals("de"))
            {
                regionId = "de";
            }

            if (Thread.CurrentThread.CurrentCulture.Name != $"{languageId.ToLower()}-{regionId.ToUpper()}")
            {
                try
                {
                    Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo($"{languageId.ToLower()}-{regionId.ToUpper()}");
                    Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                }
                catch (CultureNotFoundException)
                {
                    try
                    {
                        Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo($"{languageId.ToLower()}-{languageId.ToUpper()}");
                        Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                    }
                    catch (CultureNotFoundException)
                    {
                        Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(DefaultCulture);
                        Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
                    }
                }
            }
        }

        public static void SetRegionCulture(string culture, string cultureName, string baseCulture)
        {
            try
            {
                if (!CultureInfo.GetCultures(CultureTypes.AllCultures).Any(c => c.Name.Equals(culture, StringComparison.InvariantCultureIgnoreCase)))
                {
                    CultureInfo cultureInfo = new CultureInfo(baseCulture);
                    RegionInfo regionInfo = new RegionInfo(cultureInfo.Name);
                    CultureAndRegionInfoBuilder cultureAndRegionInfoBuilder = new CultureAndRegionInfoBuilder(culture, CultureAndRegionModifiers.None);
                    cultureAndRegionInfoBuilder.LoadDataFromCultureInfo(cultureInfo);
                    cultureAndRegionInfoBuilder.LoadDataFromRegionInfo(regionInfo);
                    cultureAndRegionInfoBuilder.CultureEnglishName = cultureName;
                    cultureAndRegionInfoBuilder.CultureNativeName = cultureName;
                    cultureAndRegionInfoBuilder.Register();

                    LogManager.EventFactory.Create()
                                      .Text($"Successfully Registered Custom Culture: {culture} - {cultureName}")
                                      .Level(LoggingEventLevel.Trace)
                                      .AddData("Culture", culture)
                                      .AddData("Culture Name", cultureName)
                                      .Push();
                }
            }
            catch (Exception ex)
            {
                LogManager.EventFactory.Create()
                                       .Text($"Exception Error Registering Custom Culture: {culture} - {cultureName}")
                                       .Level(LoggingEventLevel.Alert)
                                       .AddData("Exception Message", ex.Message)
                                       .AddData("Exception InnerException", ex.InnerException)
                                       .AddData("Exception Data", ex.Data)
                                       .AddData("Exception StackTrace", ex.StackTrace)
                                       .AddData("Culture", culture)
                                       .AddData("Culture Name", cultureName)
                                       .Push();
                throw ex;
            }
        }

        public static string GetBrowserLanguage()
        {
            string browserLanguage = HttpContext.Current.Request.UserLanguages?[0].Split('-')?.First().ToLower();
            if (!browserLanguage.IsNullOrEmpty() && browserLanguage.Equals("de"))
            {
                browserLanguage = browserLanguage.Replace("de", "deu");
            }

            return browserLanguage ?? DefaultLanguage;
        }

        public string GetSalesforceResource(string resourceName, string regionId)
        {
            return RegionalizeResource("Salesforce", resourceName, regionId, DefaultLanguage);
        }
    }
}