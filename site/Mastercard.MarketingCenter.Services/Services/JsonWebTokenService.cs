﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Services
{
    public class JsonWebTokenService : IJsonWebTokenService
    {
        public JsonWebTokenService()
        {
        }

        public string GetToken<T>(T model)
        {
            string header = "{\"typ\":\"JWT\",\"alg\":\"HS256\"}";
            string claims = JsonConvert.SerializeObject(model);
            string b64header = Convert.ToBase64String(Encoding.UTF8.GetBytes(header));
            string b64claims = Convert.ToBase64String(Encoding.UTF8.GetBytes(claims));
            string payload = $"{b64header}.{b64claims}";
            string signature = GetSignature(payload);

            return $"{payload}.{signature}";
        }

        public bool IsValidToken(string token)
        {
            if (!token.IsNullOrEmpty())
            {
                string[] tokenParameters = token.Split('.');

                return tokenParameters.Count() == 3 &&
                       tokenParameters[2].Equals(GetSignature($"{tokenParameters[0]}.{tokenParameters[1]}"), StringComparison.InvariantCultureIgnoreCase);
            }

            return false;
        }

        public T GetFromToken<T>(string token)
        {
            if (!token.IsNullOrEmpty())
            {
                string[] tokenParameters = token.Split('.');

                return tokenParameters.Count() == 3 ?
                       JsonConvert.DeserializeObject<T>(Encoding.UTF8.GetString(Convert.FromBase64String(tokenParameters[1]))) :
                       default(T);
            }

            return default(T);
        }

        private static string GetSignature(string payload)
        {
            return Convert.ToBase64String(new HMACSHA256(Encoding.UTF8.GetBytes(Constants.ApiVerificationHash)).ComputeHash(Encoding.UTF8.GetBytes(payload)));
        }

        public void GetAsync(string[] urls, object model)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", GetToken(model));
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                urls.ForEach(u => client.GetAsync(u).Wait());
            }
        }
    }
}