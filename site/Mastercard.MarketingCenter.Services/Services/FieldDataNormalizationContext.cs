﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class FieldDataNormalizationContext : IFieldDataNormalizationContext
    {
        private readonly Lazy<IEnumerable<IFieldDataRetrieval>> _fieldDataRetrievalStrategies;
        public FieldDataNormalizationContext(Lazy<IEnumerable<IFieldDataRetrieval>> fieldDataRetrievalStrategies)
        {
            _fieldDataRetrievalStrategies = fieldDataRetrievalStrategies;
        }

        private string SerializedAttribute(object entry)
        {
            return JsonConvert.SerializeObject(entry);
        }

        public IDictionary<string, object> Apply(IDictionary<string, object> originalData, IEnumerable<FieldDefinition> fieldDefinitions)
        {
            return originalData.ToDictionary(od => od.Key, od =>
             {
                 var value = od.Value;
                 if (od.Value != null && (od.Value is IEnumerable<object> || od.Value is string) || (od.Value is int))
                 {
                     var fieldDefinition = fieldDefinitions.FirstOrDefault(fd => fd.Name.Equals(od.Key));
                     if (fieldDefinition != null)
                     {
                         foreach (var dataRetrieval in _fieldDataRetrievalStrategies.Value)
                         {
                             if (dataRetrieval.ValidateFieldType(fieldDefinition))
                             {
                                 var innerValue = dataRetrieval.GetSerializedEntity(od, fieldDefinition, SerializedAttribute);
                                 value = !string.IsNullOrEmpty(innerValue) ? innerValue : od.Value;
                                 break;
                             }
                         }
                     }
                 }

                 return (object)value;
             });
        }
    }
}