﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;

namespace Mastercard.MarketingCenter.Services
{
    public class LoginService
    {
        private readonly IUserService _userService;
        private readonly IUnitOfWork _unitOfWork;

        public LoginService(IUserService userService, IUnitOfWork unitOfWork)
        {
            _userService = userService;
            _unitOfWork = unitOfWork;
        }

        public void TrackLogin(string username, string providerName)
        {
            using (var dataContext = new MasterCardPortalDataContext())
            {
                var providerUserName = $"{providerName.ToLower()}:{username}";
                var trackLogin = new LoginTracking
                {
                    UserName = providerUserName,
                    Date = DateTime.Now
                };

                var user = _userService.GetUserByUserName(providerUserName);
                if (user.Profile.GdprFollowUpStatusId != null)
                {
                    user.Profile.GdprFollowUpStatusId = null;
                    _userService.UpdateUser(user);
                    _unitOfWork.Commit();
                }

                dataContext.LoginTrackings.InsertOnSubmit(trackLogin);
                dataContext.SubmitChanges();
            }
        }
    }
}