﻿using Mastercard.MarketingCenter.Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Services
{
    public static class PromotionService
    {
        public static RetrievePromotionDetailsResult GetPromotionDetails(string promotionCode)
        {
            MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();
            return dataContext.RetrievePromotionDetails(promotionCode).SingleOrDefault();
        }

        public static PromotionCodeResponse ApplyPromotionCode(string promotionCode, int cartId, string issuerId)
        {
            PromotionCodeResponse response = new PromotionCodeResponse();
            RetrievePromotionDetailsResult promotion = GetPromotionDetails(promotionCode);
            if (promotion != null)
            {
                if (DateTime.Now >= promotion.StartDate)
                {
                    if (DateTime.Now < promotion.EndDate.AddDays(1))
                    {
                        decimal eligibleTotalPrice = 0;

                        List<RetrieveShoppingCartByUserIdResult> cartItems = ShoppingCartService.GetShoppingCartByUserId(DependencyResolver.Current.GetService<UserContext>().User.UserName);

                        if (!string.IsNullOrEmpty(promotion.Tags))
                        {
                            string[] promotionTags = promotion.Tags.Split(',');
                            foreach (RetrieveShoppingCartByUserIdResult cartItem in cartItems)
                            {
                                string[] tags = cartItem.Tags.Split(',');
                                if (tags.Intersect(promotionTags).Any())
                                {
                                    eligibleTotalPrice += ShoppingCartService.GetAssetPrice(cartItem.ContentItemID, cartItem.Quantity.Value, cartItem.ElectronicDelivery).TotalPrice;
                                }
                            }
                        }
                        else
                        {
                            foreach (RetrieveShoppingCartByUserIdResult cartItem in cartItems)
                            {
                                eligibleTotalPrice += ShoppingCartService.GetAssetPrice(cartItem.ContentItemID, cartItem.Quantity.Value, cartItem.ElectronicDelivery).TotalPrice;
                            }
                        }

                        if (eligibleTotalPrice > 0)
                        {
                            if (eligibleTotalPrice >= promotion.OrderMinimum)
                            {
                                MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();
                                if (promotion.SingleUseOnlyPerIssuer.HasValue && promotion.SingleUseOnlyPerIssuer.Value)
                                {
                                    if (!string.IsNullOrWhiteSpace(issuerId))
                                    {
                                        PromoCodeUsedByIssuer promotionsUsedByIssuer = GetPromotionsUsedByIssuer(promotion.PromoCodeID, issuerId);
                                        if (promotionsUsedByIssuer != null && promotionsUsedByIssuer.NumPromotionsUsed > 0)
                                        {
                                            response.ErrorMessage = "Sorry. This promotion code can only be used once by your financial institution and it has already been used.";
                                        }
                                    }
                                    else
                                    {
                                        response.ErrorMessage = "Unable to verify issuer.";
                                    }

                                    if (!string.IsNullOrEmpty(response.ErrorMessage))
                                    {
                                        return response;
                                    }
                                }

                                decimal discountAmount = 0;
                                if (promotion.DiscountType == "$ (dollar)")
                                {
                                    discountAmount = promotion.DiscountAmount;
                                }
                                else
                                {
                                    discountAmount = eligibleTotalPrice * (promotion.DiscountAmount / 100);
                                }

                                if (promotion.TotalFundingLimit.HasValue && promotion.TotalFundingLimit < promotion.TotalFundingCurrentAmount + discountAmount)
                                {
                                    response.ErrorMessage = "Sorry. This promotion code is no longer valid.";
                                    return response;
                                }

                                ShoppingCart cart = dataContext.ShoppingCarts.SingleOrDefault(c => c.CartID == cartId);
                                if (cart != null)
                                {
                                    cart.PromotionID = promotion.PromoCodeID;
                                    dataContext.SubmitChanges();
                                }

                                response.Description = promotion.Description;
                                response.DiscountAmount = discountAmount;
                                response.OrderTotal = eligibleTotalPrice;
                            }
                            else
                            {
                                response.ErrorMessage = "Shopping cart does not meet the minimum order.";
                            }
                        }
                        else
                        {
                            response.ErrorMessage = "Promotion code cannot be applied to the current cart.";
                        }
                    }
                    else
                    {
                        response.ErrorMessage = "Promotion code has expired.";
                    }
                }
                else
                {
                    response.ErrorMessage = "Promotion has not started.";
                }
            }
            else
            {
                response.ErrorMessage = "Invalid promotion code.";
            }

            return response;
        }

        public static void RemovePromotionCode(int cartId)
        {
            MasterCardPortalDataContext dataContext = new MasterCardPortalDataContext();
            ShoppingCart cart = dataContext.ShoppingCarts.SingleOrDefault(c => c.CartID == cartId);
            if (cart != null)
            {
                cart.PromotionID = null;
                dataContext.SubmitChanges();
            }
        }

        public static PromoCodeUsedByIssuer GetPromotionsUsedByIssuer(string promotionListItemId, string issuerId)
        {
            MasterCardPortalDataContext dc = new MasterCardPortalDataContext();
            return dc.PromoCodeUsedByIssuers.Where(p => p.PromoCodeID == promotionListItemId && p.IssuerID == issuerId).FirstOrDefault();
        }

        public static List<RetrievePromotionReportFiltersResult> GetPromotionReportFilters()
        {
            MasterCardPortalDataContext dc = new MasterCardPortalDataContext();
            return dc.RetrievePromotionReportFilters().ToList();
        }

        public static List<RetrievePromotionReportResult> GetPromotionOrders(string promotion, string issuerID)
        {
            MasterCardPortalDataContext dc = new MasterCardPortalDataContext();
            List<RetrievePromotionReportResult> orders = dc.RetrievePromotionReport(promotion).ToList();
            if (issuerID.Length > 0)
            {
                orders = orders.Where(o => o.IssuerID == issuerID).ToList();
            }

            return orders;
        }
    }

    public struct PromotionCodeResponse
    {
        public decimal DiscountAmount { get; set; }
        public string Description { get; set; }
        public string ErrorMessage { get; set; }
        public decimal OrderTotal { get; set; }
    }
}