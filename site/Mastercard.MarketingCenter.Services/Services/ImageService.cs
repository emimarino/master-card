﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Common;
using System;
using System.IO;
using System.Web;

namespace Mastercard.MarketingCenter.Services
{
    public class ImageService : BaseFileService, IImageService
    {
        private string _imagesPhysicalPath;
        private string _thumbnailsPhysicalPath;
        private string _downloadsPhysicalPath;
        private ISettingsService _settingsService;

        public ImageService(ISettingsService settingsService)
        {
            _settingsService = settingsService;
            _imagesPhysicalPath = MapPath(_settingsService.GetImagesFolder(), true);
            _thumbnailsPhysicalPath = MapPath(_settingsService.GetThumbnailsFolder(), true);
            _downloadsPhysicalPath = MapPath(_settingsService.GetDownloadImagesFolder(), true);
        }

        public Stream GetImage(string filename, int width, int height, bool crop, int jpegQuality, bool rootPath)
        {
            string physicalFile = GetResizedImagePath(filename, width, height, crop, jpegQuality, rootPath);
            if (physicalFile.IsNullOrEmpty())
            {
                Pulsus.LogManager.EventFactory.Create().Text("Image not found:" + physicalFile).Push();

                return null;
            }

            return File.OpenRead(physicalFile);
        }

        /// <summary>
        /// Resizes an image if no name is specified acopy of the resized image will be placed on the same folder as the parent with the "-resized" sufix
        /// </summary>
        /// <param name="filename">the path of the source image</param>
        /// <param name="width"> the final image's width</param>
        /// <param name="height">the final image's height</param>
        /// <param name="destinationPath">Nullable,the default will place on the same folder as the parent with the "-resized" sufix </param>
        /// <returns></returns>
        public string Resize(string filename, int width, int height, string destinationPath = null, bool crop = false, int jpegQuality = 100, bool rootPath = false)
        {
            var filePath = rootPath ? MapPath(filename.TrimStart('/')) : GetPath(filename);
            if (!File.Exists(filePath))
            {
                return null;
            }

            destinationPath = string.IsNullOrEmpty(destinationPath) ?
                              Path.Combine(Path.GetDirectoryName(filePath), $"{Path.GetFileNameWithoutExtension(filePath)}-resized.jpeg") :
                              rootPath ? MapPath(destinationPath.TrimStart('/'), true) : GetPath(destinationPath);

            var version = 0;
            while (File.Exists(destinationPath))
            {
                destinationPath = Path.Combine(Path.GetDirectoryName(destinationPath), $"{Path.GetFileNameWithoutExtension(destinationPath)}({version}).{Path.GetExtension(destinationPath)}");
                version += 1;
            }

            ResizeImage(height, width, crop, jpegQuality, filePath, destinationPath);

            return destinationPath;
        }

        private string GetResizedImagePath(string filename, int width, int height, bool crop, int jpegQuality, bool rootPath)
        {
            if (Path.GetExtension(filename).IsNullOrEmpty())
            {
                return null;
            }

            var hash = $"{filename}-width={width}-height={height}-crop={crop}-quality={jpegQuality}";
            var localFileName = $"{HttpUtility.UrlEncode(hash.GetMD5Hash())}{Path.GetExtension(filename)}";
            var destinationPath = Path.Combine(_thumbnailsPhysicalPath, localFileName);
            if (!File.Exists(destinationPath))
            {
                var filePath = rootPath ? MapPath(filename.TrimStart('/')) : GetPath(filename);
                if (!File.Exists(filePath))
                {
                    return null;
                }
                else if (width == 0 || height == 0)
                {
                    return filePath;
                }

                ResizeImage(height, width, crop, jpegQuality, filePath, destinationPath);
            }

            return destinationPath;
        }

        private void ResizeImage(int height, int width, bool crop, int jpegQuality, string originalFilePath, string destinationFilePath)
        {
            using (var fs = File.Open(originalFilePath, FileMode.Open))
            {
                var resized = new MemoryStream();
                try
                {
                    if (crop)
                    {
                        resized = (MemoryStream)ImageResizer.CropResizeImageFromStream(fs, width, height, jpegQuality, false);
                    }
                    else
                    {
                        resized = (MemoryStream)ImageResizer.ResizeImageFromStream(fs, width, height, jpegQuality, false);
                    }

                    using (var fileStream = File.OpenWrite(destinationFilePath))
                    {
                        resized.Position = 0;
                        resized.CopyTo(fileStream);
                        fileStream.Close();
                    }
                }
                finally
                {
                    resized.Close();
                    resized.Dispose();
                }
            }
        }

        private string GetPath(string fileName)
        {
            return Path.Combine(_imagesPhysicalPath, fileName.TrimStart('/'));
        }

        public string SaveImage(HttpPostedFileBase file)
        {
            var filename = $"{Guid.NewGuid()}{Path.GetExtension(file.FileName)}";
            var filePath = GetPath(Path.GetFileName(filename));
            if (!Directory.Exists(Path.GetDirectoryName(filePath)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            }

            file.SaveAs(filePath);

            return string.Format(filename, string.Empty);
        }

        public FileInfo GetFileInfo(string filename)
        {
            var filePath = GetPath(filename);
            if (!File.Exists(filePath))
            {
                return null;
            }

            return new FileInfo(filePath);
        }

        public Stream GetMappedImage(string imagePath, bool mapToImagesFolder = false)
        {
            return File.OpenRead(mapToImagesFolder ? GetPath(imagePath) : MapPath(imagePath));
        }

        public string GetImageFromUrl(string imageUrl, bool returnFullPath = true)
        {
            if (imageUrl.IsValidImage())
            {
                var filename = $"{HttpUtility.UrlEncode(imageUrl.GetMD5Hash())}{Path.GetExtension(imageUrl)}";
                var destinationPath = Path.Combine(_downloadsPhysicalPath, filename);
                if (File.Exists(destinationPath))
                {
                    return returnFullPath ? destinationPath : Path.Combine(_settingsService.GetDownloadImagesFolder(), filename);
                }

                DownloadFileFromUrl(imageUrl, destinationPath);
                if (File.Exists(destinationPath))
                {
                    return returnFullPath ? destinationPath : Path.Combine(_settingsService.GetDownloadImagesFolder(), filename);
                }
            }

            return null;
        }
    }
}