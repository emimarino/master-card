﻿using Mastercard.MarketingCenter.Services.Data;

namespace Mastercard.MarketingCenter.Services
{
    public class DomoServices
    {
        private static object domoLock = new object();

        public void UpdateData()
        {
            lock (domoLock)
            {
                using (MasterCardPortalDataContext dc = new MasterCardPortalDataContext())
                {
                    //Timeout incremented because could be a long running stored procedure
                    dc.CommandTimeout = 60 * 20;
                    dc.ExecuteCommand("EXEC [dbo].[ProcessDomoActivityApi]");
                }
            }
        }

        public void ArchiveData()
        {
            lock (domoLock)
            {
                using (MasterCardPortalDataContext dc = new MasterCardPortalDataContext())
                {
                    dc.ExecuteCommand("EXEC [Archive].[ArchiveDomoActivityApi_insert]");
                    dc.ExecuteCommand("EXEC [Archive].[DomoActivityApi_deleteOld]");
                }
            }
        }
    }
}