﻿using AutoMapper;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Entities.CustomSettings;
using Mastercard.MarketingCenter.Data.Extensions;
using Mastercard.MarketingCenter.Data.Interfaces;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Models;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services.Services
{
    public class EventLocationService : IEventLocationService
    {
        public readonly IEventLocationRepository _eventLocationRepository;
        private readonly ICachingService _cachingService;
        private readonly IMapper _mapper;

        public EventLocationService(IEventLocationRepository eventLocationRepository, ICachingService cachingService, IMapper mapper)
        {
            _eventLocationRepository = eventLocationRepository;
            _cachingService = cachingService;
            _mapper = mapper;
        }

        public void AddEventLocationToHierarchy(int parentEventLocationId, string title, string pricelessEventCity)
        {
            var hierarchyPositionId = _eventLocationRepository.GetEventLocationHierarchyPositionId(parentEventLocationId);

            var eventLocation = _eventLocationRepository.AddEventLocation(new EventLocation
            {
                Title = title,
                PricelessEventCity = pricelessEventCity,
                CreatedDate = DateTime.Now,
                ModifiedDate = DateTime.Now
            });

            var eventLocationHierarchy = new EventLocationPositionHierarchy
            {
                EventLocationId = eventLocation.EventLocationId,
                ParentPositionId = hierarchyPositionId,
            };

            _eventLocationRepository.AddEventLocationToHierarchy(eventLocationHierarchy);
        }

        public EventLocationTree GetEventLocationTree()
        {
            string cacheKey = _cachingService.GetEventLocationTreeModelCacheKey();
            if (_cachingService.Get<EventLocationTree>(cacheKey) == null)
            {
                _cachingService.Save(cacheKey, new EventLocationTree(GetEventLocations().ToList()).BuildTree());
            }

            return _cachingService.Get<EventLocationTree>(cacheKey);
        }

        public EventLocationTree GetEventLocationTree(IEnumerable<EventLocation> eventLocations)
        {
            string cacheKey = _cachingService.GetEventLocationTreeModelCacheKey();
            if (_cachingService.Get<EventLocationTree>(cacheKey) == null)
            {
                _cachingService.Save(cacheKey, new EventLocationTree(eventLocations.ToList()));
            }

            return _cachingService.Get<EventLocationTree>(cacheKey);
        }

        public EventLocationTree GetEventLocationTreeByContentId(string contentItemId)
        {
            var locationIds = _eventLocationRepository.GetRelatedEventLocationsByContentItemId(contentItemId).Select(e => e.EventLocationId);
            var tree = GetEventLocationTree().BuildTree();
            tree.RootNode.Children.ForEach(x => PopulateStatus(x, locationIds));

            return tree;
        }

        public EventLocationPositionHierarchy GetHierarchyByEventLocationId(int id)
        {
            return _eventLocationRepository.GetHierarchyByEventLocationId(id);
        }

        public IEnumerable<EventLocation> GetEventLocations()
        {
            return _eventLocationRepository.GetEventLocations();
        }

        public EventLocation GetEventLocationById(int id)
        {
            return _eventLocationRepository.GetEventLocationById(id);
        }

        public EventLocationPositionHierarchy UpdateEventLocationHierarchy(EventLocationPositionHierarchy eventLocationHierarchy)
        {
            return _eventLocationRepository.UpdateEventLocationHierarchy(eventLocationHierarchy);
        }

        public IEnumerable<EventLocation> GetEventLocationByContentItemId(string contentItemId)
        {
            return _eventLocationRepository.GetRelatedEventLocationsByContentItemId(contentItemId);
        }

        public void SetEventLocations(string itemId, IDictionary<string, object> data, FieldDefinition[] fieldDefinitions, ICmsDbConnection connection)
        {
            var locationFieldDef = fieldDefinitions.FirstOrDefault(o => o.FieldTypeCode == FieldType.Location);
            var settings = locationFieldDef?.GetSettings<EventLocationSettings>();

            if (locationFieldDef != null && settings != null)
            {
                var locations = Enumerable.Empty<string>();
                var value = data.GetData(locationFieldDef.Name);
                connection.Execute($"DELETE FROM {settings.AssociationTable} WHERE ContentItemId = '{itemId}'");

                if (!string.IsNullOrEmpty(Convert.ToString(value).Trim()))
                {
                    locations = value.ToString().Split(new char[] { ',' });
                    foreach (var locationId in locations)
                    {
                        connection.Execute($"INSERT INTO {settings.AssociationTable} VALUES ('{itemId}', '{locationId}')");
                    }
                }
            }
        }

        public void DeleteEventLocation(int id)
        {
            _eventLocationRepository.DeleteEventLocationById(id);
        }

        private void PopulateStatus(EventLocationTreeNode child, IEnumerable<int> locationIds)
        {
            if (locationIds.Contains(child.EventLocationId))
            {
                child.Status = EventLocationTreeNodeStatus.Checked;
            }

            child.Children.ForEach(c => PopulateStatus(c, locationIds));
        }

        public bool ExistPricelessEventCity(int eventLocationId, string pricelessEventCity)
        {
            return GetEventLocations().Any(el => el.EventLocationId != eventLocationId 
                                              && el.PricelessEventCity != null 
                                              && el.PricelessEventCity.Trim().Equals(pricelessEventCity.Trim(), StringComparison.InvariantCultureIgnoreCase));
        }

        public IEnumerable<EventLocationModel> GetEventLocationModel()
        {
            string cacheKey = _cachingService.GetEventLocationModelCacheKey();
            if (_cachingService.Get<IEnumerable<EventLocationModel>>(cacheKey) == null)
            {
                _cachingService.Save(cacheKey, _mapper.Map<IEnumerable<EventLocationModel>>(GetEventLocations()));
            }

            return _cachingService.Get<IEnumerable<EventLocationModel>>(cacheKey);
        }

        public void ReloadEventLocationModel()
        {
            _cachingService.Delete(_cachingService.GetEventLocationModelCacheKey());
            GetEventLocationModel();
        }
    }
}