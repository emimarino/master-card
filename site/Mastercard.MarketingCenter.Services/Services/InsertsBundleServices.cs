﻿using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Mastercard.MarketingCenter.Services
{
    public class InsertsBundleService
    {
        private readonly UserContext _userContext;
        private readonly IOrderService _orderService;
        private readonly IEmailService _emailService;

        public InsertsBundleService(UserContext userContext, IOrderService orderService, IEmailService emailService)
        {
            _userContext = userContext;
            _orderService = orderService;
            _emailService = emailService;
        }

        public List<InsertsBundle> GetBundles()
        {
            using (MasterCardPortalDataContext dc = new MasterCardPortalDataContext(ConfigurationManager.Environment.ConnectionStrings["MasterCardMarketingCenter"].ConnectionString))
            {
                return dc.InsertsBundles.Where(b => b.IsActive).OrderBy(b => b.Name).ToList();
            }
        }

        public void CreateOrders(string quarter)
        {
            MasterCardPortalDataContext dc = new MasterCardPortalDataContext(ConfigurationManager.Environment.ConnectionStrings["MasterCardMarketingCenter"].ConnectionString);

            List<InsertsBundleRegistration> registrationsToCreateOrdersFor = new List<InsertsBundleRegistration>();
            foreach (InsertsBundleRegistration registration in dc.InsertsBundleRegistrations.Where(r => !r.SkipOrder && !r.OrderNumber.HasValue))
            {
                if ((quarter == "1" && registration.InsertsBundle.Q1PromotionSKU == registration.PromotionSKU)
                    || (quarter == "2" && registration.InsertsBundle.Q2PromotionSKU == registration.PromotionSKU)
                    || (quarter == "3" && registration.InsertsBundle.Q3PromotionSKU == registration.PromotionSKU)
                    || (quarter == "4" && registration.InsertsBundle.Q4PromotionSKU == registration.PromotionSKU))
                {
                    registrationsToCreateOrdersFor.Add(registration);
                }
            }

            foreach (InsertsBundleRegistration registration in registrationsToCreateOrdersFor)
            {
                string promotionName = (quarter == "1") ? registration.InsertsBundle.Q1PromotionName :
                                        (quarter == "2") ? registration.InsertsBundle.Q2PromotionName :
                                        (quarter == "3") ? registration.InsertsBundle.Q3PromotionName :
                                        (quarter == "4") ? registration.InsertsBundle.Q4PromotionName : "";

                string promotionSKU = (quarter == "1") ? registration.InsertsBundle.Q1PromotionSKU :
                                        (quarter == "2") ? registration.InsertsBundle.Q2PromotionSKU :
                                        (quarter == "3") ? registration.InsertsBundle.Q3PromotionSKU :
                                        (quarter == "4") ? registration.InsertsBundle.Q4PromotionSKU : "";

                ShoppingCart tempCartForId = new ShoppingCart();
                tempCartForId.UserID = "XXX";
                tempCartForId.LastUpdated = DateTime.Now;
                dc.ShoppingCarts.InsertOnSubmit(tempCartForId);
                dc.SubmitChanges();

                int newOrderId = tempCartForId.CartID;
                dc.ShoppingCarts.DeleteOnSubmit(tempCartForId);
                dc.SubmitChanges();

                Order order = new Order();
                order.OrderNumber = newOrderId.ToString();
                order.OrderDate = DateTime.Now;
                order.OrderStatus = "Fulfillment";
                order.SpecialInstructions = registration.SpecialInstructions;
                order.RushOrder = false;
                order.UserID = _userContext.User.UserName;

                dc.Orders.InsertOnSubmit(order);
                dc.SubmitChanges();

                registration.OrderNumber = Convert.ToInt32(order.OrderNumber);

                ShippingInformation shippingInfo = new ShippingInformation();
                shippingInfo.ShoppingCartID = 0;
                shippingInfo.ContactName = registration.ContactName;
                shippingInfo.Phone = registration.ContactPhoneNumber;
                shippingInfo.Address = registration.ShippingAddress;

                dc.ShippingInformations.InsertOnSubmit(shippingInfo);
                dc.SubmitChanges();

                SubOrder subOrder = new SubOrder();
                subOrder.Completed = false;
                subOrder.Description = "";
                subOrder.OrderID = order.OrderID;
                subOrder.ShippingInformationID = shippingInfo.ShippingInformationID;

                dc.SubOrders.InsertOnSubmit(subOrder);
                dc.SubmitChanges();

                OrderItem orderItem = new OrderItem();
                orderItem.ElectronicDelivery = false;
                orderItem.VDP = false;
                orderItem.ItemID = "g000";
                orderItem.ItemName = string.Format("Mastercard Inserts Bundle Program: {0} Q{1}", promotionName, quarter);
                orderItem.SKU = promotionSKU;
                orderItem.OrderID = order.OrderID;
                orderItem.ShippingInformationID = shippingInfo.ShippingInformationID;
                orderItem.ItemQuantity = registration.NumUnits;
                orderItem.Modified = DateTime.Now;
                orderItem.SubOrderID = subOrder.SubOrderID;
                orderItem.Customizations = false;
                orderItem.ItemPrice = registration.OrderCost;

                dc.OrderItems.InsertOnSubmit(orderItem);
                dc.SubmitChanges();

                string shippingTemplate = String.Empty;
                if (!String.IsNullOrEmpty(registration.SpecialInstructions))
                {
                    StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath(ConfigurationManager.Environment["EmailSettings.Shipping"][0]["TemplateFile"]));
                    shippingTemplate = reader.ReadToEnd();
                    reader.Close();

                    shippingTemplate = shippingTemplate.Replace("[#SHIPPINGDETAILS]", " ");
                    shippingTemplate = shippingTemplate.Replace("[#SPECIALINSTRUCTIONS]", registration.SpecialInstructions);
                }

                var userId = dc.Users.FirstOrDefault(u => u.UserName == order.UserID)?.UserId ?? 0;
                _emailService.SendOrderConfirmationEmail(order.OrderNumber, DateTime.Now.ToString("MM/dd/yyyy"), GetItemDetailsForConfirmationEmail(order.OrderID), registration.CustomersEmail, registration.CustomersName, "", "", shippingTemplate, false, false, "", "", string.Empty, false, userId);
                _emailService.SendNewOrderEmail(order.OrderNumber, false, false, userId, order.RushOrder);
            }
        }

        public string GetItemDetailsForConfirmationEmail(int orderId)
        {
            StringBuilder sb = new StringBuilder();
            List<RetrieveOrderItemsResult> OrderItems = _orderService.GetOrderItems(orderId);

            List<TableItem> listitems = new List<TableItem>();
            string result = string.Empty;
            EmailText et;
            decimal totals = 0;

            if (OrderItems != null)
            {
                sb.Append(".................................................................");
                sb.Append(System.Environment.NewLine);

                TableItem t = new TableItem();

                t.Head = "SKU";
                t.Width = 12;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = "Title";
                t.Width = 22;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = "Quantity";
                t.Width = 17;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = "Price";
                t.Width = 17;
                t.Align = "Left";
                listitems.Add(t);

                et = new EmailText();
                result = et.GenerateText(listitems);

                sb.Append(result);
                sb.Append(".................................................................");
                sb.Append(System.Environment.NewLine);

                foreach (RetrieveOrderItemsResult r in OrderItems)
                {
                    listitems.Clear();

                    t = new TableItem();

                    t.Head = r.SKU.ToString() + " ";
                    t.Width = 12;
                    t.Align = "Left";
                    listitems.Add(t);

                    t = new TableItem();
                    t.Head = r.ItemName.ToString() + " ";
                    t.Width = 22;
                    t.Align = "Left";
                    listitems.Add(t);

                    t = new TableItem();
                    t.Head = r.ItemQuantity.ToString() + " ";
                    t.Width = 14;
                    t.Align = "Left";
                    listitems.Add(t);

                    t = new TableItem();
                    t.Head = FormatNumber(Convert.ToDecimal(r.ItemPrice.ToString()));
                    totals = totals + Convert.ToDecimal(r.ItemPrice.ToString());
                    t.Width = 12;
                    t.Align = "Right";
                    listitems.Add(t);

                    et = new EmailText();
                    result = et.GenerateText(listitems);
                    sb.Append(result);
                    //sb.Append(System.Environment.NewLine);
                }

                RetrievePromotionForOrderResult promotion = _orderService.GetPromotion(orderId);
                if (promotion != null)
                {
                    totals -= Convert.ToDecimal(promotion.PromotionAmount);

                    t = new TableItem();
                    listitems.Clear();

                    t.Head = promotion.PromotionDescription;
                    t.Width = 48;
                    t.Align = "Left";
                    listitems.Add(t);

                    t = new TableItem();
                    t.Head = String.Format("$-{0}", Convert.ToDecimal(promotion.PromotionAmount).ToString("N2"));
                    t.Width = 14;
                    t.Align = "Right";
                    listitems.Add(t);

                    et = new EmailText();
                    result = et.GenerateText(listitems);
                    sb.Append(result);
                }

                sb.Append(".................................................................");
                sb.Append(System.Environment.NewLine);

                t = new TableItem();
                listitems.Clear();

                t.Head = string.Empty;
                t.Width = 12;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = string.Empty;
                t.Width = 22;
                t.Align = "Left";
                listitems.Add(t);

                t = new TableItem();
                t.Head = "Total: ";
                t.Width = 14;
                t.Align = "Right";
                listitems.Add(t);

                t = new TableItem();
                t.Head = FormatNumber(Convert.ToDecimal(totals)) + "*";
                t.Width = 13;
                t.Align = "Right";
                listitems.Add(t);

                et = new EmailText();
                result = et.GenerateText(listitems);
                sb.Append(result);
                sb.Append(".................................................................");
            }

            return sb.ToString();
        }

        public string FormatNumber(decimal num)
        {
            System.Globalization.NumberFormatInfo nf = new System.Globalization.NumberFormatInfo();
            nf.CurrencySymbol = "$";
            nf.NumberDecimalDigits = 2;
            return num.ToString("C", nf);
        }

        public List<Enrollee> GetEnrollees()
        {
            List<Enrollee> enrollees = new List<Enrollee>();
            using (MasterCardPortalDataContext dc = new MasterCardPortalDataContext(ConfigurationManager.Environment.ConnectionStrings["MasterCardMarketingCenter"].ConnectionString))
            {
                List<Issuer> issuersRegistered = (from r in dc.InsertsBundleRegistrations select r.Issuer).Distinct().ToList();

                foreach (Issuer issuer in issuersRegistered)
                {
                    List<InsertsBundleRegistration> registrations = dc.InsertsBundleRegistrations.Where(r => r.Issuer == issuer).OrderBy(r => r.InsertsBundleRegistrationID).ToList();
                    if (registrations.Count == 4)
                    {
                        enrollees.Add(new Enrollee
                        {
                            Issuer = issuer,
                            CustomersName = registrations[0].CustomersName,
                            CustomersEmail = registrations[0].CustomersEmail,
                            Q1PromotionName = registrations[0].PromotionName,
                            Q2PromotionName = registrations[1].PromotionName,
                            Q3PromotionName = registrations[2].PromotionName,
                            Q4PromotionName = registrations[3].PromotionName,
                            Q1PromotionOrderId = (registrations[0].OrderNumber.HasValue) ? registrations[0].OrderNumber : new Nullable<int>(),
                            Q2PromotionOrderId = (registrations[1].OrderNumber.HasValue) ? registrations[1].OrderNumber : new Nullable<int>(),
                            Q3PromotionOrderId = (registrations[2].OrderNumber.HasValue) ? registrations[2].OrderNumber : new Nullable<int>(),
                            Q4PromotionOrderId = (registrations[3].OrderNumber.HasValue) ? registrations[3].OrderNumber : new Nullable<int>(),
                            Created = registrations[0].Created,
                            Q1NumUnits = registrations[0].NumUnits,
                            Q2NumUnits = registrations[1].NumUnits,
                            Q3NumUnits = registrations[2].NumUnits,
                            Q4NumUnits = registrations[3].NumUnits,
                            ShippingAddress = registrations[0].ShippingAddress,
                            ContactName = registrations[0].ContactName,
                            ContactPhoneNumber = registrations[0].ContactPhoneNumber,
                            OrderCost = registrations[0].OrderCost,
                            TotalEnrollmentCost = registrations[0].TotalEnrollmentCost,
                            AllowanceUnused = 0m
                        });
                    }
                }
            }
            return enrollees;
        }
    }

    public class Enrollee
    {
        public Issuer Issuer { get; set; }
        public string CustomersName { get; set; }
        public string CustomersEmail { get; set; }
        public string Q1PromotionName { get; set; }
        public string Q2PromotionName { get; set; }
        public string Q3PromotionName { get; set; }
        public string Q4PromotionName { get; set; }
        public int? Q1PromotionOrderId { get; set; }
        public int? Q2PromotionOrderId { get; set; }
        public int? Q3PromotionOrderId { get; set; }
        public int? Q4PromotionOrderId { get; set; }
        public DateTime Created { get; set; }
        public int Q1NumUnits { get; set; }
        public int Q2NumUnits { get; set; }
        public int Q3NumUnits { get; set; }
        public int Q4NumUnits { get; set; }
        public string ShippingAddress { get; set; }
        public string ContactName { get; set; }
        public string ContactPhoneNumber { get; set; }
        public decimal OrderCost { get; set; }
        public decimal TotalEnrollmentCost { get; set; }
        public decimal AllowanceUnused { get; set; }
    }
}