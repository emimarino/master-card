﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;

namespace Mastercard.MarketingCenter.Services
{
    public class TrackingService : ITrackingService
    {
        private static object siteTrackingLock = new object();
        private readonly ISiteTrackingRepository _siteTrackingRepository;
        public TrackingService(ISiteTrackingRepository siteTrackingRepository)
        {
            _siteTrackingRepository = siteTrackingRepository;
        }

        public long CreateVisitId(string urlReferrer)
        {
            var siteVisit = _siteTrackingRepository.Insert(new SiteVisit { UrlReferrer = urlReferrer });
            _siteTrackingRepository.Commit();

            return siteVisit.VisitID;
        }

        public void TrackPageVisit(string regionId, long visitID, string what, string who, string from, string how, string contentItemId = null, string pageGroup = null)
        {
            _siteTrackingRepository.Insert(new SiteTracking
            {
                VisitID = visitID,
                What = what,
                Who = who,
                When = DateTime.Now,
                From = from,
                How = how,
                ContentItemID = contentItemId,
                PageGroup = pageGroup,
                Region = regionId
            });
        }

        public void ProcessContentItemVisits()
        {
            _siteTrackingRepository.ProcessContentItemVisits();
        }

        public void ProcessSiteTracking()
        {
            lock (siteTrackingLock)
            {
                _siteTrackingRepository.ArchiveSiteTracking();
                _siteTrackingRepository.DeleteArchivedSiteTracking();
            }
        }
    }
}