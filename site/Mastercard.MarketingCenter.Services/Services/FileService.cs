﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using Newtonsoft.Json;
using Pulsus;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class FileService : BaseFileService, IFileService
    {
        private static readonly object reviewFilesLock = new object();
        private readonly IFileRepository _fileRepository;
        private List<ViewAllFiles> _allFiles;

        public FileService(IFileRepository fileRepository)
        {
            _fileRepository = fileRepository;
        }

        public void InsertDeletedOrphanFile(string folderKey, string fullname)
        {
            _fileRepository.InsertDeletedOrphanFile(
                new DeletedOrphanFile
                {
                    FolderKey = folderKey,
                    FileFullName = fullname,
                    FileDeletedDate = DateTime.Now
                });

            _fileRepository.Commit();
        }

        public void CleanUpReviewedFiles()
        {
            using (var context = new MarketingCenterDbContext())
            {
                using (var fileRepository = new FileRepository(context))
                {
                    fileRepository.CleanUpReviewedFiles();
                }
            }
        }

        public int ReviewFiles(string folder)
        {
            using (var context = new MarketingCenterDbContext())
            {
                using (var fileRepository = new FileRepository(context))
                {
                    lock (reviewFilesLock)
                    {
                        string path = GetPath(folder, false);
                        int filesReviewed = 0, filesMatched = 0, filesUnmatched = 0, filesReferenced = 0;
                        DirectoryInfo filesFolder = new DirectoryInfo(path);
                        foreach (FileInfo file in filesFolder.GetFiles("*", SearchOption.AllDirectories))
                        {
                            filesReviewed++;
                            string filename = filesFolder.FullName.Equals(file.Directory.FullName, StringComparison.InvariantCultureIgnoreCase) ? file.Name : $"{file.Directory.Name}/{file.Name}";
                            var dbFiles = GetAllFilesByFilename(filename);
                            if (dbFiles.Any())
                            {
                                filesMatched++;
                                foreach (var dbFile in dbFiles)
                                {
                                    filesReferenced++;
                                    fileRepository.InsertReviewedFile(GetReviewedFile(file, dbFile));
                                }
                            }
                            else
                            {
                                filesUnmatched++;
                                fileRepository.InsertReviewedFile(GetReviewedFile(file));
                            }
                        }

                        fileRepository.InsertFileReviewProcess(
                            new FileReviewProcess
                            {
                                ProcessDate = DateTime.Now,
                                Folder = folder,
                                FilesReviewed = filesReviewed,
                                FilesMatched = filesMatched,
                                FilesUnmatched = filesUnmatched,
                                FilesReferenced = filesReferenced
                            });

                        bool isSaved = false;
                        do
                        {
                            try
                            {
                                context.SaveChanges();
                                isSaved = true;
                            }
                            catch (DbUpdateException ex)
                            {
                                int detachedFailedEntries = 0;
                                string detachedFailedEntities = string.Empty;
                                foreach (var entry in ex.Entries) // get failed entries
                                {
                                    detachedFailedEntries++;
                                    detachedFailedEntities += $"{JsonConvert.SerializeObject(entry.Entity)}";
                                    entry.State = System.Data.Entity.EntityState.Detached; // change state to remove it from context
                                }

                                LogManager.EventFactory.Create()
                                                       .AddTags("FileReview")
                                                       .Text("Error executing FileReviewJob...")
                                                       .Level(LoggingEventLevel.Error)
                                                       .AddData("Exception Message", ex.Message)
                                                       .AddData("Exception InnerException", ex.InnerException)
                                                       .AddData("Exception Data", ex.Data)
                                                       .AddData("Exception StackTrace", ex.StackTrace)
                                                       .AddData("Detached Failed Entries", detachedFailedEntries)
                                                       .AddData("Detached Failed Entities", detachedFailedEntities)
                                                       .AddException(ex)
                                                       .Push();
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                        while (!isSaved);

                        return filesReviewed;
                    }
                }
            }
        }

        public IEnumerable<ViewAllFiles> GetAllFilesByFilename(string filename)
        {
            if (_allFiles == null)
            {
                _allFiles = _fileRepository.GetViewAllFiles().ToList();
            }

            return _allFiles.Where(f =>
                                       (
                                           !f.Column.Equals(MarketingCenterDbConstants.FileColumns.VideoHtmlContent, StringComparison.InvariantCultureIgnoreCase) &&
                                           f.Filename.EndsWith(filename, StringComparison.InvariantCultureIgnoreCase)
                                       ) || (
                                           f.Column.Equals(MarketingCenterDbConstants.FileColumns.VideoHtmlContent, StringComparison.InvariantCultureIgnoreCase) &&
                                           VideoFileService.ContainsHtmlVideo(f.Filename, filename)
                                       )
                                  );
        }

        private static ReviewedFile GetReviewedFile(FileInfo file, ViewAllFiles dbFile = null)
        {
            return new ReviewedFile
            {
                TableId = dbFile?.TableId,
                Table = dbFile?.Table,
                Column = dbFile?.Column,
                Filename = dbFile?.Filename,
                FullPath = file.FullName,
                Type = file.Extension,
                Size = decimal.Divide(file.Length, 1048576),
                UploadedDate = file.LastWriteTime
            };
        }

        public void MarkReviewedFiles()
        {
            foreach (var reviewedFile in _fileRepository.GetReviewedFiles().Where(rf => !(rf.TableId == null || rf.TableId.Trim().Length == 0)))
            {
                string filename = Path.Combine(Path.GetDirectoryName(reviewedFile.FullPath), ".mark");
                if (!File.Exists(filename))
                {
                    var file = File.Create(filename);
                    File.SetAttributes(filename, FileAttributes.Hidden);
                }
            }
        }

        public int MoveReviewedFiles(string folder, string newFolder, DateTime fileReviewMoveFolderDate)
        {
            lock (reviewFilesLock)
            {
                int filesMoved = 0;
                string path = GetPath(folder, false);
                string newPath = GetPath(newFolder, true);
                var parentPath = new FileInfo(path).Directory;

                foreach (var reviewedFile in _fileRepository.GetReviewedFiles().Where(rf => (rf.TableId == null || rf.TableId.Trim().Length == 0) && rf.UploadedDate < fileReviewMoveFolderDate && rf.FullPath.StartsWith(path)))
                {
                    if (File.Exists(reviewedFile.FullPath))
                    {
                        filesMoved++;
                        var reviewedFileParentPath = new FileInfo(reviewedFile.FullPath).Directory;
                        string newReviewedFilePath = reviewedFile.FullPath.Replace(parentPath.FullName, newPath);
                        var newReviewedFileParentPath = new FileInfo(newReviewedFilePath).Directory;

                        if (!Directory.Exists(newReviewedFileParentPath.FullName))
                        {
                            Directory.CreateDirectory(newReviewedFileParentPath.FullName);
                        }

                        File.Move(reviewedFile.FullPath, newReviewedFilePath);

                        if (reviewedFileParentPath.Exists && reviewedFileParentPath.GetFileSystemInfos().Length == 0 && !reviewedFileParentPath.FullName.Equals(path))
                        {
                            reviewedFileParentPath.Delete();
                        }
                    }
                }

                return filesMoved;
            }
        }

        public int CopyReviewedFiles(string folder, string newFolder, DateTime fileReviewCopyFolderStartDate, DateTime fileReviewCopyFolderEndDate)
        {
            lock (reviewFilesLock)
            {
                int filesCopied = 0;
                string path = GetPath(folder, false);
                string newPath = GetPath(newFolder, true);
                var parentPath = new FileInfo(path).Directory;

                foreach (var copyFile in _fileRepository.GetViewCopyFiles().Where(f => f.UploadedDate >= fileReviewCopyFolderStartDate && f.UploadedDate < fileReviewCopyFolderEndDate && f.FullPath.StartsWith(path)))
                {
                    if (File.Exists(copyFile.FullPath))
                    {
                        filesCopied++;
                        var copyFileParentPath = new FileInfo(copyFile.FullPath).Directory;
                        string newCopyFilePath = copyFile.FullPath.Replace(parentPath.FullName, newPath);
                        var newCopyFileParentPath = new FileInfo(newCopyFilePath).Directory;

                        if (!Directory.Exists(newCopyFileParentPath.FullName))
                        {
                            Directory.CreateDirectory(newCopyFileParentPath.FullName);
                        }

                        File.Copy(copyFile.FullPath, newCopyFilePath, true);
                    }
                }

                return filesCopied;
            }
        }

        private static string GetPath(string folder, bool createDirectoryIfNotExists = true)
        {
            string path = MapPath(folder, true, createDirectoryIfNotExists);
            if (path.IsNullOrWhiteSpace())
            {
                throw new InvalidOperationException($"Folder path not found: {folder}");
            }

            return path;
        }
    }
}