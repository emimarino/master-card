﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Routing;
using SlamConfig = Slam.Cms.Configuration;

namespace Mastercard.MarketingCenter.Services
{
    public class UrlService : IUrlService
    {
        private readonly ISettingsService _settingsService;

        public UrlService(ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        public string GetTrackingPixelUrl(string regionId, int mailDispatcherId)
        {
            return $"{RegionalizeService.GetBaseUrl(regionId).TrimEnd('/')}/{_settingsService.GetTrackMailDispatcherUrl().TrimStart('/')}?mailDispatcherId={mailDispatcherId}";
        }

        public static string GetPartialUrl(string originalUrl)
        {
            var partialUrl = originalUrl;
            Uri uri;
            if (Uri.TryCreate(partialUrl, UriKind.Absolute, out uri))
            {
                partialUrl = uri.PathAndQuery;
            }

            return Regex.Replace(partialUrl, @"^/portal/?", string.Empty);
        }

        private string GetFullUrlOnAdmin(string realtiveUrl)
        {
            if (string.IsNullOrEmpty(realtiveUrl))
            {
                return string.Empty;
            }

            return Path.Combine(SlamConfig.ConfigurationManager.Environment.AdminUrl, Regex.Replace(realtiveUrl, @"^/admin/?", string.Empty).TrimStart('/'));
        }

        public string GetFullUrlOnFrontEnd(string realtiveUrl, bool useSecondaryHost = false)
        {
            if (string.IsNullOrEmpty(realtiveUrl))
            {
                return string.Empty;
            }

            string frontEndUrl = useSecondaryHost ?
                                 SlamConfig.ConfigurationManager.Solution.Environments[_settingsService.GetSiteInApplicableRegionEnvironment()].FrontEndUrl :
                                 SlamConfig.ConfigurationManager.Environment.FrontEndUrl;

            return Path.Combine(frontEndUrl, Regex.Replace(realtiveUrl, @"^/portal/?", string.Empty).TrimStart('/'));
        }

        public string GetFullUrlOnFrontEnd(string realtiveUrl, string regionId)
        {
            if (string.IsNullOrEmpty(realtiveUrl) || string.IsNullOrEmpty(regionId))
            {
                return string.Empty;
            }

            return Path.Combine(RegionalizeService.GetBaseUrl(regionId).TrimEnd('/') + "/", _settingsService.GetPortalBaseUrl().TrimStart('/').TrimEnd('/') + "/", Regex.Replace(realtiveUrl, @"^/portal/?", string.Empty).TrimStart('/'));
        }

        public string GetFullUrlOnStatic(string realtiveUrl)
        {
            if (string.IsNullOrEmpty(realtiveUrl))
            {
                return string.Empty;
            }

            return Path.Combine(SlamConfig.ConfigurationManager.Environment.StaticUrl, Regex.Replace(realtiveUrl, @"^/static/?", string.Empty).TrimStart('/'));
        }

        public string GetFullUrlOnPricelessApi(string realtiveUrl)
        {
            if (string.IsNullOrEmpty(realtiveUrl))
            {
                return string.Empty;
            }

            return Path.Combine(_settingsService.GetPricelessApiClientServiceEndpoint(), realtiveUrl.TrimStart('/'));
        }

        public string GetAdminHomeURL()
        {
            return GetFullUrlOnAdmin("/");
        }

        public string GetFrontEndHomeURL()
        {
            return GetFullUrlOnFrontEnd("/");
        }

        public string GetContentEditURL(string contentItemId)
        {
            return GetFullUrlOnAdmin($"content/edit?id={contentItemId}");
        }

        public string GetBusinessOwnerHomeURL()
        {
            return GetFullUrlOnAdmin("BusinessOwner");
        }

        public string GetBusinessOwnerDetailsURL(string contentItemId)
        {
            return GetFullUrlOnAdmin(string.Format(_settingsService.ExpiredContentAssetDetailUrl, contentItemId));
        }

        public string GetExpiredContentExtendURL(string contentItemId)
        {
            return GetFullUrlOnAdmin(string.Format(_settingsService.ExpiredContentExtendURL, contentItemId));
        }

        public string GetExpiredContentDoNotExtendURL(string contentItemId)
        {
            return GetFullUrlOnAdmin(string.Format(_settingsService.ExpiredContentDoNotExtendURL, contentItemId));
        }

        public string GetUserProfileEditorHomeURL()
        {
            return GetFullUrlOnAdmin(_settingsService.GetUserProfileEditorHome());
        }

        public static void RedirectTo(ExceptionContext filterContext, string controllerName, string actionName = null)
        {
            filterContext.ExceptionHandled = true;
            filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary(new { controller = controllerName, action = actionName })
            );

            filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
        }

        public string SetFrontEndUrl(IEnumerable<KeyValuePair<string, object>> contentData, string frontEndUrl, string contentItemId)
        {
            if (string.IsNullOrEmpty(frontEndUrl))
            {
                return string.Empty;
            }

            var isUriRegex = new Regex(@".*(Uri|Url)", (RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase));
            frontEndUrl = frontEndUrl.Replace("{" + MarketingCenterDbConstants.FieldNames.ContentItemId + "}", contentItemId.GetAsLiveContentItemId());
            foreach (var data in contentData)
            {
                string value = Convert.ToString(data.Value);
                value = isUriRegex.IsMatch(data.Key) ? value.stringCleanPartialUrl() : value.ToURLFriendly();
                frontEndUrl = frontEndUrl.Replace("{" + data.Key + "}", value);
            }

            return GetPartialUrl(frontEndUrl);
        }

        public string GetResetPasswordURL(string key)
        {
            return GetFullUrlOnFrontEnd($"resetpassword/{key}");
        }

        public string GetChangePasswordURL()
        {
            return GetFullUrlOnFrontEnd("changepassword");
        }

        public string GetInvalidLinkURL()
        {
            return GetFullUrlOnFrontEnd("InvalidLink");
        }

        public string GetCreateAccountURL(string userName)
        {
            return GetFullUrlOnFrontEnd($"createaccount/{userName.RemoveMembershipProviderName()}");
        }

        public string GetOrderDetailsURL(string orderNumber)
        {
            return GetFullUrlOnFrontEnd($"orderdetails/{orderNumber}");
        }

        public string GetOrderDetailsReportURL(string orderNumber, string referrer = null)
        {
            return GetFullUrlOnFrontEnd($"WebFormsPages/OrderDetailNoChrome.aspx?orderid={orderNumber}{referrer.EnsureStartsWithValue("&")}");
        }

        public string GetProofFileURL(string file)
        {
            return GetFullUrlOnFrontEnd($"file/ProofFile?file={file}");
        }

        public string GetListEditURL(string listId, string itemId, string referrer = null)
        {
            return GetFullUrlOnAdmin($"List/Edit?listid={listId}&ID={itemId}{referrer.EnsureStartsWithValue("&")}");
        }

        public string GetPendingRegistrationFrontEndURL(string guid)
        {
            return GetFullUrlOnFrontEnd($"registration/pending/{guid}");
        }

        public string GetPendingRegistrationAdminURL(string guid, bool showrejected = false)
        {
            return GetFullUrlOnAdmin($"registration/pending/{guid}/{showrejected}");
        }

        public string GetProfileAccessAdminURL(string userName, string referrer = null)
        {
            return GetFullUrlOnAdmin($"profile/access?userName={userName}{referrer.EnsureStartsWithValue("&")}");
        }

        public string GetPendingRegistrationsReportURL()
        {
            return GetFullUrlOnAdmin("WebForm/Index/PendingRegistrationsReport?header=Pending%20Registrations%20Report&renderPageHeading=True");
        }

        public string GetUserOrderSearchURL(string comesFrom, string referrer = null)
        {
            return GetFullUrlOnAdmin($"WebForm/Index/UserOrderSearch?comesFrom={comesFrom}&renderPageHeading=True{referrer.EnsureStartsWithValue("&")}");
        }

        public string GetSearchURL(string searchQuery)
        {
            return $"/portal/search/{searchQuery}";
        }

        public string GetInsertsBundleReportURL()
        {
            return GetFullUrlOnAdmin("WebForm/Index/InsertsBundleReport?header=Inserts%20Bundle%20Report&renderPageHeading=True");
        }

        public string GetCustomizationQueueURL()
        {
            return GetFullUrlOnAdmin("WebForm/Index/CustomizationQueue?header=Customization%20Queue&renderPageHeading=True");
        }

        public string GetFulfillmentQueueURL()
        {
            return GetFullUrlOnAdmin("WebForm/Index/FulfillmentQueue?header=Fulfillment%20Queue&renderPageHeading=True");
        }

        public string GetManageIssuersURL()
        {
            return GetFullUrlOnAdmin("WebForm/Index/ManageIssuers?header=Manage%20Issuers&renderPageHeading=True");
        }

        public string GetPromotionReportURL()
        {
            return GetFullUrlOnAdmin("WebForm/Index/PromotionReport?header=Promotions%20Report&renderPageHeading=True");
        }

        public string GetOrderingActivityProgramsReportURL(string referrer = null)
        {
            return GetFullUrlOnFrontEnd($"report/orderingactivity{referrer.EnsureStartsWithValue("?")}");
        }

        public string GetOrderingActivityDetailProgramReportURL(string relatedProgramId, string referrer = null)
        {
            return GetFullUrlOnFrontEnd($"report/orderingactivitydetail/{relatedProgramId}{referrer.EnsureStartsWithValue("?")}");
        }

        public string GetOrderingActivityIssuersReportURL(string referrer = null)
        {
            return GetFullUrlOnFrontEnd($"report/orderingactivityissuer{referrer.EnsureStartsWithValue("?")}");
        }

        public string GetOrderingActivityDetailIssuerReportURL(string issuerId, string referrer = null)
        {
            return GetFullUrlOnFrontEnd($"report/orderingactivitydetailissuer/{issuerId}{referrer.EnsureStartsWithValue("?")}");
        }

        public string GetContentActivityReportURL()
        {
            return GetFullUrlOnFrontEnd("report/contentactivitydetail");
        }

        public string GetCheckEmailURL(string email, string region)
        {
            return GetFullUrlOnFrontEnd($"checkemail/{email}/{region.Trim()}");
        }

        public string GetInvalidateUserRoleByTokenURL(bool useSecondaryHost = false)
        {
            return GetFullUrlOnFrontEnd("Cache/InvalidateUserRoleByToken", useSecondaryHost);
        }

        public string GetReloadOfferModelByTokenURL(bool useSecondaryHost = false)
        {
            return GetFullUrlOnFrontEnd(GetRelativeReloadOfferModelByTokenURL(), useSecondaryHost);
        }

        public string GetRelativeReloadOfferModelByTokenURL()
        {
            return "Cache/ReloadOfferModelByToken";
        }

        public string GetReloadCategoryModelByTokenURL(bool useSecondaryHost = false)
        {
            return GetFullUrlOnFrontEnd(GetRelativeReloadCategoryModelByTokenURL(), useSecondaryHost);
        }

        public string GetRelativeReloadCategoryModelByTokenURL()
        {
            return "Cache/ReloadCategoryModelByToken";
        }

        public string GetReloadCardExclusivityModelByTokenURL(bool useSecondaryHost = false)
        {
            return GetFullUrlOnFrontEnd(GetRelativeReloadCardExclusivityModelByTokenURL(), useSecondaryHost);
        }

        public string GetRelativeReloadCardExclusivityModelByTokenURL()
        {
            return "Cache/ReloadCardExclusivityModelByToken";
        }

        public string GetReloadEventLocationModelByTokenURL(bool useSecondaryHost = false)
        {
            return GetFullUrlOnFrontEnd("Cache/ReloadEventLocationModelByToken", useSecondaryHost);
        }

        public string GetStaticContentCss()
        {
            return GetFullUrlOnStatic("content/css");
        }

        public string GetStaticHeaderCss()
        {
            return GetFullUrlOnStatic("header/css");
        }

        public string GetStaticHeaderJs()
        {
            return GetFullUrlOnStatic("header/js");
        }

        public string GetStaticTriggerMarketingCss()
        {
            return GetFullUrlOnStatic("triggermarketing/css");
        }

        public string GetStaticFavoritesCss()
        {
            return GetFullUrlOnStatic("favorites/css");
        }

        public string GetStaticCalendarCss()
        {
            return GetFullUrlOnStatic("calendar/css");
        }

        public string GetStaticDownloadCalendarCss()
        {
            return GetFullUrlOnStatic("downloadcalendar/css");
        }

        public string GetStaticHomeBannerCss()
        {
            return GetFullUrlOnStatic("homebanner/css");
        }

        public string GetStaticOfferCss()
        {
            return GetFullUrlOnStatic("offer/css");
        }

        public string GetStaticShoppingCartCss()
        {
            return GetFullUrlOnStatic("shoppingcart/css");
        }

        public string GetStaticSearchJs()
        {
            return GetFullUrlOnStatic("search/js");
        }

        public string GetStaticSearchCss()
        {
            return GetFullUrlOnStatic("search/css");
        }

        public string GetStaticOneTrustCss()
        {
            return GetFullUrlOnStatic("onetrust/css");
        }

        public string GetStaticCssURL(string css)
        {
            return GetFullUrlOnStatic($"styles/{css.TrimStart('/')}");
        }

        public string GetStaticBrokenImageURL(int width)
        {
            return GetStaticImageURL(_settingsService.GetBrokenImageName(width));
        }

        public string GetLoadingGifURL()
        {
            return GetStaticImageURL("loading.gif");
        }

        public string GetDefaultMostPopularImageURL(string imageFilename)
        {
            return GetStaticImageURL($"default-mostpopular-images/{imageFilename}.jpg");
        }

        public string GetDefaultMyFavoritesImageURL(string imageFilename)
        {
            return GetStaticImageURL($"default-myfavorites-images/{imageFilename}.jpg");
        }

        public string GetStaticImageURL(string image)
        {
            return GetFullUrlOnStatic($"images/{image.TrimStart('/')}");
        }

        public string GetStaticImageURL(string url, int? width = null, int? height = null, bool crop = false, int jpegQuality = 95, bool rootPath = false)
        {
            return GetFullUrlOnStatic($"GetImage?url={url}&width={width.GetValueOrDefault()}&height={height.GetValueOrDefault()}&crop={crop}&jpegQuality={jpegQuality}&rootPath={rootPath}");
        }

        public string GetStaticTagBrowserJs()
        {
            return GetFullUrlOnStatic("tag-browser/js");
        }

        public string GetStaticContentJs()
        {
            return GetFullUrlOnStatic("content/js");
        }

        public string GetStaticTriggerMarketingJs()
        {
            return GetFullUrlOnStatic("triggermarketing/js");
        }

        public string GetStaticFavoritesJs()
        {
            return GetFullUrlOnStatic("favorites/js");
        }

        public string GetStaticCalendarJs()
        {
            return GetFullUrlOnStatic("calendar/js");
        }

        public string GetStaticDownloadCalendarJs()
        {
            return GetFullUrlOnStatic("downloadcalendar/js");
        }

        public string GetStaticVueBundleJs()
        {
            return GetFullUrlOnStatic("vue/bundle/js");
        }

        public string GetStaticjQueryJs()
        {
            return GetFullUrlOnStatic("jquery/js");
        }

        public string GetStaticFooterJs()
        {
            return GetFullUrlOnStatic("footer/js");
        }

        public string GetStaticjQueryUiJs()
        {
            return GetFullUrlOnStatic("jqueryui/js");
        }

        public string GetStaticOfferJs()
        {
            return GetFullUrlOnStatic("offer/js");
        }

        public string GetStaticOfferDetailsJs()
        {
            return GetFullUrlOnStatic("offerdetails/js");
        }

        public string GetStaticCaptchaValidatorJs()
        {
            return GetFullUrlOnStatic("captchavalidator/js");
        }

        public string GetStaticShoppingCartJs()
        {
            return GetFullUrlOnStatic("shoppingcart/js");
        }

        public string GetStaticCommonFunctionJs()
        {
            return GetFullUrlOnStatic("commonfunction/js");
        }

        public string GetContentReportLink()
        {
            return "~/report/contentreport";
        }

        public string GetContentDetailReportLink(DateTime? creationStartDate, DateTime? creationEndDate, string region, string keyword)
        {
            return $"~/report/contentreportdetail?creationStartDate={creationStartDate}&creationEndDate={creationEndDate}&region={region}&keyword={keyword}";
        }

        public string GetUnauthorizedLink()
        {
            return GetFullUrlOnAdmin("Error/Unauthorized");
        }

        public string GetPricelessApiProductFeedUrl()
        {
            return GetFullUrlOnPricelessApi("product/getProductFeedUrl");
        }

        public string GetElectronicDeliveryDownloadUrl(string electronicDeliveryId, string regionId)
        {
            return $"{RegionalizeService.GetBaseUrl(regionId).TrimEnd('/')}/{_settingsService.GetElectronicDeliveryDownloadUrlFormat().TrimStart('/').Replace("{key}", electronicDeliveryId).Replace("{sku}", string.Empty).TrimEnd('/')}";
        }

        public string GetShareReportUrl(string shareKey)
        {
            return GetFullUrlOnFrontEnd($"report/share/{shareKey}");
        }
    }
}