﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class RegionService : IRegionService
    {
        private readonly RegionRepository _regionRepository;

        public RegionService(RegionRepository regionRepository)
        {
            _regionRepository = regionRepository;
        }

        public IEnumerable<Region> GetAll(bool includeGlobal = true)
        {
            return _regionRepository.GetAll(includeGlobal);
        }

        public Region GetById(string id)
        {
            return _regionRepository.GetById(id.Trim());
        }

        public IEnumerable<State> GetStates()
        {
            return _regionRepository.GetStates().OrderBy(s => s.StateName);
        }
    }
}