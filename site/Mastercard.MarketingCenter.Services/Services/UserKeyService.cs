﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;

namespace Mastercard.MarketingCenter.Services
{
    public class UserKeyService : IUserKeyService
    {
        private readonly IUserKeyRepository _userKeyRepository;
        public UserKeyService(IUserKeyRepository userKeyRepository)
        {
            _userKeyRepository = userKeyRepository;
        }

        public UserKey CreateUserKey(int userId)
        {
            return _userKeyRepository.CreateUserKey(userId);
        }

        public UserKey GetByKey(string key)
        {
            return _userKeyRepository.GetUserKeyByKey(key);
        }

        public UserKey GetByUserName(string userName)
        {
            return _userKeyRepository.GetUserKeyByUserName(userName);
        }

        public UserKey GetByUserNameAndStatus(string userName, int userKeyStatusId)
        {
            return _userKeyRepository.GetUserKeyByUserNameAndStatus(userName, userKeyStatusId);
        }

        public void ChangeUserKeyStatus(string key, int userKeyStatusId)
        {
            _userKeyRepository.ChangeUserKeyStatus(key, userKeyStatusId);
        }

        public void ChangeUserKeyStatusForRelatedKeys(string key, int userKeyStatusId)
        {
            _userKeyRepository.ChangeUserKeyStatusForRelatedKeys(key, userKeyStatusId);
        }
    }
}