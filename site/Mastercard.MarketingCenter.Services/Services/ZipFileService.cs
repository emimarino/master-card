﻿using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class ZipFileService : BaseFileService, IZipFileService
    {
        private readonly ISettingsService _settingsService;
        public ZipFileService(ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        /// <summary>
        /// Checks if the zip file contains a specific image, to then returns it.
        /// </summary>
        /// <param name="zipFileUrl">the path of the source zip file</param>
        /// <returns></returns>
        public string GetZipPreviewImage(string zipFileUrl)
        {
            if (zipFileUrl.EndsWith(".zip"))
            {
                zipFileUrl = zipFileUrl.TrimStart('/').TrimStart('\\');
                var filesPath = MapPath(_settingsService.GetFilesFolder(), true);
                var fullZipFilePath = Path.GetFullPath(Path.Combine(filesPath, zipFileUrl));

                if (File.Exists(fullZipFilePath))
                {
                    try
                    {
                        using (ZipArchive zipFile = ZipFile.OpenRead(fullZipFilePath))
                        {
                            var previewImage = zipFile.Entries.FirstOrDefault(entry => _settingsService.GetPreviewFilesZipImages().Contains(entry.Name.ToLowerInvariant()));
                            if (previewImage != null)
                            {
                                string destinationPath = Path.Combine(Path.GetDirectoryName(fullZipFilePath), previewImage.Name);
                                if (!File.Exists(destinationPath) && destinationPath.StartsWith(filesPath, StringComparison.Ordinal))
                                {
                                    previewImage.ExtractToFile(destinationPath);
                                }

                                return Path.Combine(Path.GetDirectoryName(zipFileUrl), previewImage.Name);
                            }
                        }
                    }
                    catch (InvalidDataException)
                    {
                        return null;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Checks if the zip file contains a csv file, to then returns it.
        /// </summary>
        /// <param name="zipFileUrl">the url of the source zip file</param>
        /// <returns></returns>
        public string GetZipCsvFileFromUrl(string zipFileUrl, string zipFileFolder = null)
        {
            if (zipFileUrl.EndsWith(".zip"))
            {
                zipFileFolder = Path.Combine(zipFileFolder ?? _settingsService.GetFilesFolder(), DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
                string zipFilePath = MapPath(zipFileFolder, true);
                string fullZipFilePath = Path.GetFullPath(Path.Combine(zipFilePath, $"DownloadFile.zip"));

                DownloadFileFromUrl(zipFileUrl, fullZipFilePath);

                if (File.Exists(fullZipFilePath))
                {
                    try
                    {
                        using (ZipArchive zipFile = ZipFile.OpenRead(fullZipFilePath))
                        {
                            var csvFile = zipFile.Entries.FirstOrDefault(entry => entry.Name.EndsWith(".csv"));
                            if (csvFile != null)
                            {
                                string destinationPath = Path.GetFullPath(Path.Combine(zipFilePath, csvFile.Name));
                                if (!File.Exists(destinationPath))
                                {
                                    csvFile.ExtractToFile(destinationPath);
                                }

                                return Path.Combine(zipFileFolder, csvFile.Name);
                            }
                        }
                    }
                    catch (InvalidDataException)
                    {
                        return null;
                    }
                }
            }

            return null;
        }
    }
}