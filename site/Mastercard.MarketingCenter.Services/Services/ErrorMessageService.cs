﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;

namespace Mastercard.MarketingCenter.Services.Services
{
    public class ErrorMessageService : IErrorMessageService
    {
        private readonly IErrorMessageRepository _errorMessageRepository;
        public ErrorMessageService(IErrorMessageRepository errorMessageRepository)
        {
            _errorMessageRepository = errorMessageRepository;
        }

        public ErrorMessage GetErrorMessage(int listId, string eventName)
        {
            return _errorMessageRepository.GetById(listId, eventName);
        }
    }
}