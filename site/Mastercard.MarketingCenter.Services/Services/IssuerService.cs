﻿using Mastercard.MarketingCenter.Common.Services;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class IssuerService : IIssuerService
    {
        private readonly IdGeneratorService _idGeneratorService;
        private readonly IIssuerRepository _issuerRepository;
        private readonly IUserService _userService;
        private readonly ProcessorService _processorService;
        private readonly ISegmentationService _segmentationService;
        private readonly IUnitOfWork _unitOfWork;
        private IEnumerable<string> _domainBlackList;

        public IssuerService(IdGeneratorService idGeneratorService, IIssuerRepository issuerRepository, IUserService userService, ProcessorService processorService, ISegmentationService segmentationService, IUnitOfWork unitOfWork)
        {
            _idGeneratorService = idGeneratorService;
            _issuerRepository = issuerRepository;
            _userService = userService;
            _processorService = processorService;
            _segmentationService = segmentationService;
            _unitOfWork = unitOfWork;
        }

        public Issuer GetIssuerById(string issuerId)
        {
            return _issuerRepository.GetIssuerById(issuerId);
        }

        public Issuer GetIssuerByUserId(int userId)
        {
            return _userService.GetUser(userId)?.Issuer;
        }

        public Issuer GetIssuerByCid(string cid)
        {
            return _issuerRepository.GetIssuerByCid(cid);
        }

        public Issuer GetIssuerByDomain(string domain, string regionId)
        {
            return GetIssuersByRegion(regionId).FirstOrDefault(i => i.DomainName.ToLower().Split(',').Contains(domain.ToLower()));
        }

        public IEnumerable<RetrieveIssuersByProcessorDTO> GetIssuersByProcessor(string processorId, string regionId)
        {
            return _issuerRepository.GetIssuersByProcessor(processorId, regionId).ToList();
        }

        public IEnumerable<Issuer> GetAllIssuers()
        {
            return _issuerRepository.GetAll();
        }

        public IEnumerable<Issuer> GetIssuersByRegion(string regionId)
        {
            return _issuerRepository.GetAllByRegionId(regionId);
        }

        public IEnumerable<Issuer> GetIssuersBySearchRequest(string regionId, string processorId, string searchText, string showAllValue)
        {
            var baseQuery = _issuerRepository.GetAllByRegionId(regionId);

            if (!string.IsNullOrWhiteSpace(processorId) && processorId != showAllValue && string.IsNullOrEmpty(searchText))
            {
                return baseQuery.Where(i => i.Processors.Any(p => p.ProcessorId == processorId));
            }

            if (!string.IsNullOrEmpty(searchText))
            {
                baseQuery = baseQuery.Where(i => i.Title.Contains(searchText)
                        || i.DomainName.Contains(searchText)
                        || i.Processors.Any(p => p.Title.Contains(searchText)
                        || i.IssuerId.Contains(searchText)));

                if (!processorId.Equals(showAllValue))
                    return baseQuery.Where(i => i.Processors.Any(p => p.ProcessorId.Equals(processorId)));
            }

            return baseQuery;
        }

        public IEnumerable<Issuer> GetMastercardIssuersByRegion(string regionId)
        {
            return _issuerRepository.GetAll().Where(i => i.IsMastercardIssuer && i.RegionId == regionId).ToList();
        }

        public IEnumerable<Issuer> GetIssuersWithSegmentation(string regionId)
        {
            return _issuerRepository.GetAll()
                                    .Where(i => i.RegionId == regionId)
                                    .Include(i => i.IssuerSegmentations.Select(s => s.Segmentation))
                                    .ToList();
        }

        public string CreateIssuer(
            string title,
            string domains,
            string ica,
            string[] segmentationIds,
            string processorId,
            string regionId,
            int creatingUserId,
            string cid = null)
        {
            var issuerId = _idGeneratorService.GetNewUniqueId();
            var issuer = _issuerRepository.Insert(new Issuer
            {
                IssuerId = issuerId,
                Title = title,
                DomainName = domains,
                BillingId = string.IsNullOrEmpty(ica) ? null : ica,
                DateCreated = DateTime.Now,
                ModifiedDate = DateTime.Now,
                RegionId = regionId,
                CreatedByUserId = creatingUserId,
                ModifiedByUserId = creatingUserId,
                Processors = new HashSet<Processor>()
            });

            if (!string.IsNullOrEmpty(cid))
            {
                issuer.Cid = cid;
                issuer.MatchDate = DateTime.Now;
                issuer.MatchConfirmed = true;
                issuer.MatchedByUserId = creatingUserId;
            }

            var processor = _processorService.GetProcessorById(processorId);
            if (processor != null)
            {
                issuer.Processors.Add(processor);
            }

            var segmentations = _segmentationService.GetSegmentationsByIds(segmentationIds);
            if (segmentations != null)
            {
                _segmentationService.InsertIssuerSegmentations(issuerId, segmentations.Select(s => s.SegmentationId));
            }

            _unitOfWork.Commit();

            return issuerId;
        }

        public void AddIssuerDomain(string issuerId, string domain)
        {
            var issuer = _issuerRepository.GetIssuerById(issuerId);
            issuer.DomainName += "," + domain;
            issuer.ModifiedDate = DateTime.Now;

            _unitOfWork.Commit();
        }

        public void SetIssuerHasOrdered(string issuerId)
        {
            var issuer = _issuerRepository.GetIssuerById(issuerId);
            issuer.HasOrdered = true;

            _unitOfWork.Commit();
        }

        public IssuerData GetIssuerDetail(string issuerId)
        {
            var issuer = _issuerRepository.GetIssuerById(issuerId);
            var segment = issuer.IssuerSegmentations != null ? string.Join(", ", issuer.IssuerSegmentations.Select(s => s.Segmentation.SegmentationName)) : null;
            var processor = issuer.Processors?.FirstOrDefault() != null ? issuer.Processors.First().Title : string.Empty;

            return new IssuerData
            {
                IssuerId = issuer.IssuerId,
                AudienceSegments = segment,
                Title = issuer.Title,
                Processor = processor,
                Cid = issuer.Cid,
                DateCreated = issuer.DateCreated.GetValueOrDefault(),
                MatchConfirmed = issuer.MatchConfirmed
            };
        }

        public bool IsMastercardIssuer(string issuerId)
        {
            return string.IsNullOrWhiteSpace(issuerId) ? false : _issuerRepository.GetIssuerById(issuerId)?.IsMastercardIssuer ?? false;
        }

        public bool VerifyDomainIsValid(string domain)
        {
            if (_domainBlackList == null || _domainBlackList.Any())
            {
                _domainBlackList = _issuerRepository.GetDomainBlackLists().Select(x => x.Domain).ToList();
            }

            return !_domainBlackList.Any(d => d.Equals(domain, StringComparison.OrdinalIgnoreCase));
        }

        public void DeleteIssuer(Issuer issuer)
        {
            _issuerRepository.DeleteIssuer(issuer);
        }

        public void DeleteAllMasterICAs()
        {
            foreach (var ica in _issuerRepository.GetAllMasterICAs())
            {
                _issuerRepository.DeleteMasterICA(ica);
            }

            _unitOfWork.Commit();
        }

        public void InsertMasterICAs(List<MasterICA> masterIcas)
        {
            foreach (var ica in masterIcas)
            {
                _issuerRepository.Insert(ica);
            }

            _unitOfWork.Commit();
        }

        public MasterICA GetMasterICAByIca(int ica)
        {
            return _issuerRepository.GetMasterICAByIca(ica);
        }

        public IEnumerable<MasterICA> GetMasterICAsByBillingId(string billingId)
        {
            if (int.TryParse(billingId, out int id))
            {
                return _issuerRepository.GetMasterICAByMasterIca(id);
            }

            return null;
        }
    }
}