﻿using Mastercard.MarketingCenter.Common.Filters;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Models;
using Pulsus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Services
{
    public class PardotIntegrationService : IPardotIntegrationService
    {
        private readonly ISettingsService _settingsService;

        public PardotIntegrationService(ISettingsService settingsService)
        {
            _settingsService = settingsService;
        }

        public PardotIntegrationModel GetPardotIntegrationModel(
            string regionId,
            string firstName,
            string lastName,
            string title,
            string issuerName,
            string processor,
            string phone,
            string mobile,
            string fax,
            string email,
            string country,
            string language,
            string address1,
            string address2,
            string address3,
            string city,
            string state,
            string zipCode,
            string acceptedConsents,
            bool acceptedEmailSubscriptions
        )
        {
            return new PardotIntegrationModel
            {
                RegionId = regionId,
                FirstName = firstName,
                LastName = lastName,
                Title = title,
                IssuerName = issuerName,
                Processor = processor,
                Phone = phone,
                Mobile = mobile,
                Fax = fax,
                Email = email,
                Country = country,
                Language = language,
                Address1 = address1,
                Address2 = address2,
                Address3 = address3,
                City = city,
                State = state,
                ZipCode = zipCode,
                AcceptedConsents = acceptedConsents,
                AcceptedEmailSubscriptions = acceptedEmailSubscriptions
            };
        }

        public void SubmitPardotForm(PardotIntegrationModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Email))
            {
                LogManager.EventFactory.Create()
                                       .Text("Error sending Pardot Registration - Email cannot be empty")
                                       .Level(LoggingEventLevel.Error)
                                       .AddTags("pardot")
                                       .AddData("Pardot Integration Model", model)
                                       .Push();
            }

            var content = new Dictionary<string, string>();
            model.GetType().GetProperties().Where(p => !Attribute.IsDefined(p, typeof(IgnoreAttribute))).ForEach(p =>
            {
                content.Add(p.Name, p.GetValue(model).ToString());
            });

            using (var client = new HttpClient())
            {
                string pardotIntegrationUrl = _settingsService.GetPardotIntegrationUrl(model.RegionId);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                var task = client.PostAsync(pardotIntegrationUrl, new FormUrlEncodedContent(content))
                                 .ContinueWith((response) =>
                                 {
                                     if (response.Status != System.Threading.Tasks.TaskStatus.RanToCompletion || !response.Result.IsSuccessStatusCode)
                                     {
                                         LogManager.EventFactory.Create()
                                                                .Text("Error sending Pardot Registration...")
                                                                .Level(LoggingEventLevel.Error)
                                                                .AddTags("pardot")
                                                                .AddData("Pardot Integration Url", pardotIntegrationUrl)
                                                                .AddData("Content Data", content)
                                                                .AddData("Response Status", response.Status.ToString())
                                                                .AddData("Pardot Status Code",
                                                                         response.Status == System.Threading.Tasks.TaskStatus.RanToCompletion ?
                                                                         response.Result.StatusCode.ToString() : HttpStatusCode.InternalServerError.ToString())
                                                                .Push();
                                     }
                                     else
                                     {
                                         LogManager.EventFactory.Create()
                                                                .Text("Successfully Sent Pardot Registration...")
                                                                .Level(LoggingEventLevel.Trace)
                                                                .AddTags("pardot")
                                                                .AddData("Pardot Integration Url", pardotIntegrationUrl)
                                                                .AddData("Content Data", content)
                                                                .Push();
                                     }
                                 });
                task.Wait();
            }
        }
    }
}