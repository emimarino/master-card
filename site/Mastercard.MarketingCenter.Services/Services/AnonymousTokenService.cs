﻿using Mastercard.MarketingCenter.Common.Models;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;

namespace Mastercard.MarketingCenter.Services
{
    public class AnonymousTokenService : IAnonymousTokenService
    {
        private readonly AnonymousTokenRepository _anonymousTokenRepository;
        public AnonymousTokenService(AnonymousTokenRepository anonymousTokenRepository)
        {
            _anonymousTokenRepository = anonymousTokenRepository;
        }

        public AnonymousUserProxy GetAnonymousTokenData(Guid token)
        {
            return _anonymousTokenRepository.GetAnonymousTokenData(token);
        }

        public string SetAnonymousTokenData(AnonymousUserProxy userProxy, string region)
        {
            return $"{RegionalizeService.GetBaseUrl(region).TrimEnd('/')}/Profile/Unsubscribe/{_anonymousTokenRepository.SetAnonymousTokenData(userProxy.ClaimsTo, userProxy.UserId, userProxy.RegionId, userProxy.OptionalParameter)}";
        }
    }
}