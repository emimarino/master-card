﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Interfaces.GDP;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class ConsentManagementService : IConsentManagementService
    {
        private readonly IConsentManagementRepository _consentManagementRepository;
        private readonly IUserConsentRepository _userConsentRepository;

        public ConsentManagementService(IConsentManagementRepository consentManagementRepository, IUserConsentRepository userConsentRepository)
        {
            _userConsentRepository = userConsentRepository;
            _consentManagementRepository = consentManagementRepository;
        }

        public ConsentFileData GetLatestConsentLanguageText(string useCode, string locale, string functionCode)
        {
            return _consentManagementRepository.GetLatestConsentLanguageText(useCode, locale, functionCode);
        }

        public ConsentFileData GetLatestLegalConsentData(string useCode, string locale, string functionCode = null)
        {
            return _consentManagementRepository.GetLatestLegalConsentData(useCode, locale);
        }

        public void RetrieveConsentFilesAndVersions()
        {
            _consentManagementRepository.RetrieveExternalFiles();
        }

        public IEnumerable<UserConsent> GetUserConsents(string email, int limit = 0, int offset = 0)
        {
            return ((limit == 0 && offset == 0) ? _userConsentRepository.GetByEmail(email) : _userConsentRepository.GetByEmail(email).Skip(offset).Take(limit));
        }

        public void InsertUserConsent(UserConsent userConsent)
        {
            _userConsentRepository.Insert(userConsent);
        }
        public void InsertUserConsent(IEnumerable<UserConsent> userConsents)
        {
            _userConsentRepository.Insert(userConsents);

        }
        public void InsertUserConsentByEmail(string email, IEnumerable<int> ConsentFileIds)
        {
            var userId = _userConsentRepository.GetUserIdByEmail(email);
            var userConsents = ConsentFileIds.Select(ac => new UserConsent() { ConsentDate = DateTime.Now, ConsentFileDataId = ac, UserId = userId });
            _userConsentRepository.Insert(userConsents);

        }
        public IEnumerable<ConsentFileData> GetExpiredConsentForUser(string userName, string locale)
        {
            return _userConsentRepository.GetExpiredConsentForUser(userName, locale);
        }

        public bool IsAnyUserConsentExpired(string username, string locale)
        {
            return GetExpiredConsentForUser(username, locale).Any(c => c.IsExpirable);
        }

        public IEnumerable<ConsentFileData> GetLatestConsents(string locale, string functionCode)
        {
            return _userConsentRepository.GetLatestConsents(locale, functionCode);
        }
    }
}
