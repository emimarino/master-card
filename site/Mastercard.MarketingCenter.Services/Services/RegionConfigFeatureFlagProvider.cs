﻿using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Configuration;
using System;

namespace Mastercard.MarketingCenter.Services.Services
{
    public class RegionConfigManagerFeatureFlagProvider : IFeatureFlagProvider
    {
        RegionConfigManager _regionConfigManager;
        public RegionConfigManagerFeatureFlagProvider(RegionConfigManager regionConfigManager)
        {
            _regionConfigManager = regionConfigManager;

        }

        public bool? GetSetting(string featureKey, string regionKey)
        {
            if (!string.IsNullOrEmpty(regionKey))
            {
                bool isIt;
                if (bool.TryParse(_regionConfigManager.GetSetting(featureKey, regionKey), out isIt))
                {
                    return isIt;
                }
                return null;
            }
            else
            {
                throw new NotSupportedException("A Region Must Be Defined.");
            }
        }
    }
}
