﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Microsoft.Ajax.Utilities;
using Slam.Cms.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Services
{
    public delegate IDictionary<string, List<SearchAutoCompleteItem>> AsyncSearchAutoCompleteItemsMethodCaller(int callDuration, out int threadId);

    public static class SearchAutoCompleteService
    {
        private const string searchAutoCompleteCacheKey = "SearchAutoCompleteItems";
        private static ISearchRepository _searchRepository { get { return DependencyResolver.Current.GetService<ISearchRepository>(); } }
        public static IDictionary<string, List<SearchAutoCompleteItem>> GetSearchAutoCompleteItems(int callDuration, out int threadId)
        {
            var searchAutoCompleteItemsFinal = new Dictionary<string, List<SearchAutoCompleteItem>>();

            try
            {
                if (HttpRuntime.Cache[searchAutoCompleteCacheKey] == null)
                {
                    searchAutoCompleteItemsFinal = new Dictionary<string, List<SearchAutoCompleteItem>>(_searchRepository.GetSearchAutoCompleteItems());

                    HttpRuntime.Cache.Insert(
                        searchAutoCompleteCacheKey,
                        searchAutoCompleteItemsFinal,
                        null,
                        DateTime.Now.AddMinutes(30),
                        Cache.NoSlidingExpiration,
                        CacheItemPriority.Default,
                        new CacheItemRemovedCallback(CacheItemRemoved));
                }
                else
                {
                    searchAutoCompleteItemsFinal = HttpRuntime.Cache[searchAutoCompleteCacheKey] as Dictionary<string, List<SearchAutoCompleteItem>>;
                }
            }
            catch (Exception ex)
            {
                if (HttpContext.Current != null)
                {
                    HttpContext.Current.Response.Write(ex.ToString());
                }
                else
                {
                    throw ex;
                }
            }
            finally
            {
                Thread.Sleep(callDuration);
                threadId = Thread.CurrentThread.ManagedThreadId;
            }

            return searchAutoCompleteItemsFinal;
        }

        public static void CallbackMethod(IAsyncResult ar)
        {
        }

        private static void CacheItemRemoved(string key, object val, CacheItemRemovedReason reason)
        {
            int threadID = 0;
            GetSearchAutoCompleteItems(3000, out threadID);
        }

        public static List<SearchAutoCompleteResultItem> GetSearchAutoCompleteResults(string searchTerm, IEnumerable<string> currentSegmentationIds, string currentSpecialAudience, string RegionId, string Language)
        {
            var searchAutoCompleteItemsToReturn = new List<SearchAutoCompleteResultItem>();

            if (HttpContext.Current.Cache[searchAutoCompleteCacheKey] != null)
            {
                var fullDictionary = HttpContext.Current.Cache[searchAutoCompleteCacheKey] as Dictionary<string, List<SearchAutoCompleteItem>>;
                if (!fullDictionary.TryGetValue(RegionId?.Trim(), out List<SearchAutoCompleteItem> searchAutoCompleteItemsFinal))
                {
                    searchAutoCompleteItemsFinal = new List<SearchAutoCompleteItem>();
                }

                //macro-trends
                searchAutoCompleteItemsToReturn =
                    (from i in searchAutoCompleteItemsFinal
                     where i.Title.ToLower().Contains(searchTerm.ToLower())
                     && (string.IsNullOrEmpty(Language) || string.IsNullOrEmpty(i.Language) || Language.Trim().Equals(i.Language, StringComparison.OrdinalIgnoreCase))
                     && ((!currentSegmentationIds.IsNull() && currentSegmentationIds.Any(s => s.Equals(RegionConfigManager.DefaultSegmentation, StringComparison.InvariantCultureIgnoreCase))) || !i.SegmentationIds.Any() || (!currentSegmentationIds.IsNull() && i.SegmentationIds.Any(s => currentSegmentationIds.Contains(s))))
                     && (
                            string.IsNullOrEmpty(i.RestrictToSpecialAudience) ||
                            (!string.IsNullOrEmpty(currentSpecialAudience) &&
                                (!i.RestrictToSpecialAudience.IsNullOrEmpty() && currentSpecialAudience.ToUpper().Contains(i.RestrictToSpecialAudience.ToUpper()) ||
                                 (!i.RestrictToSpecialAudience.IsNullOrEmpty() && i.RestrictToSpecialAudience.ToUpper().Contains(currentSpecialAudience.ToUpper())))
                            )
                        )
                     orderby i.SortOrder, i.Rank descending, i.Title
                     select new SearchAutoCompleteResultItem
                     {
                         id = "All",
                         value = i.Title,
                         info = "/portal/search/" + i.SearchParameter
                     })
                     .DistinctBy(i => i.value)
                     .ToList();
            }

            return searchAutoCompleteItemsToReturn;
        }

        public static void ClearAutocompleteCache()
        {
            if (HttpContext.Current.Cache[searchAutoCompleteCacheKey] != null)
            {
                Thread.Sleep(5000);
                HttpContext.Current.Cache.Remove(searchAutoCompleteCacheKey);
                CacheItemRemoved(searchAutoCompleteCacheKey, null, CacheItemRemovedReason.Expired);
            }
        }
    }
}