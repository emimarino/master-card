﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Services.Data;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ImageUrls = Mastercard.MarketingCenter.Common.Infrastructure.Constants.ImageUrls;
using SlamConfig = Slam.Cms.Configuration;

namespace Mastercard.MarketingCenter.Services
{
    public class EmailService : IEmailService
    {
        private readonly ICachingService _cachingService;
        private readonly IRegionalizeService _regionalizeService;
        private readonly IMailDispatcherService _mailDispatcherService;
        private readonly ISettingsService _settingsService;
        private readonly IUrlService _urlService;
        private readonly IUserService _userService;

        public EmailService(
            ICachingService cachingService,
            IRegionalizeService regionalizeService,
            IMailDispatcherService mailDispatcherService,
            ISettingsService settingsService,
            IUrlService urlService,
            IUserService userService)
        {
            _cachingService = cachingService;
            _regionalizeService = regionalizeService;
            _mailDispatcherService = mailDispatcherService;
            _settingsService = settingsService;
            _urlService = urlService;
            _userService = userService;
        }

        private string ApplyLayout(string emailBody, string bodyTemplateName, string region = null, string language = null)
        {
            var updateEmailBody = emailBody;
            var layoutKey = $"Email_Layout_{bodyTemplateName}_{region}_{language}";
            var layoutBody = _cachingService.Get<string>(layoutKey);
            var patternToGetByName = new Regex(@"\[\s*#\s*layout\s*:\s*(?<layoutName>.+)\s*\]", RegexOptions.Compiled);

            if (string.IsNullOrEmpty(layoutBody))
            {
                var match = patternToGetByName.Match(updateEmailBody);
                if (match.Length < 1)
                {
                    return updateEmailBody;
                }
                var layoutName = match.Groups["layoutName"].Value;
                var maxCacheTimeout = new TimeSpan(1, 0, 0);

                layoutBody = GetTemplate(layoutName, region, language);
                _cachingService.Save(layoutKey, layoutBody, maxCacheTimeout);
            }

            if (updateEmailBody != null)
            {
                updateEmailBody = patternToGetByName.Replace(updateEmailBody, string.Empty);
            }
            if (layoutBody != null)
            {
                updateEmailBody = Regex.Replace(layoutBody, @"\[\s*#\s*body\s*\]", updateEmailBody);
            }
            return updateEmailBody;
        }

        public string GetTemplate(string pathKey, string region = null, string language = null, bool applyLayout = false, bool defaultToSharedTemplate = false)
        {
            var sharedPath = ConfigurationManager.AppSettings[pathKey];
            var pathToTemplate = sharedPath ?? pathKey;
            if (!string.IsNullOrEmpty(region))
            {
                var _region = region.Trim();
                var _language = (string.IsNullOrEmpty(language) ? RegionalizeService.DefaultLanguage : language.Trim());
                pathToTemplate = RegionalizeService.RegionalizeTemplate(pathToTemplate, _region, _language);
            }

            if (!File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], pathToTemplate)))
            {
                if (!defaultToSharedTemplate || !File.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], sharedPath)))
                {
                    return null;
                }
                pathToTemplate = sharedPath;
            }

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], pathToTemplate)))
            {
                emailBody = reader.ReadToEnd();
            }

            if (applyLayout)
            {
                emailBody = ApplyLayout(emailBody, pathKey);
            }

            return emailBody;
        }

        public void SendContentAboutToExpireEmail(string region, int userId, string toEmail, string userName, IEnumerable<ContentItemDTO> contentItemsAboutToExpire)
        {
            var _region = (string.IsNullOrEmpty(region) ? RegionalizeService.DefaultRegion : region.Trim());
            var emailSubject = RegionalizeService.RegionalizeSetting("ExpiredContent.Subject", _region);
            var emailSubjectPrefix = RegionalizeService.RegionalizeSetting("ExpiredContent.SubjectPrefix", _region) ?? RegionalizeService.RegionalizeSetting($"AdminEmailSubjectPrefix", _region);
            var emailFromAddress = RegionalizeService.RegionalizeSetting("ExpiredContent.FromAddress", _region);
            var emailFromName = RegionalizeService.RegionalizeSetting("ExpiredContent.FromName", _region);
            var emailTitle = RegionalizeService.RegionalizeSetting("ExpiredContent.title", _region);
            var emailBody = GetContentAboutToExpireNotificationEmailBody(
                                                                            _region,
                                                                            _urlService.GetBusinessOwnerHomeURL(),
                                                                            emailSubject,
                                                                            emailSubjectPrefix,
                                                                            new Dictionary<string, IEnumerable<ContentItemDTO>>
                                                                            {
                                                                                {
                                                                                    ConfigurationManager.AppSettings["ExpiredContent.AdminSubSectionTemplateFile"],
                                                                                    contentItemsAboutToExpire
                                                                                }
                                                                            },
                                                                            s => _urlService.GetBusinessOwnerDetailsURL(s),
                                                                            s => _urlService.GetExpiredContentExtendURL(s),
                                                                            s => _urlService.GetExpiredContentDoNotExtendURL(s),
                                                                            emailTitle
                                                                        ).Replace("[#UserName]", userName);

            _mailDispatcherService.CreateMailDispatcher($"[{emailSubjectPrefix.ToUpper()}] {emailSubject}", emailFromAddress, emailFromName, toEmail, emailBody, userId, _region, Constants.TrackingTypes.ExpiredContent, 1, null);
        }

        public void SendContentAboutToExpireAdminEmail(string region, int userId, IEnumerable<ContentItemDTO> contentItemsAboutToExpire, IEnumerable<ContentItemDTO> programsAboutToExpire)
        {
            var _region = (string.IsNullOrEmpty(region) ? RegionalizeService.DefaultRegion : region.Trim());
            var regionAdmins = RegionalizeService.RegionalizeSetting("ExpiredContent.ToAddress", _region)?.ParseEmailAddresses(); //gets admins that will recieve this email
            var emailSubject = RegionalizeService.RegionalizeSetting("ExpiredContent.Subject", _region);
            var emailSubjectPrefix = RegionalizeService.RegionalizeSetting("ExpiredContent.SubjectPrefix", _region) ?? RegionalizeService.RegionalizeSetting($"AdminEmailSubjectPrefix", _region);
            var emailFromAddress = RegionalizeService.RegionalizeSetting("ExpiredContent.FromAddress", _region);
            var emailFromName = RegionalizeService.RegionalizeSetting("ExpiredContent.FromName", _region);
            var emailTitle = RegionalizeService.RegionalizeSetting("ExpiredContent.title", _region);
            var emailBody = GetContentAboutToExpireNotificationEmailBody(
                                                                            _region,
                                                                            _urlService.GetAdminHomeURL(),
                                                                            emailSubject,
                                                                            emailSubjectPrefix,
                                                                            new Dictionary<string, IEnumerable<ContentItemDTO>>
                                                                            {
                                                                                {
                                                                                    ConfigurationManager.AppSettings["ExpiredContent.AdminSubSectionTemplateFile"],
                                                                                    contentItemsAboutToExpire
                                                                                },
                                                                                {
                                                                                    ConfigurationManager.AppSettings["ExpiredContent.AdminSubSectionTemplateFile.Program"],
                                                                                    programsAboutToExpire
                                                                                }
                                                                            },
                                                                            s => _urlService.GetContentEditURL(s),
                                                                            s => _urlService.GetAdminHomeURL(),
                                                                            s => _urlService.GetAdminHomeURL(),
                                                                            emailTitle);
            foreach (var adminEmail in regionAdmins)
            {
                emailBody = emailBody.Replace("[#UserName]", adminEmail.Split(new[] { '.', '@' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault() ?? "Admin");

                _mailDispatcherService.CreateMailDispatcher($"[{emailSubjectPrefix.ToUpper()}] {emailSubject}", emailFromAddress, emailFromName, adminEmail, emailBody, userId, _region, Constants.TrackingTypes.ExpiredContent, 1, null);
            }
        }

        private string GetContentAboutToExpireNotificationEmailBody(
            string region,
            string assetListUrl,
            string emailSubject,
            string emailSubjectPrefix,
            Dictionary<string, IEnumerable<ContentItemDTO>> dynamicSubSections,
            Func<string, string> assetDetailUrl,
            Func<string, string> extendUrl,
            Func<string, string> doNotExtendUrl,
            string emailTitle = null)
        {
            string contactUsAddressLinks = string.Empty;
            foreach (var contactUsAddress in RegionalizeService.RegionalizeSetting("ExpiredContent.ContactUsAddress", region).ParseEmailAddresses())
            {
                contactUsAddressLinks += string.Format("<br /><a href=\"mailto:{0}\">{0}</a>", contactUsAddress);
            }
            var emailBody = GetTemplate("ExpiredContent.TemplateFile", region, applyLayout: true);
            emailBody = emailBody.Replace("[#HeaderImageUrl]", GetEmailTemplateHeaderImageUrl(region));
            emailBody = emailBody.Replace("[#AssetList]", assetListUrl);
            emailBody = emailBody.Replace("[#Subject]", emailTitle ?? emailSubject);
            emailBody = emailBody.Replace("[#SubjectPrefix]", emailSubjectPrefix);
            emailBody = emailBody.Replace("[#ContactUsAddress]", RegionalizeService.RegionalizeSetting("ExpiredContent.ContactUsAddress", region));
            emailBody = emailBody.Replace("[#ContactUsAddresses]", contactUsAddressLinks);
            emailBody = emailBody.Replace("[#CurrentYear]", DateTime.Now.Year.ToString());
            //the list of Assets is parsed and each asset's data is applied to a single item template which, then that collection is appended to the emails body
            emailBody = emailBody.Replace("[#DynamicContent]",
                dynamicSubSections.Any() ? string.Join("<br/>", dynamicSubSections.Select(s =>
                                                                s.Value != null && s.Value.Any() ?
                                                                GetDynamicContent(
                                                                    s.Key,
                                                                    s.Value.Select(aae => new Dictionary<string, string>
                                                                    {
                                                                        { "[#url]", assetDetailUrl(aae.ContentItemId) },
                                                                        { "[#title]", aae.Title },
                                                                        { "[#expirationDate]", aae.ExpirationDate.ToStringForEmail() },
                                                                        /* Extend or do not extend Url should have a "{0}" where the ContentItemID is required */
                                                                        { "[#ExtendUrl]", extendUrl(aae.ContentItemId) },
                                                                        { "[#DoNotExtendUrl]", doNotExtendUrl(aae.ContentItemId) }
                                                                    }),
                                                                    "Content") :
                                                                string.Empty)) : string.Empty);
            return emailBody;
        }

        public void SendBulkExtensionNotification(IEnumerable<Slam.Cms.Data.ContentItem> assets, bool willExtend, string comment, int daysToExtend, string userName)
        {
            var templateKey = willExtend ? "BulkExtendContentExpiration" : "BulkDoNotExtendContentExpiration";

            foreach (var assetByRegion in assets.GroupBy(ci => ci.RegionId))
            {
                var contactUsAddresses = RegionalizeService.RegionalizeSetting($"{templateKey}.ContactUsAddress", assetByRegion.Key).ParseEmailAddresses();
                string contactUsAddressLinks = "";
                foreach (var contactUsAddress in contactUsAddresses)
                {
                    contactUsAddressLinks += string.Format("<br /><a href=\"mailto:{0}\">{0}</a>", contactUsAddress);
                }

                var subTemplateKeyTokens = assetByRegion.Where(a => a is IContentItemWithTitle)
                    .Select(a =>
                    {
                        var tokens = new Dictionary<string, string> { { "[#AssetName]", ((IContentItemWithTitle)a).Title }, { "[#ExtendUrl]", _urlService.GetContentEditURL(a.ContentItemId) }, { "[#DoNotExtendUrl]", _urlService.GetAdminHomeURL() } };
                        return tokens;
                    });

                var extraTokens = new Dictionary<string, string> { { "[#DynamicContent]", GetDynamicContent(ConfigurationManager.AppSettings[$"{templateKey}.DynamicContentTemplate"], subTemplateKeyTokens, "asset") }, { "[#ContactUsAddresses]", contactUsAddressLinks }, { "[#ContactUsAddress]", RegionalizeService.RegionalizeSetting($"{templateKey}.ContactUsAddress", assetByRegion.Key) } };
                if (willExtend)
                {
                    extraTokens.Add("[#DaysToExtend]", daysToExtend.ToString());
                }

                var toAddresses = _settingsService.GetTemplateToAddresses(assetByRegion.Key, templateKey);
                SendTemplateToUsers(templateKey, toAddresses, comment, assetByRegion.Key, userName, null, true, extraTokens);
            }
        }

        public void SendContactUsEmail(string firstName, string lastName, string email, string processor, string issuer, string phone, string message, string subject, string toEmail, string country, string siteName, int userId, string region, string language)
        {
            string _region = (string.IsNullOrEmpty(region) ? RegionalizeService.DefaultRegion : region.Trim());
            string _language = (string.IsNullOrEmpty(language) ? RegionalizeService.DefaultLanguage : language.Trim());

            string emailTemplateFile = RegionalizeService.RegionalizeTemplate(ConfigurationManager.AppSettings["ContactUs.TemplateFile"], _region, _language);
            string emailSubject = $"{RegionalizeService.RegionalizeSetting("ContactUs.Subject", _region)}{(_region.ToLower() == "us" ? $" {subject}" : string.Empty)}";
            string emailFromAddress = RegionalizeService.RegionalizeSetting("ContactUs.FromAddress", _region);
            string emailFromName = RegionalizeService.RegionalizeSetting("ContactUs.FromName", _region);
            string emailToAddress = toEmail;
            string emailBccAddress = RegionalizeService.RegionalizeSetting("ContactUs.BCC", _region, false);

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#SiteName]", siteName);
            emailBody = emailBody.Replace("[#HeaderImageUrl]", GetEmailTemplateHeaderImageUrl(_region));
            emailBody = emailBody.Replace("[#Processor]", processor);
            emailBody = emailBody.Replace("[#FirstName]", firstName);
            emailBody = emailBody.Replace("[#LastName]", lastName);
            emailBody = emailBody.Replace("[#IssuerName]", issuer);
            emailBody = emailBody.Replace("[#Email]", email);
            emailBody = emailBody.Replace("[#Phone]", phone);
            emailBody = emailBody.Replace("[#Language]", (string.IsNullOrEmpty(language) ? "English" : _regionalizeService.GetLanguageTitle(language)));
            emailBody = emailBody.Replace("[#Country]", (string.IsNullOrEmpty(country) ? "United States" : _regionalizeService.GetCountryName(country)));
            emailBody = emailBody.Replace("[#Region]", _region);
            emailBody = emailBody.Replace("[#Subject]", subject);
            emailBody = emailBody.Replace("[#Message]", message);
            emailBody = emailBody.Replace("[#CurrentYear]", DateTime.Now.Year.ToString());

            _mailDispatcherService.CreateMailDispatcher(emailSubject, emailFromAddress, emailFromName, emailToAddress, emailBody, userId, _region, Constants.TrackingTypes.ContactUs, bccAddress: emailBccAddress, addTrackingPixelUrl: true);
        }

        private void SendTemplateToUsers(
            string templateKey,
            IEnumerable<string> emailToAddresses,
            string comment,
            string region,
            string userFirstName,
            string language = null,
            bool useLayout = true,
            IDictionary<string, string> otherTokens = null,
            string subjectSuffix = "",
            string titleSuffix = ""
            )
        {
            var _region = (string.IsNullOrEmpty(region) ? RegionalizeService.DefaultRegion : region.Trim());
            var _language = string.IsNullOrEmpty(language) ? RegionalizeService.DefaultLanguage : language.Trim();
            var body = GetTemplate($"{templateKey}.TemplateFile", _region, _language, useLayout);

            if (!string.IsNullOrEmpty(body))
            {
                var emailSubjectPrefix = RegionalizeService.RegionalizeSetting($"{templateKey}.SubjectPrefix", _region) ?? RegionalizeService.RegionalizeSetting($"AdminEmailSubjectPrefix", _region);
                var emailSubject = RegionalizeService.RegionalizeSetting($"{templateKey}.Subject", _region);
                var emailFromAddress = RegionalizeService.RegionalizeSetting($"{templateKey}.FromAddress", _region);
                var emailFromName = RegionalizeService.RegionalizeSetting($"{templateKey}.FromName", _region);
                var bannerPath = RegionalizeService.RegionalizeSetting($"{templateKey}.bannerPath", _region);

                body = body.Replace("[#HeaderImageUrl]", GetEmailTemplateHeaderImageUrl(_region, bannerPath));
                body = body.Replace("[#Subject]", $"{emailSubject}{(string.IsNullOrEmpty(titleSuffix) ? "" : (" " + titleSuffix))}");
                body = body.Replace("[#SubjectPrefix]", emailSubjectPrefix);
                body = body.Replace("[#UserFirstName]", userFirstName ?? "User");
                body = body.Replace("[#Comment]", comment);
                body = body.Replace("[#AssetList]", _urlService.GetAdminHomeURL());
                body = body.Replace("[#CurrentYear]", DateTime.Now.Year.ToString());

                if (otherTokens != null)
                {
                    foreach (var keyValue in otherTokens)
                    {
                        body = body.Replace(keyValue.Key, keyValue.Value);
                    }
                }

                foreach (var emailToAddress in emailToAddresses)
                {
                    var toName = _userService.GetUserByEmail(emailToAddress)?.FullName?.Split(' ')?.First() ??
                          emailToAddress.Split(new[] { '.', '@' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault()
                        ?? "Admin";

                    var bodyWithToName = body.Replace("[#ToName]", toName);
                    _mailDispatcherService.CreateMailDispatcher(
                        $"[{emailSubjectPrefix.ToUpper()}] {emailSubject}{(string.IsNullOrEmpty(subjectSuffix) ? "" : (" " + subjectSuffix))}",
                        emailFromAddress,
                        emailFromName,
                        emailToAddress,
                        bodyWithToName,
                        0,
                        _region,
                        Constants.TrackingTypes.ContentCommentNotification,
                        addTrackingPixelUrl: false);
                }
            }
            else
            {
                throw new Exception(string.Format("No body email message was found for template '{0}.TemplateFile', region '{1}', language '{2}', layout '{3}'", templateKey, _region, _language, useLayout));
            }
        }

        public void SendContentCommentNotification(
            string templateKey,
            IEnumerable<string> toAddresses,
            string assetName,
            string contentItemId,
            string comment,
            string region,
            string userFirstName,
            string editContentItemUrl,
            string language = null)
        {
            var adresses = _settingsService.GetContentCommentNotificationContactUsAddress(region);
            var contactUsAddressLinks = "";
            foreach (var contactUsAddress in adresses.ParseEmailAddresses())
            {
                contactUsAddressLinks += string.Format("<br /><a href=\"mailto:{0}\">{0}</a>", contactUsAddress);
            }

            var extraTokens = new Dictionary<string, string> {
                { "[#AssetName]", assetName },
                { "[#EditAssetUrl]", editContentItemUrl },
                { "[#ContactUsAddresses]", contactUsAddressLinks },
                { "[#ContactUsAddress]", adresses } };

            SendTemplateToUsers(templateKey, toAddresses, comment, region, userFirstName,
                language, true, extraTokens, string.Empty, string.Empty);
        }

        private Dictionary<string, string> GetAssetSuggestionTokens(string templateKey, string region, string assetName, string assetId, bool contactUsAddressLinksAtTheEnd = false)
        {
            var contactUsAddresses = RegionalizeService.RegionalizeSetting($"{templateKey}.ContactUsAddress", region).ParseEmailAddresses();
            var contactUsAddressLinks = new StringBuilder();
            foreach (var contactUsAddress in contactUsAddresses)
            {
                var contactUsAddressLink = contactUsAddressLinksAtTheEnd ?
                   $"<a href=\"mailto:{contactUsAddress}\">{contactUsAddress}</a><br />" :
                   $"<br /><a href=\"mailto:{contactUsAddress}\">{contactUsAddress}</a>";
                contactUsAddressLinks.Append(contactUsAddressLink);
            }

            return new Dictionary<string, string> { { "[#AssetName]", assetName }, { "[#ExtendUrl]", _urlService.GetContentEditURL(assetId) }, { "[#DoNotExtendUrl]", _urlService.GetAdminHomeURL() }, { "[#ContactUsAddresses]", contactUsAddressLinks.ToString() }, { "[#ContactUsAddress]", RegionalizeService.RegionalizeSetting($"{templateKey}.ContactUsAddress", region) } };
        }

        public void SendExtensionNotification(string assetName, string assetId, bool willExtend, string comment, int daysToExtend, string region, string userName, string language = null)
        {
            var templateKey = willExtend ? "ExtendContentExpiration" : "DoNotExtendContentExpiration";
            var extraTokens = GetAssetSuggestionTokens(templateKey, region, assetName, assetId);

            if (willExtend)
            {
                extraTokens.Add("[#DaysToExtend]", daysToExtend.ToString());
            }

            var toAddresses = _settingsService.GetTemplateToAddresses(region, templateKey);
            SendTemplateToUsers(templateKey, toAddresses, comment, region, userName, language, true, extraTokens);
        }

        public void SendNewAssetNotification(string assetName, string newAssetId, string region, string userName, string language = null)
        {
            var templateKey = "NewAssetNotification";
            var extraTokens = GetAssetSuggestionTokens(templateKey, region, assetName, newAssetId, true);
            var adminEmailSubjectsuffix = RegionalizeService.RegionalizeSetting($"{templateKey}.Subjectsuffix", region) ?? RegionalizeService.RegionalizeSetting($"AdminEmailSubjectPrefix", region);
            var toAddresses = _settingsService.GetTemplateToAddresses(region, templateKey);

            SendTemplateToUsers(templateKey, toAddresses, null, region, userName, language, true, extraTokens, string.Format(adminEmailSubjectsuffix, userName));
        }

        public void SendModifiedAssetNotification(string assetName, string AssetId, string region, string userName, string language = null)
        {
            var templateKey = "ModifiedAssetNotification";
            var extraTokens = GetAssetSuggestionTokens(templateKey, region, assetName, AssetId, true);
            var adminEmailSubjectsuffix = RegionalizeService.RegionalizeSetting($"{templateKey}.Subjectsuffix", region) ?? RegionalizeService.RegionalizeSetting($"AdminEmailSubjectPrefix", region);
            var titleSuffix = RegionalizeService.RegionalizeSetting($"{templateKey}.Titlesuffix", region);
            var toAddresses = _settingsService.GetTemplateToAddresses(region, templateKey);
            SendTemplateToUsers(templateKey, toAddresses, null, region, userName, language, true, extraTokens, string.Format(adminEmailSubjectsuffix, userName), titleSuffix);
        }

        public void SendNotAssetBusinessOwnerNotification(string contentItemId, string assetTitle, string businessOwnerName, string message, string region, string language = null)
        {
            var templateKey = "NotAssetBusinessOwnerNotification";
            var extraTokens = new Dictionary<string, string>
            {
                { "[#AssetTitle]", assetTitle },
                { "[#EditAssetUrl]", $"{RegionalizeService.GetBaseUrl(region).TrimEnd('/')}/admin/Content/Edit?id={ contentItemId }" },
            };
            var settingName = $"{templateKey}.ToAddress";
            var address = RegionalizeService.RegionalizeSetting(settingName, region);
            if (string.IsNullOrEmpty(address))
            {
                throw new Exception(string.Format("Could not find a setting {0}", settingName));
            }

            var contactUsAddresses = address.ParseEmailAddresses();
            string contactUsAddressLinks = "";
            foreach (var contactUsAddress in contactUsAddresses)
            {
                contactUsAddressLinks += string.Format("<br /><a href=\"mailto:{0}\">{0}</a>", contactUsAddress);
            }

            extraTokens.Add("[#ContactUsAddresses]", contactUsAddressLinks);
            var toAddresses = _settingsService.GetTemplateToAddresses(region, templateKey);
            SendTemplateToUsers(templateKey, toAddresses, message, region, businessOwnerName, language, true, extraTokens);
        }

        public void SendEmailBadDomainEmail(string firstName, string lastName, string issuerName, string phone, string mobile, string fax, string emailAddress, string address1, string address2, string address3, string city, string state, string zipCode, string country, string siteName, string region, string language)
        {
            string _region = (string.IsNullOrEmpty(region) ? RegionalizeService.DefaultRegion : region.Trim());

            string emailTemplateFile = RegionalizeService.RegionalizeTemplate(ConfigurationManager.AppSettings["EmailBadDomain.TemplateFile"], _region);
            string emailSubject = RegionalizeService.RegionalizeSetting("EmailBadDomain.Subject", _region);
            string emailFromAddress = RegionalizeService.RegionalizeSetting("EmailBadDomain.FromAddress", _region);
            string emailFromName = RegionalizeService.RegionalizeSetting("EmailBadDomain.FromName", _region);
            string emailToAddress = RegionalizeService.RegionalizeSetting("EmailBadDomain.ToAddress", _region);

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#SiteName]", siteName);
            emailBody = emailBody.Replace("[#HeaderImageUrl]", GetEmailTemplateHeaderImageUrl(_region));
            emailBody = emailBody.Replace("[#FirstName]", firstName);
            emailBody = emailBody.Replace("[#LastName]", lastName);
            emailBody = emailBody.Replace("[#IssuerName]", issuerName);
            emailBody = emailBody.Replace("[#Phone]", phone);
            emailBody = emailBody.Replace("[#Mobile]", mobile);
            emailBody = emailBody.Replace("[#Fax]", fax);
            emailBody = emailBody.Replace("[#EmailAddress]", emailAddress);
            emailBody = emailBody.Replace("[#Address1]", address1);
            emailBody = emailBody.Replace("[#Address2]", address2);
            emailBody = emailBody.Replace("[#Address3]", address3);
            emailBody = emailBody.Replace("[#City]", city);
            emailBody = emailBody.Replace("[#State]", state);
            emailBody = emailBody.Replace("[#ZipCode]", zipCode);
            emailBody = emailBody.Replace("[#Language]", language);
            emailBody = emailBody.Replace("[#Country]", country);
            emailBody = emailBody.Replace("[#Region]", _region);
            emailBody = emailBody.Replace("[#CurrentYear]", DateTime.Now.Year.ToString());

            _mailDispatcherService.CreateMailDispatcher(emailSubject, emailFromAddress, emailFromName, emailToAddress, emailBody, 0, _region, Constants.TrackingTypes.EmailBadDomain, addTrackingPixelUrl: true);
        }

        public void SendEmailVerificationEmail(string toEmail, string firstName, string userName, string siteName, int userId, string region, string language, string lastName = "")
        {
            string _region = (string.IsNullOrEmpty(region) ? RegionalizeService.DefaultRegion : region.Trim());
            string _language = string.IsNullOrEmpty(language) ? RegionalizeService.DefaultLanguage : language.Trim();

            // TODO: Move settings to SettingsService
            string emailTemplateFile = RegionalizeService.RegionalizeTemplate(_settingsService.GetEmailVerificationTemplateFile(), _region, _language);
            string emailSubject = RegionalizeService.RegionalizeSetting("EmailVerification.Subject", _region, _language);
            string emailFromAddress = RegionalizeService.RegionalizeSetting("EmailVerification.FromAddress", _region);
            string emailFromName = RegionalizeService.RegionalizeSetting("EmailVerification.FromName", _region);
            string emailToAddress = toEmail;
            string emailBccAddress = RegionalizeService.RegionalizeSetting("EmailVerification.BCC", _region, false);
            string emailBody;
            string alternativeBody = null;
            bool isMultipart = _settingsService.GetMailRegistrationMultipartEnabled();

            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _settingsService.GetEmailTemplateFolder(), emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            if (isMultipart)
            {
                string textPlainEmailTemplateFile = RegionalizeService.RegionalizeTemplate(_settingsService.GetEmailVerificationTextPlainTemplateFile(), _region, _language);
                using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _settingsService.GetEmailTemplateFolder(), textPlainEmailTemplateFile)))
                {
                    alternativeBody = reader.ReadToEnd();
                }
                alternativeBody = alternativeBody.Replace("[#SiteName]", siteName);
                alternativeBody = alternativeBody.Replace("[#FirstName]", firstName);
                alternativeBody = SetFullName(alternativeBody, firstName, lastName);
                alternativeBody = alternativeBody.Replace("[#TargetUrl]", _urlService.GetCreateAccountURL(userName));
                alternativeBody = alternativeBody.Replace("[#CurrentYear]", DateTime.Now.Year.ToString());
            }

            emailBody = emailBody.Replace("[#SiteName]", siteName);
            emailBody = emailBody.Replace("[#HeaderImageUrl]", GetEmailTemplateHeaderImageUrl(_region));
            emailBody = emailBody.Replace("[#FirstName]", firstName);
            emailBody = SetFullName(emailBody, firstName, lastName);
            emailBody = emailBody.Replace("[#TargetUrl]", _urlService.GetCreateAccountURL(userName));
            emailBody = emailBody.Replace("[#CurrentYear]", DateTime.Now.Year.ToString());

            var mailDispatcher = _mailDispatcherService.CreateMailDispatcher(emailSubject, emailFromAddress, emailFromName, emailToAddress, emailBody, userId, _region, Constants.TrackingTypes.EmailVerification, bccAddress: emailBccAddress, addTrackingPixelUrl: true, isMultipart: isMultipart, alternativeBody: alternativeBody);

            _mailDispatcherService.SendMailDispatcher(mailDispatcher);
        }

        private string SetFullName(string emailBody, string firstName, string lastName)
        {
            if (!string.IsNullOrEmpty(lastName))
            {
                return emailBody.Replace("[#FullName]", $"{firstName} {lastName}");
            }
            return emailBody;
        }

        public void SendForgotPasswordEmail(string toEmail, string firstName, string expirationTime, string userKey, string siteName, int userId, string region, string language, string lastName = "")
        {
            string _region = (string.IsNullOrEmpty(region) ? RegionalizeService.DefaultRegion : region.Trim());
            string _language = string.IsNullOrEmpty(language) ? RegionalizeService.DefaultLanguage : language.Trim();

            string emailTemplateFile = RegionalizeService.RegionalizeTemplate(ConfigurationManager.AppSettings["ForgotPassword.TemplateFile"], _region, _language);
            string emailSubject = RegionalizeService.RegionalizeSetting("ForgotPassword.Subject", _region, _language);
            string emailFromAddress = RegionalizeService.RegionalizeSetting("ForgotPassword.FromAddress", _region);
            string emailFromName = RegionalizeService.RegionalizeSetting("ForgotPassword.FromName", _region);
            string emailToAddress = toEmail;

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#SiteName]", siteName);
            emailBody = emailBody.Replace("[#HeaderImageUrl]", GetEmailTemplateHeaderImageUrl(_region));
            emailBody = emailBody.Replace("[#FirstName]", firstName);
            emailBody = SetFullName(emailBody, firstName, lastName);
            emailBody = emailBody.Replace("[#TargetUrl]", $"{RegionalizeService.GetBaseUrl(_region).TrimEnd('/')}/{ConfigurationManager.AppSettings["portalBaseURL"].ToString().TrimStart('/').TrimEnd('/')}/resetpassword/{userKey}");
            emailBody = emailBody.Replace("[#ExpirationTime]", expirationTime);
            emailBody = emailBody.Replace("[#CurrentYear]", DateTime.Now.Year.ToString());

            var mailDispatcher = _mailDispatcherService.CreateMailDispatcher(emailSubject, emailFromAddress, emailFromName, emailToAddress, emailBody, userId, _region, Constants.TrackingTypes.Forgot, addTrackingPixelUrl: true);

            _mailDispatcherService.SendMailDispatcher(mailDispatcher);
        }

        public void SendPendingRegistrationApprovedEmail(string toEmail, string firstName, string userName, string siteName, int userId, string region, string language, string lastName = "")
        {
            string _region = (string.IsNullOrEmpty(region) ? RegionalizeService.DefaultRegion : region.Trim());
            string _language = string.IsNullOrEmpty(language) ? RegionalizeService.DefaultLanguage : language.Trim();

            string emailTemplateFile = RegionalizeService.RegionalizeTemplate(ConfigurationManager.AppSettings["PendingRegistrationApproved.TemplateFile"], _region, _language);
            string emailSubject = RegionalizeService.RegionalizeSetting("PendingRegistrationApproved.Subject", _region);
            string emailFromAddress = RegionalizeService.RegionalizeSetting("PendingRegistrationApproved.FromAddress", _region);
            string emailFromName = RegionalizeService.RegionalizeSetting("PendingRegistrationApproved.FromName", _region);
            string emailToAddress = toEmail;

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#SiteName]", siteName);
            emailBody = emailBody.Replace("[#HeaderImageUrl]", GetEmailTemplateHeaderImageUrl(_region));
            emailBody = emailBody.Replace("[#FirstName]", firstName);
            emailBody = SetFullName(emailBody, firstName, lastName);
            emailBody = emailBody.Replace("[#TargetUrl]", _urlService.GetCreateAccountURL(userName));
            emailBody = emailBody.Replace("[#CurrentYear]", DateTime.Now.Year.ToString());

            var mailDispatcher = _mailDispatcherService.CreateMailDispatcher(emailSubject, emailFromAddress, emailFromName, emailToAddress, emailBody, userId, _region, Constants.TrackingTypes.PendingRegistrationApproved, addTrackingPixelUrl: true);

            _mailDispatcherService.SendMailDispatcher(mailDispatcher);
        }

        public void SendPendingRegistrationRejectedEmail(string toEmail, string rejectionReason, string siteName, int userId, string region, string language)
        {
            string _region = (string.IsNullOrEmpty(region) ? RegionalizeService.DefaultRegion : region.Trim());
            string _language = string.IsNullOrEmpty(language) ? RegionalizeService.DefaultLanguage : language.Trim();

            string emailTemplateFile = RegionalizeService.RegionalizeTemplate(ConfigurationManager.AppSettings["PendingRegistrationRejected.TemplateFile"], _region, _language);
            string emailSubject = RegionalizeService.RegionalizeSetting("PendingRegistrationRejected.Subject", _region, _language);
            string emailFromAddress = RegionalizeService.RegionalizeSetting("PendingRegistrationRejected.FromAddress", _region);
            string emailFromName = RegionalizeService.RegionalizeSetting("PendingRegistrationRejected.FromName", _region);
            string emailToAddress = toEmail;

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#SiteName]", siteName);
            emailBody = emailBody.Replace("[#HeaderImageUrl]", GetEmailTemplateHeaderImageUrl(_region));
            emailBody = emailBody.Replace("[#RejectionReason]", rejectionReason);
            emailBody = emailBody.Replace("[#CurrentYear]", DateTime.Now.Year.ToString());

            _mailDispatcherService.CreateMailDispatcher(emailSubject, emailFromAddress, emailFromName, emailToAddress, emailBody, userId, _region, Constants.TrackingTypes.PendingRegistrationRejected, addTrackingPixelUrl: true);
        }

        public void SendNewPendingRegistrationEmail(string firstName, string lastName, string title, string financialInstitution, string processor, string phone, string mobile, string fax, string emailAddress, string address1, string address2, string address3, string city, string province, string stateName, string zip, string country, string guid, string siteName, int userId, string region, string language)
        {
            string _region = (string.IsNullOrEmpty(region) ? RegionalizeService.DefaultRegion : region.Trim());
            string _language = string.IsNullOrEmpty(language) ? RegionalizeService.DefaultLanguage : language.Trim();

            string emailTemplateFile = RegionalizeService.RegionalizeTemplate(ConfigurationManager.AppSettings["NewPendingRegistration.TemplateFile"], _region, _language);
            string emailSubject = RegionalizeService.RegionalizeSetting("NewPendingRegistration.Subject", _region);
            string emailFromAddress = RegionalizeService.RegionalizeSetting("NewPendingRegistration.FromAddress", _region);
            string emailFromName = RegionalizeService.RegionalizeSetting("NewPendingRegistration.FromName", _region);
            string emailToAddress = RegionalizeService.RegionalizeSetting("NewPendingRegistration.ToAddress", _region);

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#SiteName]", siteName);
            emailBody = emailBody.Replace("[#HeaderImageUrl]", GetEmailTemplateHeaderImageUrl(_region));
            emailBody = emailBody.Replace("[#FirstName]", firstName);
            emailBody = emailBody.Replace("[#LastName]", lastName);
            emailBody = emailBody.Replace("[#Title]", title);
            emailBody = emailBody.Replace("[#FinancialInstitution]", financialInstitution);
            emailBody = emailBody.Replace("[#Processor]", string.IsNullOrEmpty(processor) ? " " : processor);
            emailBody = emailBody.Replace("[#Phone]", phone);
            emailBody = emailBody.Replace("[#Mobile]", string.IsNullOrEmpty(mobile) ? " " : mobile);
            emailBody = emailBody.Replace("[#Fax]", fax);
            emailBody = emailBody.Replace("[#EmailAddress]", emailAddress);
            emailBody = emailBody.Replace("[#Address1]", string.IsNullOrEmpty(address1) ? " " : address1);
            emailBody = emailBody.Replace("[#Address2]", string.IsNullOrEmpty(address2) ? " " : address2);
            emailBody = emailBody.Replace("[#Address3]", string.IsNullOrEmpty(address3) ? " " : address3);
            string state = string.IsNullOrEmpty(province.ToString()) ? stateName : province.ToString();
            string cityStateZip = city.Trim();
            cityStateZip += cityStateZip.Length > 0 && !string.IsNullOrWhiteSpace(state) ? $", {state.Trim()}" : state.Trim();
            cityStateZip += cityStateZip.Length > 0 && !string.IsNullOrWhiteSpace(zip) ? $", {zip.Trim()}" : zip.Trim();
            emailBody = emailBody.Replace("[#City_State_Zip]", cityStateZip.Trim());
            emailBody = emailBody.Replace("[#Language]", string.IsNullOrEmpty(language) ? "English" : _regionalizeService.GetLanguageTitle(language));
            emailBody = emailBody.Replace("[#Country]", string.IsNullOrEmpty(country) ? "United States" : _regionalizeService.GetCountryName(country));
            emailBody = emailBody.Replace("[#Region]", _region);
            emailBody = emailBody.Replace("[#PendingRegistrationUrl]", $"{RegionalizeService.GetBaseUrl(_region).TrimEnd('/')}/{ConfigurationManager.AppSettings["portalBaseURL"].ToString().TrimStart('/').TrimEnd('/')}/registration/pending/{guid}");
            emailBody = emailBody.Replace("[#CurrentYear]", DateTime.Now.Year.ToString());

            _mailDispatcherService.CreateMailDispatcher(emailSubject, emailFromAddress, emailFromName, emailToAddress, emailBody, userId, _region, Constants.TrackingTypes.NewPendingRegistration, addTrackingPixelUrl: true);
        }

        public void SendOrderShippedEmail(string orderno, string Date, string ItemDetails, string Totals, string email, string firstName, string lastName, string shippingDetail, string specialInstructions, bool sendPartialShipmentEmail, int userId)
        {
            string target = $"{RegionalizeService.GetBaseUrl("us").TrimEnd('/')}/{ConfigurationManager.AppSettings["portalBaseURL"].ToString().TrimStart('/').TrimEnd('/')}/orderdetails/{orderno}";
            string emailTemplateFile = sendPartialShipmentEmail ? ConfigurationManager.AppSettings["PartialOrderShipped.TemplateFile"].ToString() : ConfigurationManager.AppSettings["OrderShipped.TemplateFile"].ToString();
            string subject = $"Mastercard - Your order #{orderno} has been shipped";
            string fromName = ConfigurationManager.AppSettings["OrderShipped.FromName"].ToString();
            string fromAddress = ConfigurationManager.AppSettings["OrderShipped.FromAddress"].ToString();

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#ProofLink]", target);
            emailBody = emailBody.Replace("[#ORDERNO]", orderno);
            emailBody = emailBody.Replace("[#DATE]", Date);
            emailBody = emailBody.Replace("[#DETAILS]", ItemDetails);
            emailBody = emailBody.Replace("[#SHIPPINGDETAILS]", shippingDetail);
            emailBody = emailBody.Replace("[#SPECIALINSTRUCTIONS]", specialInstructions);
            emailBody = emailBody.Replace("[#TOTALS]", Totals.ToString());

            _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, email, emailBody, userId, "us", Constants.TrackingTypes.OrderShipped, bccAddress: ConfigurationManager.AppSettings["OrderShipped.BCC"], isBodyHtml: false);
        }

        public void SendProofEmail(string orderno, string email, string firstName, string lastName, int userId)
        {
            string target = $"{RegionalizeService.GetBaseUrl("us").TrimEnd('/')}/{ConfigurationManager.AppSettings["portalBaseURL"].ToString().TrimStart('/').TrimEnd('/')}/orderdetails/{orderno}";
            string emailTemplateFile = ConfigurationManager.AppSettings["ProofReady.TemplateFile"].ToString();
            string subject = $"Mastercard - Proof is ready for your order #{orderno}";
            string fromName = ConfigurationManager.AppSettings["ProofReady.FromName"].ToString();
            string fromAddress = ConfigurationManager.AppSettings["ProofReady.FromAddress"].ToString();

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#ProofLink]", target);

            _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, email, emailBody, userId, "us", Constants.TrackingTypes.ProofReady, bccAddress: ConfigurationManager.AppSettings["ProofReady.BCC"], isBodyHtml: false);
        }

        public void SendProofReminderEmail(string orderno, string email, string firstName, string lastName, string frequencyInDays, int userId)
        {
            string target = $"{RegionalizeService.GetBaseUrl("us").TrimEnd('/')}/{ConfigurationManager.AppSettings["portalBaseURL"].ToString().TrimStart('/').TrimEnd('/')}/orderdetails/{orderno}";
            string emailTemplateFile = ConfigurationManager.AppSettings["ProofReadyReminder.TemplateFile"].ToString();
            string subject = $"Mastercard - Proof is ready for your order #{orderno} (Reminder)";
            string fromName = ConfigurationManager.AppSettings["ProofReadyReminder.FromName"].ToString();
            string fromAddress = ConfigurationManager.AppSettings["ProofReadyReminder.FromAddress"].ToString();

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#ProofLink]", target);
            emailBody = emailBody.Replace("[#DAYS]", target);
            emailBody = emailBody.Replace("[#ORDER]", target);

            _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, email, emailBody, userId, "us", Constants.TrackingTypes.ProofReadyReminder, bccAddress: ConfigurationManager.AppSettings["ProofReadyReminder.BCC"], isBodyHtml: false);
        }

        public void SendProofApprovedEmail(string orderno, int userId)
        {
            var emails = _userService.GetPrinterUsers().Select(u => u.Email);
            if (emails.Any())
            {
                string target = $"{SlamConfig.ConfigurationManager.Environment.FrontEndUrl.TrimEnd('/')}/{ConfigurationManager.AppSettings["AGE.MC.MMP.PRINTER.OrderDetail"].TrimStart('/').Replace("{orderid}", orderno)}";
                string emailTemplateFile = ConfigurationManager.AppSettings["ProofApproved.TemplateFile"].ToString();
                string subject = "Mastercard - Proof approved for order #" + orderno;
                string fromName = ConfigurationManager.AppSettings["ProofApproved.FromName"].ToString();
                string fromAddress = ConfigurationManager.AppSettings["ProofApproved.FromAddress"].ToString();

                string emailBody;
                using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
                {
                    emailBody = reader.ReadToEnd();
                }

                emailBody = emailBody.Replace("[#ProofLink]", target);

                if (emails.Any())
                {
                    _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, emails.Aggregate((e1, e2) => e1 + ";" + e2), emailBody, userId, "us", Constants.TrackingTypes.ProofApproved, bccAddress: ConfigurationManager.AppSettings["ProofApproved.BCC"], isBodyHtml: false);
                }
            }
        }

        public void SendPendingCorrectionEmail(string orderno, int userId)
        {
            var emails = _userService.GetPrinterUsers().Select(u => u.Email);
            if (emails.Any())
            {
                string target = $"{SlamConfig.ConfigurationManager.Environment.FrontEndUrl.TrimEnd('/')}/{ConfigurationManager.AppSettings["AGE.MC.MMP.PRINTER.OrderDetail"].TrimStart('/').Replace("{orderid}", orderno)}";
                string emailTemplateFile = ConfigurationManager.AppSettings["PendingCorrection.TemplateFile"].ToString();
                string subject = "Mastercard - pending correction for order #" + orderno;
                string fromName = ConfigurationManager.AppSettings["PendingCorrection.FromName"].ToString();
                string fromAddress = ConfigurationManager.AppSettings["PendingCorrection.FromAddress"].ToString();

                string emailBody;
                using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
                {
                    emailBody = reader.ReadToEnd();
                }

                emailBody = emailBody.Replace("[#ProofLink]", target);

                if (emails.Any())
                {
                    _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, emails.Aggregate((e1, e2) => e1 + ";" + e2), emailBody, userId, "us", Constants.TrackingTypes.PendingCorrection, bccAddress: ConfigurationManager.AppSettings["PendingCorrection.BCC"], isBodyHtml: false);
                }
            }
        }

        public void SendOrderConfirmationEmail(string orderno, string Date, string ItemDetails, string email, string firstName, string lastName, string shippingNote, string shippingTemplate, bool customizedOrder, bool readyToPrint, string vdpTemplate, string electronicDeliveryTemplate, string estimatedDistributionTemplate, bool requiresApprovalForPOD, int userId)
        {
            string readyToPrintMessage = Environment.NewLine
                                       + "You will be contacted by email and/or telephone and provided a price quote." + Environment.NewLine + Environment.NewLine
                                       + "You'll have the option to pay via your Mastercard ICA number or, if you do not have one, by credit, debit or prepaid card." + Environment.NewLine
                                       + "A final proof will be sent for approval prior to the order printed and shipped." + Environment.NewLine
                                       + "The details of your order can be found below." + Environment.NewLine;

            if (requiresApprovalForPOD)
            {
                readyToPrintMessage = Environment.NewLine
                                   + "PLEASE NOTE: YOUR ORDER IS BEING REVIEWED." + Environment.NewLine
                                   + "Within 1 - 2 business days, you will receive another email notification once your order is approved." + Environment.NewLine + Environment.NewLine
                                   + "You'll have the option to pay via your Mastercard ICA number or, if you do not have one, by credit, debit or prepaid card." + Environment.NewLine;
            }

            string target = $"{RegionalizeService.GetBaseUrl("us").TrimEnd('/')}/{ConfigurationManager.AppSettings["portalBaseURL"].ToString().TrimStart('/').TrimEnd('/')}/orderdetails/{orderno}";
            string emailTemplateFile = customizedOrder ? ConfigurationManager.AppSettings["OrderConfirmation.TemplateFile"].ToString() : ConfigurationManager.AppSettings["OrderConfirmationNoCustomization.TemplateFile"].ToString();
            string subject = "Mastercard - Your confirmation for the Order #" + orderno;
            string fromName = ConfigurationManager.AppSettings["OrderConfirmation.FromName"].ToString();
            string fromAddress = ConfigurationManager.AppSettings["OrderConfirmation.FromAddress"].ToString();

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#ORDERNO]", orderno);
            emailBody = emailBody.Replace("[#DATE]", Date);
            emailBody = emailBody.Replace("[#DETAILS]", ItemDetails);
            emailBody = emailBody.Replace("[#READYTOPRINTMESSAGE]", readyToPrint ? readyToPrintMessage : " ");
            emailBody = emailBody.Replace("[#SHIPPINGNOTE]", string.IsNullOrEmpty(shippingNote) ? " " : shippingNote);
            emailBody = emailBody.Replace("[#ESTIMATEDDISTRIBUTION]", string.IsNullOrEmpty(estimatedDistributionTemplate) ? " " : estimatedDistributionTemplate);
            emailBody = emailBody.Replace("[#SHIPPING]", string.IsNullOrEmpty(shippingTemplate) ? " " : shippingTemplate);
            emailBody = emailBody.Replace("[#TARGETURL]", target);
            emailBody = emailBody.Replace("[#VDP]", string.IsNullOrEmpty(vdpTemplate) ? " " : vdpTemplate);
            emailBody = emailBody.Replace("[#ELECTRONICDELIVERY]", string.IsNullOrEmpty(electronicDeliveryTemplate) ? " " : electronicDeliveryTemplate);

            _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, email, emailBody, userId, "us", Constants.TrackingTypes.OrderConfirmation, bccAddress: ConfigurationManager.AppSettings["OrderConfirmation.BCC"], isBodyHtml: false);
        }

        public void SendElectronicDeliveryOrderApprovedEmail(string orderno, string Date, string ItemDetails, string email, string firstName, string lastName, string electronicDeliveryTemplate, int userId)
        {
            string target = $"{RegionalizeService.GetBaseUrl("us").TrimEnd('/')}/{ConfigurationManager.AppSettings["portalBaseURL"].ToString().TrimStart('/').TrimEnd('/')}/orderdetails/{orderno}";
            string emailTemplateFile = ConfigurationManager.AppSettings["ElectronicDeliveryOrderApproved.TemplateFile"].ToString();
            string subject = $"Mastercard - Your approval notice for your Order #{orderno}";
            string fromName = ConfigurationManager.AppSettings["ElectronicDeliveryOrderApproved.FromName"].ToString();
            string fromAddress = ConfigurationManager.AppSettings["ElectronicDeliveryOrderApproved.FromAddress"].ToString();

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#ORDERNO]", orderno);
            emailBody = emailBody.Replace("[#DATE]", Date);
            emailBody = emailBody.Replace("[#DETAILS]", ItemDetails);
            emailBody = emailBody.Replace("[#TARGETURL]", target);
            emailBody = emailBody.Replace("[#ELECTRONICDELIVERY]", string.IsNullOrEmpty(electronicDeliveryTemplate) ? " " : electronicDeliveryTemplate);

            _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, email, emailBody, userId, "us", Constants.TrackingTypes.ElectronicDeliveryOrderApproved, bccAddress: ConfigurationManager.AppSettings["ElectronicDeliveryOrderApproved.BCC"], isBodyHtml: false);
        }

        public void SendElectronicDeliveryDownloadableAssetEmail(string itemName, string link, DateTime expirationDate, int maxDownloadCount, string email, string fullName, int userId, string region, string language)
        {
            string _region = (string.IsNullOrEmpty(region) ? RegionalizeService.DefaultRegion : region.Trim());
            string _language = string.IsNullOrEmpty(language) ? RegionalizeService.DefaultLanguage : language.Trim();

            string emailTemplateFile = RegionalizeService.RegionalizeTemplate(ConfigurationManager.AppSettings["ElectronicDeliveryDownloadableAsset.TemplateFile"], _region, _language);
            string subject = string.Format(RegionalizeService.RegionalizeSetting("ElectronicDeliveryDownloadableAsset.Subject", _region), itemName);
            string fromName = ConfigurationManager.AppSettings["ElectronicDeliveryDownloadableAsset.FromName"];
            string fromAddress = RegionalizeService.RegionalizeSetting("ElectronicDeliveryDownloadableAsset.FromAddress", _region);

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#NAME]", fullName);
            emailBody = emailBody.Replace("[#ITEM_NAME]", itemName);
            emailBody = emailBody.Replace("[#EXPIRATION_DATE]", expirationDate.ToString("d"));
            emailBody = emailBody.Replace("[#MAX_DOWNLOAD]", maxDownloadCount.ToString());
            emailBody = emailBody.Replace("[#DOWNLOAD_LINK]", link);
            emailBody = emailBody.Replace("[#FORM_LINK]", RegionalizeService.RegionalizeSetting("ElectronicDeliveryDownloadableAsset.FormLink", _region, _language));

            _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, email, emailBody, userId, _region, Constants.TrackingTypes.ElectronicDeliveryOrderApproved, bccAddress: ConfigurationManager.AppSettings["ElectronicDeliveryDownloadableAsset.BCC"], isBodyHtml: false);
        }

        public void SendOrderRejectedEmail(string orderno, string rejectionReason, string email, string firstName, string lastName, int userId)
        {
            string target = $"{RegionalizeService.GetBaseUrl("us").TrimEnd('/')}/{ConfigurationManager.AppSettings["portalBaseURL"].ToString().TrimStart('/').TrimEnd('/')}/orderdetails/{orderno}";
            string subject = $"{ConfigurationManager.AppSettings["OrderRejected.SubjectPrefix"]}{orderno}";
            string emailTemplateFile = ConfigurationManager.AppSettings["OrderRejected.TemplateFile"].ToString();
            string fromName = ConfigurationManager.AppSettings["OrderRejected.FromName"].ToString();
            string fromAddress = ConfigurationManager.AppSettings["OrderRejected.FromAddress"].ToString();

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#REJECTIONREASON]", rejectionReason);
            emailBody = emailBody.Replace("[#ORDERLINK]", target);

            _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, email, emailBody, userId, "us", Constants.TrackingTypes.OrderRejected, bccAddress: ConfigurationManager.AppSettings["OrderRejected.BCC"], isBodyHtml: false);
        }

        public void SendNewOrderForApprovalEmail(string orderno, string customer, string issuer, string domain, string phone, string email, string address, string city, string state, string zip, List<RetrieveOrderItemsResult> approvableItems, string shippingAddress, int userId)
        {
            string approvalLink = $"{RegionalizeService.GetBaseUrl("us").TrimEnd('/')}/{ConfigurationManager.AppSettings["portalBaseURL"].ToString().TrimStart('/').TrimEnd('/')}/approveorder/{orderno}";
            string rejectLink = $"{RegionalizeService.GetBaseUrl("us").TrimEnd('/')}/{ConfigurationManager.AppSettings["portalBaseURL"].ToString().TrimStart('/').TrimEnd('/')}/rejectorder/{orderno}";
            string emailTemplateFile = ConfigurationManager.AppSettings["OrderApproval.TemplateFile"].ToString();
            string subject = $"Mastercard - New Order #{orderno} needs your approval";
            string fromName = ConfigurationManager.AppSettings["OrderApproval.FromName"].ToString();
            string fromAddress = ConfigurationManager.AppSettings["OrderApproval.FromAddress"].ToString();

            string approverEmailAddress = string.Empty;
            string itemRows = "<tr>";
            foreach (RetrieveOrderItemsResult item in approvableItems)
            {
                if (approverEmailAddress != "")
                {
                    approverEmailAddress = "";
                    break;
                }
                else
                {
                    approverEmailAddress = item.ApproverEmailAddress;
                }

                itemRows += $"<td>{item.SKU}</td>";
                itemRows += $"<td>{item.ItemName}</td>";
                itemRows += $"<td>{item.ItemQuantity.ToString()}</td>";
                itemRows += $"<td>${item.ItemPrice.ToString()}</td>";

                if (item.ElectronicDelivery)
                {
                    itemRows += "<td>Electronic Delivery</td>";
                }
                else
                {
                    if (item.VDP)
                    {
                        itemRows += "<td>Variable Data Printing</td>";
                    }
                    else
                    {
                        if (item.Customizations != null && item.Customizations.Value)
                        {
                            itemRows += "<td>Customizable</td>";
                        }
                        else
                        {
                            itemRows += "<td>Mastercard Branded Collateral</td>";
                        }
                    }
                }
            }

            itemRows += "</tr>";

            if (!string.IsNullOrEmpty(shippingAddress.Trim()))
            {
                shippingAddress = $"Shipping Address:<br />{shippingAddress}";
            }

            if (string.IsNullOrWhiteSpace(approverEmailAddress))
            {
                approverEmailAddress = ConfigurationManager.AppSettings["OrderApproval.DefaultApproverAddress"].ToString();
            }

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#CUSTOMER]", customer);
            emailBody = emailBody.Replace("[#ISSUER]", issuer);
            emailBody = emailBody.Replace("[#DOMAIN]", string.IsNullOrEmpty(domain) ? string.Empty : domain);
            emailBody = emailBody.Replace("[#ITEMROWS]", itemRows);
            emailBody = emailBody.Replace("[#SHIPPINGDETAILS]", shippingAddress);
            emailBody = emailBody.Replace("[#APPROVELINK]", approvalLink);
            emailBody = emailBody.Replace("[#REJECTLINK]", rejectLink);
            emailBody = emailBody.Replace("[#PHONE]", phone);
            emailBody = emailBody.Replace("[#EMAIL]", email);
            emailBody = emailBody.Replace("[#ADDRESS]", address);
            emailBody = emailBody.Replace("[#CITY]", city);
            emailBody = emailBody.Replace("[#STATE]", state);
            emailBody = emailBody.Replace("[#ZIP]", zip);

            _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, approverEmailAddress, emailBody, userId, "us", Constants.TrackingTypes.NewOrder, bccAddress: ConfigurationManager.AppSettings["OrderApproval.BCC"]);
        }

        public void SendNewOrderEmail(string orderNo, bool isCustomizedOrder, bool vdpOrder, int userId, bool isRushOrder)
        {
            var emails = _userService.GetPrinterUsers().Select(u => u.Email);
            if (emails.Any())
            {
                string target = $"{SlamConfig.ConfigurationManager.Environment.FrontEndUrl.TrimEnd('/')}/{ConfigurationManager.AppSettings["AGE.MC.MMP.PRINTER.OrderDetail"].TrimStart('/').Replace("{orderid}", orderNo)}";
                string subject = $"Mastercard - New Order #{orderNo}{(isRushOrder ? " (RUSH)" : string.Empty)}";

                string emailTemplateFile = isCustomizedOrder ? ConfigurationManager.AppSettings["NewOrder.TemplateFile"].ToString() : ConfigurationManager.AppSettings["NewOrderNoCustomization.TemplateFile"].ToString();
                string fromName = ConfigurationManager.AppSettings["NewOrder.FromName"].ToString();
                string fromAddress = ConfigurationManager.AppSettings["NewOrder.FromAddress"].ToString();

                string emailBody;
                using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
                {
                    emailBody = reader.ReadToEnd();
                }

                emailBody = emailBody.Replace("[#OrderLink]", target);
                emailBody = emailBody.Replace("[#Rush]", (isRushOrder ? "This was requested to be a RUSH ORDER" : "      "));
                if (isCustomizedOrder)
                {
                    emailBody = emailBody.Replace("[#APPENDVDP]", (vdpOrder ? " AND VDP FTP QUEUE" : "    "));
                }

                if (emails.Any())
                {
                    _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, emails.Aggregate((e1, e2) => e1 + ";" + e2), emailBody, userId, "us", Constants.TrackingTypes.NewOrder, bccAddress: ConfigurationManager.AppSettings["NewOrder.BCC"], isBodyHtml: false);
                }
            }
        }

        public void SendOrderStatusEmail(string email, string orderNo, string oldStatus, string newStatus, string notes, int userId)
        {
            string emailTemplateFile = ConfigurationManager.AppSettings["OrderStatus.TemplateFile"].ToString();
            string subject = $"{ConfigurationManager.AppSettings["OrderStatus.SubjectPrefix"].ToString()} #{orderNo}";
            string fromName = ConfigurationManager.AppSettings["OrderStatus.FromName"].ToString();
            string fromAddress = ConfigurationManager.AppSettings["OrderStatus.FromAddress"].ToString();
            string link = $"{RegionalizeService.GetBaseUrl("us").TrimEnd('/')}/{ConfigurationManager.AppSettings["portalBaseURL"].ToString().TrimStart('/').TrimEnd('/')}/orderdetails/{orderNo}";

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#OLDSTATUS]", oldStatus);
            emailBody = emailBody.Replace("[#NEWSTATUS]", newStatus);
            emailBody = emailBody.Replace("[#NOTES]", notes);
            emailBody = emailBody.Replace("[#LINK]", link);

            _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, email, emailBody, userId, "us", Constants.TrackingTypes.OrderStatus, bccAddress: ConfigurationManager.AppSettings["OrderStatus.BCC"], isBodyHtml: false);
        }

        public void SendOrderStatusEmailForPrinters(string orderNo, string oldStatus, string newStatus, string notes, int userId)
        {
            string emailTemplateFile = ConfigurationManager.AppSettings["OrderStatusForPrinter.TemplateFile"];
            string subject = $"{ConfigurationManager.AppSettings["OrderStatusForPrinter.SubjectPrefix"]} #{orderNo}";
            string fromName = ConfigurationManager.AppSettings["OrderStatusForPrinter.FromName"];
            string fromAddress = ConfigurationManager.AppSettings["OrderStatusForPrinter.FromAddress"];
            string link = $"{RegionalizeService.GetBaseUrl("us").TrimEnd('/')}/{ConfigurationManager.AppSettings["portalBaseURL"].ToString().TrimStart('/').TrimEnd('/')}/orderdetails/{orderNo}";

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#OLDSTATUS]", oldStatus);
            emailBody = emailBody.Replace("[#NEWSTATUS]", newStatus);
            emailBody = emailBody.Replace("[#NOTES]", notes);
            emailBody = emailBody.Replace("[#LINK]", link);

            var emails = _userService.GetPrinterUsers().Select(u => u.Email);
            if (emails.Any())
            {
                _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, emails.Aggregate((e1, e2) => e1 + ";" + e2), emailBody, userId, "us", Constants.TrackingTypes.OrderStatusForPrinter, bccAddress: ConfigurationManager.AppSettings["OrderStatusForPrinter.BCC"], isBodyHtml: false);
            }
        }

        public void SendVdpFtpAccountInformationReceivedEmail(string orderNumber, int userId)
        {
            var emails = _userService.GetPrinterUsers().Select(u => u.Email);
            if (emails.Any())
            {
                string target = $"{SlamConfig.ConfigurationManager.Environment.FrontEndUrl.TrimEnd('/')}/{ConfigurationManager.AppSettings["AGE.MC.MMP.PRINTER.OrderDetail"].TrimStart('/').Replace("{orderid}", orderNumber)}";
                string emailTemplateFile = ConfigurationManager.AppSettings["FtpAccountReceived.TemplateFile"].ToString();
                string subject = $"{ConfigurationManager.AppSettings["FtpAccountReceived.SubjectPrefix"]}{orderNumber}";
                string fromName = ConfigurationManager.AppSettings["FtpAccountReceived.FromName"];
                string fromAddress = ConfigurationManager.AppSettings["FtpAccountReceived.FromAddress"];

                string emailBody;
                using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
                {
                    emailBody = reader.ReadToEnd();
                }

                emailBody = emailBody.Replace("[#OrderLink]", target);
                if (emails.Any())
                {
                    _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, emails.Aggregate((e1, e2) => e1 + ";" + e2), emailBody, userId, "us", Constants.TrackingTypes.FtpAccountReceived, bccAddress: ConfigurationManager.AppSettings["FtpAccountReceived.BCC"], isBodyHtml: false);
                }
            }
        }

        public void SendFtpAccountInformationEmail(string email, string orderNo, int userId)
        {
            string emailTemplateFile = ConfigurationManager.AppSettings["FtpAccount.TemplateFile"];
            string subject = ConfigurationManager.AppSettings["FtpAccount.SubjectPrefix"].Replace("{order_number}", orderNo);
            string fromName = ConfigurationManager.AppSettings["FtpAccount.FromName"].ToString();
            string fromAddress = ConfigurationManager.AppSettings["FtpAccount.FromAddress"].ToString();
            string link = $"{RegionalizeService.GetBaseUrl("us").TrimEnd('/')}/{ConfigurationManager.AppSettings["portalBaseURL"].ToString().TrimStart('/').TrimEnd('/')}/orderdetails/{orderNo}";

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#LINK]", link);

            _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, email, emailBody, userId, "us", Constants.TrackingTypes.FtpAccount, isBodyHtml: false);
        }

        public void SendVdpOrderCompletedEmail(string email, string orderNumber, int userId)
        {
            string target = $"{RegionalizeService.GetBaseUrl("us").TrimEnd('/')}/{ConfigurationManager.AppSettings["portalBaseURL"].ToString().TrimStart('/').TrimEnd('/')}/orderdetails/{orderNumber}";
            string emailTemplateFile = ConfigurationManager.AppSettings["VdpOrderCompleted.TemplateFile"];
            string subject = ConfigurationManager.AppSettings["VdpOrderCompleted.SubjectPrefix"].Replace("{order_number}", orderNumber);
            string fromName = ConfigurationManager.AppSettings["VdpOrderCompleted.FromName"].ToString();
            string fromAddress = ConfigurationManager.AppSettings["VdpOrderCompleted.FromAddress"].ToString();

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#OrderLink]", target);

            _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, email, emailBody, userId, "us", Constants.TrackingTypes.VdpOrderCompleted, isBodyHtml: false);
        }

        public void SendVdpAccountNotificationReminderEmail(string orderNumber, string frequencyInDays, int userId)
        {
            var emails = _userService.GetPrinterUsers().Select(u => u.Email);
            if (emails.Any())
            {
                string emailTemplateFile = ConfigurationManager.AppSettings["VdpSendFtpAccountReminder.TemplateFile"].ToString();
                string subject = ConfigurationManager.AppSettings["VdpSendFtpAccountReminder.Subject"].Replace("{orderNumber}", orderNumber);
                string fromName = ConfigurationManager.AppSettings["VdpSendFtpAccountReminder.FromName"].ToString();
                string fromAddress = ConfigurationManager.AppSettings["VdpSendFtpAccountReminder.FromAddress"].ToString();
                string target = $"{SlamConfig.ConfigurationManager.Environment.FrontEndUrl.TrimEnd('/')}/{ConfigurationManager.AppSettings["AGE.MC.MMP.PRINTER.OrderDetail"].TrimStart('/').Replace("{orderid}", orderNumber)}";

                string emailBody;
                using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
                {
                    emailBody = reader.ReadToEnd();
                }

                emailBody = emailBody.Replace("[#ORDER]", orderNumber);
                emailBody = emailBody.Replace("[#DAYS]", frequencyInDays);
                emailBody = emailBody.Replace("[#OrderLink]", target);
                if (emails.Any())
                {
                    _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, emails.Aggregate((e1, e2) => e1 + ";" + e2), emailBody, userId, "us", Constants.TrackingTypes.VdpSendFtpAccountReminder, bccAddress: ConfigurationManager.AppSettings["VdpSendFtpAccountReminder.BCC"], isBodyHtml: false);
                }
            }
        }

        public void SendVdpAcceptDataFileReminderEmail(string orderNumber, string frequencyInDays, int userId)
        {
            var emails = _userService.GetPrinterUsers().Select(u => u.Email);
            if (emails.Any())
            {
                string emailTemplateFile = ConfigurationManager.AppSettings["VdpAcceptDataFileReminder.TemplateFile"].ToString();
                string subject = ConfigurationManager.AppSettings["VdpAcceptDataFileReminder.Subject"].Replace("{orderNumber}", orderNumber);
                string fromName = ConfigurationManager.AppSettings["VdpAcceptDataFileReminder.FromName"].ToString();
                string fromAddress = ConfigurationManager.AppSettings["VdpAcceptDataFileReminder.FromAddress"].ToString();
                string target = $"{SlamConfig.ConfigurationManager.Environment.FrontEndUrl.TrimEnd('/')}/{ConfigurationManager.AppSettings["AGE.MC.MMP.PRINTER.OrderDetail"].TrimStart('/').Replace("{orderid}", orderNumber)}";

                string emailBody;
                using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
                {
                    emailBody = reader.ReadToEnd();
                }

                emailBody = emailBody.Replace("[#ORDER]", orderNumber);
                emailBody = emailBody.Replace("[#DAYS]", frequencyInDays);
                emailBody = emailBody.Replace("[#OrderLink]", target);

                if (emails.Any())
                {
                    _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, emails.Aggregate((e1, e2) => e1 + ";" + e2), emailBody, userId, "us", Constants.TrackingTypes.VdpSendFtpAccountReminder, bccAddress: ConfigurationManager.AppSettings["VdpSendFtpAccountReminder.BCC"], isBodyHtml: false);
                }
            }
        }

        public void SendVdpRetrieveFtpAccountReminderEmail(string orderNumber, string email, string firstName, string lastName, string frequencyInDays, int userId)
        {
            string target = $"{RegionalizeService.GetBaseUrl("us").TrimEnd('/')}/{ConfigurationManager.AppSettings["portalBaseURL"].ToString().TrimStart('/').TrimEnd('/')}/orderdetails/{orderNumber}";
            string emailTemplateFile = ConfigurationManager.AppSettings["VdpRetrieveFtpAccountReminder.TemplateFile"].ToString();
            string subject = ConfigurationManager.AppSettings["VdpRetrieveFtpAccountReminder.Subject"].Replace("{orderNumber}", orderNumber);
            string fromName = ConfigurationManager.AppSettings["VdpRetrieveFtpAccountReminder.FromName"].ToString();
            string fromAddress = ConfigurationManager.AppSettings["VdpRetrieveFtpAccountReminder.FromAddress"].ToString();

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#OrderLink]", target);
            emailBody = emailBody.Replace("[#DAYS]", frequencyInDays);
            emailBody = emailBody.Replace("[#ORDER]", orderNumber);

            _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, email, emailBody, userId, "us", Constants.TrackingTypes.VdpRetrieveFtpAccountReminder, bccAddress: ConfigurationManager.AppSettings["VdpRetrieveFtpAccountReminder.BCC"], isBodyHtml: false);
        }

        public void SendEmailSourceUpdateNotification(IEnumerable<IDictionary<string, string>> sourceUpdate, string toEmail, string region, string userName = null, string language = null)
        {
            string _region = (string.IsNullOrEmpty(region) ? RegionalizeService.DefaultRegion : region.Trim());
            string _language = string.IsNullOrEmpty(language) ? RegionalizeService.DefaultLanguage : language.Trim();

            string emailTemplateFile = RegionalizeService.RegionalizeTemplate(ConfigurationManager.AppSettings["SourceUpdateNotification.TemplateFile"], _region, _language);
            string emailSubject = RegionalizeService.RegionalizeSetting("SourceUpdateNotification.Subject", _region);
            string emailFromAddress = RegionalizeService.RegionalizeSetting("SourceUpdateNotification.FromAddress", _region);
            string emailFromName = RegionalizeService.RegionalizeSetting("SourceUpdateNotification.FromName", _region);
            string bannerPath = RegionalizeService.RegionalizeSetting("SourceUpdateNotification.bannerPath", _region);
            string dynamicSubSectionTemplate = ConfigurationManager.AppSettings["SourceUpdateNotification.SubSectionTemplateFile"].ToString();
            string emailToAddress = toEmail;

            string body;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("[#HeaderImageUrl]", GetEmailTemplateHeaderImageUrl(_region));
            body = body.Replace("[#Subject]", emailSubject);
            body = body.Replace("[#UserFirstName]", userName ?? "User");
            body = body.Replace("[#DynamicContent]", GetDynamicContent(dynamicSubSectionTemplate, sourceUpdate, "Notification"));
            body = body.Replace("[#CurrentYear]", DateTime.Now.Year.ToString());

            _mailDispatcherService.CreateMailDispatcher(emailSubject, emailFromAddress, emailFromName, emailToAddress, body, 0, _region, Constants.TrackingTypes.SourceUpdateNotification, addTrackingPixelUrl: true);
        }

        /// <summary>
        /// this method generates nested token with suport to render based on lists. 
        /// </summary>
        /// <param name="tokensToLoopThrough">Is a list of dictionaries containg the tokens to be replaced in each Iterarion of the loop.</param>
        /// <returns></returns>
        private string GetDynamicContent(string subSectionTemplate, IEnumerable<IDictionary<string, string>> tokensToLoopThrough, string forEachTag)
        {
            string section;
            //Retrieves the content from the template file.
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], subSectionTemplate)))
            {
                section = reader.ReadToEnd();
            }
            //Gets parts of the subsection that will be repeated
            var tokenName = $"each:{forEachTag}";
            var openingTokenPData = new { displacement = $"[#{tokenName}]".Length, offset = section.IndexOf($"[#{tokenName}]") };
            var closingTokenPData = new { displacement = $"[/#{tokenName}]".Length, offset = section.IndexOf($"[/#{tokenName}]") };
            var repeatSubSection = section.Substring(openingTokenPData.offset + openingTokenPData.displacement, closingTokenPData.offset - openingTokenPData.offset - openingTokenPData.displacement);

            //aggregates indivial elements using template
            var subSectionBody = tokensToLoopThrough.Aggregate(//goes through each set of tokens to add
                        new StringBuilder(), (finalString, individualItem) =>
                        {
                            var individualIteration = individualItem.Where(tkv => !string.IsNullOrEmpty(tkv.Value)).Aggregate(
                                new StringBuilder(repeatSubSection), (iterationResult, singleToken) => iterationResult.Replace(singleToken.Key, singleToken.Value));//replaces each token refrenced in the subsection.
                            return finalString.Append(individualIteration);
                        });

            //Replaces the render section on the supplied body . 
            section = new StringBuilder(section.Substring(0, openingTokenPData.offset)).Append(subSectionBody).Append(section.Substring(closingTokenPData.offset + closingTokenPData.displacement)).ToString();

            return section;
        }

        public void WrongICAEmail(string userInfo, string ica, int userId)
        {
            string emailTemplateFile = ConfigurationManager.AppSettings["WrongICA.TemplateFile"].ToString();
            string subject = ConfigurationManager.AppSettings["WrongICA.Subject"].ToString();
            string fromName = ConfigurationManager.AppSettings["WrongICA.FromName"].ToString();
            string fromAddress = ConfigurationManager.AppSettings["WrongICA.FromAddress"].ToString();
            string[] recipients = ConfigurationManager.AppSettings["WrongICA.ToAddress"].ToString().Split(';');

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#USER_INFO]", userInfo);
            emailBody = emailBody.Replace("[#ICA]", ica);

            if (recipients.Length > 0)
            {
                _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, recipients.Aggregate((e1, e2) => e1 + ";" + e2), emailBody, userId, "us", Constants.TrackingTypes.WrongICA, bccAddress: ConfigurationManager.AppSettings["WrongICA.BCC"], isBodyHtml: false);
            }
        }

        public void SendNotificationEmail(string cid, string issuer, string domain, string ica, string name, string email, int userId)
        {
            string emailTemplateFile = ConfigurationManager.AppSettings["EmailNotification.TemplateFile"].ToString();
            string subject = ConfigurationManager.AppSettings["EmailNotification.Subject"].ToString();
            string fromAddress = ConfigurationManager.AppSettings["EmailNotification.FromAddress"].ToString();
            string fromName = ConfigurationManager.AppSettings["EmailNotification.FromName"].ToString();

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#CID]", string.IsNullOrEmpty(cid) ? " " : cid);
            emailBody = emailBody.Replace("[#ISSUER]", issuer);
            emailBody = emailBody.Replace("[#DOMAIN]", domain);
            emailBody = emailBody.Replace("[#ICA]", string.IsNullOrEmpty(ica) ? " " : ica);
            emailBody = emailBody.Replace("[#NAME]", name);
            emailBody = emailBody.Replace("[#EMAIL]", email);

            _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, ConfigurationManager.AppSettings["EmailNotification.ToAddress"].ToString(), emailBody, userId, "us", Constants.TrackingTypes.EmailNotification, isBodyHtml: false);
        }

        public string GetEmailTemplateHeaderImageUrl(string region, string pathToImage = null)
        {
            return $"{RegionalizeService.GetBaseUrl(region).TrimEnd('/')}/{(region.ToLower() == "us" ? (pathToImage ?? ImageUrls.EmailTemplateHeaderUs.Trim('/')) : (pathToImage ?? ImageUrls.EmailTemplateHeaderGlobal.Trim('/')))}";
        }

        public void SendLeadGenEmail(string email, string issuer, string name, string message, string reportUrl, int userId)
        {
            string emailTemplateFile = ConfigurationManager.AppSettings["LeadGen.TemplateFile"].ToString();
            string subject = ConfigurationManager.AppSettings["LeadGen.Subject"].ToString();
            string fromAddress = ConfigurationManager.AppSettings["LeadGen.FromAddress"].ToString();
            string fromName = ConfigurationManager.AppSettings["LeadGen.FromName"].ToString();

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#SUBJECT]", subject);
            emailBody = emailBody.Replace("[#ISSUER]", issuer);
            emailBody = emailBody.Replace("[#NAME]", name);
            emailBody = emailBody.Replace("[#EMAIL]", email);
            emailBody = emailBody.Replace("[#MESSAGE]", string.IsNullOrEmpty(message) ? " " : message);
            emailBody = emailBody.Replace("[#REPORTURL]", $"{SlamConfig.ConfigurationManager.Environment.FrontEndUrl.Replace("portal", string.Empty).TrimEnd('/')}/{reportUrl.TrimStart('/')}");
            emailBody = emailBody.Replace("[#OPTOUTURL]", ConfigurationManager.AppSettings["LeadGen.OptOutUrl"]);
            emailBody = emailBody.Replace("[#CURRENTYEAR]", DateTime.Now.Year.ToString());

            _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, email, emailBody, userId, "us", Constants.TrackingTypes.LeadGen, isBodyHtml: true, bccAddress: ConfigurationManager.AppSettings["LeadGen.BCC"]);
        }

        public void SendShareReportEmail(string email, string issuer, string name, string phone, string pdfFileName, int userId)
        {
            string emailTemplateFile = ConfigurationManager.AppSettings["ShareReport.TemplateFile"].ToString();
            string subject = ConfigurationManager.AppSettings["ShareReport.Subject"].ToString();
            string fromAddress = ConfigurationManager.AppSettings["ShareReport.FromAddress"].ToString();
            string fromName = ConfigurationManager.AppSettings["ShareReport.FromName"].ToString();

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#SUBJECT]", subject);
            emailBody = emailBody.Replace("[#ISSUERNAME]", issuer);
            emailBody = emailBody.Replace("[#CUSTOMERNAME]", name);
            emailBody = emailBody.Replace("[#PHONE]", phone);
            emailBody = emailBody.Replace("[#EMAIL]", email);

            _mailDispatcherService.CreateMailDispatcher(subject, fromAddress, fromName, ConfigurationManager.AppSettings["ShareReport.ToAddress"].ToString(), emailBody, userId, "us", Constants.TrackingTypes.ShareReport, isBodyHtml: true, attachmentFilename: pdfFileName);
        }

        public void SendGdprAwarenessFollowUpEmail(string toEmail, int userId, string firstName, DateTime expirationDate, string trackingType, string region, string language, DateTime lastLoginDate)
        {
            string _region = (string.IsNullOrEmpty(region) ? RegionalizeService.DefaultRegion : region.Trim());
            string _language = string.IsNullOrEmpty(language) ? RegionalizeService.DefaultLanguage : language.Trim();

            string emailTemplateFile = RegionalizeService.RegionalizeTemplate(ConfigurationManager.AppSettings["ExpirationAwarenessEmail.AwarenessFollowUp.TemplateFile"], _region, _language);
            string emailSubjectPrefix = RegionalizeService.RegionalizeSetting("ExpirationAwarenessEmail.AwarenessFollowUp.SubjectPrefix", _region, _language);
            string emailSubject = RegionalizeService.RegionalizeSetting("ExpirationAwarenessEmail.AwarenessFollowUp.Subject", _region, _language);
            string emailHeaderSubject = RegionalizeService.RegionalizeSetting("ExpirationAwarenessEmail.AwarenessFollowUp.HeaderSubject", _region, _language);
            string emailFromAddress = RegionalizeService.RegionalizeSetting("ExpirationAwarenessEmail.FromAddress", _region);
            string emailFromName = RegionalizeService.RegionalizeSetting("ExpirationAwarenessEmail.FromName", _region);
            string emailToAddress = toEmail;
            string contactUsAddress = RegionalizeService.RegionalizeSetting("ExpirationAwarenessEmail.ContactUsAddress", _region);
            string privacyUrl = RegionalizeService.RegionalizeSetting("ExpirationAwarenessEmail.PrivacyUrl", _region);

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }
            CultureInfo dateLanguage = new CultureInfo(_language);
            string portalBaseUrl = $"{RegionalizeService.GetBaseUrl(_region).TrimEnd('/')}/{ConfigurationManager.AppSettings["portalBaseURL"].ToString().TrimStart('/')}";
            emailBody = emailBody.Replace("[#SubjectPrefix]", emailSubjectPrefix);
            emailBody = emailBody.Replace("[#Subject]", emailHeaderSubject);
            emailBody = emailBody.Replace("[#HeaderImageUrl]", GetEmailTemplateHeaderImageUrl(_region));
            emailBody = emailBody.Replace("[#CurrentYear]", DateTime.Now.Year.ToString());
            emailBody = emailBody.Replace("[#FirstName]", firstName);
            emailBody = emailBody.Replace("[#ExpirationDate]", expirationDate.ToString("MMMM, dd, yyyy", dateLanguage));
            emailBody = emailBody.Replace("[#FrontEndUrl]", portalBaseUrl);
            emailBody = emailBody.Replace("[#ForgotPasswordUrl]", $"{portalBaseUrl}/forgotpassword");
            emailBody = emailBody.Replace("[#PrivacyUrl]", privacyUrl);
            emailBody = emailBody.Replace("[#ContactUsAddresses]", contactUsAddress);
            emailBody = emailBody.Replace("[#LastLoginDate]", lastLoginDate.ToString("MMMM, dd, yyyy", dateLanguage));

            _mailDispatcherService.CreateMailDispatcher($"[{emailSubjectPrefix.ToUpper()}] {emailSubject}", emailFromAddress, emailFromName, emailToAddress, emailBody, userId, _region, trackingType, addTrackingPixelUrl: true);
        }

        public MarketingCenter.Data.Entities.MailDispatcher SendGdprDeletedFollowUpEmail(string toEmail, int userId, string firstName, string region, string language, bool isCreating = false)
        {
            string _region = (string.IsNullOrEmpty(region) ? RegionalizeService.DefaultRegion : region.Trim());
            string _language = string.IsNullOrEmpty(language) ? RegionalizeService.DefaultLanguage : language.Trim();

            string emailTemplateFile = RegionalizeService.RegionalizeTemplate(ConfigurationManager.AppSettings["ExpirationAwarenessEmail.DeletedFollowUp.TemplateFile"], _region, _language);
            string emailSubject = RegionalizeService.RegionalizeSetting("ExpirationAwarenessEmail.DeletedFollowUp.Subject", _region, _language);
            string emailHeaderSubject = RegionalizeService.RegionalizeSetting("ExpirationAwarenessEmail.DeletedFollowUp.HeaderSubject", _region, _language);
            string emailFromAddress = RegionalizeService.RegionalizeSetting("ExpirationAwarenessEmail.FromAddress", _region);
            string emailFromName = RegionalizeService.RegionalizeSetting("ExpirationAwarenessEmail.FromName", _region);
            string emailToAddress = toEmail;
            string contactUsAddress = RegionalizeService.RegionalizeSetting("ExpirationAwarenessEmail.ContactUsAddress", _region);
            string privacyUrl = RegionalizeService.RegionalizeSetting("ExpirationAwarenessEmail.PrivacyUrl", _region);

            string emailBody;
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AWS.Email.TemplateFolder"], emailTemplateFile)))
            {
                emailBody = reader.ReadToEnd();
            }

            emailBody = emailBody.Replace("[#Subject]", emailHeaderSubject);
            emailBody = emailBody.Replace("[#HeaderImageUrl]", GetEmailTemplateHeaderImageUrl(_region));
            emailBody = emailBody.Replace("[#CurrentYear]", DateTime.Now.Year.ToString());
            emailBody = emailBody.Replace("[#FirstName]", firstName);
            emailBody = emailBody.Replace("[#RegisterUrl]", $"{RegionalizeService.GetBaseUrl(_region).TrimEnd('/')}/{ConfigurationManager.AppSettings["portalBaseURL"].ToString().TrimStart('/').TrimEnd('/')}/register");
            emailBody = emailBody.Replace("[#PrivacyUrl]", privacyUrl);
            emailBody = emailBody.Replace("[#ContactUsAddresses]", contactUsAddress);

            if (isCreating)
            {
                return _mailDispatcherService.CreateMailDispatcher(emailSubject, emailFromAddress, emailFromName, emailToAddress, emailBody, userId, _region, Constants.TrackingTypes.GdprDeletedFollowUp, mailDispatcherStatusId: MarketingCenterDbConstants.MailDispatcherStatus.Creating, saveChanges: false);
            }
            else
            {
                return _mailDispatcherService.CreateMailDispatcher(emailSubject, emailFromAddress, emailFromName, emailToAddress, emailBody, userId, _region, Constants.TrackingTypes.GdprDeletedFollowUp, addTrackingPixelUrl: true);
            }
        }

        public void SendUnmatchedPricelessOfferEmail(string unmatchedOffers)
        {
            string emailTemplateFile = _settingsService.GetPricelessOfferEmailUnmatchedTemplate();
            string emailToAddress = _settingsService.GetPricelessOfferEmailUnmatchedToAddress();
            string emailSubject = _settingsService.GetPricelessOfferEmailUnmatchedSubject();
            string emailFromAddress = _settingsService.GetPricelessOfferEmailUnmatchedFromAddress();
            string emailFromName = _settingsService.GetPricelessOfferEmailUnmatchedFromName();

            string emailBody = GetTemplate(emailTemplateFile).Replace("[#UnmatchedOffers]", unmatchedOffers);

            _mailDispatcherService.CreateMailDispatcher(emailSubject, emailFromAddress, emailFromName, emailToAddress, emailBody, 0, RegionalizeService.DefaultRegion, Constants.TrackingTypes.PricelessOfferUnmatched, addTrackingPixelUrl: true);
        }

        public void SendExceptionPricelessOfferEmail(string exceptionMessage)
        {
            string emailTemplateFile = _settingsService.GetPricelessOfferEmailExceptionTemplate();
            string emailToAddress = _settingsService.GetPricelessOfferEmailExceptionToAddress();
            string emailSubject = _settingsService.GetPricelessOfferEmailExceptionSubject();
            string emailFromAddress = _settingsService.GetPricelessOfferEmailExceptionFromAddress();
            string emailFromName = _settingsService.GetPricelessOfferEmailExceptionFromName();

            string emailBody = GetTemplate(emailTemplateFile).Replace("[#ExceptionMessage]", exceptionMessage);

            _mailDispatcherService.CreateMailDispatcher(emailSubject, emailFromAddress, emailFromName, emailToAddress, emailBody, 0, RegionalizeService.DefaultRegion, Constants.TrackingTypes.PricelessOfferException, addTrackingPixelUrl: true);
        }

        public void SendSalesforceExpirationCertificateEmail(DateTime expirationDate)
        {
            string emailTemplateFile = ConfigurationManager.AppSettings["SalesforceExpirationCertificateEmail.TemplateFile"].ToString();
            string emailSubject = ConfigurationManager.AppSettings["SalesforceExpirationCertificateEmail.Subject"].ToString();
            string emailFromAddress = ConfigurationManager.AppSettings["SalesforceExpirationCertificateEmail.FromAddress"].ToString();
            string emailFromName = ConfigurationManager.AppSettings["SalesforceExpirationCertificateEmail.FromName"].ToString();
            string emailToAddress = ConfigurationManager.AppSettings["SalesforceExpirationCertificateEmail.ToAddress"].ToString();

            string emailBody = GetTemplate(emailTemplateFile);
            emailBody = emailBody.Replace("[#Subject]", emailSubject);
            emailBody = emailBody.Replace("[#ExpirationDate]", expirationDate.ToString());

            _mailDispatcherService.CreateMailDispatcher(emailSubject, emailFromAddress, emailFromName, emailToAddress, emailBody, 0, RegionalizeService.DefaultRegion, Constants.TrackingTypes.GdprDeletedFollowUp, addTrackingPixelUrl: true);
        }

        public void SendSalesforceAwarenessExpirationCertificateEmail(DateTime expirationDate)
        {
            string emailTemplateFile = ConfigurationManager.AppSettings["SalesforceAwarenessExpirationCertificate.TemplateFile"].ToString();
            string emailSubject = ConfigurationManager.AppSettings["SalesforceAwarenessExpirationCertificate.Subject"].ToString();
            string emailFromAddress = ConfigurationManager.AppSettings["SalesforceAwarenessExpirationCertificate.FromAddress"].ToString();
            string emailFromName = ConfigurationManager.AppSettings["SalesforceAwarenessExpirationCertificate.FromName"].ToString();
            string emailToAddress = ConfigurationManager.AppSettings["SalesforceAwarenessExpirationCertificate.ToAddress"].ToString();

            string emailBody = GetTemplate(emailTemplateFile);
            emailBody = emailBody.Replace("[#Subject]", emailSubject);
            emailBody = emailBody.Replace("[#ExpirationDate]", expirationDate.ToString());

            _mailDispatcherService.CreateMailDispatcher(emailSubject, emailFromAddress, emailFromName, emailToAddress, emailBody, 0, RegionalizeService.DefaultRegion, Constants.TrackingTypes.GdprDeletedFollowUp, addTrackingPixelUrl: true);
        }
    }
}