﻿using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Services.Data;
using Slam.Cms.Common.Interfaces;
using Slam.Cms.Common.Security;
using Slam.Cms.Configuration;
using System;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Mastercard.MarketingCenter.Services
{
    public class MastercardAuthenticationService : AuthenticationService
    {
        private readonly HttpContextBase _httpContext;
        private readonly ICookieService _cookieService;

        public MastercardAuthenticationService(HttpContextBase httpContext, IPreviewMode previewMode, ICookieService cookieService)
            : base(httpContext, previewMode)
        {
            _httpContext = httpContext;
            _cookieService = cookieService;
        }

        public bool RemoveSessionIdCookie()
        {
            var Session = _httpContext.Session;

            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();

            var sessionState = (SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState");

            if (_cookieService.Get("ASP.NET_SessionId") != null)
            {
                _cookieService.Set("ASP.NET_SessionId", string.Empty, null, DateTime.Now.AddMonths(-20));
            }

            if (!string.IsNullOrEmpty(sessionState.CookieName))
            {
                _cookieService.Set(sessionState.CookieName, string.Empty, null, DateTime.Now.AddMonths(-20));
            }

            return true;
        }

        public Guid GetNewToken(string userName, string password)
        {
            using (var dc = new MasterCardPortalDataContext(ConfigurationManager.Environment.ConnectionStrings["MasterCardMarketingCenter"].ConnectionString))
            {
                var loginToken = new LoginToken()
                {
                    LoginTokenID = Guid.NewGuid(),
                    Username = userName,
                    Password = password,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now
                };

                dc.LoginTokens.InsertOnSubmit(loginToken);
                dc.SubmitChanges();

                return loginToken.LoginTokenID;
            }
        }

        public LoginToken GetToken(string userName)
        {
            using (var dc = new MasterCardPortalDataContext(ConfigurationManager.Environment.ConnectionStrings["MasterCardMarketingCenter"].ConnectionString))
            {
                return dc.LoginTokens.Where(t => t.Username == userName).FirstOrDefault();
            }
        }

        public string[] GetUsernameAndPasswordFromLoginToken(Guid token)
        {
            using (var dc = new MasterCardPortalDataContext(ConfigurationManager.Environment.ConnectionStrings["MasterCardMarketingCenter"].ConnectionString))
            {
                var loginToken = dc.LoginTokens.Where(t => t.LoginTokenID == token).FirstOrDefault();
                if (loginToken == null)
                {
                    return null;
                }

                var usernameAndPassword = new[] { loginToken.Username, loginToken.Password };
                dc.LoginTokens.DeleteOnSubmit(loginToken);
                dc.SubmitChanges();

                return usernameAndPassword;
            }
        }

        public bool SetSlamAdminCookies()
        {
            bool cookiesSet = false;
            if (!string.IsNullOrEmpty(_httpContext.User.Identity.Name) && _httpContext.User.IsInFullAdminRole(false))
            {
                CreateOrUpdateSlamAuthenticationCookie(_httpContext.User.Identity.Name);
                cookiesSet = true;
            }
            else
            {
                RemoveSlamAuthenticationCookie();
                RemoveSlamPreviewModeCookie();
            }

            return cookiesSet;
        }
    }
}