using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.DTOs;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Slam.Cms.Common;
using Slam.Cms.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class ContentItemService : IContentItemService
    {
        private readonly ContentItemRepository _contentItemRepository;
        private readonly SlamContext _slamContext;
        private readonly IEnumerable<IDefaultValueCalculator> _defaultValueCalculators;
        private readonly MigratedContentRepository _migratedContentRepository;
        private readonly ISettingsService _settingsService;
        private readonly IOfferRepository _offerRepository;

        public ContentItemService(
            ContentItemRepository contentItemRepository,
            SlamContext slamContext,
            IEnumerable<IDefaultValueCalculator> defaultValueCalculators,
            MigratedContentRepository migratedContentRepository,
            ISettingsService settingsService,
            IOfferRepository offerRepository)
        {
            _contentItemRepository = contentItemRepository;
            _slamContext = slamContext;
            _defaultValueCalculators = defaultValueCalculators;
            _migratedContentRepository = migratedContentRepository;
            _settingsService = settingsService;
            _offerRepository = offerRepository;
        }

        public IEnumerable<ContentItemCloneToRegion> GetApplicableCloneToRegions(string contentItemId)
        {
            return _contentItemRepository.GetApplicableCloneToRegions(contentItemId);
        }

        public bool IsApplicableCrossBorderRegion(string contentItemId, string regionId)
        {
            return _contentItemRepository.IsApplicableCrossBorderRegion(contentItemId, regionId);
        }

        public IEnumerable<string> GetApplicableCloneToRegionsNames(string contentItemId)
        {
            var applicableRegions = _contentItemRepository.GetApplicableCloneToRegions(contentItemId);
            var applicableRegionNames = _contentItemRepository.GetRegionNames(applicableRegions);

            return applicableRegionNames;
        }

        public void DeleteOnlyDraft(ContentItemBase contentItem, int userId)
        {
            var statusId = contentItem.StatusID;

            if (!ContentHasMultipleVersions(contentItem) && statusId == Status.DraftOnly)
            {
                _contentItemRepository.UpdateStatus(contentItem, Status.Deleted, userId);
            }
            else
            {
                _contentItemRepository.UpdateStatus(contentItem.ContentItemId.GetAsDraftContentItemId().GetAsLiveContentItemId(), Status.Published, userId);
                _contentItemRepository.UpdateStatus(contentItem.ContentItemId.GetAsDraftContentItemId().GetAsDraftContentItemId(), Status.Deleted, userId);
            }
        }

        public bool ContentHasMultipleVersions(ContentItemBase contentItem)
        {
            return _contentItemRepository.GetAllByPrimaryContentItemId(contentItem.PrimaryContentItemId).Any(singleCI => singleCI.StatusID.Equals(Status.PublishedAndDraft));
        }

        public bool ContentHasLiveVersion(ContentItemBase contentItem)
        {
            return _contentItemRepository.GetAllByPrimaryContentItemId(contentItem.PrimaryContentItemId).Any(singleCI =>
                singleCI.StatusID.Equals(Status.Published) || singleCI.StatusID.Equals(Status.PublishedAndDraft));
        }

        public ContentItemBase GetPublishedContentItem(string contentItemId)
        {
            var publishedContentItemData = GetContentItem(contentItemId.GetAsLiveContentItemId());
            var possibleLiveVersionStatus = new int[] { Status.Published, Status.PublishedAndDraft };
            return publishedContentItemData != null && possibleLiveVersionStatus.Contains(publishedContentItemData.StatusID) ? publishedContentItemData : null;
        }

        public ContentItemBase TryToGetDraft(string contentItemId)
        {
            return _contentItemRepository.GetOnlyDraftById(contentItemId);
        }

        public IEnumerable<BusinessOwnerDTO> GetBusinessOwnerContentAboutToExpire(string regionId)
        {
            return _contentItemRepository.GetContentAboutToExpire(_settingsService.GetExpiredContentNotificationDays())
                                         .Where(cae => cae.RegionId.Trim().ToLower() == regionId.Trim().ToLower())
                                         .GroupBy(cae => cae.BusinessOwner)
                                         .Select(cae => new BusinessOwnerDTO
                                         {
                                             UserId = cae.Key?.UserId ?? 0,
                                             Name = cae.Key?.FullName,
                                             Email = cae.Key?.Email,
                                             IsBlocked = cae.Key?.IsBlocked ?? false,
                                             IsDisabled = cae.Key?.IsDisabled ?? false,
                                             ContentItems = cae.Select(c => new ContentItemDTO
                                             {
                                                 ContentItemId = c.ContentItemId,
                                                 ContentTypeId = c.ContentTypeId,
                                                 Title = c.Title,
                                                 ExpirationDate = c.ExpirationDate
                                             })
                                         });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId">Id of of the Business Owner.</param>
        /// <param name="reviewState"> The Id of the current Review State the asset to be retrieved should have.</param>
        /// <param name="expirationWindowInDays">The mamixum amount of days in which the retrieved assets will expire. If null, expiration date won't be taken into account as a filter.</param>
        /// <param name="onlyPublished">if ture only pulished asset will be retrieved, else all asset will be retrieved.</param>
        /// <returns></returns>
        public IEnumerable<ContentItem> GetUserOwnedAssetContentItems(
            int userId,
            int reviewState = MarketingCenterDbConstants.ExpirationReviewStates.AllAssetsWithAStateAvailable,
            int? expirationWindowInDays = null,
            bool onlyPublished = true,
            string filterContentItemId = "")
        {
            var query = _slamContext.CreateQuery()
                .Cache(SlamQueryCacheBehavior.NoCache)
                .FilterContentTypes<AssetDTO, DownloadableAssetDTO>()
                .IncludeAllRegions()
                .GetNonExpired()
                .Filter($@"(Asset.BusinessOwnerID = {userId} or DownloadableAsset.BusinessOwnerID = {userId})")
                .FilterStatus(onlyPublished ? FilterStatus.LiveOrLiveAndDraft : FilterStatus.LatestVersion)
                .Filter($"(ReviewStateId {(reviewState == MarketingCenterDbConstants.ExpirationReviewStates.AllAssetsWithAStateAvailable ? $"<> {MarketingCenterDbConstants.ExpirationReviewStates.NotAvailable}" : $"= {reviewState}")})");

            if (!string.IsNullOrWhiteSpace(filterContentItemId))
            {
                query = query.FilterContentItemId(filterContentItemId);
            }

            return query.OrderBy("ExpirationDate").Get()
                        .Where(ct => ct.ExpirationDate.HasValue &&
                                     (!expirationWindowInDays.HasValue ||
                                     (ct.ExpirationDate.Value - DateTime.Today).Days <= expirationWindowInDays.Value))
                        .ToList();
        }

        /// <summary>
        /// Returns Live Content ITems of the Types AssetFullWidth, Asset, DownloadableAsset, OrderableAsset
        /// </summary>
        /// <param name="contentItemId">The Content Item IDs</param>
        /// <returns>Content items that are only Live or  Live and Draft</returns>
        public ContentItem GetAssetContentItem(string contentItemId, FilterStatus filterStatus = FilterStatus.LiveOrLiveAndDraft)
        {
            var primaryContentId = contentItemId.GetAsLiveContentItemId();
            return _slamContext.CreateQuery().FilterContentItemId(primaryContentId)
                    .FilterContentTypes<AssetFullWidthDTO, AssetDTO, DownloadableAssetDTO, OrderableAssetDTO>()
                    .FilterStatus(filterStatus)
                    .Get()
                    .FirstOrDefault();
        }

        /// <summary>
        /// Get A list of "Assets" Which includes ssetFullWidth, Asset, DownloadableAsset, OrderableAsset Asset Types
        /// </summary>
        /// <param name="contentItemIds">the List of Id's that belongs to the desired ContentItems</param>
        /// <param name="filterStatus">Optional: The default value is "LiveOrLiveAndDraft"</param>
        /// <returns></returns>
        public IEnumerable<ContentItem> GetAssetContentItems(IEnumerable<string> contentItemIds, FilterStatus filterStatus = FilterStatus.LiveOrLiveAndDraft)
        {
            return _slamContext.CreateQuery()
                    .FilterContentTypes<AssetFullWidthDTO, AssetDTO, DownloadableAssetDTO, OrderableAssetDTO>()
                    .IncludeAllRegions()
                    .GetNonExpired()
                    .FilterContentItemId(contentItemIds != null ? contentItemIds.Select(x => x.GetAsLiveContentItemId()).ToArray() : null)
                    .FilterStatus(filterStatus)
                    .OrderBy("ExpirationDate")
                    .Cache(SlamQueryCacheBehavior.NoCache)
                    .Get()
                    .ToList();
        }

        /// <summary>
        /// Updates both draft and published version to a status provided via <param>statusId</param>.
        /// </summary>
        /// <param name="contentItem">Draft or published version.</param>
        /// <param name="statusId">Status id to update to.</param>
        public void UpdateStatus(ContentItemBase contentItem, int statusId, int userId)
        {
            // updating both published and draft
            _contentItemRepository.UpdateStatus(contentItem, statusId, userId);

            var otherContentItemId = contentItem.ContentItemId.IsDraftContentItemId() ?
                contentItem.ContentItemId.GetAsLiveContentItemId() :
                contentItem.ContentItemId.GetAsDraftContentItemId();

            var otherContentItem = _contentItemRepository.GetById(otherContentItemId);
            if (otherContentItem != null)
            {
                _contentItemRepository.UpdateStatus(otherContentItem, statusId, userId);
            }
        }

        public void PopulateComputedDefaultValues(IDictionary<string, object> data, IEnumerable<ContentTypeFieldDefinition> contentTypeFieldDefs, Func<ContentTypeFieldDefinition, bool> filterFields = null, IDictionary<string, object> previousData = null)
        {
            var fields = filterFields == null ? contentTypeFieldDefs : contentTypeFieldDefs.Where(fd => filterFields(fd));

            foreach (var calculator in _defaultValueCalculators)
            {
                calculator.CalculateDefault(fields, data, previousData);
            }
        }

        /// <summary>
        /// This method defaults required fields for a suggestion. Ideally this information should be provided by user and not hardcoded.
        /// </summary>
        /// <param name="data"></param>
        public void PopulateDefaultValuesForSuggestion(IDictionary<string, object> data, int userId)
        {
            data[MarketingCenterDbConstants.FieldNames.EnableFavoriting] = 0;
            data[MarketingCenterDbConstants.FieldNames.NotifyWhenSourceChanges] = 0;
            data[MarketingCenterDbConstants.FieldNames.ThumbnailImage] = string.Empty;
            data[MarketingCenterDbConstants.FieldNames.BusinessOwnerId] = userId;
        }

        public IEnumerable<ContentItemSegmentation> GetSegmentation(string contentItemId)
        {
            return _contentItemRepository.GetSegmentation(contentItemId);
        }

        public IEnumerable<MarketingCenter.Data.Entities.Tag> GetTags(string contentItemId)
        {
            return _contentItemRepository.GetTags(contentItemId);
        }

        public AssetFullWidth GetAssetFullWidth(string contentItemId)
        {
            return _contentItemRepository.GetAssetFullWidth(contentItemId);
        }

        public Asset GetAsset(string contentItemId)
        {
            return _contentItemRepository.GetAsset(contentItemId);
        }

        public OrderableAsset GetOrderableAsset(string contentItemId)
        {
            return _contentItemRepository.GetOrderableAsset(contentItemId);
        }

        public DownloadableAsset GetDownloadableAsset(string contentItemId)
        {
            return _contentItemRepository.GetDownloadableAsset(contentItemId);
        }

        public Program GetProgram(string contentItemId)
        {
            return _contentItemRepository.GetProgram(contentItemId);
        }

        public Webinar GetWebinar(string contentItemId)
        {
            return _contentItemRepository.GetWebinar(contentItemId);
        }

        public ContentItemBase GetContentItem(string contentItemId)
        {
            return _contentItemRepository.GetById(contentItemId);
        }

        public IEnumerable<string> GetContentItemRelatedItems(string contentItemId)
        {
            return _contentItemRepository.GetContentItemRelatedItems(contentItemId).Select(ciri => ciri.RelatedContentItemId);
        }

        public ContentItem CheckMigratedContentItem(string id)
        {
            var migratedContent = _migratedContentRepository.GetMigratedContent(id);
            if (migratedContent == null)
            {
                return null;
            }

            return _slamContext.CreateQuery().FilterContentItemId(migratedContent.Id).Get().FirstOrDefault();
        }

        public string GetOriginalContentItemId(IDictionary<string, object> itemData)
        {
            if (itemData == null || !itemData.ContainsKey(MarketingCenterDbConstants.FieldNames.OriginalContentItemId))
            {
                return null;
            }

            return (string)itemData[MarketingCenterDbConstants.FieldNames.OriginalContentItemId];
        }

        public void SetListItemTrackingFields(IDictionary<string, object> contentData, List<FieldDefinition> fieldDefinitions, int userId)
        {
            var createdDateTrackingField = fieldDefinitions.FirstOrDefault(o => o.FieldTypeCode == FieldType.TrackingCreatedDate);
            var updateTrackingField = fieldDefinitions.FirstOrDefault(o => o.FieldTypeCode == FieldType.TrackingUpdatedDate);
            var createdByTrackingField = fieldDefinitions.FirstOrDefault(o => o.FieldTypeCode == FieldType.TrackingCreatedBy);
            var modifiedByTrackingField = fieldDefinitions.FirstOrDefault(o => o.FieldTypeCode == FieldType.TrackingModifiedBy);

            if (createdDateTrackingField != null && updateTrackingField != null)
            {
                var date = DateTime.Now;
                contentData[createdDateTrackingField.Name] = date;
                contentData[updateTrackingField.Name] = date;
            }

            if (createdByTrackingField != null)
            {
                contentData[createdByTrackingField.Name] = userId;
            }

            if (modifiedByTrackingField != null)
            {
                contentData[modifiedByTrackingField.Name] = userId;
            }
        }

        public void UpdateReviewState(string contentItemId, int state = 0, bool updateAlternateVersion = true)
        {
            var contentItem = _contentItemRepository.GetById(contentItemId);
            if (contentItem != null)
            {
                UpdateReviewState(contentItem, state, updateAlternateVersion);
            }
        }

        public void UpdateReviewState(ContentItemBase contentItem, int state = 0, bool updateAlternateVersion = true)
        {
            if (contentItem != null)
            {
                _contentItemRepository.UpdateReviewState(contentItem, state);
                if (updateAlternateVersion)
                {
                    var hasAlternateVersion = ContentHasMultipleVersions(contentItem);
                    var alternateVersionsID = hasAlternateVersion && contentItem.ContentItemId.IsDraftContentItemId() ? contentItem.ContentItemId.GetAsLiveContentItemId() : contentItem.ContentItemId.GetAsDraftContentItemId();
                    var alternateVersion = _contentItemRepository.GetById(alternateVersionsID);
                    if (alternateVersion != null)
                    {
                        _contentItemRepository.UpdateReviewState(alternateVersion, state);
                    }
                }

                _contentItemRepository.Commit();
            }
        }

        public IEnumerable<Program> GetProgramsByPrintedOrderPeriodEndDate(string regionId, DateTime fromDate, DateTime toDate)
        {
            return _contentItemRepository.GetAllPrograms()
                                         .Where(p => p.ContentItem.RegionId.Trim().ToLower() == regionId.Trim().ToLower()
                                                  && (p.ContentItem.StatusID == Status.Published || p.ContentItem.StatusID == Status.PublishedAndDraft)
                                                  && !p.ContentItemId.EndsWith("-p")
                                                  && (p.PrintedOrderPeriodEndDate > fromDate && p.PrintedOrderPeriodEndDate < toDate)
                                                  && p.ContentItem.ExpirationDate > fromDate);
        }

        public IEnumerable<ContentItemDTO> GetProgramsByPrintedOrderPeriodEndDateAsContentItemDTOs(string regionId)
        {
            return GetProgramsByPrintedOrderPeriodEndDate(regionId,
                                                          DateTime.Now.Date,
                                                          DateTime.Now.AddDays(_settingsService.GetExpiredContentNotificationDays()))
                   .Select(p => ProgramAsContentItemDTO(p));
        }

        private static ContentItemDTO ProgramAsContentItemDTO(Program program)
        {
            return new ContentItemDTO
            {
                ContentItemId = program.ContentItemId,
                Title = program.Title,
                ExpirationDate = program.PrintedOrderPeriodEndDate.Value
            };
        }

        public IEnumerable<ContentReportDTO> GetContents(DateTime startDate, DateTime endDate, string title, string regionId = null)
        {
            endDate = endDate.Date.AddDays(1);
            var contentItems = _contentItemRepository.GetContentItems()
                                                     .Where(ci => ci.CreatedDate >= startDate && ci.CreatedDate < endDate &&
                                                                 (ci.StatusID == Status.Published || ci.StatusID == Status.PublishedAndDraft));

            #region Assets

            var assetsQuery = from asset in _contentItemRepository.GetAssets()
                              join contentItem in contentItems on asset.ContentItemId equals contentItem.ContentItemId
                              select new ContentReportDTO
                              {
                                  ContentType = contentItem.ContentType.Title,
                                  Title = asset.Title,
                                  RegionName = contentItem.Region.Name,
                                  CreationDate = contentItem.CreatedDate,
                                  CreatedBy = contentItem.CreatedByUser.FullName,
                                  LastUpdatedDate = contentItem.ModifiedDate,
                                  StatusName = contentItem.Status.Name,
                                  BusinessOwner = asset.BusinessOwner != null ? asset.BusinessOwner.FullName : null,
                                  LiveContentUrl = contentItem.FrontEndUrl,
                                  ExpirationDate = contentItem.ExpirationDate.HasValue ? contentItem.ExpirationDate : null,
                                  RegionId = contentItem.RegionId,
                                  ContentItemId = contentItem.ContentItemId
                              };
            #endregion

            #region AssetsFullWidth

            var fullWidthAssetsQuery = from assetFullWidth in _contentItemRepository.GetAssetsFullWidth()
                                       join contentItem in contentItems on assetFullWidth.ContentItemId equals contentItem.ContentItemId
                                       select new ContentReportDTO
                                       {
                                           ContentType = contentItem.ContentType.Title,
                                           Title = assetFullWidth.Title,
                                           RegionName = contentItem.Region.Name,
                                           CreationDate = contentItem.CreatedDate,
                                           CreatedBy = contentItem.CreatedByUser.FullName,
                                           LastUpdatedDate = contentItem.ModifiedDate,
                                           StatusName = contentItem.Status.Name,
                                           BusinessOwner = assetFullWidth.BusinessOwner != null ? assetFullWidth.BusinessOwner.FullName : null,
                                           LiveContentUrl = contentItem.FrontEndUrl,
                                           ExpirationDate = contentItem.ExpirationDate.HasValue ? contentItem.ExpirationDate : null,
                                           RegionId = contentItem.RegionId,
                                           ContentItemId = contentItem.ContentItemId
                                       };
            #endregion

            #region DownloadableAssets

            var downloadableAssetsQuery = from downloadableAsset in _contentItemRepository.GetDownloadableAssets()
                                          join contentItem in contentItems on downloadableAsset.ContentItemId equals contentItem.ContentItemId
                                          select new ContentReportDTO
                                          {
                                              ContentType = contentItem.ContentType.Title,
                                              Title = downloadableAsset.Title,
                                              RegionName = contentItem.Region.Name,
                                              CreationDate = contentItem.CreatedDate,
                                              CreatedBy = contentItem.CreatedByUser.FullName,
                                              LastUpdatedDate = contentItem.ModifiedDate,
                                              StatusName = contentItem.Status.Name,
                                              BusinessOwner = downloadableAsset.BusinessOwner != null ? downloadableAsset.BusinessOwner.FullName : null,
                                              LiveContentUrl = contentItem.FrontEndUrl,
                                              ExpirationDate = contentItem.ExpirationDate.HasValue ? contentItem.ExpirationDate : null,
                                              RegionId = contentItem.RegionId,
                                              ContentItemId = contentItem.ContentItemId
                                          };
            #endregion

            #region OrderableAssets

            var orderableAssetsQuery = from orderableAsset in _contentItemRepository.GetOrderableAssets()
                                       join contentItem in contentItems on orderableAsset.ContentItemId equals contentItem.ContentItemId
                                       select new ContentReportDTO
                                       {
                                           ContentType = contentItem.ContentType.Title,
                                           Title = orderableAsset.Title,
                                           RegionName = contentItem.Region.Name,
                                           CreationDate = contentItem.CreatedDate,
                                           CreatedBy = contentItem.CreatedByUser.FullName,
                                           LastUpdatedDate = contentItem.ModifiedDate,
                                           StatusName = contentItem.Status.Name,
                                           BusinessOwner = orderableAsset.BusinessOwner != null ? orderableAsset.BusinessOwner.FullName : null,
                                           LiveContentUrl = contentItem.FrontEndUrl,
                                           ExpirationDate = contentItem.ExpirationDate.HasValue ? contentItem.ExpirationDate : null,
                                           RegionId = contentItem.RegionId,
                                           ContentItemId = contentItem.ContentItemId
                                       };
            #endregion

            #region Pages

            var pagesQuery = from page in _contentItemRepository.GetPages()
                             join contentItem in contentItems on page.ContentItemId equals contentItem.ContentItemId
                             select new ContentReportDTO
                             {
                                 ContentType = contentItem.ContentType.Title,
                                 Title = page.Title,
                                 RegionName = contentItem.Region.Name,
                                 CreationDate = contentItem.CreatedDate,
                                 CreatedBy = contentItem.CreatedByUser.FullName,
                                 LastUpdatedDate = contentItem.ModifiedDate,
                                 StatusName = contentItem.Status.Name,
                                 BusinessOwner = null,
                                 LiveContentUrl = contentItem.FrontEndUrl,
                                 ExpirationDate = contentItem.ExpirationDate.HasValue ? contentItem.ExpirationDate : null,
                                 RegionId = contentItem.RegionId,
                                 ContentItemId = contentItem.ContentItemId
                             };
            #endregion

            #region Programs

            var programsQuery = from programs in _contentItemRepository.GetPrograms()
                                join contentItem in contentItems on programs.ContentItemId equals contentItem.ContentItemId
                                select new ContentReportDTO
                                {
                                    ContentType = contentItem.ContentType.Title,
                                    Title = programs.Title,
                                    RegionName = contentItem.Region.Name,
                                    CreationDate = contentItem.CreatedDate,
                                    CreatedBy = contentItem.CreatedByUser.FullName,
                                    LastUpdatedDate = contentItem.ModifiedDate,
                                    StatusName = contentItem.Status.Name,
                                    BusinessOwner = programs.BusinessOwner != null ? programs.BusinessOwner.FullName : null,
                                    LiveContentUrl = contentItem.FrontEndUrl,
                                    ExpirationDate = contentItem.ExpirationDate.HasValue ? contentItem.ExpirationDate : null,
                                    RegionId = contentItem.RegionId,
                                    ContentItemId = contentItem.ContentItemId
                                };
            #endregion

            #region Offers
            var offersQuery = from offers in regionId.IsNullOrEmpty() ? _offerRepository.GetAll() : _offerRepository.GetAll().FilterByRegion(regionId)
                              select new ContentReportDTO
                              {
                                  ContentType = offers.ContentItem.ContentType.Title,
                                  Title = offers.Title,
                                  RegionName = offers.ContentItem.Region.Name,
                                  CreationDate = offers.ContentItem.CreatedDate,
                                  CreatedBy = offers.ContentItem.CreatedByUser.FullName,
                                  LastUpdatedDate = offers.ContentItem.ModifiedDate,
                                  StatusName = offers.ContentItem.Status.Name,
                                  BusinessOwner = offers.BusinessOwner != null ? offers.BusinessOwner.FullName : null,
                                  LiveContentUrl = offers.ContentItem.FrontEndUrl,
                                  ExpirationDate = offers.ContentItem.ExpirationDate.HasValue ? offers.ContentItem.ExpirationDate : null,
                                  RegionId = offers.ContentItem.RegionId,
                                  ContentItemId = offers.ContentItemId
                              };
            #endregion

            var query = assetsQuery
                .Concat(fullWidthAssetsQuery)
                .Concat(downloadableAssetsQuery)
                .Concat(orderableAssetsQuery)
                .Concat(pagesQuery)
                .Concat(programsQuery);

            if (!string.IsNullOrEmpty(regionId))
            {
                query = query.Where(q => q.RegionId.Equals(regionId));
            }

            //Offers are already filtered by region, this includes crossborder.
            query = query.Concat(offersQuery);

            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(q => q.Title.Contains(title));
            }

            return query;
        }

        public void PopulateContentItemBaseValues(Dictionary<string, object> newData, string idValue)
        {
            var contentItem = _contentItemRepository.GetContentItems().FirstOrDefault(x => x.ContentItemId.Equals(idValue));
            newData.Add(MarketingCenterDbConstants.ContentItemBase.ModifiedByUserID, contentItem.ModifiedByUserId);
            newData.Add(MarketingCenterDbConstants.ContentItemBase.ModifiedDate, contentItem.ModifiedDate);
        }
    }
}