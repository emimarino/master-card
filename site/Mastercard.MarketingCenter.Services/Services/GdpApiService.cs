﻿using Mastercard.MarketingCenter.Common.Infrastructure;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Interfaces.GDP;
using Mastercard.MarketingCenter.Data.Metadata.GDP;
using Mastercard.MarketingCenter.Domo.Services;
using Mastercard.MarketingCenter.DTO.GDP;
using Mastercard.MarketingCenter.Services.Interfaces;
using Newtonsoft.Json;
using Pulsus;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Mastercard.MarketingCenter.Services
{
    public class GdpApiService : IGdpApiService
    {
        private readonly string _serviceFunctionCode;
        private readonly IUserPrivacyDataService _userPrivacyDataService;
        private readonly IGdpRequestRepository _gdpRequestRepository;
        private readonly IUserConsentRepository _userConsentRepository;
        private readonly IGdpExternalRepository _gdpExternalRepository;
        private readonly IUserRepository _userRepository;
        readonly private IEmailService _emailService;
        readonly private ISettingsService _settingsService;
        private readonly IPendingRegistrationService _pendingRegistrationService;
        private readonly IActivityService _domoApiService;
        private readonly IDownloadsService _domoDownloadsService;
        private readonly IUrlService _urlService;
        private readonly IUnitOfWork _unitOfWork;

        public GdpApiService(
            IUserConsentRepository userConsentRepository,
            IUserPrivacyDataService userPrivacyDataService,
            IGdpRequestRepository gdpRequestRepository,
            IGdpExternalRepository gdpExternalRepository,
            IUserRepository userRepository,
            IEmailService emailService,
            ISettingsService settingsService,
            IPendingRegistrationService pendingRegistrationService,
            IActivityService domoApiService,
            IDownloadsService domoDownloadsService,
            IUrlService urlService,
            IUnitOfWork unitOfWork)
        {
            _userConsentRepository = userConsentRepository;
            _userPrivacyDataService = userPrivacyDataService;
            _gdpRequestRepository = gdpRequestRepository;
            _gdpExternalRepository = gdpExternalRepository;
            _serviceFunctionCode = ConfigurationManager.AppSettings["GdprDataAccessJob.ServiceFunctionCode"];
            _userRepository = userRepository;
            _emailService = emailService;
            _settingsService = settingsService;
            _pendingRegistrationService = pendingRegistrationService;
            _domoApiService = domoApiService;
            _domoDownloadsService = domoDownloadsService;
            _urlService = urlService;
            _unitOfWork = unitOfWork;
        }

        public int ApplyUserPrivacyDataRetention(out bool hasError, out ICollection<Exception> exception)
        {
            hasError = false;
            exception = new List<Exception>();
            var expiredUsers = new List<ExpiredUserDTO>();
            int expirationWindowInMonths = _settingsService.GetUserDataExpirationWindowInMonths();
            var expiringUserEmails = _userRepository.GetExpiredUserEmails(expirationWindowInMonths).ToList();
            foreach (var email in expiringUserEmails)
            {
                try
                {
                    if (_userPrivacyDataService.MaskUserData(email, out var expiredUser).Equals(ResponseCode.NotFound))
                    {
                        LogManager.EventFactory.Create()
                                               .Text("User Data Inconsistency")
                                               .AddData("User Email", email)
                                               .Level(LoggingEventLevel.Warning)
                                               .Push();
                    }
                    else if (expiredUser != null)
                    {
                        expiredUsers.Add(expiredUser);
                    }
                }
                catch (Exception ex)
                {
                    LogManager.EventFactory.Create()
                                           .Text("Error in ApplyUserPrivacyDataRetention")
                                           .AddException(ex)
                                           .AddData("Expiration Window In Months", expirationWindowInMonths)
                                           .AddData("Expiring User Emails", expiringUserEmails)
                                           .AddData("User Email", email)
                                           .Level(LoggingEventLevel.Error)
                                           .Push();
                    hasError = true;
                    exception.Add(ex);
                }
            }

            var gdprDeletedFollowUpEmails = new List<MailDispatcher>();
            foreach (var expiredUser in expiredUsers)
            {
                try
                {
                    gdprDeletedFollowUpEmails.Add(_emailService.SendGdprDeletedFollowUpEmail(expiredUser.Email, expiredUser.UserId, expiredUser.FirstName, expiredUser.Region, expiredUser.Language, true));
                }
                catch (Exception ex)
                {
                    LogManager.EventFactory.Create()
                                           .Text("Error in ApplyUserPrivacyDataRetention")
                                           .AddTags("GdprDeletedFollowUpEmail")
                                           .AddException(ex)
                                           .AddData("Expiration Window In Months", expirationWindowInMonths)
                                           .AddData("Expiring User Emails", expiringUserEmails)
                                           .AddData("Expired Users List", expiredUsers.Select(eu => $"UserId: {eu.UserId} - Email: {eu.Email} - FirstName: {eu.FirstName} - Region: {eu.Region} - Language: {eu.Language}"))
                                           .AddData("Expired UserId", expiredUser.UserId)
                                           .AddData("Expired Email", expiredUser.Email)
                                           .AddData("Expired FirstName", expiredUser.FirstName)
                                           .AddData("Expired Region", expiredUser.Region)
                                           .AddData("Expired Language", expiredUser.Language)
                                           .Level(LoggingEventLevel.Error)
                                           .Push();
                    hasError = true;
                    exception.Add(ex);
                }
            }

            if (gdprDeletedFollowUpEmails.Any())
            {
                try
                {
                    _unitOfWork.Commit();
                    foreach (var gdprDeletedFollowUpEmail in gdprDeletedFollowUpEmails)
                    {
                        gdprDeletedFollowUpEmail.Body = gdprDeletedFollowUpEmail.Body.Replace("[#MailDispatcherTrackingPixelUrl]", _urlService.GetTrackingPixelUrl(gdprDeletedFollowUpEmail.RegionId, gdprDeletedFollowUpEmail.MailDispatcherId));
                        gdprDeletedFollowUpEmail.MailDispatcherStatusId = MarketingCenterDbConstants.MailDispatcherStatus.New;
                        gdprDeletedFollowUpEmail.ModifiedDate = DateTime.Now;
                    }
                    _unitOfWork.Commit();
                }
                catch (Exception ex)
                {
                    LogManager.EventFactory.Create()
                                           .Text("Error in ApplyUserPrivacyDataRetention")
                                           .AddTags("GdprDeletedFollowUpEmail")
                                           .AddException(ex)
                                           .AddData("Expiration Window In Months", expirationWindowInMonths)
                                           .AddData("Expiring User Emails", expiringUserEmails)
                                           .AddData("Expired Users List", expiredUsers.Select(eu => $"UserId: {eu.UserId} - Email: {eu.Email} - FirstName: {eu.FirstName} - Region: {eu.Region} - Language: {eu.Language}"))
                                           .AddData("GDPR DeletedFollowUp Emails", gdprDeletedFollowUpEmails.Select(e => $"MailDispatcherId: {e.MailDispatcherId} - MailDispatcherStatusId: {e.MailDispatcherStatusId} - UserId: {e.UserId} - Email: {e.ToAddress} - Body: {e.Body} - Region: {e.RegionId}"))
                                           .Level(LoggingEventLevel.Error)
                                           .Push();
                    hasError = true;
                    exception.Add(ex);
                }
            }

            int expiredPendingRegistrationsCount = 0;
            try
            {
                expiredPendingRegistrationsCount = _pendingRegistrationService.ExpireOrphanPendingRegistration(expirationWindowInMonths);
                _domoApiService.ExpireActivityOrphanUserData(expirationWindowInMonths);
                _domoDownloadsService.ExpireDownloadOrphanUserData(expirationWindowInMonths);
            }
            catch (Exception ex)
            {
                LogManager.EventFactory.Create()
                                       .Text("Error in ApplyUserPrivacyDataRetention")
                                       .AddException(ex)
                                       .AddData("Expiration Window In Months", expirationWindowInMonths)
                                       .Level(LoggingEventLevel.Error)
                                       .Push();
                hasError = true;
                exception.Add(ex);
            }

            return expiringUserEmails.Count + expiredPendingRegistrationsCount;
        }

        public void ObtainGdpRequests()
        {
            _gdpExternalRepository.RetrieveRequests();

            //To be moved to a new job, probably.
            ProcessGdpRequests();
        }

        public void ProcessGdpRequests()
        {
            var pending = _gdpRequestRepository.GetPending().ToList();
            foreach (var request in pending)
            {
                ProcessGdpRequest(request);
            }
        }

        private void ProcessGdpRequest(GdpRequest request)
        {
            GdpResponseDTO serviceResponse = null;
            var user = _userPrivacyDataService.GetByEmail(request.GetEmail());
            if (user != null)
            {
                switch (request.RequestType)
                {
                    case RequestType.Delete:
                        _userPrivacyDataService.MaskUserData(request.GetEmail(), out var expiredUser);
                        if (expiredUser != null)
                        {
                            _emailService.SendGdprDeletedFollowUpEmail(expiredUser.Email, expiredUser.UserId, expiredUser.FirstName, expiredUser.Region, expiredUser.Language);
                        }
                        serviceResponse = new GdpResponseDeleteDTO
                        {
                            RequestId = request.RequestId,
                            ResponseCode = ResponseCode.Success,
                            ServiceFunctionCode = _serviceFunctionCode,
                        };
                        break;
                    case RequestType.View:
                        serviceResponse = new GdpResponseViewDTO
                        {
                            RequestId = request.RequestId,
                            ResponseCode = ResponseCode.Success,
                            ServiceFunctionCode = _serviceFunctionCode,
                            UserDTOs = new List<UserDTO>
                            {
                                user
                            }
                        };
                        break;
                    case RequestType.ConsentHistory:
                        var userConsents = _userConsentRepository.GetByEmail(request.GetEmail()).Select(
                                uc => new UserConsentDTO
                                {
                                    ConsentDate = uc.ConsentDate,
                                    LocaleCode = uc.ConsentFileData.Locale,
                                    ConsentType = uc.ConsentFileData.DocumentType,
                                    VersionID = uc.ConsentFileData.CurrentVersion
                                }).ToList();
                        serviceResponse = new GdpResponseConsentDTO
                        {
                            RequestId = request.RequestId,
                            ResponseCode = (userConsents.Count > 0 ? ResponseCode.Success : ResponseCode.NotFound),
                            ServiceFunctionCode = _serviceFunctionCode,
                            UserConsentDTOs = userConsents

                        };
                        break;
                }
            }
            else
            {
                serviceResponse = new GdpResponseDTO
                {
                    RequestId = request.RequestId,
                    ResponseCode = ResponseCode.NotFound,
                    ServiceFunctionCode = _serviceFunctionCode
                };
            }

            //Send http request to server
            var correlationId = _gdpExternalRepository.SubmitReply(serviceResponse);
            var response = new GdpResponse
            {
                GdpCorrelationId = correlationId,
                GdpRequestId = request.GdpRequestId,
                ResponseCode = serviceResponse?.ResponseCode,
                ServiceFunctionCode = _serviceFunctionCode,
                ResponseData = JsonConvert.SerializeObject(serviceResponse)
            };

            //Create response in DB
            _gdpRequestRepository.CreateResponse(response);
        }

        public int ProcessFollowUpEmails(string region)
        {
            int followUpEmails = 0;
            int expirationWindowInMonths = _settingsService.GetUserDataExpirationWindowInMonths();
            _userRepository.GetExpiringFollowUpUsers(region, MarketingCenterDbConstants.GdprFollowUpStatus.Second, expirationWindowInMonths - 1).ToList().ForEach(um =>
            {
                _emailService.SendGdprAwarenessFollowUpEmail(um.User.Email, um.User.UserId, um.Profile.FirstName, um.Membership.LastLoginDate.AddMonths(expirationWindowInMonths), Constants.TrackingTypes.GdprSecondFollowUp, region, um.User.Language, um.Membership.LastLoginDate);
                um.Profile.GdprFollowUpStatusId = MarketingCenterDbConstants.GdprFollowUpStatus.Second;
                _userRepository.Update(um.User);
                followUpEmails++;
            });
            _userRepository.Commit(); // Commit changes before next step to prevent sending more than 1 email to same user.
            _userRepository.GetExpiringFollowUpUsers(region, MarketingCenterDbConstants.GdprFollowUpStatus.First, expirationWindowInMonths - 2).ToList().ForEach(um =>
            {
                _emailService.SendGdprAwarenessFollowUpEmail(um.User.Email, um.User.UserId, um.Profile.FirstName, um.Membership.LastLoginDate.AddMonths(expirationWindowInMonths), Constants.TrackingTypes.GdprFirstFollowUp, region, um.User.Language, um.Membership.LastLoginDate);
                um.Profile.GdprFollowUpStatusId = MarketingCenterDbConstants.GdprFollowUpStatus.First;
                _userRepository.Update(um.User);
                followUpEmails++;
            });

            _userRepository.Commit();

            return followUpEmails;
        }
    }
}