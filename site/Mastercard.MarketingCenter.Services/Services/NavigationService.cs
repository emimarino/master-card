﻿using Mastercard.MarketingCenter.Common.Interfaces;
using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using Mastercard.MarketingCenter.Services.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Principal;

namespace Mastercard.MarketingCenter.Services
{
    public class NavigationService : INavigationService
    {
        private readonly UserContext _currentUser;
        private readonly ContentItemDao _contentItemDao;
        private readonly ListDao _listDao;
        private readonly ContentTypeDao _contentTypeDao;
        private readonly IAuthorizationService _authorizationService;
        private readonly IPrincipal _user;
        private readonly ISessionService _sessionService;
        private readonly ISettingsService _settingsService;

        public NavigationService(
            UserContext userContext,
            ContentItemDao contentItemDao,
            ListDao listDao,
            ContentTypeDao contentTypeDao,
            IAuthorizationService authorizationService,
            IPrincipal user,
            ISessionService sessionService,
            ISettingsService settingsService)
        {
            _currentUser = userContext;
            _contentItemDao = contentItemDao;
            _listDao = listDao;
            _contentTypeDao = contentTypeDao;
            _authorizationService = authorizationService;
            _user = user;
            _sessionService = sessionService;
            _settingsService = settingsService;
        }

        public NavigationModel GetNavigationModel(IDictionary<string, object> routeData, NameValueCollection queryString)
        {
            var access = _authorizationService.GetAuthorizedResourcesByUser();
            NavigationModel model;
            if (_sessionService.Get("NavigationNodeCollection") == null)
            {
                model = new NavigationModel(_contentItemDao, _listDao, _currentUser, _settingsService);

                UpdateContentNavigation(_contentTypeDao.GetAll(), ref model);
                UpdateListNavigation("Global Settings", _listDao.GetAll(), ref model);
                UpdateListNavigation("Management", _listDao.GetAll(), ref model);
                UpdateManagementNavigation(ref model);

                _sessionService.Set("NavigationNodeCollection", model.RootItems);
            }
            else
            {
                model = new NavigationModel(_contentItemDao, _listDao, _currentUser, _settingsService, _sessionService.Get("NavigationNodeCollection") as NavigationNodeCollection);
            }

            CleanUpMenu(access, ref model);

            var requestController = routeData["controller"]?.ToString();
            var requestAction = routeData["action"]?.ToString();
            var requestId = routeData["id"]?.ToString();
            model.DeactivateItems();
            model.ActivateMenuItem(requestAction, requestController, requestId, queryString);

            return model;
        }

        private void CleanUpMenu(IEnumerable<CmsRoleAccess> access, ref NavigationModel model)
        {
            model.RootItems = ValidateMenuAccess(model.RootItems, access);
        }

        private NavigationNodeCollection ValidateMenuAccess(NavigationNodeCollection navigationNodeCollection, IEnumerable<CmsRoleAccess> allowedAccess)
        {
            var result = new NavigationNodeCollection();

            foreach (var navItem in navigationNodeCollection)
            {
                if (navItem.HideInMenu)
                {
                    continue;
                }

                if (navItem.HasChildren)
                {
                    var children = ValidateMenuAccess(navItem.Children, allowedAccess);
                    if (children.Count > 0)
                    {
                        navItem.Children = children;
                        result.Add(navItem);
                        continue;
                    }
                }

                if (navItem.NavigationInfo != null)
                {
                    var accessibleRoutes = allowedAccess
                        .Where(x =>
                        string.Compare(x.Controller, navItem.NavigationInfo.Controller, StringComparison.InvariantCultureIgnoreCase) == 0 &&
                        (
                            string.IsNullOrEmpty(navItem.NavigationInfo.Action) ||
                            string.IsNullOrEmpty(x.Action) ||
                            string.Compare(x.Action, navItem.NavigationInfo.Action, StringComparison.InvariantCultureIgnoreCase) == 0
                        ) &&
                        (
                            (string.IsNullOrEmpty(navItem.NavigationInfo.Id) && string.IsNullOrEmpty(x.Id)) ||
                            (!string.IsNullOrEmpty(navItem.NavigationInfo.Id) &&
                            (string.IsNullOrEmpty(x.Id) || string.Compare(x.Id, navItem.NavigationInfo.Id, StringComparison.InvariantCultureIgnoreCase) == 0))
                        ));

                    if (accessibleRoutes.Any(a => !string.IsNullOrEmpty(a.RequiredRolePermissionName)))
                    {
                        accessibleRoutes = accessibleRoutes.Where(a => !string.IsNullOrEmpty(a.RequiredRolePermissionName));
                    }

                    if (accessibleRoutes.Any(a => a.RequiredRolePermissionName == null || _user.IsInPermission(a.RequiredRolePermissionName)) &&
                       (string.IsNullOrEmpty(navItem.NavigationInfo?.AccessRole) || navItem.NavigationInfo.AccessRole.Split(',').Any(u => _user.IsInRole(u))) &&
                       (string.IsNullOrEmpty(navItem.NavigationInfo?.AccessPermission) || navItem.NavigationInfo.AccessPermission.Split(',').Any(u => _user.IsInPermission(u))))
                    {
                        result.Add(navItem);
                    }
                }
            }

            return result;
        }

        private void UpdateContentNavigation(IQueryable<ContentType> contentTypesList, ref NavigationModel model)
        {
            var parentContent = model.RootItems.FirstOrDefault(x => x.Name == "Content");
            var contentTypes = contentTypesList.Where(x => !x.ContentTypeId.Equals(MarketingCenterDbConstants.ContentTypeIds.Asset)
                                                        && !x.ContentTypeId.Equals(MarketingCenterDbConstants.ContentTypeIds.AssetFullWidth)
                                                        && !x.ContentTypeId.Equals(MarketingCenterDbConstants.ContentTypeIds.DownloadableAsset)
                                                        && !x.ContentTypeId.Equals(MarketingCenterDbConstants.ContentTypeIds.OrderableAsset)
                                                        && !x.ContentTypeId.Equals(MarketingCenterDbConstants.ContentTypeIds.Faq)
                                                        && !x.ContentTypeId.Equals(MarketingCenterDbConstants.ContentTypeIds.Offer));
            UpdateNavigationMenu(parentContent, contentTypes, ref model);

            var parentAssets = parentContent.Children.FirstOrDefault(x => x.Name == "Assets");
            var assetTypes = contentTypesList.Where(x => x.ContentTypeId.Equals(MarketingCenterDbConstants.ContentTypeIds.Asset)
                                                      || x.ContentTypeId.Equals(MarketingCenterDbConstants.ContentTypeIds.AssetFullWidth)
                                                      || x.ContentTypeId.Equals(MarketingCenterDbConstants.ContentTypeIds.DownloadableAsset)
                                                      || x.ContentTypeId.Equals(MarketingCenterDbConstants.ContentTypeIds.OrderableAsset));
            UpdateNavigationMenu(parentAssets, assetTypes, ref model);
        }

        private void UpdateNavigationMenu(NavigationNode parent, IQueryable<ContentType> contentTypes, ref NavigationModel model)
        {
            contentTypes.ToList()
                .ForEach(o =>
                {
                    NavigationNode contentType = parent.Children.FirstOrDefault(x => x.Name == o.Title);

                    bool isNew = contentType == null;
                    if (isNew)
                    {
                        contentType = new NavigationNode();
                    }

                    contentType.Parent = parent;
                    contentType.Name = o.Title;
                    contentType.NavigationInfo = new NavigationInfo
                    {
                        Name = o.Title,
                        Action = "List",
                        Id = o.ContentTypeId,
                        Controller = "Content",
                        RouteValues = new
                        {
                            id = o.ContentTypeId,
                            Action = "List",
                            Controller = "Content",
                        }
                    };

                    contentType.Children = new NavigationNodeCollection
                    {
                        new NavigationNode {
                            Parent = contentType,
                            Order = 2,
                            Name = "Content",
                            HideInMenu = true,
                            IgnoreQueryStringMatch = true,
                            NavigationInfo = new NavigationInfo {
                                Name = "Create",
                                Action = "Create",
                                Controller = "Content",
                                Id = o.ContentTypeId
                            }
                        } ,
                        new NavigationNode {
                            Parent = contentType,
                            Order = 2,
                            Name = "Content",
                            HideInMenu = true,
                            IgnoreQueryStringMatch = true,
                            NavigationInfo = new NavigationInfo {
                                Name = "Edit",
                                Action = "Edit",
                                Controller = "Content",
                                Id = o.ContentTypeId
                            }
                        },
                        new NavigationNode {
                            Parent = contentType,
                            Order = 2,
                            Name = "Content",
                            HideInMenu = true,
                            IgnoreQueryStringMatch = true,
                            NavigationInfo = new NavigationInfo {
                                Name = "Delete",
                                Action = "Delete",
                                Controller = "Content",
                                Id = o.ContentTypeId
                            }
                        },
                        new NavigationNode {
                            Parent = contentType,
                            Order = 2,
                            Name = "Content",
                            HideInMenu = true,
                            IgnoreQueryStringMatch = true,
                            NavigationInfo = new NavigationInfo {
                                Name = "Clone",
                                Action = "Clone",
                                Controller = "Content",
                                Id = o.ContentTypeId
                            }
                        } ,
                    };

                    if (isNew)
                    {
                        parent.Children.Add(contentType);
                    }
                });
        }

        private void UpdateListNavigation(string section, IQueryable<List> lists, ref NavigationModel model)
        {
            var globalSettings = model.RootItems.FirstOrDefault(x => x.Name == section);
            lists.Where(o => o.ShowInNavigation)
                .ToList()
                .ForEach(o =>
                {
                    var list = globalSettings.Children.FirstOrDefault(c => c.Name == o.Name);

                    if (list != null)
                    {
                        list.Children = new NavigationNodeCollection
                        {
                            new NavigationNode{
                                Parent = list,
                                Name = "Create",
                                HideInMenu = true,
                                IgnoreQueryStringMatch = true,
                                NavigationInfo = new NavigationInfo {
                                    Name = "Create",
                                    Action = "Create",
                                    Controller = "List",
                                    Id = o.ListId.ToString()
                                }
                            },
                            new NavigationNode{
                                Parent = list,
                                Name = "Update",
                                HideInMenu = true,
                                IgnoreQueryStringMatch = true,
                                NavigationInfo = new NavigationInfo {
                                    Name = "Update",
                                    Action = "Update",
                                    Controller = "List",
                                    Id = o.ListId.ToString()
                                }
                            },
                            new NavigationNode{
                                Parent = list,
                                Name = "Edit",
                                HideInMenu = true,
                                IgnoreQueryStringMatch = true,
                                NavigationInfo = new NavigationInfo {
                                    Name = "Edit",
                                    Action = "Edit",
                                    Controller = "List",
                                    Id = o.ListId.ToString()
                                }
                            }
                        };
                    }
                });
        }

        private void UpdateManagementNavigation(ref NavigationModel model)
        {
            var management = model.RootItems.FirstOrDefault(x => x.Name == "Management");
            if (management != null)
            {
                var searchItem = management.Children.FirstOrDefault(c => c.Name.StartsWith("User/Order"));
                if (searchItem != null && !_user.IsInPermission("CanManageOrders"))
                {
                    searchItem.Name = "User Search";
                    searchItem.NavigationInfo.Name = "User Search";
                    searchItem.NavigationInfo.RouteValues = new
                    {
                        id = "UserOrderSearch",
                        comesFrom = "mastercard-admin",
                        renderPageHeading = true
                    };
                }
            }
        }
    }
}