﻿using System;

namespace Mastercard.MarketingCenter.Services.Exceptions
{
    public class ExpiredCertificateException : Exception
    {
        public DateTime ExpirationDate { get; set; }

        public ExpiredCertificateException(DateTime expirationDate) : base("The certificate has expired.")
        {
            ExpirationDate = expirationDate;
        }
    }
}