﻿using System;

namespace Mastercard.MarketingCenter.Services.Exceptions
{
    public class PricelessOfferException : Exception
    {
        public PricelessOfferException(string message) : base(message)
        {
        }

        public PricelessOfferException() : base("There was a problem with a priceless offer.")
        {
        }
    }
}