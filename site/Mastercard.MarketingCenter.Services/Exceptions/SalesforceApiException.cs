﻿using System;

namespace Mastercard.MarketingCenter.Services.Exceptions
{
    public class SalesforceApiException : Exception
    {
        public SalesforceApiException(string message) : base(message)
        {
        }

        public SalesforceApiException() : base("There was a problem with the Salesforce API.")
        {
        }
    }
}