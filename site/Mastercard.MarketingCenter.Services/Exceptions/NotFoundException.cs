﻿using System;

namespace Mastercard.MarketingCenter.Services.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string message) : base(message)
        {
        }

        public NotFoundException() : base("An object doesn't exist.")
        {
        }
    }
}