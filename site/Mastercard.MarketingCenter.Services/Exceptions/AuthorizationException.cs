﻿using System;

namespace Mastercard.MarketingCenter.Services.Exceptions
{
    public class AuthorizationException : Exception
    {
        public AuthorizationException(string message) : base(message)
        {
        }

        public AuthorizationException() : base("You don't have access.")
        {
        }
    }
}