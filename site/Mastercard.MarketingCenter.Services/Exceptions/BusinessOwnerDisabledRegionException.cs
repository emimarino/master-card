﻿using System;

namespace Mastercard.MarketingCenter.Services.Exceptions
{
    public class BusinessOwnerDisabledRegionException : Exception
    {
        public BusinessOwnerDisabledRegionException()
        {
        }

        public BusinessOwnerDisabledRegionException(string message) : base(message)
        {
        }
    }
}