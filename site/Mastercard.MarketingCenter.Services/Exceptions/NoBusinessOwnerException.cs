﻿using System;

namespace Mastercard.MarketingCenter.Services.Exceptions
{
    public class NoBusinessOwnerException : Exception
    {
    }
}