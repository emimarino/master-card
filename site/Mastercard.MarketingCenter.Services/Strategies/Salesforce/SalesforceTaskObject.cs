﻿using Mastercard.MarketingCenter.DTO.Salesforce;
using Mastercard.MarketingCenter.Services.Extensions;
using Newtonsoft.Json;
using Pulsus;
using System;
using System.Threading.Tasks;

namespace Mastercard.MarketingCenter.Services.Strategies.Salesforce
{
    public class SalesforceTaskObject : SalesforceApiRequest
    {
        private SalesforceTaskDTO SalesforceTask { get; set; }

        public SalesforceTaskObject(SalesforceTaskDTO salesforceTask) : base()
        {
            Endpoint = SalesforceApiRequestExtension.GetSobjectTaskUrl();
            SalesforceTask = salesforceTask;
        }

        protected override void ExecuteQuery()
        {
            string requestResult = string.Empty;
            try
            {
                var salesforceTask = JsonConvert.SerializeObject(SalesforceTask);
                requestResult = SendSalesforceTask(salesforceTask).Result;
                Result = requestResult.TrimStart('[').TrimEnd(']');
            }
            catch (Exception e)
            {
                LogManager.EventFactory.Create()
                    .Level(LoggingEventLevel.Error)
                    .AddTags("Salesforce")
                    .Text($"Error trying to send a salesforce task...")
                    .AddData("Salesforce Access Token", AccessToken)
                    .AddData("Salesforce Result", Result)
                    .AddData("Salesforce Endpoint", Endpoint)
                    .AddData("Salesforce Task", SalesforceTask)
                    .AddData("Salesforce Request Result", requestResult)
                    .AddData("Exception Message", e.Message)
                    .AddData("Exception InnerException", e.InnerException)
                    .AddData("Exception Data", e.Data)
                    .AddData("Exception StackTrace", e.StackTrace)
                    .Push();

                throw;
            }
        }

        private async Task<string> SendSalesforceTask(string salesforceTask)
        {
            return await PostToSalesforceApi(salesforceTask).ConfigureAwait(false);
        }
    }
}