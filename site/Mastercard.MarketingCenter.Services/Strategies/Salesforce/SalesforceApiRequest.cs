﻿using Mastercard.MarketingCenter.DTO.Salesforce;
using Mastercard.MarketingCenter.Services.Exceptions;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Helpers;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Mastercard.MarketingCenter.Services.Strategies.Salesforce
{
    public abstract class SalesforceApiRequest
    {
        protected string AccessToken { get; set; }
        public string Result { get; set; }
        protected string Endpoint { get; set; }
        protected abstract void ExecuteQuery();

        public SalesforceApiRequest()
        {
        }

        public void Execute(Action<SalesforceAccessTokenResponseDTO> registerSalesforceAccessToken = null)
        {
            if (AccessToken == null)
            {
                RequestSessionAccessToken(registerSalesforceAccessToken);
                Execute(registerSalesforceAccessToken);
            }
            else
            {
                ExecuteQuery();
            }
        }

        private void RequestSessionAccessToken(Action<SalesforceAccessTokenResponseDTO> registerSalesforceAccessToken)
        {
            //The certificate needed here it is the raxstage client certification for mutual authentication with salesforce, not the certificate used to retrieve access_token. The main difference is that access_token certificate is selfsigned, the another one must be sign by a Certification Authority.
            var salesforceAccessToken = GetSalesforceAccessToken();

            if (!string.IsNullOrEmpty(salesforceAccessToken.ErrorCode))
            {
                throw new SalesforceApiException($"{salesforceAccessToken.Message} - Error Code: [{salesforceAccessToken.ErrorCode}]");
            }

            AccessToken = salesforceAccessToken.AccessToken;

            registerSalesforceAccessToken?.Invoke(salesforceAccessToken);
        }

        protected async Task<string> QueryToSalesforceApi(string url)
        {
            var httpMessageHandler = new WebRequestHandler();
            var msslCert = SalesforceApiRequestExtension.GetSalesforceCertificateMSSL();
            httpMessageHandler.ClientCertificates.Add(msslCert);
            HttpClient queryClient = new HttpClient(httpMessageHandler);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Add("Authorization", $"Bearer {AccessToken}");
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await queryClient.SendAsync(request).ConfigureAwait(false);

            return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        }

        protected async Task<string> PostToSalesforceApi(string payload)
        {
            var httpMessageHandler = new WebRequestHandler();
            var msslCert = SalesforceApiRequestExtension.GetSalesforceCertificateMSSL();
            httpMessageHandler.ClientCertificates.Add(msslCert);
            HttpClient queryClient = new HttpClient(httpMessageHandler);
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, Endpoint)
            {
                Content = new StringContent(payload, Encoding.UTF8, "application/json")
            };
            request.Headers.Add("Authorization", $"Bearer {AccessToken}");
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await queryClient.SendAsync(request).ConfigureAwait(false);

            return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        }

        private SalesforceAccessTokenResponseDTO GetSalesforceAccessToken()
        {
            string audience = SalesforceApiRequestExtension.GetAudience();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create($"{audience}/services/oauth2/token");
            var certificate = SalesforceApiRequestExtension.GetSalesforceCertificateJWT();
            var header = new { alg = "RS256" };
            var claimset = new
            {
                iss = SalesforceApiRequestExtension.GetConsumerKey(),
                sub = SalesforceApiRequestExtension.GetUser(),
                aud = audience,
                exp = (int)DateTime.UtcNow.AddMinutes(4).Subtract(new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds
            };
            var jsSerializer = new JavaScriptSerializer();
            var headerSerialized = jsSerializer.Serialize(header);
            var headerBytes = Encoding.UTF8.GetBytes(headerSerialized);
            var headerEncoded = Base64UrlEncoder.Encode(headerBytes);
            var claimsetSerialized = jsSerializer.Serialize(claimset);
            var claimsetBytes = Encoding.UTF8.GetBytes(claimsetSerialized);
            var claimsetEncoded = Base64UrlEncoder.Encode(claimsetBytes);
            var input = $"{headerEncoded}.{claimsetEncoded}";
            var inputBytes = Encoding.UTF8.GetBytes(input);
            var signatureEncoded = Base64UrlEncoder.Encode(CertificateHelper.SignSha256X509ClientCertificateBytes(certificate, inputBytes));
            var assertion = $"{headerEncoded}.{claimsetEncoded}.{signatureEncoded}";
            var grantType = "urn:ietf:params:oauth:grant-type:jwt-bearer";
            var strPost = $"assertion={assertion}&grant_type={grantType}";
            webRequest.Method = "POST";
            webRequest.ContentLength = strPost.Length;
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.ClientCertificates.Add(certificate);
            using (var writer = new StreamWriter(webRequest.GetRequestStream()))
            {
                writer.Write(strPost);
                writer.Close();
            }
            using (var reader = new StreamReader(webRequest.GetResponse().GetResponseStream(), Encoding.UTF8))
            {
                return JsonConvert.DeserializeObject<SalesforceAccessTokenResponseDTO>(reader.ReadToEnd());
            }
        }
    }
}