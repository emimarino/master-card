﻿using Mastercard.MarketingCenter.Services.Interfaces;

namespace Mastercard.MarketingCenter.Services.Strategies.Salesforce
{
    public abstract class SalesforceOrchestratorStrategy
    {
        public ISalesforceApiService SalesforceApiService { get; set; }
        public ISalesforceService SalesforceService { get; set; }
        public abstract void ProcessSalesforceTasks(SalesforceTasksOrchestrator salesforceTasksOrchestrator);
    }
}