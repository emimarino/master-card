﻿using Mastercard.MarketingCenter.DTO.Salesforce;
using Mastercard.MarketingCenter.Services.Extensions;
using Newtonsoft.Json;
using Pulsus;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mastercard.MarketingCenter.Services.Strategies.Salesforce
{
    public class SalesforceQueryObject : SalesforceApiRequest
    {
        private string NextQueryEndpoint { get; set; }
        private string Query { get; set; }

        public SalesforceQueryObject(string query) : base()
        {
            Endpoint = SalesforceApiRequestExtension.GetQueryUrl();
            Query = query;
        }

        protected override void ExecuteQuery()
        {
            var queryResults = new List<object>();
            AddQueryResults(queryResults);
            Result = JsonConvert.SerializeObject(queryResults);
        }

        private void AddQueryResults(List<object> queryResults)
        {
            string queryResult = string.Empty;
            try
            {
                queryResult = RequestQuery().Result;
                var result = JsonConvert.DeserializeObject<SalesforceQueryResponseDTO>(queryResult);
                queryResults.AddRange(result.Records);

                if (!result.Done)
                {
                    NextQueryEndpoint = SalesforceApiRequestExtension.GetNextRecordsUrl(result.NextRecordsUrl);
                    AddQueryResults(queryResults);
                }
            }
            catch (Exception e)
            {
                LogManager.EventFactory.Create()
                    .Level(LoggingEventLevel.Error)
                    .AddTags("Salesforce")
                    .Text($"Error trying to retrieve query from salesforce...")
                    .AddData("Salesforce Access Token", AccessToken)
                    .AddData("Salesforce Result", Result)
                    .AddData("Salesforce Endpoint", Endpoint)
                    .AddData("Salesforce Query", Query)
                    .AddData("Salesforce Results", queryResults)
                    .AddData("Salesforce Request Query Result", queryResult)
                    .AddData("Exception Message", e.Message)
                    .AddData("Exception InnerException", e.InnerException)
                    .AddData("Exception Data", e.Data)
                    .AddData("Exception StackTrace", e.StackTrace)
                    .Push();

                throw;
            }
        }

        private async Task<string> RequestQuery()
        {
            return await QueryToSalesforceApi(NextQueryEndpoint ?? string.Concat(Endpoint, Query)).ConfigureAwait(false);
        }
    }
}