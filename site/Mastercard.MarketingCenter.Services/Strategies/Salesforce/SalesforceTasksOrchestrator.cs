﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Services.Exceptions;
using Mastercard.MarketingCenter.Services.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using Pulsus;
using System;

namespace Mastercard.MarketingCenter.Services.Strategies.Salesforce
{
    public class SalesforceTasksOrchestrator
    {
        private readonly ISalesforceService _salesforceService;
        private readonly IEmailService _emailService;
        public SalesforceOrchestratorStrategy InputOutputStrategy { get; set; }
        public int CurrentReportId { get; set; }
        public int SuccessfullySentTasks { get; set; }
        public int FailedTasks { get; set; }

        /// <summary>
        /// Creates a new instance of salesforce orchestrator
        /// </summary>
        /// <param name="salesforceService">Salesforce service dependency</param>
        public SalesforceTasksOrchestrator(ISalesforceService salesforceService, IEmailService emailService)
        {
            _salesforceService = salesforceService;
            _emailService = emailService;
            CurrentReportId = _salesforceService.CreateReport();
        }

        public void Start()
        {
            try
            {
                if (SalesforceApiRequestExtension.IsCertificateAboutToExpiration(out DateTime expirationDate))
                {
                    _emailService.SendSalesforceAwarenessExpirationCertificateEmail(expirationDate);
                }

                InputOutputStrategy.ProcessSalesforceTasks(this);
                CloseSalesforceActivityReport();
            }
            catch (ExpiredCertificateException e)
            {
                var loggedEventId = LogManager.EventFactory.Create()
                    .AddTags("Salesforce")
                    .Level(LoggingEventLevel.Alert)
                    .Text($"The Salesforce certificate has expired....")
                    .AddData("Salesforce Certificate Exception Date", e.ExpirationDate)
                    .AddData("Exception Message", e.Message)
                    .AddData("Exception InnerException", e.InnerException)
                    .AddData("Exception Data", e.Data)
                    .AddData("Exception StackTrace", e.StackTrace)
                    .Push().LoggingEvent.EventId;

                CloseSalesforceActivityReport(loggedEventId);
                _emailService.SendSalesforceExpirationCertificateEmail(e.ExpirationDate);

                throw;
            }
            catch (SalesforceApiException e)
            {
                var loggedEventId = LogManager.EventFactory.Create()
                    .AddTags("Salesforce")
                    .Level(LoggingEventLevel.Error)
                    .Text(e.Message)
                    .AddData("Exception Message", e.Message)
                    .AddData("Exception InnerException", e.InnerException)
                    .AddData("Exception Data", e.Data)
                    .AddData("Exception StackTrace", e.StackTrace)
                    .Push().LoggingEvent.EventId;

                CloseSalesforceActivityReport(loggedEventId);

                throw;
            }
            catch (Exception e)
            {
                var loggedEventId = LogManager.EventFactory.Create()
                    .AddTags("Salesforce")
                    .Level(LoggingEventLevel.Error)
                    .Text("Error trying to send tasks to Salesforce API...")
                    .AddData("Exception Message", e.Message)
                    .AddData("Exception InnerException", e.InnerException)
                    .AddData("Exception Data", e.Data)
                    .AddData("Exception StackTrace", e.StackTrace)
                    .Push().LoggingEvent.EventId;

                CloseSalesforceActivityReport(loggedEventId);

                throw;
            }
        }

        #region Private methods

        /// <summary>
        /// Closes a report saving specific information of it
        /// </summary>
        /// <param name="endDate">The DateTime when a report is succesfully closed</param>
        /// <param name="sent">The quantity of successfully tasks sent</param>
        /// <param name="failed">The quantity of failed tasks sent</param>
        /// <param name="isSuccessed">Boolean to mark the report as successed</param>
        /// <param name="salesforceTokenId">Token identifier used to allow requests to salesforce API</param>
        /// <param name="loggingEventId">Error identifier in event logs</param>
        private void CloseSalesforceActivityReport(string loggingEventId = null)
        {
            _salesforceService.UpdateSalesforceActivityReportById(CurrentReportId, DateTime.Now, SuccessfullySentTasks, FailedTasks, loggingEventId.IsNullOrEmpty(), null, loggingEventId);
        }

        #endregion
    }
}