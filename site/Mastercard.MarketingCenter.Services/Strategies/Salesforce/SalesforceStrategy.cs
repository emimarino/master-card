﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Services.Exceptions;
using Newtonsoft.Json;
using Pulsus;
using System;
using System.Linq;

namespace Mastercard.MarketingCenter.Services.Strategies.Salesforce
{
    public class SalesforceStrategy : SalesforceOrchestratorStrategy
    {
        public override void ProcessSalesforceTasks(SalesforceTasksOrchestrator orchestrator)
        {
            try
            {
                SalesforceService.RefreshSalesforceTasks();
                var salesforceTasks = SalesforceService.GetPendingSalesforceTasks();
                var users = salesforceTasks.GroupBy(st => st.ContactEmail).Select(st => new { ContactEmail = st.Key, SalesforceTasks = st });

                var salesforceContacts = SalesforceApiService.GetSalesforceContacts(users.Select(u => u.ContactEmail), true);
                foreach (var user in users)
                {
                    var salesforceContact = salesforceContacts?.FirstOrDefault(c => c.Email.Equals(user.ContactEmail, StringComparison.InvariantCultureIgnoreCase));
                    foreach (var salesforceTask in user.SalesforceTasks)
                    {
                        try
                        {
                            salesforceTask.SalesforceActivityReportId = orchestrator.CurrentReportId;
                            salesforceTask.SalesforceContactResponse = JsonConvert.SerializeObject(salesforceContact);
                            if (salesforceContact != null)
                            {
                                salesforceTask.ContactId = salesforceContact.Id;
                                salesforceTask.AccountId = salesforceContact.Account.Id;
                                salesforceTask.OwnerId = salesforceContact.Account.OwnerId;

                                var result = SalesforceApiService.SendSalesforceTask(salesforceTask, true);
                                salesforceTask.SalesforceTaskResponse = JsonConvert.SerializeObject(result);
                                if (result.Success)
                                {
                                    salesforceTask.TaskId = result.TaskId;
                                    salesforceTask.SalesforceTaskStatusId = MarketingCenterDbConstants.Salesforce.TaskStatus.Completed;
                                    orchestrator.SuccessfullySentTasks++;
                                }
                                else
                                {
                                    salesforceTask.SalesforceTaskStatusId = MarketingCenterDbConstants.Salesforce.TaskStatus.Failed;
                                    salesforceTask.FailedSendAttemptCount++;
                                    orchestrator.FailedTasks++;
                                }
                            }
                            else
                            {
                                salesforceTask.SalesforceTaskStatusId = MarketingCenterDbConstants.Salesforce.TaskStatus.Ignored;
                            }
                        }
                        catch (ExpiredCertificateException)
                        {
                            throw;
                        }
                        catch (Exception e)
                        {
                            orchestrator.FailedTasks++;
                            salesforceTask.SalesforceTaskStatusId = MarketingCenterDbConstants.Salesforce.TaskStatus.Failed;
                            salesforceTask.FailedSendAttemptCount++;
                            salesforceTask.LoggingEventId = LogManager.EventFactory.Create()
                                .Level(LoggingEventLevel.Error)
                                .AddTags("Salesforce")
                                .Text($"Error trying to send a salesforce task...")
                                .AddData("User Contact Email", user.ContactEmail)
                                .AddData("Salesforce Contact", salesforceContact)
                                .AddData("Salesforce Task", salesforceTask)
                                .AddData("Exception Message", e.Message)
                                .AddData("Exception InnerException", e.InnerException)
                                .AddData("Exception Data", e.Data)
                                .AddData("Exception StackTrace", e.StackTrace)
                                .Push().LoggingEvent.EventId;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                SalesforceService.SaveUpdates();
            }
        }
    }
}