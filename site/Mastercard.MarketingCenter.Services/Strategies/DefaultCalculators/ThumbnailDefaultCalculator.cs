﻿using Mastercard.MarketingCenter.Data;
using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mastercard.MarketingCenter.Services
{
    class ThumbnailDefaultCalculator : IDefaultValueCalculator
    {
        private readonly ISettingsService _settingsService;
        private readonly IImageService _imageService;
        public ThumbnailDefaultCalculator(IImageService imageService, ISettingsService settingsService)
        {
            _imageService = imageService;
            _settingsService = settingsService;
        }

        public void CalculateDefault(IEnumerable<ContentTypeFieldDefinition> contentFieldDefs, IDictionary<string, object> data, IDictionary<string, object> previousData = null)
        {
            if (data != null && contentFieldDefs != null)
            {
                var thumbPath = GetKeyValue(data, MarketingCenterDbConstants.FieldNames.ThumbnailImage);
                var mainPath = GetKeyValue(data, MarketingCenterDbConstants.FieldNames.MainImage);

                if (string.IsNullOrEmpty(GetKeyValue(previousData, thumbPath.Key).Value) && string.IsNullOrEmpty(thumbPath.Value) && !string.IsNullOrEmpty(mainPath.Value) && contentFieldDefs.Any(k => k.FieldDefinition.Name == mainPath.Key))
                {
                    var height = _settingsService.GetThumbnailHeight();
                    var width = _settingsService.GetThumbnailWidth();
                    var shouldCrop = _settingsService.GetThumbnailShouldCrop();
                    var imagesPhysicalPath = HttpContext.Current.Server.MapPath(_settingsService.GetImagesFolder());
                    var finalPath = _imageService.Resize(mainPath.Value, width, height, null, shouldCrop)?.Replace(imagesPhysicalPath, "")?.TrimStart('\\', '/');
                    if (!string.IsNullOrEmpty(finalPath))
                    {
                        if (data.Keys.Contains(thumbPath.Key))
                        {
                            data[thumbPath.Key] = finalPath;
                        }
                        else
                        {
                            data.Add(thumbPath.Key, finalPath);
                        }
                    }
                }
            }
        }

        private KeyValuePair<string, string> GetKeyValue(IDictionary<string, object> data, string key)
        {
            var result = new KeyValuePair<string, string>(key, null);
            if (data != null && data.ContainsKey(key))
            {
                result = new KeyValuePair<string, string>(key, (string)data[key]);
            }
            return result;
        }
    }
}