﻿using Slam.Cms.Data;

namespace Mastercard.MarketingCenter.Services.Extensions
{
    public static class ICachingServiceExtension
    {
        public static string GetUserRoleCacheKey(this ICachingService cachingService, int userId, string region = null)
        {
            return $"UserRole_{userId}_{region}";
        }

        public static string GetRolePermissionCacheKey(this ICachingService cachingService, int roleId, string region)
        {
            return $"RolePermissions_{roleId}_{region}";
        }

        public static string GetCardExclusivityModelCacheKey(this ICachingService cachingService)
        {
            return "CardExclusivityModel";
        }

        public static string GetCategoryModelCacheKey(this ICachingService cachingService)
        {
            return $"CategoryModel";
        }

        public static string GetOfferModelCacheKey(this ICachingService cachingService, string region)
        {
            return $"OfferModel_{region}";
        }

        public static string GetMarketApplicabilityModelCacheKey(this ICachingService cachingService, string region)
        {
            return $"MarketApplicabilityModel_{region}";
        }

        public static string GetEventLocationTreeModelCacheKey(this ICachingService cachingService)
        {
            return $"EventLocationTreeModel";
        }

        public static string GetEventLocationModelCacheKey(this ICachingService cachingService)
        {
            return $"EventLocationModel";
        }
    }
}