﻿using System.Security.Principal;

namespace Mastercard.MarketingCenter.Services
{
    public static class IPrincipalExtension
    {
        public static bool IsInPermission(this IPrincipal principal, string permission)
        {
            return (principal as MastercardRolePrincipal).IsInPermission(permission);
        }

        public static bool IsInPermission(this IPrincipal principal, string permission, string region)
        {
            return (principal as MastercardRolePrincipal).IsInPermission(permission, region);
        }

        public static bool IsInAdminRole(this IPrincipal principal)
        {
            return (principal as MastercardRolePrincipal).IsInAdminRole();
        }

        public static bool IsInFullAdminRole(this IPrincipal principal, bool currentRegion = true)
        {
            return (principal as MastercardRolePrincipal).IsInFullAdminRole(currentRegion);
        }

        public static bool IsInFullAdminRole(this IPrincipal principal, string region)
        {
            return (principal as MastercardRolePrincipal).IsInFullAdminRole(region);
        }

        public static string GetUserRoleName(this IPrincipal principal, bool currentRegion = true)
        {
            return (principal as MastercardRolePrincipal).GetUserRoleName(currentRegion);
        }

        public static void InvalidateUserRole(this IPrincipal principal, int userId)
        {
            (principal as MastercardRolePrincipal).InvalidateUserRole(userId);
        }

        public static void InvalidateRolePermissions(this IPrincipal principal, int roleId, string region)
        {
            (principal as MastercardRolePrincipal).InvalidateRolePermissions(roleId, region);
        }
    }
}