﻿using Mastercard.MarketingCenter.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;

namespace Mastercard.MarketingCenter.Services.Extensions
{
    public static class SalesforceApiRequestExtension
    {
        public static string GetSobjectTaskUrl()
        {
            return $"{GetServiceEndpoint()}/services/data/{GetServiceVersion()}/sobjects/Task";
        }

        public static string GetQueryUrl()
        {
            return $"{GetServiceEndpoint()}/services/data/{GetServiceVersion()}/query/?q=";
        }

        public static string GetNextRecordsUrl(string nextRecordsUrl)
        {
            return $"{GetServiceEndpoint()}{nextRecordsUrl}";
        }

        public static string GetConsumerKey()
        {
            return ConfigurationManager.AppSettings["SalesforceApiClient.ConsumerKey"];
        }

        public static string GetSecretKey()
        {
            return ConfigurationManager.AppSettings["SalesforceApiClient.SecretKey"];
        }

        public static string GetUser()
        {
            return ConfigurationManager.AppSettings["SalesforceApiClient.User"];
        }

        public static string GetPassword()
        {
            return ConfigurationManager.AppSettings["SalesforceApiClient.Password"];
        }

        public static string GetAudience()
        {
            return ConfigurationManager.AppSettings["SalesforceApiClient.Audience"];
        }

        public static string GetCertificateJWTSerialNumber()
        {
            return ConfigurationManager.AppSettings["SalesforceApiClient.CertificateJWTSerialNumber"];
        }

        public static string GetCertificateMSSLSerialNumber()
        {
            return ConfigurationManager.AppSettings["SalesforceApiClient.CertificateMSSLSerialNumber"];
        }

        public static StoreLocation GetCertificateStoreLocation()
        {
            return (StoreLocation)int.Parse(ConfigurationManager.AppSettings["SalesforceApiClient.CertificateStoreLocation"]);
        }

        public static bool IsCertificateAboutToExpiration(out DateTime expirationDate)
        {
            expirationDate = new DateTime(Math.Min(GetSalesforceCertificateJWT().NotAfter.Ticks, GetSalesforceCertificateMSSL().NotAfter.Ticks));
            return DateTime.Now > expirationDate.AddDays(-7);
        }

        public static X509Certificate2 GetSalesforceCertificateJWT()
        {
            return CertificateHelper.GetX509ClientCertificateBySerialNumber(GetCertificateJWTSerialNumber(), GetCertificateStoreLocation());
        }

        public static X509Certificate2 GetSalesforceCertificateMSSL()
        {
            return CertificateHelper.GetX509ClientCertificateBySerialNumber(GetCertificateMSSLSerialNumber(), GetCertificateStoreLocation());
        }

        public static string GetContactQuery(IEnumerable<string> emails)
        {
            return $"SELECT Email, Id, Account.OwnerId, Account.Id FROM Contact WHERE RecordType.Name = 'General Contact' AND Email IN ('{string.Join("', '", emails)}')";
        }

        public static string GetTaskQuery(IEnumerable<string> taskIds)
        {
            return $"SELECT Id, Status, WhatId, Activity_Group__c’, ActivityDate, WhoId, Subject, Description, CreatedDate FROM Task WHERE Id IN ('{string.Join("', '", taskIds)}')";
        }

        #region Private

        private static string GetServiceEndpoint()
        {
            return ConfigurationManager.AppSettings["SalesforceApiClient.ServiceEndpoint"];
        }

        private static string GetServiceVersion()
        {
            return ConfigurationManager.AppSettings["SalesforceApiClient.ServiceVersion"];
        }

        #endregion
    }
}