﻿using Mastercard.MarketingCenter.Data.Entities;
using Newtonsoft.Json;
using System;

namespace Mastercard.MarketingCenter.Services.Extensions
{
    public static class FieldDefinitionExtensions
    {
        public static T GetSettings<T>(this FieldDefinition fieldDefinition)
        {
            var customSettings = fieldDefinition.CustomSettings;
            if (string.IsNullOrEmpty(customSettings))
            {
                return Activator.CreateInstance<T>();
            }

            return JsonConvert.DeserializeObject<T>(customSettings);
        }
    }
}