﻿using Mastercard.MarketingCenter.Services.Services;
using System.Web;

namespace Mastercard.MarketingCenter.Services.Extensions
{
    public static class RequestExtension
    {
        public static HttpRequest IsUrlInParametersInternal(this HttpRequest request, string paramName, bool onlyValidated = true)
        {
            return SanitizeService.IsUrlInParametersInternal(request, paramName, onlyValidated);
        }

        static public HttpRequest RejectInvalidCharactersInParameters(this HttpRequest request, string parameters, bool onlyValidated = true)
        {
            SanitizeService.RejectInvalidCharactersInParameters(request, parameters, onlyValidated);
            return request;
        }
    }
}