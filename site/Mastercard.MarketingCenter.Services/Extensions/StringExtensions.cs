﻿using Mastercard.MarketingCenter.Services.Services;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Extensions
{
    public static class StringExtensions
    {
        static readonly IList<string> tagsOnLoop = new List<string> { "applet", "embed", "frame", "script", "frameset", "iframe", "layer", "ilayer", "meta", "object" };
        static readonly IList<string> attributesToClean = new List<string> { "src", "href" };

        public static string SanitizeHtmlString(this string input)
        {
            string result = input;
            foreach (string tag in tagsOnLoop)
            {
                result = SanitizeService.CommentOutOrRemoveTag(result, tag);
            }

            foreach (string attr in attributesToClean)
            {
                result = SanitizeService.EscapeOrRemoveAttribute(result, attr);
            }

            return result;
        }
    }
}