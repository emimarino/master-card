﻿namespace Mastercard.MarketingCenter.Services.Models
{
    public class CardExclusivityModel
    {
        public string CardExclusivityId { get; set; }
        public string Title { get; set; }
        public int Offers { get; set; }
    }
}