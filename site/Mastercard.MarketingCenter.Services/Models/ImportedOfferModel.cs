﻿using CsvHelper.Configuration.Attributes;

namespace Mastercard.MarketingCenter.Services.Models
{
    public class ImportedOfferModel
    {
        [Name("productId")]
        public string ProductId { get; set; }
        [Name("displayName")]
        public string DisplayName { get; set; }
        [Name("productName")]
        public string ProductName { get; set; }
        [Name("longDescription")]
        public string LongDescription { get; set; }
        [Name("description")]
        public string Description { get; set; }
        [Name("specialOffer")]
        public string SpecialOffer { get; set; }
        [Name("celebId")]
        public string CelebId { get; set; }
        [Name("localeId")]
        public string LocaleId { get; set; }
        [Name("celebName")]
        public string CelebName { get; set; }
        [Name("celebUrl")]
        public string CelebUrl { get; set; }
        [Name("celebImageUrl")]
        public string CelebImageUrl { get; set; }
        [Name("charityId")]
        public string CharityId { get; set; }
        [Name("charityName")]
        public string CharityName { get; set; }
        [Name("charityUrl")]
        public string CharityUrl { get; set; }
        [Name("finePrint")]
        public string FinePrint { get; set; }
        [Name("details")]
        public string Details { get; set; }
        [Name("productTerms")]
        public string ProductTerms { get; set; }
        [Name("isEvent")]
        public string IsEvent { get; set; }
        [Name("maxQuantityPerOrder")]
        public string MaxQuantityPerOrder { get; set; }
        [Name("experience")]
        public string Experience { get; set; }
        [Name("event-time")]
        public string EventTime { get; set; }
        [Name("endDate")]
        public string EndDate { get; set; }
        [Name("event-city")]
        public string EventCity { get; set; }
        [Name("event-state")]
        public string EventState { get; set; }
        [Name("latitude")]
        public string Latitude { get; set; }
        [Name("longitude")]
        public string Longitude { get; set; }
        [Name("geographicId")]
        public string GeographicId { get; set; }
        [Name("sharedPeoplePerItem")]
        public string SharedPeoplePerItem { get; set; }
        [Name("event-venue")]
        public string EventVenue { get; set; }
        [Name("catId")]
        public string CatId { get; set; }
        [Name("catName")]
        public string CatName { get; set; }
        [Name("catUrl")]
        public string CatUrl { get; set; }
        [Name("allCategories")]
        public string AllCategories { get; set; }
        [Name("event-duration")]
        public string EventDuration { get; set; }
        [Name("imageUrl")]
        public string ImageUrl { get; set; }
        [Name("productUrl")]
        public string ProductUrl { get; set; }
        [Name("altImageUrl")]
        public string AltImageUrl { get; set; }
        [Name("sourcedSiteName")]
        public string SourcedSiteName { get; set; }
        [Name("initiativeName")]
        public string InitiativeName { get; set; }
        [Name("voiceSkillDescription")]
        public string VoiceSkillDescription { get; set; }
        [Name("variants")]
        public string Variants { get; set; }
        [Name("programName")]
        public string ProgramName { get; set; }
        [Name("cardExclusivity")]
        public string CardExclusivity { get; set; }
        [Name("issuerExclusivity")]
        public string IssuerExclusivity { get; set; }
        [Name("punchoutInfo")]
        public string PunchoutInfo { get; set; }
        [Name("geographicInclusions")]
        public string GeographicInclusions { get; set; }
        [Name("geographicExclusions")]
        public string GeographicExclusions { get; set; }
        [Name("mastercard_global")]
        public string MastercardGlobal { get; set; }
        [Name("price")]
        public string Price { get; set; }
        [Name("currencyCode")]
        public string CurrencyCode { get; set; }
        [Name("currencyCharacter")]
        public string CurrencyCharacter { get; set; }
        [Name("inventory-count")]
        public string InventoryCount { get; set; }
        [Name("masterProductIds")]
        public string MasterProductIds { get; set; }
        [Name("luminaryHtml")]
        public string LuminaryHtml { get; set; }
        [Name("isShippable")]
        public string IsShippable { get; set; }
        [Name("processingDays")]
        public string ProcessingDays { get; set; }
        [Name("processingMsg")]
        public string ProcessingMsg { get; set; }
        [Name("restriction")]
        public string Restriction { get; set; }
    }
}