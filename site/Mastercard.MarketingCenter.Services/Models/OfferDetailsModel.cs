﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Services.Entities;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Models
{
    public class OfferDetailsModel
    {
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string MainImage { get; set; }
        public bool Evergreen { get; set; }
        public DateTime? ValidityPeriodFromDate { get; set; }
        public DateTime? ValidityPeriodToDate { get; set; }
        public DateTime? BookByDate { get; set; }
        public string LongDescription { get; set; }
        public string TermsAndConditions { get; set; }
        public bool PreviewFiles { get; set; }
        public string LogoImage { get; set; }
        public string ProductUrl { get; set; }
        public DownloadFile DownloadFile { get; set; }
        public string OfferType { get; set; }
        public bool PricelessOffer { get; set; }
        public bool ShowProductUrl { get; set; }

        public IList<OfferAttachment> Attachments { get; set; }
        public IList<OfferSecondaryImage> SecondaryImages { get; set; }
        public IList<CardExclusivity> CardExclusivities { get; set; }
        public IList<Category> Categories { get; set; }
        public IList<ContentItemRelatedItem> RelatedItems { get; set; }
        public IList<Tag> MarketApplicabilities { get; set; }
        public IList<EventLocationModel> EventLocations { get; set; }
    }
}