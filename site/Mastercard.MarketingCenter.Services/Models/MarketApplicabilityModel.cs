﻿namespace Mastercard.MarketingCenter.Services.Models
{
    public class MarketApplicabilityModel
    {
        public string MarketApplicabilityId { get; set; }
        public string Title { get; set; }
        public int Offers { get; set; }
    }
}