﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Models
{
    public class SearchFilterModel
    {
        public string SearchTerm { get; set; }
        public List<string> CurrentSegmentationIds { get; set; }
        public string CurrentSpecialAudience { get; set; }
        public string RegionId { get; set; }
        public string Language { get; set; }
    }
}