﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Mastercard.MarketingCenter.Services.Models
{
    public class NavigationNodeCollection : List<NavigationNode>
    {
        public new Enumerator GetEnumerator()
        {
            return base.GetEnumerator();
        }

        public NavigationNode GetActiveNode()
        {
            NavigationNode active = null;
            foreach (var node in this)
            {
                active = node.IsActive ? node : node.Children.GetActiveNode();

                if (active != null)
                {
                    break;
                }
            }

            return active;
        }

        public void ActivateItem(string controller, string action, string id, NameValueCollection queryString)
        {
            ActivateItem(this, controller, action, id, queryString);
        }

        private bool ActivateItem(NavigationNodeCollection nodeList, string controller, string action, string id, NameValueCollection queryString)
        {
            foreach (var node in nodeList)
            {
                if (node != null)
                {
                    if (node.NavigationInfo != null)
                    {
                        var nodeController = node.NavigationInfo.Controller == null ? string.Empty : node.NavigationInfo.Controller;
                        var nodeAction = node.NavigationInfo.Action == null ? string.Empty : node.NavigationInfo.Action;
                        var nodeId = node.NavigationInfo.Id == null ? string.Empty : node.NavigationInfo.Id;

                        bool controllerMatches = string.Equals(nodeController, controller, StringComparison.OrdinalIgnoreCase);
                        bool actionMatches = string.Equals(nodeAction, action, StringComparison.OrdinalIgnoreCase);
                        bool idMatches = string.Equals(nodeId, id, StringComparison.OrdinalIgnoreCase);

                        if (controllerMatches && actionMatches)
                        {
                            if (node.IgnoreIdMatch ||
                                (idMatches &&
                                    (node.IgnoreQueryStringMatch || queryString.Count == 0)))
                            {
                                node.Activate();
                                return true;
                            }

                            if (!node.IgnoreQueryStringMatch && queryString != null && queryString.Count > 0)
                            {
                                bool queryMembresMatches = true;
                                foreach (var key in queryString.AllKeys)
                                {
                                    if (node.NavigationInfo.RouteValues != null)
                                    {
                                        var routeProperties = node.NavigationInfo.RouteValues.GetType().GetProperties();
                                        if (routeProperties != null && routeProperties.Any())
                                        {
                                            var property = routeProperties.FirstOrDefault(x => x.Name == key);

                                            queryMembresMatches = queryMembresMatches && (property != null && property.GetValue(node.NavigationInfo.RouteValues).ToString() == queryString[key]);
                                        }
                                    }
                                }

                                if (queryMembresMatches)
                                {
                                    node.Activate();
                                    return true;
                                }
                            }
                        }
                    }

                    if (node.Children != null)
                    {
                        if (ActivateItem(node.Children, controller, action, id, queryString))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }
}