﻿using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Models
{
    public class UserRoleModel
    {
        public int RoleId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int DisplayOrder { get; set; }
        public bool ShowRegions { get; set; }
        public bool IsInCurrentRegion { get; set; }

        public IEnumerable<UserRoleRegionModel> Regions { get; set; }
    }
}