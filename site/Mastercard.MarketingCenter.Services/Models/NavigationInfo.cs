﻿namespace Mastercard.MarketingCenter.Services.Models
{
    public class NavigationInfo
    {
        public string Name { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public string Id { get; set; }
        public object RouteValues { get; set; }
        public string AccessRole { get; set; }
        public string AccessPermission { get; set; }
    }
}