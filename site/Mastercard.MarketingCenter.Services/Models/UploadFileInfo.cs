﻿namespace Mastercard.MarketingCenter.Services.Models
{
    public class UploadFileInfo
    {
        public long? size { get; set; }
        public string name { get; set; }
        public bool isAbsoluteUrl { get; set; }
    }
}