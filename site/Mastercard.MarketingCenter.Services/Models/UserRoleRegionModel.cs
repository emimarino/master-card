﻿namespace Mastercard.MarketingCenter.Services.Models
{
    public class UserRoleRegionModel
    {
        public string RegionId { get; set; }
        public string Name { get; set; }
    }
}