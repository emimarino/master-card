﻿namespace Mastercard.MarketingCenter.Services.Models
{
    public class SegmentationModel
    {
        public string SegmentationId { get; set; }
        public string SegmentationName { get; set; }
    }
}