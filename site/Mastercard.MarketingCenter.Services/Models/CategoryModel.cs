﻿namespace Mastercard.MarketingCenter.Services.Models
{
    public class CategoryModel
    {
        public string CategoryId { get; set; }
        public string Title { get; set; }
        public int Offers { get; set; }
    }
}