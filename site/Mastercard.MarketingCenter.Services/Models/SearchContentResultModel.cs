﻿using Mastercard.MarketingCenter.Data.Entities;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Models
{
    public class SearchContentResultModel
    {
        public IEnumerable<SearchResult> SearchResults { get; set; }
        public IEnumerable<FeaturedSearchResult> FeaturedSearchResults { get; set; }
    }
}