﻿using Mastercard.MarketingCenter.Common.Filters;

namespace Mastercard.MarketingCenter.Services.Models
{
    public class PardotIntegrationModel
    {
        [Ignore]
        public string RegionId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string IssuerName { get; set; }
        public string Processor { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Country { get; set; }
        public string Language { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string AcceptedConsents { get; set; }
        public bool AcceptedEmailSubscriptions { get; set; }
    }
}