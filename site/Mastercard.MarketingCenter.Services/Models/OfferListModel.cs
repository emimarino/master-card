﻿using Mastercard.MarketingCenter.Common.Extensions;
using Mastercard.MarketingCenter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Mastercard.MarketingCenter.Services.Models
{
    public class OfferListModel
    {
        public string ContentItemId { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string Image { get; set; }
        public string ImageUrl
        {
            get
            {
                string imageUrl = Image;
                bool rootPath = false;
                if (imageUrl.IsAbsoluteUrl())
                {
                    imageUrl = DependencyResolver.Current.GetService<IImageService>().GetImageFromUrl(Image, false);
                    rootPath = true;
                }
                
                return DependencyResolver.Current.GetService<IUrlService>().GetStaticImageURL(imageUrl, 347, 231, true, 85, rootPath);
            }
        }
        public string DetailsUrl { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ValidityPeriodFromDate { get; set; }
        public DateTime? ValidityPeriodToDate { get; set; }
        public string OfferType { get; set; }
        public IEnumerable<string> CategoryIds { get; set; }
        public IEnumerable<string> CardExclusivityIds { get; set; }
        public IEnumerable<string> MarketApplicabilityIds { get; set; }
        public IEnumerable<EventLocationModel> EventLocations { get; set; }
    }
}