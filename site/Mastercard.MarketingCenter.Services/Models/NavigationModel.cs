﻿using Mastercard.MarketingCenter.Data.Daos;
using Mastercard.MarketingCenter.Services.Interfaces;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Mastercard.MarketingCenter.Services.Models
{
    public class NavigationModel
    {
        public NavigationNodeCollection RootItems { get; set; }
        private readonly ContentItemDao _contentItemDao;
        private readonly ListDao _listDao;
        private readonly UserContext _userContext;
        private readonly ISettingsService _settingsService;

        public NavigationModel(ContentItemDao contentItemDao, ListDao listDao, UserContext userContext, ISettingsService settingsService) : this(contentItemDao, listDao, userContext, settingsService, null)
        {
        }

        public NavigationModel(ContentItemDao contentItemDao, ListDao listDao, UserContext userContext, ISettingsService settingsService, NavigationNodeCollection rootItems)
        {
            _contentItemDao = contentItemDao;
            _listDao = listDao;
            _userContext = userContext;
            _settingsService = settingsService;
            RootItems = rootItems;
            if (rootItems == null)
            {
                InitializeMenuItems();
            }
        }

        private void InitializeMenuItems()
        {
            RootItems = new NavigationNodeCollection();

            var globalSettings = new NavigationNode
            {
                Order = 0,
                Name = "Global Settings",
                IconCssClass = "fa-cog"
            };

            var tagManager = new NavigationNode
            {
                Parent = globalSettings,
                Name = "Tag Manager",
                Order = 4,
                NavigationInfo = new NavigationInfo
                {
                    Name = "Tag Manager",
                    Action = "TagManager",
                    Controller = "Tag",
                    RouteValues = new
                    {
                        renderPageHeading = true
                    }
                }
            };

            tagManager.Children = new NavigationNodeCollection
            {
                new NavigationNode
                {
                    Parent = tagManager,
                    Name = "Create Tag Category",
                    Order = 0,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Create Tag Category",
                        Action = "CreateCategory",
                        Controller = "Tag"
                    }
                },
                new NavigationNode
                {
                    Parent = tagManager,
                    Name = "Update Tag Category",
                    Order = 1,
                    IgnoreIdMatch = true,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Update Tag Category",
                        Action = "UpdateCategory",
                        Controller = "Tag"
                    }
                }
            };

            globalSettings.Children = new NavigationNodeCollection
            {
                new NavigationNode
                {
                    Parent = globalSettings,
                    Name = "Icons",
                    Order = 0,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Icons",
                        Action = "Index",
                        Controller = "List",
                        Id = "1",
                        RouteValues = new
                        {
                            id = 1
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = globalSettings,
                    Name = "Domain Blacklist",
                    Order = 1,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Domain Blacklist",
                        Action = "Index",
                        Controller = "List",
                        Id = "6",
                        RouteValues = new
                        {
                            id = 6
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = globalSettings,
                    Name = "Feature Locations",
                    Order = 2,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Feature Locations",
                        Action = "Index",
                        Controller = "List",
                        Id = "8",
                        RouteValues = new
                        {
                            id = 8
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = globalSettings,
                    Name = "Tags",
                    Order = 3,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Tags",
                        Action = "Index",
                        Controller = "List",
                        Id = "2009",
                        RouteValues = new
                        {
                            id = 2009
                        }
                    }
                },
                tagManager
            };

            RootItems.Add(globalSettings);

            var content = new NavigationNode
            {
                Order = 1,
                Name = "Content",
                IconCssClass = "fa-file-o"
            };

            var assetsGroup = new NavigationNode
            {
                Parent = content,
                Order = 0,
                Name = "Assets",
                IsMenuGroup = true,
            };

            assetsGroup.Children = new NavigationNodeCollection
            {
                new NavigationNode
                {
                    Parent = assetsGroup,
                    Order = 0,
                    Name = "Asset Full Width",
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Asset Full Width",
                        Action = "List",
                        Controller = "Content",
                        Id = "a",
                        RouteValues = new
                        {
                            id = "a",
                            header = "Asset Full Width",
                            renderPageHeading = true
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = assetsGroup,
                    Order = 1,
                    Name = "Asset",
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Asset",
                        Action = "List",
                        Controller = "Content",
                        Id = "b",
                        RouteValues = new
                        {
                            id = "b",
                            header = "Asset",
                            renderPageHeading = true
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = assetsGroup,
                    Order = 2,
                    Name = "Downloadable Asset",
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Downloadable Asset",
                        Action = "List",
                        Controller = "Content",
                        Id = "c",
                        RouteValues = new
                        {
                            id = "c",
                            header = "Downloadable Asset",
                            renderPageHeading = true
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = assetsGroup,
                    Order = 2,
                    Name = "Orderable Asset",
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Orderable Asset",
                        Action = "List",
                        Controller = "Content",
                        Id = "d",
                        RouteValues = new
                        {
                            id = "d",
                            header = "Orderable Asset",
                            renderPageHeading = true
                        }
                    }
                }
            };

            content.Children = new NavigationNodeCollection
            {
                assetsGroup,
                new NavigationNode
                {
                    Parent = content,
                    Order = 1,
                    Name = "Program",
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Programs",
                        Action = "List",
                        Controller = "Content",
                        Id = "e",
                        RouteValues = new
                        {
                            id = "e",
                            header = "Programs",
                            renderPageHeading = true
                        }
                    },
                },
                new NavigationNode
                {
                    Parent = content,
                    Order = 2,
                    Name = "Webinar",
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Webinars",
                        Action = "List",
                        Controller = "Content",
                        Id = "f",
                        RouteValues = new
                        {
                            id = "f",
                            header = "Webinars",
                            renderPageHeading = true
                        }
                    },
                },
                new NavigationNode
                {
                    Parent = content,
                    Order = 4,
                    Name = "Page",
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Pages",
                        Action = "List",
                        Controller = "Content",
                        Id = "1",
                        RouteValues = new
                        {
                            id = "1",
                            header = "Pages",
                            renderPageHeading = true
                        }
                    },
                },
                new NavigationNode
                {
                    Parent = content,
                    Order = 6,
                    Name = "Snippet",
                    NavigationInfo = new NavigationInfo
                        {
                        Name = "Snippets",
                        Action = "List",
                        Controller = "Content",
                        Id = "e8",
                        RouteValues = new
                        {
                            id = "e8",
                            header = "Snippets",
                            renderPageHeading = true
                        }
                    },
                },
                new NavigationNode
                {
                    Parent = content,
                    Order = 7,
                    Name = "Marquee Slide",
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Marquee Slides",
                        Action = "List",
                        Controller = "Content",
                        Id = "11",
                        RouteValues = new
                        {
                            id = "11",
                            header = "Marquee Slides",
                            renderPageHeading = true
                        }
                    },
                },
                new NavigationNode
                {
                    Parent = content,
                    Order = 8,
                    HideInMenu =true,
                    Name = "Tag Browser Link",
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Tag Browser Link",
                        Action = "List",
                        Controller = "Content",
                        Id = "1f2",
                        RouteValues = new
                        {
                            id = "1f2",
                            header = "Tag Browser Links",
                            renderPageHeading = true
                        }
                    }
                }
            };

            if (_settingsService.GetOffersEnabled(_userContext.SelectedRegion))
            {
                content.Children.Add(new NavigationNode
                {
                    Parent = content,
                    Order = 5,
                    Name = "Offer",
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Offers",
                        Action = "List",
                        Controller = "Content",
                        Id = "o",
                        RouteValues = new
                        {
                            id = "o",
                            header = "Offers",
                            renderPageHeading = true
                        }
                    }
                });
            }

            RootItems.Add(content);

            var printAdmin = new NavigationNode
            {
                Order = 3,
                Name = "Printer Queues",
                IconCssClass = "fa-print",
                NavigationInfo = new NavigationInfo
                {
                    Name = "Printer Admin",
                    Action = "Index",
                    Controller = "PrinterAdmin",
                    AccessPermission = Common.Infrastructure.Constants.Permissions.CanSetPrinterQueueAdmin
                }
            };

            printAdmin.Children = new NavigationNodeCollection
            {
                new NavigationNode
                {
                    Parent = printAdmin,
                    Order = 1,
                    Name = "Fulfillment Queue",
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Fulfillment Queue",
                        Action = "Index",
                        Controller = "WebForm",
                        Id = "FulfillmentQueue",
                        AccessPermission = Common.Infrastructure.Constants.Permissions.CanSetPrinterQueueAdmin,
                        RouteValues = new
                        {
                            id = "FulfillmentQueue",
                            header = "Fulfillment Queue",
                            renderPageHeading = true
                        }
                    },
                },
                new NavigationNode
                {
                    Parent = printAdmin,
                    Order = 0,
                    Name = "Customization Queue",
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Customization Queue",
                        Action = "Index",
                        Controller = "WebForm",
                        Id = "CustomizationQueue",
                        AccessPermission = Common.Infrastructure.Constants.Permissions.CanSetPrinterQueueAdmin,
                        RouteValues = new
                        {
                            id = "CustomizationQueue",
                            header = "Customization Queue",
                            renderPageHeading = true
                        }
                    }
                }
            };

            RootItems.Add(printAdmin);

            var procAdmin = new NavigationNode
            {
                Order = 4,
                Name = "Processor Admin",
                IconCssClass = "fa-hdd-o",
                NavigationInfo = new NavigationInfo
                {
                    AccessRole = Common.Infrastructure.Constants.Roles.Processor
                }
            };

            var manageIssuers = new NavigationNode
            {
                Name = "Manage Issuers",
                Order = 0,
                Parent = procAdmin,
                NavigationInfo = new NavigationInfo
                {
                    Action = "Index",
                    Name = "Manage Issuers",
                    Controller = "WebForm",
                    Id = "ManageIssuers",
                    AccessRole = Common.Infrastructure.Constants.Roles.Processor,
                    RouteValues = new
                    {
                        id = "ManageIssuers",
                        header = "Manage Issuers",
                        renderPageHeading = true
                    }
                }
            };

            procAdmin.Children = new NavigationNodeCollection
            {
                new NavigationNode
                {
                    Parent = procAdmin,
                    Name = "User/Order Search",
                    Order = 1,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "User/Order Search",
                        Action = "Index",
                        Controller = "WebForm",
                        Id = "UserOrderSearch",
                        AccessRole =  Common.Infrastructure.Constants.Roles.Processor,
                        RouteValues = new
                        {
                            id = "UserOrderSearch",
                            renderPageHeading = true,
                            comesFrom = "processor",
                            source = "ProcAdminUserOrderSearch"
                        }
                    }
                },
                manageIssuers
            };

            RootItems.Add(procAdmin);

            var management = new NavigationNode
            {
                Name = "Management",
                IconCssClass = "fa-gears",
                Order = 2
            };

            var icasManagement = new NavigationNode
            {
                Parent = management,
                Name = "Assign ICAs",
                Order = 5,
                NavigationInfo = new NavigationInfo
                {
                    Name = "Assign ICAs",
                    Action = "Index",
                    Controller = "List",
                    AccessPermission = Common.Infrastructure.Constants.Permissions.CanAssignIcas,
                    Id = "3",
                    RouteValues = new
                    {
                        id = "3"
                    }
                }
            };

            var issuersManagement = new NavigationNode
            {
                Parent = management,
                Name = "Manage Issuers",
                Order = 2,
                IgnoreQueryStringMatch = true,
                NavigationInfo = new NavigationInfo
                {
                    Action = "Manage",
                    Controller = "Issuers",
                    RouteValues = new
                    {
                        header = "Manage Issuers",
                        renderPageHeading = true
                    }
                }
            };

            issuersManagement.Children = new NavigationNodeCollection
            {
                new NavigationNode
                {
                    Name = "Edit Issuers",
                    Order = 0,
                    HideInMenu = true,
                    IgnoreQueryStringMatch = true,
                    Parent = icasManagement,
                    NavigationInfo = new NavigationInfo
                    {
                        Action = "Edit",
                        Name = "Edit Issuers",
                        Controller = "List",
                        Id = "3",
                        RouteValues = new
                        {
                            action = "Edit",
                            controller = "List",
                            id = "3"
                        }
                    }
                },
                new NavigationNode
                {
                    Name = "Create Issuers",
                    Order = 0,
                    HideInMenu = true,
                    IgnoreQueryStringMatch = true,
                    Parent = icasManagement,
                    NavigationInfo = new NavigationInfo
                    {
                        Action = "Create",
                        Name = "Create Issuers",
                        Controller = "List",
                        Id = "3",
                        RouteValues = new
                        {
                            action = "Edit",
                            controller = "List",
                            id = "3"
                        }
                    }
                }
            };

            var issuerMatcher = new NavigationNode
            {
                Parent = management,
                Name = "Issuer Matching",
                Order = 6,
                NavigationInfo = new NavigationInfo
                {
                    Name = "Issuer Matching",
                    Action = "Index",
                    Controller = "IssuerMatcher",
                    AccessPermission = Common.Infrastructure.Constants.Permissions.CanManageIssuerMatcher
                }
            };

            issuerMatcher.Children = new NavigationNodeCollection
            {
                new NavigationNode
                {
                    Name = "Issuer Details",
                    Order = 0,
                    HideInMenu = true,
                    IgnoreQueryStringMatch = true,
                    Parent = issuerMatcher,
                    NavigationInfo = new NavigationInfo
                    {
                        Action = "Details",
                        Name = "Issuer Details",
                        Controller = "IssuerMatcher",
                        AccessPermission = Common.Infrastructure.Constants.Permissions.CanManageIssuerMatcher,
                        RouteValues = new
                        {
                            action = "Details",
                            controller = "IssuerMatcher"
                        }
                    }
                }
            };

            var offerManagement = new NavigationNode
            {
                Name = "Merchant Offers",
                Order = 0,
                IsMenuGroup = true,
                Parent = management,
            };

            var eventLocationManager = new NavigationNode
            {
                Parent = offerManagement,
                Name = "Event Location Manager",
                Order = 4,
                NavigationInfo = new NavigationInfo
                {
                    Name = "Event Location Manager",
                    Action = "EventLocationManager",
                    Controller = "EventLocation",
                    RouteValues = new
                    {
                        renderPageHeading = true
                    }
                }
            };

            eventLocationManager.Children = new NavigationNodeCollection
            {
                new NavigationNode
                {
                    Parent = eventLocationManager,
                    Name = "Create Event Location Category",
                    Order = 0,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Create Event Location Category",
                        Action = "Create",
                        Controller = "EventLocation"
                    }
                },
                new NavigationNode
                {
                    Parent = eventLocationManager,
                    Name = "Update Event Location Category",
                    Order = 1,
                    IgnoreIdMatch = true,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Update Event Location Category",
                        Action = "Update",
                        Controller = "EventLocation"
                    }
                }
            };

            offerManagement.Children = new NavigationNodeCollection
            {
                new NavigationNode
                {
                    Parent = offerManagement,
                    Name = "Card Exclusivity",
                    Order = 1,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Card Exclusivity",
                        Action = "Index",
                        Controller = "List",
                        Id = "11",
                        RouteValues = new
                        {
                            id = 11
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = offerManagement,
                    Name = "Category",
                    Order = 2,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Category",
                        Action = "Index",
                        Controller = "List",
                        Id = "10",
                        RouteValues = new
                        {
                            id = 10
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = offerManagement,
                    Name = "Offer Type",
                    Order = 3,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Offer Type",
                        Action = "Index",
                        Controller = "List",
                        Id = "12",
                        RouteValues = new
                        {
                            id = 12
                        }
                    }
                },
                eventLocationManager
            };

            management.Children = new NavigationNodeCollection
            {
                new NavigationNode
                {
                    Parent = management,
                    Name = "User/Order Search",
                    Order = 1,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "User/Order Search",
                        Action = "Index",
                        Controller = "WebForm",
                        Id = "UserOrderSearch",
                        AccessRole =  $"{Common.Infrastructure.Constants.Roles.McAdmin},{Common.Infrastructure.Constants.Roles.DevAdmin}",
                        RouteValues = new
                        {
                            id = "UserOrderSearch",
                            comesFrom = "mastercard-admin",
                            renderPageHeading = true
                        }
                    }
                },
                issuersManagement,
                new NavigationNode
                {
                    Parent = management,
                    Name = "Issuer Segmentation",
                    Order = 3,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Issuer Segmentation",
                        Action = "Index",
                        Controller = "List",
                        Id = "9",
                        RouteValues = new
                        {
                            id = "9"
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = management,
                    Name = "Billing Report",
                    Order = 4,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Billing Report",
                        Action = "Index",
                        Controller = "WebForm",
                        Id = "BillingReportListing",
                        RouteValues = new
                        {
                            id = "BillingReportListing",
                            header = "Billing Report",
                            renderPageHeading = true
                        }
                    }
                },
                icasManagement,
                issuerMatcher,
                new NavigationNode
                {
                    Parent = management,
                    Name = "Processors",
                    Order = 7,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Processors",
                        Action = "Index",
                        Controller = "List",
                        Id = "7",
                        RouteValues = new
                        {
                            id = 7
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = management,
                    Name = "Vanity URLs",
                    Order = 8,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Vanity URLs",
                        Action = "Index",
                        Controller = "List",
                        Id = "1008",
                        RouteValues = new
                        {
                            id = 1008
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = management,
                    Name = "Search Terms",
                    Order = 9,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Search Terms",
                        Action = "Index",
                        Controller = "List",
                        Id = "2008",
                        RouteValues = new
                        {
                            id = 2008
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = management,
                    Name = "Promo Codes",
                    Order = 10,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Promo Codes",
                        Action = "Index",
                        Controller = "List",
                        Id = "2",
                        RouteValues = new
                        {
                            id = 2
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = management,
                    Name = "Pricing Schedule",
                    Order = 11,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Pricing Schedule",
                        Action = "Index",
                        Controller = "List",
                        Id = "4",
                        RouteValues = new
                        {
                            id = 4
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = management,
                    Name = "Customization Options",
                    Order = 12,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Customization Options",
                        Action = "Index",
                        Controller = "List",
                        Id = "5",
                        RouteValues = new
                        {
                            id = 5
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = management,
                    Name = "Pending Registrations",
                    Order = 13,
                    NavigationInfo = new NavigationInfo
                    {
                        Action = "Index",
                        Controller = "WebForm",
                        Id = "PendingRegistrationsReport",
                        RouteValues = new
                        {
                            id ="PendingRegistrationsReport",
                            header = "Pending Registrations",
                            renderPageHeading = true
                        }
                    }
                },
                offerManagement
            };

            if (_settingsService.GetMarketingCalendarEnabled(_userContext.SelectedRegion))
            {
                management.Children.Add(
                    new NavigationNode
                    {
                        Parent = management,
                        Name = "Event Categories",
                        Order = 15,
                        NavigationInfo = new NavigationInfo
                        {
                            Name = "Event Categories",
                            Action = "Index",
                            Controller = "List",
                            Id = "2010",
                            RouteValues = new
                            {
                                id = 2010
                            }
                        }
                    });
            }

            RootItems.Add(management);

            var reportsAdmin = new NavigationNode
            {
                Order = 2,
                Name = "Reporting",
                IconCssClass = "fa-area-chart"
            };

            reportsAdmin.Children = new NavigationNodeCollection
            {
                new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "FI-User-Logins Report",
                    Order = 0,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "FI-User-Logins",
                        Action = "FramedReport",
                        Controller = "Report",
                        RouteValues = new
                        {
                            report = "processoractivitylanding",
                            title = "FI-User-Logins Report"
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "Cloned Assets Report",
                    Order = 1,
                    NavigationInfo = new NavigationInfo
                    {
                         Id = "ClonedAssetsReport",
                        Name = "Cloned Assets Report",
                        Action = "FramedReport",
                        Controller = "Report",
                        RouteValues = new
                        {
                            report = "clonedassetsreport",
                            title = "Cloned Assets Report"
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "Content Activity Report",
                    Order = 2,
                    NavigationInfo=new NavigationInfo
                    {
                        Name = "Content Activity Report",
                        Action = "FramedReport",
                        Controller = "Report",
                        RouteValues = new
                        {
                            report = "contentactivitylanding",
                            title = "Content Activity Report"
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "Ordering Activity Report",
                    Order = 3,
                    NavigationInfo=new NavigationInfo
                    {
                        Name = "Ordering Activity Report",
                        Action = "FramedReport",
                        Controller = "Report",
                        AccessPermission = Common.Infrastructure.Constants.Permissions.CanManageOrderingActivityLanding,
                        RouteValues = new
                        {
                            report = "orderingactivitylanding",
                            title = "Ordering Activity Report"
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "Issuer Assessment Usage Report",
                    Order = 4,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Issuer Assessment Usage Report",
                        Action = "FramedReport",
                        Controller = "Report",
                        AccessPermission = Common.Infrastructure.Constants.Permissions.CanManageIssuerAssessmentUsageLanding,
                        RouteValues = new
                        {
                            report = "issuerassessmentusagelanding" ,
                            title = "Issuer Assessment Usage Report"
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "User Registration Details Report",
                    Order = 5,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "User Registration Details Report",
                        Action = "FramedReport",
                        Controller = "Report",
                        RouteValues = new
                        {
                            report = "userregistrationlanding",
                            title = "User Registration Details Report"
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "Search Terms Report",
                    Order = 6,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Search Terms Report",
                        Action = "FramedReport",
                        Controller = "Report",
                        RouteValues = new
                        {
                            report = "searchtermlanding",
                            title = "Search Terms Report"
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "Promotions Report",
                    Order = 7,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Promotions Report",
                        Action = "Index",
                        Controller = "WebForm",
                        Id = "PromotionReport",
                        AccessPermission = Common.Infrastructure.Constants.Permissions.CanIndexPromotionReport,
                        RouteValues = new
                        {
                            id = "PromotionReport",
                            header = "Promotions Report",
                            renderPageHeading = true
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "Inserts Bundle Report",
                    Order = 8,
                    NavigationInfo = new NavigationInfo
                    {
                        Action = "Index",
                        Controller = "WebForm",
                        Id = "InsertsBundleReport",
                        AccessPermission = Common.Infrastructure.Constants.Permissions.CanIndexInsertsBundleReport,
                        RouteValues = new
                        {
                            id = "InsertsBundleReport",
                            header = "Inserts Bundle Report",
                            renderPageHeading = true
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "Issuer Matching Report",
                    Order = 9,
                    NavigationInfo=new NavigationInfo
                    {
                        Name = "Issuer Matching Report",
                        Action = "FramedReport",
                        Controller = "Report",
                        AccessPermission = Common.Infrastructure.Constants.Permissions.CanManageIssuerMatcher,
                        RouteValues = new
                        {
                            report = "issuermatching",
                            title = "Issuer Matching Report"
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "User Subscriptions and Favorites Report",
                    Order = 10,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "User Subscriptions and Favorites Report",
                        Action = "FramedReport",
                        Controller = "Report",
                        AccessPermission = Common.Infrastructure.Constants.Permissions.CanManageEnableFavoriting,
                        RouteValues = new
                        {
                            report = "userssubscriptionlanding" ,
                            title = "User Subscriptions and Favorites Report"
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "Subscribable Items Report",
                    Order = 11,
                    NavigationInfo=new NavigationInfo
                    {
                        Name = "Subscribable Items Report",
                        Action = "FramedReport",
                        Controller = "Report",
                        AccessPermission = Common.Infrastructure.Constants.Permissions.CanManageEnableFavoriting,
                        RouteValues = new
                        {
                            report = "subscribableitemslanding",
                            title = "Subscribable Items Report"
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "Email Notifications Report",
                    Order = 12,
                    NavigationInfo=new NavigationInfo
                    {
                        Name = "Email Notifications Report",
                        Action = "FramedReport",
                        Controller = "Report",
                        AccessPermission = Common.Infrastructure.Constants.Permissions.CanManageTriggerMarketing,
                        RouteValues = new
                        {
                            report = "triggermarketingnotificationslanding",
                            title = "Email Notifications Report"
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "Estimated Distribution Report",
                    Order = 13,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Estimated Distribution Report",
                        Action = "FramedReport",
                        AccessPermission = Common.Infrastructure.Constants.Permissions.CanSetRequestEstimatedDistribution,
                        Controller = "Report",
                        RouteValues = new
                        {
                            report = "estimateddistributionlanding",
                            title = "Estimated Distribution Report"
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "Issuer Segmentation Report",
                    Order = 14,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Issuer Segmentation Report",
                        Action = "FramedReport",
                        Controller = "Report",
                        RouteValues = new
                        {
                            report = "issuersegmentation" ,
                            title = "Issuer Segmentation Report"
                        }
                    }
                },
                new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "Broken Links Report",
                    Order = 16,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Broken Links Report",
                        Action = "FramedReport",
                        Controller = "Report",
                        RouteValues = new
                        {
                            report = "brokenlinks" ,
                            title = "Broken Links Report"
                        }
                    }

                },
                new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "Content Report",
                    Order = 17,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "Content Report",
                        Action = "FramedReport",
                        Controller = "Report",
                        RouteValues = new
                        {
                            report = "contentreport" ,
                            title = "Content Report"
                        }
                    }

                }
            };

            if (_settingsService.GetAwarenessFollowUpEnabled(_userContext.SelectedRegion))
            {
                reportsAdmin.Children.Add(new NavigationNode
                {
                    Parent = reportsAdmin,
                    Name = "GDPR Follow-up Report",
                    Order = 15,
                    NavigationInfo = new NavigationInfo
                    {
                        Name = "GDPR Follow-up Report",
                        Action = "FramedReport",
                        Controller = "Report",
                        RouteValues = new
                        {
                            report = "gdprfollowup",
                            title = "GDPR Follow-up Report"
                        }
                    }
                });
            }

            RootItems.Add(reportsAdmin);

            Sort();
        }

        public void ActivateMenuItem(string requestAction, string requestController, string reqId, NameValueCollection queryString)
        {
            var controller = string.IsNullOrEmpty(requestController) ? string.Empty : requestController;
            var action = string.IsNullOrEmpty(requestAction) ? string.Empty : requestAction;
            var id = string.IsNullOrEmpty(reqId) ? string.Empty : reqId;

            if (controller.ToLower() == "content")
            {
                string contentId = null;
                if (action.ToLower() == "edit" || action.ToLower() == "update")
                {
                    contentId = queryString["id"];
                }
                else if (action.ToLower() == "clone")
                {
                    contentId = reqId;
                }
                if (contentId != null)
                {
                    var contentItem = _contentItemDao.GetById(contentId);

                    if (contentItem != null)
                    {
                        id = contentItem.ContentTypeId;
                    }
                }
            }

            if (controller.ToLower() == "list" && (action.ToLower() == "edit" || action.ToLower() == "update"))
            {
                var listIdString = queryString["listid"];
                if (!int.TryParse(listIdString, out int listId))
                {
                    return;
                }

                var contentItem = _listDao.GetById(listId);
                if (contentItem != null)
                {
                    id = contentItem.ListId.ToString();
                }
            }

            RootItems.ActivateItem(requestController, requestAction, id, queryString);
        }

        public void DeactivateItems()
        {
            foreach (var rootItem in RootItems)
            {
                rootItem.Deactivate();
            }
        }

        public List<NavigationInfo> GetCurrenPath()
        {
            var result = new List<NavigationInfo>();

            var current = RootItems.GetActiveNode();

            if (current != null)
            {
                result.Add(current.NavigationInfo);

                var parent = current.Parent;

                while (parent != null && parent.NavigationInfo != null)
                {
                    result.Add(parent.NavigationInfo);
                    parent = parent.Parent;
                }
            }
            result.Reverse();

            return result;
        }
        private void Sort()
        {
            Sort(this.RootItems);
        }

        private void Sort(NavigationNodeCollection nodeList)
        {
            nodeList.Sort(NavigationNode.SortingCompare);
            nodeList.ForEach(x => { x.Children.Sort(NavigationNode.SortingCompare); });
        }
    }
}