﻿using Mastercard.MarketingCenter.Data.Entities;
using Mastercard.MarketingCenter.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mastercard.MarketingCenter.Services.Models
{
    public class EventLocationTree
    {
        public readonly IEventLocationRepository _eventLocationRepository;
        public EventLocationTreeNode RootNode { get; set; }
        private List<EventLocation> EventLocations { get; set; }

        public EventLocationTree(List<EventLocation> eventLocations)
        {
            RootNode = new EventLocationTreeNode();
            EventLocations = eventLocations;
        }

        public EventLocationTree BuildTree()
        {
            var mainChildNodes = new List<EventLocationTreeNode>();
            var rootNode = new EventLocationTreeNode
            {
                Title = "root",
            };

            foreach (var mainChildNode in GetMainChildren())
            {
                var treeNode = new EventLocationTreeNode
                {
                    Title = mainChildNode?.Title,
                    IsRoot = true,
                    PositionId = mainChildNode.EventLocationPositionHierarchy.First().PositionId,
                    EventLocationId = mainChildNode.EventLocationId,
                    Parent = null
                };
                PopulateChildrenNodes(treeNode, EventLocations);
                mainChildNodes.Add(treeNode);
                rootNode.Children.Add(treeNode);
            }
            RootNode = rootNode;

            return this;
        }

        private void PopulateChildrenNodes(EventLocationTreeNode rootTreeNode, IEnumerable<EventLocation> eventLocations)
        {
            var children = from child in eventLocations
                           from hierarchy in child.EventLocationPositionHierarchy
                           where hierarchy.ParentPositionId.Equals(rootTreeNode.PositionId)
                           select new EventLocationTreeNode
                           {
                               Title = child.Title,
                               IsRoot = hierarchy.ParentPositionId.Equals(null),
                               PositionId = hierarchy.PositionId,
                               EventLocationId = child.EventLocationId,
                               Parent = rootTreeNode
                           };

            foreach (var child in children)
            {
                rootTreeNode.Children.Add(child);
                PopulateChildrenNodes(child, eventLocations);
            }
        }

        private IEnumerable<EventLocation> GetMainChildren()
        {
            return EventLocations.Where(e => e.EventLocationPositionHierarchy.Any(x => x.ParentPositionId.Equals(null)));
        }

        public EventLocationTreeNode FindNode(int eventLocationId)
        {
            return FindNode(n => n.EventLocationId == eventLocationId);
        }

        public EventLocationTreeNode FindNode(Predicate<EventLocationTreeNode> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException();
            }

            if (RootNode == null)
            {
                return null;
            }

            return FindNode(RootNode, predicate);
        }

        protected EventLocationTreeNode FindNode(EventLocationTreeNode node, Predicate<EventLocationTreeNode> predicate)
        {
            return FindNodes(node, predicate).FirstOrDefault();
        }

        public IEnumerable<EventLocationTreeNode> FindNodes(Predicate<EventLocationTreeNode> predicate)
        {
            if (predicate == null)
            {
                throw new ArgumentNullException();
            }

            if (RootNode == null)
            {
                return new List<EventLocationTreeNode>();
            }

            return FindNodes(RootNode, predicate);
        }

        protected IEnumerable<EventLocationTreeNode> FindNodes(EventLocationTreeNode node, Predicate<EventLocationTreeNode> predicate)
        {
            if (predicate(node))
            {
                yield return node;
            }

            foreach (var childNode in node.Children)
            {
                foreach (var subNode in FindNodes(childNode, predicate))
                {
                    yield return subNode;
                }
            }
        }
    }
}