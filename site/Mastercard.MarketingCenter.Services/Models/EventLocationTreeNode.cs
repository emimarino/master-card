﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Mastercard.MarketingCenter.Services.Models
{
    public class EventLocationTreeNode
    {
        public EventLocationTreeNodeStatus Status { get; set; }
        public string Title { get; set; }
        public int? PositionId { get; set; }
        public bool IsRoot { get; set; }
        public int EventLocationId { get; set; }
        [JsonIgnore]
        public EventLocationTreeNode Parent { get; set; }
        public List<EventLocationTreeNode> Children { get; set; } = new List<EventLocationTreeNode>();

        public bool HasAnyParent(Predicate<EventLocationTreeNode> predicate)
        {
            if (Parent != null && (predicate(Parent) || Parent.HasAnyParent(predicate)))
            {
                return true;
            }

            return false;
        }

        public bool HasAnyChildren(Predicate<EventLocationTreeNode> predicate)
        {
            foreach (var childNode in Children)
            {
                if (predicate(childNode) || childNode.HasAnyChildren(predicate))
                {
                    return true;
                }
            }

            return false;
        }
    }

    public enum EventLocationTreeNodeStatus
    {
        Unchecked,
        Checked
    }
}