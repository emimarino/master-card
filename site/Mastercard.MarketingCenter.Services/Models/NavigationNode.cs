﻿namespace Mastercard.MarketingCenter.Services.Models
{
    public class NavigationNode
    {
        public string IconCssClass { get; set; }
        public int Order { get; set; }
        public string Name
        {
            get;
            set;
        }
        public bool HasChildren
        {
            get { return Children != null && Children.Count > 0; }
        }
        public bool IsActive
        {
            get;
            private set;
        }
        public bool HasActiveChild
        {
            get;
            private set;
        }
        public bool IsMenuGroup
        {
            get;
            set;
        }
        public bool HideInMenu
        { get; set; }
        public bool IgnoreIdMatch { get; set; }
        public NavigationNode Parent
        {
            get;
            set;
        }
        public NavigationNodeCollection Children
        {
            get;
            set;
        }
        public NavigationInfo NavigationInfo
        {
            get;
            set;
        }
        public NavigationNode()
        {
            Children = new NavigationNodeCollection();
        }
        public void Activate()
        {
            this.Deactivate();
            this.IsActive = true;
            this.ParentChildActivate();
        }
        public static int SortingCompare(NavigationNode x, NavigationNode y)
        {
            int orderIndex = x.Order - y.Order;
            if (orderIndex != 0) return orderIndex;
            return string.Compare(x.Name, y.Name);
        }

        private void ParentChildActivate()
        {
            if (this.Parent != null)
            {
                this.Parent.HasActiveChild = true;
                this.Parent.IsActive = false;
                this.Parent.ParentChildActivate();
            }
        }

        public void Deactivate()
        {
            this.IsActive = false;
            ChildDeactivate(Children);
            this.HasActiveChild = false;
        }

        private void ChildDeactivate(NavigationNodeCollection children)
        {
            if (children != null)
            {
                foreach (var child in children)
                {
                    child.IsActive = false;
                    ChildDeactivate(child.Children);
                    child.HasActiveChild = false;
                }
            }
        }


        public bool IgnoreQueryStringMatch { get; set; }
    }
}