﻿using Autofac;
using Slam.Cms.Common.Interfaces;
using Slam.Cms.Data;
using System.Reflection;
using System.Web;

namespace Slam.Cms.Modules
{
    public class SlamCmsModule : Autofac.Module
    {
        private readonly bool _filterByCurrentRegion = false;
        private readonly Assembly _assemblyWithContentTypes;

        public SlamCmsModule(bool filterByCurrentRegion, Assembly assemblyWithContentTypes)
        {
            _filterByCurrentRegion = filterByCurrentRegion;
            _assemblyWithContentTypes = assemblyWithContentTypes;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new SlamCmsDataModule(_filterByCurrentRegion, _assemblyWithContentTypes));

            builder.Register(c =>
            {
                return new CookieBasedPreviewMode(HttpContext.Current);
            }).As<IPreviewMode>().InstancePerLifetimeScope();
        }
    }
}