﻿using System.Collections.Generic;

namespace Slam.Cms
{
	public class Breadcrumb
	{
		public IEnumerable<SitemapNode> Nodes { get; set; }
	}
}
