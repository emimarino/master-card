﻿namespace Slam.Cms
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class SitemapNode
    {
        public SitemapNode()
        {
            RouteValues = new RouteValueDictionary();
            Children = new List<SitemapNode>();
        }

        public string Key { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public bool SingleTagNode { get; set; } = false;
        public bool MustHaveSubsections { get; set; } = true;
        public string Location { get; set; }
        public string Id { get; set; }
        public string Style { get; set; }
        public RouteValueDictionary RouteValues { get; set; }
        public SitemapNode Parent { get; set; }
        public IList<SitemapNode> Children { get; set; }

        public SitemapNode AddChild(SitemapNode childNode)
        {
            Children.Add(childNode);
            return this;
        }

        public string GetUrl(UrlHelper urlHelper)
        {
            if (string.IsNullOrEmpty(this.Url))
            {
                var action = this.RouteValues["action"].ToString();
                var controller = this.RouteValues["controller"].ToString();
                Url = urlHelper.Action(action, controller, RouteValues);
            }

            return Url;
        }

        public SitemapNode Clone()
        {
            var cloned = new SitemapNode
            {
                Key = Key,
                Parent = Parent,
                RouteValues = new RouteValueDictionary(RouteValues),
                Title = Title,
                Url = Url,
                Location = Location,
                Id = Id,
                Style = Style
            };

            foreach (var child in Children)
            {
                cloned.AddChild(child.Clone());
            }

            return cloned;
        }
    }
}