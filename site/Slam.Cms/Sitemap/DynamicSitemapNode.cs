﻿namespace Slam.Cms
{
    using System.Web.Routing;

    public class DynamicSitemapNode
    {
        public DynamicSitemapNode()
        {
            this.RouteValues = new RouteValueDictionary();
        }

        public string Key { get; set; }
        public string ParentKey { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Location { get; set; }
        public string Id { get; set; }
        public bool SingleTagNode { get; set; } = false;
        public string Style { get; set; }
        public RouteValueDictionary RouteValues { get; set; }
        public bool MustHaveSubsections { get; set; } = true;
        public int Order { get; set; }
    }
}