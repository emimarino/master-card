﻿namespace Slam.Cms
{
    using System.Web.Mvc;

    internal class ViewDataContainer<TModel> : IViewDataContainer
    {
        public ViewDataContainer(TModel model)
        {
            ViewData = new ViewDataDictionary<TModel>(model);
        }

        public ViewDataDictionary ViewData { get; set; }
    }
}