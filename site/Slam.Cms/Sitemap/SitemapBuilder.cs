﻿using System.Collections.Generic;
using System.Web.Routing;
using System;
using Mastercard.MarketingCenter.Common.Extensions;

namespace Slam.Cms
{
    public class SitemapBuilder
    {
        internal SitemapBuilder()
        {
            Nodes = new List<DynamicSitemapNode>();
        }

        public IList<DynamicSitemapNode> Nodes { get; private set; }

        public DynamicSitemapNode AddNode(string key, string title, object routeValues)
        {
            return AddNode(string.Empty, key, title, routeValues);
        }

        public DynamicSitemapNode AddNode(string key, string title, string url)
        {
            return AddNode(string.Empty, key, title, url);
        }

        public DynamicSitemapNode AddNode(string parentKey, string key, string title, object routeValues)
        {
            return AddNode(parentKey, key, title, routeValues, null);
        }

        public DynamicSitemapNode AddNode(string parentKey, string key, string title, string url)
        {
            return AddNode(parentKey, key, title, null, url);
        }

        public DynamicSitemapNode AddNode(string parentKey, string key, string title, string url, string location, string id = null, string singletagnode = null, string style = null, string mustHaveSubsections = null)
        {
            return AddNode(parentKey, key, title, null, url, location, id, singletagnode, style, mustHaveSubsections);
        }

        protected DynamicSitemapNode AddNode(string parentKey, string key, string title, object routeValues, string url, string location = null, string id = null, string singletagnode = null, string style = null, string mustHaveSubsections = null)
        {
            if (routeValues == null && url.IsNullOrWhiteSpace())
            {
                throw new Exception($"The node {key ?? "selected"} should have routeValues or a URL defined");
            }

            var dynamicNode = new DynamicSitemapNode()
            {
                Key = key,
                ParentKey = parentKey,
                Title = title,
                Url = url,
                Location = location,
                Id = id,
                SingleTagNode = (!string.IsNullOrEmpty(singletagnode) ? bool.Parse(singletagnode) : false),
                Style = style,
                MustHaveSubsections = (!string.IsNullOrEmpty(mustHaveSubsections) ? bool.Parse(mustHaveSubsections) : true)
            };

            var routeValueDictionary = new RouteValueDictionary(routeValues);

            foreach (var item in routeValueDictionary)
            {
                dynamicNode.RouteValues.Add(item.Key, item.Value);
            }

            Nodes.Add(dynamicNode);

            return dynamicNode;
        }
    }
}