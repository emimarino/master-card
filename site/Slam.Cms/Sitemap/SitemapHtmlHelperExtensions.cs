﻿namespace Slam.Cms
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;

    public static class SitemapHelperExtensions
    {
        public static IHtmlString Menu(this HtmlHelper htmlHelper)
        {
            var sitemap = GetSitemap();
            return htmlHelper.CreateHtmlHelperForModel(sitemap.RootNode.Children).DisplayFor(m => sitemap.RootNode.Children, "SitemapNodeList");
        }

        public static IHtmlString Menu(this HtmlHelper htmlHelper, int maxLevel)
        {
            var sitemap = GetSitemap();
            var nodes = sitemap.RootNode.Clone().Children;
            FilterNodes(nodes, 1, maxLevel);

            return htmlHelper.CreateHtmlHelperForModel(nodes).DisplayFor(m => nodes, "SitemapNodeList");
        }

        private static void FilterNodes(IList<SitemapNode> nodes, int currentLevel, int maxLevel)
        {
            foreach (var node in nodes)
            {
                if (currentLevel < maxLevel)
                {
                    FilterNodes(node.Children, currentLevel + 1, maxLevel);
                }
                else
                {
                    node.Children.Clear();
                }
            }
        }

        public static bool IsInPathOf(this SitemapNode node, SitemapNode nodeToCheckPath)
        {
            if (nodeToCheckPath == null)
            {
                return false;
            }

            var list = GetNodePath(nodeToCheckPath, true);
            return list.Any(n => n.Key == node.Key);
        }

        public static IHtmlString Breadcrumb(this HtmlHelper htmlHelper, string currentNodeKey, IEnumerable<SitemapNode> additionalNodes, SitemapNode lastNode)
        {
            var sitemap = GetSitemap();
            var node = sitemap.FindNode(currentNodeKey);

            IList<SitemapNode> list = new List<SitemapNode>();

            if (node != null)
            {
                list = GetNodePath(node, true);
            }

            foreach (var otherNode in additionalNodes)
            {
                list.Add(otherNode);
            }

            return htmlHelper.Breadcrumb(list, lastNode);
        }

        public static IHtmlString Breadcrumb(this HtmlHelper htmlHelper, string currentNodeKey, IEnumerable<SitemapNode> additionalNodes)
        {
            var sitemap = GetSitemap();
            var node = sitemap.FindNode(currentNodeKey);

            IList<SitemapNode> list = new List<SitemapNode>();

            if (node != null)
            {
                list = GetNodePath(node, true);
            }

            foreach (var otherNode in additionalNodes)
            {
                list.Add(otherNode);
            }

            return htmlHelper.Breadcrumb(list, node);
        }

        public static IHtmlString Breadcrumb(this HtmlHelper htmlHelper, bool includeCurrentNode = true)
        {
            var node = htmlHelper.GetCurrentSiteMapNode();
            return htmlHelper.Breadcrumb(node, includeCurrentNode);
        }

        public static IHtmlString Breadcrumb(this HtmlHelper htmlHelper, string currentNodeKey, bool includeCurrentNode = true)
        {
            var sitemap = GetSitemap();
            var node = sitemap.FindNode(currentNodeKey);
            return htmlHelper.Breadcrumb(node, includeCurrentNode);
        }

        private static IHtmlString Breadcrumb(this HtmlHelper htmlHelper, SitemapNode node, bool includeCurrentNode = true)
        {
            IList<SitemapNode> list = new List<SitemapNode>();

            if (node != null)
            {
                list = GetNodePath(node, includeCurrentNode);
            }

            return htmlHelper.Breadcrumb(list, node);
        }

        private static IHtmlString Breadcrumb(this HtmlHelper htmlHelper, IEnumerable<SitemapNode> nodes, SitemapNode currentNode)
        {
            var model = new Breadcrumb()
            {
                Nodes = nodes
            };

            return htmlHelper.CreateHtmlHelperForModel(model).DisplayFor(m => model, new { CurrentSitemapNode = currentNode });
        }

        public static string Title(this HtmlHelper htmlHelper)
        {
            var node = htmlHelper.GetCurrentSiteMapNode();
            return node.Title;
        }

        private static IList<SitemapNode> GetNodePath(this SitemapNode node, bool includeCurrentNode = true)
        {
            if (node == null)
            {
                throw new ArgumentNullException("node");
            }

            var list = new List<SitemapNode>();

            if (includeCurrentNode)
            {
                list.Add(node);
            }

            node = node.Parent;

            while (node != null)
            {
                list.Add(node);
                node = node.Parent;
            }

            list.Reverse();

            return list;
        }

        public static SitemapNode GetCurrentSiteMapNode(this HtmlHelper htmlHelper)
        {
            var sitemap = GetSitemap();
            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            var url = htmlHelper.ViewContext.HttpContext.Request.RawUrl;

            return sitemap.FindNode(sitemap.RootNode, n => n.GetUrl(urlHelper).Equals(url, StringComparison.OrdinalIgnoreCase));
        }

        private static HtmlHelper<TModel> CreateHtmlHelperForModel<TModel>(this HtmlHelper htmlHelper, TModel model)
        {
            return new HtmlHelper<TModel>(htmlHelper.ViewContext, new ViewDataContainer<TModel>(model));
        }

        private static Sitemap GetSitemap()
        {
            var sitemap = DependencyResolver.Current.GetService<Sitemap>();
            if (sitemap == null)
            {
                throw new Exception("There is not Sitemap registered in the Dependency Resolver");
            }

            return sitemap;
        }

        public static IDictionary<string, string> MainMenu(this IDictionary<string, string> Menu)
        {
            var sitemap = GetSitemap();
            var FirstGrade = sitemap.RootNode.Children;

            foreach (var element in FirstGrade)
            {
                Menu.Add(element.Title, element.Url);
            }

            return Menu;
        }

        private static SitemapNode getMenuElement(string menuParentKey)
        {
            SitemapNode MenuElement = null;
            var sitemap = GetSitemap();
            var FirstGrade = sitemap.RootNode.Children;

            foreach (var element in FirstGrade)
            {
                if (element.Key == menuParentKey)
                {
                    MenuElement = element;
                    break;
                }
            }

            return MenuElement;
        }

        public static IDictionary<string, string> SubMenu(this IDictionary<string, string> Menu, string menuParentKey)
        {
            SitemapNode MenuElement = getMenuElement(menuParentKey);

            if (MenuElement == null)
            {
                return Menu;
            }

            foreach (var subElement in MenuElement.Children)
            {
                Menu.Add(subElement.Title, subElement.Url);
            }

            return Menu;
        }

        private static SitemapNode getSubMenu(string menuParentKey, string subMenuParentKey)
        {
            var sitemap = GetSitemap();

            var FirstGrade = sitemap.RootNode.Children;
            SitemapNode MenuElement = null;
            foreach (var element in FirstGrade)
            {
                if (element.Key == menuParentKey)
                {
                    MenuElement = element;
                    break;
                }
            }

            SitemapNode SubMenu = null;
            if (MenuElement == null)
            {
                return SubMenu;
            }

            foreach (var subElement in MenuElement.Children)
            {
                if (subElement.Key == subMenuParentKey)
                {
                    SubMenu = subElement;
                    break;
                }
            }

            return SubMenu;
        }

        public static IDictionary<string, string> Sections(this IDictionary<string, string> Sections, string menuParentKey, string subMenuParentKey)
        {
            SitemapNode SubMenu = getSubMenu(menuParentKey, subMenuParentKey);

            if (SubMenu == null)
            {
                return Sections;
            }

            foreach (var section in SubMenu.Children)
            {
                Sections.Add(section.Title, section.Url);
            }

            return Sections;
        }

        public static SitemapNode FilterKeys(this SitemapNode sitemapNode, params string[] keys)
        {
            var filterNodes = from node in sitemapNode.Children
                              where keys.Any(k => k.Equals(node.Key))
                              select node;

            for (int i = 0; i < filterNodes.Count(); i++)
            {
                sitemapNode.Children.Remove(filterNodes.ToArray()[i]);
            }

            sitemapNode.Children.ForEach(c => c.FilterKeys(keys));

            return sitemapNode;
        }
    }
}