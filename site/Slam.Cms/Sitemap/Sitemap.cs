﻿namespace Slam.Cms
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public abstract class Sitemap
    {
        public virtual HashSet<string> excludedUrls { get; }

        private SitemapNode rootNode;
        private bool sitemapBuilt = false;
        private DateTime sitemapLastBuilt = DateTime.MinValue;
        protected abstract void OnCreating(SitemapBuilder builder);

        public abstract IDictionary<string, string> GetLanguages();

        public void Rebuild()
        {
            sitemapBuilt = false;
            Build();
        }

        public SitemapNode RootNode
        {
            get
            {
                if (!sitemapBuilt)
                {
                    Build();
                }

                return rootNode;
            }

            protected set
            {
                rootNode = value;
            }
        }

        public SitemapNode FindNode(string key)
        {
            if (RootNode == null)
            {
                throw new InvalidOperationException("The sitemap doesn't have a root node");
            }

            return FindNode(RootNode, n => n.Key == key);
        }

        public SitemapNode FindNode(SitemapNode startingNode, Predicate<SitemapNode> evaluator)
        {
            var result = FindNodeInner(startingNode, evaluator);

            if (result == null && DateTime.Now.Subtract(sitemapLastBuilt).TotalMinutes > 5)
            {
                // rebuild the sitemap and try again
                Rebuild();

                result = FindNodeInner(startingNode, evaluator);
            }

            return result;
        }

        protected SitemapNode FindNodeInner(SitemapNode startingNode, Predicate<SitemapNode> evaluator)
        {
            if (startingNode == null)
            {
                throw new ArgumentNullException("Must specify a starting node");
            }
            else if (evaluator == null)
            {
                throw new ArgumentNullException("Must specify an evaluator function");
            }

            if (evaluator.Invoke(startingNode))
            {
                return startingNode;
            }

            foreach (var child in startingNode.Children)
            {
                var result = FindNodeInner(child, evaluator);

                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        protected void Build()
        {
            var builder = new SitemapBuilder();

            OnCreating(builder);

            BuildSitemap(builder);

            sitemapBuilt = true;
            sitemapLastBuilt = DateTime.Now;
        }

        protected void BuildSitemap(SitemapBuilder builder)
        {
            // first the root nodes
            var rootNodes = builder.Nodes.Where(n => string.IsNullOrEmpty(n.ParentKey) || n.Key == n.ParentKey);

            // no nodes
            if (rootNodes.Count() == 0)
            {
                return;
            }

            var rootNode = rootNodes.First();

            // build root node
            this.rootNode = BuildSitemapNode(null, rootNode);
            //rootNode.Key = "root";

            // more than one root node, so use a copy of the first root node as root
            if (rootNodes.Count() > 1)
            {
                foreach (var dynamicNode in rootNodes)
                {
                    var node = BuildSitemapNode(this.rootNode, dynamicNode);
                    this.rootNode.AddChild(node);
                }
            }

            // the resting nodes
            var restingNodes = builder.Nodes;
            foreach (var dynamicNode in restingNodes)
            {
                var existingNode = FindNodeInner(this.rootNode, n => n.Key == dynamicNode.Key && n.Url == dynamicNode.Url);
                if (existingNode != null)
                    continue;

                var parentNode = FindNodeInner(this.rootNode, n => n.Key == dynamicNode.ParentKey);

                if (parentNode != null)
                {
                    var node = BuildSitemapNode(parentNode, dynamicNode);
                    parentNode.AddChild(node);
                }
            }
        }

        protected SitemapNode BuildSitemapNode(SitemapNode parentNode, DynamicSitemapNode dynamicNode)
        {
            return new SitemapNode
            {
                Key = dynamicNode.Key,
                Title = dynamicNode.Title,
                RouteValues = dynamicNode.RouteValues,
                Url = dynamicNode.Url,
                Parent = parentNode,
                Location = dynamicNode.Location,
                Id = dynamicNode.Id,
                SingleTagNode = dynamicNode.SingleTagNode,
                Style = dynamicNode.Style,
                MustHaveSubsections = dynamicNode.MustHaveSubsections
            };
        }
    }
}