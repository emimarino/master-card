﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Routing;
using System;
using Slam.Cms;
using Mastercard.MarketingCenter.Common.Extensions;

namespace Slam.Cms.SqlServer
{
	public class ObjectManager
	{
		private IDbConnection sqlConnection;

		public ObjectManager(IDbConnection dbConnection)
		{
			sqlConnection = dbConnection;
		}

		public void RegisterAssembly(string assemblyName, string assemblyPath)
		{
			var assemblyHexString = assemblyPath.GetFileHexString();
			var createAssemblyTsql = @"	IF OBJECT_ID (N'{0}', N'PC') IS NOT NULL
										BEGIN
											DROP ASSEMBLY [{0}];
										END
										CREATE ASSEMBLY [{0}] FROM {1} WITH PERMISSION_SET = UNSAFE;".F(assemblyName, assemblyHexString);

			using (var cmd = sqlConnection.CreateCommand())
			{
				cmd.CommandText = createAssemblyTsql;
				cmd.ExecuteNonQuery();
			}
		}
	}
}
