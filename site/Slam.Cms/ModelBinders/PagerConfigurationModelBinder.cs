﻿using System.Web.Mvc;

namespace Slam.Cms
{
    public class PagerConfigurationModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            int itemsPerPage = 0;

            if (value != null && value.RawValue != null)
                int.TryParse(value.RawValue.ToString(), out itemsPerPage);

            if (itemsPerPage <= 0)
                itemsPerPage = int.MaxValue;

            return new PagerConfiguration()
            {
                ItemsPerPage = itemsPerPage
            };
        }
    }
}
