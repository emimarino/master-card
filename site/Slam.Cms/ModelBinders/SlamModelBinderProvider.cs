﻿using System;
using System.Web.Mvc;
using Slam.Cms.Common;

namespace Slam.Cms
{
    public class SlamModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(Type modelType)
        {
            if (modelType.IsArray && modelType.In(typeof(int[]), typeof(string[]), typeof(float[]), typeof(double[])))
                return new SlashSeparatedValuesModelBinder();

            // default behavior
            return null;
        }
    }
}