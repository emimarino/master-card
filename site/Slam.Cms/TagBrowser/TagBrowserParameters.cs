﻿using System.Collections.Generic;

namespace Slam.Cms
{
    public class TagBrowserParameters
    {
        public TagBrowser TagBrowser { get; set; }
        public string[] FilteredTags { get; set; }
        public string LastFilteredTag { get; set; }
        public bool IsLanding { get; set; }
        public int PageNumber { get; set; }
    }
}