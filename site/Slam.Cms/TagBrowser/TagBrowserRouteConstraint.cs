﻿using System.Linq;
using System.Web;
using System.Web.Routing;
using Mastercard.MarketingCenter.Common.Extensions;

namespace Slam.Cms
{
    public class TagBrowserRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            var tagBrowserId = (values[parameterName] ?? string.Empty).ToString();

            if (tagBrowserId.IsNullOrWhiteSpace())
                return false;

            var tagBrowser = TagBrowsers.Browsers.FirstOrDefault(t => t.Id == tagBrowserId);
            return tagBrowser != null;
        }
    }
}