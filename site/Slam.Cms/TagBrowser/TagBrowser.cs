﻿using System.Web.Routing;

namespace Slam.Cms
{
    public class TagBrowser
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public RouteValueDictionary RouteValues { get; set; }
    }
}
