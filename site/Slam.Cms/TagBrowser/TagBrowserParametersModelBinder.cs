﻿using Mastercard.MarketingCenter.Common.Extensions;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Slam.Cms
{
    public class TagBrowserParametersModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var tagBrowserId = (controllerContext.RouteData.Values["TagBrowserId"] ?? string.Empty).ToString();
            if (tagBrowserId.IsNullOrWhiteSpace())
            {
                return null;
            }

            var tagBrowser = TagBrowsers.Browsers.FirstOrDefault(tb => tb.Id == tagBrowserId);
            if (tagBrowser == null)
            {
                return (TagBrowserParameters)null;
            }

            var tagsString = (controllerContext.RouteData.Values["tags"] ?? string.Empty).ToString();
            var tags = tagsString.Split(@"/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList();

            // check if there is a page number
            int pageNumber = 1;
            if (tags.Count > 0 && int.TryParse(tags.Last(), out pageNumber))
            {
                tags.RemoveAt(tags.Count - 1);
            }
            else
            {
                pageNumber = 1;
            }

            return new TagBrowserParameters
            {
                TagBrowser = tagBrowser,
                FilteredTags = tags.ToArray(),
                LastFilteredTag = tags.LastOrDefault(),
                IsLanding = tags.Count == 0,
                PageNumber = pageNumber
            };
        }
    }
}