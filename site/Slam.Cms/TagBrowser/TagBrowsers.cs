﻿using System.Collections.Generic;
using System.Linq;

namespace Slam.Cms
{
    public static partial class TagBrowsers
    {
        private static readonly IList<TagBrowser> tagBrowsers = new List<TagBrowser>();

        public static IList<TagBrowser> Browsers
        {
            get
            {
                return tagBrowsers;
            }
        }

        public static TagBrowser Get(string id)
        {
            return tagBrowsers.FirstOrDefault(tb => tb.Id == id);
        }
    }
}