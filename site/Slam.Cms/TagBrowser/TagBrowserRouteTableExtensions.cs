﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Slam.Cms
{
    public static class TagBrowserRouteTableExtensions
    {
        public static TagBrowser MapTagBrowser(this RouteCollection routes, string id, string name)
        {
            return routes.MapTagBrowser(id, name, null);
        }

        public static TagBrowser MapTagBrowser(this RouteCollection routes, string id, string name, object routeValues)
        {
            var tagBrowser = new TagBrowser()
            {
                Id = id,
                Name = name
            };

            // {TagBrowserId}/{tag1}/{tag2}/{page} page number acts as a tag, it is resolved by model binder
            var route = routes.MapRoute(id + "Index", id + "/{*tags}", routeValues, new { TagBrowserId = new TagBrowserRouteConstraint() });
            tagBrowser.RouteValues = route.Defaults;

            tagBrowser.RouteValues["TagBrowserId"] = id;

            if (!tagBrowser.RouteValues.ContainsKey("controller"))
                tagBrowser.RouteValues["controller"] = id;

            if (!tagBrowser.RouteValues.ContainsKey("action"))
                tagBrowser.RouteValues["action"] = "Index";

            TagBrowsers.Browsers.Add(tagBrowser);

            return tagBrowser;
        }
    }
}
