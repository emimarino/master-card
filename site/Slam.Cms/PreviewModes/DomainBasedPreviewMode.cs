﻿using System.Web;
using Slam.Cms.Common.Interfaces;

namespace Slam.Cms
{
    public class DomainBasedPreviewMode : IPreviewMode
    {
        private readonly HttpContextBase httpContext;
        private bool? isPreview;

        public DomainBasedPreviewMode(HttpContextBase httpContext)
        {
            this.httpContext = httpContext;
            this.LookUpStrings = new string[] { "preview" };
        }

        public string[] LookUpStrings { get; set; }

        public bool Enabled
        {
            get 
            {
                return this.isPreview.HasValue? this.isPreview.Value : this.IsPreviewMode();
            }
            set
            {
                this.isPreview = value;
            }
        }

        protected bool IsPreviewMode()
        {
            var domain = this.httpContext.Request.Url.Host.ToLowerInvariant();
            foreach (var lookUpString in this.LookUpStrings)
            {
                if (domain.Contains(lookUpString))
                    return true;
            }
            return false;
        }
    }
}