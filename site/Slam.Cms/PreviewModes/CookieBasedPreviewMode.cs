﻿using Slam.Cms.Common.Interfaces;
using System;
using System.Web;

namespace Slam.Cms
{
    public class CookieBasedPreviewMode : IPreviewMode
    {
        private const string PreviewModeKey = "SlamPreviewMode";
        private readonly HttpContext httpContext;
        private bool? _enabled = null;

        private string PreviewModeCookieName
        {
            get
            {
                return this.httpContext != null ? this.httpContext.Request.Url.Host + "." + PreviewModeKey : null;
            }
        }

        public CookieBasedPreviewMode(HttpContext httpContext)
        {
            this.httpContext = httpContext;
        }

        public bool Enabled
        {
            get
            {
                if (this.httpContext == null)
                {
                    return this._enabled.HasValue ? this._enabled.Value : false;
                }

                var slamPreviewMode = this.httpContext.Request.Cookies[PreviewModeCookieName];
                if (slamPreviewMode == null)
                    return false;

                var previewMode = false;
                if (!bool.TryParse(slamPreviewMode.Value, out previewMode))
                    return false;

                return previewMode;
            }
            set
            {
                if (httpContext == null)
                {
                    _enabled = value;
                }

                if (value)
                {
                    httpContext?.Response?.Cookies?.Add(new HttpCookie(PreviewModeCookieName, "true"));
                }
                else
                {
                    var previewModeCookie = new HttpCookie(PreviewModeCookieName, "false")
                    {
                        Expires = DateTime.Now.AddDays(-1)
                    };

                    httpContext?.Response?.AppendCookie(previewModeCookie);
                }
            }
        }
    }
}