﻿using System;

namespace Slam.Cms
{
    public class Pager
    {
        public Func<int, string> UrlResolver { private get; set; }
        public int TotalCount { get; set; }
        public int TotalPages { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }

        public int NextPage
        {
            get
            {
                return this.PageNumber + 1;
            }
        }

        public int PreviousPage
        {
            get
            {
                return this.PageNumber - 1;
            }
        }

        public bool HasPreviousPage()
        {
            return this.PageNumber > 1;
        }

        public bool HasNextPage()
        {
            return this.PageNumber < this.TotalPages;
        }

        public bool IsCurrentPage(int pageNumber)
        {
            return pageNumber == this.PageNumber;
        }

        public string ResolveUrl(int pageNumber)
        {
            return this.UrlResolver(pageNumber);
        }
    }
}