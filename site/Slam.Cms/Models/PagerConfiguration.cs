﻿namespace Slam.Cms
{
    public class PagerConfiguration
    {
        public int ItemsPerPage { get; set; }
    }
}
