﻿using System.Web;
using System.Web.Mvc;

namespace Slam.Cms
{
	public class Link : IHtmlString
	{
		private TagBuilder tagBuilder = new TagBuilder("a");

		public Link(string href, string text)
		{
			this.tagBuilder.MergeAttribute("href", href);
			this.tagBuilder.InnerHtml = text;
		}

		public string Href 
		{ 
			get
			{
				return this.tagBuilder.Attributes["href"];
			}
		}

		public Link AddClass(string className)
		{
			this.tagBuilder.AddCssClass(className);
			return this;
		}

		public Link Title(string title)
		{
			this.tagBuilder.MergeAttribute("title", title);
			return this;
		}

		public Link Target(string target)
		{
			this.tagBuilder.MergeAttribute("target", target);
			return this;
		}

		public string ToHtmlString()
		{
			return tagBuilder.ToString(TagRenderMode.Normal);
		}

		public static Link Create(string href, string text)
		{
			return new Link(href, text);
		}
	}
}
