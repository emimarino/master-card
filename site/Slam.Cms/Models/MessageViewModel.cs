﻿namespace Slam.Cms
{
    public class MessageViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public Link Link { get; set; }
    }
}