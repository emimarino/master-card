﻿namespace Slam.Cms
{
    public class ErrorViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public Link Link { get; set; }
        public bool ShowTechnicalDetails { get; set; }
        public string TechnicalDetails { get; set; }
    }
}