﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI;
using Mastercard.MarketingCenter.Common.Extensions;

namespace Slam.Cms
{
    public static class HtmlHelperExtensions
    {
        const string contentFolder = "/Content";
        const string cssFolder = "Styles";
        const string imagesFolder = "Images";
        const string scriptFolder = "Scripts";

        public static IHtmlString Script(this HtmlHelper helper, string fileName)
        {
            if (!fileName.EndsWith(".js"))
                fileName += ".js";
            var jsPath = string.Format("<script src='{0}/{1}/{2}' ></script>\n", contentFolder, scriptFolder, helper.AttributeEncode(fileName));
            return jsPath.AsHtmlString();
        }

        public static IHtmlString CSS(this HtmlHelper helper, string fileName)
        {
            return CSS(helper, fileName, "screen");
        }

        public static IHtmlString CSS(this HtmlHelper helper, string fileName, string media)
        {
            if (!fileName.EndsWith(".css"))
                fileName += ".css";
            var jsPath = string.Format("<link rel='stylesheet' type='text/css' href='{0}/{1}/{2}'  media='" + media + "'/>\n", contentFolder, cssFolder, helper.AttributeEncode(fileName));
            return jsPath.AsHtmlString();
        }

        public static IHtmlString Image(this HtmlHelper helper, string fileName)
        {
            return helper.Image(fileName, null, null);
        }

        public static IHtmlString Image(this HtmlHelper helper, string fileName, string @class)
        {
            return helper.Image(fileName, @class, null);
        }

        public static IHtmlString Image(this HtmlHelper helper, string fileName, string @class, object attributes)
        {
            var imageTag = new TagBuilder("img");
            imageTag.MergeAttribute("src", helper.ImageUrl(fileName).ToHtmlString());
            if (!@class.IsNullOrWhiteSpace())
                imageTag.AddCssClass(@class);
            var attributesDictionary = new RouteValueDictionary(attributes);
            imageTag.MergeAttributes(attributesDictionary);
            return imageTag.ToString(TagRenderMode.SelfClosing).AsHtmlString();
        }

        public static IHtmlString ImageUrl(this HtmlHelper helper, string fileName)
        {
            fileName = string.Format("{0}/{1}/{2}", contentFolder, imagesFolder, fileName);
            return fileName.AsHtmlString();
        }

        public static HtmlString RenderControl(this HtmlHelper helper, string path)
        {
            return RenderControl<UserControl>(helper, path, null);
        }

        public static HtmlString RenderControl<T>(this HtmlHelper helper, string path) where T : UserControl
        {
            return RenderControl<T>(helper, path, null);
        }

        public static HtmlString RenderControl<T>(this HtmlHelper helper, string path, Action<T> action) where T : UserControl
        {
            var page = new Page();
            T control = (T)page.LoadControl(path);
            page.Controls.Add(control);

            if (action != null)
            {
                action(control);
            }

            using (var sw = new StringWriter())
            {
                HttpContext.Current.Server.Execute(page, sw, false);
                return new HtmlString(sw.ToString());
            }
        }
    }
}