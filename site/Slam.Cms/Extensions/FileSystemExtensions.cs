﻿using System;
using System.IO;

namespace Slam.Cms
{
    public static class FileSystemExtensions
    {
        public static string GetFileHexString(this string assemblyPath)
        {
            if (!Path.IsPathRooted(assemblyPath))
            {
                assemblyPath = Path.Combine(Environment.CurrentDirectory, assemblyPath);
            }

            return assemblyPath.GetFileByteArray().GetHexStringFromByteArray();
        }

        public static byte[] GetFileByteArray(this string filePath)
        {
            using (var assemblyFileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                var byteArray = new byte[assemblyFileStream.Length];
                var numBytesToRead = (int)assemblyFileStream.Length;
                var numBytesRead = 0;
                while (numBytesToRead > 0)
                {
                    var n = assemblyFileStream.Read(byteArray, numBytesRead, numBytesToRead);
                    if (n == 0)
                    {
                        break;
                    }

                    numBytesRead += n;
                    numBytesToRead -= n;
                }

                return byteArray;
            }
        }
    }
}