﻿using System;
using System.Web;
using System.Web.Mvc;
using Mastercard.MarketingCenter.Common.Extensions;

namespace Slam.Cms
{
	public static class ViewPageExtensions
	{
		public static IHtmlString GetBaseUrl(this ViewPage viewPage, bool? useHttps = null)
		{
			return GetBaseUrl(viewPage.Request.Url, useHttps);
		}

		public static IHtmlString GetBaseUrl(this ViewUserControl viewUserControl, bool? useHttps = null)
		{
			return GetBaseUrl(viewUserControl.Request.Url, useHttps);
		}

		public static IHtmlString GetBaseUrl(this WebViewPage webViewPage, bool? useHttps = null)
		{
			return GetBaseUrl(webViewPage.Request.Url, useHttps);
		}

		private static IHtmlString GetBaseUrl(Uri uri, bool? useHttps = null)
		{
			var scheme = uri.Scheme;
			if (useHttps.HasValue)
				scheme = useHttps.Value ? "https" : "http";
			return "{0}://{1}".F(scheme, uri.Authority).AsHtmlString();
		}
	}
}
