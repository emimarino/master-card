﻿using System;
using System.Web;
using System.Web.Mvc;

namespace Slam.Cms
{
	public static class ByteExtensionMethods
	{
		public static string GetHexStringFromByteArray(this byte[] byteArray)
		{
			var c = new char[byteArray.Length * 2];
			byte b;
			for (var i = 0; i < byteArray.Length; i++)
			{
				b = ((byte)(byteArray[i] >> 4));
				c[i * 2] = (char)(b > 9 ? b + 0x37 : b + 0x30);
				b = ((byte)(byteArray[i] & 0xF));
				c[i * 2 + 1] = (char)(b > 9 ? b + 0x37 : b + 0x30);
			}
			return "0x" + new string(c);
		}
	}
}