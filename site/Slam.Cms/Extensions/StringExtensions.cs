﻿using System;
using System.Web;
using System.Web.Mvc;

namespace Slam.Cms
{
    public static class StringExtensionMethods
    {        
        public static IHtmlString AsHtmlString(this string inputString)
        {
            return MvcHtmlString.Create(inputString);
        }

        public static IHtmlString AsUrl(this string input, IHtmlString baseUrl)
        {
            return input.AsUrl(baseUrl.ToHtmlString());
        }

        public static IHtmlString AsUrl(this string input, string baseUrl = null)
        {
            Uri uri;
            if (Uri.TryCreate(input, UriKind.Absolute, out uri))
                return uri.ToString().AsHtmlString();
            else if (baseUrl == null && Uri.TryCreate(input, UriKind.Relative, out uri))
                return uri.ToString().AsHtmlString();
            else if (baseUrl == null)
                return input.AsHtmlString();
            else if (Uri.TryCreate(new Uri(baseUrl, UriKind.Absolute), input, out uri))
                return uri.ToString().AsHtmlString();
            else
                return (baseUrl + input).AsHtmlString();
        }
    }
}