﻿using Mastercard.MarketingCenter.Common.Extensions;
using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Slam.Cms
{
    public static class ControllerExtensions
    {
        public static JavascriptFunctionResult JavascriptFunction(this Controller controller, string javascriptFunction)
        {
            return new JavascriptFunctionResult(javascriptFunction);
        }

        public static JsonpResult Jsonp(this Controller controller, object data)
        {
            return new JsonpResult(data);
        }

        public static string GetBaseUrl(this Controller controller)
        {
            return "{0}://{1}".F(controller.HttpContext.Request.Url.Scheme, controller.HttpContext.Request.Url.Authority);
        }

        public static string RenderViewToString(this Controller controller, string viewName, object viewData = null, bool forJavascript = false)
        {
            var context = controller.ControllerContext;

            //Create memory writer 
            var sb = new StringBuilder();
            var memWriter = new StringWriter(sb);

            //Create fake http context to render the view 
            var fakeResponse = new HttpResponse(memWriter);
            var fakeContext = new HttpContext(HttpContext.Current.Request, fakeResponse);
            var fakeControllerContext = new ControllerContext(new HttpContextWrapper(fakeContext), context.RouteData, context.Controller);
            fakeContext.User = controller.User;

            var oldContext = HttpContext.Current;
            HttpContext.Current = fakeContext;

            //Use HtmlHelper to render partial view to fake context 
            var html = new HtmlHelper(new ViewContext(fakeControllerContext, new FakeView(), new ViewDataDictionary(), new TempDataDictionary(), memWriter), new ViewPage());
            html.RenderPartial(viewName, viewData);

            //Restore context 
            HttpContext.Current = oldContext;

            //Flush memory and return output 
            memWriter.Flush();
            var result = sb.ToString();

            if (forJavascript)
                return result.Replace(Environment.NewLine, string.Empty).Replace("'", "\'");
            else
                return result;
        }

        private class FakeView : IView
        {
            public void Render(ViewContext viewContext, System.IO.TextWriter writer)
            {
                throw new NotImplementedException();
            }
        }
    }
}