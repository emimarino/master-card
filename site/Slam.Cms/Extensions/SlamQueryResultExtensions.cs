﻿using System;
using System.Linq;
using Slam.Cms.Data;
using System.Collections.Generic;

namespace Slam.Cms
{
    public static class SlamQueryResultExtensions
    {
        public static Pager ToPager<T>(this SlamQueryPagedResult<T> slamQueryPagedResult, Func<int, string> urlResolver) where T : ContentItem
        {
            return new Pager()
            {
                UrlResolver = urlResolver,
                TotalCount = slamQueryPagedResult.TotalCount,
                TotalPages = slamQueryPagedResult.TotalPages,
                PageNumber = slamQueryPagedResult.PageNumber,
                PageSize = slamQueryPagedResult.PageSize
            };
        }

        public static SlamQueryResult<T> FilterFeaturedOnTag<T>(this SlamQueryResult<T> slamQueryResult, string tagIdentifier, FilterOperator filterOperator, int featureLevel) where T : ContentItem
        {
            Func<int, int, bool> op = slamQueryResult.GetOperator(filterOperator);
            return slamQueryResult.Filter(c => c.FeatureTags.Any(ft => ft.Identifier == tagIdentifier && op(ft.FeatureLevel, featureLevel)));
        }

        public static SlamQueryResult<T> FilterFeaturedOnTag<T>(this SlamQueryResult<T> slamQueryResult, List<string> tagIdentifiers, FilterOperator filterOperator, int featureLevel) where T : ContentItem
        {
            Func<int, int, bool> op = slamQueryResult.GetOperator(filterOperator);
            return slamQueryResult.Filter(c => !tagIdentifiers.Any(t => !c.FeatureTags.Any(ft => ft.Identifier == t && op(ft.FeatureLevel, featureLevel))));
        }

        public static SlamQueryResult<T> FilterFeaturedOnLocation<T>(this SlamQueryResult<T> slamQueryResult, string locationId, FilterOperator filterOperator, int featureLevel) where T : ContentItem
        {
            Func<int, int, bool> op = slamQueryResult.GetOperator(filterOperator);
            return slamQueryResult.Filter(c => c.FeatureLocations.Any(fl => fl.FeatureLocationId == locationId && op(fl.FeatureLevel, featureLevel)));
        }

        public static SlamQueryResult<T> FilterFeaturedOnLocation<T>(this SlamQueryResult<T> slamQueryResult, List<string> locationIds, FilterOperator filterOperator, int featureLevel) where T : ContentItem
        {
            Func<int, int, bool> op = slamQueryResult.GetOperator(filterOperator);
            return slamQueryResult.Filter(c => !locationIds.Any(l => !c.FeatureLocations.Any(fl => fl.FeatureLocationId == l && op(fl.FeatureLevel, featureLevel))));
        }

        public static SlamQueryResult<T> FilterContentTypes<T, V>(this SlamQueryResult<T> slamQueryResult, IEnumerable<Type> types) where T : ContentItem
        {
            return slamQueryResult.Filter(c => types.Any(t => t == c.GetType()));
        }

        public static SlamQueryResult<T> OrderByFeaturedOnTag<T>(this SlamQueryResult<T> slamQueryResult, string tagIdentifier) where T : ContentItem
        {
            return slamQueryResult.OrderByDescending(c => c.FeatureTags.Any(ft => ft.Identifier == tagIdentifier) ? c.FeatureTags.FirstOrDefault(ft => ft.Identifier == tagIdentifier).Featured ? 1 : 0 : -10)
                                  .ThenByDescending(cd => cd.ModifiedDate);
        }

        public static SlamQueryResult<T> OrderByPriorityLevel<T>(this SlamQueryResult<T> slamQueryResult) where T : ContentItem
        {
            return slamQueryResult.OrderBy(c => c.GetType().GetProperty("PriorityLevel") != null ? Convert.ToInt32(c.GetType().GetProperty("PriorityLevel").GetValue(c, null).ToString()) : 100)
                                  .ThenByDescending(cd => cd.ModifiedDate);
        }

        public static SlamQueryResult<T> OrderByFeatureAndPriorityLevel<T>(this SlamQueryResult<T> slamQueryResult) where T : ContentItem
        {
            return slamQueryResult.OrderByDescending(c => c.FeatureLevel)
                                  .ThenBy(c => c.GetType().GetProperty("PriorityLevel") != null ? Convert.ToInt32(c.GetType().GetProperty("PriorityLevel").GetValue(c, null).ToString()) : 100)
                                  .ThenByDescending(cd => cd.ModifiedDate);
        }

        public static SlamQueryResult<T> ThenByFeatureAndPriorityLevel<T>(this SlamQueryResult<T> slamQueryResult) where T : ContentItem
        {
            return slamQueryResult.ThenByDescending(c => c.FeatureLevel)
                                  .ThenBy(c => c.GetType().GetProperty("PriorityLevel") != null ? Convert.ToInt32(c.GetType().GetProperty("PriorityLevel").GetValue(c, null).ToString()) : 100)
                                  .ThenByDescending(cd => cd.ModifiedDate);
        }

        public static SlamQueryResult<T> OrderByFeaturedOnTagAndPriorityLevel<T>(this SlamQueryResult<T> slamQueryResult, string tagIdentifier) where T : ContentItem
        {
            return slamQueryResult.OrderByDescending(c => c.FeatureTags.Any(ft => ft.Identifier == tagIdentifier) ? c.FeatureTags.FirstOrDefault(ft => ft.Identifier == tagIdentifier).Featured ? 1 : 0 : -10)
                                  .ThenBy(c => c.GetType().GetProperty("PriorityLevel") != null ? Convert.ToInt32(c.GetType().GetProperty("PriorityLevel").GetValue(c, null).ToString()) : 100)
                                  .ThenByDescending(cd => cd.ModifiedDate);
        }

        public static SlamQueryResult<T> OrderByFeaturedOnLocation<T>(this SlamQueryResult<T> slamQueryResult, string locationId) where T : ContentItem
        {
            return slamQueryResult.OrderByDescending(c => c.FeatureLocations.Any(fl => fl.FeatureLocationId == locationId) ? c.FeatureLocations.First(fl => fl.FeatureLocationId == locationId).FeatureLevel : -10);
        }

        public static SlamQueryResult<T> ThenByFeaturedOnLocation<T>(this SlamQueryResult<T> slamQueryResult, string locationId) where T : ContentItem
        {
            return slamQueryResult.ThenByDescending(c => c.FeatureLocations.Any(fl => fl.FeatureLocationId == locationId) ? c.FeatureLocations.First(fl => fl.FeatureLocationId == locationId).FeatureLevel : -10);
        }
    }
}