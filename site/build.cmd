@echo off
SETLOCAL

@REM  ----------------------------------------------------------------------------
@REM
@REM  build.cmd
@REM
@REM  author: mario.moreno@dataart.com
@REM
@REM  ----------------------------------------------------------------------------

set start_time=%time%
set msbuild_folder=C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin
set solution_folder=.
set solution_name=Mastercard.MarketingCenter.sln
set build_configuration=Debug
set nuget_folder=C:\root\bin\nuget

@REM  Shorten the command prompt for making the output easier to read
set savedPrompt=%prompt%
set prompt=$$$g$s

@REM Change to the directory where the solution file resides
pushd %solution_folder%

REM nuget restore
"%nuget_folder%\nuget.exe" restore

REM build solution
"%msbuild_folder%\msbuild.exe" /m %solution_name% /t:Rebuild /p:Configuration=%build_configuration%
@if %errorlevel% NEQ 0 goto :error

@REM  Restore the command prompt and exit
@goto :success

:error
echo an error has occured: %errorLevel%
echo start time: %start_time%
echo end time: %time%
goto :finish

:success
echo process successfully finished
echo start time: %start_time%
echo end time: %time%

:finish
popd
set prompt=%savedPrompt%

ENDLOCAL
echo on