namespace Slam.Cms.Common
{
    using System;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;

    [DebuggerStepThrough]
    public static class Guard
    {
        /// <summary>
        /// Ensures that the given expression results in a non-null value.
        /// </summary>
        /// <typeparam name="TResult">Type of the result, typically omitted as it can 
        /// be inferred by the compiler from the lambda expression.</typeparam>
        /// <param name="expression">The expression to check.</param>
        /// <exception cref="ArgumentNullException">Expression resulted in a null value.</exception>
        /// <example>
        /// The following example shows how to validate that a 
        /// constructor argument is not null:
        /// <code>
        /// public Presenter(IRepository repository, IMailSender mailer)
        /// {
        ///   Guard.NotNull(() => repository);
        ///   Guard.NotNull(() => mailer);
        ///   
        ///     this.repository = repository;
        ///     this.mailer = mailer;
        /// }
        /// </code>
        /// </example>
        public static void NotNull<TResult>(Expression<Func<TResult>> expression)
        {
            if (expression.Compile()() == null)
            {
                throw new ArgumentNullException(GetParameterName(expression));
            }
        }

        /// <summary>
        /// Ensures the given <paramref name="value"/> is not null.
        /// Throws <see cref="ArgumentNullException"/> otherwise.
        /// </summary>
        public static void NotNull<T>(Expression<Func<T>> reference, T value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(GetParameterName(reference));
            }
        }

        /// <summary>
        /// Ensures the given string <paramref name="value"/> is not null or empty.
        /// Throws <see cref="ArgumentNullException"/> in the first case, or 
        /// <see cref="ArgumentException"/> in the latter.
        /// </summary>
        public static void NotNullOrEmpty(Expression<Func<string>> reference, string value)
        {
            NotNull<string>(reference, value);
            if (value.Length == 0)
            {
                throw new ArgumentException("Value cannot be an empty string.", GetParameterName(reference));
            }
        }

        /// <summary>
        /// Checks an argument to ensure it is in the specified range including the edges.
        /// </summary>
        /// <typeparam name="T">Type of the argument to check, it must be an <see cref="IComparable"/> type.
        /// </typeparam>
        /// <param name="reference">The expression containing the name of the argument.</param>
        /// <param name="value">The argument value to check.</param>
        /// <param name="from">The minimun allowed value for the argument.</param>
        /// <param name="to">The maximun allowed value for the argument.</param>
        public static void NotOutOfRangeInclusive<T>(Expression<Func<T>> reference, T value, T from, T to)
            where T : IComparable
        {
            if (value != null && (value.CompareTo(from) < 0 || value.CompareTo(to) > 0))
            {
                throw new ArgumentOutOfRangeException(GetParameterName(reference));
            }
        }

        /// <summary>
        /// Checks an argument to ensure it is in the specified range excluding the edges.
        /// </summary>
        /// <typeparam name="T">Type of the argument to check, it must be an <see cref="IComparable"/> type.
        /// </typeparam>
        /// <param name="reference">The expression containing the name of the argument.</param>
        /// <param name="value">The argument value to check.</param>
        /// <param name="from">The minimun allowed value for the argument.</param>
        /// <param name="to">The maximun allowed value for the argument.</param>
        public static void NotOutOfRangeExclusive<T>(Expression<Func<T>> reference, T value, T from, T to)
            where T : IComparable
        {
            if (value != null && (value.CompareTo(from) <= 0 || value.CompareTo(to) >= 0))
            {
                throw new ArgumentOutOfRangeException(GetParameterName(reference));
            }
        }

        public static void CanBeAssigned(Expression<Func<object>> reference, Type typeToAssign, Type targetType)
        {
            if (!targetType.IsAssignableFrom(typeToAssign))
            {
                if (targetType.IsInterface)
                {
                    throw new ArgumentException(string.Format(
                        CultureInfo.CurrentCulture,
                        "Type {0} does not implement required interface {1}",
                        typeToAssign,
                        targetType), GetParameterName(reference));
                }

                throw new ArgumentException(string.Format(
                    CultureInfo.CurrentCulture,
                    "Type {0} does not from required type {1}",
                    typeToAssign,
                    targetType), GetParameterName(reference));
            }
        }

        private static string GetParameterName(LambdaExpression reference)
        {
            var member = (MemberExpression)reference.Body;
            return member.Member.Name;
        }
    }
}