﻿using System;
using System.IO;

namespace Slam.Cms.Common
{
	public class FileData
	{
		public Stream Content { get; set; }
		public string ContentType { get; set; }
		public DateTime LastModified { get; set; }
		public long ContentLength { get; set; }
	}
}
