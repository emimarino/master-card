﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Slam.Cms.Common
{
	public class ServiceResponse
	{
		public ServiceResponse()
		{
			MessageCode = ServiceResponseMessageCode.None;
		}

		public ServiceResponseMessageCode MessageCode
		{
			get;
			set;
		}

		public string Message
		{
			get;
			set;
		}

		public object Response
		{
			get;
			set;
		}
	}
}
