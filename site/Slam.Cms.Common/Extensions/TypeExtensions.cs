﻿using System;
using System.Reflection;

namespace Slam.Cms.Common
{
    public static class TypeExtensions
    {
        public static Assembly GetAssembly(this Type type)
        {
            return Assembly.GetAssembly(type);
        }

        public static bool Implements<T>(this Type type) where T : class
        {
            return typeof(T).IsAssignableFrom(type);
        }
    }
}