﻿using Mastercard.MarketingCenter.Common.Extensions;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Slam.Cms.Common
{
    public static class StringExtension
    {
        private static Regex _guidRegex = new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled);
        public static bool IsGuid(this string inputString)
        {
            return _guidRegex.IsMatch(inputString);
        }

        public static bool IsValidEmailAddress(this string s)
        {
            if (s.IsNullOrEmpty())
                return false;

            var regex = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{1,4}$");
            return regex.IsMatch(s);
        }

        public static string RemoveSpaces(this string inputString)
        {
            return Regex.Replace(inputString, @"\s", "");
        }

        public static string RemoveInvalidFileSystemCharacters(this string inputString)
        {
            return Regex.Replace(inputString, @"[/\<>:?|*""]", "");
        }

        public static string EncodeForPath(this string inputString)
        {
            return HttpContext.Current.Server.UrlEncode(Regex.Replace(inputString, @"[^-\w\s]|\b\w{1,2}\s", "")).Replace("+", "-").TrimEnd('-');
        }

        public static string GetSlug(this string phrase)
        {
            if (phrase.IsNullOrEmpty())
                return string.Empty;

            var str = phrase.RemoveAccent().ToLower();

            // invalid chars, make into spaces
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");

            // convert multiple spaces/hyphens into one space       
            str = Regex.Replace(str, @"[\s-]+", " ").Trim();

            // trim it
            str = str.Trim();

            // hyphens
            str = Regex.Replace(str, @"\s", "-");

            return str;
        }

        public static string RemoveAccent(this string text)
        {
            if (text.IsNullOrEmpty())
                return string.Empty;

            var bytes = Encoding.GetEncoding("Cyrillic").GetBytes(text);
            return Encoding.ASCII.GetString(bytes);
        }

        public static bool Contains(this string source, string toCheck, StringComparison stringComparison)
        {
            return source.IndexOf(toCheck, stringComparison) > -1;
        }
    }
}