﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Slam.Cms.Common
{
	public static class EnumerableExtensions
	{
		public static IEnumerable<T> GetRandom<T>(this IEnumerable<T> @enum, int count)
		{
			Guard.NotNull(() => @enum);
			Random random = new Random(DateTime.UtcNow.Millisecond);
			var randomSortDictionary = new Dictionary<double, T>();
			foreach (var elem in @enum)
				randomSortDictionary[random.NextDouble()] = elem;
			return randomSortDictionary.OrderBy(i => i.Key).Take(count).Select(i => i.Value);
		}

		public static T GetRandom<T>(this IEnumerable<T> @enum)
		{
			Guard.NotNull(() => @enum);
			Random random = new Random(DateTime.UtcNow.Millisecond);
			var item = random.Next(@enum.Count());
			return @enum.ElementAt(item);
		}

		public static IEnumerable<T> OrderBy<T>(this IEnumerable<T> list, string sortExpression)
		{
			IOrderedEnumerable<T> orderedResult = default(IOrderedEnumerable<T>);
			string[] expressionParts = sortExpression.Trim().Split(' ');
			bool descending = false;
			string propertyName = "";

			if (expressionParts.Length > 0 && !String.IsNullOrEmpty(expressionParts[0]))
			{
				propertyName = expressionParts[0];
				if (expressionParts.Length > 1)
				{
					descending = expressionParts[1].ToUpper().StartsWith("DESC");
				}

				PropertyInfo property = typeof(T).GetProperty(propertyName);
				if (property != null)
				{
					if (descending)
					{
						orderedResult = list.OrderByDescending(i => property.GetValue(i, null));
					}
					else
					{
						orderedResult = list.OrderBy(i => property.GetValue(i, null));
					}
				}
			}

			return orderedResult;
		}
	}
}
