﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Slam.Cms.Common
{
    public static class Generic
    {
        public static bool In<T>(this T source, params T[] list)
        {
            if (source == null) throw new ArgumentNullException("source");
            return list.Contains(source);
        }

        public static int Times(this int instance, Action<int> action)
        {
            for (var i = 0; i < instance; i++)
                action(i);
            return instance;
        }
    }
}
