﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Slam.Cms.Common
{
	public static class ProcessExtensions
	{
		[DllImport("kernel32.dll", CharSet = CharSet.Auto)]
		static extern IntPtr GetModuleHandle(string moduleName);

		[DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
		static extern IntPtr GetProcAddress(IntPtr hModule, [MarshalAs(UnmanagedType.LPStr)]string procName);

		[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		static extern bool IsWow64Process(IntPtr hProcess, out bool wow64Process);

		public static bool Is64Bit(this Process process)
		{	
			bool is64Bit = false;
			if (IntPtr.Size == 8)
			{
				is64Bit = true;
			}
			else
			{
				bool result;
				is64Bit = ((DoesWin32MethodExist("kernel32.dll", "IsWow64Process") && IsWow64Process(process.Handle, out result)) && result);
			}
			return is64Bit;
		}
		
		private static bool DoesWin32MethodExist(string moduleName, string methodName)
		{
			IntPtr moduleHandle = GetModuleHandle(moduleName);
			if (moduleHandle == IntPtr.Zero)
				return false;
			return (GetProcAddress(moduleHandle, methodName) != IntPtr.Zero);
		}  
	}
}
