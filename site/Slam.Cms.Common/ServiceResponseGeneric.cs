﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Slam.Cms.Common
{
	public class ServiceResponse<T> : ServiceResponse
	{
		public ServiceResponse()
		{
			MessageCode = ServiceResponseMessageCode.None;
		}

		private T _response;
		public new T Response
		{
			get
			{
				return _response;
			}
			set
			{
				_response = value;
				base.Response = value;
			}
		}
	}
}
