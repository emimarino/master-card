﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace Slam.Cms.Common.Internal
{
	internal class PostScriptImageExtracter
	{   
		[DllImport("gsdll32.dll", EntryPoint = "gsapi_new_instance")]
		private static extern int CreateApiInstance(out IntPtr pinstance, IntPtr caller_handle);

		[DllImport("gsdll32.dll", EntryPoint = "gsapi_init_with_args")]
		private static extern int InitApi(IntPtr instance, int argc, string[] argv);

		[DllImport("gsdll32.dll", EntryPoint = "gsapi_exit")]
		private static extern int ExitApi(IntPtr instance);

		[DllImport("gsdll32.dll", EntryPoint = "gsapi_delete_instance")]
		private static extern void DeleteApiInstance(IntPtr instance);

		[DllImport("gsdll64.dll", EntryPoint = "gsapi_new_instance")]
		private static extern int CreateApiInstance64(out IntPtr pinstance, IntPtr caller_handle);

		[DllImport("gsdll64.dll", EntryPoint = "gsapi_init_with_args")]
		private static extern int InitApi64(IntPtr instance, int argc, string[] argv);

		[DllImport("gsdll64.dll", EntryPoint = "gsapi_exit")]
		private static extern int ExitApi64(IntPtr instance);

		[DllImport("gsdll64.dll", EntryPoint = "gsapi_delete_instance")]
		private static extern void DeleteApiInstance64(IntPtr instance);

		public static void SaveImage(string documentPath, int page, string imagePath)
		{
			SaveImages(documentPath, page, page, imagePath);
		}

		public static void SaveImages(string documentPath, int firstPage, int lastPage, string imagePath)
		{
			string[] arguments;
			if (documentPath.Contains(".eps"))
			{
				arguments = new[]
				{   
					"-q",						//Prevent GhostScript from writing to standard output
					"-dSAFER",			//Run in safe mode
					"-dBATCH",					//Prevent GhostScript from going into interactive mode
					"-dNOPAUSE",				//Do not pause for each page
					"-dNOPROMPT",				//Disable prompts for user interaction           
					
					"-dAlignToPixels=0",
					"-dGridFitTT=0",
					"-sDEVICE=jpeg",
					"-dEPSCrop",				//Crop EPS
					"-dTextAlphaBits=4",
					"-dGraphicsAlphaBits=4",
					
					String.Format("-sOutputFile={0}", imagePath),
					documentPath
				};
			}
			else
			{
				arguments = new[]
				{
					 "-q",						//Prevent GhostScript from writing to standard output
					"-dSAFER",			//Run in safe mode
					"-dBATCH",					//Prevent GhostScript from going into interactive mode
					"-dNOPAUSE",				//Do not pause for each page
					"-dNOPROMPT",				//Disable prompts for user interaction           
					
					String.Format("-dFirstPage={0}", firstPage),
					String.Format("-dLastPage={0}", lastPage),   
					
					"-dAlignToPixels=0",
					"-dGridFitTT=0",
					"-sDEVICE=jpeg",
					"-dTextAlphaBits=4",
					"-dGraphicsAlphaBits=4",
					
					String.Format("-sOutputFile={0}", imagePath),
					documentPath
				};
			}

			IntPtr ghostScriptPointer;

			if (Process.GetCurrentProcess().Is64Bit())
			{
				CreateApiInstance64(out ghostScriptPointer, IntPtr.Zero);
				InitApi64(ghostScriptPointer, arguments.Length, arguments);

				ExitApi64(ghostScriptPointer);
				DeleteApiInstance64(ghostScriptPointer);
			}
			else
			{
				CreateApiInstance(out ghostScriptPointer, IntPtr.Zero);
				InitApi(ghostScriptPointer, arguments.Length, arguments);

				ExitApi(ghostScriptPointer);
				DeleteApiInstance(ghostScriptPointer);
			}
			
		}
	}
}
