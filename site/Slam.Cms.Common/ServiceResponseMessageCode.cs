﻿namespace Slam.Cms.Common
{
    public enum ServiceResponseMessageCode
    {
        Error,
        Warning,
        Information,
        None
    }
}