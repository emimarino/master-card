﻿namespace Slam.Cms.Common
{
    using System;
    using System.Web.Security;

    public interface IFormsAuthentication
    {
        string FormsCookieName { get; }
        string FormsCookiePath { get; }
        string Encrypt(FormsAuthenticationTicket ticket);
        FormsAuthenticationTicket Decrypt(string encryptedTicket);
        void SignOut();
        void RedirectToLoginPage();
        void RedirectToLoginPage(string extraQueryString);
        void SetAuthCookie(string userName, bool createPersistentCookie);
    }
}