﻿namespace Slam.Cms.Common
{
    using System;
    using System.Web.Security;

    public class FormsAuthenticationWrapper : IFormsAuthentication
    {
        public string FormsCookieName
        {
            get { return FormsAuthentication.FormsCookieName; }
        }

        public string FormsCookiePath
        {
            get { return FormsAuthentication.FormsCookiePath; }
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }

        public string Encrypt(FormsAuthenticationTicket ticket)
        {
            return FormsAuthentication.Encrypt(ticket);
        }

        public FormsAuthenticationTicket Decrypt(string encryptedTicket)
        {
            return FormsAuthentication.Decrypt(encryptedTicket);
        }

        public void RedirectToLoginPage()
        {
            FormsAuthentication.RedirectToLoginPage();
        }

        public void RedirectToLoginPage(string extraQueryString)
        {
            FormsAuthentication.RedirectToLoginPage(extraQueryString);
        }

        public void SetAuthCookie(string userName, bool createPersistentCookie)
        {
            FormsAuthentication.SetAuthCookie(userName, createPersistentCookie);
        }
    }
}