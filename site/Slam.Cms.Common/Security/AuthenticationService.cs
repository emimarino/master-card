﻿using System;
using System.Web;
using Slam.Cms.Common.Interfaces;

namespace Slam.Cms.Common.Security
{
    public class AuthenticationService
    {
        private readonly HttpContextBase _httpContext;
        private readonly IPreviewMode _previewMode;

        public string SlamAuthentication
        {
            get
            {
                return _httpContext.Request.Url.Host + "." + "SlamAuthentication";
            }
        }

        public AuthenticationService(HttpContextBase httpContext, IPreviewMode previewMode)
        {
            _httpContext = httpContext;
            _previewMode = previewMode;
        }

        public void CreateOrUpdateSlamPreviewModeCookie()
        {
            _previewMode.Enabled = true;
        }

        public void RemoveSlamPreviewModeCookie()
        {
            _previewMode.Enabled = false;
        }

        public void CreateOrUpdateSlamAuthenticationCookie()
        {
            var username = _httpContext.User.Identity.Name.Contains("|") ? _httpContext.User.Identity.Name.Split('|')[1] : HttpContext.Current.User.Identity.Name;
            CreateOrUpdateSlamAuthenticationCookie(username);
        }

        public void CreateOrUpdateSlamAuthenticationCookie(string username)
        {
            _httpContext.Response.AppendCookie(new HttpCookie((SlamAuthentication), EncryptionService.EncryptWithTimestamp(username)));
        }

        public void RemoveSlamAuthenticationCookie()
        {
            var slamAuthCookie = new HttpCookie(SlamAuthentication, "");
            slamAuthCookie.Expires = DateTime.Now.AddDays(-1);
            _httpContext.Response.AppendCookie(slamAuthCookie);
        }

        public string GetSlamUserName()
        {
            return GetSlamUserName(new TimeSpan(3, 0, 0));
        }

        public string GetSlamUserName(TimeSpan maximumValidAge)
        {
            var slamAuthCookie = _httpContext.Request.Cookies[SlamAuthentication] ?? _httpContext.Response.Cookies[SlamAuthentication];
            if (slamAuthCookie != null)
            {
                var userName = EncryptionService.DecryptWithExpiration(slamAuthCookie.Value, maximumValidAge);
                if (userName.Length > 0)
                {
                    CreateOrUpdateSlamAuthenticationCookie(userName);
                }

                return userName;
            }

            return string.Empty;
        }
    }
}