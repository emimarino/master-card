﻿using Mastercard.MarketingCenter.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

namespace Slam.Cms.Common
{
    [DebuggerStepThrough]
    public static class Ensure
    {
        /// <summary>
        /// Formats the specified resource string using <see cref="M:CultureInfo.CurrentCulture"/>.
        /// </summary>
        /// <param name="format">A composite format string.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        /// <returns>The formatted string.</returns>
        public static string Format(string format, params object[] args)
        {
            return string.Format(CultureInfo.CurrentCulture, format, args);
        }
            
        public static void ArgumentNotNullOrEmpty(string value, string parameterName, string message = "")
        {
            if (value.IsNullOrEmpty())
            {
                throw new ArgumentNullException(parameterName, message);
            }
        }

        public static void ArgumentNotNull(object value, string parameterName, string message = "")
        {
            if (value == null)
            {
                throw new ArgumentNullException(parameterName, message);
            }
        }

        public static void ArgumentNotEmpty<T>(IEnumerable<T> collection, string parameterName, string message = "")
        {
            if (collection == null)
            {
                throw new ArgumentNullException(parameterName, message);
            }
            if (!collection.Any())
            {
                throw new ArgumentOutOfRangeException(parameterName, message);
            }
        }

        public static void ArgumentNotOutOfRangeExclusive(int value, int minValue, int maxValue, string parameterName, string message = "")
        {
            if (value < minValue || value > maxValue)
            {
                throw new ArgumentOutOfRangeException(parameterName, value, message);
            }
        }

        public static void PropertyNotNull(object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
        }
    }
}