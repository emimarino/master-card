﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Slam.Cms.Common
{
   public class ImageSize
    {
       public int Height { get; set; }
       public int Width { get; set; }
    }
}
