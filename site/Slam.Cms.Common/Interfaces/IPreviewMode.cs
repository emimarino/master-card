﻿namespace Slam.Cms.Common.Interfaces
{
    public interface IPreviewMode
    {
        bool Enabled { get; set; }
    }
}